/*
 * Created on Sep 30, 2005
 *
 */
package simple.chart;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.ui.TextAnchor;
import org.jfree.util.TableOrder;
import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.results.DataGenre;
import simple.results.Results;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public class GenericChartType extends AbstractChartType {
    private static final Log log = Log.getLog();
    protected Class<? extends ResultsDataset> datasetClass;
    protected Method createMethod;
    protected String[] arguments;
    
	public enum GenericAxisType {
		CLUSTER, CLUSTER_LEGEND, VALUE, VALUE_LEGEND, CLUSTER_AND_VALUE
	};
    public static class GenericAxis extends BasicAxis {
		protected final String clusterName;
		protected final String valueName;
		protected final GenericAxisType genericAxisType;

		public GenericAxis(String name, DataGenre[] acceptedGenre, boolean required, String clusterName, String valueName, GenericAxisType genericAxisType) {
            super(name, acceptedGenre, required);
			this.clusterName = clusterName;
			this.valueName = valueName;
            this.genericAxisType = genericAxisType;
        }
        public GenericAxisType getGenericAxisType() {
            return genericAxisType;
        }

		public String getClusterName() {
			return clusterName;
		}

		public String getValueName() {
			return valueName;
		}
    }
    public GenericChartType(String name, Class<? extends ResultsDataset> datasetClass, Method createMethod, String[] arguments) {
        super(name);
        this.datasetClass = datasetClass;
        this.createMethod = createMethod;
        this.arguments = arguments;
    }

	public void addAxis(String name, DataGenre[] acceptedGenre, boolean required, String clusterName, String valueName, GenericAxisType genericAxisType) {
		addAxis(new GenericAxis(name, acceptedGenre, required, clusterName, valueName, genericAxisType));
    }
            
    public String[] getArguments() {
        return arguments;
    }

    public Method getCreateMethod() {
        return createMethod;
    }

    public Class<? extends ResultsDataset> getDatasetClass() {
        return datasetClass;
    }

    public ChartMaker newChartMaker() {
        return new AbstractChartMaker(this) {
            protected boolean useLegend = false;
			protected String legendLabel = null;
            protected ResultsDataset dataset = null;
            public JFreeChart makeChart(Results results) throws ChartCreationException {
                try {
                    dataset = datasetClass.newInstance();
                    useLegend = false;
                    legendLabel = null;
                    List<Format> valueFormats = new ArrayList<Format>();
                    for(Axis axis: getAxes()) {
                        if(axis instanceof GenericAxis) {
                            GenericAxis ga = (GenericAxis)axis;
                            AxisData axisData = getAxisData(ga.getName());
                            if(axisData != null) {
                            	axisData.getReader().trackAggregates(results);
                                switch(ga.genericAxisType) {
                                    case CLUSTER_LEGEND:
                                        if(axisData.getReader() != null) useLegend = true; 
                                        if(axisData.getLabel() != null) legendLabel = axisData.getLabel();
                                        //fall through to CLUSTER
                                    case CLUSTER:
										if(ga.clusterName != null)
											dataset.addCluster(ga.clusterName, axisData.getReader());
                                        break;
                                    case VALUE_LEGEND:
                                        if(axisData.getReader() != null) useLegend = true; 
                                        //if(axisData.getLabel() != null) legendLabel = axisData.getLabel();
                                        //fall through to VALUE
                                    case VALUE:
										dataset.addValueColumn(ga.valueName, axisData.getLabel(), axisData.getReader());
										valueFormats.add(axisData.getFormat());
										break;
									case CLUSTER_AND_VALUE:
										if(ga.clusterName != null)
											dataset.addCluster(ga.clusterName, axisData.getReader());
										dataset.addValueColumn(ga.valueName, axisData.getLabel(), axisData.getReader());
                                        valueFormats.add(axisData.getFormat());
										break;
                                }
                            }
                        }
                    }
                    Object[] args = new Object[arguments.length];
                    for(int i = 0; i < arguments.length; i++) {
                        args[i] = getValueForArgument(arguments[i]);
                    }
                    dataset.populate(results);
                    JFreeChart chart = (JFreeChart)createMethod.invoke(null, args);
					/*
					if(useLegend && legendLabel != null && chart.getLegend() instanceof StandardLegend) {
						StandardLegend legend = (StandardLegend)chart.getLegend();
					    legend.setTitle(legendLabel);
					}*/
                    String valueLabels;
                    try {
						valueLabels = ConvertUtils.getString(getProperty("showValueLabels"), false);
					} catch (ConvertException e) {
						log.warn(e);
						valueLabels = null;
					}
                    if(valueLabels != null && chart.getPlot() instanceof CategoryPlot) {
                    	CategoryPlot cp = (CategoryPlot)chart.getPlot();
						/* XXX:Do we still need this?
						for(int i = 0; i < valueFormats.size(); i++) {
							Format f = valueFormats.get(i);
							CategoryLabelGenerator gen;
							if(f instanceof NumberFormat) gen = new StandardCategoryLabelGenerator(StandardCategoryLabelGenerator.DEFAULT_LABEL_FORMAT_STRING, (NumberFormat)f);
							else if(f instanceof DateFormat) gen = new StandardCategoryLabelGenerator(StandardCategoryLabelGenerator.DEFAULT_LABEL_FORMAT_STRING, (DateFormat)f);
							else gen = new StandardCategoryLabelGenerator();
							if(valueFormats.size() == 1) {
								cp.getRenderer().setBaseLabelGenerator(gen);
							} else {
								cp.getRenderer().setSeriesLabelGenerator(i, gen);
							}
						}*/
                        cp.getRenderer().setBaseItemLabelsVisible(true);
						/* XXX:Do we still need this?
						if(cp.getRenderer() instanceof BarRenderer) {
							ItemLabelPosition pos = createItemLabelPosition(valueLabels);
							ItemLabelPosition neg = new ItemLabelPosition(
									ItemLabelAnchor.getVerticalOpposite(pos.getItemLabelAnchor()), 
									TextAnchor.getVerticalOpposite(pos.getTextAnchor()),
									TextAnchor.getVerticalOpposite(TextAnchor.getHorizontalOpposite(pos.getRotationAnchor())),
									pos.getAngle());
							((BarRenderer)cp.getRenderer()).setPositiveItemLabelPosition(pos);
							((BarRenderer)cp.getRenderer()).setNegativeItemLabelPosition(neg);
							((BarRenderer)cp.getRenderer()).setPositiveItemLabelPositionFallback(pos);
							((BarRenderer)cp.getRenderer()).setNegativeItemLabelPositionFallback(neg);
						}
						*/
                    }
                    return chart;
                } catch (InstantiationException e) {
                    throw new ChartCreationException(e);
                } catch (IllegalAccessException e) {
                    throw new ChartCreationException(e);
                } catch (IllegalArgumentException e) {
                    throw new ChartCreationException(e);
                } catch (InvocationTargetException e) {
                    throw new ChartCreationException(e);
                }
            }

            protected Object getValueForArgument(String argumentName) {
                if("title".equalsIgnoreCase(argumentName)) return getTitle();
                if("dataset".equalsIgnoreCase(argumentName)) return dataset;
                if("orientation".equalsIgnoreCase(argumentName)) return getOrientation();
                if("legend".equalsIgnoreCase(argumentName)) return useLegend;
                if("tooltips".equalsIgnoreCase(argumentName)) return isTooltips();
                if("urls".equalsIgnoreCase(argumentName)) return isUrls();
                if("tableorder".equalsIgnoreCase(argumentName)) {
                    //XXX: I am not sure which orientation should return which order
                    if(getOrientation().equals(PlotOrientation.VERTICAL))
                        return TableOrder.BY_ROW;
                    else 
                        return TableOrder.BY_COLUMN;
                }
                if(argumentName.endsWith("Label")) {
                    String axisName = argumentName.substring(0,argumentName.length() - 5);
                    AxisData axisData = getAxisData(axisName);
                    if(axisData != null && axisData.getLabel() != null) 
                        return axisData.getLabel();
                }
                log.warn("Argument '" + argumentName + "' is unknown. Using null for it");
                return null;
            }           
        };
    }
    protected ItemLabelPosition createItemLabelPosition(String valueLabels) {
    	String[] parts = StringUtils.split(valueLabels, StringUtils.STANDARD_DELIMS, true);
    	ItemLabelAnchor labelAnc = (parts.length > 0 && parts[0] != null ? ChartUtils.getItemLabelAnchor(parts[0].trim().toUpperCase()) : null);
    	TextAnchor textAnc = (parts.length > 1 && parts[1] != null ? ChartUtils.getTextAnchor(parts[1].trim().toUpperCase()) : null);
    	TextAnchor rotAnc = (parts.length > 2 && parts[2] != null ? ChartUtils.getTextAnchor(parts[2].trim().toUpperCase()) : null);
    	double angle = 0;
    	if(parts.length > 3 && parts[3] != null && (parts[3]=parts[3].trim()).length() > 0)
    		try {
    			angle = Double.parseDouble(parts[3]);
    		} catch(NumberFormatException nfe) {
    			log.warn("Could not convert '" + parts[3] + "' to a number", nfe);
    		}
    	
    	return new ItemLabelPosition(
    			labelAnc != null ? labelAnc : ItemLabelAnchor.OUTSIDE12, 
    			textAnc != null ? textAnc :	TextAnchor.BOTTOM_CENTER,
    			rotAnc != null ? rotAnc : TextAnchor.CENTER,
    			angle);
	}

	protected static Method getFactoryMethod(String methodName, int paramCount) {
		Method method = ReflectionUtils.findMethod(ExtraChartTypeFactory.class, methodName, new Class<?>[paramCount]);
		if(method == null)
			method = ReflectionUtils.findMethod(org.jfree.chart.ChartFactory.class, methodName, new Class<?>[paramCount]);
		return method;
    }

	public static Object readXML(ObjectBuilder builder) {
		String[] arguments = builder.get("argument", String[].class);
		Method method = getFactoryMethod(builder.get("createMethod", String.class), arguments.length);
        GenericChartType gct = new GenericChartType(
                builder.get("name", String.class),
                builder.get("datasetClass", Class.class),
                method,
                arguments // TODO: arguments should read default values
            );
        //add axes
        for(ObjectBuilder axesBuilder : builder.getSubNodes("axis")) {
            gct.addAxis(
                    axesBuilder.get("name", String.class),
                    axesBuilder.get("genre", DataGenre[].class),
                    axesBuilder.get("required", boolean.class),
                    axesBuilder.get("cluster", String.class),
                    axesBuilder.get("value", String.class),
                    axesBuilder.get("axisType", GenericAxisType.class)
            );
        }
        return gct;
    }
    public void writeXML(String name, XMLBuilder builder) throws SAXException, ConvertException {
        if(name == null) name = "chart-type";
        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put("name", getName());
        attributes.put("datasetClass", getDatasetClass().getName());
        attributes.put("createMethod", getCreateMethod().getName());        
        builder.elementStart(name, attributes);
        builder.put("argument", getArguments()); //TODO: arguments should output default values
        //add axes
        for(Axis axis : getAxes()) {
            if(axis instanceof GenericAxis) {
                GenericAxis ga = (GenericAxis)axis;
                attributes.clear();
                attributes.put("name", ga.getName());
                attributes.put("genre", ConvertUtils.convert(String.class, ga.getAcceptedGenre()));
                attributes.put("required", ConvertUtils.convert(String.class, ga.isRequired()));
				attributes.put("cluster", ga.getClusterName());
				attributes.put("value", ga.getValueName());
                attributes.put("axisType", ConvertUtils.convert(String.class, ga.getGenericAxisType()));
                builder.elementStart("axis", attributes);
                builder.elementEnd("axis");
            } else {
                builder.put("axis", axis);
            }
        }
        builder.elementEnd(name);
    }
}
