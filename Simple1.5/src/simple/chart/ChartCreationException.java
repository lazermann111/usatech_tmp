/*
 * Created on Sep 26, 2005
 *
 */
package simple.chart;

public class ChartCreationException extends Exception {
	private static final long serialVersionUID = -93482001L;

	public ChartCreationException() {
        super();
    }

    public ChartCreationException(String message) {
        super(message);
    }

    public ChartCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChartCreationException(Throwable cause) {
        super(cause);
    }

}
