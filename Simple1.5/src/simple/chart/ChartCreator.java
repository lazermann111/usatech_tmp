/*
 * ChartCreator.java
 *
 * Created on December 31, 2001, 8:34 AM
 */

package simple.chart;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.beans.IndexedPropertyDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryTick;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.Tick;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.title.Title;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.Dataset;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.Size2D;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.util.concurrent.RunOnGetFuture;

/**
 *
 * @author  Brian S. Krug
 * @version
 */

public class ChartCreator {
    private static final simple.io.Log log = simple.io.Log.getLog();
    
    protected String yCaption = "";
    protected String xCaption = "";
    protected String title = "";
    protected boolean legendUsed = true;
    protected static class LazyHolder {
    	protected static boolean graphicsAvailable = initGraphicsAvailable();
    }
    protected double maximumValue;
    protected boolean maximumValueSet = false;
    protected double minimumValue;
    protected boolean minimumValueSet = false;
    protected NumberTickUnit numberTickUnit;
    protected String chartType = "bar";
    protected DateTickUnit dateTickUnit;

    protected static final Map<String,Object> factoryTypes = new ConcurrentHashMap<String,Object>();
    protected static final Future<Map<String,Method>> factoryMethods = new RunOnGetFuture<Map<String,Method>>() {
    	@Override
		public Map<String, Method> call(Object... params) throws Exception {
    		return createFactoryMethodsMap();
    	}
    };
    
    /** Holds value of property orientation. */
    private PlotOrientation orientation = PlotOrientation.VERTICAL;
    
    /** Holds value of property dataSize. */
    private Dimension dataSize;
    
    /** Holds value of property minBarWidth. */
    private int minBarWidth = 5;
    
    static {
        /*
        factoryMethods.put("area","createAreaChart");
        factoryMethods.put("bar","createBarChart");
        factoryMethods.put("area","createAreaChart");
        factoryMethods.put("area","createAreaChart");
        factoryMethods.put("area","createAreaChart");
        factoryMethods.put("area","createAreaChart");
         **/
    }
    
    /** Creates new ChartCreator */
    public ChartCreator() {
    }
    
    /** Getter for property yCaption.
     * @return Value of property yCaption.
     */
    public String getYCaption() {
        return yCaption;
    }
    
    /** Setter for property yCaption.
     * @param yCaption New value of property yCaption.
     */
    public void setYCaption(String yCaption) {
        this.yCaption = yCaption;
    }
    
    /** Getter for property xCaption.
     * @return Value of property xCaption.
     */
    public String getXCaption() {
        return xCaption;
    }
    
    /** Setter for property xCaption.
     * @param xCaption New value of property xCaption.
     */
    public void setXCaption(String xCaption) {
        this.xCaption = xCaption;
    }
    
    /** Getter for property title.
     * @return Value of property title.
     */
    public String getTitle() {
        return title;
    }
    
    /** Setter for property title.
     * @param title New value of property title.
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void createJPEG(Dataset dataset, java.io.OutputStream out) throws java.io.IOException {
        log.debug("Creating Chart as JPEG Output Stream");
        try {
            JFreeChart chart = createChart(dataset);
            Dimension size = calculateSize(chart);
            ChartUtilities.writeChartAsJPEG(out, chart, (int)size.getWidth(), (int)size.getHeight());
        } catch(IOException e) {
        	throw e;
        } catch(Throwable t) {
            log.warn("Exception occured while creating Chart as JPEG", t);
        }
    }
    
    public void createPNG(Dataset dataset, java.io.OutputStream out) throws java.io.IOException {
        log.debug("Creating Chart as PNG Output Stream");
        try {
            JFreeChart chart = createChart(dataset);
            Dimension size = calculateSize(chart);
            ChartUtilities.writeChartAsPNG(out, chart, (int)size.getWidth(), (int)size.getHeight());
        } catch(IOException e) {
        	throw e;
        } catch(Throwable t) {
            log.warn("Exception occured while creating Chart as PNG", t);
        }
    }
    
    public JFreeChart createChart(Dataset dataset) {        
        Method m = getFactoryMethod();
        if(m == null) throw new IllegalArgumentException("Could not find a factory method for chartType, '" + getChartType() + "'");
		Class<?>[] paramTypes = m.getParameterTypes();
        Object[] paramValues = new Object[paramTypes.length];
        Object[] allValues = new Object[] {
            getTitle(), getXCaption(), getYCaption(), dataset, getOrientation(), new Boolean(isLegendUsed()), Boolean.TRUE, Boolean.TRUE
        };
        //this works for now -- its not terribly scientific
        int i = 0;
        for(int k = 0; i < paramTypes.length && k < allValues.length; k++) {
            if(ConvertUtils.convertToWrapperClass(paramTypes[i]).isAssignableFrom(allValues[k].getClass())) 
                paramValues[i++] = allValues[k];
        }
        JFreeChart chart;
        try {
            chart = (JFreeChart) m.invoke(null, paramValues);
        } catch(IllegalAccessException iae) {
            throw new RuntimeException("Creating the JFreeChart cause an exception", iae);
        } catch(java.lang.reflect.InvocationTargetException ite) {
            throw new RuntimeException("Creating the JFreeChart cause an exception", ite);
        }
        
        if(dateTickUnit != null) {
            //((DateAxis) chart.getXYPlot().getDomainAxis().setTickUnit(dateTickUnit);
            //((ValueAxis) chart.getPlot().getHorizontalAxis()).setAutoTickUnitSelection(false);
        }
        //if(minimumValueSet) ((ValueAxis) chart.getPlot().getVerticalAxis()).setMinimumAxisValue(minimumValue);
        //if(maximumValueSet) ((ValueAxis) chart.getPlot().getVerticalAxis()).setMaximumAxisValue(maximumValue);
        //if(numberTickUnit != null) ((NumberAxis) chart.getPlot().getVerticalAxis()).setTickUnit(numberTickUnit);
        return chart;
    }
    
    protected java.awt.Dimension calculateSize(JFreeChart chart) {
        //determine chart size
        log.debug("Calculating Chart Size...");
        Map<Axis,RectangleEdge> axes = new java.util.HashMap<Axis,RectangleEdge>();
        //load all axes
        Plot plot = chart.getPlot(); 
        try {
            PropertyDescriptor[] pds = ReflectionUtils.getPropertyDescriptors(plot.getClass());
            for(int i = 0; i < pds.length; i++) {
                if(pds[i] instanceof IndexedPropertyDescriptor) {
                    IndexedPropertyDescriptor ipd = (IndexedPropertyDescriptor)pds[i];
                    if(Axis.class.isAssignableFrom(ipd.getIndexedPropertyType()) && ipd.getIndexedReadMethod() != null) {
                        Integer[] param = new Integer[] { new Integer(0) };
                        Method edgeMethod = null;
                        try { 
                             edgeMethod = plot.getClass().getMethod(ipd.getIndexedReadMethod().getName() + "Edge", ipd.getIndexedReadMethod().getParameterTypes());
                        } catch(NoSuchMethodException nsme) {
                        }
                        Axis lastAxis = null;
                        while(true) try {
                            log.debug("Getting axis for method '" + ipd.getIndexedReadMethod().getName() + "' with index = " + param[0]);
                            Axis axis = (Axis)ipd.getIndexedReadMethod().invoke(plot, (Object[])param);
                            if(axis == null || (lastAxis != null && lastAxis.equals(axis))) break;
                            param[0] = new Integer(param[0].intValue() + 1);
                            if(axes.containsKey(axis)) break; //continue;
                            RectangleEdge edge = null;
                            if(edgeMethod != null && RectangleEdge.class.isAssignableFrom(edgeMethod.getReturnType()))
                                edge = (RectangleEdge)edgeMethod.invoke(plot, (Object[])param);                        
                            if(edge == null) edge = RectangleEdge.BOTTOM; // TODO: should account for name of property
                            axes.put(axis, edge);                        
                        } catch(java.lang.reflect.InvocationTargetException ite) {
                            if(!(ite.getCause() instanceof IllegalArgumentException)) //assume that index was too high and just end loop
                                log.debug("Could not load the axis for property '" + pds[i].getName() + "'", ite.getCause());
                            break;                            
                        } catch(IllegalArgumentException e) {
                            log.debug("Could not load the axis for property '" + pds[i].getName() + "'", e);
                            break;
                        } catch (IllegalAccessException e) {
                            log.debug("Could not load the axis for property '" + pds[i].getName() + "'", e);
                        }
                    }
                } else {
                    if(Axis.class.isAssignableFrom(pds[i].getPropertyType()) && pds[i].getReadMethod() != null) try {
                        log.debug("Getting axis for method '" + pds[i].getReadMethod().getName() + "'");
                        Axis axis = (Axis)pds[i].getReadMethod().invoke(plot, (Object[])null);
                        if(axes.containsKey(axis)) continue;
                        RectangleEdge edge = null;
                        Method m = null;
                        try {
                            m = plot.getClass().getMethod(pds[i].getReadMethod().getName() + "Edge", (Class[])null);
                        } catch(NoSuchMethodException nsme) {
                        }
                        if(m != null && RectangleEdge.class.isAssignableFrom(m.getReturnType()))
                            edge = (RectangleEdge)m.invoke(plot, (Object[])null);
                        if(edge == null) edge = RectangleEdge.BOTTOM; // TODO: should account for name of property
                        axes.put(axis, edge);
                    } catch(IllegalArgumentException e) {
                        log.debug("Could not load the axis for property '" + pds[i].getName() + "'", e);
                    } catch (IllegalAccessException e) {
                        log.debug("Could not load the axis for property '" + pds[i].getName() + "'", e);
                    } catch (InvocationTargetException e) {
                        log.debug("Could not load the axis for property '" + pds[i].getName() + "'", e);
                    }
                }
            }
        } catch(IllegalArgumentException e) {
            log.debug("Could not load axes into collection", e);
        } catch (IntrospectionException e) {
            log.debug("Could not load axes into collection", e);
        }
        log.debug("Found " + axes.size() + " axes");
        java.awt.image.BufferedImage image = new java.awt.image.BufferedImage(1 , 1, java.awt.image.BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = image.createGraphics();
        
        //determine data size
        Dimension dataSize = calculateDataSize(chart.getPlot(), g2);
        double width = dataSize.getWidth();
        double height = dataSize.getHeight();
        log.debug("Size of Data = " + dataSize.width + ", " + dataSize.height);        
        
        // estimate size of each axis and apply it appropriately
		for(Map.Entry<Axis, RectangleEdge> entry : axes.entrySet()) {
			Axis axis = entry.getKey();
			RectangleEdge edge = entry.getValue();
            Dimension sz = calculateAxisSize(axis, edge, dataSize, g2);
            if(RectangleEdge.isTopOrBottom(edge)) {
                width = Math.max(width, sz.getWidth());
                height += sz.getHeight();
            } else if(RectangleEdge.isLeftOrRight(edge)) {
                width += sz.getWidth();
                height = Math.max(height, sz.getHeight());
            }
            log.debug("Size of Axis (" + axis + ") = " + sz.width + ", " + sz.height);        
        }
        log.debug("Max Axis Size = " + width + ", " + height);
//        if(width == 0.0) width = 100.0;
//        if(height == 0.0) height = 100.0;        
         
        //store the plot size
//        if(plotWidth == 0.0) plotWidth = 100.0;
//        if(plotHeight == 0.0) plotHeight = 100.0;        
                
        //add title
        final float startHeight = 1.0f;
        TextTitle tt = chart.getTitle();
		double titleWidth = tt.getWidth(); // tt.getPreferredWidth(g2, startHeight);
		double titleHeight = tt.getHeight(); // tt.getPreferredHeight(g2, (float)titleWidth);
        width = Math.max(width, titleWidth);
        height += titleHeight;
        log.debug("Title Size = " + titleWidth + ", " + titleHeight);
        
        //add subtitles
		for(Iterator<?> iter = chart.getSubtitles().iterator(); iter.hasNext();) {
            Title title = (Title)iter.next();
			double w = title.getWidth(); // title.getPreferredWidth(g2, startHeight);
			double h = title.getHeight(); // title.getPreferredHeight(g2, (float)w);
            if(RectangleEdge.isTopOrBottom(title.getPosition())) {
                width = Math.max(width, w);
                height += h;
            } else if(RectangleEdge.isLeftOrRight(title.getPosition())) {
                width += w;
                height = Math.max(height, h);
            }                    
        }
        
        // add legend
		/*
		if(chart.getLegend() instanceof StandardLegend) {
		    Dimension legendSize = calculateLegendSize((StandardLegend)chart.getLegend(), width, g2);
		    int anchor = chart.getLegend().getAnchor();
		    if((anchor & Legend.EAST) != 0 || (anchor & Legend.WEST) != 0) {
		        width = Math.max(width, legendSize.getWidth());
		        height += legendSize.getHeight();
		        log.debug("Legend anchor = RIGHT or LEFT");
		    } else if((anchor & Legend.NORTH) != 0 || (anchor & Legend.SOUTH) != 0) {
		        width = Math.max(width, legendSize.getWidth());
		        height += legendSize.getHeight();
		        //XXX: not sure which way this should be
		        log.debug("Legend anchor = TOP or BOTTOM");
		    }
		    log.debug("Size of Legend = " + legendSize.width + ", " + legendSize.height);
		}*/
		if(chart.getLegend() != null) {
			Size2D legendSize = chart.getLegend().arrange(g2);
			if(RectangleEdge.isLeftOrRight(chart.getLegend().getPosition())) {
				width = Math.max(width, legendSize.getWidth());
				height += legendSize.getHeight();
				log.debug("Legend anchor = RIGHT or LEFT");
			} else if(RectangleEdge.isTopOrBottom(chart.getLegend().getPosition())) {
				width = Math.max(width, legendSize.getWidth());
				height += legendSize.getHeight();
				// XXX: not sure which way this should be
				log.debug("Legend anchor = TOP or BOTTOM");
			}
			log.debug("Size of Legend = " + legendSize.width + ", " + legendSize.height);
		}
        g2.dispose();
		RectangleInsets chartInsets = chart.getPlot().getInsets();
		width += chartInsets.getLeft() + chartInsets.getRight(); // insets
		height += chartInsets.getTop() + chartInsets.getBottom(); // insets
        log.debug("Final Size of Chart = " + width + ", " + height);
        return new java.awt.Dimension((int)Math.ceil(width), (int)Math.ceil(height));
    }

	/*
	protected Dimension calculateLegendSize(StandardLegend legend, double availableWidth, Graphics2D g2) {
	    //calc legend title size
	    Dimension titleSize = calculateLabelSize(legend.getTitle(), 0.0, new Insets(0, 0, 0, 0), legend.getTitleFont(), g2); 
	    double width = 0.0;
	    double height = 0.0;
	    double rowWidth = 0.0;
	    double rowHeight = 0.0;
	    boolean horizontal = (legend.getAnchor() & (1 << 0)) != 0;
	    availableWidth = Math.max(availableWidth, 100.0); // set a minimum
	    if(horizontal) { // title is on side
	        availableWidth = Math.max(availableWidth, titleSize.getWidth()); 
	        rowWidth = titleSize.getWidth() + legend.getOuterGap().getLeftSpace(availableWidth); 
	        rowHeight = titleSize.getHeight();
	    } else {//title is on top
	        availableWidth = Math.max(availableWidth, titleSize.getWidth()); 
	        height = titleSize.getHeight();
	        width = titleSize.getWidth();
	    }
	    
	    // calc items size - this is going to be quite a guess 
	    FontMetrics fm = g2.getFontMetrics(legend.getItemFont());
	    LegendItemCollection legendItems = legend.getChart().getPlot().getLegendItems();
	    int innerGap = 2; 
	    boolean startingNewRow = true;
	    for (int i = 0; i < legendItems.getItemCount(); i++) {
	        LegendItem item = legendItems.get(i);
	        LineMetrics lm = fm.getLineMetrics(item.getLabel(), g2);
	        // these formulas are strange - they were copied from StandardLegend internal calculations
	        double lineHeight = lm.getAscent() + lm.getDescent() + lm.getLeading();
	        double w = 2 * innerGap + 1.15f * lineHeight
	            + fm.getStringBounds(item.getLabel(), g2).getWidth()
	            + 0.5 * lm.getAscent();
	        double h = 2 * innerGap + lineHeight;            
	        //horizontal
	        if(horizontal) {       
	            if (!startingNewRow  && (w + rowWidth > availableWidth)) {
	                width = Math.max(width, rowWidth);
	                rowWidth = 0;
	                height += rowHeight;
	                i--;
	                startingNewRow = true;
	            }
	            else {
	                rowHeight = Math.max(rowHeight, h);
	                rowWidth += w;
	                startingNewRow = false;
	            }
	        } else {
	            height += h;
	            width = Math.max(width, w);
	        }
	    }

	    //horizontal
	    if(horizontal) {
	        width = Math.max(width, rowWidth);
	        height += rowHeight;
	    }
	    
	    return new Dimension((int)Math.ceil(legend.getOuterGap().getAdjustedWidth(width)),(int)Math.ceil(legend.getOuterGap().getAdjustedHeight(height)));
	}
	*/
    protected Dimension calculateDataSize(Plot plot, Graphics2D g2) {
        if(dataSize != null) return dataSize;
        int w = 300;
        int h = 300;
        // calc graphical representation (bars, lines, etc) 
        if(plot instanceof CategoryPlot) {
            CategoryItemRenderer r = ((CategoryPlot)plot).getRenderer();
            CategoryDataset dataset = ((CategoryPlot)plot).getDataset();
            int columns = dataset.getColumnCount();
            int rows = dataset.getRowCount();
            //may have to adjust if columns or rows = 0
            if(r instanceof org.jfree.chart.renderer.category.BarRenderer) {
                // min bar width * # of bars
                w = minBarWidth * columns * rows;
            } else {
                w = minBarWidth * columns;
            }
            if(PlotOrientation.HORIZONTAL.equals(((CategoryPlot)plot).getOrientation())) {
                int tmp = w;
                w = h;
                h = tmp;
            }
        }
        
        return new Dimension(w, h);
    }
    
    protected Dimension calculateAxisSize(Axis axis, RectangleEdge edge, Dimension dataSize, Graphics2D g2) {
        int h = 20; // initialize to the minimum
        int w = 20; // initialize to the minimum           
        
        // calc tick labels - must start with a guess for the dataArea so that Value Axes calculate the tick marks well
        // specifically setting the tick unit for the axis will eliminate this problem - but we can't do that here
		List<?> ticks = axis.refreshTicks(g2, new AxisState(), new Rectangle2D.Double(0, 0, dataSize.getWidth(), dataSize.getHeight()), edge);
        //if(axis.isTickLabelsVisible()) { 
        // NOTE: CategoryAxis objects report false for isTickLabelsVisible() so check if ticks are null instead        
        if(ticks != null) {
            RectangleInsets insets = axis.getTickLabelInsets();
            if(insets == null) insets = new RectangleInsets(2,2,2,2);
            Font font = axis.getTickLabelFont();
			for(Iterator<?> iter = ticks.iterator(); iter.hasNext();) {
                Tick tick = (Tick) iter.next();
                double angle = tick.getAngle();
                String text;
                if(tick instanceof CategoryTick) {
                    Object cat = ((CategoryTick)tick).getCategory();
                    text = cat == null ? "" : cat.toString();
                } else {
                    text = tick.getText();
                }
                /* XXX not sure if we need this
                if (edge == RectangleEdge.LEFT || edge == RectangleEdge.RIGHT) {
                    angle = angle - Math.PI / 2.0;
                }*/Dimension sz = calculateLabelSize(text, angle, insets, font, g2);
                log.debug("Size of Tick with text = '" + text + "' = " + sz.width + ", " + sz.height);
                h = Math.max(h, sz.height);
                w = Math.max(w, sz.width);
            }
        } 

        // calc axis label
        Dimension labelSize = calculateLabelSize(axis.getLabel(), axis.getLabelAngle(), axis.getLabelInsets(), axis.getLabelFont(), g2);
        log.debug("Size of Axis Label with text = '" + axis.getLabel() + "' = " + labelSize.width + ", " + labelSize.height);
                
        //calc total size
        double factor = 1.0;
        if(axis instanceof CategoryAxis) factor += ((CategoryAxis)axis).getLowerMargin();
        if(axis instanceof ValueAxis) factor += ((ValueAxis)axis).getLowerMargin();
        log.debug("Factor = " + factor);
        Dimension axisSize = null;
        if(RectangleEdge.isTopOrBottom(edge)) {
            axisSize = new Dimension(Math.max((int)Math.ceil(w * ticks.size() * factor), labelSize.width), h + labelSize.height); 
        } else if(RectangleEdge.isLeftOrRight(edge)) {
            axisSize = new Dimension(w + labelSize.width, Math.max((int)Math.ceil(h * ticks.size() * factor), labelSize.height)); 
        }
        
        return axisSize;
    }

    protected Dimension calculateLabelSize(String text, double angle, RectangleInsets insets, Font font, Graphics2D g2) {
        if(text == null || text.length() == 0) return new Dimension(0,0);
        FontMetrics fm = g2.getFontMetrics(font);        
        Rectangle2D bounds = fm.getStringBounds(text, g2);
        bounds.setRect(bounds.getX(), bounds.getY(),
                       bounds.getWidth() + insets.getLeft() + insets.getRight(),
                       bounds.getHeight() + insets.getTop() + insets.getBottom());
        double x = bounds.getCenterX();
        double y = bounds.getCenterY();
        AffineTransform transformer = AffineTransform.getRotateInstance(angle, x, y);
        bounds = transformer.createTransformedShape(bounds).getBounds2D();
        return new Dimension((int)Math.ceil(bounds.getWidth()), (int)Math.ceil(bounds.getHeight()));
    }
    
    public byte[] createPNG(Dataset dataset) throws java.io.IOException {
        java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
        createPNG(dataset, baos);
        baos.flush();
        return baos.toByteArray();
    }
    
    public static boolean isDisplayAvailable() {
    	return LazyHolder.graphicsAvailable;
    }
    protected static boolean initGraphicsAvailable() {
        class DisplayCheck implements Runnable {
            public boolean retVal = false;
            public void run() {
                try {
                    long start = System.currentTimeMillis();
                    java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment(); //ensure that graphs work
                    log.debug("Took " + (System.currentTimeMillis() - start) + " milliseconds to get graphics environment");
                    log.debug("Display is available with DISPLAY=" + System.getProperty("DISPLAY"));
                    retVal = true;
                } catch(Throwable e) {
                    log.warn("Display is NOT available", e);
                    retVal = false;
                }
            }
        }
        DisplayCheck dc = new DisplayCheck();
        Thread t = new Thread(dc);
        t.start();
        try {
            t.join(3000);
            if(t.isAlive()) t.interrupt();
        } catch(InterruptedException ie) {
            log.debug("Interupted display check thread", ie);
        }
        return dc.retVal;
     }
    
    /** Getter for property legendUsed.
     * @return Value of property legendUsed.
     */
    public boolean isLegendUsed() {
        return legendUsed;
    }
    
    /** Setter for property legendUsed.
     * @param legendUsed New value of property legendUsed.
     */
    public void setLegendUsed(boolean legendUsed) {
        this.legendUsed = legendUsed;
    }
    
    public static void retestDisplay() {
    	LazyHolder.graphicsAvailable = initGraphicsAvailable();
    }
    
    /** Getter for property maximumValue.
     * @return Value of property maximumValue.
     */
    public double getMaximumValue() {
        return maximumValue;
    }
    
    /** Setter for property maximumValue.
     * @param maximumValue New value of property maximumValue.
     */
    public void setMaximumValue(double maximumValue) {
        this.maximumValue = maximumValue;
        maximumValueSet = true;
    }
    
    /** Getter for property minimumValue.
     * @return Value of property minimumValue.
     */
    public double getMinimumValue() {
        return minimumValue;
    }
    
    /** Setter for property minimumValue.
     * @param minimumValue New value of property minimumValue.
     */
    public void setMinimumValue(double minimumValue) {
        this.minimumValue = minimumValue;
        minimumValueSet = true;
    }
    
    /** Getter for property numberTickUnit.
     * @return Value of property numberTickUnit.
     */
    public NumberTickUnit getNumberTickUnit() {
        return numberTickUnit;
    }
    
    /** Setter for property numberTickUnit.
     * @param numberTickUnit New value of property numberTickUnit.
     */
    public void setNumberTickUnit(NumberTickUnit numberTickUnit) {
        this.numberTickUnit = numberTickUnit;
    }
    
    /** Getter for property chartType.
     * @return Value of property chartType.
     */
    public String getChartType() {
        return chartType;
    }
    
    /** Setter for property chartType.
     * @param chartType New value of property chartType.
     */
    public void setChartType(String chartType) {
        this.chartType = chartType;
    }
    
    /** Getter for property dateTickUnit.
     * @return Value of property dateTickUnit.
     */
    public DateTickUnit getDateTickUnit() {
        return dateTickUnit;
    }
    
    /** Setter for property dateTickUnit.
     * @param dateTickUnit New value of property dateTickUnit.
     */
    public void setDateTickUnit(DateTickUnit dateTickUnit) {
        this.dateTickUnit = dateTickUnit;
    }
    
    public Method getFactoryMethod() {
        String type = getChartType();
        Object o = factoryTypes.get(type);
        if(o instanceof Method) return ((Method)o);
        String s;
        if(o == null) s = "create" + simple.text.StringUtils.capitalizeFirst(type) + "Chart";
        else s = o.toString();
        Method m;
		try {
			m = factoryMethods.get().get(s);
		} catch(InterruptedException e) {
			log.warn("Could not create factory methods map", e);
			m = null;
		} catch(ExecutionException e) {
			log.warn("Could not create factory methods map", e);
			m = null;
		}
        if(m != null) factoryTypes.put(type, m);
        return m;
    }
    
    protected static Map<String,Method> createFactoryMethodsMap() {
    	Map<String,Method> fmm = new java.util.HashMap<String,Method>();
        Method[] ms = ChartFactory.class.getMethods();
        for(int i = 0; i < ms.length; i++) {
            if(JFreeChart.class.isAssignableFrom(ms[i].getReturnType()) && Modifier.isStatic(ms[i].getModifiers())) {
            	fmm.put(ms[i].getName(), ms[i]);
            }
        }
        return fmm;
    }
    
	public Class<?> getDatasetInterface() {
        Method m = getFactoryMethod();
		Class<?>[] pt = m.getParameterTypes();
        for(int i = 0; i < pt.length; i++) if(Dataset.class.isAssignableFrom(pt[i])) return pt[i];
        return null;
    }
    /** Getter for property orientation.
     * @return Value of property orientation.
     *
     */
    public PlotOrientation getOrientation() {
        return this.orientation;
    }    
    
    /** Setter for property orientation.
     * @param orientation New value of property orientation.
     *
     */
    public void setOrientation(PlotOrientation orientation) {
        this.orientation = orientation;
    }
    
    /** Getter for property dataSize.
     * @return Value of property dataSize.
     *
     */
    public Dimension getDataSize() {
        return this.dataSize;
    }
    
    /** Setter for property dataSize.
     * @param dataSize New value of property dataSize.
     *
     */
    public void setDataSize(Dimension dataSize) {
        this.dataSize = dataSize;
    }
    
    /** Getter for property minBarWidth.
     * @return Value of property minBarWidth.
     *
     */
    public int getMinBarWidth() {
        return this.minBarWidth;
    }
    
    /** Setter for property minBarWidth.
     * @param minBarWidth New value of property minBarWidth.
     *
     */
    public void setMinBarWidth(int minBarWidth) {
        this.minBarWidth = minBarWidth;
    }
    
}
