/*
 * ResultsXYZDataset.java
 *
 * Created on December 29, 2003, 9:06 AM
 */

package simple.chart;

import org.jfree.data.DomainOrder;
import org.jfree.data.xy.XYZDataset;

/** Builds on ResultsDataset to implement the XYZDataset methods.
 * @author Brian S. Krug
 */
public class ResultsXYZDataset extends ResultsDataset implements XYZDataset {
	private static final long serialVersionUID = 910941234L;
	protected static final String[] CLUSTER_NAMES = new String[] {"series", "item"};
	protected static final double VALUE_FOR_NULL = Double.NaN;
    /** Creates a new instance of ResultsXYZDataset */
    public ResultsXYZDataset() {
        super(CLUSTER_NAMES);
    }
    
    public int getItemCount(int series) {
        return getClusterKeyCount(CLUSTER_NAMES[1]);
    }
    
    public int getSeriesCount() {
        return getClusterKeyCount(CLUSTER_NAMES[0]);
    }
    
    public String getSeriesName(int series) {
        return getClusterKey(CLUSTER_NAMES[0], series).toString();
    }
    
	protected Comparable<?>[] getKeys(int series, int item) {
		Comparable<?>[] keys = new Comparable[] {
            getClusterKey(CLUSTER_NAMES[0], series), 
            getClusterKey(CLUSTER_NAMES[1], item)
        };
        return keys;
    }
    
    public double getXValue(int series, int item) {
    	Number n = getX(series, item);
		return (n == null ? VALUE_FOR_NULL : n.doubleValue());
    }
    
    public double getYValue(int series, int item) {
    	Number n = getY(series, item);
		return (n == null ? VALUE_FOR_NULL : n.doubleValue());
    }
    
    public double getZValue(int series, int item) {
    	Number n = getZ(series, item);
		return (n == null ? VALUE_FOR_NULL : n.doubleValue());
    }

	/* (non-Javadoc)
	 * @see org.jfree.data.xy.XYDataset#getX(int, int)
	 */
	public Number getX(int series, int item) {
		return getDataValue(CLUSTER_NAMES, getKeys(series, item), "x");
    }
	
	/* (non-Javadoc)
	 * @see org.jfree.data.xy.XYDataset#getY(int, int)
	 */
	public Number getY(int series, int item) {
        return getDataValue(CLUSTER_NAMES, getKeys(series, item), "y");
    }
	
	/* (non-Javadoc)
	 * @see org.jfree.data.xy.XYZDataset#getZ(int, int)
	 */
	public Number getZ(int series, int item) {
        return getDataValue(CLUSTER_NAMES, getKeys(series, item), "z");
	}

	/* (non-Javadoc)
	 * @see org.jfree.data.xy.XYDataset#getDomainOrder()
	 */
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}

	@Override
	public Comparable getSeriesKey(int series) {
		return getClusterKey(CLUSTER_NAMES[0], series);
	}

	@Override
	public int indexOf(Comparable seriesKey) {
		return getClusterKeyIndex(CLUSTER_NAMES[0], seriesKey);
	}

}
