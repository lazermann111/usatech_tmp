/*
 * ResultsDataset.java
 *
 * Created on December 29, 2003, 9:06 AM
 */

package simple.chart;

import java.math.BigDecimal;
import java.text.Format;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.jfree.data.general.AbstractDataset;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.results.MultiColumnResultsReader;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.results.ResultsReader;
import simple.results.SingleColumnResultsReader;
import simple.util.CollectionComparator;

/** Implements many of the most common Dataset interfaces using a
 * <CODE>simple.results.Results</CODE> object to provide the data and various
 * methods to configure it.
 * @author Brian S. Krug
 */
public class ResultsDataset extends AbstractDataset {
	private static final long serialVersionUID = 910941234L;
	private static final Log log = Log.getLog();
    /*
    protected static class ValueColumnResultsReader implements ResultsReader {
        protected String[] valueColumns;

        public String getDisplayValue(Results results) {
            Comparable c = getSortValue(results);
            return (c == null ? "" : c.toString());
        }

        public Comparable getSortValue(Results results) {
            return null;
        }

        public boolean isGroupBeginning(Results results) {
            // TODO Auto-generated method stub
            return false;
        }

        public boolean isGroupEnding(Results results) {
            // TODO Auto-generated method stub
            return false;
        }

        public void addGroups(Results results) {
            // TODO Auto-generated method stub

        }

    }*/
    protected static final Comparator<Cluster> clusterOrderComparator = new Comparator<Cluster>() {
        public int compare(Cluster c1, Cluster c2) {
            return c1.getClusterOrder() - c2.getClusterOrder();
        }
    };

    protected final CollectionComparator<ClusterKey[]> clusterKeysComparator = new CollectionComparator<ClusterKey[]>();
    protected final Comparator<ClusterKey> nullsAllowedComparator = new Comparator<ClusterKey>() {
        public int compare(ClusterKey o1, ClusterKey o2) {
            if(o1 == null) {
                if(o2 == null) return 0;
                else return -getNullCompareValue();
            } else if(o2 == null) return getNullCompareValue();
            return o1.compareTo(o2);
        }
    };

    protected Map<String, ValueKey> valueColumns = new LinkedHashMap<String, ValueKey>();
    protected Map<String, Cluster> clusters = new LinkedHashMap<String, Cluster>();
    //protected Number[] maxValues;
    //protected Number[] minValues;
	protected Map<ClusterKey[], Number[]> valuesCube = new TreeMap<ClusterKey[], Number[]>(clusterKeysComparator);

    protected ClusterKey nullClusterKey = new ClusterKey();

    /** Holds value of property manageGrouping. */
    private boolean manageGrouping = true;

    /** Holds value of property clusterOrder. */
    private String[] clusterOrder;

    protected class ValueKey implements Comparable<ValueKey> {
        protected int index;
        protected ResultsReader reader;
        protected String type; //the id of the ValueKey
        protected String label;
        public ValueKey(String type, String label, int index, ResultsReader reader) {
        	this.type = type;
        	this.label = label;
        	this.index = index;
        	this.reader = reader;
        }
		public int compareTo(ValueKey o) {
			return type.compareTo(o.type);
		}
		@Override
        public boolean equals(Object o) {
        	return (o instanceof ValueKey && compareTo((ValueKey)o) == 0);
        }
		@Override
		public int hashCode() {
			return type.hashCode();
		}
		@Override
		public String toString() {
			return label;
		}
    }

    protected class Cluster {
        protected ClusterKey[] keys;
        protected Set<ClusterKey> keySet = new TreeSet<ClusterKey>(nullsAllowedComparator); // they should be ordered
        protected ResultsReader reader;

        /** Holds value of property clusterOrder. */
        private int clusterOrder;

        /** Getter for property keys.
         * @return Value of property keys.
         *
         */
        public ClusterKey[] getKeys() {
            if(keys == null) keys = keySet.toArray(new ClusterKey[keySet.size()]);
            return keys;
        }

        public void addKey(ClusterKey key) {
            keySet.add(key);
            keys = null; // reset
        }

        public void clearKeys() {
            keySet.clear();
            keys = null;
        }

        /** Getter for property clusterOrder.
         * @return Value of property clusterOrder.
         *
         */
        public int getClusterOrder() {
            return this.clusterOrder;
        }

        /** Setter for property clusterOrder.
         * @param clusterOrder New value of property clusterOrder.
         *
         */
        public void setClusterOrder(int clusterOrder) {
            this.clusterOrder = clusterOrder;
        }

        public ResultsReader getReader() {
            return reader;
        }

        public void setReader(ResultsReader reader) {
            this.reader = reader;
        }

    }

    protected class ClusterKey implements Comparable<ClusterKey> {
        protected String displayValue = ""; // charts don't do well with null labels
        /** Holds value of property orderValue. */
		protected Comparable<?> orderValue;

        public ClusterKey() {
        }

		public ClusterKey(String displayValue, Comparable<?> orderValue) {
			this.displayValue = displayValue;
			this.orderValue = orderValue;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public int compareTo(ClusterKey ck) {
            if(ClusterKey.this == ck) return 0; // quick return
            if(ck == null) return getNullCompareValue();
            int i = 0;
            if(orderValue == null) {
                if(ck.orderValue != null) i = -getNullCompareValue();
            } else if(ck.orderValue == null) {
                i = getNullCompareValue();
            } else {
				i = ((Comparable) orderValue).compareTo(ck.orderValue);
            }
            if(i == 0) return displayValue.compareToIgnoreCase(ck.displayValue); // XXX should we ignore case?
            else return i;
        }
        @Override
		public boolean equals(Object o) {
            return o instanceof ClusterKey && compareTo((ClusterKey)o) == 0;
        }
        @Override
		public int hashCode() {
            return displayValue.hashCode() + (orderValue == null ? 0 : orderValue.hashCode());
        }
        @Override
		public String toString() {
            return displayValue;
        }

        /** Getter for property displayValue.
         * @return Value of property displayValue.
         *
         */
        public String getDisplayValue() {
            return this.displayValue;
        }

        /** Setter for property displayValue.
         * @param displayValue New value of property displayValue.
         *
         */
        public void setDisplayValue(String display) {
            this.displayValue = display;
        }

        /** Getter for property orderValue.
         * @return Value of property orderValue.
         *
         */
		public Comparable<?> getOrderValue() {
            return this.orderValue;
        }

        /** Setter for property orderValue.
         * @param orderValue New value of property orderValue.
         *
         */
		public void setOrderValue(Comparable<?> orderValue) {
            this.orderValue = orderValue;
        }
    }

    /** Creates a new instance of ResultsDataset */
    public ResultsDataset() {
    }

    /** Creates a new instance of ResultsDataset */
    public ResultsDataset(String[] clusterOrder) {
        setClusterOrder(clusterOrder);
    }

    // configuration methods
    /** Adds a value column to this dataset
     * @param valueType The type of data value. These types come from the get<I>Type</I>Value() methods.
     * Thus, getXValue() would look at the data value for value type "x".
     * @param label
     * @param columnName The name of the column in the Results object that holds the data. Generally, a
     * number or date column.
     */
    public void addValueColumn(String valueType, String label, String valueColumnName, Aggregate aggregate) {
        addValueColumn(valueType, label, new SingleColumnResultsReader(valueColumnName, valueColumnName, null, aggregate));
    }
    /** Adds a value column to this dataset
     * @param valueType The type of data value. These types come from the get<I>Type</I>Value() methods.
     * Thus, getXValue() would look at the data value for value type "x".
     * @param label
     * @param reader The ResultsReader that will return the values for the value column.
     */
    public void addValueColumn(String valueType, String label, ResultsReader reader) {
        ValueKey vk = new ValueKey(valueType, label, valueColumns.size(), reader);
        valueColumns.put(valueType, vk);
    }
    /** Adds a new cluster to this dataset. The primary cluster should be added first, then the secondary, etc.
    *
    */
   public void addCluster(String clusterName, String displayColumnName, String orderColumnName, Format displayFormat) {
       addCluster(clusterName, new SingleColumnResultsReader(displayColumnName, orderColumnName, displayFormat, null));
   }

   /** Adds a new cluster to this dataset. The primary cluster should be added first, then the secondary, etc.
   *
   */
    public void addCluster(String clusterName, String[] displayColumnNames, String[] orderColumnNames, Format displayFormat) {
        addCluster(clusterName, new MultiColumnResultsReader(displayColumnNames, orderColumnNames, displayFormat));
    }

    /** Adds a new cluster to this dataset. The primary cluster should be added first, then the secondary, etc.
     *
     */
    public void addCluster(String clusterName, ResultsReader reader) {
        Cluster c = getCluster(clusterName);
        if(c == null) {
            c = new Cluster();
            c.setClusterOrder(getClusterOrder(clusterName));
            clusters.put(clusterName, c);
        }
        c.setReader(reader);
    }

    protected int getClusterOrder(String clusterName) {
        //clusterOrder -- use clusterOrder first, then let the order of addition determine the cluster order
        int missing = 0;
        /*
        // NOTE: we are going to ensure every pre-configured cluster (specified by setClusterOrder()) is
        // in the clusters map so missing will always = 0;
        String[] names = getClusterOrder();
        if(names != null) {
            for(int i = 0; i < names.length; i++) {
                if(clusterName.equals(names[i])) return i + 1;
                if(!clusters.containsKey(names[i])) missing++;
            }
        }
        // */
        return clusters.size() + missing + 1;
     }

    public String[] getValueTypes() {
        return valueColumns.keySet().toArray(new String[valueColumns.size()]);
    }

    /** Reads from the <CODE>simple.results.Results</CODE> object to populate its own
     * data storage.
     */
    protected void init(Results results) {
        Cluster[] cs =  getClusters();
        // configure results
        if(manageGrouping) {
            for(Cluster c : cs) {
                if(c.getReader() != null) c.getReader().addGroups(results);
            }
        }
        //keep track of range of each value type
        //maxValues = new Number[valueColumns.size()];
        //minValues = new Number[valueColumns.size()];
		while(results.next()) {
            boolean group = false;
            for(Cluster c : cs) {
                if(c.getReader() != null && c.getReader().isGroupEnding(results)) {
                    group = true;
                    break;
                }
            }
            //add values
            Iterator<ValueKey> iter = valueColumns.values().iterator();
			Number[] values = new Number[valueColumns.size()];
            for(int k = 0; k < values.length; k++) {
                ValueKey vk = iter.next();
				values[vk.index] = addTo(values[vk.index], vk.reader.getSortValue(results), vk.reader.getAggregates());
            }
            if(group) {
                //store values in cube-like structure
				ClusterKey[] cubeKey = new ClusterKey[cs.length];
				for(int i = 0; i < cs.length; i++) {
					ClusterKey key;
                    if(cs[i].getReader() != null) {
                        //create new cluster key
                        key = new ClusterKey();
                        key.setDisplayValue(cs[i].getReader().getDisplayValue(results));
                        key.setOrderValue(cs[i].getReader().getSortValue(results));
					} else
						key = nullClusterKey;
                    cubeKey[i] = key;
                    cs[i].addKey(key);
                }
				// if(log.isDebugEnabled()) log.debug("Got aggregate values of " + Arrays.asList(values).toString() + " for " + Arrays.asList(cubeKey).toString() + "");
				valuesCube.put(cubeKey, values);
				// maxValues[vk.index] = max(values[vk.index], maxValues[vk.index]);
				// minValues[vk.index] = min(values[vk.index], minValues[vk.index]);
            }
        }
        log.debug("Values Cube has " + valuesCube.size() + " entries");
    }

    protected Number max(Number n1, Number n2) {
        if(n1 == null) return n2;
        if(n2 == null) return n1;
        if(n1.doubleValue() < n2.doubleValue()) return n2;
        return n1;
    }

    protected Number min(Number n1, Number n2) {
        if(n1 == null) return n2;
        if(n2 == null) return n1;
        if(n1.doubleValue() > n2.doubleValue()) return n2;
        return n1;
    }

	protected static Number addTo(Number prev, Object o, Aggregate[] aggregates) {
		try {
			Number n;
			if(o == null || o instanceof Number)
				n = (Number) o;
			else if(aggregates == null || aggregates.length < 2)
				n = ConvertUtils.convert(Number.class, o);
			else {
				Collection<Number> coll = ConvertUtils.asCollection(o, Number.class);
				if(coll.size() > 0)
					n = coll.iterator().next();
				else
					n = null;
			}
			if(prev == null)
				return n;
			if(n == null)
				return prev;
			BigDecimal prevBD = ConvertUtils.convert(BigDecimal.class, prev);
			BigDecimal nextBD = ConvertUtils.convert(BigDecimal.class, n);
			if(prevBD == null)
				return nextBD;
			if(nextBD == null)
				return prevBD;
			return prevBD.add(nextBD);
        } catch(ConvertException e) {
            IllegalArgumentException iae = new IllegalArgumentException("Could not convert value '" + o + "' to a BigDecimal");
            iae.initCause(e);
            throw iae;
        }
    }

    /** Returns an array of the clusters configured for this dataset.
     *  NOTE: Implementations <strong>must</strong> return them in order of
     *  grouping heirarchy.
     *
     */
    protected Cluster[] getClusters() {
        Cluster[] c = clusters.values().toArray(new Cluster[clusters.size()]);
        Arrays.sort(c, clusterOrderComparator);
        return c;
    }

	protected Number getDataValue(String[] clusterNames, Comparable<?>[] clusterKeys, String valueType) {
		Map<Cluster, Comparable<?>> map = new TreeMap<Cluster, Comparable<?>>(clusterOrderComparator);
        for(int i = 0; i < clusterNames.length; i++) {
            map.put(getCluster(clusterNames[i]), clusterKeys[i]);
        }
		Comparable<?>[] orderedKeys = map.values().toArray(new Comparable[clusterKeys.length]);
        return getDataValue(orderedKeys, valueType);
    }

    /** Retrieves the data value from the values cube
     *  NOTE: clusterKeys must be in correct order
     */
	protected Number getDataValue(Comparable<?>[] clusterKeys, String valueType) {
        ValueKey vk = valueColumns.get(valueType);
		if(vk == null)
			throw new IllegalArgumentException("Could not find a data value type '" + valueType + "'");
        Number[] values = valuesCube.get(clusterKeys);
        Number n = (values == null ? null : values[vk.index]);
        //log.debug("Returning " + n + " as the data value for '" + valueType + "' for cluster keys = " + Arrays.asList(clusterKeys).toString());
        return n;
    }

    protected Cluster getCluster(String clusterName) {
        return clusters.get(clusterName);
    }

	protected Comparable<?> getClusterKey(String clusterName, int index) {
        Cluster c = getCluster(clusterName);
        if(c == null) {
            log.debug("Could not find a cluster named, '" + clusterName + "'");
            return null;
        }
		Comparable<?> key = c.getKeys()[index];
        return key;
    }

    protected int getClusterKeyCount(String clusterName) {
        Cluster c = getCluster(clusterName);
        if(c == null) {
            log.debug("Could not find a cluster named, '" + clusterName + "'");
            return 0;
        }
        int cnt = c.getKeys().length;
        //log.debug("Cluster Count for '" + clusterName + "' = " + cnt);
        return cnt;
    }

	protected int getClusterKeyIndex(String clusterName, Comparable<?> clusterKey) {
        Cluster c = getCluster(clusterName);
        return Arrays.binarySearch(c.getKeys(), clusterKey);
    }

    protected boolean equal(Object o1, Object o2) {
        if(o1 == null) return o2 == null;
        return o2 != null && o1.equals(o2);
    }

    protected void clearData() {
        valuesCube.clear();
        Cluster[] cc = getClusters();
        for(int i = 0; i < cc.length; i++) {
            cc[i].clearKeys();
        }
    }

    /** Populates the dataset with the data from the specified Results object.
     *
     */
    public void populate(Results results) {
        clearData();
        init(results);
    }

    /** Getter for property manageGrouping.
     * @return Value of property manageGrouping.
     *
     */
    public boolean isManageGrouping() {
        return this.manageGrouping;
    }

    /** Setter for property manageGrouping.
     * @param manageGrouping New value of property manageGrouping.
     *
     */
    public void setManageGrouping(boolean manageGrouping) {
        this.manageGrouping = manageGrouping;
    }

    /** Getter for property clusterOrder.
     * @return Value of property clusterOrder.
     *
     */
    public String[] getClusterOrder() {
        return this.clusterOrder;
    }

    /** Setter for property clusterOrder.
     * @param clusterOrder New value of property clusterOrder.
     *
     */
    public void setClusterOrder(String[] clusterOrder) {
        this.clusterOrder = clusterOrder;
        //redo clusters
        Map<String, Cluster> tmp = new HashMap<String, Cluster>();
        for(int i = 0; i < clusterOrder.length; i++) {
            Cluster c = clusters.remove(clusterOrder[i]);
            if(c == null) c = new Cluster();
            c.setClusterOrder(i+1);
            tmp.put(clusterOrder[i], c);
        }
        //get any remaining clusters in their order
        Cluster[] clustArr = getClusters();
        for(int i = 0; i < clustArr.length; i++) {
            clustArr[i].setClusterOrder(clusterOrder.length + i + 1);
        }
        clusters.putAll(tmp);
    }

    /** Getter for property nullsFirst.
     * @return Value of property nullsFirst.
     *
     */
    public boolean isNullsFirst() {
        return clusterKeysComparator.isNullsFirst();
    }

    /** Setter for property nullsFirst.
     * @param nullsFirst New value of property nullsFirst.
     *
     */
    public void setNullsFirst(boolean nullsFirst) {
        clusterKeysComparator.setNullsFirst(nullsFirst);
    }

    protected int getNullCompareValue() {
        return clusterKeysComparator.getNullCompareValue();
    }

    protected boolean valueTypeExists(String valueType) {
        return valueColumns.containsKey(valueType);
    }
}
