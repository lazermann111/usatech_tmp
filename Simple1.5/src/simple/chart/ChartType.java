/*
 * Created on Sep 30, 2005
 *
 */
package simple.chart;

import java.util.Collection;

import simple.results.DataGenre;

public interface ChartType {
    public interface Axis {   
        /**
         * @return Returns the acceptedGenre.
         */
        public DataGenre[] getAcceptedGenre();
    
        /**
         * @return Returns the name.
         */
        public String getName();
    
        /**
         * @return Returns the required.
         */
        public boolean isRequired();
    
    }
    public String getName() ;
    public Collection<Axis> getAxes() ;
    public Axis getAxis(String axisName) ;
    public ChartMaker newChartMaker() ;
}
