/*
 * ChartUtils.java
 *
 * Created on December 29, 2003, 1:45 PM
 */

package simple.chart;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.Format;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryTick;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.Tick;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.block.RectangleConstraint;
import org.jfree.chart.encoders.EncoderUtil;
import org.jfree.chart.encoders.ImageEncoderFactory;
import org.jfree.chart.encoders.ImageFormat;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.xy.StackedXYBarRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.title.Title;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.Dataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.Size2D;
import org.jfree.ui.TextAnchor;
import org.jfree.util.TableOrder;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.results.Results;
import simple.results.ResultsReader;
import simple.results.SingleColumnResultsReader;
import simple.text.StringUtils;

/**
 *
 * @author Brian S. Krug
 */
public class ChartUtils {
	private static final Log log = Log.getLog();

	protected static final Map<String, ChartType> chartTypes = new HashMap<String, ChartType>();
	protected static final Map<Class<? extends Plot>, Map<String, AxisGatherer>> axisGatherers = new HashMap<Class<? extends Plot>, Map<String, AxisGatherer>>();

	protected static final Class<?>[] datasetClasses = new Class[] { ResultsCategoryDataset.class,
 ResultsXYZDataset.class };

	public static interface MethodParameter {
		// public Object getParameterValue(Object bean) throws Exception;
	}

	public static class DefaultMethodParameter implements MethodParameter {
		protected Class<?> paramClass;

		protected Object defaultValue;

		protected String paramName;

		/*
		 * public Object getParameterValue(Object bean) throws Exception {
		 * if(paramName == null) return defaultValue; Object val =
		 * ConvertUtils.convert(paramClass, ReflectionUtils.getProperty(bean,
		 * paramName)); return (val == null ? defaultValue : val); }
		 */

		/**
		 * @return Returns the defaultValue.
		 */
		public Object getDefaultValue() {
			return defaultValue;
		}

		/**
		 * @return Returns the paramClass.
		 */
		public Class<?> getParamClass() {
			return paramClass;
		}

		/**
		 * @return Returns the paramName.
		 */
		public String getParamName() {
			return paramName;
		}
	}

	public static class DatasetMethodParameter implements MethodParameter {
		protected Class<?> paramClass;

		protected String paramName;/*
									* public Object getParameterValue(Object
									* bean) throws Exception{ Results results;
									* if(bean instanceof Results) results =
									* (Results) bean; else results = (Results)
									* ReflectionUtils.getProperty(bean,
									* paramName); ResultsDataset rd =
									* (ResultsDataset) paramClass.newInstance();
									* rd.populate(results); return rd; throw new
									* UnsupportedOperationException("Do not use
									* this method"); }
									*/

		/**
		 * @return Returns the paramClass.
		 */
		public Class<?> getParamClass() {
			return paramClass;
		}

		/**
		 * @return Returns the paramName.
		 */
		public String getParamName() {
			return paramName;
		}

	}

	public static class ChartType {
		protected String name;

		protected Method createMethod;

		protected MethodParameter[] params;

		protected String[] clusters;

		protected String[] values;

		public String[] getClusters() {
			return clusters;
		}

		public String getName() {
			return name;
		}

		public String[] getValues() {
			return values;
		}

		public JFreeChart createChart(Results results, Map<String, Object> paramValues, String[] clusterColumnNames,
 String[] clusterOrderColumnNames, Format[] clusterFormats, String[] valuesColumnNames) throws Exception {
			return ChartUtils.createChart(this, results, paramValues, clusterColumnNames, clusterOrderColumnNames, clusterFormats, valuesColumnNames);
		}

		public MethodParameter[] getMethodParameters() {
			return params;
		}

		public String getMethodName() {
			return createMethod.getName();
		}

	}

	/** Creates a new instance of ChartUtils */
	public ChartUtils() {
	}

	public static Collection<ChartType> getAvailableChartTypes() {
		if(chartTypes.isEmpty()) {
			// figure out which ResultsDatasets we have and which dataset
			// interfaces they implement
			Map<Class<?>, Class<?>> datasetMap = new HashMap<Class<?>, Class<?>>();
			Map<Class<?>, String[]> clustersMap = new HashMap<Class<?>, String[]>();
			for(int i = 0; i < datasetClasses.length; i++) {
				try {
					List<Class<?>> list = new ArrayList<Class<?>>();
					gatherInterfaces(datasetClasses[i], list);
					for(Class<?> c : list) {
						if(Dataset.class.isAssignableFrom(c))
							datasetMap.put(c, datasetClasses[i]);
					}
					String[] clusters = ((ResultsDataset) datasetClasses[i].newInstance()).getClusterOrder();
					clustersMap.put(datasetClasses[i], clusters);
				} catch(SecurityException e) {
					log.warn("Could not create ResultsDataset '" + datasetClasses[i].getName() + "'", e);
				} catch(InstantiationException e) {
					log.warn("Could not create ResultsDataset '" + datasetClasses[i].getName() + "'", e);
				} catch(IllegalAccessException e) {
					log.warn("Could not create for ResultsDataset '" + datasetClasses[i].getName() + "'", e);
				}
			}
			// Figure out which methods are available and then ensure we have a
			// ResultsDatasets that implements the necessary dataset
			Method[] methods = ChartFactory.class.getMethods();
			for(int i = 0; i < methods.length; i++) {
				if(methods[i].getName().startsWith("create") && JFreeChart.class.isAssignableFrom(methods[i].getReturnType())) {
					Class<?>[] pts = methods[i].getParameterTypes();
					MethodParameter[] params = new MethodParameter[pts.length];
					int b = 0;
					for(int k = 0; k < pts.length; k++) {
						if(String.class.equals(pts[k])) {
							DefaultMethodParameter dfp = new DefaultMethodParameter();
							dfp.paramClass = pts[k];
							switch(k) {
								case 0:
									dfp.paramName = "chart.title";
									break;
								case 1:
									dfp.paramName = "chart.axis1";
									break;
								case 2:
									dfp.paramName = "chart.axis2";
									break;
								default:
									log.warn("Cannot determine what the " + i + "th paramter represents for method '" + methods[i].getName() + "'.");
							}
							params[k] = dfp;
						} else if(Boolean.class.equals(ConvertUtils.convertToWrapperClass(pts[k]))) {
							DefaultMethodParameter dfp = new DefaultMethodParameter();
							dfp.paramClass = pts[k];
							dfp.defaultValue = Boolean.TRUE;
							switch(b++) {
								case 0:
									dfp.paramName = "chart.legend";
									break;
								case 1:
									dfp.paramName = "chart.tooltips";
									break;
								case 2:
									dfp.paramName = "chart.urls";
									break;
								default:
									log.warn("Cannot determine what the " + i + "th paramter represents for method '" + methods[i].getName() + "'.");
							}
							params[k] = dfp;
						} else if(PlotOrientation.class.isAssignableFrom(pts[k])) {
							DefaultMethodParameter dfp = new DefaultMethodParameter();
							dfp.paramClass = pts[k];
							dfp.paramName = "chart.orientation";
							dfp.defaultValue = PlotOrientation.VERTICAL;// PlotOrientation.HORIZONTAL;
							params[k] = dfp;
						} else if(TableOrder.class.isAssignableFrom(pts[k])) {
							DefaultMethodParameter dfp = new DefaultMethodParameter();
							dfp.paramClass = pts[k];
							dfp.paramName = "chart.order";
							dfp.defaultValue = TableOrder.BY_ROW;
							params[k] = dfp;
						} else if(Dataset.class.isAssignableFrom(pts[k])) {
							// create the parameter
							Class<?> rdClass = datasetMap.get(pts[k]);
							if(rdClass != null) {
								DatasetMethodParameter dmp = new DatasetMethodParameter();
								dmp.paramClass = rdClass;
								dmp.paramName = "chart.results";
								params[k] = dmp;
								// create the chartType and add it to the
								// collection
								ChartType ct = new ChartType();
								ct.createMethod = methods[i];
								ct.name = methods[i].getName().substring(6); // separateAtUpperCase(methods[i].getName().substring(6));
								ct.params = params;

								// figure out the values and clusters names
								ct.clusters = clustersMap.get(rdClass);
								Method[] dsMeths = pts[k].getMethods();
								Set<String> values = new TreeSet<String>();
								for(int l = 0; l < dsMeths.length; l++) {
									Class<?> rt = ConvertUtils.convertToWrapperClass(dsMeths[l].getReturnType());
									if(rt == null || rt.equals(Void.class))
										continue;
									log.debug("Checking method '" + dsMeths[l].getName() + "' with return type=" + rt.getName());
									if(Number.class.isAssignableFrom(rt) && dsMeths[l].getName().startsWith("get") && dsMeths[l].getParameterTypes().length == ct.clusters.length && dsMeths[l].getName().endsWith("Value")) {
										// this is one so add it
										values.add(dsMeths[l].getName().substring(3, dsMeths[l].getName().length() - 5));
									}
								}
								ct.values = values.toArray(new String[values.size()]);
								log.debug("Adding chart type " + ct.getName() + " with clusters=" + StringUtils.toStringList(ct.getClusters()) + " and values=" + StringUtils.toStringList(ct.getValues()));
								chartTypes.put(ct.name, ct);
							}
						}
					}
				}
			}
		}
		return chartTypes.values();
	}

	protected static void gatherInterfaces(Class<?> baseClass, List<Class<?>> list) {
		list.add(baseClass);
		Class<?>[] interfaces = baseClass.getInterfaces();
		for(int i = 0; i < interfaces.length; i++) {
			gatherInterfaces(interfaces[i], list);
		}
		if(baseClass.getSuperclass() != null)
			gatherInterfaces(baseClass.getSuperclass(), list);
	}

	protected static String separateAtUpperCase(String s) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < s.length(); i++) {
			if(i > 0 && Character.isUpperCase(s.charAt(i)) && i + 1 < s.length() && Character.isLowerCase(s.charAt(i + 1))) {
				sb.append(" ");
			}
			sb.append(s.charAt(i));
		}
		return sb.toString();
	}

	/**
	 * @param type
	 * @param results
	 * @param paramValues
	 * @param clusterColumnNames
	 * @param valuesColumnNames
	 * @return
	 * @throws Exception
	 * @Deprecated Use simple.chart.ChartFactory instead
	 */
	public static JFreeChart createChart(String chartTypeName, Results results, Map<String, Object> paramValues,
 String[] clusterColumnNames, String[] clusterOrderColumnNames, Format[] clusterFormats, String[] valuesColumnNames) throws InstantiationException, ConvertException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		getAvailableChartTypes(); // ensure that it is initialized
		ChartType type = chartTypes.get(chartTypeName);
		if(type == null)
			throw new IllegalArgumentException("Chart Type '" + chartTypeName + "' is not valid");
		return createChart(type, results, paramValues, clusterColumnNames, clusterOrderColumnNames, clusterFormats, valuesColumnNames);
	}

	/**
	 * @param type
	 * @param results
	 * @param paramValues
	 * @param clusterColumnNames
	 * @param valuesColumnNames
	 * @return
	 * @throws Exception
	 * @Deprecated Use simple.chart.ChartFactory instead
	 */
	public static JFreeChart createChart(String chartTypeName, Results results, Map<String, Object> paramValues,
 ResultsReader[] clusterReaders, String[] valuesColumnNames) throws InstantiationException, ConvertException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		getAvailableChartTypes(); // ensure that it is initialized
		ChartType type = chartTypes.get(chartTypeName);
		if(type == null)
			throw new IllegalArgumentException("Chart Type '" + chartTypeName + "' is not valid");
		return createChart(type, results, paramValues, clusterReaders, valuesColumnNames);
	}

	/**
	 * @param type
	 * @param results
	 * @param paramValues
	 * @param clusterColumnNames
	 * @param valuesColumnNames
	 * @return
	 * @throws InstantiationException
	 * @throws ConvertException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	protected static JFreeChart createChart(ChartType type, Results results, Map<String, Object> paramValues,
 String[] clusterColumnNames, String[] clusterOrderColumnNames, Format[] clusterFormats, String[] valuesColumnNames) throws InstantiationException, ConvertException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		ResultsReader[] readers = new ResultsReader[clusterColumnNames.length];
		for(int i = 0; i < readers.length; i++) {
			readers[i] = new SingleColumnResultsReader(clusterColumnNames[i], clusterOrderColumnNames[i], clusterFormats[i]);
		}
		return createChart(type, results, paramValues, readers, valuesColumnNames);
	}

	/**
	 * @param type
	 * @param results
	 * @param paramValues
	 * @param clusterReaders
	 * @param valuesColumnNames
	 * @return
	 * @throws InstantiationException
	 * @throws ConvertException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws Exception
	 */
	protected static JFreeChart createChart(ChartType type, Results results, Map<String, Object> paramValues,
 ResultsReader[] clusterReaders, String[] valuesColumnNames) throws InstantiationException, ConvertException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		// get the arguments for the method call
		Object[] args = new Object[type.params.length];
		// paramValues.put("chart.results", results);
		for(int i = 0; i < type.params.length; i++) {
			if(type.params[i] instanceof DatasetMethodParameter) {
				DatasetMethodParameter dmp = (DatasetMethodParameter) type.params[i];
				ResultsDataset rd = (ResultsDataset) dmp.paramClass.newInstance();
				String[] values = type.getValues();
				for(int j = 0; j < values.length && j < valuesColumnNames.length; j++) {
					rd.addValueColumn(values[j], valuesColumnNames[j], valuesColumnNames[j], null);
				}
				String[] clusters = type.getClusters();
				for(int j = 0; j < clusters.length && j < clusterReaders.length; j++) {
					rd.addCluster(clusters[j], clusterReaders[j]);
				}
				rd.populate(results);
				args[i] = rd;
			} else if(type.params[i] instanceof DefaultMethodParameter) {
				DefaultMethodParameter dfp = (DefaultMethodParameter) type.params[i];
				if(dfp.paramName == null)
					args[i] = dfp.defaultValue;
				else {
					Object val = ConvertUtils.convert((Class<?>) dfp.paramClass, paramValues.get(dfp.paramName));
					args[i] = (val == null ? dfp.defaultValue : val);
				}
			}
		}
		// create the chart
		return (JFreeChart) type.createMethod.invoke(null, args);
	}

	// We should be able to configure these, but for now let's hard-code
	protected static int categoryMarginSize = 5;
	protected static int lowerMarginSize = 5;
	protected static int upperMarginSize = 5;
	protected static boolean useFixedMargins = true;

	protected static Dimension calculateAxisSize(Axis axis, RectangleEdge edge, Dimension dataSize, Graphics2D g2, List<?> ticks) {
		int h = 10; // initialize to the minimum
		int w = 10; // initialize to the minimum

		// if(axis.isTickLabelsVisible()) {
		// NOTE: CategoryAxis objects report false for isTickLabelsVisible() so check if ticks are null instead
		int majorTickCount = 0;
		if(ticks != null) {
			RectangleInsets insets = axis.getTickLabelInsets();
			if(insets == null)
				insets = new RectangleInsets(5, 5, 5, 5);
			Font font = axis.getTickLabelFont();
			for(Iterator<?> iter = ticks.iterator(); iter.hasNext();) {
				Tick tick = (Tick) iter.next();
				double angle = tick.getAngle();
				String text;
				if(tick instanceof CategoryTick) {
					Object cat = ((CategoryTick) tick).getCategory();
					text = cat == null ? "" : cat.toString();
				} else {
					text = tick.getText();
				}
				if(!StringUtils.isBlank(text)) {
					majorTickCount++;
					/* XXX not sure if we need this
					if (edge == RectangleEdge.LEFT || edge == RectangleEdge.RIGHT) {
					    angle = angle - Math.PI / 2.0;
					}*/Dimension sz = calculateLabelSize(text, angle, insets, font, g2);
					log.debug("Size of Tick with text = '" + text + "' = " + sz.width + ", " + sz.height);
					h = Math.max(h, sz.height);
					w = Math.max(w, sz.width);
				}
			}
		}

		// calc axis label
		Dimension labelSize = calculateLabelSize(axis.getLabel(), axis.getLabelAngle(), axis.getLabelInsets(), axis.getLabelFont(), g2);
		log.debug("Size of Axis Label with text = '" + axis.getLabel() + "' = " + labelSize.width + ", " + labelSize.height);

		Dimension axisSize = null;
		if(useFixedMargins) {
			// calculate the factor based on the above margin sizes
			int[] margins = getFixedMargins(axis, ticks.size());
			int marginTotal = margins[0] + margins[1] + margins[2];
			log.debug("Margins Total = " + marginTotal);

			// calc total size
			if(RectangleEdge.isTopOrBottom(edge)) {
				axisSize = new Dimension(Math.max((int) Math.ceil(w * majorTickCount) + marginTotal, labelSize.width), h + labelSize.height);
			} else if(RectangleEdge.isLeftOrRight(edge)) {
				axisSize = new Dimension(w + labelSize.width, Math.max((int) Math.ceil(h * majorTickCount) + marginTotal, labelSize.height));
			}
		} else {
			// calculate the factor based on the above margin sizes
			double factor;
			if(axis instanceof CategoryAxis) {
				CategoryAxis ca = (CategoryAxis) axis;
				factor = 1 - (ca.getLowerMargin() + ca.getUpperMargin() + ca.getCategoryMargin());
			} else if(axis instanceof ValueAxis) {
				ValueAxis va = (ValueAxis) axis;
				factor = 1 - (va.getLowerMargin() + va.getUpperMargin());
			} else {
				factor = 0.95;
			}
			log.debug("Factor = " + factor);

			// calc total size
			if(RectangleEdge.isTopOrBottom(edge)) {
				axisSize = new Dimension(Math.max((int) Math.ceil(w * majorTickCount / factor), labelSize.width), h + labelSize.height);
			} else if(RectangleEdge.isLeftOrRight(edge)) {
				axisSize = new Dimension(w + labelSize.width, Math.max((int) Math.ceil(h * majorTickCount / factor), labelSize.height));
			}
		}
		return axisSize;
	}

	protected static int[] getFixedMargins(Axis axis, int tickCount) {
		if(axis instanceof CategoryAxis) {
			if(axis.getPlot() instanceof CategoryPlot) {
				CategoryItemRenderer rend = ((CategoryPlot) axis.getPlot()).getRenderer();
				if(rend instanceof BarRenderer3D) {
					return new int[] { 15, 15, 15 * tickCount };
				}
			}
			return new int[] { lowerMarginSize, upperMarginSize, categoryMarginSize * tickCount };
		} else if(axis instanceof ValueAxis) {
			return new int[] { lowerMarginSize, upperMarginSize, 0 };
		} else {
			return new int[] { 15, 15, 0 };
		}
	}

	/**
	 * Sets the upper and lower margins to a certain
	 * fixed amounts (instead of as a percent of the total axis width)
	 * 
	 * @param axis
	 * @param edge
	 * @param width
	 * @param height
	 */
	protected static void adjustAxisMargins(Axis axis, RectangleEdge edge, double width, double height, int tickCount) {
		double dim = (RectangleEdge.isTopOrBottom(edge) ? width : height);
		int[] margins = getFixedMargins(axis, tickCount);
		if(axis instanceof CategoryAxis) {
			CategoryAxis ca = (CategoryAxis) axis;
			ca.setLowerMargin(margins[0] / dim);
			ca.setUpperMargin(margins[1] / dim);
			ca.setCategoryMargin(margins[2] / dim);
		} else if(axis instanceof ValueAxis) {
			ValueAxis va = (ValueAxis) axis;
			va.setLowerMargin(margins[0] / dim);
			va.setUpperMargin(margins[1] / dim);
		}
	}

	protected static long calendarFieldToMillis(int calendarField) {
		switch(calendarField) {
			case Calendar.YEAR:
				return TimeUnit.DAYS.toMillis(365);
			case Calendar.MONTH:
				return TimeUnit.DAYS.toMillis(30);
			case Calendar.DATE:
				return TimeUnit.DAYS.toMillis(1);
			case Calendar.HOUR_OF_DAY:
				return TimeUnit.HOURS.toMillis(1);
			case Calendar.MINUTE:
				return TimeUnit.MINUTES.toMillis(1);
			case Calendar.SECOND:
				return TimeUnit.SECONDS.toMillis(1);
			case Calendar.MILLISECOND:
				return TimeUnit.MILLISECONDS.toMillis(1);
			default:
				return 0;
		}
	}
	protected static Dimension calculateDataSize(Plot plot, int minBarWidth, Graphics2D g2) {
		int w = 300;
		int h = 300;
		// calc graphical representation (bars, lines, etc)
		if(plot instanceof CategoryPlot) {
			CategoryItemRenderer r = ((CategoryPlot) plot).getRenderer();
			CategoryDataset dataset = ((CategoryPlot) plot).getDataset();
			int columns = dataset.getColumnCount();
			int rows = dataset.getRowCount();
			// may have to adjust if columns or rows = 0
			if(r instanceof org.jfree.chart.renderer.category.BarRenderer) {
				// min bar width * # of bars
				w = minBarWidth * columns * rows;
			} else {
				w = minBarWidth * columns;
			}
			if(PlotOrientation.HORIZONTAL.equals(((CategoryPlot) plot).getOrientation())) {
				int tmp = w;
				w = h;
				h = tmp;
			}
		} else if(plot instanceof XYPlot) {
			ValueAxis domainAxis = ((XYPlot)plot).getDomainAxis();
			XYDataset dataset = ((XYPlot) plot).getDataset();
			XYItemRenderer r = ((XYPlot) plot).getRenderer();
			int series;
			if(r instanceof StackedXYBarRenderer)
				series = 1;
			else if(r instanceof XYBarRenderer)
				series = 1; // dataset.getSeriesCount();
			else
				series = 0;
			if(series > 0.0 && domainAxis instanceof DateAxis) {
				DateAxis da = (DateAxis) domainAxis;
				long range = da.getMaximumDate().getTime() - da.getMinimumDate().getTime();
				DateTickUnit tu = da.getTickUnit();
				long interval = calendarFieldToMillis(tu.getCalendarField()) * tu.getMultiple();
				if(interval > 0) {
					int count = (int) (range / interval);
					w = Math.max(w, minBarWidth * count * series);
				}
			}
			if(PlotOrientation.HORIZONTAL.equals(((XYPlot) plot).getOrientation())) {
				int tmp = w;
				w = h;
				h = tmp;
			}
		}

		return new Dimension(w, h);
	}

	protected static Dimension calculateLabelSize(String text, double angle, RectangleInsets insets, Font font, Graphics2D g2) {
		if(text == null || text.length() == 0)
			return new Dimension(0, 0);
		FontMetrics fm = g2.getFontMetrics(font);
		Rectangle2D bounds = fm.getStringBounds(text, g2);
		bounds.setRect(bounds.getX(), bounds.getY(), bounds.getWidth() + insets.getLeft() + insets.getRight(), bounds.getHeight() + insets.getTop() + insets.getBottom());
		double x = bounds.getCenterX();
		double y = bounds.getCenterY();
		AffineTransform transformer = AffineTransform.getRotateInstance(angle, x, y);
		bounds = transformer.createTransformedShape(bounds).getBounds2D();
		return new Dimension((int) Math.ceil(bounds.getWidth()), (int) Math.ceil(bounds.getHeight()));
	}

	/*
	protected static Dimension calculateLegendSize(StandardLegend legend, double availableWidth, Graphics2D g2) {
	    //calc legend title size
	    Dimension titleSize = calculateLabelSize(legend.getTitle(), 0.0, new Insets(0, 0, 0, 0), legend.getTitleFont(), g2);
	    double width = 0.0;
	    double height = 0.0;
	    double rowWidth = 0.0;
	    double rowHeight = 0.0;
	    boolean horizontal = (legend.getAnchor() & (1 << 0)) != 0;
	    availableWidth = Math.max(availableWidth, 100.0); // set a minimum
	    if(horizontal) { // title is on side
	        availableWidth = Math.max(availableWidth, titleSize.getWidth());
	        rowWidth = titleSize.getWidth() + legend.getOuterGap().getLeftSpace(availableWidth);
	        rowHeight = titleSize.getHeight();
	    } else {//title is on top
	        availableWidth = Math.max(availableWidth, titleSize.getWidth());
	        height = titleSize.getHeight();
	        width = titleSize.getWidth();
	    }

	    // calc items size - this is going to be quite a guess
	    FontMetrics fm = g2.getFontMetrics(legend.getItemFont());
	    LegendItemCollection legendItems = legend.getChart().getPlot().getLegendItems();
	    int innerGap = 2;
	    boolean startingNewRow = true;
	    for (int i = 0; i < legendItems.getItemCount(); i++) {
	        LegendItem item = legendItems.get(i);
	        LineMetrics lm = fm.getLineMetrics(item.getLabel(), g2);
	        // these formulas are strange - they were copied from StandardLegend internal calculations
	        double lineHeight = lm.getAscent() + lm.getDescent() + lm.getLeading();
	        double w = 2 * innerGap + 1.15f * lineHeight
	            + fm.getStringBounds(item.getLabel(), g2).getWidth()
	            + 0.5 * lm.getAscent();
	        double h = 2 * innerGap + lineHeight;
	        //horizontal
	        if(horizontal) {
	            if (!startingNewRow  && (w + rowWidth > availableWidth)) {
	                width = Math.max(width, rowWidth);
	                rowWidth = 0;
	                height += rowHeight;
	                i--;
	                startingNewRow = true;
	            }
	            else {
	                rowHeight = Math.max(rowHeight, h);
	                rowWidth += w;
	                startingNewRow = false;
	            }
	        } else {
	            height += h;
	            width = Math.max(width, w);
	        }
	    }

	    //horizontal
	    if(horizontal) {
	        width = Math.max(width, rowWidth);
	        height += rowHeight;
	    }

	    return new Dimension((int)Math.ceil(legend.getOuterGap().getAdjustedWidth(width)),(int)Math.ceil(legend.getOuterGap().getAdjustedHeight(height)));
	}
	 */
	protected static class AxisInfo {
		public RectangleEdge edge;
		public int tickCount;
	}

	protected static interface AxisGatherer {
		public void gatherAxes(Plot plot, Map<Axis, AxisInfo> axes);
	}

	protected static class IndexedPropertyAxisGatherer implements AxisGatherer {
		protected final Method axisMethod;
		protected final Method edgeMethod;
		protected final Method countMethod;

		public IndexedPropertyAxisGatherer(Method axisMethod, Method edgeMethod, Method countMethod) {
			this.axisMethod = axisMethod;
			this.edgeMethod = edgeMethod;
			this.countMethod = countMethod;
		}

		@Override
		public void gatherAxes(Plot plot, Map<Axis, AxisInfo> axes) {
			try {
				int count = (Integer) countMethod.invoke(plot);
				for(int i = 0; i < count; i++) {
					Axis axis = (Axis) axisMethod.invoke(plot, i);
					if(axes.containsKey(axis))
						continue;
					RectangleEdge edge = (RectangleEdge) edgeMethod.invoke(plot, i);
					axes.put(axis, createAxisInfo(axis, edge));
				}
			} catch(IllegalArgumentException e) {
				log.info("Could not load the axis for property '" + axisMethod.getName() + "'", e);
			} catch(IllegalAccessException e) {
				log.info("Could not load the axis for property '" + axisMethod.getName() + "'", e);
			} catch(InvocationTargetException e) {
				log.info("Could not load the axis for property '" + axisMethod.getName() + "'", e);
			}
		}
	}

	protected static class PropertyAxisGatherer implements AxisGatherer {
		protected final Method axisMethod;
		protected final Method edgeMethod;

		public PropertyAxisGatherer(Method axisMethod, Method edgeMethod) {
			this.axisMethod = axisMethod;
			this.edgeMethod = edgeMethod;
		}

		@Override
		public void gatherAxes(Plot plot, Map<Axis, AxisInfo> axes) {
			try {
				Axis axis = (Axis) axisMethod.invoke(plot);
				if(axes.containsKey(axis))
					return;
				RectangleEdge edge = (RectangleEdge) edgeMethod.invoke(plot);
				axes.put(axis, createAxisInfo(axis, edge));
			} catch(IllegalArgumentException e) {
				log.info("Could not load the axis for property '" + axisMethod.getName() + "'", e);
			} catch(IllegalAccessException e) {
				log.info("Could not load the axis for property '" + axisMethod.getName() + "'", e);
			} catch(InvocationTargetException e) {
				log.info("Could not load the axis for property '" + axisMethod.getName() + "'", e);
			}
		}
	}

	protected static final Pattern READ_AXIS_METHOD_NAME_PATTERN = Pattern.compile("get(.*)Axis");
	protected static void gatherAxes(Plot plot, Map<Axis, AxisInfo> axes) {
		Map<String, AxisGatherer> ags = axisGatherers.get(plot.getClass());
		if(ags == null) {
			ags = new HashMap<String, ChartUtils.AxisGatherer>(4);
			// NOTE: we can't use property descriptors b/c get*Axis() and get*Axis(int) return the same class instead of the first returning an array
			Method[] methods;
			try {
				methods = plot.getClass().getMethods();
			} catch(SecurityException e) {
				log.info("Could not load axes into collection", e);
				return;
			}

			for(Method method : methods) {
				if(Axis.class.isAssignableFrom(method.getReturnType())) {
					Matcher matcher = READ_AXIS_METHOD_NAME_PATTERN.matcher(method.getName());
					if(matcher.matches()) {
						String axisName = matcher.group(1);
						Class<?>[] paramTypes = method.getParameterTypes();
						switch(paramTypes.length) {
							case 0:
								if(!ags.containsKey(axisName))
									try {
										Method edgeMethod = plot.getClass().getMethod(method.getName() + "Edge");
										ags.put(axisName, new PropertyAxisGatherer(method, edgeMethod));
									} catch(NoSuchMethodException nsme) {
									}
								break;
							case 1:
								if(paramTypes[0].equals(int.class)) {
									try {
										Method edgeMethod = plot.getClass().getMethod(method.getName() + "Edge", paramTypes);
										Method countMethod = plot.getClass().getMethod(method.getName() + "Count");
										ags.put(axisName, new IndexedPropertyAxisGatherer(method, edgeMethod, countMethod));
									} catch(NoSuchMethodException nsme) {
									}
								}
								break;
						}
					}
				}
			}
			axisGatherers.put(plot.getClass(), ags);
		}
		for(AxisGatherer ag : ags.values())
			ag.gatherAxes(plot, axes);
	}

	public static java.awt.Dimension calculateSize(JFreeChart chart, Map<String, Object> settings) {
		// determine chart size
		log.debug("Calculating Chart Size...");
		Map<Axis, AxisInfo> axes = new java.util.HashMap<Axis, AxisInfo>();
		// load all axes
		gatherAxes(chart.getPlot(), axes);
		log.debug("Found " + axes.size() + " axes");
		java.awt.image.BufferedImage image = new java.awt.image.BufferedImage(1, 1, java.awt.image.BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = image.createGraphics();

		// determine data size
		int minBarWidth = 4; // TODO: get this from settings
		Dimension dataSize = calculateDataSize(chart.getPlot(), minBarWidth, g2);
		log.debug("Size of Data = " + dataSize.width + ", " + dataSize.height);

		// estimate size of each axis and apply it appropriately
		double maxAxisWidth = 0;
		double maxAxisHeight = 0;
		double extraAxisWidth = 0;
		double extraAxisHeight = 0;
		for(Map.Entry<Axis, AxisInfo> entry : axes.entrySet()) {
			Axis axis = entry.getKey();
			RectangleEdge edge = entry.getValue().edge;
			// calc tick labels - must start with a guess for the dataArea so that Value Axes calculate the tick marks well
			// specifically setting the tick unit for the axis will eliminate this problem - but we can't do that here
			List<?> ticks = axis.refreshTicks(g2, new AxisState(), new Rectangle2D.Double(0, 0, Math.max(300.0, dataSize.getWidth()), Math.max(300.0, dataSize.getHeight())), edge);
			entry.getValue().tickCount = ticks.size();
			Dimension sz = calculateAxisSize(axis, edge, dataSize, g2, ticks);
			if(RectangleEdge.isTopOrBottom(edge)) {
				maxAxisWidth = Math.max(maxAxisWidth, sz.getWidth());
				extraAxisHeight += sz.getHeight();
			} else if(RectangleEdge.isLeftOrRight(edge)) {
				extraAxisWidth += sz.getWidth();
				maxAxisHeight = Math.max(maxAxisHeight, sz.getHeight());
			}
			log.debug("Size of Axis (" + axis + ") = " + sz.width + ", " + sz.height);
		}

		double width = Math.max(dataSize.getWidth(), maxAxisWidth) + extraAxisWidth;
		double height = Math.max(dataSize.getHeight(), maxAxisHeight) + extraAxisHeight;

		log.debug("Max Axis Size = " + width + ", " + height);

		// Now adjust axis margins based on calculated width/height
		for(Map.Entry<Axis, AxisInfo> entry : axes.entrySet()) {
			adjustAxisMargins(entry.getKey(), entry.getValue().edge, width, height, entry.getValue().tickCount);
		}

		// if(width == 0.0) width = 100.0;
		// if(height == 0.0) height = 100.0;

		// store the plot size
		// if(plotWidth == 0.0) plotWidth = 100.0;
		// if(plotHeight == 0.0) plotHeight = 100.0;

		// add title
		// final float startHeight = 1.0f;
		TextTitle tt = chart.getTitle();
		double titleWidth = (tt == null ? 0 : tt.getWidth() /*tt.getPreferredWidth(g2, startHeight)*/);
		double titleHeight = (tt == null ? 0 : tt.getHeight() /*tt.getPreferredHeight(g2, (float)titleWidth)*/);
		width = Math.max(width, titleWidth);
		height += titleHeight;
		log.debug("Title Size = " + titleWidth + ", " + titleHeight);

		// add subtitles
		for(Iterator<?> iter = chart.getSubtitles().iterator(); iter.hasNext();) {
			Title title = (Title) iter.next();
			double w = title.getWidth(); // title.getPreferredWidth(g2, startHeight);
			double h = title.getHeight(); // title.getPreferredHeight(g2, (float)w);
			if(RectangleEdge.isTopOrBottom(title.getPosition())) {
				width = Math.max(width, w);
				height += h;
			} else if(RectangleEdge.isLeftOrRight(title.getPosition())) {
				width += w;
				height = Math.max(height, h);
			}
		}

		// add legend
		if(chart.getLegend() != null) {
			Size2D legendSize = chart.getLegend().arrange(g2, new RectangleConstraint(width, height));
			if(RectangleEdge.isLeftOrRight(chart.getLegend().getPosition())) {
				width = Math.max(width, legendSize.getWidth());
				height += legendSize.getHeight();
				log.debug("Legend anchor = RIGHT or LEFT");
			} else if(RectangleEdge.isTopOrBottom(chart.getLegend().getPosition())) {
				width = Math.max(width, legendSize.getWidth());
				height += legendSize.getHeight();
				// XXX: not sure which way this should be
				log.debug("Legend anchor = TOP or BOTTOM");
			}
			log.debug("Size of Legend = " + legendSize.width + ", " + legendSize.height);
		}
		g2.dispose();
		RectangleInsets chartInsets = chart.getPlot().getInsets();
		width += chartInsets.getLeft() + chartInsets.getRight(); // insets
		height += chartInsets.getTop() + chartInsets.getBottom(); // insets
		log.debug("Final Size of Chart = " + width + ", " + height);
		return new java.awt.Dimension((int) Math.ceil(width), (int) Math.ceil(height));
	}

	protected static AxisInfo createAxisInfo(Axis axis, RectangleEdge edge) {
		AxisInfo ai = new AxisInfo();
		ai.edge = edge;
		return ai;
	}

	protected static Map<String, ItemLabelAnchor> itemLabelAnchors = new HashMap<String, ItemLabelAnchor>();
	protected static Map<String, TextAnchor> textAnchors = new HashMap<String, TextAnchor>();
	static {
		for(Field f : ItemLabelAnchor.class.getFields()) {
			if(Modifier.isPublic(f.getModifiers()) && ItemLabelAnchor.class.isAssignableFrom(f.getType())
 && Modifier.isStatic(f.getModifiers())) {
				try {
					itemLabelAnchors.put(f.getName(), (ItemLabelAnchor)f.get(null));
				} catch (IllegalArgumentException e) {
					log.warn("Could not get field '" + f.getName() + "' on ItemLabelAnchor", e);
				} catch (IllegalAccessException e) {
					log.warn("Could not get field '" + f.getName() + "' on ItemLabelAnchor", e);
				}
			}
		}
		for(Field f : TextAnchor.class.getFields()) {
			if(Modifier.isPublic(f.getModifiers()) && TextAnchor.class.isAssignableFrom(f.getType())
 && Modifier.isStatic(f.getModifiers())) {
				try {
					textAnchors.put(f.getName(), (TextAnchor)f.get(null));
				} catch (IllegalArgumentException e) {
					log.warn("Could not get field '" + f.getName() + "' on TextAnchor", e);
				} catch (IllegalAccessException e) {
					log.warn("Could not get field '" + f.getName() + "' on TextAnchor", e);
				}
			}
		}
	}

	public static ItemLabelAnchor getItemLabelAnchor(String name) {
		if(name == null || (name = name.trim()).length() == 0)
			return null;
		return itemLabelAnchors.get(name);
	}

	public static TextAnchor getTextAnchor(String name) {
		if(name == null || (name = name.trim()).length() == 0)
			return null;
		return textAnchors.get(name);
	}

	static {
		ImageEncoderFactory.setImageEncoder("gif", "simple.chart.SunGIFEncoderAdapter");
	}

	public static void writeChartAsGIF(OutputStream out, JFreeChart chart, int width, int height) throws IOException {
		EncoderUtil.writeBufferedImage(chart.createBufferedImage(width, height, null), ImageFormat.GIF, out);
	}

}