package simple.modem;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.service.modem.service.dto.USATDevice;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * DAO IMPL to populate USATDevice request param
 *
 * @author dlozenko
 */
public class USATDeviceDaoImpl implements USATDeviceDao {
    private static final simple.io.Log log = simple.io.Log.getLog();

    @Override
    public USATDevice populateByDeviceId(long deviceId) throws DataLayerException, SQLException, ConvertException {
        USATDevice rv = new USATDevice();
        rv.setDeviceId(deviceId);
        fillModemIdAndProvider(rv, deviceId);
        fillCustomerEmailAndZip(rv, deviceId);
        if (rv.getZipCode() == null) {
        	populateZips(new ArrayList<>(Arrays.asList(rv)));
        }
        //rv.setEsn();
        // TODO: These todos are needed for mass SIM activation only
        //rv.setImei(); //TODO: implement for Verizon, this will need for SIM activation.
        //rv.setMeid(); //TODO: implement for Verizon, this will need for SIM activation
        //rv.setName();
        //rv.setAccountName();
        //rv.setServicePlan(); //TODO: do we need to store service plan for SIM card somewhere? This will need for SIM activation
        return rv;
    }

    @Override
    public USATDevice[] populateByDeviceIds(long[] deviceIds) throws DataLayerException, SQLException, ConvertException {
        if (deviceIds == null || deviceIds.length == 0) {
            return null;
        }
        List<USATDevice> rv = new ArrayList<>();
        fillIccIdAndProviders(rv, deviceIds);
        fillCustomerEmailAndZips(rv);
        if (hasEmptyZips(rv)) {
        	populateZips(rv);
        }
        return rv.isEmpty() ? null : rv.toArray(new USATDevice[rv.size()]);
    }

    private boolean hasEmptyZips(List<USATDevice> rv) {
			for (USATDevice device : rv) {
				if (device.getZipCode() == null) {
					return true;
				}
			}
			return false;
		}

		void fillModemIdAndProvider(USATDevice modem, long deviceId) throws SQLException, DataLayerException, ConvertException {
        Results iccidHostTypeResult = DataLayerMgr.executeQuery("GET_MODEM_ICCID_HOST_TYPE_BY_DEVICE_ID", new Object[]{deviceId}, false);
        if (iccidHostTypeResult.next()) {
            // for testing, please not commit
            //modem.setIccid("8944538523009066054");
        		modem.setWithSim("Y".equals(iccidHostTypeResult.getValue("has_sim", String.class)));
        		if (modem.isWithSim()) {
        			modem.setIccid(iccidHostTypeResult.getValue("modem_iccid", String.class));
        			modem.setSerialCode(modem.getIccid());
        		} else {
        			modem.setMeid(iccidHostTypeResult.getValue("modem_iccid", String.class));
        			modem.setSerialCode(modem.getMeid());
        		}
            modem.setProviderName(iccidHostTypeResult.getValue("comm_method", String.class));
      			modem.setImei(iccidHostTypeResult.getValue("modem_imei", String.class));
        } else {
            log.warn("Modem status was requested for Device ID = " + deviceId + ", but USATDeviceDao failed to resolve its ICCID and host_type(providerName).");
        }
    }

    void fillCustomerEmailAndZip(USATDevice modem, long deviceId) throws SQLException, DataLayerException, ConvertException {
        Results emailsResult = DataLayerMgr.executeQuery("GET_CUS_EMAILS_BY_DEVICE_ID", new Object[]{deviceId}, false);
        if (emailsResult.next()) {
            String email = emailsResult.getValue("adm_email", String.class);
            if (email == null) {
                email = emailsResult.getValue("corp_email", String.class);
            }
            modem.setOwnerEmail(email);
            modem.setZipCode(emailsResult.getValue("zip", String.class));
        }
    }

    void fillIccIdAndProviders(List<USATDevice> modems, long[] deviceIds) throws SQLException, DataLayerException, ConvertException {
        Results iccidHostTypeResult = DataLayerMgr.executeQuery("GET_MODEM_ICCID_HOST_TYPE_BY_DEVICE_IDS", new Object[]{toCsv(deviceIds)}, false);
        while (iccidHostTypeResult.next()) {
            USATDevice modem = new USATDevice();
            modem.setWithSim("Y".equals(iccidHostTypeResult.getValue("has_sim", String.class)));
            modem.setDeviceId(iccidHostTypeResult.getValue("device_id", Long.class));
        		if (modem.isWithSim()) {
        			modem.setIccid(iccidHostTypeResult.getValue("modem_iccid", String.class));
        			modem.setSerialCode(modem.getIccid());
        		} else {
        			modem.setMeid(iccidHostTypeResult.getValue("modem_iccid", String.class));
        			modem.setSerialCode(modem.getMeid());
        		}
            modem.setProviderName(iccidHostTypeResult.getValue("comm_method", String.class));
      			modem.setImei(iccidHostTypeResult.getValue("modem_imei", String.class));
            modems.add(modem);
        }
        if (modems.size() != deviceIds.length) {
            log.warn("Modem status was requested for Device IDs = {" + Arrays.toString(deviceIds) +
                    "}, but USATDeviceDao failed to resolve some ICCIDs and host_types(providerNames)."+
                    "resolved count: " + modems.size() + ", expected count: " + deviceIds.length);
        }
        log.info("Populated " + toCsv(deviceIds) + " with this: " + modems);
    }

    void fillCustomerEmailAndZips(List<USATDevice> modems) throws SQLException, DataLayerException, ConvertException {
        if (modems.isEmpty()) {
            return;
        }
        long[] deviceIds = new long[modems.size()];
        for (int i = 0; i < modems.size(); i++) {
            deviceIds[i] = modems.get(i).getDeviceId();
        }
        Results emailsResult = DataLayerMgr.executeQuery("GET_CUS_EMAILS_BY_DEVICE_IDS", new Object[]{toCsv(deviceIds)}, false);
        while (emailsResult.next()) {
            Long id = emailsResult.getValue("device_id", Long.class);
            String email = emailsResult.getValue("adm_email", String.class);
            if (email == null) {
                email = emailsResult.getValue("corp_email", String.class);
            }
            String zip = emailsResult.getValue("zip", String.class);
            for (USATDevice md : modems) {
                if (id.equals(md.getDeviceId())) {
                    md.setOwnerEmail(email);
                    md.setZipCode(zip);
                    break;
                }
            }
        }
    }
    
    private void populateZips(List<USATDevice> modems) throws SQLException, DataLayerException, ConvertException {
      if (modems.isEmpty()) {
	        return;
	    }
      long[] deviceIds = new long[modems.size()];
      for (int i = 0; i < modems.size(); i++) {
          deviceIds[i] = modems.get(i).getDeviceId();
      }
      Results zipsResult = DataLayerMgr.executeQuery("GET_ZIPS_BY_DEVICE_IDS", new Object[]{toCsv(deviceIds)}, false);
      while(zipsResult.next()) {
        String zip = zipsResult.getValue("zip", String.class);
    		Long id = zipsResult.getValue("device_id", Long.class);
        for (USATDevice modem : modems) {
            if (id.equals(modem.getDeviceId()) && modem.getZipCode() == null) {
                modem.setZipCode(zip);
                break;
            }
        }
      }
    }

    String toCsv(long[] values) {
        StringBuilder sb = new StringBuilder("");
        for (long val : values) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(val);
        }
        return sb.toString();
    }
}
