package simple.modem;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.service.modem.service.dto.USATDevice;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import java.sql.SQLException;

/**
 * Modem service to manage modem status and read it state
 *
 * @author dlozenko
 */
public interface USATModemService {
    void activate(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException;
    void deactivate(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException;
    void suspend(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException;
    void resume(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException;
    void refreshActivationStatus(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException;
}
