package simple.modem;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.service.modem.service.dto.USATDevice;
import java.sql.SQLException;

/**
 * DAO to populate USATDevice request param
 *
 * @author dlozenko
 */
public interface USATDeviceDao {
    USATDevice populateByDeviceId(long deviceId) throws DataLayerException, SQLException, ConvertException;
    USATDevice[] populateByDeviceIds(long[] deviceIds) throws DataLayerException, SQLException, ConvertException;
}
