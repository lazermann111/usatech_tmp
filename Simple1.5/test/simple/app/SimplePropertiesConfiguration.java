package simple.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.configuration.AbstractFileConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.PropertyConverter;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.IOUtils;
import simple.io.IOUtils.Replacement;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

public class SimplePropertiesConfiguration extends AbstractFileConfiguration {
	protected static class PropertyInfo implements Replacement {
		public final String comment;
		public final String separator;
		public final String propertyKey;
		public long startProperty;
		public long endProperty;
		public long startValue;
		public long endValue;
		public final FileInfo fileInfo;
		public PropertyInfo nextPropertyInfo;
		public byte[] bytes;
		public byte[] unsavedPrefixBytes;
		public boolean saved;
		public long savedStartValue;
		public long savedEndValue;

		public PropertyInfo(String propertyKey, Object value, FileInfo fileInfo, String separator) {
			this.propertyKey = propertyKey;
			this.separator = separator;
			this.fileInfo = fileInfo;
			long last;
			fileInfo.lock();
			try {
				this.nextPropertyInfo = fileInfo.lastPropertyInfo;
				fileInfo.lastPropertyInfo = this;
				if(fileInfo.firstPropertyInfo == null)
					fileInfo.firstPropertyInfo = this;
				if(fileInfo.lastPropertyInfo == null) {
					last = 0;
				} else
					last = fileInfo.lastPropertyInfo.endProperty;
				if(fileInfo.comment != null && fileInfo.comment.length() > 0) {
					switch(fileInfo.comment.charAt(fileInfo.comment.length() - 1)) {
						case '\n':
						case '\r':
							this.comment = fileInfo.comment;
							break;
						default:
							String eol = fileInfo.newline;
							if(StringUtils.isBlank(eol))
								eol = System.getProperty("line.separator", "\n");
							this.comment = fileInfo.comment + eol;
					}
					fileInfo.comment = "";
				} else {
					String eol = fileInfo.newline;
					if(StringUtils.isBlank(eol))
						eol = System.getProperty("line.separator", "\n");
					this.comment = eol;
				}
			} finally {
				fileInfo.unlock();
			}
			byte[] commentBytes;
			try {
				commentBytes = this.comment.getBytes(fileInfo.encoding);
			} catch(UnsupportedEncodingException e) {
				commentBytes = this.comment.getBytes();
			}
			startProperty = last + commentBytes.length;
			String escapedKey = escapeKey(propertyKey);
			byte[] keyBytes;
			try {
				keyBytes = escapedKey.getBytes(fileInfo.encoding);
			} catch(UnsupportedEncodingException e) {
				keyBytes = escapedKey.getBytes();
			}
			byte[] sepBytes;
			try {
				sepBytes = this.separator.getBytes(fileInfo.encoding);
			} catch(UnsupportedEncodingException e) {
				sepBytes = this.separator.getBytes();
			}
			this.startValue = startProperty + keyBytes.length + sepBytes.length;
			String escapedValue = escapeValue(value);
			try {
				this.bytes = escapedValue.getBytes(fileInfo.encoding);
			} catch(UnsupportedEncodingException e) {
				this.bytes = escapedValue.getBytes();
			}
			this.endValue = this.startValue + this.bytes.length;
			this.endProperty = this.endValue;
			unsavedPrefixBytes = new byte[commentBytes.length + keyBytes.length + sepBytes.length];
			System.arraycopy(commentBytes, 0, unsavedPrefixBytes, 0, commentBytes.length);
			System.arraycopy(keyBytes, 0, unsavedPrefixBytes, commentBytes.length, keyBytes.length);
			System.arraycopy(sepBytes, 0, unsavedPrefixBytes, commentBytes.length + keyBytes.length, sepBytes.length);
			this.savedStartValue = last;
			this.savedEndValue = last;
		}

		public PropertyInfo(String propertyKey, Object value, long startProperty, long endProperty, long startValue, long endValue, FileInfo fileInfo, String comment, String separator) {
			this.propertyKey = propertyKey;
			this.startProperty = startProperty;
			this.endProperty = endProperty;
			this.startValue = startValue;
			this.endValue = endValue;
			this.fileInfo = fileInfo;
			this.comment = comment;
			this.separator = separator;
			fileInfo.lock();
			try {
				this.nextPropertyInfo = fileInfo.lastPropertyInfo;
				fileInfo.lastPropertyInfo = this;
				if(fileInfo.firstPropertyInfo == null)
					fileInfo.firstPropertyInfo = this;
			} finally {
				fileInfo.unlock();
			}
			setValue(value);
			markAsSaved();
		}

		public void adjustOffsets(long diff) {
			startProperty += diff;
			endProperty += diff;
			startValue += diff;
			endValue += diff;
		}

		public void setValue(Object newValue) {
			String newString = escapeValue(newValue);
			byte[] newBytes;
			try {
				newBytes = newString.getBytes(fileInfo.encoding);
			} catch(UnsupportedEncodingException e) {
				newBytes = newString.getBytes();
			}
			fileInfo.lock();
			try {
				if(Arrays.equals(bytes, newBytes))
					return; // nothing to do
				long diff = newBytes.length - bytes.length;
				bytes = newBytes;
				saved = false;
				endProperty += diff;
				endValue += diff;
				// adjust all pi's after this
				for(PropertyInfo pi = nextPropertyInfo; pi != null; pi = pi.nextPropertyInfo)
					pi.adjustOffsets(diff);
			} finally {
				fileInfo.unlock();
			}
		}

		public void markAsSaved() {
			saved = true;
			savedStartValue = startValue;
			savedEndValue = endValue;
		}

		@Override
		public long getStart() {
			return savedStartValue;
		}

		@Override
		public long getEnd() {
			return savedStartValue;
		}

		@Override
		public byte[] getBytes() {
			return bytes;
		}
	}

	protected static class FileInfo extends ReentrantLock {
		private static final long serialVersionUID = -1981723488139377583L;
		public final RandomAccessFile raf;
		public PropertyInfo lastPropertyInfo;
		public PropertyInfo firstPropertyInfo;
		public final String encoding;
		public String newline = "\n";
		public String comment;

		public FileInfo(RandomAccessFile raf, String encoding) {
			this.raf = raf;
			this.encoding = encoding;
		}
	}

	protected static class CharProducer {
		protected final FileChannel channel;
		protected final ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
		protected final CharBuffer charBuffer = CharBuffer.allocate(1); // Read one at a time so we can get the file position
		protected final CharsetDecoder decoder;
		protected CoderResult result = CoderResult.UNDERFLOW;

		public CharProducer(FileChannel channel, String encoding) {
			super();
			this.channel = channel;
			if(encoding == null)
				this.decoder = Charset.defaultCharset().newDecoder();
			else
				this.decoder = Charset.forName(encoding).newDecoder();
		}

		public int next() throws IOException {
			OUTER: while(true) {
				if(result.isUnderflow()) {
					int r = channel.read(byteBuffer);
					switch(r) {
						case -1:
							result = decoder.decode(byteBuffer, charBuffer, true);
							if(result.isUnderflow())
								return -1;
						case 0:
							continue OUTER;
					}
				} else if(result.isOverflow()) {
					// process and call again
					char ch = charBuffer.get(0);
					charBuffer.clear();
					result = decoder.decode(byteBuffer, charBuffer, false);
					return ch;
				} else if(result.isError() || result.isMalformed() || result.isUnmappable()) {
					result.throwException();
				}
			}
		}

		public long position() throws IOException {
			return channel.position() + byteBuffer.position();
		}
	}

	protected boolean ignoreUnparseableLines = true;
	protected final Cache<String, List<PropertyInfo>, RuntimeException> propertyInfos = new LockSegmentCache<String, List<PropertyInfo>, RuntimeException>() {
		protected List<PropertyInfo> createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new ArrayList<PropertyInfo>(1);
		}
	};
	protected final Cache<File, FileInfo, IOException> fileInfos = new LockSegmentCache<File, FileInfo, IOException>() {
		protected FileInfo createValue(File key, Object... additionalInfo) throws IOException {
			return new FileInfo(new RandomAccessFile(key, "rw"), getEncoding());
		}
	};
	protected String include = "include";
	protected File firstFile;

	protected static enum Stage {
		START, COMMENT, EOL, KEY, SEPARATOR, VALUE
	}
	@Override
	public void load(File file) throws ConfigurationException {
		try {
			FileInfo fileInfo = fileInfos.getOrCreate(file);
			FileLock fileLock = fileInfo.raf.getChannel().lock(0L, Long.MAX_VALUE, true);
			try {
				if(firstFile == null)
					firstFile = file;
				clearPropertyInfos(fileInfo);
				FileChannel channel = fileInfo.raf.getChannel();
				CharProducer producer = new CharProducer(channel, fileInfo.encoding);
				StringBuilder ws = new StringBuilder();
				int n = 1;
				boolean nl = false;
				int ch;
				// 1. find first non whitespace
				StringBuilder comment = new StringBuilder();				
				COMMENT: while((ch = producer.next()) >= 0) {
					switch(ch) {
						case '\r':
							comment.append((char) ch);
							if(nl)
								nl = false;
							else
								n++;
							break;
						case '\n':
							comment.append((char) ch);
							nl = true;
							n++;
							break;
						case ' ':
						case '\t':
							comment.append(ch);
							nl = false;
							break;							
						case '\\':
							nl = false;
							if((ch = producer.next()) < 0) {
								if(!isIgnoreUnparseableLines())
									throw new ConfigurationException("Unparseable at line " + n + "; char " + producer.position());
								comment.append('/');
							} else {
								switch(ch) {
									case '\n':
										nl = true;
									case '\r':
										// continuation
										comment.append('/');
										n++;
										break;
									case 't':
										ch = '\t';
										break;
									case 'b':
										ch = '\b';
										break;
									case 'n':
										ch = '\n';
										break;
									case 'f':
										ch = '\f';
										break;
									case 'r':
										ch = '\r';
										break;
									case 'u':
										char[] unicode = new char[4];
										for(int i = 0; i < 4; i++) {
											ch = producer.next();
											if(ch < 0)
												throw new ConfigurationException("Unfinished unicode escape at line " + n + "; char " + producer.position());
											unicode[i] = (char) ch;
										}
										ch = Integer.parseInt(new String(unicode), 16);
										break;
								}
								comment.append((char) ch);
							}
							break;
						case '#':
						case '!':
							comment.append((char) ch);
							// read to new line
							while((ch = producer.next()) >= 0) {
								comment.append((char) ch);
								switch(ch) {
									case '\n':
										nl = true;										
									case '\r':
										n++;
										continue COMMENT;
								}
							}
							nl = false;
							break;
						default:
							// 2. find separator
							nl = false;
							comment.append(ws);
							ws.setLength(0);
							StringBuilder key = new StringBuilder();				
							key.append((char)ch);
							/*KEY:*/while((ch = producer.next()) >= 0) {
								switch(ch) {
									case '\n':
										nl = true;
									case '\r':
										if(!isIgnoreUnparseableLines())
											throw new ConfigurationException("Unparseable at line " + n + "; char " + producer.position());
										n++;
										comment.append(key);
										comment.append(ws);
										ws.setLength(0);
										comment.append((char) ch);
										continue COMMENT;
									case '\\':
										nl = false;
										key.append(ws);
										ws.setLength(0);
										if((ch = producer.next()) < 0) {
											if(!isIgnoreUnparseableLines()) 
												throw new ConfigurationException("Unparseable at line " + n + "; char " + producer.position());
											ch = '\\';
										} else {
											switch(ch) {
												case '\n':
													nl = true;
												case '\r':
													if(!isIgnoreUnparseableLines())
														throw new ConfigurationException("Unparseable at line " + n + "; char " + producer.position());
													n++;
													comment.append(key);
													comment.append('\\');
													comment.append((char) ch);
													continue COMMENT;
												case 't':
													ch = '\t';
													break;
												case 'b':
													ch = '\b';
													break;
												case 'n':
													ch = '\n';
													break;
												case 'f':
													ch = '\f';
													break;
												case 'r':
													ch = '\r';
													break;
												case 'u':
													char[] unicode = new char[4];
													for(int i = 0; i < 4; i++) {
														ch = producer.next();
														if(ch < 0)
															throw new ConfigurationException("Unfinished unicode escape at line " + n + "; char " + producer.position());
														unicode[i] = (char) ch;
													}
													ch = Integer.parseInt(new String(unicode), 16);
													break;
											}
										}
										key.append((char) ch);
										break;
									case ':':
									case '=':
										// 3. find eol
										nl = false;
										StringBuilder sep = new StringBuilder();
										sep.append(ws);
										ws.setLength(0);
										sep.append((char) ch);
										StringBuilder value = new StringBuilder();
										List<String> valueList = null;
										long startProperty = producer.position();
										long startValue = 0;
										VALUE: while((ch = producer.next()) >= 0) {
											switch(ch) {
												case '\n':
													nl = true;
												case '\r':
													n++;
													//4. create property
													long endProperty = producer.position();
													if(startValue == 0) {
														sep.append(ws);
														ws.setLength(0);
														startValue = endProperty;
													}
													String skey = key.toString();
													if(valueList != null) {
														valueList.add(value.toString());
														if(isInclude(skey)) {
															for(String include : valueList)
																if(include != null && !(include = include.trim()).isEmpty())
																	loadIncludeFile(include);
														} else
															addProperty(skey, valueList, fileInfo, startProperty, endProperty, startValue, endProperty, comment.toString(), sep.toString());
													} else {
														String svalue = value.toString();
														if(isInclude(skey)) {
															if(!(svalue = svalue.trim()).isEmpty())
																loadIncludeFile(svalue);
														} else
															addProperty(skey, svalue, fileInfo, startProperty, endProperty, startValue, endProperty, comment.toString(), sep.toString());
													}
													comment.setLength(0);
													comment.append(ws);
													ws.setLength(0);
													comment.append((char) ch);
													continue COMMENT;
												case '\\':
													if(startValue == 0) {
														sep.append(ws);
														startValue = producer.position();
													} else if(value.length() > 0)
														value.append(ws);
													ws.setLength(0);
													if((ch = producer.next()) < 0) {
														if(!isIgnoreUnparseableLines()) 
															throw new ConfigurationException("Unparseable at line " + n + "; char " + producer.position());
														ch = '\\';
													} else {
														switch(ch) {
															case '\n':
																nl = true;
															case '\r': //continuation
																n++;
																continue VALUE;
															case 't':
																ch = '\t';
																break;
															case 'b':
																ch = '\b';
																break;
															case 'n':
																ch = '\n';
																break;
															case 'f':
																ch = '\f';
																break;
															case 'r':
																ch = '\r';
																break;
															case 'u':
																char[] unicode = new char[4];
																for(int i = 0; i < 4; i++) {
																	ch = producer.next();
																	if(ch < 0)
																		throw new ConfigurationException("Unfinished unicode escape at line " + n + "; char " + producer.position());
																	unicode[i] = (char) ch;
																}
																ch = Integer.parseInt(new String(unicode), 16);
																break;
														}
													}
													value.append((char) ch);
													break;
												case ' ':
												case '\t':
													ws.append(ch);
													break;
												case ',':
													// a list!
													if(valueList == null)
														valueList = new ArrayList<String>(2);
													valueList.add(value.toString());
													value.setLength(0);
													ws.setLength(0);
													break;
												default:
													if(startValue == 0) {
														sep.append(ws);
														startValue = producer.position();
													} else if(value.length() > 0)
														value.append(ws);
													ws.setLength(0);
													value.append((char) ch);
													break;
											}
										}
										break;
									case ' ':
									case '\t':
										ws.append(ch);
										break;
									default:
										key.append(ws);
										ws.setLength(0);
										key.append((char) ch);
										break;
								}
							}
							break;
					}
				}
				fileInfo.comment = comment.toString();
			} finally {
				fileLock.release();
			}
		} catch(IOException e) {
			throw new ConfigurationException(e);
		}
	}

	protected void clearPropertyInfos(FileInfo fileInfo) {
		PropertyInfo pi = fileInfo.firstPropertyInfo;
		for(; pi != null; pi = pi.nextPropertyInfo) {
			List<PropertyInfo> pis = propertyInfos.getIfPresent(pi.propertyKey);
			if(pis != null)
				synchronized(pis) {
					pis.remove(pi);
				}
		}
	}

	protected boolean isInclude(String key) {
		return !StringUtils.isBlank(getInclude()) && key.equalsIgnoreCase(getInclude());
	}

	protected void addProperty(String key, Object value, FileInfo fileInfo, long startProperty, long endProperty, long startValue, long endValue, String comment, String separator) {
		List<PropertyInfo> pis = propertyInfos.getOrCreate(key);
		synchronized(pis) {
			if(!pis.isEmpty()) {
				PropertyInfo lastPi = pis.get(pis.size() - 1);
				if(!lastPi.fileInfo.equals(fileInfo))
					clearPropertyDirect(key);
			}
			pis.add(new PropertyInfo(key, value, startProperty, endProperty, startValue, endValue, fileInfo, comment, separator));
		}
		addPropertyDirect(key, value);
	}

	protected void loadIncludeFile(String fileName) throws ConfigurationException {
		URL url = ConfigurationUtils.locate(getFileSystem(), getBasePath(), fileName);
		if(url == null) {
			URL baseURL = getURL();
			if(baseURL != null) {
				url = ConfigurationUtils.locate(getFileSystem(), baseURL.toString(), fileName);
			}
		}

		if(url == null)
			throw new ConfigurationException("Cannot resolve include file " + fileName);

		load(url);
	}

	@Override
	public void load(URL url) throws ConfigurationException {
		File file = ConfigurationUtils.fileFromURL(url);
		if(file != null)
			load(file);
		else
			super.load(url);
	}

	@Override
	public void load(Reader in) throws ConfigurationException {
		throw new ConfigurationException("This method is not supported. Use load(File file) or load(String fileName) or load(URL url) instead");
	}

	@Override
	public void save(Writer out) throws ConfigurationException {
		File file = getFile();
		if(file == null) {
			file = firstFile;
			if(file == null)
				return;
		}
		try {
			FileInfo fileInfo = fileInfos.getIfPresent(file);
			if(fileInfo == null)
				return;
			for(PropertyInfo pi = fileInfo.firstPropertyInfo; pi != null; pi = pi.nextPropertyInfo) {
				if(pi.comment != null)
					out.write(pi.comment);
				out.write(pi.propertyKey);
				out.write(pi.separator);
				String svalue = ConvertUtils.getStringSafely(getProperty(pi.propertyKey), "");
				out.write(svalue);
			}
			if(fileInfo.comment != null)
				out.write(fileInfo.comment);
			out.flush();
		} catch(IOException e) {
			throw new ConfigurationException(e);
		}
	}

	@Override
	public void save(File file) throws ConfigurationException {
		try {
			FileInfo fileInfo = fileInfos.getIfPresent(file);
			if(fileInfo == null)
				save(new FileOutputStream(file));
		} catch(IOException e) {
			throw new ConfigurationException(e);
		}
		// TODO: see if there are pending file updates and process them
	}

	@Override
	public void save(URL url) throws ConfigurationException {
		File file = ConfigurationUtils.fileFromURL(url);
		if(file != null)
			save(file);
		else
			super.save(url);
	}

	public boolean isIgnoreUnparseableLines() {
		return ignoreUnparseableLines;
	}

	public void setIgnoreUnparseableLines(boolean ignoreUnparseableLines) {
		this.ignoreUnparseableLines = ignoreUnparseableLines;
	}

	protected void updatePropertyInFile(String key, Object oldValue, Object newValue) throws IOException {
		String oldString = escapeValue(oldValue);
		String newString = escapeValue(newValue);
		if(!oldString.equals(newString)) {
			List<PropertyInfo> pis = propertyInfos.getOrCreate(key);
			FileInfo fileInfo;
			synchronized(pis) {
				if(!pis.isEmpty()) {
					ListIterator<PropertyInfo> iter = pis.listIterator(pis.size());
					PropertyInfo pi = iter.previous();
					while(iter.hasPrevious()) {
						PropertyInfo prevPi = iter.previous();
						if(prevPi.fileInfo.equals(pi.fileInfo))
							pi = prevPi;
						else
							break;
					}
					pi.setValue(newValue);
					fileInfo = pi.fileInfo;
				} else {
					File file = new File(fileName);
					fileInfo = fileInfos.getOrCreate(file);
					String separator;
					PropertyInfo pi = fileInfo.lastPropertyInfo;
					if(pi != null)
						separator = pi.separator;
					else
						separator = "=";
					pis.add(new PropertyInfo(key, newValue, fileInfo, separator));
				}
			}
			if(isAutoSave())
				saveUpdate(fileInfo);
		}
	}

	protected void saveUpdate(FileInfo fileInfo) throws IOException {
		FileLock fileLock = fileInfo.raf.getChannel().lock(0L, Long.MAX_VALUE, false);
		try {
			for(PropertyInfo pi = fileInfo.firstPropertyInfo; pi != null; pi = pi.nextPropertyInfo) {
				if(!pi.saved) {
					IOUtils.replaceRandomAccessBytes(fileInfo.raf, pi.getStart(), pi.getEnd(), pi.getBytes());
					pi.markAsSaved();
				}
			}
		} finally {
			fileLock.release();
		}
	}

	@Override
	public void setProperty(String key, Object value) {
		Object oldValue = getProperty(key);
		fireEvent(EVENT_SET_PROPERTY, key, value, true);
		setDetailEvents(false);
		try {
			clearPropertyDirect(key);
			addPropertyDirect(key, value);
		} finally {
			setDetailEvents(true);
		}
		fireEvent(EVENT_SET_PROPERTY, key, value, false);
		if(isAutoSave())
			try {
				updatePropertyInFile(key, oldValue, value);
			} catch(IOException e) {
				throw new UndeclaredThrowableException(e);
			}
	}

	public void addProperty(String key, Object value) {
		Object oldValue = getProperty(key);
		if(oldValue != null) {
			List<Object> list;
			if(ConvertUtils.isCollectionType(oldValue.getClass()))
				try {
					list = new ArrayList<Object>(ConvertUtils.asCollection(oldValue));
				} catch(ConvertException e) {
					list = new ArrayList<Object>(2);
					list.add(oldValue);
				}
			else if(!isDelimiterParsingDisabled() && oldValue instanceof String) {
				list = new ArrayList<Object>(PropertyConverter.split((String) oldValue, getListDelimiter()));
			} else {
				list = new ArrayList<Object>(2);
				list.add(oldValue);
			}
			list.add(value);
			value = list;
		}
		fireEvent(EVENT_ADD_PROPERTY, key, value, true);
		addPropertyDirect(key, value);
		fireEvent(EVENT_ADD_PROPERTY, key, value, false);
		if(isAutoSave())
			try {
				updatePropertyInFile(key, oldValue, value);
			} catch(IOException e) {
				throw new UndeclaredThrowableException(e);
			}
	}

	public void clear() {
		propertyInfos.clear();
		super.clear();
		for(FileInfo fileInfo : fileInfos.values()) {
			fileInfo.comment = "";
			fileInfo.firstPropertyInfo = null;
			fileInfo.lastPropertyInfo = null;
			if(isAutoSave())
				try {
					fileInfo.raf.setLength(0);
				} catch(IOException e) {
					throw new UndeclaredThrowableException(e);
				}
		}
	}

	@Override
	public void clearProperty(String key) {
		// TODO:
	}

	protected static String escapeKey(String key) {
		if(key == null)
			return "";
		return escapeKey(new StringBuilder(), key).toString();
	}

	protected static String escapeValue(Object value) {
		if(value == null)
			return "";
		return escapeValue(new StringBuilder(), value).toString();
	}

	protected static StringBuilder escapeValue(StringBuilder sb, Object value) {
		if(value != null) {
			if(ConvertUtils.isCollectionType(value.getClass())) {
				// NOTE: n-dimensional collections get flattened
				boolean first = true;
				try {
					for(Object o : ConvertUtils.asCollection(value)) {
						if(first)
							first = false;
						else
							sb.append(',');
						escapeValue(sb, o);
					}
					return sb;
				} catch(ConvertException e) {
					// do nothing
				}
			} else {
				return escapeValue(sb, ConvertUtils.getStringSafely(value));
			}
		}
		return sb;
	}

	protected static StringBuilder escapeValue(StringBuilder sb, String svalue) {
		if(svalue != null)
			for(int i = 0; i < svalue.length(); i++) {
				char ch = svalue.charAt(i);
				switch(ch) {
					case '\n':
						sb.append('\\').append('n');
						break;
					case '\r':
						sb.append('\\').append('r');
						break;
					case '\t':
						sb.append('\\').append('t');
						break;
					case '\b':
						sb.append('\\').append('b');
						break;
					case '\f':
						sb.append('\\').append('f');
						break;
					case '\\':
					case ',':
						sb.append('\\').append(ch);
						break;
					case ' ':
						if(i + 1 >= svalue.length())
							sb.append('\\');
					default:
						sb.append(ch);
				}
			}
		return sb;
	}

	protected static StringBuilder escapeKey(StringBuilder sb, String key) {
		if(key != null)
			for(int i = 0; i < key.length(); i++) {
				char ch = key.charAt(i);
				switch(ch) {
					case '\n':
						sb.append('\\').append('n');
						break;
					case '\r':
						sb.append('\\').append('r');
						break;
					case '\t':
						sb.append('\\').append('t');
						break;
					case '\b':
						sb.append('\\').append('b');
						break;
					case '\f':
						sb.append('\\').append('f');
						break;
					case '\\':
					case '=':
					case ':':
						sb.append('\\').append(ch);
						break;
					case ' ':
						if(i + 1 >= key.length())
							sb.append('\\');
					default:
						sb.append(ch);
				}
			}
		return sb;
	}

	public String getInclude() {
		return include;
	}

	public void setInclude(String include) {
		this.include = include;
	}
}

