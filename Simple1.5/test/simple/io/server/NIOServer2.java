package simple.io.server;

import java.io.EOFException;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.ByteChannel;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import simple.app.ServiceException;
import simple.io.ByteChannelByteInput;
import simple.io.ByteChannelByteOutput;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.io.Log;
import simple.lang.Initializer;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.CreationException;
import simple.util.concurrent.ExtReentrantLock;
import simple.util.concurrent.LockSegmentCache;
import simple.util.concurrent.Pool;

public abstract class NIOServer2<R extends InputInterpretter> implements Pool<R>, NIOServerMXBean {
	private static final Log log = Log.getLog();
	protected InetSocketAddress socketAddress;
	protected Selector selector;
	protected ServerSocketChannel serverChannel;
	protected int maxConnections = 64;
	protected int bufferSize = 1024;
	protected final AtomicInteger connections = new AtomicInteger();
	protected final AtomicInteger active = new AtomicInteger();
	protected final Map<R,Attachment> attachments = new ConcurrentHashMap<R,Attachment>();
	protected final ExtReentrantLock lock = new ExtReentrantLock();
	protected final Queue<SelectionKey> readableKeys = new ConcurrentLinkedQueue<SelectionKey>();
	protected static final int THREAD_STATUS_RUN = 0;
	protected static final int THREAD_STATUS_INTERRUPT = 1;
	protected static enum AttachmentState {READY, PREPARED }
	protected static final String jmxNamePrefix = NIOServer2.class.getPackage().getName() + ":Type=" + NIOServer2.class.getSimpleName();
	protected boolean jmxEnabled = true;
	protected boolean jmxDetailEnabled = false;
	protected ObjectName jmxObjectName;
	
	protected final Cache<Thread, AtomicBoolean, RuntimeException> threadStatus = new LockSegmentCache<Thread, AtomicBoolean, RuntimeException>() {
		/**
		 * @see simple.util.concurrent.LockSegmentCache#createValue(java.lang.Object, java.lang.Object[])
		 */
		@Override
		protected AtomicBoolean createValue(Thread key, Object... additionalInfo) throws RuntimeException {
			return new AtomicBoolean();
		}
	};

	protected final Initializer<IOException> initializer = new Initializer<IOException>() {
		@Override
		protected void doInitialize() throws IOException {
			selector = Selector.open();
			serverChannel = ServerSocketChannel.open();
			serverChannel.socket().bind(socketAddress);
			serverChannel.configureBlocking(false);
			serverChannel.register(selector, serverChannel.validOps(), null);
			log.info("Listening on port " + socketAddress + " for requests");
			if(isJmxEnabled()) {
				try {
					ObjectName jmxObjectName = new ObjectName(jmxNamePrefix + ",Address=" + StringUtils.prepareJMXNameValue(socketAddress.toString()));
					ManagementFactory.getPlatformMBeanServer().registerMBean(NIOServer2.this, jmxObjectName);
					NIOServer2.this.jmxObjectName = jmxObjectName;
				} catch(InstanceAlreadyExistsException e) {
					log.warn("Could not register NIOServer at '" + socketAddress.toString() + "' in JMX", e);
				} catch(MBeanRegistrationException e) {
					log.warn("Could not register NIOServer at '" + socketAddress.toString() + "' in JMX", e);
				} catch(NotCompliantMBeanException e) {
					log.warn("Could not register NIOServer at '" + socketAddress.toString() + "' in JMX", e);
				} catch(MalformedObjectNameException e) {
					log.warn("Could not register NIOServer at '" + socketAddress.toString() + "' in JMX", e);
				} catch(NullPointerException e) {
					log.warn("Could not register NIOServer at '" + socketAddress.toString() + "' in JMX", e);
				}
			}
		}
		@Override
		protected void doReset() {
			if(selector != null) {
				try {
					selector.close();
				} catch(IOException e) {
					log.warn("Could not close selector", e);
				}
				selector = null;
			}
			if(serverChannel != null) {
				try {
					serverChannel.close();
				} catch(IOException e) {
					log.warn("Could not close server socket channel", e);
				}
				serverChannel = null;
			}
			connections.set(0);
			if(jmxObjectName != null) {
				try {
					ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmxObjectName);
				} catch(InstanceNotFoundException e) {
					log.warn("Could not unregister NIOServer at '" + socketAddress.toString() + "' in JMX", e);
				} catch(MBeanRegistrationException e) {
					log.warn("Could not unregister NIOServer at '" + socketAddress.toString() + "' in JMX", e);
				} catch(NullPointerException e) {
					log.warn("Could not unregister NIOServer at '" + socketAddress.toString() + "' in JMX", e);
				}
				jmxObjectName = null;
			}
		}
	};

	protected static String getClientInfo(SocketChannel socketChannel) {
		if(socketChannel != null) {
			Socket socket = socketChannel.socket();
			if(socket != null)
				return ", client: " + socket.getRemoteSocketAddress() + " on port " + socket.getLocalPort();
		}
		return "";
	}

	protected interface DataHandler {
		public SocketReadResult handleData(ByteInput input, ByteOutput output) throws ServiceException, IOException;
	}
	protected class Attachment {
		protected final SocketChannel socketChannel;
		protected final ByteChannelByteInput input;
		protected final ByteChannelByteOutput output;
		protected final DataHandler handler;

		public Attachment(SocketChannel socketChannel, ByteChannel byteChannel, DataHandler handler) {
			super();
			this.socketChannel = socketChannel;
			this.handler = handler;
			this.input = new ByteChannelByteInput(byteChannel, getBufferSize());
			this.output = new ByteChannelByteOutput(byteChannel, getBufferSize());
		}

		public Socket getSocket() {
			return socketChannel.socket();
		}

		public DataHandler getDataHandler() {
			return handler;
		}
		
		public R getInterpretter() {
			return null;
		}

		public SocketReadResult read() {
			try {
				socketChannel.configureBlocking(true);
			} catch(IOException e) {
				log.info("Closing connection" + getClientInfo(socketChannel) + " because could not set socket to blocking mode: " + e.getMessage());
				close(false);
				return SocketReadResult.CANCEL;
			}
			SocketReadResult result;
			try {
				result = handler.handleData(input, output);
			} catch(EOFException e) {
				close(true);
				return SocketReadResult.CLOSED;
			} catch(IOException e) {
				log.info("Closing connection" + getClientInfo(socketChannel) + " because could not read from socket: " + e.getMessage());
				close(false);
				return SocketReadResult.CANCEL;
			} catch(ServiceException e) {
				log.warn("Processing data on connection" + getClientInfo(socketChannel) + " failed", e);
				result = SocketReadResult.RETURN;
			}
			switch(result) {
				case CLOSED:
					close(true);
					break;
				case CANCEL:
					log.info("Closing cancelled connection" + getClientInfo(socketChannel));
					close(false);
					break;
				case RETURN:
					try {
						socketChannel.configureBlocking(false);
					} catch(IOException e) {
						log.info("Closing connection" + getClientInfo(socketChannel) + " because could not set socket to non-blocking mode: " + e.getMessage());
						close(false);
						return SocketReadResult.CANCEL;
					}
					break;
			}
			return result;
		}

		protected void close(boolean clientInitiated) {
			try {
				socketChannel.close(); // this will cancel the channel in the selector
			} catch(IOException e) {
				log.warn("Could not close socketChannel" + getClientInfo(socketChannel), e);
			}
			remove(clientInitiated);
		}

		protected void remove(boolean clientInitiated) {
			if(attachments.remove(handler) != null) {
				int n = connections.decrementAndGet(); //decrement whether we successfully closed channel or not
				log.info((clientInitiated ? "Client" : "Server") + " closed the connection" + getClientInfo(socketChannel) + " [" + connections.get() + " connections active]");
				if(n < getMaxConnections()) {
					SelectionKey key = serverChannel.keyFor(selector);
					if(key == null) {
						log.warn("ServerChannel key for port " + serverChannel.socket().getLocalPort() + " is not registered with the selector. Either application is being shutdown or an unexpected state occurred");
					} else if(key.interestOps() == 0) {
						try {
							key.interestOps(serverChannel.validOps());//start interest in ACCEPTABLE
						} catch(IllegalArgumentException e) {
							log.warn("Invalid interest ops for server channel on port " + serverChannel.socket().getLocalPort(), e);
						} catch(CancelledKeyException e) {
							log.warn("Selection key is cancelled for server channel on port " + serverChannel.socket().getLocalPort(), e);
						}
						log.info("Re-registered ACCEPTABLE interest for ServerChannel key for port " + serverChannel.socket().getLocalPort() + " to accept new connections again");
						selector.wakeup();
					}
				}
			}
			// interpretter.close(clientInitiated);
		}

		public void complete(boolean closeConnection, boolean clientInitiated) {
			// interpretter.flushOutput();
			if(closeConnection) {
				close(clientInitiated);
			} else {
				boolean hasNext = input.available() > 0.;
				//re-register channel for next request
				Selector sel = selector;
				if(sel != null && sel.isOpen())
		            try {
		            	SelectionKey key = socketChannel.keyFor(sel);
		            	if(key != null) {
		            		if(!hasNext || !readableKeys.offer(key)) {
			            		if(key.isValid()) {
			            			key.interestOps(SelectionKey.OP_READ);
			            			//the above does not seem to notify select() on the selector, so we will
			            			sel.wakeup();
			            		} else
			            			close(false);
		            		} else {
		            			sel.wakeup();
		            		}
		            	} else {
		            		if(hasNext) {
		            			key = socketChannel.register(sel, 0, this);
		            			if(!readableKeys.offer(key)) {
		            				if(key.isValid()) {
				            			key.interestOps(SelectionKey.OP_READ);
				            			//the above does not seem to notify select() on the selector, so we will
				            			sel.wakeup();
				            		} else
				            			close(false);
		            			} else {
			            			sel.wakeup();
			            		}
		            		} else {
		            			socketChannel.register(sel, SelectionKey.OP_READ, this);
		            		}		            		
		            		log.info("Had to re-register channel" + getClientInfo(socketChannel));
		            	}
					} catch(ClosedChannelException e) {
						remove(false);
						log.info("Could not re-register channel because it is closed" + getClientInfo(socketChannel), e);
					}
			}
		}
	}
	public NIOServer2(int port) {
		this(new InetSocketAddress(port));
	}

	public NIOServer2(InetSocketAddress socketAddress) {
		this.socketAddress = socketAddress;
	}

	public NIOServer2() {
	}

	public void clear() {
		// do nothing
	}

	public void close() {
		initializer.reset();
	}

	/** Determines if a thread was cancelled and then clears its cancelled status. If the thread had been cancelled
	 * and this call cleared that status then <code>true</code> is returned
	 * @param thread
	 * @return
	 */
	protected boolean isCancelled(Thread thread) {
		return threadStatus.getOrCreate(thread).compareAndSet(true, false);
	}

	/** Cancels the given thread so that the next call to borrowObject returns null
	 * @param thread
	 */
	public void cancel(Thread thread) {
		threadStatus.getOrCreate(thread).set(true);
		Selector selector = this.selector; //Guard against NPE if selector changes mid-stream
		if(selector != null && selector.isOpen())
			selector.wakeup();
	}

	public R borrowObject() throws TimeoutException, CreationException, InterruptedException {
		while(true) {
			lock.lockInterruptibly();
			SelectionKey key;
			try {
				key = nextSelectionkey();
			} catch(IOException e) {
				throw new CreationException(e);
			} finally {
				lock.unlock();
			}
			if(key == null) return null;
			final Attachment attachment = (Attachment)key.attachment();
			SocketReadResult result = attachment.read();
			switch(result) {
				case CLOSED:
					key.cancel();
					attachment.close(true);
					break;
				case CANCEL:
					key.cancel();
					attachment.close(false);
					break;
				case RETURN:
					active.incrementAndGet();
					return attachment.getInterpretter();
				case CONTINUE:
					try {
						key.interestOps(SelectionKey.OP_READ);//start interest in READABLE
						selector.wakeup();
					} catch(CancelledKeyException e) {
						if (log.isDebugEnabled())
							log.debug("Key was cancelled. Getting next.");
						attachment.close(false);
					}
			}
		}
	}

	public void invalidateObject(R obj) {
		invalidateObject(obj, false);
	}
	
	public void invalidateObject(R obj, boolean clientInitiated) {
		Attachment attachment = attachments.get(obj);
		if(attachment != null) {
			attachment.complete(true, clientInitiated);
			active.decrementAndGet();
		}
	}

	public void returnObject(R obj) {
		Attachment attachment = attachments.get(obj);
		if(attachment != null) {
			attachment.complete(false, false);
			active.decrementAndGet();
		}
	}

	public int getNumActive() {
		return active.get();
	}

	public int getNumIdle() {
		return connections.get() - getNumActive();
	}

	public int getNumConnected() {
		return connections.get();
	}
	
	/** This implementation is NOT thread-safe nor are the NIORequest objects returned from it.
	 *  However, the NIORequest objects returned from this implementation are independent of
	 *  each other and invocations on different NIORequest objects may occur at the same time in
	 *  different threads.
	 * @throws IOException
	 *
	 */
	protected SelectionKey nextSelectionkey() throws IOException {
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			log.info("Interrupted while initializing");
			return null;
		}
		Thread thread = null;
		while(true) {
			SelectionKey key = readableKeys.poll();
			if(key != null) {
				if(log.isDebugEnabled())
					log.debug("Found READABLE Channel " + key);
				return key;
			}
			Selector selector = this.selector; //Guard against NPE
			if(selector == null || !selector.isOpen())
                return null;
			while(selector.selectedKeys().isEmpty()) {
				if(thread == null)
					thread = Thread.currentThread();
				if(isCancelled(thread))
					return null;
				if(log.isDebugEnabled())
					log.debug("Polling selector for next available socket");
				selector.select();
				key = readableKeys.poll();
				if(key != null) {
					if(log.isDebugEnabled())
						log.debug("Found READABLE Channel " + key);
					return key;
				}
			}
			Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
			while(iter.hasNext()) {
				key = iter.next();
				SocketChannel socketChannel = null;
				try {
					if(key.isAcceptable() && key.channel() instanceof ServerSocketChannel) {
						if(connections.getAndIncrement() < getMaxConnections()) {
							try {
								socketChannel = ((ServerSocketChannel)key.channel()).accept();
							} catch(ClosedByInterruptException e) {
								log.warn("Server Channel closed by interrupt" + getClientInfo(socketChannel));
								connections.decrementAndGet();
								return null;
							} catch(ClosedChannelException e) {
								connections.decrementAndGet();
								log.warn("Server Channel is closed before accepting" + getClientInfo(socketChannel));
								continue;
							} catch(IOException e) {
								connections.decrementAndGet();
								log.warn("Could not accept channel" + getClientInfo(socketChannel), e);
								continue;
							}
							if(socketChannel == null) {
								connections.decrementAndGet();
								if (log.isDebugEnabled())
									log.debug("Received null channel from accept()");
								//remove key from selected set
								iter.remove();
							} else if(!allowConnection(socketChannel.socket())) {
								if (log.isDebugEnabled())
									log.debug("Rejecting connection" + getClientInfo(socketChannel));
								socketChannel.close();
								connections.decrementAndGet();
								//remove key from selected set
								iter.remove();
							} else {
								final ByteChannel byteChannel = (jmxObjectName != null && isJmxDetailEnabled() ? new TrackingJMXByteChannel(socketChannel, jmxObjectName.toString(), socketChannel.socket().getRemoteSocketAddress().toString()) : socketChannel);
								R interpretter;
								try {
									socketChannel.configureBlocking(false);
									interpretter = createInterpretter(socketChannel.socket(), byteChannel);
								} catch(IOException e) {
									log.warn("Could not setup socket or intepretter" + getClientInfo(socketChannel), e);
									continue;
								}
								//socketChannel.register(selector, SelectionKey.OP_READ, new Attachment(socketChannel, byteChannel, interpretter));
								socketChannel.register(selector, SelectionKey.OP_READ, new Attachment(socketChannel, byteChannel, null));
								onSocketAccepted(socketChannel, interpretter);
								log.info("Accepting a connection" + getClientInfo(socketChannel) + " [" + connections.get() + " connections active]");
							}
						} else {
							connections.decrementAndGet();
							// deregister interest in acceptable ops for the serversocketchannel?
							key.interestOps(0); //cancel();
							iter.remove();
							log.info("Max Connections (" + getMaxConnections() + ") reached. Not accepting more.");
							if(log.isDebugEnabled()) {
								// show info on each connection
								StringBuilder sb = new StringBuilder();
								sb.append("List of Current Connections:");
								for(SelectionKey sk : selector.keys()) {
									SelectableChannel channel = sk.channel();
									if(channel instanceof SocketChannel) {
										SocketChannel sc = (SocketChannel) channel;
										Socket socket = sc.socket();
										Attachment attachment = (Attachment)sk.attachment();
										if(sc.isConnected()) {
											sb.append("\nConnection [From=").append(socket.getInetAddress()).append("; LocalPort=").append(sc.socket().getLocalPort())
												.append("] is connected ");
											if(attachment == null)
												sb.append("WIHTOUT a session");
											else
												sb.append("with session ").append(attachment.getInterpretter());
										} else {
											sb.append("\nConnection [LocalPort=").append(socket.getLocalPort())
												.append("] is disconnected ")
												.append(sc.isRegistered() ? " but is still registered" : " but is not registered");
											if(attachment == null)
												sb.append("WIHTOUT a session");
											else
												sb.append("with session ").append(attachment.getInterpretter());
										}
									}
								}
								log.debug(sb);
							}
						}
					} else if(key.isReadable() && key.channel() instanceof SocketChannel) {
						key.interestOps(0);//stop interest in READABLE
						iter.remove();
						if(log.isDebugEnabled())
							log.debug("Found READABLE Channel " + key);
						return key;
					} else {
						log.warn("Key is Selected; but doesn't match expected operations: Channel Class=" + key.channel().getClass().getName() + "; Ready Ops=" + key.readyOps());
					}
				} catch(CancelledKeyException e) {
					if (log.isDebugEnabled())
						log.debug("Key was cancelled. Getting next.");
					iter.remove();
					final Attachment attachment = (Attachment)key.attachment();
					if(attachment != null) {
						attachment.close(false);
					}
				} catch(ClosedByInterruptException e) {
					log.warn("Socket Channel closed by interrupt" + getClientInfo(socketChannel));
					iter.remove();
					final Attachment attachment = (Attachment)key.attachment();
					if(attachment != null) {
						attachment.close(false);
					}
					return null;
				} catch(ClosedChannelException e) {
					log.warn("Channel is closed while accessing" + getClientInfo(socketChannel));
					iter.remove();
					final Attachment attachment = (Attachment)key.attachment();
					if(attachment != null) {
						attachment.close(true); //XXX:Is this client initiated?
					}
				}
			}
			if(log.isDebugEnabled() && connections.get() < getMaxConnections()) {
				log.debug("Did not find any requests; Selecting again.");
			}
		}
	}

	/**
	 * @param socket
	 * @return
	 */
	protected boolean allowConnection(Socket socket) {
		return true;
	}

	/** A hook for sub-classes
	 * @param socketChannel
	 * @param interpretter
	 */
	protected void onSocketAccepted(SocketChannel socketChannel, R interpretter) {

	}

	/** Sub-classes will implement this to create a stateful interpretter. The
	 * interpretter is used only by one thread at a time in strict order.
	 * @return
	 */
	protected abstract R createInterpretter(Socket socket, ByteChannel byteChannel) throws IOException ;

	public int getMaxConnections() {
		return maxConnections;
	}

	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

	public InetSocketAddress getSocketAddress() {
		return socketAddress;
	}

	public void setSocketAddress(InetSocketAddress socketAddress) {
		/*if(serverChannel != null)
			throw new IOException("The socketAddress cannot be changed on a running NIOServerSocketHandler. Call shutdown() first.");
		*/
		this.socketAddress = socketAddress;
		initializer.reset();
	}

	public String getConnectionInfo() {
		StringBuilder sb = new StringBuilder();
		for(Attachment a : attachments.values()) {
			sb.append(a.getSocket().getRemoteSocketAddress()).append('=').append(a.getInterpretter()).append("\r\n");
		}
		return sb.toString();
	}
	/**
	 * @throws IOException
	 */
	public void setPort(int port) throws IOException {
		if(socketAddress == null)
			setSocketAddress(new InetSocketAddress(port));
		else
			setSocketAddress(new InetSocketAddress(socketAddress.getAddress(), port));
	}

	/**
	 * @param hostname
	 * @throws IOException
	 */
	public void setHostname(String hostname) throws IOException {
		if(socketAddress == null)
			setSocketAddress(new InetSocketAddress(hostname, 0));
		else
			setSocketAddress(new InetSocketAddress(hostname, socketAddress.getPort()));

	}

	public void setSocketAddress(String hostAndPort) throws IOException {
		if(hostAndPort == null || (hostAndPort=hostAndPort.trim()).length() == 0)
			setSocketAddress((InetSocketAddress)null);
		else {
			int pos = hostAndPort.lastIndexOf(':');
			if(pos < 0)
				setHostname(hostAndPort);
			else {
				setSocketAddress(new InetSocketAddress(hostAndPort.substring(0, pos), Integer.parseInt(hostAndPort.substring(pos+1))));
			}
		}
	}

	public void initialize() throws InterruptedException, IOException {
		initializer.initialize();
	}

	public boolean isJmxEnabled() {
		return jmxEnabled;
	}

	public void setJmxEnabled(boolean jmxEnabled) {
		this.jmxEnabled = jmxEnabled;
	}

	public boolean isJmxDetailEnabled() {
		return isJmxEnabled() && jmxDetailEnabled;
	}

	public void setJmxDetailEnabled(boolean jmxDetailEnabled) {
		this.jmxDetailEnabled = jmxDetailEnabled;
		if(jmxDetailEnabled && !isJmxEnabled())
			setJmxEnabled(jmxDetailEnabled);
			
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}
}
