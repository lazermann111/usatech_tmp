package simple.mq.peer;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.lmax.disruptor.AlertException;
import com.lmax.disruptor.ClaimStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.Sequence;
import com.lmax.disruptor.SequenceBarrier;
import com.lmax.disruptor.WaitStrategy;

import simple.lang.Holder;
import simple.util.concurrent.QueueBuffer;

public class RingQueueBuffer<E> implements QueueBuffer<E> {
	protected final EventFactory<Holder<E>> EVENT_FACTORY = new EventFactory<Holder<E>>() {
		@Override
		public Holder<E> newInstance() {
			return new Holder<E>();
		}
	};
	protected final RingBuffer<Holder<E>> ringBuffer;
	protected final AtomicInteger count = new AtomicInteger();
	protected final Sequence sequence = new Sequence();
	protected final SequenceBarrier sequenceBarrier;
	protected final AtomicLong index = new AtomicLong();

	protected static int ceilE2(int i) {
		i--;
		i |= i >> 1;
		i |= i >> 2;
		i |= i >> 4;
		i |= i >> 8;
		i |= i >> 16;
		return ++i;
	}
	public RingQueueBuffer(int capacity) {
		int bufferSize = ceilE2(capacity);
		ringBuffer = new RingBuffer<Holder<E>>(EVENT_FACTORY, bufferSize, ClaimStrategy.Option.MULTI_THREADED, WaitStrategy.Option.SLEEPING);
		sequence.set(ringBuffer.getCursor());
		index.set(ringBuffer.getCursor()); // This must be set or it misses the second item
		ringBuffer.setGatingSequences(sequence); // XXX: Not sure what to do with this
		sequenceBarrier = ringBuffer.newBarrier();
	}
	@Override
	public int getCapacity() {
		return ringBuffer.getBufferSize();
	}

	@Override
	public int size() {
		return count.get();
	}

	@Override
	public E take() throws InterruptedException {
		sequenceBarrier.clearAlert();
		long nextIndex = index.incrementAndGet();
		sequence.set(nextIndex - 1L);// Does not cycle buffer without this. Not sure why.
		try {
			sequenceBarrier.waitFor(nextIndex);
		} catch(AlertException e) {
			InterruptedException ie = new InterruptedException(e.getMessage());
			ie.initCause(e);
			throw ie;
		}
		count.decrementAndGet();
		return ringBuffer.get(nextIndex).getValue();
	}

	@Override
	public void put(E message, boolean awaitNotFull) throws InterruptedException {
		long nextIndex = ringBuffer.next();
		ringBuffer.get(nextIndex).setValue(message);
		count.incrementAndGet();
		ringBuffer.publish(nextIndex);
	}

	@Override
	public void put(Iterator<E> messages, boolean awaitNotFull) throws InterruptedException {
		while(messages.hasNext())
			put(messages.next(), awaitNotFull);
	}
	@Override
	public void awaitNotFull() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

}
