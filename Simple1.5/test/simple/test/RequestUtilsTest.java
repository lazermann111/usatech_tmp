package simple.test;

import java.io.File;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.ConfigSource;
import simple.io.Log;
import simple.servlet.RequestUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

public class RequestUtilsTest extends UnitTest {
    protected static final Pattern uriAddPattern = Pattern.compile("([^?#]+)(?:(\\?)([^#]*))?(?:(\\#.*))?"); // groups: 1=path; 2=? or empty; 3=query string 4=# or empty

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testAcceptHeader() throws Exception {
		// String accept = "text/*;q=0.3, text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5";
		String accept = "text/html, application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
		String[][] variety = new String[][] {
			new String[] {"text/*;q=0.3", "text/html;q=0.7", "text/html;level=1", "text/html;level=2;q=0.4", "*/*;q=0.5"},
			new String[] {"text/html", "application/xhtml+xml","application/xml;q=0.9","*/*;q=0.8"},
			null,
			new String[] {"text/plain"},
			new String[] {"text/plain", "text/html", "application/xhtml+xml","application/xml;q=0.9","*/*;q=0.8"},
			new String[] {"text/html;level=2"},
			
		};
		for(String[] v : variety) {
			String[] result = RequestUtils.getPreferredContentType(accept, v);
			log.info("\nPotentials=" + Arrays.toString(v) + "\nResult=" + Arrays.toString(result));
		}
	}

	@Test
	public void testAddLastMod() throws Exception {
		String[] fileNames = new String[] {
				"E:/TEMP/activation1.csv",
				"E:/TEMP/CCE_Fill_Matches_With_Looser_Dates2.xls",
				"E:/TEMP/Insert Historical Basis Fill Records.sql",
				"E:/TEMP/CVS Access Request Form - Phil Aikey.doc",
				"E:/TEMP/Esuds Reports Tracing.txt",
				"E:/TEMP/calendar_test.html",
				"E:/TEMP/event.sql"};

		LockSegmentCache<String, File, RuntimeException> uriFileCache = new LockSegmentCache<String, File, RuntimeException>() {
			@Override
			protected File createValue(String key, Object... additionalInfo) {
				return new File(key);
			}
			@Override
			protected boolean keyEquals(String key1, String key2) {
				return key1.equals(key2);
			}
			@Override
			protected boolean valueEquals(File value1, File value2) {
				return value1.equals(value2);
			}
	    };
	    LockSegmentCache<String, ConfigSource, RuntimeException> uriCSCache = new LockSegmentCache<String, ConfigSource, RuntimeException>() {
			@Override
			protected ConfigSource createValue(String key, Object... additionalInfo) {
				return ConfigSource.createConfigSource(new File(key));
			}
			@Override
			protected boolean keyEquals(String key1, String key2) {
				return key1.equals(key2);
			}
			@Override
			protected boolean valueEquals(ConfigSource value1, ConfigSource value2) {
				return value1.equals(value2);
			}
	    };
	    for(int k = 0; k < 1; k++) {
		long start = System.currentTimeMillis();
	    for(int i = 0; i < 10; i++) {
	    	int index = (int)(Math.random() * fileNames.length);
	    	log.debug("File Name is '" + addLastModifiedToFile(fileNames[index], fileNames[index], uriCSCache) + "'");
	    }
	    log.info("ConfigSource version took " + (System.currentTimeMillis()- start) + " milliseconds");
		/*start = System.currentTimeMillis();
	    for(int i = 0; i < 10; i++) {
	    	int index = (int)(Math.random() * fileNames.length);
	    	RequestUtils.addLastModifiedToFile(fileNames[index], fileNames[index], uriFileCache);
	    }
	    log.info("File version took " + (System.currentTimeMillis()- start) + " milliseconds");
	    */
	    }
		log.info("Done ---------------------------");
	}
    protected static String addLastModifiedToFile(String originalPath, String filePath, Cache<String, ConfigSource, RuntimeException> csCache) {
    	ConfigSource localFile;
    	if(csCache == null)
    		localFile = ConfigSource.createConfigSource(new File(filePath));
    	else
			localFile = csCache.getOrCreate(filePath);
		long lastModified = localFile.lastModified();
		String paramName = "t";
		Matcher matcher = uriAddPattern.matcher(originalPath);
		StringBuilder sb = new StringBuilder();
		if(matcher.matches()) {
			sb.setLength(0);
			sb.append(matcher.group(1));
			if(matcher.group(2) == null) {
				sb.append('?').append(paramName).append("=").append(lastModified);
			} else {
				if(matcher.group(3) != null && matcher.group(3).length() > 0)
					sb.append('?').append(matcher.group(3)).append('&');
				sb.append(paramName).append("=").append(lastModified);
			}
			if(matcher.group(4) != null)
				sb.append(matcher.group(4));
			return sb.toString();
		} else
			return originalPath;
    }
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
