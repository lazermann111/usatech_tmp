package simple.test;

import org.junit.Before;

/*
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.text.FormattedFile;
import simple.text.FormattedFile.DataType;
import simple.text.FormattedFile.Row;
import simple.text.FormattedFile.RowFormat;
import simple.text.FormattedFile.RowType;
import simple.text.FormattedFile.ValidationException;
*/
public class DelimitedFileTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}
/*
	@Test
	public void testValidationOk() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.NOT_REQUIRED);
		
		file.validate();
	}
	
	@Test(expected=ValidationException.class)
	public void testValidationFailureMissingColumn() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 5, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.NOT_REQUIRED);
		
		file.validate();
	}

	@Test(expected=IllegalArgumentException.class)
	public void testValidationFailureDuplicateColumn() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.NOT_REQUIRED);

		file.validate();
	}

	@Test()
	public void testValidationOrder() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.NOT_REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);

		file.validate();
	}

	@Test(expected=ValidationException.class)
	public void testValidationFailureStartingColum() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 2, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.NOT_REQUIRED);
		file.defineField(RowType.DATA, 4, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);

		file.validate();
	}

	@Test()
	public void testAppendTo() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.NOT_REQUIRED);
		
		Row row1 = file.createDataRow();
		row1.setValue("RECORD_INDICATOR", "TEST ROW");
		row1.setValue("IDENTIFIER", "1");
		row1.setValue("NAME", "Foo Bar");
		row1.setValue("DBA", "DBA");
		file.addDataRow(row1);

		file.validate();

		StringBuilder sb = new StringBuilder();
		file.appendTo(sb);
		
		assertEquals("TEST ROW|1|Foo Bar|DBA\n", sb.toString());
	}

	@Test(expected=ValidationException.class)
	public void testRequired() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.NOT_REQUIRED);
		
		Row row1 = file.createDataRow();
		row1.setValue("RECORD_INDICATOR", "TEST ROW");
		row1.setValue("NAME", "Foo Bar");
		row1.setValue("DBA", "DBA");
		file.addDataRow(row1);

		file.validate();
	}
	
	@Test()
	public void testRequiredIfPopulatedOk() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.requiredIfPopulated("NAME"));
		
		Row row1 = file.createDataRow();
		row1.setValue("RECORD_INDICATOR", "TEST ROW");
		row1.setValue("IDENTIFIER", "1");
		row1.setValue("NAME", "Foo Bar");
		row1.setValue("DBA", "DBA");
		file.addDataRow(row1);
		
		file.validate();
	}
	
	@Test(expected=ValidationException.class)
	public void testRequiredIfPopulatedFailure() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.requiredIfPopulated("NAME"));
		
		Row row1 = file.createDataRow();
		row1.setValue("RECORD_INDICATOR", "TEST ROW");
		row1.setValue("IDENTIFIER", "1");
		row1.setValue("NAME", "Foo Bar");
		//row1.setValue("DBA", "DBA");
		file.addDataRow(row1);
		
		file.validate();
	}
	
	@Test()
	public void testRequiredIfPopulatedMissingOk() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.NOT_REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.requiredIfPopulated("NAME"));
		
		Row row1 = file.createDataRow();
		row1.setValue("RECORD_INDICATOR", "TEST ROW");
		row1.setValue("IDENTIFIER", "1");
		//row1.setValue("NAME", "Foo Bar");
		//row1.setValue("DBA", "DBA");
		file.addDataRow(row1);
		
		file.validate();
	}
	
	@Test(expected=ValidationException.class)
	public void testRequiredIfEqualsFailure() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.NOT_REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.requiredIfEquals("NAME", "Foo"));
		
		Row row1 = file.createDataRow();
		row1.setValue("RECORD_INDICATOR", "TEST ROW");
		row1.setValue("IDENTIFIER", "1");
		row1.setValue("NAME", "Foo");
		//row1.setValue("DBA", "DBA");
		file.addDataRow(row1);
		
		file.validate();
	}
	
	@Test()
	public void testRequiredIfEqualsOk() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.NOT_REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.requiredIfEquals("NAME", "Foo"));
		
		Row row1 = file.createDataRow();
		row1.setValue("RECORD_INDICATOR", "TEST ROW");
		row1.setValue("IDENTIFIER", "1");
		row1.setValue("NAME", "Foo");
		row1.setValue("DBA", "DBA");
		file.addDataRow(row1);
		
		file.validate();
	}
	
	@Test()
	public void testRequiredIfEqualsNotEquals() throws Exception {
		FormattedFile file = new FormattedFile(RowFormat.DELIMITED, "|", "\n");

		file.defineField(RowType.DATA, 1, 6, DataType.ALPHANUMERIC, "RECORD_INDICATOR", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 2, 20, DataType.ALPHANUMERIC, "IDENTIFIER", FormattedFile.REQUIRED);
		file.defineField(RowType.DATA, 3, 50, DataType.ALPHANUMERIC, "NAME", FormattedFile.NOT_REQUIRED);
		file.defineField(RowType.DATA, 4, 50, DataType.ALPHANUMERIC, "DBA", FormattedFile.requiredIfEquals("NAME", "Foo"));
		
		Row row1 = file.createDataRow();
		row1.setValue("RECORD_INDICATOR", "TEST ROW");
		row1.setValue("IDENTIFIER", "1");
		row1.setValue("NAME", "Bar");
		//row1.setValue("DBA", "DBA");
		file.addDataRow(row1);
		
		file.validate();
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
*/
}
