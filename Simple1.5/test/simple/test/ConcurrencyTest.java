package simple.test;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.lang.Holder;

public class ConcurrencyTest extends UnitTest {

	protected static interface Lock {
		public void lock();

		public void unlock();
	}

	protected static class VunerableCASLock implements Lock {
		protected final AtomicBoolean locked = new AtomicBoolean();

		@Override
		public void lock() {
			int attempt = 0;
			while(!locked.compareAndSet(false, true)) {
				if(attempt < 100)
					attempt++;
				else if(attempt < 200) {
					attempt++;
					Thread.yield();
				} else
					LockSupport.parkNanos(1L);
			}
		}

		@Override
		public void unlock() {
			if(!locked.compareAndSet(true, false))
				throw new IllegalMonitorStateException("Not Locked");
		}
	}

	protected static class ReentrantLockLock implements Lock {
		protected final ReentrantLock lock = new ReentrantLock();

		@Override
		public void lock() {
			lock.lock();
		}

		@Override
		public void unlock() {
			lock.unlock();
		}
	}

	protected static class NoLock implements Lock {
		@Override
		public void lock() {
		}

		@Override
		public void unlock() {
		}
	}
	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testLocks() throws Throwable {
		LockSupport.parkNanos(1000L * 1000L * 1000L * 5L);
		for(int i = 0; i < 20; i++) {
			testLock(new NoLock(), 2, 10000000);
			testLock(new VunerableCASLock(), 2, 10000000);
			testLock(new ReentrantLockLock(), 2, 10000000);
			LockSupport.parkNanos(1000L * 1000L * 1000L * 5L);
		}/*
			for(int i = 0; i < 2; i++) {
			testLock(new NoLock(), 10, 1000000);
			testLock(new VunerableCASLock(), 10, 1000000);
			testLock(new ReentrantLockLock(), 10, 1000000);
			LockSupport.parkNanos(1000L * 1000L * 5L);
			}*/
	}

	//@Test
	public void testNoLock() throws Throwable {
		final Holder<Integer> holder = new Holder<Integer>();
		holder.setValue(0);
		Runnable runnable = new Runnable() {
			public void run() {
				holder.setValue(holder.getValue() + 1);
			}
		};
		multiThreadTest(runnable, 10, 1000000, 0, "NoLock");
		log.debug("Value = " + holder.getValue());
	}
	//@Test
	public void testAtomicInteger() throws Throwable {
		final AtomicInteger value = new AtomicInteger();
		final Holder<Integer> holder = new Holder<Integer>();
		Runnable runnable = new Runnable() {
			public void run() {
				holder.setValue(value.incrementAndGet());
			}
		};
		multiThreadTest(runnable, 10, 1000000, 0, "AtomicInteger");
		log.debug("Value = " + holder.getValue());
	}

	protected void testLock(final Lock lock, int threadCount, int executionCount) throws Throwable {
		final Holder<Integer> holder = new Holder<Integer>();
		holder.setValue(0);
		Runnable runnable = new Runnable() {
			public void run() {
				lock.lock();
				try {
					holder.setValue(holder.getValue() + 1);
				} finally {
					lock.unlock();
				}
			}
		};
		multiThreadTest(runnable, threadCount, executionCount, 0, lock.getClass().getName());
		log.debug("Value = " + holder.getValue());
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
