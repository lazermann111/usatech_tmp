package simple.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import simple.io.Log;
import simple.io.logging.AbstractLog;
import simple.io.logging.PrefixedLog;
import simple.text.MessageFormat;

public class PrefixedLogTest
{
  public static final int FATAL = 1000; 
  public static final int ERROR = 900;
  public static final int WARN = 800;
  public static final int INFO = 700;
  public static final int DEBUG = 600;
  public static final int TRACE = 500;
  public static final int ALL = 0;
  
	static class LogEntry
	{
		int level;
		Object msg;
		Throwable exception;
		
		LogEntry(int level, Object msg, Throwable exception)
		{
			this.level = level;
			this.msg = msg;
			this.exception = exception;
		}
		
		public String toString()
		{
			return String.format("%s,%s,%s", level, msg, exception);
		}
	}
	
	static LogEntry lastLog;
	
	static int currentLevel = INFO;
	
	static Log log = new PrefixedLog(new AbstractLog()
	{
		@Override
		protected void logMessage(int level, Object msg, Throwable exception)
		{
			lastLog = new LogEntry(level, msg, exception);
		}
		
		@Override
		protected int getTraceLevel()
		{
			return TRACE;
		}
		
		@Override
		protected int getDebugLevel()
		{
			return DEBUG;
		}

		@Override
		protected int getInfoLevel()
		{
			return INFO;
		}
		
		@Override
		protected int getWarnLevel()
		{
			return WARN;
		}

		@Override
		protected int getErrorLevel()
		{
			return ERROR;
		}

		@Override
		protected int getFatalLevel()
		{
			return FATAL;
		}
		
		@Override
		protected int getEffectiveLevel()
		{
			return currentLevel;
		}
	}, "prefix: ");
	
	@Test
	public void testLog()
	{
		String msg = "test";
		
		log.info(msg);
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals("prefix: " + msg, lastLog.msg);
	}
	
	@Test
	public void testFormattedLog()
	{
		String msg = "test={0}";
		
		log.info(msg);
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals("prefix: " + msg, lastLog.msg);
	}
}
