package simple.test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.NoSuchElementException;

import org.apache.commons.httpclient.HttpException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.tools.GeoInfoLookup;
import simple.tools.GeoInfoLookup.GeoInfo;

public class GeoLookupTest extends UnitTest {
	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}

	@Test
	public void testCanadianLookup() throws NoSuchElementException, HttpException, SecurityException, IllegalArgumentException, IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		GeoInfoLookup gil = new GeoInfoLookup();
		// GeoInfo gi = gil.lookupGeoInfo("N1E3G4", "CA");
		GeoInfo gi = gil.lookupGeoInfo("L1W3R8", "CA");
		log.info("Result: " + gi);
	}

	@Test
	public void testUSLookup() throws NoSuchElementException, HttpException, SecurityException, IllegalArgumentException, IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		GeoInfoLookup gil = new GeoInfoLookup();
		// GeoInfo gi = gil.lookupGeoInfo("19355", "US");
		GeoInfo gi = gil.lookupGeoInfo("98391", "US");
		log.info("Result: " + gi);
	}
}
