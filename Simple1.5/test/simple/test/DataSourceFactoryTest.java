package simple.test;

import java.awt.Window;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DBUnwrap;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceFactory;
import simple.db.config.ConfigLoader;
import simple.io.GuiInteraction;
import simple.io.IOUtils;
import simple.io.Interaction;
import simple.io.Log;
import simple.results.Results;
import simple.swt.UIUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class DataSourceFactoryTest extends UnitTest {
	static {
		System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		System.setProperty("file.encoding", "ISO8859-1");
	}
	
	@Before
	public void setUp() throws Exception {
		setupLog();
		// setupBasicDSF("TEST", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@usadbd02.usatech.com:1531:usadbd02", "REPORT", "USATECH");
		System.setProperty("javax.xml.parsers.SAXParserFactory", "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");
	}

	@Test
	public void testStatementMonitor() throws IOException, ServiceException {
		setupDSF("test.properties");
		final String callId = "TIMEOUT";// "VALUES"; //
		final int iterations = 100;
		final long delay = 100L;
		int threadCount = 20;
		int repeats = 3;
		Runnable r = new Runnable() {
			@Override
			public void run() {
				for(int i = 0; i < iterations; i++) {
					Object[] ret;
					try {
						ret = DataLayerMgr.executeCall(callId, null, true);
						log.info("Results = " + CollectionUtils.deepToString(ret));
					} catch(SQLException | DataLayerException e) {
						log.info(e);
					}
					if(delay > 0)
						try {
							Thread.sleep(delay);
						} catch(InterruptedException e) {
							//
						}
				}
			}
		};
		for(int k = 0; k < repeats; k++) {
			final Thread[] threads = new Thread[threadCount];
			for(int i = 0; i < threadCount; i++)
				threads[i] = new Thread(r);
			for(int i = 0; i < threadCount; i++)
				threads[i].start();
			Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
				@Override
				public void run() {
					Thread thread = threads[(int) (Math.random() * threads.length)];
					log.info("Interrupting " + thread);
					thread.interrupt();
				}
			}, 2000L, 2000L, TimeUnit.MILLISECONDS);
			for(int i = 0; i < threadCount; i++)
				try {
					threads[i].join();
				} catch(InterruptedException e) {
					// ignore
				}
		}
	}

	@Test
	public void testFailover() throws Exception {
		setupDSF("test.properties");
		Map<String, Object> params = new HashMap<String, Object>();
		for(int i = 0; i < 20; i++) {
			log.debug("Iteration #" + (i + 1));
			Connection conn = DataLayerMgr.getConnection("PGS");
			try {
				Connection logConn = DBUnwrap.getRealConnection(conn);
				log.debug("Using connection " + logConn);
				DataLayerMgr.selectInto(conn, "VALUES", params);
				log.debug("Output: " + params);
				Thread.sleep(500);
			} finally {
				conn.close();
			}
		}

	}
	@Test
	public void testConfig() throws Exception {
		setupDSF("test.properties");
		Map<String, Object> params = new HashMap<String, Object>();
		DataLayerMgr.selectInto("DUMMY", params);
		log.debug("Output: " + params);
	}
	@Test
	public void testMaxAge() throws Throwable {
		setupDSF("test.properties");
		final DataSourceFactory dsf = DataLayerMgr.getDataSourceFactory();
		final Map<String, Object> params = new HashMap<String, Object>();
		multiThreadTest(new Runnable() {
			protected int iteration = 1;

			public void run() {
				try {
					log.info("Iteration #" + iteration++ + ": " + dsf.getDataSource("PGS").toString());
					Connection conn = DataLayerMgr.getConnection("PGS");
					try {
						Connection logConn = DBUnwrap.getRealConnection(conn);
						log.debug("Using connection " + logConn);
						DataLayerMgr.selectInto(conn, "VALUES", params);
						log.debug("Output: " + params);
						Thread.sleep((long) (Math.random() * 500));
					} finally {
						conn.close();
					}
				} catch(Exception e) {
					log.warn("Exception testing connection", e);
				}
			}
		}, 10, 100, 0, "Test PoolDataSource");

		/*
		for(int i = 0; i < 60; i++) {
			log.info("Iteration #" + (i + 1) + "a: " + dsf.getDataSource("TEST").toString());
			Connection conn =  DataLayerMgr.getConnection("TEST");
			try {
				Connection logConn = DBUnwrap.getRealConnection(conn);
				log.debug("Using connection " + logConn);
				DataLayerMgr.selectInto(conn, "DUMMY", params);
				log.debug("Output: " + params);
				Thread.sleep(500);
			} finally {
				conn.close();
			}
			log.info("Iteration #" + (i + 1) + "b: " + dsf.getDataSource("PGS").toString());
			conn = DataLayerMgr.getConnection("PGS");
			try {
				Connection logConn = DBUnwrap.getRealConnection(conn);
				log.debug("Using connection " + logConn);
				DataLayerMgr.selectInto(conn, "VALUES", params);
				log.debug("Output: " + params);
				Thread.sleep(500);
			} finally {
				conn.close();
			}
		}
		*/
	}

	@Test
	public void testBlob() throws Exception {
		setupDSF("test.properties");
		String callId = "GET_FILE_FOR_READ";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileId", "411");

		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("TEST");
		Connection realConn = DBUnwrap.getRealConnection(conn);
		Blob blob;
		try {
			log.debug("Executing call");
			Object[] ret = DataLayerMgr.executeCall(realConn, callId, params);
			log.debug("Results = " + CollectionUtils.deepToString(ret));
			blob = ConvertUtils.convert(Blob.class, params.get("fileContent"));
		} finally {
			log.debug("Closing connection");
			realConn.close();
			conn.close();
		}
		log.debug("Contents:\n" + IOUtils.readFully(blob.getBinaryStream()));
	}
	
	@Test
	public void testBlobWithAccentedChars() throws Exception {
		setupDSF("test.properties");
		String callId = "GET_FILE_TRANSFER_FOR_READ";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileId", "55483");

		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("TEST");
		Blob blob;
		try {
			log.debug("Executing call");
			Object[] ret = DataLayerMgr.executeCall(conn, callId, params);
			log.debug("Results = " + CollectionUtils.deepToString(ret));
			blob = ConvertUtils.convert(Blob.class, params.get("fileContent"));
			String content = new String(blob.getBytes(1L, (int) blob.length()), Charset.forName("ISO8859-1"));
			log.info("DEBUG: blob: " + content);
		} finally {
			log.debug("Closing connection");
			conn.close();
		}
		log.debug("Contents:\n" + IOUtils.readFully(blob.getBinaryStream()));
	}
	
	@Test
	public void testCall2() throws Exception {
		setupBasicDSF("TEST", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=DEVDBP02)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "REPORT", "USATECH");
		ConfigLoader.loadConfig("simple/test/db-test-data-layer.xml");		
		String callId = "QUERY";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("days", new Date[] {ConvertUtils.convert(Date.class, "01/01/2009"),ConvertUtils.convert(Date.class, "01/02/2009"),ConvertUtils.convert(Date.class, "01/03/2009")});
		params.put("terminalId", 12341324);
		params.put("transTypeIds", new int[] {16,17,18,19,20,21,22});
		params.put("currencyId", 1);
		params.put("startDate", ConvertUtils.convert(Date.class, "01/01/2009"));
		params.put("endDate", ConvertUtils.convert(Date.class, "01/04/2009"));
		params.put("userId", 7);

		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			log.debug("Executing call");
			Object[] ret = DataLayerMgr.executeCall(conn, callId, params);
			log.debug("Results = " + CollectionUtils.deepToString(ret));
		} finally {
			log.debug("Closing connection");
			conn.close();
		}
	}

	@Test
	public void testCall3() throws Exception {
		Interaction interaction = new GuiInteraction((Window) null);
		char[] pwd = interaction.readPassword("Enter password for OPER");
		setupBasicDSF(
				"OPER",
				"oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))",
				"BKRUG", new String(pwd));
		ConfigLoader.loadConfig("file:../AppLayer/src/applayer-data-layer.xml");		
		String callId = "UPDATE_SESSION_START";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sessionStartTime", 1391529223717L);
		params.put("netlayerName", "NetworkLayer");
		params.put("protocol", 7);
		params.put("deviceName", "TD122561");
		params.put("remoteAddress", "10.228.168.249");
		params.put("deviceSerialCd", "VJ100001188");
		params.put("deviceType", 13);
		params.put("action", "START");
		params.put("netlayerHost", "usanet33.trooper.usatech.com");
		params.put("remotePort", 17132);
		params.put("netlayerPort", 14108);
		params.put("globalSessionCode", "A:32507@usanet33.trooper.usatech.com:143E3A5E99D:3FC360FE1D8248BF");

		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("OPER");
		try {
			log.debug("Executing call");
			Object[] ret = DataLayerMgr.executeCall(conn, callId, params);
			conn.rollback();
			log.info("Results = " + CollectionUtils.deepToString(ret));
			log.info("Params = " + params);
		} finally {
			log.debug("Closing connection");
			conn.close();
		}
	}

	@Test
	public void testClobQuery() throws Exception {
		setupBasicDSF("TEST", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "REPORT", "USATECH");
		ConfigLoader.loadConfig("simple/test/db-test-data-layer.xml");
		String callId = "CLOB_QUERY";
		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			log.debug("Executing call");
			Object[] ret = DataLayerMgr.executeCall(conn, callId, null);
			log.debug("Results = " + CollectionUtils.deepToString(ret));
		} finally {
			log.debug("Closing connection");
			conn.close();
		}
	}

	@Test
	public void testCall() throws Exception {
		setupDSF("test.properties");
		String callId = "CREATE_PTA_FOR_TRAN";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceName", "TD000065");
		params.put("authTime", System.currentTimeMillis());
		params.put("paymentType", 'R');
		params.put("tranStartTime", System.currentTimeMillis());

		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			log.debug("Executing call");
			Object[] ret = DataLayerMgr.executeCall(conn, callId, params);
			log.debug("Results = " + CollectionUtils.deepToString(ret));
		} finally {
			log.debug("Closing connection");
			conn.close();
		}
	}
	@Test
	public void testQuery() throws Exception {
		setupDSF("test.properties");
		ConfigLoader.loadConfig("file:///E:/Java Projects/POSMLayer/conf/authority-data-layer.xml");
		String callId = "GET_AUTHORITY_ACTION_DETAILS";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tranId", 815379);
		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			log.debug("Executing call");
			Results results = DataLayerMgr.executeQuery(conn, callId, params);
			CharArrayWriter caw = new CharArrayWriter();
			StringUtils.writeCSV(results, new PrintWriter(caw), true);
			log.debug("Results:\n" + caw);
		} finally {
			log.debug("Closing connection");
			conn.close();
		}
	}
	@Test
	public void testSqlDirect() throws Exception {
		setupDSF("test.properties");
		String sql = "{call DBMS_SESSION.MODIFY_PACKAGE_STATE(DBMS_SESSION.REINITIALIZE)}";
		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			log.debug("Creating statement");
			Statement st = conn.createStatement();
			log.debug("Executing sql");
			boolean hasResults = st.execute(sql);
			log.debug("Checking results");
			if(hasResults) {
				ResultSet rs = st.getResultSet();
				if(rs.next())
					log.info("Connection OKAY");
				else
					log.info("Connection INVALID");
				rs.close();
			} else {
				int count = st.getUpdateCount();
				log.info("Update Count = " + count);
			}
		} finally {
			log.debug("Closing connection");
			conn.close();
		}
	}
	@Test
	public void testUTCConversion() throws Exception {
		setupDSF("test.properties");
		String sql = "SELECT SYS_EXTRACT_UTC(SYSTIMESTAMP), CURRENT_TIMESTAMP, CURRENT_TIMESTAMP AT TIME ZONE 'US/Pacific' FROM DUAL";
		log.debug("Getting connection");
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			log.debug("Executing sql");
			Results results = DataLayerMgr.executeSQL(conn, sql, null, null);
			log.debug("Checking results");
			if(results.next()) {
				log.info("Connection OKAY");
				Map<String, Object> values = new HashMap<String, Object>(results.getValuesMap());
				log.info("Object is '" + values + "'");
				Calendar cal = ConvertUtils.convert(Calendar.class, results.getValue(1));
				log.info("Calendar is '" + cal + "'");
				if(cal != null) {
					long updateTime = ConvertUtils.getLocalTime(cal.getTimeInMillis(), cal.getTimeZone());
					log.info("Time is '" + updateTime + "'");
					log.info("As Date is '" + new Date(updateTime) + "'");
				}
			} else
				log.info("Connection INVALID");
			results.close();
		} finally {
			log.debug("Closing connection");
			conn.close();
		}
	}

	@Test
	public void testConnectionProduct() throws Throwable {
		// setupBasicDSF("TEST", "oracle.jdbc.driver.OracleDriver",
		// "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=DEVDBP02)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "REPORT", "USATECH");
		String username = "BKRUG";
		String password = UIUtils.promptForPassword("Enter the OPER password for " + username, "Database Login", null);

		setupBasicDSF("TEST", "oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.61)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.62)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(INSTANCE_NAME=USAPDB2)(SERVER = DEDICATED)(SERVICE_NAME=usaopdb.world)))",
				username, password);
		password = null;
		Connection conn = DataLayerMgr.getConnection("TEST");
		DatabaseMetaData md = conn.getMetaData();
		log.info("Connection Product: " + conn.getMetaData().getDatabaseProductName());
		
	}
	@Test
	public void testConnection() throws Throwable {
		setupDSF("test.properties");
		multiThreadTest(new Runnable() {
			public void run() {
				try {
					testSqlDirect();
				} catch(Exception e) {
					log.warn("Exception testing connection", e);
				}
			}
		}, 1, 100, 0, "Test Connection");
	}

	@Test
	public void testPool() throws Throwable {
		setupDSF("test.properties");
		multiThreadTest(new Runnable() {
			protected final AtomicInteger iteration = new AtomicInteger();

			public void run() {
				try {
					long s = System.nanoTime();
					try (Connection conn = DataLayerMgr.getConnection("MQ")) {
						log.info("Warm-up #" + iteration.incrementAndGet() + ": Got connection " + conn);
					}
				} catch(Exception e) {
					log.warn("Warm-up #" + iteration.incrementAndGet() + ": Exception testing connection", e);
				}
			}
		}, 2, 10, 5, "Test Connection");
		Thread.sleep(2000);
		final Collection<Exception> exceptions = new ConcurrentLinkedQueue<Exception>();
		final AtomicLong poolTime = new AtomicLong();
		long start = System.nanoTime();
		multiThreadTest(new Runnable() {
			protected final AtomicInteger iteration = new AtomicInteger();
			public void run() {
				try {
					long s = System.nanoTime();
					try (Connection conn = DataLayerMgr.getConnection("MQ")) {
						poolTime.addAndGet(System.nanoTime() - s);
						log.info("Iteration #" + iteration.incrementAndGet() + ": Got connection " + conn);
					}
				} catch(Exception e) {
					exceptions.add(e);
					log.warn("Iteration #" + iteration.incrementAndGet() + ": Exception testing connection", e);
				}
			}
		}, 100, 10000, 5, "Test Connection");
		long time = System.nanoTime() - start;
		for(Exception e : exceptions)
			log.warn("Exception testing connection", e);
		log.info("Completed test in " + time + " ns with " + poolTime.get() + " ns of pool time and " + exceptions.size() + " exceptions");
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
