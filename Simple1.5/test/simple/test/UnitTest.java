package simple.test;

import java.awt.Button;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.CancellationException;

import javax.swing.JOptionPane;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.app.ServiceException;
import simple.db.BasicDataSourceFactory;
import simple.db.DataLayerMgr;
import simple.db.DataSourceFactory;
import simple.io.Log;
import simple.lang.Holder;

public abstract class UnitTest {
	protected static Log log;

	public static boolean dsfInitialized = false; 
	
	public static void main(String[] args) throws Throwable {
		setupLog();
		//new Foo().run();
		Log.finish();
	}
	public static void setupLog() {
        //System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
		System.setProperty("log4j.configuration", "log4j-test.properties");
        log = Log.getLog();
	}
	protected void setupDSF(String propertiesFileName) throws IOException, ServiceException {
        
		// Data source layer is a singleton. Only the first unit test should initialize it.
		// No threading concern here. 
		if(dsfInitialized != true)  
		{
			Configuration config = MainWithConfig.loadConfig(propertiesFileName, getClass(), null);
	        BaseWithConfig.configureDataSourceFactory(config, null);
	        BaseWithConfig.configureDataLayer(config);
			dsfInitialized = true; 
		}
	}
	protected String getPassword(String title, String prompt) throws CancellationException {
		class Status {
			public boolean cancelled = false;
			public String value;
		}
		final Status status = new Status();
		Frame hiddenFrame = JOptionPane.getRootFrame();
		final Dialog dialog = new Dialog(hiddenFrame, title, true);
		dialog.setLocation(100, 100);
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				status.cancelled = true;
				dialog.dispose();
			}
		});

		Panel buttonPanel = new Panel();
		Label label = new Label(prompt);
		final TextField textField = new TextField(16);
		textField.setEchoChar('*');
		ActionListener actionListener = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				status.value = textField.getText();
				dialog.dispose();
			}
		};
		textField.addActionListener(actionListener);
		Button okButton = new Button("Ok");
		okButton.addActionListener(actionListener);
		Button cancelButton = new Button("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				status.cancelled = true;
				dialog.dispose();
			}
		});
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		dialog.add(label, "North");
		dialog.add(textField, "Center");
		dialog.add(buttonPanel, "South");
		dialog.pack();
		dialog.setVisible(true);
		hiddenFrame.dispose();
		if(status.cancelled)
			throw new CancellationException("Cancelled by user.");
		return status.value;
	}

	protected void multiThreadTest(final Runnable runnable, int threadCount, final int executionCount, final long sleepBetweenExecutions, final String taskName) throws Throwable  {
    	Thread[] threads = new Thread[threadCount];
        final Thread mainThread = Thread.currentThread();
        final Holder<Throwable> runningException = new Holder<Throwable>();
    	for(int i = 0; i < threads.length; i++)
        	threads[i] = new Thread("Runner-" + (i+1)) {
				@Override
				public void run() {
					long start = System.currentTimeMillis();
					for(int i = 0; i < executionCount; i++) {
						try {
							runnable.run();
						} catch(Throwable e) {
							log.warn("Exception while running '" + taskName + "'", e);
							runningException.setValue(e);
							mainThread.interrupt();
						}
						if(sleepBetweenExecutions > 0)
							try {
								Thread.sleep(sleepBetweenExecutions);
							} catch(InterruptedException e) {
								log.warn("Sleeping interrupted", e);
							}
					}
					long duration = System.currentTimeMillis() - start;
					log.debug("Execution of '" + taskName + "' took " + duration + " milliseconds");
				}
        };
        for(int i = 0; i < threads.length; i++)
        	threads[i].start();
        for(int i = 0; i < threads.length; i++)
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				log.warn(e);
				if(runningException.getValue() != null)
					throw runningException.getValue();
			}
    }

	protected static void addDS(Properties props, String name, String driver, String url, String username, String password) {
        props.put(name + "." + BasicDataSourceFactory.PROP_DATABASE, url);
        props.put(name + "." + BasicDataSourceFactory.PROP_DRIVER, driver);
        props.put(name + "." + BasicDataSourceFactory.PROP_USERNAME, username);
        props.put(name + "." + BasicDataSourceFactory.PROP_PASSWORD, password);
    }

	protected static void setupBasicDSF(String name, String driver, String url, String username, String password) {
		DataSourceFactory dsf = DataLayerMgr.getGlobalDataLayer().getDataSourceFactory();
		BasicDataSourceFactory bdsf;
		if(dsf instanceof BasicDataSourceFactory)
			bdsf = (BasicDataSourceFactory) dsf;
		else {
			bdsf = new BasicDataSourceFactory();
			DataLayerMgr.getGlobalDataLayer().setDataSourceFactory(bdsf);
		}
		bdsf.addDataSource(name, driver, url, username, password);
	}
}
