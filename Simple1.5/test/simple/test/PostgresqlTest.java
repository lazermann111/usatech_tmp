package simple.test;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import simple.db.Argument;
import simple.db.Call;
import simple.db.Cursor;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;

public class PostgresqlTest {
	private static final Log log = Log.getLog();

	public static class SSLLoggingHostnameVerifier implements HostnameVerifier {
		@Override
		public boolean verify(String host, SSLSession sslSession) {
			log.info("Connected to '" + host + "' with " + sslSession.getProtocol() + " using " + sslSession.getCipherSuite());
			return true;
		}
	}
	public static void main(String args[]) throws Exception {
		// System.setProperty("jdk.tls.client.protocols", "TLSv1.1,TLSv1.2");
		// System.setProperty("jdk.tls.client.protocols", "SSLv3,TLSv1");
		// System.setProperty("jdk.tls.server.protocols", "SSLv3,TLSv1");
		// SSLContext.getDefault().getDefaultSSLParameters().setProtocols(new String[] { "TLSv1" });
		SSLContext ctxt = SSLContext.getDefault();
		// log.info("Enabled Protocols: " + Arrays.toString(ctxt.createSSLEngine().getEnabledProtocols()));
		Class.forName("org.postgresql.Driver");
		// String url =
		// "jdbc:postgresql://devmst05:5432/bkkm1?ssl=require&connectTimeout=5&socketTimeout=20&tcpKeepAlive=true&sslhostnameverifier=simple.test.PostgresqlTest.SSLLoggingHostnameVerifier";
		String url = "jdbc:postgresql://devtool11:5432/app_2?ssl=require&connectTimeout=5&socketTimeout=20&tcpKeepAlive=true&sslhostnameverifier=simple.test.PostgresqlTest$SSLLoggingHostnameVerifier";
		Call call = new Call();
		Cursor cursor = new Cursor();
		call.setSql("SELECT * FROM APP_CLUSTER.DEVICE_INFO LIMIT 10");
		cursor.setAllColumns(true);
		call.setArguments(new Argument[] { cursor });
		
		/*
		call.setSql("SELECT ENCRYPTED_DATA FROM KM.ACCOUNT WHERE GLOBAL_ACCOUNT_ID = ?");
		Column column1 = new Column();
		column1.setIndex(1);
		column1.setPropertyName("encryptedData");
		cursor.setColumns(new Column[] { column1 });
		Parameter param1 = new Parameter();
		param1.setIn(true);
		param1.setPropertyName("globalAccountId");
		param1.setRequired(true);
		param1.setSqlType(new SQLType(Types.BIGINT));
		call.setArguments(new Argument[] { cursor, param1 });
		*/
		Connection conn = DriverManager.getConnection(url,"admin_1","ADMIN_1");
		// Protocol
		try {
			long globalAccountId = 1001674850L;
			Results results = call.executeQuery(conn, new Object[] { globalAccountId }, null);
			StringUtils.writeCSV(results, System.out, true);
			// conn.commit();
		} finally {
			conn.close();
		}
	}

}