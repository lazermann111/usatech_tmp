package simple.test;

import java.lang.management.ManagementFactory;
import java.text.Format;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.lang.Holder;
import simple.text.NumberAsWordsFormat;
import simple.util.CollectionUtils;
import simple.util.concurrent.BoundedCache;
import simple.util.concurrent.Cache;
import simple.util.concurrent.ConcurrentMapCache;
import simple.util.concurrent.DefaultLockSegmentFutureCache;
import simple.util.concurrent.Factory;
import simple.util.concurrent.LockSegmentCache;
import simple.util.concurrent.RunOnGetFuture;

public class CacheTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testCaches() throws Throwable {
		testCaches(1000, 1000 * 1000, 10, 0);
		LockSupport.parkNanos(1000L * 1000L * 1000L * 5);
		testCaches(1000, 1000 * 1000, 10, 0);
		testCaches(1000, 1000 * 1000, 10, 50);
	}
	@Test
	public void testLRUIssueCache() throws Throwable {
		DefaultLockSegmentFutureCache<Object,Object> cache = new DefaultLockSegmentFutureCache<Object,Object>(15, 16, 0.75f, 16);
		cache.setValueFactory(new Factory<Object, Object, Exception>() {
			public Object create(final Object argument, Object... additionalInfo) throws InterruptedException {
				return argument;
			};
		});
		testLRUCache(cache);
	}

	@Test
	public void testCacheRemoval() throws Throwable {
		final Cache<Object, Object, ?> cache = createSegmentLockFutureCache(50, true);
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					testRemoveFromCache(cache, 500);
				} catch(Throwable e) {
					log.error(e);
				}
			}
		};
		testCache(cache, "SegmentLockFutureCache", 40, 1, runnable);
	}
	protected void testCaches(int distinctKeys, int iterations, int threads, int creationDelay) throws Throwable {
		String[] names = {"LockSegmentCache", "SegmentLockFutureCache", "ConcurrentMapCache", "LambdaCache"};
		Cache<Object, Object, ?>[] caches = (Cache<Object, Object, ?>[]) new Cache<?, ?, ?>[] {
				createLockSegmentCache(creationDelay),
				createSegmentLockFutureCache(creationDelay, false),
				createCMCache(creationDelay),
				//createLambdaCache(creationDelay),				
		};
		for(int i = 0; i < caches.length; i++) {
			testCacheIntegerKeys(caches[i], names[i] + " - Integer - " + creationDelay + " ms delay", distinctKeys, threads, iterations);
		}
		for(int i = 0; i < caches.length; i++) {
			testCacheStringKeys(caches[i], names[i] + " - String - " + creationDelay + " ms delay", distinctKeys, threads, iterations);
		}
		/*
		Cache<Object,Object,RuntimeException> cache1 = createLockSegmentCache(creationDelay);
		Cache<Object, Object, ExecutionException> cache2 = createSegmentLockFutureCache(creationDelay, false);

		testCacheIntegerKeys(cache1, "LockSegmentCache - Integer - " + creationDelay + " ms delay", distinctKeys, threads, iterations);
		testCacheIntegerKeys(cache2, "SegmentLockFutureCache - Integer - " + creationDelay + " ms delay", distinctKeys, threads, iterations);
		cache1.clear();
		cache2.clear();
		testCacheStringKeys(cache1, "LockSegmentCache - String - " + creationDelay + " ms delay", distinctKeys, threads, iterations);
		testCacheStringKeys(cache2, "SegmentLockFutureCache - String - " + creationDelay + " ms delay", distinctKeys, threads, iterations);
		 */
		//testCache(new SegmentLockFutureCache<Object,Object>(1000, 0.75f, 16), "SegmentLock1");
		//testCache(new SingleLockCache<Object,Object>(1000, 0.75f), "SingleLock");
		//testCache(new SegmentLockFutureCache<Object,Object>(1000, 0.75f, 64), "SegmentLock2-HighConcurr");
		//testCache(new SegmentLockFutureCache<Object,Object>(1000, 0.75f, 8), "SegmentLock2-8Concurr");
		//testCache(new SegmentLockFutureCache<Object,Object>(1000, 0.75f, 1), "SegmentLock2-LowConcurr");
	}

	protected Cache<Object, Object, ExecutionException> createSegmentLockFutureCache(final int creationDelay, final boolean old) {
		DefaultLockSegmentFutureCache<Object, Object> cache = new DefaultLockSegmentFutureCache<Object, Object>(Integer.MAX_VALUE, 100, 0.75f, 16) {
			public Object remove(Object key) {
				if(!old)
					return super.remove(key);
				int hash = hash(key);
				CacheFuture future = segmentFor(hash).remove(key, getEntryHash(hash), null);
				if(future != null)
					future.cancel(true);
				Object old;
				if(future == null || !future.isDone())
					old = null;
				else {
					try {
						old = future.get();
					} catch(InterruptedException e) {
						old = null;
					} catch(ExecutionException e) {
						old = null;
					}
					future.reset();
				}
				return old;
			}
		};
		cache.setValueFactory(new Factory<Object, Object, Exception>() {
			public Object create(final Object argument, Object... additionalInfo) throws InterruptedException {
				if(creationDelay > 0)
					Thread.sleep(creationDelay);
				return argument;
			};
		});
		return cache;
	}
	protected Cache<Object,Object,RuntimeException> createLockSegmentCache(final int creationDelay) {
		LockSegmentCache<Object,Object,RuntimeException> cache = new LockSegmentCache<Object,Object,RuntimeException>(100, 0.75f, 16) {
			@Override
			protected Object createValue(Object key, Object... additionalInfo) {
				if(creationDelay > 0)
					try {
						Thread.sleep(creationDelay);
					} catch(InterruptedException e) {
					}
				return key;
			}

			@Override
			protected boolean keyEquals(Object key1, Object key2) {
				return CollectionUtils.deepEquals(key1, key2);
			}

			@Override
			protected boolean valueEquals(Object value1, Object value2) {
				return CollectionUtils.deepEquals(value1, value2);
			}
		};
		return cache;
	}

	protected Cache<Object, Object, RuntimeException> createCMCache(final int creationDelay) {
		return new ConcurrentMapCache<Object, Object, RuntimeException>() {
			@Override
			protected Object createValue(Object key, Object... additionalInfo) throws RuntimeException {
				if(creationDelay > 0)
					try {
						Thread.sleep(creationDelay);
					} catch(InterruptedException e) {
					}
				return key;
			}
		};
	}

	/*protected Cache<Object, Object, RuntimeException> createLambdaCache(final int creationDelay) {
		return new LambdaCache<Object, Object, RuntimeException>() {
			@Override
			protected Object createValue(Object key, Object... additionalInfo) throws RuntimeException {
				if(creationDelay > 0)
					try {
						Thread.sleep(creationDelay);
					} catch(InterruptedException e) {
					}
				return key;
			}
		};
	}*/
	protected void testLRUCache(BoundedCache<Object,Object,?> cache) throws Throwable {
		int n = 300;
		int k = 0;
		for(int i = 0; i < n; i++) {
			if(cache.size() >= cache.getMaxSize())
				log.debug("Remove LRU");
			cache.getOrCreate(i);
			switch(i % 10) {
				case 4: cache.remove(k); log.debug("Remove " + k); k++; break;
				case 8: cache.remove(i); log.debug("Remove " + i); break;
				case 9: log.debug("Cache=" + new TreeSet<Object>(cache.keySet())); break;
			}
		}
	}

	protected void testRemoveFromCache(Cache<Object, Object, ?> cache, int n) throws Throwable {
		for(int i = 0; i < n; i++) {
			int v = (int) (Math.random() * 10);
			switch(i % 10) {
				case 4:
				case 5:
					cache.remove(v);
					log.debug("Remove " + v);
					break;
				case 9:
					log.debug("Cache=" + new TreeSet<Object>(cache.keySet()));
					break;
				default:
					log.debug("Add " + v);
					cache.getOrCreate(v);
			}
		}
	}
	protected void testCache(Cache<Object,Object,?> cache, String cacheType, int threads, int iterations, Runnable runnable) throws Throwable {
		long heapInitial = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
		multiThreadTest(runnable, threads, iterations, 0, cacheType + " - " + threads + " threads - " + iterations + " times");
		log.debug("Size = " + cache.size());
		Log.finish();
        long heapBefore = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
        System.gc();
        long heapAfter = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
        log.debug("Initial Heap: " + (heapInitial / 1024) + " KB; Before GC: " + (heapBefore/1024) + " KB; After GC: " + (heapAfter/1024) + " KB");
        Log.finish();
	}
	protected void testCacheIntegerKeys(final Cache<Object,Object,?> cache, String cacheType, final int distinctKeys, int threads, int iterations) throws Throwable {
		Runnable runnable = new Runnable() {
			public void run() {
				int key = (int)(Math.random() * distinctKeys);
				Object ret;
				try {
					ret = cache.getOrCreate(key);
				} catch(Exception e) {
					log.warn(e);
					return;
				}
				if(((Integer)ret).intValue() != key)
					throw new RuntimeException("Returned value is wrong!");
			}
		};
		testCache(cache, cacheType, threads, iterations, runnable);
	}
	protected void testCacheStringKeys(final Cache<Object,Object,?> cache, String cacheType, final int distinctKeys, int threads, int iterations) throws Throwable {
		Runnable runnable = new Runnable() {
			public void run() {
				char[] ch = new char[(int)Math.round(Math.log10(distinctKeys))];
				for(int i = 0; i < ch.length; i++)
					ch[i] = (char)((Math.random() * 10) + 'A');
				String key = new String(ch);
				try {
					cache.getOrCreate(key);
				} catch(Exception e) {
					log.warn(e);
				}
			}
		};
		testCache(cache, cacheType, threads, iterations, runnable);
	}
	protected void testCacheArrayKeys(final Cache<Object,Object,?> cache, String cacheType, final int distinctKeys, int threads, int iterations) throws Throwable {
		Runnable runnable = new Runnable() {
			public void run() {
				byte[] key = new byte[(int)Math.round(Math.log10(distinctKeys))];
				for(int i = 0; i < key.length; i++)
					key[i] = (byte)((Math.random() * 10));
				try {
					cache.getOrCreate(key);
				} catch(Exception e) {
					log.warn(e);
				}
			}
		};
		testCache(cache, cacheType, threads, iterations, runnable);
	}
/*
	@Test
	public void testExpiringLRUMapInt() throws Exception {
		final ExpiringLRUMap<String, String> map = new ExpiringLRUMap<String, String>(100);
		map.setExpirationTime(1000);
		//map.setWatchdogType(WatchdogType.PER_INSTANCE);
		Runnable runnable = new Runnable() {
			protected char next = 'A';
			public void run() {
				map.put(String.valueOf(next++), String.valueOf((System.currentTimeMillis() % 60000) / 1000.0));
			}
		};
		ScheduledExecutorService monitor = Executors.newSingleThreadScheduledExecutor();
		monitor.scheduleAtFixedRate(new Runnable() {
			public void run() {
				log.debug(map);
			}
		}, 100, 100, TimeUnit.MILLISECONDS);
		multiThreadTest(runnable, 1, 40, 250);
		Thread.sleep(1000);
		assertTrue("Entries did not expire in time", map.isEmpty());
	}
*/
	//@Test
	public void testRunOnGetFuture() throws Throwable {
		Callable<Integer> callable = new Callable<Integer>() {
			public Integer call() throws Exception {
				int sleep = (int)(Math.random() * 1000);
				Thread.sleep(sleep);
				return sleep;
			}
        };
        final RunOnGetFuture<Integer> future = new RunOnGetFuture<Integer>(callable);
        Runnable runnable = new Runnable() {
			public void run() {
				try {
					log.debug("Got from future: " + future.get());
				} catch(InterruptedException e) {
					log.warn("Getting from future", e);
				} catch(ExecutionException e) {
					log.warn("Getting from future", e);
				}
				if(Math.random() > 0.5) {
					log.debug("Resetting the future");
					log.debug("Reset " + (future.reset()?"successful":"failed"));
				}
			}
        };
		multiThreadTest(runnable, 20, 40, 10, "RunOnGetFuture");
		//assertTrue("Entries did not expire in time", map.isEmpty());
	}
	//@Test
	public void testNoLock() throws Throwable {
		final Holder<Integer> holder = new Holder<Integer>();
		holder.setValue(0);
		Runnable runnable = new Runnable() {
			public void run() {
				holder.setValue(holder.getValue() + 1);
			}
		};
		multiThreadTest(runnable, 10, 1000000, 0, "NoLock");
		log.debug("Value = " + holder.getValue());
	}
	//@Test
	public void testAtomicInteger() throws Throwable {
		final AtomicInteger value = new AtomicInteger();
		final Holder<Integer> holder = new Holder<Integer>();
		Runnable runnable = new Runnable() {
			public void run() {
				holder.setValue(value.incrementAndGet());
			}
		};
		multiThreadTest(runnable, 10, 1000000, 0, "AtomicInteger");
		log.debug("Value = " + holder.getValue());
	}
	//@Test
	public void testLock() throws Throwable {
		final ReentrantLock lock = new ReentrantLock();
		final Holder<Integer> holder = new Holder<Integer>();
		holder.setValue(0);
		Runnable runnable = new Runnable() {
			public void run() {
				lock.lock();
				try {
					holder.setValue(holder.getValue() + 1);
				} finally {
					lock.unlock();
				}
			}
		};
		multiThreadTest(runnable, 10, 1000000, 0, "LockOnInteger");
		log.debug("Value = " + holder.getValue());
	}
	@Test
	public void testValuesIterator() throws Exception {
		Cache<Object,Object,RuntimeException> cache = createLockSegmentCache(0);
		Format nf = new NumberAsWordsFormat();
		log.debug("Cache Values at 0 = " + cache.values());
		for(int i = 1; i <= 10; i++) {
			cache.getOrCreate(nf.format(i));
			log.debug("Cache Values at " + i + " = " + cache.values());
		}
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
