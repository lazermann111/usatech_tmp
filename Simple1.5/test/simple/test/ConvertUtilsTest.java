package simple.test;

import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.bean.converters.InputStreamConverter;
import simple.bean.converters.ReaderConverter;
import simple.io.IOUtils;
import simple.io.Log;
import simple.util.IntegerRangeSet;
//import sun.misc.Launcher;

public class ConvertUtilsTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testSubFormats() throws Exception {
		//Class<?>[] bootstrapClasses = ReflectionUtils.getSubTypes(Format.class, Launcher.getBootstrapClassPath().getURLs(), Format.class.getClassLoader());
		Class<?>[] appClasses = ReflectionUtils.getSubTypes(Format.class);
		/*for(Class<?> cls : bootstrapClasses)
			if(Modifier.isPublic(cls.getModifiers()) && !Modifier.isAbstract(cls.getModifiers()) && !Modifier.isInterface(cls.getModifiers()))
				log.info("Found sub-format: " + cls.getName());*/
		for(Class<?> cls : appClasses)
			if(Modifier.isPublic(cls.getModifiers()) && !Modifier.isAbstract(cls.getModifiers()) && !Modifier.isInterface(cls.getModifiers()))
				log.info("Found sub-format: " + cls.getName());
	}

	@Test
	public void testIllegalFormat() throws Exception {
		String format = "simple.text.OrdinalFormat";
		for(int i = -2; i < 5; i++)
			log.info(ConvertUtils.formatObject(i, format));

		log.info(ConvertUtils.formatObject(6, "javax.mail.internet.MailDateFormat"));
	}

	@Test
	public void testFormat() throws Exception {
		// String format = "MESSAGE:{0,CHOICE,0#Next Isis Purchase is Free|1#1 More Tap for Free Purchase|1<{0} More Taps for Free Purchase}";
		String format = "NUMBER:###0'.'00";
		for(int i = -2; i < 5; i++)
			log.info(ConvertUtils.formatObject(new Object[] { i }, format));

	}

	@Test
	public void testFormat2() throws Exception {
		String format = "MESSAGE:{params.ShowAll,MATCH,1=All Devices;.*='{params.HealthClassification,MATCH,NO_COMM=Service Required - No Communication in {params.NoCommDaysMin} to {params.NoCommDaysMax} Days;NO_CASHLESS=Service Required - No Cashless in {params.NoCashlessDaysMin} to {params.NoCashlessDaysMax} Days;REASSIGN_NEEDED=Inactive Devices Needing Redeployment;DEACTIVATION_NEEDED=Inactive Devices Needing Deactivation;INACTIVE=Other Inactive Devices;INVENTORY=Inventory (Never Called In);ACTIVE=Active Devices;}'}";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("params.ShowAll", 0);
		params.put("params.HealthClassification", "ACTIVE");
		log.info(ConvertUtils.formatObject(params, format));

	}

	@Test
	public void testFindAltMethod() throws Exception {
		Method altMethod = ReflectionUtils.findMethod(org.eclipse.jetty.security.ConstraintSecurityHandler.class, "setConstraintMappings", ReflectionUtils.ARRAY_PARAMS);
		log.info("Found method " + altMethod);
	}

	@Test
	public void testFindAltMethod2() throws Exception {
		Class<?> type = org.eclipse.jetty.security.ConstraintSecurityHandler.class;
		ReflectionUtils.WrappedIndexedBeanProperty wibp = (ReflectionUtils.WrappedIndexedBeanProperty) ReflectionUtils.findBeanProperty(type, "constraintMappings");
		log.info("IndexedType: " + wibp.getIndexedType().getName());
	}

	@Test
	public void testIntegerRangeSetConversion() throws Exception {
		Object[] objects = new Object[] {
			"15-20|27-34|14-16|30-39|16|3|40",
			new int[] {1,3,5,6,7,8,10},
			new String[] {"1-3", "5-10", "11|12|14-19", "9-13"},
			new HashSet<Integer>(Arrays.asList(new Integer(1), new Integer(3), new Integer(4)))
		};
		for(Object o : objects) {
			IntegerRangeSet irs = ConvertUtils.convert(IntegerRangeSet.class, o);
			log.debug("Converted '" + o + "' to " + irs);
		}
	}

	@Test
	public void testDateConversion() throws Exception {
		String[] patterns = new String[] {
				((SimpleDateFormat)DateFormat.getDateInstance()).toPattern(),
				"MM-dd-yyyy HH:mm:ss.SSS",
				"MMM dd yy",
				"MM/dd/yy",
				"EEE MMM dd yyyy",
				"EEE MM-dd-yyyy",
				"yyyy-MM-dd HH:mm",
				"dd-MMM-yyyy GG hh:mm a Z",
				"EEEE, MMMM dd, yyyy hh:mm a z",
				"MMMd,yy HH-mm-ss.S",
				"MMddyyyy",//*/
				"MMM   \t yyyy",
				"MM-yyyy",
				"MMMM , yyyy",
				"M - yyyy",
				"HH:mm:ss.SSS",
				"hh-mm a",
				"MM-dd-yyyy HH-mm-ss",
				"MMddyyyy HHmmss",
				"EEE MMM dd HH:mm:ss zzz yyyy"//*/
		};
		for(String pat : patterns) {
			long off = 1000L*24L*60L*60L*1000L;
			Date d = new Date(System.currentTimeMillis()-off);
			String df = ConvertUtils.formatObject(d, new SimpleDateFormat(pat));
			Date res = ConvertUtils.convert(Date.class, df);
			log.debug("Formatted " + d + " using " + pat + " then parsed and got " + res);
		}

		String[] times = new String[] { "Feb 23, 2013 04:07:09.123 US/Mountain", "Fri Jun 05 00:00:00 EDT 2015" };
		for(String t : times) {
			Date res = ConvertUtils.convert(Date.class, t);
			log.debug("Parsed " + t + " and got " + res);
		}
	}
	
	@Test
	public void testLocalDateConversion() throws Exception {
		Date dateTime1 = new SimpleDateFormat("MM/dd/yyyy HH:mm").parse("03/31/2016 04:33");
		LocalDate localDate1 = ConvertUtils.convert(LocalDate.class, dateTime1);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate1));
		
		Date dateTime2 = new SimpleDateFormat("MM/dd/yyyy").parse("03/31/2016");
		LocalDate localDate2 = ConvertUtils.convert(LocalDate.class, dateTime2);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate2));
		
		Date dateTime3 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss ZZZ").parse("03/31/2016 04:33:15 EST");
		LocalDate localDate3 = ConvertUtils.convert(LocalDate.class, dateTime3);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate3));

		Date dateTime4 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss ZZZ").parse("03/31/2016 04:33:15 GMT");
		LocalDate localDate4 = ConvertUtils.convert(LocalDate.class, dateTime4);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate4));
		
		Date dateTime5 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss ZZZ").parse("03/31/2016 04:33:15 GMT");
		Calendar calendar5 = GregorianCalendar.getInstance();
		calendar5.setTime(dateTime5);
		
		LocalDate localDate5 = ConvertUtils.convert(LocalDate.class, calendar5);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate5));
		
		Date dateTime6 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss ZZZ").parse("03/31/2016 04:33:15 EST");
		Calendar calendar6 = GregorianCalendar.getInstance();
		calendar6.setTime(dateTime6);
		
		LocalDate localDate6 = ConvertUtils.convert(LocalDate.class, calendar6);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate6));

		Date dateTime7 = new SimpleDateFormat("MM/dd/yyyy").parse("03/31/2016");
		Calendar calendar7 = GregorianCalendar.getInstance();
		calendar7.setTime(dateTime7);
		
		LocalDate localDate7 = ConvertUtils.convert(LocalDate.class, calendar7);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate7));
		
		Date dateTime8 = new Date(1459457301707L);
		LocalDate localDate8 = ConvertUtils.convert(LocalDate.class, dateTime8);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate8));
		
		long dateTime9 = 1459457301707L;
		LocalDate localDate9 = ConvertUtils.convert(LocalDate.class, dateTime9);
		assertEquals("03/31/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy").format(localDate9));

	}

	@Test
	public void testLocalToUTCConversion() throws Exception {
		String[] tzIds = new String[] {"GMT", "America/New_York", "America/Chicago", "America/Denver", "America/Los_Angeles"};
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		for(String tz : tzIds) {
			long startTime = df.parse("11/06/2010 12:00").getTime();
			for(int i = 0; i < 24; i++) {
				long time = startTime + (i * 60 * 60 * 1000L);
				log.debug("LocalTime: " + df.format(new Date(time)) + " " + tz + " is UTCTime: " + df.format(new Date(convertToUTC(time, TimeZone.getTimeZone(tz)))) + " is UTCTime_ByCal: " + df.format(new Date(convertToUTC_Cal(time, TimeZone.getTimeZone(tz)))));	
			}
			startTime = df.parse("03/13/2010 12:00").getTime();
			for(int i = 0; i < 24; i++) {
				long time = startTime + (i * 60 * 60 * 1000L);
				log.debug("LocalTime: " + df.format(new Date(time)) + " " + tz + " is UTCTime: " + df.format(new Date(convertToUTC(time, TimeZone.getTimeZone(tz))))+ " is UTCTime_ByCal: " + df.format(new Date(convertToUTC_Cal(time, TimeZone.getTimeZone(tz)))));	
			}
		}
	}
	
	@Test
	public void findDaylightCutoverConversion() throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("Available timezones:");
		for(String tz : TimeZone.getAvailableIDs())
			sb.append("\n\t").append(tz);
		log.debug(sb);
		Date localTime = new Date(System.currentTimeMillis() - (6 * 30 * 24 * 60 * 60 * 1000L));
		String[] tzIds = new String[] {"America/New_York", "America/Chicago", "America/Denver", "America/Los_Angeles"};
		for(String tz : tzIds) {
			TimeZone timeZone = TimeZone.getTimeZone(tz);
			long cutover = findCutover(localTime.getTime(), timeZone);
			Calendar cal = Calendar.getInstance(timeZone);
			cal.setTimeInMillis(cutover);
			log.debug("Cutover=" + cutover + "; Which is=" + cal.getTime() + " " + tz);
		}
	}
	
	protected long findCutover(long localTime, TimeZone timeZone) {
		if(!timeZone.useDaylightTime())
			return 0;
		long interval = 30 * 24 * 60 * 60 * 1000L;
		long startTime = localTime;
		long endTime = localTime + interval;
		while(true) {
			int startOffset = timeZone.getOffset(startTime);
			int endOffset = timeZone.getOffset(endTime);
			if(startOffset != endOffset) {
				while(true) {
					if(endTime - startTime <= 60 * 1000)
						return startTime;
					long midTime = (startTime + endTime) / 2;
					int midOffset = timeZone.getOffset(midTime);
					if(startOffset != midOffset) {
						endTime = midTime;
					} else if(endOffset != midOffset)
						startTime = midTime;
					else
						throw new IllegalStateException("This should not happen");
				}
			} else {
				startTime = endTime;
				endTime = startTime + interval;
			}
		}
	}
	protected long convertToUTC(long localTime, TimeZone timeZone) {
		return ConvertUtils.getMillisUTC(localTime, timeZone);
		/*long offset = timeZone.inDaylightTime(new Date())
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(localTime);
    	cal.setTimeZone(timeZone);
    	cal.clear(Calendar.ZONE_OFFSET);
    	log.debug("TimeZone=" + timeZone + "; Cal=" + cal.getTime());
    	return cal.getTimeInMillis();
    	*/
	}
	
	protected long convertToUTC_Cal(long localTime, TimeZone timeZone) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(timeZone);
		cal.setTimeInMillis(localTime);
		return ConvertUtils.getMillisUTC(cal);
		/*long offset = timeZone.inDaylightTime(new Date())
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(localTime);
    	cal.setTimeZone(timeZone);
    	cal.clear(Calendar.ZONE_OFFSET);
    	log.debug("TimeZone=" + timeZone + "; Cal=" + cal.getTime());
    	return cal.getTimeInMillis();
    	*/
	}
	
	@Test
	public void testTimezoneUTCConversion() throws Exception {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		log.debug("Date = " + df.format(new Date()));

		long time = System.currentTimeMillis();
		//long offset = TimeZone.getTimeZone("UTC").getOffset(time);
		long offset = TimeZone.getDefault().getOffset(time);
		log.debug("Time = " + time + "; Offset = " + offset + "; Date = " + new Date(time - offset));
		log.debug("DateUTC = " + ConvertUtils.getDateUTC(time));

		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar cal2 = Calendar.getInstance();
		int[] fields = new int[] {Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND };
		for(int f : fields) {
			cal2.set(f, cal.get(f));
		}
		log.debug("Date = " + cal2.getTime() + "; Millis = " + cal2.getTimeInMillis());


		/*
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone();
		cal.setTimeInMillis(time);

		Date date = new Date()
		log.debug("Calendar = " + cal + "; Date = " + cal.getTime() + "; Millis = " + cal.getTimeInMillis());
		//cal.set(Calendar.DST_OFFSET, 0);
		//cal.set(Calendar.ZONE_OFFSET, 0);
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		log.debug("Calendar = " + cal + "; Date = " + cal.getTime() + "; Millis = " + cal.getTimeInMillis());
		long time = cal.getTimeInMillis();
		cal.clear();
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(time);
		log.debug("Calendar = " + cal + "; Date = " + cal.getTime() + "; Millis = " + cal.getTimeInMillis());
		cal.set(2008, 10, 17, 15, 5, 33);
		log.debug("Calendar = " + cal + "; Date = " + cal.getTime() + "; Millis = " + cal.getTimeInMillis());
*/
	}

	@Test
	public void testColorConversion() throws Exception {
		String[] colors = new String[] {
				"#334455", "#204060", "blue", "0", "#1"
		};
		for(String c : colors) {
			Color color = ConvertUtils.convert(Color.class, c);
			log.debug("Converted '" + c + "' to " + color + " and formated to '" + ConvertUtils.getStringSafely(color) + "'");
		}
	}

	@Test
	public void testFormatNumber() throws Exception {
		double d = 2394894.8930;
		String[] formats = new String[] {
				"#0", "#,##0.00", "#", "0000000000000000000000000.0000000"
		};
		for(String f : formats) {
			String s = ConvertUtils.formatObject(d, "NUMBER:" + f);
			log.debug("Converted '" + d + "' to " + s + " using '" + f + "'");
		}
	}

	@Test
	public void testByteArray() throws Exception {
		char[] ch1 = "This is a new String".toCharArray();
		Character[] ch2 = new Character[] {'T', 'h', 'i', 's'};
		byte[] b1 = ConvertUtils.convert(byte[].class, ch1);
		byte[] b2 = ConvertUtils.convert(byte[].class, ch2);
		log.debug("'" + new String(ch1) + "' = '" + new String(b1) + "'");
		log.debug("'This' = '" + new String(b2) + "'");
	}

	@Test
	public void testInputStream() throws Exception {
		ConvertUtils.register(new InputStreamConverter());
		Object[] tests = new Object[] {"A String", "A byte Array".getBytes(), 19238491238401L, new BigInteger("A Big Integer".getBytes()), new File("E:/TEMP/TEST01.txt")};
		for(Object t : tests) {
			InputStream in = ConvertUtils.convert(InputStream.class, t);
			log.debug("Converted '" + t + "' to '" + IOUtils.readFully(in) + "'");
		}
	}
	@Test
	public void testReader() throws Exception {
		ConvertUtils.register(new InputStreamConverter());
		ConvertUtils.register(new ReaderConverter());
		Object[] tests = new Object[] {"A String", "A byte Array".getBytes(), 19238491238401L, new BigInteger("A Big Integer".getBytes()), new File("E:/TEMP/TEST01.txt")};
		for(Object t : tests) {
			Reader in = ConvertUtils.convert(Reader.class, t);
			log.debug("Converted '" + t + "' to '" + IOUtils.readFully(in) + "'");
		}
	}

	@Test
	public void testConvert() throws ConvertException {
		Object result = ConvertUtils.convert(org.apache.commons.configuration.PropertiesConfiguration.class, "./classes/loader.properties");
		log.info("Got " + result);
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
