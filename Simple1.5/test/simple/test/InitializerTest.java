package simple.test;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.lang.Initializer;
import simple.util.concurrent.RunOnGetFuture2;
import simple.util.concurrent.WaitForUpdateFuture;

public class InitializerTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testInitializer() throws Throwable {
		final AtomicInteger next = new AtomicInteger();
		final AtomicInteger resetCount = new AtomicInteger();
		final AtomicInteger cancelCount = new AtomicInteger();
		final AtomicInteger inRunCount = new AtomicInteger();
		final AtomicInteger inResetCount = new AtomicInteger();
		final Initializer<RuntimeException> initializer = new Initializer<RuntimeException>() {
			// final AltInitializer<RuntimeException> initializer = new AltInitializer<RuntimeException>() {
			@Override
			protected void doInitialize() throws RuntimeException {
				String threadName = Thread.currentThread().getName();
				System.out.println("Initializing - State: " + getState() + " in thread " + threadName);
				int n = inRunCount.incrementAndGet();
				try {
					assert n == 1 : "Too many threads in doInitialization()! [" + n + "] - State: " + getState();
					int n2 = n + inResetCount.get();
					assert n2 == 1 : "Too many threads in doInitialization() and doReset()! [" + n2 + "] - State: " + getState();
					try {
						Thread.sleep(100);
					} catch(InterruptedException e) {
						// ignore
					}
					if(Math.random() < 0.1) {
						System.out.println("Throwing exception in doInitialize() for testing: Integer = " + next.incrementAndGet() + " in thread " + threadName);
						throw new RuntimeException("An expected error occurred");
					}
					System.out.println("Initialized Integer = " + next.incrementAndGet() + " in thread " + threadName);
				} finally {
					inRunCount.decrementAndGet();
				}
			}
			@Override
			protected void doReset() {
				String threadName = Thread.currentThread().getName();
				System.out.println("Resetting - State: " + getState() + " in thread " + threadName);
				int n = inResetCount.incrementAndGet();
				assert n == 1 : "Too many threads in doInitialization()! [" + n + "] - State: " + getState();
				int n2 = n + inRunCount.get();
				assert n2 == 1 : "Too many threads in doInitialization() and doReset()! [" + n2 + "] - State: " + getState();
				try {
					Thread.sleep(100);
				} catch(InterruptedException e) {
					//ignore
				}
				System.out.println("Reset Integer = " + next.decrementAndGet() + " in thread " + threadName);
				inResetCount.decrementAndGet();
			}

			public boolean cancel(boolean mayInterruptIfRunning) {
				int before = getState();
				boolean success = super.cancel(mayInterruptIfRunning);
				int after = getState();
				if(success)
					System.out.println("Canceled from State " + before + " to " + after);
				else
					System.out.println("Failed cancel from State " + before + " to " + after);
				return success;
			}
			/*
			@Override
			protected boolean isResetOnException() {
				return false;
			}*/
		};

		multiThreadTest(new Runnable() {
			public void run() {
				switch((int)(5*Math.random())) {
					case 0: //assert false : "testing in runnable";
					case 1:
						try {
							// System.out.println("Initializing...");
							initializer.initialize();
						} catch(InterruptedException e) {
							System.out.println("Get Integer <INTERRUPTED> in thread " + Thread.currentThread().getName());
							//e.printStackTrace();
						} catch(CancellationException e) {
							System.out.println("Get Integer <CANCELLED> in thread " + Thread.currentThread().getName());
							//e.printStackTrace();
						} catch(RuntimeException e) {
							System.out.println("Get Integer <EXCEPTION> in thread " + Thread.currentThread().getName());
							e.printStackTrace();
						}
						break;
					case 2: case 3:
						// System.out.println("Resetting...");
						if(initializer.reset())
							resetCount.incrementAndGet();
						else
							System.out.println("Failed reset...");
						break;
					case 4:
						if(initializer.cancel(true))
							cancelCount.incrementAndGet();
						break;
				}
			}
		}, 10, 100, 0, "InitializerTest");
		System.out.println("Reset Count = " + resetCount.get());
		System.out.println("Cancel Count = " + cancelCount.get());

	}

	/** Running this test seems to indicate the the Initializer is faster but only
	 * very slightly to the InitByLock class, but neither is significantly slower than InitNoLock.
	 * @throws Throwable
	 */
	@Test
	public void testInitializerPerf() throws Throwable {
		final int threads = 20;
		final int times = 1000000;
		final int initMS = 50;
		final double percentReset = 0.0;
		final AtomicInteger resetCount = new AtomicInteger();
		final AtomicInteger initCount = new AtomicInteger();
		final Initializer<RuntimeException> initializer = new Initializer<RuntimeException>() {
			@Override
			protected void doInitialize() throws RuntimeException {
				try {
					Thread.sleep(initMS);
				} catch(InterruptedException e) {
					//ignore
				}
				//log.debug("Initializing");
				initCount.incrementAndGet();
			}
			@Override
			protected void doReset() {
				try {
					Thread.sleep(50);
				} catch(InterruptedException e) {
					//ignore
				}
				//log.debug("Resetting");
				resetCount.decrementAndGet();
			}
		};
		abstract class InitByLock {
			protected final ReentrantLock lock = new ReentrantLock();
			protected boolean initialized = false;
			public void initialize() {
				lock.lock();
				try {
					if(initialized == false) {
						doInitialize();
					}
					initialized = true;
				} finally {
					lock.unlock();
				}
			}
			public void reset() {
				lock.lock();
				try {
					if(initialized == true) {
						doReset();
					}
					initialized = false;
				} finally {
					lock.unlock();
				}
			}
			protected abstract void doInitialize() ;
			protected abstract void doReset() ;

		}
		final InitByLock initializer2 = new InitByLock() {
			@Override
			protected void doInitialize() {
				try {
					Thread.sleep(initMS);
				} catch(InterruptedException e) {
					//ignore
				}
				//log.debug("Initializing");
				initCount.incrementAndGet();
			}
			@Override
			protected void doReset() {
				try {
					Thread.sleep(50);
				} catch(InterruptedException e) {
					//ignore
				}
				//log.debug("Resetting");
				resetCount.decrementAndGet();
			}
		};
		abstract class InitNoLock {
			protected volatile boolean initialized = false;
			public void initialize() {
				if(initialized == false) {
					doInitialize();
				}
				initialized = true;

			}
			public void reset() {
				if(initialized == true) {
					doReset();
				}
				initialized = false;

			}
			protected abstract void doInitialize() ;
			protected abstract void doReset() ;

		}
		final InitNoLock initializer3 = new InitNoLock() {
			@Override
			protected void doInitialize() {
				try {
					Thread.sleep(initMS);
				} catch(InterruptedException e) {
					//ignore
				}
				//log.debug("Initializing");
				initCount.incrementAndGet();
			}
			@Override
			protected void doReset() {
				try {
					Thread.sleep(50);
				} catch(InterruptedException e) {
					//ignore
				}
				//log.debug("Resetting");
				resetCount.decrementAndGet();
			}
		};
		multiThreadTest(new Runnable() {
			public void run() {
				if(Math.random() < percentReset) {
					initializer3.reset();
				} else {
					try {
						initializer3.initialize();
					} catch(CancellationException e) {
						log.debug("Get Integer <CANCELLED>", e);
					} catch(RuntimeException e) {
						log.debug("Get Integer <EXCEPTION>", e);
					}
				}
			}
		}, threads, times, 0, "InitNoLockThroughputTest");
		log.info("Reset Count = " + resetCount.get());
		log.info("Init Count = " + initCount.get());
		initCount.set(0);
		resetCount.set(0);
		multiThreadTest(new Runnable() {
			public void run() {
				if(Math.random() < percentReset) {
					initializer.reset();
				} else {
					try {
						initializer.initialize();
					} catch(InterruptedException e) {
						log.debug("Get Integer <INTERRUPTED>", e);
					} catch(CancellationException e) {
						log.debug("Get Integer <CANCELLED>", e);
					} catch(RuntimeException e) {
						log.debug("Get Integer <EXCEPTION>", e);
					}
				}
			}
		}, threads, times, 0, "InitializerThroughputTest");
		log.info("Reset Count = " + resetCount.get());
		log.info("Init Count = " + initCount.get());
		initCount.set(0);
		resetCount.set(0);
		multiThreadTest(new Runnable() {
			public void run() {
				if(Math.random() < percentReset) {
					initializer2.reset();
				} else {
					try {
						initializer2.initialize();
					} catch(CancellationException e) {
						log.debug("Get Integer <CANCELLED>", e);
					} catch(RuntimeException e) {
						log.debug("Get Integer <EXCEPTION>", e);
					}
				}
			}
		}, threads, times, 0, "InitByLockThroughputTest");
		log.info("Reset Count = " + resetCount.get());
		log.info("Init Count = " + initCount.get());
	}

	@Test
	public void testRunOnGetFuture() throws Throwable {
		//assert false : "Test";
		final AtomicInteger next = new AtomicInteger();
		final AtomicInteger resetCount = new AtomicInteger();
		final AtomicInteger cancelCount = new AtomicInteger();
		final AtomicInteger inRunCount = new AtomicInteger();
		final AtomicInteger inResetCount = new AtomicInteger();
		final RunOnGetFuture2<Integer> future = new RunOnGetFuture2<Integer>(new Callable<Integer>() {
			public Integer call() throws Exception {
				/*switch((int)(5*Math.random())) {
					case 0: throw new Exception("Testing an exception");
				}
				*/try {
					Thread.sleep(300);
				} catch(InterruptedException e) {
					//ignore
				}
				return next.incrementAndGet();
			}
		}) {
			@Override
			protected void runComplete(Integer result) {
				int n = inRunCount.incrementAndGet();
				assert n == 1 : "Too many threads in runComplete()! [" + n + "]";
				int n2 = n + inResetCount.get();
				assert n2 == 1 : "Too many threads in runComplete() and resettingRunResult()! [" + n2 + "]";
				inRunCount.decrementAndGet();
			}
			@Override
			protected void resettingRunResult(Integer result) {
				int n = inResetCount.incrementAndGet();
				assert n == 1 : "Too many threads in resettingRunResult()! [" + n + "]";
				int n2 = n + inRunCount.get();
				assert n2 == 1 : "Too many threads in runComplete() and resettingRunResult()! [" + n2 + "]";
				inResetCount.decrementAndGet();
			}
		};

		multiThreadTest(new Runnable() {
			public void run() {
				switch((int)(5*Math.random())) {
					case 0: //assert false : "testing in runnable";
					case 1:
						try {
							Integer i = future.get();
							assert (i != null) : "Future returned a null!";
							System.out.println("Get Integer = " + i);
						} catch(InterruptedException e) {
							System.out.println("Get Integer <INTERRUPTED>");
							//e.printStackTrace();
						} catch(CancellationException e) {
							System.out.println("Get Integer <CANCELLED>");
							//e.printStackTrace();
						} catch(ExecutionException e) {
							System.out.println("Get Integer <EXCEPTION>");
							e.printStackTrace();
						}
						break;
					case 2: case 3:
						System.out.println("Resetting");
						if(future.reset()) resetCount.incrementAndGet();
						break;
					case 4:
						System.out.println("Cancelling");
						if(future.cancel(true)) cancelCount.incrementAndGet();
						break;
				}
			}
		}, 10, 100, 0, "RunOnGetFutureTest");
		System.out.println("Reset Count = " + resetCount.get());
		System.out.println("Cancel Count = " + cancelCount.get());

	}
	@Test
	public void testWaitForUpdateFuture() throws Throwable {
		//assert false : "Test";
		final AtomicInteger next = new AtomicInteger();
		final AtomicInteger resetCount = new AtomicInteger();
		final AtomicInteger cancelCount = new AtomicInteger();
		final AtomicInteger exceptionCount = new AtomicInteger();
		final WaitForUpdateFuture<Integer> future = new WaitForUpdateFuture<Integer>();
		multiThreadTest(new Runnable() {
			public void run() {
				switch((int)(5*Math.random())) {
					case 0: //assert false : "testing in runnable";
						int n = next.incrementAndGet();
						System.out.println("Set Integer = " + n);
						future.set(n);
						break;
					case 1:
						try {
							Integer i = future.get();
							assert (i != null) : "Future returned a null!";
							System.out.println("Get Integer = " + i);
						} catch(InterruptedException e) {
							System.out.println("Get Integer <INTERRUPTED>");
							//e.printStackTrace();
						} catch(CancellationException e) {
							System.out.println("Get Integer <CANCELLED>");
							//e.printStackTrace();
						} catch(ExecutionException e) {
							System.out.println("Get Integer <EXCEPTION>");
							e.printStackTrace();
						}
						break;
					case 2:
						System.out.println("Setting Exception");
						future.setException(new ExecutionException("Fake Exception", null));
						exceptionCount.incrementAndGet();
						break;
					case 3:
						System.out.println("Resetting");
						if(future.reset()) resetCount.incrementAndGet();
						break;
					case 4:
						System.out.println("Cancelling");
						if(future.cancel(true)) cancelCount.incrementAndGet();
						break;
				}
			}
		}, 10, 100, 0, "RunOnGetFutureTest");
		System.out.println("Exception Count = " + exceptionCount.get());
		System.out.println("Reset Count = " + resetCount.get());
		System.out.println("Cancel Count = " + cancelCount.get());

	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
