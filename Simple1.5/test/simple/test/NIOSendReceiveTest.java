package simple.test;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class NIOSendReceiveTest {
	protected static final int DEFAULT_PORT = 12345;
	protected static DateFormat logDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
	protected ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 10, 2000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(1000));

	protected static enum WorkerType {
		RECEIVE, SEND
	};

	protected static abstract class Worker implements Runnable {
		protected final String id;
		protected final SocketChannel socketChannel;
		protected final ByteBuffer buffer;
		protected final AtomicInteger index = new AtomicInteger();
		protected final WorkerType workerType;

		public Worker(SocketChannel socketChannel, WorkerType workerType, int bufferSize) {
			this.socketChannel = socketChannel;
			this.workerType = workerType;
			this.buffer = ByteBuffer.allocate(bufferSize);
			final Socket socket = socketChannel.socket();
			id = socket.getRemoteSocketAddress().toString() + " on port " + socket.getLocalPort();
			log_info("Opened connection to " + id + " to " + workerType.toString().toLowerCase() + " requests");
		}

		public void run() {
			switch(workerType) {
				case RECEIVE:
					runReceive();
					break;
				case SEND:
					runSend();
					break;
			}
		}

		protected abstract void runReceive();

		protected abstract void runSend();

		protected void read(int limit) {
			// read from socket
			for(int i = 0; limit > 0 && i < limit; i++) {
				int r;
				try {
					r = socketChannel.read(buffer);
				} catch(IOException e) {
					log_error("Could not read from " + id, e);
					try {
						socketChannel.close();
					} catch(IOException e1) {
						log_error("Could not close " + id, e);
					}
					return;
				}
				if(r < 0) {
					log_info("Connection closed from " + id);
					try {
						socketChannel.close();
					} catch(IOException e) {
						log_error("Could not close " + id, e);
					}
					return;
				}
				log_info("Read " + r + " bytes from " + id);
				if(limit > 0 && i > 0 && r == 0)
					break;
				buffer.flip();
				while(buffer.remaining() > 1) {
					int s = readUnsignedShort(buffer);
					if(index.get() != s)
						log_error("BYTES WERE SKIPPED AT POSITION " + (index.get() * 2) + " ON " + id + "; expected " + index.get() + " but received " + s, new Throwable());
					index.set(s + 1);
				}
				buffer.compact();
			}
		}

		protected void write(long pause, int limit) {
			while(index.get() < Short.MAX_VALUE) {
				for(int i = 0; buffer.position() < buffer.limit() - 1 && (limit < 1 || i < limit) && index.get() < Short.MAX_VALUE; i++) {
					writeUnsignedShort(buffer, index.getAndIncrement());
				}
				buffer.flip();
				while(buffer.hasRemaining()) {
					int w;
					try {
						w = socketChannel.write(buffer);
					} catch(IOException e) {
						log_error("Could not write to connection " + id, e);
						return;
					}
					log_info("Wrote " + w + " bytes to " + id + " (index=" + index.get() + ")");
				}
				buffer.compact();
				if(pause > 0)
					try {
						Thread.sleep(pause);
					} catch(InterruptedException e) {
						log_info("Thread interuppted for client " + id + "; exiting...");
					}
			}
		}
	}
	protected static String getClientInfo(SocketChannel socketChannel) {
		if(socketChannel != null) {
			Socket socket = socketChannel.socket();
			if(socket != null)
				return ", client: " + socket.getRemoteSocketAddress() + " on port " + socket.getLocalPort();
		}
		return "";
	}

	protected static int readUnsignedShort(ByteBuffer buffer) {
		return (((buffer.get() & 0xff) << 8) + ((buffer.get() & 0xff) << 0));
	}

	protected static void writeUnsignedShort(ByteBuffer buffer, int s) {
		buffer.put((byte) ((s >>> 8) & 0xFF));
		buffer.put((byte) ((s >>> 0) & 0xFF));
	}

	protected static void log_info(String message) {
		synchronized(logDateFormat) {
			System.out.print(logDateFormat.format(new Date()));
			System.out.print(' ');
			System.out.println(message);
		}
	}

	protected static void log_error(String message, Throwable exception) {
		synchronized(logDateFormat) {
			System.out.print(logDateFormat.format(new Date()));
			System.out.print(' ');
			System.out.println(message);
			exception.printStackTrace(System.out);
		}
	}

	protected static WorkerType getWorkerType(String workerTypeString) {
		for(WorkerType workerType : WorkerType.values()) {
			if(workerType.toString().equalsIgnoreCase(workerTypeString.trim()))
				return workerType;
		}
		return null;
	}

	protected static String getStringArg(String[] args, int index, String defaultValue) {
		String value;
		if(args.length > index && args[index] != null && (value = args[index].trim()).length() > 0)
			return value;
		return defaultValue;
	}

	protected static int getIntArg(String[] args, int index, int defaultValue) {
		if(args.length > index && args[index] != null)
			try {
				return Integer.parseInt(args[index].trim());
			} catch(Exception e) {
				return defaultValue;
			}
		return defaultValue;
	}

	public static void main(String[] args) {
		if(args == null || args.length == 0 || args[0] == null)
			System.err.println("You must specify the first argument ('server' or 'client')");
		else if(args.length == 1 || args[1] == null)
			System.err.println("You must specify the second argument ('send' or 'receive')");
		else if(args[0].equalsIgnoreCase("server")) {
			WorkerType workerType = getWorkerType(args[1]);
			if(workerType == null) {
				System.err.println("Invalid worker type '" + args[1] + "'; please specify either 'send' or 'receive'");
			} else {
				int port = getIntArg(args, 2, DEFAULT_PORT);
				long pause = getIntArg(args, 3, 100);
				int bufferSize = getIntArg(args, 4, 1024);
				try {
					new NIOSendReceiveServer(port).runServer(workerType, pause, bufferSize);
				} catch(IOException e) {
					log_error("Could not open server on port " + port, e);
				}
			}
		} else if(args[0].equalsIgnoreCase("client")) {
			WorkerType workerType = getWorkerType(args[1]);
			if(workerType == null) {
				System.err.println("Invalid worker type '" + args[1] + "'; please specify either 'send' or 'receive'");
			} else {
				String host = getStringArg(args, 2, "localhost");
				int port = getIntArg(args, 3, DEFAULT_PORT);
				int num = getIntArg(args, 4, 1);
				long pause = getIntArg(args, 5, 100);
				int bufferSize = getIntArg(args, 6, 1024);
				try {
					new NIOSendReceiveClient(host, port).runClient(workerType, num, pause, bufferSize);
				} catch(IOException e) {
					log_error("Could not open client on " + host + ':' + port, e);
				} catch(InterruptedException e) {
					log_error("Could not open client on " + host + ':' + port, e);
				} catch(ExecutionException e) {
					log_error("Could not open client on " + host + ':' + port, e);
				}
			}
		} else {
			System.err.println("The first argument must be either 'server' or 'client'");
		}
		System.exit(0);
	}
}
