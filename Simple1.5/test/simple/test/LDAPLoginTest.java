/**
 *
 */
package simple.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.PartialResultException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.TextOutputCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

/**
 * @author Brian S. Krug
 *
 */
public class LDAPLoginTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//testJNDI();
		testLogin();
	}
	protected static void testLogin() {
		URL config = ClassLoader.getSystemResource("simple/test/test-jaas.config");
		String s = "E:\\Java Projects\\Simple1.5\\test\\simple\\test\\test-jaas.config";
		//String s = config.toString();
		System.setProperty("java.security.auth.login.config", "=" + s);
		LoginContext lc = null;
		try {
		    lc = new LoginContext("Test8", new CallbackHandler() {
		    	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		    	for (int i = 0; i < callbacks.length; i++) {
		    	    if (callbacks[i] instanceof TextOutputCallback) {

		    		// display the message according to the specified type
		    		TextOutputCallback toc = (TextOutputCallback)callbacks[i];
		    		switch (toc.getMessageType()) {
		    		case TextOutputCallback.INFORMATION:
		     		    System.out.println(toc.getMessage());
		     		    break;
		     		case TextOutputCallback.ERROR:
		     		    System.out.println("ERROR: " + toc.getMessage());
		     		    break;
		     		case TextOutputCallback.WARNING:
		     		    System.out.println("WARNING: " + toc.getMessage());
		     		    break;
		     		default:
		     		    throw new IOException("Unsupported message type: " +
		     					toc.getMessageType());
		     		}

		     	    } else if (callbacks[i] instanceof NameCallback) {

		     		// prompt the user for a username
		     		NameCallback nc = (NameCallback)callbacks[i];

		     		System.err.print(nc.getPrompt());
		     		System.err.flush();
		     		nc.setName((new BufferedReader
		    			(new InputStreamReader(System.in))).readLine());

		     	    } else if (callbacks[i] instanceof PasswordCallback) {

		     		// prompt the user for sensitive information
		     		PasswordCallback pc = (PasswordCallback)callbacks[i];
		     		System.err.print(pc.getPrompt());
		     		System.err.flush();
		     		pc.setPassword((new BufferedReader
			    			(new InputStreamReader(System.in))).readLine().toCharArray());

		     	    } else {
		     		throw new UnsupportedCallbackException
		     			(callbacks[i], "Unrecognized Callback");
		     	    }
		    	}
		        }

		    });
		} catch (LoginException le) {
		    System.err.println("Cannot create LoginContext. "
		        + le.getMessage());
		    System.exit(-1);
		} catch (SecurityException se) {
		    System.err.println("Cannot create LoginContext. "
		        + se.getMessage());
		    System.exit(-1);
		}

		// the user has 3 attempts to authenticate successfully
		int i;
		for (i = 0; i < 1; i++) {
		    try {

			// attempt authentication
			lc.login();

			// if we return with no exception, authentication succeeded
			break;

		    } catch (LoginException le) {

			  System.err.println("Authentication failed:");
			  System.err.println("  " + le.getMessage());
		    }
		}

		// did they fail three times?
		if (i == 3) {
		    System.out.println("Sorry");
		    System.exit(-1);
		}

		System.out.println("Authentication succeeded!");

	}

	protected static void testJNDI() {
		Hashtable env = new Hashtable(9);
		env.put(Context.INITIAL_CONTEXT_FACTORY,
		    "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, "ldap://usadevdc01.usatech.com:389");
		env.remove(Context.SECURITY_PROTOCOL);
		env.put(Context.SECURITY_CREDENTIALS, "goN2stk");
		env.put(Context.SECURITY_PRINCIPAL, "USATECHTEST\\bkrug");
		env.put("java.naming.referral", "ignore");
	    try {
		// Connect to the LDAP server (using simple bind)
	    	LdapContext ctx = new InitialLdapContext(env, null);
	    	//Attributes attributes = ctx.getAttributes(ctx.getNameInNamespace() );
	    	//Attribute attribute = attributes.get("defaultNamingContext");
	    	//NamingEnumeration<NameClassPair> names = ctx.list("DC=usatechtest,DC=com");
	    	//Object obj = ctx.lookup("DC=usatechtest,DC=com");
	        printJNDITree(ctx, System.out, 0);
	        NamingEnumeration<SearchResult> results = ctx.search("DC=usatechtest,DC=com", "(samaccountname=bkrug)", new SearchControls(SearchControls.SUBTREE_SCOPE, 0, 0, null, false, false));
	    	while(results.hasMore()) {
	    		SearchResult result = results.next();
	    		System.out.println("MATCH: " + result.getNameInNamespace());
	    	}
	    } catch(NamingException e) {
	    	e.printStackTrace();
	    	return;
	    } catch(ClassNotFoundException e) {
	    	e.printStackTrace();
	    	return;
		}
	}

	public static void printJNDITree(DirContext ctx, PrintStream ps, int level) throws NamingException,
    ClassNotFoundException {
		Attributes attributes = ctx.getAttributes(ctx.getNameInNamespace());
		Attribute attribute = attributes.get("defaultNamingContext");
		String name = (String)attribute.get(0);
		printJNDITree(ctx, name, ps, level);
}
	public static void printJNDITree(DirContext ctx, String name, PrintStream ps, int level) throws NamingException,
    ClassNotFoundException {
		final char[] fill = new char[level * 4];
		java.util.Arrays.fill(fill, ' ');
		final String indent = new String(fill);
		NamingEnumeration<NameClassPair> names = ctx.list(name);
		try {
    while(names.hasMore()) {
	    NameClassPair ncp = names.next();
	    Attributes attributes = ctx.getAttributes(ncp.getNameInNamespace());
	    Attribute attribute = attributes.get("samaccountname");
	    if(attribute != null && attribute.contains("bkrug")) {
	    	ps.print("*");
	    }
	    ps.println(indent + "+ " + ncp.getName() + " (" + ncp.getClassName() + "): " + attributes);
	    printJNDITree(ctx, ncp.getNameInNamespace(), ps, level + 1);
   }} catch(PartialResultException e) {
    	ps.println(e.getMessage());
    }
    //printJNDITree((DirContext) ctx.lookup(name), ps, level + 1);
}

}
