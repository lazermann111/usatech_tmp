package simple.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.text.RegexUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

public class RegexTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testCheckboxEditorRegex() throws Exception {
		String regex = "(?:~(\\d+)~)?([?+*]?)(.*)";
		testRegex(regex, "*hbc=uwei", null);
		testRegex(regex, "+hbc=uwei", null);
		testRegex(regex, "hbc=uwei", null);
		testRegex(regex, "~16~?hbc=uwei", null);
		testRegex(regex, "~16~hbc=uwei", null);
	}

	@Test
	public void testFileNameRegex() throws Exception {
		String regex = "(?:.*/)?([^/]+)_[^/]+\\.csv";
		testFind(regex, "./abc/def/AccountInfo_one.csv", null);
		testFind(regex, "AccountInfo_one.csv", null);
		testFind(regex, "/AccountInfo_two.csv", null);
		testFind(regex, "/root/AccountInfo_two.csv", null);
	}

	@Test
	public void testRenameRegex() throws Exception {
		String regex = "(.*/)?([^/]+)";
		testFind(regex, "./abc/def/one.csv", null);
		testFind(regex, "one.csv", null);
		testFind(regex, "/two.csv", null);
		testFind(regex, "/root/two.csv", null);
	}

	@Test
	public void testFromQuotes() throws Exception {
		String[] ss = new String[] {
				"\\Q/test/data/253145/\\E\\d{1,}\\.150927\\.d\\.A\\d{3}\\.dfr",
				"\\.klj" + Pattern.quote("/data/test/2343244/") + "\\d{1,}\\.",
				"nohting",
				Pattern.quote("\\E\\Q\\q\\e")
		};
		for(String s : ss)
			log.info("From '" + s + "' to '" + RegexUtils.fromQuotesToBackslash(s) + "'");
	}
	@Test
	public void testOutputTypeLinkRegex() throws Exception {
		String regex = "[?&]outputType=(?!22($|\\D))\\d+";
		testFind(regex, "./select_date_range_frame_sqlfolio.i?basicReportId=415&outputType=22", null);
		testFind(regex, "./select_date_range_frame_sqlfolio.i?basicReportId=415&outputType=222", null);
		testFind(regex, "./select_date_range_frame_sqlfolio.i?basicReportId=415&outputType=22&nothing=somthing", null);
		testFind(regex, "./select_date_range_frame_sqlfolio.i?basicReportId=415", null);
		testFind(regex, "./select_date_range_frame_sqlfolio.i?basicReportId=415&outputType=21", null);
		testFind(regex, "./select_date_range_frame_sqlfolio.i?basicReportId=415&outputType=422", null);
		testFind(regex, "./select_date_range_frame_sqlfolio.i?basicReportId=415&outputType=12", null);
		testFind(regex, "./select_date_range_frame_sqlfolio.i?basicReportId=415&outputType=23&nothing=somthing", null);

	}

	@Test
	public void testQueueMatchRegex() throws Exception {
		testRegex("usat\\.keymanager\\.(?:account\\.token\\.create|retrieve|account\\.lookup)", "usat.keymanager.account.token.create", null);

	}

	@Test
	public void testNonCreditRegex() throws Exception {
		//String regex = "^;?(?!(?:4[0-9]{15}|5[1-5][0-9]{14}|3[47][0-9]{13}|(?:30[0-5][0-9]|3095|35[2-8][0-9]|36|3[8-9][0-9]{2}|6011|622[1-9]|62[4-6][0-9]|628[2-8]|64[4-9][0-9]|65[0-9]{2})[0-9]{12}|639621[0-9]{10,13}|627722[0-9]{10})(?:=[0-9]+|$))([0-9]{6,}(?:=[0-9]+)?)(?:\\?([\\x00-\\xFF])?)?$";
		String regex = "^;?(?!(?:4[0-9]{15}|5[1-5][0-9]{14}|3[47][0-9]{13}|(?:30[0-5][0-9]|3095|35[2-8][0-9]|36|3[8-9][0-9]{2}|6011|622[1-9]|62[4-6][0-9]|628[2-8]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})(?:=[0-9]+|$))([0-9]{6,}(?:=[0-9]+)?)(?:\\?([\\x00-\\xFF])?)?$";
		String[] tests = { ";5454545454545454=123452359?7", ";54545454545454=123452359?7", "368018123454208=1234123", "368018123454208", "3680181234542089=1234123", "3680181234542089", "36801812345420=1234123", "36801812345420" };
		for(String test : tests)
			testRegex(regex, test, null);
	}

	@Test
	public void testOS400List() throws Exception {
		// String regex = "\\s*(\\S+)\\s+(\\d+)\\s+(\\d\\d/\\d\\d/\\d{4} \\d\\d:\\d\\d:\\d\\d)\\s+\\*(\\S+)\\s+(\\S.*)/?\\s*";
		String regex = "\\s*(\\S+)\\s+(\\d+)\\s+(\\d\\d/\\d\\d/\\d{2,4} \\d\\d:\\d\\d:\\d\\d)\\s+\\*(\\S+)\\s+(\\S.*)/?\\s*";
		String test = "QSYS           155648 08/08/14 22:10:58 *DIR       QOpenSys/";
		testRegex(regex, test, null);
	}

	@Test
	public void testFileParsing() throws Exception {
		File file = new File("C:\\Users\\bkrug\\Documents\\Chase Paymentech\\GBINP.V005.04012015.P");
		// Pattern pattern =
		// Pattern.compile("^D\\|([0-9]{6,9})[0-9]{0,10}\\|([0-9]{6,9})[0-9]{0,10}\\|[0-9]{1,2}\\|[^|]{0,60}\\|[^|]{0,3}\\|[A-Z]\\|[PEHS][^|]?\\|[^|]{0,2}\\|[^|]{0,2}\\|[^|]{0,2}\\|[^|]{0,2}\\|[^|]{0,60}\\|[^|]{0,2}\\|[^|]{0,2}\\|[^|]{0,71}$");
		// Pattern pattern =
		// Pattern.compile("^D\\|([0-9]{6,9})[0-9]{0,10}\\|([0-9]{6,9})[0-9]{0,10}\\|[0-9]{1,2}\\|[^|]{0,60}\\|[^|]{0,3}\\|[A-Z]\\|[^|]*[PEHS][^|]*\\|[^|]{0,2}\\|[^|]{0,6}\\|[^|]{0,2}\\|[^|]{0,60}\\|[^|]{0,2}\\|[^|]{0,2}\\|[^|]{0,71}$");
		Pattern pattern = Pattern.compile("^D\\|([0-9]{6,9})[0-9]{0,10}\\|([0-9]{6,9})[0-9]{0,10}\\|[0-9]{1,2}\\|[^|]{0,60}\\|[^|]{0,3}\\|[A-Z]\\|([^|]*[PEHS][^|]*)\\|[^|]{0,2}\\|[^|]{0,6}(?:\\|[^|]*)+$");
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		try {
			String line;
			int n = 0;
			while((line = reader.readLine()) != null && n < 1000) {
				n++;
				Matcher matcher = pattern.matcher(line);
				sb.setLength(0);
				StringUtils.appendPadded(sb, Integer.toString(n), ' ', 4, Justification.RIGHT);
				sb.append(": ");
				if(matcher.matches()) {
					sb.append("MATCH: [");
					for(int i = 1; i <= matcher.groupCount(); i++) {
						if(i > 1)
							sb.append(", ");
						sb.append(matcher.group(i));
					}
					sb.append(']');
				} else {
					sb.append("NO MATCH: '").append(line).append("'");
				}
				log.info(sb.toString());

			}
		} finally {
			reader.close();
		}
	}

	@Test
	public void testCAPostalParsing() throws Exception {
		String content = "<td width=\"20%\" valign=\"top\"><span style=\"line-height: 125%\"><b>A1A</b><br />\r\n<a href=\"/wiki/St._John%27s,_Newfoundland_and_Labrador\" title=\"St. John's, Newfoundland and Labrador\">St. John's</a><br />\r\nNorth</span></td>\r\n<td width=\"20%\" valign=\"top\"><span style=\"line-height: 125%\"><b>A2A</b><br />\r\n<a href=\"/wiki/Grand_Falls,_Newfoundland_and_Labrador\" title=\"Grand Falls, Newfoundland and Labrador\" class=\"mw-redirect\">Grand Falls</a></span></td>\r\n<td width=\"20%\" valign=\"top\"><span style=\"line-height: 125%\"><b>A5A</b><br />\r\n<a href=\"/wiki/Clarenville,_Newfoundland_and_Labrador\" title=\"Clarenville, Newfoundland and Labrador\" class=\"mw-redirect\">Clarenville</a></span></td>\r\n<td width=\"20%\" valign=\"top\"><span style=\"line-height: 125%\"><b>A8A</b><br />\r\n<a href=\"/wiki/Deer_Lake,_Newfoundland_and_Labrador\" title=\"Deer Lake, Newfoundland and Labrador\">Deer Lake</a></span></td>\r\n<td width=\"20%\" valign=\"top\"><span style=\"color: #CCC; line-height: 125%\"><b>A9A</b><br />\r\n<i>Not assigned</i></span></td>\r\n</tr>\r\n<tr>\r\n<td width=\"20%\" valign=\"top\"><span style=\"line-height: 125%\"><b>A1B</b><br />\r\n<a href=\"/wiki/St._John%27s,_Newfoundland_and_Labrador\" title=\"St. John's, Newfoundland and Labrador\">St. John's</a><br />\r\nNorthwest</span>\r\n<hr />\r\n<p><span style=\"line-height: 125%\"><span style=\"font-size: smaller; line-height: 125%;\"><b>Newfoundland &amp; Labrador Provincial Government</b></span></span></p>\r\n</td>\r\n<td width=\"20%\" valign=\"top\"><span style=\"line-height: 125%\"><b>A2B</b><br />\r\n<a href=\"/wiki/Windsor,_Newfoundland_and_Labrador\" title=\"Windsor, Newfoundland and Labrador\" class=\"mw-redirect\">Windsor</a></span></td>\r\n<td width=\"20%\" valign=\"top\"><span style=\"color: #CCC; line-height: 125%\"><b>A5B</b><br />\r\n<i>Not assigned</i></span></td>\r\n<td width=\"20%\" valign=\"top\"><span style=\"color: #CCC; line-height: 125%\"><b>A8B</b><br />\r\n<i>Not assigned</i></span></td>\r\n<td width=\"20%\" valign=\"top\"><span style=\"color: #CCC; line-height: 125%\"><b>A9B</b><br />\r\n<i>Not assigned</i></span></td>\r\n</tr>";
		String content2 = "<td width=\"20%\" valign=\"top\"><b>A0N</b><br />\r\nSouthwest Newfoundland / <a href=\"/wiki/Port_au_Port_Peninsula\" title=\"Port au Port Peninsula\">Port au Port Peninsula</a>\r\n<p><span style=\"font-size: smaller; line-height: 125%;\">1A0: <a href=\"/wiki/Aguathuna,_Newfoundland_and_Labrador\" title=\"Aguathuna, Newfoundland and Labrador\" class=\"mw-redirect\">Aguathuna</a><br />\r\n1B0: <a href=\"/wiki/Barachois_Brook,_Newfoundland_and_Labrador\" title=\"Barachois Brook, Newfoundland and Labrador\">Barachois Brook</a><br />\r\n1C0: <a href=\"/wiki/Cape_Ray,_Newfoundland_and_Labrador\" title=\"Cape Ray, Newfoundland and Labrador\">Cape Ray</a><br />\r\n1E0: <a href=\"/wiki/Cape_St._George,_Newfoundland_and_Labrador\" title=\"Cape St. George, Newfoundland and Labrador\">Cape St. George</a><br />\r\n1G0: <a href=\"/wiki/Cartyville,_Newfoundland_and_Labrador\" title=\"Cartyville, Newfoundland and Labrador\">Cartyville</a><br />\r\n1H0: <a href=\"/wiki/Codroy,_Newfoundland_and_Labrador\" title=\"Codroy, Newfoundland and Labrador\">Codroy</a><br />\r\n1J0: <a href=\"/w/index.php?title=Doyles,_Newfoundland_and_Labrador&amp;action=edit&amp;redlink=1\" class=\"new\" title=\"Doyles, Newfoundland and Labrador (page does not exist)\">Doyles</a><br />\r\n1K0: <a href=\"/w/index.php?title=Grand_Bay_East,_Newfoundland_and_Labrador&amp;action=edit&amp;redlink=1\" class=\"new\" title=\"Grand Bay East, Newfoundland and Labrador (page does not exist)\">Grand Bay East</a><br />\r\n1M0: <a href=\"/wiki/Heatherton,_Newfoundland_and_Labrador\" title=\"Heatherton, Newfoundland and Labrador\">Heatherton</a><br />\r\n1N0: <a href=\"/wiki/Highlands,_Newfoundland_and_Labrador\" title=\"Highlands, Newfoundland and Labrador\">Highlands</a><br />\r\n1P0: <a href=\"/wiki/Jeffrey%27s,_Newfoundland_and_Labrador\" title=\"Jeffrey's, Newfoundland and Labrador\">Jeffrey's</a><br />\r\n1R0: <a href=\"/wiki/Lourdes,_Newfoundland_and_Labrador\" title=\"Lourdes, Newfoundland and Labrador\">Lourdes</a><br />\r\n1S0: <a href=\"/wiki/Noel%27s_Pond,_Newfoundland_and_Labrador\" title=\"Noel's Pond, Newfoundland and Labrador\">Noels Pond</a><br />\r\n1T0: <a href=\"/wiki/Port_au_Port,_Newfoundland_and_Labrador\" title=\"Port au Port, Newfoundland and Labrador\">Port au Port</a><br />\r\n1V0: <a href=\"/wiki/Robinsons,_Newfoundland_and_Labrador\" title=\"Robinsons, Newfoundland and Labrador\">Robinsons</a><br />\r\n1W0: <a href=\"/wiki/St._Andrews,_Newfoundland_and_Labrador\" title=\"St. Andrews, Newfoundland and Labrador\" class=\"mw-redirect\">St. Andrews</a><br />\r\n1X0: <a href=\"/wiki/St._David%27s,_Newfoundland_and_Labrador\" title=\"St. David's, Newfoundland and Labrador\">St. David's</a><br />\r\n1Y0: <a href=\"/wiki/St._Fintan%27s,_Newfoundland_and_Labrador\" title=\"St. Fintan's, Newfoundland and Labrador\">St. Fintan's</a><br />\r\n1Z0: <a href=\"/wiki/St._George%27s,_Newfoundland_and_Labrador\" title=\"St. George's, Newfoundland and Labrador\">St. George's</a><br />\r\n2B0: <a href=\"/wiki/South_Branch,_Newfoundland_and_Labrador\" title=\"South Branch, Newfoundland and Labrador\">South Branch</a><br />\r\n2C0: <a href=\"/wiki/Stephenville_Crossing,_Newfoundland_and_Labrador\" title=\"Stephenville Crossing, Newfoundland and Labrador\" class=\"mw-redirect\">Stephenville Crossing</a><br />\r\n2E0: <a href=\"/wiki/West_Bay_Centre,_Newfoundland_and_Labrador\" title=\"West Bay Centre, Newfoundland and Labrador\" class=\"mw-redirect\">West Bay Centre</a><br />\r\n2G0: <a href=\"/w/index.php?title=Black_Duck_Siding,_Newfoundland_and_Labrador&amp;action=edit&amp;redlink=1\" class=\"new\" title=\"Black Duck Siding, Newfoundland and Labrador (page does not exist)\">Black Duck Siding</a><br />\r\n2H0: <a href=\"/wiki/Burgeo,_Newfoundland_and_Labrador\" title=\"Burgeo, Newfoundland and Labrador\" class=\"mw-redirect\">Burgeo</a><br />\r\n2J0: <a href=\"/wiki/Ramea,_Newfoundland_and_Labrador\" title=\"Ramea, Newfoundland and Labrador\" class=\"mw-redirect\">Ramea</a><br />\r\n2K0: <a href=\"/wiki/Fran%C3%A7ois,_Newfoundland_and_Labrador\" title=\"Fran�ois, Newfoundland and Labrador\" class=\"mw-redirect\">Fran�ois</a><br />\r\n2L0: <a href=\"/wiki/Grey_River,_Newfoundland_and_Labrador\" title=\"Grey River, Newfoundland and Labrador\">Grey River</a></span></p>\r\n</td>";
		String content3 = "<td width=\"14.2857%\" valign=\"top\"><b>K2A</b><br />\r\n<span style=\"font-size: smaller; line-height: 125%;\"><b><a href=\"/wiki/Ottawa\" title=\"Ottawa\">Ottawa</a></b><br />\r\n(<a href=\"/wiki/Highland_Park_(Ottawa)\" title=\"Highland Park (Ottawa)\" class=\"mw-redirect\">Highland Park</a> / <a href=\"/wiki/McKellar_Park\" title=\"McKellar Park\">McKellar Park</a> / <a href=\"/wiki/Carlingwood\" title=\"Carlingwood\" class=\"mw-redirect\">Carlingwood</a>)</span></td>\r\n";
		// String regex = "<b>(A\\d[A-Z])</b>\\s*<br />\\s*(?:<span[^>]*>)?(?:[^<]*<[ab][^>]*>)?([^\\s<][^<]*)(?:</[ab]>)?(?:.*<p>(.*?)</p>.*)??</td>";
		// String regex = "<b>(A\\d[A-Z])</b>\\s*<br />\\s*(?:<span[^>]*>)?(?:[^<]*<[ab][^>]*>)?([^\\s<][^<]*)(?:</[ab]>)?(?!</td>|<p>)*(?:<p>((?!</p>)*)</p>)?\\s*</td>";//
		// (?!</td>)*(?:<p>(.*?)</p>)?\\s*</td>
		String regex = "<b>(K[1-9][A-Z])</b>\\s*<br />\\s*(?:<span[^>]*>)?(?:[^<]*<[ab][^>]*>)*([^\\s<][^<]*)(?:</[ab]>)*";
		// String regex =
		// "<b>(A[1-9][A-Z])</b>\\s*<br />\\s*(?:<span[^>]*>)?(?:[^<]*<[ab][^>]*>)?([^\\s<][^<]*)(?:</[ab]>)?|<b>(A0[A-Z])</b>\\s*<br />\\s*(?:<span[^>]*>)?(?:[^<]*<[ab][^>]*>)?([^\\s<][^<]*)(?:</[ab]>)?.*?<p>(.*?)</p>";//
		// (?!</td>)*(?:<p>(.*?)</p>)?\\s*</td>
		String regex2 = "\\s+(\\d[A-Z]\\d):\\s*<a href=\"[^\"]*\"\\s+title=\"[^\"]*\"\\s*>([^<]+)</a>";
		testFind(regex, content3, "s");
		/*
		testFind(regex, content, "s");
		testFind(regex, content2, "s");
		testFind(regex2, content2, "s");
		*/
	}

	@Test
	public void testWildcards() throws Exception {
		String[] tests = new String[] {
				"this should not contain wildcards",
				"this? may contain one",
				"this\\? does not it is escaped",
				"[hH]ere is one",
				"^somthing$",
				"\\tAtab will be wild",
				"\\d Digits will be wild",
				"\\ Space is escaped"
		};
		for(String t : tests) {
			if(RegexUtils.containsWildcards(t))
				log.info("Found wildcards in: " + t);
			else
				log.info("NO WILDCARDS in: " + t);
		}

		for(String t : tests) {
			log.info("Unescaped = " + RegexUtils.unescapeBackslash(t));
		}
	}
	@Test
	public void testStepParsing() throws Exception {
		String regex = "step(?:\\.?(\\d+)|\\[(\\d+)\\]|\\((\\d+)\\))\\.(queueKey|(?:attribute|resultAttribute)(?:\\.(.+)|\\((.+)\\)))";
		testRegex(regex, "step1.queueKey", null);
		testRegex(regex, "step[1].queueKey", null);
		testRegex(regex, "step(1).queueKey", null);
		testRegex(regex, "step.1.attribute.NAME1", null);
		testRegex(regex, "step1.attribute(NAME1)", null);
		testRegex(regex, "step[1].attribute.abc.def(yaba).2", null);
		testRegex(regex, "step(1).attribute(nothing.something)", null);
		testRegex(regex, "step.1.resultAttribute(fileName)", null);

	}

	@Test
	public void testEditor() throws Exception {
		String regex = "([^:]+)(?::((?:[^\\\\]*(?:\\\\\\\\)*)*))?(?:\\\\(.*))?";
		testRegex(regex, "BITMAP:1=1 - Capture DEX Data;2=2 - Clear A-Counters;4=4 - Clear I-Counters;8=8 - Reserved;16=16 - Show A-Counter: Cash Vended Items;32=32 - Show A-Counter: Cash Vended Amount (Pennies);64=64 - Show A-Counter: Cashless Vended Items;128=128 - Show A-Counter: Cashless Vended Amount (Pennies);256=256 - Show I-Counter: Cash Vended Items;512=512 - Show I-Counter: Cash Vended Amount (Pennies);1024=1024 - Show I-Counter: Cashless Vended Items;2048=2048 - Show I-Counter: Cashless Vended Amount (Pennies);4096=4096 - Show P-Counter: Cash Vended Items;8192=8192 - Show P-Counter: Cash Vended Amount (Pennies);16384=16384 - Show P-Counter: Cashless Vended Items;32768=32768 - Show P-Counter: Cashless Vended Amount (Pennies)", null);
		testRegex(regex, "TEXT:something", null);
		testRegex(regex, "TEXT:", null);
		testRegex(regex, "TEXT", null);
		testRegex(regex, "SELECT:1=One;2=Two", null);
		testRegex(regex, "SELECT:1=One;2=Two\\my happy javascript\\and more", null);
		testRegex(regex, "SELECT:1=One;2=Two\\\\my happy javascript\\and more", null);
		
	}
	
	@Test
	public void testResponseCode() throws Exception {
		String regex = "([^;]*)(?:;CVV:([^;]*))?(?:;AVS:([^;]*))?";
		testRegex(regex, "00;AVS: ", null);
		testRegex(regex, "00;AVS:A", null);
		testRegex(regex, "00", null);
		testRegex(regex, "00;CVV:N", null);
		testRegex(regex, "00;CVV:N;AVS:A", null);

	}
	@Test
	public void testPathToPackage() throws Exception {
		testReplace("[/\\\\]", "com/usatech/special/Test.class", ".", null);
		testReplace("[/\\\\]", "com\\usatech\\special\\Test.class", ".", null);

	}

	@Test
	public void testManualCard() throws Exception {
		String regex = "\\|(\\w[^|]*)$";
		testRegex(regex, "|blah blah .-#4    blah", null);
		testRegex(regex, "|432 W. Ninth #289-098", null);
		testRegex(regex, "|    N.   7th St.  DC & BF", null);

	}

	@Test
	public void testLogParsing() throws Exception {
		String regex = "2014-06-06 10:[12]\\d:\\d\\d,\\d+ \\[RecordAuth Thread #\\d+\\] INFO  com.usatech.app.MessageChainService - FINISHED step '([^']+)'.*attributes=\\{.*globalAccountId(?:\\.\\d+)?=(\\d+)[,}].*";
		//String regex = "2014-06-06 10:[12]\\d:\\d\\d,\\d+ \\[RecordAuth Thread #\\d+\\] INFO <> com.usatech.app.MessageChainService - FINISHED step '([^']+)'.*";
		String search = "2014-06-06 10:27:03,599 [RecordAuth Thread #46] INFO  com.usatech.app.MessageChainService - FINISHED step '9385@usanet32.trooper.usatech.com:146715D6B90:226653:2': [\"usat.inbound.auth.record\"]<--[\"usat.keymanager.store#not.4\" by 39047@usakls31.trooper.usatech.com in 2ms]<--[\"usat.keymanager.store#any\" by 60064@usakls34.trooper.usatech.com in 2ms]<--[\"netlayer-usanet32-14107.outbound\" by 9385@usanet32.trooper.usatech.com in 10ms]<--[\"usat.authority.realtime.authority_iso8583_elavon\" by 21006@usaapr34.trooper.usatech.com in 585ms]<--[\"usat.inbound.message.auth\" by 6384@usaapr31.trooper.usatech.com in 8ms]<--[\"usat.keymanager.account.lookup\" by 39047@usakls31.trooper.usatech.com in 2ms]: attributes={trackData=426684******0038, currencyCd=USD, cardKey=null, invalidEventId=N, traceNumber=4600884552876886389, deviceName=EV219111, eventId=1402046808, balanceAmount=null, timeZoneGuid=US/Central, minorCurrencyFactor=100, authAuthorityMiscData=elavonAuthorizationData=000000  50APPROVAL^\\CPSData=E464157520235320Z74PB 0^\\merchantDefinedData=005864^\\, approvedAmount=125, passThru=false, instance=4, authActionBitmap=null, authAuthorityTranCd=01106B, globalEventCd=A:EV219111:1402046808, authAuthorityRefCd=415714112342, authAuthorityTs=1402064823000, paymentType=C, requestedAmount=125.0000, sentToDevice=true, authTime=1402064822735, authAmount=500, authHoldUsed=true, globalAccountId=1002140204, authResultCd=Y, authActionId=null, consumerAcctId=null, posPtaId=15738061, authorityRespCd=00, entryMethod=SWIPE, authorityRespDesc=APPROVAL, globalSessionCode=A:9385@usanet32.trooper.usatech.com:146715D6B7E:3FD096D09D3F9195}; resultCode=0; resultAttributes={saleAmount=0, authId=1393862108, saleSessionStartTime=null, sessionUpdateNeeded=N, saleGlobalSessionCode=null, tranLineItemCount=null, tranId=2235025391, tranImportNeeded=N, tranStateCd=6, clientPaymentTypeCd=null} [in 60 ms; published 70 ms ago]";
		testRegex(regex, search, null);

	}
	@Test
	public void testCoteUrl() throws Exception {
		String regex = "(?:(/[^/]+)?/-)?/([^./]+)\\.\\w+";
		testRegex(regex, "/-/top_logo.img", null);
		testRegex(regex, "/pepi/-/top_logo.img", null);
		testRegex(regex, "/special/-/nothing.img", null);
		testRegex(regex, "/top_logo.img", null);

	}


	@Test
	public void testEFTImageUrl() throws Exception {
		String baseUrl = "https://usalive.usatech.com/eft_auth.i?bankAcctId=2481855";
		String regex = "[^/]*$";
		String result = RegexUtils.substitute(regex, baseUrl, "", "");
		log.info("Substitute " + regex + " on " + baseUrl + ": " + result);
	}

	@Test
	public void testFtab() throws Exception {
		String regex = "(\\S+\\s+/opt/USAT\\s+\\S+\\s+(?!(\\S+,)*acl(,\\S+)*)\\S+(,\\S+)*)(\\s+)";
		testFind(regex, "/dev/mapper/appvg-usatvol  /opt/USAT            ext3    defaults,nodev        1 2", null);
		testFind(regex, "/dev/mapper/appvg-usatvol  /opt/USAT            ext3    defaults,nodev,acl        1 2", null);

	}

	@Test
	public void testReportUrl() throws Exception {
		String regex = "(?:./)?(?:(?:activity|activity_summary|activity_graph|activity_ext|activity_detail|activity_detail_ext|diagnostic|dex_status)\\.i(\\?.*)?|(?:run_report|run_report_async|select_date_range_frame|transaction_totals)\\.i(\\?)(?:.+&)?(?:folioId|reportId)=\\d+.*)";
		// String regex = "(?:run_report|run_report_async|select_date_range_frame|transaction_totals)\\.i(\\?)(?:folioId|reportId)=\\d+.*";

		testRegex(regex, "run_report_async.i?folioId=1221&reportTitle=Prepaid+Balance+by+Customer&profileId=7", null);

	}

	@Test
	public void testEscape() throws Exception {
		for(String s : new String[] { "abc!def", "@#$(*$&(@$)", "12345", "~nnm#890*" }) {
			log.info("String '" + s + "' becomes '" + RegexUtils.escape(s) + "'");
		}
	}

	@Test
	public void testTrigger() throws Exception {
		String regex = "(^|.*&)hover($|&.*)";

		testRegex(regex, "hover", null);
		testRegex(regex, "hover&click", null);
		testRegex(regex, "click&hover", null);
		testRegex(regex, "click", null);
	}

	@Test
	public void testDimensions() throws Exception {
		String regex = "\\{\\s*(-?\\d+(?:\\.\\d+)?)\\s*,\\s*(-?\\d+(?:\\.\\d+)?)\\s*\\}";

		testRegex(regex, "{-9,-8}", null);
		testRegex(regex, "{9,8}", null);
		testRegex(regex, "{9.02,-4.32}", null);
		testRegex(regex, "{  -10 , 5.44 }", null);
	}

	@Test
	public void testDirectXHTMLChartPattern() throws Exception {
		String configEntryRE = "(?:(?:(?:\\s*\"[^\"]*(?:\"{2}[^\"]*)*\"\\s*)|(?:\\s*\\'[^\\']*(?:\\'{2}[^\\']*)*\\'\\s*)|[^\\=\\;]*)=(?:(?:\\s*\"[^\"]*(?:\"{2}[^\"]*)*\"\\s*)|(?:\\s*\\'[^\\']*(?:\\'{2}[^\\']*)*\\'\\s*)|[^\\=\\;]*)(?:$|\\;))*";
		String chartTrueRE = "(?:(?:(?:\\s*\"chart\"\\s*)|(?:\\s*\\'chart\\'\\s*)|chart)=(?:(?:\\s*\"true\"\\s*)|(?:\\s*\\'true\\'\\s*)|true)(?:$|\\;))";
		String chartConfigPattern = "[^:]*:" + configEntryRE + chartTrueRE + configEntryRE;

		testRegex(chartConfigPattern, "com.usatech.report.custom.USALiveXHTMLGenerator:extendedOutputTypes=true", null);
		testRegex(chartConfigPattern, "com.usatech.report.custom.USALiveXHTMLGenerator:extendedOutputTypes=true;chart=true;chart=fae", null);
		testRegex(chartConfigPattern, "com.usatech.report.custom.USALiveXHTMLGenerator:extendedOutputTypes=true;chart=tfrue;chart=true", null);
	}

	@Test
	public void testMobileUserAgentPattern() throws Exception {
		String regex = "\\bmobile(?:\\b|\\W)";
		testFind(regex, "Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10", "i");
	}

	@Test
	public void testChangingDefaultPattern() throws Exception {
		String regex = "[^']*(?:(?:'[^']*){2})*nextval\\s*[(].*";
		testRegex(regex, "NEXTVAL('rdw.item_fact_item_fact_id_seq'::regclass)", "i");
		testRegex(regex, "999", "i");
		testRegex(regex, "'.*'", "i");
		testRegex(regex, "'nextval() is cool!'", "i");
		testRegex(regex, "nextval('rdw.item_fact_item_fact_id_seq'::regclass)", "i");
		testRegex(regex, "'nobo' || nextval('rdw.item_fact_item_fact_id_seq'::regclass)", "i");

	}

	@Test
	public void testNagiosCommandPattern() throws Exception {
		String regex = "^\\[\\d+\\] (PROCESS_HOST_CHECK_RESULT|PROCESS_SERVICE_CHECK_RESULT|UPDATE_HOST_CHECK_CONFIG|UPDATE_SERVICE_CHECK_CONFIG);([ -~]*)$";
		testRegex(regex, "[1366394187] UPDATE_SERVICE_CHECK_CONFIG;name=AppLayerService_InboundFileTransferBlockAck_perf;template=app-monitor-service;host=bkrugi7;desc=InboundFileTransferBlockAck on AppLayerService_2 on bkrugi7;", null);
		testRegex(regex, "[1366394187] PROCESS_SERVICE_CHECK_RESULT;bkrugi7;AppLayerService_InboundFileTransferBlockAck_perf;0;AppLayerService_InboundFileTransferBlockAck_perf is OK at 2013-04-19 13:56:27 EDT|Iterations=0c;~:;~:;0;9223372036854775807 Average_Latency_Time=?ms;~:;~:;0;~\\nStat     Value     Result\\nIterations     0c     UNKNOWN\\nAverage Processing Time          UNKNOWN\\nAverage Latency Time      ms     UNKNOWN\\nAverage Other Time          UNKNOWN\\nPercent Idle          UNKNOWN\\n", null);
	}

	@Test
	public void testMAAIncludePattern() throws Exception {
		String regex = "(?!9998023013405000).*";
		testRegex(regex, "9998023013405000", null);
		testRegex(regex, "9998023013406000", null);
		testRegex(regex, "", null);
		testRegex(regex, "9998023013", null);
		testRegex(regex, "9998023013405000123", null);
		testRegex(regex, "1239998023013405000123", null);
	}

	@Test
	public void testPropertyLinePattern() throws Exception {
		String regex = "^\\s*([^:=]+?)\\s*[:=]\\s*(.*?)\\s*$";
		testRegex(regex, "abc=def", null);
		testRegex(regex, "   abc  =\t def  ", null);
		testRegex(regex, " abc  =  ", null);
		testRegex(regex, "abc = jki \\", null);
		testRegex(regex, "abc:jki\\\\", null);
		testRegex(regex, "abc : = \\ nothing \\\\", null);
		testRegex(regex, "abc = : \\ nothing \\", null);
		testRegex(regex, "abc = : \\ nothing \\\\\\", null);

	}
	@Test
	public void testCommentPattern() throws Exception {
		String regex = "^([#!].*|\\s*)$";
		testRegex(regex, "", null);
		testRegex(regex, "    ", null);
		testRegex(regex, "  owqepr    ", null);
		testRegex(regex, "# somethign ", null);
		testRegex(regex, "#", null);
		testRegex(regex, "!!", null);
		testRegex(regex, "more", null);

	}

	@Test
	public void testHttpdPattern() throws Exception {
		String regex = "^(?:|/dyn)/?$";
		testRegex(regex, "", null);
		testRegex(regex, "/", null);
		testRegex(regex, "/dyn", null);
		testRegex(regex, "/dyn/", null);
		testRegex(regex, "/a/dyn", null);
		testRegex(regex, "/dync", null);

	}

	@Test
	public void testExpDatePattern() throws Exception {
		String regex = "^(\\d{2,4})(\\d{2})$";
		testRegex(regex, "1302", null);
		testRegex(regex, "201302", null);
		testRegex(regex, "10304", null);
		testRegex(regex, "1612", null);
	}

	@Test
	public void tesNagiosPerfPattern() throws Exception {
		// String regex = "^([^=]+)=([\\d\\.\\-]+)([\\w\\/%]*);?([\\d\\.\\-:~@]+)?;?([\\d\\.\\-:~@]+)?;?([\\d\\.\\-]+)?;?([\\d\\.\\-]+)?;?\\s*";
		String regex = "^([^=]+)=([\\d\\.\\-]+)([\\w\\/%]*);?([\\d\\.\\-:~@]+)?;?([\\d\\.\\-:~@]+)?;?([\\d\\.\\-]+)?;?([\\d\\.\\-]+)?;?\\s*";
		testFind(regex, "Iterations=~:;~:;0:9223372036854775807 Average_Processing_Time=~:;~:;0:~ Average_Latency_Time=~:;~:;0:~ Average_Other_Time=200.00000000000003:400;400.00000000000006:~;0:~ Percent_Idle=~:;~:;0:1 ", null);

	}

	@Test
	public void testReplacementPattern() throws Exception {
		String regex = "\\b(layersalerts@usatech.com)\\b";
		testFind(regex, "log4j.appender.email.to = layersalerts@usatech.com\nlog4j.appender.email.from = ${app.servicename}@${app.hostname}.usatech.com\n", null);
		testFind(regex, "log4j.appender.email.to=layersalerts@usatech.com\nlog4j.appender.email.from = ${app.servicename}@${app.hostname}.usatech.com\n", null);
		testFind(regex, "log4j.appender.email.to=layersalerts@usatech.com", null);
		testFind(regex, "log4j.appender.email.to = abcd@usatech.com,layersalerts@usatech.com,gogg@guck.com\nlog4j.appender.email.from = ${app.servicename}@${app.hostname}.usatech.com\n", null);

	}

	@Test
	public void testNagiosPattern() throws Exception {
		String regex = ".*^Nagios Core 3.4.1$.*";
		testRegex(regex, "\nNagios Core 3.4.1\n\nMore stuff here\n done", "sm");
	}

	@Test
	public void testValidCardPattern() throws Exception {
		String regex = "[\\x20-\\x7F]*";
		testRegex(regex, ";6011000995500000=20121015432112345678?", null);
	}

	@Test
	public void testSQLExpPattern() throws Exception {
		String regex = "\\s*(?:('[^']*')|((?:[A-Za-z]\\w*\\.)*([A-Za-z]\\w*))\\s*(\\()(?!\\s*\\+\\s*\\))|((?:[A-Za-z]\\w*\\.)+([A-Za-z]\\w*))(?:\\s*\\Q(+)\\E)?(?!\\()|(\\:[.\\w]+(\\:\\:\\w+(?:\\(\\s*\\d+(?:,\\s*\\d+)?\\s*\\))?(?:\\:\\w+(?:\\(\\s*\\d+(?:,\\s*\\d+)?\\s*\\)?))?)?)|(\\()|(\\)))";
		testFindAll(regex, "REPORT.EPORT.EPORT_SERIAL_NUM", null);
		testFindAll(regex, "DECODE(:param1, 'A', 'B', '1', '2', 'h')", null);
		testFindAll(regex, "CASE WHEN REPORT.EPORT.EPORT_SERIAL_NUM IS NULL THEN :abc::VARCHAR(24) WHEN NICE_FUNC(REPORT.TERMINAL.TERMINAL_ID, 123) = 'G' THEN SUM(REPORT.TRANS.CLOSE_DATE) ELSE NULL END", null);
		testFindAll(regex, "SUM(COUNT(AVERAGE(G4OP.DEX_FILE.DEX_DATE))) + 123 + :9 + REPORT.EPORT.LAST_DEX_DATE", null);
		testFindAll(regex, "SUM(:keals::ARRAY:NUMBER(12,9)) + SUM(:seals::ARRAY:NUMBER(8))", null);
		testFindAll(regex, "DEVICE.PKG_DEVICE_CONFIGURATION.CHANGE_ME(:l_n)/12", null);
		testFindAll(regex, "((:a + 9) / 4) * 100.89", null);

	}

	@Test
	public void testChartTruePattern() throws Exception {
		String configEntryRE = "(?:(?:(?:\\s*\"[^\"]*(?:\"{2}[^\"]*)*\"\\s*)|(?:\\s*\\'[^\\']*(?:\\'{2}[^\\']*)*\\'\\s*)|[^\\=\\;]*)=(?:(?:\\s*\"[^\"]*(?:\"{2}[^\"]*)*\"\\s*)|(?:\\s*\\'[^\\']*(?:\\'{2}[^\\']*)*\\'\\s*)|[^\\=\\;]*)(?:$|\\;))*";
		String chartTrueRE = "(?:(?:(?:\\s*\"chart\"\\s*)|(?:\\s*\\'chart\\'\\s*)|chart)=(?:(?:\\s*\"true\"\\s*)|(?:\\s*\\'true\\'\\s*)|true)(?:$|\\;))*";
		String regex = "[^:]*:" + configEntryRE + chartTrueRE + configEntryRE;
		testRegex(regex, "multipart/related;junk=\"nothing\";   type=\"text/plain\"; more=\"nothing\"", "s");
		testRegex(regex, "direct:com.usatech.report.custom.USALiveXHTMLGenerator:chart=true", "s");
		testRegex(regex, "direct:com.usatech.report.custom.USALiveXHTMLGenerator:'chart' = 'true' ;nothing=something;go away=fierce", "s");
		testRegex(regex, "direct:com.usatech.report.custom.USALiveXHTMLGenerator:move=\"groove\";chart=true;", "s");
		testRegex(regex, "direct:com.usatech.report.custom.USALiveXHTMLGenerator:chart=\"true\"", "s");
	}
	@Test
	public void testContentTypeParsePattern() throws Exception {
		// String regex = "multipart/[^;]+(?:;\\s*\\w+=\"[^\"]*\")*;?";
		// String regex = "multipart/[^;]+(?:;\\s*\\w+=\"[^\"]*\")*(?:;\\s*type=(\"[^\"]*\"))(?:;\\s*\\w+=\"[^\"]*\")*;?";
		/*String regex = "multipart/[^;]+(?:;\\s*\\w+=(?:\"[^\"]*\"|'[^']*'))*(?:;\\s*type=(\"[^\"]*\"|'[^']*'))(?:;\\s*\\w+=(?:\"[^\"]*\"|'[^']*'))*;?";
		testRegex(regex, "multipart/related; type=\"text/html\";", "s");
		testRegex(regex, "multipart/related;\ntype='text/html';", "s");
		testRegex(regex, "multipart/related;junk='nothing';   type=\"text/plain\";", "s");
		testRegex(regex, "multipart/related;junk=\"nothing\";   type=\"text/plain\"; more=\"nothing\"", "s");*/
		String regex = "([^;/]*)/([^;/]*)(?:[;/](.*))?";
		testRegex(regex, "text/html", "s");
		testRegex(regex, "multipart/related;\ntype='text/html';", "s");
		testRegex(regex, "text/csv;charset=US-ASCII", "s");
		testRegex(regex, "/", "s");
	}

	@Test
	public void testReplace() throws Exception {
		String[] tests = new String[] { "&#!\\/;one%~'", "two" };
		for(String test : tests) {
			System.out.println("'" + test + "' = '" + test.replaceAll("([()&\\\\])", "\\\\$1") + "'");
		}

	}

	@Test
	public void testUpdateMemPattern() throws Exception {
		String regex = "\\B-Xmx\\d+[A-Za-z]?\\b";
		// String regex = "\\b-Xmx\\d+[A-Za-z]?\\b";
		// String regex = "[ \"]-Xmx\\d+[A-Za-z]?[ \"]";
		String[] values = new String[] {
				"JAVA_OPTS=\"-Dsun.net.inetaddr.ttl=86400 -Xmx512m -XX:MaxPermSize=128M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=logs/java_pid%p.hprof -Djava.util.logging.config.file=/opt/USAT/conf/logging.properties\"",
				"JAVA_OPTS=\"-Xmx512m -XX:MaxPermSize=128M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=logs/java_pid%p.hprof -Djava.util.logging.config.file=/opt/USAT/conf/logging.properties\"",
				"JAVA_OPTS=\"-Dsun.net.inetaddr.ttl=86400 -Xmx512m\"",
		};
		for(String value : values) {
		testFind(regex, value, null);
		System.out.println("Replaced = '" + value.replaceAll(regex, "-Xmx3272M") + "'");
		}
	}

	@Test
	public void testHttpdVersionPattern() throws Exception {
		String regex = "^Server version: Apache/2\\.2\\.22 .*";
		testRegex(regex, "Server version: Apache/2.2.22 (Unix)\r\nServer built:   Mar 29 2012 10:25:58", "s");
		testRegex(regex, "Server version: Apache/2.2.21 (Unix)\nServer built:   Mar 29 2012 10:25:58", "s");

	}
	@Test
	public void testStandardTextEditorPattern() throws Exception {
		String regex = "(?:(\\d+)?\\s*(?:[\\x2D]|to)\\s*(\\d+)?)?(?:[;](.*))?";
		testRegex(regex, "0-", null);
	}

	@Test
	public void testServleConfigPattern() throws Exception {
		String regex = "(servlet|filter)\\(([^)]+)\\)\\..+";
		testRegex(regex, "servlet(HybridServlet).something", null);
		testRegex(regex, "servlet(HybridServlet)", null);
		testRegex(regex, "servlet(HybridServlet).", null);
		testRegex(regex, "servlet(HybridServlet).something(hallaballlo).forth", null);
		testRegex(regex, "filter(NiceFilter).nothing", null);

	}

	@Test
	public void testBaseUrlPattern() throws Exception {
		String regex = "(.*)/[^/]+\\.[^/]+";
		testFind(regex, "abc.cvs", null);
		testFind(regex, "abc.cvs?reportTitle=nothing&referrer=activity_parameters.i&beginDate=never", null);
		testFind(regex, "file:../usalive/web/css/usalive-app-style.css?t=1248329483", null);
		testFind(regex, "file:../usalive/web/css/usalive-app-style.css?", null);
		testFind(regex, "file:../usalive/web/css/usalive-app-style.css", null);
		testFind(regex, "../usalive/web/css/usalive-app-style.css?t=1248329483", null);
		testFind(regex, "../usalive/web/css/usalive-app-style.css?", null);
		testFind(regex, "../usalive/web/css/usalive-app-style.css", null);
		testFind(regex, "file:http://../usalive/web/css/usalive-app-style.css?hello=no?good=bybye", null);
	}
	@Test
	public void testPathStripperPattern() throws Exception {
		String regex = "(\\w+:)?([^?]+)([?].*)?";
		testFind(regex, "abc.cvs", null);
		testFind(regex, "abc.cvs?reportTitle=nothing&referrer=activity_parameters.i&beginDate=never", null);
		testFind(regex, "file:../usalive/web/css/usalive-app-style.css?t=1248329483", null);
		testFind(regex, "file:../usalive/web/css/usalive-app-style.css?", null);
		testFind(regex, "file:../usalive/web/css/usalive-app-style.css", null);
		testFind(regex, "../usalive/web/css/usalive-app-style.css?t=1248329483", null);
		testFind(regex, "../usalive/web/css/usalive-app-style.css?", null);
		testFind(regex, "../usalive/web/css/usalive-app-style.css", null);
		testFind(regex, "file:http://../usalive/web/css/usalive-app-style.css?hello=no?good=bybye", null);
	}

	@Test
	public void testReferrerExtractionPattern() throws Exception {
		String regex = ".+[?](?:(.+)&)?referrer=([^&]+)(?:&(.+))?";
		testFind(regex, "", null);
		testFind(regex, "run_report_async.i?reportTitle=nothing&referrer=activity_parameters.i&beginDate=never", null);
		testFind(regex, "run_report_async.i?reportTitle=nothing&referrer=activity_parameters.i&beginDate=never&help=no&fragment=true&everyone=yes", null);
		testFind(regex, "run_report_async.i?reportTitle=nothing&referrer=activity_parameters.i", null);
		testFind(regex, "run_report_async.i?referrer=activity_parameters.i&beginDate=never&help=no&fragment=true&everyone=yes", null);
		testFind(regex, "run_report_async.i?reportTitle=nothing&beginDate=never&help=no&fragment=true&everyone=yes", null);
	}

	@Test
	public void testLayoutPattern() throws Exception {
		String regex = "(^|[&])(unframed|fragment)=";
		testFind(regex, "", null);
		testFind(regex, "unframed=false&good=true", null);
		testFind(regex, "help=no&fragment=true&everyone=yes", null);
		testFind(regex, "fragment", null);
		testFind(regex, "&yesfragment=something", null);
	}

	@Test
	public void testHREFPattern() throws Exception {
		String regex = "(?:([^:]+):)?([^?#]*)(?:(\\?)([^#]*))?(?:(\\#.*))?";
		testRegex(regex, "./preferences.i", null);
	}

	@Test
	public void testInternalIPPattern() throws Exception {
		String regex = "(192\\.168\\.\\d{1,3}|10\\.0\\.0)\\.\\d{1,3}|127\\.0\\.0\\.1";
		// "10\\.0\\.0\\.\\d+|192\\.168\\.\\d+\\.\\d+|127\\.0\\.0\\.1"
		testRegex(regex, "127.0.0.1", null);
		testRegex(regex, "10.0.0.147", null);
		testRegex(regex, "10.0.4.147", null);
		testRegex(regex, "192.168.5.6", null);
		testRegex(regex, "192.168.100.219", null);
		testRegex(regex, "192.0.0.1", null);
		testRegex(regex, "192.168.10.0.0.3", null);
		testRegex(regex, "10.0.0.127.0.0.1", null);
	}

	@Test
	public void testFillCardPattern() throws Exception {
		String card = "639621150000000024=123456789";
		String cardMatch = "6396211A";
		String cardRegex = "^(;?)6396211([0-9]{1})([0-9]{9})([0-9]{1})=([0-9]{4})([0-9]{5})(\\??)$";
		if(cardMaskMatch(cardMatch, card))
			System.out.println("Card '" + card + "' does match '" + cardMatch + "'");
		else
			System.out.println("Card '" + card + "' does NOT match '" + cardMatch + "'");
		testRegex(cardRegex, card, null);
		
	}
	protected boolean cardMaskMatch(String localServiceCardMask, String card) {
		for(int i = 0; i < localServiceCardMask.length(); i++) {
			char ch = localServiceCardMask.charAt(i);
			switch(ch) {
				case 'A': case 'a':
					break;
				case 'B': case 'b':
					return false;
				case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
					if(card.charAt(i) != ch)
						return false;
					break;
				default:
					log.warn("Invalid character '" + ch + "' in local service card mask");
					return false;
			}
		}
		return true;
	}
	@Test
	public void testDMSProxyPattern() throws Exception {
		String regex = "^/?|/.+\\.(?:i|i\\?.*|jsp|jsp\\?.*)$";
		testRegex(regex, "/js/esudsPrivileges.js?t=2", null);
		testRegex(regex, "/js/esudsPrivileges.jsp?t=2", null);
		testRegex(regex, "/nada.i?t=2", null);
		testRegex(regex, "/nada.i?t=2#go", null);
	}

	@Test
	public void testMatchPattern() throws Exception {
		// String regex = "(.[\\n\\r]+)*total 0[.\\n\\r]*";
		String regex = ".*^total 0$.*";
		testRegex(regex, "/opt/USAT/postgres/tblspace/file_repo_data:\r\ntotal 0\r\n\r\n/opt/USAT/postgres/tblspace/mq_data:\r\ntotal 0", "ms");
	}

	@Test
	public void testCronPattern() throws Exception {
		// String regex = "((?:\\*|(?:\\d+(-\\d+)?(?:,\\d+(-\\d+)?)*)\\s+){5})(\\S.*)";
		// String regex = "((?:(?:\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+){5})(\\S.*)";
		String regex = "(\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec|\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(sun|mon|tue|wed|thu|fri|sat|\\*(?:/\\d+)?|\\d+(?:-\\d+(?:/\\d+)?)?(?:,\\d+(?:-\\d+)?)*)\\s+(\\S.*)";

		String[] tests = new String[] {
				"5 0 * * *       $HOME/bin/daily.job >> $HOME/tmp/out 2>&1",
				"15 14 1 * *     $HOME/bin/monthly",
				"0 22 * * 1-5    mail -s \"It�s 10pm\" joe%Joe,%%Where are your kids?%",
				"23 0-23/2 * * * echo \"run 23 minutes after midn, 2am, 4am ..., everyday\"",
				"5 4 * * sun     echo \"run at 5 after 4 every sunday\"",
				"3 4 * * * /opt/USAT/postgres/latest/share/delete_expired_resources.sh > /opt/USAT/postgres/latest/data/logs/delete_expired_resources.log 2>&1",
				"9 * * * * /opt/USAT/postgres/archivelogs.sh >> /opt/USAT/postgres/archivelogs.log 2>&1",
				"0,5,10,15,20,25,30,35,40,45,50,55 * * * * /opt/USAT/postgres/latest/share/cleanup_dead_connections.sh >> /opt/USAT/postgres/latest/data/logs/cleanup_dead_connections.log 2>&1",
				"3 4 * * * /opt/USAT/postgres/latest/share/delete_expired_resources.sh > /opt/USAT/postgres/latest/data/logs/delete_expired_resources.log 2>&1",
				"9 * * * * /opt/USAT/postgres/archivelogs.sh >> /opt/USAT/postgres/archivelogs.log 2>&1",
				"0,5,10,15,20,25,30,35,40,45,50,55 * * * * /opt/USAT/postgres/latest/share/cleanup_dead_connections.sh >> /opt/USAT/postgres/latest/data/logs/cleanup_dead_connections.log 2>&1",
		};
		for(String t : tests)
			testRegex(regex, t, null);
	}

	@Test
	public void testIPTablesPattern() throws Exception {
		String regex = "(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)(?:\\s+tcp(?:(?:\\s+\\S+)*\\s+spt:(\\d+))?(?:(?:\\s+\\S+)*\\s+dpt:(\\d+))?)?(?:\\s+(.+))??(?:\\s+state\\s+(\\S+))?\\s*";
		// String regex = "(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)(?:\\s+tcp(?:(?:\\s+\\S+)*\\s+spt:(\\d+))?(?:(?:\\s+\\S+)*\\s+dpt:(\\d+))?(?:\\s+\\S+)*?)?(?:\\s+state\\s+(\\S+))?\\s*";
		String[] tests = new String[] {
				"ACCEPT     udp  --  0.0.0.0/0            10.0.0.9            udp dpt:53 ",
				"ACCEPT     tcp  --  0.0.0.0/0            10.0.0.9            tcp dpt:53",
				"ACCEPT     udp  --  0.0.0.0/0            10.0.0.10           udp dpt:53 ",
				"ACCEPT     tcp  --  0.0.0.0/0            10.0.0.10           tcp dpt:53 ",
				"ACCEPT     tcp  --  0.0.0.0/0            10.0.0.0/24         tcp spt:22 dpts:1024:65535 state ESTABLISHED ",
				"ACCEPT     tcp  --  0.0.0.0/0            10.0.0.0/24         tcp dpt:22 state NEW,ESTABLISHED",
				"ACCEPT     udp  --  0.0.0.0/0            10.0.0.7            udp dpt:123",
				"ACCEPT     tcp  --  0.0.0.0/0            10.0.0.113          tcp spts:1024:65535 dpt:25",
				"ACCEPT     tcp  --  0.0.0.0/0            10.0.0.63           tcp dpts:389:636",
				"DROP       tcp  --  0.0.0.0/0            0.0.0.0/0           tcp flags:0x3F/0x3F",
				"DROP       tcp  --  0.0.0.0/0            0.0.0.0/0           tcp flags:0x3F/0x00",
				"ACCEPT     tcp  --  0.0.0.0/0            0.0.0.0/0           state ESTABLISHED",
				"ACCEPT     udp  --  0.0.0.0/0            0.0.0.0/0           state ESTABLISHED",
				"REDIRECT   tcp  --  0.0.0.0/0            0.0.0.0/0           tcp dpt:80 redir ports 8680"
		};
		for(String t : tests)
			testRegex(regex, t, null);
	}
	
	@Test
	public void testPromptPattern() throws Exception {
		String regex = "(?:[\\w.-]*|\\[\\w+@\\w+ [^\\]]+\\])[$#] ";
		testRegex(regex, "-bash-3.00$ ", null);
		testRegex(regex, "# ", null);
		testRegex(regex, "[root@devapr11 bkrug]# ", null);
		testRegex(regex, "[bkrug@devapr11 ~]$ ", null);
		testRegex(regex, "abc@.com", null);
	}

	@Test
	public void testEmailPattern() throws Exception {
		// String regex =
		// "^(?:s*w+[w.-]*@w+[w.-]*?.[A-Za-z]{2,3}s*(?:[,;]|$))+$";
		String regex = "^(?:\\s*\\w+[\\w.-]*@\\w+[\\w.-]*?\\.[A-Za-z]{2,3}\\s*(?:[,;]|$))+$";
		// String regex = "\\w+[\\w.-]*@\\w+[\\w-]*\\.[A-Za-z]{2,3}";
		testRegex(regex, "abc@def.com", null);
		testRegex(regex, "abc", null);
		testRegex(regex, "abc@dm", null);
		testRegex(regex, "abc@", null);
		testRegex(regex, "abc@.com", null);
		testRegex(regex, "dkouznetsov@usatech.com", null);
	}

	@Test
	public void testIPFPattern() throws Exception {
		//String regex = "(pass|block)\\s+(in|out)(?:\\s+(log))?(?:\\s+(quick))?(?:\\s+on\\s+(\\w+))?(?:\\s+proto\\s+(tcp|upd|icmp))?(?:\\s+from\\s+(\\S+)\\s+to\\s+(\\S+))?(?:\\s+port\\s*=\\s*(\\d+))?(?:\\s+flags\\s+(\\S+)|(?:\\s+keep\\s+(frags))|(?:\\s+keep\\s+(state)))*\\s*";
		String regex = "(pass|block)\\s+(in|out)\\s+proto\\s+(tcp|upd|icmp).*";
		String test = "block in proto udp from any to any";
		testRegex(regex, test, null);
	}
	
	@Test
	public void testZipPattern() throws Exception {
		String regex = "\\d{5}(-(\\d{4})?)?";
		testRegex(regex, "89808", null);
		testRegex(regex, "12345-", null);
		testRegex(regex, "12345-6789", null);
	}

	@Test
	public void testPhonePattern() throws Exception {
		String regex = "\\s*\\(?\\s*(\\d{3})\\s*\\)?\\s*[-.]?\\s*(\\d{3})\\s*[-.]?\\s*(\\d{4})\\s*";
		testRegex(regex, "6104368832", null);
		testRegex(regex, " (610) 436-8832", null);
		testRegex(regex, "610   436 8832", null);
		testRegex(regex, "  610.436.8832", null);
		testRegex(regex, "610-436-8832", null);
		testRegex(regex, "610 - 436 - 8832 ", null);
	}
	
	@Test
	public void testQueueNamePattern() throws Exception {
		//String regex = "usat\\.authority\\.realtime\\..+";
		String regex = "usat\\.authority\\.(?!realtime\\.).+";
		testRegex(regex, "usat.authority.realtime.blackboard", null);
		testRegex(regex, "usat.authority.blackboard", null);
		testRegex(regex, "usat.authority.realtime_blackboard", null);
	}
	@Test
	public void testDiffEntryPattern() throws Exception {
		String regex = "\\+{3} (\\S+)\t.*";
		testRegex(regex, "--- org/postgresql/core/v2/ConnectionFactoryImpl.java	Mon Jan 19 17:26:36 1970", null);
		testRegex(regex, "+++ org/postgresql/core/v2/ConnectionFactoryImpl.java	Mon Jan 19 17:26:36 1970", null);
		testRegex(regex, "@@ -40,7 +40,7 @@", null);
		testRegex(regex, "     private static final int AUTH_REQ_MD5 = 5;", null);
		testRegex(regex, "     private static final int AUTH_REQ_SCM = 6;", null);
		testRegex(regex, "-    public ProtocolConnection openConnectionImpl(String host, int port, String user, String database, Properties info, Logger logger) throws SQLException {", null);
	}
	
	@Test
	public void testMultiMatchPattern() throws Exception {
		String regex = "\\s*((?:\\S+)\\s+)+(\\S+)?";
		testRegex(regex, "--- org/postgresql/core/v2/ConnectionFactoryImpl.java	Mon Jan 19 17:26:36 1970", null);
		testRegex(regex, "+++ org/postgresql/core/v2/ConnectionFactoryImpl.java	Mon Jan 19 17:26:36 1970", null);
		testRegex(regex, "@@ -40,7 +40,7 @@", null);
		testRegex(regex, "     private static final int AUTH_REQ_MD5 = 5;", null);
		testRegex(regex, "     private static final int AUTH_REQ_SCM = 6;", null);
		testRegex(regex, "-    public ProtocolConnection openConnectionImpl(String host, int port, String user, String database, Properties info, Logger logger) throws SQLException {", null);
	}
	
	@Test
	public void testNLProcessorPattern() throws Exception {
		String regex = "(com\\.usatech\\.networklayer\\.processors\\.\\w+(?:\\.\\w+)*?)\\.queueKey";
		testRegex(regex, "com.usatech.networklayer.processors.OFFLINE.2F.queueKey", null);
		testRegex(regex, "com.usatech.networklayer.processors.USR.queueKey", null);
		testRegex(regex, ";\\w*", null);
	}

	@Test
	public void testTextParamEditorPattern() throws Exception {
		String regex = "(?:(\\d+)?\\s*(?:[\\x2D]|to)\\s*(\\d+)?)?(?:[;](.*))?";
		testRegex(regex, "0 to 16;\\w*", null);
		testRegex(regex, "0-16;\\w*", null);
		testRegex(regex, ";\\w*", null);
	}
	@Test
	public void testCalendarStringPattern() throws Exception {
		String regex = "(^|,)id=\\\"([^\"]+)\\\".*";
		//String regex = "((?:[A-Za-z$_][\\w$]*\\.)*[A-Za-z$_][\\w$]*Calendar)\\[time=(\\?|\\d+),areFieldsSet=(false|true),areAllFieldsSet=(false|true),lenient=(false|true)," +
		//	"zone=((?:[A-Za-z$_][\\w$]*\\.)*[A-Za-z$_][\\w$]*)\\[(.*)\\],firstDayOfWeek=(\\d+),minimalDaysInFirstWeek=(\\d+)(,.*)\\]";
//		"zone=((?:[A-Za-z$_][\\w$]*\\.)*[A-Za-z$_][\\w$]*)\\[(.*)\\],firstDayOfWeek=(\\d+),minimalDaysInFirstWeek=(\\d+)((?:,\\w+=(?:\\?|\\d+))+)\\]";

		//String regex = "((?:[A-Za-z$_][\\w$]*\\.)*[A-Za-z$_][\\w$]*Calendar)\\[time=(\\?|\\d+),areFieldsSet=(false|true),areAllFieldsSet=(false|true),lenient=(false|true),.*";

		//testRegex(regex, "java.util.GregorianCalendar[time=1241640003000,areFieldsSet=true,areAllFieldsSet=true,lenient=true,zone=sun.util.calendar.ZoneInfo[id=\"America/New_York\",offset=-18000000,dstSavings=3600000,useDaylight=true,transitions=235,lastRule=java.util.SimpleTimeZone[id=America/New_York,offset=-18000000,dstSavings=3600000,useDaylight=true,startYear=0,startMode=3,startMonth=2,startDay=8,startDayOfWeek=1,startTime=7200000,startTimeMode=0,endMode=3,endMonth=10,endDay=1,endDayOfWeek=1,endTime=7200000,endTimeMode=0]],firstDayOfWeek=1,minimalDaysInFirstWeek=1,ERA=1,YEAR=2009,MONTH=4,WEEK_OF_YEAR=19,WEEK_OF_MONTH=2,DAY_OF_MONTH=6,DAY_OF_YEAR=126,DAY_OF_WEEK=4,DAY_OF_WEEK_IN_MONTH=1,AM_PM=1,HOUR=4,HOUR_OF_DAY=16,MINUTE=0,SECOND=3,MILLISECOND=0,ZONE_OFFSET=-18000000,DST_OFFSET=3600000]", null);
		//testRegex(regex, Calendar.getInstance().toString(), null);
		testRegex(regex, "id=\"America/New York\",offset=123", null);
		//testRegex(regex, "(char)", null);
	}
	@Test
	public void testEmailListPattern() throws Exception {
		String regex = "^(?:\\s*\\w+[\\w\\.-]*\\@\\w+[\\w\\.-]*?\\.[A-Za-z]{2,3}\\s*(?:[,;]|$))+";
		//String regex = "^(?:\\s*$)|(?:\\s*\\w+[\\w\\.-]*\\@\\w+[\\w\\.-]*?\\.[A-Za-z]{2,3}\\s*(?:[,;]|$))+";
		testRegex(regex, "", null);
		testRegex(regex, "bkrug@usatech.com", null);
		testRegex(regex, " 8-lasdfjlasdkf@yo.com;-op@u.com ", null);
		testRegex(regex, " bkrug-q@usatech.com ", null);
		testRegex(regex, "bkrug-q@usatech", null);
		testRegex(regex, "bkrug.q@usatech.co.uk", null);
		testRegex(regex, "bkrug.q@usa-tech.co.uk ,bkrug-q@usatech.com, bkrug@usa-tech.com  ", null);
		
	}
	
	@Test
	public void testUserAgentPattern() throws Exception {
		//String regex = "(([(](byte)|(short)|(char)|(long)[)])?0x\\d{2,8})";
		//String regex = "[(]((byte)|(short)|(char)|(long))[)]";
		//String regex = "(?:Mozilla/.*(?!MS(?:P?)IE))|(?:Opera/).*";
		String regex = "(?:Mozilla/(?:.(?!MSP?IE\\s+[0-8]\\.))*?)|(?:Opera/.*)";
		//String regex = "(([A-Za-z_][A-Za-z0-9_]*)|(\\d+L?)|(((\\(byte|short|char|long\\))?0x\\d{2,8}))|(\\'(.|\\\\.)\\'))([,;]([A-Za-z_][A-Za-z0-9_]*)|\\d+L?|((\\(byte|short|char|long\\))?0x\\d{2,8})|\\'(.|\\\\.)\\')*";
		//String regex = "(([A-Za-z_][A-Za-z0-9_]*)|\\d+L?)([,;](([A-Za-z_][A-Za-z0-9_]*)|\\d+L?))*";
		String osregex = "\\((Macintosh|iPhone|iPad|iPod); ";
		RowValuesIterator rvIter = SheetUtils.getExcelRowValuesIterator(new FileInputStream("C:\\Documents and Settings\\bkrug\\My Documents\\usalive-user-agents_20120328.xls"));
		PrintWriter out = new PrintWriter("C:\\Documents and Settings\\bkrug\\My Documents\\User-Agents-Results-2.csv");
		String[] columnNames = rvIter.getColumnNames();
		Object[] data = new Object[columnNames.length + 2];
		System.arraycopy(columnNames, 0, data, 0, columnNames.length);
		data[columnNames.length] = "SupportsDataUri".toUpperCase();
		data[columnNames.length + 1] = "AvoidExcel".toUpperCase();
		StringUtils.writeCSVLine(data, out);
		while(rvIter.hasNext()) {
			Map<String,Object> values = rvIter.next();
			String ua = (String) values.get("HTTP_USER_AGENT");
			boolean supportsDataUri = ua != null && Pattern.matches(regex, ua);
			boolean avoidExcel = ua != null && Pattern.compile(osregex).matcher(ua).find();
			for(int i = 0; i < columnNames.length; i++)
				data[i] = values.get(columnNames[i]);
			data[columnNames.length] = supportsDataUri;
			data[columnNames.length + 1] = avoidExcel;
			StringUtils.writeCSVLine(data, out);
		}
		out.flush();
		out.close();
		/*
		testRegex(regex, "Mozilla/3.0 (compatible; Opera/3.0; Windows 3.1) v3.1", null);
		testRegex(regex, "Mozilla/2.0 (compatible; MSIE 3.0B; Win32)", null);
		testRegex(regex, "Opera/6.x (Windows NT 4.0; U) [de]", null);
		testRegex(regex, "Mozilla/1.1 (compatible; MSPIE 2.0; Windows CE)", null);
		//testRegex(regex, "(char)0x45", null);
		//testRegex(regex, "(char)", null);
		 */
	}
	@Test
	public void testClassListPattern() throws Exception {
		//String regex = "(([(](byte)|(short)|(char)|(long)[)])?0x\\d{2,8})";
		//String regex = "[(]((byte)|(short)|(char)|(long))[)]";
		String regex = "(([A-Za-z_][A-Za-z0-9_]*\\.)*[A-Za-z_][A-Za-z0-9_]*[:,;| ])*([A-Za-z_][A-Za-z0-9_]*\\.)*[A-Za-z_][A-Za-z0-9_]*";
		//String regex = "(([A-Za-z_][A-Za-z0-9_]*)|(\\d+L?)|(((\\(byte|short|char|long\\))?0x\\d{2,8}))|(\\'(.|\\\\.)\\'))([,;]([A-Za-z_][A-Za-z0-9_]*)|\\d+L?|((\\(byte|short|char|long\\))?0x\\d{2,8})|\\'(.|\\\\.)\\')*";
		//String regex = "(([A-Za-z_][A-Za-z0-9_]*)|\\d+L?)([,;](([A-Za-z_][A-Za-z0-9_]*)|\\d+L?))*";
		testRegex(regex, "com.usatech.test.Test,simple.util.List", null);
		//testRegex(regex, "(char)0x45", null);
		//testRegex(regex, "(char)", null);
	}
	@Test
	public void testCaseListPattern() throws Exception {
		//String regex = "(([(](byte)|(short)|(char)|(long)[)])?0x\\d{2,8})";
		//String regex = "[(]((byte)|(short)|(char)|(long))[)]";
		String regex = "(([A-Za-z_][A-Za-z0-9_]*)|\\d+L?|((\\((byte|short|char|long)\\))?0x\\d{2,8})|\\'(.|\\\\.)\\')([,;](([A-Za-z_][A-Za-z0-9_]*)|\\d+L?|((\\((byte|short|char|long)\\))?0x\\d{2,8})|\\'(.|\\\\.)\\'))*";
		//String regex = "(([A-Za-z_][A-Za-z0-9_]*)|(\\d+L?)|(((\\(byte|short|char|long\\))?0x\\d{2,8}))|(\\'(.|\\\\.)\\'))([,;]([A-Za-z_][A-Za-z0-9_]*)|\\d+L?|((\\(byte|short|char|long\\))?0x\\d{2,8})|\\'(.|\\\\.)\\')*";
		//String regex = "(([A-Za-z_][A-Za-z0-9_]*)|\\d+L?)([,;](([A-Za-z_][A-Za-z0-9_]*)|\\d+L?))*";
		testRegex(regex, "'\\f',123L,ABC,(char)0x45,asd,'d'", null);
		//testRegex(regex, "(char)0x45", null);
		//testRegex(regex, "(char)", null);
	}

	@Test
	public void testRepoKeyPattern() throws Exception {
		String regex = "(?:\\[((?:[\\w.-]*(?:\\:[\\\\/\\w+*.%-]*)?[,;])*[\\w.-]*(?:\\:[\\\\/\\w+*.%-]*)?)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?(?!\\.\\w+$))(?:\\((\\d+)\\))?(\\.\\w+$)?)";
		testRegex(regex, "[devmst01:/opt/USAT/repostrn,devmst04:/opt/USAT/repostrn]20090909/Transaction+Report+-+Batch+%2349949(22363)_ZIP_", null);
		testRegex(regex, "[devmst01:/opt/USAT/repostrn,devmst04:/opt/USAT/repostrn]20090909/Transaction+Report+-+Batch+%2349949(22363)._ZIP_", null);
		}

	@Test
	public void testSftpRepoPattern() throws Exception {
		String regex = "sftp\\:([^:@]+)(?:\\:([^:@]*)(?:\\:([^:@]*)(?:\\:([^@]+))?)?)?@([^:@]+)\\:(.+)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?(?!\\.\\w+$))(?:\\((\\d+)\\))?(\\.\\w+$)?)";
		testRegex(regex, "sftp:bkrug@usadev01:/home/bkrug/repo1", null);
		testRegex(regex, "sftp:bkrug:secret@usadev01:/home/bkrug/repo1", null);
		testRegex(regex, "sftp:bkrug:secret@usaapd1.usatech.com:/home/bkrug/file_repo/layers", null);
		testRegex(regex, "sftp:bkrug:secret:something:/home/bkrug/pk.rsa@usadev01:/home/bkrug/repo1", null);
		testRegex(regex, "sftp:bkrug:secret:something:E:\\TEMP\\pk.rsa@usadev01:/", null);
	}

	@Test
	public void testExtractCardNumPattern() throws Exception {
		String regex = "[^\\d]*(\\d{2})(\\d{7,}?)(\\d{4})(?:[^\\d].*)?";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?(?!\\.\\w+$))(?:\\((\\d+)\\))?(\\.\\w+$)?)";
		testRegex(regex, "6010567006507365=00010004000070773779", null);
		testRegex(regex, ";6010567006507365=00010004000070773779?2", null);
		testRegex(regex, "%B6010567006507365^JEFFREY JIM^1410123678?2", null);
		testRegex(regex, "12345", null);
		testRegex(regex, "123456", null);
		testRegex(regex, "1234567=PIN#12093", null);
		testRegex(regex, "123456789012=PIN#12093", null);
		testRegex(regex, "1234567890123=PIN#12093", null);
	}
	@Test
	public void testPropertyListPattern() throws Exception {
		String regex = "((?:\\d+(?:\\-\\d+)?)(?:\\|\\d+(?:\\-\\d+)?)*)(?:\\!|(?:\\=([^\\r\\n]*)))";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?(?!\\.\\w+$))(?:\\((\\d+)\\))?(\\.\\w+$)?)";
		testRegex(regex, "1=First", null);
		testRegex(regex, "1-9|7|123-125=Lots", null);
		testRegex(regex, "2|5|8!", null);
		testRegex(regex, "2-8!", null);
		testRegex(regex, "3-9=", null);
	}
	@Test
	public void testCleanTrackPattern() throws Exception {
		String regex = "(^[%;])|((\\?[\\x01-\\xFF]?)?(\\x00[\\x00-\\xFF]*)?$)";
		testReplace(regex, "545454545?\u00FF", "", null);
		testReplace(regex, ";545454545?", "", null);
		testReplace(regex, ";545454545", "", null);
		testReplace(regex, "545454545", "", null);
		testReplace(regex, "545454545?\u00FF\u0000Ef-\u0000?", "", null);
		testReplace(regex, ";545454545?\u0000Ef-\u0000?", "", null);
		testReplace(regex, ";545454545\u0000Ef-\u0000?", "", null);
		testReplace(regex, "545454545\u0000Ef-\u0000?", "", null);		
		testReplace(regex, "545454545??\u0000", "", null);
		testReplace(regex, ";545454545\u0000?", "", null);
		testReplace(regex, ";545454545\u0000", "", null);
		testReplace(regex, "545454545?\u0000", "", null);	
		testReplace(regex, ";6544000000000000=122700012345?", "", null);	
		testReplace(regex, ";6544000000000000=122700012345?\u0000", "", null);	
		testReplace(regex, ";6544000000000000=122700012345\u0000", "", null);	
		
	}
	@Test
	public void testExtractTrackPattern() throws Exception {
		String regex = "[%;]?([\\x20-\\x3E\\x40-\\x7F]+)(?:\\?[\\x01-\\xFF]?)?(?:\\x00[\\x00-\\xFF]*)?";
		testRegex(regex, "545454545?\u00FF", null);
		testRegex(regex, ";545454545?", null);
		testRegex(regex, ";545454545", null);
		testRegex(regex, "545454545", null);
		testRegex(regex, "545454545?\u00FF\u0000Ef-\u0000?", null);
		testRegex(regex, ";545454545?\u0000Ef-\u0000?", null);
		testRegex(regex, ";545454545\u0000Ef-\u0000?", null);
		testRegex(regex, "545454545\u0000Ef-\u0000?", null);
		testRegex(regex, "545454545??\u0000", null);
		testRegex(regex, ";545454545\u0000?", null);
		testRegex(regex, ";545454545\u0000", null);
		testRegex(regex, "545454545?\u0000", null);
		testRegex(regex, ";6544000000000000=122700012345?", null);
		testRegex(regex, ";6544000000000000=122700012345?\u0000", null);
		testRegex(regex, ";6544000000000000=122700012345\u0000", null);

	}

	@Test
	public void testCardPattern() throws Exception {
		//"370000000008431=00000000000000000000?"
		String regex;
		/*regex = "^;?(5[1-5][0-9]{14})=([0-9]{4})([0-9]{3})([0-9]*)(?:\\?([\\x00-\\xFF])?)?$";
		//String regex = "^;?(4[0-9]{15})=([0-9]{4})([0-9]{3})([0-9]*)\\??([0-9]{0,3})$";
		//String regex = "^;?(4[0-9]{15})=([0-9]{4})([0-9]{3})([0-9]*)(?:\\?([0-9]))?$";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		*/
		regex = "^([0-9]{9})$";
		testRegex(regex, "545454545?\u00FF", null);
		testRegex(regex, ";545454545?", null);
		testRegex(regex, ";545454545", null);
		testRegex(regex, "545454545", null);
		regex = "^;?([0-9]{9})(?:\\?([\\x00-\\xFF])?)?$";
		testRegex(regex, "545454545?\u00FF", null);
		testRegex(regex, ";545454545?", null);
		testRegex(regex, ";545454545", null);
		testRegex(regex, "545454545", null);
		
		/*
		regex = "^%?B(5[1-5][0-9]{14})\\^([A-Za-z0-9 \\/]{2,26})\\^([0-9]{4}?)\\^([0-9]{3}?)([A-Za-z0-9 \\/]*)(?:\\?([\\x00-\\xFF])?)$";
		testRegex(regex, "%B5454545454545454^TEST CARD^1234^567?\u0000", null);
		testRegex(regex, "B5454545454545454^TEST CARD^1234^567?", null);
		//*///'^;?((?:6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(?:\?([\x00-\xFF])?)?$'
		/*
		regex = "^;?((?:6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(?:\\?([\\x00-\\xFF])?)?$";
		testRegex(regex, ";6011000000000000=122700012345?", null);
		testRegex(regex, ";6229000000000000=122700012345?", null);
		testRegex(regex, ";6011230002890012558=0108123456789", null);
		testRegex(regex, ";6544000000000000=122700012345?", null);
		regex = " ^;?(6011|622[0-9]|64[4-9][0-9]|65[0-9]{2}[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(?:\\?([\\x00-\\xFF])?)?$";
		testRegex(regex, ";6011000000000000=122700012345?", null);
		testRegex(regex, ";6229000000000000=122700012345?", null);
		testRegex(regex, "6011230002890012558=0108123456789", null);
		testRegex(regex, ";6544000000000000=122700012345?", null);
		*/
	}
	@Test
	public void testSftpKeyPattern() throws Exception {
		String regex = "(?:\\[((?:[\\w.-]*(?:\\:[\\\\/\\w+*.%-]*)?[,;])*[\\w.-]*(?:\\:[\\\\/\\w+*.%-]*)?)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?(?!\\.\\w+$))(?:\\((\\d+)\\))?(\\.\\w+$)?)";
		testRegex(regex, "[localhost:/C%58/TEMP/file_repo_1;127.0.0.1]/abc/def/xyz.txt", null);
		testRegex(regex, "[localhost;127.0.0.1;usadev01.usatech.com]/abc/def.jkl/xyz.txt", null);
		testRegex(regex, "[localhost,127.0.0.1,www.esuds-tst.net]/abc/def.jkl/xyz.stu.txt", null);
		testRegex(regex, "[localhost,127.0.0.1,usadev01:file_repo_1]abc/def/x.y-z(123).txt", null);
		testRegex(regex, "[localhost,127.0.0.1,usadev01:/home/bkrug/file_repo_1]xyz(123).txt", null);
		testRegex(regex, "[localhost]/abc/def/x-yz", null);
		testRegex(regex, "[localhost]/abc/def/xyz(456)", null);
		testRegex(regex, "/ab-\\c/def/xyz(123).txt", null);
		testRegex(regex, "abc+def%40foo%24/xyz%21(123).txt", null);
		testRegex(regex, "[localhost,127.0.0.1,usadev01]/abc+def%40foo%24/xyz%21(123).txt", null);
	}
	@Test
	public void testPropertyIndexListPattern() throws Exception {
		String regex = "(\\d+)(?:-(\\d+))?(?:\\||$)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)";
		//String regex = "(?:\\[([^\\]]+)\\])?(([\\\\/\\w+*.%-]+?(?!\\.\\w+$))(?:\\((\\d+)\\))?(\\.\\w+$)?)";
		testRegex(regex, "12|", null);
		testRegex(regex, "12", null);
		testRegex(regex, "12-15|", null);
		testRegex(regex, "12-15", null);
	}

	@Test
	public void testInetAddressMatchPattern() throws Exception {
		String regex = "([^:/]*)(?:\\/(\\d{1,3}(?:\\.\\d{1,3}){3,5}))?:(\\d{1,5})";
		testRegex(regex, "localhost/127.0.0.1:3333", null);
		testRegex(regex, "usalive.usatech.com:8080", null);
		testRegex(regex, "www.apache.org", null);
		testRegex(regex, "/10.0.0.101", null);
		testRegex(regex, "/10.0.0.131:456", null);
		testRegex(regex, "nada/10.0.0.141:0", null);
		testRegex(regex, "faulty1:789/2.3.4.234", null);
		testRegex(regex, "faulty2/789/2.3.4.234", null);
		testRegex(regex, "faulty3:789:2.3.4.234", null);
		testRegex(regex, "faulty3/2.3.4.234:abc", null);
	}

	@Test
	public void testInternalIPAddressMatchPattern() throws Exception {
		String regex = "^(?:(?:192\\.168|10\\.0)\\.\\d{1,3}\\.\\d{1,3})|127\\.0\\.0\\.1$";
		testRegex(regex, "127.0.0.1", null);
		testRegex(regex, "10.0.0.101", null);
		testRegex(regex, "192.168.3.1", null);
		testRegex(regex, "172.30.0.9", null);
	}

	@Test
	public void testCardMatchPattern() throws Exception {
		String regex = "^(5[1-5][0-9]{14})\\|([0-9]{4})\\|(|[0-9]{3,4})\\|(|[A-Za-z ,.'-]{2,26})\\|(|[0-9]{5,9})\\|(|[A-Za-z0-9 ,.'-]{3,200})$";
		testRegex(regex, "5454545454545454|1301|||19355|100 Deerfield Ln Suite 110", null);
	}

	@Test
	public void testAbsMatchPattern() throws Exception {
		String regex = "(([\\/])|(\\w+\\:)).+";
		testRegex(regex, "http://usalive.ussatech.com/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "//usalive.ussatech.com/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "../test.i?go=true#frag", null);
		testRegex(regex, "http://usalive.ussatech.com:8080/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "https://usalive.ussatech.com:8080/", null);
		testRegex(regex, "http://usalive.ussatech.com:8080", null);
	}
	@Test
	public void testUriMatchPattern() throws Exception {
		String regex = "(?:(?:(?:\\w+\\:)?//([^/:]*)(?:\\:\\d+)?)?(/)?)?(.*)";
		testRegex(regex, "http://usalive.ussatech.com/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "//usalive.ussatech.com/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "../test.i?go=true#frag", null);
		testRegex(regex, "http://usalive.ussatech.com:8080/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "https://usalive.ussatech.com:8080/", null);
		testRegex(regex, "http://usalive.ussatech.com:8080", null);
	}
	@Test
	public void testUriAddPattern() throws Exception {
		//String regex = "([^?#]*)(?:(\\?)([^#]*))?(?:(\\#.*))?";
		String regex = "(?:([^:]+):)?(?://)?([^/?#:]*)(?:\\:(\\d+))?(/[^/#?;.]+)?(?:/(?:.+\\.[^.]+)?)?";
		testRegex(regex, "http://usalive.usatech.com/test.i", null);
		testRegex(regex, "http://usalive.ussatech.com/CustomerReporting/test.i", null);
		testRegex(regex, "https://localhost:8580/pepi/test.html", null);
		testRegex(regex, "https://localhost:8580/test.html", null);
		testRegex(regex, "https://localhost:8580/", null);
		testRegex(regex, "https://localhost:8580", null);
		testRegex(regex, "https://localhost:8580/abc/def/jkl.how.you.know.it", null);

	}
	
	@Test
	public void testUrlProtocolPattern() throws Exception {
		String regex = "\\s*([a-zA-Z]{2,}):.+";
		testRegex(regex, "resource:/com/usatech/report/rg-data-layer.xml", null);
		testRegex(regex, "   resource:/com/usatech/report/rg-data-layer.xml", null);
		testRegex(regex, "e:/temp/test.csv", null);
		testRegex(regex, "  e:/temp/test.csv", null);
		testRegex(regex, "file:///e:/temp/test.csv", null);
		testRegex(regex, "   file:///e:/temp/test.csv", null);
		testRegex(regex, "http://usalive.ussatech.com:8080/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "https://usalive.ussatech.com:8080/", null);
	}
	@Test
	public void testArrayPattern() throws Exception {
		String regex = "\\s*(?:(\\d+)|(?:\\[(\\d+)\\]))(?:(?:\\.(.+))|(\\[.+)|(\\(.+))?\\s*";
		testRegex(regex, "0.map[0](\"key\")", null);
		testRegex(regex, "  1 ", null);
		testRegex(regex, "0", null);
		testRegex(regex, "", null);
		testRegex(regex, "0map", null);
		testRegex(regex, "[0].everybody", null);
		testRegex(regex, "[4]", null);
		testRegex(regex, "[5][7].blanco", null);
		testRegex(regex, "[5](key)[9].three", null);
	}
	@Test
	public void testMultiline() throws Exception {
		String regex = "^\\s*MODE Z\\s*$";
		testFind(regex, " AUTH TLS\n AUTH SSL\n SOMETHING\n MODE Z\n MORE\n GOBB hfdfaj=i\n", "m");
		testFind(regex, " AUTH TLS\n AUTH SSL\n SOMETHING\n MORE\n GOBB hfdfaj=i\n", "m");
		testFind(regex, " AUTH TLS\n AUTH SSL\n SOMETHING\nMODE Z\n GOBB hfdfaj=i\n", "m");
		testFind(regex, " AUTH TLS\n AUTH SSL\n SOMETHING\nMODE Z   \n GOBB hfdfaj=i\n", "m");
		testFind(regex, " AUTH TLS\n AUTH SSL\n SOMETHING\nMODE ZSW   \n GOBB hfdfaj=i\n", "m");
		testFind(regex, "", "m");
		testFind(regex, " MODE Z", "m");
	}

	@Test
	public void testPathPattern() throws Exception {
		String regex = "\\s*([a-zA-Z]{2,}):([^?;#]+)(?:[?;#](.*))?";
		testRegex(regex, "web:/com/usatech/report/rg-data-layer.xml?t=1232134#abc", null);
		testRegex(regex, "web:/com/usatech/report/rg-data-layer.xml;t=1232134;123#abc", null);
		testRegex(regex, "web:/com/usatech/report/rg-data-layer.xml", null);
		testRegex(regex, "resource:/com/usatech/report/rg-data-layer.xml", null);
		testRegex(regex, "   resource:/com/usatech/report/rg-data-layer.xml", null);
		testRegex(regex, "e:/temp/test.csv", null);
		testRegex(regex, "  e:/temp/test.csv", null);
		testRegex(regex, "file:///e:/temp/test.csv", null);
		testRegex(regex, "   file:///e:/temp/test.csv", null);
		testRegex(regex, "http://usalive.ussatech.com:8080/CustomerReporting/test.i?go=true#frag", null);
		testRegex(regex, "https://usalive.ussatech.com:8080/", null);
	}

	protected void testRegex(String regex, String value, String flags) throws Exception {
		String[] groups = RegexUtils.match(regex, value, flags);
		if(groups == null) {
			System.out.println("Value '" + value + "' does NOT match '" + regex + "'");
		} else {
			System.out.print("Value '" + value + "' matches '" + regex + "': [");
			for(int i = 1; i < groups.length; i++) {
				if(i > 1) System.out.print(", ");
				System.out.print(groups[i]);
			}
			System.out.println("]");
		}
	}

	protected void testFind(String regex, String value, String flags) throws Exception {
		String[] groups = RegexUtils.find(regex, value, flags).find();
		if(groups == null) {
			System.out.println("Value '" + value + "' does NOT contain '" + regex + "'");
		} else {
			System.out.print("Value '" + value + "' contains '" + regex + "': [");
			for(int i = 1; i < groups.length; i++) {
				if(i > 1)
					System.out.print(", ");
				System.out.print(groups[i]);
			}
			System.out.println("]");
		}
	}

	protected void testFindAll(String regex, String value, String flags) throws Exception {
		System.out.println("Searching Value '" + value + "' for '" + regex + "':");
		Matcher matcher = Pattern.compile(regex, RegexUtils.translateFlags(flags)).matcher(value);
		while(matcher.find()) {
			System.out.print("Found '" + regex + "' at position " + matcher.start() + " [");
			for(int i = 1; i < matcher.groupCount() + 1; i++) {
				if(i > 1)
					System.out.print(", ");
				System.out.print(matcher.group(i));
			}
			System.out.println("]");
		}
		System.out.println("Searching complete");
	}
	protected void testReplace(String regex, String value, String replacement, String flags) throws Exception {
		String result = RegexUtils.substitute(regex, value, replacement, flags);
		if(result.equals(value)) {
			System.out.println("Did not replace anything in '" + value + "' for '" + regex + "'");
		} else {
			System.out.println("Replaced Value '" + value + "' for '" + regex + "' with '" + result + "'");		
		}
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
