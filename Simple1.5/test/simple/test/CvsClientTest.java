package simple.test;

import java.awt.Window;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.GuiInteraction;
import simple.io.IOUtils;
import simple.io.Interaction;
import simple.io.Log;
import simple.io.versioning.CvsClient;

public class CvsClientTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	@Test
	public void testExport() throws Exception {
		String path = "DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk";
		String revision = "HEAD"; // "1.167";
		CvsClient cvs = new CvsClient("cvs.usatech.com", "/usr/local/cvsroot/NetworkServices");
		Interaction interaction = new GuiInteraction((Window) null);
		char[] pwd = interaction.readPassword("Enter your password for cvs:");
		if(pwd == null)
			return;
		cvs.connect("darkhipov", new String(pwd));
		InputStream in = cvs.export(path, revision);
		long size = IOUtils.readForLength(in);
		log.info("Read " + size + " bytes for " + path + " (" + revision + ")");
		cvs.close();
	}

	@Test
	public void testTag() throws Exception {
		String path = "Simple1.5/test/simple/test/CvsClientTest.java";
		String revision = "1.1"; // "1.167";
		String tag = "TEST_1";
		CvsClient cvs = new CvsClient("cvs.usatech.com", "/usr/local/cvsroot/NetworkServices");
		Interaction interaction = new GuiInteraction((Window) null);
		char[] pwd = interaction.readPassword("Enter your password for cvs:");
		if(pwd == null)
			return;
		cvs.connect("bkrug", new String(pwd));
		cvs.tag(path, revision, tag);
		log.info("Tagged " + path + " (" + revision + ") with " + tag);
		cvs.close();
	}

	@Test
	public void testHistory() throws Exception {
		String path = "Simple1.5/test/simple/test/CvsClientTest.java";
		// String revision = "HEAD"; // "1.167";
		// String tag = "TEST_1";
		CvsClient cvs = new CvsClient("cvs.usatech.com", "/usr/local/cvsroot/NetworkServices");
		Interaction interaction = new GuiInteraction((Window) null);
		char[] pwd = interaction.readPassword("Enter your password for cvs:");
		if(pwd == null)
			return;
		cvs.connect("bkrug", new String(pwd));
		String history = cvs.historyOfCommits(path);
		log.info("Got history of " + path + ":\n" + history);
		cvs.close();
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
