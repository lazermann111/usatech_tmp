package simple.test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class NIOSendReceiveServer extends NIOSendReceiveTest {
	protected final Selector selector;
	protected final InetSocketAddress socketAddress;
	protected final ServerSocketChannel serverChannel;

	protected class Attachment extends Worker {
		protected long pause;

		public Attachment(SocketChannel socketChannel, WorkerType workerType, long pause, int bufferSize) {
			super(socketChannel, workerType, bufferSize);
			this.pause = pause;
		}

		@Override
		public void runReceive() {
			read(2);
			if(socketChannel.isOpen()) {
				SelectionKey key = socketChannel.keyFor(selector);
				key.interestOps(SelectionKey.OP_READ);
				// the above does not seem to notify select() on the selector, so we will
				selector.wakeup();
			}
		}

		@Override
		protected void runSend() {
			write(pause, 0);
			if(socketChannel.isOpen()) {
				try {
					socketChannel.close();
				} catch(IOException e) {
					log_error("Could not close " + id, e);
				}
			}
		}
	}

	public NIOSendReceiveServer(int port) throws IOException {
		socketAddress = new InetSocketAddress(port);
		selector = Selector.open();
		serverChannel = ServerSocketChannel.open();
		serverChannel.socket().bind(socketAddress);
		serverChannel.configureBlocking(false);
		serverChannel.register(selector, serverChannel.validOps());
		log_info("Listening on port " + socketAddress + " for requests");

	}

	protected void runServer(WorkerType workerType, long pause, int bufferSize) throws IOException {
		while(true) {
			while(selector.selectedKeys().isEmpty()) {
				selector.select();
			}
			Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
			while(iter.hasNext()) {
				SelectionKey key = iter.next();
				SocketChannel socketChannel = null;
				try {
					if(key.isAcceptable() && key.channel() instanceof ServerSocketChannel) {
						socketChannel = ((ServerSocketChannel) key.channel()).accept();
						if(socketChannel != null) {
							socketChannel.configureBlocking(false);
							switch(workerType) {
								case RECEIVE:
									socketChannel.register(selector, SelectionKey.OP_READ, new Attachment(socketChannel, workerType, pause, bufferSize));
									break;
								case SEND:
									socketChannel.register(selector, SelectionKey.OP_WRITE, new Attachment(socketChannel, workerType, pause, bufferSize));
									break;
							}
							log_info("Accepting a connection" + getClientInfo(socketChannel) + " to " + workerType.toString().toLowerCase() + " requests");
						} else
							iter.remove();
					} else if(workerType == WorkerType.RECEIVE && key.isReadable() && key.channel() instanceof SocketChannel) {
						key.interestOps(0);// stop interest in READABLE
						iter.remove();
						log_info("Found READABLE Channel " + key);
						executor.execute((Runnable) key.attachment());
					} else if(workerType == WorkerType.SEND && key.isWritable() && key.channel() instanceof SocketChannel) {
						key.interestOps(0);// stop interest in WRITABLE
						iter.remove();
						log_info("Found WRITABLE Channel " + key);
						executor.execute((Runnable) key.attachment());
					}
				} catch(CancelledKeyException e) {
					log_info("Key was cancelled. Getting next.");
					iter.remove();
				} catch(ClosedByInterruptException e) {
					log_info("Socket Channel closed by interrupt" + getClientInfo(socketChannel));
					iter.remove();
				} catch(ClosedChannelException e) {
					log_info("Channel is closed while accessing" + getClientInfo(socketChannel));
					iter.remove();
				}
			}
		}
	}
}
