package simple.test;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.AttributeList;
import org.xml.sax.DocumentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import simple.io.Log;
import simple.xml.sax.AggregatingAdapter2;
import simple.xml.sax.AggregatingInputSource2;
import simple.xml.sax.BeanAdapter2;
import simple.xml.sax.BeanInputSource;

import com.jclark.xml.parse.StartElementEvent;

public class XMLAdapterTest extends UnitTest {
	public static class A {
		public A() {
			for(int i = 0; i < 3; i++)
				dogElephantMap.put(new D(new D(null)), new E());
		}
		public B buffalo = new B();
		public C cow = new C();
		public Map<D,E> dogElephantMap = new HashMap<D, E>();
	}
	public static class B {
		public D dog = new D(null);
		public E elephant;
	}
	public static class C {
		public int i = 256;
		public String title="hello";
	}
	public static class D {
		public D(D subDog) {
			this.subDog = subDog;
		}
		public D subDog;
		public Color hair = Color.BLACK;
		public double height = 4.5;
	}
	public static class E {
		public byte[] segments = new byte[] {34,37,83,53};
		public C[] cows = new C[] { new C(), new C() };
	}
	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testBeanAdapter2() throws Exception {
		BeanInputSource bis = new BeanInputSource(new A(), null);
		BeanAdapter2 ba = new BeanAdapter2();
		log.info("XML:\n" + ba.toXML(bis));
	}

	@Test
	public void testAggregatingAdapter2() throws Exception {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("foo", "bar");
		params.put("boo", 33);

		AggregatingInputSource2 ais = new AggregatingInputSource2(
				new Source[] {
						new SAXSource(new BeanAdapter2(),new BeanInputSource(new A(), null)),
						new SAXSource(new BeanAdapter2(),new BeanInputSource(new String("BlahBlahBlah"),null)),
				},
				new String[] {
						"mybean",
						"mystring"
				},
 null, params);
		AggregatingAdapter2 ba = new AggregatingAdapter2();
		log.info("XML:\n" + ba.toXML(ais));
	}

	@Test
	public void testXmlParsing() throws FileNotFoundException, SAXException, IOException {
		com.jclark.xml.sax.Driver driver = new com.jclark.xml.sax.Driver() {
			protected StartElementEvent event;

			public void startElement(StartElementEvent event) throws SAXException {
				this.event = event;
				super.startElement(event);
			}

			public String getValue(int i) {
				return event.getAttributeUnnormalizedValue(i);
			}
		};
		driver.setDocumentHandler(new DocumentHandler() {
			
			@Override
			public void startElement(String name, AttributeList atts) throws SAXException {
				StringBuilder sb = new StringBuilder();
				sb.append("START " + name + " with\n");
				for(int i = 0; i < atts.getLength(); i++)
					sb.append("\n\t").append(atts.getName(i)).append("='").append(atts.getValue(i)).append("';");
				log.info(sb.toString());
			}
			
			@Override
			public void startDocument() throws SAXException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setDocumentLocator(Locator locator) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void processingInstruction(String target, String data) throws SAXException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void endElement(String name) throws SAXException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void endDocument() throws SAXException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void characters(char[] ch, int start, int length) throws SAXException {
				// TODO Auto-generated method stub
				
			}
		});
		driver.parse(new InputSource(new FileReader("test/simple/test/db-test-data-layer.xml")));

	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
