package simple.test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.results.DatasetHandler;
import simple.servlet.ResponseUtils;
import simple.text.AbstractDelimitedParser.ExtraHandling;
import simple.text.MinimalCDataCharset;
import simple.text.MultiRecordDelimitedParser;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class StringUtilsTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void junit_BuildUrl() throws Exception {
		StringBuilder sb = new StringBuilder();
		ResponseUtils.appendQueryParam("fir&st", true, sb);
		ResponseUtils.appendQueryParam("==nothing==", "", sb);
		ResponseUtils.appendQueryParam("whack a mole", new Date(), sb);
		ResponseUtils.appendQueryParam("haglah#:", "%*", sb);
		ResponseUtils.appendQueryParam("Don't show anything", null, sb);
		ResponseUtils.appendQueryParam("cal[]", Calendar.getInstance(), sb);
		log.info("Query: " + sb.toString());
	}

	@Test
	public void junit_DelimitedParsing() throws Exception {
		testDelimitedParsing("|", "\r\n", "\"", "*DFRBEG|PID=253145|FREQ=DAILY|CO=248258\r\nHFIN0011|248258|06/16/2015|06/16/2015|06/16/2015|02:49:27\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|CZ||\"Chase Merchant Discount Rate\"|R|2|-0.0300|-0.68|-0.021500|0.07\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|CZ||\"Chase Merchant Discount Rate\"|S|4|0.0300|7.99|0.021500|-0.29\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|VI|VUSC|\"Interchange\"|S|1|0.0200|2.00|0.020000|-0.06\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|VI||\"Visa Authorization Processing \"|S|3|0.0195|22.00|0|-0.06\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|MC|MUSF|\"Interchange\"|S|2|0.0200|3.50|0.020000|-0.11\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|MC||\"MC Auth Access Fee\"|S|4|0.0050|48.50|0|-0.02\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|MC||\"MasterCard NABU Fee\"|A|6|0.0195|33.50|0|-0.12\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|MC||\"MC Acquiring License Fee\"|S|2|0.0000|3.50|0.000040|0.00\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|DI|DCES|\"Discover Data Usage\"|S|2|0.0185|1.97|0|-0.04\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|DI|DCES|\"Interchange\"|S|2|0.0000|1.97|0.019500|-0.04\r\nRFIN0011|IA|IC|TD|254242|2839397|33331143|USD|26276449|DI||\"Discover Network Authorization\"|S|5|0.0025|16.97|0|-0.01\r\nRFIN0011|IA|AS|TD|254242|2839397|33331143|USD||VI||\"Assessment\"|S||0.0000|2.00|0.001300|0.00\r\nRFIN0011|IA|AS|TD|254242|2839397|33331143|USD||MC||\"Assessment\"|S||0.0000|3.50|0.001200|0.00\r\nRFIN0011|IA|AS|TD|254242|2839397|33331143|USD||DI||\"Assessment\"|S||0.0000|1.97|0.001100|0.00\r\nRFIN0011|PFEE|AUTH|TD|254242|2839397|33331143|USD|26276449|CZ||\"PNS Authorization TCP/IP\"|S|10|0.0000|38.74|0|0.00\r\nRFIN0011|PFEE|AUTH|TD|254242|2839397|33331143|USD|26276449|VI||\"PNS Authorization TCP/IP\"|S|3|0.0000|22.00|0|0.00\r\nRFIN0011|PFEE|AUTH|TD|254242|2839397|33331143|USD|26276449|VI||\"Full Authorization Reversal\"|S|1|0.0000|10.00|0|0.00\r\nRFIN0011|PFEE|AUTH|TD|254242|2839397|33331143|USD|26276449|MC||\"Full Authorization Reversal\"|S|2|0.0000|15.00|0|0.00\r\nRFIN0011|PFEE|AUTH|TD|254242|2839397|33331143|USD|26276449|MC||\"PNS Authorization TCP/IP\"|S|6|0.0000|33.50|0|0.00\r\nRFIN0011|PFEE|AUTH|TD|254242|2839397|33331143|USD|26276449|DI||\"PNS Authorization TCP/IP\"|S|5|0.0000|16.97|0|0.00\r\nRFIN0011|PFEE|AUTH|TD|254242|2839397|33331143|USD|26276449|DI||\"Full Authorization Reversal\"|S|1|0.0000|5.00|0|0.00\r\nRFIN0011|PFEE|DEP|TD|254242|2839397|33331143|USD|26276449|VI||\"Settled Dep. Fee\"|S|1|0.0014|2.00|0|0.00\r\nRFIN0011|PFEE|DEP|TD|254242|2839397|33331143|USD|26276449|MC||\"Settled Dep. Fee\"|S|2|0.0014|3.50|0|0.00\r\nRFIN0011|PFEE|DEP|TD|254242|2839397|33331143|USD|26276449|DI||\"Settled Dep. Fee\"|S|2|0.0000|1.97|0|0.00\r\nHACT0010|248258|06/16/2015|06/16/2015|06/16/2015|02:52:07\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|8|TD|254242|USD|\"EV322524-1434380193\"||XXXXXXXXXXXX3966|10/18|5.46|CZ|D|06/15/2015|08998C|100||||5999|||||CMRS|N|0|0|0|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|16|TD|254242|USD|\"EV064211-1434375464\"||XXXXXXXXXXXX4935|08/16|2.00|VI|D|06/15/2015|02035C|100||||5814|||||VUSC|N|.02|.02|-.06|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|17|TD|254242|USD|\"TD139324-1434345734\"||XXXXXXXXXXXX6277|08/15|1.50|MC|D|06/15/2015|386384|100||||5814|||||MUSF|N|.02|.02|-.05|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|18|TD|254242|USD|\"TD139324-1434345735\"||XXXXXXXXXXXX0753|03/18|1.50|CZ|D|06/15/2015|07459C|100||||5814|||||CRWS|N|0|0|0|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|19|TD|254242|USD|\"EV064211-1434375648\"||XXXXXXXXXXXX6877|08/15|2.00|MC|D|06/15/2015|916905|100||||5814|||||MUSF|N|.02|.02|-.06|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|20|TD|254242|USD|\"TD027016-1434390246\"||XXXXXXXXXXXX3966|10/18|0.47|CZ|D|06/15/2015|05964C|100||||5814|||||CRWS|N|0|0|0|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|21|TD|254242|USD|\"TD027016-1434390419\"||XXXXXXXXXXXX8833|10/17|0.47|DI|D|06/15/2015|01531R|100||||5814|||||DCES|N|0|.0195|-.01|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|22|TD|254242|USD|\"TD139324-1434345736\"||XXXXXXXXXXXX8833|10/17|1.50|DI|D|06/15/2015|01515R|100||||5814|||||DCES|N|0|.0195|-.03|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|23|TD|254242|USD|\"EV256536-1434376170\"||XXXXXXXXXXXX3966|10/18|0.56|CZ|D|06/15/2015|08113C|100||||5814|||||CRWS|N|0|0|0|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|56|TD|254242|USD|\"EV152309-1434122670-R1\"||XXXXXXXXXXXX3966|10/18|-.21|CZ|R|06/15/2015||100||||5814|||||CRFC|N|0|0|0|0|||0\r\nRACT0010|06/15/2015|253826|\"usatec\"|\"50616.0002h\"|57|TD|254242|USD|\"TD027016-1434120418-R1\"||XXXXXXXXXXXX3966|10/18|-.47|CZ|R|06/15/2015||100||||5814|||||CRFC|N|0|0|0|0|||0\r\nHACT0002|248258|06/16/2015|06/16/2015|06/16/2015|02:50:39\r\nHACT0027|248258|06/16/2015|06/16/2015|06/16/2015|02:50:39\r\nRACT0027|TD|254242|USD|             001|        166001|XXXXXXXXXXXX3966||EV322524-1434380193|6/16/2015|Sale|08998C|5.46|5.46\r\nRACT0027|TD|254242|USD|             001|        166002|XXXXXXXXXXXX4935||EV064211-1434375464|6/15/2015|Sale|02035C|2.00|10.00\r\nRACT0027|TD|254242|USD|             001|        166002|XXXXXXXXXXXX6877||EV064211-1434375648|6/15/2015|Sale|916905|2.00|10.00\r\nRACT0027|TD|254242|USD|             001|        166002|XXXXXXXXXXXX3966||EV256536-1434376170|6/15/2015|Sale|08113C|0.56|10.00\r\nRACT0027|TD|254242|USD|             001|        166002|XXXXXXXXXXXX3966||TD027016-1434390246|6/15/2015|Sale|05964C|0.47|10.00\r\nRACT0027|TD|254242|USD|             001|        166002|XXXXXXXXXXXX8833||TD027016-1434390419|6/15/2015|Sale|01531R|0.47|10.00\r\nRACT0027|TD|254242|USD|             001|        166002|XXXXXXXXXXXX6277||TD139324-1434345734|6/15/2015|Sale|386384|1.50|10.00\r\nRACT0027|TD|254242|USD|             001|        166002|XXXXXXXXXXXX0753||TD139324-1434345735|6/15/2015|Sale|07459C|1.50|10.00\r\nRACT0027|TD|254242|USD|             001|        166002|XXXXXXXXXXXX8833||TD139324-1434345736|6/15/2015|Sale|01515R|1.50|10.00\r\nRACT0027|TD|254242|USD|             001|        166004|XXXXXXXXXXXX3966||EV152309-1434122670-R1|6/15/2015|Refund|      |-0.21|-0.68\r\nRACT0027|TD|254242|USD|             001|        166004|XXXXXXXXXXXX3966||TD027016-1434120418-R1|6/15/2015|Refund|      |-0.47|-0.68\r\nHANS0013|248258|06/16/2015|06/16/2015|06/16/2015|02:46:06\r\n*DFREND|PID=253145|FREQ=DAILY|CO=248258\r\n");

	}

	protected void testDelimitedParsing(final String columnSeparator, final String lineSeparator, String quotes, String data) throws IOException, RuntimeException {
		MultiRecordDelimitedParser<String> parser = new MultiRecordDelimitedParser<>();
		parser.setColumnSeparator(columnSeparator);
		parser.setLineSeparator(lineSeparator);
		if(quotes != null && quotes.length() > 0)
			parser.setQuotes(quotes.toCharArray());
		parser.setExtraColumnHandling(ExtraHandling.OUTPUT);
		parser.setMissingSelectorHandling(ExtraHandling.OUTPUT);
		parser.setSelectorColumn("Selector", String.class);
		final StringBuilder out = new StringBuilder();
		DatasetHandler<RuntimeException> handler = new DatasetHandler<RuntimeException>() {
			protected boolean firstCol = true;

			@Override
			public void handleDatasetStart(String[] columnNames) throws RuntimeException {

			}

			@Override
			public void handleRowStart() throws RuntimeException {
				firstCol = true;
			}

			@Override
			public void handleValue(String columnName, Object value) throws RuntimeException {
				if(!firstCol)
					out.append(columnSeparator);
				out.append(value);
				firstCol = false;
			}

			@Override
			public void handleRowEnd() throws RuntimeException {
				out.append(lineSeparator);
			}

			@Override
			public void handleDatasetEnd() throws RuntimeException {
			}
		};
		Reader reader = new StringReader(data);
		try {
			parser.parse(reader, handler);
		} finally {
			reader.close();
		}
		if(quotes != null && quotes.length() > 0) {
			StringBuilder unquotedData = new StringBuilder(data.length());
			for(int p = 0; p < data.length(); p++) {
				char ch = data.charAt(p);
				if(quotes.indexOf(ch) < 0)
					unquotedData.append(ch);
			}
			data = unquotedData.toString();
		}
		
		if(!data.equals(out.toString()))
			throw new IOException("Output does not match input");
	}

	@Test
	public void testSplit() {
		String[] samples = { "/nothing/", "1/23/", "////", "one/two/three/four/five/test.html", "/", "\\//", "././././." };
		for(String s : samples) {
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.split(s, "/l".toCharArray(), true)));
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.split(s, "/")));
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.split(s, '/')));
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.split(s, '/', '"')));
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.split(s, '/', '"', true)));
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.split(s, '/', 10)));
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.split(s, '/', 1)));
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.split(s, '/', 2)));

		}
	}

	@Test
	public void testSplitJoin() {
		String[] samples = { "/not'hing/", "1/23/", "/'//'/", "one/two/three/four/five/test.html", "/", "\\//", "./'./.'/./." };
		for(String s : samples) {
			String[] split = StringUtils.split(s, '/', '\'');
			String recons = StringUtils.join(split, '/', '\'');
			String[] split2 = StringUtils.split(recons, '/', '\'');
			log.info((Arrays.equals(split, split2) ? "Okay: " : "ERROR!!! ") + "ORIG=" + s + "; SPLIT=" + Arrays.toString(split) + "; RECONS=" + recons);
		}
	}
	@Test
	public void testSplitAnywhere() {
		String[] samples = { "one,two,three,four", "\"one,two,three,four\"", "\"this,one\",\"that,one\",more,stuff," };
		for(String s : samples) {
			log.info("ORIG=" + s + "; SPLIT=" + Arrays.toString(StringUtils.splitQuotesAnywhere(s, ",;", "\"", false)));
		}
	}

	@Test
	public void testDirectoryExtraction() {
		String[] samples = { "test.html", "one/two/three/four/five/test.html", "/", "\\//", "/abc/def/hij/gru/test.html", "one/two/three/four/five/test.html", ".", "./././././nth/a.html",
 "/one/two/../three/four/../five/test.html", "/one/two/../../three/four/../../five/test.html", "../junk/real/more/now////test.html", "../../..", "././././." };
		for(String s : samples) {
			log.info("PATH=" + s + "; DIRS=" + extractDirs(s));
		}
	}
	protected String extractDirs(String path) {
		final String[] dirs = StringUtils.split(path, "/\\".toCharArray(), true);
		if(dirs != null && dirs.length > 1) {
			int len = dirs.length - 1;
			for(int i = 0; i < len; i++) {
				if(StringUtils.isBlank(dirs[i]) || dirs[i].equals(".")) {
					System.arraycopy(dirs, i + 1, dirs, i, len - i - 1);
					len--;
					i--;
				} else if(dirs[i].equals("..")) {
					if(i == 0) {
						System.arraycopy(dirs, i + 2, dirs, i, len - i - 1);
					} else {
						System.arraycopy(dirs, i + 1, dirs, i - 1, len - i - 1);
						i -= 2;
					}
					len -= 2;
				}
			}
			if(len > 0) {
				StringBuilder sb = new StringBuilder();
				StringUtils.join(dirs, "/", 0, len, null, sb);
				return sb.toString();
			} else
				return "";
		}
		return "";
	}
	@Test
	public void printCharacterTypes() {
		for(char ch = ' '; ch <= '~'; ch++)
			log.info("'" + ch + "' = " + getTypeName(Character.getType(ch)));
	}

	@Test
	public void testReversePieces() {
		String[] ss = { "ec2.usatech.com", "nodotsatall", "m.a.n.y.d.o.t.s.f.o.r.u" };
		for(String s : ss)
			log.info(s + " becomes " + reversePieces(s, '.'));
	}

	protected static String reversePieces(String orig, char delimiter) {
		StringBuilder sb = new StringBuilder();
		int p = orig.length();
		while(true) {
			int next = orig.lastIndexOf(delimiter, p - 1);
			if(next < 0) {
				sb.append(orig, 0, p);
				return sb.toString();
			}
			sb.append(orig, next + 1, p).append(delimiter);
			p = next;
			next = orig.lastIndexOf(delimiter, p - 1);
		}
	}

	protected String getTypeName(int type) {
		switch(type) {
			case Character.COMBINING_SPACING_MARK:
				return "COMBINING_SPACING_MARK";
			case Character.CONNECTOR_PUNCTUATION:
				return "CONNECTOR_PUNCTUATION";
			case Character.CONTROL:
				return "CONTROL";
			case Character.DASH_PUNCTUATION:
				return "DASH_PUNCTUATION";
			case Character.DECIMAL_DIGIT_NUMBER:
				return "DECIMAL_DIGIT_NUMBER";
			case Character.ENCLOSING_MARK:
				return "ENCLOSING_MARK";
			case Character.END_PUNCTUATION:
				return "END_PUNCTUATION";
			case Character.FINAL_QUOTE_PUNCTUATION:
				return "FINAL_QUOTE_PUNCTUATION";
			case Character.FORMAT:
				return "FORMAT";
			case Character.INITIAL_QUOTE_PUNCTUATION:
				return "INITIAL_QUOTE_PUNCTUATION";
			case Character.LETTER_NUMBER:
				return "LETTER_NUMBER";
			case Character.LINE_SEPARATOR:
				return "LINE_SEPARATOR";
			case Character.LOWERCASE_LETTER:
				return "LOWERCASE_LETTER";
			case Character.MATH_SYMBOL:
				return "MATH_SYMBOL";
			case Character.MODIFIER_LETTER:
				return "MODIFIER_LETTER";
			case Character.MODIFIER_SYMBOL:
				return "MODIFIER_SYMBOL";
			case Character.NON_SPACING_MARK:
				return "NON_SPACING_MARK";
			case Character.OTHER_LETTER:
				return "OTHER_LETTER";
			case Character.OTHER_NUMBER:
				return "OTHER_NUMBER";
			case Character.OTHER_PUNCTUATION:
				return "OTHER_PUNCTUATION";
			case Character.OTHER_SYMBOL:
				return "OTHER_SYMBOL";
			case Character.PARAGRAPH_SEPARATOR:
				return "PARAGRAPH_SEPARATOR";
			case Character.PRIVATE_USE:
				return "PRIVATE_USE";
			case Character.SPACE_SEPARATOR:
				return "SPACE_SEPARATOR";
			case Character.CURRENCY_SYMBOL:
				return "CURRENCY_SYMBOL";
			case Character.START_PUNCTUATION:
				return "START_PUNCTUATION";
			case Character.SURROGATE:
				return "SURROGATE";
			case Character.TITLECASE_LETTER:
				return "TITLECASE_LETTER";
			case Character.UNASSIGNED:
				return "UNASSIGNED";
			case Character.UPPERCASE_LETTER:
				return "UPPERCASE_LETTER";
			default:
				return "UNKNOWN";
		}
	}

	@Test
	public void testToWords() throws Exception {
		String[] strings = {
				"thisIsMyFirstESAYOne",
				"NowWith99NumbersOr44OrMore",
				"How-about-some-spaces?",
				"ANAwesomeOne!"
		};
		for(String s : strings)
			log.info("From '" + s + "' to '" + StringUtils.toWords(s) + "'");
	
	}
	@Test
	public void testGapBetween() throws Exception {
		char[] SORTED_ASCII_CHARS = new char[95];
		for(int i = 0; i < SORTED_ASCII_CHARS.length; i++)
			SORTED_ASCII_CHARS[i] = (char) (32 + i);
		String value1 = "ABC";
		for(String value2 : new String[] { "ADA", "ABC", "ACF", "ABCDEF", "GH", "BBBB" }) {
			BigInteger gap = StringUtils.getGapBetween(value1.toCharArray(), value2.toCharArray(), SORTED_ASCII_CHARS);
			log.info("Found a gap of " + gap + " between '" + value1 + "' and '" + value2 + "'");
			char[] values = new char[Math.max(value1.length(), value2.length())];
			System.arraycopy(value1.toCharArray(), 0, values, 0, value1.length());
			StringUtils.increment(values, gap, SORTED_ASCII_CHARS);
			log.info("Incremented '" + value1 + "' by " + gap + " and got '" + new String(values) + "'");
		}
	}
	@Test
	public void testReverse() throws Exception {
		String[] originals = new String[] {
				"123456789",
				"hello",
				"NOT ODD$",
				"",
				null,
				"NP",
				"O"
		};
		for(String original : originals)
			log.info("'" + original + "' to '" + StringUtils.reverse(original) + "'");
	}
	
	@Test
	public void testAppendHtml() throws Exception {
		String raw = "This is an html string\nwith some characters & some escaping\n\rhow did we do?\t<GOOD>.";
		StringBuilder sb = new StringBuilder();
		StringUtils.appendHTMLBlank(sb, new StringReader(raw));
		log.info("Start:\n" + raw);
		log.info("Final:\n" + sb.toString());
	}

	@Test
	public void testCDataCharset() throws Exception {
		MinimalCDataCharset charset = new MinimalCDataCharset();
		ByteBuffer bb = charset.encode("some &#0;&#0;&#0; cdata &amp; some &#34;more&#34; &#38; more!");
		byte[] b = new byte[bb.remaining()];
		bb.get(b);
		log.info("Encoded:\n" + new String(b));
		CharBuffer cb = charset.decode(ByteBuffer.wrap("\0 hello & how are ya!\t\nnewline\r'and \"lots of <gook>.".getBytes()));
		log.info("Decoded:\n" + cb.toString());

	}

	@Test
	public void testPad() throws Exception {
		String s = "1234567890";
		assert StringUtils.pad(s, ' ', 4, Justification.LEFT).equals("1234");
		assert StringUtils.pad(s, ' ', 4, Justification.RIGHT).equals("7890");
		assert StringUtils.pad(s, ' ', 4, Justification.CENTER).equals("4567");
	}
	@Test
	public void testPad2() throws Exception {
		String s = "9998015719167000";
		String p = StringUtils.appendPadded(new StringBuilder(), s, ' ', 15, Justification.RIGHT).toString();
		log.info("Padding '" + s + "' yields '" + p + "'");
	}

	@Test
	public void testAsChars() throws Exception {
		int[] lengths = new int[] {2,3,4,5,6,7,8};
		for(int k = 0; k < lengths.length; k++) {
			for(int i = 0; i < 50; i++) {
				//long num = (long)(Math.random() * 100 /*Long.MAX_VALUE*/);
				long num = i;
				System.out.print(num);
				System.out.print(" as ");
				System.out.print(lengths[k]);
				System.out.print(" chars = '");
				System.out.print(StringUtils.asCharacters(num, 'A', lengths[k]));
				System.out.println("'");
			}
		}
	}
	@Test
	public void testTrim() throws Exception {
		String[] originals = new String[] {
				"/abc/def/",
				"abc/def",
				"/////abc/def",
				"abc/def////",
				"\\/\\/abc/def\\\\",
				"/////",
				""
		};
		char[] trimChars = "/\\".toCharArray();
		for(String s : originals) {
			System.out.print('\'');
			System.out.print(s);
			System.out.print("' trimmed = '");
			System.out.print(StringUtils.trimChars(s, trimChars));
			System.out.println('\'');
		}
	}
	@Test
	public void testSubstringBeforeLast() throws Exception {
		String[] originals = new String[] {
				"/abc/def/",
				"abc/def",
				"/////abc/def",
				"abc/def////",
				"\\/\\/abc/def\\\\",
				"/////",
				""
		};
		char[] separators = "/\\".toCharArray();
		for(String s : originals) {
			System.out.print('\'');
			System.out.print(s);
			System.out.print("' beforelast = '");
			System.out.print(StringUtils.substringBeforeLast(s, separators));
			System.out.println('\'');
		}
	}
	@Test
	public void testIntoMap() throws Exception {
		Map<String,String> map = new HashMap<String, String>();
		String[] propStrings = new String[] {
				"abc=123;def=456;jk=90",
				"abc=123;def=456;jk=90;giggles",
				"'abc'=123;def='456';jk=90;;",
				"'abc' =    123  ; def  =  '45  6'  ; j k = 9  0 ; "
		};
		for(String ps : propStrings) {
			String last = StringUtils.intoMap(map, ps, '=', ';', new char[] {'"', '\''}, true, true);
			//StringUtils.intoMap(map, ps);
			System.out.println(ps);
			System.out.println("yielded");
			System.out.println(map);
			System.out.println("with '" + last + "' remaining");
			System.out.println();
			map.clear();
		}
	}
	@Test
	public void testStringBefore() throws Exception {
		Map<String,String> map = new HashMap<String, String>();
		String[] propStrings = new String[] {
				"P={xi:approvedAmount}|00",
				"P={{N=V|R=F|O=W|I={a=1|b=2|c=3}}}|E=U|00",
				"P={xi:approvedAmount=|okay}|V='='00'|'end|S=yes"
		};
		StringUtils.ParsingCharacters parsing = new StringUtils.ParsingCharacters();
		parsing.addDelimiter('|');
		parsing.addGrouping('{', '}');
		parsing.addQuotes(StringUtils.STANDARD_QUOTES);
		for(String ps : propStrings) {
			CharBuffer cb = CharBuffer.wrap(ps);
			System.out.println("Matching '" + ps + "'");
			int i = 1;
			while(cb.hasRemaining()) {
				System.out.println("" + i++ + ".\t" + StringUtils.stringBefore(cb, parsing));
			}
			System.out.println("------------");
			System.out.println();
			map.clear();
		}
	}

	@Test
	public void testToHex() throws Exception {
		Random random = new Random();
		for(int i = 0; i < 10; i++) {
			long l = random.nextLong();
			log.debug("Translate " + l + " to " + Long.toHexString(l) + "? " + StringUtils.toHex(l));
			assert StringUtils.pad(Long.toHexString(l), '0', 16, Justification.RIGHT).equalsIgnoreCase(StringUtils.toHex(l));
		}
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
