package simple.test;

import java.lang.management.ManagementFactory;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.lang.Holder;
import simple.util.concurrent.ConcurrentLRUMap;
import simple.util.concurrent.ConcurrentLRUMap2;

public class ConcurrentMapTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testMaps() throws Throwable {
		testMap(new ConcurrentLRUMap<Object, Object>(1000,100, 0.75f, 16), "ConcurrentLRUMap");
		testMap(new ConcurrentHashMap<Object, Object>(100, 0.75f, 16), "ConcurrentHashMap");
		testMap(new ConcurrentLRUMap2<Object, Object>(1000, 100, 0.75f, 16), "ConcurrentLRUMap2");
		Thread.sleep(5000);
		testMap(new ConcurrentLRUMap<Object, Object>(1000, 100, 0.75f, 16), "ConcurrentLRUMap");
		testMap(new ConcurrentHashMap<Object, Object>(100, 0.75f, 16), "ConcurrentHashMap");
		testMap(new ConcurrentLRUMap2<Object, Object>(1000, 100, 0.75f, 16), "ConcurrentLRUMap2");

	}
	protected void testMap(final ConcurrentMap<Object,Object> map, String mapName) throws Throwable {
		testMapIntegerKeys(map, mapName + " - Integer", 1000);
		testMapStringKeys(map, mapName + " - String", 3);
		//testMapArrayKeys(map, mapName + " - Array", 5);
	}
	protected void testMap(final ConcurrentMap<Object,Object> map, String mapName, int threads, int times, final Callable<? extends Object> keyCreator) throws Throwable {
		Runnable runnable = new Runnable() {
			public void run() {
				Object key;
				try {
					key = keyCreator.call();
				} catch(Exception e) {
					throw new RuntimeException("Could not create key", e);
				}
				switch((int)(Math.random() * 3)) {
					case 0:
						map.put(key, key);
						break;
					default:
						Object ret = map.get(key);
						if(ret != null && !ret.equals(key))
							throw new RuntimeException("Returned value is wrong!");
				}
			}
		};
		long heapInitial = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
		multiThreadTest(runnable, threads, times, 0, mapName + " - " + times + " times");
		log.debug("Size = " + map.size());
		Log.finish();
        long heapBefore = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
        System.gc();
        long heapAfter = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
        log.debug("Initial Heap: " + (heapInitial / 1024) + " KB; Before GC: " + (heapBefore/1024) + " KB; After GC: " + (heapAfter/1024) + " KB");
        Log.finish();
	}

	protected void testMapIntegerKeys(final ConcurrentMap<Object,Object> map, String mapName, final int variations) throws Throwable {
		Callable<Integer> keyCreator = new Callable<Integer>() {
			public Integer call() throws Exception {
				return (int)(Math.random() * variations);
			}
		};
		testMap(map, mapName, 10, 1000000, keyCreator);
	}
	protected void testMapStringKeys(final ConcurrentMap<Object,Object> map, String mapName, final int length) throws Throwable {
		Callable<String> keyCreator = new Callable<String>() {
			public String call() throws Exception {
				char[] ch = new char[length];
				for(int i = 0; i < ch.length; i++)
					ch[i] = (char)((Math.random() * 10) + 'A');
				return new String(ch);
			}
		};
		testMap(map, mapName, 10, 1000000, keyCreator);
	}
	protected void testMapArrayKeys(final ConcurrentMap<Object,Object> map, String mapName, final int length) throws Throwable {
		Callable<byte[]> keyCreator = new Callable<byte[]>() {
			public byte[] call() throws Exception {
				byte[] key = new byte[3];
				for(int i = 0; i < key.length; i++)
					key[i] = (byte)((Math.random() * 10));
				return key;
			}
		};
		testMap(map, mapName, 10, 1000000, keyCreator);
	}
	//@Test
	public void testAtomicInteger() throws Throwable {
		final AtomicInteger value = new AtomicInteger();
		final Holder<Integer> holder = new Holder<Integer>();
		Runnable runnable = new Runnable() {
			public void run() {
				holder.setValue(value.incrementAndGet());
			}
		};
		multiThreadTest(runnable, 10, 1000000, 0, "AtomicInteger");
		log.debug("Value = " + holder.getValue());
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
