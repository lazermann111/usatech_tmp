package simple.test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.Format;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.collections.list.CursorableLinkedList;
import org.apache.commons.collections.list.CursorableLinkedList.Cursor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import simple.app.ServiceException;
import simple.db.ConnectionGroup;
import simple.db.DataLayerException;
import simple.io.Log;
import simple.lang.Decision;
import simple.mq.peer.ConsumedPeerMessage;
import simple.text.NumberAsWordsFormat;
import simple.util.ArrayListIterator;
import simple.util.CollectionUtils;
import simple.util.CompositeSet;
import simple.util.Cycle;
import simple.util.IntegerRangeSet;
import simple.util.OptimizedIntegerRangeSet;
import simple.util.RecentExceptionSet;
import simple.util.SimpleLinkedHashMap;
import simple.util.SingletonListIterator;
import simple.util.concurrent.Cabinent;
import simple.util.concurrent.ConcurrentQueueBuffer;
import simple.util.concurrent.CreationException;
import simple.util.concurrent.LockOnWriteList;
import simple.util.concurrent.QueueBuffer;
import simple.util.concurrent.SingleLockArrayCycle;
import simple.xml.MapXMLLoader;
import simple.xml.MapXMLLoader.ParentMap;

public class CollectionsTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testCursor() {
		CursorableLinkedList list = new CursorableLinkedList();
		list.add(1);
		Cursor cursor = list.cursor();
		while(cursor.hasNext()) {
			Object o = cursor.next();
			log.info("Got item: " + o);
			cursor.remove();
		}

	}

	@Test
	public void testLambda() {
		ConcurrentMap<Long, String> map = new ConcurrentHashMap<>();
		final AtomicInteger gen = new AtomicInteger();
		final Format format = new NumberAsWordsFormat();
		final Random random = new Random();
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				for(int i = 0; i < 10; i++) {
					int v = random.nextInt(15);
					String s = map.computeIfAbsent((long) v, k -> {
						log.info("CREATING VALUE FOR " + k);
						return format.format(k) + ":" + gen.getAndIncrement();
					});
					log.info("Got '" + s + "' for " + v);
				}
			}
		};
		Thread[] threads = new Thread[12];
		for(int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(runnable, "TestThread-" + (i + 1));
			threads[i].start();
		}
		for(int i = 0; i < threads.length; i++) {
			try {
				threads[i].join();
			} catch(InterruptedException e) {
			}
		}

	}

	@Test
	public void testCabinent() {
		final AtomicInteger gen = new AtomicInteger();
		final Format format = new NumberAsWordsFormat();
		final Cabinent<Long, String> cab = new Cabinent<Long, String>() {
			@Override
			protected void closeItem(String item) throws Exception {
				// do nothing
			}

			@Override
			protected String createItem(Long key) throws Exception {
				return format.format(key) + ":" + gen.getAndIncrement();
			}

			@Override
			protected void configureItem(Long key, String item, boolean initial) throws Exception {
				// do nothing
			}
		};
		cab.setMaxActive(10);
		cab.setMaxKeys(12);
		cab.setMaxInactivity(10000L);
		cab.setMaxWait(2000L);
		cab.setMaxIdle(5);
		cab.schedulePurgeJob(Executors.newSingleThreadScheduledExecutor(), 1000L);

		final Random random = new Random();
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				for(int i = 0; i < 100; i++)
					getAndReturnItem(random.nextInt(15), cab, 100);
			}
		};
		Thread[] threads = new Thread[12];
		for(int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(runnable, "TestThread-" + (i + 1));
			threads[i].start();
		}
		for(int i = 0; i < threads.length; i++) {
			try {
				threads[i].join();
			} catch(InterruptedException e) {
			}
		}
	}

	protected void getAndReturnItem(long id, Cabinent<Long, String> cab, long delay) {
		String item;
		try {
			item = cab.getItem(id);
		} catch(CreationException | TimeoutException | InterruptedException e) {
			log.warn("Could not get item for key " + id, e);
			return;
		}
		log.info("Obtained item '" + item + "' for key " + id);
		log.info("Cabinent size=" + cab.getNumKeys() + " with " + cab);
		if(delay > 0)
		try {
				Thread.sleep(delay);
		} catch(InterruptedException e) {
			//
		}
		cab.returnItem(id, item);
	}

	@Test
	public void testParentMap() throws IOException, SAXException, ParserConfigurationException {
		String xml = "<AprivaPosXml><Credit MessageType=\"Response\" Version=\"\" ProcessingCode=\"Sale\"><Stan>1</Stan><AuthId>V45459</AuthId><CardType>Visa</CardType><ResponseCode>00</ResponseCode><AddressVerifyResponse><ResponseCode>X</ResponseCode><ResponseText>Zip and Address Match</ResponseText></AddressVerifyResponse><CardVerificationResponse ><ResponseCode>Y</ResponseCode><ResponseText>Mismatch</ResponseText></CardVerificationResponse ><ResponseText>Approved</ResponseText></Credit></AprivaPosXml>";
		MapXMLLoader xmlLoader = new MapXMLLoader();
		ParentMap result = xmlLoader.load(new ByteArrayInputStream(xml.getBytes()));
		log.info("Result=\n" + result);
	}

	@Test
	public void testAddToArray() throws Exception {
		String[] array = { "zero", "one", "two", "Three", null };
		String[] newArray = CollectionUtils.add(array, array.length - 1, "four");
		log.info("From " + Arrays.toString(array) + " to " + Arrays.toString(newArray));
	}

	@Test
	public void testFilterArray() throws Exception {
		Integer[] orig = new Integer[26];
		for(int i = 0; i < orig.length; i++)
			orig[i] = i;
		Integer[] filtered = CollectionUtils.filter(orig, new Decision<Integer>() {
			@Override
			public boolean decide(Integer argument) {
				return argument != null && (argument % 3) == 0;
			}
		});
		log.info("Filtered = " + Arrays.toString(filtered));
	}

	@Test
	public void testMatchPrefix() throws Exception {
		String[] strings = new String[] {
				"a.b.g.q",
				"a.b",
				"a",
				"a.c",
				"a.c.r.q",
				"a.b.g.s",
				"a.b.g",
				"a.b.g.a"				
		};
		String[] tests = new String[] {
				"a.b.g.q",
				"a.b",
				"a",
				"a.c",
				"a.c.r.q",
				"a.b.g.s",
				"a.b.g",
				"a.b.g.a",
				"a.d",
				"able",
				"a.b.g.q.2",
				"a.b.3"
		};
		Set<String> set = new TreeSet<String>(Arrays.asList(strings));
		
		for(String s : tests) {
			String v = CollectionUtils.bestPrefixMatch(set, s);
			log.info("'" + s + "' = '" + v);
		}
	}
	
	@Test
	public void testPrefixString() throws Exception {
		String[] strings = new String[] {
				"a.b.g.q",
 "a.b",
				"a",
				"a.c",
				"a.c.r.q",
				"a.b.g.s",
				"a.b.g",
				"a.b.g.a"				
		};
		for(String s : strings) {
			String v = getOrCreateClosest(s);
			log.info("'" + s + "' = '" + v + "'; Cache=" + queueSettingsCache);
		}
	}
	
	protected final ConcurrentNavigableMap<String, String> queueSettingsCache = new ConcurrentSkipListMap<String, String>();

	protected String getOrCreateClosest(String queueName) {
		Map.Entry<String, String> closest = queueSettingsCache.floorEntry(queueName);
		while(closest != null) {
			if(queueName.startsWith(closest.getKey()))
				return closest.getValue();
			if(closest.getKey().length() < queueName.length())
				break; // didn't find one
			closest = queueSettingsCache.lowerEntry(closest.getKey());
		}		
		String old = queueSettingsCache.putIfAbsent(queueName, queueName);
		return old == null ? queueName : old;
	}
	@Test
	public void testQueueBuffer() throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		reader.readLine();
		//testQueueBuffer(new RingQueueBuffer(250), 10, 10, 100, 5L, 1L);
		log.info("---------------------------------------------------------");
		reader.readLine();
		testQueueBuffer(new ConcurrentQueueBuffer(250), 10, 10, 100, 5L, 1L);
	}

	protected void testQueueBuffer(final QueueBuffer queueBuffer, int consumers, int publishers, final int count, final long consumePause, final long publishPause) {
		final AtomicInteger failures = new AtomicInteger();
		final AtomicInteger instanceGen = new AtomicInteger();
		final boolean[] consumedArray = new boolean[count * publishers];
		final long[] latency = new long[count * publishers];
		final AtomicLong consumeTime = new AtomicLong();
		final AtomicLong publishTime = new AtomicLong();
		class StubConsumedPeerMessage extends ConsumedPeerMessage {
			protected final AtomicInteger consumptionCount = new AtomicInteger(0);
			protected final int instanceNum;

			public StubConsumedPeerMessage(int instanceNum) {
				super("test:" + instanceNum, null);
				this.instanceNum = instanceNum;
			}

			@Override
			public boolean isRedeliveryTracked() {
				return false;
			}

			@Override
			public boolean markInprocess() {
				return false;
			}

			@Override
			public void markComplete(ConnectionGroup connGroup) throws SQLException, DataLayerException {
			}

			@Override
			public boolean isHybrid() {
				return false;
			}

			@Override
			public boolean isDbMessage() {
				return false;
			}

			@Override
			public void acknowledge(boolean success) {
			}
		}
		final int ccnt = (int) ((count * publishers) / (double) consumers);
		final AtomicInteger consumedCount = new AtomicInteger();

		Runnable consumer = new Runnable() {
			@Override
			public void run() {
				StubConsumedPeerMessage message;
				for(int i = 0; i < ccnt; i++) {
					try {
						long start = System.nanoTime();
						message = (StubConsumedPeerMessage) queueBuffer.take();
						long end = System.nanoTime();
						consumeTime.addAndGet(end - start);
						long lat = end - message.getPosted();
						latency[message.instanceNum - 1] = lat;
						consumedArray[message.instanceNum - 1] = true;
						if(message.consumptionCount.getAndIncrement() > 0) {
							failures.incrementAndGet();
							log.warn("Message " + message.getInstanceId() + " posted " + lat + " ns ago by " + message.getDataSourceName() + " has already been consumed!!!");
						} else {
							consumedCount.incrementAndGet();
						}
						log.debug("Consumed message " + message.getInstanceId() + " posted " + lat + " ns ago by " + message.getDataSourceName() + "; QueueBuffer has " + queueBuffer.size() + " entries");
					} catch(InterruptedException e) {
						log.warn("Consumer was interrupted", e);
					}
					try {
						Thread.sleep((long) (Math.random() * consumePause));
					} catch(InterruptedException e) {
					}
				}
			}
		};
		Thread[] consumerThreads = new Thread[consumers];
		for(int i = 0; i < consumers; i++) {
			consumerThreads[i] = new Thread(consumer, "ConsumerThread-" + (i + 1));
			consumerThreads[i].start();
		}
		Runnable publisher = new Runnable() {
			@Override
			public void run() {
				ConsumedPeerMessage message;
				for(int i = 0; i < count; i++) {
					try {
						message = new StubConsumedPeerMessage(instanceGen.incrementAndGet());
						message.setDataSourceName(Thread.currentThread().getName());
						message.setPosted(System.nanoTime());
						long start = System.nanoTime();
						queueBuffer.put(message, false);
						long end = System.nanoTime();
						publishTime.addAndGet(end - start);
						log.debug("Published message " + message.getInstanceId() + "; QueueBuffer has " + queueBuffer.size() + " entries");
					} catch(InterruptedException e) {
						log.warn("Consumer was interrupted", e);
					}
					try {
						Thread.sleep((long) (Math.random() * publishPause));
					} catch(InterruptedException e) {
					}
				}
			}
		};
		Thread[] publisherThreads = new Thread[publishers];
		for(int i = 0; i < publishers; i++) {
			publisherThreads[i] = new Thread(publisher, "PublisherThread-" + (i + 1));
			publisherThreads[i].start();
		}
		for(int i = 0; i < publishers; i++) {
			try {
				publisherThreads[i].join();
			} catch(InterruptedException e) {
			}
		}
		for(int i = 0; i < consumers; i++) {
			try {
				consumerThreads[i].join();
			} catch(InterruptedException e) {
			}
		}
		log.info("All threads have completed for " + queueBuffer.getClass().getName());
		if(failures.get() > 0)
			log.error("There were consumption overlaps");
		if(consumedCount.get() + queueBuffer.size() != count * publishers)
			log.error("Published " + count * publishers + " messages, but " + queueBuffer.size() + " are in the queue still and only " + consumedCount.get() + " were consumed");
		log.info("Average latency = " + avg(latency));
		log.info("Spent " + consumeTime.get() + " ns withdrawing from queuebuffer");
		log.info("Spent " + publishTime.get() + " ns depositing to queuebuffer");

	}

	protected static long avg(long[] values) {
		BigInteger total = BigInteger.ZERO;
		for(long value : values)
			total = total.add(BigInteger.valueOf(value));
		return total.divide(BigInteger.valueOf(values.length)).longValue();
	}
	@Test
	public void testSimpleLinkedHashMap() throws Exception {
		int[] sizes = new int[] { 0, 1, 5 };
		Format format = new NumberAsWordsFormat();
		for(int size : sizes) {
			SimpleLinkedHashMap<Integer, String> map = new SimpleLinkedHashMap<Integer, String>();
			for(int i = 0; i < size; i++)
				map.put(i, format.format(i));
			log.info("Created map: " + map);
			for(int i = 0; i < size; i++)
				log.debug("Key " + i + "=" + map.get(i));
			List<Map.Entry<Integer, String>> entryList = map.getOrderedEntries();
			for(int r = 0; r < size + 1; r++) {
				log.debug("Getting listIterator for index " + r);
				ListIterator<Map.Entry<Integer, String>> li = entryList.listIterator(r);
				for(int k = 0; k < size * 2; k++) {
					log.debug("NextIndex: " + li.nextIndex() + "; PrevIndex: " + li.previousIndex() + "; HasNext: " + li.hasNext() + "; HasPrev: " + li.hasPrevious());
					if(!li.hasNext()) {
						if(!li.hasPrevious())
							break;
						else
							log.debug("Prev=" + li.previous());
					} else if(!li.hasPrevious() || Math.random() < 0.5)
						log.debug("Next=" + li.next());
					else
						log.debug("Prev=" + li.previous());
				}
			}
		}
	}

	@Test
	public void testIntegerRangeSet() throws Exception {
		String[] rangeStrings = new String[] {
			"1|2|3|4|5|6|7",
			"1-4|2-6|7",
			"15-20|27-34|14-16|30-39|16|3|40",
			"3|1|4-9|7-9|33-39|39-41"
		};
		IntegerRangeSet irs = new OptimizedIntegerRangeSet();
		for(String rs : rangeStrings) {
			log.debug("Parsing '" + rs + "'");
			irs.clear();
			irs.addRanges(rs);
			log.debug("RangeSet = '" + irs + "'");
			SortedSet<Integer> ss = new TreeSet<Integer>(irs);
			log.debug("Size = " + irs.size() + " (should be " + ss.size() + "); Elements=" + ss);
			/*for(Integer i : ss) {
				if(!irs.contains(i))
					log.warn("IntegerRangeSet does not contain " + i + " as it should");
				if(Math.random() > 0.7) {
					log.debug("Removing " + i);
					irs.remove(i);
				}
			}
			*/
			for(Iterator<Integer> iter = irs.iterator(); iter.hasNext(); ) {
				Integer i = iter.next();
				if(!irs.contains(i))
					log.warn("IntegerRangeSet does not contain " + i + " as it should");
				if(Math.random() > 0.7) {
					log.debug("Removing " + i);
					iter.remove();
				}
			}
			log.debug("Modified RangeSet = '" + irs + "'");
			ss = new TreeSet<Integer>(irs);
			log.debug("Modified Size = " + irs.size() + " (should be " + ss.size() + "); Elements=" + ss);
		}
	}

	@Test
	public void testRecentExceptionsSet() throws Exception {
		RecentExceptionSet set = new RecentExceptionSet(5);
		for(int i = 0; i < 20; i++) {
			Throwable e;
			switch(i%3) {
				case 0:
					e = new ServiceException("Same", new IllegalStateException());
					break;
				case 1:
					e = new ServiceException("Same", new IOException());
					break;
				case 2: default:
					e = new ServiceException("Same", new IOException());
					break;
						
			}
			if(set.add(e))
				log.debug("Added", e);
		}
		log.info("Result: " + set);
	}
	
	@Test
	public void testCycle() throws Exception {
		class Key {
			public int index;
			public String value;
			@Override
			public String toString() {
				return value;
			}
		}
		final Cycle<Key> cycle = new SingleLockArrayCycle<Key>(3, Key.class);
		Thread t = new Thread() {
			public void run() {
				while(true) {
					int size = cycle.size();
					StringBuilder sb = new StringBuilder("Cycle = [");
					for(int i = 0; i < size; i++)
						sb.append(cycle.next()).append(", ");
					sb.setCharAt(sb.length() - 2, ']');
					log.debug(sb.toString());
					try {
						Thread.sleep(200);
					} catch(InterruptedException e) {
						//
					}
				}
			}
		};
		t.setDaemon(true);
		t.start();
		for(int i = 0; i < 1000; i++) {
			if(!cycle.isEmpty() && Math.random() > 0.75) {
				Key k = cycle.next();
				cycle.remove(k.index, k);
			} else {
				Key k = new Key();
				k.value = String.valueOf(i);
				k.index = cycle.add(k);
			}
			try {
				Thread.sleep(10);
			} catch(InterruptedException e) {
				//
			}		
		}
	}
	@Test
	public void testListIterators() throws Exception {
		log.debug("-1 % 3 = " + (-1%3));
		testListIterator(new SingletonListIterator<String>("A"));
		String[] array = new String[5];
		for(int i = 0; i < array.length; i++)
			array[i] = String.valueOf((char)('A' + i));
		testListIterator(new ArrayListIterator<String>(array));
	}

	protected void testListIterator(ListIterator<String> listIter) throws Exception {
		//assert false : "assertions test";
		log.debug("At Beginning " + listIter);
		while(listIter.hasNext()) {
			String o = listIter.next();
			assert listIter.hasPrevious() : "should have previous and doesn't";
			assert listIter.previousIndex() == listIter.nextIndex() - 1 : "previous and next indices don't match";

			log.debug("Element " + o + " at position " + listIter.previousIndex());
		}
		log.debug("At End " + listIter);
		while(listIter.hasPrevious()) {
			String o = listIter.previous();
			assert listIter.hasNext() : "should have next and doesn't";
			assert listIter.previousIndex() == listIter.nextIndex() - 1 : "previous and next indices don't match";

			log.debug("Element " + o + " at position " + listIter.nextIndex());
		}

		log.debug("At Beginning " + listIter);
		while(listIter.hasNext()) {
			String o = listIter.next();
			assert listIter.hasPrevious() : "should have previous and doesn't";
			assert listIter.previousIndex() == listIter.nextIndex() - 1 : "previous and next indices don't match";

			log.debug("Element " + o + " at position " + listIter.previousIndex());
		}
		log.debug("At End " + listIter);
		while(listIter.hasPrevious()) {
			String o = listIter.previous();
			assert listIter.hasNext() : "should have next and doesn't";
			assert listIter.previousIndex() == listIter.nextIndex() - 1 : "previous and next indices don't match";

			log.debug("Set Element " + o + " at position " + listIter.nextIndex() + " to " + o + " - 1");
			listIter.set(o + "-1");
		}
		log.debug("At Beginning " + listIter);
		while(listIter.hasNext()) {
			String o = listIter.next();
			assert listIter.hasPrevious() : "should have previous and doesn't";
			assert listIter.previousIndex() == listIter.nextIndex() - 1 : "previous and next indices don't match";

			log.debug("Element " + o + " at position " + listIter.previousIndex());
		}
		log.debug("At End " + listIter);
	}
	@Test
	public void testCompositeSet() throws Exception {
		CompositeSet<Integer> set = new CompositeSet<Integer>();
		for(int i = 0; i < 5; i++){
			Set<Integer> sub = new HashSet<Integer>();
			for(int k = 0; k < 4; k++) {
				sub.add(i * 3 + k);
			}
			set.merge(sub);
		}

		String s = "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]";
		assert s.equalsIgnoreCase(set.toString());
		log.debug(set);
	}
	@Test
	public void testList() throws Throwable {
		final List<Integer> list = new LockOnWriteList<Integer>();
		final AtomicInteger next = new AtomicInteger();
		final AtomicInteger nextEven = new AtomicInteger();
		for(int i = 0; i < 10; i++)
			list.add(next.incrementAndGet());
		multiThreadTest(new Runnable() {
			public void run() {
				int n;
				int k;
				Integer[] arr;
				switch((int)(16*Math.random())) {
					case 0: case 13:
						//add one number
						n = next.incrementAndGet();
						log.debug("Adding " + n);
						list.add(n);
						break;
					case 1: case 15:
						//add several numbers
						n = 3 + (int)(5*Math.random());
						arr = new Integer[n];
						k = next.getAndAdd(n) + 1;
						for(int i = 0; i < n; i++)
							arr[i] = k + i;
						log.debug("Adding " + k + " to " + (k+n-1));
						list.addAll(Arrays.asList(arr));
						break;
					case 2:
						// remove one even
						n = nextEven.addAndGet(2);
						log.debug("Removing " + n);
						list.remove((Object)n);
						break;
					case 3:
						//remove several evens
						n = 2 + (int)(4*Math.random());
						arr = new Integer[n];
						k = nextEven.getAndAdd(n*2) + 2;
						for(int i = 0; i < n; i++)
							arr[i] = k + 2*i;
						log.debug("Removing " + k + " to " + (k+(n-1)*2));
						list.removeAll(Arrays.asList(arr));
						break;
					case 4: case 8: case 12:
						log.info("List=" + list.toString());
						break;
					case 5: case 9: case 14:
						StringBuilder sb = new StringBuilder();
						sb.append("List Reverse=[");
						for(ListIterator<Integer> iter = list.listIterator(list.size()); iter.hasPrevious(); ) {
							sb.append(iter.previous());
							sb.append(',');
						}
						sb.setLength(sb.length() - 1);
						sb.append(']');
						log.info(sb.toString());
						break;
					case 6: case 10:
						arr = list.toArray(new Integer[list.size()]);
						log.info("ListAsPreArray=" + Arrays.toString(arr));
						break;
					case 7: case 11:
						Object[] arr2 = list.toArray();
						log.info("ListAsObjArray=" + Arrays.toString(arr2));
						break;
				}
			}
		}, 5, 50, 0, "LockOnWriteListTest");

		checkList(list, true);
	}

	protected void checkList(List<Integer> list, boolean strict) {
		Integer[] arr = list.toArray(new Integer[list.size()]);
		Arrays.sort(arr);
		int n = -1;
		boolean hitEvens = false;
		for(int i : arr) {
			if((!hitEvens || !strict) && i == n + 2 && (i % 2) == 1) {
				n += 2;
			} else if(i == n + 1) {
				n++;
				hitEvens = true;
			} else {
				//trouble
				throw new IllegalStateException("Expected " + (hitEvens ? "" : "" + (n+2) + " or " ) + (n + 1) + " but got " + i + " from:\n" + Arrays.toString(arr));
			}
		}
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
