package simple.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import simple.io.Log;
import simple.io.logging.AbstractLog;
import simple.text.MessageFormat;

public class LogTest
{
  public static final int FATAL = 1000; 
  public static final int ERROR = 900;
  public static final int WARN = 800;
  public static final int INFO = 700;
  public static final int DEBUG = 600;
  public static final int TRACE = 500;
  public static final int ALL = 0;
  
	static class LogEntry
	{
		int level;
		Object msg;
		Throwable exception;
		
		LogEntry(int level, Object msg, Throwable exception)
		{
			this.level = level;
			this.msg = msg;
			this.exception = exception;
		}
		
		public String toString()
		{
			return String.format("%s,%s,%s", level, msg, exception);
		}
	}
	
	static LogEntry lastLog;
	
	static int currentLevel = INFO;
	
	static Log log = new AbstractLog()
	{
		@Override
		protected void logMessage(int level, Object msg, Throwable exception)
		{
			lastLog = new LogEntry(level, msg, exception);
		}
		
		@Override
		protected int getTraceLevel()
		{
			return TRACE;
		}
		
		@Override
		protected int getDebugLevel()
		{
			return DEBUG;
		}

		@Override
		protected int getInfoLevel()
		{
			return INFO;
		}
		
		@Override
		protected int getWarnLevel()
		{
			return WARN;
		}

		@Override
		protected int getErrorLevel()
		{
			return ERROR;
		}

		@Override
		protected int getFatalLevel()
		{
			return FATAL;
		}
		
		@Override
		protected int getEffectiveLevel()
		{
			return currentLevel;
		}
	};
	
	@Test
	public void testMessageNoArg()
	{
		String msg = "test";
		
		log.info(msg);
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals(msg, lastLog.msg);
	}
	
	@Test
	public void testFormattedMessageNoArg()
	{
		String msg = "test={0}";
		
		log.info(msg);
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals(msg, lastLog.msg);
	}	
	
	@Test
	public void testNonFormattedMessageAndThrowable()
	{
		String msg = "test";
		Throwable t = new Throwable(msg);
		
		log.info(msg, t);
		
		assertNotNull(lastLog);
		assertEquals(msg, lastLog.msg);
		assertEquals(t, lastLog.exception);
	}
	
	@Test
	public void testFormattedMessageWithOneArg()
	{
		String msg = "test={0}";
		
		log.info(msg, "1");
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals("test=1", lastLog.msg);
	}
	
	@Test
	public void testFormattedMessageWithMultipleArgs()
	{
		String msg = "arg1={0}, arg2={1}";
		
		log.info(msg, 1, 2);
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals("arg1=1, arg2=2", lastLog.msg);
	}
	
	@Test
	public void testFormattedMessageWithSingleExceptionArg()
	{
		String msg = "arg1={0}";
		Exception e = new Exception("test");
		
		log.info(msg, e);
		
		assertNotNull(lastLog);
		assertNotNull(lastLog.exception);
		assertEquals(e, lastLog.exception);
		
		// it would be nice to insert the exception for {0} but there's a chance that 
		// could cause issues with existing code if it happens to have a valid message format token in the message 
		
		//assertEquals("arg1=java.lang.Exception: test", lastLog.msg);
		assertEquals("arg1={0}", lastLog.msg);
	}
	
	@Test
	public void testNonFormattedMessageWithSingleExceptionArg()
	{
		String msg = "test";
		Exception e = new Exception("test");
		
		log.info(msg, e);
		
		assertNotNull(lastLog);
		assertNotNull(lastLog.exception);
		assertEquals(e, lastLog.exception);
		assertEquals(msg, lastLog.msg);
	}
	
	@Test
	public void testFormattedMessageWithMultipleArgsAndException()
	{
		String msg = "arg1={0}, arg2={1}";
		Exception e = new Exception("test");
		
		log.info(msg, e, 711);
		
		assertNotNull(lastLog);
		assertNotNull(lastLog.exception);
		assertEquals(e, lastLog.exception);
		assertEquals("arg1=java.lang.Exception: test, arg2=711", lastLog.msg);
	}
	
	@Test
	public void testFormattedMessageTooManyArgs()
	{
		String msg = "arg1={0}, arg2={1}";
		
		log.info(msg, 1, 2, 3);
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals("arg1=1, arg2=2", lastLog.msg);
	}
	
	@Test
	public void testFormattedMessageNotEnoughArgs()
	{
		String msg = "arg1={0}, arg2={1}";
		
		log.info(msg, 1);
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals("arg1=1, arg2=", lastLog.msg);
	}
	
	@Test
	public void testFormattedMessageWithMultipleArgsAndExtraException()
	{
		String msg = "arg1={0}, arg2={1}";
		Exception e = new Exception("test");
		
		log.info(msg, 1, 2, e);
		
		assertNotNull(lastLog);
		assertNotNull(lastLog.exception);
		assertEquals(e, lastLog.exception);
		assertEquals("arg1=1, arg2=2", lastLog.msg);
	}
	
	@Test
	public void testIntegerFormat()
	{
		String result = new MessageFormat("{0,number}").format(new Object[] {2});
		assertEquals(result, "2");
	}
	
	@Test
	public void testFormattedMessageWithFormattedArgs() throws Exception
	{
		String msg = "arg1={0,date,YYYY}, arg2={1,number,#}, arg2={1,number}";
		Date date = new SimpleDateFormat("MM/DD/YYYY Z").parse("01/01/2014 EST");
		
		log.info(msg, date, 1.9);
		
		assertNotNull(lastLog);
		assertNull(lastLog.exception);
		assertEquals("arg1=2014, arg2=2, arg2=1.9", lastLog.msg);
	}	
	
}
