package simple.test;

import java.io.Console;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JDBCConnectionTest {
	protected static DateFormat logDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
	protected static Console console = System.console();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args == null || args.length < 2)
			System.err.println("Expecting two arguments <driver class> and <url>");
		else
			testJDBC(args[0], args[1], args.length > 2 ? args[2] : null, args.length > 3 ? args[3].toCharArray() : null);
		System.exit(0);
	}

	protected static Connection createConnection(String driver, String url, String username, char[] password) {
		log_info("Loading class '" + driver + "'...");
		try {
			Class.forName(driver);
		} catch(Throwable e) {
			log_error("Could not load class '" + driver + "'", e);
			return null;
		}
		log_info("Class loaded; getting connection for '" + url + "'...");
		if(username == null)
			username = prompt("Database Username:");
		else if(console != null)
			write("Database Username: " + username);
		if(password == null)
			password = promptSecret("Database Password:");
		try {
			return DriverManager.getConnection(url, username, new String(password));
		} catch(SQLException e) {
			log_error("Could not get connection for '" + url + "'", e);
			return null;
		}
	}


	protected static void testJDBC(String driver, String url, String username, char[] password) {
		log_info("Loading class '" + driver + "'...");
		try {
			Class.forName(driver);
		} catch(Throwable e) {
			log_error("Could not load class '" + driver + "'", e);
			return;
		}
		log_info("Class loaded; getting connection for '" + url + "'...");
		if(username == null)
			username = prompt("Database Username:");
		else if(console != null)
			write("Database Username: " + username);
		if(password == null)
			password = promptSecret("Database Password:");
		Connection conn;
		try {
			conn = DriverManager.getConnection(url, username, new String(password));
		} catch(SQLException e) {
			log_error("Could not get connection for '" + url + "'", e);
			return;
		}
		String product;
		try {
			product = conn.getMetaData().getDatabaseProductName();
		} catch(SQLException e) {
			log_error("Could not get product name for '" + url + "'", e);
			return;
		}
		String testSql;
		if(product.toUpperCase().contains("ORACLE")) {
			testSql = "SELECT * FROM DUAL";
		} else if(product.toUpperCase().contains("POSTGRESQL")) {
			testSql = "VALUES(1)";
		} else {
			testSql = null;
		}
		if(testSql != null) {
			try {
				conn.setAutoCommit(true);
				Statement st = conn.createStatement();
				try {
					boolean hasResults = st.execute(testSql);
					if(hasResults) {
						ResultSet rs = st.getResultSet();
						try {
							if(rs.next())
								log_info("Validation query returned at least 1 row");
							else
								log_info("Validation query returned 0 rows");
						} finally {
							rs.close();
						}
					} else {
						log_info("Validation query suceeded");
					}
				} finally {
					st.close();
				}
			} catch(SQLException e) {
				log_error("Validation query  for '" + url + "' failed", e);
			}
		}
		try {
			DatabaseMetaData md = conn.getMetaData();
			StringBuilder sb = new StringBuilder();
			sb.append("Metadata Properties:\n");
			/*
			for(Map.Entry<String, ?> entry : ReflectionUtils.toPropertyMap(conn.getMetaData()).entrySet()) {
				sb.append(entry.getKey()).append(" = ").append(entry.getValue()).append("\n");
			}*/
			for(Method method : DatabaseMetaData.class.getMethods()) {
				if(method.getParameterTypes().length == 0) {
					sb.append(method.getName()).append(" = ");
					try {
						sb.append(method.invoke(md));
					} catch(IllegalArgumentException e) {
						sb.append(e);
					} catch(IllegalAccessException e) {
						sb.append(e);
					} catch(InvocationTargetException e) {
						sb.append(e);
					}
					sb.append("\n");
				}
			}
			log_info(sb.toString());
		} catch(SQLException e) {
			log_error("Gettting metadata properties failed", e);
		}
	}

	protected static void write(String message) {
		console.format(message);
	}
	protected static String prompt(String message) {
		return console.readLine(message);
	}

	protected static char[] promptSecret(String message) {
		return console.readPassword(message);
	}

	protected static void log_info(String message) {
		synchronized(logDateFormat) {
			System.out.print(logDateFormat.format(new Date()));
			System.out.print(' ');
			System.out.println(message);
		}
	}

	protected static void log_error(String message, Throwable exception) {
		synchronized(logDateFormat) {
			System.out.print(logDateFormat.format(new Date()));
			System.out.print(' ');
			System.out.println(message);
			if(exception != null)
				exception.printStackTrace(System.out);
		}
	}
}
