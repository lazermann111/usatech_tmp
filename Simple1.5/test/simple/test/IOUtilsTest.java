package simple.test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.UnmappableCharacterException;
import java.nio.file.FileSystems;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.mail.internet.AddressException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.usatech.util.PortedUtils;

import simple.app.SimpleConfiguration;
import simple.io.ByteArrayUtils;
import simple.io.EncodingInputStream;
import simple.io.EnhancedBufferedReader;
import simple.io.HeapBufferStream;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.ReaderInputStream;
import simple.io.StartAtPrefixInputStream;
import simple.io.TLVParser;
import simple.io.UUEncode;
import simple.io.WriterOutputStream;
import simple.mail.EmailInfo;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class IOUtilsTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	@Test
	public void testFromBCD() {
		byte[] result = ByteArrayUtils.fromBCD(ByteArrayUtils.fromHex("B4055011111111129D18121015432112345678F2"));
		log.info("Got result: " + new String(result) + "\nHex: " + StringUtils.toHex(result));
	}

	@Test
	public void testToBCD() throws UnmappableCharacterException {
		byte[] result = ByteArrayUtils.toBCD(";4055011111111129=18121015432112345678?2".getBytes());
		log.info("Got result: " + StringUtils.toHex(result));
	}

	@Test
	public void testTLVParser() throws IOException {
		TLVParser parser = new TLVParser();/*
											parser.registerTag("DF8167", 0);
											parser.registerTag("DF8168", 0);
											parser.registerTag("DF8169", 0);
											parser.registerTag("DF816A", 0);
											parser.registerTag("DF816F", 0);
											parser.registerTag("DF69", 0);
											*/
		byte[] source = ByteArrayUtils
				.fromHex(//"FC81D0E981B5DF81678198EC7736D319913150F30036BC639A864C1B00980734306439E59B2077835391364CEDBD2E49645DD9E2F24212C2880C7612C309ECCB335ADABB31E35E491BCB81F95E25E039E910171DA59AD56D22600BC9449BC99403A45B44B1DB630F8C03F32F546C9B19C1D5C2DEE70BFD2371C6E834A4CA955C9936B61F81A18699628568F54B6E3B3F913D7D811CE85EB10797076B0BFAC05EC09AE5DF81690101DF816A0A0000002002641C400002DF816F0100DF816A0A0000002002641C400002DF8168020011DF690180"
				 "FC81E8E981CDDF816781B0802C5B0036A40AACF99F5D97B40A9F1EBE98E24B1A8B24451C764AD0EC872A863924B48CFCBF21045F4AB5DB759E9F86347A46626DD825F773BFE56686DC9A1D69CE102CAED87F6D27F232925C2C83B5DBF5E0355139315FD564BCAF6B79EC00599AD608F3D78528BC3AD639DCBEEB64DF316035C38E3CFA2D154890609C39CFF087CDB47C41F975E565DBA115E290485618ED794479BD161CBCF2331690CEFED141B3FC3B37D5A7427BFC700A47AE9ADF81690101DF816A0A0000002002641C400002DF816F0100DF816A0A0000002002641C400002DF8168020000DF690100"
				);
		Map<byte[], byte[]> values = parser.parse(source, 6, source.length - 6);
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<byte[], byte[]> entry : values.entrySet()) {
			StringUtils.appendHex(sb, entry.getKey());
			sb.append('=');
			StringUtils.appendHex(sb, entry.getValue());
			sb.append(',').append(' ');
		}
		sb.setLength(sb.length() - 2);
		sb.append('}');
		log.info(sb);
	}

	@Test
	public void testReadWriteFileChannel() throws IOException {
		FileChannel fc = FileChannel.open(FileSystems.getDefault().getPath(System.getProperty("user.dir", "."), "FileChannelTest.txt"), StandardOpenOption.CREATE, StandardOpenOption.READ, StandardOpenOption.WRITE);
		for(int i = 0; i < 3; i++) {
			Reader r = Channels.newReader(fc, "UTF-8");
			String content = IOUtils.readFully(r);
			log.info("Read " + content.length() + " chars");
			// r.close();
			fc.position(0);
			Writer w = Channels.newWriter(fc, "UTF-8");
			content = "Hello this is a test (#" + (i + 1) + ") to see what is written. Then read. then writen then read. Is this showing up okay? " + StringUtils.pad("", '-', i * 10, Justification.RIGHT);
			w.write(content);
			w.flush();
			log.info("Wrote " + content.length() + " chars");
			// w.close();
			fc.truncate(fc.position());
			fc.position(0);
		}
		fc.close();
	}

	@Test
	public void testRemoveLastModified() {
		String[] uris = new String[] {
				"D:\\Development\\Java Projects\\ReportGenerator\\..\\usalive\\web\\css\\usalive-app-style.css?t=1417721788722",
				"css\\usalive-app-style.css?t=141772178872",
				"nothing to remove",
				"hello?stuff=8&fluff=7&t=123456789&gob=nob&nothing=something",
				"hello?stuff=8&fluff=7&t=123456789&gob=nob&nothing=something#fragment and more??not sobut&t=023902139",
				"css\\usalive-app-style.css?t=141772178872#gragment",
				"css\\usalive-app-style.css?hello=nothing&t=141772178872#gragment",
				"css\\usalive-app-style.css?t=141772178872&hello=nothing#gragment",
				
		};
		for(String uri : uris)
			log.info("URI '" + uri + "' = '" + IOUtils.removeLastModifiedFromUri(uri) + "'");
	}

	@Test
	public void testWriterOutputStream() throws IOException {
		File orig = new File(System.getProperty("user.home"), "Documents/notepad-working.txt");
		File copy = new File(orig.getAbsolutePath() + ".COPY");
		InputStream in = new FileInputStream(orig);
		try {
			WriterOutputStream wos = new WriterOutputStream(new FileWriter(copy), false);
			try {
				long tot = IOUtils.copy(in, wos);
				log.info("Copied " + tot + " bytes");
			} finally {
				wos.flush();
				wos.close();
			}
		} finally {
			in.close();
		}
	}

	@Test
	public void testEncodingInputStream() throws IOException {
		File orig = new File(System.getProperty("user.home"), "Documents/notepad-working.txt");
		File copy = new File(orig.getAbsolutePath() + ".COPY");
		EncodingInputStream in = new EncodingInputStream(new FileReader(orig));
		try {
			OutputStream out = new FileOutputStream(copy);
			try {
				long tot = IOUtils.copy(in, out);
				log.info("Copied " + tot + " bytes");
			} finally {
				out.flush();
				out.close();
			}
		} finally {
			in.close();
		}
	}

	@Test
	public void testReaderInputStream() throws IOException {
		File orig = new File(System.getProperty("user.home"), "Documents/notepad-working.txt");
		File copy = new File(orig.getAbsolutePath() + ".COPY");
		ReaderInputStream in = new ReaderInputStream(new FileReader(orig));
		try {
			OutputStream out = new FileOutputStream(copy);
			try {
				long tot = IOUtils.copy(in, out);
				log.info("Copied " + tot + " bytes");
			} finally {
				out.flush();
				out.close();
			}
		} finally {
			in.close();
		}
	}

	@Test
	public void testPagination() {
		printPagination(1, 5);
		printPagination(1, 13);
		printPagination(1, 25);
		printPagination(6, 9);
		printPagination(8, 11);
		printPagination(10, 15);
		printPagination(10, 25);
		printPagination(45, 48);
		printPagination(33, 33);
		printPagination(25, 50);
		printPagination(90, 100);

	}

	protected void printPagination(int pageIndex, int pageCount) {
		if(pageIndex > pageCount)
			pageIndex = pageCount;
		int rangeStart;
		if(pageIndex < 8)
			rangeStart = 1;
		else if(pageCount < 16)
			rangeStart = 1;
		else if(pageIndex > pageCount - 8)
			rangeStart = pageCount - 14;
		else
			rangeStart = pageIndex - 7;
		int rangeEnd;
		if(pageIndex > pageCount - 8)
			rangeEnd = pageCount;
		else if(pageCount < 16)
			rangeEnd = pageCount;
		else if(pageIndex < 8)
			rangeEnd = 15;
		else
			rangeEnd = pageIndex + 7;
		log.info("For page " + pageIndex + " of " + pageCount + ": " + rangeStart + " - " + rangeEnd);
	}

	@Test
	public void testParseMailAddresses() throws AddressException {
		String recips = ",bad@blank.com,,bad2@blank.com,bad3@blank.com,";
		log.info("Addresses=" + Arrays.toString(EmailInfo.toAddresses(Arrays.asList(EmailInfo.parseRecips(recips)))));
	}
	
	@Test
	public void testReplaceRandomAccessBytes() throws IOException {
		int cols = 80;
		int rows = 50;
		File file = new File("C:\\Users\\bkrug\\Documents\\raf_test_1.txt");
		byte[] bytes = new byte[cols];
		Arrays.fill(bytes, (byte) 46);
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		raf.setLength(0);
		for(int i = 0; i < rows; i++) {
			raf.write(bytes);
			raf.write((byte) 13);
		}

		long[] pos = new long[] { 13, 50, 101, 240, 260, 333 };
		int[] len = new int[] { 3, 10, 5, 20, 1, 30 };
		int replace = 5;
		for(int i = 0; i < pos.length; i++) {
			bytes = new byte[len[i]];
			Arrays.fill(bytes, (byte) 43);
			IOUtils.replaceRandomAccessBytes(raf, pos[i], pos[i] + replace, bytes);
		}
	}

	@Test
	public void testEnhancedBufferedReader() throws IOException {
		class MyHeapBufferStream extends HeapBufferStream {
			public MyHeapBufferStream() {
				super(1024, 1024, true);
			}
			public byte[] getBuffer() {
				return buffer;
			}
		}
		MyHeapBufferStream bufferStream = new MyHeapBufferStream();
		Writer writer = new OutputStreamWriter(bufferStream.getOutputStream());
		Random random = new Random();
		for(int i = 0; i < 100; i++) {
			int len = random.nextInt(10);
			for(int p = 0; p < len; p++)
				writer.append((char) (65 + random.nextInt(26)));
			switch(random.nextInt(3)) {
				case 0:
					writer.append('\n');
					break;
				case 1:
					writer.append('\r');
					break;
				case 2:
					writer.append('\r').append('\n');
					break;
			}
		}
		/*
		int len = random.nextInt(10);
		for(int p = 0; p < len; p++)
			writer.append((char) (65 + random.nextInt(26)));
			*/
		writer.flush();
		log.info("Wrote " + bufferStream.getLength() + " bytes");
		EnhancedBufferedReader reader = new EnhancedBufferedReader(new InputStreamReader(bufferStream.getInputStream()));
		int length = (int)bufferStream.getLength();
		ByteArrayOutputStream baos = new ByteArrayOutputStream(length);
		writer = new OutputStreamWriter(baos);
		String line;
		while((line=reader.readLine()) != null) {
			writer.write(line);
			String eol = reader.readEOL();
			if(eol != null)
				writer.write(eol);
		}
		writer.flush();
		byte[] origBytes = new byte[length];
		System.arraycopy(bufferStream.getBuffer(), 0, origBytes, 0, length);
		if(!Arrays.equals(origBytes, baos.toByteArray())) {
			log.warn("original and result differ");
		} else
			log.info("Test passed");
	}

	@Test
	public void testBufferStreamFile() throws IOException, InterruptedException {
		String content = "This is the message that we will put in the BUffer Stream and see about writing and reading it alot\n";
		HeapBufferStream bufferStream = new HeapBufferStream(16, 16, false);
		bufferStream.getOutputStream().write(content.getBytes());
		bufferStream.getOutputStream().write(content.getBytes());
		System.out.println("RESULT:");
		IOUtils.copy(bufferStream.getInputStream(), System.out);
		System.out.println("--------------------");
		bufferStream = new HeapBufferStream(16, 16, true);
		bufferStream.getOutputStream().write(content.getBytes());
		bufferStream.getOutputStream().write(content.getBytes());
		System.out.println("RESULT #1:");
		IOUtils.copy(bufferStream.getInputStream(), System.out);
		System.out.println("--------------------");
		System.out.println("RESULT #2:");
		IOUtils.copy(bufferStream.getInputStream(), System.out);
		System.out.println("--------------------");
		InputStream in = bufferStream.getInputStream();
		in.mark(Integer.MAX_VALUE);
		System.out.println("RESULT #3:");
		IOUtils.copy(in, System.out);
		System.out.println("--------------------");
		in.reset();
		System.out.println("RESULT #4:");
		IOUtils.copy(in, System.out);
		System.out.println("--------------------");
	}

	@Test
	public void testBatFile() throws IOException, InterruptedException {
		String batchFile = "D:\\Development\\Java Projects\\KeyManagerService\\test_src\\reloader"; // test/batch101
		String cmd = IOUtils.findExecutableFile(new File(batchFile)).getAbsolutePath();
		List<String> command = new ArrayList<String>();
		command.add(cmd);
		RuntimeMXBean rmx = ManagementFactory.getRuntimeMXBean();
		command.add(System.getProperty("java.home"));
		command.add(rmx.getClassPath());
		// command.add(StringUtils.join(rmx.getInputArguments(), " "));
		command.addAll(rmx.getInputArguments());
		ProcessBuilder pb = new ProcessBuilder(command);
		Process p = pb.start();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if(IOUtils.copy(p.getErrorStream(), baos) > 0) {
			log.warn("Got error from process: " + baos.toString());
		}
		baos = new ByteArrayOutputStream();
		if(IOUtils.copy(p.getInputStream(), baos) > 0) {
			log.info("Got output from process: " + baos.toString());
		}
		int ec = p.waitFor();
		log.info("Process exitted with code " + ec);
	}

	@Test
	public void testConfigValue() throws IOException, ConfigurationException {
		HeapBufferStream bufferStream = new HeapBufferStream();
		OutputStream out = bufferStream.getOutputStream();
		PrintWriter pw = new PrintWriter(out);
		pw.println("Test1=G(WDR2>B/c[1/LBg");
		pw.println("Test2=Nothing");
		pw.flush();
		pw.close();
		PropertiesConfiguration config = new PropertiesConfiguration();
		config.load(bufferStream.getInputStream());
		log.info("Test1=`" + config.getString("Test1") + "'");
	}

	@Test
	public void testConfiguration() throws ConfigurationException {
		FileConfiguration config = new SimpleConfiguration();
		config.load("test/TestConfig1.properties");
		log.info("\n" + config.toString());
		config.setProperty("simple.monitor.RemoteAppMonitor.listenPort", 9999);
		config.setProperty("first.EXTRA", " a bunch of stuff to have ");
		config.setProperty("\u0000first\tESCAPED ", " E\tS\\C\nA\"P=E{D$ ");
		config.setProperty("second.file.stuff", "blah blah blah");
		config.save();
	}

	@Test
	public void testUUEncode() throws Exception {
		for(int i = 0; i < 49; i++) {
			int len = (i + (2 - (i-1)%3)) / 3 * 4;
			log.debug(i + " * 4 / 3 = " + len + " then " + len + " * 3 / 4 = " + (len * 3 / 4));
		}

		byte[][] values = new byte[][] {
			new byte[] {0,1,2,3,4,5,6,7,8,9,10},
			"abcdefghi".getBytes(),
			"032\n\n\r\t\b".getBytes(),
			"gather".getBytes(),
			"okay this is now working okay but if not then not".getBytes(),
			new byte[] {127, -126, 122, -78, 43, 16},
		};
		for(byte[] bytes : values) {
			log.info("Encoding bytes '" + StringUtils.toHex(bytes) + "'");
			byte[] encoded = UUEncode.encode(bytes);
			log.info("Encoded       '" + StringUtils.toHex(encoded) + "'");
			//log.info("AltEncoded    '" + StringUtils.toHex(UUEncode.encodeAlt(bytes)) + "'");
			log.info("LegacyEncoded '" + StringUtils.toHex(PortedUtils.uuencode(bytes).getBytes()) + "'");
			byte[] decoded = UUEncode.decode(encoded);
			log.info("Decoded '" + StringUtils.toHex(decoded) + "'");
		}


		for(int i = 0; i < 100; i++) {
			byte[] bytes = new byte[1 + (int)(Math.random() * 100)];
			for(int k = 0; k < bytes.length; k++)
				bytes[k] = (byte)(Math.random() * 256);
			log.info("Encoding bytes '" + StringUtils.toHex(bytes) + "'");
			byte[] encoded = UUEncode.encode(bytes);
			byte[] lencoded = PortedUtils.uuencode(bytes).getBytes();
			byte[] decoded = UUEncode.decode(encoded);
			if(!Arrays.equals(encoded, lencoded))
				throw new Exception("Encoding for '" + StringUtils.toHex(bytes) + " (" + bytes.length + " bytes) differs");
			if(!Arrays.equals(bytes, decoded))
				throw new Exception("Decoded for '" + StringUtils.toHex(bytes) + " (" + bytes.length + " bytes) differs");

			log.info("Encoded       '" + StringUtils.toHex(encoded) + "'");
			log.info("LegacyEncoded '" + StringUtils.toHex(PortedUtils.uuencode(bytes).getBytes()) + "'");
			log.info("Decoded '" + StringUtils.toHex(decoded) + "'");
		}
	}

	@Test
	public void testZipDirectory() throws Exception {
		File tmpFile;
		try {
			tmpFile = File.createTempFile("zip-test-", ".zip");
		} catch(IOException e) {
			throw new SQLException("Could not create temp file", e);
		}
		OutputStream out = new FileOutputStream(tmpFile);
		long bytes = IOUtils.zip(new File("E:\\Java Projects\\AppLayer\\db\\AppLayerDB"), out);
		out.close();
		log.info("Zipped " + bytes + " bytes to '" + tmpFile.getAbsolutePath() + "'");
	}
	@Test
	public void testInterruptReader() throws Exception {
		final InputStream in = System.in;
		Thread thread = new Thread() {
			@Override
			public void run() {
				log.debug("Starting reader thread...");
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String line;
				try {
					while((line=br.readLine()) != null) {
						log.debug("Got input: " + line);
					}
				} catch(IOException e) {
					log.warn("Error reading input", e);
				}
				log.debug("Exitting reader thread...");
			}
		};
		thread.start();
		Thread.sleep(5000);
		log.debug("Trying to close input");
		//in.close();
		thread.interrupt();
		log.debug("Done");
	}
	@Test
	public void testCompress() throws Exception {
		//assert false;
		Random random = new Random();
		int[] lengths = new int[] {10,20,30,40,50,60,70,80,90,100,120,140,160,180,200,250,300,400,500,600,700,800,900,1000};
		/*
		String[] texts = new String[] { "abcdefghijklmnopqrstuvwxyz",
				"1234567890",
				"This is a sentence that is a little longer to see if we can compress and then decompress it.",
				"",

		};
		for(String s : texts) {*/
		for(int l : lengths) {
			byte[] orig = new byte[l];
			random.nextBytes(orig);
			byte[] bytes = IOUtils.compressByteArray(orig);
			log.debug("Compressed " + orig.length + " bytes to " + bytes.length + " bytes");
			byte[] decomp = IOUtils.decompressByteArray(bytes);
			assert Arrays.equals(orig, decomp);
		}
	}

	@Test
	public void testStartAtPrefixInputStream() throws Exception {
		testPrefix(512, false, "def");
		testPrefix(12, false, "DEF");
		testPrefix(62, false, "1");
		testPrefix(512, true, "def");
		testPrefix(12, true, "DEF");
		testPrefix(62, true, "1");
	}

	protected void testPrefix(int arraySize, final boolean markSupported, String prefix) throws Exception {
		byte[] arr = new byte[arraySize];
		for(int i = 0 ; i < arr.length; i++) {
			if(i < 26) arr[i] = (byte)(65 + i);
			else if(i < 52) arr[i] = (byte)(71 + i);
			else arr[i] = (byte)(48 + i % 10);
		}
		InputStream in = new ByteArrayInputStream(arr) {
			@Override
			public boolean markSupported() {
				return markSupported;
			}
		};
		System.out.print("Original is '");
		IOUtils.copy(in, System.out);
		System.out.println("'");
		in = new ByteArrayInputStream(arr) {
			@Override
			public boolean markSupported() {
				return markSupported;
			}
		};
		StartAtPrefixInputStream sapis = new StartAtPrefixInputStream(in, prefix);
		System.out.print("Prefixed Stream is '");
		IOUtils.copy(sapis, System.out);
		System.out.println("'");
	}
	
	@Test
	public void testSocketConnect() {
		Socket socket = new Socket();
        InetSocketAddress address = new InetSocketAddress("devmst01", 5432);
        //socket.connect(address, 2);
        
	}
	@Test
	public void testURLs() throws Exception {
		new URL("this/is/a/fragment.html");
		new URL("http://this.com/is/a/fragment.html");
		new URL("https://this.com/is/a/fragment.html");
		new URL("#section");
		new URL("javascript: alert('Do you see me?');");
		
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
