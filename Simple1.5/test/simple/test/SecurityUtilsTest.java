package simple.test;

import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Provider.Service;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.KeyGenerator;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.auth.BasicScheme;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Base64;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.lang.Holder;
import simple.lang.InvalidValueException;
import simple.security.SecureHash;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;
import simple.security.crypt.CryptUtils.CopyEntryFilter;
import simple.security.crypt.EncryptedBytes;
import simple.security.crypt.KeySupply;
import simple.lang.sun.misc.CRC16;
import simple.swt.UIUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class SecurityUtilsTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	@Test
	public void hashSHA256Base64() throws GeneralSecurityException {
		String password = "nothing";
		byte[] salt = SecureHash.getSalt();
		byte[] hash = SecureHash.getHash(password.getBytes(), salt, "SHA-256");
		String passwordBase64 = new String(java.util.Base64.getEncoder().encode(hash));
		String saltBase64 = new String(java.util.Base64.getEncoder().encode(salt));
		log.info("Base 64 is\n" + passwordBase64 + ',' + saltBase64);
	}

	@Test
	public void addJDKCertsToTruststore() throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableEntryException {
		char[] targetKeystorePassword = "usatech".toCharArray();
		char[] sourceKeystorePassword = "changeit".toCharArray();
		final String prefix = "__jdk__";
		File targetFile = new File("..\\LayersCommon\\conf\\net\\truststore.ts");
		File sourceFile = new File("C:\\Program Files\\Java\\jdk1.6.0_45\\jre\\lib\\security\\cacerts");
		InputStream targetIn = new FileInputStream(targetFile);
		InputStream sourceIn = new FileInputStream(sourceFile);
		KeyStore targetKeystore;
		KeyStore sourceKeystore;
		try {
			targetKeystore = SecurityUtils.getKeyStore(targetIn, "JKS", null, targetKeystorePassword);
			sourceKeystore = SecurityUtils.getKeyStore(sourceIn, "JKS", null, sourceKeystorePassword);
		} finally {
			targetIn.close();
			sourceIn.close();
		}
		int cnt = CryptUtils.copyKeystoreEntries(sourceKeystore.aliases(), sourceKeystore, sourceKeystorePassword, targetKeystore, targetKeystorePassword, new CopyEntryFilter() {
			public String getTargetAlias(String sourceAlias, Entry sourceEntry) {
				return prefix + sourceAlias;
			}
		}, false);
		
		if(cnt > 0) {
			log.info("Copied  (" + cnt + ") entries into keystore '" + targetFile.getAbsolutePath() + "'");
			OutputStream out = new FileOutputStream(targetFile);
			try { 
				targetKeystore.store(out, targetKeystorePassword);
				out.flush();
			} finally {
				out.close();
			}
			log.info("Saved entries into keystore '" + targetFile.getAbsolutePath() + "'");			
		}
	}
	
	@Test
	public void checkAcctHash() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String accountCd = "639621150020002553";
		log.info("Hash1=" + StringUtils.toHex(SecureHash.getUnsaltedHash(accountCd.getBytes())));
		log.info("Hash1000=" + StringUtils.toHex(SecureHash.getHash(accountCd, null)));
	}

	@Test
	public void checkCreateSecurityCode() throws NoSuchAlgorithmException, NoSuchProviderException {
		SecureRandom random = SecurityUtils.getSecureRandom();
		String security_cd = "5721";
		byte[] salt = SecureHash.getSalt(random, SecureHash.SALT_LENGTH);
		log.info("HASH=" + StringUtils.toHex(SecureHash.getHash(security_cd.getBytes(), salt)));
		log.info("SALT=" + StringUtils.toHex(salt));
		log.info("ALG=" + SecureHash.HASH_ALGORITHM_ITERS);
	}

	protected byte[] guessNumericCode(int length, byte[] hash, byte[] salt, String desc) throws NoSuchAlgorithmException {
		byte[] guess = new byte[length];
		Arrays.fill(guess, (byte) '0');
		long start = System.currentTimeMillis();
		while(true) {
			byte[] guesshash = SecureHash.getHash(guess, salt);
			if(Arrays.equals(guesshash, hash)) {
				long duration = System.currentTimeMillis() - start;
				log.info("Found " + desc + ": " + new String(guess) + " after " + duration + "ms");
				return guess;
			}
			int i = guess.length - 1;
			for(; i >= 0; i--) {
				if(guess[i] == '9')
					guess[i] = '0';
				else {
					guess[i]++;
					break;
				}
			}
			if(i < 0) {
				long duration = System.currentTimeMillis() - start;
				log.info("Did not find " + desc + " after " + duration + "ms");
				return null;
			}
		}
	}

	@Test
	public void guessPasscode() throws NoSuchAlgorithmException {
		byte[] hash = ByteArrayUtils.fromHex("BE0163653D2A946A4DAB7D435F47990F63A686F3641426BA77DA9333C276BD0C");
		byte[] salt = ByteArrayUtils.fromHex("C76001069F8B84C079BA941944612AB64229F6CA476282717852383C05BC1AE5");
		guessNumericCode(8, hash, salt, "Passcode");
	}

	@Test
	public void guessSecurityCode() throws NoSuchAlgorithmException {
		byte[] hash = ByteArrayUtils.fromHex("97679D2ED778A99B7D2CF60E7705368CF8ECA7DC3C19ED081DBDDFC10379B63F");
		byte[] salt = ByteArrayUtils.fromHex("AAE807BAA914B2653DCF026DEB669E734704680426B99CF0209E521F39AB2382");
		guessNumericCode(4, hash, salt, "Security Code");
	}

	// 902712A9AFBB1239B4C757A1A1236D912276B502AA9BD4ABC1A8DF0816FCCBC4 4EC1240658BCBD0B7FD772F1880C46034C6F460B1C7639646040AA5B87672836 SHA-256/1000
	@Test
	public void checkHash() throws NoSuchAlgorithmException {
		String secret = "25242338";
		log.info("8-bit hash of password is " + (secret.hashCode() & 0xFF));
		String[] saltsHex = new String[] { 
 "C76001069F8B84C079BA941944612AB64229F6CA476282717852383C05BC1AE5"
		};
		String alg = "SHA-256/1000";
		for(String sh : saltsHex) {
			byte[] salt = ByteArrayUtils.fromHex(sh);
			byte[] hash = SecureHash.getHash(secret.getBytes(), salt, alg);
			log.info("SHA-256 hash of password is " + StringUtils.toHex(hash));
		}
	}
	@Test
	public void testPassword() throws NoSuchAlgorithmException {
		byte[] hash = ByteArrayUtils.fromHex("8204C8DC0BA8E8FE75DE2079978D61197ACDE443E637D62A93DF99EC99336CEF");
		byte[] salt = ByteArrayUtils.fromHex("6E74CC2ED2A3FDB82CD63E23FD1E55637657B211F281F9234DDFAAE251AA380B");
		String alg = "SHA-256/1000";
		for(String s : new String[] { "12559672" }) {
			byte[] testhash = SecureHash.getHash(s.getBytes(), salt, alg);
			if(Arrays.equals(hash, testhash))
				log.info("Found password: '" + s + "' with 8-bit hash of password = " + (s.hashCode() & 0xFF) + "!");
			else
				log.info("Hash " + StringUtils.toHex(testhash) + " does not match " + StringUtils.toHex(hash) + " for password '" + s + "'");
		}
	}

	@Test
	public void guessPassCode() throws NoSuchAlgorithmException {
		byte[] realHash = ByteArrayUtils.fromHex("504D4CC310211808DA739E5A95007A60611B02AC6B15DA1312C8A63C79412354");
		byte[] salt = ByteArrayUtils.fromHex("309F4ABF73F92561AC7F2C4FED2E62689EBB37220BD6EA2CDB3DB7806CDB57DB");
		byte[] securityCd = "00000000".getBytes();
		long start = System.currentTimeMillis();
		while(true) {
			byte[] guesshash = SecureHash.getHash(securityCd, salt);
			if(Arrays.equals(guesshash, realHash)) {
				long duration = System.currentTimeMillis() - start;
				log.info("Found Pass Code: " + new String(securityCd) + " after " + duration + "ms");
				return;
			}
			int i = securityCd.length - 1;
			for(; i >= 0; i--) {
				if(securityCd[i] == '9')
					securityCd[i] = '0';
				else {
					securityCd[i]++;
					break;
				}
			}
			if(i < 0) {
				long duration = System.currentTimeMillis() - start;
				log.info("Did not find Pass Code after " + duration + "ms");
				return;
			}
		}
	}

	@Test
	public void secureHashing() throws NoSuchAlgorithmException, NoSuchProviderException {
		String secret = "This is my very secret string";
		byte[] salt = SecureHash.getSalt(5);
		secureHashing(secret, "SHA-256/1000", salt);
		secureHashing(secret, "SHA-256 / 499  ", salt);
		secureHashing(secret, "  SHA-256/0", salt);
		secureHashing(secret, "SHA-256", salt);
		secureHashing(secret, "MD5/500", salt);
		secureHashing(secret, "SHA-256/500", salt);

	}

	@Test
	public void secureCardHashing() throws NoSuchAlgorithmException, NoSuchProviderException {
		String secret = "34478902";
		byte[] salt = ByteArrayUtils.fromHex("FB2FB1B4EB0BF2A1E851675E73206E5FAF8A9B3702BEF95C22731BF3132598F0");
		secureHashing(secret, "SHA-256/1000", salt);
	}

	@Test
	public void secureCardHashing3() throws NoSuchAlgorithmException {
		byte[] secret = ByteArrayUtils.fromHex("C32525F41E97C7AB4B3ABE5B9270C84FCBAE06CB388E4BB7C3F6BA4C6DB40F7F");
		byte[] hash = SecureHash.getHash(secret, null, "SHA-256/999");
		log.info("Got hash " + StringUtils.toHex(hash) + "; algorithm=SHA-256/999");

	}

	@Test
	public void secureCardHashing4() throws NoSuchAlgorithmException {
		for(int i = 0; i < 10; i++) {
			String card = "6396211" + i + "0020000015";
			byte[] hash = SecureHash.getUnsaltedHash(card.getBytes());
			log.info("For " + card + " got hash " + StringUtils.toHex(hash));
		}
	}
	@Test
	public void secureCardHashing5() throws NoSuchAlgorithmException {
		String card = "639621150020000015";
		byte[] hash = SecureHash.getUnsaltedHash(card.getBytes(), "SHA-1");
		log.info("For " + card + " got hash " + StringUtils.toHex(hash) + "; " + Base64.encodeBytes(hash, true));
	}

	@Test
	public void secureCardHashing2() throws NoSuchAlgorithmException, NoSuchProviderException {
		String secret1 = "6396212009905315827=9912006912213";
		String secret2 = "639621150020000015";

		byte[] salt = ByteArrayUtils.fromHex("5B3F79A09B16462C6FCFE3C01F49A4BB49AA1FFD236E96B6EDCFCA2241A6FB17");
		secureHashing(secret1, "SHA-256/1000", salt);
		secureHashing(secret2, "SHA-256", null);
		log.info("Got unsalted hash " + StringUtils.toHex(SecureHash.getUnsaltedHash(secret2.getBytes())));
	}

	@Test
	public void guessTrack() throws Exception {
		String fullCard = "6396210060623783541";// ;639621************=*********?;
		byte[] trackHash = ByteArrayUtils.fromHex("E0B0CC86A130EADA0C3CB1738F6187BC0B9A25B52D8420776D870FF79BF52C92");
		byte[] trackSalt = ByteArrayUtils.fromHex("B0461E7624DBC047659F8129A05DEA35EAE38AD3A2A509519C701CC638CAC93F");
		String trackAlg = "SHA-256/1000";
		int[] targetCrcs = new int[] { 7441, 58332 };
		Arrays.sort(targetCrcs);
		StringBuilder sb = new StringBuilder(40);
		// String expDate = "9912";
		char[] expDateArray = new char[4];
		for(int d = 0; d < 1200; d++) {
			sb.setLength(0);
			int year = 2010 + (d / 12);
			int month = 1 + (d % 12);
			expDateArray[0] = (char) ('0' + ((year % 100) / 10));
			expDateArray[1] = (char) ('0' + (year % 10));
			expDateArray[2] = (char) ('0' + (month / 10));
			expDateArray[3] = (char) ('0' + (month % 10));
			String expDate = new String(expDateArray);
			log.info("Trying with expDate=" + expDate);
			int issueCd = 0;
			for(int e = 0; e < 100000; e++) {
				sb.setLength(0);
				sb.append(fullCard).append('=').append(expDate);
				StringUtils.appendPadded(sb, String.valueOf(issueCd), '0', 2, Justification.RIGHT);
				StringUtils.appendPadded(sb, String.valueOf(e), '0', 5, Justification.RIGHT);
				int mod97 = mod97(sb.toString().replace('=', '0'));
				sb.append((mod97 / 10) % 10).append(mod97 % 10);
				String track = sb.toString();
				/*
				byte[] checkHash = SecureHash.getHash(track.getBytes(), trackSalt, trackAlg);
				if(Arrays.equals(trackHash, checkHash)) {
					log.info("Track Hash matches for " + track);
					CRC16 crc = new CRC16();
					for(byte b : track.getBytes())
						crc.update(b);
					log.info("CRC=" + crc.value);
					return;
				}
				*/
				CRC16 crc = new CRC16();
				for(byte b : track.getBytes())
					crc.update(b);
				if(Arrays.binarySearch(targetCrcs, crc.value) >= 0) {
					log.info("Track CRC " + crc.value + " matches for " + track);
					byte[] checkHash = SecureHash.getHash(track.getBytes(), trackSalt, trackAlg);
					if(Arrays.equals(trackHash, checkHash)) {
						log.info("And Track Hash matches for " + track);
						return;
					} else
						log.info("But Track Hash does not match for " + track);
				}
			}
		}
		/*
		char[] expDateArray = new char[4];
		for(int d = 0; d < 1200; d++) {
			sb.setLength(0);
			int year = 2010 + (d / 12);
			int month = 1 + (d % 12);
			expDateArray[0] = (char) ('0' + ((year % 100) / 10));
			expDateArray[1] = (char) ('0' + (year % 10));
			expDateArray[2] = (char) ('0' + (month / 10));
			expDateArray[3] = (char) ('0' + (month % 10));
			if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
				if(log.isDebugEnabled())
					log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
				return sb.toString();
			}
		}
		throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
		log.info("Found Track '" + trackData + "'");
		*/
		log.info("Could not find track for card '" + fullCard + "'");

	}

	@Test
	public void guessCard5() throws Exception {
		// 639621********0018, cd_hash=094DE062CFB0EEE3FA309408A3F7E9583AB2DC93BDAB77E54B5C0060C12CFCF4, raw_hash=A3040C4551E131CE0D4ED8E415D08697CAF1AC544FB3C8755AA49E14E790FB89,
		// raw_salt=789713E6BF4904BAFF18D32F8F09C882A9C12386229464C2951A5DA9527B1E01
		String rawAccountCd = "002000144";
		Integer fmtId = 1;
		byte[] accountCdHash = ByteArrayUtils.fromHex("CDE0C75CA51D7124E2631828F64B4D2EFE4F784C7AEE5A9568F76B0ECB56EB56");
		String trackData;
		Integer trackCrc;
		String merchantCd = "002";
		String expDate = "1012";
		String extra = "22426";
		byte[] trackHash = null;
		byte[] trackSalt = null;
		String trackAlg = "SHA-256/1000";
		int hashIterations = 1000;
		Holder<byte[]> accountCdHashHolder = new Holder<byte[]>();

		trackData = calculateTrack(rawAccountCd, accountCdHash, fmtId, merchantCd, expDate, extra, trackHash, trackSalt, trackAlg, hashIterations, accountCdHashHolder);
		log.info("Found Track '" + trackData + "'");
	}

	@Test
	public void guessCard() throws Exception {
		// 639621********0018, cd_hash=094DE062CFB0EEE3FA309408A3F7E9583AB2DC93BDAB77E54B5C0060C12CFCF4, raw_hash=A3040C4551E131CE0D4ED8E415D08697CAF1AC544FB3C8755AA49E14E790FB89,
		// raw_salt=789713E6BF4904BAFF18D32F8F09C882A9C12386229464C2951A5DA9527B1E01
		String rawAccountCd = "639621*********5297";
		Integer fmtId = 1;
		byte[] accountCdHash = ByteArrayUtils.fromHex("3E1B5A05AE46E5BE601DE3DE3AD3D7D3FA60CD109D8702F29C2817349EA01A7D");
		String trackData;
		Integer trackCrc;
		String merchantCd = null; // "046";
		String expDate = "9912";
		String extra = "94924";
		byte[] trackHash = ByteArrayUtils.fromHex("A3040C4551E131CE0D4ED8E415D08697CAF1AC544FB3C8755AA49E14E790FB89");
		byte[] trackSalt = ByteArrayUtils.fromHex("789713E6BF4904BAFF18D32F8F09C882A9C12386229464C2951A5DA9527B1E01");
		String trackAlg = "SHA-256/1000";
		int hashIterations = 1000;
		Holder<byte[]> accountCdHashHolder = new Holder<byte[]>();

		trackData = calculateTrack(rawAccountCd, accountCdHash, fmtId, merchantCd, expDate, extra, trackHash, trackSalt, trackAlg, hashIterations, accountCdHashHolder);
		log.info("Found Track '" + trackData + "'");
	}

	@Test
	public void guessCard2() throws Exception {
		// "639621150010001201"
		// 639621********0018, cd_hash=094DE062CFB0EEE3FA309408A3F7E9583AB2DC93BDAB77E54B5C0060C12CFCF4, raw_hash=A3040C4551E131CE0D4ED8E415D08697CAF1AC544FB3C8755AA49E14E790FB89,
		// raw_salt=789713E6BF4904BAFF18D32F8F09C882A9C12386229464C2951A5DA9527B1E01
		// String rawAccountCd = "639621********0018";
		String rawAccountCd = "639621*********1659";
		Integer fmtId = 0;
		byte[] accountCdHash = ByteArrayUtils.fromHex("063C6F28897204CCB046CE547C456389CD75D3B9ED4246436E547121EE4C4D1B");
		String trackData;
		Integer trackCrc;
		String merchantCd = "984";
		String expDate = "9912";
		String extra = "0005960";
		byte[] trackHash = ByteArrayUtils.fromHex("DCEBF44EF86B466F07286D3E27C960A1601B13B7DBD7C9DB3A98EE2AAC36C835");
		byte[] trackSalt = ByteArrayUtils.fromHex("98529F3B484D49BED35B39E2BC08E2F2A2125024B3012FEB7950BA7422656F94");
		String trackAlg = "SHA-256/1000";
		int hashIterations = 0;
		Holder<byte[]> accountCdHashHolder = new Holder<byte[]>();

		trackData = calculateTrack(rawAccountCd, accountCdHash, fmtId, merchantCd, expDate, extra, trackHash, trackSalt, trackAlg, hashIterations, accountCdHashHolder);
		log.info("Found Track '" + trackData + "'");
	}

	@Test
	public void guessCard3() throws Exception {
		// 639621*********1201
		// 639621150010001201
		// 639621********0018, cd_hash=094DE062CFB0EEE3FA309408A3F7E9583AB2DC93BDAB77E54B5C0060C12CFCF4, raw_hash=A3040C4551E131CE0D4ED8E415D08697CAF1AC544FB3C8755AA49E14E790FB89,
		// raw_salt=789713E6BF4904BAFF18D32F8F09C882A9C12386229464C2951A5DA9527B1E01
		String rawAccountCd = "639621********0566";// ;639621************=*********?;
		Integer fmtId = 1;
		byte[] accountCdHash = ByteArrayUtils.fromHex("4AE8172C50233CE58962A0CD23632EC4D98C54214A06AEF0E14D1EE79B41D3D0");
		String trackData;
		Integer trackCrc;
		String merchantCd = "011";
		String expDate = "9912";
		String extra = "32332";
		byte[] trackHash = ByteArrayUtils.fromHex("E0B0CC86A130EADA0C3CB1738F6187BC0B9A25B52D8420776D870FF79BF52C92");
		byte[] trackSalt = ByteArrayUtils.fromHex("B0461E7624DBC047659F8129A05DEA35EAE38AD3A2A509519C701CC638CAC93F");
		String trackAlg = "SHA-256/1000";
		int hashIterations = 0;
		Holder<byte[]> accountCdHashHolder = new Holder<byte[]>();

		trackData = calculateTrack(rawAccountCd, accountCdHash, fmtId, merchantCd, expDate, extra, trackHash, trackSalt, trackAlg, hashIterations, accountCdHashHolder);
		log.info("Found Track '" + trackData + "'");
	}


	@Test
	public void guessCard4() throws Exception {
		String maskedCardNumber = "639621*********8290";
		byte[] accountCdHash = ByteArrayUtils.fromHex("E0DE86CCFB0F66C6813E8C1344833C37058048E6B3721FEC82851891FB5336E0");
		int hashIterations = 0;
		String fullCard = findCardNumber(maskedCardNumber, accountCdHash, hashIterations);
		log.info("Found Card '" + fullCard + "'");
	}
	/*
	E911EBC19B9B9163110913FEC17C7E4386903C4F3D106C78A820110010BC7A42
	7AF3DCDA87529DBB725C8564F8FAC2C39723036A27EC0BA987694DE790277859
	A2B20E30A080EE81408AE70A5841EFCAC24B50BB7829F7BFE20FE602E2C5B070
	9BDB9C6B3A20E25F6728F84CC4E44F1427CEBBC4743D362B4058882F797F9647
	*/
	@Test
	public void getCheckDigit() {
		String orig = "639621150010001193";// "6396211*2560000153";
		byte[] array = orig.getBytes();
		setCheckDigit(array);
		String result = new String(array);
		log.info("Started with " + orig + " and ended with " + result);
	}

	@Test
	public void calcCardDigit() {
		// String orig = "60110009955000*3";
		// String orig = "540010002000400*";
		// String orig = "63962101588367484*";
		// String orig = "6396211588367484*";
		String orig = "543139000000000*";
		byte[] array = orig.getBytes();
		int i = orig.indexOf('*');
		int d = calcDigit(orig, i);
		array[i] = (byte) ('0' + d);
		String result = new String(array);
		log.info("Started with " + orig + " and ended with " + result);
	}

	@Test
	public void calcRandomCardDigit() {
		Random rand = new Random();
		for(int k = 0; k < 20; k++) {
			char[] array = new char[18];
			for(int a = 0; a < array.length - 1; a++)
				array[a] = (char) ('0' + rand.nextInt(10));

			array[array.length - 1] = '*';
			String orig = new String(array);
			// byte[] array = orig.getBytes();
			int i = orig.indexOf('*');
			int d = calcDigit(orig, i);
			// array[i] = (byte) ('0' + d);
			// String result = new String(array);
			log.info(orig + "= " + d);
			// /log.info("MOD10=" + CreditCardInfo.validateChecksum(pan));
		}
	}
	protected int calcDigit(String accountCd, int index) {
		int[] a = new int[accountCd.length()];
		for(int i = 0; i < a.length; i++)
			a[i] = Character.getNumericValue(accountCd.charAt(i));

		int len = a.length;
		int checksum = 0;
		int tmp;

		for(int i = (len % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i] * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = ((len + 1) % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i];
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}

		checksum = (10 - (checksum % 10)) % 10;
		if(((len - index) % 2) == 0)
			return (checksum % 2) == 0 ? checksum / 2 : (9 + checksum) / 2;
		return checksum;
	}

	protected String calculateTrack(String cardNumber, byte[] accountCdHash, int fmtId, String merchantCd, String expDate, String extra, byte[] trackHash, byte[] trackSalt, String trackAlg, int hashIterations, Holder<byte[]> hash0Holder) throws InvalidValueException, NoSuchAlgorithmException, Exception {
		if(!cardNumber.startsWith("639621")) {
			if(accountCdHash != null && cardNumber.length() >= 9 && cardNumber.indexOf('*') < 0) {
				byte[] cardArray;
				switch(fmtId) {
					case 0:
					case 2:
						cardArray = new byte[cardNumber.length() + 8];
						getBytesFromString("639621", 0, 6, cardArray, 0);
						cardArray[6] = (byte) ('0' + fmtId);
						getBytesFromString(cardNumber, 0, cardNumber.length(), cardArray, 7);
						setCheckDigit(cardArray);
						byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
						byte[] guessHashMatch;
						if(hashIterations > 0)
							guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
						else
							guessHashMatch = guessHash;
						if(Arrays.equals(accountCdHash, guessHashMatch)) {
							if(log.isDebugEnabled())
								log.debug("Found account cd for " + cardNumber);
							hash0Holder.setValue(guessHash);
							// calc track & crc
							StringBuilder sb = new StringBuilder(40);
							String fullCard = new String(cardArray);
							if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
								if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
									if(log.isDebugEnabled())
										log.debug("Track Hash matches for " + cardNumber);
									return sb.toString();
								}
								char[] expDateArray = new char[4];
								for(int d = 0; d < 1200; d++) {
									sb.setLength(0);
									int year = 2010 + (d / 12);
									int month = 1 + (d % 12);
									expDateArray[0] = (char) ('0' + ((year % 100) / 10));
									expDateArray[1] = (char) ('0' + (year % 10));
									expDateArray[2] = (char) ('0' + (month / 10));
									expDateArray[3] = (char) ('0' + (month % 10));
									if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
										if(log.isDebugEnabled())
											log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
										return sb.toString();
									}
								}
								throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
							}
							return sb.toString();
						}
						break;
					case 1:
						cardArray = new byte[cardNumber.length() + 9];
						getBytesFromString("639621", 0, 6, cardArray, 0);
						cardArray[6] = (byte) ('0' + fmtId);
						getBytesFromString(cardNumber, 0, cardNumber.length(), cardArray, 8);
						for(int pc = 0; pc < 10; pc++) {
							cardArray[7] = (byte) ('0' + pc);
							setCheckDigit(cardArray);
							guessHash = SecureHash.getUnsaltedHash(cardArray);
							if(hashIterations > 0)
								guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
							else
								guessHashMatch = guessHash;
							if(Arrays.equals(accountCdHash, guessHashMatch)) {
								if(log.isDebugEnabled())
									log.debug("Found account cd for " + cardNumber);
								hash0Holder.setValue(guessHash);
								// calc track & crc
								StringBuilder sb = new StringBuilder(40);
								String fullCard = new String(cardArray);
								if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
									if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
										if(log.isDebugEnabled())
											log.debug("Track Hash matches for " + cardNumber);
										return sb.toString();
									}
									char[] expDateArray = new char[4];
									for(int d = 0; d < 1200; d++) {
										sb.setLength(0);
										int year = 2010 + (d / 12);
										int month = 1 + (d % 12);
										expDateArray[0] = (char) ('0' + ((year % 100) / 10));
										expDateArray[1] = (char) ('0' + (year % 10));
										expDateArray[2] = (char) ('0' + (month / 10));
										expDateArray[3] = (char) ('0' + (month % 10));
										if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
											if(log.isDebugEnabled())
												log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
											return sb.toString();
										}
									}
									throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
								}
								return sb.toString();
							}
						}
						break;
					default:
						throw new InvalidValueException("Unrecognized format id " + fmtId, cardNumber);
				}
				throw new Exception("Could NOT find account cd from unmasked account innards for " + cardNumber);
			}
			throw new InvalidValueException("Unrecognized card prefix '" + cardNumber.substring(0, 6), cardNumber);
		}
		byte[] groupIDPadded;
		if(merchantCd == null)
			groupIDPadded = null;
		else
			groupIDPadded = StringUtils.pad(String.valueOf(Integer.parseInt(merchantCd.substring(merchantCd.length() - 3))), '0', 3, Justification.RIGHT).getBytes();
		byte[] cardArray = cardNumber.getBytes();
		switch(fmtId) {
			case 0:
			case 2:
				if(cardArray[6] == '*')
					cardArray[6] = (byte) ('0' + fmtId);
				int start;
				if(groupIDPadded == null)
					start = 7;
				else {
					for(int i = 7; i < 10; i++)
						if(cardArray[i] == '*')
							cardArray[i] = groupIDPadded[i - 7];
					start = 10;
				}
				for(; start < cardArray.length; start++)
					if(cardArray[start] == '*')
						break;
				int end = cardArray.length - 1;
				for(; end >= start; end--)
					if(cardArray[end] == '*')
						break;
				int baseChecksum = addChecksum(cardArray, 0, start) + addChecksum(cardArray, end + 1, cardArray.length);
				if(log.isDebugEnabled())
					log.debug("Must guess " + (end - start + 1) + " digits for " + cardNumber);
				for(int i = start; i <= end; i++)
					cardArray[i] = '0';
				int max = (int) Math.pow(10, (end - start + 1));
				int n;
				for(n = 1; n < max; n++) {
					int checksum = baseChecksum + addChecksum(cardArray, start, end + 1);
					if((checksum % 10) == 0) {
						byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
						byte[] guessHashMatch;
						if(hashIterations > 0)
							guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
						else
							guessHashMatch = guessHash;
						String fullCard = new String(cardArray);
						if(log.isDebugEnabled())
							log.debug("Trying " + fullCard);
						if(accountCdHash == null && !StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
							// calc track
							StringBuilder sb = new StringBuilder(40);
							if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
								hash0Holder.setValue(guessHash);
								if(log.isDebugEnabled())
									log.debug("Track Hash matches for " + cardNumber);
								return sb.toString();
							}
						} else if(Arrays.equals(accountCdHash, guessHashMatch)) {
							if(log.isDebugEnabled())
								log.debug("Found account cd for " + cardNumber);
							hash0Holder.setValue(guessHash);
							// calc track & crc
							StringBuilder sb = new StringBuilder(40);
							if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
								if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
									if(log.isDebugEnabled())
										log.debug("Track Hash matches for " + cardNumber);
									return sb.toString();
								}
								char[] expDateArray = new char[4];
								for(int d = 0; d < 1200; d++) {
									sb.setLength(0);
									int year = 2010 + (d / 12);
									int month = 1 + (d % 12);
									expDateArray[0] = (char) ('0' + ((year % 100) / 10));
									expDateArray[1] = (char) ('0' + (year % 10));
									expDateArray[2] = (char) ('0' + (month / 10));
									expDateArray[3] = (char) ('0' + (month % 10));
									if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
										if(log.isDebugEnabled())
											log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
										return sb.toString();
									}
								}
								throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
							}
							return fullCard;
						}
					}
					String s = StringUtils.pad(Integer.toString(n), '0', end - start + 1, Justification.RIGHT);
					getBytesFromString(s, 0, s.length(), cardArray, start);
				}
				throw new Exception("Could NOT find account cd after " + n + " guesses for " + cardNumber);
			case 1:
				if(cardArray[6] == '*')
					cardArray[6] = (byte) ('0' + fmtId);
				// cardArray[7] = process code and is unknown
				if(groupIDPadded == null)
					start = 8;
				else {
					for(int i = 8; i < 11; i++)
						if(cardArray[i] == '*')
							cardArray[i] = groupIDPadded[i - 8];
					start = 11;
				}

				for(; start < cardArray.length; start++)
					if(cardArray[start] == '*')
						break;
				end = cardArray.length - 1;
				for(; end >= start; end--)
					if(cardArray[end] == '*')
						break;
				baseChecksum = addChecksum(cardArray, 0, 7) + addChecksum(cardArray, 8, start) + addChecksum(cardArray, end + 1, cardArray.length);
				if(log.isDebugEnabled())
					log.debug("Must guess " + (end - start + 2) + " digits for " + cardNumber);
				cardArray[7] = '0';
				for(int i = start; i <= end; i++)
					cardArray[i] = '0';
				max = (int) Math.pow(10, (end - start + 2));
				for(n = 1; n < max; n++) {
					int checksum = baseChecksum + addChecksum(cardArray, start, end + 1) + addChecksum(cardArray, 7, 8);
					if((checksum % 10) == 0) {
						byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
						byte[] guessHashMatch;
						if(hashIterations > 0)
							guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
						else
							guessHashMatch = guessHash;
						String fullCard = new String(cardArray);
						if(log.isDebugEnabled())
							log.debug("Trying " + fullCard);
						if(accountCdHash == null && !StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
							// calc track
							StringBuilder sb = new StringBuilder(40);
							if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
								hash0Holder.setValue(guessHash);
								if(log.isDebugEnabled())
									log.debug("Track Hash matches for " + cardNumber);
								return sb.toString();
							}
						} else if(Arrays.equals(accountCdHash, guessHashMatch)) {
							if(log.isDebugEnabled())
								log.debug("Found account cd for " + cardNumber);
							hash0Holder.setValue(guessHash);
							// calc track
							StringBuilder sb = new StringBuilder(40);
							if(!StringUtils.isBlank(trackAlg) && trackHash != null && trackHash.length > 0) {
								if(testTrack(fullCard, expDate, extra, trackHash, trackSalt, trackAlg, sb)) {
									if(log.isDebugEnabled())
										log.debug("Track Hash matches for " + cardNumber);
									return sb.toString();
								}
								char[] expDateArray = new char[4];
								for(int d = 0; d < 1200; d++) {
									sb.setLength(0);
									int year = 2010 + (d / 12);
									int month = 1 + (d % 12);
									expDateArray[0] = (char) ('0' + ((year % 100) / 10));
									expDateArray[1] = (char) ('0' + (year % 10));
									expDateArray[2] = (char) ('0' + (month / 10));
									expDateArray[3] = (char) ('0' + (month % 10));
									if(testTrack(fullCard, new String(expDateArray), extra, trackHash, trackSalt, trackAlg, sb)) {
										if(log.isDebugEnabled())
											log.debug("Found real exp date: " + new String(expDateArray) + " for " + cardNumber);
										return sb.toString();
									}
								}
								throw new InvalidValueException("Track Hash Does NOT match for " + cardNumber + "; expdate=" + expDate, cardNumber);
							}
							return fullCard;
						}
					}
					String s = StringUtils.pad(Integer.toString(n), '0', end - start + 2, Justification.RIGHT);
					getBytesFromString(s, 0, 1, cardArray, 7);
					getBytesFromString(s, 1, s.length(), cardArray, start);
				}
				throw new Exception("Could NOT find account cd after " + n + " guesses for " + cardNumber);
			default:
				throw new InvalidValueException("Unrecognized format id " + fmtId, cardNumber);
		}
	}

	protected String findCardNumber(String maskedCardNumber, byte[] accountCdHash, int hashIterations) throws Exception {
		byte[] cardArray = maskedCardNumber.getBytes();
		int start = 0;
		for(; start < cardArray.length; start++)
			if(cardArray[start] == '*')
				break;
		int end = cardArray.length - 1;
		for(; end >= start; end--)
			if(cardArray[end] == '*')
				break;
		int baseChecksum = addChecksum(cardArray, 0, start) + addChecksum(cardArray, end + 1, cardArray.length);
		if(log.isDebugEnabled())
			log.debug("Must guess " + (end - start + 1) + " digits for " + maskedCardNumber);
		for(int i = start; i <= end; i++)
			cardArray[i] = '0';
		int max = (int) Math.pow(10, (end - start + 1));
		int n;
		for(n = 1; n < max; n++) {
			int checksum = baseChecksum + addChecksum(cardArray, start, end + 1);
			if((checksum % 10) == 0) {
				byte[] guessHash = SecureHash.getUnsaltedHash(cardArray);
				byte[] guessHashMatch;
				if(hashIterations > 0)
					guessHashMatch = SecureHash.getHash(guessHash, null, SecureHash.HASH_ALGORITHM, hashIterations - 1);
				else
					guessHashMatch = guessHash;
				String fullCard = new String(cardArray);
				// if(log.isDebugEnabled())
				// log.debug("Trying " + fullCard);
				if(Arrays.equals(accountCdHash, guessHashMatch)) {
					if(log.isDebugEnabled())
						log.debug("Found account cd for " + maskedCardNumber);
					return fullCard;
				}
			}
			String s = Integer.toString(n);
			getBytesFromString(s, 0, s.length(), cardArray, end - s.length() + 1);
		}
		throw new Exception("Could NOT find account cd after " + n + " guesses for " + maskedCardNumber);
	}

	public void setCheckDigit(byte[] cardArray) {
		int checksum = addChecksum(cardArray, 0, cardArray.length - 1);
		checksum = (10 - (checksum % 10)) % 10;
		cardArray[cardArray.length - 1] = (byte) ('0' + checksum);
	}
	@SuppressWarnings("deprecation")
	protected void getBytesFromString(String s, int srcBegin, int srcEnd, byte[] dst, int dstBegin) {
		s.getBytes(srcBegin, srcEnd, dst, dstBegin);
	}

	protected boolean testTrack(String cardNumber, String expDate, String extra, byte[] trackHash, byte[] trackSalt, String trackAlg, StringBuilder sb) throws NoSuchAlgorithmException {
		sb.append(cardNumber).append('=').append(expDate).append(extra);
		String track = sb.toString();
		byte[] checkHash = SecureHash.getHash(track.getBytes(), trackSalt, trackAlg);
		if(Arrays.equals(trackHash, checkHash))
			return true;
		int mod97 = mod97(sb.toString().replace('=', '0'));
		sb.append((mod97 / 10) % 10).append(mod97 % 10);
		track = sb.toString();
		checkHash = SecureHash.getHash(track.getBytes(), trackSalt, trackAlg);
		if(!Arrays.equals(trackHash, checkHash))
			return false;

		return true;
	}

	public int addChecksum(byte[] cardArray, int start, int end) {
		int len = cardArray.length;
		int checksum = 0;
		int tmp;

		for(int i = start + ((len + start) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]) * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = start + ((len + start + 1) % 2); i < end; i += 2) {
			tmp = Character.getNumericValue(cardArray[i]);
			checksum += tmp;
		}

		return checksum;
	}

	protected int mod97(String s) {
		BigInteger i = new BigInteger(s);
		return bi98.subtract(i.multiply(bi100).mod(bi97)).mod(bi97).intValue();
	}

	private static final BigInteger bi97 = BigInteger.valueOf(97);
	private static final BigInteger bi98 = BigInteger.valueOf(98);
	private static final BigInteger bi100 = BigInteger.valueOf(100);

	protected void secureHashing(String secret, String alg, byte[] salt) throws NoSuchAlgorithmException {
		byte[] hash = SecureHash.getHash(secret.getBytes(), salt, alg);
		// log.info("Got hash " + Base64.encodeBytes(hash, true) + " using salt=" + Base64.encodeBytes(salt, true) + "; algorithm=" + alg);
		log.info("Got hash " + StringUtils.toHex(hash) + " using salt=" + StringUtils.toHex(salt) + "; algorithm=" + alg);
	}
	@Test
	public void addCertToTruststore() throws FileNotFoundException, IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException {
		File certFile = new File("..\\LayersCommon\\conf\\usat_root_20250203.crt");
		File truststoreFile = new File("..\\LayersCommon\\conf\\kls\\truststore.ts");
		// File truststoreFile = new File("..\\LayersCommon\\conf\\apr\\truststore.ts");
		char[] password = "usatech".toCharArray();
		KeyStore keystore = SecurityUtils.getKeyStore(new FileInputStream(truststoreFile), "JKS", null, password);
		InputStream certStream = new FileInputStream(certFile);
		try {
			CryptUtils.importTrustedCertificate(keystore, "usat_root_20250203", certStream);
		} finally {
			certStream.close();
		}
		log.info("Added certificate to truststore '" + truststoreFile.getAbsolutePath() + "'");
		OutputStream out = new FileOutputStream(truststoreFile);
		try {
			keystore.store(out, password);
			out.flush();
		} finally {
			out.close();
		}
		log.info("Saved entries into keystore '" + truststoreFile.getAbsolutePath() + "'");
	}

	@Test
	public void copyCertToTruststore() throws FileNotFoundException, IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException {
		File keystoreFile = new File("D:\\Development\\Java Projects\\LayersCommon\\conf\\net\\keystore.ks");
		File truststoreFile = new File("D:\\Development\\Java Projects\\LayersCommon\\conf\\kls\\truststore.ts");
		String[] appNames = { "outauthlayer" };
		char[] password = "usatech".toCharArray();
		KeyStore keystore = SecurityUtils.getKeyStore(new FileInputStream(keystoreFile), "JKS", null, password);
		KeyStore trustStore = SecurityUtils.getKeyStore(new FileInputStream(truststoreFile), "JKS", null, password);
		final Pattern aliasPattern = Pattern.compile("mce\\.([^.]+)\\.data\\.([^.]+)");
		final Map<String, String> lastAliasSuffix = new HashMap<String, String>();
		for(Enumeration<String> en = keystore.aliases(); en.hasMoreElements();) {
			String alias = en.nextElement();
			Matcher matcher = aliasPattern.matcher(alias);
			if(matcher.matches()) {
				String appName = matcher.group(1);
				String aliasSuffix = matcher.group(2);
				String prevAliasSuffix = lastAliasSuffix.get(appName);
				if(prevAliasSuffix == null || prevAliasSuffix.compareTo(aliasSuffix) < 0) {
					lastAliasSuffix.put(appName, aliasSuffix);
				}
			}
		}
		for(String appName : appNames) {
			String aliasSuffix = lastAliasSuffix.get(appName);
			if(aliasSuffix != null) {
				String alias = "mce." + appName + ".data." + aliasSuffix;
				CryptUtils.copyKeystoreEntry(alias, keystore, password, trustStore, password, CryptUtils.CERTIFICATES_ONLY_ENTRY_FILTER);
				trustStore.store(new FileOutputStream(truststoreFile), password);
				log.info("Copied public key '" + alias + "' to truststore");
			}
		}
	}

	@Test
	public void viewEntry() throws FileNotFoundException, IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException {
		File keystoreFile = new File("D:\\Development\\Java Projects\\LayersCommon\\conf\\kls\\keystore.ks");
		String alias = "bkrugi7-applet";
		char[] password = "usatech".toCharArray();
		KeyStore keystore = SecurityUtils.getKeyStore(new FileInputStream(keystoreFile), "JKS", null, password);
		Entry entry = keystore.getEntry(alias, new PasswordProtection(password));
		log.info("Got Entry: " + entry);
	}

	@Test
	public void renewHostKeyInJKS() throws FileNotFoundException, IOException {
		File jksFile = new File("..\\LayersCommon\\conf\\net\\keystore.ks");
		String commonName = "localhost"; // InetAddress.getLocalHost().getCanonicalHostName();
		char[] password = "usatech".toCharArray();
		String alias = StringUtils.substringBefore(commonName, ".");
		renewKeyInJKS(jksFile, password, alias, commonName, 30, "USATWebServer");
	}

	@Test
	public void copyHostKeyToTruststore() throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableEntryException {
		char[] password = "usatech".toCharArray();
		String commonName = "localhost"; // InetAddress.getLocalHost().getCanonicalHostName();
		File targetFile = new File("..\\LayersCommon\\conf\\apr\\truststore.ts");
		File sourceFile = new File("..\\LayersCommon\\conf\\apr\\keystore.ks");
		InputStream targetIn = new FileInputStream(targetFile);
		InputStream sourceIn = new FileInputStream(sourceFile);
		KeyStore targetKeystore;
		KeyStore sourceKeystore;
		try {
			targetKeystore = SecurityUtils.getKeyStore(targetIn, "JKS", null, password);
			sourceKeystore = SecurityUtils.getKeyStore(sourceIn, "JKS", null, password);
		} finally {
			targetIn.close();
			sourceIn.close();
		}
		boolean changed = CryptUtils.copyKeystoreEntry(commonName, sourceKeystore, password, targetKeystore, password, CryptUtils.CERTIFICATES_ONLY_ENTRY_FILTER);

		if(changed) {
			log.info("Copied certificate for '" + commonName + "' into keystore '" + targetFile.getAbsolutePath() + "'");
			OutputStream out = new FileOutputStream(targetFile);
			try {
				targetKeystore.store(out, password);
				out.flush();
			} finally {
				out.close();
			}
			log.info("Saved entries into keystore '" + targetFile.getAbsolutePath() + "'");
		}
	}

	@Test
	public void copyHostKeyToKeystore() throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableEntryException {
		char[] password = "usatech".toCharArray();
		String commonName = "localhost"; // InetAddress.getLocalHost().getCanonicalHostName();
		File targetFile = new File("..\\LayersCommon\\conf\\apr\\keystore.ks");
		File sourceFile = new File("..\\LayersCommon\\conf\\net\\keystore.ks");
		InputStream targetIn = new FileInputStream(targetFile);
		InputStream sourceIn = new FileInputStream(sourceFile);
		KeyStore targetKeystore;
		KeyStore sourceKeystore;
		try {
			targetKeystore = SecurityUtils.getKeyStore(targetIn, "JKS", null, password);
			sourceKeystore = SecurityUtils.getKeyStore(sourceIn, "JKS", null, password);
		} finally {
			targetIn.close();
			sourceIn.close();
		}
		boolean changed = CryptUtils.copyKeystoreEntry(commonName, sourceKeystore, password, targetKeystore, password, null);

		if(changed) {
			log.info("Copied certificate for '" + commonName + "' into keystore '" + targetFile.getAbsolutePath() + "'");
			OutputStream out = new FileOutputStream(targetFile);
			try {
				targetKeystore.store(out, password);
				out.flush();
			} finally {
				out.close();
			}
			log.info("Saved entries into keystore '" + targetFile.getAbsolutePath() + "'");
		}
	}

	@Test
	public void renewJarSignerInJKS() throws FileNotFoundException, IOException {
		File jksFile = new File("D:\\Development\\Java Projects\\LayersCommon\\conf\\kls\\keystore.ks");
		String commonName = InetAddress.getLocalHost().getCanonicalHostName();
		char[] password = "usatech".toCharArray();
		String alias = "jarsigner";
		renewKeyInJKS(jksFile, password, alias, commonName, 30, "USATCodeSigning");
	}
	
	@Test
	public void renewMessageChainKeyInJKS() throws FileNotFoundException, IOException, KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException {
		File jksFile = new File("D:\\Development\\Java Projects\\LayersCommon\\conf\\kls\\keystore.ks");
		File trustFile = new File("D:\\Development\\Java Projects\\LayersCommon\\conf\\net\\truststore.ts");
		String appName = "keymgr";
		String serviceName = "KeyManagerService";
		final DateFormat aliasSuffixFormat = (DateFormat) ConvertUtils.getFormat("DATE", "yyyyMMdd");
		String aliasSuffix = aliasSuffixFormat.format(new Date());
		String alias = "mce." + appName + ".data." + aliasSuffix;
		char[] password = "usatech".toCharArray();
		KeyStore keystore = renewKeyInJKS(jksFile, password, alias, serviceName, 30, "USATWebServer");
		KeyStore trustStore = SecurityUtils.getKeyStore(new FileInputStream(trustFile), "JKS", null, password);
		CryptUtils.copyKeystoreEntry(alias, keystore, password, trustStore, password, CryptUtils.CERTIFICATES_ONLY_ENTRY_FILTER);
	}

	public static KeyStore renewKeyInJKS(File jksFile, char[] password, String alias, String commonName, int daysBefore, String templateName) throws FileNotFoundException, IOException {
		try {
			KeyStore keyStore = SecurityUtils.getKeyStore(new FileInputStream(jksFile), "JKS", null, password);
			Date expiration = new Date(System.currentTimeMillis() + (daysBefore * 24 * 60 * 60 * 1000L));
			String certificateAuthority = "usasubca.usatech.com";
			String webUser = System.getProperty("user.name");

			// check if expired
			Certificate[] certs = keyStore.getCertificateChain(alias);
			boolean expired = false;
			if(certs == null)
				expired = true;
			else
				for(Certificate cert : certs) {
					if(cert instanceof X509Certificate) {
						Date expiredDate = ((X509Certificate) cert).getNotAfter();
						if(expiration.after(expiredDate)) {
							expired = true;
							break;
						}
					}
				}
			if(expired) {
				// Create new entry
				StringBuilder csr = new StringBuilder();
				KeyPair pair = CryptUtils.generateCSR("RSA", 2048, "CN=" + commonName + ", O=USA Technologies\\, Inc, L=Malvern, S=PA, C=US", csr);

				// Sign entries
				HttpClient httpClient = new HttpClient();
				String url = "https://" + certificateAuthority + "/certsrv/certfnsh.asp";
				HttpState state = new HttpState();
				String webPassword = promptForPassword("Please enter the password for " + webUser + "@" + certificateAuthority);
				state.setCredentials(new AuthScope(null, -1, null, "basic"), new UsernamePasswordCredentials(webUser, webPassword));

				PostMethod method = new PostMethod(url);
				method.addParameter("CertRequest", csr.toString());
				method.addParameter("TargetStoreFlags", "0");
				method.addParameter("SaveCert", "yes");
				method.addParameter("Mode", "newreq");
				method.addParameter("CertAttrib", "CertificateTemplate:" + templateName); // USATSmartcardUser or USATWebServer
				method.getHostAuthState().setAuthScheme(new BasicScheme());

				int rc = httpClient.executeMethod(null, method, state);
				if(rc != 200)
					throw new IOException("Could not sign certificate. Got http code " + rc);
				int requestId = 0;
				BufferedReader reader = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
				Pattern pattern = Pattern.compile("\\?ReqID=(\\d+)\\&");
				String line;
				while((line = reader.readLine()) != null) {
					Matcher matcher = pattern.matcher(line);
					if(matcher.find()) {
						try {
							requestId = ConvertUtils.getInt(matcher.group(1));
						} catch(ConvertException e) {
							log.warn("Could not convert requestId", e);
						}
						break;
					}
				}
				if(requestId == 0)
					throw new IOException("Could not find requestId in response");
				url = "https://" + certificateAuthority + "/certsrv/certnew.p7b?ReqID=" + requestId + "&Enc=b64";
				GetMethod getMethod = new GetMethod(url);
				getMethod.getHostAuthState().setAuthScheme(new BasicScheme());

				rc = httpClient.executeMethod(null, getMethod, state);
				if(rc != 200)
					throw new IOException("Could not get certificate. Got http code " + rc);
				Header contentTypeHeader = getMethod.getResponseHeader("Content-Type");
				if(contentTypeHeader != null && contentTypeHeader.getValue() != null && contentTypeHeader.getValue().startsWith("text/html")) {
					throw new IOException("Did not get DER-encoded certificate chain. Instead got:\n" + getMethod.getResponseBodyAsString());
				}
				InputStream certStream = getMethod.getResponseBodyAsStream();
				CryptUtils.importCertificateChain(keyStore, alias, pair, certStream, password);
				keyStore.store(new FileOutputStream(jksFile), password);
			}
			return keyStore;
		} catch(KeyStoreException e) {
			throw new IOException(e);
		} catch(NoSuchProviderException e) {
			throw new IOException(e);
		} catch(NoSuchAlgorithmException e) {
			throw new IOException(e);
		} catch(CertificateException e) {
			throw new IOException(e);
		} catch(InvalidKeyException e) {
			throw new IOException(e);
		} catch(SignatureException e) {
			throw new IOException(e);
		}
	}

	protected static String promptForPassword(String prompt) throws IOException {
		Console console = System.console();
		if(console == null) {
			String pwd = UIUtils.promptForPassword(prompt, "Please enter password", null);
			if(pwd == null)
				throw new IOException("Canceled by user");
			/*
			System.out.println(prompt + ":");
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String pwd = reader.readLine();
			*/
			return pwd;
		}
		char[] pwd = console.readPassword(prompt);
		if(pwd != null)
			return new String(pwd);
		return null;
	}

	@Test
	public void testProviders() throws Exception {
		// String[] providerNames = new String[] { "SunJCE", "SunEC" };
		// for(String pn : providerNames) {
		// Provider provider = Security.getProvider(pn);

		Provider[] providers = Security.getProviders();
		for(Provider provider : providers) {
			log.info("Provider '" + provider.getName() + "' ----------");
			for(Service service : provider.getServices()) {
				log.info(service);
			}
			log.info("--------------------------------");
		}
	}
	@Test
	public void testEncryption() throws Exception {
		System.setProperty("javax.net.ssl.keyStore", "E:\\Java Projects\\LayersCommon\\conf\\client.ks");
		System.setProperty("javax.net.ssl.keyStorePassword", "usatech");
		System.setProperty("javax.net.ssl.trustStore", "E:\\Java Projects\\LayersCommon\\conf\\client.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		//String cipherName = "AES/CBC/PKCS5PADDING"; //"RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING"
		//String test = "This is a very long string to see if encryption and decryption works. If it does then that is very good";
		//byte[] unencrypted = new byte[300];
		//byte[] bytes = test.getBytes();
		//for(int i = 0; i < unencrypted.length; i += bytes.length) {
		//	System.arraycopy(bytes, 0, unencrypted, i, Math.min(bytes.length, unencrypted.length - i));
		//}
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);

		Random random = new Random();
		int minSize = 400;
		int maxSize = 4000;
		long start, time;
		int n = 10;
		byte[][] tab = new byte[n][];
		byte[][] etab = new byte[n][];
		byte[][] ektab = new byte[n][];
		int byteCount = 0;
		for(int i = 0; i < tab.length; i++) {
			int len = minSize + random.nextInt(maxSize - minSize);
			byteCount += len;
			tab[i] = new byte[len];
			random.nextBytes(tab[i]);
		}
		start = System.currentTimeMillis();
		for(int i = 0; i < tab.length; i++) {
			EncryptedBytes eb1 = new EncryptedBytes("RSA", "AES", keyGenerator, "mce.applayer.data", tab[i]);
			etab[i] = eb1.getEncrypted();
			ektab[i] = eb1.getEncryptedKey();
		}
		time = System.currentTimeMillis() - start;
		log.info("Encrypted " + tab.length + " entries of a total " + byteCount + " in " + time + "ms");
		byteCount = 0;
		start = System.currentTimeMillis();
		for(int i = 0; i < tab.length; i++) {
			byteCount += etab[i].length + (ektab[i] == null ? 0 : ektab[i].length);
			EncryptedBytes eb1 = new EncryptedBytes(etab[i], ektab[i], "RSA", "AES", "mce.applayer.data");
			byte[] decrypted = eb1.getUnencrypted();
			assert Arrays.equals(tab[i], decrypted);
		}
		time = System.currentTimeMillis() - start;
		log.info("Decrypted " + tab.length + " entries of a total " + byteCount + " in " + time + "ms");
	}
	@Test
	public void testDecrypt() throws Exception {
		String file = "C:\\Users\\bkrug\\Downloads\\keystore_extract.ks";
		String keyAlias = "eportconnect";
		char[] password = "usatech".toCharArray();
		KeyStore keystore = KeyStore.getInstance("JKS");
		InputStream stream = new FileInputStream(file);
		try {
			keystore.load(stream, password);
		} finally {
			stream.close();
		}

		Key key = keystore.getKey(keyAlias, password);
		if(key == null)
			throw new GeneralSecurityException("Key '" + keyAlias + "' not found in keystore");
		byte[] data = ByteArrayUtils
				.fromHex("6752440e23334a6ab89e36ccb992e33d6a10d9c56c8c3250118ad32d64df523f5addd8e6253b7dc914fe0a6113dcae98742c0d1a4e2bd6f80d1aa043b2c7d163254d5a8e67480550eaae92d32d92d1f58f3c448657130550118def345e45d3eaba405e23b9b24da8bc2993f808a2f0e8215b47fe9391dbef1e66c0b1f87fa6d5b5ed40ffc73d85ee656ee5bfec938ddde3ce27cef7b1b798d4d1af75a52c350ac6f2081d6753be1cf2cd9143f32b0fdfaf0fbbea0818375d96f5f97041c1324cad5b404bc82063c3044cc5f9ada3d8acd93ddfbd34faa035f54c1eeebe680d4c675b8bd54f2fbc050aa70b2dcad12198384e4d8379e56351c861e711b53c9dc");
		String[] paddings = new String[] { "PKCS1PADDING", "OAEPPADDING", "OAEPWITHMD5ANDMGF1PADDING", "OAEPWITHSHA1ANDMGF1PADDING", "OAEPWITHSHA-1ANDMGF1PADDING", "OAEPWITHSHA-224ANDMGF1PADDING", "OAEPWITHSHA-256ANDMGF1PADDING", "OAEPWITHSHA-384ANDMGF1PADDING", "OAEPWITHSHA-512ANDMGF1PADDING", "NOPADDING" };
		for(String padding : paddings) {
			try {
				byte[] result = CryptUtils.decrypt("RSA/ECB/" + padding, key, data, 0, data.length);
				log.info("Padding '" + padding + "' worked! Got result:\n\t" + StringUtils.toHex(result));
				break;
			} catch(GeneralSecurityException e) {
				log.warn("Padding '" + padding + "' failed", e);
			}
		}
		// byte[] result = CryptUtils.decrypt("RSA/ECB/OAEPPADDING", key, data, 0, data.length);
		// byte[] result = CryptUtils.decrypt("RSA/ECB/OAEPWITHDSHA-384ANDMGF1PADDING", key, data, 0, data.length);
		// byte[] result = CryptUtils.decrypt("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING", key, data, 0, data.length);
		// log.info("Got result:\n\t" + StringUtils.toHex(result));
		// --------------
		/*
		byte[] resultWithPadding = new byte[] { 123, 68, 110, -54, -122, -27, 29, -82, 121, 117, -64, -55, -49, 20, 16, -107, -35, -22, -86, -86, -62, 46, 46, -51, -76, -46, -53, -1, 117, 43, -106, 99, 2, -105, -51, 57, -16, -4, -93, 45, -73, -120, -56, 46, 116, -31, -103, 70, -40, 5, -96, 98, -127, 83, -10, -101, -21, 37, -57, -114, 103, 42, -50, 94, 117, 51, 113, -32, 34, -109, 3, -38, -27, 63,
				-92, -23, -105, -86, -98, -25, 81, 44, 114, 60, 11, 0, -24, 92, -47, 104, 34, 89, -58, 100, 112, 86, -97, 59, 71, -94, 60, 120, -114, -113, -29, -30, -123, 53, -53, -112, 110, -23, -94, 51, 23, 25, -22, 3, -24, -9, 61, 19, -34, 115, 33, 9, -123, 125, 0, 9, 72, -23, -29, -71, -103, -5, -118, -68, -105, -87, 66, -17, -72, -8, -78, 96, 23, 48, 103, 66, -85, -97, -53, -30, -104, 25,
				-113, 66, -128, 107, 73, 114, -88, -79, 5, 12, 112, 85, 14, -101, 110, -121, 46, -116, 71, -96, -35, -87, -8, -98, 103, 85, 57, 76, -11, -79, 30, -73, -54, -107, -71, -122, 18, 20, 88, 61, -79, -106, 109, -58, -19, -34, -7, 86, -25, -117, 96, -28, -101, 16, 63, -34, 85, 88, -117, 118, 80, -20, -12, -60, -6, -57, 69, -16, -127, -118, 97, -122, -29, -75, -66, -94, -57, -59, 46, 110,
				-90, -93, 109, 123, -22, -126, 52, 4, 112, -109, -92, 37, 55, 56, 6, -84, -103, 95, -47, 111 };
		byte[] data2 = ByteArrayUtils.fromHex("55a0326d61cee1ca3f9a07c0cb9640c59d1ee2b25d9904864896bafe3e791ddd84b2b6f3f86db736064e87a912457dd18d16222bfea886bf52fe865c7c84e1d07");
		RSAPadding padding = RSAPadding.getInstance(RSAPadding.PAD_OAEP_MGF1, paddedSize, random, spec);
		*/
	}

	@Test
	public void testDecrypt2() throws Exception {
		String file = "C:\\Users\\bkrug\\Downloads\\keystore_extract.ks";
		String keyAlias = "eportconnect";
		char[] password = "usatech".toCharArray();
		KeyStore keystore = KeyStore.getInstance("JKS");
		InputStream stream = new FileInputStream(file);
		try {
			keystore.load(stream, password);
		} finally {
			stream.close();
		}

		Key key = keystore.getKey(keyAlias, password);
		if(key == null)
			throw new GeneralSecurityException("Key '" + keyAlias + "' not found in keystore");
		byte[] data = ByteArrayUtils
				.fromHex("3887969123d8de4a4e3ee08892a2965aec67396970093c1b695fb515db6ee2c3691c6ca9531f08a367806e114c551da02984043169a35708d3a1d5a83744ce54a1dfc2afae7d1786b75c9bec39a6cf451d7dde4f6affc792159de14488556ba60df9f23de3f69e7e96c028db626dbccd532584f9898b5c2c4907b26760e946666f2aaca9f7aca9c09feff07016f43a5e0c81b120db6d1f2ef0d20c164325ad63936b0c33b1bce5dc5f8eac270479851d314b404d08ff1fd282390bab3b7bdf688def3e4eba0a496e0ca3fb1e8fb59ae4d13a27b8025f9cc09455f81539c4892c3f90383c5117cd62af837df367f03710cba8bdf77c9901185a2d4dbaa209394f");
		String[] paddings = new String[] { "PKCS1PADDING", "OAEPPADDING", "OAEPWITHMD5ANDMGF1PADDING", "OAEPWITHSHA1ANDMGF1PADDING", "OAEPWITHSHA-1ANDMGF1PADDING", "OAEPWITHSHA-224ANDMGF1PADDING", "OAEPWITHSHA-256ANDMGF1PADDING", "OAEPWITHSHA-384ANDMGF1PADDING", "OAEPWITHSHA-512ANDMGF1PADDING", "NOPADDING" };
		for(String padding : paddings) {
			try {
				byte[] result = CryptUtils.decrypt("RSA/ECB/" + padding, key, data, 0, data.length);
				log.info("Padding '" + padding + "' worked! Got result:\n\t" + StringUtils.toHex(result));
				break;
			} catch(GeneralSecurityException e) {
				log.warn("Padding '" + padding + "' failed", e);
			}
		}
	}

	@Test
	public void testKeySupply() throws Exception {
		System.setProperty("javax.net.ssl.keyStore", "E:\\Java Projects\\LayersCommon\\conf\\client.ks");
		System.setProperty("javax.net.ssl.keyStorePassword", "usatech");
		System.setProperty("javax.net.ssl.trustStore", "E:\\Java Projects\\LayersCommon\\conf\\client.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		KeySupply ts = CryptUtils.getDefaultTrustSupply();
		KeySupply ks = CryptUtils.getDefaultKeySupply();
		PublicKey pubKey = ts.getPublicKey("mce.applayer.data");
		Key privKey = ks.getKey("mce.applayer.data");
		String[] digests = new String[] {"SHA-1", "SHA-256", "SHA-384"};
		for(String md : digests) {
			log.info("Hash '" + md + "' has length " + MessageDigest.getInstance(md).getDigestLength());
		}

		//String test = "This is a very long string to see if encryption and decryption works. If it does then that is very good";
		String cipherName = "RSA"; //"RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING"; //"RSA/ECB/NOPADDING"; //
		/*String[] cipherNames = new String[] {"RSA/ECB/NOPADDING", "RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING", "RSA/ECB/OAEPWITHSHA-1ANDMGF1PADDING"}; //"RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING"
		for(String cn : cipherNames) {
			Cipher cipher = Cipher.getInstance(cn);
			log.info("Cipher: " + cipher + "; Algorithm: " + cipher.getAlgorithm());
			log.info("Cipher Parameters: " + cipher.getParameters());
			log.info("Cipher BlockSize: " + cipher.getBlockSize());
			byte[] iv = cipher.getIV();
			log.info("Cipher IV Length: " + (iv == null ? 0 : iv.length));
		}*/
		int maxSize = 128;
		long start, time;
		Random random = new Random();
		for( int i = 0; i < 10; i++) {
			byte[] unencrypted = new byte[random.nextInt(maxSize)];
			random.nextBytes(unencrypted);
			start = System.currentTimeMillis();
			byte[] encrypted = CryptUtils.encrypt(cipherName, "mce.applayer.data", unencrypted);
			time = System.currentTimeMillis() - start;
			log.info("Encrypted in " + time + "ms from " + unencrypted.length + " bytes into " + encrypted.length + " bytes");
			start = System.currentTimeMillis();
			byte[] decrypted = CryptUtils.decrypt(cipherName, "mce.applayer.data", encrypted);
			time = System.currentTimeMillis() - start;
			log.info("Decrypted in " + time + "ms from " + encrypted.length + " bytes into " + decrypted.length + " bytes; match=" + Arrays.equals(decrypted, unencrypted));
		}
	}
	@Test
	public void testKeySupplyList() throws Exception {
		String type = KeyStore.getDefaultType();
		String path;
		//File file = new File("C:\\Documents and Settings\\bkrug\\My Documents\\truststore_root.ts"); String password = "usatech";
		File file = new File("C:\\Documents and Settings\\bkrug\\My Documents\\devapr01-keystore.ks"); String password = "usatech";
		path = file.getAbsolutePath();
		/*
		String password = null;
		File file = new File(System.getProperty("java.home") + File.separator + "lib" + File.separator + "security" + File.separator + "jssecacerts");
		if(file.exists()) {
			path = file.getPath();
		} else {
			file = new File(System.getProperty("java.home") + File.separator + "lib" + File.separator + "security" + File.separator + "cacerts");
			if(file.exists()) {
				path = file.getPath();
			} else {
				path = "No File Available, using empty keystore.";
				file = null;
			}
		}
		 */
		KeyStore keyStore = KeyStore.getInstance(type);
		keyStore.load(new FileInputStream(file), password == null ? null : password.toCharArray());
		Enumeration<String> aliases = keyStore.aliases();
		StringBuilder sb = new StringBuilder();
		sb.append("Keystore '").append(path).append("' contains aliases:\n");
		int i = 0;
		while(aliases.hasMoreElements()) {
			sb.append("\t" + ++i + ". " + aliases.nextElement() + "\n");
		}
		sb.append("----------------\n");
		log.info(sb);
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
