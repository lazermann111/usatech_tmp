package simple.test;

import java.io.ByteArrayOutputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.ByteArrayByteInput;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;

public class ByteInputOutputTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testInputOutput() throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ByteOutput output = new OutputStreamByteOutput(baos);
		// write
		output.writeByte(250);
		output.writeByte(253);
		output.writeBytes("Hello");
		output.writeChar(12803);
		output.writeShort(Short.MAX_VALUE + 1);
		output.writeShort(Short.MIN_VALUE);
		output.writeInt(Integer.MAX_VALUE);
		output.writeInt(Integer.MAX_VALUE + 1);
		output.writeLong(Long.MAX_VALUE);
		output.writeLong(Long.MAX_VALUE + 1L);

		ByteInput input = new ByteArrayByteInput(baos.toByteArray());
		//read
		StringBuilder sb = new StringBuilder();
		sb.append(input.readUnsignedByte());
		sb.append(',');
		sb.append(input.readByte());
		sb.append(',');
		sb.append(input.readBytes(5));
		sb.append(',');
		sb.append(input.readChar());
		sb.append(',');
		sb.append(input.readUnsignedShort());
		sb.append(',');
		sb.append(input.readUnsignedShort());
		sb.append(',');
		sb.append(input.readUnsignedInt());
		sb.append(',');
		sb.append(input.readUnsignedInt());
		sb.append(',');
		sb.append(input.readLong());
		sb.append(',');
		sb.append(input.readLong());
		log.info("Output = " + sb.toString());

	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
