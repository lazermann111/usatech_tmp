package simple.test;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.DivideNumberFormat;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class FormatTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	@Test
	public void testMatchFormat() throws Exception {
		String format = "MATCH:\\s*ORA-\\d+:\\s*(.+?)\\s*(?:ORA-\\d+:.*)?=\\1;.*=\\0";
		testFormat(format, "ORA-20805: You may not refund this transaction because it does not belong to you.\nORA-06512: at \"CORP.REFUND_PKG\", line 232\nORA-06512: at \"CORP.REFUND_PKG\", line 32\nORA-06512: at line 1");
		testFormat(format, "ORA-20805: You may not refund this transaction because it does not belong to you. ");
		testFormat(format, "  You may not refund this transaction because it does not belong to you. ");
		testFormat(format, "You may not refund this transaction because it does not belong to you.");

	}

	@Test
	public void testReplaceFormat() throws Exception {
		testFormat("simple.text.ReplaceFormat:[a-d];_", "how many letters do I have?");
		testFormat("simple.text.ReplaceFormat:[a-z];$0_", "how many letters do I have?");
		testFormat("simple.text.ReplaceFormat:\\s;+", "how many letters do I have?");
		testFormat("simple.text.ReplaceFormat:letters|have;XXX", "how many letters do I have?");
		testFormat("simple.text.ReplaceFormat:([aeiou])|t;'$1'", "how many letters do I have?");
		testFormat("simple.text.ReplaceFormat:';';\\n", "first;second;third");
	}

	@Test
	public void testRegionFormat() throws Exception {
		testFormat("NULL:Region,", "North");
		testFormat("NULL:Region,", (Object[]) null);

		testFormat("MESSAGE:{1,NULL,Subregion,Region}", "West", "North");
		testFormat("MESSAGE:{1,NULL,Subregion,Region}", null, null);

		testFormat("MESSAGE:{0,NULL,{0} /' '}{1}", "North", "West");
		testFormat("MESSAGE:{0,NULL,{0} /' '}{1}", null, "Red");
		testFormat("MESSAGE:{0,NULL,{0} /' '}{1}", null, null);
	}
	
	@Test
	public void testCountFormat() throws Exception {
		testFormat("simple.text.CountFormat:[a-z]", "how many letters do I have?");
		testFormat("simple.text.CountFormat:[A-Z]", "how many letters do I have?");
		testFormat("simple.text.CountFormat:.", "how many letters do I have?");
		testFormat("simple.text.CountFormat:\\s", "how many letters do I have?");
		testFormat("simple.text.CountFormat:\\w+", "how many letters do I have?");
	}
	@Test
	public void testMessageFormat6() throws Exception {
		String format = "MESSAGE:{,MATCH,.*(Check RX/TX for short-circuit|DEX Read Error|Dex error|Need to retrieve passcode for this VMC|Please check the DEX cable|Please check the cabling|Please check cable).*={};^$=;{,simple.text.CountFormat,^[^\\x3B]|\\x3B} Alert(s)}";
		testFormat(format, "Need to retrieve passcode for this VMC; Error=888; Please check the DEX cable @ ePort end',34,'Please check the DEX cable @ ePort end; Please check the DEX cable @ VMC end',34,'Please check the DEX cable @ VMC end; Check RX/TX for short-circuit',34,'Check RX/TX for short-circuit");
		testFormat(format, "Big Ole Error; Another issue");
		testFormat(format, "");
		testFormat(format, (String) null);
	}

	@Test
	public void testMessageFormat5() throws Exception {
		String format = "MESSAGE:{0,NULL,No Communication;{1,NULL,No Cash Sales;{2,NULL,No Credit Sales;{3,NULL,No Driver Fills;{4,NULL,No DEX transfers;Okay}}}}}";
		testFormat(format, null, 1, null, 1);
		testFormat(format, null, null, null, null);
		testFormat(format, 1, 1, 1, 1);
	}

	@Test
	public void testResendAction() throws Exception {
		String format = "MESSAGE:{5,MATCH,9=promptDialog(''''''''Report Resent'''''''', ''''''''retransmit_report.i?fragment=true&userReportId={0}&batchId={1}&beginDate={2,DATE,MM/dd/yyyy HH:mm:ss}&endDate={3,DATE,MM/dd/yyyy HH:mm:ss}&fileId={4}&batchTypeId={5}'''''''');.*=promptDialog(''''''''Report Resent'''''''', ''''''''resend_report.i?fragment=true&userReportId={0}&batchId={1}&beginDate={2,DATE,MM/dd/yyyy HH:mm:ss}&endDate={3,DATE,MM/dd/yyyy HH:mm:ss}&batchTypeId={5}'''''''')}";
		testFormat(format, 213, 7674, new Date(), new Date(), 14914914, 2);
	}

	@Test
	public void testGeocodeFormat() throws Exception {
		String format = "NUMBER:+000.00000;-000.00000";
		Number[] tests = { 65.86, -12.34, 14, 12.9999, 40.9999999, 1000010 };
		for(int i = 0; i < tests.length; i++)
			testFormat(format, tests[i]);
	}

	@Test
	public void testSplit() {
		String pattern = "'%'#0.00##";
		String[] parts = StringUtils.splitQuotesAnywhere(pattern, '|', '\'', false);
		for(int i = 0; i < parts.length; i++)
			System.out.println("Part " + i + ": " + parts[i]);
	}

	@Test
	public void testMessageFormat4() throws Exception {
		// String format = "MESSAGE:{0,NULL,'{1,CURRENCY} on {0,DATE,MM/dd/yyyy} (Owned)'}{0,NULL,'{2,NULL,; }'}{2,NULL,'{3,CURRENCY} on {2,DATE,MM/dd/yyyy} (Rental)'}";
		// String format = "MESSAGE:{1,CHOICE,-1#is negative| 0#is zero or fraction | 1#is one |1.0<is 1+ |2#is two |2<is more than 2.}";// CHOICE;-1#is negative| 0#is zero or fraction | 1#is one
		// String format = "MESSAGE:{2,MATCH,[1-46]='{1,CHOICE,-1#javascript: createRefund({0},\"refund\");|90<}'}";// CHOICE;-1#is negative| 0#is zero or fraction | 1#is one |1.0<is 1+
		String format = "MESSAGE:{0,CHOICE,0#|0<'{0,NUMBER,''%''#0.00##}{1,CHOICE,0#|0< +'' ''}'}{1,CHOICE,0#|0<'{1,NUMBER,'$'#0.00}'}";
		// String format = "MESSAGE:{0,CHOICE,0#|0<'{0,NUMBER,''%''#0.00##}'}";
		testFormat(format, 123456, 91.0, 1);
		testFormat(format, 0, 3.4, 5);
		testFormat(format, -8, 100.0, 6);
		testFormat(format, 123456, 12.8, 8);
		testFormat(format, 123456, 0.0, 3);
		testFormat(format, 123456, 0.3, 4);
		testFormat(format, 123456, -0.3, 3);
		testFormat(format, 123456, -3.6, 6);
//
	}

	@Test
	public void testIntervalFormat() throws Exception {
		// String format = "MESSAGE:{0,NULL,'{1,CURRENCY} on {0,DATE,MM/dd/yyyy} (Owned)'}{0,NULL,'{2,NULL,; }'}{2,NULL,'{3,CURRENCY} on {2,DATE,MM/dd/yyyy} (Rental)'}";
		// String format = "MESSAGE:{1,CHOICE,-1#is negative| 0#is zero or fraction | 1#is one |1.0<is 1+ |2#is two |2<is more than 2.}";// CHOICE;-1#is negative| 0#is zero or fraction | 1#is one
		// String format = "MESSAGE:{2,MATCH,[1-46]='{1,CHOICE,-1#javascript: createRefund({0},\"refund\");|90<}'}";// CHOICE;-1#is negative| 0#is zero or fraction | 1#is one |1.0<is 1+
		String format = "simple.text.IntervalFormat";
		// String format = "MESSAGE:{0,CHOICE,0#|0<'{0,NUMBER,''%''#0.00##}'}";
		testFormat(format, 83918301823L);
		testFormat(format, 1234L);
		testFormat(format, 3 * 24 * 60 * 60 * 1000L);
		testFormat(format, 3 * 24 * 60 * 60 * 1000L + 2 * 60 * 60 * 1000L + 35 * 60 * 1000L);
		testFormat(format, 34.894);
		testFormat(format, 1 * 24 * 60 * 60 * 1000L + 1 * 60 * 60 * 1000L + 1 * 60 * 1000L);

	}

	@Test
	public void testMessageFormat3() throws Exception {
		String format = "MESSAGE:{0,NULL,{0},{1}}@{2}";
		testFormat(format, null, "prepaid", "usatech.com");
		testFormat(format, "pepi_food_service", "prepaid", "usatech.com");
		testFormat(format, "pepi_food_service", null, "usatech.com");

	}

	@Test
	public void testMessageFormat2() throws Exception {
		// String format = "MESSAGE:{0,CHOICE,-Infinity<'({1}{0,NUMBER,#;#,###,###,###,##0.00})'|0#|0<'{1}{0,NUMBER,#,###,###,###,##0.00}'}";
		// String format = "simple.text.MultiCurrencyFormat:*$#,##0.00;($#,##0.00)";
		// String format = "java.text.DecimalFormat:\u00A4#,###,###,###,##0.00;(\u00A4#,###,###,###,##0.00)";
		// String format = "MESSAGE:{0,CHOICE,0#|0<'{0,NUMBER,''%''#0.00##}{1,CHOICE,0#|0< +'' ''}'}{1,CHOICE,0#|0<'{1,NUMBER,'$'#0.00}'}";
		// String format = "MESSAGE:{0,CHOICE,0#|0<'{0,NUMBER,#0.00##}{1,CHOICE,0#|0<+}'}{1,CHOICE,0#|0<'{1,NUMBER,#0.00}'}";
		String format = "MESSAGE:{,CHOICE,0#|0<'{,NUMBER,'$'#0.00}'}";
		/*testFormat(format, null, 11.95);
		testFormat(format, null, null);
		testFormat(format, 0.0456, null);
		testFormat(format, 0.0329, 0.04);
		testFormat(format, 0, 11.95);
		testFormat(format, 0, 0);
		testFormat(format, 0.0456, 0);
		testFormat(format, 0.1);
		testFormat(format, 0.10);
		*/
		String result = ConvertUtils.formatObject(0.10, format);
		System.out.println("Format `" + format + "' of " + 0.10 + " = `" + result + "'");
	}
	@Test
	public void testMessageFormat() throws Exception {
		testFormat("MESSAGE:The {3,MATCH,US=Zip;CA=Postal;Postal} Code of the Billing Address of the {1} card {2} does not match", "AVS Check R44", "Visa", "414123******4952", "US");
		testFormat("MESSAGE:The {3,MATCH,US=Zip;CA=Postal;Postal} Code of the Billing Address of the {1} card {2} does not match", "AVS Check R44", "Visa", "414123******4952", "CA");
		testFormat("MESSAGE:The {3,MATCH,US=Zip;CA=Postal;Postal} Code of the Billing Address of the {1} card {2} does not match", "AVS Check R44", "Visa", "414123******4952", "GB");
		// testFormat("MESSAGE:{0}{2,NULL,' ({1,CHOICE,1#|1<''{1,NUMBER,#,##0} * ''}{2,CURRENCY})','{1,CHOICE,1#|1<'' ({1,NUMBER,#,##0})''}'}", "My nice item", 2, 4.50);
		// testFormat("MESSAGE:{0}{2,NULL,' ({1,CHOICE,1#|1<''{1,NUMBER,#,##0} * ''}{2,CURRENCY})','{1,CHOICE,1#|1<'' ({1,NUMBER,#,##0})''}'}", "My nice item", 3, null);
		// testFormat("MESSAGE:{0}{2,NULL,' ({1,CHOICE,1#|1<''{1,NUMBER,#,##0} * ''}{2,CURRENCY})','{1,CHOICE,1#|1<'' ({1,NUMBER,#,##0})''}'}", "My nice item", 1, null);
	}

	@Test
	public void testDecimalFormatParsing() throws Exception {
		//String[] tests = {"23.456", "0000000010", "0.1234", "12", "000040.9999999", "1000010"};
		String[] tests = {"000000065.86 +", "000000012.34 -","0000000010", "12", "000040.9999999", "1000010"};
		String[] formats = {"NUMBER:0 '+';0 '-'"};
		for(String f : formats) {
			for(String t : tests)
				testParse(f, t);
		}
	}

	@Test
	public void testRoudingNumberDecimalFormat() throws Exception {
		// String[] tests = {"23.456", "0000000010", "0.1234", "12", "000040.9999999", "1000010"};
		Number[] tests = { 65.86, -12.34, 14, 12.9999, 40.9999999, 1000010 };
		String[] formats = { "NUMBER:#,##0|DOWN", "NUMBER:#,##0|UP" };
		for(String f : formats) {
			for(Number t : tests)
				testFormat(f, t);
		}
	}
	@Test
	public void testWithDefaultFormats() throws Exception {
		// String[] tests = {"23.456", "0000000010", "0.1234", "12", "000040.9999999", "1000010"};
		Number[] tests = { 65.86, 12.34, 10L, 12, 40.9999999, 1000010, null, 0 };
		String[] formats = { "NUMBER-0:#,##0", "NUMBER-0:0" };
		for(String f : formats) {
			for(Number t : tests)
				testFormat(f, t);
		}
	}

	@Test
	public void testArrayFormat() throws Exception {
		testFormat("simple.text.ArrayFormat:|;NUMBER:$0,000.0##", 23.456, 10, 0.1234, 1.2, 40.9999999, 1000010.4);
		testFormat("simple.text.ArrayFormat:|;MESSAGE:{0} - {1, NUMBER, $0,000.0##}", new Object[] {"ABC", 23.456},  new Object[] {"DEF", 10},  new Object[] {"GHI", 0.1234});
		testFormat("simple.text.ArrayFormat:|*MESSAGE:{0} - {1, NUMBER, $0,000.0##}", new Object[] {new String[] {"ABC","DEF", "GHI"}, new Double[] {23.456, 10.0, 0.1234}} );
		testFormat("simple.text.ArrayFormat:|*MESSAGE:{0} - {1, NUMBER, $0,000.0##}", new Object[] {new Object[] {"ABC","DEF", "GHI"}, new double[] {23.456, 10.0, 0.1234}} );
		testFormat("simple.text.ArrayFormat:|", new Object[] {new Object[] {"ABC","DEF", "GHI"}, new double[] {23.456, 10.0, 0.1234}} );
	}

	@Test
	public void testQuotesInMessageFormat() throws Exception {
		String format = "MESSAGE:{0,MATCH,U=Not Matched;''O={1,NULL,Upload Not Complete,Waiting for {2,MATCH,''''2=Fill;3=Week''''''''s End;4=Month''''''''s End;8=Export;.*=End of Time Period''''}}'';''C|D={3,MATCH,0=Yes;.*=Waiting for Pending Trans}'';L|F=Yes;.*=Unknown}";
		testFormat(format, "U", new Date(), 2, 0);
		testFormat(format, "O", new Date(), 8, 0);
		testFormat(format, "O", null, 8, 0);
		testFormat(format, "O", null, 4, 0);
		testFormat(format, "O", null, 3, 0);
		testFormat(format, "O", null, 2, 0);
		testFormat(format, "O", new Date(), 4, 3);
		testFormat(format, "O", new Date(), 2, 3);
		testFormat(format, "L", new Date(), 1, 0);
		testFormat(format, "F", new Date(), 4, 3);
		testFormat(format, "D", new Date(), 4, 3);
		testFormat(format, "D", new Date(), 2, 0);
		}

	protected void testParse(String format, String text) throws Exception {
		Object result = ConvertUtils.getFormat(format).parseObject(text);
		System.out.println("Parsed `" + text+ "' using '" + format + "` and got `" + result + "'" + (result == null ? "" : " (" + result.getClass().getName() + ")"));
	}
	
	protected void testFormat(String format, Object... params) throws Exception {
		String result = ConvertUtils.formatObject(params, format);
		System.out.println("Format `" + format+ "' of " + CollectionUtils.deepToString(params) + " = `" + result + "'");
	}

	@Test
	public void testDivideNumberFormat()throws Exception{
		DivideNumberFormat df=new DivideNumberFormat(".00");
		String x=null;
		double[] numbers=new double[2];
		System.out.println("String:"+df.format(x));
		Assert.assertTrue(df.format(x).equals(""));
		System.out.println("no value double2:"+df.format(numbers));
		numbers=new double[1];
		System.out.println("no value double1:"+df.format(numbers));
		numbers=new double[2];
		numbers[0]=3;
		numbers[1]=0;
		System.out.println("divide by zero:"+df.format(numbers));
		numbers[1]=6;
		System.out.println("not divide by zero:"+df.format(numbers));
		Assert.assertTrue(".50".equals(df.format(numbers)));
		numbers=new double[2];
		numbers[0]=-3;
		System.out.println("no second number negative:"+df.format(numbers));

	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
