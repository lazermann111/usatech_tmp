package simple.test;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import simple.io.Log;
import simple.text.MessageFormat;

public class MessageFormatTest extends UnitTest
{
	@BeforeClass
	public static void init()
	{
		setupLog();
	}
	
	@AfterClass
	public static void tearDown() 
	{
		Log.finish();
	}
	
	@Test
	public void testNullArgs()
	{
		String msg = "test";
		
		assertEquals(msg, MessageFormat.format(msg, null));
	}
	
	@Test
	public void testNoFormat()
	{
		String msg = "test";
		assertEquals(msg, MessageFormat.format(msg, "foo"));
	}
	
	@Test
	public void testFormat()
	{
		String msg = "{0}";
		assertEquals("test", MessageFormat.format(msg, "test"));
	}	
	
	@Test
	public void testHtmlFormat()
	{
		String msg = "{0,HTML}";
		assertEquals("foo&amp;bar", MessageFormat.format(msg, "foo&bar"));
	}	
	
	@Test
	public void testCDataFormat()
	{
		String msg = "{0,CDATA}";
		assertEquals("foo&#38;bar", MessageFormat.format(msg, "foo&bar"));
	}	
	
}
