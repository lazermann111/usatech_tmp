package simple.test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Arrays;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.Call;
import simple.db.Cursor;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.Parameter;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.db.specific.DbSpecific;
import simple.db.specific.OracleSpecific;
import simple.io.HeapBufferStream;
import simple.io.IOUtils;
import simple.io.LoadingException;
import simple.io.Log;
import simple.results.Results;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;

public class CallTest extends UnitTest {
	protected class ColTypeDef {
		public final String name;
		public final boolean nullable;
		public final SQLType sqlType;
		public ColTypeDef(String name, boolean nullable, SQLType sqlType) {
			super();
			this.name = name;
			this.nullable = nullable;
			this.sqlType = sqlType;
		}
	}

	@Before
	public void setUp() throws Exception {
		setupLog();
		setupBasicDSF("TEST", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "SYSTEM", "usatmanager");
		setupBasicDSF("PG_TEST", "org.postgresql.Driver", "jdbc:postgresql://devdbs11:5432/main?ssl=true&connectTimeout=5&tcpKeepAlive=true", "usalive_2", "USALIVE_1");
		System.setProperty("javax.xml.parsers.SAXParserFactory", "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");
	}

	@Test
	public void compareMetaData() throws Exception {
		Writer writer = new FileWriter("C:/Users/bkrug/Documents/DbMetadataCompare.csv");
		try {
			compareMetadata(writer, DataLayerMgr.getConnection("TEST"), DataLayerMgr.getConnection("PG_TEST"));
		} finally {
			writer.close();
		}
	}
	protected void compareMetadata(Writer writer, Connection... conns) {
		if(conns == null || conns.length == 0) {
			log.error("No connections provided to compare");
			return;
		}
		try {
			DatabaseMetaData[] mds = new DatabaseMetaData[conns.length];
			for(int i = 0; i < conns.length; i++)
				mds[i] = conns[i].getMetaData();
			PrintWriter out = new PrintWriter(writer);
			out.print("Property");
			for(int i = 0; i < conns.length; i++) {
				out.print(",Connection #");
				out.print(i + 1);
			}
			out.println();
			Object[] data = new Object[conns.length + 1];
			for(Method method : DatabaseMetaData.class.getMethods()) {
				if(method.getParameterTypes().length == 0) {
					data[0] = method.getName();
					for(int i = 1; i < data.length; i++)
						try {
							data[i] = method.invoke(mds[i - 1]);
						} catch(IllegalArgumentException e) {
							data[i] = e;
						} catch(IllegalAccessException e) {
							data[i] = e;
						} catch(InvocationTargetException e) {
							data[i] = e;
						}
					StringUtils.writeCSVLine(data, out);
				}
			}
			out.flush();
			log.info("Wrote metadata info for " + conns.length + " connections");
		} catch(SQLException e) {
			log.error("Gettting metadata properties failed", e);
		}
	}
	@Test
	public void testFetchTimings() throws SQLException, DataLayerException, ConfigException, IOException, LoadingException {
		ConfigLoader.loadConfig("test-data-layer.xml");
		Call call = DataLayerMgr.getGlobalDataLayer().findCall("FETCH");
		long start;
		for(int i = 0; i < 5; i++) {
			start = System.currentTimeMillis();
			Connection conn = DataLayerMgr.getConnection(call.getDataSourceName());
			try {
				long end = System.currentTimeMillis();
				log.info("Got connection: " + (end - start) + " ms");
				start = System.currentTimeMillis();
				CallableStatement st = conn.prepareCall(call.getSql());
				end = System.currentTimeMillis();
				log.info("Prepared call: " + (end - start) + " ms");
				start = System.currentTimeMillis();
				boolean hasResults = st.execute();
				end = System.currentTimeMillis();
				log.info("Executed call: " + (end - start) + " ms");
				if(hasResults) {
					start = System.currentTimeMillis();
					ResultSet rs = st.getResultSet();
					end = System.currentTimeMillis();
					log.info("Got resultset: " + (end - start) + " ms");
					start = System.currentTimeMillis();
					int cnt = 0;
					while(rs.next())
						cnt++;
					end = System.currentTimeMillis();
					log.info("Retrieved " + cnt + " rows: " + (end - start) + " ms");
				}
			} finally {
				conn.close();
			}
		}
	}
	@Test
	public void testConvert() throws Exception {
		// String sql = "{call DBMS_SESSION.MODIFY_PACKAGE_STATE(DBMS_SESSION.REINITIALIZE)}";
		String sql = "select 'N' countersDisplayedFlag from dual";
		Call call = new Call();
		call.setSql(sql);
		Cursor cursor = new Cursor();
		cursor.setAllColumns(true);
		call.setArguments(new Argument[] { cursor });
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			Results results = call.executeQuery(conn, null, null);
			while(results.next()) {
				Boolean value = results.getValue("countersDisplayedFlag", Boolean.class);
				log.info("Got value `" + value + "'");
			}
		} finally {
			conn.close();
		}

	}

	@Test
	public void testSqlDirect() throws Exception {
		//String sql = "{call DBMS_SESSION.MODIFY_PACKAGE_STATE(DBMS_SESSION.REINITIALIZE)}";
		String sql = "select SYSDATE, ds.*, date_to_millis(ds.last_updated_ts) from device.device d join device.device_setting ds on d.device_id = ds.device_id where ds.device_setting_parameter_cd = '70' and d.device_active_yn_flag = 'Y' and d.device_name = 'TD000065'";
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			Statement st = conn.createStatement();
			boolean hasResults = st.execute(sql);
			if(hasResults) {
				ResultSet rs = st.getResultSet();
				StringUtils.writeCSV(rs, System.out, true);
				rs.close();
			} else {
				int count = st.getUpdateCount();
				log.info("Update Count = " + count);
			}
		} finally {
			conn.close();
		}

	}
	@Test
	public void testCallDirect() throws Exception {
		//String sql = "{call DBMS_SESSION.MODIFY_PACKAGE_STATE(DBMS_SESSION.REINITIALIZE)}";
		String sql = "select SYSDATE, ds.*, date_to_millis(ds.last_updated_ts) from device.device d join device.device_setting ds on d.device_id = ds.device_id where ds.device_setting_parameter_cd = '70' and d.device_active_yn_flag = 'Y' and d.device_name = 'TD000065'";
		Call call = new Call();
		call.setSql(sql);
		Cursor cursor = new Cursor();
		cursor.setAllColumns(true);
		call.setArguments(new Argument[] { cursor});
		Connection conn = DataLayerMgr.getConnection("TEST");
		try {
			Results results = call.executeQuery(conn, null, null);
			StringUtils.writeCSV(results, System.out, true);
		} finally {
			conn.close();
		}

	}
	@Test
	public void testPGSql() throws Exception {
		String sql = "SELECT * FROM MAIN.DEVICE_SESSION ORDER BY CALL_IN_START_TS DESC LIMIT 10";
		Call call = new Call();
		call.setSql(sql);
		Cursor cursor = new Cursor();
		cursor.setAllColumns(true);
		call.setArguments(new Argument[] { cursor });
		Connection conn = DataLayerMgr.getConnection("PG_TEST");
		try {
			Results results = call.executeQuery(conn, null, null);
			StringUtils.writeCSV(results, System.out, true);
			conn.commit();
		} finally {
			conn.close();
		}

	}

	@Test
	public void testCallwParams() throws Exception {
		String sql = "SELECT ?, TRUNC(?, 'DD'), TRUNC(?, 'DAY'), TRUNC(?, 'HH'), CURRENT_TIMESTAMP";
		Call call = new Call();
		call.setSql(sql);
		call.setDataSourceName("PG_TEST");
		Cursor cursor = new Cursor();
		cursor.setAllColumns(true);
		Parameter param = new Parameter();
		param.setRequired(true);
		param.setSqlType(new SQLType(Types.DATE));
		param.setDefaultValue(System.currentTimeMillis());
		call.setArguments(new Argument[] { cursor, param, param, param, param });
		Connection conn = DataLayerMgr.getConnection(call.getDataSourceName());
		try {
			Results results = call.executeQuery(conn, null, null);
			StringUtils.writeCSV(results, System.out, true);
		} finally {
			conn.close();
		}

	}

	@Test
	public void testInsertSelect() throws Exception {
		//create various calls
		HeapBufferStream bs = new HeapBufferStream(1024,1024);
		OutputStreamWriter writer = new OutputStreamWriter(bs.getOutputStream());
		boolean first;
		String[] types = new String[] {
				"VARCHAR(30)", "CHAR(10)", "CHAR(1)", "NUMERIC(22,8)", "DECIMAL(22)", "BIGINT", "DATE", "TIMESTAMP", "BLOB", "CLOB"
		};
		ColTypeDef[] cols = new ColTypeDef[types.length];
		for(int i = 0; i < types.length; i++)
			cols[i] = new ColTypeDef("COL_" + (i+1), true, ConvertUtils.convert(SQLType.class, types[i]));

		writer.append("<data-layer dataSource=\"TEST\">\n");

		//make create table call
		writer.append("<call id=\"CREATE\" sql=\"CREATE TABLE TMP_APP_TEST_1(\n\t");
		//DBHelper dbHelper = DataLayerMgr.getDBHelper("TEST");
		DbSpecific dbSpecific = new OracleSpecific();
		first = true;
		for(ColTypeDef col : cols) {
			if(first) first = false;
			else writer.append(",\n\t");

			writer.append(col.name).append(' ').append(dbSpecific.getNormalizedType(col.sqlType).toString());
			if(!col.nullable) {
				writer.append(" NOT NULL");
			}
		}
		writer.append(")\"/>\n");

		// make drop table call
		writer.append("<call id=\"DROP\" sql=\"DROP TABLE TMP_APP_TEST_1\"/>\n");

		//make insert call
		writer.append("<call id=\"INSERT\" sql=\"INSERT INTO TMP_APP_TEST_1(\n\t");
		first = true;
		for(ColTypeDef col : cols) {
			if(first) first = false;
			else writer.append(",\n\t");

			writer.append(col.name);
		}
		writer.append(")\nVALUES(\n\t?");
		writer.append(StringUtils.repeat(",\n\t?", cols.length-1));
		writer.append(")\">\n");
		for(ColTypeDef col : cols) {
			writer.append("\t<parameter property=\"").append(col.name).append("\" type=\"");
			writer.append(SQLTypeUtils.getTypeCodeName(col.sqlType.getTypeCode())).append("\"/>\n");
		}
		writer.append("</call>");

		//make query each col
		writer.append("<query id=\"SELECT\" sql=\"SELECT\n\t");
		first = true;
		for(ColTypeDef col : cols) {
			if(first) first = false;
			else writer.append(",\n\t");

			writer.append(col.name);
		}
		writer.append("\nFROM TMP_APP_TEST_1\">\n");
		for(ColTypeDef col : cols) {
			writer.append("\t<column property=\"").append(col.name).append("\"/>\n");
		}
		writer.append("</query>");

		//make query all
		writer.append("<query id=\"SELECT_ALL\" sql=\"SELECT * FROM TMP_APP_TEST_1\" allColumns=\"true\"/>\n");

		writer.append("</data-layer>\n");
		writer.flush();
		//read in file
		ConfigLoader.loadConfig(bs.getInputStream());

		//run tests
		DataLayerMgr.executeCall("CREATE", null, true);
		try {
			Object[] values1 = getValues(cols, 1);
			DataLayerMgr.executeCall("INSERT", values1, true);
			Results results = DataLayerMgr.executeQuery("SELECT", null);
			assert results.next() : "No results in select query";
			compareResults(results, cols, values1);
		} finally {
			DataLayerMgr.executeCall("DROP", null, true);
		}
	}
	protected void compareResults(Results results, ColTypeDef[] cols, Object[] values) throws ConvertException, IOException, SQLException {
		for(int i = 0; i < cols.length; i++) {
			Object value = results.getValue(cols[i].name);
			compareValues(value, values[i], cols[i]);
		}
	}

	protected void compareValues(Object actual, Object expected, ColTypeDef col) throws ConvertException, IOException, SQLException {
		if(expected != null) {
			if(actual instanceof java.sql.Blob)
				actual = IOUtils.readFully(((java.sql.Blob)actual).getBinaryStream());
			else if(actual instanceof java.sql.Clob)
				actual = IOUtils.readFully(((java.sql.Clob)actual).getAsciiStream());

			if(expected.getClass().isInstance(actual)) {
				expected = ConvertUtils.convert(actual.getClass(), expected);
			} else {
				actual = ConvertUtils.convert(expected.getClass(), actual);
			}
		}

		boolean fail;
		if(expected instanceof Comparable && actual instanceof Comparable) {
			fail = ((Comparable)expected).compareTo(actual) != 0;
		} else {
			fail = !ConvertUtils.areEqual(actual, expected);
		}
		if(fail)
			log.warn("MISMATCH on " + col.name + " (type=" + col.sqlType.toString() + "): " + actual + " != " + expected);
		else
			log.info("GOOD for "+ col.name + " (type=" + col.sqlType.toString() + "): " + actual + " == " + expected);
	}

	protected Object[] getValues(ColTypeDef[] cols, int num) {
		Object[] values = new Object[cols.length];
		for(int i = 0; i < cols.length; i++) {
			values[i] = getValueFor(cols[i].sqlType, num);
		}
		return values;
	}
	protected Object getValueFor(SQLType sqlType, int num) {
		Class<?> javaType = ConvertUtils.convertToWrapperClass(SQLTypeUtils.getJavaType(sqlType.getTypeCode()));
		int triple;
		switch(num % 3) {
			case 0:
				triple = 123; break;
			case 1:
				triple = 456; break;
			case 2:
				triple = 789; break;
			default:
				triple = 0;
		}
		if(String.class.isAssignableFrom(javaType)) {
			String s = asChars(num) + " Testing " + triple;
			if(sqlType.getPrecision() != null && s.length() > sqlType.getPrecision()) {
				s = s.substring(0, sqlType.getPrecision());
			}
			return s;
		} else if(Number.class.isAssignableFrom(javaType)) {
			return triple * 31.0 / 5.0;
		} else if(Date.class.isAssignableFrom(javaType)) {
			return new Date(System.currentTimeMillis() - (triple * 10000));
		} else if(Boolean.class.isAssignableFrom(javaType)) {
			return triple % 2 == 0;
		} else if(byte[].class == javaType) {
			String s = asChars(num) + " Testing " + triple;
			if(sqlType.getPrecision() != null && s.length() > sqlType.getPrecision()) {
				s = s.substring(0, sqlType.getPrecision());
			}
			return s.getBytes();
		}

		return null;
	}

	protected String asChars(int num) {
		char buf[] = new char[33];
		int charPos = 32;

		while (num >= 26) {
		    buf[charPos--] = (char)('A' + (num % 26));
		    num = num / 26;
		}
		buf[charPos] = (char)('A' + num);
		return new String(buf, charPos, (33 - charPos));
	}

	@Test
	public void testDescribe() throws SQLException, DataLayerException {
		String sql = "select c.customer_name \"Customer\", d.device_serial_cd \"Device\", pp.pos_pta_device_serial_cd \"Sprout POS ID\", t.asset_nbr \"Asset #\", l.location_name \"Location\"\nfrom device.device d\njoin pss.pos p on d.device_id = p.device_id\njoin pss.pos_pta pp on p.pos_id = pp.pos_id and pp.pos_pta_activation_ts <= sysdate and (pp.pos_pta_deactivation_ts is null or pp.pos_pta_deactivation_ts > sysdate)\njoin pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id and ps.payment_subtype_class = 'Sprout'\nleft outer join report.eport e on d.device_serial_cd = e.eport_serial_num\nleft outer join report.vw_terminal_eport te on e.eport_id = te.eport_id\nleft outer join report.terminal t on te.terminal_id = t.terminal_id\nleft outer join corp.customer c on t.customer_id = c.customer_id and c.customer_id = case ? when 0 then c.customer_id else ? end\nleft outer join report.location l on t.location_id = l.location_id\nwhere d.device_active_yn_flag = 'Y'\norder by c.customer_name, e.eport_serial_num";
		// String sql =
		// "select c.customer_name \"Customer\", d.device_serial_cd \"Device\", pp.pos_pta_device_serial_cd \"Sprout POS ID\", t.asset_nbr \"Asset #\", l.location_name \"Location\"\nfrom device.device d\njoin pss.pos p on d.device_id = p.device_id\njoin pss.pos_pta pp on p.pos_id = pp.pos_id and pp.pos_pta_activation_ts <= sysdate and (pp.pos_pta_deactivation_ts is null or pp.pos_pta_deactivation_ts > sysdate)\njoin pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id and ps.payment_subtype_class = 'Sprout'\nleft outer join report.eport e on d.device_serial_cd = e.eport_serial_num\nleft outer join report.vw_terminal_eport te on e.eport_id = te.eport_id\nleft outer join report.terminal t on te.terminal_id = t.terminal_id\nleft outer join corp.customer c on t.customer_id = c.customer_id and c.customer_id = case CAST(? as NUMBER) when 0 then c.customer_id else CAST(? as NUMBER) end\nleft outer join report.location l on t.location_id = l.location_id\nwhere d.device_active_yn_flag = 'Y'\norder by c.customer_name, e.eport_serial_num";
		Connection conn = DataLayerMgr.getConnection("TEST");
		DbSpecific dbSpecific = new OracleSpecific();
		dbSpecific.initialize(conn);
		System.out.println(Arrays.toString(dbSpecific.describeQuery(conn, sql)));

	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
