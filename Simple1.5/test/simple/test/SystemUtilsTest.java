package simple.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.lang.SystemUtils;
import simple.util.DynaBeanMap;
import simple.xml.serializer.TagFlags;

public class SystemUtilsTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testSystemInfo() throws Exception {
		System.out.println("SystemInfo: " + new DynaBeanMap(SystemUtils.getSystemInfo()));
	}
	
	@Test
	public void testTagFlags() throws Exception {
		int flags = 0;
		System.out.println("Initial Value: " + Integer.toBinaryString(flags));
		for(TagFlags tf : TagFlags.values()) {
			flags = tf.set(flags);
			System.out.println("Add " + tf + "; New Value: " + Integer.toBinaryString(flags));			
		}
		for(TagFlags tf : TagFlags.values()) {
			flags = tf.unset(flags);
			System.out.println("Remove " + tf + "; New Value: " + Integer.toBinaryString(flags));			
		}
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
