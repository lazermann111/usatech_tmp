package simple.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class StuckThreadsTest extends Thread {
	protected static final AtomicInteger threadIdGenerator = new AtomicInteger();
	protected static final DateFormat logDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
	protected static final ReentrantLock logLock = new ReentrantLock();
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
	protected final AtomicLong lastWakeup = new AtomicLong();
	protected final long waitTime;
	
	public StuckThreadsTest(long waitTime) {
		super("StuckThread #" + threadIdGenerator.incrementAndGet());
		this.waitTime = waitTime;
	}

	@Override
	public void run() {
		while(true) {
			long time = System.currentTimeMillis();
			long last = lastWakeup.get();
			while(time > last && !lastWakeup.compareAndSet(last, time))
				last = lastWakeup.get();
			long late = time - last - waitTime;
			if(late > waitTime)
				log("Awakened " + late + " ms late");
			try {
				lock.lockInterruptibly();
				try {
					signal.await(waitTime, TimeUnit.MILLISECONDS);
				} finally {
					lock.unlock();
				}
			} catch(InterruptedException e) {
				log("Interrupted");
			}
		}
	}
	protected void log(String message) {
		log(message, null);
	}
	protected void log(String message, Throwable throwable) {
		logThreadMessage(this, message, throwable);
	}
	protected static void logThreadMessage(Thread thread, String message, Throwable throwable) {
		logLock.lock();
		try {
			System.out.print(logDateFormat.format(new Date()));
			System.out.print(" [");
			System.out.print(thread.getName());
			System.out.print("] ");
			System.out.println(message);
			if(throwable != null)
				throwable.printStackTrace(System.out);
		} finally {
			logLock.unlock();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int num = (args != null && args.length > 0 ? Integer.parseInt(args[0]) : 10);
		int wait = (args != null && args.length > 1 ? Integer.parseInt(args[1]) : 1000);
		StuckThreadsTest[] threads = new StuckThreadsTest[num];
		for(int i = 0; i < threads.length; i++) {
			threads[i] = new StuckThreadsTest(wait);
			threads[i].start();
		}
		while(true) {
			try {
				Thread.sleep(60000);
			} catch(InterruptedException e) {
				logThreadMessage(Thread.currentThread(), "Interrupted", null);
			}
			long time = System.currentTimeMillis();
			for(int i = 0; i < threads.length; i++) {
				if(time - threads[i].lastWakeup.get() > wait * 4) {
					logThreadMessage(Thread.currentThread(), "Thread '" + threads[i].getName() + "' is STUCK!", null);
				}
			}
		}
	}

}
