package simple.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.bean.ConditionUtils;
import simple.io.Log;

public class ConditionUtilsTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testChecks() throws Exception {
		Object[] ol1 = new Object[] {
			1,
			'a',
			"butterfly",
			"10,12,13",
			"80-90",
			"14",
			256,
			"p",
			null
		};
		Object[] ol2 = new Object[] {
			2,
			'b',
			"elephant",
			"5,7,9",
			"12-17",
			85,
			"t",
			null
		};

		String[] operators = new ConditionUtils.BaseCondition().getOperators();

		for(Object o1 : ol1) {
			for(Object o2 : ol2) {
				for(String op : operators) {
					log.debug("Is " + o1 + " (" + (o1 == null ? "" : o1.getClass().getName()) + ") "
							+ op +  " " + o2 + "  (" + (o2 == null ? "" : o2.getClass().getName()) + ") ? " +
							ConditionUtils.check(o1, op, o2));

				}
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
