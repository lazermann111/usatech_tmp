package simple.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

public class SheetUtilsTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testReadCSV() throws Exception {
		File file = new File("E:/TEMP/activation1.csv");
		RowValuesIterator rvIterator = SheetUtils.getExcelRowValuesIterator(new FileInputStream(file));
		while(rvIterator.hasNext()) {
			Map<String,Object> map = rvIterator.next();
			log.info("Row # " + rvIterator.getCurrentRowNum() + ":" + map);
		}
		log.info("Done ---------------------------");
	}
	@Test
	public void testReadFiles() throws Exception {
		File[] files = new File[] {
			new File("C:\\Users\\bkrug\\Documents\\MessageTypes.xlsx"),
			new File("C:\\Users\\bkrug\\Documents\\LineItemsForCountDiscrepancy.xls"),
			new File("C:\\Users\\bkrug\\Documents\\numidle_mq_1.csv"),				
		};
		for(File file : files) {
			log.info("File '" + file.getAbsolutePath() + "':");
			RowValuesIterator rvIterator = SheetUtils.getExcelRowValuesIterator(new FileInputStream(file));
			while(rvIterator.hasNext()) {
				Map<String,Object> map = rvIterator.next();
				log.info("Row # " + rvIterator.getCurrentRowNum() + ":" + map);
			}
			log.info("Done ---------------------------");
		}
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
