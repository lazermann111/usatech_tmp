package simple.test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class NIOSendReceiveClient extends NIOSendReceiveTest {
	protected class Instance extends Worker {
		protected long pause;

		public Instance(SocketChannel socketChannel, WorkerType workerType, long pause, int bufferSize) {
			super(socketChannel, workerType, bufferSize);
			this.pause = pause;
		}

		@Override
		protected void runReceive() {
			while(socketChannel.isOpen()) {
				read(2);
				if(pause > 0)
					try {
						Thread.sleep(pause);
					} catch(InterruptedException e) {
						log_info("Thread interuppted for client " + id + "; exiting...");
						return;
					}
			}
		}

		@Override
		protected void runSend() {
			write(pause, 0);
			if(socketChannel.isOpen()) {
				try {
					socketChannel.close();
				} catch(IOException e) {
					log_error("Could not close " + id, e);
				}
			}
		}
	}

	protected final InetSocketAddress socketAddress;

	public NIOSendReceiveClient(String host, int port) {
		this.socketAddress = new InetSocketAddress(host, port);
	}

	protected void runClient(WorkerType workerType, int num, long pause, int bufferSize) throws IOException, InterruptedException, ExecutionException {
		Future<?>[] completions = new Future<?>[num];
		for(int i = 0; i < completions.length; i++) {
			completions[i] = executor.submit(createInstance(workerType, pause, bufferSize));
		}
		for(Future<?> comp : completions)
			comp.get();
	}

	protected Instance createInstance(WorkerType workerType, long pause, int bufferSize) throws IOException {
		SocketChannel socketChannel = SocketChannel.open();
		final Socket socket = socketChannel.socket();
		socket.connect(socketAddress, 5000);
		socket.setKeepAlive(true);
		return new Instance(socketChannel, workerType, pause, bufferSize);
	}
}
