package simple.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.zip.DeflaterOutputStream;

import org.apache.commons.configuration.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.event.TaskListener;
import simple.event.WriteCSVTaskListener;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.io.resource.ResourceUtils;
import simple.io.resource.ResourceUtils.ResourceKey;
import simple.io.resource.sftp.AbstractSftpResourceFolder;
import simple.io.resource.sftp.ReplicatedSftpResourceFolder;
import simple.io.resource.sftp.SftpRepository;

public class ResourceTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testResourceKeyPattern() throws Exception {
		String[] keys = new String[] {
				"[localhost:/C%58/TEMP/file_repo_1;127.0.0.1]/abc/def/xyz.txt",
				"[localhost;127.0.0.1;usadev01.usatech.com]/abc/def.jkl/xyz.txt",
				"[localhost,127.0.0.1,www.esuds-tst.net]/abc/def.jkl/xyz.stu.txt.gz",
				"[localhost,127.0.0.1,usadev01:file_repo_1]abc/def/x.y-z(123).txt",
				"[localhost,127.0.0.1,usadev01:/home/bkrug/file_repo_1]xyz(123).txt.gz",
				"[localhost]/abc/def/x-yz",
				"[localhost]/abc/def/xyz(456)",
				"/ab-\\c/def/xyz(123).txt",
				"abc+def%40foo%24/xyz%21(123).txt.gz",
				"[localhost,127.0.0.1,usadev01]/abc+def%40foo%24/xyz%21(123).txt"
			};
		for(String key : keys) {
			ResourceKey resKey = ResourceUtils.parseKey(key, ".gz");
			log.info("Key '" + key + "' produced '" + resKey + "'");
			resKey.insertSubDirectory("/_\\");
			log.info("With inserted directory '" + resKey + "'");
			resKey.nextPath();
			log.info("With next path '" + resKey + "'");
		}
	}
	@Test
	public void testReplicatedSftp() throws Exception {
		AbstractSftpResourceFolder folder = setupResourceFolder(2, 5);
		folder.setSubFolderNames(new String[] {"_0", "_1", "_2"});
		folder.setMaxChildrenPerFolder(6);
		folder.setCompressing(true);
		testReplicatedSftp(folder, null, 0, 0, 20);
		folder.release();
	}
	@Test
	public void testReplicatedSftpPermissions() throws Exception {
		AbstractSftpResourceFolder folder = setupResourceFolder(2, 5);
		testReplicatedSftp(folder, null, 0, 0);
		testReplicatedSftp(folder, "r--r--r--", 0, 0);
		testReplicatedSftp(folder, "rwxrw-r--", 5196, 0);
		testReplicatedSftp(folder, "rw-rw-rw-", 5196, 3986);
		testReplicatedSftp(folder, "rw-rw----", 0, 3986);
		testReplicatedSftp(folder, null, 0, 3986);
		testReplicatedSftp(folder, null, 5196, 3986);
		testReplicatedSftp(folder, null, 5196, 0);
		folder.release();
	}

	protected void testReplicatedSftp(AbstractSftpResourceFolder folder, String permissions, long uid, long gid) throws Exception {
		testReplicatedSftp(folder, permissions, uid, gid, 1);
	}
	protected void testReplicatedSftp(AbstractSftpResourceFolder folder, String permissions, long uid, long gid, int files) throws Exception {
		folder.setInitialFilePermissions(permissions);
		folder.setInitialFileUID(uid);
		folder.setInitialFileGID(gid);
		for(int i = 0; i < files; i++) {
			Resource resource = folder.getResource("abc/def/test_a_" + (i+1) + ".txt", ResourceMode.CREATE);
			try {
				PrintWriter out = new PrintWriter(resource.getOutputStream());
				out.write("this is a test for replicated sftp resource folder\n");
				out.flush();
				out.close();
				log.info("write file name:" + resource.getName());
				log.info("write file key:" + resource.getKey());
			} finally {
				resource.release();
			}
			Resource resource2 = folder.getResource(resource.getKey(), ResourceMode.READ);
			try {
				resource2.setContentType("text/plain");
				log.info("file exist:" + resource2.exists());
				log.info("file size:" + resource2.getLength());
				log.info("file type:" + resource2.getContentType());
				log.info("file name:" + resource2.getName());
				log.info("file key:" + resource2.getKey());
				log.info("file content is:" + IOUtils.readFully(resource2.getInputStream()));
			} finally {
				resource2.release();
			}
		}
	}
	protected ReplicatedSftpResourceFolder setupResourceFolder(int replicas, int concurrency) {
		ReplicatedSftpResourceFolder sftpFolder = new ReplicatedSftpResourceFolder();
		SftpRepository[] repos = new SftpRepository[] {
			new SftpRepository("usawst01.usatech.com", "root", null, "/home/bkrug/file_repo_1")	,
			new SftpRepository("usaapd1.usatech.com", "bkrug", null, "/home/bkrug/file_repo_1")	,
			new SftpRepository("usadev01.usatech.com", "bkrug", null, "/home/bkrug/file_repo_1")
		};
		for(SftpRepository repo : repos) {
			if(repo.getPassword() == null) {
				String pass = getPassword("Resource Test", "Please enter the password for '" + repo.getUsername() + "' on " + repo.getHost());
				if(pass == null)
					throw new CancellationException("User cancelled Test");
				repo.setPassword(pass);
			}
		}
		sftpFolder.setRepositories(repos);
		sftpFolder.setConcurrency(concurrency);
		sftpFolder.setReplicas(replicas);
		return sftpFolder;
	}

	protected TaskListener setupTaskListener() throws IOException {
		final File taskTimesFile = File.createTempFile("TaskTimes-", ".csv");
		log.info("Writing Task times to " + taskTimesFile.getPath());
        PrintWriter pw = new PrintWriter(taskTimesFile);
        return new WriteCSVTaskListener(pw);
	}

	@Test
	public void testReplicatedSftpPerf() throws Exception {
		int[] sizes = new int[] {100,1000,10000,100000,1000000}; //,10000000};
		int[] replicas = new int[] { 1, 2, 3 };
		runReplicatedSftpPerfTest(sizes, replicas, new boolean[] {false,true}, new boolean[] {false,true}, 5);
	}
	@Test
	public void testReplicatedSftpConcurrent() throws Throwable {
		final TaskListener tl = setupTaskListener();
		final ReplicatedSftpResourceFolder folder = setupResourceFolder(2, 5);
		final Set<String> files = new HashSet<String>();
		final ResourceMode[] modes = ResourceMode.values();
		final int sz = 100000;

		multiThreadTest(new Runnable() {
			public void run() {
				try {
					String fileName = "concurrent_test_" + Thread.currentThread().getId() + ".txt";
					ResourceMode mode;
					if(files.add(fileName)) {
						mode = ResourceMode.CREATE;
					} else {
						mode = modes[(int)(modes.length*Math.random())];
					}
					log.info("Getting resource '" + fileName + "' with mode " + mode);
					tl.taskStarted("Get Resource", mode.toString());
					Resource res = folder.getResource(fileName, mode);
					try {
					tl.taskEnded("Get Resource");
					if(mode != ResourceMode.CREATE) {
						tl.taskStarted("Read File", mode.toString());
						int cnt = readFile(res);
						tl.taskEnded("Read File");
						log.info("Read " + cnt + " bytes from file '" + res.getKey() + "' of " + res.getLength()  + " bytes");
					}
					if(mode != ResourceMode.READ) {
						tl.taskStarted("Write File", mode.toString());
						writeFile(res.getOutputStream(), sz, false);
						tl.taskEnded("Write File");
						log.info("Wrote file '" + res.getKey() + "' of " + sz + " bytes");

					}
					} finally {
						res.release();
					}
				} catch(IOException e) {
					log.warn("Could not work with resource", e);
				}
			}
		}, 10, 400, 10, "ReplicatedSftpResourceTest");
		tl.flush();
		folder.release();
	}
	@Test
	public void testReplicatedSftpFaults() throws Throwable {
		final TaskListener tl = setupTaskListener();
		final ReplicatedSftpResourceFolder folder = setupResourceFolder(2, 5);
		final Set<String> files = new HashSet<String>();
		final ResourceMode[] modes = ResourceMode.values();
		final int sz = 100000;
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					String fileName = "faults_test_" + Thread.currentThread().getId() + ".txt";
					ResourceMode mode;
					if(files.add(fileName)) {
						mode = ResourceMode.CREATE;
					} else {
						mode = modes[(int)(modes.length*Math.random())];
					}
					log.info("Getting resource '" + fileName + "' with mode " + mode);
					tl.taskStarted("Get Resource", mode.toString());
					Resource res = folder.getResource(fileName, mode);
					try {
					tl.taskEnded("Get Resource");
					if(mode != ResourceMode.CREATE) {
						tl.taskStarted("Read File", mode.toString());
						int cnt = readFile(res);
						tl.taskEnded("Read File");
						log.info("Read " + cnt + " bytes from file '" + res.getKey() + "' of " + res.getLength()  + " bytes");
					}
					if(mode != ResourceMode.READ) {
						tl.taskStarted("Write File", mode.toString());
						writeFile(res.getOutputStream(), sz, false);
						tl.taskEnded("Write File");
						log.info("Wrote file '" + res.getKey() + "' of " + sz + " bytes");

					}
					} finally {
						res.release();
					}
				} catch(IOException e) {
					log.warn("Could not work with resource", e);
				}
			}
		};
		for(int i = 0; i < 4; i++)
			runnable.run();
		System.out.println("Press any key to continue...");
		System.in.read();
		for(int i = 0; i < 4; i++)
			runnable.run();

		tl.flush();
		folder.release();
	}
	protected int readFile(Resource res) throws IOException {
		InputStream in = res.getInputStream();
		byte[] buffer = new byte[1024];
		int read = 0;
        int count = 0;
        while((read = in.read(buffer)) > -1) {
            count += read;
        }
		return count;
	}

	@Test
	public void testReplicatedSftpParallell() throws Exception {
		//runReplicatedSftpPerfTest(new int[] {100000, 1000000, 5000000}, new int[] {1,2,3}, new boolean[] {false,true}, 3);
		runReplicatedSftpPerfTest(new int[] {500000}, new int[] {2}, new boolean[] {true}, new boolean[] {false,true}, 10);
	}
	protected void runReplicatedSftpPerfTest(int[] sizes, int[] replicas, boolean[] parallel, boolean[] compression, int times) throws Exception {
		TaskListener tl = setupTaskListener();
		ReplicatedSftpResourceFolder folder = setupResourceFolder(1, 1);
		for(int r : replicas) {
			folder.setReplicas(r);
			for(boolean p : parallel) {
				folder.setParallelOutputStream(p);
				for(boolean c : compression) {
					for(int sz : sizes) {
						String name = "size_replica_test_" + sz + "_" + r + ".txt";
						String details = "Size: " + sz + "; Replicas: " + r + "; Parallel: " + p+ "; Compressed: " + c;
						for(int i = 0; i < times; i++) {
							tl.taskStarted("Get Resource", details);
							Resource res = folder.getResource(name, ResourceMode.CREATE);
							tl.taskEnded("Get Resource");
							log.info("Retrieved Resource " + res.getKey());
							tl.taskStarted("Write File", details);
							try {
								writeFile(res.getOutputStream(), sz, c);
								tl.taskEnded("Write File");
							} finally {
								res.release();
							}
							tl.flush();
							log.info("Wrote " + sz + " bytes to Resource " + res.getKey());
						}
					}
				}
			}
		}
		folder.release();
	}

	@Test
	public void testSizePerf() throws Exception {
		runPerfTest("simple/test/ResourceTest.properties", new int[] {500000}, 10);
	}

	protected void runPerfTest(String propertiesFileName, int[] sizes, int times) throws Exception {
		TaskListener tl = setupTaskListener();
		ResourceFolder folder = setupResourceFolder(propertiesFileName);
		for(int sz : sizes) {
			String name = folder.getClass().getSimpleName().toLowerCase() + "_test_size_" + sz + ".txt";
			String details = "ResourceFolder: " + folder + "; Size: " + sz;
			for(int i = 0; i < times; i++) {
				tl.taskStarted("Get Resource", details);
				Resource res = folder.getResource(name, ResourceMode.CREATE);
				tl.taskEnded("Get Resource");
				log.info("Retrieved Resource " + res.getKey());
				tl.taskStarted("Write File", details);
				try {
					writeFile(res.getOutputStream(), sz, false);
					tl.taskEnded("Write File");
				} finally {
					res.release();
				}
				tl.flush();
				log.info("Wrote " + sz + " bytes to Resource " + res.getKey());
			}
		}
		folder.release();
	}

	protected void writeFile(OutputStream outputStream, int sz, boolean compression) throws IOException {
		if(compression)
			outputStream = new DeflaterOutputStream(outputStream);
		byte[] prefix = "TEST RESOURCE FILE:".getBytes();
		byte[] suffix = "]\n".getBytes();
		char[] padding = new char[(int)Math.ceil(Math.log10(sz))];
		for(int i = 0; i < padding.length - 2; i++)
			padding[i] = '0';
		padding[padding.length - 2] = ':';
		padding[padding.length - 1] = '[';
		DecimalFormat format = new DecimalFormat(new String(padding));
		int extraDigits = 100 - prefix.length - suffix.length - padding.length;
		int i = 0;
		for(; i < sz - 100; i += 100) {
			outputStream.write(prefix);
			outputStream.write(format.format(1 + (i/100)).getBytes());
			for(int k = 0; k < extraDigits; k++)
				outputStream.write((char)(32 + (i + k) % 64));
			outputStream.write(suffix);
		}
		for(; i < sz; i++) {
			outputStream.write('-');
		}
		outputStream.flush();
	}
	@Test
	public void testResourceFolder() throws Exception {
		testResourceFolder("simple/test/ResourceTest.properties");
	}
	@Test
	public void testResourceFolderConcurrency() throws Throwable {
		final ResourceFolder folder = setupResourceFolder("simple/test/ResourceTest.properties");
		final int files = 50;
		final boolean update = true;
		final boolean delete = true;
		multiThreadTest(new Runnable() {
			public void run() {
				try {
					testResourceFolder(folder, files, update, delete);
				} catch(Exception e) {
					log.error("Got error", e);
				}
			}
		}, 10, 2, 10, "ConcurrenctResourceTest");
		folder.release();
	}
	protected ResourceFolder setupResourceFolder() throws Exception {
		return setupResourceFolder("simple/test/ResourceTest.properties");
	}

	protected ResourceFolder setupResourceFolder(String propertiesFileName) throws Exception {
		Configuration config = MainWithConfig.loadConfig(propertiesFileName, getClass(), null);
        BaseWithConfig.configureDataSourceFactory(config, null);
        BaseWithConfig.configureDataLayer(config);
        return BaseWithConfig.configure(ResourceFolder.class, config.subset("simple.io.resource.ResourceFolder"));

	}
	protected void testResourceFolder(String propertiesFileName) throws Exception {
		ResourceFolder folder = setupResourceFolder(propertiesFileName);
		try {
			//testResourceFolder(folder, 5, true, true);
			//testLargeResourceFolder(folder, new File("C:\\Users\\bkrug\\Documents\\_j2ssh.log"));
			testLargeResourceFolder(folder, new File("C:\\Users\\bkrug\\Documents\\app_layer_map_2068.dmp"));
		} catch(Exception e) {
			log.fatal("Failure", e);
		}
		log.info("Test Complete");
	}

	protected void testLargeResourceFolder(ResourceFolder folder, File file) throws Exception {
		Resource resource = folder.getResource(file.getName(), ResourceMode.CREATE);
		int size;
		try {
			OutputStream out = resource.getOutputStream();
			FileInputStream in = new FileInputStream(file);
			try {
				size = (int)IOUtils.copy(in, out);
			} finally {
				in.close();
			}
			out.flush();
			out.close();
			log.info("write file name:" + resource.getName());
			log.info("write file key:" + resource.getKey());
			log.info("write file length:" + resource.getLength());
			log.info("write file size:" + file.length());
		} finally {
			resource.release();
		}
		Resource resource2 = folder.getResource(resource.getKey(), ResourceMode.READ);
		try {
			log.info("file exist:" + resource2.exists());
			log.info("file size:" + resource2.getLength());
			log.info("file type:" + resource2.getContentType());
			log.info("file name:" + resource2.getName());
			log.info("file key:" + resource2.getKey());
			File tmp = new File(file.getAbsolutePath() + ".tmp");
			InputStream in = resource.getInputStream();
			FileOutputStream out = new FileOutputStream(tmp);
			try {
				size = (int)IOUtils.copy(in, out);
			} finally {
				out.close();
			}
			log.info("wrote " + size + " bytes to " + tmp.getAbsolutePath());
		} finally {
			resource2.release();
		}
		Resource resource4 = folder.getResource(resource.getKey(), ResourceMode.DELETE);
		try {
			if(resource4.delete()) {
				log.info("Deleted file '" + resource.getKey());
			} else {
				log.warn("Failed to deleted file '" + resource.getKey());
			}
		} finally {
			resource4.release();
		}	
	}
	protected void testResourceFolder(ResourceFolder folder, int files, boolean update, boolean delete) throws Exception {
		for(int i = 0; i < files; i++) {
			Resource resource = folder.getResource("abc/def/test_a_" + (i+1) + ".txt", ResourceMode.CREATE);
			int size;
			try {
				OutputStream out = resource.getOutputStream();
				String s = "this is a test for " + folder + " at " + new Date() + "\n";
				out.write(s.getBytes());
				out.flush();
				out.close();
				log.info("write file name:" + resource.getName());
				log.info("write file key:" + resource.getKey());
				log.info("write file length:" + resource.getLength());
				size = s.length();
				log.info("write file size:" + size);
			} finally {
				resource.release();
			}
			Resource resource2 = folder.getResource(resource.getKey(), ResourceMode.READ);
			try {
				log.info("file exist:" + resource2.exists());
				log.info("file size:" + resource2.getLength());
				log.info("file type:" + resource2.getContentType());
				log.info("file name:" + resource2.getName());
				log.info("file key:" + resource2.getKey());
				log.info("file content is:" + IOUtils.readFully(resource2.getInputStream()));
			} finally {
				resource2.release();
			}
			if(update) {
				Resource resource3 = folder.getResource(resource.getKey(), ResourceMode.UPDATE);
				try {
					resource3.setContentType("text/plain");
					PrintWriter out = new PrintWriter(resource3.getOutputStream());
					String s = "this is a test to UPDATE " + folder + " at " + new Date() + "\n";
					out.write(s);
					out.flush();
					out.close();
					log.info("updated file name:" + resource3.getName());
					log.info("updated file key:" + resource3.getKey());
					size = s.length();
					log.info("updated file length:" + resource3.getLength());
					log.info("updated file size:" + size);
				} finally {
					resource3.release();
				}
			}
			if(delete) {
				Resource resource4 = folder.getResource(resource.getKey(), ResourceMode.DELETE);
				try {
					if(resource4.delete()) {
						log.info("Deleted file '" + resource.getKey());
					} else {
						log.warn("Failed to deleted file '" + resource.getKey());
					}
				} finally {
					resource4.release();
				}
			}
		}
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
