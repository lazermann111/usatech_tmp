package simple.test;

import java.io.PrintWriter;
import java.text.ParseException;

import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceMode;
import simple.io.resource.sftp.ReplicatedSftpResourceFolder;
import simple.io.resource.sftp.SftpRepository;
import simple.net.protocol.AddProtocols;

public class ResourceTestCommand{
	private static final Log log = Log.getLog();
	protected static ReplicatedSftpResourceFolder setupResourceFolder(String[] urls, int replicas, int concurrency) throws ParseException{
		ReplicatedSftpResourceFolder sftpFolder = new ReplicatedSftpResourceFolder();
		SftpRepository[] repos = new SftpRepository[urls.length];
		for(int i=0; i<urls.length; i++){
			repos[i]=new SftpRepository(urls[i]);
		}
		
		sftpFolder.setRepositories(repos);
		sftpFolder.setConcurrency(concurrency);
		sftpFolder.setReplicas(replicas);
		sftpFolder.setCompressing(false);;
		return sftpFolder;
	}
	
	public static void main(String args[])throws Exception{
		AddProtocols.run();
		ReplicatedSftpResourceFolder folder=setupResourceFolder(args, args.length, 5);
		//folder.setInitialFilePermissions("rw-rw-rw-");
		//folder.setInitialFileUID(0);
		//folder.setInitialFileGID(0);
		Resource resource = folder.getResource("test.txt", ResourceMode.CREATE);
		try {
			PrintWriter out = new PrintWriter(resource.getOutputStream());
			out.write("this is a test for replicated sftp resource folder\n");
			out.flush();
			out.close();
			log.debug("write file name:" + resource.getName());
			log.debug("write file key:" + resource.getKey());
		} finally {
			resource.release();
		}
		Resource resource2 = folder.getResource(resource.getKey(), ResourceMode.READ);
		try {
			resource2.setContentType("text/plain");
			log.debug("file exist:" + resource2.exists());
			log.debug("file size:" + resource2.getLength());
			log.debug("file type:" + resource2.getContentType());
			log.debug("file name:" + resource2.getName());
			log.debug("file key:" + resource2.getKey());
			log.debug("file content is:" + IOUtils.readFully(resource2.getInputStream()));
		} finally {
			resource2.release();
		}
		folder.release();
	}
}
