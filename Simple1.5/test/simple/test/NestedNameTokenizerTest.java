package simple.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.bean.NestedNameTokenizer;
import simple.bean.NestedNameTokenizer.Token;
import simple.io.Log;

public class NestedNameTokenizerTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	
	@Test
	public void testTokenize() throws Exception {
		String[] props = new String[] {"name.first.last", "name(first).last", "name[1].last(first)", 
				"name[9]", "first.name[8]", "this.that[4].more", "this[that].goes.well",
				//"invalid[1.more", 
				//"invalid(2.more", 
				//"invalid[3]yuck.okay", 
				"invalid(four)bad.okay"};
		StringBuilder sb = new StringBuilder();
		for(String p : props) {
			System.out.println("'" + p + "':");
			
			Token[] tokens = new NestedNameTokenizer(p).tokenize();
			for(Token tok : tokens) {
				System.out.println(tok.getProperty() + "\t" + tok.getIndex() + "\t" + tok.getKey());
				sb.append(tok.getProperty());
				if(tok.isIndexed())
					sb.append('[').append(tok.getIndex()).append(']');
				else if(tok.isMapped())
					sb.append('(').append(tok.getKey()).append(')');
				sb.append('.');
			}
			System.out.println("------------------------------------");
			System.out.println();
			String s = sb.substring(0, sb.length()-1);
			assert p.equals(s) : '\'' + p + "' != '" + s + '\'';
		}
	}
	
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
