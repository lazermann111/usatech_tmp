package simple.xml.output;

import java.io.Writer;
import java.math.BigInteger;
import java.net.URI;
import java.util.Date;
import java.util.Locale;

import org.xml.sax.SAXException;

import simple.xml.Serializer;
import simple.xml.XSDUtils;

public class XhtmlOutput {
	protected final Serializer serializer;

	public abstract class ElementBase {
		protected final String name;
		protected ElementBase(String name) throws SAXException {
			this.name = name;
			serializer.startElement(name);
		}

		public void end() throws SAXException {
			serializer.endElement(name);
		}
	}

	public XhtmlOutput(Serializer serializer) {
		this.serializer = serializer;
	}
	public static enum AttributeDataType_ImgAlign {
		TOP("top"),
		MIDDLE("middle"),
		BOTTOM("bottom"),
		LEFT("left"),
		RIGHT("right"),
		;

		private final String value;

		private AttributeDataType_ImgAlign(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_TRules {
		NONE("none"),
		GROUPS("groups"),
		ROWS("rows"),
		COLS("cols"),
		ALL("all"),
		;

		private final String value;

		private AttributeDataType_TRules(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_InputType {
		TEXT("text"),
		PASSWORD("password"),
		CHECKBOX("checkbox"),
		RADIO("radio"),
		SUBMIT("submit"),
		RESET("reset"),
		FILE("file"),
		HIDDEN("hidden"),
		IMAGE("image"),
		BUTTON("button"),
		;

		private final String value;

		private AttributeDataType_InputType(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_Shape {
		RECT("rect"),
		CIRCLE("circle"),
		POLY("poly"),
		DEFAULT("default"),
		;

		private final String value;

		private AttributeDataType_Shape(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_TAlign {
		LEFT("left"),
		CENTER("center"),
		RIGHT("right"),
		;

		private final String value;

		private AttributeDataType_TAlign(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_TFrame {
		VOID("void"),
		ABOVE("above"),
		BELOW("below"),
		HSIDES("hsides"),
		LHS("lhs"),
		RHS("rhs"),
		VSIDES("vsides"),
		BOX("box"),
		BORDER("border"),
		;

		private final String value;

		private AttributeDataType_TFrame(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_Scope {
		ROW("row"),
		COL("col"),
		ROWGROUP("rowgroup"),
		COLGROUP("colgroup"),
		;

		private final String value;

		private AttributeDataType_Scope(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_CAlign {
		TOP("top"),
		BOTTOM("bottom"),
		LEFT("left"),
		RIGHT("right"),
		;

		private final String value;

		private AttributeDataType_CAlign(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_ULStyle {
		DISC("disc"),
		SQUARE("square"),
		CIRCLE("circle"),
		;

		private final String value;

		private AttributeDataType_ULStyle(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public static enum AttributeDataType_LAlign {
		TOP("top"),
		BOTTOM("bottom"),
		LEFT("left"),
		RIGHT("right"),
		;

		private final String value;

		private AttributeDataType_LAlign(String value) {
			this.value = value;
		}

		public String toString() {
			return value;
		}
	}

	public class Map extends ElementBase {
		protected Map() throws SAXException {
			super("map");
		}
		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String value) throws SAXException {
			serializer.addAttribute("", "class", "class", (value != null ? value : ""));
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

		public Area area() throws SAXException {
			return new Area();
		}

	}

	public class Font extends ElementBase {
		protected Font() throws SAXException {
			super("font");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _size(String value) throws SAXException {
			serializer.addAttribute("", "size", "size", (value != null ? value : ""));
		}

		public Writer _size() throws SAXException {
			return serializer.addAttributeByStream("", "size", "size");
		}

		public void _color(String value) throws SAXException {
			serializer.addAttribute("", "color", "color", (value != null ? value : ""));
		}

		public Writer _color() throws SAXException {
			return serializer.addAttributeByStream("", "color", "color");
		}

		public void _face(String value) throws SAXException {
			serializer.addAttribute("", "face", "face", (value != null ? value : ""));
		}

		public Writer _face() throws SAXException {
			return serializer.addAttributeByStream("", "face", "face");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class I extends ElementBase {
		protected I() throws SAXException {
			super("i");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Noframes extends ElementBase {
		protected Noframes() throws SAXException {
			super("noframes");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Hr extends ElementBase {
		protected Hr() throws SAXException {
			super("hr");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _noshade(String value) throws SAXException {
			serializer.addAttribute("", "noshade", "noshade", (value != null ? value : ""));
		}

		public Writer _noshade() throws SAXException {
			return serializer.addAttributeByStream("", "noshade", "noshade");
		}

		public void _size(BigInteger value) throws SAXException {
			serializer.addAttribute("", "size", "size", (value != null ? value.toString() : ""));
		}

		public Writer _size() throws SAXException {
			return serializer.addAttributeByStream("", "size", "size");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

	}

	public class Em extends ElementBase {
		protected Em() throws SAXException {
			super("em");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Iframe extends ElementBase {
		protected Iframe() throws SAXException {
			super("iframe");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _longdesc(URI value) throws SAXException {
			serializer.addAttribute("", "longdesc", "longdesc", (value != null ? value.toString() : ""));
		}

		public Writer _longdesc() throws SAXException {
			return serializer.addAttributeByStream("", "longdesc", "longdesc");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _src(URI value) throws SAXException {
			serializer.addAttribute("", "src", "src", (value != null ? value.toString() : ""));
		}

		public Writer _src() throws SAXException {
			return serializer.addAttributeByStream("", "src", "src");
		}

		public void _frameborder(String value) throws SAXException {
			serializer.addAttribute("", "frameborder", "frameborder", (value != null ? value : ""));
		}

		public Writer _frameborder() throws SAXException {
			return serializer.addAttributeByStream("", "frameborder", "frameborder");
		}

		public void _marginwidth(BigInteger value) throws SAXException {
			serializer.addAttribute("", "marginwidth", "marginwidth", (value != null ? value.toString() : ""));
		}

		public Writer _marginwidth() throws SAXException {
			return serializer.addAttributeByStream("", "marginwidth", "marginwidth");
		}

		public void _marginheight(BigInteger value) throws SAXException {
			serializer.addAttribute("", "marginheight", "marginheight", (value != null ? value.toString() : ""));
		}

		public Writer _marginheight() throws SAXException {
			return serializer.addAttributeByStream("", "marginheight", "marginheight");
		}

		public void _scrolling(String value) throws SAXException {
			serializer.addAttribute("", "scrolling", "scrolling", (value != null ? value : ""));
		}

		public Writer _scrolling() throws SAXException {
			return serializer.addAttributeByStream("", "scrolling", "scrolling");
		}

		public void _align(AttributeDataType_ImgAlign value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value.toString() : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _height(String value) throws SAXException {
			serializer.addAttribute("", "height", "height", (value != null ? value : ""));
		}

		public Writer _height() throws SAXException {
			return serializer.addAttributeByStream("", "height", "height");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Select extends ElementBase {
		protected Select() throws SAXException {
			super("select");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _size(BigInteger value) throws SAXException {
			serializer.addAttribute("", "size", "size", (value != null ? value.toString() : ""));
		}

		public Writer _size() throws SAXException {
			return serializer.addAttributeByStream("", "size", "size");
		}

		public void _multiple(String value) throws SAXException {
			serializer.addAttribute("", "multiple", "multiple", (value != null ? value : ""));
		}

		public Writer _multiple() throws SAXException {
			return serializer.addAttributeByStream("", "multiple", "multiple");
		}

		public void _disabled(String value) throws SAXException {
			serializer.addAttribute("", "disabled", "disabled", (value != null ? value : ""));
		}

		public Writer _disabled() throws SAXException {
			return serializer.addAttributeByStream("", "disabled", "disabled");
		}

		public void _tabindex(BigInteger value) throws SAXException {
			serializer.addAttribute("", "tabindex", "tabindex", (value != null ? value.toString() : ""));
		}

		public Writer _tabindex() throws SAXException {
			return serializer.addAttributeByStream("", "tabindex", "tabindex");
		}

		public void _onfocus(String value) throws SAXException {
			serializer.addAttribute("", "onfocus", "onfocus", (value != null ? value : ""));
		}

		public Writer _onfocus() throws SAXException {
			return serializer.addAttributeByStream("", "onfocus", "onfocus");
		}

		public void _onblur(String value) throws SAXException {
			serializer.addAttribute("", "onblur", "onblur", (value != null ? value : ""));
		}

		public Writer _onblur() throws SAXException {
			return serializer.addAttributeByStream("", "onblur", "onblur");
		}

		public void _onchange(String value) throws SAXException {
			serializer.addAttribute("", "onchange", "onchange", (value != null ? value : ""));
		}

		public Writer _onchange() throws SAXException {
			return serializer.addAttributeByStream("", "onchange", "onchange");
		}

		public Optgroup optgroup() throws SAXException {
			return new Optgroup();
		}

		public Option option() throws SAXException {
			return new Option();
		}

	}

	public class Label extends ElementBase {
		protected Label() throws SAXException {
			super("label");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _for(String value) throws SAXException {
			serializer.addAttribute("", "for", "for", (value != null ? value : ""));
		}

		public Writer _for() throws SAXException {
			return serializer.addAttributeByStream("", "for", "for");
		}

		public void _accesskey(String value) throws SAXException {
			serializer.addAttribute("", "accesskey", "accesskey", (value != null ? value : ""));
		}

		public Writer _accesskey() throws SAXException {
			return serializer.addAttributeByStream("", "accesskey", "accesskey");
		}

		public void _onfocus(String value) throws SAXException {
			serializer.addAttribute("", "onfocus", "onfocus", (value != null ? value : ""));
		}

		public Writer _onfocus() throws SAXException {
			return serializer.addAttributeByStream("", "onfocus", "onfocus");
		}

		public void _onblur(String value) throws SAXException {
			serializer.addAttribute("", "onblur", "onblur", (value != null ? value : ""));
		}

		public Writer _onblur() throws SAXException {
			return serializer.addAttributeByStream("", "onblur", "onblur");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Object extends ElementBase {
		protected Object() throws SAXException {
			super("object");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _declare(String value) throws SAXException {
			serializer.addAttribute("", "declare", "declare", (value != null ? value : ""));
		}

		public Writer _declare() throws SAXException {
			return serializer.addAttributeByStream("", "declare", "declare");
		}

		public void _classid(URI value) throws SAXException {
			serializer.addAttribute("", "classid", "classid", (value != null ? value.toString() : ""));
		}

		public Writer _classid() throws SAXException {
			return serializer.addAttributeByStream("", "classid", "classid");
		}

		public void _codebase(URI value) throws SAXException {
			serializer.addAttribute("", "codebase", "codebase", (value != null ? value.toString() : ""));
		}

		public Writer _codebase() throws SAXException {
			return serializer.addAttributeByStream("", "codebase", "codebase");
		}

		public void _data(URI value) throws SAXException {
			serializer.addAttribute("", "data", "data", (value != null ? value.toString() : ""));
		}

		public Writer _data() throws SAXException {
			return serializer.addAttributeByStream("", "data", "data");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _codetype(String value) throws SAXException {
			serializer.addAttribute("", "codetype", "codetype", (value != null ? value : ""));
		}

		public Writer _codetype() throws SAXException {
			return serializer.addAttributeByStream("", "codetype", "codetype");
		}

		public void _archive(String value) throws SAXException {
			serializer.addAttribute("", "archive", "archive", (value != null ? value : ""));
		}

		public Writer _archive() throws SAXException {
			return serializer.addAttributeByStream("", "archive", "archive");
		}

		public void _standby(String value) throws SAXException {
			serializer.addAttribute("", "standby", "standby", (value != null ? value : ""));
		}

		public Writer _standby() throws SAXException {
			return serializer.addAttributeByStream("", "standby", "standby");
		}

		public void _height(String value) throws SAXException {
			serializer.addAttribute("", "height", "height", (value != null ? value : ""));
		}

		public Writer _height() throws SAXException {
			return serializer.addAttributeByStream("", "height", "height");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _usemap(URI value) throws SAXException {
			serializer.addAttribute("", "usemap", "usemap", (value != null ? value.toString() : ""));
		}

		public Writer _usemap() throws SAXException {
			return serializer.addAttributeByStream("", "usemap", "usemap");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _tabindex(BigInteger value) throws SAXException {
			serializer.addAttribute("", "tabindex", "tabindex", (value != null ? value.toString() : ""));
		}

		public Writer _tabindex() throws SAXException {
			return serializer.addAttributeByStream("", "tabindex", "tabindex");
		}

		public void _align(AttributeDataType_ImgAlign value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value.toString() : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _border(BigInteger value) throws SAXException {
			serializer.addAttribute("", "border", "border", (value != null ? value.toString() : ""));
		}

		public Writer _border() throws SAXException {
			return serializer.addAttributeByStream("", "border", "border");
		}

		public void _hspace(BigInteger value) throws SAXException {
			serializer.addAttribute("", "hspace", "hspace", (value != null ? value.toString() : ""));
		}

		public Writer _hspace() throws SAXException {
			return serializer.addAttributeByStream("", "hspace", "hspace");
		}

		public void _vspace(BigInteger value) throws SAXException {
			serializer.addAttribute("", "vspace", "vspace", (value != null ? value.toString() : ""));
		}

		public Writer _vspace() throws SAXException {
			return serializer.addAttributeByStream("", "vspace", "vspace");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public Param param() throws SAXException {
			return new Param();
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Sub extends ElementBase {
		protected Sub() throws SAXException {
			super("sub");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Col extends ElementBase {
		protected Col() throws SAXException {
			super("col");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _span(BigInteger value) throws SAXException {
			serializer.addAttribute("", "span", "span", (value != null ? value.toString() : ""));
		}

		public Writer _span() throws SAXException {
			return serializer.addAttributeByStream("", "span", "span");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _char(String value) throws SAXException {
			serializer.addAttribute("", "char", "char", (value != null ? value : ""));
		}

		public Writer _char() throws SAXException {
			return serializer.addAttributeByStream("", "char", "char");
		}

		public void _charoff(String value) throws SAXException {
			serializer.addAttribute("", "charoff", "charoff", (value != null ? value : ""));
		}

		public Writer _charoff() throws SAXException {
			return serializer.addAttributeByStream("", "charoff", "charoff");
		}

		public void _valign(String value) throws SAXException {
			serializer.addAttribute("", "valign", "valign", (value != null ? value : ""));
		}

		public Writer _valign() throws SAXException {
			return serializer.addAttributeByStream("", "valign", "valign");
		}

	}

	public class P extends ElementBase {
		protected P() throws SAXException {
			super("p");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Q extends ElementBase {
		protected Q() throws SAXException {
			super("q");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _cite(URI value) throws SAXException {
			serializer.addAttribute("", "cite", "cite", (value != null ? value.toString() : ""));
		}

		public Writer _cite() throws SAXException {
			return serializer.addAttributeByStream("", "cite", "cite");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Html extends ElementBase {
		protected Html() throws SAXException {
			super("html");
		}
		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public Head head() throws SAXException {
			return new Head();
		}

		public Body body() throws SAXException {
			return new Body();
		}

	}

	public class S extends ElementBase {
		protected S() throws SAXException {
			super("s");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Ol extends ElementBase {
		protected Ol() throws SAXException {
			super("ol");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _compact(String value) throws SAXException {
			serializer.addAttribute("", "compact", "compact", (value != null ? value : ""));
		}

		public Writer _compact() throws SAXException {
			return serializer.addAttributeByStream("", "compact", "compact");
		}

		public void _start(BigInteger value) throws SAXException {
			serializer.addAttribute("", "start", "start", (value != null ? value.toString() : ""));
		}

		public Writer _start() throws SAXException {
			return serializer.addAttributeByStream("", "start", "start");
		}

		public Li li() throws SAXException {
			return new Li();
		}

	}

	public class U extends ElementBase {
		protected U() throws SAXException {
			super("u");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Kbd extends ElementBase {
		protected Kbd() throws SAXException {
			super("kbd");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Body extends ElementBase {
		protected Body() throws SAXException {
			super("body");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _onload(String value) throws SAXException {
			serializer.addAttribute("", "onload", "onload", (value != null ? value : ""));
		}

		public Writer _onload() throws SAXException {
			return serializer.addAttributeByStream("", "onload", "onload");
		}

		public void _onunload(String value) throws SAXException {
			serializer.addAttribute("", "onunload", "onunload", (value != null ? value : ""));
		}

		public Writer _onunload() throws SAXException {
			return serializer.addAttributeByStream("", "onunload", "onunload");
		}

		public void _background(URI value) throws SAXException {
			serializer.addAttribute("", "background", "background", (value != null ? value.toString() : ""));
		}

		public Writer _background() throws SAXException {
			return serializer.addAttributeByStream("", "background", "background");
		}

		public void _bgcolor(String value) throws SAXException {
			serializer.addAttribute("", "bgcolor", "bgcolor", (value != null ? value : ""));
		}

		public Writer _bgcolor() throws SAXException {
			return serializer.addAttributeByStream("", "bgcolor", "bgcolor");
		}

		public void _text(String value) throws SAXException {
			serializer.addAttribute("", "text", "text", (value != null ? value : ""));
		}

		public Writer _text() throws SAXException {
			return serializer.addAttributeByStream("", "text", "text");
		}

		public void _link(String value) throws SAXException {
			serializer.addAttribute("", "link", "link", (value != null ? value : ""));
		}

		public Writer _link() throws SAXException {
			return serializer.addAttributeByStream("", "link", "link");
		}

		public void _vlink(String value) throws SAXException {
			serializer.addAttribute("", "vlink", "vlink", (value != null ? value : ""));
		}

		public Writer _vlink() throws SAXException {
			return serializer.addAttributeByStream("", "vlink", "vlink");
		}

		public void _alink(String value) throws SAXException {
			serializer.addAttribute("", "alink", "alink", (value != null ? value : ""));
		}

		public Writer _alink() throws SAXException {
			return serializer.addAttributeByStream("", "alink", "alink");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Form extends ElementBase {
		protected Form() throws SAXException {
			super("form");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _action(URI value) throws SAXException {
			serializer.addAttribute("", "action", "action", (value != null ? value.toString() : ""));
		}

		public Writer _action() throws SAXException {
			return serializer.addAttributeByStream("", "action", "action");
		}

		public void _method(String value) throws SAXException {
			serializer.addAttribute("", "method", "method", (value != null ? value : ""));
		}

		public Writer _method() throws SAXException {
			return serializer.addAttributeByStream("", "method", "method");
		}

		public void _enctype(String value) throws SAXException {
			serializer.addAttribute("", "enctype", "enctype", (value != null ? value : ""));
		}

		public Writer _enctype() throws SAXException {
			return serializer.addAttributeByStream("", "enctype", "enctype");
		}

		public void _onsubmit(String value) throws SAXException {
			serializer.addAttribute("", "onsubmit", "onsubmit", (value != null ? value : ""));
		}

		public Writer _onsubmit() throws SAXException {
			return serializer.addAttributeByStream("", "onsubmit", "onsubmit");
		}

		public void _onreset(String value) throws SAXException {
			serializer.addAttribute("", "onreset", "onreset", (value != null ? value : ""));
		}

		public Writer _onreset() throws SAXException {
			return serializer.addAttributeByStream("", "onreset", "onreset");
		}

		public void _accept(String value) throws SAXException {
			serializer.addAttribute("", "accept", "accept", (value != null ? value : ""));
		}

		public Writer _accept() throws SAXException {
			return serializer.addAttributeByStream("", "accept", "accept");
		}

		public void _acceptCharset(String value) throws SAXException {
			serializer.addAttribute("", "accept-charset", "accept-charset", (value != null ? value : ""));
		}

		public Writer _acceptCharset() throws SAXException {
			return serializer.addAttributeByStream("", "accept-charset", "accept-charset");
		}

		public void _target(String value) throws SAXException {
			serializer.addAttribute("", "target", "target", (value != null ? value : ""));
		}

		public Writer _target() throws SAXException {
			return serializer.addAttributeByStream("", "target", "target");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Acronym extends ElementBase {
		protected Acronym() throws SAXException {
			super("acronym");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Option extends ElementBase {
		protected Option() throws SAXException {
			super("option");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _selected(String value) throws SAXException {
			serializer.addAttribute("", "selected", "selected", (value != null ? value : ""));
		}

		public Writer _selected() throws SAXException {
			return serializer.addAttributeByStream("", "selected", "selected");
		}

		public void _disabled(String value) throws SAXException {
			serializer.addAttribute("", "disabled", "disabled", (value != null ? value : ""));
		}

		public Writer _disabled() throws SAXException {
			return serializer.addAttributeByStream("", "disabled", "disabled");
		}

		public void _label(String value) throws SAXException {
			serializer.addAttribute("", "label", "label", (value != null ? value : ""));
		}

		public Writer _label() throws SAXException {
			return serializer.addAttributeByStream("", "label", "label");
		}

		public void _value(String value) throws SAXException {
			serializer.addAttribute("", "value", "value", (value != null ? value : ""));
		}

		public Writer _value() throws SAXException {
			return serializer.addAttributeByStream("", "value", "value");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

	}

	public class Li extends ElementBase {
		protected Li() throws SAXException {
			super("li");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _value(BigInteger value) throws SAXException {
			serializer.addAttribute("", "value", "value", (value != null ? value.toString() : ""));
		}

		public Writer _value() throws SAXException {
			return serializer.addAttributeByStream("", "value", "value");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Param extends ElementBase {
		protected Param() throws SAXException {
			super("param");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _value(String value) throws SAXException {
			serializer.addAttribute("", "value", "value", (value != null ? value : ""));
		}

		public Writer _value() throws SAXException {
			return serializer.addAttributeByStream("", "value", "value");
		}

		public void _valuetype(String value) throws SAXException {
			serializer.addAttribute("", "valuetype", "valuetype", (value != null ? value : ""));
		}

		public Writer _valuetype() throws SAXException {
			return serializer.addAttributeByStream("", "valuetype", "valuetype");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

	}

	public class Basefont extends ElementBase {
		protected Basefont() throws SAXException {
			super("basefont");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _size(String value) throws SAXException {
			serializer.addAttribute("", "size", "size", (value != null ? value : ""));
		}

		public Writer _size() throws SAXException {
			return serializer.addAttributeByStream("", "size", "size");
		}

		public void _color(String value) throws SAXException {
			serializer.addAttribute("", "color", "color", (value != null ? value : ""));
		}

		public Writer _color() throws SAXException {
			return serializer.addAttributeByStream("", "color", "color");
		}

		public void _face(String value) throws SAXException {
			serializer.addAttribute("", "face", "face", (value != null ? value : ""));
		}

		public Writer _face() throws SAXException {
			return serializer.addAttributeByStream("", "face", "face");
		}

	}

	public class Br extends ElementBase {
		protected Br() throws SAXException {
			super("br");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _clear(String value) throws SAXException {
			serializer.addAttribute("", "clear", "clear", (value != null ? value : ""));
		}

		public Writer _clear() throws SAXException {
			return serializer.addAttributeByStream("", "clear", "clear");
		}

	}

	public class Textarea extends ElementBase {
		protected Textarea() throws SAXException {
			super("textarea");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _accesskey(String value) throws SAXException {
			serializer.addAttribute("", "accesskey", "accesskey", (value != null ? value : ""));
		}

		public Writer _accesskey() throws SAXException {
			return serializer.addAttributeByStream("", "accesskey", "accesskey");
		}

		public void _tabindex(BigInteger value) throws SAXException {
			serializer.addAttribute("", "tabindex", "tabindex", (value != null ? value.toString() : ""));
		}

		public Writer _tabindex() throws SAXException {
			return serializer.addAttributeByStream("", "tabindex", "tabindex");
		}

		public void _onfocus(String value) throws SAXException {
			serializer.addAttribute("", "onfocus", "onfocus", (value != null ? value : ""));
		}

		public Writer _onfocus() throws SAXException {
			return serializer.addAttributeByStream("", "onfocus", "onfocus");
		}

		public void _onblur(String value) throws SAXException {
			serializer.addAttribute("", "onblur", "onblur", (value != null ? value : ""));
		}

		public Writer _onblur() throws SAXException {
			return serializer.addAttributeByStream("", "onblur", "onblur");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _rows(BigInteger value) throws SAXException {
			serializer.addAttribute("", "rows", "rows", (value != null ? value.toString() : ""));
		}

		public Writer _rows() throws SAXException {
			return serializer.addAttributeByStream("", "rows", "rows");
		}

		public void _cols(BigInteger value) throws SAXException {
			serializer.addAttribute("", "cols", "cols", (value != null ? value.toString() : ""));
		}

		public Writer _cols() throws SAXException {
			return serializer.addAttributeByStream("", "cols", "cols");
		}

		public void _disabled(String value) throws SAXException {
			serializer.addAttribute("", "disabled", "disabled", (value != null ? value : ""));
		}

		public Writer _disabled() throws SAXException {
			return serializer.addAttributeByStream("", "disabled", "disabled");
		}

		public void _readonly(String value) throws SAXException {
			serializer.addAttribute("", "readonly", "readonly", (value != null ? value : ""));
		}

		public Writer _readonly() throws SAXException {
			return serializer.addAttributeByStream("", "readonly", "readonly");
		}

		public void _onselect(String value) throws SAXException {
			serializer.addAttribute("", "onselect", "onselect", (value != null ? value : ""));
		}

		public Writer _onselect() throws SAXException {
			return serializer.addAttributeByStream("", "onselect", "onselect");
		}

		public void _onchange(String value) throws SAXException {
			serializer.addAttribute("", "onchange", "onchange", (value != null ? value : ""));
		}

		public Writer _onchange() throws SAXException {
			return serializer.addAttributeByStream("", "onchange", "onchange");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

	}

	public class Thead extends ElementBase {
		protected Thead() throws SAXException {
			super("thead");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _char(String value) throws SAXException {
			serializer.addAttribute("", "char", "char", (value != null ? value : ""));
		}

		public Writer _char() throws SAXException {
			return serializer.addAttributeByStream("", "char", "char");
		}

		public void _charoff(String value) throws SAXException {
			serializer.addAttribute("", "charoff", "charoff", (value != null ? value : ""));
		}

		public Writer _charoff() throws SAXException {
			return serializer.addAttributeByStream("", "charoff", "charoff");
		}

		public void _valign(String value) throws SAXException {
			serializer.addAttribute("", "valign", "valign", (value != null ? value : ""));
		}

		public Writer _valign() throws SAXException {
			return serializer.addAttributeByStream("", "valign", "valign");
		}

		public Tr tr() throws SAXException {
			return new Tr();
		}

	}

	public class Sup extends ElementBase {
		protected Sup() throws SAXException {
			super("sup");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Bdo extends ElementBase {
		protected Bdo() throws SAXException {
			super("bdo");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Meta extends ElementBase {
		protected Meta() throws SAXException {
			super("meta");
		}
		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _httpEquiv(String value) throws SAXException {
			serializer.addAttribute("", "http-equiv", "http-equiv", (value != null ? value : ""));
		}

		public Writer _httpEquiv() throws SAXException {
			return serializer.addAttributeByStream("", "http-equiv", "http-equiv");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _content(String value) throws SAXException {
			serializer.addAttribute("", "content", "content", (value != null ? value : ""));
		}

		public Writer _content() throws SAXException {
			return serializer.addAttributeByStream("", "content", "content");
		}

		public void _scheme(String value) throws SAXException {
			serializer.addAttribute("", "scheme", "scheme", (value != null ? value : ""));
		}

		public Writer _scheme() throws SAXException {
			return serializer.addAttributeByStream("", "scheme", "scheme");
		}

	}

	public class Blockquote extends ElementBase {
		protected Blockquote() throws SAXException {
			super("blockquote");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _cite(URI value) throws SAXException {
			serializer.addAttribute("", "cite", "cite", (value != null ? value.toString() : ""));
		}

		public Writer _cite() throws SAXException {
			return serializer.addAttributeByStream("", "cite", "cite");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Center extends ElementBase {
		protected Center() throws SAXException {
			super("center");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Title extends ElementBase {
		protected Title() throws SAXException {
			super("title");
		}
		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

	}

	public class Noscript extends ElementBase {
		protected Noscript() throws SAXException {
			super("noscript");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Table extends ElementBase {
		protected Table() throws SAXException {
			super("table");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _summary(String value) throws SAXException {
			serializer.addAttribute("", "summary", "summary", (value != null ? value : ""));
		}

		public Writer _summary() throws SAXException {
			return serializer.addAttributeByStream("", "summary", "summary");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _border(BigInteger value) throws SAXException {
			serializer.addAttribute("", "border", "border", (value != null ? value.toString() : ""));
		}

		public Writer _border() throws SAXException {
			return serializer.addAttributeByStream("", "border", "border");
		}

		public void _frame(AttributeDataType_TFrame value) throws SAXException {
			serializer.addAttribute("", "frame", "frame", (value != null ? value.toString() : ""));
		}

		public Writer _frame() throws SAXException {
			return serializer.addAttributeByStream("", "frame", "frame");
		}

		public void _rules(AttributeDataType_TRules value) throws SAXException {
			serializer.addAttribute("", "rules", "rules", (value != null ? value.toString() : ""));
		}

		public Writer _rules() throws SAXException {
			return serializer.addAttributeByStream("", "rules", "rules");
		}

		public void _cellspacing(String value) throws SAXException {
			serializer.addAttribute("", "cellspacing", "cellspacing", (value != null ? value : ""));
		}

		public Writer _cellspacing() throws SAXException {
			return serializer.addAttributeByStream("", "cellspacing", "cellspacing");
		}

		public void _cellpadding(String value) throws SAXException {
			serializer.addAttribute("", "cellpadding", "cellpadding", (value != null ? value : ""));
		}

		public Writer _cellpadding() throws SAXException {
			return serializer.addAttributeByStream("", "cellpadding", "cellpadding");
		}

		public void _align(AttributeDataType_TAlign value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value.toString() : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _bgcolor(String value) throws SAXException {
			serializer.addAttribute("", "bgcolor", "bgcolor", (value != null ? value : ""));
		}

		public Writer _bgcolor() throws SAXException {
			return serializer.addAttributeByStream("", "bgcolor", "bgcolor");
		}

		public Caption caption() throws SAXException {
			return new Caption();
		}

		public Col col() throws SAXException {
			return new Col();
		}

		public Colgroup colgroup() throws SAXException {
			return new Colgroup();
		}

		public Thead thead() throws SAXException {
			return new Thead();
		}

		public Tfoot tfoot() throws SAXException {
			return new Tfoot();
		}

		public Tbody tbody() throws SAXException {
			return new Tbody();
		}

		public Tr tr() throws SAXException {
			return new Tr();
		}

	}

	public class Script extends ElementBase {
		protected Script() throws SAXException {
			super("script");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _charset(String value) throws SAXException {
			serializer.addAttribute("", "charset", "charset", (value != null ? value : ""));
		}

		public Writer _charset() throws SAXException {
			return serializer.addAttributeByStream("", "charset", "charset");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _language(String value) throws SAXException {
			serializer.addAttribute("", "language", "language", (value != null ? value : ""));
		}

		public Writer _language() throws SAXException {
			return serializer.addAttributeByStream("", "language", "language");
		}

		public void _src(URI value) throws SAXException {
			serializer.addAttribute("", "src", "src", (value != null ? value.toString() : ""));
		}

		public Writer _src() throws SAXException {
			return serializer.addAttributeByStream("", "src", "src");
		}

		public void _defer(String value) throws SAXException {
			serializer.addAttribute("", "defer", "defer", (value != null ? value : ""));
		}

		public Writer _defer() throws SAXException {
			return serializer.addAttributeByStream("", "defer", "defer");
		}

		public void _xml_space(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "space", "space", (value != null ? value : ""));
		}

		public Writer _xml_space() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "space", "space");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

	}

	public class Base extends ElementBase {
		protected Base() throws SAXException {
			super("base");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _href(URI value) throws SAXException {
			serializer.addAttribute("", "href", "href", (value != null ? value.toString() : ""));
		}

		public Writer _href() throws SAXException {
			return serializer.addAttributeByStream("", "href", "href");
		}

		public void _target(String value) throws SAXException {
			serializer.addAttribute("", "target", "target", (value != null ? value : ""));
		}

		public Writer _target() throws SAXException {
			return serializer.addAttributeByStream("", "target", "target");
		}

	}

	public class Isindex extends ElementBase {
		protected Isindex() throws SAXException {
			super("isindex");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _prompt(String value) throws SAXException {
			serializer.addAttribute("", "prompt", "prompt", (value != null ? value : ""));
		}

		public Writer _prompt() throws SAXException {
			return serializer.addAttributeByStream("", "prompt", "prompt");
		}

	}

	public class H1 extends ElementBase {
		protected H1() throws SAXException {
			super("h1");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class H2 extends ElementBase {
		protected H2() throws SAXException {
			super("h2");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class H3 extends ElementBase {
		protected H3() throws SAXException {
			super("h3");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class H4 extends ElementBase {
		protected H4() throws SAXException {
			super("h4");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class H5 extends ElementBase {
		protected H5() throws SAXException {
			super("h5");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Input extends ElementBase {
		protected Input() throws SAXException {
			super("input");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _accesskey(String value) throws SAXException {
			serializer.addAttribute("", "accesskey", "accesskey", (value != null ? value : ""));
		}

		public Writer _accesskey() throws SAXException {
			return serializer.addAttributeByStream("", "accesskey", "accesskey");
		}

		public void _tabindex(BigInteger value) throws SAXException {
			serializer.addAttribute("", "tabindex", "tabindex", (value != null ? value.toString() : ""));
		}

		public Writer _tabindex() throws SAXException {
			return serializer.addAttributeByStream("", "tabindex", "tabindex");
		}

		public void _onfocus(String value) throws SAXException {
			serializer.addAttribute("", "onfocus", "onfocus", (value != null ? value : ""));
		}

		public Writer _onfocus() throws SAXException {
			return serializer.addAttributeByStream("", "onfocus", "onfocus");
		}

		public void _onblur(String value) throws SAXException {
			serializer.addAttribute("", "onblur", "onblur", (value != null ? value : ""));
		}

		public Writer _onblur() throws SAXException {
			return serializer.addAttributeByStream("", "onblur", "onblur");
		}

		public void _type(AttributeDataType_InputType value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value.toString() : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _value(String value) throws SAXException {
			serializer.addAttribute("", "value", "value", (value != null ? value : ""));
		}

		public Writer _value() throws SAXException {
			return serializer.addAttributeByStream("", "value", "value");
		}

		public void _checked(String value) throws SAXException {
			serializer.addAttribute("", "checked", "checked", (value != null ? value : ""));
		}

		public Writer _checked() throws SAXException {
			return serializer.addAttributeByStream("", "checked", "checked");
		}

		public void _disabled(String value) throws SAXException {
			serializer.addAttribute("", "disabled", "disabled", (value != null ? value : ""));
		}

		public Writer _disabled() throws SAXException {
			return serializer.addAttributeByStream("", "disabled", "disabled");
		}

		public void _readonly(String value) throws SAXException {
			serializer.addAttribute("", "readonly", "readonly", (value != null ? value : ""));
		}

		public Writer _readonly() throws SAXException {
			return serializer.addAttributeByStream("", "readonly", "readonly");
		}

		public void _size(String value) throws SAXException {
			serializer.addAttribute("", "size", "size", (value != null ? value : ""));
		}

		public Writer _size() throws SAXException {
			return serializer.addAttributeByStream("", "size", "size");
		}

		public void _maxlength(BigInteger value) throws SAXException {
			serializer.addAttribute("", "maxlength", "maxlength", (value != null ? value.toString() : ""));
		}

		public Writer _maxlength() throws SAXException {
			return serializer.addAttributeByStream("", "maxlength", "maxlength");
		}

		public void _src(URI value) throws SAXException {
			serializer.addAttribute("", "src", "src", (value != null ? value.toString() : ""));
		}

		public Writer _src() throws SAXException {
			return serializer.addAttributeByStream("", "src", "src");
		}

		public void _alt(String value) throws SAXException {
			serializer.addAttribute("", "alt", "alt", (value != null ? value : ""));
		}

		public Writer _alt() throws SAXException {
			return serializer.addAttributeByStream("", "alt", "alt");
		}

		public void _usemap(URI value) throws SAXException {
			serializer.addAttribute("", "usemap", "usemap", (value != null ? value.toString() : ""));
		}

		public Writer _usemap() throws SAXException {
			return serializer.addAttributeByStream("", "usemap", "usemap");
		}

		public void _onselect(String value) throws SAXException {
			serializer.addAttribute("", "onselect", "onselect", (value != null ? value : ""));
		}

		public Writer _onselect() throws SAXException {
			return serializer.addAttributeByStream("", "onselect", "onselect");
		}

		public void _onchange(String value) throws SAXException {
			serializer.addAttribute("", "onchange", "onchange", (value != null ? value : ""));
		}

		public Writer _onchange() throws SAXException {
			return serializer.addAttributeByStream("", "onchange", "onchange");
		}

		public void _accept(String value) throws SAXException {
			serializer.addAttribute("", "accept", "accept", (value != null ? value : ""));
		}

		public Writer _accept() throws SAXException {
			return serializer.addAttributeByStream("", "accept", "accept");
		}

		public void _align(AttributeDataType_ImgAlign value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value.toString() : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

	}

	public class H6 extends ElementBase {
		protected H6() throws SAXException {
			super("h6");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Button extends ElementBase {
		protected Button() throws SAXException {
			super("button");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _accesskey(String value) throws SAXException {
			serializer.addAttribute("", "accesskey", "accesskey", (value != null ? value : ""));
		}

		public Writer _accesskey() throws SAXException {
			return serializer.addAttributeByStream("", "accesskey", "accesskey");
		}

		public void _tabindex(BigInteger value) throws SAXException {
			serializer.addAttribute("", "tabindex", "tabindex", (value != null ? value.toString() : ""));
		}

		public Writer _tabindex() throws SAXException {
			return serializer.addAttributeByStream("", "tabindex", "tabindex");
		}

		public void _onfocus(String value) throws SAXException {
			serializer.addAttribute("", "onfocus", "onfocus", (value != null ? value : ""));
		}

		public Writer _onfocus() throws SAXException {
			return serializer.addAttributeByStream("", "onfocus", "onfocus");
		}

		public void _onblur(String value) throws SAXException {
			serializer.addAttribute("", "onblur", "onblur", (value != null ? value : ""));
		}

		public Writer _onblur() throws SAXException {
			return serializer.addAttributeByStream("", "onblur", "onblur");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _value(String value) throws SAXException {
			serializer.addAttribute("", "value", "value", (value != null ? value : ""));
		}

		public Writer _value() throws SAXException {
			return serializer.addAttributeByStream("", "value", "value");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _disabled(String value) throws SAXException {
			serializer.addAttribute("", "disabled", "disabled", (value != null ? value : ""));
		}

		public Writer _disabled() throws SAXException {
			return serializer.addAttributeByStream("", "disabled", "disabled");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Img extends ElementBase {
		protected Img() throws SAXException {
			super("img");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _src(URI value) throws SAXException {
			serializer.addAttribute("", "src", "src", (value != null ? value.toString() : ""));
		}

		public Writer _src() throws SAXException {
			return serializer.addAttributeByStream("", "src", "src");
		}

		public void _alt(String value) throws SAXException {
			serializer.addAttribute("", "alt", "alt", (value != null ? value : ""));
		}

		public Writer _alt() throws SAXException {
			return serializer.addAttributeByStream("", "alt", "alt");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _longdesc(URI value) throws SAXException {
			serializer.addAttribute("", "longdesc", "longdesc", (value != null ? value.toString() : ""));
		}

		public Writer _longdesc() throws SAXException {
			return serializer.addAttributeByStream("", "longdesc", "longdesc");
		}

		public void _height(String value) throws SAXException {
			serializer.addAttribute("", "height", "height", (value != null ? value : ""));
		}

		public Writer _height() throws SAXException {
			return serializer.addAttributeByStream("", "height", "height");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _usemap(URI value) throws SAXException {
			serializer.addAttribute("", "usemap", "usemap", (value != null ? value.toString() : ""));
		}

		public Writer _usemap() throws SAXException {
			return serializer.addAttributeByStream("", "usemap", "usemap");
		}

		public void _ismap(String value) throws SAXException {
			serializer.addAttribute("", "ismap", "ismap", (value != null ? value : ""));
		}

		public Writer _ismap() throws SAXException {
			return serializer.addAttributeByStream("", "ismap", "ismap");
		}

		public void _align(AttributeDataType_ImgAlign value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value.toString() : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _border(String value) throws SAXException {
			serializer.addAttribute("", "border", "border", (value != null ? value : ""));
		}

		public Writer _border() throws SAXException {
			return serializer.addAttributeByStream("", "border", "border");
		}

		public void _hspace(BigInteger value) throws SAXException {
			serializer.addAttribute("", "hspace", "hspace", (value != null ? value.toString() : ""));
		}

		public Writer _hspace() throws SAXException {
			return serializer.addAttributeByStream("", "hspace", "hspace");
		}

		public void _vspace(BigInteger value) throws SAXException {
			serializer.addAttribute("", "vspace", "vspace", (value != null ? value.toString() : ""));
		}

		public Writer _vspace() throws SAXException {
			return serializer.addAttributeByStream("", "vspace", "vspace");
		}

	}

	public class Fieldset extends ElementBase {
		protected Fieldset() throws SAXException {
			super("fieldset");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public Legend legend() throws SAXException {
			return new Legend();
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Del extends ElementBase {
		protected Del() throws SAXException {
			super("del");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _cite(URI value) throws SAXException {
			serializer.addAttribute("", "cite", "cite", (value != null ? value.toString() : ""));
		}

		public Writer _cite() throws SAXException {
			return serializer.addAttributeByStream("", "cite", "cite");
		}

		public void _datetime(Date value) throws SAXException {
			serializer.addAttribute("", "datetime", "datetime", XSDUtils.DATE_TIME_TYPE_FORMAT.format(value));
		}

		public Writer _datetime() throws SAXException {
			return serializer.addAttributeByStream("", "datetime", "datetime");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Td extends ElementBase {
		protected Td() throws SAXException {
			super("td");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _abbr(String value) throws SAXException {
			serializer.addAttribute("", "abbr", "abbr", (value != null ? value : ""));
		}

		public Writer _abbr() throws SAXException {
			return serializer.addAttributeByStream("", "abbr", "abbr");
		}

		public void _axis(String value) throws SAXException {
			serializer.addAttribute("", "axis", "axis", (value != null ? value : ""));
		}

		public Writer _axis() throws SAXException {
			return serializer.addAttributeByStream("", "axis", "axis");
		}

		public void _headers(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "headers", "headers", sb.toString());
		}

		public Writer _headers() throws SAXException {
			return serializer.addAttributeByStream("", "headers", "headers");
		}

		public void _scope(AttributeDataType_Scope value) throws SAXException {
			serializer.addAttribute("", "scope", "scope", (value != null ? value.toString() : ""));
		}

		public Writer _scope() throws SAXException {
			return serializer.addAttributeByStream("", "scope", "scope");
		}

		public void _rowspan(BigInteger value) throws SAXException {
			serializer.addAttribute("", "rowspan", "rowspan", (value != null ? value.toString() : ""));
		}

		public Writer _rowspan() throws SAXException {
			return serializer.addAttributeByStream("", "rowspan", "rowspan");
		}

		public void _colspan(BigInteger value) throws SAXException {
			serializer.addAttribute("", "colspan", "colspan", (value != null ? value.toString() : ""));
		}

		public Writer _colspan() throws SAXException {
			return serializer.addAttributeByStream("", "colspan", "colspan");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _char(String value) throws SAXException {
			serializer.addAttribute("", "char", "char", (value != null ? value : ""));
		}

		public Writer _char() throws SAXException {
			return serializer.addAttributeByStream("", "char", "char");
		}

		public void _charoff(String value) throws SAXException {
			serializer.addAttribute("", "charoff", "charoff", (value != null ? value : ""));
		}

		public Writer _charoff() throws SAXException {
			return serializer.addAttributeByStream("", "charoff", "charoff");
		}

		public void _valign(String value) throws SAXException {
			serializer.addAttribute("", "valign", "valign", (value != null ? value : ""));
		}

		public Writer _valign() throws SAXException {
			return serializer.addAttributeByStream("", "valign", "valign");
		}

		public void _nowrap(String value) throws SAXException {
			serializer.addAttribute("", "nowrap", "nowrap", (value != null ? value : ""));
		}

		public Writer _nowrap() throws SAXException {
			return serializer.addAttributeByStream("", "nowrap", "nowrap");
		}

		public void _bgcolor(String value) throws SAXException {
			serializer.addAttribute("", "bgcolor", "bgcolor", (value != null ? value : ""));
		}

		public Writer _bgcolor() throws SAXException {
			return serializer.addAttributeByStream("", "bgcolor", "bgcolor");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _height(String value) throws SAXException {
			serializer.addAttribute("", "height", "height", (value != null ? value : ""));
		}

		public Writer _height() throws SAXException {
			return serializer.addAttributeByStream("", "height", "height");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Menu extends ElementBase {
		protected Menu() throws SAXException {
			super("menu");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _compact(String value) throws SAXException {
			serializer.addAttribute("", "compact", "compact", (value != null ? value : ""));
		}

		public Writer _compact() throws SAXException {
			return serializer.addAttributeByStream("", "compact", "compact");
		}

		public Li li() throws SAXException {
			return new Li();
		}

	}

	public class Link extends ElementBase {
		protected Link() throws SAXException {
			super("link");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _charset(String value) throws SAXException {
			serializer.addAttribute("", "charset", "charset", (value != null ? value : ""));
		}

		public Writer _charset() throws SAXException {
			return serializer.addAttributeByStream("", "charset", "charset");
		}

		public void _href(URI value) throws SAXException {
			serializer.addAttribute("", "href", "href", (value != null ? value.toString() : ""));
		}

		public Writer _href() throws SAXException {
			return serializer.addAttributeByStream("", "href", "href");
		}

		public void _hreflang(Locale value) throws SAXException {
			serializer.addAttribute("", "hreflang", "hreflang", XSDUtils.toLanguage(value));
		}

		public Writer _hreflang() throws SAXException {
			return serializer.addAttributeByStream("", "hreflang", "hreflang");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _rel(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "rel", "rel", sb.toString());
		}

		public Writer _rel() throws SAXException {
			return serializer.addAttributeByStream("", "rel", "rel");
		}

		public void _rev(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "rev", "rev", sb.toString());
		}

		public Writer _rev() throws SAXException {
			return serializer.addAttributeByStream("", "rev", "rev");
		}

		public void _media(String value) throws SAXException {
			serializer.addAttribute("", "media", "media", (value != null ? value : ""));
		}

		public Writer _media() throws SAXException {
			return serializer.addAttributeByStream("", "media", "media");
		}

		public void _target(String value) throws SAXException {
			serializer.addAttribute("", "target", "target", (value != null ? value : ""));
		}

		public Writer _target() throws SAXException {
			return serializer.addAttributeByStream("", "target", "target");
		}

	}

	public class Th extends ElementBase {
		protected Th() throws SAXException {
			super("th");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _abbr(String value) throws SAXException {
			serializer.addAttribute("", "abbr", "abbr", (value != null ? value : ""));
		}

		public Writer _abbr() throws SAXException {
			return serializer.addAttributeByStream("", "abbr", "abbr");
		}

		public void _axis(String value) throws SAXException {
			serializer.addAttribute("", "axis", "axis", (value != null ? value : ""));
		}

		public Writer _axis() throws SAXException {
			return serializer.addAttributeByStream("", "axis", "axis");
		}

		public void _headers(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "headers", "headers", sb.toString());
		}

		public Writer _headers() throws SAXException {
			return serializer.addAttributeByStream("", "headers", "headers");
		}

		public void _scope(AttributeDataType_Scope value) throws SAXException {
			serializer.addAttribute("", "scope", "scope", (value != null ? value.toString() : ""));
		}

		public Writer _scope() throws SAXException {
			return serializer.addAttributeByStream("", "scope", "scope");
		}

		public void _rowspan(BigInteger value) throws SAXException {
			serializer.addAttribute("", "rowspan", "rowspan", (value != null ? value.toString() : ""));
		}

		public Writer _rowspan() throws SAXException {
			return serializer.addAttributeByStream("", "rowspan", "rowspan");
		}

		public void _colspan(BigInteger value) throws SAXException {
			serializer.addAttribute("", "colspan", "colspan", (value != null ? value.toString() : ""));
		}

		public Writer _colspan() throws SAXException {
			return serializer.addAttributeByStream("", "colspan", "colspan");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _char(String value) throws SAXException {
			serializer.addAttribute("", "char", "char", (value != null ? value : ""));
		}

		public Writer _char() throws SAXException {
			return serializer.addAttributeByStream("", "char", "char");
		}

		public void _charoff(String value) throws SAXException {
			serializer.addAttribute("", "charoff", "charoff", (value != null ? value : ""));
		}

		public Writer _charoff() throws SAXException {
			return serializer.addAttributeByStream("", "charoff", "charoff");
		}

		public void _valign(String value) throws SAXException {
			serializer.addAttribute("", "valign", "valign", (value != null ? value : ""));
		}

		public Writer _valign() throws SAXException {
			return serializer.addAttributeByStream("", "valign", "valign");
		}

		public void _nowrap(String value) throws SAXException {
			serializer.addAttribute("", "nowrap", "nowrap", (value != null ? value : ""));
		}

		public Writer _nowrap() throws SAXException {
			return serializer.addAttributeByStream("", "nowrap", "nowrap");
		}

		public void _bgcolor(String value) throws SAXException {
			serializer.addAttribute("", "bgcolor", "bgcolor", (value != null ? value : ""));
		}

		public Writer _bgcolor() throws SAXException {
			return serializer.addAttributeByStream("", "bgcolor", "bgcolor");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _height(String value) throws SAXException {
			serializer.addAttribute("", "height", "height", (value != null ? value : ""));
		}

		public Writer _height() throws SAXException {
			return serializer.addAttributeByStream("", "height", "height");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Optgroup extends ElementBase {
		protected Optgroup() throws SAXException {
			super("optgroup");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _disabled(String value) throws SAXException {
			serializer.addAttribute("", "disabled", "disabled", (value != null ? value : ""));
		}

		public Writer _disabled() throws SAXException {
			return serializer.addAttributeByStream("", "disabled", "disabled");
		}

		public void _label(String value) throws SAXException {
			serializer.addAttribute("", "label", "label", (value != null ? value : ""));
		}

		public Writer _label() throws SAXException {
			return serializer.addAttributeByStream("", "label", "label");
		}

		public Option option() throws SAXException {
			return new Option();
		}

	}

	public class Head extends ElementBase {
		protected Head() throws SAXException {
			super("head");
		}
		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _profile(URI value) throws SAXException {
			serializer.addAttribute("", "profile", "profile", (value != null ? value.toString() : ""));
		}

		public Writer _profile() throws SAXException {
			return serializer.addAttributeByStream("", "profile", "profile");
		}

		public Script script() throws SAXException {
			return new Script();
		}

		public Style style() throws SAXException {
			return new Style();
		}

		public Meta meta() throws SAXException {
			return new Meta();
		}

		public Link link() throws SAXException {
			return new Link();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Title title() throws SAXException {
			return new Title();
		}

	}

	public class Small extends ElementBase {
		protected Small() throws SAXException {
			super("small");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Var extends ElementBase {
		protected Var() throws SAXException {
			super("var");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Pre extends ElementBase {
		protected Pre() throws SAXException {
			super("pre");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _width(BigInteger value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value.toString() : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _xml_space(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "space", "space", (value != null ? value : ""));
		}

		public Writer _xml_space() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "space", "space");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Style extends ElementBase {
		protected Style() throws SAXException {
			super("style");
		}
		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _media(String value) throws SAXException {
			serializer.addAttribute("", "media", "media", (value != null ? value : ""));
		}

		public Writer _media() throws SAXException {
			return serializer.addAttributeByStream("", "media", "media");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _xml_space(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "space", "space", (value != null ? value : ""));
		}

		public Writer _xml_space() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "space", "space");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

	}

	public class Dd extends ElementBase {
		protected Dd() throws SAXException {
			super("dd");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Big extends ElementBase {
		protected Big() throws SAXException {
			super("big");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Applet extends ElementBase {
		protected Applet() throws SAXException {
			super("applet");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _codebase(URI value) throws SAXException {
			serializer.addAttribute("", "codebase", "codebase", (value != null ? value.toString() : ""));
		}

		public Writer _codebase() throws SAXException {
			return serializer.addAttributeByStream("", "codebase", "codebase");
		}

		public void _archive(String value) throws SAXException {
			serializer.addAttribute("", "archive", "archive", (value != null ? value : ""));
		}

		public Writer _archive() throws SAXException {
			return serializer.addAttributeByStream("", "archive", "archive");
		}

		public void _code(String value) throws SAXException {
			serializer.addAttribute("", "code", "code", (value != null ? value : ""));
		}

		public Writer _code() throws SAXException {
			return serializer.addAttributeByStream("", "code", "code");
		}

		public void _object(String value) throws SAXException {
			serializer.addAttribute("", "object", "object", (value != null ? value : ""));
		}

		public Writer _object() throws SAXException {
			return serializer.addAttributeByStream("", "object", "object");
		}

		public void _alt(String value) throws SAXException {
			serializer.addAttribute("", "alt", "alt", (value != null ? value : ""));
		}

		public Writer _alt() throws SAXException {
			return serializer.addAttributeByStream("", "alt", "alt");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _height(String value) throws SAXException {
			serializer.addAttribute("", "height", "height", (value != null ? value : ""));
		}

		public Writer _height() throws SAXException {
			return serializer.addAttributeByStream("", "height", "height");
		}

		public void _align(AttributeDataType_ImgAlign value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value.toString() : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _hspace(BigInteger value) throws SAXException {
			serializer.addAttribute("", "hspace", "hspace", (value != null ? value.toString() : ""));
		}

		public Writer _hspace() throws SAXException {
			return serializer.addAttributeByStream("", "hspace", "hspace");
		}

		public void _vspace(BigInteger value) throws SAXException {
			serializer.addAttribute("", "vspace", "vspace", (value != null ? value.toString() : ""));
		}

		public Writer _vspace() throws SAXException {
			return serializer.addAttributeByStream("", "vspace", "vspace");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public Param param() throws SAXException {
			return new Param();
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Tr extends ElementBase {
		protected Tr() throws SAXException {
			super("tr");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _char(String value) throws SAXException {
			serializer.addAttribute("", "char", "char", (value != null ? value : ""));
		}

		public Writer _char() throws SAXException {
			return serializer.addAttributeByStream("", "char", "char");
		}

		public void _charoff(String value) throws SAXException {
			serializer.addAttribute("", "charoff", "charoff", (value != null ? value : ""));
		}

		public Writer _charoff() throws SAXException {
			return serializer.addAttributeByStream("", "charoff", "charoff");
		}

		public void _valign(String value) throws SAXException {
			serializer.addAttribute("", "valign", "valign", (value != null ? value : ""));
		}

		public Writer _valign() throws SAXException {
			return serializer.addAttributeByStream("", "valign", "valign");
		}

		public void _bgcolor(String value) throws SAXException {
			serializer.addAttribute("", "bgcolor", "bgcolor", (value != null ? value : ""));
		}

		public Writer _bgcolor() throws SAXException {
			return serializer.addAttributeByStream("", "bgcolor", "bgcolor");
		}

		public Th th() throws SAXException {
			return new Th();
		}

		public Td td() throws SAXException {
			return new Td();
		}

	}

	public class Tbody extends ElementBase {
		protected Tbody() throws SAXException {
			super("tbody");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _char(String value) throws SAXException {
			serializer.addAttribute("", "char", "char", (value != null ? value : ""));
		}

		public Writer _char() throws SAXException {
			return serializer.addAttributeByStream("", "char", "char");
		}

		public void _charoff(String value) throws SAXException {
			serializer.addAttribute("", "charoff", "charoff", (value != null ? value : ""));
		}

		public Writer _charoff() throws SAXException {
			return serializer.addAttributeByStream("", "charoff", "charoff");
		}

		public void _valign(String value) throws SAXException {
			serializer.addAttribute("", "valign", "valign", (value != null ? value : ""));
		}

		public Writer _valign() throws SAXException {
			return serializer.addAttributeByStream("", "valign", "valign");
		}

		public Tr tr() throws SAXException {
			return new Tr();
		}

	}

	public class Cite extends ElementBase {
		protected Cite() throws SAXException {
			super("cite");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Tt extends ElementBase {
		protected Tt() throws SAXException {
			super("tt");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Dl extends ElementBase {
		protected Dl() throws SAXException {
			super("dl");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _compact(String value) throws SAXException {
			serializer.addAttribute("", "compact", "compact", (value != null ? value : ""));
		}

		public Writer _compact() throws SAXException {
			return serializer.addAttributeByStream("", "compact", "compact");
		}

		public Dt dt() throws SAXException {
			return new Dt();
		}

		public Dd dd() throws SAXException {
			return new Dd();
		}

	}

	public class Address extends ElementBase {
		protected Address() throws SAXException {
			super("address");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

		public P p() throws SAXException {
			return new P();
		}

	}

	public class Strike extends ElementBase {
		protected Strike() throws SAXException {
			super("strike");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Area extends ElementBase {
		protected Area() throws SAXException {
			super("area");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _accesskey(String value) throws SAXException {
			serializer.addAttribute("", "accesskey", "accesskey", (value != null ? value : ""));
		}

		public Writer _accesskey() throws SAXException {
			return serializer.addAttributeByStream("", "accesskey", "accesskey");
		}

		public void _tabindex(BigInteger value) throws SAXException {
			serializer.addAttribute("", "tabindex", "tabindex", (value != null ? value.toString() : ""));
		}

		public Writer _tabindex() throws SAXException {
			return serializer.addAttributeByStream("", "tabindex", "tabindex");
		}

		public void _onfocus(String value) throws SAXException {
			serializer.addAttribute("", "onfocus", "onfocus", (value != null ? value : ""));
		}

		public Writer _onfocus() throws SAXException {
			return serializer.addAttributeByStream("", "onfocus", "onfocus");
		}

		public void _onblur(String value) throws SAXException {
			serializer.addAttribute("", "onblur", "onblur", (value != null ? value : ""));
		}

		public Writer _onblur() throws SAXException {
			return serializer.addAttributeByStream("", "onblur", "onblur");
		}

		public void _shape(AttributeDataType_Shape value) throws SAXException {
			serializer.addAttribute("", "shape", "shape", (value != null ? value.toString() : ""));
		}

		public Writer _shape() throws SAXException {
			return serializer.addAttributeByStream("", "shape", "shape");
		}

		public void _coords(String value) throws SAXException {
			serializer.addAttribute("", "coords", "coords", (value != null ? value : ""));
		}

		public Writer _coords() throws SAXException {
			return serializer.addAttributeByStream("", "coords", "coords");
		}

		public void _href(URI value) throws SAXException {
			serializer.addAttribute("", "href", "href", (value != null ? value.toString() : ""));
		}

		public Writer _href() throws SAXException {
			return serializer.addAttributeByStream("", "href", "href");
		}

		public void _nohref(String value) throws SAXException {
			serializer.addAttribute("", "nohref", "nohref", (value != null ? value : ""));
		}

		public Writer _nohref() throws SAXException {
			return serializer.addAttributeByStream("", "nohref", "nohref");
		}

		public void _alt(String value) throws SAXException {
			serializer.addAttribute("", "alt", "alt", (value != null ? value : ""));
		}

		public Writer _alt() throws SAXException {
			return serializer.addAttributeByStream("", "alt", "alt");
		}

		public void _target(String value) throws SAXException {
			serializer.addAttribute("", "target", "target", (value != null ? value : ""));
		}

		public Writer _target() throws SAXException {
			return serializer.addAttributeByStream("", "target", "target");
		}

	}

	public class Abbr extends ElementBase {
		protected Abbr() throws SAXException {
			super("abbr");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Strong extends ElementBase {
		protected Strong() throws SAXException {
			super("strong");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Dt extends ElementBase {
		protected Dt() throws SAXException {
			super("dt");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Dir extends ElementBase {
		protected Dir() throws SAXException {
			super("dir");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _compact(String value) throws SAXException {
			serializer.addAttribute("", "compact", "compact", (value != null ? value : ""));
		}

		public Writer _compact() throws SAXException {
			return serializer.addAttributeByStream("", "compact", "compact");
		}

		public Li li() throws SAXException {
			return new Li();
		}

	}

	public class Colgroup extends ElementBase {
		protected Colgroup() throws SAXException {
			super("colgroup");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _span(BigInteger value) throws SAXException {
			serializer.addAttribute("", "span", "span", (value != null ? value.toString() : ""));
		}

		public Writer _span() throws SAXException {
			return serializer.addAttributeByStream("", "span", "span");
		}

		public void _width(String value) throws SAXException {
			serializer.addAttribute("", "width", "width", (value != null ? value : ""));
		}

		public Writer _width() throws SAXException {
			return serializer.addAttributeByStream("", "width", "width");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _char(String value) throws SAXException {
			serializer.addAttribute("", "char", "char", (value != null ? value : ""));
		}

		public Writer _char() throws SAXException {
			return serializer.addAttributeByStream("", "char", "char");
		}

		public void _charoff(String value) throws SAXException {
			serializer.addAttribute("", "charoff", "charoff", (value != null ? value : ""));
		}

		public Writer _charoff() throws SAXException {
			return serializer.addAttributeByStream("", "charoff", "charoff");
		}

		public void _valign(String value) throws SAXException {
			serializer.addAttribute("", "valign", "valign", (value != null ? value : ""));
		}

		public Writer _valign() throws SAXException {
			return serializer.addAttributeByStream("", "valign", "valign");
		}

		public Col col() throws SAXException {
			return new Col();
		}

	}

	public class Dfn extends ElementBase {
		protected Dfn() throws SAXException {
			super("dfn");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Div extends ElementBase {
		protected Div() throws SAXException {
			super("div");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Caption extends ElementBase {
		protected Caption() throws SAXException {
			super("caption");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(AttributeDataType_CAlign value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value.toString() : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Tfoot extends ElementBase {
		protected Tfoot() throws SAXException {
			super("tfoot");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _align(String value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void _char(String value) throws SAXException {
			serializer.addAttribute("", "char", "char", (value != null ? value : ""));
		}

		public Writer _char() throws SAXException {
			return serializer.addAttributeByStream("", "char", "char");
		}

		public void _charoff(String value) throws SAXException {
			serializer.addAttribute("", "charoff", "charoff", (value != null ? value : ""));
		}

		public Writer _charoff() throws SAXException {
			return serializer.addAttributeByStream("", "charoff", "charoff");
		}

		public void _valign(String value) throws SAXException {
			serializer.addAttribute("", "valign", "valign", (value != null ? value : ""));
		}

		public Writer _valign() throws SAXException {
			return serializer.addAttributeByStream("", "valign", "valign");
		}

		public Tr tr() throws SAXException {
			return new Tr();
		}

	}

	public class Legend extends ElementBase {
		protected Legend() throws SAXException {
			super("legend");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _accesskey(String value) throws SAXException {
			serializer.addAttribute("", "accesskey", "accesskey", (value != null ? value : ""));
		}

		public Writer _accesskey() throws SAXException {
			return serializer.addAttributeByStream("", "accesskey", "accesskey");
		}

		public void _align(AttributeDataType_LAlign value) throws SAXException {
			serializer.addAttribute("", "align", "align", (value != null ? value.toString() : ""));
		}

		public Writer _align() throws SAXException {
			return serializer.addAttributeByStream("", "align", "align");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Code extends ElementBase {
		protected Code() throws SAXException {
			super("code");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Samp extends ElementBase {
		protected Samp() throws SAXException {
			super("samp");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class A extends ElementBase {
		protected A() throws SAXException {
			super("a");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _accesskey(String value) throws SAXException {
			serializer.addAttribute("", "accesskey", "accesskey", (value != null ? value : ""));
		}

		public Writer _accesskey() throws SAXException {
			return serializer.addAttributeByStream("", "accesskey", "accesskey");
		}

		public void _tabindex(BigInteger value) throws SAXException {
			serializer.addAttribute("", "tabindex", "tabindex", (value != null ? value.toString() : ""));
		}

		public Writer _tabindex() throws SAXException {
			return serializer.addAttributeByStream("", "tabindex", "tabindex");
		}

		public void _onfocus(String value) throws SAXException {
			serializer.addAttribute("", "onfocus", "onfocus", (value != null ? value : ""));
		}

		public Writer _onfocus() throws SAXException {
			return serializer.addAttributeByStream("", "onfocus", "onfocus");
		}

		public void _onblur(String value) throws SAXException {
			serializer.addAttribute("", "onblur", "onblur", (value != null ? value : ""));
		}

		public Writer _onblur() throws SAXException {
			return serializer.addAttributeByStream("", "onblur", "onblur");
		}

		public void _charset(String value) throws SAXException {
			serializer.addAttribute("", "charset", "charset", (value != null ? value : ""));
		}

		public Writer _charset() throws SAXException {
			return serializer.addAttributeByStream("", "charset", "charset");
		}

		public void _type(String value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _name(String value) throws SAXException {
			serializer.addAttribute("", "name", "name", (value != null ? value : ""));
		}

		public Writer _name() throws SAXException {
			return serializer.addAttributeByStream("", "name", "name");
		}

		public void _href(URI value) throws SAXException {
			serializer.addAttribute("", "href", "href", (value != null ? value.toString() : ""));
		}

		public Writer _href() throws SAXException {
			return serializer.addAttributeByStream("", "href", "href");
		}

		public void _hreflang(Locale value) throws SAXException {
			serializer.addAttribute("", "hreflang", "hreflang", XSDUtils.toLanguage(value));
		}

		public Writer _hreflang() throws SAXException {
			return serializer.addAttributeByStream("", "hreflang", "hreflang");
		}

		public void _rel(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "rel", "rel", sb.toString());
		}

		public Writer _rel() throws SAXException {
			return serializer.addAttributeByStream("", "rel", "rel");
		}

		public void _rev(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "rev", "rev", sb.toString());
		}

		public Writer _rev() throws SAXException {
			return serializer.addAttributeByStream("", "rev", "rev");
		}

		public void _shape(AttributeDataType_Shape value) throws SAXException {
			serializer.addAttribute("", "shape", "shape", (value != null ? value.toString() : ""));
		}

		public Writer _shape() throws SAXException {
			return serializer.addAttributeByStream("", "shape", "shape");
		}

		public void _coords(String value) throws SAXException {
			serializer.addAttribute("", "coords", "coords", (value != null ? value : ""));
		}

		public Writer _coords() throws SAXException {
			return serializer.addAttributeByStream("", "coords", "coords");
		}

		public void _target(String value) throws SAXException {
			serializer.addAttribute("", "target", "target", (value != null ? value : ""));
		}

		public Writer _target() throws SAXException {
			return serializer.addAttributeByStream("", "target", "target");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Ins extends ElementBase {
		protected Ins() throws SAXException {
			super("ins");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _cite(URI value) throws SAXException {
			serializer.addAttribute("", "cite", "cite", (value != null ? value.toString() : ""));
		}

		public Writer _cite() throws SAXException {
			return serializer.addAttributeByStream("", "cite", "cite");
		}

		public void _datetime(Date value) throws SAXException {
			serializer.addAttribute("", "datetime", "datetime", XSDUtils.DATE_TIME_TYPE_FORMAT.format(value));
		}

		public Writer _datetime() throws SAXException {
			return serializer.addAttributeByStream("", "datetime", "datetime");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public P p() throws SAXException {
			return new P();
		}

		public H1 h1() throws SAXException {
			return new H1();
		}

		public H2 h2() throws SAXException {
			return new H2();
		}

		public H3 h3() throws SAXException {
			return new H3();
		}

		public H4 h4() throws SAXException {
			return new H4();
		}

		public H5 h5() throws SAXException {
			return new H5();
		}

		public H6 h6() throws SAXException {
			return new H6();
		}

		public Div div() throws SAXException {
			return new Div();
		}

		public Ul ul() throws SAXException {
			return new Ul();
		}

		public Ol ol() throws SAXException {
			return new Ol();
		}

		public Dl dl() throws SAXException {
			return new Dl();
		}

		public Menu menu() throws SAXException {
			return new Menu();
		}

		public Dir dir() throws SAXException {
			return new Dir();
		}

		public Pre pre() throws SAXException {
			return new Pre();
		}

		public Hr hr() throws SAXException {
			return new Hr();
		}

		public Blockquote blockquote() throws SAXException {
			return new Blockquote();
		}

		public Address address() throws SAXException {
			return new Address();
		}

		public Center center() throws SAXException {
			return new Center();
		}

		public Noframes noframes() throws SAXException {
			return new Noframes();
		}

		public Isindex isindex() throws SAXException {
			return new Isindex();
		}

		public Fieldset fieldset() throws SAXException {
			return new Fieldset();
		}

		public Table table() throws SAXException {
			return new Table();
		}

		public Form form() throws SAXException {
			return new Form();
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Noscript noscript() throws SAXException {
			return new Noscript();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class B extends ElementBase {
		protected B() throws SAXException {
			super("b");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Span extends ElementBase {
		protected Span() throws SAXException {
			super("span");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void content(String text) throws SAXException {
			serializer.characters(text);
		}

		public A a() throws SAXException {
			return new A();
		}

		public Br br() throws SAXException {
			return new Br();
		}

		public Span span() throws SAXException {
			return new Span();
		}

		public Bdo bdo() throws SAXException {
			return new Bdo();
		}

		public Object object() throws SAXException {
			return new Object();
		}

		public Applet applet() throws SAXException {
			return new Applet();
		}

		public Img img() throws SAXException {
			return new Img();
		}

		public Map map() throws SAXException {
			return new Map();
		}

		public Iframe iframe() throws SAXException {
			return new Iframe();
		}

		public Tt tt() throws SAXException {
			return new Tt();
		}

		public I i() throws SAXException {
			return new I();
		}

		public B b() throws SAXException {
			return new B();
		}

		public U u() throws SAXException {
			return new U();
		}

		public S s() throws SAXException {
			return new S();
		}

		public Strike strike() throws SAXException {
			return new Strike();
		}

		public Big big() throws SAXException {
			return new Big();
		}

		public Small small() throws SAXException {
			return new Small();
		}

		public Font font() throws SAXException {
			return new Font();
		}

		public Basefont basefont() throws SAXException {
			return new Basefont();
		}

		public Em em() throws SAXException {
			return new Em();
		}

		public Strong strong() throws SAXException {
			return new Strong();
		}

		public Dfn dfn() throws SAXException {
			return new Dfn();
		}

		public Code code() throws SAXException {
			return new Code();
		}

		public Q q() throws SAXException {
			return new Q();
		}

		public Samp samp() throws SAXException {
			return new Samp();
		}

		public Kbd kbd() throws SAXException {
			return new Kbd();
		}

		public Var var() throws SAXException {
			return new Var();
		}

		public Cite cite() throws SAXException {
			return new Cite();
		}

		public Abbr abbr() throws SAXException {
			return new Abbr();
		}

		public Acronym acronym() throws SAXException {
			return new Acronym();
		}

		public Sub sub() throws SAXException {
			return new Sub();
		}

		public Sup sup() throws SAXException {
			return new Sup();
		}

		public Input input() throws SAXException {
			return new Input();
		}

		public Select select() throws SAXException {
			return new Select();
		}

		public Textarea textarea() throws SAXException {
			return new Textarea();
		}

		public Label label() throws SAXException {
			return new Label();
		}

		public Button button() throws SAXException {
			return new Button();
		}

		public Ins ins() throws SAXException {
			return new Ins();
		}

		public Del del() throws SAXException {
			return new Del();
		}

		public Script script() throws SAXException {
			return new Script();
		}

	}

	public class Ul extends ElementBase {
		protected Ul() throws SAXException {
			super("ul");
		}
		public void _id(String value) throws SAXException {
			serializer.addAttribute("", "id", "id", (value != null ? value : ""));
		}

		public Writer _id() throws SAXException {
			return serializer.addAttributeByStream("", "id", "id");
		}

		public void _class(String... values) throws SAXException {
		StringBuilder sb = new StringBuilder();
		for(String value : values)
			sb.append((value != null ? value : ""));
			serializer.addAttribute("", "class", "class", sb.toString());
		}

		public Writer _class() throws SAXException {
			return serializer.addAttributeByStream("", "class", "class");
		}

		public void _style(String value) throws SAXException {
			serializer.addAttribute("", "style", "style", (value != null ? value : ""));
		}

		public Writer _style() throws SAXException {
			return serializer.addAttributeByStream("", "style", "style");
		}

		public void _title(String value) throws SAXException {
			serializer.addAttribute("", "title", "title", (value != null ? value : ""));
		}

		public Writer _title() throws SAXException {
			return serializer.addAttributeByStream("", "title", "title");
		}

		public void _lang(Locale value) throws SAXException {
			serializer.addAttribute("", "lang", "lang", XSDUtils.toLanguage(value));
		}

		public Writer _lang() throws SAXException {
			return serializer.addAttributeByStream("", "lang", "lang");
		}

		public void _xml_lang(String value) throws SAXException {
			serializer.addAttribute("http://www.w3.org/XML/1998/namespace", "lang", "lang", (value != null ? value : ""));
		}

		public Writer _xml_lang() throws SAXException {
			return serializer.addAttributeByStream("http://www.w3.org/XML/1998/namespace", "lang", "lang");
		}

		public void _dir(String value) throws SAXException {
			serializer.addAttribute("", "dir", "dir", (value != null ? value : ""));
		}

		public Writer _dir() throws SAXException {
			return serializer.addAttributeByStream("", "dir", "dir");
		}

		public void _onclick(String value) throws SAXException {
			serializer.addAttribute("", "onclick", "onclick", (value != null ? value : ""));
		}

		public Writer _onclick() throws SAXException {
			return serializer.addAttributeByStream("", "onclick", "onclick");
		}

		public void _ondblclick(String value) throws SAXException {
			serializer.addAttribute("", "ondblclick", "ondblclick", (value != null ? value : ""));
		}

		public Writer _ondblclick() throws SAXException {
			return serializer.addAttributeByStream("", "ondblclick", "ondblclick");
		}

		public void _onmousedown(String value) throws SAXException {
			serializer.addAttribute("", "onmousedown", "onmousedown", (value != null ? value : ""));
		}

		public Writer _onmousedown() throws SAXException {
			return serializer.addAttributeByStream("", "onmousedown", "onmousedown");
		}

		public void _onmouseup(String value) throws SAXException {
			serializer.addAttribute("", "onmouseup", "onmouseup", (value != null ? value : ""));
		}

		public Writer _onmouseup() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseup", "onmouseup");
		}

		public void _onmouseover(String value) throws SAXException {
			serializer.addAttribute("", "onmouseover", "onmouseover", (value != null ? value : ""));
		}

		public Writer _onmouseover() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseover", "onmouseover");
		}

		public void _onmousemove(String value) throws SAXException {
			serializer.addAttribute("", "onmousemove", "onmousemove", (value != null ? value : ""));
		}

		public Writer _onmousemove() throws SAXException {
			return serializer.addAttributeByStream("", "onmousemove", "onmousemove");
		}

		public void _onmouseout(String value) throws SAXException {
			serializer.addAttribute("", "onmouseout", "onmouseout", (value != null ? value : ""));
		}

		public Writer _onmouseout() throws SAXException {
			return serializer.addAttributeByStream("", "onmouseout", "onmouseout");
		}

		public void _onkeypress(String value) throws SAXException {
			serializer.addAttribute("", "onkeypress", "onkeypress", (value != null ? value : ""));
		}

		public Writer _onkeypress() throws SAXException {
			return serializer.addAttributeByStream("", "onkeypress", "onkeypress");
		}

		public void _onkeydown(String value) throws SAXException {
			serializer.addAttribute("", "onkeydown", "onkeydown", (value != null ? value : ""));
		}

		public Writer _onkeydown() throws SAXException {
			return serializer.addAttributeByStream("", "onkeydown", "onkeydown");
		}

		public void _onkeyup(String value) throws SAXException {
			serializer.addAttribute("", "onkeyup", "onkeyup", (value != null ? value : ""));
		}

		public Writer _onkeyup() throws SAXException {
			return serializer.addAttributeByStream("", "onkeyup", "onkeyup");
		}

		public void _type(AttributeDataType_ULStyle value) throws SAXException {
			serializer.addAttribute("", "type", "type", (value != null ? value.toString() : ""));
		}

		public Writer _type() throws SAXException {
			return serializer.addAttributeByStream("", "type", "type");
		}

		public void _compact(String value) throws SAXException {
			serializer.addAttribute("", "compact", "compact", (value != null ? value : ""));
		}

		public Writer _compact() throws SAXException {
			return serializer.addAttributeByStream("", "compact", "compact");
		}

		public Li li() throws SAXException {
			return new Li();
		}

	}

}
