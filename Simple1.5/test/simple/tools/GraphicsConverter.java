/*
 * GraphicsConverter.java
 *
 * Created on June 22, 2004, 8:55 AM
 */

package simple.tools;

import java.io.File;

/**
 *
 * @author  briank
 */
public class GraphicsConverter extends org.apache.tools.ant.Task {
    
    /**
     * Holds value of property imageFile.
     */
    private File imageFile;
    
    /**
     * Holds value of property outputFile.
     */
    private File outputFile;
    
    /**
     * Holds value of property outputType.
     */
    private String outputType;
    
    /** Creates a new instance of GraphicsConverter */
    public GraphicsConverter() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String image = null;
        String output = null;
        String type = null;
        for(int i = 0; i < args.length; i++) {
            if(args[i].length() > 1 && args[i].charAt(0) == '-') {
                String val = (args.length > i + 1 ? args[i+1] : null);
                switch(args[i].charAt(1)) {
                    case 'i': // input
                        image = val;
                        break;
                    case 'o': //output
                        output = val;
                        break;
                    case 't':
                        type = val;
                        break;                        
                }
            } else if(image == null) {//input
                image = args[i];
            }
        }
        if(type == null) type = "ppm";
        if(image != null && output == null) {
            int p = image.lastIndexOf('.');
            if(p > -1) output = image.substring(0,p+1) + type;
            else output = image + "." + type;
        }
        convertFile(new File(image), new java.io.FileOutputStream(output), type);
    }
    
    public static void convertFile(File imageFile, java.io.OutputStream out, String outputType) throws Exception {
        System.out.println("Loading...");
        java.awt.Image img1 = java.awt.Toolkit.getDefaultToolkit().getImage(imageFile.getAbsolutePath());
        System.out.println("Loaded Image with AWT");
        /*
         net.sourceforge.jiu.data.PixelImage img = net.sourceforge.jiu.gui.awt.ToolkitLoader.loadViaToolkitOrCodecs(imageFile.getAbsolutePath());
        System.out.println("Creating codec...");
        net.sourceforge.jiu.codecs.ImageCodec codec = new net.sourceforge.jiu.codecs.PNMCodec();
        System.out.println("Setting codec info...");
        codec.setOutputStream(out);
        codec.setImage(img);
        System.out.println("Saving...");
        codec.process();
        System.out.println("Closing...");
        codec.close();
        */
    }
    
    /**
     * Getter for property imageFile.
     * @return Value of property imageFile.
     */
    public File getImageFile() {
        return this.imageFile;
    }
    
    /**
     * Setter for property imageFile.
     * @param imageFile New value of property imageFile.
     */
    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }
    
    /**
     * Getter for property outputFile.
     * @return Value of property outputFile.
     */
    public File getOutputFile() {
        return this.outputFile;
    }
    
    /**
     * Setter for property outputFile.
     * @param outputFile New value of property outputFile.
     */
    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }
    
    /**
     * Getter for property outputType.
     * @return Value of property outputType.
     */
    public String getOutputType() {
        return this.outputType;
    }
    
    /**
     * Setter for property outputType.
     * @param outputType New value of property outputType.
     */
    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }
    
    public void execute() throws org.apache.tools.ant.BuildException {
        try {
            convertFile(getImageFile(), new java.io.FileOutputStream(getOutputFile()), getOutputType());
        } catch(Exception e) {
            throw new org.apache.tools.ant.BuildException("Could not convert because:", e);
        }
    }
    
}
