/*
 * Created on Apr 1, 2005
 *
 */
package simple.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MergeTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        //System.out.println("" + ((1000000 + 1000000) >> 1));
        //*
        long[] arr1 = createArray(1000000);
        long[] arr2 = createArray(1000000);
        Arrays.sort(arr1);
        Arrays.sort(arr2); 
        long start = System.currentTimeMillis();
        Long[] ret = intersect(arr1, arr2);
        long ms = System.currentTimeMillis() - start;
        //System.out.println("Array 1 = " + Arrays.toString(arr1));
        //System.out.println("Array 2 = " + Arrays.toString(arr2));
        System.out.println("Interestion = " + ret.length + " entries");//Arrays.toString(ret));
        System.out.println("Time = " + ms + " milleseconds!"); 
        //*/       
    }

    private static Long[] intersect(long[] arr1, long[] arr2) {
        long[] big;
        long[] small;
        if(arr1.length > arr2.length) {
            big = arr1;
            small = arr2;
        } else {
            big = arr2;
            small = arr1;            
        }
        List<Long> list = new ArrayList<Long>();
        int i0 = 0;
        int i1 = 0;
        while(i0 < small.length) {
            i1 = binarySearch(big, small[i0], i1);           
            if(i1 >= 0) { // yes
                list.add(small[i0]);
                i1++;
                i0++;
            } else {
                i1 = -i1 + 1;
                i0++;
            }
            //move i0 to next possible
            if(i1 >= big.length) i0 = small.length;
            else while(i0 < small.length && small[i0] < big[i1]) i0++;
        }
       
        return list.toArray(new Long[list.size()]);
    }

    private static long[] createArray(int size) {
        Random r = new Random();
        long[] array = new long[size];
        for(int i = 0; i < array.length; i++) {
            array[i] = r.nextInt(size * 5); //r.nextLong();
        }
        return array;
    }
    
    private static int binarySearch(long[] a, long key, int low) {
        int high = a.length-1;

        while (low <= high) {
            int mid = (low + high) >> 1;
            long midVal = a[mid];

            if (midVal < key)
            low = mid + 1;
            else if (midVal > key)
            high = mid - 1;
            else
            return mid; // key found
        }
        return -(low + 1);  // key not found.
   }

}
