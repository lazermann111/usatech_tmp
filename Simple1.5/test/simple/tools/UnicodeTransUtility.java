/*
 * GeoLookupUtility.java
 *
 * Created on May 14, 2004, 12:37 PM
 */

package simple.tools;

import java.io.BufferedReader;
import java.io.PrintStream;

/**
 *
 * @author  Brian S. Krug
 */
public class UnicodeTransUtility {
    protected static final String NEWLINE = System.getProperty("line.separator", "\n");
    
    /** Creates a new instance of UnicodeTransUtility */
    public UnicodeTransUtility() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        new UnicodeTransUtility().start();
    }
    
    public void start() throws Exception {
        BufferedReader in = new BufferedReader(new java.io.FileReader("C:/CEI/GeoInfoByZip.txt"));
        PrintStream out = new PrintStream(new java.io.FileOutputStream("C:/CEI/GeoInfoByZip-trans.txt"));
        int cnt = 0;
        for(String line = in.readLine(); line != null; line = in.readLine()) {
            cnt++;
            if(line.trim().length() == 0) {
                //System.out.println("Line " + cnt + " had nothing in it. Skipping it.");
                continue;
            }
            if((int)line.charAt(0) == 0) {
                char[] condensed = new char[line.length()/2];
                for(int i = 0; i < condensed.length; i++) condensed[i] = line.charAt(i*2 + 1);
                line = new String(condensed);
            }
            out.println(line);
        }
        System.out.println("Read " + cnt + " lines");
        in.close();
        out.close();
    }
 
}
