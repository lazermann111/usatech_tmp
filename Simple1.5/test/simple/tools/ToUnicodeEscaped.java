/*
 * Created on May 6, 2005
 *
 */
package simple.tools;

import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;

import simple.io.IOUtils;
import simple.lang.SystemUtils;

public class ToUnicodeEscaped {

    public ToUnicodeEscaped() {
        super();
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        convertToBase64b("C:/TEMP/html-test/Test Chart_files/get_chart.png");
        //new ToUnicodeEscaped().startBase64("C:/TEMP/html-test/Test Chart_files/get_chart.png");
        //compareBinaries("C:/TEMP/hearthand.png-base64b.txt","C:/TEMP/hearthand.png-base64.txt");
    }
    
    public static void convertToBase64b(String fileName) throws Exception {
        Reader in = new BufferedReader(new java.io.FileReader(fileName));
        OutputStream out = new java.io.FileOutputStream(fileName + "-base64b.txt");
        byte[] data = IOUtils.readFully(in).getBytes();
        in.close();
        byte[] result = com.sshtools.j2ssh.util.Base64.encodeBytes(data, false).replace("\n", SystemUtils.getNewLine()).getBytes();       
        out.write(result);
        out.close();
        System.out.println("Read " + data.length + " bytes");
    }
    
    public void startBase64(String fileName) throws Exception {
        Reader in = new BufferedReader(new java.io.FileReader(fileName));
        OutputStream out = new java.io.FileOutputStream(fileName + "-base64.txt");
        byte[] data = IOUtils.readFully(in).getBytes();
        in.close();
        byte[] result = simple.io.Base64.encodeBytes(data, true).getBytes();
        out.write(result);
        out.close();
        System.out.println("Read " + data.length + " bytes");
    }
    
    public void startUnicode(String fileName) throws Exception {
        Reader in = new BufferedReader(new java.io.FileReader(fileName));
        OutputStream out = new java.io.FileOutputStream(fileName + "-unicodeescaped.txt");
        int cnt = 0;
        int ch;
        byte[] prefix = "\\u".getBytes();  
        byte[] zeros = "0000".getBytes();
        while((ch=in.read()) >= 0) {
            out.write(prefix);
            String s = Integer.toHexString(ch);
            out.write(zeros, 0, 4 - s.length());
            out.write(s.getBytes());
            cnt++;
        }
        System.out.println("Read " + cnt + " bytes");
        in.close();
        out.close();
    }
    
    public static void compareBinaries(String fileName1, String fileName2) throws Exception {
        Reader in1 = new BufferedReader(new java.io.FileReader(fileName1));
        Reader in2 = new BufferedReader(new java.io.FileReader(fileName2));
        PrintStream out = new PrintStream(new java.io.FileOutputStream(fileName1 + "-diff.txt"));
        int ch1 = 0;
        int ch2 = 0;
        int cnt = 0;
        while(true) {
            if(ch1 >= 0) ch1 = in1.read();
            if(ch2 >= 0) ch2 = in2.read();
            while(ch1 >= 0 && Character.isWhitespace(ch1)) ch1 = in1.read();
            while(ch2 >= 0 && Character.isWhitespace(ch2)) ch2 = in2.read();
            if(ch1 < 0 && ch2 < 0) break;
            if(ch1 < 0) out.print("  ");
            else out.print(Integer.toHexString(ch1).toUpperCase());
            out.print("\t");
            if(ch2 < 0) out.print("  ");
            else out.print(Integer.toHexString(ch2).toUpperCase());
            out.print("\t");
            if(ch1 != ch2) {
                out.print("*");                
            }
            out.println();
            cnt++;
        }
        System.out.println("Read " + cnt + " bytes");
        in1.close();
        in2.close();
    }
}
