package simple.tools;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class SolarisTimedWaitBugTest {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		final ReentrantLock lock = new ReentrantLock();
		final Condition signal = lock.newCondition();
		Runnable run = new Runnable() {
			public void run() {
				try {
					lock.lockInterruptibly();
					try {
						System.out.println(new Date() + " - Testing for bug by waiting for 1 second. If another message is not printed in 1 second then your system is affected by the bug");
						signal.await(1000, TimeUnit.MILLISECONDS);
						System.out.println(new Date() + " - Congratulations! your system is not affected by the bug.");			
					} finally {
						lock.unlock();
					}
				} catch(InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		Thread[] threads = new Thread[10];
		for(int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(run);
		}
		for(int i = 0; i < threads.length; i++) {
			threads[i].start();
		}
		System.out.println(new Date() + " - Started " +threads.length + " threads");
		for(int i = 0; i < threads.length; i++) {
			threads[i].join();
		}
		System.out.println(new Date() + " - Joined " +threads.length + " threads; Exitting...");		
	}

}
