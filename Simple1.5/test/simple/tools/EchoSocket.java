package simple.tools;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import simple.bean.ConvertUtils;

public class EchoSocket {

	public static void main(String[] args) throws Exception {
		int port = ConvertUtils.getInt(args[0]);
		ServerSocketChannel ssc = ServerSocketChannel.open();
		ssc.socket().bind(new InetSocketAddress(port));
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss.SSS");
		while(true) {
			SocketChannel channel = ssc.accept();
			try {
				while(channel.isOpen()) {
					int r = channel.read(buffer);
					if(r == -1)
						break;
					if(r > 0) {
						System.out.print(dateFormat.format(new Date()));
						System.out.print(" Received: ");
						System.out.println(new String(buffer.array(), 0, r));
						buffer.flip();
						channel.write(buffer);
						System.out.print(dateFormat.format(new Date()));
						System.out.print(" Sent    : ");
						System.out.println(new String(buffer.array(), 0, r));
						buffer.clear();
					}
				}
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}
