/*
 * DataExtractor.java
 *
 * Created on June 6, 2003, 1:04 PM
 */

package simple.tools;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Properties;

import simple.db.BasicDataSourceFactory;
import simple.db.DataSourceFactory;
import simple.io.Log;

/**
 *
 * @author  Brian S. Krug
 */
public class DataExtractor {
    private static Log log = Log.getLog();
    /** Creates a new instance of DataExtractor */
    public DataExtractor() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Properties props = simple.io.App.getProperties("simple/dbpt/PerformanceTester");
            DataSourceFactory dsf = new BasicDataSourceFactory(props);
            for(int i = 0; i < args.length; i++) {
                String ds;
                String tab;
                int p = args[i].indexOf('.');
                if(p > -1) {
                    ds = args[i].substring(0,p);
                    tab = args[i].substring(p+1);
                } else {
                    ds = "";
                    tab = args[i];
                }
                log.info("Starting extraction for table [" + tab + "] in data source '" + ds + "'");
                OutputStream out = new FileOutputStream(tab + "_DATA_" + System.currentTimeMillis() + ".csv");
                Connection conn = dsf.getDataSource(ds).getConnection();
                try {
                    ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM " + tab);
                    simple.text.StringUtils.writeCSV(rs, out);
                    log.info("Successfully extracted data from table [" + tab + "] in data source '" + ds + "'");                
                } finally {
                    out.flush();
                    conn.close();
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
}
