package simple.tools;

import java.io.PrintStream;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import simple.app.Base;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.security.provider.SimpleProvider;

import com.sun.org.apache.bcel.internal.classfile.Constant;
import com.sun.org.apache.bcel.internal.classfile.ConstantClass;
import com.sun.org.apache.bcel.internal.classfile.ConstantPool;
import com.sun.org.apache.bcel.internal.generic.ClassGen;

public class DependencyFinder extends Base {
	protected final static Pattern EXCLUDE_DEPENDENTS = Pattern.compile("(?:java|javax|sun)\\..+");
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineArgument(false, "startClasses", "Classes to find dependencies of");
		registerCommandLineSwitch('i', "individual", true, false, "individual", "Whether to print each classes depenents separately or not");
	}

	@Override
	protected void execute(Map<String, Object> argMap, Properties properties) {
		Class<?>[] startClasses;
		boolean individual;
		try {
			startClasses = ConvertUtils.convertRequired(Class[].class, argMap.get("startClasses"));
			individual = ConvertUtils.getBoolean(argMap.get("individual"), false);
		} catch(ConvertException e) {
			finishAndExit("Could not find classes '" + argMap.get("startClasses") + "'", 1, e);
			return;
		}
		SortedMap<String, SortedSet<String>> classesMap = new TreeMap<String, SortedSet<String>>();
		for(Class<?> c : startClasses)
			findDependents(c, classesMap, individual);
		if(!individual)
			printAllDependents(classesMap);
	}

	protected void printAllDependents(SortedMap<String, SortedSet<String>> classesMap) {
		PrintStream output = System.out;
		for(Map.Entry<String, SortedSet<String>> entry : classesMap.entrySet()) {
			output.println(entry.getKey());
			for(String cn : entry.getValue()) {
				output.print('\t');
				output.println(cn);
			}
		}
	}

	protected void findDependents(Class<?> c, SortedMap<String, SortedSet<String>> classesMap, boolean individual) {
		ClassGen cg;
		try {
			cg = SimpleProvider.getClassGen(c);
		} catch(ClassNotFoundException e) {
			System.out.println("WARNING: could not find class file for " + c.getName() + ". Not able to fully realize dependents");
			return;
		}
		SortedMap<String, SortedSet<String>> localClassesMap = individual ? new TreeMap<String, SortedSet<String>>() : classesMap;
		ConstantPool cp = cg.getConstantPool().getFinalConstantPool();
		OUTER: for(int i = 0; i < cp.getLength(); i++) {
			Constant constant = cp.getConstant(i);
			if(constant instanceof ConstantClass) {
				String classname = ((ConstantClass) constant).getBytes(cp).replace('/', '.');
				int p;
				if(classname.startsWith("[")) {
					for(p = 1; p < classname.length(); p++)
						if(classname.charAt(p) != '[')
							break;
					switch(classname.charAt(p)) {
						case 'L':
							classname = classname.substring(p + 1, classname.length() - 1);
							break;
						case 'Z':
							classname = "boolean";
							continue OUTER;
						case 'B':
							classname = "byte";
							continue OUTER;
						case 'C':
							classname = "char";
							continue OUTER;
						case 'D':
							classname = "double";
							continue OUTER;
						case 'F':
							classname = "float";
							continue OUTER;
						case 'I':
							classname = "int";
							continue OUTER;
						case 'J':
							classname = "long";
							continue OUTER;
						case 'S':
							classname = "short";
							continue OUTER;
					}
				}
				p = classname.lastIndexOf('.');
				String pkg, cls;
				if(p >= 0) {
					pkg = classname.substring(0, p);
					cls = classname.substring(p + 1);
				} else {
					pkg = "";
					cls = classname;
				}
				SortedSet<String> classesSet = localClassesMap.get(pkg);
				if(classesSet == null) {
					classesSet = new TreeSet<String>();
					localClassesMap.put(pkg, classesSet);
				}
				if(classesSet.add(cls) && !EXCLUDE_DEPENDENTS.matcher(classname).matches() && !individual)
					try {
						findDependents(Class.forName(classname), classesMap, individual);
					} catch(ClassNotFoundException e) {
						System.out.println("WARNING: could not find class " + classname + ". Not able to fully realize dependents");
					} catch(NoClassDefFoundError e) {
						System.out.println("WARNING: could not find class " + classname + ". Not able to fully realize dependents");
					}
			}
		}
		if(individual) {
			PrintStream output = System.out;
			output.print("Dependents of ");
			output.print(c.getName());
			output.println(':');
			printAllDependents(localClassesMap);
			output.println("-------------------------------");
			for(Map.Entry<String, SortedSet<String>> entry : localClassesMap.entrySet()) {
				SortedSet<String> classesSet = classesMap.get(entry.getKey());
				if(classesSet == null) {
					classesSet = new TreeSet<String>();
					classesMap.put(entry.getKey(), classesSet);
				}
				for(String cls : entry.getValue()) {
					String classname = entry.getKey() == null || entry.getKey().length() == 0 ? cls : entry.getKey() + '.' + cls;
					if(classesSet.add(cls) && !EXCLUDE_DEPENDENTS.matcher(classname).matches() && !classname.equals(c.getName()))
						try {
							findDependents(Class.forName(classname), classesMap, individual);
						} catch(ClassNotFoundException e) {
							System.out.println("WARNING: could not find class " + classname + ". Not able to fully realize dependents");
						} catch(NoClassDefFoundError e) {
							System.out.println("WARNING: could not find class " + classname + ". Not able to fully realize dependents");
						}
				}
			}
		}
	}

	@Override
	protected boolean hasPropertiesFile() {
		return false;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new DependencyFinder().run(args);
	}

}
