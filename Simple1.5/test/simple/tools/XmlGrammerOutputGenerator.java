package simple.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.util.Date;
import java.util.Locale;

import org.xml.sax.SAXException;

import simple.text.StringUtils;
import simple.xml.Serializer;
import simple.xml.XSDUtils;

import com.sun.org.apache.xerces.internal.impl.xs.XMLSchemaLoader;
import com.sun.org.apache.xerces.internal.xs.StringList;
import com.sun.org.apache.xerces.internal.xs.XSAttributeUse;
import com.sun.org.apache.xerces.internal.xs.XSComplexTypeDefinition;
import com.sun.org.apache.xerces.internal.xs.XSConstants;
import com.sun.org.apache.xerces.internal.xs.XSElementDeclaration;
import com.sun.org.apache.xerces.internal.xs.XSModel;
import com.sun.org.apache.xerces.internal.xs.XSModelGroup;
import com.sun.org.apache.xerces.internal.xs.XSNamedMap;
import com.sun.org.apache.xerces.internal.xs.XSObject;
import com.sun.org.apache.xerces.internal.xs.XSObjectList;
import com.sun.org.apache.xerces.internal.xs.XSParticle;
import com.sun.org.apache.xerces.internal.xs.XSSimpleTypeDefinition;
import com.sun.org.apache.xerces.internal.xs.XSTerm;
import com.sun.org.apache.xerces.internal.xs.XSTypeDefinition;

public class XmlGrammerOutputGenerator {

	/**
	 * @param args
	 * @throws SAXException
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws SAXException, IOException {
		File xsdFile = new File(args[0]);
		File srcDir = new File(args[1]);
		new XmlGrammerOutputGenerator().generateJavaFile(xsdFile, srcDir, false);
	}

	protected void generateJavaFile(File xsdFile, File srcDir, boolean topLevel) throws IOException {
		String cls;
		XSModel model = new XMLSchemaLoader().loadURI(xsdFile.toURI().toString());
		StringList nms = model.getNamespaces();
		if(nms.getLength() > 0) {
			cls = namespaceToClass(nms.item(0));
		} else {
			cls = "Output";
		}
		String pkg = "simple.xml.output";

		File javaFile = new File(srcDir, pkg.replace('.', '/') + (pkg.length() > 0 ? '/' : "") + cls + ".java");
		javaFile.getParentFile().mkdirs();
		PrintWriter pw = new PrintWriter(javaFile);
		pw.print("package ");
		pw.print(pkg);
		pw.print(";");
		pw.println();
		pw.println();
		Class<?>[] imports = new Class<?>[] {
			Writer.class,
			BigInteger.class,
			URI.class,
			Date.class,
			Locale.class,
			SAXException.class,
			Serializer.class,
			XSDUtils.class
		};
		for(Class<?> i : imports)
			pw.append("import ").append(i.getName()).append(";\n");
		pw.println();

		pw.print("public class ");
		pw.print(cls);
		pw.print(" {");
		pw.println();
		pw.println("\tprotected final Serializer serializer;");
		pw.println();

		pw.println("\tpublic abstract class ElementBase {");
		pw.println("\t\tprotected final String name;");
		pw.println("\t\tprotected ElementBase(String name) throws SAXException {");
		pw.println("\t\t\tthis.name = name;");
		pw.println("\t\t\tserializer.startElement(name);");
		pw.println("\t\t}");
		pw.println();
		pw.println("\t\tpublic void end() throws SAXException {");
		pw.println("\t\t\tserializer.endElement(name);");
		pw.println("\t\t}");
		pw.println("\t}");
		pw.println();
		pw.print("\tpublic ");
		pw.print(cls);
		pw.print('(');
		pw.println("Serializer serializer) {");
		pw.println("\t\tthis.serializer = serializer;");
		pw.println("\t}");

		XSNamedMap allTypes = model.getComponents(XSConstants.TYPE_DEFINITION);
		for(int i = 0; i < allTypes.getLength(); i++) {
			XSObject o = allTypes.item(i);
			if(o instanceof XSSimpleTypeDefinition) {
				processSimpleType((XSSimpleTypeDefinition) o, pw);
			}
		}
		XSNamedMap allElements = model.getComponents(XSConstants.ELEMENT_DECLARATION);
		for(int i = 0; i < allElements.getLength(); i++) {
			XSObject o = allElements.item(i);
			if(o instanceof XSElementDeclaration) {
				processElementDecl((XSElementDeclaration) o, topLevel, pw);
			}
		}
		pw.print("}");
		pw.println();
		pw.flush();

	}

	protected void processSimpleType(XSSimpleTypeDefinition simpleType, Appendable appendTo) throws IOException {
		StringList options = simpleType.getLexicalEnumeration();
		if(options != null && !StringUtils.isBlank(simpleType.getName()) && options.getLength() > 0 && options.getLength() < 100) {
			// create enum
			appendTo.append("\tpublic static enum AttributeDataType_").append(simpleType.getName()).append(" {\n\t\t");
			for(int i = 0; i < options.getLength(); i++) {
				String op = options.item(i);
				appendTo.append(StringUtils.toJavaName(op).toUpperCase()).append("(\"").append(op.replace("\"", "\"\"")).append("\"),\n\t\t");
			}
			appendTo.append(";\n\n\t\tprivate final String value;\n\n\t\tprivate AttributeDataType_").append(simpleType.getName()).append("(String value) {\n\t\t\tthis.value = value;\n\t\t}\n\n\t\tpublic String toString() {\n\t\t\treturn value;\n\t\t}\n\t}\n\n");
		}
	}

	protected void processElementDecl(XSElementDeclaration elem, boolean topLevel, Appendable appendTo) throws IOException {
		String elemJavaName = StringUtils.toJavaName(elem.getName());
		String elemClassName = StringUtils.capitalizeFirst(elemJavaName);
		appendTo.append("\tpublic class ");
		appendTo.append(elemClassName);
		appendTo.append(" extends ElementBase {\n\t\tprotected ");
		appendTo.append(elemClassName);
		appendTo.append("() throws SAXException {\n\t\t\tsuper(\"");
		appendTo.append(elem.getName().replace("\"", "\\\""));
		appendTo.append("\");\n\t\t}\n");

		XSTypeDefinition typeDef = elem.getTypeDefinition();
		if(typeDef instanceof XSComplexTypeDefinition) {
			XSComplexTypeDefinition ctd = (XSComplexTypeDefinition) typeDef;
			XSObjectList attList = ctd.getAttributeUses();
			for(int i = 0; i < attList.getLength(); i++) {
				XSAttributeUse au = (XSAttributeUse) attList.item(i);
				String attName = au.getAttrDeclaration().getName();
				String attNamespace = au.getAttrDeclaration().getNamespace();
				String attJavaName = StringUtils.toJavaName(attName);
				XSSimpleTypeDefinition attrType = au.getAttrDeclaration().getTypeDefinition();
				StringBuilder typeDeclaration = new StringBuilder();
				StringBuilder typeConversion = new StringBuilder();
				StringBuilder typePreparation = new StringBuilder();
				getJavaType(attrType, typeDeclaration, typePreparation, typeConversion);
				appendTo.append("\t\tpublic void _");
				if(!StringUtils.isBlank(attNamespace)) {
					appendTo.append(getPrefix(attNamespace));
					appendTo.append("_");
				}
				appendTo.append(attJavaName);
				appendTo.append('(');
				appendTo.append(typeDeclaration.toString());
				appendTo.append(") throws SAXException {\n");
				if(typePreparation.length() > 0)
					appendTo.append(typePreparation.toString()).append("\n");
				appendTo.append("\t\t\tserializer.addAttribute(\"");
				if(attNamespace != null)
					appendTo.append(attNamespace.replace("\"", "\\\""));
				appendTo.append("\", \"");
				appendTo.append(attName.replace("\"", "\\\""));
				appendTo.append("\", \"");
				appendTo.append(attName.replace("\"", "\\\""));
				appendTo.append("\", ");
				appendTo.append(typeConversion);
				appendTo.append(");\n\t\t}\n\n\t\tpublic Writer _");
				if(!StringUtils.isBlank(attNamespace)) {
					appendTo.append(getPrefix(attNamespace));
					appendTo.append("_");
				}
				appendTo.append(attJavaName);
				appendTo.append("() throws SAXException {\n");
				appendTo.append("\t\t\treturn serializer.addAttributeByStream(\"");
				if(attNamespace != null)
					appendTo.append(attNamespace.replace("\"", "\\\""));
				appendTo.append("\", \"");
				appendTo.append(attName.replace("\"", "\\\""));
				appendTo.append("\", \"");
				appendTo.append(attName.replace("\"", "\\\""));
				appendTo.append("\");\n\t\t}\n\n");
			}
			switch(ctd.getContentType()) {
				case XSComplexTypeDefinition.CONTENTTYPE_MIXED:
				case XSComplexTypeDefinition.CONTENTTYPE_SIMPLE:
					appendTo.append("\t\tpublic void content(String text) throws SAXException {\n");
					appendTo.append("\t\t\tserializer.characters(text);\n");
					appendTo.append("\t\t}\n\n");
					break;
			}
			XSParticle part = ctd.getParticle();
			if(part != null) {
				processTerm(part.getTerm(), appendTo);
			}
		} else if(typeDef instanceof XSSimpleTypeDefinition) {

		}

		appendTo.append("\t}\n\n");
		if(topLevel)
			processTerm(elem, appendTo);
	}

	protected void processTerm(XSTerm term, Appendable appendTo) throws IOException {
		if(term instanceof XSModelGroup) {
			XSObjectList subParts = ((XSModelGroup) term).getParticles();
			for(int i = 0; i < subParts.getLength(); i++) {
				XSParticle part = (XSParticle) subParts.item(i);
				processTerm(part.getTerm(), appendTo);
			}
		} else if(term instanceof XSElementDeclaration) {
			XSElementDeclaration subElem = (XSElementDeclaration) term;
			String subJavaName = StringUtils.toJavaName(subElem.getName());
			String subClassName = StringUtils.capitalizeFirst(subJavaName);
			appendTo.append("\t\tpublic ");
			appendTo.append(subClassName);
			appendTo.append(" ");
			appendTo.append(subJavaName);
			appendTo.append("() throws SAXException {\n\t\t\treturn new ");
			appendTo.append(subClassName);
			appendTo.append("();\n\t\t}\n\n");
		}
	}
	protected String getPrefix(String namespace) {
		if("http://www.w3.org/XML/1998/namespace".equals(namespace))
			return "xml";
		if("http://www.w3.org/2000/xmlns/".equals(namespace))
			return "xmlns";
		return StringUtils.substringAfterLast(namespace, "/");
	}

	protected String namespaceToClass(String namespace) {
		return StringUtils.capitalizeFirst(StringUtils.substringAfterLast(namespace, "/")) + "Output";
	}

	protected Class<?> getJavaType(XSSimpleTypeDefinition simpleType, StringBuilder declaration, StringBuilder preparation, StringBuilder conversion) {
		switch(simpleType.getVariety()) {
			case XSSimpleTypeDefinition.VARIETY_ATOMIC:
				switch(simpleType.getBuiltInKind()) {
					case XSConstants.BYTE_DT:
						declaration.append("byte value");
						conversion.append("String.valueOf(value)");
						return byte.class;
					case XSConstants.UNSIGNEDSHORT_DT:
					case XSConstants.INT_DT:
						declaration.append("int value");
						conversion.append("String.valueOf(value)");
						return int.class;
					case XSConstants.POSITIVEINTEGER_DT:
					case XSConstants.NEGATIVEINTEGER_DT:
					case XSConstants.NONNEGATIVEINTEGER_DT:
					case XSConstants.NONPOSITIVEINTEGER_DT:
					case XSConstants.INTEGER_DT: // 0 fraction digits, infinite bounds
					case XSConstants.UNSIGNEDLONG_DT:
						declaration.append("BigInteger value");
						conversion.append("(value != null ? value.toString() : \"\")");
						return BigInteger.class;
					case XSConstants.LANGUAGE_DT:
						declaration.append("Locale value");
						conversion.append("XSDUtils.toLanguage(value)");
						return Locale.class;
					case XSConstants.UNSIGNEDINT_DT:
					case XSConstants.LONG_DT:
						declaration.append("long value");
						conversion.append("String.valueOf(value)");
						return long.class;
					case XSConstants.SHORT_DT:
					case XSConstants.UNSIGNEDBYTE_DT:
						declaration.append("short value");
						conversion.append("String.valueOf(value)");
						return short.class;
					case XSConstants.ANYURI_DT:
						declaration.append("URI value");
						conversion.append("(value != null ? value.toString() : \"\")");
						return URI.class;
					case XSConstants.BASE64BINARY_DT:
						declaration.append("byte[] value");
						conversion.append("(value != null ? Base64.encodeBytes(value, true) : \"\")");
						return byte[].class;
					case XSConstants.BOOLEAN_DT:
						declaration.append("boolean value");
						conversion.append("String.valueOf(value)");
						return Boolean.class;
					case XSConstants.DATE_DT:
						declaration.append("Date value");
						conversion.append("XSDUtils.DATE_TYPE_FORMAT.format(value)");
						return Date.class;
					case XSConstants.DATETIME_DT:
						declaration.append("Date value");
						conversion.append("XSDUtils.DATE_TIME_TYPE_FORMAT.format(value)");
						return Date.class;
					case XSConstants.TIME_DT:
						declaration.append("Date value");
						conversion.append("XSDUtils.TIME_TYPE_FORMAT.format(value)");
						return Date.class;
					case XSConstants.DECIMAL_DT:
						declaration.append("BigDecimal value");
						conversion.append("(value != null ? value.toString() : \"\")");
						return BigDecimal.class;
					case XSConstants.DOUBLE_DT:
						declaration.append("double value");
						conversion.append("String.valueOf(value)");
						return double.class;
					case XSConstants.DURATION_DT: // TODO implement this format : example: -P15MT8S
						declaration.append("String value");
						conversion.append("(value != null ? value : \"\")");
						return String.class;
					case XSConstants.FLOAT_DT:
						declaration.append("float value");
						conversion.append("String.valueOf(value)");
						return float.class;
					case XSConstants.GDAY_DT:
					case XSConstants.GMONTH_DT:
					case XSConstants.GMONTHDAY_DT:
					case XSConstants.GYEAR_DT:
					case XSConstants.GYEARMONTH_DT:
						// NOTE: These are ill-frequently used so just use String
						declaration.append("String value");
						conversion.append("(value != null ? value : \"\")");
						return String.class;
					case XSConstants.HEXBINARY_DT:
						declaration.append("byte[] value");
						conversion.append("(value != null ? StringUtils.toHex(value) : \"\")");
						return byte[].class;
					case XSConstants.NOTATION_DT:
					case XSConstants.ANYSIMPLETYPE_DT:
					case XSConstants.ENTITY_DT:
					case XSConstants.ID_DT:
					case XSConstants.IDREF_DT:
					case XSConstants.NAME_DT:
					case XSConstants.NCNAME_DT:
					case XSConstants.NMTOKEN_DT:
					case XSConstants.NORMALIZEDSTRING_DT:
					case XSConstants.TOKEN_DT:
					case XSConstants.UNAVAILABLE_DT:
					case XSConstants.QNAME_DT:
					case XSConstants.STRING_DT:
					default:
						StringList options = simpleType.getLexicalEnumeration();
						if(options != null && !StringUtils.isBlank(simpleType.getName()) && options.getLength() > 0 && options.getLength() < 100) {
							declaration.append("AttributeDataType_").append(simpleType.getName()).append(" value");
							conversion.append("(value != null ? value.toString() : \"\")");
						} else {
							declaration.append("String value");
							conversion.append("(value != null ? value : \"\")");
						}
						return String.class;
				}
			case XSSimpleTypeDefinition.VARIETY_LIST:
				StringBuilder componentConversion = new StringBuilder();
				// TODO we may need to handle nested lists
				Class<?> componentType = getJavaType(simpleType.getItemType(), declaration, preparation, componentConversion);
				int pos = declaration.indexOf(" ");
				declaration.insert(pos, "...");
				declaration.append('s');
				preparation.append("\t\tStringBuilder sb = new StringBuilder();\n\t\tfor(").append(declaration, 0, pos).append(" value : values)\n\t\t\tsb.append(").append(componentConversion).append(");");
				conversion.append("sb.toString()");
				return componentType;
			default:
				declaration.append("String value");
				conversion.append("(value != null ? value : \"\")");
				return String.class;
		}
	}
}
