/*
 * Created on Mar 17, 2005
 *
 */
package simple.tools;
/*
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import com.sun.javadoc.*;
*/
/*
import com.sun.tools.doclets.Configuration;
import com.sun.tools.doclets.DirectoryManager;
import com.sun.tools.doclets.DocletAbortException;
*/
/**
 * @author bkrug
 *  
 */
public class ConvertTo1_5Doclet {
	/*
    public static class ConvertTo1_5Configuration extends Configuration {
       public void setSpecificDocletOptions(RootDoc arg0) {
       }
       public int optionLength(String arg0) {
            return super.generalOptionLength(arg0);
       }        
    }

    public ConvertTo1_5Configuration configuration;
    
    public ConvertTo1_5Doclet() {
        configuration = configuration();
    }

    protected void startGeneration(RootDoc rootdoc) {
        if (rootdoc.classes().length == 0) {
            //configuration.standardmessage.error("doclet.No_Public_Classes_To_Document");
            return;
        }
        configuration.setOptions(rootdoc);
        File destDir = new File(configuration.destdirname);
        for(int i = 0; i < rootdoc.classes().length; i++) {
            ClassDoc cd = rootdoc.classes()[i];
            File classFile = new File(destDir, DirectoryManager.getDirectoryPath(cd.containingPackage()) + cd.name() + ".java");
            //now search for items needing conversion, then use the SourcePosition of each to write the file
            Map changes = new TreeMap(new Comparator() {
                public int compare(Object o1, Object o2) {
                    SourcePosition sp1 = (SourcePosition)o1;
                    SourcePosition sp2 = (SourcePosition)o2;
                    if(sp1.line() != sp2.line()) return sp1.line() - sp2.line();
                    if(sp1.column() != sp2.column()) return sp1.column() - sp2.column();
                    return 0;
                }               
            });
            //1. look for possible enums (public static final int )
            for(int k = 0; k < cd.fields().length; k++) {
                FieldDoc fd = cd.fields()[k];
                
                if(fd.isFinal() && fd.isStatic() && fd.isPublic() && fd.type().qualifiedTypeName().equals("int")) {
                    
                }
            }
            //2. Look for synchronizations
            
            //3. Look for for loops
            
            //4. Look for generics
            
            // print file
            
        }
    }

    public static boolean start(RootDoc rootdoc) throws IOException {
        try {
            (new ConvertTo1_5Doclet()).startGeneration(rootdoc);
        } catch (DocletAbortException docletabortexception) {
            return false;
        }
        return true;
    }

    protected static ConvertTo1_5Configuration configuration() {
        return new ConvertTo1_5Configuration();
    }
    public static int optionLength(String s) {
        return configuration().optionLength(s);
    }

    public static boolean validOptions(String as[][], DocErrorReporter docerrorreporter) throws IOException {
        return configuration().generalValidOptions(as, docerrorreporter);
    }
*/
}