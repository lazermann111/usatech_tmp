/**
 *
 */
package simple.tools;

import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;

import simple.io.Base64;

/**
 * Adopted from http://mark.foster.cc/pub/java/ExportPriv.java
 *
 */
public class ExportPrivateKey {
	public static void main(String args[]) throws Exception {
		String keystore;
		String alias;
		char[] password;
		switch(args.length) {
			case 3:
				keystore = args[0];
				alias = args[1];
				password = args[2].toCharArray();
				break;
			case 2:
				keystore = args[0];
				alias = args[1];
				Console console = System.console();
				if(console == null) {
					System.err.println("Console is not available, password must be given on command line.");
					System.err.println("Usage: java ExportPriv <keystore> <alias> <password>");
					System.exit(1);
					return;
				}
				password = console.readPassword("Keystore Password:");
				break;
			default:
				System.err.println("Usage: java ExportPriv <keystore> <alias> <password>");
				System.exit(1);
				return;
		}
		ExportPrivateKey myep = new ExportPrivateKey();
		myep.doit(keystore, alias, password);
	}

	public void doit(String fileName, String aliasName, char[] passPhrase) throws Exception {
		KeyStore ks = KeyStore.getInstance("JKS");
		File certificateFile = new File(fileName);
		ks.load(new FileInputStream(certificateFile), passPhrase);
		KeyPair kp = getPrivateKey(ks, aliasName, passPhrase);
		PrivateKey privKey = kp.getPrivate();
		String b64 = Base64.encodeBytes(privKey.getEncoded(), true);
		System.out.println("-----BEGIN PRIVATE KEY-----");
		System.out.println(b64);
		System.out.println("-----END PRIVATE KEY-----");
	}

	// From http://javaalmanac.com/egs/java.security/GetKeyFromKs.html
	public KeyPair getPrivateKey(KeyStore keystore, String alias, char[] password) {
		try {
			// Get private key
			Key key = keystore.getKey(alias, password);
			if(key instanceof PrivateKey) {
				// Get certificate of public key
				Certificate cert = keystore.getCertificate(alias);
				// Get public key
				PublicKey publicKey = cert.getPublicKey();
				// Return a key pair
				return new KeyPair(publicKey, (PrivateKey) key);
			}
		} catch(UnrecoverableKeyException e) {
		} catch(NoSuchAlgorithmException e) {
		} catch(KeyStoreException e) {
		}
		return null;
	}
}
