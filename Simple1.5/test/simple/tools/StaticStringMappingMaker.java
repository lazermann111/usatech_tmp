package simple.tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import simple.io.Log;
import simple.lang.SystemUtils;

public class StaticStringMappingMaker {
	static {System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");}
    public static class ClassName {
    	public ClassName(String packageName, String simpleName) {
    		this.packageName = packageName;
    		this.simpleName = simpleName;
    	}
    	public final String packageName;
    	public final String simpleName;
    }
	private static final Log log = Log.getLog();
	protected static String NEWLINE = SystemUtils.getNewLine();
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String[] keys = new String[] {
			"get", "four", "seven", "tough", "at", "I", "to","elephant", "an", "as","ti","te","ty","seats", "separ"	
		};
		Map<String,String> map = new HashMap<String, String>();
		for(String key : keys) {
			map.put(key, String.valueOf(key.hashCode()));
		}
		makeMapping(map, System.out, "test.FirstMapping", "Integer");
	}

	public static void makeMapping(Map<String, String> map, File srcDir, String mappingClassName, String valueClassName) throws IOException {
		File javaFile = new File(srcDir, mappingClassName.replace('.', '/') + ".java");
		if(javaFile.getParentFile().mkdirs())
			log.debug("Created directory " + javaFile.getParentFile().getAbsolutePath());
		else
			log.debug("Directory " + javaFile.getParentFile().getAbsolutePath() + " already exists");
		Writer writer = new FileWriter(javaFile);
		makeMapping(map, writer, mappingClassName, valueClassName);
		writer.flush();
	}
	
	protected static void makeMapping(Map<String, String> map, Appendable writer, String mappingClassName, String valueClassName) throws IOException {
		String[] keys = map.keySet().toArray(new String[map.size()]);
		Arrays.sort(keys, new Comparator<String>() {
			public int compare(String s1, String s2) {
				int i = s1.length() - s2.length();
				if(i != 0) return i;
				return s1.compareTo(s2);
			}			
		});
		ClassName mappingClass = splitPackage(mappingClassName);
		ClassName valueClass = splitPackage(valueClassName);
		writer.append("package ").append(mappingClass.packageName).append(';').append(NEWLINE);
		writer.append(NEWLINE);
		writer.append("import simple.util.Mapping;").append(NEWLINE);
		if(valueClass.packageName != null && !valueClass.packageName.equals("java.lang"))
			writer.append("import ").append(valueClassName).append(';').append(NEWLINE);
		writer.append(NEWLINE);
		writer.append("public class ").append(mappingClass.simpleName).append(" implements Mapping<String,").append(valueClass.simpleName).append("> {").append(NEWLINE);
		writeIndent(writer, 1);
		writer.append("public ").append(valueClass.simpleName).append(" get(String key) {").append(NEWLINE);
		writeIndent(writer, 2);
		writer.append("switch(key.length()) {").append(NEWLINE);
		//break down by length
		int length = 0;
		for(int i = 0; i < keys.length; ) {
			int start = i;
			length = keys[i].length();
			i++;
			while(i < keys.length && keys[i].length() == length) i++;
			writeIndent(writer, 3);
			writer.append("case ").append(String.valueOf(length)).append(':').append(NEWLINE);
			writeCharSwitches(writer, keys, start, i, 0, map);
			writeIndent(writer, 4);
			writer.append("break;").append(NEWLINE);
		}
		
		writeIndent(writer, 2);
		writer.append("}").append(NEWLINE);//end of switch
		writeIndent(writer, 2);
		writer.append("return null;").append(NEWLINE);
		writeIndent(writer, 1);
		writer.append("}").append(NEWLINE);//end of method
		writer.append("}").append(NEWLINE);// end of class
	}
	
	protected static void writeCharSwitches(Appendable writer, String[] keys, int start, int end, int k, Map<String,String> values) throws IOException {
		writeIndent(writer, 4 + (k*2));
		writer.append("switch(key.charAt(").append(String.valueOf(k)).append(")) {").append(NEWLINE);
		for(int i = start; i < end; ) {
			start = i;
			char ch = keys[i].charAt(k);
			i++;
			while(i < keys.length && keys[i].charAt(k) == ch) i++;
			writeIndent(writer, 5 + (k*2));
			writer.append("case '").append(ch).append("':");
			if(k + 1 == keys[start].length()) {
				//return value
				assert start == i - 1;
				writer.append(" return ").append(values.get(keys[start])).append(";").append(NEWLINE);
			} else {
				writer.append(NEWLINE);
				writeCharSwitches(writer, keys, start, i, k+1, values);
				writeIndent(writer, 6 + (k*2));
				writer.append("break;").append(NEWLINE);
			}
		}
		writeIndent(writer, 4 + (k*2));
		writer.append('}').append(NEWLINE);
	}

	protected static ClassName splitPackage(String className) {
		int pos = className.lastIndexOf('.');
		if(pos < 0) {
			return new ClassName(null, className);
		} else {
			return new ClassName(className.substring(0, pos),className.substring(pos+1));
		}
	}
	
	protected static void writeIndent(Appendable writer, int count) throws IOException {
		for(int p = 0; p < count; p++)
			writer.append("    ");		
	}
}
