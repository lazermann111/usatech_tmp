/*
 * ScriptDatabase.java
 *
 * Created on January 26, 2004, 11:26 AM
 */

package simple.tools;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import simple.db.BasicDataSourceFactory;
import simple.io.Log;
import simple.text.MessageFormat;

/**
 *
 * @author  Brian S. Krug
 */
public class ScriptLoadDataWarehouse {
    private static Log log = Log.getLog();
    protected static final String NEWLINE = "\r\n";
    protected static Map sections = new java.util.HashMap();
    
    /**
     * Holds value of property directory.
     */
    private File directory;
    
    /**
     * Holds value of property connection.
     */
    private Connection connection;
    
    /** Creates a new instance of ScriptDatabase */
    public ScriptLoadDataWarehouse() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        log.debug("Start");
        Properties props = loadProps(ScriptLoadDataWarehouse.class.getClassLoader().getResourceAsStream("simple/tools/Script.properties"));
        BasicDataSourceFactory bdsf = new BasicDataSourceFactory(props);
        File directory = new File(props.getProperty("OutputDirectory", ""));
        for(Iterator iter = props.entrySet().iterator(); iter.hasNext(); ) {
            Map.Entry e = (Map.Entry) iter.next();
            if(e.getKey().toString().startsWith("Section.")) {
                sections.put(e.getKey().toString().substring(8), new MessageFormat(e.getValue().toString()));
            }
        }
        Connection conn = bdsf.getDataSource("").getConnection();
        try {
            ScriptLoadDataWarehouse sldw = new ScriptLoadDataWarehouse();
            sldw.setConnection(conn);
            sldw.setDirectory(directory);
            sldw.scriptLoadTables(props.getProperty("Catalog"), props.getProperty("Schema"));
        } finally {
            conn.close();
            conn = null;
        }
        log.debug("Finished!");
        log.finish();
    }
    
    public void scriptLoadTables(String catalogName, String schemaName) throws SQLException, IOException {
        DatabaseMetaData md = getConnection().getMetaData();
        Map[] tabs = retrieveTables(md, catalogName, schemaName);
        for(int i = 0; i < tabs.length; i++) {
            OutputStream out = null;
            try {
                String table = (String)tabs[i].get("TABLE_NAME");
                //if(table.startsWith("Date") || table.startsWith("Time"))
                if(table.endsWith("Fact") || table.endsWith("Dim") || table.endsWith("Bridge")) {
                    log.debug("Processing " + table + "...");
                    out = new java.io.FileOutputStream(new File(directory, "p_Load" + table + ".sql"));
                    scriptLoadTable(out, tabs[i]);
                }
            } finally {
                if(out != null) {
                    out.flush();
                    out.close();
                }
            }           
        }        
    }
    
    public void scriptLoadTable(OutputStream out, Map tableProps) throws SQLException, IOException  {
        DatabaseMetaData md = getConnection().getMetaData();
        String tableName = (String)tableProps.get("TABLE_NAME");
        String schemaName = (String)tableProps.get("TABLE_SCHEM");
        String catalogName = (String)tableProps.get("TABLE_CAT");
        out.write(formatSection("header", tableProps).getBytes());
        Map[] cols = retrieveColumns(md, tableName, catalogName, schemaName);
        boolean reseed = false;
        for(int i = 0; i < cols.length; i++) {
            if(isAutoIncrement(cols[i])) {
                reseed = true;
            }
        }
        if(reseed) { //if identity then reseed
            out.write(formatSection("reseed", tableProps).getBytes());
        }
        ValueFactory vf = null;
        if(tableName.toUpperCase().endsWith("Fact".toUpperCase())) {
            
        } else if(tableName.toUpperCase().endsWith("LiteralDim".toUpperCase()) || tableName.toUpperCase().endsWith("LiteralBridge".toUpperCase()) || tableName.equalsIgnoreCase("ConditionDim") || tableName.equalsIgnoreCase("AcctDim")) {
            if(reseed) scriptInsert(out, "unknown", tableProps, cols, true, new UnknownValueFactory()); //if reseed and dim then insert dummy record
            vf = new LiteralValueFactory();
        } else if(tableName.toUpperCase().endsWith("XLateDim".toUpperCase()) || tableName.toUpperCase().endsWith("XLateBridge".toUpperCase())) {
            if(reseed) scriptInsert(out, "insert", tableProps, cols, false, new UnknownXLateValueFactory()); //if reseed and dim then insert dummy record
            vf = new XlateValueFactory();
        } else if(tableName.toUpperCase().endsWith("Dim".toUpperCase())) {
            if(reseed) scriptInsert(out, "unknown", tableProps, cols, true, new UnknownValueFactory()); //if reseed and dim then insert dummy record
            
        } else if(tableName.toUpperCase().endsWith("Bridge".toUpperCase())) {
            
        } 
        
        scriptInsert(out, "insert", tableProps, cols, false, vf);
        out.write(formatSection("footer", tableProps).getBytes());        
    }
    
    public void scriptInsert(OutputStream out, String section, Map tableProps, Map[] cols, boolean useAutoIncColumn, ValueFactory vf) throws SQLException, IOException {
        out.write(formatSection(section + ".header", tableProps).getBytes());
        boolean first = true;
        for(int i = 0; i < cols.length; i++) {
            if(isAutoIncrement(cols[i]) && !useAutoIncColumn) continue;
            if(!first) out.write(("," + NEWLINE).getBytes());
            else first = false;
            out.write(formatSection(section + ".columns", cols[i]).getBytes());
        }
        out.write(formatSection(section + ".middle", tableProps).getBytes());
        first = true;
        for(int i = 0; i < cols.length; i++) {
            if(isAutoIncrement(cols[i]) && !useAutoIncColumn) continue;
            if(!first) out.write(("," + NEWLINE).getBytes());
            else first = false;
            cols[i].put("VALUE", (vf != null ? vf.getValue(cols[i]) : null));
            out.write(formatSection(section + ".values", cols[i]).getBytes());
        }
        tableProps.put("VALUE_SOURCE", (vf != null ? vf.getValueSource(tableProps) : null));
        out.write(formatSection(section + ".footer", tableProps).getBytes());        
    }
    
    public interface ValueFactory {
         public String getValue(Map colProps) throws SQLException ;
         public String getValueSource(Map tableProps) ;
    }
    
    public class UnknownValueFactory implements ValueFactory {
        public String getValueSource(Map tableProps) {
            return null;
        }
        
        public String getValue(Map colProps) {
            if(((Integer)colProps.get("NULLABLE")).intValue() == DatabaseMetaData.columnNullable) {
                return "NULL";
            } else {
                switch(((Integer)colProps.get("DATA_TYPE")).intValue()) {
                    case Types.CHAR: case Types.VARCHAR: case Types.LONGVARCHAR:
                        if(colProps.get("COLUMN_NAME").toString().startsWith("Coded") || ((Integer)colProps.get("COLUMN_SIZE")).intValue() < 3)
                            return "''";
                        else
                            return "dbo.f_UnknownValue()";
                    case Types.BIGINT: case Types.BIT: case Types.DECIMAL: case Types.DOUBLE: case Types.FLOAT: case Types.INTEGER: case Types.NUMERIC: case Types.REAL: case Types.SMALLINT: case Types.TINYINT:
                        return "0";
                    case Types.DATE: case Types.TIME: case Types.TIMESTAMP:
                        return "GetDate()";
                    default:
                        return "/*???*/";
                }
            }
        } 
    }
    
    public class UnknownXLateValueFactory implements ValueFactory {
        public String getValueSource(Map tableProps) {
            return "EU_Staging.dbo.XlateCoded" + NEWLINE + "    WHERE CodedCategory = 'Unknown' AND CodedValue = ''";
        }
        
        public String getValue(Map colProps) {
            if("LocaleName".equalsIgnoreCase((String)colProps.get("COLUMN_NAME"))) {
                return "LocaleName";
            } else if(((Integer)colProps.get("NULLABLE")).intValue() == DatabaseMetaData.columnNullable) {
                return "NULL";
            } else {
                switch(((Integer)colProps.get("DATA_TYPE")).intValue()) {
                    case Types.CHAR: case Types.VARCHAR: case Types.LONGVARCHAR:
                        if(colProps.get("COLUMN_NAME").toString().startsWith("Coded") || ((Integer)colProps.get("COLUMN_SIZE")).intValue() < 3)
                            return "''";
                        else
                            return "XLateValue";
                    case Types.BIGINT: case Types.BIT: case Types.DECIMAL: case Types.DOUBLE: case Types.FLOAT: case Types.INTEGER: case Types.NUMERIC: case Types.REAL: case Types.SMALLINT: case Types.TINYINT:
                        return "0";
                    case Types.DATE: case Types.TIME: case Types.TIMESTAMP:
                        return "GetDate()";
                    default:
                        return "/*???*/";
                }
            }
        } 
    }
    
    public class LiteralValueFactory implements ValueFactory {
        protected String from = "";
        protected int z = 0;
        public String getValueSource(Map tableProps) {     
            return from;
        }
        public String getValue(Map colProps) throws SQLException {
            String colName = (String)colProps.get("COLUMN_NAME");
            if(colName.toUpperCase().startsWith("Source".toUpperCase())) {
                //find pk in source db with same name
                String sql = "SELECT TOP 1 * FROM (SELECT SourceTable TABLE_NAME, SourceColumn COLUMN_NAME, 1 Priority, 0 Ident"
                    + " FROM EU_Staging.dbo.TmpSource"
                    + " WHERE TargetTable = '"+ (String)colProps.get("TABLE_NAME") + "' AND TargetColumn = '" + colName + "'"
                    + " UNION ALL SELECT obj.name, col.name, 2, col.status & 0x80" 
                    + " from IntelliClaimNet.dbo.sysconstraints con"
                    + " join IntelliClaimNet.dbo.sysobjects obj on con.id = obj.id"
                    + " join IntelliClaimNet.dbo.sysobjects nm on con.constid = nm.id "
                    + " join IntelliClaimNet.dbo.sysindexes ind on con.id = ind.id and nm.name = ind.name"
                    + " join IntelliClaimNet.dbo.syscolumns col on col.id = con.id and col.name = index_col('IntelliClaimNet.dbo.' + obj.name, ind.indid, 1) "
                    + " where con.status & 0xf = 1"
                    + " and ind.keycnt = 1 and col.name IN('" + colName.substring(6) + "', EU_Staging.dbo.f_ExpandName('" + colName.substring(6) + "'))"
                    + " ) a ORDER BY Priority, Ident DESC";
                log.debug("SQL='" + sql + "'");
                ResultSet rs = getConnection().prepareStatement(sql).executeQuery();
                try {
                    if(rs.next()) {
                        String tab = "IntelliClaimNet.dbo." + rs.getString("TABLE_NAME");
                        String col = rs.getString("COLUMN_NAME");
                        String alias = createAlias(z++);
                        if(from.length() > 0) from += NEWLINE + "    CROSS JOIN "; //"," + NEWLINE + "         ";
                        from += tab + " " + alias;        
                        return alias + "." + col;
                    } else {
                        log.warn("Could not find source for column '" + colName + "' on table '" + (String)colProps.get("TABLE_NAME") + "'");
                    }
                } finally {
                    rs.close();
                    rs = null;
                }
        
            } else if(colName.toUpperCase().startsWith("Coded".toUpperCase())) {
                //get the type
                String sql = "SELECT TOP 1 CodedCategory FROM EU_Staging.dbo.XlateCoded WHERE '" + colName.substring(5) 
                    + "' LIKE '%' + CodedCategory ORDER BY LEN(CodedCategory) DESC";
                log.debug("SQL='" + sql + "'");
                ResultSet rs = getConnection().prepareStatement(sql).executeQuery();
                try {
                    if(rs.next()) {
                        String tab = "(SELECT DISTINCT CodedValue FROM EU_Staging.dbo.XlateCoded WHERE CodedCategory = '" 
                            + rs.getString("CodedCategory") + "')";
                        String alias = createAlias(z++);
                        if(from.length() > 0) from += NEWLINE + "    CROSS JOIN "; //"," + NEWLINE + "         ";
                        from += tab + " " + alias;        
                        return alias + ".CodedValue";
                    } else {
                        log.warn("Could not find translation category for column '" + colName + "' on table '" + (String)colProps.get("TABLE_NAME") + "'");
                    }
                } finally {
                    rs.close();
                    rs = null;
                }
        
            } else if(colName.equalsIgnoreCase("CreateDate")) {
                return "GetDate()";
            } else if(colName.equalsIgnoreCase("UpdateDate")) {
                return "NULL";
            }
            return "/*???*/";
        }
    }
    
    public class XlateValueFactory implements ValueFactory {
        protected int z = 0;
        protected String from = "";
        protected String litTable = null;
        protected String litAlias = "litDim";
        public String getValueSource(Map tableProps) {
            return from;
        }
        public String getValue(Map colProps) throws SQLException {
            if(litTable == null) {
                litTable = ((String)colProps.get("TABLE_NAME")).replaceAll("X[lL]ate", "Literal");
                if(litTable.equalsIgnoreCase("ConditionLiteralDim")) litTable = "ConditionDim";
                from += "EU_CEIADW.dbo." + litTable + " " + litAlias + " CROSS JOIN IntelliClaimNet.dbo.Locale locale" 
                    + NEWLINE + "    JOIN IntelliClaimNet.dbo.XLate xlate ON locale.Literal_ResID = xlate.LiteralID AND locale.LocaleID = xlate.LocaleID";               
            }
            String colName = (String)colProps.get("COLUMN_NAME");
            if(colName.equalsIgnoreCase("LocaleName")) {
                return "xlate.XLateValue";
            } else if(colName.equalsIgnoreCase(litTable + "Id")) {
                return litAlias + "." + litTable + "Id";
            } else if(colName.equalsIgnoreCase("CreateDate")) {
                return "GetDate()";
            } else if(colName.equalsIgnoreCase("UpdateDate")) {
                return "NULL";
            }
            //figure out which column from literal table holds the values
            String sql = "SELECT TOP 1 * FROM (SELECT SourceTable TABLE_NAME, SourceColumn COLUMN_NAME, 1 Priority"
                + " FROM EU_Staging.dbo.TmpSource"
                + " WHERE TargetTable = '"+ (String)colProps.get("TABLE_NAME") + "' AND TargetColumn = '" + colName + "'"
                + " UNION ALL SELECT o.name, c.name, 2" 
                + " FROM " + (String)colProps.get("TABLE_CAT") + "." + (String)colProps.get("TABLE_SCHEM")
                + ".sysobjects o JOIN " + (String)colProps.get("TABLE_CAT") + "." + (String)colProps.get("TABLE_SCHEM")
                + ".syscolumns c ON o.id = c.id "
                + " WHERE o.name = '" + litTable + "'"
                + " AND c.name LIKE '%" + colName + "%') a"
                + " ORDER BY Priority, LEN(COLUMN_NAME) ASC";
            log.debug("SQL='" + sql + "'");
            ResultSet rs1 = getConnection().prepareStatement(sql).executeQuery();
            try {
                if(rs1.next()) {
                    String litCol = rs1.getString("COLUMN_NAME");
                    if(litCol.toUpperCase().startsWith("Source".toUpperCase())) {
                        //find pk in source db with same name
                        sql = "SELECT TOP 1 * FROM (SELECT SourceTable TABLE_NAME, SourceColumn COLUMN_NAME, 1 Priority, 0 Ident"
                            + " FROM EU_Staging.dbo.TmpSource"
                            + " WHERE TargetTable = '"+ litTable + "' AND TargetColumn = '" + litCol + "'"
                            + " UNION ALL SELECT obj.name, col.name, 2, col.status & 0x80" 
                            + " from IntelliClaimNet.dbo.sysconstraints con"
                            + " join IntelliClaimNet.dbo.sysobjects obj on con.id = obj.id"
                            + " join IntelliClaimNet.dbo.sysobjects nm on con.constid = nm.id "
                            + " join IntelliClaimNet.dbo.sysindexes ind on con.id = ind.id and nm.name = ind.name"
                            + " join IntelliClaimNet.dbo.syscolumns col on col.id = con.id and col.name = index_col('IntelliClaimNet.dbo.' + obj.name, ind.indid, 1) "
                            + " where con.status & 0xf = 1"
                            + " and ind.keycnt = 1 and col.name IN('" + litCol.substring(6) + "', EU_Staging.dbo.f_ExpandName('" + litCol.substring(6) + "'))"
                            + " ) a ORDER BY Priority, Ident DESC";
                        log.debug("SQL='" + sql + "'");
                        ResultSet rs2 = getConnection().prepareStatement(sql).executeQuery();
                        try {
                            if(rs2.next()) {
                                String tab = "IntelliClaimNet.dbo." + rs2.getString("TABLE_NAME");
                                String col = rs2.getString("COLUMN_NAME");
                                String alias = createAlias(z++);
                                from += NEWLINE + "    JOIN " + tab + " " + alias 
                                    + " ON " + alias + "." + col + " = " + litAlias + "." + litCol
                                    + NEWLINE + "    JOIN IntelliClaimNet.dbo.XLate xlate_" + alias 
                                    + " ON locale.LocaleID = xlate_" + alias + ".LocaleID AND " + alias 
                                    + ".Literal_ResID = xlate_" + alias + ".LiteralID";
                                return "xlate_" + alias + ".XLateValue";
                            } else {
                                log.warn("Could not find source for column '" + litCol + "' on table '" + litTable + "'");
                            }
                        } finally {
                            rs2.close();
                            rs2 = null;
                        }
                    } else if(litCol.toUpperCase().startsWith("Coded".toUpperCase())) {
                        //get the type
                        sql = "SELECT TOP 1 CodedCategory FROM EU_Staging.dbo.XlateCoded WHERE '" + litCol.substring(5) 
                            + "' LIKE '%' + CodedCategory ORDER BY LEN(CodedCategory) DESC";
                        log.debug("SQL='" + sql + "'");
                        ResultSet rs3 = getConnection().prepareStatement(sql).executeQuery();
                        try {
                            if(rs3.next()) {
                                String alias = createAlias(z++);
                                from += NEWLINE + "    JOIN EU_Staging.dbo.XLateCoded coded_" + alias + " ON coded_" + alias 
                                    + ".CodedValue = " + litAlias + "." + litCol + " AND coded_" + alias + ".CodedCategory = '" 
                                    + rs3.getString("CodedCategory") + "' AND coded_" + alias + ".LocaleName = XLate.XLateValue";
                                return "coded_" + alias + ".XLateValue";
                            } else {
                                log.warn("Could not find translation category for column '" + litCol + "' on table '" + litTable + "'");
                            }
                        } finally {
                            rs3.close();
                            rs3 = null;
                        }
                    } else {
                        log.warn("Not sure how to get source for Column '" + litCol + "'");
                    }
                } else {
                    log.warn("Could not find source for column '" + colName + "' on table '" + (String)colProps.get("TABLE_NAME") + "'");
                }
            } finally {
                rs1.close();
                rs1 = null;
            }
            return "/*???*/";
        }
    }

    public static String createAlias(int n) {
        char c[] = Integer.toString(n, 26).toCharArray();
        final int nAdj = 'a' - '0'; 
        final int aAdj = 10;
        for(int i = 0; i < c.length; i++) c[i] = (char)(c[i] + (c[i] < 'a' ? nAdj : aAdj));
        return new String(c);
    }
    
    public static boolean isAutoIncrement(Map colProps) {
        return colProps.get("TYPE_NAME").toString().endsWith(" identity");
    }
    
    public static String formatSection(String section, Map props) {
        MessageFormat mf = (MessageFormat) sections.get(section);
        if(mf == null) throw new IllegalArgumentException("Section '" + section + "' was not configured in the properties file");
        return mf.format(props);
    }
    
    public static Map[] retrieveColumns(DatabaseMetaData md, String tableName, String catalogName, String schemaName) throws SQLException {
        ResultSet rs = md.getColumns(catalogName, schemaName, tableName, null);
        List cols = new java.util.LinkedList();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            String[] columnNames = new String[rsmd.getColumnCount()];
            for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
            while(rs.next()) {
                Map m = new java.util.HashMap();
                for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs.getObject(i+1));
                cols.add(m);
            }
        } finally {
            rs.close();
            rs = null;
        }
        return (Map[])cols.toArray(new Map[cols.size()]);
    }
    
    public static Map[] retrieveTables(DatabaseMetaData md, String catalogName, String schemaName) throws SQLException {
        //ResultSet rs = md.getTables(catalogName, schemaName, null, new String[] {"TABLE"});
        //ResultSet rs = md.getTables(catalogName, null, null, null);
        String sql = "SELECT '" + catalogName 
            + "' TABLE_CAT, su.name TABLE_SCHEM, so.name TABLE_NAME, CASE so.xtype WHEN 'S' THEN 'SYSTEM TABLE' WHEN 'U' THEN 'TABLE' WHEN 'V' THEN 'VIEW' END TABLE_TYPE"
            + " FROM " + catalogName + ".dbo.sysobjects so JOIN " + catalogName + ".dbo.sysusers su ON so.uid = su.uid"
            + " WHERE so.xtype = 'U'"
            + (schemaName == null ? "" : " AND su.name = '" + schemaName + "'")
            + " ORDER BY TABLE_TYPE, TABLE_SCHEM, TABLE_NAME";
        log.debug("SQL='" + sql + "'");
        ResultSet rs = md.getConnection().prepareStatement(sql).executeQuery();
        List tabs = new java.util.LinkedList();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            String[] columnNames = new String[rsmd.getColumnCount()];
            for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
            while(rs.next()) {
                Map m = new java.util.HashMap();
                for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs.getObject(i+1));
                tabs.add(m);
            }
        } finally {
            rs.close();
            rs = null;
        }
        return (Map[])tabs.toArray(new Map[tabs.size()]);
    }
    
    public static Map[] retrieveViews(DatabaseMetaData md, String schemaName) throws SQLException {
        ResultSet rs = md.getTables(null, schemaName, null, new String[] {"VIEW"});
        List tabs = new java.util.LinkedList();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            String[] columnNames = new String[rsmd.getColumnCount()];
            for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
            while(rs.next()) {
                Map m = new java.util.HashMap();
                for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs.getObject(i+1));
                tabs.add(m);
            }
        } finally {
            rs.close();
            rs = null;
        }
        return (Map[])tabs.toArray(new Map[tabs.size()]);
    }
   
    public static Properties loadProps(java.io.InputStream in) throws IOException {
        Properties props = new Properties();
        java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(in));
        String line;
        for(int i = 1; (line=br.readLine()) != null; i++) {
            if(line.trim().length() == 0 || line.trim().charAt(0) == '#') continue;
            java.util.StringTokenizer token = new java.util.StringTokenizer(line);
            try {
                String key = token.nextToken(":=");
                String value = token.nextToken("").substring(1);// b/c the previous token is returned with this call
                props.put(key, convertProperty(value));
            } catch(java.util.NoSuchElementException nsee) {
                log.info("Invalid property '" + line + "'");
            }            
        }
        return props;
    }
    
    /*
     * Converts encoded &#92;uxxxx to unicode chars
     * and changes special saved chars to their original forms
     */
    public static String convertProperty(String value) {
        char ch;
        int len = value.length();
        StringBuffer sb = new StringBuffer(len);
        for (int x = 0; x < len; ) {
            ch = value.charAt(x++);
            if(ch == '\\') {
                ch = value.charAt(x++);
                switch(ch) {
                    case 'u':
                        // Read the xxxx
                        int v = 0;
                        for (int i = 0; i < 4; i++) {
                            ch = value.charAt(x++);
                            switch (ch) {
                              case '0': case '1': case '2': case '3': case '4':
                              case '5': case '6': case '7': case '8': case '9':
                                 v = (v << 4) + ch - '0';
                                 break;
                              case 'a': case 'b': case 'c':
                              case 'd': case 'e': case 'f':
                                 v = (v << 4) + 10 + ch - 'a';
                                 break;
                              case 'A': case 'B': case 'C':
                              case 'D': case 'E': case 'F':
                                 v = (v << 4) + 10 + ch - 'A';
                                 break;
                              default:
                                  throw new IllegalArgumentException(
                                               "Malformed \\uxxxx encoding.");
                            }
                        }
                        ch = (char)v;
                        break;
                    case 't': ch = '\t'; break;
                    case 'r': ch = '\r'; break;
                    case 'n': ch = '\n'; break;
                    case 'f': ch = '\f'; break;
                }
            } 
            sb.append(ch);
        }
        return sb.toString();
    }
    
    /**
     * Getter for property directory.
     * @return Value of property directory.
     */
    public File getDirectory() {
        return this.directory;
    }
    
    /**
     * Setter for property directory.
     * @param directory New value of property directory.
     */
    public void setDirectory(File directory) {
        this.directory = directory;
    }
    
    /**
     * Getter for property connection.
     * @return Value of property connection.
     */
    public Connection getConnection() {
        return this.connection;
    }
    
    /**
     * Setter for property connection.
     * @param connection New value of property connection.
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
}
