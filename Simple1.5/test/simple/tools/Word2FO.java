/*
 * Word2FO.java
 *
 * Created on December 8, 2003, 11:44 AM
 */

package simple.tools;
/*
import java.io.*;
import java.io.File;
import javax.swing.JFileChooser;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import org.apache.fop.apps.Driver;
import org.xml.sax.InputSource;
import wh2fo.apps.ElementTreeWriter;
import wh2fo.elements.Element;
import wh2fo.parser.DocumentParser;
*/
/**
 *
 * @author  Brian S. Krug
 */
public class Word2FO {
    
    /** Creates a new instance of Word2FO */
    public Word2FO() {
    }
    
    /**
     * @param args the command line arguments
     * @throws Exception
     */
/*    
    public static void main(String[] args) throws Exception {
        boolean full = true;
        JFileChooser jfc = new JFileChooser();
        File inFile = null;
        if(full) {
            //get input file
            jfc.addChoosableFileFilter(new simple.io.ExtensionFileFilter(new String[] {"html", "htm"}, "Word HTML Documents"));
            jfc.setDialogTitle("Open Source File");
            if(jfc.showOpenDialog(null) != jfc.APPROVE_OPTION) {
                return;
            }
            inFile = jfc.getSelectedFile();       
            jfc.resetChoosableFileFilters();
        }
        
        //get output file
        jfc.setDialogTitle("Save Location");
        if(jfc.showSaveDialog(null) != jfc.APPROVE_OPTION) {
            return;
        }
        File outFile = jfc.getSelectedFile();       
        
        if(full) {
            transform(inFile, outFile, false);
            System.out.println("Finished creating FO... Generating PDF");
        }
        
        generatePDF(outFile);        
        System.out.println("DONE!");
        
        System.exit(0);
    }

    public static void transform0(File inFile, File outFile) throws Exception {
                //transform
        //transform(new FileInputStream(inFile), new FileOutputStream(outFile),  false);
        String tmp = outFile.getAbsolutePath();        
        new wh2fo.apps.Translator(inFile.getAbsolutePath(), tmp + ".xml",tmp + ".xsl", tmp + "Attr.xsl", false, false, false);
        //Transform to xml:fo
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(new StreamSource(new File(tmp + ".xsl")));
        
        //Setup input for XSLT transformation
        Source src = new StreamSource(new File(tmp + ".xml"));

        //Resulting SAX events (the generated FO) must be piped through to FOP
        Result res = new StreamResult(outFile);

        //Start XSLT transformation and FOP processing
        transformer.transform(src, res);
    }
    
    public static void transform(File inFile, File outFile, boolean toc) throws IOException, TransformerException, TransformerConfigurationException {
        String producer = wh2fo.apps.Version.producer;
        File xslFile = File.createTempFile("Word2FO", ".xsl");
	File attrFile = File.createTempFile("Word2FO", "Attr.xsl");
	File xmlFile = File.createTempFile("Word2FO", ".xml");
	DocumentParser parser = new DocumentParser(inFile.getAbsolutePath(), attrFile.getAbsolutePath(), xmlFile.getAbsolutePath(), false, false, toc);
        
	ElementTreeWriter attrWriter = new ElementTreeWriter(attrFile.getAbsolutePath(), producer);
        Element attrElem = parser.getXSLAttributesRoot();
        attrWriter.write(attrElem);
	//StringBuffer attr = attrWriter.writeToBuffer(attrElem);
        
	ElementTreeWriter xslWriter = new ElementTreeWriter(xslFile.getAbsolutePath(), producer);
        Element xslElem = parser.getXSLTemplatesRoot();
        xslWriter.write(xslElem);
	//xslElem.removeChildAt(0); //let's hope this is the import element
    //    for(int i = 0; i < attrElem.getNumberOfChildren(); i++) xslElem.addChildAt(attrElem.getChild(i), i);
	//StringBuffer xsl = xslWriter.writeToBuffer(xslElem);   
        
        // -- for debug
        //simple.io.IOUtils.saveToFile("debug.xml", new java.io.ByteArrayInputStream(xsl.toString().getBytes()));
        
	ElementTreeWriter xmlWriter = new ElementTreeWriter(xmlFile.getAbsolutePath(), producer);
	xmlWriter.write(parser.getXMLPreProc());
        //StringBuffer xml = xmlWriter.writeToBuffer(parser.getXMLPreProc());
        
        //Transform to xml:fo
        TransformerFactory factory = TransformerFactory.newInstance();
        Source tsrc = new StreamSource(xslFile);
        //Source tsrc = new StreamSource(new java.io.StringReader(xsl.toString()));
        Transformer transformer = factory.newTransformer(tsrc);
        
        //Setup input for XSLT transformation
        Source src = new StreamSource(xmlFile);
        //Source src = new StreamSource(new java.io.StringReader(xml.toString()));

        //Resulting SAX events (the generated FO) must be piped through to FOP
        Result res = new StreamResult(outFile);
        //Result res = new StreamResult(out);

        //Start XSLT transformation and FOP processing
        transformer.transform(src, res);
        
        //clean up
        xslFile.delete();
        attrFile.delete();
        xmlFile.delete();
    }
    
    public static void transform2(InputStream in, OutputStream out, boolean toc) throws IOException, TransformerException, TransformerConfigurationException {
        String producer = wh2fo.apps.Version.producer;
	DocumentParser parser = new DocumentParser(new java.io.BufferedReader(new java.io.InputStreamReader(in)), false, toc);
        
	ElementTreeWriter attrWriter = new ElementTreeWriter(producer);
        Element attrElem = parser.getXSLAttributesRoot();
	//StringBuffer attr = attrWriter.writeToBuffer(attrElem);
        
	ElementTreeWriter xslWriter = new ElementTreeWriter(producer);
        Element xslElem = parser.getXSLTemplatesRoot();
        xslElem.removeChildAt(0); //let's hope this is the import element
        for(int i = 0; i < attrElem.getNumberOfChildren(); i++) xslElem.addChildAt(attrElem.getChild(i), i);
        
	StringBuffer xsl = xslWriter.writeToBuffer(xslElem);        
        // -- for debug
        simple.io.IOUtils.saveToFile("debug.xml", new java.io.ByteArrayInputStream(xsl.toString().getBytes()));
        
	ElementTreeWriter xmlWriter = new ElementTreeWriter(producer);
	StringBuffer xml = xmlWriter.writeToBuffer(parser.getXMLPreProc());
        
        //Transform to xml:fo
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(new StreamSource(new java.io.StringReader(xsl.toString())));
        
        //Setup input for XSLT transformation
        Source src = new StreamSource(new java.io.StringReader(xml.toString()));

        //Resulting SAX events (the generated FO) must be piped through to FOP
        Result res = new StreamResult(out);

        //Start XSLT transformation and FOP processing
        transformer.transform(src, res);
    }

    public static void generatePDF(File foFile) throws Exception {
            //org.apache.fop.configuration.Configuration.put("fontBaseDir","C:/my projects/my fonts");
            new org.apache.fop.apps.Options(new File("C:/my projects/Simple/src/fop-config.xml"));
            OutputStream out = new BufferedOutputStream(new FileOutputStream(foFile.getAbsolutePath() + ".pdf"));
            Driver driver = new Driver(new InputSource(new FileInputStream(foFile)), out);
            //driver.setLogger(new ApacheToAvalonLogger(log));
            driver.setRenderer(Driver.RENDER_PDF);
            driver.run();
            
            out.flush();
            out.close();
    }
*/
}
