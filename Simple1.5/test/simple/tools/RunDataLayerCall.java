package simple.tools;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.Call;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.Parameter;
import simple.db.ParameterException;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.App;
import simple.io.ExtensionFileFilter;
import simple.io.LoadingException;
import simple.io.Log;
import simple.results.ScrollableResults;
import simple.swt.ResultsTableModel;

public class RunDataLayerCall {
	private static final Log log = Log.getLog();
	protected JFrame frame = new JFrame();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new RunDataLayerCall().run();
		} catch(Exception e) {
			log.error(e);
		}
		Log.finish();
	}

	public void run() throws FileNotFoundException, ConfigException, LoadingException, ConvertException, ParameterException, SQLException, DataLayerException {
		// get the connection properties file
		final File propertiesFile = chooseFile("Connection Properties File", "The file containing the connection properties", "properties", "Properties file");
		if(propertiesFile == null)
			return;
		App.setPropertiesLoader(new App.PropertiesLoader() {
			public Properties loadProperties(String appName) throws IOException {
				Properties props = new Properties();
				props.load(new FileInputStream(propertiesFile));
				return props;
			}
		});
		// get the datalayer file
		File dataLayerFile = chooseFile("Data Layer File", "The file containing the database call", "xml", "DataLayer file");
		if(dataLayerFile == null)
			return;
		ConfigLoader.loadConfig(new FileInputStream(dataLayerFile));

		String callId = chooseOption("Select Database Call", "Which Call would you like to run?", DataLayerMgr.getGlobalDataLayer().getCallSource().getCallIds());
		if(callId == null)
			return;

		Call call = DataLayerMgr.getGlobalDataLayer().findCall(callId);
		Map<String, Argument> inputs = new HashMap<String, Argument>();
		int k = 0;
		for(Argument argument : call.getArguments()) {
			if(argument.isIn()) {
				k++;
				if(argument.getPropertyName() != null && argument.getPropertyName().trim().length() > 0) {
					Argument prev = inputs.get(argument.getPropertyName().trim());
					if(!(prev instanceof Parameter)
							|| (argument instanceof Parameter
							&& ((((Parameter)argument).isRequired() && !((Parameter)prev).isRequired()))
							|| ((Parameter)prev).getDefaultValue() == null)) {
						inputs.put(argument.getPropertyName().trim(), argument);
					}
				} else {
					inputs.put("Parameter #" + k, argument);
				}
			}
		}
		Map<String,Object> parameters = new HashMap<String, Object>();
		for(Map.Entry<String, Argument> entry : inputs.entrySet()) {
			Object o = enterValue(entry.getKey(), entry.getValue().getJavaType(),
					entry.getValue() instanceof Parameter && ((Parameter)entry.getValue()).isRequired(),
					entry.getValue() instanceof Parameter ? ((Parameter)entry.getValue()).getDefaultValue() : null);
			parameters.put(entry.getKey(), o);

		}
		k = 0;
		Object[] paramArray = new Object[call.getInCount()];
		for(Argument argument : call.getArguments()) {
			if(argument.isIn()) {
				k++;
				if(argument.getPropertyName() != null && argument.getPropertyName().trim().length() > 0) {
					paramArray[k-1] = parameters.get(argument.getPropertyName().trim());
				} else {
					paramArray[k-1] = parameters.get("Parameter #" + k);
				}
			}
		}
		Connection conn = DataLayerMgr.getConnection(call.getDataSourceName());
		try {
			Object[] result = call.executeCall(conn, paramArray, null);
			if(result[0] instanceof Number) {
				conn.rollback();
				StringBuilder sb = new StringBuilder();
				sb.append("Modified ").append(result[0]).append(" rows");
				if(result.length > 1) {
					sb.append(" and output the following:");
					for(int i = 1; i < result.length; i++) {
						sb.append("\n\t").append(result[i]);
					}
				}
				JOptionPane.showConfirmDialog(frame,  sb.toString(), "Database Results", JOptionPane.OK_OPTION);
			} else if(result[0] instanceof ScrollableResults) {
				JPanel panel = new JPanel();
				panel.setLayout(new BorderLayout());
				JTable table = new JTable(new ResultsTableModel((ScrollableResults)result[0]));
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.add(table);
				panel.add(scrollPane, BorderLayout.CENTER);
				frame.removeAll();
				frame.getContentPane().add(panel);
				frame.setSize(500, 700);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		} finally {
			conn.close();
		}
	}

	public <T> T enterValue(String name, Class<T> javaType, boolean required, Object defaultValue) throws ConvertException {
		String value = JOptionPane.showInputDialog(frame, "Enter a value for " + name + " (" + javaType.getName() + ")" + (required ? "" : " (optional)"), defaultValue);
		return ConvertUtils.convert(javaType, value);
	}

	@SuppressWarnings("unchecked")
	public <T> T chooseOption(String title, String prompt, Set<T> options) {
		T[] optionsArray = (T[])options.toArray();
		return chooseOption(title, prompt, optionsArray);
	}

	public <T> T chooseOption(String title, String prompt, T[] options) {
		int index = JOptionPane.showOptionDialog(frame, prompt, title, JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
		if(index == JOptionPane.CLOSED_OPTION)
			return null;
		else
			return options[index];
	}

	public  File chooseFile(String name, String description, String extension, String extensionDescription) {
		JFileChooser chooser = new JFileChooser();
		FileFilter filter = new ExtensionFileFilter(extensionDescription, extension);
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(frame);
		if(returnVal == JFileChooser.APPROVE_OPTION)
			return chooser.getSelectedFile();
		else
			return null;
	}

}
