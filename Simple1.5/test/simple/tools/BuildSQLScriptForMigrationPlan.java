package simple.tools;

import java.awt.Window;
import java.io.Console;
import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;

import simple.io.ConsoleInteraction;
import simple.io.GuiInteraction;
import simple.io.IOUtils;
import simple.io.InOutInteraction;
import simple.io.Interaction;
import simple.io.Log;

public class BuildSQLScriptForMigrationPlan {
	private static final Log log = Log.getLog();
	protected static class SlashCheck extends FilterOutputStream {
		protected int pos = 0;
		public SlashCheck(OutputStream out) {
			super(out);
		}
		@Override
		public void write(int b) throws IOException {
			checkChar((char) b);
			super.write(b);
		}
		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			for(int i = off; i < off + len; i++) {
				checkChar((char) b[i]);
			}
			super.write(b, off, len);
		}

		public boolean isNeedsSlash() {
			return pos == 4;
		}

		protected void checkChar(char ch) {
			switch(pos) {
				case 0: // e, E
					if('e' == ch || 'E' == ch)
						pos++;
					break;
				case 1:
					if('n' == ch || 'N' == ch)
						pos++;
					else
						pos = 0;
					break;
				case 2:
					if('d' == ch || 'D' == ch)
						pos++;
					else
						pos = 0;
					break;
				case 3:
					if(';' == ch)
						pos++;
					else if(!Character.isWhitespace(ch))
						pos = 0;
					break;
				case 4:
					if('/' == ch)
						pos++;
					else if(!Character.isWhitespace(ch))
						pos = 0;
					break;
				default:
					if(!Character.isWhitespace(ch))
						pos = 0;
			}
		}
	};
	protected static String getSystemProperty(String systemProperty, String defaultValue) {
		String value = System.getProperty(systemProperty);
		if (value == null || value.trim().length() == 0)
			return defaultValue;
		else
			return value;
	}
	
	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 */
	public static void main(String[] args) throws IOException, URISyntaxException, KeyManagementException, NoSuchAlgorithmException {
		String release = getSystemProperty("release", "R26");
		String kbPagePrefix = getSystemProperty("kb.page.prefix", "EdgeServer");
		String filePrefix = getSystemProperty("file.prefix", "D:\\Development\\Database Projects\\DatabaseScripts\\releases\\REL_" + kbPagePrefix + release.replace(" - ", "_").replace("-", "_").replace(" ", "_").replace(".sql", "").replace(".", "_"));
		Interaction interaction;
		Console console;
		if((console = System.console()) != null) {
			interaction = new ConsoleInteraction(console);
		} else if(!java.awt.GraphicsEnvironment.isHeadless()) {
			interaction = new GuiInteraction((Window)null);
		} else {
			interaction = new InOutInteraction(System.out, System.in);
		}
		String user = System.getProperty("user.name");
		String username1 = user;
		char[] password1 = interaction.readPassword("Enter the knowledgebase password for %1$s:", username1);
		if(password1 == null)
			return;
		String username2 = user;
		char[] password2 = interaction.readPassword("Enter the ViewCVS password for %1$s:", username2);
		if(password2 == null)
			return;
		Pattern contextPattern = Pattern.compile("Details of SQL Scripts:?\\s*(?:<a[^<>]*>#</a>)?</h4>\\s*<ul>", Pattern.DOTALL);
		Pattern resourcesPattern = Pattern.compile("<li>\\s*<a class=\"external\"\\s+[^>]*>(.*?)</a>\\s*(?:<img\\s+.*?/>)?\\s*<ul>(.*?)</ul>", Pattern.DOTALL);
		Pattern resourceInstancePattern = Pattern.compile("<li>\\s*<a class=\"external\" href=\"([^\"]+)\"", Pattern.DOTALL);
		
		concatResourcesFrom(interaction, kbPagePrefix + release, username1, new String(password1), username2, new String(password2), contextPattern, resourcesPattern, resourceInstancePattern, filePrefix, release);
	}

	protected static void concatResourcesFrom(Interaction interaction, String kbPage, String username1, String password1, String username2, String password2, Pattern contextPattern, Pattern resourcesPattern, Pattern resourceInstancePattern, String outputFilePrefix, String release) throws IOException, URISyntaxException, KeyManagementException, NoSuchAlgorithmException {
		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		}}, null);
		final SSLSocketFactory factory = context.getSocketFactory();
		ProtocolSocketFactory protocolFactory = new SecureProtocolSocketFactory() {
			public Socket createSocket(String host, int port, InetAddress clientHost, int clientPort) throws IOException, UnknownHostException {
				return factory.createSocket(host, port, clientHost, clientPort);
			}

			public Socket createSocket(final String host, final int port, final InetAddress localAddress, final int localPort, final HttpConnectionParams params) throws IOException, UnknownHostException, ConnectTimeoutException {
				if(params == null) {
					throw new IllegalArgumentException("Parameters may not be null");
				}
				int timeout = params.getConnectionTimeout();
				if(timeout == 0)
					return createSocket(host, port, localAddress, localPort);
				Socket socket = factory.createSocket();
				socket.bind(new InetSocketAddress(localAddress, localPort));
				SocketAddress socketAddress = new InetSocketAddress(host, port);
				socket.connect(socketAddress, timeout);
				return socket;
			}

			public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
				return factory.createSocket(host, port);
			}

			public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
				return factory.createSocket(socket, host, port, autoClose);
			}

			public boolean equals(Object obj) {
				return ((obj != null) && obj.getClass().equals(getClass()));
			}

			public int hashCode() {
				return getClass().hashCode();
			}
		};
		Protocol.registerProtocol("https", new Protocol("https", protocolFactory, 443));
		
		URI kbLoginUri = new URI("https://knowledgebase.usatech.com/JSPWiki/Login.jsp");
		PostMethod method = new PostMethod(kbLoginUri.toString());
		method.addParameter("redirect", kbPage);
		HttpState state = new HttpState();
		state.setCredentials(new AuthScope(null, -1, null, "https"), new UsernamePasswordCredentials(username1, password1));
		//HttpClientParams params = new HttpClientParams();
		HttpClient client = new HttpClient();
		int code = client.executeMethod(null, method, state);
        if(code != HttpStatus.SC_OK) {
        	throw new IOException("Request failed with status code " + code + " (" + method.getStatusText() + ")");
        }
        
        URI kbUri = new URI("https://knowledgebase.usatech.com/JSPWiki/j_security_check");
		method = new PostMethod(kbUri.toString()) {
			public boolean getFollowRedirects() {
		        return true;
		    }
		};
		method.addParameter("redirect", kbPage);
		method.addParameter("j_username", username1);
		method.addParameter("j_password", password1);
		method.addParameter("submitlogin", "Login");
		
		code = client.executeMethod(null, method, state);
        if(code != HttpStatus.SC_OK) {
        	throw new IOException("Request failed with status code " + code + " (" + method.getStatusText() + ")");
        }
        String response = method.getResponseBodyAsString();
        Matcher matcher = contextPattern.matcher(response);
        PrintWriter pw = interaction.getWriter();
        if(!matcher.find()) {
        	pw.println("<==========>");
        	pw.println(response);
        	pw.println("<==========>");
			pw.flush();
        	throw new IOException("Context not found");
        }
        int start = matcher.end();
        matcher = resourcesPattern.matcher(response);
        matcher.region(start, response.length());
        if(!matcher.find()) {
        	pw.println("<==========>");
        	pw.println(response);
        	pw.println("<==========>");
			pw.flush();
        	throw new IOException("Resources not found");
        }
		state.setCredentials(new AuthScope(null, -1, null, "https"), new UsernamePasswordCredentials(username2, password2));

        int cnt = 0;
        do {
	        String fileName = matcher.group(1);
	        String tag = "REL_" + fileName.replace(" - ", "_").replace("-", "_").replace(" ", "_").replace(".sql", "").replace(".", "_");
	        String slice = matcher.group(2);
	        Matcher innerMatcher = resourceInstancePattern.matcher(slice);
	        File outputFile = new File(outputFilePrefix + '\\' + fileName);
			
	        PrintStream out = new PrintStream(outputFile);
	        SlashCheck slashCheck = new SlashCheck(out);
	        int subCnt = 0;
	        out.println("WHENEVER SQLERROR EXIT FAILURE COMMIT;");
	        out.println();
	        while(innerMatcher.find()) {
	        	String link = innerMatcher.group(1);
	        	pw.println("Concatenating '" + link + "' to file");
				pw.flush();
				method = new PostMethod(link) {
	    			public boolean getFollowRedirects() {
	    		        return true;
	    		    }
	    		};
	    		method.addParameter("user", username2);
				method.addParameter("pass", password2);
				method.addParameter("action", "Login"); 			
	    		if(!method.getURI().getScheme().equals("https")) {
	    			method.setURI(new org.apache.commons.httpclient.URI("https", method.getURI().getAuthority(), method.getURI().getPath(), method.getURI().getQuery(), method.getURI().getFragment()));
	    		}
	        	code = client.executeMethod(null, method, state);
	            if(code != HttpStatus.SC_OK) {
	            	pw.println("<==========>");
	            	pw.println(method.getResponseBodyAsString());
	            	pw.println("<==========>");
	            	throw new IOException("Could not get resource '" + link + "'; Status code " + code + " (" + method.getStatusText() + ")");
	            }
	            InputStream in = method.getResponseBodyAsStream();
	            out.print("-- Resource: ");
	            out.println(link);
	            IOUtils.copy(in, slashCheck);
	            out.println();
				if(slashCheck.isNeedsSlash()) {
	            	out.println('/');
	            	out.println();
	            }
	            //tag file in CVS
	            String rev = link.substring(link.indexOf("?rev=") + 5);
				String cvsFileName = URLDecoder.decode(link.substring(link.indexOf('/', link.indexOf(".cgi/") + 5) + 1, link.indexOf("?rev=")), "UTF-8");
	            pw.println("Tagging " + cvsFileName + " rev " + rev + " in CVS with tag " + tag);
				pw.flush();
				Process process = new ProcessBuilder("cvs", "rtag", "-F", "-r", rev, tag, cvsFileName).start();
	            String cvsError = IOUtils.readFully(process.getErrorStream());
	            if (cvsError != null && cvsError.length() > 0)
	            	throw new IOException("Error tagging the file " + cvsFileName + " in CVS: " + cvsError);
	            cnt++;
	            subCnt++;
	        }
	        pw.println("Concatentated " + subCnt + " resources into '" + outputFile.getCanonicalPath() + "'");
			pw.flush();
			log.info("Concatentated " + subCnt + " resources into '" + outputFile.getCanonicalPath() + "'");
        } while(matcher.find()) ;
        
        pw.println("Concatentated " + cnt + " resources for " + kbPage);
		pw.flush();
		log.info("Concatentated " + cnt + " resources for " + kbPage);
	}
}
