/*
 * WrapperGenerator.java
 *
 * Created on December 10, 2001, 9:26 AM
 */

package simple.tools;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.beans.ParameterDescriptor;
import java.io.File;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.sql.DataSource;

import simple.db.DataLayerMgr;
import simple.text.StringUtils;

public class DelegatingDataSourceGenerator {
    protected final static String newline = "\r\n";
    protected static boolean doSuper = true;
    protected static Comparator<Method> methodComparator = new java.util.Comparator<Method>() {
        public int compare(Method m1, Method m2) {
            int i = m1.getName().compareTo(m2.getName());
            if(i != 0) return i;
            Class<?>[] p1 = m1.getParameterTypes();
            Class<?>[] p2 = m2.getParameterTypes();
            i = p1.length - p2.length;
            if(i != 0) return i;
            for(int k = 0; k < p1.length; k++) {
                i = p1[k].getName().compareTo(p2[k].getName());
                if(i != 0) return i;
            }
            return 0;
        }
    };
    protected static Comparator<MethodDescriptor> methodDescComparator = new java.util.Comparator<MethodDescriptor>() {
        public int compare(MethodDescriptor m1, MethodDescriptor m2) {
            int i = m1.getName().compareTo(m2.getName());
            if(i != 0) return i;
            ParameterDescriptor[] pd1 = m1.getParameterDescriptors();
            ParameterDescriptor[] pd2 = m2.getParameterDescriptors();
            if(pd1 == null || pd2 == null) {
            	Class<?>[] p1 = m1.getMethod().getParameterTypes();
                Class<?>[] p2 = m2.getMethod().getParameterTypes();
                i = p1.length - p2.length;
                if(i != 0) return i;
                for(int k = 0; k < p1.length; k++) {
                    i = p1[k].getName().compareTo(p2[k].getName());
                    if(i != 0) return i;
                }
            }
            i = pd1.length - pd2.length;
            if(i != 0) return i;
            for(int k = 0; k < pd1.length; k++) {
                i = pd1[k].getName().compareTo(pd2[k].getName());
                if(i != 0) return i;
            }
            return 0;
        }
    };
    protected static Comparator<Class<?>> classComparator = new java.util.Comparator<Class<?>>() {
        public int compare(Class<?> c1, Class<?> c2) {
            return c1.getName().compareTo(c2.getName());
        }
    };
    /** Creates new WrapperGenerator */
    public DelegatingDataSourceGenerator() {
    }
    
    /**
     * @param args the command line arguments
     * @throws URISyntaxException 
     */
    public static void main(String args[]) throws Exception {
        doSuper = true;
        String packageName = "simple.db";
        String className = "DelegatingDataSource";
        File basePath = new File(DataLayerMgr.class.getClassLoader().getResource("simple/db/DataLayerMgr.class").toURI()).getParentFile();
        java.io.PrintStream out = new java.io.PrintStream(new java.io.FileOutputStream(new File(basePath, className + ".java")));
        makeWrapper(className,packageName, out);
        out.flush();
        System.out.println("Create of "+className+" succeeded");
        System.out.println("Done -------------------------------------------");
    }
    
    public static void makeWrapper(String className, String packageName, PrintStream out) throws IntrospectionException {
    	Set<Class<?>> imports = new TreeSet<Class<?>>(classComparator);
    	imports.add(DataSource.class);
    	imports.add(Connection.class);
    	imports.add(Statement.class);
    	imports.add(PreparedStatement.class);
    	imports.add(CallableStatement.class);
    	imports.add(SQLException.class);
    	imports.add(ResultSet.class);
    	
        Class<?> clazz = DataSource.class;
        java.text.DateFormat df = new java.text.SimpleDateFormat("MMMM dd, yyyy, h:mm a");
        out.println("/*");
        out.println(" * " + className + ".java");
        out.println(" *");
        out.println(" * Created on " + df.format(new java.util.Date()));
        out.println(" */");
        out.println();
        out.println("package " + packageName + ";");
        out.println();
        out.println("/**");
        out.println(" *");
        out.println(" * @author  Brian S. Krug");
        out.println(" */");
        out.println();
        for(Class<?> cl : imports) {
        	out.println("import " + cl.getName() + ";");
        }
        out.println();
        out.println("public abstract class " + className + " implements DataSource {");
        out.println("protected DataSource delegate;");
        out.println();
        out.println("public " + className + "(DataSource delegate) {");
        out.println("\nthis.delegate = delegate;");
        out.println("}");
        out.println();
        out.println("protected DataSource getDelegate() {");
        out.println();
        out.println("\treturn delegate;");
        out.println("}");
        out.println();
        
        Set<?> importsG = (Set<?>)imports;
        printDelegateMethods(clazz, Collections.singleton(Connection.class), importsG, out); 
        out.println("// The inner classes --------------------------------------");
        Set<Object> wrapReturnTypes1 = new HashSet<Object>();
        Set<Object> wrapReturnTypes2 = Collections.singleton((Object)ResultSet.class);
        wrapReturnTypes1.add(Statement.class);
        wrapReturnTypes1.add(PreparedStatement.class);
        wrapReturnTypes1.add(CallableStatement.class);
        printInnerClass(Connection.class, null, wrapReturnTypes1, importsG, out);
        printInnerClass(Statement.class, null, wrapReturnTypes2, importsG, out);
        printInnerClass(PreparedStatement.class, Statement.class, wrapReturnTypes2, importsG, out);
        printInnerClass(CallableStatement.class, PreparedStatement.class, wrapReturnTypes2, importsG, out);
        printInnerClass(ResultSet.class, null, null, importsG, out);
        out.println("// The inner class methods --------------------------------------");
        printOuterMethods(Connection.class, wrapReturnTypes1, importsG, out);
        printOuterMethods(Statement.class, wrapReturnTypes2, importsG, out);
        printOuterMethods(PreparedStatement.class, wrapReturnTypes2, importsG, out);
        printOuterMethods(CallableStatement.class, wrapReturnTypes2, importsG, out);
        printOuterMethods(ResultSet.class, null, importsG, out);
        out.println("}");
    }
    
    public static void printInnerClass(Class<?> clazz, Class<?> superClazz, Set<?> wrapReturnTypes, Set<?> imports, PrintStream out) throws IntrospectionException {
    	String prefix = lowerCaseFirst(clazz.getSimpleName());
    	out.print("protected final class Inner");
    	out.print(clazz.getSimpleName());
    	if(superClazz != null) {
    		if(!clazz.isInterface())
    			throw new IllegalArgumentException("Inner class " + clazz + " is not an interface and must be when a super class is used");
    		out.print(" extends ");
    		out.print(superClazz.getSimpleName());     	
    	}
    	out.print(clazz.isInterface() ? " implements " : " extends ");
    	out.print(clazz.getSimpleName());
    	out.println(" {");
    	out.print("protected ");
    	out.print(clazz.getSimpleName());
    	out.println(" delegate;");
        out.println();
        out.print("public Inner");
        out.print(clazz.getSimpleName());
        out.print("(");
        out.print(clazz.getSimpleName());
        out.println(" delegate) {");
        out.println("this.delegate = delegate;");
        out.print(prefix);
        out.println("Init(this);");
        out.println("}");
        out.println();
        out.print("protected ");
        out.print(clazz.getSimpleName());
        out.println(" getDelegate() {");
        out.println();
        out.println("\treturn delegate;");
        out.println("}");
        out.println();
        
        printInnerMethods(clazz, prefix, imports, out);
        out.println("}");
        out.println();
        
    }
    
    public static String lowerCaseFirst(String s) {
    	return s.substring(0, 1).toLowerCase() + s.substring(1);
    }
    public static void printInnerMethods(Class<?> clazz, String prefix, Set<?> imports, PrintStream out) throws IntrospectionException {
    	BeanInfo bi = Introspector.getBeanInfo(clazz);
        MethodDescriptor[] methods = bi.getMethodDescriptors();
        Arrays.sort(methods, methodDescComparator);
        StringBuilder vars = new StringBuilder();
        for(MethodDescriptor md : methods) {
        	Method m = md.getMethod();
            if(m.getDeclaringClass() != Object.class && (m.getDeclaringClass() == clazz) &&
            /*(m[i].getModifiers() & Modifier.FINAL) !=  Modifier.FINAL&& */ (m.getModifiers() & Modifier.STATIC) !=  Modifier.STATIC) {
            	out.print("public ");
                out.print(getSourceName(m.getReturnType(), imports));
                out.print(" ");
                out.print(m.getName());
                out.print("(");
                Class<?>[] paramTypes = m.getParameterTypes();
                ParameterDescriptor[] paramDescs = md.getParameterDescriptors();
                vars.setLength(0);
                for(int k = 0; k < paramTypes.length; k++) {
                    if(k > 0) {
                        out.print(", ");
                    }
                    out.print(getSourceName(paramTypes[k], imports));
                    out.print(" ");
                    String varName = (paramDescs == null ? getVarName(paramTypes[k], k) : paramDescs[k].getName());
                    out.print(varName);
                    vars.append(", ");
                    vars.append(varName);
                }
                out.print(")");
                Class<?>[] exceptTypes = m.getExceptionTypes();
                for(int k = 0; k < exceptTypes.length; k++) {
                    if(k > 0) out.print(", ");
                    else out.print(" throws ");
                    out.print(getSourceName(exceptTypes[k], imports));
                }
                out.println(" {");
                out.print("\t");
                if(m.getReturnType() != Void.TYPE && m.getReturnType() != null) {
                	out.print("return ");
                }
                out.print(prefix);
                out.print(StringUtils.capitalizeFirst(m.getName()));
                out.print("(this");
                out.print(vars);
	            out.println(");");
                out.println("}");
                out.println();
            }
        }
    }
    public static void printInnerMethodsOld(Class<?> clazz, String prefix, Set<?> imports, PrintStream out) {
    	Method[] methods = clazz.getMethods();
        Arrays.sort(methods, methodComparator);
        StringBuilder vars = new StringBuilder();
        for(Method m : methods) {
            if(m.getDeclaringClass() != Object.class && (m.getDeclaringClass() == clazz) &&
            /*(m[i].getModifiers() & Modifier.FINAL) !=  Modifier.FINAL&& */ (m.getModifiers() & Modifier.STATIC) !=  Modifier.STATIC) {
            	out.print("public ");
                out.print(getSourceName(m.getReturnType(), imports));
                out.print(" ");
                out.print(m.getName());
                out.print("(");
                Class<?>[] paramTypes = m.getParameterTypes();
                vars.setLength(0);
                for(int k = 0; k < paramTypes.length; k++) {
                    if(k > 0) {
                        out.print(", ");
                    }
                    out.print(getSourceName(paramTypes[k], imports));
                    out.print(" ");
                    String varName = getVarName(paramTypes[k], k);
                    out.print(varName);
                    vars.append(", ");
                    vars.append(varName);
                }
                out.print(")");
                Class<?>[] exceptTypes = m.getExceptionTypes();
                for(int k = 0; k < exceptTypes.length; k++) {
                    if(k > 0) out.print(", ");
                    else out.print(" throws ");
                    out.print(getSourceName(exceptTypes[k], imports));
                }
                out.println(" {");
                out.print("\t");
                if(m.getReturnType() != Void.TYPE && m.getReturnType() != null) {
                	out.print("return ");
                }
                out.print(prefix);
                out.print(StringUtils.capitalizeFirst(m.getName()));
                out.print("(this");
                out.print(vars);
	            out.println(");");
                out.println("}");
                out.println();
            }
        }
    }
    
    public static void printOuterMethods(Class<?> clazz, Set<?> wrapReturnTypes, Set<?> imports, PrintStream out) throws IntrospectionException {
    	printOuterMethods(clazz, "Inner" + clazz.getSimpleName(), lowerCaseFirst(clazz.getSimpleName()), wrapReturnTypes, imports, out);
	}
    
    public static void printOuterMethods(Class<?> clazz, String innerClassName, String prefix, Set<?> wrapReturnTypes, Set<?> imports, PrintStream out) throws IntrospectionException {
    	out.print("protected void ");
        out.print(prefix);
        out.print("Init(");
        out.print(innerClassName);
        out.print(" ");
        out.print(lowerCaseFirst(clazz.getSimpleName()));
        out.println(") {");
        out.println("}");
        
        BeanInfo bi = Introspector.getBeanInfo(clazz);
        MethodDescriptor[] methods = bi.getMethodDescriptors();
        Arrays.sort(methods, methodDescComparator);
        StringBuilder vars = new StringBuilder();
        for(MethodDescriptor md : methods) {
        	Method m = md.getMethod();
            if(m.getDeclaringClass() != Object.class && (m.getDeclaringClass() == clazz) &&
            /*(m[i].getModifiers() & Modifier.FINAL) !=  Modifier.FINAL&& */ (m.getModifiers() & Modifier.STATIC) !=  Modifier.STATIC) {
            	out.print("protected ");
                out.print(getSourceName(m.getReturnType(), imports));
                out.print(" ");
                out.print(prefix);
                out.print(StringUtils.capitalizeFirst(m.getName()));
                out.print("(");
                out.print(innerClassName);
                out.print(" ");
                out.print(lowerCaseFirst(clazz.getSimpleName()));
                Class<?>[] paramTypes = m.getParameterTypes();
                ParameterDescriptor[] paramDescs = md.getParameterDescriptors();
                vars.setLength(0);
                for(int k = 0; k < paramTypes.length; k++) {
                    if(k > 0) {
                        vars.append(", ");
                    }
                    out.print(", ");
                    out.print(getSourceName(paramTypes[k], imports));
                    out.print(" ");                   
                    String varName = (paramDescs == null ? getVarName(paramTypes[k], k) : paramDescs[k].getName());
                    out.print(varName);
                    vars.append(varName);
                }
                out.print(")");
                Class<?>[] exceptTypes = m.getExceptionTypes();
                for(int k = 0; k < exceptTypes.length; k++) {
                    if(k > 0) out.print(", ");
                    else out.print(" throws ");
                    out.print(getSourceName(exceptTypes[k], imports));
                }
                out.println(" {");
                out.print("\t");
                boolean extraParen = false;
                if(m.getReturnType() != Void.TYPE && m.getReturnType() != null) {
                	out.print("return ");
                	if(wrapReturnTypes != null && wrapReturnTypes.contains(m.getReturnType())) {
                		extraParen = true;
                		out.print("new Inner");
                		out.print(m.getReturnType().getSimpleName());
                		out.print("(");
                	}
                }
                out.print(lowerCaseFirst(clazz.getSimpleName()));
                out.print(".getDelegate().");
                out.print(m.getName());
                out.print("(");
                out.print(vars);
	            if(extraParen) out.println(")");
                out.println(");");
                out.println("}");
                out.println();
            }
        }
    }
    
    public static void printOuterMethodsOld(Class<?> clazz, String innerClassName, String prefix, Set<?> wrapReturnTypes, Set<?> imports, PrintStream out) {
    	out.print("protected void ");
        out.print(prefix);
        out.print("Init(");
        out.print(innerClassName);
        out.print(" ");
        out.print(lowerCaseFirst(clazz.getSimpleName()));
        out.println(") {");
        out.println("}");
        	
    	Method[] methods = clazz.getMethods();
        Arrays.sort(methods, methodComparator);
        StringBuilder vars = new StringBuilder();
        for(Method m : methods) {
            if(m.getDeclaringClass() != Object.class && (m.getDeclaringClass() == clazz) &&
            /*(m[i].getModifiers() & Modifier.FINAL) !=  Modifier.FINAL&& */ (m.getModifiers() & Modifier.STATIC) !=  Modifier.STATIC) {
            	out.print("protected ");
                out.print(getSourceName(m.getReturnType(), imports));
                out.print(" ");
                out.print(prefix);
                out.print(StringUtils.capitalizeFirst(m.getName()));
                out.print("(");
                out.print(innerClassName);
                out.print(" ");
                out.print(lowerCaseFirst(clazz.getSimpleName()));
                Class<?>[] paramTypes = m.getParameterTypes();
                vars.setLength(0);
                for(int k = 0; k < paramTypes.length; k++) {
                    if(k > 0) {
                        vars.append(", ");
                    }
                    out.print(", ");
                    out.print(getSourceName(paramTypes[k], imports));
                    out.print(" ");                   
                    String varName = getVarName(paramTypes[k], k);
                    out.print(varName);
                    vars.append(varName);
                }
                out.print(")");
                Class<?>[] exceptTypes = m.getExceptionTypes();
                for(int k = 0; k < exceptTypes.length; k++) {
                    if(k > 0) out.print(", ");
                    else out.print(" throws ");
                    out.print(getSourceName(exceptTypes[k], imports));
                }
                out.println(" {");
                out.print("\t");
                boolean extraParen = false;
                if(m.getReturnType() != Void.TYPE && m.getReturnType() != null) {
                	out.print("return ");
                	if(wrapReturnTypes != null && wrapReturnTypes.contains(m.getReturnType())) {
                		extraParen = true;
                		out.print("new Inner");
                		out.print(m.getReturnType().getSimpleName());
                		out.print("(");
                	}
                }
                out.print(lowerCaseFirst(clazz.getSimpleName()));
                out.print(".getDelegate().");
                out.print(m.getName());
                out.print("(");
                out.print(vars);
	            if(extraParen) out.println(")");
                out.println(");");
                out.println("}");
                out.println();
            }
        }
    }
    
    public static void printDelegateMethods(Class<?> clazz, Set<?> wrapReturnTypes, Set<?> imports, PrintStream out) throws IntrospectionException {
    	BeanInfo bi = Introspector.getBeanInfo(clazz);
        MethodDescriptor[] methods = bi.getMethodDescriptors();
        Arrays.sort(methods, methodDescComparator);
        StringBuilder vars = new StringBuilder();
        for(MethodDescriptor md : methods) {
        	Method m = md.getMethod();
            if(m.getDeclaringClass() != Object.class && (m.getDeclaringClass() == clazz) &&
            /*(m[i].getModifiers() & Modifier.FINAL) !=  Modifier.FINAL&& */ (m.getModifiers() & Modifier.STATIC) !=  Modifier.STATIC) {
            	out.print("public ");
                out.print(getSourceName(m.getReturnType(), imports));
                out.print(" ");
                out.print(m.getName());
                out.print("(");
                Class<?>[] paramTypes = m.getParameterTypes();
                ParameterDescriptor[] paramDescs = md.getParameterDescriptors();
                vars.setLength(0);
                for(int k = 0; k < paramTypes.length; k++) {
                    if(k > 0) {
                        out.print(", ");
                        vars.append(", ");
                    }
                    out.print(getSourceName(paramTypes[k], imports));
                    out.print(" ");
                    String varName = (paramDescs == null ? getVarName(paramTypes[k], k) : paramDescs[k].getName());
                    out.print(varName);
                    vars.append(varName);
                }
                out.print(")");
                Class<?>[] exceptTypes = m.getExceptionTypes();
                for(int k = 0; k < exceptTypes.length; k++) {
                    if(k > 0) out.print(", ");
                    else out.print(" throws ");
                    out.print(getSourceName(exceptTypes[k], imports));
                }
                out.println(" {");
                out.print("\t");
                boolean extraParen = false;
                if(m.getReturnType() != Void.TYPE && m.getReturnType() != null) {
                	out.print("return ");
                	if(wrapReturnTypes != null && wrapReturnTypes.contains(m.getReturnType())) {
                		extraParen = true;
                		out.print("new Inner");
                		out.print(m.getReturnType().getSimpleName());
                		out.print("(");
                	}
                }
                out.print("getDelegate().");
                out.print(m.getName());
                out.print("(");
                out.print(vars);
	            if(extraParen) out.println(")");
                out.println(");");
                out.println("}");
                out.println();
            }
        }
    }
    public static void printDelegateMethodsOld(Class<?> clazz, Set<?> wrapReturnTypes, Set<?> imports, PrintStream out) {
    	Method[] methods = clazz.getMethods();
        Arrays.sort(methods, methodComparator);
        StringBuilder vars = new StringBuilder();
        for(Method m : methods) {
            if(m.getDeclaringClass() != Object.class && (m.getDeclaringClass() == clazz) &&
            /*(m[i].getModifiers() & Modifier.FINAL) !=  Modifier.FINAL&& */ (m.getModifiers() & Modifier.STATIC) !=  Modifier.STATIC) {
            	out.print("public ");
                out.print(getSourceName(m.getReturnType(), imports));
                out.print(" ");
                out.print(m.getName());
                out.print("(");
                Class<?>[] paramTypes = m.getParameterTypes();
                vars.setLength(0);
                for(int k = 0; k < paramTypes.length; k++) {
                    if(k > 0) {
                        out.print(", ");
                        vars.append(", ");
                    }
                    out.print(getSourceName(paramTypes[k], imports));
                    out.print(" ");
                    String varName = getVarName(paramTypes[k], k);
                    out.print(varName);
                    vars.append(varName);
                }
                out.print(")");
                Class<?>[] exceptTypes = m.getExceptionTypes();
                for(int k = 0; k < exceptTypes.length; k++) {
                    if(k > 0) out.print(", ");
                    else out.print(" throws ");
                    out.print(getSourceName(exceptTypes[k], imports));
                }
                out.println(" {");
                out.print("\t");
                boolean extraParen = false;
                if(m.getReturnType() != Void.TYPE && m.getReturnType() != null) {
                	out.print("return ");
                	if(wrapReturnTypes != null && wrapReturnTypes.contains(m.getReturnType())) {
                		extraParen = true;
                		out.print("new Inner");
                		out.print(m.getReturnType().getSimpleName());
                		out.print("(");
                	}
                }
                out.print("getDelegate().");
                out.print(m.getName());
                out.print("(");
                out.print(vars);
	            if(extraParen) out.println(")");
                out.println(");");
                out.println("}");
                out.println();
            }
        }
    }
    public static String getShortName(String originalClassName) {
        int p = originalClassName.lastIndexOf('.');
        if(p < 0) {
            return originalClassName;
        } else {
            return originalClassName.substring(p + 1);
        }
    }
    
    public static String getSourceName(Class<?> clazz, Set<?> imports) {
        if(clazz.isArray()) {
            return getSourceName(clazz.getComponentType(), imports) + "[]";
        } else if(imports != null && imports.contains(clazz)) {
        	return clazz.getSimpleName();
        } else {
            return clazz.getName();
        }
    }
    
    public static String getVarName(Class<?> paramType, int i) {
        while(paramType.isArray()) paramType = paramType.getComponentType();
        return decapitalize(getShortName(paramType.getName()) + "Var" + i);
    }
    
    public static String decapitalize(String s) {
        StringBuffer sb = new StringBuffer();
        int i = 0;
        for(; i < s.length(); i++){
            if(Character.isLowerCase(s.charAt(i))) break;
            else sb.append(Character.toLowerCase(s.charAt(i)));
        }
        sb.append(s.substring(i));
        return sb.toString();
    }
}
