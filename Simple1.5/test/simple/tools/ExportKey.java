/**
 *
 */
package simple.tools;

import static simple.io.Base64Constants.BAD_ENCODING;
import static simple.io.Base64Constants.DECODABET;
import static simple.io.Base64Constants.EQUALS_SIGN_ENC;
import static simple.io.Base64Constants.white_SPACE_ENC;

import java.io.BufferedReader;
import java.io.Console;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import simple.io.Base64;
import simple.io.HeapBufferStream;

/**
 * Adopted from http://mark.foster.cc/pub/java/ExportPriv.java
 *
 */
public class ExportKey {
	public static void main(String args[]) throws Exception {
		String keystore;
		String alias;
		char[] password;
		String keytype;
		switch(args.length) {
			case 4:
				keystore = args[0];
				alias = args[1];
				keytype = args[2];
				password = args[3].toCharArray();
				break;
			case 3:
				keystore = args[0];
				alias = args[1];
				keytype = args[2];
				Console console = System.console();
				if(console == null) {
					System.err.println("Keystore Password:");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					String p = br.readLine();
					if(p == null) {
						System.err.println("Exitting");
						System.exit(1);
						return;
					}
					password = p.toCharArray();
				} else
					password = console.readPassword("Keystore Password:");
				break;
			case 2:
				String file = args[0];
				keytype = args[1];
				if("info".equalsIgnoreCase(keytype)) {
					ExportKey myep = new ExportKey();
					myep.printInfo2(file);
					return;
				}
			default:
				System.err.println("Usage: java ExportKey <keystore> <alias> <keytype (private or public)> <password>");
				System.exit(1);
				return;
		}
		ExportKey myep = new ExportKey();
		myep.doit(keystore, alias, keytype, password);
	}

	public void printInfo(String fileName) throws IOException, GeneralSecurityException {
		File file = new File(fileName);
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		byte[] bytes = new byte[(int) file.length()];
		in.readFully(bytes);
		int fs = 0;
		int fe = 0;
		RSAPublicKey pubKey = null;
		RSAPrivateKey privKey = null;
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		while(true) {
			fs = findEnd(bytes, "---BEGIN ".getBytes(), fe);
			String canonicalFile = file.getCanonicalPath();
			if(fs <= 0) {
				System.out.println("End of content");
				System.exit(0);
				return;
			}
			byte[] dash = "-".getBytes();
			fe = findStart(bytes, dash, fs);
			if(fe <= 0) {
				System.err.println("Invalid keys file '" + canonicalFile + "': '-' after '---BEGIN ' not found");
				System.exit(5);
				return;
			}
			String outputType = new String(bytes, fs, fe - fs).trim().toUpperCase();
			while(bytes[++fe] == dash[0])
				;
			fs = fe;
			fe = findStart(bytes, dash, fe);
			if(fe <= fs) {
				System.err.println("Invalid keys file '" + canonicalFile + "': end '-' after header not found");
				System.exit(5);
				return;
			}
			byte[] encoded = new byte[fe - fs];
			System.arraycopy(bytes, fs, encoded, 0, encoded.length);
			fs = findEnd(bytes, "---END ".getBytes(), fe);
			if(fs <= 0) {
				System.err.println("Invalid keys file '" + canonicalFile + "': '---END ' not found");
				System.exit(5);
				return;
			}
			fe = findStart(bytes, dash, fs);
			if(fe <= fs) {
				System.err.println("Invalid keys file '" + canonicalFile + "': end '-' after footer not found");
				System.exit(5);
				return;
			}
			String confirmOutputType = new String(bytes, fs, fe - fs).trim().toUpperCase();
			while(bytes[++fe] == dash[0])
				;
			fs = fe;
			if("PRIVATE KEY".equals(outputType)) {
				privKey = (RSAPrivateKey) keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encoded));
				System.out.print("Found private key: modulus=");
				System.out.println(privKey.getModulus());
			} else if("CERTIFICATE".equals(outputType)) {
				pubKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(encoded));
				System.out.print("Found public key: modulus=");
				System.out.println(pubKey.getModulus());
			}
		}

	}

	protected static final Pattern WRAPPER_PATTERN = Pattern.compile("\\s*-{3,}(BEGIN|END) (PRIVATE KEY|CERTIFICATE|NEW CERTIFICATE REQUEST|PUBLIC KEY|RSA PUBLIC KEY|DSA PUBLIC KEY)-{3,}\\s*");

	public void printInfo2(String fileName) throws IOException, GeneralSecurityException {
		File file = new File(fileName);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		try {
			HeapBufferStream bufferStream = new HeapBufferStream(true);
			OutputStream out = bufferStream.getOutputStream();
			String outputType = null;
			int position = -1;
			byte[] b4 = new byte[4];
			int n = 0;
			String line;
			while((line = reader.readLine()) != null) {
				if(!StringUtils.isBlank(line)) {
					Matcher matcher = WRAPPER_PATTERN.matcher(line);
					if(matcher.matches()) {
						if("BEGIN".equals(matcher.group(1))) {
							if(bufferStream.getLength() != 0)
								throw new IOException("Found ---BEGIN line in wrong place");
							outputType = matcher.group(2);
							position = 0;
						} else if("END".equals(matcher.group(1))) {
							if(bufferStream.getLength() == 0)
								throw new IOException("Found ---END line in wrong place");
							if(!outputType.equals(matcher.group(2)))
								System.err.println("---BEGIN line does not match ---END line");
							// System.out.println("Got bytes:");
							// System.out.println(simple.text.StringUtils.toHex(encoded));
							// System.out.println("In Base64:");
							// System.out.println(com.sun.org.apache.xml.internal.security.utils.Base64.encode(encoded));
							if("PRIVATE KEY".equals(outputType)) {
								byte[] encoded = new byte[bufferStream.size()];
								bufferStream.getInputStream().read(encoded);
								KeyFactory keyFactory = KeyFactory.getInstance("RSA");
								RSAPrivateKey privKey = (RSAPrivateKey) keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encoded));
								System.out.print("Found private key: modulus=");
								System.out.println(privKey.getModulus());
							} else if("CERTIFICATE".equals(outputType)) {
								CertificateFactory certFactory = CertificateFactory.getInstance("X509");
								X509Certificate cert = (X509Certificate) certFactory.generateCertificate(bufferStream.getInputStream());
								System.out.print("Found certificate: modulus=");
								System.out.println(((RSAPublicKey) cert.getPublicKey()).getModulus());
							} else if("PUBLIC KEY".equals(outputType)) {
								byte[] encoded = new byte[bufferStream.size()];
								bufferStream.getInputStream().read(encoded);
								KeyFactory keyFactory = KeyFactory.getInstance("RSA");
								RSAPublicKey pubKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(encoded));
								System.out.print("Found public key: modulus=");
								System.out.println(pubKey.getModulus());
							}
							bufferStream.clear();
							outputType = null;
						} else
							System.err.println("Invalid wrapper: '" + line + "'");
					} else if(outputType == null) {
						System.err.println("Ignoring invalid line '" + line + "'");
					} else {
						// System.out.println("#" + (++n) + ": Found " + line.length() + " character line");
						int orig = bufferStream.size();
						LOOP: for(int i = 0; i < line.length(); i++) {
							int b = line.charAt(i);
							b4[position] = DECODABET[b & 0x7f];
							switch(b4[position]) {
								case white_SPACE_ENC:
									break; // continue LOOP;
								case EQUALS_SIGN_ENC:
									position = -1;
									break LOOP;// end of stream reached
								case BAD_ENCODING:
									throw new IOException("Invalid byte '" + b + "'");
								default:
									switch(position) {
										case 0:
											position = 1;
											break; // continue LOOP
										case 1:
											position = 2;
											out.write(((b4[0] & 0x3F) << 2) | ((b4[1] & 0x30) >> 4)); // pos = 1;
											break;
										case 2:
											position = 3;
											out.write(((b4[1] & 0x0F) << 4) | ((b4[2] & 0x3C) >> 2)); // pos = 2;
											break;
										case 3:
											position = 0;
											out.write(((b4[2] & 0x03) << 6) | ((b4[3] & 0x3F) >> 0)); // pos = 3;
											break;
										default:
											break LOOP;
									}
							}
						}
						// System.out.println("Converted to " + (bufferStream.size() - orig) + " bytes");
					}
				}
			}
		} finally {
			reader.close();
		}
	}
	public void doit(String fileName, String aliasName, String keytype, char[] passPhrase) throws Exception {
		KeyStore keystore = KeyStore.getInstance("JKS");
		File certificateFile = new File(fileName);
		keystore.load(new FileInputStream(certificateFile), passPhrase);
		String outputType;
		byte[] encoded;
		if(keytype.trim().equalsIgnoreCase("private")) {
			outputType = "PRIVATE KEY";
			Key key = keystore.getKey(aliasName, passPhrase);
			if(!(key instanceof PrivateKey)) {
				System.err.println("Alias '" + aliasName + "' is not a private key");
				System.exit(2);
				return;
			}
			encoded = key.getEncoded();
			printCert(outputType, encoded);
		} else if(keytype.trim().equalsIgnoreCase("public")) {
			outputType = "CERTIFICATE";
			Certificate cert = keystore.getCertificate(aliasName);
			encoded = cert.getEncoded();
			printCert(outputType, encoded);
		} else if(keytype.trim().equalsIgnoreCase("public-chain")) {
			outputType = "CERTIFICATE";
			Certificate[] certs = keystore.getCertificateChain(aliasName);
			for(Certificate cert : certs) {
				printCert(outputType, cert.getEncoded());
			}
		} else if(keytype.trim().equalsIgnoreCase("import")) {
			File file = new File(fileName);
			DataInputStream in = new DataInputStream(new FileInputStream(file));
			byte[] bytes = new byte[(int)file.length()];
			in.readFully(bytes);
			int fs = 0;
			int fe = 0;
			RSAPublicKey pubKey = null;
			RSAPrivateKey privKey = null;
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            while(pubKey == null || privKey == null) {
				fs = findEnd(bytes, "---BEGIN ".getBytes(), fe);
				String canonicalFile = file.getCanonicalPath();
				if(fs <= 0) {
					System.err.println("Invalid keys file '" + canonicalFile + "': '---BEGIN ' not found");
					System.exit(5);
					return;
				}
				byte[] dash = "-".getBytes();
				fe = findStart(bytes, dash, fs);
				if(fe <= 0) {
					System.err.println("Invalid keys file '" + canonicalFile + "': '-' after '---BEGIN ' not found");
					System.exit(5);
					return;
				}
				outputType = new String(bytes, fs, fe - fs).trim().toUpperCase();
				while(bytes[++fe] == dash[0]) ;
				fs = fe;
				fe = findStart(bytes, dash, fe);
				if(fe <= fs) {
					System.err.println("Invalid keys file '" + canonicalFile + "': end '-' after header not found");
					System.exit(5);
					return;
				}
				encoded = new byte[fe-fs];
				System.arraycopy(bytes, fs, encoded, 0, encoded.length);
				fs = findEnd(bytes, "---END ".getBytes(), fe);
				if(fs <= 0) {
					System.err.println("Invalid keys file '" + canonicalFile + "': '---END ' not found");
					System.exit(5);
					return;
				}
				String confirmOutputType = new String(bytes, fs, fe - fs).trim().toUpperCase();
				while(bytes[++fe] == dash[0]) ;
				fs = fe;
				if("PRIVATE KEY".equals(outputType)) {
					privKey = (RSAPrivateKey) keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encoded));
					System.out.print("Found private key: modulus=");
					System.out.println(privKey.getModulus());
				} else if("CERTIFICATE".equals(outputType)) {
					pubKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(encoded));
					System.out.print("Found public key: modulus=");
					System.out.println(pubKey.getModulus());
				}
			}
			
			//keystore.setKeyEntry(aliasName, privKey, null, pubKey.);
			
		} else {
			System.err.println("Key type '" + keytype + "' is not supported");
			System.exit(2);
			return;
		}

	}

	protected static int findEnd(byte[] in, byte[] pattern, int start) {
		int p = 0;
		for(int i = start; i < in.length; i++) {
			if(in[i] == pattern[p]) {
				p++;
				if(p >= pattern.length)
					return i+1;
			} else {
				i -= p;
				p = 0;
			}			
		}
		return -1;
	}
	protected int findStart(byte[] in, byte[] pattern, int start) {
		int r = findEnd(in, pattern, start);
		if(r > 0)
			return r - pattern.length;
		return -1;
	}
	protected void printCert(String outputType, byte[] encoded) {
		String b64 = Base64.encodeBytes(encoded, true);
		System.out.print("-----BEGIN ");
		System.out.print(outputType);
		System.out.println("-----");
		System.out.println(b64);
		System.out.print("-----END ");
		System.out.print(outputType);
		System.out.println("-----");
	}
}
