/**
 *
 */
package simple.tools;
/*
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.eclipse.core.internal.localstore.SafeChunkyInputStream;
import org.eclipse.core.internal.localstore.SafeChunkyOutputStream;
*/
/**
 * @author Brian S. Krug
 *
 */
public class UpdateEclipseProjectLocations {
	/**
	 * @param args
	 * @throws IOException
	 */
/*
	public static void main(String[] args) throws IOException {
		String workspacePath = args[0];
		Pattern currentBaseRegex = Pattern.compile(args[1]);
		String newBaseReplacement = args[2];

		File workspaceDir = new File(workspacePath);
		File projectsDir = new File(workspaceDir, ".metadata/.plugins/org.eclipse.core.resources/.projects");
		if(projectsDir.exists())
			System.out.println("Looking in '" + projectsDir.getAbsolutePath() + "' for projects");
		else {
			System.out.println("ERROR: Folder '" + projectsDir.getAbsolutePath() + "' does not exist");
			return;
		}
		for(File projectDir : projectsDir.listFiles()) {
			System.out.println("Updating project '" + projectDir.getName() + "'");
			File file = new File(projectDir, ".location");
			if(!file.exists()) {
				System.out.println("No .location file found for '" + projectDir.getName() + "'");
				continue;
			}
			InputStream in = new SafeChunkyInputStream(file);
			ByteArrayOutputStream copy = new ByteArrayOutputStream((int)file.length());
			byte[] buffer = new byte[128];
			int r;
			while((r=in.read(buffer)) >= 0) {
				copy.write(buffer, 0, r);
			}
			copy.flush();
			in.close();
			System.out.println("Read " + copy.size() + " bytes from project file for '" + projectDir.getName() + "'");
			ByteArrayInputStream back = new ByteArrayInputStream(copy.toByteArray());
			DataInputStream dis = new DataInputStream(back);
			String location = dis.readUTF();
			int numRefs = dis.readInt();
			String[] refs = new String[numRefs];
			String[] newRefs = new String[numRefs];
			for(int i = 0; i < numRefs; i++) {
				refs[i] = dis.readUTF();
				newRefs[i] = currentBaseRegex.matcher(refs[i]).replaceAll(newBaseReplacement);
			}
			String newLocation = currentBaseRegex.matcher(location).replaceAll(newBaseReplacement);
			if(!location.equals(newLocation) || !Arrays.deepEquals(refs, newRefs)) {
				System.out.print("Replacing location '" + location + "' [refs=");
				for(int i = 0; i < refs.length; i++) {
					if(i > 0)
						System.out.print(",");
					System.out.print(refs[i]);
				}
				System.out.print("] with '" + newLocation + "' [refs=");
				for(int i = 0; i < newRefs.length; i++) {
					if(i > 0)
						System.out.print(",");
					System.out.print(newRefs[i]);
				}
				System.out.print("] for '" + projectDir.getName() + "'");
				File original = new File(file.getAbsolutePath());
				File old = new File(file.getAbsolutePath() + ".old");
				old.delete();
				file.renameTo(old);
				if(original.exists()) {
					System.out.println("ERROR: Attempted to rename '" + original.getAbsolutePath() + "' to '" + old.getAbsolutePath() + "' but it did NOT work for '" + projectDir.getName() + "'");
					continue;
				}
				SafeChunkyOutputStream out = new SafeChunkyOutputStream(original);
				DataOutputStream dos = new DataOutputStream(out);
				dos.writeUTF(newLocation);
				dos.writeInt(newRefs.length);
				for(String s : newRefs)
					dos.writeUTF(s);
				dos.flush();
				while((r=dis.read(buffer)) >= 0) {
					out.write(buffer, 0, r);
				}
				out.succeed();
				out.flush();
				System.out.println("Wrote new location file for '" + projectDir.getName() + "'");
			} else {
				System.out.println("Not replacing location '" + location + "' for '" + projectDir.getName() + "'");
			}
		}
	}
*/
}
