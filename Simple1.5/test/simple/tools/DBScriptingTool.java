package simple.tools;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import simple.app.Base;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.BasicDataSourceFactory;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceNotFoundException;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.IOUtils;
import simple.io.InOutInteraction;
import simple.io.Interaction;
import simple.io.LoadingException;
import simple.io.Log;
import simple.io.TrimTrailingWhitespaceWriter;
import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;
import simple.results.Results;
import simple.text.StringUtils;

public class DBScriptingTool extends Base {
	private static final Log log = Log.getLog();
	public static enum DbObjectType {
		VIEW("vws"), PACKAGE_SPEC("PACKAGE", "psk"), PACKAGE_BODY("pbk"), TRIGGER("trg"), CONSTRAINT("con"), FUNCTION("fnc"), PROCEDURE("prc") ;
		private static final EnumStringValueLookup<DbObjectType> lookup = new EnumStringValueLookup<DbObjectType>(DbObjectType.class);
		private final String dbTypeName;
		private final String extension;
		private final String label;
		private DbObjectType(String extension) {
			this.dbTypeName = name().replace('_', ' ');
			this.extension = extension;
			this.label = makeLabel();
		}
		private DbObjectType(String dbTypeName, String extension) {
			this.dbTypeName = dbTypeName;
			this.extension = extension;
			this.label = makeLabel();
		}
		public String getDbTypeName() {
			return dbTypeName;
		}
		public String getExtension() {
			return extension;
		}

		public String getDdlName() {
			return name();
		}
		protected String makeLabel() {
			StringBuilder sb = new StringBuilder();
			String nm = name();
			boolean capNext = true;
			for(int i = 0; i < nm.length(); i++) {
				char ch = nm.charAt(i);
				switch(ch) {
					case '_': sb.append(' '); capNext = true; break;
					default:
						if(capNext) {
							ch = Character.toUpperCase(ch);
							capNext = false;
						} else {
							ch = Character.toLowerCase(ch);
						}
						sb.append(ch);
				}
			}
			
			return sb.toString();
		}
		public String getLabel() {
			return label;
		}

		public String getValue() {
			return getDbTypeName();
		}

		public static DbObjectType getByValue(String value) throws InvalidValueException {
			return lookup.getByValue(value);
		}
	}
	protected static interface ExecutionInfo {
		public String getUrl();

		public void setUrl(String url);

		public String getDriver();

		public void setDriver(String driver);

		public String getUsername();

		public void setUsername(String username);

		public String getDir();

		public void setDir(String dir);

		public String[] getSchemas();

		public void setSchemas(String[] schemas);

		public DbObjectType[] getTypes();

		public void setTypes(DbObjectType[] types);

		public String getPassword();

		public void setPassword(String password);

		public String[] getObjects();

		public void setObjects(String[] objects);

		public Connection connect() throws DataLayerException, SQLException;
		
		public boolean populate() ;
	}
	protected final static String dsn = "DEFAULT";
	protected final static BasicDataSourceFactory dsf = new BasicDataSourceFactory();
	static {
		DataLayerMgr.setDataSourceFactory(dsf);
	}
	protected static abstract class AbstractExecutionInfo implements ExecutionInfo {
		public Connection connect() throws DataLayerException, SQLException {
			dsf.addDataSource(dsn, getDriver(), getUrl(), getUsername(), getPassword());
			try {
				ConfigLoader.loadConfig("simple/tools/db-script-data-layer.xml");
			} catch(ConfigException e) {
				throw new DataLayerException(e);
			} catch(IOException e) {
				throw new DataLayerException(e);
			} catch(LoadingException e) {
				throw new DataLayerException(e);
			}
			return dsf.getDataSource(dsn).getConnection();
		}
	}
	protected static class BeanExecutionInfo extends AbstractExecutionInfo {
		protected String url;
		protected String driver;
		protected String username;
		protected String dir;
		protected String[] schemas;
		protected String[] objects;
		protected DbObjectType[] types;
		protected String password;
		
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#getUrl()
		 */
		public String getUrl() {
			return url;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#setUrl(java.lang.String)
		 */
		public void setUrl(String url) {
			this.url = url;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#getDriver()
		 */
		public String getDriver() {
			return driver;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#setDriver(java.lang.String)
		 */
		public void setDriver(String driver) {
			this.driver = driver;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#getUsername()
		 */
		public String getUsername() {
			return username;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#setUsername(java.lang.String)
		 */
		public void setUsername(String username) {
			this.username = username;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#getDir()
		 */
		public String getDir() {
			return dir;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#setDir(java.lang.String)
		 */
		public void setDir(String dir) {
			this.dir = dir;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#getSchemas()
		 */
		public String[] getSchemas() {
			return schemas;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#setSchemas(java.lang.String[])
		 */
		public void setSchemas(String[] schemas) {
			this.schemas = schemas;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#getTypes()
		 */
		public DbObjectType[] getTypes() {
			return types;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#setTypes(simple.tools.DBScriptingTool.DbObjectType[])
		 */
		public void setTypes(DbObjectType[] types) {
			this.types = types;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#getPassword()
		 */
		public String getPassword() {
			return password;
		}
		/* (non-Javadoc)
		 * @see simple.tools.ExecutionInfo#setPassword(java.lang.String)
		 */
		public void setPassword(String password) {
			this.password = password;
		}
		public boolean populate() {
			Interaction interact = new InOutInteraction();
			char[] pwd =  interact.readPassword("Please enter the database password for %1$s:", getUsername());
			if(pwd == null)
				return false;
			setPassword(new String(pwd));
			if(getObjects() == null || getObjects().length == 0) {
				String objects = interact.readLine("Please enter a list of objects:");
				if(!StringUtils.isBlank(objects))
					setObjects(StringUtils.split(objects, StringUtils.STANDARD_DELIMS, false));
				else {
					if(getSchemas() == null || getSchemas().length == 0) {
						String schema = interact.readLine("Please choose a schema:");
						if(schema == null)
							return false;
						setSchemas(new String[] { schema });
					}
					if(getTypes() == null || getTypes().length == 0) {
						String type = interact.readLine("Please choose an object type:");
						DbObjectType dbType;
						try {
							dbType = ConvertUtils.convert(DbObjectType.class, type);
						} catch(ConvertException e) {
							interact.printf("Could not convert '%1$s' to a DbObjectType: $2$s", type, e.getMessage());
							return false;
						}
						if(dbType == null)
							return false;
						setTypes(new DbObjectType[] { dbType });
					}
				}
			}
			return true;
		}

		public String[] getObjects() {
			return objects;
		}

		public void setObjects(String[] objects) {
			this.objects = objects;
		}
	}
	protected static class GuiExecutionInfo extends AbstractExecutionInfo {
		protected JDialog dialog;
		protected JPanel mainPanel;
		protected JPanel connectPanel;
		protected JTextField driverField;
		protected JTextField urlField;
		protected JTextField usernameField;
		protected JPasswordField passwordField;
		protected JButton connectButton;
		protected JCheckBox[] schemaCheckboxes;
		protected JPanel schemasPanel;
		protected JPanel schemaListPanel;
		protected JCheckBox[] typeCheckboxes;
		protected JPanel typesPanel;
		protected JTextField dirField;
		protected JFileChooser dirChooser;
		protected JButton scriptButton;
		protected ActionListener connectListener;
		protected ActionListener scriptListener;
		protected JToggleButton selectAllSchemasButton;
		protected boolean proceed;
		
		public GuiExecutionInfo() {
		}
		//gui components
		protected JCheckBox makeCheckbox(JPanel parent, String key, String label) {
			JCheckBox checkbox = new JCheckBox(label);
			checkbox.setActionCommand(key);
			parent.add(checkbox);
			return checkbox;
		}
		protected void addField(JPanel panel, String label, JComponent comp, ActionListener actionListener) {
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 1;
			panel.add(new JLabel(label), gbc);
			gbc.gridx = 2;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1.0;
			panel.add(comp, gbc);	
			if(actionListener != null && comp instanceof JTextField)
				((JTextField)comp).addActionListener(actionListener);
		}
		
		protected JDialog getDialog() {
			if(dialog == null) {
				dialog = new JDialog();
				dialog.setTitle("DB Scripting Tool");
				dialog.setModalityType(ModalityType.APPLICATION_MODAL);
				dialog.setSize(500, 800);
				dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				dialog.getContentPane().add(getMainPanel());	
			}
			return dialog;
		}
		protected JPanel getMainPanel() {
			if(mainPanel == null) {
				mainPanel = new JPanel();
				mainPanel.setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.weightx = 1.0;
				gbc.gridwidth = 2;
				mainPanel.add(getConnectPanel(), gbc);
				gbc.weighty = 1.0;
				gbc.fill = GridBagConstraints.BOTH;
				mainPanel.add(getSchemasPanel(), gbc);	
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.weighty = 0.0;
				mainPanel.add(getTypesPanel(), gbc);		
				addField(mainPanel, "Output Directory:", getDirField(), null);
				gbc.fill = GridBagConstraints.NONE;
				mainPanel.add(getScriptButton(), gbc);		
			}
			return mainPanel;
		}
		protected JPanel getConnectPanel() {
			if(connectPanel == null) {
				connectPanel = new JPanel();
				connectPanel.setBorder(new TitledBorder("Database Connection"));
				connectPanel.setLayout(new GridBagLayout());
				addField(connectPanel, "Driver:", getDriverField(), getConnectListener());
				addField(connectPanel, "Url:", getUrlField(), getConnectListener());
				addField(connectPanel, "Username:", getUsernameField(), getConnectListener());
				addField(connectPanel, "Password:", getPasswordField(), getConnectListener());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 1;
				gbc.gridwidth = 2;
				gbc.fill = GridBagConstraints.NONE;
				gbc.weightx = 1.0;
				connectPanel.add(getConnectButton(), gbc);
			}
			return connectPanel;
		}
		protected JTextField getDriverField() {
			if(driverField == null) {
				driverField = new JTextField();
			}
			return driverField;
		}
		protected JTextField getUrlField() {
			if(urlField == null) {
				urlField = new JTextField();
			}
			return urlField;
		}
		protected JTextField getUsernameField() {
			if(usernameField == null) {
				usernameField = new JTextField();
			}
			return usernameField;
		}
		protected JPasswordField getPasswordField() {
			if(passwordField == null) {
				passwordField = new JPasswordField();
			}
			return passwordField;
		}
		protected JButton getConnectButton() {
			if(connectButton == null) {
				connectButton = new JButton("Connect");
				connectButton.addActionListener(getConnectListener());
			}
			return connectButton;
		}
		
		protected JPanel getSchemasPanel() {
			if(schemasPanel == null) {
				schemasPanel = new JPanel();
				schemasPanel.setBorder(new TitledBorder("Schemas"));
				schemasPanel.setLayout(new BorderLayout());
				schemasPanel.add(getSelectAllSchemasButton(), BorderLayout.EAST);
				JScrollPane scrollPane = new JScrollPane(getSchemaListPanel());
				schemasPanel.add(scrollPane, BorderLayout.CENTER);
			}
			return schemasPanel;
		}
		protected JPanel getSchemaListPanel() {
			if(schemaListPanel == null) {
				schemaListPanel = new JPanel();
				schemaListPanel.setLayout(new GridLayout(0, 1));
			}
			return schemaListPanel;
		}
		protected JPanel getTypesPanel() {
			if(typesPanel == null) {
				typesPanel = new JPanel();
				typesPanel.setBorder(new TitledBorder("Object Types"));
				typesPanel.setLayout(new GridLayout(0, 1));
				DbObjectType[] types = DbObjectType.values();
				Arrays.sort(types, new Comparator<DbObjectType>() {
					public int compare(DbObjectType o1, DbObjectType o2) {
						return o1.toString().compareToIgnoreCase(o2.toString());
					}
				});
				typeCheckboxes = new JCheckBox[types.length];
				for(int i = 0; i < types.length; i++) {
					typeCheckboxes[i] = makeCheckbox(typesPanel, types[i].toString(), types[i].getLabel());
				}
			}
			return typesPanel;
		}
		protected JTextField getDirField() {
			if(dirField == null) {
				dirField = new JTextField();
				dirField.setEditable(false);
				dirField.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						showDirChooser();
					}
				});
				dirField.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						showDirChooser();
					}
				});
			}
			return dirField;
		}
		protected void showDirChooser() {
			JFileChooser fc = getDirChooser();
			int ret = fc.showOpenDialog(dirField);
			switch(ret) {
				case JFileChooser.APPROVE_OPTION:
					dirField.setText(fc.getSelectedFile().getAbsolutePath());
					break;
			}
		}
		protected JFileChooser getDirChooser() {
			if(dirChooser == null) {
				dirChooser = new JFileChooser();
				dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			}
			return dirChooser;
		}
		protected JButton getScriptButton() {
			if(scriptButton == null) {
				scriptButton = new JButton("Generate Script(s)");
				scriptButton.addActionListener(getScriptListener());
			}
			return scriptButton;
		}
		protected String[] getCheckedButtons(JCheckBox[] checkboxes) {
			if(checkboxes == null || checkboxes.length == 0)
				return new String[0];
			List<String> checked = new ArrayList<String>();
			for(JCheckBox cb : checkboxes) {
				if(cb.isSelected())
					checked.add(cb.getActionCommand());
			}
			return checked.toArray(new String[checked.size()]);
		}
		protected void setCheckedButtons(JCheckBox[] checkboxes, String[] checked) {
			if(checkboxes == null || checkboxes.length == 0)
				return;
			Set<String> checkedSet;
			if(checked == null || checked.length == 0)
				checkedSet = Collections.emptySet();
			else
				checkedSet = new HashSet<String>(Arrays.asList(checked));
			for(JCheckBox cb : checkboxes) {
				cb.setSelected(checkedSet.contains(cb.getActionCommand()));
			}
		}
		protected ActionListener getConnectListener() {
			if(connectListener == null) {
				connectListener = new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						Connection conn;
						try {
							conn = connect();
						} catch(DataLayerException e) {
							log.warn("Could not connect", e);
							JOptionPane.showMessageDialog(dialog, e.getMessage(), "Connection Error", JOptionPane.ERROR_MESSAGE);
							return;
						} catch(SQLException e) {
							log.warn("Could not connect", e);
							JOptionPane.showMessageDialog(dialog, e.getMessage(), "Connection Error", JOptionPane.ERROR_MESSAGE);
							return;
						}
						try {
							// get schemas with objects
							Map<String,String> schemaMap = getSchemaOptions(conn);
							JPanel panel = getSchemaListPanel();
							if(schemaCheckboxes != null)
								for(JCheckBox cb : schemaCheckboxes)
									schemasPanel.remove(cb);
							schemaCheckboxes = new JCheckBox[schemaMap.size()];
							int i = 0;
							for(Map.Entry<String, String> entry : schemaMap.entrySet()) {
								schemaCheckboxes[i++] = makeCheckbox(panel, entry.getKey(), entry.getValue());
							}
						} catch(SQLException e) {
							log.warn("Could not get schema options", e);
							JOptionPane.showMessageDialog(dialog, e.getMessage(), "SQL Error", JOptionPane.ERROR_MESSAGE);
							return;
						} catch(DataLayerException e) {
							log.warn("Could not get schema options", e);
							JOptionPane.showMessageDialog(dialog, e.getMessage(), "SQL Error", JOptionPane.ERROR_MESSAGE);
							return;
						} catch(ConvertException e) {
							log.warn("Could not get schema options", e);
							JOptionPane.showMessageDialog(dialog, e.getMessage(), "SQL Error", JOptionPane.ERROR_MESSAGE);
							return;
						} finally {
							try {
								conn.close();
							} catch(SQLException e) {
								log.warn("Could not close connection", e);
							}
						}
					}
				};
			}
			return connectListener;
		}
		protected ActionListener getScriptListener() {
			if(scriptListener == null) {
				scriptListener = new ActionListener() {				
					public void actionPerformed(ActionEvent e) {
						proceed = true;
						getDialog().dispose();
					}
				};
			}
			return scriptListener;
		}
		protected JToggleButton getSelectAllSchemasButton() {
			if(selectAllSchemasButton == null) {
				selectAllSchemasButton = new JToggleButton("Select All");
				selectAllSchemasButton.addItemListener(new ItemListener() {				
					public void itemStateChanged(ItemEvent e) {
						if(selectAllSchemasButton.isSelected()) {
							if(schemaCheckboxes != null)
								for(JCheckBox cb : schemaCheckboxes)
									cb.setSelected(true);
							selectAllSchemasButton.setText("Deselect All");
						} else {
							if(schemaCheckboxes != null)
								for(JCheckBox cb : schemaCheckboxes)
									cb.setSelected(false);
							selectAllSchemasButton.setText("Select All");
						}
					}
				});
			}
			return selectAllSchemasButton;
		}
		// normal interface properties
		public String getDir() {
			return getDirField().getText();
		}

		public String getDriver() {
			return getDriverField().getText();
		}

		public String getPassword() {
			return new String(getPasswordField().getPassword());
		}

		public String[] getSchemas() {
			return getCheckedButtons(schemaCheckboxes);
		}

		public DbObjectType[] getTypes() {
			String[] typeNames = getCheckedButtons(typeCheckboxes);
			return ConvertUtils.convertSafely(DbObjectType[].class, typeNames, null);
		}

		public String getUrl() {
			return getUrlField().getText();
		}

		public String getUsername() {
			return getUsernameField().getText();
		}

		public void setDir(String dir) {
			getDirField().setText(dir);
		}

		public void setDriver(String driver) {
			getDriverField().setText(driver);
		}

		public void setPassword(String password) {
			getPasswordField().setText(password);
		}

		public void setSchemas(String[] schemas) {
			setCheckedButtons(schemaCheckboxes, schemas);
		}

		public void setTypes(DbObjectType[] types) {
			setCheckedButtons(typeCheckboxes, ConvertUtils.convertSafely(String[].class, types, null));
		}

		public void setUrl(String url) {
			getUrlField().setText(url);
		}

		public void setUsername(String username) {
			getUsernameField().setText(username);
			
		}
		public boolean populate() {
			proceed = false;
			getDialog().setVisible(true);
			return proceed;
		}

		@Override
		public String[] getObjects() {
			return null;
		}

		@Override
		public void setObjects(String[] objects) {

		}
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new DBScriptingTool().run(args);
	}

	public void run(String[] args) {
		printInvocation(args);
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}

		execute(argMap, null);

		Log.finish();
		System.exit(0);
	}
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('u', "username", false, true, "database-username", "The database username");
		registerCommandLineSwitch('r', "driver", false, true, "database-driver", "The database driver");
		registerCommandLineSwitch('c', "connect", false, true, "database-url", "The database connect string");
		registerCommandLineSwitch('s', "schemas", true, true, "schemas", "The database schema(s) to script");
		registerCommandLineSwitch('t', "types", true, true, "types", "The database type(s) to script");
		registerCommandLineSwitch('o', "objects", true, true, "objects", "The database object(s) to script");
		registerCommandLineSwitch('l', "nongraphical", true, false, "nongraphical", "Whether to show gui or not");
		registerCommandLineArgument(false, "base-dir", "The base directory into which script files are written");		
	}
	@Override
	protected void execute(Map<String, Object> argMap, Properties properties) {
		try {
			boolean nongraphical = ConvertUtils.getBoolean(argMap.get("nongraphical"), false);
			ExecutionInfo info = nongraphical ? new BeanExecutionInfo() : new GuiExecutionInfo();
			info.setUrl((String) argMap.get("database-url"));
			info.setDriver((String) argMap.get("database-driver"));
			info.setUsername((String) argMap.get("database-username"));
			info.setDir((String) argMap.get("base-dir"));

			info.setObjects(StringUtils.split((String) argMap.get("objects"), StringUtils.STANDARD_DELIMS, false));
			info.setSchemas(StringUtils.split((String) argMap.get("schemas"), StringUtils.STANDARD_DELIMS, false));
			info.setTypes(ConvertUtils.convert(DbObjectType[].class, StringUtils.split((String) argMap.get("types"), StringUtils.STANDARD_DELIMS, false)));
			
			if(!info.populate())
				return;
			File dir = new File(info.getDir());
			Connection conn = info.connect();
			try {
				if(info.getObjects() != null && info.getObjects().length > 0)
					scriptObjects(dir, conn, info.getObjects());
				if(info.getSchemas() != null && info.getSchemas().length > 0 && info.getTypes() != null && info.getTypes().length > 0)
					scriptObjects(dir, conn, info.getSchemas(), info.getTypes());
			} finally {
				conn.close();
			}
		} catch(DataSourceNotFoundException e) {
			log.error("Could not get connection", e);
		} catch(DataLayerException e) {
			log.error("Could not script database", e);
		} catch(ConvertException e) {
			log.error("Could not script databasee", e);
		} catch(SQLException e) {
			log.error("Could not use connection", e);
		} catch(IOException e) {
			log.error("Could not load data layer file", e);
		}
	}
	
	protected static Map<String,String> getSchemaOptions(Connection conn) throws SQLException, DataLayerException, ConvertException {
		DbObjectType[] types = DbObjectType.values();
		String[] dbTypeNames = new String[types.length];
		for(int i = 0; i < types.length; i++) {
			dbTypeNames[i] = types[i].getDbTypeName();
		}
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("dbTypeNames", dbTypeNames);
		Results results = DataLayerMgr.executeQuery(conn, "GET_DB_SCHEMAS", params);
		Map<String,String> output = new LinkedHashMap<String, String>();
		while(results.next()) {
			String schema = results.getValue("schemaName", String.class);
			int count = results.getValue("objectCount", Integer.class);
			output.put(schema, schema + " (" + count + " objects)");
		}
		return output;
	}

	protected void scriptObjects(File dir, Connection conn, String[] schemas, DbObjectType[] types) throws SQLException, DataLayerException, ConvertException, IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		for(String schema : schemas) {
			params.put("schemaName", schema.toUpperCase());
			for(DbObjectType type : types) {
				params.put("objectType", type);
				Results results = DataLayerMgr.executeQuery(conn, "GET_DB_SOURCE", params);
				int cnt = 0;
				while(results.next()) {
					String schemaName = results.getValue("schemaName", String.class);
					String objectName = results.getValue("objectName", String.class);
					Object script = results.getValue("script");
					scriptObject(dir, type, schemaName, objectName, script);
					cnt++;
				}
				log.info("Scripted " + cnt + " objects of type " + type + " to '" + dir.getAbsolutePath() + '/' + schema + '\'');
			}
		}
	}

	protected void scriptObjects(File dir, Connection conn, String[] schemaAndObjectName) throws SQLException, DataLayerException, ConvertException, IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("objectNames", schemaAndObjectName);
		Results results = DataLayerMgr.executeQuery(conn, "GET_DB_SOURCE_FROM_LIST", params);
		int cnt = 0;
		while(results.next()) {
			String schemaName = results.getValue("schemaName", String.class);
			String objectName = results.getValue("objectName", String.class);
			DbObjectType type = results.getValue("objectType", DbObjectType.class);
			Object script = results.getValue("script");
			scriptObject(dir, type, schemaName, objectName, script);
			cnt++;
		}
		log.info("Scripted " + cnt + " various objects to '" + dir.getAbsolutePath() + '\'');
	}

	protected void scriptObject(File dir, DbObjectType type, String schemaName, String objectName, Object script) throws SQLException, ConvertException, IOException {
		Reader reader;
		if(script instanceof Clob) {
			reader = ((Clob) script).getCharacterStream();
		} else {
			reader = new StringReader(ConvertUtils.getString(script, true));
		}
		File file = new File(dir, schemaName + '/' + objectName + '.' + type.getExtension());
		log.debug("Scripting '" + schemaName + '.' + objectName + "' to '" + file.getAbsolutePath() + '\'');
		file.getParentFile().mkdirs();
		Writer writer = new FileWriter(file);
		BufferedReader buf;
		if(reader instanceof BufferedReader) {
			buf = (BufferedReader) reader;
		} else {
			buf = new BufferedReader(reader);
		}
		int r;
		boolean done = false;
		boolean start = false;
		while(!done && (r = buf.read()) >= 0) {
			char ch = (char) r;
			if(start) {
				switch(ch) {
					case '"':
						break; // ignore
					case ')':
						if(type == DbObjectType.VIEW) {
							done = true;
						}
						writer.write(r);
						break;
					case '\n':
					case '\r':
						if(type != DbObjectType.VIEW) {
							done = true;
						}
						// fall-thru
					default:
						writer.write(r);
				}
			} else if(!Character.isWhitespace(ch)) {
				start = true;
				writer.write(r);
			} // trim leading whitespace
		}
		
		IOUtils.copy(buf, new TrimTrailingWhitespaceWriter(writer));
		writer.write("\n/\n");
		writer.flush();
		writer.close();
	}
}
