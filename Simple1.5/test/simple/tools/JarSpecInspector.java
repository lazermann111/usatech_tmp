package simple.tools;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class JarSpecInspector {
	public static void main(String[] args) throws Exception {
		if(args == null || args.length < 1)
			throw new IOException("This program requires one argument: the jar file to inspect");
		File file = new File(args[0]);
		JarFile jarfile = new JarFile(file);
		Manifest manifest = jarfile.getManifest();
		Attributes atts = manifest.getMainAttributes();
		System.out.println("Inspecting '" + file.getAbsolutePath() + "'");
		System.out.println("From Manifest ------------------------------------------");
		System.out.println("Specification-Version: " + atts.getValue("Specification-Version"));
		System.out.println("Implementation-Version: " + atts.getValue("Implementation-Version"));
		if(args.length > 1) {
			System.out.println();
			URLClassLoader ucl = new URLClassLoader(new URL[] { file.toURI().toURL() }, JarSpecInspector.class.getClassLoader());
			Class<?> clazz = ucl.loadClass(args[1]);
			Package pack = clazz.getPackage();
			System.out.println("From Package ------------------------------------------");
			System.out.println("Specification-Version: " + pack.getSpecificationVersion());
			System.out.println("Implementation-Version: " + pack.getImplementationVersion());
		}
	}
}
