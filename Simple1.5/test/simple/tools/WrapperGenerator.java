/*
 * WrapperGenerator.java
 *
 * Created on December 10, 2001, 9:26 AM
 */

package simple.tools;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class WrapperGenerator {
    protected final static String newline = "\r\n";
    protected static boolean doSuper = true;
    
    /** Creates new WrapperGenerator */
    public WrapperGenerator() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        doSuper = true;
        String packageName = "simple.db";
        String folderPath = "H:\\SAF\\src\\simple\\db\\";
        for(int i = 0; i < args.length; i++) {
            try {
                String s = makeWrapper(args[i],packageName);
                java.io.PrintStream out = new java.io.PrintStream(new java.io.FileOutputStream(folderPath + getShortName(args[i]) + "Wrapper.java"));
                out.println(s);
                out.flush();
                System.out.println("Create of Wrapper for " + args[i] + " succeeded");
            } catch(Exception e) {
                System.out.println("Create of Wrapper for " + args[i] + " failed: ");
                e.printStackTrace();
            }
        }
        System.out.println("Done -------------------------------------------");
    }
    
    public static String makeWrapper(String originalClassName, String packageName) throws ClassNotFoundException {
        Class clazz = Class.forName(originalClassName);
        String shortClassName =  getShortName(originalClassName);
        String newClassName = shortClassName + "Wrapper";
        String varName = decapitalize(shortClassName);
        String delMethodName = " get" + "Delegate"/*shortClassName*/;
        java.text.DateFormat df = new java.text.SimpleDateFormat("MMMM dd, yyyy, h:mm a");
        String header = "/*" + newline + " * " + newClassName + ".java" + newline + " *" + newline + " * Created on " + df.format(new java.util.Date())  + newline + " */"
        + newline + newline + "package " + packageName + ";" + newline + newline + "/**" + newline + " *" + newline + " * @author  Brian S. Krug"
        + newline + " */" + newline + "import " + originalClassName + ";" + newline + newline + "public abstract class " + newClassName + (clazz.isInterface() ? " implements " : " extends ") + shortClassName + " {" + newline
        + "protected " + shortClassName + " " + varName + ";" + newline
        + newline + "public " + newClassName + "(" + shortClassName + " delegate) {"	+ newline + "this." + varName + " = delegate;" + newline + "}" + newline + newline
        + "protected " + shortClassName + " " + delMethodName + "() {" + newline + "\treturn " + varName + ";" + newline + "}" + newline;
        String footer = newline + "}" + newline;
        return header + makeWrapperMethods(clazz,  " " + delMethodName) + footer;
    }
    
    public static String makeWrapperMethods(Class clazz, String delMethodName) {
        StringBuffer sb = new StringBuffer();
        //use a treeset to ensure method name & parameter uniqueness and alpha ordering
        java.util.Set set = new java.util.TreeSet(new java.util.Comparator() {
            public int compare(Object o1, Object o2) {
                Method m1 = (Method) o1;
                Method m2 = (Method) o2;
                int i = m1.getName().compareTo(m2.getName());
                if(i != 0) return i;
                Class[] p1 = m1.getParameterTypes();
                Class[] p2 = m2.getParameterTypes();
                i = p1.length - p2.length;
                if(i != 0) return i;
                for(int k = 0; k < p1.length; k++) {
                    i = p1[k].getName().compareTo(p2[k].getName());
                    if(i != 0) return i;
                }
                return 0;
            }
        });
        set.addAll(Arrays.asList(clazz.getMethods()));
        Method[] m = (Method[])set.toArray(new Method[set.size()]);
        
                /*if(doSuper && clazz.isInterface()) {
                        Class[] interfaces = clazz.getInterfaces();
                        if(interfaces.length > 0) {
                                java.util.List l = new java.util.ArrayList();
                                l.addAll(java.util.Arrays.asList(m));
                                for(int i =0; i < interfaces.length; i++) {
                                        Method[] m1 = interfaces[i].getMethods();
                                        for(int k = 0; k < m1.length; k++) {
                                                try {
                                                        clazz.getMethod(m1[k].getName(), m1[k].getParameterTypes());
                                                } catch(NoSuchMethodException nsme) {
                                                        l.add(m1[k]);
                                                }
                                        }
                                }
                                m = (Method[]) l.toArray(m);
                        }
                }*/
        StringBuffer vars = new StringBuffer();
        for(int i = 0; i < m.length; i++) {
            if(m[i].getDeclaringClass() != Object.class && (m[i].getDeclaringClass() == clazz || doSuper) &&
            /*(m[i].getModifiers() & Modifier.FINAL) !=  Modifier.FINAL&& */ (m[i].getModifiers() & Modifier.STATIC) !=  Modifier.STATIC) {
                sb.append(newline);
                sb.append("public ");
                sb.append(getSourceName(m[i].getReturnType()));
                sb.append(" ");
                sb.append(m[i].getName());
                sb.append("(");
                Class[] paramTypes = m[i].getParameterTypes();
                vars.setLength(0);
                for(int k = 0; k < paramTypes.length; k++) {
                    if(k > 0) {
                        sb.append(", ");
                        vars.append(", ");
                    }
                    sb.append(getSourceName(paramTypes[k]));
                    sb.append(" ");
                    String varName = getVarName(paramTypes[k], k);
                    sb.append(varName);
                    vars.append(varName);
                }
                sb.append(")");
                Class[] exceptTypes = m[i].getExceptionTypes();
                for(int k = 0; k < exceptTypes.length; k++) {
                    if(k > 0) sb.append(", ");
                    else sb.append(" throws ");
                    sb.append(exceptTypes[k].getName());
                }
                sb.append(" {");
                sb.append(newline);
                sb.append("\t");
                if(m[i].getReturnType() != Void.TYPE && m[i].getReturnType() != null) sb.append("return ");
                sb.append(delMethodName);
                sb.append("().");
                sb.append(m[i].getName());
                sb.append("(");
                sb.append(vars);
                sb.append(");");
                sb.append(newline);
                sb.append("}");
                sb.append(newline);
            }
        }
        return sb.toString();
    }
    
    public static String getShortName(String originalClassName) {
        int p = originalClassName.lastIndexOf('.');
        if(p < 0) {
            return originalClassName;
        } else {
            return originalClassName.substring(p + 1);
        }
    }
    
    public static String getSourceName(Class clazz) {
        if(clazz.isArray()) {
            return getSourceName(clazz.getComponentType()) + "[]";
        } else {
            return clazz.getName();
        }
    }
    
    public static String getVarName(Class paramType, int i) {
        while(paramType.isArray()) paramType = paramType.getComponentType();
        return decapitalize(getShortName(paramType.getName()) + "Var" + i);
    }
    
    public static String decapitalize(String s) {
        StringBuffer sb = new StringBuffer();
        int i = 0;
        for(; i < s.length(); i++){
            if(Character.isLowerCase(s.charAt(i))) break;
            else sb.append(Character.toLowerCase(s.charAt(i)));
        }
        sb.append(s.substring(i));
        return sb.toString();
    }
}
