package simple.tools;

public class DTDParser {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws XNIException 
	 */
	/*public static void main(String[] args) throws XNIException, IOException {
		testMap();
	}
	public static void testSpeed() throws XNIException, IOException {	
		String url = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"; //"-//W3C//DTD XHTML 1.0 Transitional//EN"
		Map<String,String> map = makeMap(url);
		//Mapping<String,Integer> mapping = new XHTMLTagFlags();
		String[] keys = map.keySet().toArray(new String[map.size()]);
		int times = 10000000;
		long start, end;
		for(int k = 0; k < 3; k++) {
		start = System.currentTimeMillis();
		for(int i = 0; i < times; i++) {
			int rnd = (int)(Math.random() * keys.length);
			map.get(keys[rnd]);
		}
		end = System.currentTimeMillis();
		System.out.println("Map took " +(end-start) + " milliseconds");
	*/	
		/*start = System.currentTimeMillis();
		for(int i = 0; i < times; i++) {
			int rnd = (int)(Math.random() * keys.length);
			mapping.get(keys[rnd]);
		}		
		end = System.currentTimeMillis();
		System.out.println("Mapping took " +(end-start) + " milliseconds");*/
	/*	}
		
	}
	public static void testMap() throws XNIException, IOException {
		String url = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"; //"-//W3C//DTD XHTML 1.0 Transitional//EN"
		//String url = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd";
		//String url = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd";
		
		XMLDTDLoader loader = new XMLDTDLoader();
		Grammar grammar = loader.loadGrammar(new XMLInputSource(null, url, null, true));
		System.out.println(grammar.getGrammarDescription());
		DTDGrammar dtdGrammar = (DTDGrammar)grammar;
		System.out.println("----------------------------------");
		int elementDeclIndex = 0;
        XMLElementDecl elementDecl = new XMLElementDecl();
        Map<String,String> map = new HashMap<String, String>();
        while (dtdGrammar.getElementDecl(elementDeclIndex++, elementDecl)) {
        	System.out.print(elementDecl.name.rawname);
        	System.out.print('\t');
        	switch(elementDecl.type) {
        		case XMLElementDecl.TYPE_ANY:
        			System.out.print("ANY"); break;
        		case XMLElementDecl.TYPE_CHILDREN:
        			System.out.print("CHILDREN"); break;
        		case XMLElementDecl.TYPE_EMPTY:
        			System.out.print("EMPTY"); break;
        		case XMLElementDecl.TYPE_MIXED:
        			System.out.print("MIXED"); break;
        		case XMLElementDecl.TYPE_SIMPLE:
        			System.out.print("SIMPLE"); break;        			
        	}
        	System.out.println();
        	String value;
        	if("td".equals(elementDecl.name.rawname) || "th".equals(elementDecl.name.rawname)) {
        		value = "TAG_FLAG_ADD_NBSP";
        	} else {
	        	if(elementDecl.type != XMLElementDecl.TYPE_EMPTY) {
	        		value = "TAG_FLAG_NON_EMPTY";
	        	} else
	        		value = null;
	        	int index = dtdGrammar.getAttributeDeclIndex(elementDeclIndex-1, "xml:space");
	        	if(index >= 0) {
	        		//xml:space   (preserve)     #FIXED 'preserve'
	        		value = (value == null ? "" : value + " | ") + "TAG_FLAG_PRESERVE_SPACE";
	        	}
        	}
        	if(value != null) {
        		map.put(elementDecl.name.rawname, value);
        	}
        }
        //StaticStringMappingMaker.makeMapping(map, new File("E:/Java Projects/Simple1.5/src"), "simple.xml.serializer.XHTMLTagFlags", "Integer");
        System.out.println("----------------------------------");
		Properties props = new Properties();
		props.putAll(map);
		props.store(new FileOutputStream("E:/Java Projects/Simple1.5/src/simple/xml/serializer/XHTMLTagFlags.properties"), "XHTML Transitional Tag Flags");
	}

	public static Map<String,String> makeMap(String url) throws XNIException, IOException {
		XMLDTDLoader loader = new XMLDTDLoader();
		Grammar grammar = loader.loadGrammar(new XMLInputSource(null, url, null, true));
		DTDGrammar dtdGrammar = (DTDGrammar)grammar;
		int elementDeclIndex = 0;
        XMLElementDecl elementDecl = new XMLElementDecl();
        Map<String,String> map = new HashMap<String, String>();
        while (dtdGrammar.getElementDecl(elementDeclIndex++, elementDecl)) {
        	String value;
        	if("td".equals(elementDecl.name.rawname) || "th".equals(elementDecl.name.rawname)) {
        		value = "TAG_FLAG_ADD_NBSP";
        	} else {
	        	if(elementDecl.type != XMLElementDecl.TYPE_EMPTY) {
	        		value = "TAG_FLAG_NON_EMPTY";
	        	} else
	        		value = null;
	        	int index = dtdGrammar.getAttributeDeclIndex(elementDeclIndex-1, "xml:space");
	        	if(index >= 0) {
	        		//xml:space   (preserve)     #FIXED 'preserve'
	        		value = (value == null ? "" : value + " | ") + "TAG_FLAG_PRESERVE_SPACE";
	        	}
        	}
        	if(value != null) {
        		map.put(elementDecl.name.rawname, value);
        	}
        }
        return map;
	}
	*/	
}
