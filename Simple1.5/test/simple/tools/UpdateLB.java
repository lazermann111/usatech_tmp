package simple.tools;

import java.awt.Window;
import java.io.Console;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.auth.BasicScheme;
import org.apache.commons.httpclient.methods.PostMethod;

import simple.io.ConsoleInteraction;
import simple.io.GuiInteraction;
import simple.io.InOutInteraction;
import simple.io.Interaction;

public class UpdateLB {

	/**
	 * @param args
	 * @throws IOException
	 * @throws HttpException
	 * @throws URISyntaxException
	 */
	public static void main(String[] args) throws HttpException, IOException, URISyntaxException {
		URI uri = new URI("https://usalb023.trooper.usatech.com:9443/lbadmin/config/display.php");

		PostMethod method = new PostMethod(uri.toString());
		NameValuePair vipNVP = new NameValuePair("v", null);
		NameValuePair[] params = new NameValuePair[] { new NameValuePair("srcfile", "ldirectord"), new NameValuePair("action", "modreal"), new NameValuePair("subaction", "adddata"), new NameValuePair("l", "e"), new NameValuePair("t", String.valueOf(System.currentTimeMillis())), vipNVP };
		/*method.addParameter("srcfile", "ldirectord");
		method.addParameter("action", "modreal");
		method.addParameter("subaction", "adddata");
		method.addParameter("l", "e");
		method.addParameter("t", String.valueOf(System.currentTimeMillis()));
		*/
		method.addParameter("fwd", "gate");
		method.addParameter("go", "Update");
		method.addParameter("newmaxconn", "0");
		method.addParameter("newminconn", "0");
		method.addParameter("newweight", "1");
		Interaction interaction;
		Console console;
		if((console = System.console()) != null) {
			interaction = new ConsoleInteraction(console);
		} else if(!java.awt.GraphicsEnvironment.isHeadless()) {
			interaction = new GuiInteraction((Window) null);
		} else {
			interaction = new InOutInteraction(System.out, System.in);
		}
		String user = System.getProperty("user.name");
		String username1 = "loadbalancer";
		char[] password1 = interaction.readPassword("Enter the loadbalancer password for %1$s:", username1);
		if(password1 == null)
			return;

		method.getHostAuthState().setAuthScheme(new BasicScheme());
		HttpState state = new HttpState();
		state.setCredentials(new AuthScope(uri.getHost(), uri.getPort(), "Load Balancer", "BASIC"), new UsernamePasswordCredentials(username1, new String(password1)));

		int[] vipIds = new int[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 };
		int[] ripOrdinals = new int[] { 2, 3, 4 };

		// HttpClientParams params = new HttpClientParams();
		HttpClient client = new HttpClient();
		for(int v : vipIds) {
			// method.setParameter("v", String.valueOf(v));
			vipNVP.setValue(String.valueOf(v));
			method.setQueryString(params);
			for(int ord : ripOrdinals) {
				method.setParameter("label", "usaweb3" + ord);
				int port = ((v % 2) == 1 ? 80 : 443);
				method.setParameter("newreal", "192.168.79.20" + ord + ':' + port + " ");

				int code = client.executeMethod(null, method, state);
				if(code != HttpStatus.SC_OK) {
					throw new IOException("Request failed with status code " + code + " (" + method.getStatusText() + ")");
				}
			}
		}
		/*
		 * display.php?srcfile=ldirectord&action=modreal&subaction=adddata&v=17&t=1334338969&l=e
		label=usaweb32
		newreal=192.168.79.202:80
		newweight=1
		newminconn=0

		action	modreal
		l	e
		srcfile	ldirectord
		subaction	adddata
		t	1334338969
		v	17
		---
		fwd	gate
		go	Update
		label	usaweb32
		newmaxconn	0
		newminconn	0
		newreal	192.168.79.202:80
		newweight	1
		 */
		System.exit(0);
	}

}
