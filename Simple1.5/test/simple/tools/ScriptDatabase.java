/*
 * ScriptDatabase.java
 *
 * Created on January 26, 2004, 11:26 AM
 */

package simple.tools;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.Format;
import java.util.List;
import java.util.Map;

import simple.db.BasicDataSourceFactory;
import simple.text.MessageFormat;

/**
 *
 * @author  Brian S. Krug
 */
public class ScriptDatabase {
    
    /** Creates a new instance of ScriptDatabase */
    public ScriptDatabase() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BasicDataSourceFactory bdsf = new BasicDataSourceFactory(args[0]);
        Connection conn = bdsf.getDataSource("").getConnection();
        try {
            //scriptSchema(System.out, conn.getMetaData(), args[1], false);
            scriptSchemaViews(System.out, conn.getMetaData(), args[1], false);
        } finally {
            conn.close();
            conn = null;
        }
    }
    
    public static void scriptSchemaViews(PrintStream out, DatabaseMetaData md, String schemaName, boolean schemaQualify) throws SQLException {
        Map[] tabs = retrieveViews(md, schemaName);
        for(int i = 0; i < tabs.length; i++) {
            scriptTable(out, md, (String)tabs[i].get("TABLE_NAME"), (String)tabs[i].get("SCHEMA_NAME"), schemaQualify);
        }        
    }
    
    public static void scriptSchema(PrintStream out, DatabaseMetaData md, String schemaName, boolean schemaQualify) throws SQLException {
        Map[] tabs = retrieveTables(md, schemaName);
        for(int i = 0; i < tabs.length; i++) {
            scriptTable(out, md, (String)tabs[i].get("TABLE_NAME"), (String)tabs[i].get("SCHEMA_NAME"), schemaQualify);
        }        
    }
    
    public static void scriptTable(PrintStream out, DatabaseMetaData md, String tableName, String schemaName, boolean schemaQualify) throws SQLException {
        //String TABLE_PATTERN = "CREATE TABLE {SCHEMA_NAME,null,{SCHEMA_NAME}.}{TABLE_NAME} '{'";
        String COLUMN_PATTERN = "{COLUMN_NAME} {TYPE_NAME}({COLUMN_SIZE}) {NULLABLE,choice,0#NOT NULL|1#NULL}";
        Format colFormat = new MessageFormat(COLUMN_PATTERN);
        out.print("CREATE TABLE ");
        if(schemaQualify && schemaName != null) {
            out.print(schemaName);
            out.print('.');
        }
        out.print(tableName);
        out.print(" {");
        Map[] cols = retrieveColumns(md, tableName, schemaName);
        for(int i = 0; i < cols.length; i++) {
            if(i != 0) out.print(",");
            out.print("\n\t");
            out.print(colFormat.format(cols[i]));
        }
        out.print("\n};\n\n");        
    }
    
    public static Map[] retrieveColumns(DatabaseMetaData md, String tableName, String schemaName) throws SQLException {
        ResultSet rs = md.getColumns(null, schemaName, tableName, null);
        List cols = new java.util.LinkedList();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            String[] columnNames = new String[rsmd.getColumnCount()];
            for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
            while(rs.next()) {
                Map m = new java.util.HashMap();
                for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs.getObject(i+1));
                cols.add(m);
            }
        } finally {
            rs.close();
            rs = null;
        }
        return (Map[])cols.toArray(new Map[cols.size()]);
    }
    
    public static Map[] retrieveTables(DatabaseMetaData md, String schemaName) throws SQLException {
        ResultSet rs = md.getTables(null, schemaName, null, new String[] {"TABLE"});
        List tabs = new java.util.LinkedList();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            String[] columnNames = new String[rsmd.getColumnCount()];
            for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
            while(rs.next()) {
                Map m = new java.util.HashMap();
                for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs.getObject(i+1));
                tabs.add(m);
            }
        } finally {
            rs.close();
            rs = null;
        }
        return (Map[])tabs.toArray(new Map[tabs.size()]);
    }
    
    public static Map[] retrieveViews(DatabaseMetaData md, String schemaName) throws SQLException {
        ResultSet rs = md.getTables(null, schemaName, null, new String[] {"VIEW"});
        List tabs = new java.util.LinkedList();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            String[] columnNames = new String[rsmd.getColumnCount()];
            for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
            while(rs.next()) {
                Map m = new java.util.HashMap();
                for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs.getObject(i+1));
                tabs.add(m);
            }
        } finally {
            rs.close();
            rs = null;
        }
        return (Map[])tabs.toArray(new Map[tabs.size()]);
    }
   
}
