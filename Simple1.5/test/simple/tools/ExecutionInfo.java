package simple.tools;

import java.sql.Connection;
import java.sql.SQLException;

import simple.db.DataSourceNotFoundException;
import simple.tools.DBScriptingTool.DbObjectType;

interface ExecutionInfo {

	public abstract String getUrl();

	public abstract void setUrl(String url);

	public abstract String getDriver();

	public abstract void setDriver(String driver);

	public abstract String getUsername();

	public abstract void setUsername(String username);

	public abstract String getDir();

	public abstract void setDir(String dir);

	public abstract String[] getSchemas();

	public abstract void setSchemas(String[] schemas);

	public abstract DbObjectType[] getTypes();

	public abstract void setTypes(DbObjectType[] types);

	public abstract String getPassword();

	public abstract void setPassword(String password);

	public abstract Connection connect() throws DataSourceNotFoundException, SQLException;

}