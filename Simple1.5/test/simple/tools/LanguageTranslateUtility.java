/*
 * LanguageTranslateUtility.java
 *
 * Created on May 14, 2004, 12:37 PM
 */

package simple.tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import simple.db.BasicDataSourceFactory;
import simple.io.Log;

/**
 *
 * @author  Brian S. Krug
 */
public class LanguageTranslateUtility {
    private static final Log log = Log.getLog();
    protected HttpClient httpClient = new HttpClient();
    protected static final String SERVICE_URL = "http://ets.freetranslation.com/";
    protected static final String NEWLINE = System.getProperty("line.separator", "\n");
    /**
     * Holds value of property stealthMode.
     */
    private boolean stealthMode;
    
    /** Creates a new instance of GeoLookupUtility */
    public LanguageTranslateUtility() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        new LanguageTranslateUtility().start();
    }
    
    public void start() throws Exception {
        final int LIMIT = 0;
        String sql = "select x.literalid, x.xlatevalue, a.literalvalue, l.localeid FROM IntelliClaimNet.dbo.XLate x " +
        "CROSS JOIN IntelliClaimNet.dbo.Locale l join IntelliClaimNet.dbo.literal a on l.literal_resid = a.literalid WHERE x.LocaleID = 1033 " +
        "AND NOT EXISTS(SELECT 1 FROM EU_Staging.dbo.XLateBackup i WHERE i.LiteralID = x.LiteralID AND i.LocaleID = l.LocaleID) " +
        "and l.localeid NOT IN(0, 1033, 2057, 3084, 4105, 1043, 2070)";
        /*"select x.codedvalue, x.xlatevalue, a.literalvalue, l.localeid, x.CodedCategory " +
        "FROM EU_Staging.dbo.XLateCoded x  " +
        "CROSS JOIN IntelliClaimNet.dbo.Locale l  " +
        "join IntelliClaimNet.dbo.literal a on l.literal_resid = a.literalid " +
        "WHERE x.LocaleID = 1033  " +
        "AND NOT EXISTS(SELECT 1 FROM EU_Staging.dbo.XLateCoded i " +
        "WHERE i.CodedCategory = x.CodedCategory and i.codedvalue = x.codedvalue AND i.LocaleID = l.LocaleID) " +
        "and l.localeid NOT IN(0, 1033, 2057, 3084, 4105)";*/
        
        String updSql = "insert into EU_Staging.dbo.XLateBackup(LocaleID, LiteralID, XLateValue, UpdateDate) " +
        "values(?, ?, ?, GetDate())";
        /*"insert into EU_Staging.dbo.XLateCoded(LocaleID, CodedCategory, CodedValue, XLateValue, LocaleName) " + 
        "SELECT l.LocaleID, ?, ?, ?, x.XLateValue " +
        "FROM IntelliClaimNet.dbo.Locale l " +
        "JOIN IntelliClaimNet.dbo.XLate x " +
        "   ON l.Literal_resID = x.LiteralID " +
        "  AND l.LocaleID = x.LocaleID " +
        "WHERE l.LocaleID = ? ";*/

        log.debug("Start");
        Properties props = new Properties();
        props.load(ScriptLoadDataWarehouse.class.getClassLoader().getResourceAsStream("simple/tools/Script.properties"));
        BasicDataSourceFactory bdsf = new BasicDataSourceFactory(props);        
        Connection conn = bdsf.getDataSource("").getConnection();
        try {
            PreparedStatement ps = conn.prepareStatement(updSql);
            ResultSet rs = conn.prepareStatement(sql).executeQuery();
            int cnt = 0;
            while(rs.next() && (LIMIT <= 0 || cnt++ < LIMIT)) {
                Object literalId = rs.getObject(1);
                String text = rs.getString(2);
                String lang = rs.getString(3);
                Object localeId = rs.getObject(4);
                //Object category = rs.getObject(5);
                try {
                    String trans = translate(text, "English", lang);
                    //ps.setObject(1, category);
                    ps.setObject(2, literalId);
                    ps.setObject(3, trans);
                    ps.setObject(1, localeId);
                    ps.execute();
                    log.debug("Added " + lang + " translation for '" + text + "': " + trans);
                } catch(Exception e) {
                    log.warn("Could not translate '" + text + "' to '" + lang + "'", e);
                }
            }
        } finally {
            conn.close();
            conn = null;
        }
        log.debug("Finished!");
        log.finish();

     }
    
    protected String translate(String text, String sourceLanguage, String targetLanguage) throws Exception {
        PostMethod method = new PostMethod(SERVICE_URL) {
            protected void addRequestHeaders(org.apache.commons.httpclient.HttpState state, org.apache.commons.httpclient.HttpConnection conn)
            throws java.io.IOException, org.apache.commons.httpclient.HttpException {
                //do nothing if in stealth mode
                if(!stealthMode) super.addRequestHeaders(state, conn);
            }
        };
        method.setParameter("language", sourceLanguage + "/" + targetLanguage);
        method.setParameter("srctext", text);
        method.setParameter("sequence", "core");
        httpClient.executeMethod(method);
        String trans = method.getResponseBodyAsString();       
        return trans;
    }
    
    /**
     * Getter for property stealthMode.
     * @return Value of property stealthMode.
     */
    public boolean isStealthMode() {
        return this.stealthMode;
    }
    
    /**
     * Setter for property stealthMode.
     * @param stealthMode New value of property stealthMode.
     */
    public void setStealthMode(boolean stealthMode) {
        this.stealthMode = stealthMode;
    }
    
}
