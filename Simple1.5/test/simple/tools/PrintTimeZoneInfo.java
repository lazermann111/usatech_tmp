package simple.tools;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.TimeZone;

public class PrintTimeZoneInfo {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		printSQLInsert("FRONT.TIME_ZONE", false);
		printTabSeparated();
	}
	public static void printTabSeparated() throws Exception {
		String SEP = "\t";
		PrintStream out = new PrintStream(new FileOutputStream("C:\\TEMP\\TimeZoneInfo.txt")); //System.out;
		out.print("ID");
		out.print(SEP);
		out.print("Name");
		out.print(SEP);
		out.print("Raw Offset");
		out.print(SEP);
		out.print("Daylight Savings Offset");
		out.print(SEP);
		out.print("Standard Abbr");
		out.print(SEP);
		out.print("Daylight Savings Abbr");
		out.println();
		for(String id : TimeZone.getAvailableIDs()) {
			TimeZone tz = TimeZone.getTimeZone(id);
			out.print(id);
			out.print(SEP);
			out.print(tz.getDisplayName());
			out.print(SEP);
			out.print(tz.getRawOffset());
			out.print(SEP);
			out.print(tz.getDSTSavings());
			out.print(SEP);
			out.print(tz.getDisplayName(false, TimeZone.SHORT));
			out.print(SEP);
			out.print(tz.getDisplayName(true, TimeZone.SHORT));
			out.println();
		}
	}

	public static void printSQLInsert(String table, boolean createTable) throws Exception {
		PrintStream out = new PrintStream(new FileOutputStream("C:\\Development\\workspace\\DatabaseScripts\\REPTP\\FRONT\\LoadTimeZoneData.sql")); // System.out;
		if(createTable)
			out.println("CREATE TABLE " + table + "(TIME_ZONE_ID NUMBER, TIME_ZONE_NAME VARCHAR2(90) NOT NULL,UTC_OFFSET_MS NUMBER(8) NOT NULL,DAYLIGHT_OFFSET_MS NUMBER(8) NOT NULL,STANDARD_ABBR VARCHAR2(10),DAYLIGHT_ABBR VARCHAR2(10),EXAMPLE VARCHAR2(4000));");
		int timeZoneId = 198;
		for(String id : TimeZone.getAvailableIDs()) {
			TimeZone tz = TimeZone.getTimeZone(id);
			out.print("INSERT INTO " + table + "(TIME_ZONE_ID, TIME_ZONE_NAME, EXAMPLE, UTC_OFFSET_MS, DAYLIGHT_OFFSET_MS, STANDARD_ABBR, DAYLIGHT_ABBR) SELECT " + timeZoneId + ", '");
			out.print(escapeSQLString(id));
			out.print("', '");
			out.print(escapeSQLString(tz.getDisplayName()));
			out.print("', ");
			out.print(tz.getRawOffset());
			out.print(", ");
			out.print(tz.getDSTSavings());
			out.print(", '");
			out.print(escapeSQLString(tz.getDisplayName(false, TimeZone.SHORT)));
			out.print("', '");
			out.print(escapeSQLString(tz.getDisplayName(true, TimeZone.SHORT)));
			out.print("' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM " + table + " WHERE TIME_ZONE_NAME = '" + escapeSQLString(id) + "');");
			out.println();
			timeZoneId++;
		}
		out.println("COMMIT;");
	}
	
	protected static String escapeSQLString(String s) {
		if (s == null)
			return s;
		return s.replaceAll("'", "''").replaceAll("&", "&' || '");
	}

}
