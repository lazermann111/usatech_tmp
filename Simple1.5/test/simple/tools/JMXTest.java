/**
 *
 */
package simple.tools;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import simple.io.ConsoleInteraction;
import simple.io.InOutInteraction;
import simple.io.Interaction;

/**
 * @author Brian S. Krug
 *
 */
public class JMXTest {

	public static void main(String[] args) throws Exception {
		Interaction interaction;
		{
			Console console = System.console();
			if(console != null)
				interaction = new ConsoleInteraction(console);
			else
				interaction = new InOutInteraction();
		}
		String host = interaction.readLine("Enter the host name and port: ");

		JMXServiceURL address = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + "/jmxrmi");

		String username = interaction.readLine("Enter the user name: ");// System.getProperty("user.name");
		char[] password = interaction.readPassword("Please enter the password for %1$s:", username);
		Map<String,Object> environment = new HashMap<String,Object>();
	    if(username != null) {
	    	environment.put(JMXConnector.CREDENTIALS,  new String[] {username, new String(password) });
	    }

	    JMXConnector connector = JMXConnectorFactory.connect(address, environment);
	    try {
		    MBeanServerConnection conn = connector.getMBeanServerConnection();
		    Set<ObjectInstance> beans = conn.queryMBeans(new ObjectName("simple.app.jmx:Type=ServiceManagerWithConfig,ServiceName=*,ThreadName=*"), null);
		    interaction.printf("Found %1$s beans", beans.size());
		} finally {
	    	connector.close();
	    }
	}
}
