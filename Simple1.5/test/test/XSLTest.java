package test;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.xml.TemplatesLoader;

/**
 * @author bkrug
 *
 */
public class XSLTest {
    public static void main(String[] args) throws Throwable {
    	testTransform();
    }
    public static void testTransform() throws Throwable {
    	//PrintStream out = new PrintStream(new FileOutputStream("E:\\Java Projects\\Simple1.5\\test\\test\\XSLTest-2.log"));
    	PrintStream out = System.out;
    	File xslFile = new File("E:\\Java Projects\\usalive\\xsl\\templates\\general\\serialize.xsl");
    	File xmlFile = new File("E:\\Java Projects\\usalive\\xsl\\templates\\general\\serialize.xsl");
        System.setProperty("javax.xml.transform.TransformerFactory", "com.jclark.xsl.trax.TransformerFactoryImpl");
        TransformerFactory tf = TransformerFactory.newInstance();
        out.println("*** Running a test using TransformerFactory " + tf.getClass().getName());
        Translator translator = DefaultTranslatorFactory.getTranslatorInstance();
        TemplatesLoader tl = TemplatesLoader.getInstance(translator);
        tl.setFactory(tf);
        Transformer transformer = tl.newTransformer(xslFile);
        //transformer.setParameter("context", context);
        transformer.transform(new StreamSource(new FileInputStream(xmlFile)),  new StreamResult(System.out));

    }
    public static void testGeneral() throws Throwable {
        PrintStream out = new PrintStream(new FileOutputStream("C:\\Java Projects\\Simple1.5\\test\\test\\XSLTest.log"));
        File xslFile = new File("C:\\Java Projects\\Falcon\\src\\simple\\falcon\\templates\\general\\xml.xsl");
        File xmlFile = new File("C:\\Java Projects\\Simple1.5\\test\\test\\select_fields.xml");
        Map<String,Object> a = new HashMap<String,Object>();
        a.put("http://xml.apache.org/xalan/features/incremental", true);
        testXSL("org.apache.xalan.processor.TransformerFactoryImpl", a, new FileInputStream(xslFile), new FileInputStream(xmlFile), out);
        a.clear();
        a.put("generate-translet", true);
        a.put("debug", true);
        a.put("jar-name", "xsltc-translets.jar");
        //testXSL("org.apache.xalan.xsltc.trax.TransformerFactoryImpl", a, new FileInputStream(xslFile), new FileInputStream(xmlFile), out);
        //testXSL("com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl", a, new FileInputStream(xslFile), new FileInputStream(xmlFile), out);

    }
    public static void testXSL(String transformerFactoryClass, Map<String,Object> attributes, InputStream xsl, InputStream xml, PrintStream out) throws Throwable {
        try {
            System.setProperty("javax.xml.transform.TransformerFactory", transformerFactoryClass);
            TransformerFactory tf = TransformerFactory.newInstance();
            for(Map.Entry<String, Object> e : attributes.entrySet()) {
                tf.setAttribute(e.getKey(), e.getValue());
            }
            out.println("*** Running a test using TransformerFactory " + tf.getClass().getName());
            Transformer t = tf.newTransformer(new StreamSource(xsl));
            t.transform(new StreamSource(xml), new StreamResult(out));
            out.println();
            out.println("*** Done running this test");
            out.println();
        } catch(Exception e) {
            out.println("ERROR: ");
            e.printStackTrace(out);
        }
    }

}
