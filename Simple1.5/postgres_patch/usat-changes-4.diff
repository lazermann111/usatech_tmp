--- org/postgresql/core/PGStream.java
+++ org/postgresql/core/PGStream.java
@@ -14,6 +14,7 @@
 import java.io.IOException;
 import java.io.EOFException;
 import java.io.Writer;
+import java.net.InetAddress;
 import java.net.InetSocketAddress;
 import java.net.Socket;
 import java.sql.SQLException;
@@ -33,6 +34,7 @@
 public class PGStream
 {
     private final HostSpec hostSpec;
+    private final int connectTimeout;
 
     private final byte[] _int4buf;
     private final byte[] _int2buf;
@@ -52,12 +54,13 @@
      * @param hostSpec the host and port to connect to
      * @exception IOException if an IOException occurs below it.
      */
-    public PGStream(HostSpec hostSpec) throws IOException
+    public PGStream(HostSpec hostSpec, int connectTimeout) throws IOException
     {
         this.hostSpec = hostSpec;
+        this.connectTimeout = connectTimeout;
 
         Socket socket = new Socket();
-        socket.connect(new InetSocketAddress(hostSpec.getHost(), hostSpec.getPort()));
+        socket.connect(hostSpec.getHost() != null ? new InetSocketAddress(hostSpec.getHost(), hostSpec.getPort()) : new InetSocketAddress(InetAddress.getByName(null), hostSpec.getPort()), connectTimeout);
         changeSocket(socket);
         setEncoding(Encoding.getJVMEncoding("US-ASCII"));
 
@@ -65,6 +68,10 @@
         _int4buf = new byte[4];
     }
 
+    public int getConnectTimeout() {
+    	return connectTimeout;
+    }
+    
     public HostSpec getHostSpec() {
         return hostSpec;
     }
--- org/postgresql/core/v2/ConnectionFactoryImpl.java
+++ org/postgresql/core/v2/ConnectionFactoryImpl.java
@@ -83,19 +83,27 @@
         PGStream newStream = null;
         try
         {
-            newStream = new PGStream(hostSpec);
+        	// Set the connect timeout if the "connectTimeout" property has been set.
+        	String connectTimeoutProperty = info.getProperty("connectTimeout", "20");
+        	int connectTimeout;
+        	try {
+        		connectTimeout = (int)(Double.parseDouble(connectTimeoutProperty) * 1000);
+        	} catch (NumberFormatException nfe) {
+        		connectTimeout = 20;
+        		logger.info("Couldn't parse connectTimeout value:" + connectTimeoutProperty + " - using default of " + connectTimeout + " seconds");
+        	}
+        	newStream = new PGStream(hostSpec, connectTimeout);
 
             // Construct and send an ssl startup packet if requested.
             if (trySSL)
                 newStream = enableSSL(newStream, requireSSL, info, logger);
             
-            
             // Set the socket timeout if the "socketTimeout" property has been set.
             String socketTimeoutProperty = info.getProperty("socketTimeout", "0");
             try {
-                int socketTimeout = Integer.parseInt(socketTimeoutProperty);
+            	int socketTimeout = (int)(Double.parseDouble(socketTimeoutProperty) * 1000);
                 if (socketTimeout > 0) {
-                    newStream.getSocket().setSoTimeout(socketTimeout*1000);
+                    newStream.getSocket().setSoTimeout(socketTimeout);
                 }
             } catch (NumberFormatException nfe) {
                 logger.info("Couldn't parse socketTimeout value:" + socketTimeoutProperty);
@@ -199,7 +206,7 @@
 
             // We have to reconnect to continue.
             pgStream.close();
-            return new PGStream(pgStream.getHostSpec());
+            return new PGStream(pgStream.getHostSpec(), pgStream.getConnectTimeout());
 
         case 'N':
             if (logger.logDebug())
--- org/postgresql/core/v2/ProtocolConnectionImpl.java
+++ org/postgresql/core/v2/ProtocolConnectionImpl.java
@@ -87,7 +87,7 @@
             if (logger.logDebug())
                 logger.debug(" FE=> CancelRequest(pid=" + cancelPid + ",ckey=" + cancelKey + ")");
 
-            cancelStream = new PGStream(pgStream.getHostSpec());
+            cancelStream = new PGStream(pgStream.getHostSpec(), pgStream.getConnectTimeout());
             cancelStream.SendInteger4(16);
             cancelStream.SendInteger2(1234);
             cancelStream.SendInteger2(5678);
--- org/postgresql/core/v3/ConnectionFactoryImpl.java
+++ org/postgresql/core/v3/ConnectionFactoryImpl.java
@@ -98,7 +97,15 @@
         PGStream newStream = null;
         try
         {
-            newStream = new PGStream(hostSpec);
+        	String connectTimeoutProperty = info.getProperty("connectTimeout", "20");
+        	int connectTimeout;
+        	try {
+        		connectTimeout = (int)(Double.parseDouble(connectTimeoutProperty) * 1000);
+        	} catch (NumberFormatException nfe) {
+        		connectTimeout = 20;
+        		logger.info("Couldn't parse connectTimeout value:" + connectTimeoutProperty + " - using default of " + connectTimeout + " seconds");
+        	}
+        	newStream = new PGStream(hostSpec, connectTimeout);
 
             // Construct and send an ssl startup packet if requested.
             if (trySSL)
@@ -107,9 +114,9 @@
             // Set the socket timeout if the "socketTimeout" property has been set.
             String socketTimeoutProperty = info.getProperty("socketTimeout", "0");
             try {
-                int socketTimeout = Integer.parseInt(socketTimeoutProperty);
+            	int socketTimeout = (int)(Double.parseDouble(socketTimeoutProperty) * 1000);
                 if (socketTimeout > 0) {
-                    newStream.getSocket().setSoTimeout(socketTimeout*1000);
+                    newStream.getSocket().setSoTimeout(socketTimeout);
                 }
             } catch (NumberFormatException nfe) {
                 logger.info("Couldn't parse socketTimeout value:" + socketTimeoutProperty);
@@ -296,7 +303,7 @@
 
             // We have to reconnect to continue.
             pgStream.close();
-            return new PGStream(pgStream.getHostSpec());
+            return new PGStream(pgStream.getHostSpec(), pgStream.getConnectTimeout());
 
         case 'N':
             if (logger.logDebug())
--- org/postgresql/core/v3/ProtocolConnectionImpl.java
+++ org/postgresql/core/v3/ProtocolConnectionImpl.java
@@ -89,7 +89,7 @@
             if (logger.logDebug())
                 logger.debug(" FE=> CancelRequest(pid=" + cancelPid + ",ckey=" + cancelKey + ")");
 
-            cancelStream = new PGStream(pgStream.getHostSpec());
+            cancelStream = new PGStream(pgStream.getHostSpec(), pgStream.getConnectTimeout());
             cancelStream.SendInteger4(16);
             cancelStream.SendInteger2(1234);
             cancelStream.SendInteger2(5678);
--- org/postgresql/jdbc2/AbstractJdbc2Array.java
+++ org/postgresql/jdbc2/AbstractJdbc2Array.java
@@ -9,6 +9,7 @@
 
 import org.postgresql.core.*;
 import org.postgresql.util.ByteConverter;
+import org.postgresql.util.PGbytea;
 import org.postgresql.util.PSQLException;
 import org.postgresql.util.PSQLState;
 import org.postgresql.util.GT;
@@ -707,6 +708,17 @@
             {
                 Object v = input.get(index++);
                 oa[length++] = dims > 1 && v != null ? buildArray((PgArrayList) v, 0, -1) : v;
+            }
+        }
+        else if (type == Types.BINARY || type == Types.VARBINARY)
+        {
+            Object[] oa = null;
+            ret = oa = (dims > 1 ? (Object[]) java.lang.reflect.Array.newInstance(String.class, dimsLength) : new byte[count][]);
+
+            for (; count > 0; count--)
+            {
+                Object v = input.get(index++);
+                oa[length++] = dims > 1 && v != null ? buildArray((PgArrayList) v, 0, -1) : v instanceof byte[] ? v : v != null ? PGbytea.toBytes(v.toString().getBytes()) : v;
             }
         }
 

--- org/postgresql/util/PSQLException.java
+++ org/postgresql/util/PSQLException.java
@@ -35,5 +35,16 @@ public class PSQLException extends SQLException
     {
         return _serverError;
     }
+	
+	public int getErrorCode() 
+    {
+    	int errCode = 0;
+    	try {
+    		errCode = Integer.parseInt(getSQLState());
+    	} catch (NumberFormatException e) {
+    	    // Do nothing
+    	}
+    	return errCode;
+    }
 
 }
