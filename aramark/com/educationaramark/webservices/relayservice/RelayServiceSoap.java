/**
 * RelayServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public interface RelayServiceSoap extends java.rmi.Remote {
    public com.educationaramark.webservices.relayservice.POSResponse POS_Transaction(com.educationaramark.webservices.relayservice.POSLOG POSTransaction) throws java.rmi.RemoteException;
}
