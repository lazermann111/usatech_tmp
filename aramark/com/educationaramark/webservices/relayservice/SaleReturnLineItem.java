/**
 * SaleReturnLineItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class SaleReturnLineItem  implements java.io.Serializable {
    private java.lang.String actualUnitPrice;
    private java.lang.String extendedAmount;
    private java.lang.String unitCost;
    private java.lang.String itemID;
    private java.lang.String orginialRetailStoreID;
    private java.lang.String orginisalRetailTransactionLineItemSequenceNumber;
    private java.lang.String orginisalWorkstationID;
    private java.lang.String POSDepartmentID;
    private java.lang.String POSItemID;
    private java.lang.String quantity;
    private java.lang.String discountAmount;
    private java.lang.String extendedDiscountAmount;

    public SaleReturnLineItem() {
    }

    public java.lang.String getActualUnitPrice() {
        return actualUnitPrice;
    }

    public void setActualUnitPrice(java.lang.String actualUnitPrice) {
        this.actualUnitPrice = actualUnitPrice;
    }

    public java.lang.String getExtendedAmount() {
        return extendedAmount;
    }

    public void setExtendedAmount(java.lang.String extendedAmount) {
        this.extendedAmount = extendedAmount;
    }

    public java.lang.String getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(java.lang.String unitCost) {
        this.unitCost = unitCost;
    }

    public java.lang.String getItemID() {
        return itemID;
    }

    public void setItemID(java.lang.String itemID) {
        this.itemID = itemID;
    }

    public java.lang.String getOrginialRetailStoreID() {
        return orginialRetailStoreID;
    }

    public void setOrginialRetailStoreID(java.lang.String orginialRetailStoreID) {
        this.orginialRetailStoreID = orginialRetailStoreID;
    }

    public java.lang.String getOrginisalRetailTransactionLineItemSequenceNumber() {
        return orginisalRetailTransactionLineItemSequenceNumber;
    }

    public void setOrginisalRetailTransactionLineItemSequenceNumber(java.lang.String orginisalRetailTransactionLineItemSequenceNumber) {
        this.orginisalRetailTransactionLineItemSequenceNumber = orginisalRetailTransactionLineItemSequenceNumber;
    }

    public java.lang.String getOrginisalWorkstationID() {
        return orginisalWorkstationID;
    }

    public void setOrginisalWorkstationID(java.lang.String orginisalWorkstationID) {
        this.orginisalWorkstationID = orginisalWorkstationID;
    }

    public java.lang.String getPOSDepartmentID() {
        return POSDepartmentID;
    }

    public void setPOSDepartmentID(java.lang.String POSDepartmentID) {
        this.POSDepartmentID = POSDepartmentID;
    }

    public java.lang.String getPOSItemID() {
        return POSItemID;
    }

    public void setPOSItemID(java.lang.String POSItemID) {
        this.POSItemID = POSItemID;
    }

    public java.lang.String getQuantity() {
        return quantity;
    }

    public void setQuantity(java.lang.String quantity) {
        this.quantity = quantity;
    }

    public java.lang.String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(java.lang.String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public java.lang.String getExtendedDiscountAmount() {
        return extendedDiscountAmount;
    }

    public void setExtendedDiscountAmount(java.lang.String extendedDiscountAmount) {
        this.extendedDiscountAmount = extendedDiscountAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaleReturnLineItem)) return false;
        SaleReturnLineItem other = (SaleReturnLineItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actualUnitPrice==null && other.getActualUnitPrice()==null) || 
             (this.actualUnitPrice!=null &&
              this.actualUnitPrice.equals(other.getActualUnitPrice()))) &&
            ((this.extendedAmount==null && other.getExtendedAmount()==null) || 
             (this.extendedAmount!=null &&
              this.extendedAmount.equals(other.getExtendedAmount()))) &&
            ((this.unitCost==null && other.getUnitCost()==null) || 
             (this.unitCost!=null &&
              this.unitCost.equals(other.getUnitCost()))) &&
            ((this.itemID==null && other.getItemID()==null) || 
             (this.itemID!=null &&
              this.itemID.equals(other.getItemID()))) &&
            ((this.orginialRetailStoreID==null && other.getOrginialRetailStoreID()==null) || 
             (this.orginialRetailStoreID!=null &&
              this.orginialRetailStoreID.equals(other.getOrginialRetailStoreID()))) &&
            ((this.orginisalRetailTransactionLineItemSequenceNumber==null && other.getOrginisalRetailTransactionLineItemSequenceNumber()==null) || 
             (this.orginisalRetailTransactionLineItemSequenceNumber!=null &&
              this.orginisalRetailTransactionLineItemSequenceNumber.equals(other.getOrginisalRetailTransactionLineItemSequenceNumber()))) &&
            ((this.orginisalWorkstationID==null && other.getOrginisalWorkstationID()==null) || 
             (this.orginisalWorkstationID!=null &&
              this.orginisalWorkstationID.equals(other.getOrginisalWorkstationID()))) &&
            ((this.POSDepartmentID==null && other.getPOSDepartmentID()==null) || 
             (this.POSDepartmentID!=null &&
              this.POSDepartmentID.equals(other.getPOSDepartmentID()))) &&
            ((this.POSItemID==null && other.getPOSItemID()==null) || 
             (this.POSItemID!=null &&
              this.POSItemID.equals(other.getPOSItemID()))) &&
            ((this.quantity==null && other.getQuantity()==null) || 
             (this.quantity!=null &&
              this.quantity.equals(other.getQuantity()))) &&
            ((this.discountAmount==null && other.getDiscountAmount()==null) || 
             (this.discountAmount!=null &&
              this.discountAmount.equals(other.getDiscountAmount()))) &&
            ((this.extendedDiscountAmount==null && other.getExtendedDiscountAmount()==null) || 
             (this.extendedDiscountAmount!=null &&
              this.extendedDiscountAmount.equals(other.getExtendedDiscountAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActualUnitPrice() != null) {
            _hashCode += getActualUnitPrice().hashCode();
        }
        if (getExtendedAmount() != null) {
            _hashCode += getExtendedAmount().hashCode();
        }
        if (getUnitCost() != null) {
            _hashCode += getUnitCost().hashCode();
        }
        if (getItemID() != null) {
            _hashCode += getItemID().hashCode();
        }
        if (getOrginialRetailStoreID() != null) {
            _hashCode += getOrginialRetailStoreID().hashCode();
        }
        if (getOrginisalRetailTransactionLineItemSequenceNumber() != null) {
            _hashCode += getOrginisalRetailTransactionLineItemSequenceNumber().hashCode();
        }
        if (getOrginisalWorkstationID() != null) {
            _hashCode += getOrginisalWorkstationID().hashCode();
        }
        if (getPOSDepartmentID() != null) {
            _hashCode += getPOSDepartmentID().hashCode();
        }
        if (getPOSItemID() != null) {
            _hashCode += getPOSItemID().hashCode();
        }
        if (getQuantity() != null) {
            _hashCode += getQuantity().hashCode();
        }
        if (getDiscountAmount() != null) {
            _hashCode += getDiscountAmount().hashCode();
        }
        if (getExtendedDiscountAmount() != null) {
            _hashCode += getExtendedDiscountAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaleReturnLineItem.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "SaleReturnLineItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualUnitPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ActualUnitPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ExtendedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitCost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "UnitCost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ItemID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orginialRetailStoreID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "OrginialRetailStoreID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orginisalRetailTransactionLineItemSequenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "OrginisalRetailTransactionLineItemSequenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orginisalWorkstationID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "OrginisalWorkstationID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSDepartmentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSDepartmentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSItemID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSItemID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "DiscountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendedDiscountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ExtendedDiscountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
