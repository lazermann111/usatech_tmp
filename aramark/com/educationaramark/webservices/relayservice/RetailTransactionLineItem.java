/**
 * RetailTransactionLineItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class RetailTransactionLineItem  implements java.io.Serializable {
    private java.lang.String beginDateTimeStamp;
    private java.lang.String endDateTimeStamp;
    private java.lang.String typeCode;
    private com.educationaramark.webservices.relayservice.ArrayOfSaleReturnLineItem saleReturnLineItem;
    private com.educationaramark.webservices.relayservice.DiscountLineItem discountLineItem;
    private com.educationaramark.webservices.relayservice.ArrayOfTenderLineItem tenderLineItem;
    private com.educationaramark.webservices.relayservice.ArrayOfTaxLineItem taxLineItem;
    private com.educationaramark.webservices.relayservice.ArrayOfTenderChange tenderChange;

    public RetailTransactionLineItem() {
    }

    public java.lang.String getBeginDateTimeStamp() {
        return beginDateTimeStamp;
    }

    public void setBeginDateTimeStamp(java.lang.String beginDateTimeStamp) {
        this.beginDateTimeStamp = beginDateTimeStamp;
    }

    public java.lang.String getEndDateTimeStamp() {
        return endDateTimeStamp;
    }

    public void setEndDateTimeStamp(java.lang.String endDateTimeStamp) {
        this.endDateTimeStamp = endDateTimeStamp;
    }

    public java.lang.String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(java.lang.String typeCode) {
        this.typeCode = typeCode;
    }

    public com.educationaramark.webservices.relayservice.ArrayOfSaleReturnLineItem getSaleReturnLineItem() {
        return saleReturnLineItem;
    }

    public void setSaleReturnLineItem(com.educationaramark.webservices.relayservice.ArrayOfSaleReturnLineItem saleReturnLineItem) {
        this.saleReturnLineItem = saleReturnLineItem;
    }

    public com.educationaramark.webservices.relayservice.DiscountLineItem getDiscountLineItem() {
        return discountLineItem;
    }

    public void setDiscountLineItem(com.educationaramark.webservices.relayservice.DiscountLineItem discountLineItem) {
        this.discountLineItem = discountLineItem;
    }

    public com.educationaramark.webservices.relayservice.ArrayOfTenderLineItem getTenderLineItem() {
        return tenderLineItem;
    }

    public void setTenderLineItem(com.educationaramark.webservices.relayservice.ArrayOfTenderLineItem tenderLineItem) {
        this.tenderLineItem = tenderLineItem;
    }

    public com.educationaramark.webservices.relayservice.ArrayOfTaxLineItem getTaxLineItem() {
        return taxLineItem;
    }

    public void setTaxLineItem(com.educationaramark.webservices.relayservice.ArrayOfTaxLineItem taxLineItem) {
        this.taxLineItem = taxLineItem;
    }

    public com.educationaramark.webservices.relayservice.ArrayOfTenderChange getTenderChange() {
        return tenderChange;
    }

    public void setTenderChange(com.educationaramark.webservices.relayservice.ArrayOfTenderChange tenderChange) {
        this.tenderChange = tenderChange;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetailTransactionLineItem)) return false;
        RetailTransactionLineItem other = (RetailTransactionLineItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.beginDateTimeStamp==null && other.getBeginDateTimeStamp()==null) || 
             (this.beginDateTimeStamp!=null &&
              this.beginDateTimeStamp.equals(other.getBeginDateTimeStamp()))) &&
            ((this.endDateTimeStamp==null && other.getEndDateTimeStamp()==null) || 
             (this.endDateTimeStamp!=null &&
              this.endDateTimeStamp.equals(other.getEndDateTimeStamp()))) &&
            ((this.typeCode==null && other.getTypeCode()==null) || 
             (this.typeCode!=null &&
              this.typeCode.equals(other.getTypeCode()))) &&
            ((this.saleReturnLineItem==null && other.getSaleReturnLineItem()==null) || 
             (this.saleReturnLineItem!=null &&
              this.saleReturnLineItem.equals(other.getSaleReturnLineItem()))) &&
            ((this.discountLineItem==null && other.getDiscountLineItem()==null) || 
             (this.discountLineItem!=null &&
              this.discountLineItem.equals(other.getDiscountLineItem()))) &&
            ((this.tenderLineItem==null && other.getTenderLineItem()==null) || 
             (this.tenderLineItem!=null &&
              this.tenderLineItem.equals(other.getTenderLineItem()))) &&
            ((this.taxLineItem==null && other.getTaxLineItem()==null) || 
             (this.taxLineItem!=null &&
              this.taxLineItem.equals(other.getTaxLineItem()))) &&
            ((this.tenderChange==null && other.getTenderChange()==null) || 
             (this.tenderChange!=null &&
              this.tenderChange.equals(other.getTenderChange())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBeginDateTimeStamp() != null) {
            _hashCode += getBeginDateTimeStamp().hashCode();
        }
        if (getEndDateTimeStamp() != null) {
            _hashCode += getEndDateTimeStamp().hashCode();
        }
        if (getTypeCode() != null) {
            _hashCode += getTypeCode().hashCode();
        }
        if (getSaleReturnLineItem() != null) {
            _hashCode += getSaleReturnLineItem().hashCode();
        }
        if (getDiscountLineItem() != null) {
            _hashCode += getDiscountLineItem().hashCode();
        }
        if (getTenderLineItem() != null) {
            _hashCode += getTenderLineItem().hashCode();
        }
        if (getTaxLineItem() != null) {
            _hashCode += getTaxLineItem().hashCode();
        }
        if (getTenderChange() != null) {
            _hashCode += getTenderChange().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetailTransactionLineItem.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RetailTransactionLineItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDateTimeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "BeginDateTimeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDateTimeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "EndDateTimeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleReturnLineItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "SaleReturnLineItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfSaleReturnLineItem"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountLineItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "DiscountLineItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "DiscountLineItem"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tenderLineItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TenderLineItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTenderLineItem"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxLineItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxLineItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTaxLineItem"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tenderChange");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TenderChange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTenderChange"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
