/**
 * POSResponse_Transaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class POSResponse_Transaction  implements java.io.Serializable {
    private java.lang.String beginDateTimeStamp;
    private java.lang.String transactionTypeCode;
    private java.lang.String status;
    private java.lang.String statusCode;
    private java.lang.String responseMessage;
    private java.lang.String responseText;
    private com.educationaramark.webservices.relayservice.POSResponse_Customer customer;

    public POSResponse_Transaction() {
    }

    public java.lang.String getBeginDateTimeStamp() {
        return beginDateTimeStamp;
    }

    public void setBeginDateTimeStamp(java.lang.String beginDateTimeStamp) {
        this.beginDateTimeStamp = beginDateTimeStamp;
    }

    public java.lang.String getTransactionTypeCode() {
        return transactionTypeCode;
    }

    public void setTransactionTypeCode(java.lang.String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    public java.lang.String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }

    public java.lang.String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(java.lang.String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public java.lang.String getResponseText() {
        return responseText;
    }

    public void setResponseText(java.lang.String responseText) {
        this.responseText = responseText;
    }

    public com.educationaramark.webservices.relayservice.POSResponse_Customer getCustomer() {
        return customer;
    }

    public void setCustomer(com.educationaramark.webservices.relayservice.POSResponse_Customer customer) {
        this.customer = customer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof POSResponse_Transaction)) return false;
        POSResponse_Transaction other = (POSResponse_Transaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.beginDateTimeStamp==null && other.getBeginDateTimeStamp()==null) || 
             (this.beginDateTimeStamp!=null &&
              this.beginDateTimeStamp.equals(other.getBeginDateTimeStamp()))) &&
            ((this.transactionTypeCode==null && other.getTransactionTypeCode()==null) || 
             (this.transactionTypeCode!=null &&
              this.transactionTypeCode.equals(other.getTransactionTypeCode()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.responseMessage==null && other.getResponseMessage()==null) || 
             (this.responseMessage!=null &&
              this.responseMessage.equals(other.getResponseMessage()))) &&
            ((this.responseText==null && other.getResponseText()==null) || 
             (this.responseText!=null &&
              this.responseText.equals(other.getResponseText()))) &&
            ((this.customer==null && other.getCustomer()==null) || 
             (this.customer!=null &&
              this.customer.equals(other.getCustomer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBeginDateTimeStamp() != null) {
            _hashCode += getBeginDateTimeStamp().hashCode();
        }
        if (getTransactionTypeCode() != null) {
            _hashCode += getTransactionTypeCode().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getResponseMessage() != null) {
            _hashCode += getResponseMessage().hashCode();
        }
        if (getResponseText() != null) {
            _hashCode += getResponseText().hashCode();
        }
        if (getCustomer() != null) {
            _hashCode += getCustomer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(POSResponse_Transaction.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSResponse_Transaction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDateTimeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "BeginDateTimeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TransactionTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "StatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ResponseMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ResponseText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Customer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "POSResponse_Customer"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
