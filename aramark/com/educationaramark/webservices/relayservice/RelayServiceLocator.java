/**
 * RelayServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class RelayServiceLocator extends org.apache.axis.client.Service implements com.educationaramark.webservices.relayservice.RelayService {

    // Use to get a proxy class for RelayServiceSoap
	private java.lang.String RelayServiceSoap_address;
	
	public RelayServiceLocator()
	{
		super();
	}
	
	public RelayServiceLocator(String relayServiceAddress)
	{
		super();
		RelayServiceSoap_address = relayServiceAddress;
	}

    public java.lang.String getRelayServiceSoapAddress() {
        return RelayServiceSoap_address;
    }
    
    public void setRelayServiceSoapAddress(java.lang.String address)
    {
        RelayServiceSoap_address = address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RelayServiceSoapWSDDServiceName = "RelayServiceSoap";

    public java.lang.String getRelayServiceSoapWSDDServiceName() {
        return RelayServiceSoapWSDDServiceName;
    }

    public void setRelayServiceSoapWSDDServiceName(java.lang.String name) {
        RelayServiceSoapWSDDServiceName = name;
    }

    public com.educationaramark.webservices.relayservice.RelayServiceSoap getRelayServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RelayServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRelayServiceSoap(endpoint);
    }
    
    public com.educationaramark.webservices.relayservice.RelayServiceSoap getRelayServiceSoap(java.lang.String relayServiceSoapAddress) throws javax.xml.rpc.ServiceException {
        java.net.URL endpoint;
         try {
             RelayServiceSoap_address = relayServiceSoapAddress;
             endpoint = new java.net.URL(RelayServiceSoap_address);
         }
         catch (java.net.MalformedURLException e) {
             throw new javax.xml.rpc.ServiceException(e);
         }
         return getRelayServiceSoap(endpoint);
     }

    public com.educationaramark.webservices.relayservice.RelayServiceSoap getRelayServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.educationaramark.webservices.relayservice.RelayServiceSoapStub _stub = new com.educationaramark.webservices.relayservice.RelayServiceSoapStub(portAddress, this);
            _stub.setPortName(getRelayServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.educationaramark.webservices.relayservice.RelayServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.educationaramark.webservices.relayservice.RelayServiceSoapStub _stub = new com.educationaramark.webservices.relayservice.RelayServiceSoapStub(new java.net.URL(RelayServiceSoap_address), this);
                _stub.setPortName(getRelayServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("RelayServiceSoap".equals(inputPortName)) {
            return getRelayServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RelayService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("RelayServiceSoap"));
        }
        return ports.iterator();
    }

}
