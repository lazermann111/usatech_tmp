/**
 * TaxLineItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class TaxLineItem  implements java.io.Serializable {
    private java.lang.String taxableAmount;
    private java.lang.String taxAmount;
    private java.lang.String taxPercent;
    private java.lang.String taxAuthorityID;

    public TaxLineItem() {
    }

    public java.lang.String getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(java.lang.String taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public java.lang.String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(java.lang.String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public java.lang.String getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(java.lang.String taxPercent) {
        this.taxPercent = taxPercent;
    }

    public java.lang.String getTaxAuthorityID() {
        return taxAuthorityID;
    }

    public void setTaxAuthorityID(java.lang.String taxAuthorityID) {
        this.taxAuthorityID = taxAuthorityID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TaxLineItem)) return false;
        TaxLineItem other = (TaxLineItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.taxableAmount==null && other.getTaxableAmount()==null) || 
             (this.taxableAmount!=null &&
              this.taxableAmount.equals(other.getTaxableAmount()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.taxPercent==null && other.getTaxPercent()==null) || 
             (this.taxPercent!=null &&
              this.taxPercent.equals(other.getTaxPercent()))) &&
            ((this.taxAuthorityID==null && other.getTaxAuthorityID()==null) || 
             (this.taxAuthorityID!=null &&
              this.taxAuthorityID.equals(other.getTaxAuthorityID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTaxableAmount() != null) {
            _hashCode += getTaxableAmount().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getTaxPercent() != null) {
            _hashCode += getTaxPercent().hashCode();
        }
        if (getTaxAuthorityID() != null) {
            _hashCode += getTaxAuthorityID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TaxLineItem.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxLineItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxableAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxableAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxPercent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxPercent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAuthorityID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxAuthorityID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
