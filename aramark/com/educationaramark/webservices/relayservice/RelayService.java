/**
 * RelayService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public interface RelayService extends javax.xml.rpc.Service {
    public java.lang.String getRelayServiceSoapAddress();

    public com.educationaramark.webservices.relayservice.RelayServiceSoap getRelayServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.educationaramark.webservices.relayservice.RelayServiceSoap getRelayServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
