/**
 * ArrayOfTaxLines.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class ArrayOfTaxLines  implements java.io.Serializable {
    private com.educationaramark.webservices.relayservice.TaxLines[] taxLines;

    public ArrayOfTaxLines() {
    }

    public com.educationaramark.webservices.relayservice.TaxLines[] getTaxLines() {
        return taxLines;
    }

    public void setTaxLines(com.educationaramark.webservices.relayservice.TaxLines[] taxLines) {
        this.taxLines = taxLines;
    }

    public com.educationaramark.webservices.relayservice.TaxLines getTaxLines(int i) {
        return taxLines[i];
    }

    public void setTaxLines(int i, com.educationaramark.webservices.relayservice.TaxLines value) {
        this.taxLines[i] = value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArrayOfTaxLines)) return false;
        ArrayOfTaxLines other = (ArrayOfTaxLines) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.taxLines==null && other.getTaxLines()==null) || 
             (this.taxLines!=null &&
              java.util.Arrays.equals(this.taxLines, other.getTaxLines())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTaxLines() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTaxLines());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTaxLines(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArrayOfTaxLines.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTaxLines"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxLines");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxLines"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxLines"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
