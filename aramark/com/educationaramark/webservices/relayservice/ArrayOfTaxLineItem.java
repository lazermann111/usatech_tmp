/**
 * ArrayOfTaxLineItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class ArrayOfTaxLineItem  implements java.io.Serializable {
    private com.educationaramark.webservices.relayservice.TaxLineItem[] taxLineItem;

    public ArrayOfTaxLineItem() {
    }

    public com.educationaramark.webservices.relayservice.TaxLineItem[] getTaxLineItem() {
        return taxLineItem;
    }

    public void setTaxLineItem(com.educationaramark.webservices.relayservice.TaxLineItem[] taxLineItem) {
        this.taxLineItem = taxLineItem;
    }

    public com.educationaramark.webservices.relayservice.TaxLineItem getTaxLineItem(int i) {
        return taxLineItem[i];
    }

    public void setTaxLineItem(int i, com.educationaramark.webservices.relayservice.TaxLineItem value) {
        this.taxLineItem[i] = value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ArrayOfTaxLineItem)) return false;
        ArrayOfTaxLineItem other = (ArrayOfTaxLineItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.taxLineItem==null && other.getTaxLineItem()==null) || 
             (this.taxLineItem!=null &&
              java.util.Arrays.equals(this.taxLineItem, other.getTaxLineItem())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTaxLineItem() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTaxLineItem());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTaxLineItem(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ArrayOfTaxLineItem.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ArrayOfTaxLineItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxLineItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxLineItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TaxLineItem"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
