/**
 * Transaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class Transaction  implements java.io.Serializable {
    private java.lang.String beginDateTimeStamp;
    private java.lang.String endDateTimeStamp;
    private java.lang.String operatorID;
    private java.lang.String transactionTypeCode;
    private java.lang.String transactionID;
    private java.lang.String revenueCenterID;
    private com.educationaramark.webservices.relayservice.RetailTransaction retailTransaction;
    private com.educationaramark.webservices.relayservice.Board board;
    private com.educationaramark.webservices.relayservice.DecliningBalance decliningBalance;
    private com.educationaramark.webservices.relayservice.CashEquivalency cashEquivalency;
    private com.educationaramark.webservices.relayservice.Inquiry inquiry;
    private com.educationaramark.webservices.relayservice.Credit credit;
    private java.lang.String offLine;

    public Transaction() {
    }

    public java.lang.String getBeginDateTimeStamp() {
        return beginDateTimeStamp;
    }

    public void setBeginDateTimeStamp(java.lang.String beginDateTimeStamp) {
        this.beginDateTimeStamp = beginDateTimeStamp;
    }

    public java.lang.String getEndDateTimeStamp() {
        return endDateTimeStamp;
    }

    public void setEndDateTimeStamp(java.lang.String endDateTimeStamp) {
        this.endDateTimeStamp = endDateTimeStamp;
    }

    public java.lang.String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(java.lang.String operatorID) {
        this.operatorID = operatorID;
    }

    public java.lang.String getTransactionTypeCode() {
        return transactionTypeCode;
    }

    public void setTransactionTypeCode(java.lang.String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    public java.lang.String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(java.lang.String transactionID) {
        this.transactionID = transactionID;
    }

    public java.lang.String getRevenueCenterID() {
        return revenueCenterID;
    }

    public void setRevenueCenterID(java.lang.String revenueCenterID) {
        this.revenueCenterID = revenueCenterID;
    }

    public com.educationaramark.webservices.relayservice.RetailTransaction getRetailTransaction() {
        return retailTransaction;
    }

    public void setRetailTransaction(com.educationaramark.webservices.relayservice.RetailTransaction retailTransaction) {
        this.retailTransaction = retailTransaction;
    }

    public com.educationaramark.webservices.relayservice.Board getBoard() {
        return board;
    }

    public void setBoard(com.educationaramark.webservices.relayservice.Board board) {
        this.board = board;
    }

    public com.educationaramark.webservices.relayservice.DecliningBalance getDecliningBalance() {
        return decliningBalance;
    }

    public void setDecliningBalance(com.educationaramark.webservices.relayservice.DecliningBalance decliningBalance) {
        this.decliningBalance = decliningBalance;
    }

    public com.educationaramark.webservices.relayservice.CashEquivalency getCashEquivalency() {
        return cashEquivalency;
    }

    public void setCashEquivalency(com.educationaramark.webservices.relayservice.CashEquivalency cashEquivalency) {
        this.cashEquivalency = cashEquivalency;
    }

    public com.educationaramark.webservices.relayservice.Inquiry getInquiry() {
        return inquiry;
    }

    public void setInquiry(com.educationaramark.webservices.relayservice.Inquiry inquiry) {
        this.inquiry = inquiry;
    }

    public com.educationaramark.webservices.relayservice.Credit getCredit() {
        return credit;
    }

    public void setCredit(com.educationaramark.webservices.relayservice.Credit credit) {
        this.credit = credit;
    }

    public java.lang.String getOffLine() {
        return offLine;
    }

    public void setOffLine(java.lang.String offLine) {
        this.offLine = offLine;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Transaction)) return false;
        Transaction other = (Transaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.beginDateTimeStamp==null && other.getBeginDateTimeStamp()==null) || 
             (this.beginDateTimeStamp!=null &&
              this.beginDateTimeStamp.equals(other.getBeginDateTimeStamp()))) &&
            ((this.endDateTimeStamp==null && other.getEndDateTimeStamp()==null) || 
             (this.endDateTimeStamp!=null &&
              this.endDateTimeStamp.equals(other.getEndDateTimeStamp()))) &&
            ((this.operatorID==null && other.getOperatorID()==null) || 
             (this.operatorID!=null &&
              this.operatorID.equals(other.getOperatorID()))) &&
            ((this.transactionTypeCode==null && other.getTransactionTypeCode()==null) || 
             (this.transactionTypeCode!=null &&
              this.transactionTypeCode.equals(other.getTransactionTypeCode()))) &&
            ((this.transactionID==null && other.getTransactionID()==null) || 
             (this.transactionID!=null &&
              this.transactionID.equals(other.getTransactionID()))) &&
            ((this.revenueCenterID==null && other.getRevenueCenterID()==null) || 
             (this.revenueCenterID!=null &&
              this.revenueCenterID.equals(other.getRevenueCenterID()))) &&
            ((this.retailTransaction==null && other.getRetailTransaction()==null) || 
             (this.retailTransaction!=null &&
              this.retailTransaction.equals(other.getRetailTransaction()))) &&
            ((this.board==null && other.getBoard()==null) || 
             (this.board!=null &&
              this.board.equals(other.getBoard()))) &&
            ((this.decliningBalance==null && other.getDecliningBalance()==null) || 
             (this.decliningBalance!=null &&
              this.decliningBalance.equals(other.getDecliningBalance()))) &&
            ((this.cashEquivalency==null && other.getCashEquivalency()==null) || 
             (this.cashEquivalency!=null &&
              this.cashEquivalency.equals(other.getCashEquivalency()))) &&
            ((this.inquiry==null && other.getInquiry()==null) || 
             (this.inquiry!=null &&
              this.inquiry.equals(other.getInquiry()))) &&
            ((this.credit==null && other.getCredit()==null) || 
             (this.credit!=null &&
              this.credit.equals(other.getCredit()))) &&
            ((this.offLine==null && other.getOffLine()==null) || 
             (this.offLine!=null &&
              this.offLine.equals(other.getOffLine())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBeginDateTimeStamp() != null) {
            _hashCode += getBeginDateTimeStamp().hashCode();
        }
        if (getEndDateTimeStamp() != null) {
            _hashCode += getEndDateTimeStamp().hashCode();
        }
        if (getOperatorID() != null) {
            _hashCode += getOperatorID().hashCode();
        }
        if (getTransactionTypeCode() != null) {
            _hashCode += getTransactionTypeCode().hashCode();
        }
        if (getTransactionID() != null) {
            _hashCode += getTransactionID().hashCode();
        }
        if (getRevenueCenterID() != null) {
            _hashCode += getRevenueCenterID().hashCode();
        }
        if (getRetailTransaction() != null) {
            _hashCode += getRetailTransaction().hashCode();
        }
        if (getBoard() != null) {
            _hashCode += getBoard().hashCode();
        }
        if (getDecliningBalance() != null) {
            _hashCode += getDecliningBalance().hashCode();
        }
        if (getCashEquivalency() != null) {
            _hashCode += getCashEquivalency().hashCode();
        }
        if (getInquiry() != null) {
            _hashCode += getInquiry().hashCode();
        }
        if (getCredit() != null) {
            _hashCode += getCredit().hashCode();
        }
        if (getOffLine() != null) {
            _hashCode += getOffLine().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Transaction.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Transaction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDateTimeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "BeginDateTimeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDateTimeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "EndDateTimeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "OperatorID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TransactionTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "TransactionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("revenueCenterID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RevenueCenterID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retailTransaction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RetailTransaction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RetailTransaction"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("board");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Board"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Board"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("decliningBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "DecliningBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "DecliningBalance"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashEquivalency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "CashEquivalency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "CashEquivalency"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inquiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Inquiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Inquiry"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Credit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "Credit"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offLine");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "OffLine"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
