/**
 * RetailTransaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.educationaramark.webservices.relayservice;

public class RetailTransaction  implements java.io.Serializable {
    private java.lang.String customerID;
    private java.lang.String unitCount;
    private java.lang.String operatorID;
    private com.educationaramark.webservices.relayservice.RetailTransactionLineItem retailTransactionLineItem;
    private java.lang.String receiptDateTime;
    private java.lang.String specialOrderNumber;

    public RetailTransaction() {
    }

    public java.lang.String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(java.lang.String customerID) {
        this.customerID = customerID;
    }

    public java.lang.String getUnitCount() {
        return unitCount;
    }

    public void setUnitCount(java.lang.String unitCount) {
        this.unitCount = unitCount;
    }

    public java.lang.String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(java.lang.String operatorID) {
        this.operatorID = operatorID;
    }

    public com.educationaramark.webservices.relayservice.RetailTransactionLineItem getRetailTransactionLineItem() {
        return retailTransactionLineItem;
    }

    public void setRetailTransactionLineItem(com.educationaramark.webservices.relayservice.RetailTransactionLineItem retailTransactionLineItem) {
        this.retailTransactionLineItem = retailTransactionLineItem;
    }

    public java.lang.String getReceiptDateTime() {
        return receiptDateTime;
    }

    public void setReceiptDateTime(java.lang.String receiptDateTime) {
        this.receiptDateTime = receiptDateTime;
    }

    public java.lang.String getSpecialOrderNumber() {
        return specialOrderNumber;
    }

    public void setSpecialOrderNumber(java.lang.String specialOrderNumber) {
        this.specialOrderNumber = specialOrderNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetailTransaction)) return false;
        RetailTransaction other = (RetailTransaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customerID==null && other.getCustomerID()==null) || 
             (this.customerID!=null &&
              this.customerID.equals(other.getCustomerID()))) &&
            ((this.unitCount==null && other.getUnitCount()==null) || 
             (this.unitCount!=null &&
              this.unitCount.equals(other.getUnitCount()))) &&
            ((this.operatorID==null && other.getOperatorID()==null) || 
             (this.operatorID!=null &&
              this.operatorID.equals(other.getOperatorID()))) &&
            ((this.retailTransactionLineItem==null && other.getRetailTransactionLineItem()==null) || 
             (this.retailTransactionLineItem!=null &&
              this.retailTransactionLineItem.equals(other.getRetailTransactionLineItem()))) &&
            ((this.receiptDateTime==null && other.getReceiptDateTime()==null) || 
             (this.receiptDateTime!=null &&
              this.receiptDateTime.equals(other.getReceiptDateTime()))) &&
            ((this.specialOrderNumber==null && other.getSpecialOrderNumber()==null) || 
             (this.specialOrderNumber!=null &&
              this.specialOrderNumber.equals(other.getSpecialOrderNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomerID() != null) {
            _hashCode += getCustomerID().hashCode();
        }
        if (getUnitCount() != null) {
            _hashCode += getUnitCount().hashCode();
        }
        if (getOperatorID() != null) {
            _hashCode += getOperatorID().hashCode();
        }
        if (getRetailTransactionLineItem() != null) {
            _hashCode += getRetailTransactionLineItem().hashCode();
        }
        if (getReceiptDateTime() != null) {
            _hashCode += getReceiptDateTime().hashCode();
        }
        if (getSpecialOrderNumber() != null) {
            _hashCode += getSpecialOrderNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetailTransaction.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RetailTransaction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "CustomerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "UnitCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "OperatorID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retailTransactionLineItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RetailTransactionLineItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "RetailTransactionLineItem"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiptDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "ReceiptDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specialOrderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://webservices.educationaramark.com/relayservice", "SpecialOrderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
