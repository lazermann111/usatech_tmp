/*
 * Created on Nov 22, 2004
 */
package com.usatech.aramark;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.axis.client.Stub;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.educationaramark.webservices.relayservice.ArrayOfTaxLines;
import com.educationaramark.webservices.relayservice.Board;
import com.educationaramark.webservices.relayservice.CashEquivalency;
import com.educationaramark.webservices.relayservice.Credit;
import com.educationaramark.webservices.relayservice.DecliningBalance;
import com.educationaramark.webservices.relayservice.Inquiry;
import com.educationaramark.webservices.relayservice.POSLOG;
import com.educationaramark.webservices.relayservice.POSResponse;
import com.educationaramark.webservices.relayservice.POSResponse_Customer;
import com.educationaramark.webservices.relayservice.POSResponse_Transaction;
import com.educationaramark.webservices.relayservice.RelayServiceLocator;
import com.educationaramark.webservices.relayservice.RelayServiceSoap;
import com.educationaramark.webservices.relayservice.TaxLines;
import com.educationaramark.webservices.relayservice.Transaction;

/*
 * @author dKouznetsov
 */
public class WSClient
{
	private static final String VERSION = "1.0.0";
	private static final String TRAN_TYPE_INQUIRY = "INQUIRY";
	private static final String TRAN_TYPE_DECLININGBALANCE = "DECLININGBALANCE";
	private static final String TRAN_TYPE_CREDIT = "CREDIT";
	private static final String TRAN_TYPE_BOARD = "BOARD";
	private static final String TRAN_TYPE_CASHEQUIVALENCY = "CASHEQUIVALENCY";

	private static final String classInfo = "AramarkGateway::" + WSClient.class.getName();
	private static Log log = LogFactory.getLog(classInfo);
	
	private static final String CARD_DATA_REGEX = "([0-9]{7,})";
	private static Pattern cardDataPattern = Pattern.compile(CARD_DATA_REGEX);

	private static final RelayServiceLocator locator;
	private RelayServiceSoap ws;
	private POSLOG request;
	private POSResponse response;

	private TripleDesEncryption tde;
	private String transactionTypeCode;
	private boolean isEncryptionEnabled;
	private String remoteServerAddress_, cryptoKey_, cryptoIV_, account_, cardNumber_, resultDate_;
	private String operatorID_, transactionID_, revenueCenterID_, offline_, tranAmount_, unitCount_;
	
	static {
		locator = new RelayServiceLocator();
	}

	public WSClient() throws AramarkException
	{
		try
		{
			request = new POSLOG();
			response = new POSResponse();
        }
        catch(Exception e)
        {
            throw new AramarkException(classInfo, new Exception(e));
        }
	}
	
    private static String maskCard(String card) {
    	if(card == null) return null;
    	Matcher cardDataMatcher = cardDataPattern.matcher(card);
    	if (cardDataMatcher.find()) {
    		byte[] digits = cardDataMatcher.group(1).getBytes();
			int end = digits.length - 4;
			for(int i = 2; i < end; i++)
				digits[i] = '*';
			return card.length() + " bytes, masked: " + new String(digits);    		
    	} else
			return card;
    }

	private void processRequest() throws AramarkException
	{
		try
		{
			if (isEncryptionEnabled == true)
			{
				if ((tde == null) || (tde.getCryptoKey() != cryptoKey_) || (tde.getCryptoIV() != cryptoIV_))
					tde = new TripleDesEncryption(cryptoKey_, cryptoIV_);

				cardNumber_ = tde.encryptString(cardNumber_);
		    	operatorID_ = tde.encryptString(operatorID_);
		    	transactionID_ = tde.encryptString(transactionID_);
		    	revenueCenterID_ = tde.encryptString(revenueCenterID_);
		    	offline_ = tde.encryptString(offline_);
			}

			Transaction requestTransaction = new Transaction();

			if (transactionTypeCode.equals(TRAN_TYPE_INQUIRY) == true)
			{
				Inquiry requestTransInquiry  = new Inquiry();
				requestTransInquiry.setCustomerID(cardNumber_);
	            requestTransaction.setInquiry(requestTransInquiry);
			}
			else if (transactionTypeCode.equals(TRAN_TYPE_DECLININGBALANCE) == true)
			{
				if (isEncryptionEnabled == true) tranAmount_ = tde.encryptString(tranAmount_);

				DecliningBalance requestTransDecliningBalance  = new DecliningBalance();
				requestTransDecliningBalance.setCustomerID(cardNumber_);
				requestTransDecliningBalance.setAmount(tranAmount_);
	            requestTransaction.setDecliningBalance(requestTransDecliningBalance);
			}
			else if (transactionTypeCode.equals(TRAN_TYPE_CREDIT) == true)
			{
				if (isEncryptionEnabled == true) tranAmount_ = tde.encryptString(tranAmount_);

				Credit requestTransCredit  = new Credit();
				requestTransCredit.setCustomerID(cardNumber_);
				requestTransCredit.setAmount(tranAmount_);
	            requestTransaction.setCredit(requestTransCredit);
			}
			else if (transactionTypeCode.equals(TRAN_TYPE_BOARD) == true)
			{
				if (isEncryptionEnabled == true) unitCount_ = tde.encryptString(unitCount_);

				Board requestTransBoard  = new Board();
				requestTransBoard.setCustomerID(cardNumber_);
				requestTransBoard.setUnitCount(unitCount_);
	            requestTransaction.setBoard(requestTransBoard);
			}
			else if (transactionTypeCode.equals(TRAN_TYPE_CASHEQUIVALENCY) == true)
			{
				if (isEncryptionEnabled == true) tranAmount_ = tde.encryptString(tranAmount_);

				CashEquivalency requestTransCashEquivalency  = new CashEquivalency();
				requestTransCashEquivalency.setCustomerID(cardNumber_);
				requestTransCashEquivalency.setAmount(tranAmount_);
	            requestTransaction.setCashEquivalency(requestTransCashEquivalency);
			}

			requestTransaction.setBeginDateTimeStamp(resultDate_);
			requestTransaction.setEndDateTimeStamp(resultDate_);
			requestTransaction.setOperatorID(operatorID_);
			if (isEncryptionEnabled == true) transactionTypeCode = tde.encryptString(transactionTypeCode);
			requestTransaction.setTransactionTypeCode(transactionTypeCode);
			requestTransaction.setTransactionID(transactionID_);
			requestTransaction.setRevenueCenterID(revenueCenterID_);
			requestTransaction.setOffLine(offline_);

	        request.setTransaction(requestTransaction);
	        request.setAccount(account_);

	        response = ws.POS_Transaction(request);
	        POSResponse_Transaction responseTransaction = response.getTransaction();
	        POSResponse_Customer responseCustomer = responseTransaction.getCustomer();

	        if (isEncryptionEnabled == true && !"INVALID".equalsIgnoreCase(responseTransaction.getStatus()))
	        {
	            responseTransaction.setTransactionTypeCode(tde.decryptString(responseTransaction.getTransactionTypeCode()));
	        	responseTransaction.setResponseMessage(tde.decryptString(responseTransaction.getResponseMessage()));
	            responseTransaction.setResponseText(tde.decryptString(responseTransaction.getResponseText()));
	            responseTransaction.setStatus(tde.decryptString(responseTransaction.getStatus()));
	            responseTransaction.setStatusCode(tde.decryptString(responseTransaction.getStatusCode()));

		        responseCustomer.setCustomerID(tde.decryptString(responseCustomer.getCustomerID()));
		        responseCustomer.setCusotmerIssueID(tde.decryptString(responseCustomer.getCusotmerIssueID()));
	            responseCustomer.setAmount(tde.decryptString(responseCustomer.getAmount()));
	            responseCustomer.setFirstName(tde.decryptString(responseCustomer.getFirstName()));
	            responseCustomer.setLastName(tde.decryptString(responseCustomer.getLastName()));

            	ArrayOfTaxLines responseTax = responseCustomer.getTax();
            	if (responseTax != null)
            	{
            		TaxLines[] responseTaxLines = responseTax.getTaxLines();
            		for (int i=0; i < responseTaxLines.length; i++)
            		{
            			responseTaxLines[i].setTaxAuthorityID(tde.decryptString(responseTaxLines[i].getTaxAuthorityID()));
            			responseTaxLines[i].setUseTax(tde.decryptString(responseTaxLines[i].getUseTax()));
            		}
            	}
	        }

	        if (log.isInfoEnabled())
	        {
	            log.info("Response: transactionTypeCode=" + responseTransaction.getTransactionTypeCode() +
	                    ", account=" + response.getAccount() +
	                    ", dateTimeStamp=" + responseTransaction.getBeginDateTimeStamp() +
	                    ", cardNumber=" + maskCard(responseCustomer.getCustomerID()) +
	                    ", cardIssueID=" + responseCustomer.getCusotmerIssueID() +
	                    ", balance=" + responseCustomer.getAmount() +
	                    ", responseMessage=" + responseTransaction.getResponseMessage() +
	                    ", responseText=" + responseTransaction.getResponseText() +
	                    ", status=" + responseTransaction.getStatus() +
	                    ", statusCode=" + responseTransaction.getStatusCode());
	        }
		}
		catch(Exception e)
		{
			throw new AramarkException(classInfo, new Exception(e));
		}
	}

	public String getVersion()
	{
		return VERSION;
	}

    public POSResponse inquiry(String remoteServerAddress, String account, String cardNumber, String resultDate, String operatorID,
    		String transactionID, String revenueCenterID, String offline, String enableEncryption, String cryptoKey, String cryptoIV)
    		throws AramarkException
    {
		transactionTypeCode = TRAN_TYPE_INQUIRY;
		setParameters(remoteServerAddress, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, offline, enableEncryption, cryptoKey, cryptoIV);
        processRequest();
        return response;
    }

    public POSResponse decliningBalance(String remoteServerAddress, String account, String cardNumber, String resultDate, String operatorID,
    		String transactionID, String revenueCenterID, String offline, String debitAmount, String enableEncryption, String cryptoKey, String cryptoIV)
    		throws AramarkException
    {
		transactionTypeCode = TRAN_TYPE_DECLININGBALANCE;
		tranAmount_ = debitAmount;
		setParameters(remoteServerAddress, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, offline, enableEncryption, cryptoKey, cryptoIV);
		processRequest();
        return response;
    }

    public POSResponse credit(String remoteServerAddress, String account, String cardNumber, String resultDate, String operatorID,
    		String transactionID, String revenueCenterID, String offline, String creditAmount, String enableEncryption, String cryptoKey, String cryptoIV)
    		throws AramarkException
    {
		transactionTypeCode = TRAN_TYPE_CREDIT;
		tranAmount_ = creditAmount;
		setParameters(remoteServerAddress, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, offline, enableEncryption, cryptoKey, cryptoIV);
        processRequest();
        return response;
    }

    public POSResponse board(String remoteServerAddress, String account, String cardNumber, String resultDate, String operatorID,
    		String transactionID, String revenueCenterID, String offline, String unitCount, String enableEncryption, String cryptoKey, String cryptoIV)
    		throws AramarkException
    {
		transactionTypeCode = TRAN_TYPE_BOARD;
		setParameters(remoteServerAddress, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, offline, enableEncryption, cryptoKey, cryptoIV);
		unitCount_ = unitCount;
        processRequest();
        return response;
    }

    public POSResponse cashEquivalency(String remoteServerAddress, String account, String cardNumber, String resultDate, String operatorID,
    		String transactionID, String revenueCenterID, String offline, String cashAmount, String enableEncryption, String cryptoKey, String cryptoIV)
    		throws AramarkException
    {
		transactionTypeCode = TRAN_TYPE_CASHEQUIVALENCY;
		tranAmount_ = cashAmount;
		setParameters(remoteServerAddress, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, offline, enableEncryption, cryptoKey, cryptoIV);
        processRequest();
        return response;
    }

    public POSResponse sampleMessage() throws AramarkException
    {
    	//throw new AramarkException(classInfo, "Sample Message", "Sample Text", "Sample Description");
    	POSResponse response = new POSResponse();
		response.setAccount("Sample Account");

		Date Current_Date = new Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'0000000-04:00'");
        String resultDate = sdf.format(Current_Date);

		POSResponse_Transaction tran = new POSResponse_Transaction();
    	tran.setBeginDateTimeStamp(resultDate);
    	tran.setResponseMessage("Sample Customer Message");
    	tran.setResponseText("SAMPLE RESPONSE TEXT");
    	tran.setStatus("VALID");
    	tran.setStatusCode("0");
    	tran.setTransactionTypeCode("SAMPLEMESSAGE");

    	POSResponse_Customer customer = new POSResponse_Customer();
    	customer.setAmount("1.00");
    	customer.setCusotmerIssueID("");
    	customer.setCustomerID("00000001");
    	customer.setFirstName("John");
    	customer.setLastName("Doe");

    	ArrayOfTaxLines tax = new ArrayOfTaxLines();
    	TaxLines[] taxLines = new TaxLines[4];

    	for (int i=0; i < 4; i++)
    	{
    		taxLines[i] = new TaxLines();
    		taxLines[i].setTaxAuthorityID("Sample Tax " + (i + 1));
    		taxLines[i].setUseTax("TRUE");
		}

		tax.setTaxLines(taxLines);
		customer.setTax(tax);

    	tran.setCustomer(customer);
    	response.setTransaction(tran);

    	return response;
    }

    private void setParameters(String remoteServerAddress, String account, String cardNumber, String resultDate, String operatorID,
    		String transactionID, String revenueCenterID, String offline, String enableEncryption, String cryptoKey, String cryptoIV)
    		throws AramarkException
    {
        if (log.isInfoEnabled())
        {
            String logInfo = "Request: transactionTypeCode=" + transactionTypeCode +
            ", account=" + account +
            ", cardNumber=" + maskCard(cardNumber) +
            ", transactionID=" + transactionID;

            if ((transactionTypeCode.equals(TRAN_TYPE_DECLININGBALANCE) == true) ||
                    (transactionTypeCode.equals(TRAN_TYPE_CREDIT) == true) ||
                    (transactionTypeCode.equals(TRAN_TYPE_CASHEQUIVALENCY) == true))
			{
                logInfo += ", amount=" + tranAmount_;
			}
			else if (transactionTypeCode.equals(TRAN_TYPE_BOARD) == true)
			{
			    logInfo += ", unitCount=" + unitCount_;
			}

            logInfo += ", resultDate=" + resultDate +
            ", operatorID=" + operatorID +
            ", revenueCenterID=" + revenueCenterID +
            ", offline=" + offline +
            ", enableEncryption=" + enableEncryption +
            ", cryptoKey=" + cryptoKey.length() + " bytes" +
            ", cryptoIV=" + cryptoIV.length() + " bytes" +
            ", remoteServerAddress=" + remoteServerAddress;

            log.info(logInfo);
        }

        try
		{
			if ((ws == null) || (remoteServerAddress_ == null) || (remoteServerAddress_.equals(remoteServerAddress) == false))
			{
			    remoteServerAddress_ = remoteServerAddress;
			    ws = locator.getRelayServiceSoap(remoteServerAddress_);
			    Stub stub = (Stub) ws;
			    //set timeout to 0 for configuration properties to take effect
			    stub.setTimeout(0);
			}
		}
		catch (Exception e)
		{
			throw new AramarkException(classInfo, "Unable to connect to Aramark Web Service", new Exception(e));
		}

    	account_ = account;
		cardNumber_ = cardNumber;
		resultDate_ = resultDate;
		operatorID_ = operatorID;
		transactionID_ = transactionID;
		revenueCenterID_ = revenueCenterID;
		offline_ = offline;

		if (enableEncryption.toUpperCase().equals("FALSE") == true)
			isEncryptionEnabled = false;
		else
			isEncryptionEnabled = true;

		cryptoKey_ = cryptoKey;
		cryptoIV_ = cryptoIV;
    }
}
