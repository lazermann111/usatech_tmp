/*
 * Created on Nov 30, 2004
 */
package com.usatech.aramark;

/**
 * @author dKouznetsov
 */
public class Log 
{
	public static final String NONE = "NONE";
	public static final String INFO = "INFO";
	public static final String DEBUG = "DEBUG";
	public static final String EXCEPTION = "EXCEPTION";
	public static final String WARN = "WARN";
	public static final String CRITICAL = "CRITICAL";
	
	private Log(){}
	

	public static void output(String logCode, String logMessage)
	{
		com.usatech.util.Util.output(logCode, logMessage);
	}
}
