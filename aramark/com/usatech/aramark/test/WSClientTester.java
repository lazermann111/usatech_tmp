/*
 * Created on Nov 22, 2004
 */
package com.usatech.aramark.test;

import java.util.Date;

import com.educationaramark.webservices.relayservice.*;
import com.usatech.aramark.*;

/*
 * @author dKouznetsov
 */
public class WSClientTester {
	
	private static final String transactionTypeCode = "INQUIRY";
	private static final String account = "ScanPlus_90";
	private static final String cardNumber = "999999999";
	private static final String operatorID = "";
	private static final String transactionID = "123456";
	private static final String revenueCenterID = "";
	private static final String offline = "FALSE";
	private static final String debitAmount = "1";
	private static final String creditAmount = "1";
	private static final String cryptoKey = "g9f9tr4j34t66lkjgovjo8rs";
	private static final String cryptoIV = "vgv8gfd9";
	private static final String encryptionEnabled = "TRUE";
	private static final String remoteServerAddress = "http://demo.aramarkcampusit.com/Relay_V2/RelayService.asmx";

	public static void main(String[] args)
    {
		POSResponse response = null;
        try
        {	
        	WSClient wsc = new WSClient();
        	
            Date Current_Date = new Date();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'0000000-04:00'");
            String resultDate = sdf.format(Current_Date);

			if (transactionTypeCode.equals("INQUIRY") == true)
			{
				response = wsc.inquiry(remoteServerAddress, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, offline, encryptionEnabled, cryptoKey, cryptoIV);
			}
			else if (transactionTypeCode.equals("DECLININGBALANCE") == true)
			{
				response = wsc.decliningBalance(remoteServerAddress, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, offline, debitAmount, encryptionEnabled, cryptoKey, cryptoIV);
			}
			else if (transactionTypeCode.equals("CREDIT") == true)
			{
				response = wsc.credit(remoteServerAddress, account, cardNumber, resultDate, operatorID, transactionID, revenueCenterID, offline, creditAmount, encryptionEnabled, cryptoKey, cryptoIV);
			}
			else if (transactionTypeCode.equals("SAMPLEMESSAGE") == true)
			{
				response = wsc.sampleMessage();
			}
			
			if (response != null)
			{
				POSResponse_Transaction tran = response.getTransaction();
				POSResponse_Customer customer = tran.getCustomer();				
	
	            System.out.println("-------------------- POS " + transactionTypeCode + " RESULTS --------------------------");
	            System.out.println("Account = " + response.getAccount());
	            System.out.println("Card Number: " + customer.getCustomerID());
	            System.out.println("Balance: " + customer.getAmount());
	            System.out.println("ClientFirstName: " + customer.getFirstName());
	            System.out.println("ClientLastName: " + customer.getLastName());
	            System.out.println("ResponseMessage(Customer): " + tran.getResponseMessage());
	            System.out.println("ResponseText(Error): " + tran.getResponseText());
	            System.out.println("Status: " + tran.getStatus());
	            System.out.println("StatusCode: " + tran.getStatusCode());
	            if ((tran.getStatus().equals("VALID") == true))
	            {
	            	ArrayOfTaxLines responseTax = customer.getTax();
	            	if (responseTax != null)
	            	{
	            		TaxLines[] responseTaxLines = responseTax.getTaxLines();
	            		for (int i=0; i < responseTaxLines.length; i++)
	            		{
	            			System.out.println("Authority" + (i + 1) + " = " + responseTaxLines[i].getTaxAuthorityID());
	            			System.out.println("Tax" + (i + 1) + " = " + responseTaxLines[i].getUseTax()); 
	            		}
	            	}
	            }
	            System.out.println("-------------------------------------------------------------------");
			}
        }
        catch(AramarkException ae)
        {
            System.out.println("---------------------- POS " + transactionTypeCode + " ARAMARK EXCEPTION ----------------------------");
            System.out.println("Description: " + ae.getDescription());
            System.out.println("Response Message: " + ae.getResponseMessage());
            System.out.println("Response Text: " + ae.getResponseText());
            ae.printStackTrace(System.out);
            System.out.println("---------------------------------------------------------");
        }
        catch(Exception e)
        {
            System.out.println("---------------------- POS " + transactionTypeCode + " ----------------------------");
            System.out.println(e.getMessage());
            e.printStackTrace(System.out);
            System.out.println("---------------------------------------------------------");
        }
    }
}
