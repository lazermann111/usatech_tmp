/*
 * Created on Nov 23, 2004
 */
package com.usatech.aramark.test;

import com.usatech.aramark.TripleDesEncryption;

/**
 * @author dKouznetsov
 */
public class TripleDesEncryptionTester {

	public static void main(String[] args) 
	{
		try
		{
			TripleDesEncryption tde = new TripleDesEncryption("g9f9tr4j34t66lkjgovjo8rs", "vgv8gfd9");
	
		    // Encrypt
		    String encrypted = tde.encryptString("INQUIRY");
		    System.out.println("Encrypted: " + encrypted);
	
		    // Decrypt
		    String decrypted = tde.decryptString(encrypted);
		    System.out.println("Decrypted: " + decrypted);
		}
    	catch (Exception e)
		{
    		System.out.println(e.getMessage());
		}
	}
}
