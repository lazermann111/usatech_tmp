package com.usatech.aramark;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AramarkException extends Exception
{
    private static final long serialVersionUID = 1L;
    private String source;			//exception source
	private String description;		//exception description
	private String status; 			//transaction status
	private String statusCode; 		//transaction status code
	private String responseMessage;	//response message from POS system
	private String responseText;	//response text from POS system

	private static final String classInfo = "AramarkGateway::" + AramarkException.class.getName();
	private static Log log = LogFactory.getLog(classInfo);

	public AramarkException(String source, String description, String status, String statusCode, String responseMessage, String responseText)
	{
	    if (source == null)
			this.source = "";
		else
			this.source = source;
		if (description == null)
			this.description = "";
		else
			this.description = description;
		if (status == null)
			this.status = "";
		else
			this.status = status;
		if (statusCode == null)
			this.statusCode = "";
		else
			this.statusCode = statusCode;
		if (responseMessage == null)
			this.responseMessage = "";
		else
			this.responseMessage = responseMessage;
		if (responseText == null)
			this.responseText = "";
		else
			this.responseText = responseText;
		if (log.isWarnEnabled())
		{
		    log.warn("Error source: " + source + ", description: " + description + ", status: " + status + ", statusCode: " + statusCode + ", responseMessage: " + responseMessage + ", responseText: " + responseText );
		}
	}

	public AramarkException(String source, String description)
	{
	    if (source == null)
			this.source = "";
		else
			this.source = source;
		if (description == null)
			this.description = "";
		else
			this.description = description;
		status = "";
		statusCode = "";
		responseMessage = "";
		responseText = "";
		if (log.isWarnEnabled())
		{
		    log.warn("Error source: " + source + ", description: " + description);
		}
	}

	public AramarkException(String source, Throwable e)
	{
	    if (source == null)
			this.source = "";
		else
			this.source = source;
		description = e.getMessage();
		status = "";
		statusCode = "";
		responseMessage = "";
		responseText = "";
		super.setStackTrace(e.getStackTrace());
		if (log.isWarnEnabled())
		{
		    log.warn("Error source: " + source + ", description: " + description + ", Stack Trace: " + com.usatech.util.Util.getStackTraceAsString(e));
		}
	}

    public AramarkException(String source, String description, Throwable e)
    {
        if (source == null)
            this.source = "";
        else
            this.source = source;
        if (description == null)
            this.description = "";
        else
            this.description = description;
        status = "";
        statusCode = "";
        responseMessage = "";
        responseText = "";
        super.setStackTrace(e.getStackTrace());
        if (log.isWarnEnabled())
        {
            log.warn("Error source: " + source + ", description: " + description + ", Stack Trace: " + com.usatech.util.Util.getStackTraceAsString(e));
        }
    }

	public String getResponseMessage()
	{
		return responseMessage;
	}

	public String getResponseText()
	{
		return responseText;
	}

	public String getDescription()
	{
		return description;
	}

	public String getStatus()
	{
		return status;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	/**
	 * Returns a String representation of this object.
	 */
	public String toString()
	{
		return "Source: " + source + ", description: " + description + ", status: " + status + ", statusCode: " + statusCode + ", responseMessage: " + responseMessage + ", responseText: " + responseText;
	}
}
