package com.usatech.aramark;

import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class TripleDesEncryption {
    private Cipher ecipher, dcipher;
    private String cryptoKey, cryptoIV;
    
    private static final String classInfo = "AramarkGateway::" + TripleDesEncryption.class.getName();

    public TripleDesEncryption(String cryptoKey, String cryptoIV) throws AramarkException 
	{
    	if ((cryptoKey == null) || (cryptoKey.length() != 24) || (cryptoIV == null) || (cryptoIV.length() != 8))
        {
    		throw new AramarkException(classInfo, "Incorrect encryption key format");
        }
    	
        try 
		{
        	this.cryptoKey = cryptoKey;
        	this.cryptoIV = cryptoIV;
        	
        	DESedeKeySpec keySpec = new DESedeKeySpec(cryptoKey.getBytes());
            SecretKey key = SecretKeyFactory.getInstance("DESede").generateSecret(keySpec);
            
            ecipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            dcipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");

            IvParameterSpec paramSpec = new IvParameterSpec(cryptoIV.getBytes());

            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
        } 
        /*catch (java.security.InvalidAlgorithmParameterException e) {
        } catch (java.security.spec.InvalidKeySpecException e) {
        } catch (javax.crypto.NoSuchPaddingException e) {
        } catch (java.security.NoSuchAlgorithmException e) {
        } catch (java.security.InvalidKeyException e) {
        } */
        catch (Exception e) 
		{
        	throw new AramarkException(classInfo, e);
        }
    }

    public String encryptString(String str) throws AramarkException 
	{
    	if ((str == null) || (str.length() == 0))
    	{
    		return null;
    	}
        	
    	try
		{
    		str = URLEncoder.encode(str, "UTF8");
		}
    	catch (Exception e)
		{
    		str = str.replace(' ', '+');
		}
        	
        try 
		{
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return new simple.lang.sun.misc.BASE64Encoder().encode(enc);
        } 
        /*catch (javax.crypto.BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        }*/
        catch (Exception e) 
		{
        	throw new AramarkException(classInfo, e);
        }
    }

    public String decryptString(String str) throws AramarkException 
	{    
    	if ((str == null) || (str.length() == 0))
    	{
    		return null;
    	}
    	
    	try 
		{	
            // Decode base64 to get bytes
            byte[] dec = new simple.lang.sun.misc.BASE64Decoder().decodeBuffer(str);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            String decrStr = new String(utf8, "UTF8");
            
            try
			{
	    		decrStr = URLDecoder.decode(decrStr, "UTF8");
			}
	    	catch (Exception e)
			{
	    		decrStr = decrStr.replace('+', ' ');
			}
	    	
	    	return decrStr;
        } 
        /*catch (javax.crypto.BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (java.io.IOException e) {
        }*/
        catch (Exception e) 
		{
        	throw new AramarkException(classInfo, e);
        }
    }
    
    
	/**
	 * @return Returns the cryptoIV.
	 */
	public String getCryptoIV() {
		return cryptoIV;
	}
	/**
	 * @return Returns the cryptoKey.
	 */
	public String getCryptoKey() {
		return cryptoKey;
	}
}



