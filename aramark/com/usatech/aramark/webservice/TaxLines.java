/**
 * TaxLines.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.usatech.aramark.webservice;

public class TaxLines  implements java.io.Serializable {
    private java.lang.String taxAuthorityID;
    private java.lang.String useTax;

    public TaxLines() {
    }

    public java.lang.String getTaxAuthorityID() {
        return taxAuthorityID;
    }

    public void setTaxAuthorityID(java.lang.String taxAuthorityID) {
        this.taxAuthorityID = taxAuthorityID;
    }

    public java.lang.String getUseTax() {
        return useTax;
    }

    public void setUseTax(java.lang.String useTax) {
        this.useTax = useTax;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TaxLines)) return false;
        TaxLines other = (TaxLines) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.taxAuthorityID==null && other.getTaxAuthorityID()==null) || 
             (this.taxAuthorityID!=null &&
              this.taxAuthorityID.equals(other.getTaxAuthorityID()))) &&
            ((this.useTax==null && other.getUseTax()==null) || 
             (this.useTax!=null &&
              this.useTax.equals(other.getUseTax())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTaxAuthorityID() != null) {
            _hashCode += getTaxAuthorityID().hashCode();
        }
        if (getUseTax() != null) {
            _hashCode += getUseTax().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TaxLines.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://relayservice.webservices.educationaramark.com", "TaxLines"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAuthorityID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TaxAuthorityID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("useTax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UseTax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
