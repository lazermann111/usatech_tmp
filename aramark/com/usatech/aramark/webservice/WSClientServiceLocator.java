/**
 * WSClientServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.usatech.aramark.webservice;

public class WSClientServiceLocator extends org.apache.axis.client.Service implements com.usatech.aramark.webservice.WSClientService {

    // Use to get a proxy class for aramarkgateway
    private final java.lang.String aramarkgateway_address = "http://localhost:9100/axis/services/aramarkgateway";

    public java.lang.String getaramarkgatewayAddress() {
        return aramarkgateway_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String aramarkgatewayWSDDServiceName = "aramarkgateway";

    public java.lang.String getaramarkgatewayWSDDServiceName() {
        return aramarkgatewayWSDDServiceName;
    }

    public void setaramarkgatewayWSDDServiceName(java.lang.String name) {
        aramarkgatewayWSDDServiceName = name;
    }

    public com.usatech.aramark.webservice.WSClient getaramarkgateway() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(aramarkgateway_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getaramarkgateway(endpoint);
    }

    public com.usatech.aramark.webservice.WSClient getaramarkgateway(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.aramark.webservice.AramarkgatewaySoapBindingStub _stub = new com.usatech.aramark.webservice.AramarkgatewaySoapBindingStub(portAddress, this);
            _stub.setPortName(getaramarkgatewayWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.aramark.webservice.WSClient.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.aramark.webservice.AramarkgatewaySoapBindingStub _stub = new com.usatech.aramark.webservice.AramarkgatewaySoapBindingStub(new java.net.URL(aramarkgateway_address), this);
                _stub.setPortName(getaramarkgatewayWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("aramarkgateway".equals(inputPortName)) {
            return getaramarkgateway();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:aramark.usatech.com", "WSClientService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("aramarkgateway"));
        }
        return ports.iterator();
    }

}
