/**
 * AramarkgatewaySoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.usatech.aramark.webservice;

public class AramarkgatewaySoapBindingImpl implements com.usatech.aramark.webservice.WSClient{
    private com.usatech.aramark.WSClient wsc;
    private com.usatech.aramark.webservice.POSResponse response;
    private com.educationaramark.webservices.relayservice.POSResponse serverResponse;
    
    public AramarkgatewaySoapBindingImpl() throws AramarkException, Exception
    {
    	wsc = new com.usatech.aramark.WSClient();
    }
    
    private void parseResponse()
    {
    	com.educationaramark.webservices.relayservice.POSResponse_Transaction serverTran = null;
    	com.usatech.aramark.webservice.POSResponse_Transaction tran = null;
    	com.educationaramark.webservices.relayservice.POSResponse_Customer serverCustomer = null;
    	com.usatech.aramark.webservice.POSResponse_Customer customer = null;

    	response = new com.usatech.aramark.webservice.POSResponse();
		response.setAccount(serverResponse.getAccount());
		
		serverTran = serverResponse.getTransaction();
		tran = new com.usatech.aramark.webservice.POSResponse_Transaction();
    	tran.setBeginDateTimeStamp(serverTran.getBeginDateTimeStamp());
    	tran.setResponseMessage(serverTran.getResponseMessage());
    	tran.setResponseText(serverTran.getResponseText());
    	tran.setStatus(serverTran.getStatus());
    	tran.setStatusCode(serverTran.getStatusCode());
    	tran.setTransactionTypeCode(serverTran.getTransactionTypeCode());

    	serverCustomer = serverTran.getCustomer();
    	customer = new com.usatech.aramark.webservice.POSResponse_Customer();
    	customer.setAmount(serverCustomer.getAmount());
    	customer.setCusotmerIssueID(serverCustomer.getCusotmerIssueID());
    	customer.setCustomerID(serverCustomer.getCustomerID());
    	customer.setFirstName(serverCustomer.getFirstName());
    	customer.setLastName(serverCustomer.getLastName());

    	com.educationaramark.webservices.relayservice.ArrayOfTaxLines serverTax = serverCustomer.getTax();
    	if (serverTax != null)
    	{
    		com.educationaramark.webservices.relayservice.TaxLines[] serverTaxLines = serverTax.getTaxLines();
    		com.usatech.aramark.webservice.ArrayOfTaxLines tax = new com.usatech.aramark.webservice.ArrayOfTaxLines();
    		com.usatech.aramark.webservice.TaxLines[] taxLines = new com.usatech.aramark.webservice.TaxLines[serverTaxLines.length];
   
    		for (int i=0; i < serverTaxLines.length; i++)
    		{
    			taxLines[i] = new com.usatech.aramark.webservice.TaxLines();
    			taxLines[i].setTaxAuthorityID(serverTaxLines[i].getTaxAuthorityID());
    			taxLines[i].setUseTax(serverTaxLines[i].getUseTax());
    		}
    		
    		tax.setTaxLines(taxLines);
    		customer.setTax(tax);
    	}
    	
    	tran.setCustomer(customer);
    	response.setTransaction(tran);
    }

    public com.usatech.aramark.webservice.POSResponse inquiry(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String result_Date, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) 
    	throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException 
	{	
    	try
		{
	    	response = null;
			serverResponse = wsc.inquiry(remoteServerAddress, account, cardNumber, result_Date, operatorID, transactionID, revenueCenterID, offline, enableEncryption, cryptoKey, cryptoIV);
			if (serverResponse != null) parseResponse();
	    	return response;
		}
    	catch (com.usatech.aramark.AramarkException ae)
		{
    		throw new com.usatech.aramark.webservice.AramarkException(ae.getDescription(), ae.getResponseMessage(), ae.getResponseText(), ae.getStatus(), ae.getStatusCode());
		}
    }
    
    public com.usatech.aramark.webservice.POSResponse decliningBalance(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String result_Date, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String debitAmount, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) 
    	throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException
	{
    	try
		{
	    	response = null;
			serverResponse = wsc.decliningBalance(remoteServerAddress, account, cardNumber, result_Date, operatorID, transactionID, revenueCenterID, offline, debitAmount, enableEncryption, cryptoKey, cryptoIV);
	    	if (serverResponse != null) parseResponse();
	    	return response;
		}
    	catch (com.usatech.aramark.AramarkException ae)
		{
    		throw new com.usatech.aramark.webservice.AramarkException(ae.getDescription(), ae.getResponseMessage(), ae.getResponseText(), ae.getStatus(), ae.getStatusCode());
		}
	}
    
    public com.usatech.aramark.webservice.POSResponse credit(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String result_Date, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String creditAmount, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) 
    	throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException
	{
    	try
		{
	    	response = null;
			serverResponse = wsc.credit(remoteServerAddress, account, cardNumber, result_Date, operatorID, transactionID, revenueCenterID, offline, creditAmount, enableEncryption, cryptoKey, cryptoIV);
	    	if (serverResponse != null) parseResponse();
	    	return response;
		}
		catch (com.usatech.aramark.AramarkException ae)
		{
			throw new com.usatech.aramark.webservice.AramarkException(ae.getDescription(), ae.getResponseMessage(), ae.getResponseText(), ae.getStatus(), ae.getStatusCode());
		}
	}
    
    public com.usatech.aramark.webservice.POSResponse board(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String result_Date, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String unitCount, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) 
    	throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException
	{
    	try
		{
	    	response = null;
	    	serverResponse = wsc.board(remoteServerAddress, account, cardNumber, result_Date, operatorID, transactionID, revenueCenterID, offline, unitCount, enableEncryption, cryptoKey, cryptoIV);
	    	if (serverResponse != null) parseResponse();
	    	return response;
		}
    	catch (com.usatech.aramark.AramarkException ae)
		{
    		throw new com.usatech.aramark.webservice.AramarkException(ae.getDescription(), ae.getResponseMessage(), ae.getResponseText(), ae.getStatus(), ae.getStatusCode());
		}
	}
    
    public com.usatech.aramark.webservice.POSResponse cashEquivalency(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String result_Date, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String cashAmount, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) 
    	throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException
	{
    	try
		{
	    	response = null;
			serverResponse = wsc.cashEquivalency(remoteServerAddress, account, cardNumber, result_Date, operatorID, transactionID, revenueCenterID, offline, cashAmount, enableEncryption, cryptoKey, cryptoIV);
	    	if (serverResponse != null) parseResponse();
	    	return response;
		}
    	catch (com.usatech.aramark.AramarkException ae)
		{
    		throw new com.usatech.aramark.webservice.AramarkException(ae.getDescription(), ae.getResponseMessage(), ae.getResponseText(), ae.getStatus(), ae.getStatusCode());
		}
	}
    
    public String getVersion()
    {
    	return wsc.getVersion();
    }
    
    public com.usatech.aramark.webservice.POSResponse sampleMessage() 
    	throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException
	{
    	try
		{
	    	response = null;
			serverResponse = wsc.sampleMessage();
	    	if (serverResponse != null) parseResponse();
	    	return response;
		}
    	catch (com.usatech.aramark.AramarkException ae)
		{
    		throw new com.usatech.aramark.webservice.AramarkException(ae.getDescription(), ae.getResponseMessage(), ae.getResponseText(), ae.getStatus(), ae.getStatusCode());
		}
	}
}
