/**
 * WSClientService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.usatech.aramark.webservice;

public interface WSClientService extends javax.xml.rpc.Service {
    public java.lang.String getaramarkgatewayAddress();

    public com.usatech.aramark.webservice.WSClient getaramarkgateway() throws javax.xml.rpc.ServiceException;

    public com.usatech.aramark.webservice.WSClient getaramarkgateway(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
