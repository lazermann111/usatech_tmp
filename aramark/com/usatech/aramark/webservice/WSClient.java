/**
 * WSClient.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis WSDL2Java emitter.
 */

package com.usatech.aramark.webservice;

public interface WSClient extends java.rmi.Remote {
    public java.lang.String getVersion() throws java.rmi.RemoteException;
    public com.usatech.aramark.webservice.POSResponse inquiry(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String resultDate, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException;
    public com.usatech.aramark.webservice.POSResponse decliningBalance(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String resultDate, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String debitAmount, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException;
    public com.usatech.aramark.webservice.POSResponse credit(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String resultDate, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String creditAmount, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException;
    public com.usatech.aramark.webservice.POSResponse board(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String resultDate, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String unitCount, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException;
    public com.usatech.aramark.webservice.POSResponse cashEquivalency(java.lang.String remoteServerAddress, java.lang.String account, java.lang.String cardNumber, java.lang.String resultDate, java.lang.String operatorID, java.lang.String transactionID, java.lang.String revenueCenterID, java.lang.String offline, java.lang.String cashAmount, java.lang.String enableEncryption, java.lang.String cryptoKey, java.lang.String cryptoIV) throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException;
    public com.usatech.aramark.webservice.POSResponse sampleMessage() throws java.rmi.RemoteException, com.usatech.aramark.webservice.AramarkException;
}
