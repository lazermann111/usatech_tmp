#!/usr/bin/perl

use strict;
use SOAP::Lite;

#my $WSGatewayURL = "http://localhost:9900/axis/services/aramarkgateway";
my $WSGatewayURL = "http://usaapr01.usatech.com:9100/axis/services/aramarkgateway";
#Port 12345 is used for tracing SOAP messages in TcpTunnelGui tool
#my $WSGatewayURL = "http://localhost:12345/axis/services/aramarkgateway";
my $tranCode = "INQUIRY";
my $tranRemoteServerAddress = "http://demo.aramarkcampusit.com/Relay_V2/RelayService.asmx";
#my $tranAccount = "ScanPlus_90";
my $tranAccount = "SCanPlus_ENCRYPT_USATECH";
#my $tranAccount = "ScanPlus_USATECH";
#my $tranCardNumber = "999999999";
my $tranCardNumber = "6014760029279301";
#my $tranCardNumber = "20000000000025";
my $tranResultDate = "2005-09-30T10:05:00.0000000-04:00";
my $tranOperatorID = "";
my $tranID = "123456";
my $tranRevenueCenterID = "";
my $tranOffline = "FALSE";
my $tranDebitAmount = "1.00";
my $tranCreditAmount = "1.00";
my $tranCashAmount = "1.00";
my $tranUnitCount = "1";
my $tranEnableEncryption = "true";
my $tranCryptoKey = "g9f9tr4j34t66lkjgovjo8rs";
my $tranCryptoIV = "vgv8gfd9";

print "\nConnecting to Aramark Gateway - $tranCode...\n\n";

my $remoteServerAddress = SOAP::Data->type('string');
$remoteServerAddress->name('remoteServerAddress');
$remoteServerAddress->value($tranRemoteServerAddress);

my $account = SOAP::Data->type('string');
$account->name('account');
$account->value($tranAccount);

my $cardNumber = SOAP::Data->type('string');
$cardNumber->name('cardNumber');
$cardNumber->value($tranCardNumber);

my $resultDate = SOAP::Data->type('string');
$resultDate->name('resultDate');
$resultDate->value($tranResultDate);

my $operatorID = SOAP::Data->type('string');
$operatorID->name('operatorID');
$operatorID->value($tranOperatorID);

my $transactionID = SOAP::Data->type('string');
$transactionID->name('transactionID');
$transactionID->value($tranID);

my $revenueCenterID = SOAP::Data->type('string');
$revenueCenterID->name('revenueCenterID');
$revenueCenterID->value($tranRevenueCenterID);

my $offline = SOAP::Data->type('string');
$offline->name('offline');
$offline->value($tranOffline);

my $enableEncryption = SOAP::Data->type('string');
$enableEncryption->name('enableEncryption');
$enableEncryption->value($tranEnableEncryption);

my $cryptoKey = SOAP::Data->type('string');
$cryptoKey->name('cryptoKey');
$cryptoKey->value($tranCryptoKey);

my $cryptoIV = SOAP::Data->type('string');
$cryptoIV->name('cryptoIV');
$cryptoIV->value($tranCryptoIV);

my $tranAmount;
my $unitCount;

if ($tranCode eq 'DECLININGBALANCE')
{
	$tranAmount = SOAP::Data->type('string');
	$tranAmount->name('debitAmount');
	$tranAmount->value($tranDebitAmount);
}
elsif ($tranCode eq 'CREDIT')
{
	$tranAmount = SOAP::Data->type('string');
	$tranAmount->name('creditAmount');
	$tranAmount->value($tranCreditAmount);
}
elsif ($tranCode eq 'CASHEQUIVALENCY')
{
	$tranAmount = SOAP::Data->type('string');
	$tranAmount->name('cashAmount');
	$tranAmount->value($tranCashAmount);
}
elsif ($tranCode eq 'BOARD')
{
	$unitCount = SOAP::Data->type('string');
	$unitCount->name('unitCount');
	$unitCount->value($tranUnitCount);
}

my $ws = SOAP::Lite
	#-> outputxml("1")
    #-> uri('urn:webservice.aramark.usatech.com')
    -> proxy($WSGatewayURL);

my $response;
my $proceed = 1;
my $processResponse = 1;

if ($tranCode eq 'INQUIRY')
{
	$response = $ws->inquiry($remoteServerAddress, $account, $cardNumber, $resultDate, $operatorID, $transactionID, $revenueCenterID, $offline, $enableEncryption, $cryptoKey, $cryptoIV);
}
elsif ($tranCode eq 'DECLININGBALANCE')
{
	$response = $ws->decliningBalance($remoteServerAddress, $account, $cardNumber, $resultDate, $operatorID, $transactionID, $revenueCenterID, $offline, $tranAmount, $enableEncryption, $cryptoKey, $cryptoIV);
}
elsif ($tranCode eq 'CREDIT')
{
	$response = $ws->credit($remoteServerAddress, $account, $cardNumber, $resultDate, $operatorID, $transactionID, $revenueCenterID, $offline, $tranAmount, $enableEncryption, $cryptoKey, $cryptoIV);
}
elsif ($tranCode eq 'CASHEQUIVALENCY')
{
	$response = $ws->cashEquivalency($remoteServerAddress, $account, $cardNumber, $resultDate, $operatorID, $transactionID, $revenueCenterID, $offline, $tranAmount, $enableEncryption, $cryptoKey, $cryptoIV);
}
elsif ($tranCode eq 'BOARD')
{
	$response = $ws->board($remoteServerAddress, $account, $cardNumber, $resultDate, $operatorID, $transactionID, $revenueCenterID, $offline, $unitCount, $enableEncryption, $cryptoKey, $cryptoIV);
}
elsif ($tranCode eq 'SAMPLEMESSAGE')
{
	$response = $ws->sampleMessage();
}
elsif ($tranCode eq 'GETVERSION')
{
	$response = $ws->getVersion();
	$processResponse = 0;
}
else
{
	print "Unsupported transaction type code\n";
	$proceed = 0;
}

if ($proceed == 1)
{
	unless ($response->fault) 
	{
		if ($processResponse == 1)
		{
		   my $POSResponse = $response->result;
		   my $POSTransaction = $POSResponse->{Transaction};
		   my $POSCustomer = $POSTransaction->{Customer};
		   
		   print "-------------------- POS $tranCode RESULTS --------------------------\n";
		   print "Account = " . $POSResponse->{Account} . "\n";
		   print "DateTime = " . $POSTransaction->{BeginDateTimeStamp} . "\n";
		   print "CardNumber = " . $POSCustomer->{CustomerID} . "\n";
		   print "CardIssue = " . $POSCustomer->{CusotmerIssueID} . "\n";
		   print "Balance = " . $POSCustomer->{Amount} . "\n";
		   print "ClientFirstName = " . $POSCustomer->{FirstName} . "\n";
		   print "ClientLastName = " . $POSCustomer->{LastName} . "\n";
		   print "ResponseMessage(Customer) = ". $POSTransaction->{ResponseMessage} . "\n";
		   print "ResponseText(Error) = ". $POSTransaction->{ResponseText} . "\n";
		   print "Status = ". $POSTransaction->{Status} . "\n";
		   print "StatusCode = ". $POSTransaction->{StatusCode} . "\n"; 
		   print "-------------------------------------------------------------------\n";
		}
		elsif ($tranCode eq 'GETVERSION')
		{
			print "Aramark Gateway Version = " . $response->result . "\n";
		}
	} 
	else 
	{		
	  	my $errstr = "Exception occurred:\nfaultcode=" . $response->faultcode . "\nfaultstring=" . $response->faultstring . "\n";
	    if (ref($response->faultdetail) eq 'HASH')
	    {
	    	my %faultdetail = %{$response->faultdetail};
	    	$errstr .= "exceptionName=" . $faultdetail{exceptionName} . "\n";
	    	my %fault = %{$faultdetail{fault}};
	    	foreach my $key (keys %fault)
	    	{
	    		$errstr .= $key . "=" . $fault{$key} . "\n";
	    	}
	    }
	    else
	    {
	    	$errstr .= "faultdetail=" . $response->faultdetail;
	    }
	    print $errstr;
	}
}
 
print "\nFinished\n";
