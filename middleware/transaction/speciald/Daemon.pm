package Daemon;
use strict;
use Evend::SpecialCard::SpecialCardHandler;
use Net::Daemon;
use Evend::Database::Database;
use lib '/opt/speciald';
use vars qw($VERSION @ISA);
$VERSION = '0.01';
@ISA = qw(Net::Daemon);

sub Run
{


	my $self = shift;
	my $socket = $self->{socket};

	my $specialcard = Evend::SpecialCard::SpecialCardHandler->new();
	my $timestamp = localtime();

	warn("new connection at $timestamp\n");

	while (my $line = $socket->getline())
	{

		chomp $line;
		chop $line if $line =~ /\r$/;

		warn("inbound line: $line\n");
                warn("command: $specialcard->{command}\n");

		$specialcard->parse($line);

		$specialcard->setup();

		$specialcard->authorize($line) if ($specialcard->{command} eq 't1');
		$specialcard->batch($line) if ($specialcard->{command} eq 't2');
		$specialcard->force_sale($line) if ($specialcard->{command} eq 't4');

		foreach (keys %$specialcard)
		{
			warn("$_ -> $specialcard->{$_}\n");
		}

		print $socket "$specialcard->{return_code},$specialcard->{current_balance},$specialcard->{magstrip}\n";
		

	}

	$specialcard->cleanup();


}


1;

