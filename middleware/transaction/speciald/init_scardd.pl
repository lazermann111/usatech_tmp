#!/bin/perl

use lib '/opt/speciald';
use strict;
use Daemon;

my ($child_pid, $server);

my $server = Daemon->new({
	pidfile   => '/opt/speciald/speciald.pid', 
	localport => 10103,
	logfile   => '/opt/speciald/speciald.log',
	mode      => 'fork',
});
$server->Bind();


