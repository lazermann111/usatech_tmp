require 5.004;

use strict;
use Evend::Database::Database;
use CardUtils;
use IO::Socket;
use IO::Select;
require 'pss_interface.pm';

sub logit;

my $BatchSettleHours = 22;
my $BatchSettleTransactions = 500;

my $DATABASE = Evend::Database::Database->new(	print_query => 1, 
												execute_flatfile=>1, 
												debug => 1, 
												debug_die => 0);
					
	# kick off the batch settlement for KODAK

	# Because of "Quick" PSS limitations we need to provide
	# a KODAK terminal number for the process to initiate
	# T. Shannon 6/22/2003
	
	# KODAK batch...
	
	logit "Batching KODAK...";
	
	my $client_type = 'KODAK';
	my ($client_id, $result, $result_text) = (0,'', '');
	
	my $array_ref = $DATABASE->select(
					table			=> 'quick_pss_merchant@pss_tulsap1',
					select_columns	=> 'client_id',
					where_columns	=> [ 'client_type = ?'],
					where_values	=> [ $client_type ]
				);
	if( ! ($array_ref->[0]) )
	{
		logit "Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
	}
	$client_id = $array_ref->[0][0];

	($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);
	
	logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
	logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
	logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

	# Sony batch...
	
	logit "Batching SONY...";

	$client_type = 'SONY';				
	$array_ref = $DATABASE->select(
					table			=> 'quick_pss_merchant@pss_tulsap1',
					select_columns	=> 'client_id',
					where_columns	=> [ 'client_type = ?'],
					where_values	=> [ $client_type ]
				);
	if( ! ($array_ref->[0]) )
	{
		logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
	}
	$client_id = $array_ref->[0][0];

	($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);
	
	logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
	logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
	logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";
					
	# E-Port batch...
	
	logit "Batching EPORT...";
	
	$client_type = 'EPORT';				
	$array_ref = $DATABASE->select(
					table			=> 'quick_pss_merchant@pss_tulsap1',
					select_columns	=> 'client_id',
					where_columns	=> [ 'client_type = ?'],
					where_values	=> [ $client_type ]
				);
	if( ! ($array_ref->[0]) )
	{
		logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
	}
	$client_id = $array_ref->[0][0];

	($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);
	
	logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
	logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
	logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";
	
	# Coca Cola batch...
	
	logit "Batching Coca Cola...";
	
	$client_type = 'COKE';				
	$array_ref = $DATABASE->select(
					table			=> 'quick_pss_merchant@pss_tulsap1',
					select_columns	=> 'client_id',
					where_columns	=> [ 'client_type = ?'],
					where_values	=> [ $client_type ]
				);
	if( ! ($array_ref->[0]) )
	{
		logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
	}
	
	$client_id = $array_ref->[0][0];

	($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);
	
	logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
	logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
	logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

	# USA Technologies batch...
	
	logit "Batching USA Technologies...";
	
	$client_type = 'USA';				
	$array_ref = $DATABASE->select(
					table			=> 'quick_pss_merchant@pss_tulsap1',
					select_columns	=> 'client_id',
					where_columns	=> [ 'client_type = ?'],
					where_values	=> [ $client_type ]
				);
	if( ! ($array_ref->[0]) )
	{
		logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
	}
	
	$client_id = $array_ref->[0][0];

	($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);
	
	logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
	logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
	logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # Citigroup Revalue Unit batch...
        
        logit "Batching Citigroup Revalue Unit ...";
        
        $client_type = 'CITIREVALU';                          
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }
        
        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # Soda Snack Vending batch...

        logit "Batching Soda Snack Vending ...";

        $client_type = 'SODASNACKV';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # Credit Revalue Station batch...

        logit "Batching Credit Revalue Station ...";

        $client_type = 'CRDRVALSTN';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # Standard Change Tokens batch...

        logit "Batching Standard Change Tokens ...";

        $client_type = 'STDCHGTOKN';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # Desert Car Wash batch...

        logit "Desert Car Wash Tokens ...";

        $client_type = 'DESERTCARW';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

	# Altria batch...

        logit "Altria E-Port ...";

        $client_type = 'ALTRIA';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # EPort 0405 batch...

        logit "E-Port 0405 ...";

        $client_type = 'EPORT0405';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # Flik Internation @ CSFB batch...

        logit "Flik International @ CSFB ...";

        $client_type = 'FLIKCSFB';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # WFCV...

        logit "World Financial Center Vending ...";

        $client_type = 'WFCV';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # Range Servant...

        logit "Range Servant Golf Vending ...";

        $client_type = 'RANGESERV';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # Russell Speeder's NE2 ...

        logit "Russell Speeder's NE2  ...";

        $client_type = 'RUSLSPEED';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";

        # USA Tech Vending Equipment Sales...

        logit "USA Tech Vending Equipment Sales ...";

        $client_type = 'USATEQUIP';
        $array_ref = $DATABASE->select(
                                        table                   => 'quick_pss_merchant@pss_tulsap1',
                                        select_columns  => 'client_id',
                                        where_columns   => [ 'client_type = ?'],
                                        where_values    => [ $client_type ]
                                );
        if( ! ($array_ref->[0]) )
        {
                logit "    Cannot find client type $client_type in QUICK_PSS table. Batch Settlement not processed.";
        }

        $client_id = $array_ref->[0][0];

        ($result, $result_text) = pss_BatchSettleConditional($client_id,'',$BatchSettleHours, $BatchSettleTransactions);

        logit "FHMS->talk() - BATCH_SETTLE - $client_type...(derived from $client_id)";
        logit "FHMS->talk() - BATCH_SETTLE - Result     : $result";
        logit "FHMS->talk() - BATCH_SETTLE - Response   : $result_text";





sub logit
{
	my $line = shift;
	warn "[" . localtime() . "] $line\n";
}

