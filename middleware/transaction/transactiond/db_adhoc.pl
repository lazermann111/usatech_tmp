#!/bin/perl -w

require 5.004;

use strict;
use Evend::Database::Database;

sub logit($);

logit ("Creating database handle...");
my $DATABASE = Evend::Database::Database->new(
						print_query			=> 1, 
						execute_flatfile	=> 1, 
						debug				=> 1, 
						debug_die			=> 0);
logit ("...Database handle created successfully");

my $ssn = 'E4070692'; 

$ssn = unpack("H*", $ssn);

my $array_ref = $DATABASE->select(
		table			=> 'dual',
		select_columns	=> 'rerix_machine_id');


my $machine_id = $array_ref->[0][0];


$DATABASE->insert(
		table			=> 'Rerix_modem_to_serial',
		insert_columns	=> 'modem_id, machine_id, '.
							'network, ' .
							'MACHINE_INDEX',
		insert_values	=> [$ssn, $machine_id,
							'P', 0]);


#$DATABASE->update(
#	table			=> 'Rerix_modem_to_serial',
#	update_columns	=> 'modem_id',
#	update_values	=> [$ssn],
#	where_columns	=> ['machine_id = ?'],
#	where_values	=> ['EV17116']
#);


#$DATABASE->query ( query => qq{delete machine_command_inbound_hist
#                            WHERE machine_id = ?},
#                  values => ['EV17116']);


#$DATABASE->query ( query => qq{delete machine_command
#                           WHERE modem_id = ?},
#                  values => ['EV17116']);

#--- inserts new rerix_initialization
#$DATABASE->insert(
#		table			=> 'Rerix_initialization',
#		insert_columns	=> 'INITIALIZATION_ID, MODEM_ID, '.
#							'INITIALIZATION_DATE, SERIAL_NUMBER, ' .
#							'MODEL_NUMBER,BUILD_STANDARD, SILICON_SERIAL_NUMBER,' . 
#							'DECIMAL_POINT_POSITION, COIN_MECH_TYPE, ESCROW_MODE' ,
#		insert_values	=> [11753, $ssn, "05/22/2003",
#							  'EV17120', 'G5', 1, $ssn, '0', '0', '0']);

#$DATABASE->update(
#	table			=> 'Rerix_initialization',
#	update_columns	=> 'modem_id, silicon_serial_number',
#	update_values	=> [$ssn, $ssn],
#	where_columns	=> ['initialization_id = ?'],
#	where_values	=> ['11753']
#);

#my ($machine_trans_no, $magstrip, $track2) =  ('T:88030EF1:595', 
#												'B4217661182786251^MENDOZA/SERGIO^0607101721910000318000000', 
#												'4217661182786251=0607');

#my $txn_ref = $DATABASE->select(
#					table			=> 'Transaction_Record',
#					select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
#										'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
#										'AUTH_ACTION_CD, TRANS_NO, ' .
#										'FORCE_ACTION_CD',
#					where_columns	=> ['(MACHINE_TRANS_NO = ?) and (MAG_STRIPE = ? or MAG_STRIPE = ?)'],
#					where_values	=> [$machine_trans_no, $magstrip, $track2] );	

#logit "query returned - trans_no = $txn_ref->[0][6]";


#logit ("The machine_id = $machine_id\n");4534313233343536

sub logit($)
{
	my $line = shift;

	print "[" . localtime() . "] $line\n";
	
}
