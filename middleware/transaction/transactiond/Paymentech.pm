package Paymentech;
require 5.004;

use strict;
use Evend::Database::Database;
use IO::Socket;
use IO::Select;

BEGIN {
use Exporter	();
use vars	qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
$VERSION	= '0.01';
@ISA		= qw(Exporter);
    
#export by default
@EXPORT		= qw();

#export on demand
@EXPORT_OK	= qw();

#global/constants
use vars	qw( $STX $ETX $FS 
                    $CLIENT_NUMBER $SYSTEM_INFO %MERCHANT_ID
                    $NUM_TERMINALS $SALE $AUTH_ONLY $FORCE $RETURN $VOID $BATCH_INQUIRY $BATCH_RELEASE );

# all these can be access outside of this module by ThisModuleName:: operator

$STX = chr(0x2);  # start transmission indicator
$ETX = chr(0x3);  # end transmission indicator
$FS  = chr(0x1c); # file separator indicator


$CLIENT_NUMBER = '0002';
$SYSTEM_INFO = 'EVNDNET041200Version1.0';
%MERCHANT_ID = (
                  KODAK => "311000402212",
                  METAB => "310990375735",
                  PEPSI => "311000603520",
                  GENERAL => '311000603546',
                  KDKPM => "311000900850",   
                  KDKDK => "311000900850",
                  SONY => "300003978531"
               );

    
sub new
{
	warn "\n*** ENTERING Paymentech.pm new()\n";
   my ($class, %arg) = @_;


   my $ORACLE = new Evend::Database::Database(print_query=>1);

   my $group_code;
   if ($arg{group_code}) { $group_code = $arg{group_code}; }
   else {
   warn "*** About to perform query \n";
      my $group_rec =  $ORACLE->select(table          => 'machine_hierarchy',
                                             select_columns => 'Group_Cd',
                                             where_columns  => ['Machine_Id=?'],
                                             where_values   => [$arg{machine}] );
	warn "*** processed query\n";
      $group_code = $group_rec->[0][0];
   }
   $ORACLE->close();
	$group_code ||= 'GENERAL';


   warn("**************** MATT  group code is $group_code\n");
   warn("$MERCHANT_ID{$group_code} $MERCHANT_ID{'GENERAL'}");

   my $this = {
		NUM_TERMINALS 		=> 50,
		SALE		=> '01',
		AUTH_ONLY	=> '02',
		FORCE		=> '03',
		RETURN		=> '06',
		VOID		=> '41',
		BATCH_INQUIRY	=> '50',
		BATCH_RELEASE	=> '51',
               MACHINE_ID          => $arg{machine},
               GROUP_CODE          => $group_code,
               MERCHANT_NUMBER     => $MERCHANT_ID{$group_code}
											|| $MERCHANT_ID{'GENERAL'},
               TRANSACTION_TYPE    => $arg{transaction_type},
               TERMINAL_NUMBER     => '000',
               SEQUENCE_NUMBER     => '000000',
               TIMED_OUT => 0, # just to make this explicit
               ACTION_CODE         => 'T',
               REQUEST             => '', # the major payloa
               RESPONSE            => '', 
              };
   
   bless $this, $class;
}


sub close 
{
   my ($self) = @_;

   undef $self;
}


sub talk 
{
   my ($self) = @_;

   my $request;
   my $response;
   my $responseBuffer;
   my $size;
   my $ptechSocket = new IO::Socket::INET ( PeerAddr => '63.74.163.212:16100' ); #'localhost:16100' );
   warn "Using 63.74.163.212:16100 to access Paymentech";
   my $ptechSelect = new IO::Select();

   if( ! defined $ptechSocket )
   {
      # failed to connect to server
      warn "Failed to connect to server";
      $self->{RESPONSE} = 'RIXCLIENT_TIMEOUT';
      return;
   }

   $ptechSelect->add($ptechSocket);

   $request = (pack "nxxxx", length($self->{REQUEST})) . $self->{REQUEST};

   if( $ptechSocket->send( $request ) != length($request) )
   {
      # failed to send request
      warn "Failed to send request";
      $self->{RESPONSE} = 'RIXCLIENT_TIMEOUT';
      $ptechSocket->close();
      return;
   }

   # try to read the response, timeout if not received
   # timeout in 10 seconds on auths, since the machine won't use them
   # after ten seconds anyway.  Timeout in 10 minutes on everything else,
   # don't want to miss force sales
   while( 1 == $ptechSelect->can_read(
                   $self->{TRANSACTION_TYPE} == $self->{AUTH_ONLY} ? 10:600) )
   {
      $ptechSocket->recv($responseBuffer, 200);

      $response .= $responseBuffer;

      # if the header has arrived, check the size of the response to
      # see if all of the response has arrived
      if( length($response) > 6 )
      {
         ($size) = unpack "n", (substr $response, 0, 2);

         if( ($size + 6) >= length($response) )
         {
            # the entire response is here, fill RESPONSE and exit loop
            $self->{RESPONSE} = substr $response, 6, $size;

            $ptechSocket->close();

            if ($self->{TRANSACTION_TYPE} == $self->{BATCH_RELEASE})
            {
               $self->read_batch_response();
            }
            else
            {
               $self->read_auth_response();
            }

            return;
         }
      }

      if( length($responseBuffer) == 0)
      {
          # server disconnected, and the response is incomplete
          warn "server disconnect before response completed";
          $self->{RESPONSE} = 'RIXCLIENT_TIMEOUT';
          return;
      }
   }

   # timeout has occured

   warn "response timeout";
   $self->{RESPONSE} = 'RIXCLIENT_TIMEOUT';

   $ptechSocket->close();
}


sub make_auth_only 
{
   my ($self, $magstrip, $txn_amt) = @_;

   $self->{TRANSACTION_TYPE} = $self->{AUTH_ONLY};

   # request points to the request string
   my $request;

   # A - Header Information
   # 1 - STX
   $request .= $STX;
   # 2 - System Indicator
   $request .= 'L.'; # L. = Host Capture Indicator
   # 3 - Routing Indicator
   $request .= 'A02000';
   # 4 - Client Number
   $request .= $CLIENT_NUMBER;
   # 5 - Merchant Number 
   $request .= $self->{MERCHANT_NUMBER};
   # 6 - Terminal Number
   $request .= $self->{TERMINAL_NUMBER};
   # 7 - Transaction Sequence Flag
   $request .= '1'; # 1 = single transaction (NOT SURE ABOUT THIS!)
   # 8 - Sequence Number
   $request .= $self->{SEQUENCE_NUMBER};
   # 9 - Transaction Class
   $request .= 'F'; # F = Financial Transaction
   # 10 - Transaction Code 
   $request .= $self->{TRANSACTION_TYPE}; # 02 = Authorization Only
   # 11 - PIN Capability Code
   $request .= '1'; # 1 = Terminal device accepts PIN entry  (NOT SURE ABOUT THIS!)
   # 12 - Entry Data Source
   if( $magstrip =~ /^B/ )
   {
      $request .= '04'; # 04 = Track 1
   }
   else
   {
      $request .= '03'; # 03 = Track 2
   }


   # B - Cardholder Information
   # 1 - Full Magnetic Stripe Information
   $request .= $magstrip;
   # 2 - Field Separator
   $request .= $FS;


   # C - Transaction Information
   # 1 - Transaction Amount 
   $request .= $txn_amt;
   # 2 - Field Separator
   $request .= $FS;
   # 3 - Filler - LRR#
   $request .= "00000000"; # field length = 8
   # 4 - Field Separator
   $request .= $FS;
   # 5 - Field Separator
   $request .= $FS;
   # 6 - Field Separator
   $request .= $FS;
   # 7 - Field Separator
   $request .= $FS;
   # 8 - Field Separator
   $request .= $FS;
   # 9 - Field Separator
   $request .= $FS;

   # K - End of Packet
   # 1 - ETX
   $request .= $ETX;
   
   $self->{REQUEST} = $request;
}



sub make_sale 
{
   my ($self, $magstrip, $txn_amt, $invoice_num) = @_;

   $self->{TRANSACTION_TYPE} = $self->{SALE};
   
   # request points to the request string
   my $request;

   # A - Header Information
   # 1 - STX
   $request .= $STX;
   # 2 - System Indicator
   $request .= 'L.'; # L. = Host Capture Indicator
   # 3 - Routing Indicator
   $request .= 'A02000';
   # 4 - Client Number
   $request .= $CLIENT_NUMBER;
   # 5 - Merchant Number
   $request .= $self->{MERCHANT_NUMBER};
   # 6 - Terminal Number
   $request .= $self->{TERMINAL_NUMBER};
   # 7 - Transaction Sequence Flag
   $request .= '1'; # 1 = single transaction (NOT SURE ABOUT THIS!)
   # 8 - Sequence Number
   $request .= $self->{SEQUENCE_NUMBER};
   # 9 - Transaction Class
   $request .= 'F'; # F = Financial Transaction
   # 10 - Transaction Code 
   $request .= $self->{TRANSACTION_TYPE}; # 01 = Sale
   # 11 - PIN Capability Code
   $request .= '1'; # 1 = Terminal device accepts PIN entry  (NOT SURE ABOUT THIS!)
   # 12 - Entry Data Source
   if( $magstrip =~ /^B/ )
   {
      $request .= '04'; # 04 = Track 1
   }
   else
   {
      $request .= '03'; # 03 = Track 2
   }


   # B - Cardholder Information
   # 1 - Full Magnetic Stripe Information
   $request .= $magstrip;
   # 2 - Field Separator
   $request .= $FS;


   # C - Transaction Information
   # 1 - Transaction Amount 
   $request .= $txn_amt;
   # 2 - Field Separator
   $request .= $FS;
   # 3 - Filler - LRR#
   $request .= '00000000';
   # 4 - Field Separator
   $request .= $FS;
   # 5 - Field Separator
   $request .= $FS;
   # 6 - Field Separator
   $request .= $FS;

   # F - Industry Specific
   # 1 - Industry Code
   $request .= '004'; # 004 = Retail
   # 2 - Invoice Number
   if ($invoice_num ne "")
   { 
      $request .= $invoice_num; # has to be 6 chars long, zero-filled, right justify
   }
   else 
   { 
      $request .= '000000';  
   } 
   # 3 - Item Code
   $request .= '00000000000000000000'; # field length = 20

   # G - Miscellaneous Information
   # 1 - Field Separator
   $request .= $FS;
   # 2 - Field Separator
   $request .= $FS;
   # 3 - Field Separator
   $request .= $FS;

   # K - End of Packet
   # 1 - ETX
   $request .= $ETX;

   $self->{REQUEST} = $request;
}


sub make_force 
{

   my ($self, $magstrip, $txn_amt, $auth_code, $invoice_num) = @_;

   $self->{TRANSACTION_TYPE} = $self->{FORCE};

   # request points to the request string
   my $request;

   # A - Header Information
   # 1 - STX
   $request .= $STX;
   # 2 - System Indicator
   $request .= 'L.'; # L. = Host Capture Indicator
   # 3 - Routing Indicator
   $request .= 'A02000';
   # 4 - Client Number
   $request .= $CLIENT_NUMBER;
   # 5 - Merchant Number 
   $request .= $self->{MERCHANT_NUMBER};
   # 6 - Terminal Number
   $request .= $self->{TERMINAL_NUMBER};
   # 7 - Transaction Sequence Flag
   $request .= '1'; # 1 = single transaction (NOT SURE ABOUT THIS!)
   # 8 - Sequence Number
   $request .= $self->{SEQUENCE_NUMBER};
   # 9 - Transaction Class
   $request .= 'F'; # F = Financial Transaction
   # 10 - Transaction Code 
   $request .= $self->{TRANSACTION_TYPE}; # 01 = Prior Sale
   # 11 - PIN Capability Code
   $request .= '1'; # 1 = Terminal device accepts PIN entry  (NOT SURE ABOUT THIS!)
   # 12 - Entry Data Source
   if( $magstrip =~ /^B/ )
   {
      $request .= '04'; # 04 = Track 1
   }
   else
   {
      $request .= '03'; # 03 = Track 2
   }


   # B - Cardholder Information
   # 1 - Full Magnetic Stripe Information
   $request .= $magstrip;
   # 2 - Field Separator
   $request .= $FS;


   # C - Transaction Information
   # 1 - Transaction Amount 
   $request .= $txn_amt;
   # 2 - Field Separator
   $request .= $FS;
   # 3 - Filler - LRR#
   $request .= '00000000';
   # 4 - Field Separator
   $request .= $FS;
   # 5 - Field Separator
   $request .= $FS;
   # 6 - Field Separator
   $request .= $FS;

   # F - Industry Specific
   # 1 - Industry Code
   $request .= '004'; # 004 = Retail
   # 2 - Invoice Number
   $request .= '000000'; # 0 fill, right justify (NOT SURE ABOUT THIS!)
   # 3 - Item Code
   $request .= '00000000000000000000'; # field length = 20

   # G - Miscellaneous Information
   # 1 - Field Separator
   $request .= $FS;
   # 2 - Authorization Code - this must match the auth# that was received on the original authorization
   $request .= $auth_code; 
   # 3 - Field Separator
   $request .= $FS;
   # 4 - Field Separator
   $request .= $FS;
   # 5 - Field Separator
   $request .= $FS;
   # 6 - Field Separator
   $request .= $FS;

   # K - End of Packet
   # 1 - ETX
   $request .= $ETX;

   $self->{REQUEST} = $request;
}



sub make_return 
{
   my ($self, $magstrip, $txn_amt, $manual, $acct_num, $exp_date) = @_;

   $self->{TRANSACTION_TYPE} = $self->{RETURN};

   # request points to the request string
   my $request;

   # A - Header Information
   # 1 - STX
   $request .= $STX;
   # 2 - System Indicator
   $request .= 'L.'; # L. = Host Capture Indicator
   # 3 - Routing Indicator
   $request .= 'A02000';
   # 4 - Client Number
   $request .= $CLIENT_NUMBER;
   # 5 - Merchant Number 
   $request .= $self->{MERCHANT_NUMBER};
   # 6 - Terminal Number
   $request .= $self->{TERMINAL_NUMBER};
   # 7 - Transaction Sequence Flag
   $request .= "1"; # 1 = single transaction (NOT SURE ABOUT THIS!)
   # 8 - Sequence Number
   $request .= $self->{SEQUENCE_NUMBER};
   # 9 - Transaction Class
   $request .= 'F'; # F = Financial Transaction
   # 10 - Transaction Code 
   $request .= $self->{TRANSACTION_TYPE}; # 06 = Return
   # 11 - PIN Capability Code
   $request .= '1'; # 1 = Terminal device accepts PIN entry  (NOT SURE ABOUT THIS!)
   # 12 - Entry Data Source
   if ($manual == 1)
   {
      $request .= '02'; # 02 = Manual
   }
   else
   {
      $request .= '04'; # 04 = Track 1
   }



   # B - Cardholder Information
   if ($manual == 1)
   {
      # 1 - Account number
      $request .= $acct_num;
      # 2 - Field Separator
      $request .= $FS;
      # 3 - Expiration Date
      $request .= $exp_date;
      # 4 - Field Separator
      $request .= $FS;
   }
   else
   {
      # 1 - Full Magnetic Stripe Information
      $request .= $magstrip;
      # 2 - Field Separator
      $request .= $FS;
   }
   


   # C - Transaction Information
   # 1 - Transaction Amount 
   $request .= $txn_amt;
   # 2 - Field Separator
   $request .= $FS;
   # 3 - Filler - LRR#
   $request .= '00000000';
   # 4 - Field Separator
   $request .= $FS;
   

   # K - End of Packet
   # 1 - ETX
   $request .= $ETX;
         
   $self->{REQUEST} = $request;
}



sub make_void 
{

   my ($self, $ret_ref_num, $acct_num, $manual) = @_;

   $self->{TRANSACTION_TYPE} = $self->{VOID};

   # request points to the request string
   my $request;

   # A - Header Information
   # 1 - STX
   $request .= $STX;
   # 2 - System Indicator
   $request .= 'L.'; # L. = Host Capture Indicator
   # 3 - Routing Indicator
   $request .= 'A02000';
   # 4 - Client Number
   $request .= $CLIENT_NUMBER;
   # 5 - Merchant Number 
   $request .= $self->{MERCHANT_NUMBER};
   # 6 - Terminal Number
   $request .= $self->{TERMINAL_NUMBER};
   # 7 - Transaction Sequence Flag
   $request .= '1'; # 1 = single transaction (NOT SURE ABOUT THIS!)
   # 8 - Sequence Number
   $request .= $self->{SEQUENCE_NUMBER};
   # 9 - Transaction Class
   $request .= 'F'; # F = Financial Transaction
   # 10 - Transaction Code
   $request .= $self->{TRANSACTION_TYPE}; # 41 = VOID
   # 11 - Last Retrieval Reference Number
   $request .=  '00000000';
   # 12 - Field Separator
   $request .= $FS;

   # G - Miscellaneous Information
   # 1 - Retreival Reference Number
   $request .= $ret_ref_num;
   # 2 - Field Separator
   $request .= $FS;
   # 3 - Account Number
   $request .= $acct_num;
   # 4 - Field Separator
   $request .= $FS;

   # J - End of Packet
   # 17 - ETX
   $request .= $ETX;

   $self->{REQUEST} = $request;
}



sub read_auth_response 
{
   my ($self) = @_;

   my $response =  $self->{RESPONSE};
   
#   $self->{PAY_CLIENT_PID} = substr($response, 0, 6);
#   $self->{PAY_CLIENT_PID} =~ s/^0+//g;

#   $self->{RIX_CLIENT_PID} = substr($response, 6, 6);
#   $self->{RIX_CLIENT_PID} =~ s/^0+//g;

#   $response = substr($response, 12);

   if ($response eq 'RIXCLIENT_TIMEOUT') {
      $self->{TIMED_OUT} = 1;
      return;
   }

   # 1 - STX
   # 2 - Action Code
   $self->{ACTION_CODE} = substr($response, 1, 1);
   # 3 - Action Verification Response Code
   # 4 - Authorization/Error Code
   $self->{AUTH_CODE} = substr($response, 3, 6);
   # 5 - Batch Number
   $self->{BATCH_NUMBER} = substr($response, 9, 6);
   # 6 - Retrieval Reference Number
   $self->{RETRIEVAL_REF_NUMBER} = substr($response, 15, 8);
   # 7 - Sequence Number (this also is the terminal ID used to comm with Paymentech)
   $self->{SEQUENCE_NUMBER} = substr($response, 23, 6);
   $self->{TERMINAL_NUMBER} = substr($self->{SEQUENCE_NUMBER}, 3, 3);
   # 8 - Response Message
   $self->{RESPONSE_MESSAGE} = substr($response, 29, 32);
   # 9 - Card Type
   $self->{CARD_TYPE} = substr($response, 61, 2);
   # 10 - FS
   # 11 - Interchange Compliance (if card type is Visa or MasterCard, then Interchange Compliance req'd)
   my $ic_shift;
   if ( (($self->{CARD_TYPE} eq "VI") || ($self->{CARD_TYPE} eq "MC")) &&
        ($self->{ACTION_CODE} eq 'A') )
   {
      $self->{INTERCHANGE_COMPLIANCE} = substr($response, 64, 40);
      $ic_shift = 40;
   }
   else
   {
      $ic_shift = 0;
   }
   # 12 - FS
   # 13 - Authorizing Network ID
   $self->{AUTH_NET_ID} = substr($response, 65+$ic_shift, 2);
   # 14 - Authorizing Source
   $self->{AUTH_SOURCE} = substr($response, 67+$ic_shift, 1);
   # 15 - FS
   # 27 - FS
   # 28 - ETX
}


sub make_batch_inquiry 
{
   # make_batch_inquiry() is slightly altered. previously, we recv'd MERCHANT_NUMBER and 
   # TERMINAL_NUMBER as input. this, we shall assume is part of the paymentech object already.
   #($rix_paymentech_GLOBAL{"MERCHANT_NUMBER"},$rix_paymentech_GLOBAL{"TERMINAL_NUMBER"}) = @_;

   my ( $self, $term_num) = @_;

   $self->{TERMINAL_NUMBER} = sprintf("%03d", $term_num);
   $self->{TRANSACTION_TYPE} = $self->{BATCH_INQUIRY};

   # init the final formatted message
   my $request;

   # 1 - STX
   $request .= $STX;
   # 2 - System Indicator
   $request .= 'L.'; # L. = Host Capture Indicator
   # 3 - Routing Indicator
   $request .= 'A02000';
   # 4 - Client Number
   $request .= $CLIENT_NUMBER;
   # 5 - Merchant Number 
   $request .= $self->{MERCHANT_NUMBER};
   # 6 - Terminal Number
   $request .= $self->{TERMINAL_NUMBER};
   # 7 - Transaction Sequence Flag
   $request .= '1'; # 1 = single transaction (NOT SURE ABOUT THIS!)
   # 8 - Sequence Number
   $request .= $self->{SEQUENCE_NUMBER};
   # 9 - Transaction Class
   $request .= 'F'; # F = Financial Transaction
   # 10 - Transaction Code
   $request .= $self->{TRANSACTION_TYPE}; # 50 = BATCH INQUIRY
   # 11 - Batch Number
   $request .= '000000'; # host will ONLY send back for CURRENT batch
   # 12 - Batch Sequence Number
   $request .= '000';
   # 13 - Batch Offset
   $request .= '0';
   # 14 - Transaction Count
   $request .= '000000';
   # 15 - Net Amount
   $request .= '0000000.00';
   # 16 - Field Separator
   $request .= $FS;
   # 17 - System Information
   $request .= $SYSTEM_INFO;
   # 18 - Field Separator
   $request .= $FS;
   # 19 - Filler - LRR#
   $request .= '00000000';
   # 20 - ETX
   $request .= $ETX;

   $self->{REQUEST} = $request;
}


sub make_batch_release 
{
   my ( $self, $term_num) = @_;

   $self->{TERMINAL_NUMBER} = sprintf("%03d", $term_num);
   $self->{TRANSACTION_TYPE} = $self->{BATCH_RELEASE};

   # init the final formatted message
   my $request;

   # 1 - STX
   $request .= $STX;
   # 2 - System Indicator
   $request .= 'L.'; # L. = Host Capture Indicator
   # 3 - Routing Indicator
   $request .= 'A02000';
   # 4 - Client Number
   $request .= $CLIENT_NUMBER;
   # 5 - Merchant Number 
   $request .= $self->{MERCHANT_NUMBER};
   # 6 - Terminal Number
   $request .= $self->{TERMINAL_NUMBER};
   # 7 - Transaction Sequence Flag
   $request .= '1'; # 1 = single transaction (NOT SURE ABOUT THIS!)
   # 8 - Sequence Number  
   $request .= $self->{SEQUENCE_NUMBER};
   # 9 - Transaction Class
   $request .= 'F'; # F = Financial Transaction
   # 10 - Transaction Code
   $request .= $self->{TRANSACTION_TYPE}; # 50 = BATCH RELEASE
   # 11 - Batch Number
   $request .= '000000'; # host will ONLY send back for CURRENT batch
   # 12 - Batch Sequence Number
   $request .= '000';
   # 13 - Batch Offset
   $request .= '0';
   # 14 - Transaction Count
   $request .= '000000';
   # 15 - Net Amount
   $request .= '0000000.00';
   # 16 - Field Separator
   $request .= $FS;
   # 17 - System Information
   $request .= $SYSTEM_INFO;
   # 18 - Field Separator
   $request .= $FS;
   # 19 - Filler - LRR#
   $request .= '00000000';
   # 20 - ETX
   $request .= $ETX;

   $self->{REQUEST} = $request;
}

sub read_batch_response 
{
   my ($self) = @_;

   my $response =  $self->{RESPONSE};
   
#   $self->{PAY_CLIENT_PID} = substr($response, 0, 6);
#   $self->{PAY_CLIENT_PID} =~ s/^0+//g;
   
#   $self->{RIX_CLIENT_PID} = substr($response, 6, 6);
#   $self->{RIX_CLIENT_PID} =~ s/^0+//g;

#   $response = substr($response, 12);
   
   if ($response eq 'RIXCLIENT_TIMEOUT') {
      $self->{TIMED_OUT} = 1;
      return;
   }

   if ($response)
   {
      # 1 - STX
      # 2 - Action Code
      $self->{ACTION_CODE} = substr($response, 1, 1);
      # 3 - Action Verification Response Code
      # 4 - Authorization/Error Code
      $self->{AUTH_CODE} = substr($response, 3, 6);
      # 5 - Batch Number
      $self->{BATCH_NUMBER} = substr($response, 9, 6);
      # 6 - Retrieval Reference Number
      $self->{RETRIEVAL_REF_NUMBER} = substr($response, 15, 8);
      # 7 - Sequence Number (and hence, Terminal Number
      $self->{SEQUENCE_NUMBER} = substr($response, 23, 6);
      $self->{TERMINAL_NUMBER} = substr($self->{SEQUENCE_NUMBER}, 3, 3);
      # 8 - Response Message
      $self->{RESPONSE_MESSAGE} = substr($response, 29, 32);
      # 9 - FS
      # 10 - Download Flag
      # 11 - Multi Message Flag
      # 12 - Batch Open Date/Time
      $self->{BATCH_OPEN_TIMESTAMP} = substr($response, 64, 10);
      # 13 - Batch Close Date/Time
      $self->{BATCH_CLOSE_TIMESTAMP} = substr($response, 74, 10);
      # 14 - Batch Transaction Count
      $self->{BATCH_TRANSACTION_COUNT} = substr($response, 84, 6);
      # 15 - Batch Net Amount
      my $FSpos = index($response, $FS, 90); 
      $self->{BATCH_NET_AMOUNT} = substr($response, 90, ($FSpos-90));
      # 16 - FS
      # 17 - Working Key
      $FSpos = index($response, $FS, $FSpos+1);
      if ($FSpos < 0) { return; }             
      # 18 - FS
      # 19 - Payment Type #1
      $self->{PAYMENT_1_TYPE} = substr($response, $FSpos+1, 3);
      # 20 - Transaction Count
      $self->{PAYMENT_1_COUNT} = substr($response, $FSpos+4, 6);
      # 21 - Net Amount
      my $tmpFSpos = index($response, $FS, $FSpos+1);
      if ($tmpFSpos < 0) 
      {
         $self->{PAYMENT_1_NET_AMOUNT} = substr($response, $FSpos+10);
         chop($self->{PAYMENT_1_NET_AMOUNT});
         return;
      }
      else
      {
         $self->{PAYMENT_1_NET_AMOUNT} = substr($response, $FSpos+10, ($tmpFSpos-$FSpos-10));
         $FSpos = $tmpFSpos;
      }
      # 22 - FS
      # 23 - Payment Type #2
      $self->{PAYMENT_2_TYPE} = substr($response, $FSpos+1, 3);
      # 24 - Transaction Count
      $self->{PAYMENT_2_COUNT} = substr($response, $FSpos+4, 6);
      # 25 - Net Amount 
      $tmpFSpos = index($response, $FS, $FSpos+1);
      if ($tmpFSpos < 0) 
      {
         $self->{PAYMENT_2_NET_AMOUNT} = substr($response, $FSpos+10);
         chop($self->{PAYMENT_2_NET_AMOUNT});
         return; 
      }
      else
      {
         $self->{PAYMENT_2_NET_AMOUNT} = substr($response, $FSpos+10, ($tmpFSpos-$FSpos-10));
         $FSpos = $tmpFSpos;
      }
      # 26 - FS
      # 27 - Payment Type #3
      $self->{PAYMENT_3_TYPE} = substr($response, $FSpos+1, 3);
      # 28 - Transaction Count
      $self->{PAYMENT_3_COUNT} = substr($response, $FSpos+4, 6);
      # 29 - Net Amount
      $tmpFSpos = index($response, $FS, $FSpos+1);
      if ($tmpFSpos < 0) 
      {
         $self->{PAYMENT_3_NET_AMOUNT} = substr($response, $FSpos+10);
         chop($self->{PAYMENT_3_NET_AMOUNT});
         return;
      }
      else
      {
         $self->{PAYMENT_3_NET_AMOUNT} = substr($response, $FSpos+10, ($tmpFSpos-$FSpos-10));
         $FSpos = $tmpFSpos;
      }
      # 30 - FS
      # 31 - Payment Type #4
      $self->{PAYMENT_4_TYPE} = substr($response, $FSpos+1, 3);
      # 32 - Transaction Count
      $self->{PAYMENT_4_COUNT} = substr($response, $FSpos+4, 6);
      # 33 - Net Amount
      $tmpFSpos = index($response, $FS, $FSpos+1);
      if ($FSpos < 0) 
      {
         $self->{PAYMENT_4_NET_AMOUNT} = substr($response, $FSpos+10);
         chop($self->{PAYMENT_4_NET_AMOUNT});
         return;
      }
      else
      {
         $self->{PAYMENT_4_NET_AMOUNT} = substr($response, $FSpos+10, ($tmpFSpos-$FSpos-10));
         $FSpos = $tmpFSpos;
      }
      # 34 - FS
      # 35 - ETX
   }
}



END { }

} # BEGIN

1;










