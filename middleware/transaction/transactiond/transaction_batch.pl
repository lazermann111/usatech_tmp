#!/usr/bin/perl

use lib '/opt/transactiond';
use Evend::Database::Database;
use Paymentech;
use Txndatabase;

Perform_Server_Batch();

sub Perform_Server_Batch 
{
   $localtime = localtime();
   warn("time is $localtime\n");

   my $ORACLE = new Evend::Database::Database(print_query=>1);
   
#   Retry_Failed_Force_Transactions($ORACLE);

#   Retry_Failed_Sale_Transactions($ORACLE);

   Release_Batch($ORACLE);

   $ORACLE->close();

#   `perl /websites/e-vend/lib/MakeSellThru.pl`;
}

sub Retry_Failed_Force_Transactions 
{
	my $ORACLE = shift;

   my $txns = $ORACLE->select(            table => 'Transaction_Record',
                                 select_columns => 'TRANS_NO,
                                                    MACHINE_ID,
                                                    COLUMN_ID,
                                                    INVENTORY_NUMBER,
                                                    QUESTION_1_DATA,
                                                    QUESTION_2_DATA,
                                                    BUTTON_ID,
                                                    TRANSACTION_STATUS,
                                                    CARD_TYPE,
                                                    MAG_STRIPE,
                                                    CARD_NUMBER,
                                                    TRANSACTION_AMOUNT,
                                                    TAX_AMOUNT,
                                                    AUTH_CD,
                                                    FORCE_CD,
                                                    FORCE_ACTION_CD',
                                 where_columns =>  ["(FORCE_ACTION_CD='E' OR FORCE_ACTION_CD='T')", 
                                                    "TRANSACTION_CODE='03'",
                                                    "CARD_TYPE in ('VI', 'MC', 'DS', 'DC', 'AE')",
													"MAG_STRIPE is not null"]);

   warn "num of failed forces is ", scalar(@$txns), "\n";

   foreach my $txn (@$txns)
   {
      my (  $txn_num,
            $machine,
            $col_num,
            $inv_num,
            $q1_data,
            $q2_data,
            $button_num,
            $txn_status,
            $card_type,
            $magstrip,
            $card_num,
            $txn_amt,
            $tax_amt,
            $auth_code,
            $force_code, 
            $force_action_code ) = @$txn;
      my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

      warn "TRANSACTION NUMBER IS $txn_num\n";

    my $paymentech = Paymentech->new( machine => $machine);
    my $txndatabase = Txndatabase->new( processor_obj => $paymentech);

      $paymentech->make_force($magstrip, $total_amt, $auth_code); 
      $paymentech->talk();

      $txndatabase->batch($ORACLE, $machine,  $txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status);

      $txndatabase->close();
      $paymentech->close();
   }

   return;
}


sub Retry_Failed_Sale_Transactions 
{
	my $ORACLE = shift;

   my $txns = $ORACLE->select(            table => 'Transaction_Record',
                                 select_columns => "TRANS_NO,
                                                    MACHINE_ID,
                                                    to_char(TRANSACTION_DATE, 'MM/DD/YYYY'),
                                                    to_char(TRANSACTION_DATE, 'HH24:MI'),
                                                    COLUMN_ID,
                                                    INVENTORY_NUMBER,
                                                    QUESTION_1_DATA,
                                                    QUESTION_2_DATA,
                                                    BUTTON_ID,
                                                    CARD_TYPE,
                                                    MAG_STRIPE,
                                                    CARD_NUMBER,
                                                    INVOICE_NUMBER,
                                                    TRANSACTION_AMOUNT,
                                                    TAX_AMOUNT",
                                 where_columns =>  ["(FORCE_ACTION_CD='E' OR FORCE_ACTION_CD='T')", 
                                                    "TRANSACTION_CODE='01'",
                                                    "CARD_TYPE in ('VI', 'MC', 'DS', 'DC', 'AE')",
													"MAG_STRIPE is not null"]);

   warn "num of failed sales is ", scalar(@$txns), "\n";

   foreach my $txn (@$txns)
   {
      my (  $txn_num,
            $machine,
            $txn_date,
            $txn_time,
            $col_num,
            $inv_num,
            $q1_data,
            $q2_data,
            $button_num,
            $card_type,
            $magstrip,
            $card_num,
            $auth_num,
            $txn_amt,
            $tax_amt ) = @$txn;
      my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

      warn "TRANSACTION NUMBER IS $txn_num\n";

      my $paymentech = Paymentech->new( machine => $machine);
      my $txndatabase = Txndatabase->new( processor_obj => $paymentech);


      $paymentech->make_sale($magstrip, $total_amt);
      $paymentech->talk();

      $txndatabase->local_batch($ORACLE, $machine, $txn_time, $txn_date,  $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num);

      $txndatabase->close();
      $paymentech->close();
   }
   
   return;
}

sub Release_Batch 
{

	my $ORACLE = shift;

   my %merchant_id = %Paymentech::MERCHANT_ID;

   foreach my $merchant (keys %merchant_id)
   {
      my $merchant_num = $merchant_id{$merchant};

      for(my $term_num = 1; $term_num <= 5; $term_num++)
      {
         my $paymentech = Paymentech->new( group_code => $merchant); 
         my $txndatabase = Txndatabase->new( processor_obj => $paymentech);

         $paymentech->make_batch_release($term_num);
         $paymentech->talk();
	 #$paymentech->read_batch_response();

         $txndatabase->paymentech_batch($ORACLE);
         
         # move txns in MySQL from [68] to [72] SAK 1/22/01
         #MoveMySQLTxns($paymentech);

         $txndatabase->close();
         $paymentech->close();
      }
   }
}

