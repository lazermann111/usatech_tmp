#!/usr/bin/perl

use strict;

use DBI;

#$ENV{ORACLE_HOME} = "/opt/OraHome1";
my $dbh = DBI->connect("DBI:Oracle:usadbd02","rerix_engine","rerix_engine",{PrintError => 1, RaiseError => 1, AutoCommit => 1}) or print "<br><br>Couldn't connect to database: " . DBI->errstr . "<br><br>";

my $failures_stmt = $dbh->prepare("select 'xxxxxxxxxxxx' || substr(card_number, -4), card_type, count(1), sum(transaction_amount) from transaction_record_hist where trunc(batch_date) > trunc((sysdate-1)) and force_action_cd = 'E' and card_type in ('AE', 'MC', 'VI', 'DS') and force_response_msg = 'FINANCIAL FAILURE' group by card_number, card_type order by sum(transaction_amount) desc");
$failures_stmt->execute();

my $tran_amount_total = 0;

while (my @data = $failures_stmt->fetchrow_array()) 
{
	my $card_number 	= $data[0];
	my $card_type 		= $data[1];
	my $count			= $data[2];
	my $sum				= $data[3];
	
	$tran_amount_total = ($tran_amount_total + $sum);
	
	my $sum_formatted = sprintf "%.2f", $sum;
	
	print "$card_number\t$card_type\t$count\t\$$sum_formatted\n";
}

print "tran_amount_total = $tran_amount_total\n";

$failures_stmt->finish();
