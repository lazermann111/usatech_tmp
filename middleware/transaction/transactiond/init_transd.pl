#!/usr/bin/perl

use lib '/opt/transactiond';
use strict;
use Transactiond;
use Evend::Database::Database;
use Paymentech;
use SpecialCard;
use Txndatabase;
use CardUtils;
use TransactionHandler;

my $server = Transactiond->new({
	pidfile   => '/opt/transactiond/transd.pid',
	localport => 10104,
	logfile   => '/opt/transactiond/transd.log',
	mode	=> 'fork',
});

$server->Bind();


