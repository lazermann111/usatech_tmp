package TransactionHandler;

#---------------------------------------------------------------------------------------------
# Change History
#
# Version       Date            Programmer      Description
# -------       ----------      ----------      ----------------------------------------------
# 1.00          12/22/2002      T Shannon       Original Version
# 1.01          12/22/2002      T Shannon       Enhanced code to prevent Sony clients from
#                                               creating dupes during resend of force batches
# 1.02			02/08/2003		T Shannon		Hack solution for Sony auth dupe on retry attempt
# 1.03			05/05/2003		T Shannon		Added code to use the Payment Support System to
#												perform transactions
# 1.04			11/07/2003		T Shannon		Changes for G4 and timezone/daylight saving handling
# 1.05			12/03/2003		P Cowan			Bug Fix: Regular expression to check if transaction was
#												cancelled was evaluating to true for everything
# 1.06			03/08/2004		P Cowan			Added call to FHMS to cancel a cancelled transaction 
#												in Payment Manager for network auths
# 1.07			04/13/2004		P Cowan			Bug in Debitek G4s send a space for the transaction result
#												instead of the success result code, so I'm inserting an awfull
#												kludge in a few places to work with a space character.
# 1.08		    07/20/2005	    D Kouznetsov	Added writing transactions to PSS schema
# 1.09          10/11/2005      D Kouznetsov    Added passing track2 parameter to local_batch_v2

use strict;
use Net::Daemon;
use Evend::Database::Database;
use Paymentech;
use SpecialCard;
use FHMS;
use Txndatabase;
use CardUtils;

use Exporter    ();
use vars qw($VERSION @ISA);
$VERSION = '0.01';
@ISA = qw(Net::Daemon);

sub logit($);

sub new
{

        my $type = shift;
        my $this = {};
        %$this = @_;

#        $this->{database} = Evend::Database::Database->new(print_query => 1, primary => 'taz1', backup => 'taz2');
        $this->{database} = Evend::Database::Database->new(print_query => 1);

        bless $this,$type;

}
	

sub t1	# Rix auth
{
	
	my $handler = shift;
	my $payload = shift;

	my $ORACLE = $handler->{database};
	warn("payload; $payload\n");

	my ($machine,$magstrip, $pin, $auth_amt, $auth_question_data, $auth_info, $card_type) = split /,/, $payload;

# --- while testing PSS we will branch to a different version of the t1 handler 
		return t1_PSS($handler, $payload);
# -----------------------------------------------------------------------------------------------------		
	if (length($magstrip) > 80)
	{
		$magstrip =~ /(.*)\?/;
		$magstrip = $1;
	}

	my $processor;
	$processor = Paymentech->new( machine => $machine ) if $card_type == 0;
	$processor = SpecialCard->new( machine => $machine ) if $card_type == 1;

	$processor->{magstrip} = $magstrip;
   
	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date) = $cardreader->parse($magstrip);

	$processor->make_auth_only($magstrip, $auth_amt);
	$processor->talk();
	$processor->read_auth_response();
	
	$processor->{ACTION_CODE} = 'E' if $processor->{TIMED_OUT};

	if( $processor->{magstrip} =~ m#LEVI/LAURA# )
	{
		$processor->{ACTION_CODE} = 'E';
	}

	$txndatabase->authorize($ORACLE, $machine, $pin, $auth_amt, $auth_question_data, $auth_info, $card_type);

        foreach (keys %$processor)
        {
                warn("processor: $_ =>  $processor->{$_}\n");
        }

	return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, $processor->{BALANCE};
}

sub t1_PSS	# Rix auth
{
	# -- SPECIAL NOTE : PSS change regarding database updating --
	#
	# Ordinarily we write a tran record to the database after we have performed a transaction  
	# thru the payment processor. However, to use the Payment Support System we must provide an 'order id'. 
	# To guarantee a unique order id value we must obtain the next trans_no from the transaction_record
	# table using the Oracle sequence defined for it. This 'reserves' a place in the table for the
	# row we will create. This is necessary because we don't want to write the row until we have all 
	# of the data or we'll cause the triggers in the table to go nuts.
	
	my $handler = shift;
	my $payload = shift;

	my $ORACLE = $handler->{database};
	logit "t1_PSS payload; $payload";

	my ($machine,$magstrip, $pin, $auth_amt, $auth_question_data, $auth_info, $card_type) = split /,/, $payload;

	if (length($magstrip) > 80)
	{
		$magstrip =~ /(.*)\?/;
		$magstrip = $1;
	}

	my ($order_nbr, $processor);
	$processor = FHMS->new( machine => $machine) if $card_type == 0 ;
	$processor = SpecialCard->new( machine => $machine ) if $card_type == 1;
	
	$processor->{magstrip} = $magstrip;
   
	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date) = $cardreader->parse($magstrip);

	my $track2;
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);

	$track2 = $cardreader->GetTrack2($magstrip);
	logit "track2 = $track2";
	logit "auth_info = $auth_info";
	
	# before authing we need to obtain an order number
	$order_nbr = $txndatabase->createOrderNbr($ORACLE);
	if (! ($order_nbr > 0) ) 
	{
		# if we fail to generate an order number just exit with failure info
		return 'E', 0, 0;
	}
	
	$processor->make_auth_only($track2, $auth_amt, $order_nbr);
	$processor->talk();
	$processor->read_auth_response();
	
	$processor->{ACTION_CODE} = 'E' if $processor->{TIMED_OUT};

	$processor->{magstrip} = $track2;
	# using $auth_info no longer appears to work - in it's place $track2 will be used	T. Shannon 7/10/2003
	#$txndatabase->authorize_PSS($ORACLE, $machine, $pin, $auth_amt, $auth_question_data, $auth_info, $card_type, '', $order_nbr);
	$txndatabase->authorize_PSS($ORACLE, $machine, $pin, $auth_amt, $auth_question_data, $track2, $card_type, '', $order_nbr);

    foreach (keys %$processor)
    {
            warn("processor: $_ =>  $processor->{$_}\n");
    }

	return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, $processor->{BALANCE};
}


sub t2 # Rix Special Card batch
{
	my $handler = shift;
	my $payload = shift;


	my ($machine,$txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status) = split /,/, $payload;

	my $ORACLE = $handler->{database};

        my $err;
        if ($txn_num =~ /CANCEL|VENDTOOLONG|DENIED/)
        {
                ($err, $txn_num) = split /\|/, $txn_num;
        }


# --- while testing PSS we will branch to a different version of the t2 handler (only machine EV14176)
		return t2_PSS($handler, $payload);
# -----------------------------------------------------------------------------------------------------		

	my $processor;

	my $txn_ref = $ORACLE->select(table =>          'Transaction_Record',
		select_columns => 'CARD_TYPE, MAG_STRIPE, CARD_NUMBER, AUTH_AMT, ' .
							'AUTH_CD, AUTH_ACTION_CD, FORCE_ACTION_CD',
		where_columns =>  ['TRANS_NO = ?'],
		where_values =>   [$txn_num] );

	do { $processor->{ACTION_CODE} = 'E'; warn("transaction not in database\n"); return 0; } if !$txn_ref->[0][0];

    my ($card_type, $magstrip, $card_num,
		 $auth_amt, $auth_code, $action_code, $force_action_cd)
															= @{$txn_ref->[0]};

	my $txn_ref_hist = $ORACLE->select(table => 'Transaction_Record_Hist',
		select_columns => 'FORCE_ACTION_CD',
		where_columns =>  ['TRANS_NO = ?'],
		where_values =>   [$txn_num] );

    my $force_action_cd_hist;
    if( defined $txn_ref_hist->[0] )
    {
        $force_action_cd_hist = $txn_ref_hist->[0][0];
    }

	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

	# credit card types
	my @cccardtype = ('MC','VI','DS','AE','DC','JC','ENR');

    $processor = Paymentech->new( machine => $machine ) if grep (/$card_type/, @cccardtype);
    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'SP';

	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	$txndatabase->{TRANSACTION_NUMBER} = $txn_num;

	if (!$err)
	{
		warn "Sale to Paymentech\n";
        $processor->make_sale($magstrip, $total_amt, $card_num) if ($action_code eq 'T');
        $processor->make_force($magstrip, $total_amt, $auth_code) if ($action_code eq 'A');
		$processor->talk();
		$processor->read_auth_response();
	}
	else
	{
		$txndatabase->{$err} = 1;
		$processor->{ACTION_CODE} = 'E';
	}


	$txndatabase->batch($ORACLE, $machine, $txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status);

    foreach (keys %$processor)
    {
            warn("processor: $_ =>  $processor->{$_}\n");
    }

	# If this sale has been forced already, notify
	if( (defined $force_action_cd) or (defined $force_action_cd_hist) or $err)
	{
	    return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
	    return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}
}


sub t2_PSS # Rix Special Card batch
{
	# -- SPECIAL NOTE : PSS change regarding database updating --
	#
	# Ordinarily we write a tran record to the database after we have performed a transaction  
	# thru the payment processor. However, to use the Payment Support System we must provide an 'order id'. 
	# To guarantee a unique order id value we must obtain the next trans_no from the transaction_record
	# table using the Oracle sequence defined for it. This 'reserves' a place in the table for the
	# row we will create. This is necessary because we don't want to write the row until we have all 
	# of the data or we'll cause the triggers in the table to go nuts.
	

	my $handler = shift;
	my $payload = shift;


	my ($machine,$txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status) = split /,/, $payload;

	my $ORACLE = $handler->{database};

    my $err;
    if ($txn_num =~ /CANCEL|VENDTOOLONG|DENIED/)
    {
            ($err, $txn_num) = split /\|/, $txn_num;
    }

	my $processor;

	my $txn_ref = $ORACLE->select(table =>          'Transaction_Record',
		select_columns => 'CARD_TYPE, MAG_STRIPE, CARD_NUMBER, AUTH_AMT, ' .
							'AUTH_CD, AUTH_ACTION_CD, FORCE_ACTION_CD, TRANS_NO',
		where_columns =>  ['TRANS_NO = ?'],
		where_values =>   [$txn_num] );

	do { $processor->{ACTION_CODE} = 'E'; warn("transaction not in database\n"); return 0; } if !$txn_ref->[0][7];

    my ($card_type, $magstrip, $card_num,
		 $auth_amt, $auth_code, $action_code, $force_action_cd)
															= @{$txn_ref->[0]};

	my $txn_ref_hist = $ORACLE->select(table => 'Transaction_Record_Hist',
		select_columns => 'FORCE_ACTION_CD',
		where_columns =>  ['TRANS_NO = ?'],
		where_values =>   [$txn_num] );

    my $force_action_cd_hist;
    if( defined $txn_ref_hist->[0] )
    {
        $force_action_cd_hist = $txn_ref_hist->[0][0];
    }

	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

	# credit card types
	my @cccardtype = ('MC','VI','DS','AE','DC','JC','ENR');


# --- PSS code ---
	my ($order_nbr, $track2);

	my $cardreader = CardUtils->new();

	my ($card_num, $customer_name, $exp_date) = $cardreader->parse($magstrip);

	warn "t2_PSS - processing $machine\n";

	$track2 = $cardreader->GetTrack2($magstrip);

	$processor->{magstrip} = $track2;

	$processor = FHMS->new( machine => $machine) if grep (/$card_type/, @cccardtype);

    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'SP';
#---------------------------------------------------------------------------------------------

	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	$txndatabase->{TRANSACTION_NUMBER} = $txn_num;

	if (!$err)
	{
        warn "Sale to FHMS\n";
        #$processor->make_sale($track2, $total_amt, $txn_num)  if ($action_code eq 'T');
        #$processor->make_force($track2, $total_amt, $txn_num)  if ($action_code eq 'A');
 		
 		# appears to be incorrect - should use $txn_num instead of $order_nbr	T. Shannon 7/10/2003
        #$processor->make_sale($track2, $total_amt, $order_nbr);
        $processor->make_sale($track2, $total_amt, $txn_num);
		$processor->talk();
		$processor->read_auth_response();
	}

	else
	{
		$txndatabase->{$err} = 1;
		$processor->{ACTION_CODE} = 'E';
	}

	# we can still use the old $txndatabase->batch() method because it uses the trans_no as the lookup column
	$txndatabase->batch($ORACLE, $machine, $txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status);

    foreach (keys %$processor)
    {
            warn("processor: $_ =>  $processor->{$_}\n");
    }

	# If this sale has been forced already, notify
	if( (defined $force_action_cd) or (defined $force_action_cd_hist) or $err)
	{
	    return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
	    return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}
}


sub t4	# Rix net batch
{
	my $handler = shift;
	my $payload = shift;

	my $writeUSA = 0;

  	my $ORACLE = $handler->{database};
 
	my ($machine,$txn_time, $txn_date, $magstrip, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num) = split /,/,$payload;

# --- while testing PSS we will branch to a different version of the t4 handler (only machine EV14176)
		return t4_PSS($handler, $payload);
# -----------------------------------------------------------------------------------------------------		

	if (length($magstrip) > 80)
	{
	        $magstrip =~ /(.*)\?/;
	        $magstrip = $1;
	}

	my $processor;
    $processor = Paymentech->new( machine => $machine ) if $card_type == 0;
    $processor = SpecialCard->new( machine => $machine ) if $card_type == 1;

	my $txndatabase = Txndatabase->new( processor_obj => $processor );

	my $err;
	if ($auth_num =~ /CANCEL|VENDTOOLONG|DENIED/)
	{
		($err, $auth_num) = split /\|/, $auth_num;
		$txndatabase->{$err} = 1;
		$processor->{ACTION_CODE} = 'E';
	}	

	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);
	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date) = $cardreader->parse($magstrip);
	if (!$err)
	{
		$processor->make_sale($magstrip, $total_amt, $card_num);
		$processor->talk();
		$processor->read_auth_response();

		$writeUSA = 0;
	}

	$txndatabase->local_batch($ORACLE,$machine,  $txn_time, $txn_date, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num);

	if( $writeUSA == 1 )
	{
		warn "Writing USA\n";

		my $trans_no = sprintf "%09i", $txndatabase->{TRANSACTION_NUMBER};

		open(BATCH, ">/opt/transactiond/USA/K$trans_no.PND");

		print BATCH "SA(SX$trans_no),";
		print BATCH "BATCHED-";
		for(my $i = length($machine); $i < 8; $i++)
		{
			print BATCH "0";
		}
		print BATCH "$machine,";
		print BATCH "E.179001283995939427,";
		print BATCH (($txn_amt+$tax_amt)*100);
		print BATCH ",";
		print BATCH (($txn_amt+$tax_amt)*100);
		print BATCH ",";
		print BATCH "$magstrip,";
		print BATCH ",11,1,AP-none,";
		$txn_date =~ /(.*)\/(.*)\//;
		print BATCH "$1$2\n";

		close BATCH;
	}

	foreach (keys %$processor)
	{
		warn("processor: $_ =>  $processor->{$_}\n");
	}

	if( $err )
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}
}


sub t4_PSS	# Rix net batch PSS
{
	# -- SPECIAL NOTE : PSS change regarding database updating --
	#
	# Ordinarily we write a tran record to the database after we have performed a transaction  
	# thru the payment processor. However, to use the Payment Support System we must provide an 'order id'. 
	# To guarantee a unique order id value we must obtain the next trans_no from the transaction_record
	# table using the Oracle sequence defined for it. This 'reserves' a place in the table for the
	# row we will create. This is necessary because we don't want to write the row until we have all 
	# of the data or we'll cause the triggers in the table to go nuts.
	

	my $handler = shift;
	my $payload = shift;

	my $writeUSA = 0;

  	my $ORACLE = $handler->{database};
 
	my ($machine,$txn_time, $txn_date, $magstrip, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num) = split /,/,$payload;

	if (length($magstrip) > 80)
	{
	        $magstrip =~ /(.*)\?/;
	        $magstrip = $1;
	}

	my ($processor, $order_nbr, $client, $track2);
	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date) = $cardreader->parse($magstrip);


	warn "t4_PSS - processing $machine\n";

	$track2 = $cardreader->GetTrack2($magstrip);

	$processor = FHMS->new( machine => $machine) if $card_type == 0;

    $processor = SpecialCard->new( machine => $machine ) if $card_type == 1;

	$processor->{magstrip} = $track2;

	my $txndatabase = Txndatabase->new( processor_obj => $processor );

	my $err;
	if ($auth_num =~ /CANCEL|VENDTOOLONG|DENIED/)
	{
		($err, $auth_num) = split /\|/, $auth_num;
		$txndatabase->{$err} = 1;
		$processor->{ACTION_CODE} = 'E';
	}	

	# before authing we need to obtain an order number
	$order_nbr = $txndatabase->createOrderNbr($ORACLE);
	if (! ($order_nbr > 0) ) 
	{
		# if we fail to generate an order number just exit with failure info
		return 'E', 0, 0;
	}
	$processor->{TRANSACTION_NUMBER} = $order_nbr;

	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);
	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date) = $cardreader->parse($magstrip);
	if (!$err)
	{
        warn "Sale to FHMS\n";
        #$processor->make_sale($track2, $total_amt, $order_nbr);
        $processor->make_force($track2, $total_amt, $order_nbr);

		$processor->talk();
		$processor->read_auth_response();

		$writeUSA = 1;
	}

	#$txndatabase->local_batch($ORACLE,$machine,  $txn_time, $txn_date, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $order_nbr);
	$txndatabase->local_batch_PSS($ORACLE,$machine,  $txn_time, $txn_date, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, undef, undef, $order_nbr);

	if( $writeUSA == 1 )
	{
		warn "Writing USA\n";

		my $trans_no = sprintf "%09i", $txndatabase->{TRANSACTION_NUMBER};

		open(BATCH, ">/opt/transactiond/USA/K$trans_no.PND");

		print BATCH "SA(SX$trans_no),";
		print BATCH "BATCHED-";
		for(my $i = length($machine); $i < 8; $i++)
		{
			print BATCH "0";
		}
		print BATCH "$machine,";
		print BATCH "E.179001283995939427,";
		print BATCH (($txn_amt+$tax_amt)*100);
		print BATCH ",";
		print BATCH (($txn_amt+$tax_amt)*100);
		print BATCH ",";
		print BATCH "$magstrip,";
		print BATCH ",11,1,AP-none,";
		$txn_date =~ /(.*)\/(.*)\//;
		print BATCH "$1$2\n";

		close BATCH;
	}

	foreach (keys %$processor)
	{
		warn("processor: $_ =>  $processor->{$_}\n");
	}

	if( $err )
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}
}


sub t6	#auth handler for ReRix Auth v2
{
	my $handler = shift;
	my $payload = shift;
	
	# the following three fields are no t currently implemented, but are needed for compliance with the authorize() api call

	my $pin;
	my $auth_question_data;
	my $auth_info;

	my $ORACLE = $handler->{database};
	warn("payload t6; $payload\n");

	my ($machine,$magstrip, $auth_amt,
		$card_type, $machine_trans_no) = split /,/, $payload;

# --- while testing PSS we will branch to a different version of the t4 handler (only machine EV14176)
		return t6_PSS($handler, $payload);
# -----------------------------------------------------------------------------------------------------		


	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?',
										'MAG_STRIPE = ?'],
					where_values	=> [$machine_trans_no, $magstrip] );

	if( $txn_ref->[0][0] )
	{
		return $txn_ref->[0][1], $txn_ref->[0][0], 0;
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record_Hist',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?',
										'MAG_STRIPE = ?'],
					where_values	=> [$machine_trans_no, $magstrip] );

	if( $txn_ref->[0][0] )
	{
		# if this sale has completed, something is very wrong
		# return an auth failure

		warn "Sale already completed\n";

		return $txn_ref->[0][1], 'E', 0;
	}

	if (length($magstrip) > 80)
	{
		$magstrip =~ /(.*)\?/;
		$magstrip = $1;
	}


	my $processor;
	$processor = Paymentech->new( machine => $machine ) if $card_type eq 'C';
	$processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';

	warn "Card Type:$card_type:\n";

	$processor->{magstrip} = $magstrip;
   
	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	my $cardreader = CardUtils->new();
	my $track2;
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);

	$processor->make_auth_only($magstrip, $auth_amt, $machine_trans_no);

	$processor->talk();
	$processor->read_auth_response();
	$processor->{ACTION_CODE} = 'E' if $processor->{TIMED_OUT};

	$txndatabase->authorize($ORACLE, $machine, $pin, $auth_amt, $auth_question_data, 
							$auth_info, $card_type, $machine_trans_no);

        foreach (keys %$processor)
        {
                warn("processor: $_ =>  $processor->{$_}\n");
        }


	return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, $processor->{BALANCE};
}


sub t6_PSS	#auth handler for ReRix Auth v2
{

	# -- SPECIAL NOTE : PSS change regarding database updating --
	#
	# Ordinarily we write a tran record to the database after we have performed a transaction  
	# thru the payment processor. However, to use the Payment Support System we must provide an 'order id'. 
	# To guarantee a unique order id value we must obtain the next trans_no from the transaction_record
	# table using the Oracle sequence defined for it. This 'reserves' a place in the table for the
	# row we will create. This is necessary because we don't want to write the row until we have all 
	# of the data or we'll cause the triggers in the table to go nuts.
	

	my $handler = shift;
	my $payload = shift;
	
	# the following three fields are no t currently implemented, but are needed for compliance with the authorize() api call
	my $pin;
	my $auth_question_data;
	my $auth_info;

	my $ORACLE = $handler->{database};
	warn("payload t6_PSS; $payload\n");

	my ($machine,$magstrip, $auth_amt,
		$card_type, $machine_trans_no, $txn_date, $track_data) = split /,/, $payload;

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?'],
					where_values	=> [$machine_trans_no] );

	if( $txn_ref->[0][0] )
	{
		logit "Transaction previously authorized"; 
		return $txn_ref->[0][1], $txn_ref->[0][0], 0, 0;
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record_Hist',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?'],
					where_values	=> [$machine_trans_no] );

	if( $txn_ref->[0][0] )
	{
		# if this sale has completed, something is very wrong
		# return an auth failure

		warn "Sale already completed\n";

		return $txn_ref->[0][1], 'E', 0, 0;
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Failures',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?'],
					where_values	=> [$machine_trans_no] );

	if( $txn_ref->[0][0] )
	{
		logit "Transaction previously failed authorization"; 
		return $txn_ref->[0][1], $txn_ref->[0][0], 0, 0;
	}

	if (length($magstrip) > 80)
	{
		$magstrip =~ /(.*)\?/;
		$magstrip = $1;
	}

	my $processor;
	my ($order_nbr, $client);

	$processor = FHMS->new( machine => $machine)  if $card_type eq 'C' ;

	$processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';

	warn "Card Type:$card_type:\n";
   
	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	# before authing we need to obtain an order number
	$order_nbr = $txndatabase->createOrderNbr($ORACLE);
	if (! ($order_nbr > 0) ) 
	{
		# if we fail to generate an order number just exit with failure info
		return 'E', 0, 0, 0;
	}
	$processor->{TRANSACTION_NUMBER} = $order_nbr;


	my $cardreader = CardUtils->new();
	my $track2;
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);
	$track2 = $cardreader->GetTrack2($magstrip);
	warn "t6_PSS - track2 = $track2\n";
	
	# added 10/06/2004 so we can explicitely tell quick pss wether to use track2 or not
	# if firmware is version 2.2.8 we can not use track2 for live auth because of a bug 
	my $use_track_2 = 'Y';
	my $firmware_version_ref = $ORACLE->select(
					table 			=> 'device_setting, device',
					select_columns	=> 'device_setting.device_setting_value',
					where_columns	=> ['device_setting.device_id = device.device_id', 'device_setting_parameter_cd = ?', 'device.device_active_yn_flag = ?', 'device.device_name = ?'],
					where_values	=> ['Firmware Version', 'Y', $machine] );
					
	if($firmware_version_ref->[0][0] =~ /^USA-E42 V2\.2\.8/)
	{
		logit "Firmware version 2.2.8 detected!  Setting use_track_2 = N";
		$use_track_2 = 'N';
	}
	
	$processor->{magstrip} = $track2;

	$processor->make_auth_only($track2, $auth_amt, $order_nbr, $use_track_2);

	$processor->talk();
	
	$processor->read_auth_response();
	
	$processor->{ACTION_CODE} = 'E' if $processor->{TIMED_OUT};
	
	logit "track2: $track2";

	$txndatabase->authorize_PSS($ORACLE, $machine, $pin, $auth_amt, $auth_question_data, 
							$track2, $card_type, $machine_trans_no, $order_nbr, $txn_date, $track_data);

        foreach (keys %$processor)
        {
                warn("processor: $_ =>  $processor->{$_}\n");
        }


	return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, $processor->{BALANCE}, $txndatabase->{PSS_TRAN_ID};
}


sub t7 # ReRix 1.0 auth
{
	my $handler = shift;
	my $payload = shift;

	my $ORACLE = $handler->{database};

	warn("t7 payload; $payload\n");

	my ($machine,$magstrip, $pin, $auth_amt,
		$auth_question_data, $auth_info,
		$card_type, $machine_trans_no) = split /,/, $payload;

# --- while testing PSS we will branch to a different version of the t4 handler 
		return t7_PSS($handler, $payload);
# -----------------------------------------------------------------------------------------------------		

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?',
										'MAG_STRIPE = ?'],
					where_values	=> [$machine_trans_no, $magstrip] );

	if( $txn_ref->[0][0] )
	{
		return $txn_ref->[0][1], $txn_ref->[0][0], 0;
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record_Hist',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?',
										'MAG_STRIPE = ?'],
					where_values	=> [$machine_trans_no, $magstrip] );

	if( $txn_ref->[0][0] )
	{
		# if this sale has completed, something is very wrong
		# return an auth failure

		warn "Sale already completed\n";

		return $txn_ref->[0][1], 'E', 0;
	}

	if (length($magstrip) > 80)
	{
		$magstrip =~ /(.*)\?/;
		$magstrip = $1;
	}

	my $processor;

	$processor = Paymentech->new( machine => $machine ) if $card_type eq 'C';

	$processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';

	warn "Card Type:$card_type:\n";

	$processor->{magstrip} = $magstrip;
   
	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);

#	$processor->make_auth_only($magstrip, $auth_amt);

	$processor->make_auth_only($magstrip, $auth_amt, $machine_trans_no);



 	# -1.02- hacked solution to Sony auth dupes
	#### bypass doing the auth when it's a Sony that authed within the last 60 seconds..
	#### get the action_code, transaction_number from the existing auth in the database

	my $sony_ref = $ORACLE->select(
					table		=> 'transaction_record a, rerix_initialization b',
					select_columns	=> 'a.trans_no, a.transaction_date, a.auth_action_cd, a.machine_trans_no,' .
										'a.response_msg, a.auth_cd, a.batch_no, a.retrieval_ref_no, a.card_type',
					where_columns	=> ['a.machine_id = b.serial_number',
							"b.model_number = 'SonyTest'",
							'a.auth_amt = ?',
							'a.mag_stripe = ?',
							'a.machine_id = ?',
							"a.transaction_date >= (SYSDATE - .0006944)"], # 60 second difference
					where_values	=> [$auth_amt, 
										$magstrip, 
										$machine] );
	if (defined $sony_ref->[0][0])
	{
		#we found a row that is likely a dupe, so use its auth info
		#and don't process another auth
		$processor->{ACTION_CODE} = $sony_ref->[0][2];
		$processor->{RESPONSE_MESSAGE} = $sony_ref->[0][4];
		$processor->{AUTH_CODE} = $sony_ref->[0][5];
		$processor->{BATCH_NUMBER} = $sony_ref->[0][6];
		$processor->{RETRIEVAL_REF_NUMBER} = $sony_ref->[0][7];
		$processor->{CARD_TYPE} = $sony_ref->[0][8];
		print "\n(T7) Found a Sony dupe auth in the TRANSACTION_RECORD table...\n";
		print "\tORIGINAL_TRANS         : $sony_ref->[0][0]\n";
		print "\tMACHINE_ID             : $machine\n";
		print "\tMAG_STRIPE             : $magstrip\n";
		print "\tTRANSACTION_AMOUNT     : $auth_amt\n";
		print "\tTRAN DATE              : $sony_ref->[0][1]\n";			
		print "\t{ACTION_CODE}          : $processor->{ACTION_CODE}\n";
		print "\t{RESPONSE_MESSAGE}     : $processor->{RESPONSE_MESSAGE}\n";
		print "\t{AUTH_CODE}            : $processor->{AUTH_CODE}\n";
		print "\t{BATCH_NUMBER}         : $processor->{BATCH_NUMBER}\n";
		print "\t{RETRIEVAL_REF_NUMBER} : $processor->{RETRIEVAL_REF_NUMBER}\n";
		print "\t{CARD_TYPE}            : $processor->{CARD_TYPE}\n";
	}
	else
	{
		$processor->talk();
		$processor->read_auth_response();
		$processor->{ACTION_CODE} = 'E' if $processor->{TIMED_OUT};
	}

    # --- disabled and moved above for revision 1.02 ---
    #$processor->talk();
    #$processor->read_auth_response();
    #$processor->{ACTION_CODE} = 'E' if $processor->{TIMED_OUT};


	if( $processor->{magstrip} =~ m#LEVI/LAURA# )
	{
		$processor->{ACTION_CODE} = 'E';
	}

	$txndatabase->authorize($ORACLE, $machine, $pin, $auth_amt, $auth_question_data, 
							$auth_info, $card_type, $machine_trans_no);

        foreach (keys %$processor)
        {
                warn("processor: $_ =>  $processor->{$_}\n");
        }


	return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, $processor->{BALANCE};
}


sub t7_PSS # ReRix 1.0 auth
{
	# -- SPECIAL NOTE : PSS change regarding database updating --
	#
	# Ordinarily we write a tran record to the database after we have performed a transaction  
	# thru the payment processor. However, to use the Payment Support System we must provide an 'order id'. 
	# To guarantee a unique order id value we must obtain the next trans_no from the transaction_record
	# table using the Oracle sequence defined for it. This 'reserves' a place in the table for the
	# row we will create. This is necessary because we don't want to write the row until we have all 
	# of the data or we'll cause the triggers in the table to go nuts.
	
	my $handler = shift;
	my $payload = shift;

	my $ORACLE = $handler->{database};
	warn("t7_PSS payload; $payload\n");

	my ($machine,$magstrip, $pin, $auth_amt,
		$auth_question_data, $auth_info,
		$card_type, $machine_trans_no) = split /,/, $payload;

	my $cardreader = CardUtils->new();
	my $track2 = $cardreader->GetTrack2($magstrip);

	warn "t7_PSS - machine_trans_no = $machine_trans_no\n";
	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?',
										'MAG_STRIPE = ?'],
                                        where_values   => [$machine_trans_no, $track2] );
 
#					where_values	=> [$machine_trans_no, $magstrip] );

	if( $txn_ref->[0][0] )
	{
		warn "!!! t7_PSS - found existing record in transaction record table for this record\n";
		return $txn_ref->[0][1], $txn_ref->[0][0], 0;
		
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record_Hist',
					select_columns	=> 'TRANS_NO, AUTH_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?',
										'MAG_STRIPE = ?'],
                                        where_values   => [$machine_trans_no, $track2] );
				#	where_values	=> [$machine_trans_no, $magstrip] );

	if( $txn_ref->[0][0] )
	{
		# if this sale has completed, something is very wrong
		# return an auth failure

		warn "Sale already completed\n";

		return $txn_ref->[0][1], 'E', 0;
	}

	if (length($magstrip) > 80)
	{
		$magstrip =~ /(.*)\?/;
		$magstrip = $1;
	}

	my $processor;

	$processor = FHMS->new( machine => $machine) if $card_type eq 'C';

	$processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';

	warn "Card Type:$card_type:\n";

	$processor->{CARD_TYPE} = $card_type;
	
	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	# before authing we need to obtain an order number
	my $order_nbr = $txndatabase->createOrderNbr($ORACLE);
	if (! ($order_nbr > 0) ) 
	{
		# if we fail to generate an order number just exit with failure info
		return 'E', 0, 0;
	}
	warn("t7_PSS order_nbr : $order_nbr\n");
	
#	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);

#	$processor->make_auth_only($magstrip, $auth_amt);

	warn "t7_PSS - processing $machine\n";
	#my $track2 = $cardreader->GetTrack2($magstrip);
	warn "t7_PSS - track2: $track2\n";
		
	$processor->{magstrip} = $track2;   
	warn "t7_PSS - processor->{magstrip}: $processor->{magstrip}\n";

	$processor->make_auth_only($track2, $auth_amt, $order_nbr);

 	# -1.02- hacked solution to Sony auth dupes
	#### bypass doing the auth when it's a Sony that authed within the last 60 seconds..
	#### get the action_code, transaction_number from the existing auth in the database

	my $sony_ref = $ORACLE->select(
					table		=> 'transaction_record a, rerix_initialization b',
					select_columns	=> 'a.trans_no, a.transaction_date, a.auth_action_cd, a.machine_trans_no,' .
										'a.response_msg, a.auth_cd, a.batch_no, a.retrieval_ref_no, a.card_type',
					where_columns	=> ['a.machine_id = b.serial_number',
							"b.model_number = 'SonyTest'",
							'a.auth_amt = ?',
							'a.mag_stripe = ?',
							'a.machine_id = ?',
							"a.transaction_date >= (SYSDATE - .0006944)"], # 60 second difference
					where_values	=> [$auth_amt, 
										$track2, 
										$machine] );

	if (defined $sony_ref->[0][0])
	{
		#we found a row that is likely a dupe, so use its auth info
		#and don't process another auth
		$processor->{ACTION_CODE} = $sony_ref->[0][2];
		$processor->{RESPONSE_MESSAGE} = $sony_ref->[0][4];
		$processor->{AUTH_CODE} = $sony_ref->[0][5];
		$processor->{BATCH_NUMBER} = $sony_ref->[0][6];
		$processor->{RETRIEVAL_REF_NUMBER} = $sony_ref->[0][7];
		$processor->{CARD_TYPE} = $sony_ref->[0][8];
		print "\n(T7_PSS) Found a Sony dupe auth in the TRANSACTION_RECORD table...\n";
		print "\tORIGINAL_TRANS         : $sony_ref->[0][0]\n";
		print "\tMACHINE_ID             : $machine\n";
		print "\tMAG_STRIPE             : $magstrip\n";
		print "\tTRANSACTION_AMOUNT     : $auth_amt\n";
		print "\tTRAN DATE              : $sony_ref->[0][1]\n";			
		print "\t{ACTION_CODE}          : $processor->{ACTION_CODE}\n";
		print "\t{RESPONSE_MESSAGE}     : $processor->{RESPONSE_MESSAGE}\n";
		print "\t{AUTH_CODE}            : $processor->{AUTH_CODE}\n";
		print "\t{BATCH_NUMBER}         : $processor->{BATCH_NUMBER}\n";
		print "\t{RETRIEVAL_REF_NUMBER} : $processor->{RETRIEVAL_REF_NUMBER}\n";
		print "\t{CARD_TYPE}            : $processor->{CARD_TYPE}\n";
	}
	else
	{
		$processor->talk();
		$processor->read_auth_response();
		$processor->{ACTION_CODE} = 'E' if $processor->{TIMED_OUT};
	}

    # --- disabled and moved above for revision 1.02 ---
    #$processor->talk();
    #$processor->read_auth_response();
    #$processor->{ACTION_CODE} = 'E' if $processor->{TIMED_OUT};


	if( $processor->{magstrip} =~ m#LEVI/LAURA# )
	{
		$processor->{ACTION_CODE} = 'E';
	}

	$txndatabase->authorize_PSS($ORACLE, $machine, $pin, $auth_amt, $auth_question_data, 
							$track2, $card_type, $machine_trans_no, $order_nbr);

        foreach (keys %$processor)
        {
                warn("processor: $_ =>  $processor->{$_}\n");
        }


	return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, $processor->{BALANCE};
}

sub t8	# ReRix 1.0 net batch
{
	my $handler = shift;
	my $payload = shift;

	my ($machine, $machine_trans_no, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status) = split /,/, $payload;

# --- while testing PSS we will branch to a different version of the t8 handler (only machine EV14176)
		return t8_PSS($handler, $payload);
# -----------------------------------------------------------------------------------------------------		

	my $ORACLE = $handler->{database};

	my $processor;

	my $txn_ref = $ORACLE->select(
						table			=> 'Transaction_Record',
						select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
											'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
											'AUTH_ACTION_CD, TRANS_NO, ' .
											'FORCE_ACTION_CD',
						where_columns	=> ['MACHINE_TRANS_NO = ?'],
						where_values	=> [$machine_trans_no] );

	if( ! defined $txn_ref->[0] )
	{
		$processor->{ACTION_CODE} = 'E';
		warn("transaction not in database\n");
		return 0;
	}

	my ($card_type, $magstrip, $card_num, $auth_amt, $auth_code,
		$action_code, $txn_num, $force_action_cd ) = @{$txn_ref->[0]};

	if( $force_action_cd eq 'A' )
	{
		warn("transaction already forced sale\n");

		return $force_action_cd, $txn_num, 0;
	}

	
	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

	# credit card types
	my @cccardtype = ('MC','VI','DS','AE','DC','JC','ENR');

    $processor = Paymentech->new( machine => $machine ) if grep (/$card_type/, @cccardtype);
    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'SP';

	my $cardreader = CardUtils->new();

	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);

    $processor = Paymentech->new( machine => $machine ) if grep (/$card_type/, @cccardtype);

	$processor->{magstrip} = $magstrip;

    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'SP';

	$processor->{magstrip} = $magstrip;

	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	$txndatabase->{TRANSACTION_NUMBER} = $txn_num;

	if( $txn_status eq 'T' )
	{
		warn "Sale timeout\n";
		$txndatabase->{VENDTOOLONG} = 1;
		$processor->{ACTION_CODE} = 'E';
	}
    elsif ( $txn_status eq 'D') 
    {
           warn "Sale Denied \n";
        
           $txndatabase->{DENIED} = 1;
           $processor->{ACTION_CODE} = 'E';
    }
    elsif (($txn_status =~ m/S|R|N|Q|0/) && ($txn_amt > 0)) #elsif ( $txn_status =~ /S|R|N|Q|0||/) # bug fix - 12/03/2003 - pcowan 
    {

			warn "Sale to Paymentech\n";
            $processor->make_sale($magstrip, $total_amt, $card_num) if ($action_code eq 'T');
            $processor->make_force($magstrip, $total_amt, $auth_code) if ($action_code eq 'A');
            $processor->talk();
            $processor->read_auth_response();
    }

    else #if ($txn_status =~ /C|F|I|U/) MDH: Logic was changed.  The defaulted transaction status is now canceled
    {
            warn "Sale cancelled\n";

            $txndatabase->{CANCEL} = 1;
            $processor->{ACTION_CODE} = 'E';
    } 


	$txndatabase->batch($ORACLE, $machine, $txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status, $machine_trans_no);

        foreach (keys %$processor)
        {
                warn("processor: $_ =>  $processor->{$_}\n");
        }

	if( defined $force_action_cd )
	{
		# if it's already been completed, don't adjust inventory
		warn "Inv = 0\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	elsif ( $txn_status =~ /F|C|T|U|D/)
	{
		# if a product wasn't vended
		warn "Inv = 0\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
		warn "Inv = 1\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}

}

sub t8_PSS	# ReRix 1.0 net batch
{

	my $handler = shift;
	my $payload = shift;

	my ($machine, $machine_trans_no, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status) = split /,/, $payload;
	
	# kludge - 04/13/2004 - pcowan
	if($txn_status eq ' ')
	{
		if($txn_amt > 0)
		{
			$txn_status = 'S';
			warn "kludging txn_status           : $txn_status\n";
		}
		else
		{
			$txn_status = 'C';
			warn "kludging txn_status           : $txn_status\n";
		}			
	}

	my $ORACLE = $handler->{database};

	my $processor;

	my $txn_ref = $ORACLE->select(
						table			=> 'Transaction_Record',
						select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
											'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
											'AUTH_ACTION_CD, TRANS_NO, ' .
											'FORCE_ACTION_CD',
						where_columns	=> ['MACHINE_TRANS_NO = ?'],
						where_values	=> [$machine_trans_no] );

	if( ! defined $txn_ref->[0] )
	{
		$processor->{ACTION_CODE} = 'E';
		warn("transaction not in database\n");
		return 0;
	}

	my ($card_type, $magstrip, $card_num, $auth_amt, $auth_code,
		$action_code, $txn_num, $force_action_cd ) = @{$txn_ref->[0]};

	if( $force_action_cd eq 'A' )
	{
		warn("transaction already forced sale\n");

		return $force_action_cd, $txn_num, 0;
	}
	
	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

	# credit card types
	my @cccardtype = ('MC','VI','DS','AE','DC','JC','ENR');

	my ($order_nbr, $client, $track2);
	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);

	#(undef, $client, $order_nbr) = split /:/,$machine_trans_no ;

	warn "t8_PSS - processing $machine\n";

	$track2 = $cardreader->GetTrack2($magstrip);

	$processor->{magstrip} = $track2;

	$processor = FHMS->new( machine => $machine) if grep (/$card_type/, @cccardtype);

    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'SP';

	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	$txndatabase->{TRANSACTION_NUMBER} = $txn_num;

	if( $txn_status eq 'T' )
	{
		warn "Sale timeout\n";
		$txndatabase->{VENDTOOLONG} = 1;
		$processor->{ACTION_CODE} = 'E';
	}
    elsif ( $txn_status eq 'D') 
    {
           warn "Sale Denied \n";
        
           $txndatabase->{DENIED} = 1;
           $processor->{ACTION_CODE} = 'E';
    }
    elsif (($txn_status =~ m/S|R|N|Q|0/) && ($txn_amt > 0)) #elsif ( $txn_status =~ /S|R|N|Q|0||/) # bug fix - 12/03/2003 - pcowan 
    {
			# changed code back to original (desperation move!) 5/10/2003
            $processor->make_sale($track2, $total_amt, $txn_num); 
            #$processor->make_sale($track2, $total_amt, $txn_num) if ($action_code eq 'T');
            #$processor->make_force($track2, $total_amt, $txn_num) if ($action_code eq 'A');
            $processor->talk();
            $processor->read_auth_response();
    }

    else #if ($txn_status =~ /C|F|I|U/) MDH: Logic was changed.  The defaulted transaction status is now canceled
    {
            warn "Sale cancelled\n";

            $txndatabase->{CANCEL} = 1;
            $processor->{ACTION_CODE} = 'E';
    } 

	# can use the existing batch()
	$txndatabase->batch($ORACLE, $machine, $txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status, $machine_trans_no);

        foreach (keys %$processor)
        {
                warn("processor: $_ =>  $processor->{$_}\n");
        }

	if( defined $force_action_cd )
	{
		# if it's already been completed, don't adjust inventory
		warn "Inv = 0\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	elsif ( $txn_status =~ /F|C|T|U|D/)
	{
		# if a product wasn't vended
		warn "Inv = 0\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
		warn "Inv = 1\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}

}

sub t9	# ReRix 1.0 force batch
{
	my $handler = shift;
	my $payload = shift;

	my $writeUSA = 0;

  	my $ORACLE = $handler->{database};
 	
 	# -1.01- added $timestamp to param list
	my ($machine, $txn_time, $txn_date, $magstrip, $card_type, $col_num, $inv_num, $txn_amt, $tax_amt, $button_num, $txn_status, $machine_trans_no, $timestamp) = split /,/,$payload;

# --- while testing PSS we will branch to a different version of the t9 handler (only machine EV14176)
		return t9_PSS($handler, $payload);
# -----------------------------------------------------------------------------------------------------		

	# ---- Dupe checks ----

	# this test works for most devices, but not for Sony
	my $txn_ref = $ORACLE->select(
						table			=> 'Transaction_Record',
						select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
											'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
											'AUTH_ACTION_CD, TRANS_NO, ' .
											'FORCE_ACTION_CD',
						where_columns	=> ['MACHINE_TRANS_NO = ?',
											'MAG_STRIPE = ?'],
						where_values	=> [$machine_trans_no, $magstrip] );
						
	# -1.01- if no rows were returned do the alternate test for Sony just to make sure...
	print "\nTransactionHandler.pm: " . '$txn_ref before Sony check returned TRANS_NO = ' . $txn_ref->[0][6] . "\n";
	my $txn_date_time = $txn_date . " " . $txn_time;
	if (($txn_ref)->[0][6] == '' )
	{
		print "\nNow checking for Sony dupes...\n";
		
		print '$txn_date_time = ' . $txn_date_time . "\n";

		$txn_ref = $ORACLE->select(
						table		=> 'Transaction_Record',
						select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
								'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
								'AUTH_ACTION_CD, TRANS_NO, ' .
								'FORCE_ACTION_CD',
						where_columns	=> ['MACHINE_ID = ?',
								'MAG_STRIPE = ?',
								'AUTH_AMT = ?',
								"TRANSACTION_DATE = to_date(?, 'MM/DD/YYYY HH24:MI:SS')"],
						where_values	=> [$machine_trans_no, 
									$magstrip, 
									$txn_amt, 
									$txn_date_time ] );
	
		print "\nTransactionHandler.pm: " . '$txn_ref after Sony check returned TRANS_NO = ' . $txn_ref->[0][6] . "\n";
		print "\tMACHINE_ID		: $machine\n";
		print "\tMAG_STRIPE		: $magstrip\n";
		print "\tAUTH_AMT		: $txn_amt\n";
		print "\tTIMESTAMP       	: $timestamp\n";
		print "\tDATE			: $txn_date\n";
		print "\tTIME			: $txn_time\n";
		print "\tDATETIME               : $txn_date_time\n";
	}
	

	if( $txn_ref->[0][0] )
	{
		# if the transaction has not had a forced sale, but has an auth,
		if( (!$txn_ref->[0][7]) and ($txn_ref->[0][5] eq 'A') )
		{
			# convert this transaction to a network authed batch

			print "\nConverting to network authed batch\n";

			return t8($handler, "$machine,$machine_trans_no,$col_num,$inv_num,$txn_amt,$tax_amt,,,$button_num,$txn_status");
		}
		elsif( $txn_ref->[0][7] )
		{
			# there is a forced sale, do nothing

			print "\nAlready forced sale on this trans\n";

			return $txn_ref->[0][7], $txn_ref->[0][6], 0;
		}

		warn "\nNo appproved auth, forcing sale\n";

		# left are transactions with no forced sale or auth, process normally
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record_Hist',
					select_columns	=> 'TRANS_NO, FORCE_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?',
										'MAG_STRIPE = ?'],
					where_values	=> [$machine_trans_no, $magstrip] );

	# -1.01- once again, if no rows were returned do the alternate test for Sony just to make sure...
	
	if (!defined($txn_ref))
	{
		my $txn_ref = $ORACLE->select(
						table			=> 'Transaction_Record_Hist',
						select_columns	=> 'TRANS_NO, FORCE_ACTION_CD',
                                         	where_columns   => ['MACHINE_ID = ?',
                                                                     'MAG_STRIPE = ?',
                                                                     'AUTH_AMT = ?',
                                                                     "TRANSACTION_DATE = to_date(?, 'MM/DD/YYYY HH24:MI:SS')"],
                                                     where_values    => [$machine_trans_no,
                                                                             $magstrip,
                                                                             $txn_amt,
                                                                             $txn_date_time ] );
	}

	if( $txn_ref->[0][0] )
	{
		print "\nSale already forced\n";

		return $txn_ref->[0][1], $txn_ref->[0][0], 0;
	}


	if (length($magstrip) > 80)
	{
        $magstrip =~ /(.*)\?/;
        $magstrip = $1;
	}

	my $processor;

	$processor = Paymentech->new( machine => $machine ) if $card_type eq 'C';

	$processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';
	
	my $cardreader = CardUtils->new();
	
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);
	
    $processor = Paymentech->new( machine => $machine ) if $card_type eq 'C';

    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';

	$processor->{magstrip} = $magstrip;

	my $txndatabase = Txndatabase->new( processor_obj => $processor );

	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);
	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);

    if( $txn_status eq 'T' )
    {
            print "\nSale timeout\n";
            $txndatabase->{VENDTOOLONG} = 1;
            $processor->{ACTION_CODE} = 'E';
    }
    elsif ( $txn_status eq 'D')
    {
           print "\nSale Denied \n";

           $txndatabase->{DENIED} = 1;
           $processor->{ACTION_CODE} = 'E';
    }
    elsif (($txn_status =~ m/S|R|N|Q|0/) && ($txn_amt > 0)) #elsif ( $txn_status =~ /S|R|N|Q|0||/) # bug fix - 12/03/2003 - pcowan
    {

		warn "Sale to Paymentech\n";
    	$processor->make_sale($magstrip, $total_amt, $card_num); #if ($action_code eq 'T');
        $processor->talk();
        $processor->read_auth_response();
    }

    else #if ($txn_status =~ /C|F|I|U/) MDH: Logic was changed.  The default transaction status is now canceled
    {
            warn "Sale cancelled\n";

            $txndatabase->{CANCEL} = 1;
            $processor->{ACTION_CODE} = 'E';
    }
	

	$txndatabase->local_batch($ORACLE, $machine, $txn_time, $txn_date, $card_type, '', $col_num, $inv_num, $txn_amt, $tax_amt, '', '', $button_num, $txn_status, $machine_trans_no);

	if ( $writeUSA == 1 )
	{
		warn "Writing USA\n";

		my $trans_no = sprintf "%09i", $txndatabase->{TRANSACTION_NUMBER};

		open(BATCH, ">/opt/transactiond/USA/K$trans_no.PND");

		print BATCH "SA(SX$trans_no),";
		print BATCH "BATCHED-";
		for(my $i = length($machine); $i < 8; $i++)
		{
			print BATCH "0";
		}
		print BATCH "$machine,";
		print BATCH "E.179001283995939427,";
		print BATCH (($txn_amt+$tax_amt)*100);
		print BATCH ",";
		print BATCH (($txn_amt+$tax_amt)*100);
		print BATCH ",";
		print BATCH "$magstrip,";
		print BATCH ",11,1,AP-none,";

		$txn_date =~ /(.*)\/(.*)\//;
		print BATCH "$1$2\n";

		close BATCH;
	}

	foreach (keys %$processor)
	{
		warn("processor: $_ =>  $processor->{$_}\n");
	}

	if ( $txn_status =~ /F|C|T|U|D/)
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}
}

sub t9_PSS	# ReRix 1.0 force batch
{
	# -- SPECIAL NOTE : PSS change regarding database updating --
	#
	# Ordinarily we write a tran record to the database after we have performed a transaction  
	# thru the payment processor. However, to use the Payment Support System we must provide an 'order id'. 
	# To guarantee a unique order id value we must obtain the next trans_no from the transaction_record
	# table using the Oracle sequence defined for it. This 'reserves' a place in the table for the
	# row we will create. This is necessary because we don't want to write the row until we have all 
	# of the data or we'll cause the triggers in the table to go nuts.
	

	my $handler = shift;
	my $payload = shift;

	warn "t9_PSS() - payload : $payload\n";
	my $writeUSA = 0;

  	my $ORACLE = $handler->{database};
 	
 	my $cardreader = CardUtils->new();

 	# -1.01- added $timestamp to param list
	my ($machine, $txn_time, $txn_date, $magstrip, $card_type, $col_num, $inv_num, $txn_amt, $tax_amt, $button_num, $txn_status, $machine_trans_no, $timestamp) = split /,/,$payload;
	
	# kludge - 04/13/2004 - pcowan
	if($txn_status eq ' ')
	{
		if($txn_amt > 0)
		{
			$txn_status = 'S';
			warn "kludging txn_status           : $txn_status\n";
		}
		else
		{
			$txn_status = 'C';
			warn "kludging txn_status           : $txn_status\n";
		}			
	}

 	my $track2 = $cardreader->GetTrack2($magstrip) if $card_type ne 'S';

	warn "t9_PSS() - payload          : $payload\n";
	warn "t9_PSS() - txn_status       : $txn_status\n";
	warn "t9_PSS() - machine_trans_no : $machine_trans_no\n";
	warn "t9_PSS() - magstrip         : $magstrip\n";
	warn "t9_PSS() - track2           : $track2\n";

	# ---- Dupe checks ----

	# this test works for most devices, but not for Sony
	my $txn_ref = $ORACLE->select(
						table			=> 'Transaction_Record',
						select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
											'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
											'AUTH_ACTION_CD, TRANS_NO, ' .
											'FORCE_ACTION_CD',
						#where_columns	=> ['MACHINE_TRANS_NO = ?',
						#					'MAG_STRIPE = ?'],
						#where_values	=> [$machine_trans_no, $magstrip, $track2] );	
						where_columns	=> ['(MACHINE_TRANS_NO = ?) and (MAG_STRIPE = ? or MAG_STRIPE = ?)'],
						where_values	=> [$machine_trans_no, $magstrip, $track2] );

						
	# -1.01- if no rows were returned do the alternate test for Sony just to make sure...
	print "\nTransactionHandler.pm: " . '$txn_ref before Sony check returned TRANS_NO = ' . $txn_ref->[0][6] . "\n";
	my $txn_date_time = $txn_date . " " . $txn_time;
	if (($txn_ref)->[0][6] == '' )
	{
		print "\nNow checking for Sony dupes...\n";
		
		print '$txn_date_time = ' . $txn_date_time . "\n";

		$txn_ref = $ORACLE->select(
						table		=> 'Transaction_Record',
						select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
								'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
								'AUTH_ACTION_CD, TRANS_NO, ' .
								'FORCE_ACTION_CD',
						where_columns	=> ['MACHINE_ID = ?',
								#'MAG_STRIPE = ?',
								'(MAG_STRIPE = ? or MAG_STRIPE = ?)',
								'AUTH_AMT = ?',
								"TRANSACTION_DATE = to_date(?, 'MM/DD/YYYY HH24:MI:SS')"],
						where_values	=> [$machine_trans_no, 
									$magstrip, 
									$track2, 
									$txn_amt, 
									$txn_date_time ] );
	
		print "\nTransactionHandler.pm: " . '$txn_ref after Sony check returned TRANS_NO = ' . $txn_ref->[0][6] . "\n";
		print "\tMACHINE_ID     : $machine\n";
		print "\tMAG_STRIPE     : $magstrip\n";
		print "\tAUTH_AMT       : $txn_amt\n";
		print "\tTIMESTAMP      : $timestamp\n";
		print "\tDATE           : $txn_date\n";
		print "\tTIME           : $txn_time\n";
		print "\tDATETIME       : $txn_date_time\n";
	}

	if( $txn_ref->[0][0] )
	{
		# if the transaction has not had a forced sale, but has an auth,
		if( (!$txn_ref->[0][7]) and ($txn_ref->[0][5] eq 'A') )
		{
			# convert this transaction to a network authed batch

			print "\nConverting to network authed batch\n";

			return t8($handler, "$machine,$machine_trans_no,$col_num,$inv_num,$txn_amt,$tax_amt,,,$button_num,$txn_status");
		}
		elsif( $txn_ref->[0][7] )
		{
			# there is a forced sale, do nothing

			print "\nAlready forced sale on this trans\n";

			return $txn_ref->[0][7], $txn_ref->[0][6], 0;
		}

		warn "\nNo appproved auth, forcing sale\n";

		# left are transactions with no forced sale or auth, process normally
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record_Hist',
					select_columns	=> 'TRANS_NO, FORCE_ACTION_CD',
#					where_columns	=> ['MACHINE_TRANS_NO = ?',
#										'MAG_STRIPE = ?'],
					where_columns	=> ['(MACHINE_TRANS_NO = ?) and (MAG_STRIPE = ? or MAG_STRIPE = ?)'],
					where_values	=> [$machine_trans_no, $magstrip, $track2] );

	# -1.01- once again, if no rows were returned do the alternate test for Sony just to make sure...
	
	if (!defined($txn_ref))
	{
		my $txn_ref = $ORACLE->select(
						table			=> 'Transaction_Record_Hist',
						select_columns	=> 'TRANS_NO, FORCE_ACTION_CD',
                                         	where_columns   => ['MACHINE_ID = ?',
                                                                     #'MAG_STRIPE = ?',
                                                                     '(MAG_STRIPE = ? or MAG_STRIPE = ?)',
                                                                     'AUTH_AMT = ?',
                                                                     "TRANSACTION_DATE = to_date(?, 'MM/DD/YYYY HH24:MI:SS')"],
                                                     where_values    => [$machine_trans_no,
                                                                             $magstrip,
                                                                             $track2,
                                                                             $txn_amt,
                                                                             $txn_date_time ] );
	}

	if( $txn_ref->[0][0] )
	{
		print "\nSale already forced\n";

		return $txn_ref->[0][1], $txn_ref->[0][0], 0;
	}


	if (length($magstrip) > 80)
	{
        $magstrip =~ /(.*)\?/;
        $magstrip = $1;
	}

	my $processor;
	
	my ($order_nbr, $client);
	
	#my $cardreader = CardUtils->new();
	
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);
	
	$processor = FHMS->new( machine => $machine);
	
	warn "t9_PSS() - magstrip : $magstrip\n";
	
	#$track2 = $cardreader->GetTrack2($magstrip) if $card_type ne 'S';

	warn "t9_PSS() - track2 : $track2\n";
    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';

	$processor->{magstrip} = $track2;

	my $txndatabase = Txndatabase->new( processor_obj => $processor );

	# before forcing the tran we need to obtain an order number
	my $order_nbr = $txndatabase->createOrderNbr($ORACLE);
	if (! ($order_nbr > 0) ) 
	{
		# if we fail to generate an order number just exit with failure info
		return 'E', 0, 0;
	}
	
	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

    if( $txn_status eq 'T' )
    {
            print "\nSale timeout\n";
            $txndatabase->{VENDTOOLONG} = 1;
            $processor->{ACTION_CODE} = 'E';
    }
    elsif ( $txn_status eq 'D')
    {
           print "\nSale Denied \n";

           $txndatabase->{DENIED} = 1;
           $processor->{ACTION_CODE} = 'E';
    }
    elsif (($txn_status =~ m/S|R|N|Q|0/) && ($txn_amt > 0)) #elsif ( $txn_status =~ /S|R|N|Q|0||/) # bug fix - 12/03/2003 - pcowan
    {

			warn "Force to FHMS\n";
			$processor->make_force($track2, $total_amt, $order_nbr); 
            $processor->talk();
            $processor->read_auth_response();
    }

    else #if ($txn_status =~ /C|F|I|U/) MDH: Logic was changed.  The default transaction status is now canceled
    {
            warn "Sale cancelled\n";

            $txndatabase->{CANCEL} = 1;
            $processor->{ACTION_CODE} = 'E';
    }
	

	$txndatabase->local_batch_PSS($ORACLE, $machine, $txn_time, $txn_date, $card_type, '', $col_num, $inv_num, $txn_amt, $tax_amt, '', '', $button_num, $txn_status, $machine_trans_no, $order_nbr );

	if ( $writeUSA == 1 )
	{
		warn "Writing USA\n";

		my $trans_no = sprintf "%09i", $txndatabase->{TRANSACTION_NUMBER};

		open(BATCH, ">/opt/transactiond/USA/K$trans_no.PND");

		print BATCH "SA(SX$trans_no),";
		print BATCH "BATCHED-";
		for(my $i = length($machine); $i < 8; $i++)
		{
			print BATCH "0";
		}
		print BATCH "$machine,";
		print BATCH "E.179001283995939427,";
		print BATCH (($txn_amt+$tax_amt)*100);
		print BATCH ",";
		print BATCH (($txn_amt+$tax_amt)*100);
		print BATCH ",";
		print BATCH "$magstrip,";
		print BATCH ",11,1,AP-none,";

		$txn_date =~ /(.*)\/(.*)\//;
		print BATCH "$1$2\n";

		close BATCH;
	}

	foreach (keys %$processor)
	{
		warn("processor: $_ =>  $processor->{$_}\n");
	}

	if ( $txn_status =~ /F|C|T|U|D/)
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}
}

sub t10	# ReRix 2.0 net batch
{
	my $handler = shift;
	my $payload = shift;

	logit "Executing t10() process\n";

	my ($machine, $machine_trans_no, $txn_amt, $tax_amt, 
		$txn_status, $vend_qty, %vended_items) = split /,/, $payload;

	my $ORACLE = $handler->{database};
	
	# PSS override...
	return t10_PSS($handler, $payload);
	
	my $processor;

	# *** DEBUGGING CODE START ****	
	logit "ORACLE = $ORACLE";
	logit "machine = $machine";
	logit "txn_amt = $txn_amt";
	logit "tax_amt = $tax_amt";
	logit "txn_status = $txn_status";
	logit "machine_trans_no = '$machine_trans_no'";

	my $key;
	foreach $key (%vended_items)
	{
		if ( defined($vended_items{$key}{QTY}) )
		{
			logit "\t\tColumn : $key";
			logit "\t\tAMT    : $vended_items{$key}{AMT}";
			logit "\t\tQTY    : $vended_items{$key}{QTY}\n";
		}
	}	
	# *** DEBUGGING CODE END ****	

	my $txn_ref = $ORACLE->select(
					table		=> 'Transaction_Record',
					select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
							'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
							'AUTH_ACTION_CD, TRANS_NO, ' .
							'FORCE_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?'],
					where_values	=> [$machine_trans_no] );

	if( ! defined $txn_ref->[0] )
	{
		$processor->{ACTION_CODE} = 'E';
		warn("transaction not in database\n");
		return 0;
	}

	my ($card_type, $magstrip, $card_num, $auth_amt, $auth_code,
		$action_code, $txn_num, $force_action_cd ) = @{$txn_ref->[0]};

	if( $force_action_cd eq 'A' )
	{
		warn("transaction already forced sale\n");

		return $force_action_cd, $txn_num, 0;
	}
	
	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

	# credit card types
	my @cccardtype = ('MC','VI','DS','AE','DC','JC','ENR');

	my $cardreader = CardUtils->new();

	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);

    $processor = Paymentech->new( machine => $machine ) if grep (/$card_type/, @cccardtype);

	$processor->{magstrip} = $magstrip;

    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'SP';

	my $txndatabase = Txndatabase->new( processor_obj => $processor);

	$txndatabase->{TRANSACTION_NUMBER} = $txn_num;

	if( $txn_status eq 'T' )
	{
		warn "Sale timeout\n";
		$txndatabase->{VENDTOOLONG} = 1;
		$processor->{ACTION_CODE} = 'E';
	}
    elsif ( $txn_status eq 'D') 
    {
           warn "Sale Denied \n";
        
           $txndatabase->{DENIED} = 1;
           $processor->{ACTION_CODE} = 'E';
    }
    elsif (($txn_status =~ m/S|R|N|Q|0/) && ($txn_amt > 0)) #elsif ( $txn_status =~ /S|R|N|Q|0||/) # bug fix - 12/03/2003 - pcowan 
    {
            
			warn "Sale to Paymentech\n";
            $processor->make_sale($magstrip, $total_amt, $card_num) if ($action_code eq 'T');
            $processor->make_force($magstrip, $total_amt, $auth_code) if ($action_code eq 'A');

            $processor->talk();
            $processor->read_auth_response();
    }
    else #if ($txn_status =~ /C|F|I|U/) MDH: Logic was changed.  The defaulted transaction status is now canceled
    {
            warn "Sale cancelled\n";

            $txndatabase->{CANCEL} = 1;
            $processor->{ACTION_CODE} = 'E';
    } 


	$txndatabase->batch_v2($ORACLE, $machine, $txn_num, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, %vended_items);

    foreach (keys %$processor)
    {
            warn("processor: $_ =>  $processor->{$_}\n");
    }

	if( defined $force_action_cd )
	{
		# if it's already been completed, don't adjust inventory
		warn "Inv = 0\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	elsif ( $txn_status =~ /F|C|T|U|D/)
	{
		# if a product wasn't vended
		warn "Inv = 0\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
		warn "Inv = 1\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}

}


sub t10_PSS	# ReRix 2.0 net batch
{
	# -- SPECIAL NOTE : PSS change regarding database updating --
	#
	# Ordinarily we write a tran record to the database after we have performed a transaction  
	# thru the payment processor. However, to use the Payment Support System we must provide an 'order id'. 
	# To guarantee a unique order id value we must obtain the next trans_no from the transaction_record
	# table using the Oracle sequence defined for it. This 'reserves' a place in the table for the
	# row we will create. This is necessary because we don't want to write the row until we have all 
	# of the data or we'll cause the triggers in the table to go nuts.
	
	my $handler = shift;
	my $payload = shift;

	logit "Executing t10_PSS() process\n";

	my ($machine, $machine_trans_no, $txn_amt, $tax_amt, $txn_status, $vend_qty, %vended_items) = split /,/, $payload;
	my $ORACLE = $handler->{database};
	my $processor;
	
	# kludge - 04/13/2004 - pcowan
	if($txn_status eq ' ')
	{
		if($txn_amt > 0)
		{
			$txn_status = 'S';
			warn "kludging txn_status           : $txn_status\n";
		}
		else
		{
			$txn_status = 'C';
			warn "kludging txn_status           : $txn_status\n";
		}			
	}

	# *** DEBUGGING CODE START ****	
	logit "ORACLE = $ORACLE";
	logit "machine = $machine";
	logit "txn_amt = $txn_amt";
	logit "tax_amt = $tax_amt";
	logit "txn_status = $txn_status";
	logit "machine_trans_no = '$machine_trans_no'";

	my $key;
	foreach $key (%vended_items)
	{		
		if ( defined($vended_items{$key}{QTY}) )
		{
			logit "\t\tColumn : $key";
			logit "\t\tAMT    : $vended_items{$key}{AMT}";
			logit "\t\tQTY    : $vended_items{$key}{QTY}\n";
		}
	}	
	# *** DEBUGGING CODE END ****	

	# first, test to see if already batched
	my $txn_ref = $ORACLE->select(
					table			=> 'Transaction_Record_Hist',
					select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
									   'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
									   'AUTH_ACTION_CD, TRANS_NO, ' .
									   'FORCE_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?'],
					where_values	=> [$machine_trans_no] );

	if($txn_ref->[0][6])
	{
		logit "Transaction previously batched";
		return($txn_ref->[0][7], $txn_ref->[0][6], 0, 0);
	}

	my $txn_ref = $ORACLE->select(
					table			=> 'Transaction_Record',
					select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
									   'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
									   'AUTH_ACTION_CD, TRANS_NO, ' .
									   'FORCE_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?'],
					where_values	=> [$machine_trans_no] );

	if(!defined $txn_ref->[0])
	{
		$processor->{ACTION_CODE} = 'E';
		warn("transaction not in database\n");
		return 0;
	}

	my ($card_type, $magstrip, $card_num, $auth_amt, $auth_code, $action_code, $txn_num, $force_action_cd ) = @{$txn_ref->[0]};

	if( $force_action_cd eq 'A' )
	{
		warn("transaction already forced sale\n");
		return $force_action_cd, $txn_num, 0, 0;
	}
	
	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

	# credit card types
	my @cccardtype = ('MC','VI','DS','AE','DC','JC','ENR');

	#my ($order_nbr, $track2);
	my ($track2);

	my $cardreader = CardUtils->new();
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);
	$track2 = $cardreader->GetTrack2($magstrip);
	$processor->{magstrip} = $track2;

	$processor = FHMS->new(machine => $machine) if grep (/$card_type/, @cccardtype);
    $processor = SpecialCard->new(machine => $machine) if $card_type eq 'SP';

	my $txndatabase = Txndatabase->new( processor_obj => $processor);
	$txndatabase->{TRANSACTION_NUMBER} = $txn_num;

	if($txn_status eq 'T')
	{
		warn "Sale timeout\n";
		$txndatabase->{VENDTOOLONG} = 1;
		$processor->{ACTION_CODE} = 'E';
	}
    elsif ( $txn_status eq 'D') 
    {
           warn "Sale Denied \n";
           $txndatabase->{DENIED} = 1;
           $processor->{ACTION_CODE} = 'E';
    }
    elsif (($txn_status =~ m/S|R|N|Q|0/) && ($txn_amt > 0)) #elsif ( $txn_status =~ /S|R|N|Q|0||/) # bug fix - 12/03/2003 - pcowan
    {
            #$processor->make_sale($total_amt, $card_num) if ($action_code eq 'T');
            #$processor->make_force($track2, $total_amt, $auth_code) if ($action_code eq 'A');
            warn "Sale to FHMS\n";
            
            if($total_amt > $auth_amt)
            {
            	# the device allowed a sale for more than the auth amount...
            	# payment manager craps out when this happens
            	# the easiest way to fix it is to cancel the auth and force a new batch
            	
            	warn "Total transaction amount is greater than the auth amount!! ($total_amt > $auth_amt)\n
            		  Canceling the previous transaction in PSS and forcing a new one for $total_amt\n";
				
            	$processor->make_cancel($txn_num);
				$processor->talk();
				$processor->read_auth_response();
				
				warn "Forcing new batch for automatically canceled transaction $txn_num\n";

				$processor->make_force($track2, $total_amt, $txn_num);
				$processor->talk();
				$processor->read_auth_response();
            }
            else
            {
				$processor->make_sale($track2, $total_amt, $txn_num);
				$processor->talk();
				$processor->read_auth_response();
            }
    }
    else #if ($txn_status =~ /C|F|I|U/) MDH: Logic was changed.  The defaulted transaction status is now canceled
    {
		warn "Sale cancelled\n";
            
		$processor->make_cancel($txn_num);
		$processor->talk();
		$processor->read_auth_response();


		$txndatabase->{CANCEL} = 1;
		$processor->{ACTION_CODE} = 'A';
    } 

	$txndatabase->batch_v2($ORACLE, $machine, $txn_num, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, %vended_items);

    foreach (keys %$processor)
    {
            warn("processor: $_ =>  $processor->{$_}\n");
    }

	if( defined $force_action_cd )
	{
		# if it's already been completed, don't adjust inventory
		warn "Inv = 0\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0, $txndatabase->{PSS_TRAN_ID};
	}
	elsif ( $txn_status =~ /F|C|T|U|D/)
	{
		# if a product wasn't vended
		warn "Inv = 0\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0, $txndatabase->{PSS_TRAN_ID};
	}
	else
	{
		warn "Inv = 1\n";
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1, $txndatabase->{PSS_TRAN_ID};
	}

}

sub t11	# ReRix 2.0 force batch
{
	my $handler = shift;
	my $payload = shift;

	my $writeUSA = 0;

	logit "Executing t11() process\n";

  	my $ORACLE = $handler->{database};

	my ($machine, $txn_time, $txn_date, $magstrip, $card_type, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, $timestamp, $vend_qty, %vended_items) = split /,/,$payload;

	# PSS override...
	return t11_PSS($handler, $payload);
		
	# this test works for most devices, but not for Sony
	my $txn_ref = $ORACLE->select(
						table			=> 'Transaction_Record',
						select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
											'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
											'AUTH_ACTION_CD, TRANS_NO, ' .
											'FORCE_ACTION_CD',
						where_columns	=> ['MACHINE_TRANS_NO = ?',
											'MAG_STRIPE = ?'],
						where_values	=> [$machine_trans_no, $magstrip] );
						
	my $txn_date_time = $txn_date . " " . $txn_time;

	# *** DEBUGGING CODE START ****	
	logit "ORACLE = $ORACLE";
	logit "machine = $machine";
	logit "txn_amt = $txn_amt";
	logit "tax_amt = $tax_amt";
	logit "txn_status = $txn_status";
	logit "machine_trans_no = '$machine_trans_no'";
	logit "magstrip = $magstrip";
	logit "card_type = $card_type";
	logit "vend_qty = $vend_qty";

	my $key;
	foreach $key (%vended_items)
	{
		if ( defined($vended_items{$key}{QTY}) )
		{
			logit "\t\tColumn : $key";
			logit "\t\tAMT    : $vended_items{$key}{AMT}";
			logit "\t\tQTY    : $vended_items{$key}{QTY}\n";
		}
	}	
	# *** DEBUGGING CODE END ****	

	if( $txn_ref->[0][6] )
	{
		# if the transaction has not had a forced sale, but has an auth,
		if( (!$txn_ref->[0][7]) and ($txn_ref->[0][5] eq 'A') )
		{
			# convert this transaction to a network authed batch

			logit "Converting to network authed batch v2";

			return t10($handler, "$machine,$machine_trans_no,$txn_amt,$tax_amt,$txn_status,$vend_qty,%vended_items");
		}
		elsif( $txn_ref->[0][7] )
		{
			# there is a forced sale, do nothing

			logit "Already forced sale on this trans";

			return $txn_ref->[0][7], $txn_ref->[0][6], 0;
		}

		logit "No approved auth, forcing sale";

		# left are transactions with no forced sale or auth, process normally
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record_Hist',
					select_columns	=> 'TRANS_NO, FORCE_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?',
										'MAG_STRIPE = ?'],
					where_values	=> [$machine_trans_no, $magstrip] );


	if( $txn_ref->[0][0] )
	{
		logit "Sale already forced";

		return $txn_ref->[0][1], $txn_ref->[0][0], 0;
	}


	if (length($magstrip) > 80)
	{
        $magstrip =~ /(.*)\?/;
        $magstrip = $1;
	}

	my $processor;
	
	my $cardreader = CardUtils->new();
	
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);
	
    $processor = Paymentech->new( machine => $machine ) if $card_type eq 'C';

    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';

	$processor->{magstrip} = $magstrip;

	my $txndatabase = Txndatabase->new( processor_obj => $processor );

	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

    if( $txn_status eq 'T' )
    {
            logit "Sale timeout";
            $txndatabase->{VENDTOOLONG} = 1;
            $processor->{ACTION_CODE} = 'E';
    }
    elsif ( $txn_status eq 'D')
    {
           logit "Sale Denied";

           $txndatabase->{DENIED} = 1;
           $processor->{ACTION_CODE} = 'E';
    }
    elsif (($txn_status =~ m/S|R|N|Q|0/) && ($txn_amt > 0)) #elsif ( $txn_status =~ /S|R|N|Q|0||/) # bug fix - 12/03/2003 - pcowan
    {
			warn "Sale to Paymentech\n";
        	$processor->make_sale($magstrip, $total_amt, $card_num); #if ($action_code eq 'T');
            $processor->talk();
            $processor->read_auth_response();
    }

    else #if ($txn_status =~ /C|F|I|U/) MDH: Logic was changed.  The default transaction status is now canceled
    {
            warn "Sale cancelled\n";
            $txndatabase->{CANCEL} = 1;
            $processor->{ACTION_CODE} = 'E';
    }
	
	$txndatabase->local_batch_v2($ORACLE, $machine, $txn_time, $txn_date, $card_type, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, %vended_items);

	foreach (keys %$processor)
	{
		warn("processor: $_ =>  $processor->{$_}\n");
	}

	if ( $txn_status =~ /F|C|T|U|D/)
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0;
	}
	else
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1;
	}
}


sub t11_PSS	# ReRix 2.0 force batch
{
	# -- SPECIAL NOTE : PSS change regarding database updating --
	#
	# Ordinarily we write a tran record to the database after we have performed a transaction  
	# thru the payment processor. However, to use the Payment Support System we must provide an 'order id'. 
	# To guarantee a unique order id value we must obtain the next trans_no from the transaction_record
	# table using the Oracle sequence defined for it. This 'reserves' a place in the table for the
	# row we will create. This is necessary because we don't want to write the row until we have all 
	# of the data or we'll cause the triggers in the table to go nuts.
	
	my $handler = shift;
	my $payload = shift;

	my $writeUSA = 0;

	logit "Executing t11_PSS() process\n";

  	my $ORACLE = $handler->{database};

	my ($machine, $txn_time, $txn_date, $magstrip, $card_type, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, $timestamp, $track_data, $vend_qty, %vended_items) = split /,/,$payload;

	# kludge - 04/13/2004 - pcowan
	if($txn_status eq ' ')
	{
		if($txn_amt > 0)
		{
			$txn_status = 'S';
			warn "kludging txn_status           : $txn_status\n";
		}
		else
		{
			$txn_status = 'C';
			warn "kludging txn_status           : $txn_status\n";
		}			
	}

	# try searching the TRANSACTION_RECORD table first for an existing tran
	my $txn_ref = $ORACLE->select(
						table			=> 'Transaction_Record',
						select_columns	=> 'CARD_TYPE, MAG_STRIPE, ' .
											'CARD_NUMBER, AUTH_AMT, AUTH_CD, ' .
											'AUTH_ACTION_CD, TRANS_NO, ' .
											'FORCE_ACTION_CD',
						where_columns	=> ['MACHINE_TRANS_NO = ?'],
						where_values	=> [$machine_trans_no] );
						
	my $txn_date_time = $txn_date . " " . $txn_time;
	
	# *** DEBUGGING CODE START ****	
	logit "ORACLE = $ORACLE";
	logit "machine = $machine";
	logit "txn_amt = $txn_amt";
	logit "tax_amt = $tax_amt";
	logit "txn_status = $txn_status";
	logit "machine_trans_no = '$machine_trans_no'";
	logit "magstrip = $magstrip";
	logit "card_type = $card_type";
	logit "vend_qty = $vend_qty";

	my $key;
	foreach $key (%vended_items)
	{
		if ( defined($vended_items{$key}{QTY}) )
		{
			logit "\t\tColumn : $key";
			logit "\t\tAMT    : $vended_items{$key}{AMT}";
			logit "\t\tQTY    : $vended_items{$key}{QTY}\n";
		}
	}	
	# *** DEBUGGING CODE END ****	

	if( $txn_ref->[0][6] )
	{
		# if the transaction has not had a forced sale, but has an auth,
		if( (!$txn_ref->[0][7]) and ($txn_ref->[0][5] eq 'A') )
		{
			# convert this transaction to a network authed batch

			logit "Converting to network authed batch v2";

			return t10_PSS($handler, "$machine,$machine_trans_no,$txn_amt,$tax_amt,$txn_status,$vend_qty,%vended_items");
		}
		elsif( $txn_ref->[0][7] )
		{
			# there is a forced sale, do nothing

			logit "Already forced sale on this trans";

			return $txn_ref->[0][7], $txn_ref->[0][6], 0, 0;
		}

		logit "No approved auth, forcing sale";

		# left are transactions with no forced sale or auth, process normally
	}

	my $txn_ref = $ORACLE->select(
					table 			=> 'Transaction_Record_Hist',
					select_columns	=> 'TRANS_NO, FORCE_ACTION_CD',
					where_columns	=> ['MACHINE_TRANS_NO = ?'],
					where_values	=> [$machine_trans_no] );


	if( $txn_ref->[0][0] )
	{
		logit "Sale already forced";

		return $txn_ref->[0][1], $txn_ref->[0][0], 0, 0;
	}


	if (length($magstrip) > 80)
	{
        $magstrip =~ /(.*)\?/;
        $magstrip = $1;
	}

	my $processor;
	
	my ($order_nbr, $track2);
	
	my $cardreader = CardUtils->new();
	
	my ($card_num, $customer_name, $exp_date, undef, $svc_code, $other_data) = $cardreader->parse($magstrip);
	
	$processor = FHMS->new( machine => $machine);

	$track2 = $cardreader->GetTrack2($magstrip);	#if $card_type eq 'C';

    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'S';

	$processor->{magstrip} = $track2;

	my $txndatabase = Txndatabase->new( processor_obj => $processor );

	# before forcing the tran we need to obtain an order number
	my $order_nbr = $txndatabase->createOrderNbr($ORACLE);
	if (! ($order_nbr > 0) ) 
	{
		# if we fail to generate an order number just exit with failure info
		return 'E', 0, 0, 0;
	}

	my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);

    if( $txn_status eq 'T' )
    {
            logit "Sale timeout";
            $txndatabase->{VENDTOOLONG} = 1;
            $processor->{ACTION_CODE} = 'E';
    }
    elsif ( $txn_status eq 'D')
    {
           logit "Sale Denied";

           $txndatabase->{DENIED} = 1;
           $processor->{ACTION_CODE} = 'E';
    }
    elsif (($txn_status =~ m/S|R|N|Q|0/) && ($txn_amt > 0)) #elsif ( $txn_status =~ /S|R|N|Q|0||/) # bug fix - 12/03/2003 - pcowan
    {
			if ($card_type eq 'S') {
				warn "Sale to SpecialCard\n"
			}
			else {
				warn "Sale to FHMS\n";
			}
			$processor->make_force($track2, $total_amt, $order_nbr); #if ($action_code eq 'T');
            $processor->talk();
            $processor->read_auth_response();
    }

    else #if ($txn_status =~ /C|F|I|U/) MDH: Logic was changed.  The default transaction status is now canceled
    {
            warn "Sale cancelled\n";

            $txndatabase->{CANCEL} = 1;
            $processor->{ACTION_CODE} = 'E';
    }
	

	$txndatabase->local_batch_v2($ORACLE, $machine, $txn_time, $txn_date, $card_type, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, $order_nbr, $track_data, $track2, %vended_items);


	foreach (keys %$processor)
	{
		warn("processor: $_ =>  $processor->{$_}\n");
	}

	if ( $txn_status =~ /F|C|T|U|D/)
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 0, $txndatabase->{PSS_TRAN_ID};
	}
	else
	{
		return $processor->{ACTION_CODE}, $txndatabase->{TRANSACTION_NUMBER}, 1, $txndatabase->{PSS_TRAN_ID};
	}
}


sub logit($)
{
	my $line = shift;

	warn "[" . localtime() . "] $line\n";
}

1;

