package SpecialCard;
require 5.004;
use strict;
use IO::Socket;

BEGIN
{



sub new
{
	my $class = shift;
	my $arg = {};
	%$arg = @_;

		$arg->{CARD_TYPE}	= 'SP';
                $arg->{SALE}            = '01';
                $arg->{AUTH_ONLY}       = '02';
                $arg->{FORCE}           = '03';
                $arg->{RETURN}          = '06';
                $arg->{VOID}            = '41';
                $arg->{BATCH_INQUIRY}   = '50';
                $arg->{BATCH_RELEASE}   = '51';
		$arg->{TIMED_OUT} 	= 0;
		$arg->{SEQUENCE_NUMBER} = '000000';
		$arg->{TERMINAL_NUMBER} = '000';


	bless $arg,$class;

}

sub make_auth_only
{
	
	my $self = shift;
	my $magstrip = shift;
	my $auth_amount = shift;
	$self->{TRANSACTION_TYPE} = $self->{AUTH_ONLY};
	$self->{CHARGE_OFF} = '0';

	$self->{REQUEST} = qq{t1,$self->{machine},,$magstrip,,$auth_amount,,,,,};

	return 1;

}

sub make_sale
{

	my $self = shift;
	my ($magstrip,$total_amount,$transaction_number) = @_;

	$self->{TRANSACTION_TYPE} = $self->{SALE};
	$self->{CHARGE_OFF} = '0';

	$self->{REQUEST} = "t2,$self->{machine},$transaction_number,$magstrip,,,,,$total_amount,";
	
	return 1;

}

sub make_force
{

	my $self = shift;
	my ($magstrip,$total_amount,$authorization_code) = @_;

	$self->{TRANSACTION_TYPE} = $self->{FORCE};
	$self->{CHARGE_OFF} = '0';
	$self->{REQUEST} = "t4,$self->{machine},,$magstrip,,,,,$total_amount,$authorization_code";

	return 1;

}

sub make_cancel
{
	my ($self, $order_id) = @_;
	$self->{TRANSACTION_TYPE} = $self->{CANCEL};
	$self->{ORDER_ID} = $order_id;
}

sub talk
{

	my $self = shift;

	my $socket = IO::Socket::INET->new(PeerAddr => '127.0.0.1',	#production
	#my $socket = IO::Socket::INET->new(PeerAddr => 'localhost',		#development
					PeerPort => '10103',
					Proto => 'tcp',
					Timeout => 5);


	
	if (!$socket)
	{
		$self->{TIMED_OUT} = 1;
		return 0;
	}

	print $socket $self->{REQUEST} . "\n";
	my $line = <$socket>;
	chomp $line;

	$self->{RESPONSE} = $line;

	$socket->close();

	return 1;

}

sub read_auth_response
{
	my $self  = shift;

	($self->{ACTION_CODE},$self->{BALANCE},$self->{magstrip}) = split /\,/, $self->{RESPONSE};

	warn("ACTION CODE: $self->{ACTION_CODE} BALANCE: $self->{BALANCE}\n");
	
	return 1;
}

END { }

}  

1;
