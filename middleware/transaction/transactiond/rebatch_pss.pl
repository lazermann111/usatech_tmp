#!/usr/bin/perl

#---------------------------------------------------------------------------------------------
#	Description
#	-----------
#	Used for rebatching transactions that were not correctly batched with PSS
#
# Change History
# --------------
# Version       Date            Programmer      Description
# -------       ----------      ----------      ----------------------------------------------
# 1.00          12/22/2002      T Shannon       Original Version
# 2.00			06/28/2004		P Cowan			I'm gonna make this work with Payment Manager and First Horizon and fix it up a bit.
# 2.01			06/30/2004		M Heilman		Changed SQL, removed Database.pm usage
# 2.02			07/02/2004		P Cowan			Call to make_force doesn work in all cases.  Changed sql to
#												pull quick_pss state and make different calls depending on state
# 2.03			08/12/2004		P Cowan			Removed where clause to pickup force_action_cd = E... as long as
#												force_response_msg is an error it should get retried
# 2.04			10/22/2004		P Cowan			Added reprocessing of AUTH_DECLINE transactions for particular response codes.
#

# Rebatch List:
#
#	com.ibm.etill.framework.clientap
#	Communication Timeout
#	Cassette Error
#

use strict;

use lib '/opt/transactiond';
use pss_interface;
use Txndatabase;
use CardUtils;
use DBI;

my $days_old = $ARGV[0];
if(not defined $days_old)
{
	print "Usage: perl ./rebatch_pss.pl <days old> [-debug]\n";
	exit(1);
}

my $debug = 0;
if($ARGV[1] eq '-debug')
{
	&output("DEBUG ON");
	$debug = 1;
}

#Connect to DB using DBI
#$ENV{ORACLE_HOME} = "/opt/OraHome1";
my $dbHandle = (DBI->connect('DBI:Oracle:USADBP', 'web_user', 'wxkj21a9'));

#Find all transactions that failed because of cassette errors or communication time outs
my $get_failed_trans_sql =
    'SELECT machine_id, trans_no, mag_stripe, transaction_amount, card_type, transaction_status, tax_amount, pss.wcp_tran_id, pss.state '
  . 'FROM transaction_record_hist, pss@pss_tulsap1 '
  . 'WHERE trans_no = pss.client_tran_id (+)  '
  . 'AND (trunc(batch_date) > trunc((sysdate - :days_old)) AND trunc(batch_date) <= trunc(sysdate)) '
  . 'AND force_retries < 5  '
  . 'AND card_type IN (\'MC\', \'VI\', \'DS\', \'AE\', \'DC\', \'JC\', \'ENR\')  '
  . 'AND (   UPPER(force_response_msg) LIKE \'COM.IBM.ETILL.FRAMEWORK%\' OR '
  . '        UPPER(force_response_msg) LIKE \'CASSETTE ERROR%\' OR '
  . '        UPPER(force_response_msg) LIKE \'COMMUNICATION TIMEOUT%\' OR '
  . '        UPPER(force_response_msg) LIKE \'AUTH DECLINED%\' OR '
  . '        UPPER(force_response_msg) LIKE \'AUTH DOWN%\' OR '
  . '        UPPER(force_response_msg) LIKE \'DECLINED%\' OR '
  . '        UPPER(force_response_msg) LIKE \'PSORDER STATE%\' OR '
  . '        UPPER(force_response_msg) LIKE \'%IS ALREADY APPROVED\' OR '
  . '        UPPER(force_response_msg) LIKE \'FORCE_REBATCH\' OR '
  . '        UPPER(force_response_msg) LIKE \'FINANCIAL FAILURE\' ) ';

if($debug)
{
	output($get_failed_trans_sql);
}

my $get_failed_trans_stmt = $dbHandle->prepare($get_failed_trans_sql) || die "Couldn't prepare statement: " . $dbHandle->errstr;
$get_failed_trans_stmt->bind_param(":days_old", $days_old);
$get_failed_trans_stmt->execute();

my $failed_tran_aref = $get_failed_trans_stmt->fetchall_arrayref();

# this is our standard currency code, needed in some places
my $currency  = '840';

# Loop through each failed transaction that met the criteria and retry the settlement
foreach my $failed_tran_aref (@{$failed_tran_aref})
{
	# these are the values we will get back from the processor
	my ($ResultXML, $ResultCode, $ResultText);
	
	# these are the values we will use to update the database
	my ($code, $message);	

	my $machine_id         = $failed_tran_aref->[0];
	my $trans_no           = $failed_tran_aref->[1];
	my $mag_stripe         = $failed_tran_aref->[2];
	my $transaction_amount = $failed_tran_aref->[3];
	my $card_type          = $failed_tran_aref->[4];
	my $transaction_status = $failed_tran_aref->[5];
	my $tax_amount         = $failed_tran_aref->[6];
	my $wcp_tran_id        = $failed_tran_aref->[7];
	my $state              = $failed_tran_aref->[8];

	my $total_amt = sprintf("%.2f", ($transaction_amount + $tax_amount));

	my $cardreader = CardUtils->new();
	my $track2     = $cardreader->GetTrack2($mag_stripe);

	&output("Processing $trans_no           : $wcp_tran_id, $state, $machine_id, $total_amt, $card_type, $mag_stripe");
	&output("Track2 $trans_no               : $track2");
	
	if ($state eq 'NEW_ORDER' || $state eq 'AUTH_DECLINE')
	{
		&output("State $trans_no                : Approve, Deposit");
		
		# order exists, but we still need to approve and deposit
		
		if($debug)
		{
			($ResultCode, $ResultText) = ('0', 'Debug OrderApprove');
		}
		else
		{
			$ResultXML = &pss_Send_Request("OrderApprove", $machine_id, $trans_no, ($total_amt * 100));
			($ResultCode, $ResultText) = &pss_Parse_Result($ResultXML);
		}
		
		if($ResultCode eq '0')
		{
			&output("OrderApprove $trans_no         : Success!");
			
			if($debug)
			{
				($ResultCode, $ResultText) = ('0', 'Debug pss_Batch');
			}
			else
			{
				# batch order like it was a live auth
				($ResultCode, $ResultText) = pss_Batch (	$machine_id,
														$trans_no,
														($total_amt * 100),
														$currency,
														$track2);
			}
													
			if($ResultCode eq '0')
			{
				&output("pss_Batch $trans_no             : success ($ResultCode, $ResultText)");

				$code = 'A';
				$message = $ResultText;
			}
			else
			{
				&output("pss_Batch $trans_no             : failed! ($ResultCode, $ResultText)");
				
				$code = 'E';
				$message = $ResultText;
			}
		}
		else
		{
			&output("OrderApprove $trans_no         : failed! ($ResultCode, $ResultText)");
			
			$code = 'E';
			$message = $ResultText;
		}
	}
	elsif ($state eq 'AUTH_SUCCESS')
	{
		&output("State $trans_no                : Deposit");
		
		if($debug)
		{
			($ResultCode, $ResultText) = ('0', 'Debug pss_Batch');
		}
		else
		{
			# order exists and has been authorized, but need to be batched like it was a live auth
			($ResultCode, $ResultText) = pss_Batch (	$machine_id,
														$trans_no,
														($total_amt * 100),
														$currency,
														$track2);
		}

		if($ResultCode eq '0')
		{
			&output("pss_Batch $trans_no            : success ($ResultCode, $ResultText)");

			$code = 'A';
			$message = $ResultText;
		}
		else
		{
			&output("pss_Batch $trans_no            : failed! ($ResultCode, $ResultText)");
			
			$code = 'E';
			$message = $ResultText;
		}
	}
	elsif ((not defined $state) || $state eq '' || length($state) == 0)
	{
		&output("State $trans_no                : Create, Approve, Deposit");
		
		if($debug)
		{
			($ResultCode, $ResultText) = ('0', 'Debug pss_LocalAuthBatch');
		}
		else
		{
			# force order like it was a local auth
			($ResultCode, $ResultText) = pss_LocalAuthBatch (	$machine_id,
																$trans_no,
																($total_amt * 100),
																$track2,
																$currency);
		}
																
		if($ResultCode eq '0')
		{
			&output("pss_LocalAuthBatch $trans_no   : success ($ResultCode, $ResultText)");

			$code = 'A';
			$message = $ResultText;
		}
		else
		{
			&output("pss_LocalAuthBatch $trans_no   : failed! ($ResultCode, $ResultText)");
			
			$code = 'E';
			$message = $ResultText;
		}
	}
	elsif ($state eq 'WAITING_FOR_SETTLEMENT' || $state eq 'CLOSED' || $state eq 'SETTLE_SUCCESS')
	{
		# this transaction is fine, so just update our records but don't do any reprocessing
		&output("State $trans_no                : $state, not processing but adjusting status");
		
		$code = 'A';
		$message = $trans_no;
	}
	elsif ($state eq 'SETTLE_FAIL')
	{
		# i'm going to treat this one like a success, but not sure if that's correct...
		# transaction was processed correctly but quick_pss thinks the batch failed to close
		&output("State $trans_no                : $state, not processing but adjusting status");
		
		$code = 'A';
		$message = $trans_no;
	}
#	elsif ( $state eq 'AUTH_DECLINE' )
#	{
#		# rebatch these??  not now but maybe later
#		
#		&output("State $trans_no                : $state, looking up and updating auth response text");
#		
#		# query payman for the declined response text and update our database
#		
#		my $get_resp_sql = 'select authresponsetext from payman.visanetpayment@pss_tulsap1 where ordernumber = :wcp_tran_id';
#		my $get_resp_text_stmt = $dbHandle->prepare($get_resp_sql) || die "Couldn't prepare statement: " . $dbHandle->errstr;
#		$get_resp_text_stmt->bind_param(":wcp_tran_id", $wcp_tran_id);
#		$get_resp_text_stmt->execute();
#		my @payman_data = $get_resp_text_stmt->fetchrow_array();
#		if(not defined @payman_data)
#		{
#			die "Failed to lookup authresponsetext for ordernumber $wcp_tran_id\n";
#		}
#		$get_resp_text_stmt->finish();
#
#		$code = 'E';
#		$message = $payman_data[0];
#	}
	else
	{
		# do nothing with these
		&output("State $trans_no                : WARNING - Unhandled state! ($state)");
	}   
	
	if(defined  $code)
	{
		if($ResultCode eq '30' && $message =~ /^Order (.*) is already approved$/)
		{
			# we found a case where quick_pss is not up-to-date with payment manager
			# in this case we will update quick_pss, then next time this program is run the settlement will work right
			
			my $update_quick_pss_sql = 'update pss@pss_tulsap1 set state = :state where wcp_tran_id = :wcp_tran_id';
			
			output("Update $trans_no               : update pss\@pss_tulsap1 set state = AUTH_SUCCESS where wcp_tran_id = $wcp_tran_id");
			
			if(!$debug)
			{
				my $update_quick_pss_stmt = $dbHandle->prepare($update_quick_pss_sql) || die "Couldn't prepare statement: " . $dbHandle->errstr;
				$update_quick_pss_stmt->bind_param(":state", "AUTH_SUCCESS");
				$update_quick_pss_stmt->bind_param(":wcp_tran_id", $wcp_tran_id);
				$update_quick_pss_stmt->execute();
				$update_quick_pss_stmt->finish();
			}
		}
		else
		{
			# Update the transaction with the result codes from the latest settlement attempt
			# Only the most recent result code will be stored in the db.
			
			my $update_tran_sql =
		    "UPDATE /*+ INDEX (transaction_record_hist PK_TRANSACTION_NO_NEW) */ transaction_record_hist "
		  	. "SET force_date = sysdate, "
		 	. "force_action_cd = :code, "
			. "force_response_msg = SUBSTR(:message,0,32), "
			. "force_retries = NVL(force_retries, 0) + 1 "
			. "WHERE trans_no = :trans_no ";
			
			output("Update $trans_no               : force_action_cd = $code, force_response_msg = " . substr($message,0,32));
			
			if(!$debug)
			{
				my $update_tran_stmt = $dbHandle->prepare($update_tran_sql) || die "Couldn't prepare statement: " . $dbHandle->errstr;
				$update_tran_stmt->bind_param(":code", $code);
				$update_tran_stmt->bind_param(":message", $message);
				$update_tran_stmt->bind_param(":trans_no", $trans_no);
				$update_tran_stmt->execute();
				$update_tran_stmt->finish();
			}
		}
	}

	sleep(2);
	#exit(0);
}

$get_failed_trans_stmt->finish();

$dbHandle->disconnect();

sub output
{
	my $line = shift;
	print "[" . localtime() . "] $line\n";
}

1;
