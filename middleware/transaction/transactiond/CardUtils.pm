package CardUtils;
use strict;

BEGIN
{

use Exporter	();
use vars	qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
$VERSION	= '0.01';
@ISA		= qw(Exporter);
@EXPORT		= qw();
@EXPORT_OK	= qw();

sub new
{

	my $class = shift;
	my $this = {};
	bless $this, $class;

}

sub type
{
	my $object = shift;
	my $aD = shift;
	my $i = 0;

	my @aH = (
		[MC=> '5[1-5]', 16, 10, "Master Card"],
		[VI=>'4', 16, 10, "Visa"],
		[VI=>'4', 13, 10, "Visa"],
		[DS=>'6011', 16, 10, "Discover"],
 		[AE=>'34|37', 15, 10, "American Express"],
		[DC=>'30[0-5]|36|38', 14, 10, "Dinners Club/Carte Blanche"],
 		[JC=>'3', 16, 10, "JCB"],
 		[ENR=>'2014|2149', 15, 1, "enRoute"],
 		[JC=>'2131|1800', 15, 10, "JCB"],
	);

	$aD =~ s/\D//g;
	my @aE = reverse split //, $aD;
	my $len = length($aD);
	return undef if($len < 13);
	my $aF =0;

	for($i=0; $i<$len; $i++)
	{
		my $aC = $aE[$i]*(1+ ($i%2));
		$aF += int $aC/10 + $aC%10;
	}

	for (@aH)
	{
		next if $len != $_->[2];
		next if not $aD =~ /^($_->[1])/;
		next if ($aF % $_->[3]);
		return $_->[0];
	}
}

sub typeAbbrev
{
	my $object = shift;
	my $aD = shift;
	my $i = 0;

	my @aH = (
		[MC=> '5[1-5]', 16, 10, "MC"],
		[VI=>'4', 16, 10, "VI"],
		[VI=>'4', 13, 10, "VI"],
		[DS=>'6011', 16, 10, "DS"],
 		[AE=>'34|37', 15, 10, "AE"],
		[DC=>'30[0-5]|36|38', 14, 10, "DC"],
 		[JC=>'3', 16, 10, "JC"],
 		[ENR=>'2014|2149', 15, 1, "ER"],
 		[JC=>'2131|1800', 15, 10, "JC"],
	);

	$aD =~ s/\D//g;
	my @aE = reverse split //, $aD;
	my $len = length($aD);
	return undef if($len < 13);
	my $aF =0;

	for($i=0; $i<$len; $i++)
	{
		my $aC = $aE[$i]*(1+ ($i%2));
		$aF += int $aC/10 + $aC%10;
	}

	for (@aH)
	{
		next if $len != $_->[2];
		next if not $aD =~ /^($_->[1])/;
		next if ($aF % $_->[3]);
		return $_->[0];
	}
}


sub parse
{
	my $object = shift;
	my $magstrip = shift;

#	warn "parse() magstrip = $magstrip\n";

	if(	!($magstrip =~ /^B/) and
		!($magstrip =~ /^S/) and
		!(index($magstrip, "=")) )
	{
		return ('', '', '', '', '', '');
	}

	my ($format_code, $card_number, $numpos, $customer_name, $namepos, $yy, $mm, $exp_date, $other_data, $svc_code);

#	my $numpos = index($magstrip, "^");
#	my $namepos = index($magstrip, "^", $numpos+1);
#	my $format_code = substr($magstrip, 0, 1);
#
#	my $card_number = substr($magstrip, 1, $numpos-1);
#	# remove any spaces and dashes present in card number
#	$card_number =~ s/ //g;
#	$card_number =~ s/-//g;
#	my $customer_name = substr($magstrip, $numpos+1, $namepos-$numpos-1);
#	my  $yy = substr($magstrip, $namepos+1, 2);
#	my $mm = substr($magstrip, $namepos+3, 2);
#	my $exp_date = $yy.$mm;
#	my $other_data = substr($magstrip, $namepos+1);
#	my $svc_code = substr($magstrip, $namepos+5, 11);

#	return ($card_number, $customer_name, $exp_date, $format_code, $svc_code, $other_data);

	$numpos = index($magstrip, "^");

	# if $numpos = 0 then this is a track2 magstripe, otherwise it's track1 so return a 'moto' swipe string
	
#	warn "**** numpos = $numpos\n";
	
	if ($numpos == -1)
	{
		# this must be a track2 swipe
#		warn "**** found track2\n";
#		warn "**** data = $magstrip\n";
		$format_code = '';
	
		my $fspos = index($magstrip, '=');	#position of the '=' sign after the card number
		
		if ($fspos == -1)
		{
			$card_number = $magstrip;
			$yy = '';
			$mm = '';
			$exp_date = '';
			$other_data = '';
			$svc_code = '';
		}
		else
		{
			$card_number = substr($magstrip, 0, $fspos);
			$yy = substr($magstrip, $fspos+1, 2);
			$mm = substr($magstrip, $fspos+3, 2);
			$exp_date = $yy.$mm;
			$other_data = substr($magstrip, $fspos+1);
			$svc_code = substr($magstrip, $fspos+5, 11);
		}
		
		# remove any spaces and dashes present in card number
		$card_number =~ s/ //g;
		$card_number =~ s/-//g;
	
		$customer_name = '';
#		warn "found track1\n";
#		warn "card_number   : $card_number\n";
#		warn "exp_date      : $exp_date\n";
	}
	else
	{
		# this must be a track1 swipe so return a 'moto' string in lieu of track2
		$format_code = substr($magstrip, 0, 1);
		$namepos = index($magstrip, "^", $numpos+1);
	
		$card_number = substr($magstrip, 1, $numpos-1);
		# remove any spaces and dashes present in card number
		$card_number =~ s/ //g;
		$card_number =~ s/-//g;
	
		$customer_name = substr($magstrip, $numpos+1, $namepos-$numpos-1);
		$yy = substr($magstrip, $namepos+1, 2);
		$mm = substr($magstrip, $namepos+3, 2);
		$exp_date = $yy.$mm;
		$other_data = substr($magstrip, $namepos+1);
		$svc_code = substr($magstrip, $namepos+5, 11);
#		warn "found track1\n";
#		warn "card_number   : $card_number\n";
#		warn "exp_date      : $exp_date\n";
	}
	
	return ($card_number, $customer_name, $exp_date, $format_code, $svc_code, $other_data);	
}

sub GetTrack2()
{
	my $object = shift;
	my $magstrip = shift;
	
	warn "GetTrack2() magstrip = $magstrip\n";
	
	if(	!($magstrip =~ /^B/) and
		!($magstrip =~ /^S/) and
		!(index($magstrip, "=")) )
	{
		return ('', '', '', '');
	}

	my $numpos = index($magstrip, "^");

	# if $numpos = 0 then this is a track2 magstripe, otherwise it's track1 so return a 'moto' swipe string
	my $namepos = index($magstrip, "^", $numpos+1);
	
	warn "CardUtils.GetTrack2() - magstrip : $magstrip\n";
	
	if (($numpos == -1) or (substr($magstrip, 0, 1) eq 'S'))
	{
		# this must be a track2 swipe or Evend card
		warn "**** found track2 or Evend card\n";
		return $magstrip;
	}
	else
	{
			
		# this must be a track1 swipe so return a 'moto' string in lieu of track2
		my $format_code = substr($magstrip, 0, 1);
	
		my $card_number = substr($magstrip, 1, $numpos-1);
		# remove any spaces, dashes, or commas present in card number
		$card_number =~ s/ //g;
		$card_number =~ s/-//g;
		$card_number =~ s/,//g;
	
		my $customer_name = substr($magstrip, $numpos+1, $namepos-$numpos-1);
		my  $yy = substr($magstrip, $namepos+1, 2);
		my $mm = substr($magstrip, $namepos+3, 2);
		my $exp_date = $yy.$mm;
		warn "found track1\n";
		warn "card_number   : $card_number\n";
		warn "exp_date      : $exp_date\n";
		return ($card_number . "=" . $exp_date);
	}
}


sub cleanSwipeData()
{
	# cleans the card of any junk chars
	
	my $object = shift;
	my $magstrip = shift;
	
	$magstrip =~ s/ //g;
	$magstrip =~ s/-//g;
	$magstrip =~ s/,//g;
	
	return $magstrip;
	
}

END { }

}

1;
