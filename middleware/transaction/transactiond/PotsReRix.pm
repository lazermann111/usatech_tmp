#!/bin/perl -w

package PotsReRix;

use strict;

use Shared;

use Evend::Database::Database;        # Access to the Database

use IO::Socket;
use IO::Select;

sub getPacket ($);

sub Run ($)
{
	my $sock = shift;

	$| = 1;

	print "Connected from " . $sock->peerhost . ":" . $sock->peerport. "\n";

	my $TIMEOUT = 60*3;

	printf("Before database create handle...\n");
	my $DATABASE = Evend::Database::Database->new(
							print_query			=> 1, 
							execute_flatfile	=> 1, 
							debug				=> 1, 
							debug_die			=> 0);
	printf("After database create handle...\n");

	my $machine_id;
	my $network = 'P';
	my $ssn;

	my $rh_set;
	my $wh_set;

	my $rh;
	my $wh;

	my $sequence_in;
	my $sequence_out = 5;
	my $rerix;

	my $read_set = new IO::Select();
	my $write_set = new IO::Select();
	$read_set->add($sock);

	my $next_command_check = time() + 5;
	my $last_command_resend;

	my $outBuffer;
	my $inBuffer;

	my $outCommand;

	my $outCommandID;

	my $watchdog = time;

	while(1)
	{
		my $select_t;

		if( defined $outCommandID )
		{
			$select_t = ($last_command_resend - time()) > 0 ?
								$last_command_resend - time() : 0;
		}
		else
		{
			$select_t = ($next_command_check - time()) > 0 ?
								$next_command_check - time() : 0;
		}

		print "Selecting $select_t\n";

		($rh_set, $wh_set) =
						IO::Select->select($read_set, $write_set,
											undef, $select_t);

		if( $watchdog + $TIMEOUT < time )
		{
			$sock->close();

			print "Timeout " . localtime() . "\n";

			return;
		}

		foreach $rh (@$rh_set)
		{
			my $tempBuffer;

			$watchdog = time;

			$rh->recv($tempBuffer, 512);

			if( length( $tempBuffer ) == 0 )
			{
				$sock->close();

				print "Connection Lost " . localtime() . "\n";

				return;
			}
			else
			{
				$inBuffer .= $tempBuffer;

				my $packet;

				print "inbuffer: " . $inBuffer . "\n";
				print "inbuffer hex: " . unpack('H*', $inBuffer) . "\n";

				($packet, $inBuffer) = getPacket($inBuffer);

				while( defined $packet )
				{
					if( substr($packet, 0, 1) eq "i" )
					{
						(undef, undef, undef, $ssn) =
									unpack('cccH*', substr($packet, 0, -1));

						print "SSN:$ssn\n";

						$ssn = uc(substr($ssn, 0, 16));

						print "uc(SSN):$ssn\n";

						my $array_ref = $DATABASE->select(
								table			=> 'Rerix_modem_to_serial',
								select_columns	=> 'modem_id, machine_id',
								where_columns	=> ['network=?',
													'upper(modem_id) = ?'],
								where_values	=> [$network, $ssn]
							);

						if( defined $array_ref->[0] )
						{
							$machine_id = $array_ref->[0][1];
						}
						else
						{
							print "New machine, fetching new Machine ID\n";

							$array_ref = $DATABASE->select(
									table			=> 'dual',
									select_columns	=> 'rerix_machine_id');

							$machine_id = $array_ref->[0][0];

							$DATABASE->insert(
									table			=> 'Rerix_modem_to_serial',
									insert_columns	=> 'modem_id, machine_id, '.
														'network, ' .
														'MACHINE_INDEX',
									insert_values	=> [$ssn, $machine_id,
														$network, 0]);
						}

						print "Machine ID is $machine_id\n";

						$outBuffer .= "fff";
						$write_set->add( $sock );
					}
					elsif( substr($packet, 0, 1) eq "d" )
					{
						print "Incoming data\n";

						my $sequence_old = $sequence_in;

						(undef, undef, undef, $sequence_in, $rerix) =
									unpack('cccaH*', substr($packet, 0, -1));

						if( defined $machine_id )
						{
							if( (!defined $sequence_old) or
								($sequence_in ne $sequence_old) )
							{
								$DATABASE->insert(
									table			=>'machine_command_inbound',
									insert_columns	=> 'machine_id, ' .
														'inbound_command',
									insert_values	=> [$machine_id,
														$rerix]);
							}
							else
							{
								print "Repeat, dropping\n";
							}

							$outBuffer .= 'b' . uuencode($sequence_in) . "\n";
							$write_set->add( $sock );
						}
						else
						{
							print "ReRix sent without identifying, closing\n";

							return;
						}
					}
					elsif( substr($packet, 0, 1) eq "a" )
					{
						my (undef, $ack) = unpack('cc', $packet);

						print "ACK $ack $sequence_out\n";

						if(($ack == $sequence_out) and (defined $outCommandID))
						{
							$DATABASE->update(
									table			=> 'Machine_Command',
									update_columns	=> 'Execute_Cd',
									update_values	=> ['Y'],
									where_columns	=> ['Command_ID = ?'],
									where_values	=> [$outCommandID]
								);

							undef $outCommandID;
							$next_command_check = time() + 5;
						}
					}
					elsif( $packet eq "xxx" )
					{
						print "Client Requested Disconnect\n";
						return;
					}
					else
					{
						print "Unknown Packet\n";
					}

					($packet, $inBuffer) = getPacket($inBuffer);
				} # while ( defined $packet )
			}
			$next_command_check = 0;
		}

		foreach $wh (@$wh_set)
		{
			my $sent = $wh->send($outBuffer);

			$watchdog = time;

			print "Sent: " . substr($outBuffer, 0, $sent) . "\x0a";
			$outBuffer = substr $outBuffer, $sent;

			# if there's no more data to send,
			# remove it from the select()
			if( length( $outBuffer ) == 0 )
			{
				$write_set->remove( $wh );
			}
		}

		if( (defined $outCommandID) and ($last_command_resend <= time()) )
		{
			print "Resending\n";

			# send the command again, it timed out

			$outBuffer .= $outCommand;

			$write_set->add( $sock );

			$last_command_resend = time() + 5;
		}


		if( ($next_command_check <= time()) and (!defined $outCommandID) and
			(defined $machine_id) )
		{
			print "Checking machine_command\n";
			$next_command_check = time() + 5;

			$DATABASE->{print_query} = 0;
			my $commandref = $DATABASE->select(
									table			=> 'Machine_Command',
									select_columns	=> 'COMMAND_ID, MODEM_ID,'.
														'COMMAND, Execute_Cd',
									order			=> 'Command_ID',
									where_columns	=> ['(Execute_cd = ? or ' .
														'Execute_cd = ?)',
														'modem_id = ?'],
									where_values	=> ['N',
														"H$network",
														$machine_id]
								);
			$DATABASE->{print_query} = 1;

			if( defined $commandref->[0] )
			{
				my ($command_id, $modem_id,
					$command, $execute_cd) = @{$commandref->[0]};

				if( $execute_cd eq 'N' )
				{
					print "Sending $command_id: $command\n";

					$outCommandID = $command_id;

					$sequence_out = 5 if $sequence_out > 100;

					$outCommand = pack 'ca*',	++$sequence_out,
												pack('H*' ,$command);

					$outCommand = pack('n', getcrc16i($outCommand))
															. $outCommand;

					$outCommand = "e" . uuencode($outCommand) . "\x0a";

					$outBuffer .= $outCommand;

					$write_set->add( $sock );

					$last_command_resend = time() + 5;
				}
				elsif( $execute_cd eq "H$network" )
				{
					my $array_ref = $DATABASE->select(
									table			=> 'Rerix_modem_to_serial',
									select_columns	=> 'modem_id, machine_id',
									where_columns	=> ['network=?',
														'modem_id = ?'],
									where_values	=> [$network, $ssn]
								);

					if( defined $array_ref->[0] )
					{
						$machine_id = $array_ref->[0][1];
					}
					else
					{
						print "Confused, no EV# for this connection, closing\n";
						return;
					}
print "HHHHHHHHHHHH Network";
					$DATABASE->update(
									table			=> 'Machine_Command',
									update_columns	=> 'Execute_Cd',
									update_values	=> ['Y'],
									where_columns	=> ['Command_ID = ?'],
									where_values	=> [$command_id]
								);
				}
			}
		}
		elsif( $next_command_check <= time() )
		{
			$next_command_check = time() + 5;
		}
	}
}

sub getPacket ($)
{
	my $inBuffer = shift;

	my $packet;
	my $byte;

	while( length($inBuffer) )
	{
		$byte = substr $inBuffer, 0, 1;
		$inBuffer = substr $inBuffer, 1;

		if( ($byte eq "i") or ($byte eq "a") or ($byte eq "d") )
		{
			$packet = $byte;

			while( length($inBuffer) )
			{
				$byte = substr $inBuffer, 0, 1;
				$inBuffer = substr $inBuffer, 1;

				if( $byte eq "\x0a" )
				{
					$packet .= $byte;

					if( substr($packet, 0, 1) eq "a" )
					{
						return (uudecode($packet), $inBuffer);
					}
					else
					{
						return (check(uudecode($packet)), $inBuffer);
					}
				}
				elsif(	($byte eq "i") or ($byte eq "a") or
						($byte eq "d") or ($byte eq "x") )
				{
					return getPacket($byte . $inBuffer);
				}
				else
				{
					$packet .= $byte;
				}
			}

			# the packet is incomplete
			return (undef, $packet);
		}
		elsif( $byte eq "x" )
		{
			if( substr($inBuffer, 0, 2) eq "xx" )
			{
				return ("xxx", substr($inBuffer, 2) );
			}
			else
			{
				return getPacket($inBuffer);
			}
		}
	}

	return( undef, '');
}

sub check ($)
{
	my $packet = shift;

	return(undef) if !defined $packet;

	my $crc = ord(substr($packet,1,1))<<8 | ord(substr($packet,2,1));

	my $datablock = substr(substr($packet, 3), 0, -1);

	my $calcrc = getcrc16i($datablock);

	printf('Got Checksum: 0x%4.4X Computed: 0x%4.4X' ."\n", $crc, $calcrc);

	if( ($crc == $calcrc) or ($crc == 0) )
	{
		return($packet);
	}
	else
	{
		print "Bad CRC\n";
		return(undef);
	}
}

sub uuencode
{
   my @byte = split('', shift);

   my $len = scalar @byte;

   for(my $i = 0; $i < $len; $i++)
   {
      $byte[$i] = ord($byte[$i]);
   }

   my ($final, $ptr);
   for(my $i = 0; $i < $len; $i += 3)
   {
      $final .= sprintf("%c", 32 + (($byte[$i] >> 2) & 0x3f));
      if( defined $byte[$i+1] )
      {
         $final .= sprintf("%c", 32 + ((($byte[$i]<<4) |
										((($byte[$i+1]>>4)&0xf))) & 0x3f));
         if( defined $byte[$i+2] )
         {
            $final .= sprintf("%c", 32 + (((($byte[$i+1]<<2)) |
											((($byte[$i+2]>>6)&0x3))) &0x3f));
            $final .= sprintf("%c", 32 + (($byte[$i+2]) & 0x3f));
         }
         else
         {
            $final .= sprintf("%c", 32 + (($byte[$i+1]<<2) & 0x3f));
         }
      }
      else
      {
         $final .= sprintf("%c", 32 + (($byte[$i]<<4) & 0x3f));
      }
   }

   return $final;
}

sub uudecode
{
   my @byte = split //, $_[0];

   my $header = shift @byte;
   my $footer = pop @byte;

   my $val;
   my $final;
   my $mod;

   for(my $i = 0; $i < @byte; $i++)
   {
      $mod = $i % 4;
      my $info = ord($byte[$i]) - 32;

      if ($mod == 0) {
         $val = ($info & 0x3f) << 2;
      }
      elsif ($mod == 1) {
         $val |= ($info >> 4) & 0x03;
         $final .= sprintf("%c", $val);
         $val  = ($info & 0x0f) << 4;
      }
      elsif ($mod == 2) {
         $val |= ($info >> 2) & 0x0f;
         $final .= sprintf("%c", $val);
         $val  = ($info & 0x03) << 6;
      }
      elsif ($mod == 3) {
         $val |= ($info & 0x3f);
         $final .= sprintf("%c", $val);
      }
   }

   return $header . $final . $footer;
}


1;
