package Txndatabase;
require 5.004;
#-----------------------------------------------------------------
# Rev Level	Date	Programmer	  Description
# ---------	----	----------	  --------------------------
#  1.0		5/03	T Shannon	  Added FHMS processing
#  1.1		11/03	T Shannon	  Added timezone and G4 handling
#  1.2		07/05	D Kouznetsov  Added writing to PSS schema
#  1.3      10/05   D Kouznetsov  Changes for authority integration
#

use strict;
use Evend::Database::Database;
use CardUtils;
use Utils;
use DBI qw(:sql_types);



BEGIN {
	use Exporter    ();
	use vars        qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);


	$VERSION        = '1.1';

	@ISA            = qw(Exporter);

	#export by default
	# i always want people to refer to it as $transaction-> and not funciton()
	# so im not going to export any functions, function() is bad
	@EXPORT = qw();

	#export on demand
	@EXPORT_OK   = qw();

	#global/constants	
	use vars        qw();
}

sub logit($);

sub new
{
   my ($class, %arg) = @_;


   my $this = {
               processor_obj => $arg{processor_obj}
              }; # 'this' is used to init the module handle


   bless $this,$class;
}



sub close 
{
   my ($self) = @_;

   undef $self;
}

sub createOrderNbr
{
	my ($self, $ORACLE) = @_;
	my $transaction_num = $ORACLE->select(  table => 'Dual',
	                                       select_columns => 'TRANS_NO_SEQ.NEXTVAL' );
	logit "Txndatabase.createOrderNbr() - Creating new order number : $transaction_num->[0][0]";	                                       
	return $transaction_num->[0][0];   
}

sub authorize 
{
   # my ($self,$magstrip,$auth_amt) = @_;
   # in the format of a t1 command
   my ($self, $ORACLE, $machine, $pin, $auth_amt, $auth_question_data, $auth_info, $card_type, $machine_trans_no) = @_;
   warn "authorize() \n";
   my $cardreader = CardUtils->new();
   my ($card_num, $customer_name, $exp_date) = $cardreader->parse($self->{processor_obj}{magstrip});


   my $machine_rec = $ORACLE->select( table     => 'rix_Machine',
                                       select_columns => 'Promotion_Template,Location_Number',
                                        where_columns => ['Machine_ID=?'],
                                         where_values => [$machine] );

	my ($promo_num, $location_num) = ('', '');

	if( defined $machine_rec->[0] )
	{
		$promo_num = $machine_rec->[0][0];
		$location_num = $machine_rec->[0][1];
	}

   my $transaction_num = $ORACLE->select(  table => 'Dual',
                                           select_columns => 'TRANS_NO_SEQ.NEXTVAL' );

   $self->{TRANSACTION_NUMBER} = $transaction_num->[0][0];
   $self->{CARD_TYPE} = $cardreader->typeAbbrev($card_num);

   $ORACLE->insert( table          => 'Transaction_Record',
                    insert_columns => 'TRANS_NO,
                                      CLIENT_NO,
                                      MERCHANT_NO,
                                      AUTH_PCLIENT_ID,
                                      AUTH_RCLIENT_ID,
                                      AUTH_TERMINAL_NO,
                                      AUTH_SEQUENCE_NO,
                                      MAG_STRIPE,
                                      CUSTOMER_NAME,
                                      CARD_NUMBER,
                                      EXPIRATION_DATE,
                                      AUTH_AMT,
                                      AUTH_ACTION_CD,
                                      AUTH_CD,
                                      AUTH_1_DATA,
                                      BATCH_NO,
                                      RETRIEVAL_REF_NO,
                                      RESPONSE_MSG,
                                      CARD_TYPE,
                                      AUTH_NETWORK_ID,
                                      AUTH_SOURCE,
                                      MACHINE_ID,
                                      LOCATION_NUMBER,
                                      TEMPLATE_NUMBER,
                                      TRANSACTION_CODE,
                                      MACHINE_TRANS_NO',
                   insert_values =>   [$self->{TRANSACTION_NUMBER},
                                      $Paymentech::CLIENT_NUMBER,
                                      $self->{processor_obj}{MERCHANT_NUMBER},
                                      $self->{processor_obj}{PAY_CLIENT_PID},
                                      $self->{processor_obj}{RIX_CLIENT_PID},
                                      $self->{processor_obj}{TERMINAL_NUMBER},
                                      $self->{processor_obj}{SEQUENCE_NUMBER},
                                      $self->{processor_obj}{magstrip}, 
                                      $customer_name,
                                      $card_num, 
                                      $exp_date, 
                                      $auth_amt, 
                                      $self->{processor_obj}{ACTION_CODE},
                                      $self->{processor_obj}{AUTH_CODE},
                                      $auth_info,
                                      $self->{processor_obj}{BATCH_NUMBER},
                                      $self->{processor_obj}{RETRIEVAL_REF_NUMBER},
                                      substr($self->{processor_obj}{RESPONSE_MESSAGE},0,32),
                                      $self->{processor_obj}{CARD_TYPE},
                                      $self->{processor_obj}{AUTH_NET_ID},
                                      $self->{processor_obj}{AUTH_SOURCE},
                                      $machine,
                                      $location_num,
                                      $promo_num,
                                      $self->{processor_obj}{TRANSACTION_TYPE},
										(defined $machine_trans_no ?
											$machine_trans_no : '') ]
									);

   return;

}

sub authorize_PSS
{
   # my ($self,$magstrip,$auth_amt) = @_;
   # in the format of a t1 command
   my ($self, $ORACLE, $machine, $pin, $auth_amt, $auth_question_data, $auth_info, $card_type, $machine_trans_no, $order_nbr, $txn_date, $track_data) = @_;
   warn "authorize_PSS() payload = $machine, $pin, $auth_amt, $auth_question_data, $auth_info, $card_type, $machine_trans_no, $order_nbr, $txn_date, $track_data\n";

   my $tran_card_type = $card_type;
   my $cardreader = CardUtils->new();

   my ($card_num, $customer_name, $exp_date) = $cardreader->parse($auth_info);

   my $machine_rec = $ORACLE->select( table     => 'rix_Machine',
                                       select_columns => 'Promotion_Template,Location_Number',
                                        where_columns => ['Machine_ID=?'],
                                         where_values => [$machine] );

	my ($promo_num, $location_num) = ('', '');

	if( defined $machine_rec->[0] )
	{
		$promo_num = $machine_rec->[0][0];
		$location_num = $machine_rec->[0][1];
	}

	
# -- code changes for PSS ---
#   my $transaction_num = $ORACLE->select(  table => 'Dual',
#                                           select_columns => 'TRANS_NO_SEQ.NEXTVAL' );

#   $self->{TRANSACTION_NUMBER} = $transaction_num->[0][0];
#-------------------------------------------------------------------------------------------
	$self->{TRANSACTION_NUMBER} = $order_nbr;
	# the following is a sucky kluge. probably won't be the last.	T Shannon 5/22/2003
	if ($card_type ne 'S')
	{   
		$self->{CARD_TYPE} = $cardreader->typeAbbrev($card_num);
	}
	else
	{
		$self->{CARD_TYPE} = 'SP'
	}

	# test to see if a txn_date was passed - if not create one using EST
	warn "authorize_PSS() transaction date (asc): $txn_date\n";
	if ((!defined $txn_date) or ($txn_date == 0) or ($txn_date eq ' '))
	{
		$txn_date = ReRix::Utils::getAdjustedTimestamp('EST');		
	   	warn "Txndatabase::authorize_PSS() transaction date created : $txn_date\n";
	}
	my ($sec, $min, $hr, $dy, $mon, $yr, $wday, $yday, $isdst) = gmtime($txn_date);
	$txn_date = sprintf("%02d/%02d/%04d %02d:%02d:%02d", $mon + 1, $dy, $yr + 1900, $hr, $min, $sec );

	warn "authorize_PSS() transaction date prepared for DB update: $txn_date\n";

	warn "authorize_PSS() self->{TRANSACTION_NUMBER} : $self->{TRANSACTION_NUMBER}\n";

  $ORACLE->insert( table          => 'Transaction_Record',
                    insert_columns => 'TRANS_NO,
                                      CLIENT_NO,
                                      MERCHANT_NO,
                                      AUTH_PCLIENT_ID,
                                      AUTH_RCLIENT_ID,
                                      AUTH_TERMINAL_NO,
                                      AUTH_SEQUENCE_NO,
                                      MAG_STRIPE,
                                      CUSTOMER_NAME,
                                      CARD_NUMBER,
                                      EXPIRATION_DATE,
                                      AUTH_AMT,
                                      AUTH_ACTION_CD,
                                      AUTH_CD,
                                      AUTH_1_DATA,
                                      BATCH_NO,
                                      RETRIEVAL_REF_NO,
                                      RESPONSE_MSG,
                                      CARD_TYPE,
                                      AUTH_NETWORK_ID,
                                      AUTH_SOURCE,
                                      MACHINE_ID,
                                      LOCATION_NUMBER,
                                      TEMPLATE_NUMBER,
                                      TRANSACTION_CODE,
                                      MACHINE_TRANS_NO,
                                      TRANSACTION_DATE',
                   insert_values =>   [$self->{TRANSACTION_NUMBER},
                                      $Paymentech::CLIENT_NUMBER,
                                      $self->{processor_obj}{MERCHANT_NUMBER},
                                      $self->{processor_obj}{PAY_CLIENT_PID},
                                      $self->{processor_obj}{RIX_CLIENT_PID},
                                      $self->{processor_obj}{TERMINAL_NUMBER},
                                      $self->{processor_obj}{SEQUENCE_NUMBER},
                                      $auth_info,
                                      $customer_name,
                                      $card_num, 
                                      $exp_date, 
                                      $auth_amt, 
                                      $self->{processor_obj}{ACTION_CODE},
                                      $self->{processor_obj}{AUTH_CODE},
                                      $auth_info,
                                      $self->{processor_obj}{BATCH_NUMBER},
                                      $self->{processor_obj}{RETRIEVAL_REF_NUMBER},
                                      substr($self->{processor_obj}{RESPONSE_MESSAGE},0,32),
                                      (defined $self->{CARD_TYPE} ?
							$self->{CARD_TYPE} : ''),
                                      $self->{processor_obj}{AUTH_NET_ID},
                                      $self->{processor_obj}{AUTH_SOURCE},
                                      $machine,
                                      $location_num,
                                      $promo_num,
                                      $self->{processor_obj}{TRANSACTION_TYPE},
				      (defined $machine_trans_no ?
						$machine_trans_no : ''),
				      "to_date(?, 'MM/DD/YYYY HH24:MI:SS')"],
		      noquote_columns =>[26],
		      noquote_values  =>["$txn_date"] );
		     
	# update PSS schema
	$self->{PSS_TRAN_ID} = 0;
	
	# get pos_pta_id
	my $dbh_debug_die = $ORACLE->{debug_die};
	$ORACLE->{debug_die} = 0;
		
	my $card_type_validated = 'N';
	my ($pos_pta_id, $regex);
	$tran_card_type = uc(defined $tran_card_type ? $tran_card_type : 'S');
	if (!($tran_card_type =~ m/^(C|P|S|R)$/))
	{
		$tran_card_type = 'S';
	}
		
	### Verify card type can be used at specified device (POS) and validate magstripe and/or resolve ambiguous payment type if multiple exist for card type ###
	my $c_get_pos_payment_type_auth = $ORACLE->query(
		query	=> q{
			SELECT 	pp.pos_pta_id, 
				pp.pos_pta_regex,
				apmb.authority_payment_mask_regex, 
				apma.authority_payment_mask_regex
			FROM pos p, pos_pta pp, payment_subtype ps, client_payment_type cpt, authority_payment_mask apma, authority_payment_mask apmb
			WHERE p.pos_id = pp.pos_id
			AND ps.payment_subtype_id = pp.payment_subtype_id
			AND cpt.client_payment_type_cd = ps.client_payment_type_cd
			AND ps.authority_payment_mask_id = apma.authority_payment_mask_id
			AND pp.authority_payment_mask_id = apmb.authority_payment_mask_id(+)
			AND p.device_id = (SELECT device_id FROM device.device WHERE device_name = ? AND device_active_yn_flag = 'Y')
			AND cpt.client_payment_type_cd = ?
			AND pp.pos_pta_activation_ts IS NULL 
			ORDER BY NVL(pp.pos_pta_priority, 1000)
		}
		,values => [
			$machine
			,$tran_card_type
		]
	);
	
	if (!defined $c_get_pos_payment_type_auth->[0]->[0])
	{
		logit "No pos_pta record exists for device $machine, card type $tran_card_type and transaction timestamp $txn_date. Unable to insert transaction into PSS, machine_trans_no: $machine_trans_no.";
	}
	else
	{
		my @payment_type_auth_choices;
		foreach my $row_aref (@{$c_get_pos_payment_type_auth})
		{
			REGEX_PRECEDENCE_LOOP: for (my $i = 1; $i < 4; $i++)
			{
				if ((@{$row_aref})[$i]) 
				{
					push @payment_type_auth_choices, [(@{$row_aref})[0,$i]];
					last REGEX_PRECEDENCE_LOOP;
				}
			}
		}
		
		foreach my $row_aref (@payment_type_auth_choices)
		{
			($pos_pta_id, $regex) = @{$row_aref};

			### determine if card matches this regex
			if (defined $regex && $regex ne '' && $track_data =~ m/$regex/i) 
			{
				$card_type_validated = 'Y';
				last;
			}
		}

		if ($card_type_validated eq 'N')
		{
			logit "Card $track_data cannot be validated using pos_pta settings for device $machine, card type $tran_card_type. Unable to insert transaction into PSS, machine_trans_no: $machine_trans_no.";
		}
		else
		{				
			my $db_tran_id = $ORACLE->select(  table => 'DUAL',
								       select_columns => "PSS.SEQ_TRAN_ID.NEXTVAL, TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS')");

			if( defined $db_tran_id->[0] )
			{		
				my $tran_id = $db_tran_id->[0][0];
				$self->{PSS_TRAN_ID} = $tran_id;
				my $current_dt = $db_tran_id->[0][1];
				
				my ($tran_state_cd, $auth_state_id, $auth_amt_approved, $auth_result_cd);
							
				if ($self->{processor_obj}{ACTION_CODE} eq 'A')
				{
					$tran_state_cd 		= '6'; #PROCESSED_SERVER_AUTH_SUCCESS
					$auth_state_id 		= '2'; #AUTH_STATE_SUCCESS
					$auth_amt_approved 	= $auth_amt;
					$auth_result_cd 	= 'Y';
				}
				else
				{
					$tran_state_cd 		= '5'; #PROCESSED_SERVER_AUTH_FAILURE
					$auth_state_id 		= '4'; #AUTH_STATE_FAILURE
					$auth_amt_approved 	= '';
					$auth_result_cd 	= 'N';
				}

				my $device_tran_cd;
				if (defined $machine_trans_no)
				{
					(undef, undef, $device_tran_cd) = split /:/, $machine_trans_no;
				}
				else
				{
					$device_tran_cd = '';
				}
				
				my $rpt_acct_num = $card_num;
				if (length($rpt_acct_num) > 4)
				{
					my $rpt_acct_num1 = substr($rpt_acct_num, 0, -4);
					my $rpt_acct_num2 = substr($rpt_acct_num, -4, 4);					
					$rpt_acct_num1 =~ s/./*/g;
					$rpt_acct_num = $rpt_acct_num1 . $rpt_acct_num2;
				}
				
				logit "Inserting transaction tran_id $tran_id into pss.tran";

				# insert transaction into pss.tran
				$ORACLE->insert( table => 'PSS.TRAN',
					insert_columns => 
								'tran_id,
								tran_start_ts,
								tran_end_ts,
								tran_state_cd,
								tran_device_tran_cd,
								tran_received_raw_acct_data,
								tran_parsed_acct_name,
								tran_parsed_acct_exp_date,
								tran_parsed_acct_num,
								tran_reportable_acct_num,
								pos_pta_id,
								tran_legacy_trans_no,
								tran_global_trans_cd',
					insert_values => 
					[
								$tran_id,
								"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
								"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
								$tran_state_cd,
								$device_tran_cd,
								substr($track_data, 0, 107),
								$customer_name,
								$exp_date,
								$card_num,
								$rpt_acct_num,
								$pos_pta_id,
								$self->{TRANSACTION_NUMBER},
								(defined $machine_trans_no ? $machine_trans_no : '')
					],
					noquote_columns =>[1, 2],
					noquote_values  =>[$txn_date, $txn_date] );
					      
				my $db_auth_id = $ORACLE->select(  table => 'DUAL',
								       select_columns => "PSS.SEQ_AUTH_ID.NEXTVAL");

				if( defined $db_auth_id->[0] )
				{		
					my $auth_id = $db_auth_id->[0][0];	
					
					logit "Inserting live auth auth_id $auth_id into pss.auth";
				
					# insert transaction into pss.auth
					$ORACLE->insert( table => 'PSS.AUTH',
						insert_columns => 	
									'auth_id,
									tran_id,
									auth_type_cd,
									auth_state_id,
									auth_parsed_acct_data,
									acct_entry_method_cd,
									auth_amt,
									auth_amt_approved,
									auth_ts,
									auth_result_cd,
									auth_resp_cd,
									auth_resp_desc',
						insert_values =>    
						[
									$auth_id,
									$tran_id,
									'N', #network live auth
									$auth_state_id,
									substr($self->{processor_obj}{magstrip}, 0, 107),
									'3', #magnetic stripe
									$auth_amt,
									$auth_amt_approved,									
									"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
									$auth_result_cd,
									substr($self->{processor_obj}{RETRIEVAL_REF_NUMBER},0,60),
									substr($self->{processor_obj}{RESPONSE_MESSAGE},0,255)
						],
						noquote_columns =>[8],
					    noquote_values  =>[$current_dt] );
				}
			}
		}
	}
		      
	$ORACLE->{debug_die} = $dbh_debug_die;

   	return;

}


sub batch 
{
   # in the format of a t2 command
   my ($self, $ORACLE, $machine, $txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status) = @_;
   my $cancel_code;
   if ($self->{CANCEL}) { $cancel_code = 'C'; }
   elsif ($self->{VENDTOOLONG}) { $cancel_code = 'T'; }
   elsif ($self->{DENIED}) { $cancel_code = 'D'; }

    warn "*********************** batch CANCEL_CD = $cancel_code";
   $ORACLE->update(  table => 'Transaction_Record',
                     update_columns => 'BATCH_NO,
                                       COLUMN_ID,
                                       INVENTORY_NUMBER,
                                       TRANSACTION_AMOUNT,
                                       TAX_AMOUNT,
                                       QUESTION_1_DATA,
                                       QUESTION_2_DATA,
                                       BUTTON_ID,
                                       TRANSACTION_STATUS,
                                       QUANTITY,
                                       INVOICE_NUMBER,
                                       FORCE_PCLIENT_ID,
                                       FORCE_RCLIENT_ID,
                                       FORCE_TERMINAL_NO,
                                       FORCE_SEQUENCE_NO,
                                       FORCE_ACTION_CD,
                                       FORCE_CD,
                                       FORCE_RET_REF_NO,
                                       FORCE_RESPONSE_MSG,
                                       TRANSACTION_CODE,
                                       CHARGE_OFF,
                                       CANCEL_CD',
                     update_values =>  [$self->{processor_obj}{BATCH_NUMBER},
                                       $col_num,
                                       $inv_num,
                                       $txn_amt,
                                       $tax_amt,
                                       $q1_data,
                                       $q2_data,
                                       $button_num,
                                       $txn_status,
                                       1,
                                       $txn_num,
                                       $self->{processor_obj}{PAY_CLIENT_PID},
                                       $self->{processor_obj}{RIX_CLIENT_PID},
                                       $self->{processor_obj}{TERMINAL_NUMBER},
                                       $self->{processor_obj}{SEQUENCE_NUMBER},
                                       $self->{processor_obj}{ACTION_CODE},
                                       $self->{processor_obj}{AUTH_CODE},
                                       $self->{processor_obj}{RETRIEVAL_REF_NUMBER},
                                       substr($self->{processor_obj}{RESPONSE_MESSAGE},0,32),
                                       $self->{processor_obj}{TRANSACTION_TYPE}, # FORCE or RETAIL SALE
                                       $self->{processor_obj}{CHARGE_OFF},
                                       $cancel_code],
                     where_columns =>  ['TRANS_NO = ?'],
                     where_values =>   [$txn_num] );



   return;

}


sub batch_v2
{	
	# in the format of a t2 command
	logit "Entering Txndatabase.batch_v2()";
	my ($self, $ORACLE, $machine, $txn_num, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, %vended_items) = @_;
	logit "self = $self";
	logit "ORACLE = $ORACLE";
	logit "machine = $machine";
	logit "txn_num = $txn_num";
	logit "txn_amt = $txn_amt";
	logit "tax_amt = $tax_amt";
	logit "txn_status = $txn_status";
	logit "machine_trans_no = $machine_trans_no";

	my $key;
	foreach $key (%vended_items)
	{
		if ( defined($vended_items{$key}{QTY}) )
		{
			logit "\t\tColumn : $key";
			logit "\t\tAMT    : " . sprintf("%.02f", .01 * unpack("V", pack("V", hex(unpack("h*", pack("xh*", $vended_items{$key}{AMT}))))));
			logit "\t\tQTY    : $vended_items{$key}{QTY}\n";
		}
	}	
	
	my $cancel_code;
	if ($self->{CANCEL}) { $cancel_code = 'C'; }
	elsif ($self->{VENDTOOLONG}) { $cancel_code = 'T'; }
	elsif ($self->{DENIED}) { $cancel_code = 'D'; }
	
    logit "*********************** batch CANCEL_CD = $cancel_code";
   $ORACLE->update(  table => 'Transaction_Record',
                     update_columns => 'BATCH_NO,
                                       TRANSACTION_AMOUNT,
                                       TAX_AMOUNT,
                                       TRANSACTION_STATUS,
                                       INVOICE_NUMBER,
                                       FORCE_PCLIENT_ID,
                                       FORCE_RCLIENT_ID,
                                       FORCE_TERMINAL_NO,
                                       FORCE_SEQUENCE_NO,
                                       FORCE_ACTION_CD,
                                       FORCE_CD,
                                       FORCE_RET_REF_NO,
                                       FORCE_RESPONSE_MSG,
                                       TRANSACTION_CODE,
                                       CHARGE_OFF,
                                       CANCEL_CD',
                     update_values =>  [$self->{processor_obj}{BATCH_NUMBER},
                                       $txn_amt,
                                       $tax_amt,
                                       $txn_status,
                                       $txn_num,
                                       $self->{processor_obj}{PAY_CLIENT_PID},
                                       $self->{processor_obj}{RIX_CLIENT_PID},
                                       $self->{processor_obj}{TERMINAL_NUMBER},
                                       $self->{processor_obj}{SEQUENCE_NUMBER},
                                       $self->{processor_obj}{ACTION_CODE},
                                       $self->{processor_obj}{AUTH_CODE},
                                       $self->{processor_obj}{RETRIEVAL_REF_NUMBER},
                                       substr($self->{processor_obj}{RESPONSE_MESSAGE},0,32),
                                       $self->{processor_obj}{TRANSACTION_TYPE}, # FORCE or RETAIL SALE
                                       $self->{processor_obj}{CHARGE_OFF},
                                       $cancel_code],
                     where_columns =>  ['TRANS_NO = ?'],
                     where_values =>   [$txn_num] );
                     
     
    # update PSS schema
    $self->{PSS_TRAN_ID} = 0;
    
	my $dbh_debug_die = $ORACLE->{debug_die};
	$ORACLE->{debug_die} = 0;
    my $db_tran = $ORACLE->select(table => 'PSS.TRAN',
		select_columns => "TRAN_ID, TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS')",
		where_columns =>  ['TRAN_LEGACY_TRANS_NO = ?'],
		where_values =>   [$txn_num] );

    if( defined $db_tran->[0] )
    {
        my $tran_id = $db_tran->[0][0];
        $self->{PSS_TRAN_ID} = $tran_id;
        my $current_dt = $db_tran->[0][1];
		
		my $db_auth = $ORACLE->select(table => 'PSS.AUTH',
			select_columns => "AUTH_ID, AUTH_PARSED_ACCT_DATA",
			order => 'AUTH_ID DESC',
			where_columns =>  ['TRAN_ID = ?', 'AUTH_TYPE_CD = ?'],
			where_values =>   [$tran_id, 'N'] );
			
		if (defined $db_auth->[0])
		{
			my $auth_id = $db_auth->[0][0];				
			my $auth_parsed_acct_data = $db_auth->[0][1];
			
			my $settlement_batch_state_id 	= '3'; #SERVER_SETTLEMENT_FAILURE;
			my $tran_state_cd 				= 'E'; #TRANSACTION_COMPLETE_ERROR
			my $auth_state_id 				= '4'; #AUTH_STATE_FAILURE
			my $auth_amt_approved 			= '';
			my $auth_result_cd 				= 'N';

			if ($txn_amt > 0 && $self->{processor_obj}{ACTION_CODE} eq 'A')
			{
				$settlement_batch_state_id 	= '1'; #SERVER_SETTLEMENT_SUCCESS;
				$tran_state_cd 				= 'D'; #TRANSACTION_COMPLETE
				$auth_state_id 				= '2'; #AUTH_STATE_SUCCESS
				$auth_amt_approved 			= $txn_amt;
				$auth_result_cd 			= 'Y';
			}
			elsif ($txn_amt == 0)
			{
				$settlement_batch_state_id 	= '5'; #SERVER_SETTLEMENT_CANCELLED;	
			}

			$txn_status = uc(defined $txn_status ? $txn_status : 'I');
			if (!($txn_status =~ m/^(C|F|I|N|Q|R|S|T|U)$/))
			{
				$txn_status = 'I'; # incomplete
			}
        
			$ORACLE->update( table => 'PSS.TRAN',
				 update_columns => 'TRAN_UPLOAD_TS,
				                   TRAN_STATE_CD,
								   TRAN_DEVICE_RESULT_TYPE_CD',
				 update_values =>  
				 [
				 				   "to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
								   $tran_state_cd,
								   $txn_status
				 ],
				 noquote_columns =>[0],
			     noquote_values  =>[$current_dt],
				 where_columns =>  ['TRAN_ID = ?'],
				 where_values =>   [$tran_id] );
				 
			my $db_post_auth_id = $ORACLE->select(  table => 'DUAL',
											   select_columns => "PSS.SEQ_AUTH_ID.NEXTVAL");

			if( defined $db_post_auth_id->[0] )
			{		
				my $post_auth_id = $db_post_auth_id->[0][0];	

				logit "Inserting post-auth sale auth_id $post_auth_id into pss.auth";

				# insert transaction into pss.auth
				$ORACLE->insert( table => 'PSS.AUTH',
					insert_columns => 	
								'auth_id,
								tran_id,
								auth_type_cd,
								auth_state_id,
								auth_parsed_acct_data,
								acct_entry_method_cd,
								auth_amt,
								auth_amt_approved,
								auth_ts,
								auth_result_cd,
								auth_resp_cd,
								auth_resp_desc',
					insert_values =>    
					[
								$post_auth_id,
								$tran_id,
								'U', #post-auth sale
								$auth_state_id,
								$auth_parsed_acct_data,
								'3', #magnetic stripe
								$txn_amt,
								$auth_amt_approved,									
								"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
								$auth_result_cd,
								substr($self->{processor_obj}{RETRIEVAL_REF_NUMBER},0,60),
								substr($self->{processor_obj}{RESPONSE_MESSAGE},0,255)
					],
					noquote_columns =>[8],
					noquote_values  =>[$current_dt] );

				my $db_batch_id = $ORACLE->select(  table => 'DUAL',
												   select_columns => 'PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL' );

				if( defined $db_batch_id->[0] )
				{
					my $settlement_batch_id = $db_batch_id->[0][0];   

					$ORACLE->insert( table => 'PSS.SETTLEMENT_BATCH',
					insert_columns => 	
									 'settlement_batch_id,
									 settlement_batch_start_ts,
									 settlement_batch_end_ts,
									 settlement_batch_state_id,
									 settlement_batch_resp_cd,
									 settlement_batch_resp_desc',
					insert_values =>    
					[
									 $settlement_batch_id,
									 "to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
									 "to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
									 $settlement_batch_state_id,
									 substr($self->{processor_obj}{ACTION_CODE},0,60),
									 substr($self->{processor_obj}{RESPONSE_MESSAGE},0,60)
					],
					noquote_columns =>[1, 2],
					noquote_values  =>[$current_dt, $current_dt]);

					$ORACLE->insert( table => 'PSS.TRAN_SETTLEMENT_BATCH',
					insert_columns => 	
									'auth_id,
									settlement_batch_id,
									tran_settlement_b_resp_cd,
									tran_settlement_b_resp_desc,
									tran_settlement_b_amt',
					insert_values =>    
					[
									$post_auth_id,
									$settlement_batch_id,
									substr($self->{processor_obj}{ACTION_CODE},0,60),
									substr($self->{processor_obj}{RESPONSE_MESSAGE},0,255),
									$txn_amt			
					] );

				}
			}
		}
    }
	$ORACLE->{debug_die} = $dbh_debug_die;

	logit "batch_v2() - vended items persistence not yet implemented.";

	return;

}


sub local_batch 
{
   # in the format of a t4 command
   logit "Entering Txndatabase.local_batch()";
   my ($self, $ORACLE, $machine, $txn_time, $txn_date, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status, $machine_trans_no) = @_;

   my $cancel_code;
   if ($self->{CANCEL}) { $cancel_code = 'C'; }
   elsif ($self->{VENDTOOLONG}) { $cancel_code = 'T'; }
   elsif ($self->{DENIED}) { $cancel_code = 'D'; }

   warn "******************** local_batch cancel_code = $cancel_code";

   my $cardreader = CardUtils->new();
   my ($card_num, $customer_name, $exp_date) = $cardreader->parse($self->{processor_obj}{magstrip});


   my $machine_rec = $ORACLE->select( table     => 'rix_Machine',
                                       select_columns => 'Promotion_Template,
                                                          Location_Number',                                                  
                                        where_columns => ['Machine_ID=?'],
                                         where_values => [$machine] );

	my ($promo_num, $location_num) = ('', '');

	if( defined $machine_rec->[0] )
	{
		$promo_num = $machine_rec->[0][0];
		$location_num = $machine_rec->[0][1];
	}

   my $transaction_num = $ORACLE->select(  table => 'Dual',
                                           select_columns => 'TRANS_NO_SEQ.NEXTVAL' );

   $self->{TRANSACTION_NUMBER} = $transaction_num->[0][0];
   $self->{CARD_TYPE} = $cardreader->typeAbbrev($card_num);
   
   $ORACLE->insert( table => 'Transaction_Record',
                    insert_columns =>  'TRANS_NO,
                                       TRANSACTION_DATE,
                                       CLIENT_NO,
                                       MERCHANT_NO,
                                       MAG_STRIPE,
                                       CUSTOMER_NAME,
                                       CARD_NUMBER,
                                       EXPIRATION_DATE,
                                       INVOICE_NUMBER,
                                       COLUMN_ID,
                                       INVENTORY_NUMBER,
                                       TRANSACTION_AMOUNT,
                                       TAX_AMOUNT,
                                       QUESTION_1_DATA,
                                       QUESTION_2_DATA,
                                       BUTTON_ID,
                                       QUANTITY,
                                       FORCE_PCLIENT_ID,
                                       FORCE_RCLIENT_ID,
                                       FORCE_TERMINAL_NO,
                                       FORCE_SEQUENCE_NO,
                                       FORCE_ACTION_CD,
                                       FORCE_CD,
                                       BATCH_NO,
                                       FORCE_RET_REF_NO,
                                       FORCE_RESPONSE_MSG,
                                       CARD_TYPE,
                                       AUTH_NETWORK_ID,
                                       AUTH_SOURCE,
                                       MACHINE_ID,
                                       LOCATION_NUMBER,
                                       TEMPLATE_NUMBER,
                                       TRANSACTION_CODE,
                                       CHARGE_OFF,
                                       CANCEL_CD,
                                       TRANSACTION_STATUS,
                                       MACHINE_TRANS_NO',
                     insert_values =>  [$self->{TRANSACTION_NUMBER},
                                       "to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
                                       $Paymentech::CLIENT_NUMBER,
                                       $self->{processor_obj}{MERCHANT_NUMBER},
                                       $self->{processor_obj}{magstrip}, 
                                       $customer_name,
                                       $card_num, 
                                       $exp_date, 
                                       $auth_num, 
                                       $col_num, 
                                       $inv_num, 
                                       $txn_amt,
                                       $tax_amt,
                                       $q1_data, 
                                       $q2_data, 
                                       $button_num,
                                       1,
                                       $self->{processor_obj}{PAY_CLIENT_PID},
                                       $self->{processor_obj}{RIX_CLIENT_PID},
                                       $self->{processor_obj}{TERMINAL_NUMBER},
                                       $self->{processor_obj}{SEQUENCE_NUMBER},
                                       $self->{processor_obj}{ACTION_CODE},
                                       $self->{processor_obj}{AUTH_CODE},
                                       $self->{processor_obj}{BATCH_NUMBER},
                                       $self->{processor_obj}{RETRIEVAL_REF_NUMBER},
                                       substr($self->{processor_obj}{RESPONSE_MESSAGE},0,32),
                                       $self->{processor_obj}{CARD_TYPE},
                                       $self->{processor_obj}{AUTH_NET_ID},
                                       $self->{processor_obj}{AUTH_SOURCE},
                                       $machine,
                                       $location_num,
                                       $promo_num,
                                       $self->{processor_obj}{TRANSACTION_TYPE},
                                       $self->{processor_obj}{CHARGE_OFF},
                                       $cancel_code,
					(defined $txn_status ?
								$txn_status : ''),
					(defined $machine_trans_no ?
								$machine_trans_no : '')],
                     noquote_columns =>[1],
                     noquote_values  =>["$txn_date $txn_time"] );


   return;

}


sub local_batch_PSS
{
   # in the format of a t4 command
   logit "Entering Txndatabase.local_batch_PSS()";
   my ($self, $ORACLE, $machine, $txn_time, $txn_date, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status, $machine_trans_no, $order_nbr) = @_;
   warn "local_batch_PSS() payload : @_\n";

   my $cancel_code;
   if ($self->{CANCEL}) { $cancel_code = 'C'; }
   elsif ($self->{VENDTOOLONG}) { $cancel_code = 'T'; }
   elsif ($self->{DENIED}) { $cancel_code = 'D'; }

   warn "******************** local_batch_PSS cancel_code = $cancel_code";

   my $cardreader = CardUtils->new();
   my ($card_num, $customer_name, $exp_date) = $cardreader->parse($self->{processor_obj}{magstrip});


   my $machine_rec = $ORACLE->select( table     => 'rix_Machine',
                                       select_columns => 'Promotion_Template,
                                                          Location_Number',                                                  
                                        where_columns => ['Machine_ID=?'],
                                         where_values => [$machine] );

	my ($promo_num, $location_num) = ('', '');

	if( defined $machine_rec->[0] )
	{
		$promo_num = $machine_rec->[0][0];
		$location_num = $machine_rec->[0][1];
	}

	$self->{TRANSACTION_NUMBER} = $order_nbr;
	$self->{CARD_TYPE} = $cardreader->typeAbbrev($card_num);


	warn "local_batch_PSS() - order_nbr : $order_nbr\n";
	warn "local_batch_PSS() - self->{TRANSACTION_NUMBER}: $self->{TRANSACTION_NUMBER}\n";
	
   $ORACLE->insert( table => 'Transaction_Record',
                    insert_columns =>  'TRANS_NO,
                                       TRANSACTION_DATE,
                                       CLIENT_NO,
                                       MERCHANT_NO,
                                       MAG_STRIPE,
                                       CUSTOMER_NAME,
                                       CARD_NUMBER,
                                       EXPIRATION_DATE,
                                       INVOICE_NUMBER,
                                       COLUMN_ID,
                                       INVENTORY_NUMBER,
                                       TRANSACTION_AMOUNT,
                                       TAX_AMOUNT,
                                       QUESTION_1_DATA,
                                       QUESTION_2_DATA,
                                       BUTTON_ID,
                                       QUANTITY,
                                       FORCE_PCLIENT_ID,
                                       FORCE_RCLIENT_ID,
                                       FORCE_TERMINAL_NO,
                                       FORCE_SEQUENCE_NO,
                                       FORCE_ACTION_CD,
                                       FORCE_CD,
                                       BATCH_NO,
                                       FORCE_RET_REF_NO,
                                       FORCE_RESPONSE_MSG,
                                       CARD_TYPE,
                                       AUTH_NETWORK_ID,
                                       AUTH_SOURCE,
                                       MACHINE_ID,
                                       LOCATION_NUMBER,
                                       TEMPLATE_NUMBER,
                                       TRANSACTION_CODE,
                                       CHARGE_OFF,
                                       CANCEL_CD,
                                       TRANSACTION_STATUS,
                                       MACHINE_TRANS_NO',
                     insert_values =>  [$self->{TRANSACTION_NUMBER},
                                       "to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
                                       $Paymentech::CLIENT_NUMBER,
                                       $self->{processor_obj}{MERCHANT_NUMBER},
                                       $self->{processor_obj}{magstrip}, 
                                       $customer_name,
                                       $card_num, 
                                       $exp_date, 
                                       $auth_num, 
                                       $col_num, 
                                       $inv_num, 
                                       $txn_amt,
                                       $tax_amt,
                                       $q1_data, 
                                       $q2_data, 
                                       $button_num,
                                       1,
                                       $self->{processor_obj}{PAY_CLIENT_PID},
                                       $self->{processor_obj}{RIX_CLIENT_PID},
                                       $self->{processor_obj}{TERMINAL_NUMBER},
                                       $self->{processor_obj}{SEQUENCE_NUMBER},
                                       $self->{processor_obj}{ACTION_CODE},
                                       $self->{processor_obj}{AUTH_CODE},
                                       $self->{processor_obj}{BATCH_NUMBER},
                                       $self->{processor_obj}{RETRIEVAL_REF_NUMBER},
                                       substr($self->{processor_obj}{RESPONSE_MESSAGE},0,32),
                                      (defined $self->{CARD_TYPE} ?
											$self->{CARD_TYPE} : ''),
                                       $self->{processor_obj}{AUTH_NET_ID},
                                       $self->{processor_obj}{AUTH_SOURCE},
                                       $machine,
                                       $location_num,
                                       $promo_num,
                                       $self->{processor_obj}{TRANSACTION_TYPE},
                                       $self->{processor_obj}{CHARGE_OFF},
                                       $cancel_code,
										(defined $txn_status ?
													$txn_status : ''),
										(defined $machine_trans_no ?
													$machine_trans_no : '')],
                     noquote_columns =>[1],
                     noquote_values  =>["$txn_date $txn_time"] );


   return;

}



sub local_batch_v2
{
   # in the format of a t4 command
   my ($self, $ORACLE, $machine, $txn_time, $txn_date, $tran_card_type, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, $order_nbr, $track_data, $auth_info, %vended_items) = @_;
   logit "Entering Txndatabase.local_batch_v2(), payload = $machine, $txn_time, $txn_date, $tran_card_type, $txn_amt, $tax_amt, $txn_status, $machine_trans_no, $order_nbr, $track_data, $auth_info";
   
   my $cancel_code;
   if ($self->{CANCEL}) { $cancel_code = 'C'; }
   elsif ($self->{VENDTOOLONG}) { $cancel_code = 'T'; }
   elsif ($self->{DENIED}) { $cancel_code = 'D'; }

   logit "******************** local_batch cancel_code = $cancel_code";

   my $cardreader = CardUtils->new();
   my ($card_num, $customer_name, $exp_date) = $cardreader->parse($self->{processor_obj}{magstrip});


   my $machine_rec = $ORACLE->select( table     => 'rix_Machine',
                                       select_columns => 'Promotion_Template,
                                                          Location_Number',                                                  
                                        where_columns => ['Machine_ID=?'],
                                         where_values => [$machine] );

	my ($promo_num, $location_num) = ('', '');

	if( defined $machine_rec->[0] )
	{
		$promo_num = $machine_rec->[0][0];
		$location_num = $machine_rec->[0][1];
	}

	my ($trans_no, $transaction_num);
	
	if (not defined $order_nbr)
	{
		$transaction_num = $ORACLE->select(  table => 'Dual',
		                                     select_columns => 'TRANS_NO_SEQ.NEXTVAL' );
		$trans_no = $transaction_num->[0][0];
	}
	else
	{
		$trans_no = $order_nbr;
	}
	

   $self->{TRANSACTION_NUMBER} = $trans_no;
   my $card_type = $cardreader->typeAbbrev($card_num);


	logit "local_batch_v2() - trans_no = $trans_no, self->{TRANSACTION_NUMBER} = $self->{TRANSACTION_NUMBER}"; 
	logit "local_batch_v2() - CARD_TYPE = $self->{CARD_TYPE}";
	
	$ORACLE->insert( table => 'Transaction_Record',
                    insert_columns =>  'TRANS_NO,
                                       TRANSACTION_DATE,
                                       CLIENT_NO,
                                       MERCHANT_NO,
                                       MAG_STRIPE,
                                       CUSTOMER_NAME,
                                       CARD_NUMBER,
                                       EXPIRATION_DATE,
                                       TRANSACTION_AMOUNT,
                                       TAX_AMOUNT,
                                       QUANTITY,
                                       FORCE_PCLIENT_ID,
                                       FORCE_RCLIENT_ID,
                                       FORCE_TERMINAL_NO,
                                       FORCE_SEQUENCE_NO,
                                       FORCE_ACTION_CD,
                                       FORCE_CD,
                                       BATCH_NO,
                                       FORCE_RET_REF_NO,
                                       FORCE_RESPONSE_MSG,
                                       CARD_TYPE,
                                       AUTH_NETWORK_ID,
                                       AUTH_SOURCE,
                                       MACHINE_ID,
                                       LOCATION_NUMBER,
                                       TEMPLATE_NUMBER,
                                       TRANSACTION_CODE,
                                       CHARGE_OFF,
                                       CANCEL_CD,
                                       TRANSACTION_STATUS,
                                       MACHINE_TRANS_NO',
                     insert_values =>  [$self->{TRANSACTION_NUMBER},
                                       "to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
                                       $Paymentech::CLIENT_NUMBER,
                                       $self->{processor_obj}{MERCHANT_NUMBER},
                                       $self->{processor_obj}{magstrip}, 
                                       $customer_name,
                                       $card_num, 
                                       $exp_date, 
                                       $txn_amt,
                                       $tax_amt,
                                       1,
                                       $self->{processor_obj}{PAY_CLIENT_PID},
                                       $self->{processor_obj}{RIX_CLIENT_PID},
                                       $self->{processor_obj}{TERMINAL_NUMBER},
                                       $self->{processor_obj}{SEQUENCE_NUMBER},
                                       $self->{processor_obj}{ACTION_CODE},
                                       $self->{processor_obj}{AUTH_CODE},
                                       $self->{processor_obj}{BATCH_NUMBER},
                                       $self->{processor_obj}{RETRIEVAL_REF_NUMBER},
                                       substr($self->{processor_obj}{RESPONSE_MESSAGE},0,32),
                                       $card_type,
                                       $self->{processor_obj}{AUTH_NET_ID},
                                       $self->{processor_obj}{AUTH_SOURCE},
                                       $machine,
                                       $location_num,
                                       $promo_num,
                                       $self->{processor_obj}{TRANSACTION_TYPE},
                                       $self->{processor_obj}{CHARGE_OFF},
                                       $cancel_code,
					(defined $txn_status ?
								$txn_status : ''),
					(defined $machine_trans_no ?
								$machine_trans_no : '')],
                     noquote_columns =>[1],
                     noquote_values  =>["$txn_date $txn_time"] );
                     
    # update PSS schema
    $self->{PSS_TRAN_ID} = 0;
    
	my $dbh_debug_die = $ORACLE->{debug_die};
	$ORACLE->{debug_die} = 0;
	
	my $card_type_validated = 'N';
	my ($pos_pta_id, $regex);
	$tran_card_type = uc(defined $tran_card_type ? $tran_card_type : 'S');
	if (!($tran_card_type =~ m/^(C|P|S|R)$/))
	{
		$tran_card_type = 'S';
	}
	
	### Verify card type can be used at specified device (POS) and validate magstripe and/or resolve ambiguous payment type if multiple exist for card type ###
	my $c_get_pos_payment_type_auth = $ORACLE->query(
		query	=> q{
			SELECT 	pp.pos_pta_id, 
				pp.pos_pta_regex,
				apmb.authority_payment_mask_regex, 
				apma.authority_payment_mask_regex
			FROM pos p, pos_pta pp, payment_subtype ps, client_payment_type cpt, authority_payment_mask apma, authority_payment_mask apmb
			WHERE p.pos_id = pp.pos_id
			AND ps.payment_subtype_id = pp.payment_subtype_id
			AND cpt.client_payment_type_cd = ps.client_payment_type_cd
			AND ps.authority_payment_mask_id = apma.authority_payment_mask_id
			AND pp.authority_payment_mask_id = apmb.authority_payment_mask_id(+)
			AND p.device_id = (SELECT device_id FROM device.device WHERE device_name = ? AND device_active_yn_flag = 'Y')
			AND cpt.client_payment_type_cd = ?
			AND pp.pos_pta_activation_ts IS NULL
			ORDER BY NVL(pp.pos_pta_priority, 1000)
		}
		,values => [
			$machine
			,$tran_card_type
		]
	);

	if (!defined $c_get_pos_payment_type_auth->[0]->[0])
	{
		logit "No pos_pta record exists for device $machine, card type $tran_card_type and transaction timestamp $txn_date. Unable to insert transaction into PSS, machine_trans_no: $machine_trans_no.";
	}
	else
	{
		my @payment_type_auth_choices;
		foreach my $row_aref (@{$c_get_pos_payment_type_auth})
		{
			REGEX_PRECEDENCE_LOOP: for (my $i = 1; $i < 4; $i++)
			{
				if ((@{$row_aref})[$i]) 
				{
					push @payment_type_auth_choices, [(@{$row_aref})[0,$i]];
					last REGEX_PRECEDENCE_LOOP;
				}
			}
		}	

		foreach my $row_aref (@payment_type_auth_choices)
		{
			($pos_pta_id, $regex) = @{$row_aref};

			### determine if card matches this regex
			if (defined $regex && $regex ne '' && $track_data =~ m/$regex/i) 
			{
				$card_type_validated = 'Y';
				last;
			}
		}

		if ($card_type_validated eq 'N')
		{
			logit "Card $track_data cannot be validated using pos_pta settings for device $machine, card type $tran_card_type. Unable to insert transaction into PSS, machine_trans_no: $machine_trans_no.";
		}
		else
		{
			my $db_tran_id = $ORACLE->select(  table => 'DUAL',
							       select_columns => "PSS.SEQ_TRAN_ID.NEXTVAL, TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS')");

			if( defined $db_tran_id->[0] )
			{
				my $tran_id = $db_tran_id->[0][0]; 
				$self->{PSS_TRAN_ID} = $tran_id;
				my $current_dt = $db_tran_id->[0][1];
				
				my ($tran_state_cd, $auth_state_id, $auth_amt_approved, $auth_result_cd, $settlement_batch_state_id, $tran_state_cd);
											
				if ($txn_amt > 0 && $self->{processor_obj}{ACTION_CODE} eq 'A')
				{
					$tran_state_cd 				= '6'; #PROCESSED_SERVER_AUTH_SUCCESS
					$auth_state_id 				= '2'; #AUTH_STATE_SUCCESS
					$auth_amt_approved 			= $txn_amt;
					$auth_result_cd 			= 'Y';
					$settlement_batch_state_id 	= '1'; #SERVER_SETTLEMENT_SUCCESS;
					$tran_state_cd 				= 'D'; #TRANSACTION_COMPLETE
				}
				else
				{
					$tran_state_cd 				= '5'; #PROCESSED_SERVER_AUTH_FAILURE
					$auth_state_id 				= '4'; #AUTH_STATE_FAILURE
					$auth_amt_approved 			= '';
					$auth_result_cd 			= 'N';
					$settlement_batch_state_id 	= '3'; #SERVER_SETTLEMENT_FAILURE;
					$tran_state_cd 				= 'E'; #TRANSACTION_COMPLETE_ERROR
				}

				my $device_tran_cd;
				if (defined $machine_trans_no)
				{
					(undef, undef, $device_tran_cd) = split /:/, $machine_trans_no;
				}
				else
				{
					$device_tran_cd = '';
				}
				
				my ($parsed_acct_num, $parsed_acct_name, $parsed_acct_exp_date) = $cardreader->parse($auth_info);
				
				my $rpt_acct_num = $parsed_acct_num;
				if (length($rpt_acct_num) > 4)
				{
					my $rpt_acct_num1 = substr($rpt_acct_num, 0, -4);
					my $rpt_acct_num2 = substr($rpt_acct_num, -4, 4);					
					$rpt_acct_num1 =~ s/./*/g;
					$rpt_acct_num = $rpt_acct_num1 . $rpt_acct_num2;
				}
				
				$txn_status = uc(defined $txn_status ? $txn_status : 'I');
				if (!($txn_status =~ m/^(C|F|I|N|Q|R|S|T|U)$/))
				{
					$txn_status = 'I'; # incomplete
				}

				logit "Inserting transaction tran_id $tran_id into pss.tran";

				# insert transaction into pss.tran
				$ORACLE->insert( table => 'PSS.TRAN',
				insert_columns => 	'tran_id,
								tran_start_ts,
								tran_end_ts,
								tran_upload_ts,
								tran_state_cd,
								tran_device_tran_cd,
								tran_received_raw_acct_data,
								tran_parsed_acct_name,
								tran_parsed_acct_exp_date,
								tran_parsed_acct_num,
								tran_reportable_acct_num,
								pos_pta_id,
								tran_legacy_trans_no,
								tran_global_trans_cd,
								tran_device_result_type_cd',
				insert_values =>    
				[
								$tran_id,
								"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
								"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
								"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
								$tran_state_cd,
								$device_tran_cd,
								substr($track_data, 0, 107),
								$parsed_acct_name,
								$parsed_acct_exp_date,
								$parsed_acct_num,  
								$rpt_acct_num,
								$pos_pta_id,
								$self->{TRANSACTION_NUMBER},
								(defined $machine_trans_no ? $machine_trans_no : ''),
								$txn_status,
				],
				noquote_columns =>[1, 2, 3],
				noquote_values  =>["$txn_date $txn_time", "$txn_date $txn_time", $current_dt] );
					      
				my $db_auth_id = $ORACLE->select(  table => 'DUAL',
												       select_columns => "PSS.SEQ_AUTH_ID.NEXTVAL");
				
				if( defined $db_auth_id->[0] )
				{		
					my $auth_id = $db_auth_id->[0][0];	

					logit "Inserting local auth auth_id $auth_id into pss.auth";

					# insert transaction into pss.auth
					$ORACLE->insert( table => 'PSS.AUTH',
						insert_columns => 	
									'auth_id,
									tran_id,
									auth_type_cd,
									auth_state_id,
									auth_parsed_acct_data,
									acct_entry_method_cd,
									auth_amt,
									auth_amt_approved,
									auth_ts,
									auth_result_cd,
									auth_resp_cd,
									auth_resp_desc',
						insert_values =>    
						[
									$auth_id,
									$tran_id,
									'L', #local auth
									$auth_state_id,
									substr($self->{processor_obj}{magstrip}, 0, 107),
									'3', #magnetic stripe
									$txn_amt,
									$auth_amt_approved,									
									"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
									$auth_result_cd,
									substr($self->{processor_obj}{RETRIEVAL_REF_NUMBER},0,60),
									substr($self->{processor_obj}{RESPONSE_MESSAGE},0,255)
						],
						noquote_columns =>[8],
						noquote_values  =>[$current_dt] );
						
					my $db_post_auth_id = $ORACLE->select(  table => 'DUAL',
												       select_columns => "PSS.SEQ_AUTH_ID.NEXTVAL");
				
					if( defined $db_post_auth_id->[0] )
					{		
						my $post_auth_id = $db_post_auth_id->[0][0];	

						logit "Inserting post-auth sale auth_id $post_auth_id into pss.auth";

						# insert transaction into pss.auth
						$ORACLE->insert( table => 'PSS.AUTH',
							insert_columns => 	
										'auth_id,
										tran_id,
										auth_type_cd,
										auth_state_id,
										auth_parsed_acct_data,
										acct_entry_method_cd,
										auth_amt,
										auth_amt_approved,
										auth_ts,
										auth_result_cd,
										auth_resp_cd,
										auth_resp_desc',
							insert_values =>    
							[
										$post_auth_id,
										$tran_id,
										'U', #post-auth sale
										$auth_state_id,
										substr($self->{processor_obj}{magstrip}, 0, 107),
										'3', #magnetic stripe
										$txn_amt,
										$auth_amt_approved,									
										"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
										$auth_result_cd,
										substr($self->{processor_obj}{RETRIEVAL_REF_NUMBER},0,60),
										substr($self->{processor_obj}{RESPONSE_MESSAGE},0,255)
							],
							noquote_columns =>[8],
							noquote_values  =>[$current_dt] );							

						my $db_batch_id = $ORACLE->select(  table => 'DUAL',
										   select_columns => 'PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL' );

						if( defined $db_batch_id->[0] )
						{
							my $settlement_batch_id = $db_batch_id->[0][0];   

							$ORACLE->insert( table => 'PSS.SETTLEMENT_BATCH',
								insert_columns => 	'settlement_batch_id,
												settlement_batch_start_ts,
												settlement_batch_end_ts,
												settlement_batch_state_id,
												settlement_batch_resp_cd,
												settlement_batch_resp_desc',
							insert_values =>    
							[
												$settlement_batch_id,
												"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
												"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
												$settlement_batch_state_id,
												substr($self->{processor_obj}{ACTION_CODE},0,60),
												substr($self->{processor_obj}{RESPONSE_MESSAGE},0,60)
							],
							noquote_columns =>[1, 2],
							noquote_values  =>[$current_dt, $current_dt]);

							$ORACLE->insert( table => 'PSS.TRAN_SETTLEMENT_BATCH',
								insert_columns => 	
											'auth_id,
											settlement_batch_id,
											tran_settlement_b_resp_cd,
											tran_settlement_b_resp_desc,
											tran_settlement_b_amt',
							insert_values =>    
							[
											$post_auth_id,
											$settlement_batch_id,
											substr($self->{processor_obj}{ACTION_CODE},0,60),
											substr($self->{processor_obj}{RESPONSE_MESSAGE},0,255),
											$txn_amt
							] );
						}
					}
				}
			}
		}
	}
	
	$ORACLE->{debug_die} = $dbh_debug_die;

	return;
}



sub paymentech_batch 
{

   my ($self, $ORACLE) = @_;

   my $batch_num = $ORACLE->select(  table => 'Dual',
                            select_columns => 'BATCH_NO_SEQ.NEXTVAL' );

   $ORACLE->insert( table => 'Paymentech_Batch',
                    insert_columns =>  'BATCH_ID,
                                       BATCH_NO,
                                       TERMINAL_NO,
                                       CLIENT_NO,
                                       MERCHANT_NO,
                                       MERCHANT_NAME,
                                       ACTION_CD,
                                       BATCH_CD,
                                       BATCH_RET_REF_NO,
                                       BATCH_SEQUENCE_NO,
                                       BATCH_RESPONSE_MSG,
                                       BATCH_OPEN_DATE,
                                       BATCH_CLOSE_DATE,
                                       BATCH_TRAN_CT,
                                       BATCH_NET_AMT,
                                       PMT1_TYPE,
                                       PMT1_TRAN_CT,
                                       PMT1_NET_AMT,
                                       PMT2_TYPE,
                                       PMT2_TRAN_CT,
                                       PMT2_NET_AMT,
                                       PMT3_TYPE,
                                       PMT3_TRAN_CT,
                                       PMT3_NET_AMT,
                                       PMT4_TYPE,
                                       PMT4_TRAN_CT,
                                       PMT4_NET_AMT',
                     insert_values=>   [$batch_num->[0][0],
                                       $self->{processor_obj}{BATCH_NUMBER},
                                       $self->{processor_obj}{TERMINAL_NUMBER},
                                       $Paymentech::CLIENT_NUMBER,
                                       $self->{processor_obj}{MERCHANT_NUMBER},
                                       $self->{processor_obj}{GROUP_CODE},
                                       $self->{processor_obj}{ACTION_CODE},
                                       $self->{processor_obj}{AUTH_CODE},
                                       $self->{processor_obj}{RETRIEVAL_REF_NUMBER},
                                       $self->{processor_obj}{SEQUENCE_NUMBER},
                                       substr($self->{processor_obj}{RESPONSE_MESSAGE},0,32),
                                       $self->{processor_obj}{BATCH_OPEN_TIMESTAMP},
                                       $self->{processor_obj}{BATCH_CLOSE_TIMESTAMP},
                                       $self->{processor_obj}{BATCH_TRANSACTION_COUNT},
                                       $self->{processor_obj}{BATCH_NET_AMOUNT},
                                       $self->{processor_obj}{PAYMENT_1_TYPE},
                                       $self->{processor_obj}{PAYMENT_1_COUNT},
                                       $self->{processor_obj}{PAYMENT_1_NET_AMOUNT},
                                       $self->{processor_obj}{PAYMENT_2_TYPE},
                                       $self->{processor_obj}{PAYMENT_2_COUNT},
                                       $self->{processor_obj}{PAYMENT_2_NET_AMOUNT},
                                       $self->{processor_obj}{PAYMENT_3_TYPE},
                                       $self->{processor_obj}{PAYMENT_3_COUNT},
                                       $self->{processor_obj}{PAYMENT_3_NET_AMOUNT},
                                       $self->{processor_obj}{PAYMENT_4_TYPE},
                                       $self->{processor_obj}{PAYMENT_4_COUNT},
                                       $self->{processor_obj}{PAYMENT_4_NET_AMOUNT}] );

}


sub logit($)
{
	my $line = shift;

	warn "[" . localtime() . "] $line\n";
}

1; # return true
