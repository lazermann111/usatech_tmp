#!/usr/bin/perl
##################################################################
#
# pss_interface-lib.pl
# 
# Version 1.0
#
##################################################################
#
# Written By: Scott Wasserman
# Created:  4/17/2003
# Last Modified: 4/17/2003
# (c)2003 USA Technologies, Inc., All Rights Reserved
#
#
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		04/17/2003	S Wasserman	Original Version
# 1.01		04/21/2003	T Shannon	Converted this library to a perl module
# 1.02      04/23/2003  T Shannon   Changed the pss_SendRequest() method to
#									correctly report a timeout condition back to
#									the calling function.
# 1.03		06/10/2003	T Shannon	Changed the batch and local batch to use
#									once-a-day PSS batch command (OrderDeposit)
# 1.04		06/22/2003	T Shannon	Added BatchSettle command to perform settlements
#									of batched transactions
# 1.05		12/04/2003	P Cowan		Fixed bug involving Amex card refunds
# 1.06		07/02/2004	P Cowan		- Adding approve function
#									- Increasing connection timeout
# 1.07		11/04/2004	P Cowan		Added kludge to Approve for > $1.00 for Local Auth AMEX cards < $1.00
#
#################################################################
# Functions Availible:
#################################################################
#
#  pss_Auth(ClientId,OrderId,AmountLowestCurr,PaymentData,[Currency])
#  pss_Batch(ClientId,OrderId,AmountLowestCurr,[Currency])
#  pss_LocalAuthBatch(ClientId,OrderId,AmountLowestCurr,PaymentData,[Currency])
#  pss_Cancel(ClientId,OrderId)
#  pss_Approve(ClientId,OrderId,AmountLowestCurr,[Currency])
#
#################################################################
# Required Libraries
#################################################################

require LWP::UserAgent;
use HTTP::Request::Common qw(POST);
use CardUtils;

#################################################################
# Globals
#################################################################

$CONNECTION_TIMEOUT = 10;

#################################################################
# ResultCodes
#################################################################
# 0 -	Operation completed successfully
# 1 -	Operation failed due to communication failure with processor
# 2 -	Operation failed due to communication failure with database
# 3 -	Operation failed due to invalid Authority configuration
# 4 -   Operation failed due to WCP object being in an incorrect state for operation requested
# 5 -   Operation failed due to an invalid parameter value
# 10 -	Approve operation failed due to decline from processor
# 11 -	Approve operation failed due to expired instrument
# 12 -	OrderCreate operation failed due to invalid credit card track 2 data
# 20 -	Operation failed due to order not found (Approve or Query)
# 30 -	Operation failed due to duplicate condition (Create)
# 40 -	Operation failed for financial reasons
# 50 -	Unexpected Failure
# 80 -	User has insufficient privileges to perform requested operation
# 81 -	Invalid user/password specified
# 90 -  Payment System does not support the operation specified
# 98 - 	Request parameters are in an incorrect state
# 99 -	Unable to communicate with specified Payment System
# 255 -   Request Timed Out

$VERSION        = '0.01';

# Added 04/14/2005 - pcowan
my $global_modify_approval_switch = 0;

########################################
#
# pss_Auth
#
# Written By: Scott Wasserman
# Created: 4/18/2003
# Last Modified: 4/18/2003
#
########################################
#
# INPUT:  ClientId,OrderId,AmountLowestCurr,PaymentData,[Currency]
# OUTPUT: ResultCode
#
# Note: Currency is optional and will default to 840 (US Dollars) if it is null
#
########################################
sub pss_Auth 
{
	local(	$ResultXML,
			$ResultCode,
			$ResultText,
			$ClientId,
			$OrderId,
			$AmountLowestCurr,
			$PaymentData,
			$Currency,
			$UseTrack2);

	(	$ClientId,
		$OrderId,
		$AmountLowestCurr,
		$PaymentData,
		$Currency,
		$UseTrack2) = @_;

	if ($Currency eq "") 
	{
		$Currency = "840";
	}

	warn "PSSOperation     = $PSSOperation\n";
	warn "ClientId         = $ClientId\n";
	warn "OrderId          = $OrderId\n";
	warn "AmountLowestCurr = $AmountLowestCur\n";
	warn "PaymentData      = $PaymentData\n";
	warn "Currency         = $Currency\n";
	warn "UseTrack2        = $UseTrack2\n";
	
	# Authorize Transaction
	$ResultXML = &pss_Send_Request( "OrderCreateAndApprove",
									$ClientId,
									$OrderId,
									$AmountLowestCurr,
									$PaymentData,
									$Currency,
									$UseTrack2);
	
	# Parse ResultXML
	($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);

	return ($ResultCode,$ResultText);
}


########################################
#
# pss_Batch
#
# Written By: Scott Wasserman
# Created: 4/18/2003
# Last Modified: 4/28/2003
#
# Change Log
# ---------------------------------------
# Date		Programmer		Description
# ----		----------		-----------
# 4/28/03	T. Shannon		Added PSS command OrderComplete to change 
#							order to WaitingForSettlement state
########################################
#
# INPUT:  ClientId,OrderId,AmountLowestCurr,[Currency]
# OUTPUT: ResultCode
#
# Note: Currency is optional and will default to 840 (US Dollars) if it is null
#
########################################
sub pss_Batch 
{
	local(	$ResultXML,
			$ResultCode,
			$ResultText,
			$ClientId,
			$OrderId,
			$AmountLowestCurr,                 
			$Currency,
			$CardType,
			$cr);

	(	$ClientId,
		$OrderId,
		$AmountLowestCurr,
		$Currency,
		$swipe) = @_;

	if ($Currency eq "") 
	{
		$Currency = "840";
	}
	
	if($global_modify_approval_switch)
	{
		# Check amount of transaction to see if it's different from the authed amount
		$ResultXML = &pss_Send_Request("ApprovalQuery", $ClientId, $OrderId);
	
		# Parse ResultXML
		($ResultCode, $ResultText) = &pss_Parse_Result($ResultXML);
		
		if ($ResultText ne $AmountLowestCurr)
		{
			# Modify amount of transaction
		
			warn "pss_Batch() Authed Amount <> Transaction Amount : Auth = $ResultText, Tran = $AmountLowestCurr\n";
	
			#!!! HACK ALERT !!! T Shannon 5/14/2003
			# because of a problem with the PaymentManager casette not being able to ApprovalModify Amex cards
			# we must instead apply a refund for the difference between the authed amount and the transaction
			# amount. 
			
			# Bug Fix - P Cowan - 12/04/2003
			# The hack above was causing people to get a refund that should not have received.  Since we can't 
			# do ApprovalModify for Amex cards, just deposit the amount and don't modify the approval.
			
			$cr = CardUtils->new();
			$CardType = $cr->typeAbbrev(split(/=/,$swipe));

			# Changed to skip modify if amount > $10 - P. Cowan - 03/31/2005
			if ($CardType ne 'AE' && $AmountLowestCurr < 1000)
			{
				# if not an Amex card do the approval modify...
				$ResultXML = &pss_Send_Request(  "ApprovalModify",
				                            $ClientId,
				                            $OrderId,
				                            $AmountLowestCurr,
				                            "",
				                            $Currency);
	
				# Parse ResultXML
				($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
				
				# if command failed then exit
				if ( ! (($ResultCode eq '0') or ($ResultCode eq '6')) )
				{
					return ($ResultCode,$ResultText);
				}
			}
			else
			{
				warn "American Express card, skipping ApprovalModify for $ClientId $OrderId $AmountLowestCur\n";
			}
			#else
			#{
			#	# refund the difference between the auth amount and the transaction amount
			#	my $diff = $ResultText - $AmountLowestCurr;
			#	$ResultXML = &pss_Send_Request(  "OrderRefund",
			#	                            $ClientId,
			#	                            $OrderId,
			#	                            $diff);			
			#	
			#}		
		}
	}
		
	# Batch the transaction
	$ResultXML = &pss_Send_Request(  "OrderDeposit",
	                            $ClientId,
	                            $OrderId,
	                            $AmountLowestCurr);
	
	# Parse ResultXML
	($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
	
	return ($ResultCode, $ResultText);
		
}

########################################
#
# pss_BatchSettle
#
# Written By: Tom Shannon
# Created: 6/22/2003
# Last Modified: 6/22/2003
#
# Change Log
# ---------------------------------------
# Date		Programmer		Description
# ----		----------		-----------
# 6/22/03	T. Shannon		New command to perform settlement of  
#							batched commands
########################################
#
# INPUT:  ClientId,OrderId,AmountLowestCurr,[Currency]
# OUTPUT: ResultCode
#
# Note: Currency is optional and will default to 840 (US Dollars) if it is null
#
########################################
sub pss_BatchSettle
{
       local(	$ResultXML,
                $ResultCode,
                $ResultText,
                $ClientId,
                $OrderId,
                $AmountLowestCurr,                 
                $Currency,
                $CardType,
                $cr);

	($ClientId,
	$OrderId) = @_;
	
	$AmountLowestCur = 0;
	$PaymentData = '';
	$Currency = "840";

	$ResultXML = &pss_Send_Request(  "BatchSettle",
	                            $ClientId,
	                            $OrderId,
	                            $AmountLowestCur,
	                            $PaymentData,
	                            $Currency);			
			
	# Parse ResultXML
	
	($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
	
	return ($ResultCode,$ResultText);
		
}

sub pss_BatchSettleConditional
{
       local(	$ResultXML,
                $ResultCode,
                $ResultText,
                $ClientId,
                $OrderId,
                $AmountLowestCurr,                 
                $Currency,
                $CardType,
                $cr,
                $MaxHours,
                $MaxTransactions);

	($ClientId,	$OrderId, $MaxHours, $MaxTransactions) = @_;
	
	$AmountLowestCur = 0;
	$PaymentData = '';
	$Currency = "840";

	$ResultXML = &pss_Send_Request(  "BatchSettleConditional",
	                            $ClientId,
	                            $OrderId,
	                            $AmountLowestCur,
	                            $PaymentData,
	                            $Currency,
	                            "",
	                            $MaxHours, 
	                            $MaxTransactions);
			
	# Parse ResultXML
	
	($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
	
	return ($ResultCode,$ResultText);
		
}


########################################
#
# pss_BatchImmediate
#
# Written By: 		Tom Shannon
# Created: 			4/28/2003
# Last Modified: 	4/28/2003
#
########################################
#
# INPUT:  	ClientId,OrderId,AmountLowestCurr,[Currency]
# OUTPUT: 	ResultCode
# USAGE:  	This version is called by applications that require an immediate
#			response from the PSS system rather than pss_Batch which adds
#			the transaction to a queue for later processing
#
# Note: Currency is optional and will default to 840 (US Dollars) if it is null
#
########################################
sub pss_BatchImmediate
{
       local(	$ResultXML,
                $ResultCode,
                $ResultText,
                $ClientId,
                $OrderId,
                $AmountLowestCurr,                 
                $Currency,
                $CardType,
                $cr);

	(  $ClientId,
	   $OrderId,
	   $AmountLowestCurr,
	   $Currency,
	   $swipe) = @_;
	
	if ($Currency eq "") 
	{
	  $Currency = "840";
	}
	
	if($global_modify_approval_switch)
	{
		# Check amount of transaction to see if it's different from the authed amount
		$ResultXML = &pss_Send_Request(  "ApprovalQuery",
	                                $ClientId,
	                                $OrderId);

		# Parse ResultXML
		
		($ResultCode, $ResultText) = &pss_Parse_Result($ResultXML);
	
		if ($ResultText ne $AmountLowestCurr)
		{
			# Modify amount of transaction
		
			warn "pss_BatchImmediate() Authed Amount <> Transaction Amount : Auth = $ResultText, Tran = $AmountLowestCurr\n";
			warn "pss_BatchImmediate() Card Type : $CardType\n";
			
			#!!! HACK ALERT !!! T Shannon 5/14/2003
			# because of a problem with the PaymentManager casette not being able to ApprovalModify Amex cards
			# we must instead apply a refund for the difference between the authed amount and the transaction
			# amount. 
			
			# Bug Fix - P Cowan - 12/04/2003
			# The hack above was causing people to get a refund that should not have received.  Since we can't 
			# do ApprovalModify for Amex cards, just deposit the amount and don't modify the approval.
			
			$cr = CardUtils->new();
			$CardType = $cr->typeAbbrev(split(/=/,$swipe));
	
			# Changed to skip modify if amount > $10 - P. Cowan - 03/31/2005
			if ($CardType ne 'AE' && $AmountLowestCurr < 1000)
			{
				# if not an Amex card do the approval modify...
				$ResultXML = &pss_Send_Request(  "ApprovalModify",
				                            $ClientId,
				                            $OrderId,
				                            $AmountLowestCurr,
				                            "",
				                            $Currency);
	
				# Parse ResultXML
				
				($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
				
				# if command failed then exit
				
				if ( ! (($ResultCode eq '0') or ($ResultCode eq '6')) )
				{
					return ($ResultCode,$ResultText);
				}
			}
			else
			{
				warn "American Express card, skipping ApprovalModify for $ClientId $OrderId $AmountLowestCur\n";
			}
			#else
			#{
			#	# refund the difference between the auth amount and the transaction amount
			#	my $diff = $ResultText - $AmountLowestCurr;
			#	$ResultXML = &pss_Send_Request(  "OrderRefund",
			#	                            $ClientId,
			#	                            $OrderId,
			#	                            $diff);			
			#	
			#}		
		}
	}
	
	# Try to batch the order
	
	$ResultXML = &pss_Send_Request(  "OrderBatch",
	                                $ClientId,
	                                $OrderId,
	                                $AmountLowestCurr);
	
	# Parse ResultXML
	warn "PSS - Operation  = OrderBatch\n";
	warn "PSS - ResultCode = $ResultCode\n";
	
	($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
	
	return ($ResultCode,$ResultText);

}

########################################
#
# pss_LocalAuthBatch
#
# Written By: Scott Wasserman
# Created: 4/18/2003
# Last Modified: 4/18/2003
#
########################################
#
# INPUT:  ClientId,OrderId,AmountLowestCurr,PaymentData,[Currency]
# OUTPUT: ResultCode
#
# Note: Currency is optional and will default to 840 (US Dollars) if it is null
#
########################################
sub pss_LocalAuthBatch
{
	local(	$ResultXML,
		$ResultCode,
		$ResultText,
		$ClientId,
		$OrderId,
		$AmountLowestCurr,
		$PaymentData,
		$Currency,
		$cr);

	(	$ClientId,
		$OrderId,
		$AmountLowestCurr,
		$PaymentData,
		$Currency) = @_;

	if ($Currency eq "") 
	{
		$Currency = "840";
	}
	
	$cr = CardUtils->new();
	$CardType = $cr->typeAbbrev(split(/=/,$PaymentData));
	
	if($CardType eq 'AE' && $AmountLowestCurr < 100)
	{
		warn "Substituting 200 for AMEX Local Auth Batch value $AmountLowestCurr\n";
		
		# AMEX will not authorize a card for less than $1, so hardcode $2 and we'll deposit the correct amount
		# Authorize Transaction
		$ResultXML = &pss_Send_Request(	"OrderCreateAndApprove",
										$ClientId,
										$OrderId,
										'200',
										$PaymentData,
										$Currency);
	}	
	else
	{
		# Authorize Transaction
		$ResultXML = &pss_Send_Request(	"OrderCreateAndApprove",
										$ClientId,
										$OrderId,
										$AmountLowestCurr,
										$PaymentData,
										$Currency);
	}

	# Parse ResultXML
	($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
	warn "PSS - Operation  = OrderCreateAndApprove\n";
	warn "PSS - ResultCode = $ResultCode\n";

	# If We Were Successfully Authorized Try to Settle
	if ($ResultCode eq '0') 
	{
		# Settle Transaction
		$ResultXML = &pss_Send_Request(	"OrderDeposit",
										$ClientId,
										$OrderId,
										$AmountLowestCurr);

		# Parse ResultXML
		($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
	}
	
	warn "PSS - Operation  = OrderComplete\n";
	warn "PSS - ResultCode = $ResultCode\n";

	return ($ResultCode,$ResultText);
}


########################################
#
# pss_LocalAuthBatchImmediate
#
# Written By: 		Tom Shannon	
# Created: 			4/28/2003
# Last Modified: 	4/28/2003
#
########################################
#
# INPUT:  	ClientId,OrderId,AmountLowestCurr,PaymentData,[Currency]
# OUTPUT: 	ResultCode
# USAGE:  	This version is called by applications that require an immediate
#			response from the PSS system rather than pss_LocalAuthBatch which adds
#			the transaction to a queue for later processing
#
# Note: Currency is optional and will default to 840 (US Dollars) if it is null
#
########################################
sub pss_LocalAuthBatchImmediate
{
       local(	$ResultXML,
                $ResultCode,
                $ResultText,
                $ClientId,
                $OrderId,
                $AmountLowestCurr,
                $PaymentData,
                $Currency);

    (  $ClientId,
       $OrderId,
       $AmountLowestCurr,
       $PaymentData,
       $Currency) = @_;

   if ($Currency eq "") 
   {
      $Currency = "840";
   }

   # Authorize Transaction

   $ResultXML = &pss_Send_Request(  "OrderCreateAndApprove",
                                    $ClientId,
                                    $OrderId,
                                    $AmountLowestCurr,
                                    $PaymentData,
                                    $Currency);

   # Parse ResultXML

   ($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);
	warn "PSS - Operation  = OrderCreateAndApprove\n";
	warn "PSS - ResultCode = $ResultCode\n";

   # If We Were Successfully Authorized try to settle

   if ($ResultCode eq '0') 
   {
      # Settle Transaction

      $ResultXML = &pss_Send_Request(  "OrderBatch",
                                    $ClientId,
                                    $OrderId,
                                    $AmountLowestCurr,
                                    $PaymentData,
                                    $Currency);

      # Parse ResultXML

      ($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);

   }
	warn "PSS - Operation  = OrderComplete\n";
	warn "PSS - ResultCode = $ResultCode\n";

   return ($ResultCode, $ResultText);

}

########################################
#
# pss_Cancel
#
# Written By: Scott Wasserman
# Created: 4/18/2003
# Last Modified: 12/17/2003
#
########################################
#
# INPUT:  ClientId, OrderId
# OUTPUT: ResultCode
#
########################################
sub pss_Cancel 
{
	local(	$ResultXML,
			$ResultCode,
			$ResultText,
			$ClientId,
			$OrderId );

	($ClientId, $OrderId) = @_;

	# Cancel Transaction
	$ResultXML = &pss_Send_Request("OrderCancel", $ClientId, $OrderId);

	# Parse ResultXML
	($ResultCode,$ResultText) = &pss_Parse_Result($ResultXML);

	return ($ResultCode,$ResultText);
}


########################################
#
# pss_Send_Request
#
# INPUT:   PSSOperation,ClientId,OrderId,AmountLowestCurr,PaymentData,Currency
# OUTPUT: ResultXML
#
########################################
sub pss_Send_Request 
{
   local(	$url,
                $agent_name,
                $user_agent,
                $response,
                $post_data_request,
                $PSSOperation,
                $ClientId,
                $OrderId,
                $AmountLowestCurr,
                $PaymentData,
                $Currency,
                $UseTrack2,
                $MaxHours,
                $MaxTransactions	);

    (  $PSSOperation,
       $ClientId,
       $OrderId,
       $AmountLowestCurr,
       $PaymentData,
       $Currency,
       $UseTrack2,
       $MaxHours,
       $MaxTransactions
	) = @_;

   $PaymentData =~ s/=/%3D/gi;

   warn "PSSOperation     = $PSSOperation\n";
   warn "ClientId         = $ClientId\n";
   warn "OrderId          = $OrderId\n";
   warn "AmountLowestCurr = $AmountLowestCur\n";
   warn "PaymentData      = $PaymentData\n";
   warn "Currency         = $Currency\n";
   warn "UseTrack2        = $UseTrack2\n";
   warn "Max Hours        = $MaxHours\n";
   warn "Max Transactions = $MaxTransactions\n";
	
   $post_data = "PSSOperation=$PSSOperation&ClientId=$ClientId&OrderId=$OrderId&AmountLowestCurr=$AmountLowestCurr&PaymentData=$PaymentData&Currency=$Currency&AVS1=123%20MAIN%20ST&AVS2=12345&MaxHours=$MaxHours&MaxTransactions=$MaxTransactions&UseTrack2=$UseTrack2";
   #$url = "http://192.168.13.115/professor/PssRequestServlet";
   $url = "http://10.24.77.139/professor/PssRequestServlet";
   #$url = "http://10.0.0.84/professor/PssRequestServlet";

   $agent_name = "PSSReRIXAgent/1.0";   

   # GET Code
   $user_agent = new LWP::UserAgent;

   $user_agent->agent($agent_name);

   $user_agent->timeout($CONNECTION_TIMEOUT);

   $request = new HTTP::Request('GET', $url."?".$post_data);

   $response = $user_agent->request($request); 
   
   warn "pss_SendRequest() - url = $url\n";
   warn "pss_SendRequest() - post_data = $post_data\n";
   warn "pss_SendRequest() - response = " . $response->as_string() . "\n";
   
# Check the outcome of the response
	if ($response->is_success) {
	  return $response->as_string();
	} else {
	  return  "read timeout";
	}  

   return $response->as_string();

}

########################################
#
# pss_Parse_Result
#
# INPUT: ResultXML
# OUTPUT: ResultCode,ResultText
#
########################################
sub pss_Parse_Result 
{
   local( $ResultXML,
          $ResultCode,
          $ResultText,
          $start_of_result_code,
          $end_of_result_code,
          $start_of_result_text,
          $end_of_result_text);

   $ResultXML = $_[0];
   
   if ($ResultXML =~ "read timeout") 
   {
      $ResultCode = "255";
      $ResultText = "Communication Timeout";
   }
   else
   {
      $ResultCode = &find_XML_Field_Data($ResultXML,"ResultCode");
      $ResultText = &find_XML_Field_Data($ResultXML,"ResultText");
   }
   
   return ($ResultCode,$ResultText);

}                         

########################################
#
# find_XML_Field_Data
#
# INPUT: XML_Data,XML_Tag,nth_occurence
# OUTPUT: XML_Field_Data
#
########################################
sub find_XML_Field_Data 
{
   local(   $XML_Data,
            $XML_Tag,
            $nth_occurence,
            $start_of_XML_tag,
            $start_of_XML_field,
            $end_of_XML_field,
            $XML_field_data,
            $n_count,
            $found_error);

   $XML_Data = $_[0];
   $XML_Tag = $_[1];
   $nth_occurence = $_[2];

   if ($nth_occurence eq "")
   {
      $nth_occurence = 1;
   }

   $n_count = 0;
   $start_of_XML_tag = -1;
   $found_error = 0;

   while ( ($n_count < $nth_occurence) && ($found_error == 0) )
   {

      $start_of_XML_tag += 1;

      $start_of_XML_tag = index($XML_Data,'<'.$XML_Tag.'>',$start_of_XML_tag);
      
      if ($start_of_XML_tag < 0) 
      {
          $found_error = 1;
      }
      else
      {
         $n_count++;
      }

   }

      # Did we find the nth occurence of the tag?
   if ($found_error) 
   {
      $XML_field_data = "";
   }
   else
   {

      $start_of_XML_field = $start_of_XML_tag+length($XML_Tag)+2;

      $end_of_XML_field = index($XML_Data,'</'.$XML_Tag.'>',$start_of_XML_field);

      # Did we find the end tag?
      if ($end_of_XML_field < 0)
      {
         $XML_field_data = "";
      }
      else
      {
         $XML_field_data = substr($XML_Data,$start_of_XML_field,($end_of_XML_field-$start_of_XML_field));
      }


   }

   return $XML_field_data;
   
   
}


########################################
#
# clean_URL_Data(URL)
#  
########################################
sub clean_URL_Data
{

	local($urlout,@url_out_split,$url_out_char);
	
	$urlout = $_[0];
	
	#$urlout =~ s/\t/%09/gi; # Tab (\t)
	$urlout =~ s/ /+/gi;	# Space ( )
	#$urlout =~ s/#/%23/gi;	# Number sign (#)
	$urlout =~ s/%/%25/gi;	# Percent sign (%)
	$urlout =~ s/&/%26/gi;	# Ampersand (&)
	
	$urlout =~ s/\(/%28/gi;	# Right Parenthesis (()
	$urlout =~ s/\)/%29/gi;	# Left Parenthesis ())
	
	#$urlout =~ s/'/%27/gi;	# Apostrophe (')
	$urlout =~ s/\?/%3f/gi;	# Question mark (?)
	$urlout =~ s/\@/%40/gi;	# At symbol (@)
	#$urlout =~ s/^/%5f/gi;	# Caret symbol (^)
	
        # Replace # with %23
        @url_out_split = split(//,$urlout);
        $urlout = "";

        foreach $url_out_char (@url_out_split)
        {
            if (ord($url_out_char) == 35) 
            {
               $urlout .= '%23';
                             
            }
            else
            {
               $urlout .= $url_out_char;
            }
        }

	return $urlout;

}

########################################
#
# email_Message(to,from,subject,bodytext,cc)
#  
########################################
sub email_Message
{

	open (MAIL, "|/usr/lib/sendmail -t");
	print (MAIL "To: ",$_[0],"\n");
        if ($_[4] ne "") 
        {
           print (MAIL "Cc: ",$_[4],"\n");
        }
	print (MAIL "From: ",$_[1],"\n");
	print (MAIL "Subject: ",$_[2],"\n\n");
	print (MAIL $_[3],"\n");
	print (MAIL "\n");
	close (MAIL);

}


#END { }

#} # BEGIN loop ends here

1; # return true


