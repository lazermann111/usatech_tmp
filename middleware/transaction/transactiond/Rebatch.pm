#!/usr/bin/perl

#---------------------------------------------------------------------------------------------
#	Description
#	-----------
#	Used for rebatching transactions that were not correctly batched with PSS
#
# Change History
# --------------
# Version       Date            Programmer      Description
# -------       ----------      ----------      ----------------------------------------------
# 1.00          12/22/2002      T Shannon       Original Version

use strict;
use Net::Daemon;
use Evend::Database::Database;
use Paymentech;
use SpecialCard;
use FHMS;
use Txndatabase;
use CardUtils;


sub logit($);


	my $ORACLE = Evend::Database::Database->new(print_query => 1, 
						execute_flatfile=>1, 
						debug => 1, 
						debug_die => 0);



	my $txn_ref = $ORACLE->select(table => 'transaction_record_hist', 
		             select_columns => 'machine_id, ' .
                                               'trans_no, ' .
                                               'mag_strip, ' .
                                               'transaction_amount, ' .
                                               'card_type, ' .
                                               'transaction_status, '.
                                               'tax_amount ',
		            where_columns =>  ["transaction_date BETWEEN TO_DATE('20030701','YYYYMMDD') AND TO_DATE('20030711','YYYYMMDD')",
                                              "force_response_msg = 'NULL'", 
                                              "transaction_status not in ('SS','SF')",
                                              "card_type <> 'SP'", 
                                              "cancel_cd IS NULL"]);

	my $i = 0;
	foreach my $txtrow (@$txn_ref)
	{

	    my ($machine, $txn_num, $magstrip, 
	    		$txn_amt, $card_type, $txn_status, $tax_amt) = @{$txn_ref->[$i++]};
	
	
		my $total_amt = sprintf("%.2f", $txn_amt + $tax_amt);
	
		# credit card types
		my @cccardtype = ('MC','VI','DS','AE','DC','JC','ENR');
	
	
	# --- PSS code ---
		my ($order_nbr, $track2);
	
		my $cardreader = CardUtils->new();
	
		my ($card_num, $customer_name, $exp_date) = $cardreader->parse($magstrip);
	
		warn "t2_PSS - processing $machine\n";
	
		$track2 = $cardreader->GetTrack2($magstrip);
	
		$processor->{magstrip} = $track2;
	
		$processor = FHMS->new( machine => $machine) if grep (/$card_type/, @cccardtype);
	
	    $processor = SpecialCard->new( machine => $machine ) if $card_type eq 'SP';
	#---------------------------------------------------------------------------------------------
	
	    warn "Sale to FHMS\n";
	    $processor->make_sale($track2, $total_amt, $txn_num);
		$processor->talk();
		$processor->read_auth_response();
	
	    foreach (keys %$processor)
	    {
	            warn("processor: $_ =>  $processor->{$_}\n");
	    }
	
	    logit "Batch results : $processor->{ACTION_CODE}, $txn_num, 1";
	    
	} # end of foreach()




sub logit($)
{
	my $line = shift;

	warn "[" . localtime() . "] $line\n";
}

1;
