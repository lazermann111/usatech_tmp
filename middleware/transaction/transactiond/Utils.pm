#!/usr/bin/perl -w

#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		09/19/2003	pcowan		Creation of commands:
#

package ReRix::Utils;

use strict;

use Evend::Database::Database;        # Access to the Database


sub get_device_data
{
	my ($machine_id, $DATABASE) = @_;
	
	my $array_ref = $DATABASE->select(
						table          => 'device',
						select_columns => 'device_id, device_serial_cd, device_type_id',
						where_columns  => ['device_name = ?', 'device_active_yn_flag=?'],
						where_values   => [ $machine_id, 'Y' ]
				);

	if(not $array_ref->[0])
	{
		return (undef, undef, undef);
	}
	
	return ($array_ref->[0][0], $array_ref->[0][1], $array_ref->[0][2]);
}

sub get_message_number
{
	my ($DATABASE) = @_;
	
	my $message_no_ref = $DATABASE->select(
				table			=> 'dual',
				select_columns	=> 'tazdba.rerix_message_num_seq.nextval');
				
	return $message_no_ref->[0][0];
}

sub blob_select
{
	my ($DATABASE, $sqlStmt) = @_;
	my $db = $DATABASE->{handle};	

	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB

	my $stmt = $db->prepare(q{$sqlStmt});

	$stmt->execute();
	
	if(defined $db->errstr)
	{
		print "blob_select error: " . $db->errstr . "\n";
		return undef;
	}

	my $row = 0;
	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}

sub blob_insert
{
	my ($DATABASE, $sqlStmt, $buf) = @_;
	my $db = $DATABASE->{handle};	
	my $LONG_RAW_TYPE=24;
	my $stmt = $db->prepare(q{$sqlStmt});
	my %attrib;
	$attrib{'ora_type'} = $LONG_RAW_TYPE;
	$stmt->bind_param(":blob", $buf, \%attrib);  
	$stmt->execute();
	if(defined $db->errstr)
	{
		print "blob_insert error: " . $db->errstr . "\n";
	}

}

sub getAdjustedTimestamp
{
#----------------------------------------------------------------------------------------------------------
# 	Function: 		: getAdjustedTimestamp ($timezone, $timestamp)
# 	Description 		: This function returns an adjusted 32-bit timestamp value based upon GMT
#	Params:
#		$timezone 	: The timezone to adjust **TO**. If one is not provided
#				  EST will be the default
#		$timestamp	: The timestamp to adjust. If one is not provided then localtime
#				  EST will be used as the default
#	Returns:
#		The adjusted 32-bit timestamp
#
#	Change History		:
#		Version		Developer	Date		Description
#		1.0		T Shannon	10/15/03	first rev
#				
#----------------------------------------------------------------------------------------------------------

	my ($timezone, $timestamp) = @_;
	
	# we need to know if its daylight savings time; if so move the timestamp 'forward' an additional hour
	my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
	# for testing normal time during DST ONLY!!
	#$isdst = 0;

	# if a timestamp is not provided grab the system time (which is in GMT)
	if (!defined $timestamp)
	{
		$timestamp = time();
	}
	
	SWITCH: {
		if ($timezone eq 'CST') {$timestamp -= (3600 * (6 - $isdst)); last SWITCH;}	
		if ($timezone eq 'MT') {$timestamp -= (3600 * (7 - $isdst)); last SWITCH;}	
		if ($timezone eq 'PT') {$timestamp -= (3600 * (8 - $isdst)); last SWITCH;}
		# if the time zone is not identified (or IS set to "EST") adjust the time back 
		# only 5 hours (normal time)to adjust to EST
		$timestamp -= (3600 * (5 - $isdst));
		}
	return $timestamp;
}

1;
