package Transactiond;

use strict;
use Net::Daemon;
use TransactionHandler;

use vars qw($VERSION @ISA $DATABASE);
$VERSION = '0.01';
@ISA = qw(Net::Daemon);

#globals/constants
use vars    qw(@PREFIXES $TABLE $DATABASE);


sub Run
{
	my ($self) = @_;
	my $sock = $self->{socket};

	my $timestamp = localtime();
	warn("new connection at $timestamp\n");

	my $handler = TransactionHandler->new();


	while (my $line = $sock->getline())
	{
		chomp $line;            
		chop $line if ($line =~ /\r$/);

		warn "inbound $line\n";
                warn "***************** LINE = $line";

  		my ($command, $payload) = split /,/, $line, 2;

		my ($auth_code,$transaction_number,$balance, $inv, $pss_tran_id);

		($auth_code, $transaction_number, $balance) = $handler->t1($payload) if $command eq 't1';

		($auth_code,$transaction_number, $inv) = $handler->t2($payload) if $command eq 't2';

		($auth_code,$transaction_number, $inv) = $handler->t4($payload) if $command eq 't4';

		($auth_code, $transaction_number, $balance, $pss_tran_id) = $handler->t6($payload) if $command eq 't6';

		($auth_code, $transaction_number, $balance) = $handler->t7($payload) if $command eq 't7';

		($auth_code, $transaction_number, $inv) = $handler->t8($payload) if $command eq 't8';

		($auth_code, $transaction_number, $inv) = $handler->t9($payload) if $command eq 't9';

		($auth_code, $transaction_number, $inv, $pss_tran_id) = $handler->t10($payload) if $command eq 't10';

		($auth_code, $transaction_number, $inv, $pss_tran_id) = $handler->t11($payload) if $command eq 't11';

		print $sock "$auth_code,$transaction_number,$balance,$inv,$pss_tran_id\n";

  	}
 
   $sock->close();
   
   return;
}


1;
