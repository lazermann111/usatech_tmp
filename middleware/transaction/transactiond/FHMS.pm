package FHMS;
require 5.004;

use strict;
use Evend::Database::Database;
use CardUtils;
use IO::Socket;
use IO::Select;
require 'pss_interface.pm';



BEGIN {
	
#use Exporter	();
#use vars	qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
#$VERSION	= '0.01';
#@ISA		= qw(Exporter);
    
#export by default
#@EXPORT		= qw();

#export on demand
#@EXPORT_OK	= qw();

#global/constants
use vars	qw( $STX $ETX $FS $CLIENT_NUMBER $SYSTEM_INFO %MERCHANT_ID $NUM_TERMINALS $SALE $AUTH_ONLY $FORCE $RETURN $VOID $BATCH_INQUIRY $BATCH_RELEASE $CANCEL);

# all these can be access outside of this module by ThisModuleName:: operator

sub logit;
 
sub new
{
	my ($class, %arg) = @_;
	
	my $ORACLE = new Evend::Database::Database(print_query=>1);
	
	my $group_code;
	if ($arg{group_code}) 
	{ 
		$group_code = $arg{group_code}; 
	}
	else 
	{
		my $group_rec =  $ORACLE->select(table          => 'machine_hierarchy',
	                                     select_columns => 'Group_Cd',
	                                     where_columns  => ['Machine_Id=?'],
	                                     where_values   => [$arg{machine}] );
		$group_code = $group_rec->[0][0];
   }
   
   $ORACLE->close();
   $group_code ||= 'GENERAL';


   warn("$MERCHANT_ID{$group_code} $MERCHANT_ID{'GENERAL'}");

   my $this = {
		SALE		=> '01',
		AUTH_ONLY	=> '02',
		FORCE		=> '03',
		RETURN		=> '06',
		VOID		=> '41',
		BATCH_INQUIRY	=> '50',
		BATCH_RELEASE	=> '51',
		CURRENCY    => '840',
		MACHINE_ID          => $arg{machine},
		#CLIENT_ID   		=> $arg{client_id},
		GROUP_CODE          => $group_code,
		MERCHANT_NUMBER     => $MERCHANT_ID{$group_code}
									|| $MERCHANT_ID{'GENERAL'},
		TRANSACTION_TYPE    => $arg{transaction_type},
		TERMINAL_NUMBER     => '000',
		SEQUENCE_NUMBER     => '000000',
		TIMED_OUT => 0, # just to make this explicit
		ACTION_CODE         => 'T',
		REQUEST             => '', # the major payloa
		RESPONSE            => '',
		CARD_TYPE			=> '',
		USE_TRACK_2			=> 'Y'
   };
     
   bless $this, $class;
}


sub close 
{
   my ($self) = @_;

   undef $self;
}


sub talk 
{
	my ($self) = @_;
	
	my $request;
	my $result;
	my $result_text;
	my $response;
	my $responseBuffer;
	my $size;
	my $paymentdata;

#--- PSS ------------------------------------------------------------------------------

# -- Auth --

	logit "FHMS->talk() - trans type = $self->{TRANSACTION_TYPE}";

	if ($self->{TRANSACTION_TYPE} eq $self->{AUTH_ONLY})
	{
		logit "FHMS->talk() - AUTH - client		: $self->{MACHINE_ID}";
		logit "FHMS->talk() - AUTH - order id	: $self->{ORDER_ID}";
		logit "FHMS->talk() - AUTH - auth amt	: $self->{AUTH_AMT}";
		logit "FHMS->talk() - AUTH - card swipe : $self->{PAYMENT_DATA}";
		logit "FHMS->talk() - AUTH - use track2 : $self->{USE_TRACK_2}";
		
		if ($self->{AUTH_AMT} != 0)
		{
			($result, $result_text) = pss_Auth($self->{MACHINE_ID},
		       									$self->{ORDER_ID},
		       									$self->{AUTH_AMT} * 100,
		       									$self->{PAYMENT_DATA},
		       									$self->{CURRENCY},
		       									$self->{USE_TRACK_2});
		}
		
		logit "FHMS->talk() - AUTH - Result     : $result";
		logit "FHMS->talk() - AUTH - Response   : $result_text";
					
		$self->{RESPONSE_MESSAGE} = $result_text;
		if ($result eq '0')
		{
			$self->{ACTION_CODE} = 'A';
		}
		else
		{
			$self->{ACTION_CODE} = 'E';
		}
		return;
	}

# -- Batch ---
	if ($self->{TRANSACTION_TYPE} eq $self->{SALE})
	{
		logit "FHMS->talk() - BATCH - client     : $self->{MACHINE_ID}";
		logit "FHMS->talk() - BATCH - order id   : $self->{ORDER_ID}";
		logit "FHMS->talk() - BATCH - tran amt   : $self->{TXN_AMT}";
		logit "FHMS->talk() - BATCH - swipe      : $self->{PAYMENT_DATA}";


		# code deprecated to use once-a-day batching instead of realtime 
		# batch settlement 	T. Shannon	6/10/2003
 		#($result, $result_text) = pss_BatchImmediate ($self->{MACHINE_ID},
		#									$self->{ORDER_ID},
		#									$self->{TXN_AMT} * 100,
		#									$self->{CURRENCY},
		#									$self->{PAYMENT_DATA});
 		($result, $result_text) = pss_Batch ($self->{MACHINE_ID},
											$self->{ORDER_ID},
											$self->{TXN_AMT} * 100,
											$self->{CURRENCY},
											$self->{PAYMENT_DATA});
		$self->{RESPONSE_MESSAGE} = $result_text;

		logit "FHMS->talk() - BATCH - Result     : $result";
		logit "FHMS->talk() - BATCH - Response   : $result_text";

		if ($result eq '0')
		{
			$self->{ACTION_CODE} = 'A';
		}
		else
		{
			$self->{ACTION_CODE} = 'A';
		}
		return;
	}

# -- Force Batch --
	if ($self->{TRANSACTION_TYPE} eq $self->{FORCE})
	{
		logit "FHMS->talk() - FORCE - client     : $self->{MACHINE_ID}";
		logit "FHMS->talk() - FORCE - order id   : $self->{ORDER_ID}";
		logit "FHMS->talk() - FORCE - auth amt   : $self->{TXN_AMT}";
		logit "FHMS->talk() - FORCE - card swipe : $self->{PAYMENT_DATA}";

		# code deprecated to use once-a-day force batch routine	T. Shannon 6/10/2003
		#($result, $result_text) = pss_LocalAuthBatchImmediate ($self->{MACHINE_ID},
		#												$self->{ORDER_ID},
		#										       	$self->{TXN_AMT} * 100,
		#										       	$self->{PAYMENT_DATA},
		#										       	$self->{CURRENCY});

		($result, $result_text) = pss_LocalAuthBatch ($self->{MACHINE_ID},
														$self->{ORDER_ID},
												       	$self->{TXN_AMT} * 100,
												       	$self->{PAYMENT_DATA},
												       	$self->{CURRENCY});


		$self->{RESPONSE_MESSAGE} = $result_text;

		logit "FHMS->talk() - FORCE - Result     : $result";
		logit "FHMS->talk() - FORCE - Response   : $result_text";

		if ($result eq '0')
		{
			$self->{ACTION_CODE} = 'A';
		}
		else
		{
			$self->{ACTION_CODE} = 'E';
		}
		return;
	}
	
# -- Cancel --
	if ($self->{TRANSACTION_TYPE} eq $self->{CANCEL})
	{
		logit "FHMS->talk() - CANCEL - client     : $self->{MACHINE_ID}";
		logit "FHMS->talk() - CANCEL - order id   : $self->{ORDER_ID}";

		($result, $result_text) = pss_Cancel ($self->{MACHINE_ID}, $self->{ORDER_ID});

		$self->{RESPONSE_MESSAGE} = $result_text;

		logit "FHMS->talk() - CANCEL - Result     : $result";
		logit "FHMS->talk() - CANCEL - Response   : $result_text";

		if ($result eq '0')
		{
			$self->{ACTION_CODE} = 'A';
		}
		else
		{
			$self->{ACTION_CODE} = 'E';
		}
		return;
	}
}


sub make_auth_only 
{
	# invokes pss_Auth (Client ID, Client Trans Nbr, Auth Amount, Currency)
	
	my ($self, $magstrip, $auth_amt, $order_id, $use_track_2) = @_;

	$self->{TRANSACTION_TYPE} = $self->{AUTH_ONLY};
	$self->{ORDER_ID} = $order_id;
	$self->{PAYMENT_DATA} = $magstrip;
	$self->{AUTH_AMT} = $auth_amt;
	$self->{USE_TRACK_2} = $use_track_2;
}


sub make_sale 
{
	# invokes pss_Batch 
	
   my ($self, $swipe, $txn_amt, $order_id) = @_;
	#logit "FHMS->talk() - BATCH - swipe      : $swipe";
	#logit "FHMS->talk() - BATCH - order id   : $order_id";
	#logit "FHMS->talk() - BATCH - txn amt    : $txn_amt";
   $self->{TRANSACTION_TYPE} = $self->{SALE};
   $self->{ORDER_ID} = $order_id;
   $self->{TXN_AMT} = $txn_amt;
   $self->{PAYMENT_DATA} = $swipe;
	#logit "FHMS->talk() - BATCH - swipe      : $self->{PAYMENT_DATA}";
	#logit "FHMS->talk() - BATCH - order id   : $self->{ORDER_ID}";
	#logit "FHMS->talk() - BATCH - txn amt    : $self->{TXN_AMT}";
   
}

sub make_force
{
	# invokes pss_LocalAuthBatch  
	
   my ($self, $swipe, $txn_amt, $order_id) = @_;
   #warn "FHMS->make_force()\n";
	#logit "FHMS->talk() - FORCE - swipe      : $swipe";
	#logit "FHMS->talk() - FORCE - order id   : $order_id";
	#logit "FHMS->talk() - FORCE - txn amt    : $txn_amt";

   $self->{TRANSACTION_TYPE} = $self->{FORCE};
   $self->{ORDER_ID} = $order_id;
   $self->{TXN_AMT} = $txn_amt;
   $self->{PAYMENT_DATA} = $swipe;
   
}

sub make_cancel
{
	# invokes pss_Cancel  
	my ($self, $order_id) = @_;
	$self->{TRANSACTION_TYPE} = $self->{CANCEL};
	$self->{ORDER_ID} = $order_id;
}

sub read_auth_response 
{
	# no implementation at this time, but still exists to maintain a consistent interface for clients
}

sub logit
{
	my $line = shift;

	warn "[" . localtime() . "] $line\n";
}

END { }

} # BEGIN

1;
