#!/usr/bin/perl -w
require 5.004;

use strict;
use Evend::Database::Database;
#use CardUtils;
use IO::Socket;
use IO::Select;

sub logit;

my $DATABASE = Evend::Database::Database->new(	print_query => 1, 
												execute_flatfile=>1, 
												debug => 1, 
												debug_die => 0);
					
# create a cursor of settled PSS trans and update the tazdba database
my $where_value = 'SETTLE_FAIL';
my $pss = $DATABASE->select(
				table			=> 'pss@pss_tulsap1',
				select_columns	=> 'client_tran_id',
				where_columns	=> [ 'last_state_change_date > sysdate - 1','state = ?'],
				where_values	=> [ $where_value  ]);

# Because of "Quick" PSS limitations we need to provide
# a KODAK terminal number for the process to initiate
# T. Shannon 6/22/2003
	
if(!($pss->[0]))
{
	logit "There were no PSS transactions in the $where_value state to update in tazdba.";
}
else
{
	logit "Now processing PSS $where_value transactions...";
	
	my $i = 0;
	foreach my $pssrow (@$pss)
	{
		my $order_number = @$pssrow;
		logit "Item # $i : Order Number $pssrow->[0]";

		# update the TRANSACTION_RECORD_HIST table to reflect the status in the PSS table
		$DATABASE->update(
			table			=> 'transaction_record_hist',
			update_columns	=> 'transaction_status',
			update_values	=> ['SF'],
			where_columns	=> ['trans_no = ?'],
			where_values	=> [$pssrow->[0]]
		);
		
		$i++;
	}
}

# create a cursor of settled PSS trans and update the tazdba database
$where_value = 'SETTLE_SUCCESS';
$pss = $DATABASE->select(
					table			=> 'pss@pss_tulsap1',
					select_columns	=> 'client_tran_id',
					where_columns	=> [ 'last_state_change_date > sysdate - 1','state = ?'],
					where_values	=> [ $where_value  ]);

# Because of "Quick" PSS limitations we need to provide
# a KODAK terminal number for the process to initiate
# T. Shannon 6/22/2003
	
if(!($pss->[0]) )
{
	logit "There were no PSS transactions in the $where_value state to update in tazdba.";
}
else
{
	logit "Now processing PSS $where_value transactions...";
	
	my $i = 0;
	foreach my $pssrow (@$pss)
	{
		my $order_number = @$pssrow;
		logit "Item # $i : Order Number $pssrow->[0]";

		# update the TRANSACTION_RECORD_HIST table to reflect the status in the PSS table
		$DATABASE->update(
			table			=> 'transaction_record_hist',
			update_columns	=> 'transaction_status',
			update_values	=> ['SS'],
			where_columns	=> ['trans_no = ?'],
			where_values	=> [$pssrow->[0]]
		);

		$i++;
	}
}

sub logit
{
	my $line = shift;
	warn "[" . localtime() . "] $line\n";
}

