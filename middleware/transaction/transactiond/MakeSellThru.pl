#!/usr/bin/perl

use strict;
use Evend::Database::Database;

my $dbase_oracle = new Evend::Database::Database(print_query=>1); 
$|=1; # set autoflush
                        

     my @machine_list = (
  #                       '003L','003R','0041','0042',
  #                       '0043','0044','004B','0045',
  #                       '0046','0047','0048','0049','004A',
                         '002U','0029','0006','CCU1867','CCU1875',
                         '002X','000V','EV11080','CCU2248',
                         '0024','000N','002R','0022',
                         '001S','001P','000G','003Q','003N',
                         '003D','003E','003F','003G','003H',
                         '003J','003V','003W','003X','003Y',
                         '003Z','0040','002Z','002S','0014',
                         '0027','0008','0000','001D','000M',
                         '001H',
						'CCU1682',
						'CCU1532',

						# rolling meadows route
						'004B', '0032', '0033', '0034', '0035', '0036',
						'0037', '0038', '0039', '003A', '003B', '003C',
						'004C',

						# SCUs also need an overloaded $dialedin{machine_id}
#						'EV13526',
							# Olympic SCUs
#							'EV13556', 'EV13660', 'EV13673',
#							'EV13662', 'EV13664', 'EV13666', 'EV13668',
#							'EV13670', 'EV13672', 'EV13658', 'EV13653',
#							'EV13496', 'EV13554', 'EV13558', 'EV13656',
#							'EV13676', 'EV13245', 'EV13229', 'EV13223',

						#temp for price elasticity fix
#							'EV11087',
						);

#  removed 002T
# my @machine_list =  ('');
# my @maytag_list = ('');
     my @maytag_list  = ('CCU1100','CCU1136','CCU1143','CCU1144','CCU1145', #--DRYERS
                       'CCU1140','CCU1141','CCU1142','CCU1148','CCU1149', #---washers
                       'CCU1151','CCU1152','CCU1153','CCU1154','CCU2398','CCU2399','CCU1150');

  my @kdkdk_list = ('CCUKDK',
                        'CCU2109',
                        'CCU2110',
                        'CCU2111',
                        'CCU2112',
                        'CCU2113',
                        'CCU2114',
                        'CCU2115',
                        'CCU2116',
                       'CCU2275','CCU2272', 
                        'CCU2117');

  my $yesterday = $dbase_oracle->select(table=>"DUAL",
    select_columns=>"to_char(YESTERDAY, 'MM/DD/YYYY'), to_char(TODAY, 'MM/DD/YYYY')",
  );
  my $today     = $yesterday->[0][1];
  $yesterday = $yesterday->[0][0];

  my $string = join("\',\'", @machine_list);

  my $string2 = join("\',\'", @maytag_list);
  
  my $string3 = join("\',\'", @kdkdk_list);

  $string = $string."','".$string2."','".$string3;

  print $string."\n";


  my $checkStatus      = $dbase_oracle->select(table   => "Machine_Status_Hist",
                            select_columns=> "Machine_Id",
                            where_columns => ["Machine_Id in (\'$string\')", "Dialup_Date >= YESTERDAY","Dialup_Date <= TODAY"],
                          );



  my %dialedIn;
  $dialedIn{$_->[0]} = 1 for @$checkStatus;
  

	# SCUs need to always be set to have dialed in

	$dialedIn{'EV13526'} = 1;
	$dialedIn{'EV13556'} = 1;
	$dialedIn{'EV13660'} = 1;
	$dialedIn{'EV13673'} = 1;
	$dialedIn{'EV13662'} = 1;
	$dialedIn{'EV13664'} = 1;
	$dialedIn{'EV13666'} = 1;
	$dialedIn{'EV13668'} = 1;
	$dialedIn{'EV13670'} = 1;
	$dialedIn{'EV13672'} = 1;
	$dialedIn{'EV13658'} = 1;
	$dialedIn{'EV13653'} = 1;
	$dialedIn{'EV13496'} = 1;
	$dialedIn{'EV13554'} = 1;
	$dialedIn{'EV13558'} = 1;
	$dialedIn{'EV13656'} = 1;
	$dialedIn{'EV13676'} = 1;
	$dialedIn{'EV13245'} = 1;
	$dialedIn{'EV13229'} = 1;
	$dialedIn{'EV13223'} = 1;
  
  foreach my $machine (@machine_list) {

     my %sellthru;

     # I need to pass a ref to the SellThru hash that looks like...
     # Sellthru_Hash
     # --------------
     # 0) button id
     # 1) sell thru
     # 2) sales (in pennies)
     # 3) sold out
     # 4) inv num
     # 5) price

     # step 1) collect sellthru info for each machine



     # get num buttons

     my $machine_type_rec      = $dbase_oracle->select(table      => 'Rix_Machine A, rix_Machine_Type B',
                                               select_columns     => 'b.Column_Translation_Type',
                                                where_columns     => ['a.Machine_Type_Number = b.Machine_Type_Number', 'a.machine_id= ?'],
                                                where_values      => [$machine]);

     my $column_type  = $machine_type_rec->[0][0];
     print "column type is $column_type\n";

     # get inventory hash
     my $rerix_inventory = $dbase_oracle->select( table        => 'Rerix_Inventory A, TABLE(a.Inventory) B',
                                                  select_columns => 'b.button_id, b.inventory_item_number',
                                                  where_columns  => ['a.machine_id = ?'],
                                                  where_values   => [$machine] );
     # get price hash
     my $rerix_prices  = $dbase_oracle->select( table       => 'Rerix_Pricing A, TABLE(a.Rerix_Price) B',
                                               select_columns => 'distinct(b.button_id), b.price',
                                               where_columns  => ['a.machine_id = ?'],
                                               where_values   => [$machine] );


     # my $num_buttons = scalar @$rerix_inventory;
     my $num_buttons = scalar @$rerix_prices;
     print "num buttons is $num_buttons\n";
     # print "num buttons2 is $num_buttons2\n";


     # create inventory hash and price hash
     my (%price, %inventory, %button);
     if ($column_type == 0) { # NO TRANSLATION - regular machine
        #print "no translation required\n";

        for (my $i = 0; $i < $num_buttons; $i++) {
           $price{$i}->[0] = $i+1;
           $price{$i}->[1] = $rerix_prices->[$i][1];

           $inventory{$i}->[0] = $i+1;
           $inventory{$i}->[1] = $rerix_inventory->[$i][1];

           # button hash is used to translate buttoon id's to index. this allows us
           # to pick out the index within inv and price hashes for a particular button id
           $button{$i+1} =  $i;

        }
     }
     elsif ($column_type == 1) { # APC machine

        for (my $i = 0; $i < $num_buttons; $i++) {
           $price{$i}->[0] = $rerix_prices->[$i][0];
           $price{$i}->[1] = $rerix_prices->[$i][1];

           $inventory{$i}->[0] = $rerix_inventory->[$i][0];
           $inventory{$i}->[1] = $rerix_inventory->[$i][1];

           $button{$rerix_inventory->[$i][0]} = $i;
        }

        #print "button hash is...\n";
        #foreach (keys %button) {
        #   print "$_: $button{$_}\n";
        #}
     }
     elsif ($column_type == 2) { # CRANE machine
     }

     # cycle thru all txns and fill out sellthru hash

     my $txns = $dbase_oracle->select( table          => 'TRANS_DTL_RPT',
                                       select_columns => 'Trans_Date,
                                                          Button_Id,
                                                          Transaction_Amt',
                                       where_columns  => ['Machine_Id = ?', "Sell_Thru = '0'"],
                                       where_values   => [$machine] );

     print "got ", scalar(@$txns), " txns for machine $machine\n";



     foreach my $txn (@$txns) {
        my ($txn_date, $button_id, $txn_amt) = @$txn;

        my $button_index;
        if ($column_type == 0) {
           $button_index = $button{$button_id+1};
           $sellthru{$txn_date}->[$button_index][0]  = $button_id+1; # button id
        }
        elsif ($column_type == 1) {
           $button_index = $button{$button_id};
           $sellthru{$txn_date}->[$button_index][0]  = $button_id; # button id
        }
        $sellthru{$txn_date}->[$button_index][1] += 1; # sell thru
        $sellthru{$txn_date}->[$button_index][2] += $txn_amt*100; # sales (in pennies)
        $sellthru{$txn_date}->[$button_index][3]  = 0; # sold out
        $sellthru{$txn_date}->[$button_index][4]  = $inventory{$button_index}->[1]; # inv num
        $sellthru{$txn_date}->[$button_index][5]  = $price{$button_index}->[1]; # price
     }

	if ($dialedIn{$machine})
	{
		print "Dialed in\n";

		if (scalar(keys(%sellthru)) == 0)
		{
			# create ZERO-record if no transactions...

			$sellthru{$yesterday} = undef;
		}
	}

	print "Zeroing\n";

	foreach my $date (keys %sellthru)
	{
		print "Looking at $date, $num_buttons\n";

		for (	my $button_index = 0;
				$button_index < $num_buttons;
				$button_index++)
		{
			if (!defined $sellthru{$date}->[$button_index])
			{
				print "zeroing $date, $button_index\n";

				$sellthru{$date}->[$button_index][0]  = $price{$button_index}->[0]; # button id
				$sellthru{$date}->[$button_index][1]  = 0; # sell thru
				$sellthru{$date}->[$button_index][2]  = 0; # sales (in pennies)
				$sellthru{$date}->[$button_index][3]  = 0; # sold out
				$sellthru{$date}->[$button_index][4]  = $inventory{$button_index}->[1]; # inv num
				$sellthru{$date}->[$button_index][5]  = $price{$button_index}->[1]; # price
			}
		}
	}


     my $count = 0;

     foreach my $date (keys %sellthru) {
        print "DATE1: $date\n";

        foreach my $button (@{$sellthru{$date}}) {
           print "$button\t$button->[0]\t$button->[1]\t$button->[2]\t$button->[3]\t$button->[4]\t$button->[5]\n";
        }
        $count++;
     }

  #   print "num insertions into sellthru is ", $count, "\n";
  #   print "size is ", scalar(@$txns), "\n";

       foreach my $date (keys %sellthru) {
          $dbase_oracle->insert(table      => 'Rerix_Sell_Through',
                         insert_columns    => 'machine_id, SALES_DATE, BASELINE, SELL_THROUGH',
                         insert_values     => [$machine, $date, 'N', $sellthru{$date}] );
       }

       $dbase_oracle->update(  table          => 'TRANS_DTL_RPT',
                               update_columns => 'Sell_Thru',
                               update_values  => ['1'],
                               where_columns  => ['Machine_Id = ?', "Sell_Thru = '0'"],
                               where_values   => [$machine] );

  }



foreach my $machine (@maytag_list) {

   my %sellthru;

   # I need to pass a ref to the SellThru hash that looks like...
   # Sellthru_Hash
   # --------------
   # 0) button id
   # 1) sell thru
   # 2) sales (in pennies)
   # 3) sold out
   # 4) inv num
   # 5) price

   # step 1) collect sellthru info for each machine

   # get num buttons 
   my $machine_rec    = $dbase_oracle->select(table	=> 'Rix_Machine',
                                     select_columns     => 'Machine_Type_Number',
                                     where_columns      => ['Machine_ID = ?'],
                                     where_values       => [$machine] );
                                        

   my $machine_type_rec      = $dbase_oracle->select(table	=> 'Rix_Machine_Type',
                                             select_columns     => 'Column_Translation_Type',
                                              where_columns     => ['Machine_Type_Number = ?'],
                                              where_values      => [$machine_rec->[0][0]] );

   my $column_type  = $machine_type_rec->[0][0];
   print "column type is $column_type\n";

   # get inventory hash
   my $rerix_inventory = $dbase_oracle->select( table        => 'Rerix_Inventory A, TABLE(a.Inventory) B',
                                                select_columns => 'b.button_id, b.inventory_item_number',
                                                where_columns  => ['a.machine_id = ?'],
                                                where_values   => [$machine] );
   # get price hash
   my $rerix_prices  = $dbase_oracle->select( table       => 'Rerix_Pricing A, TABLE(a.Rerix_Price) B',
                                             select_columns => 'distinct(b.button_id), b.price',
                                             where_columns  => ['a.machine_id = ?'],
                                             where_values   => [$machine] );


   # my $num_buttons = scalar @$rerix_inventory;
   my $num_buttons = scalar @$rerix_prices;
   print "num buttons is $num_buttons\n";
   # print "num buttons2 is $num_buttons2\n";


	# create inventory hash and price hash
	my (%price, %inventory, %button);
	for (my $i = 0; $i < $num_buttons; $i++)
	{
		$price{$i}->[0] = $i+1;
		$price{$i}->[1] = $rerix_prices->[$i][1];
   
		$inventory{$i}->[0] = $i+1;
		$inventory{$i}->[1] = $rerix_inventory->[$i][1];

		# button hash is used to translate buttoon id's to index. this allows
		# us to pick out the index within inv and price hashes for a
		# particular button id
		$button{$i+1} =  $i;
	}
   
   # cycle thru all txns and fill out sellthru hash
   my $txns = $dbase_oracle->select( table          => 'TRANS_DTL_RPT',
                                     select_columns => 'Trans_Date, Column_Id,Transaction_Amt, Inventory_Number',
                                     where_columns  => ['Machine_Id = ?', "Sell_Thru = '0'", "Transaction_Amt > 0"],
                                     where_values   => [$machine] );

   print "got ", scalar(@$txns), " txns for machine $machine\n";



	foreach my $txn (@$txns)
	{
		my ($txn_date, $button_id, $txn_amt, $inv_number) = @$txn;

		my (%wash_hash, $row);
		my $button_index;
		$button_index = $button{$button_id};
		#---Dryer button id = 0,1--#
		#---Wash  button id = 2,3--#
		if($inv_number eq "INV471")
		{
			#--this is a wash cycle--#
			my $diff = $txn_amt - 1.25;
			if ($diff != 0) 
			{
				$row=1;
				#this is not a dry and is more than a wash.
				$row++;
				$wash_hash{$row}[0] = $price{1}->[1];           #price
				$wash_hash{$row}[1] = $inventory{1}->[1]; #inv_num
				$wash_hash{$row}[2] = 1;
				if ($diff == 0.50)
				{  #==TIDE==#
					$row++;
					$wash_hash{$row}[0] = $price{2}->[1];
					$wash_hash{$row}[1] = $inventory{2}->[1];#"Tide Detergent";
					$wash_hash{$row}[2] = 2;
				}
				elsif ($diff == 0.25)
				{  #==DOWNY==#
					$row++;
					$wash_hash{$row}[0] = $price{3}->[1];
					$wash_hash{$row}[1] = $inventory{3}->[1];#"Downy Fabric Softener";
					$wash_hash{$row}[2] = 3;
				}
				elsif ($diff == 0.75)
				{  #==BOTH==#
					$row++;
					$wash_hash{$row}[0] = $price{2}->[1];
					$wash_hash{$row}[1] = $inventory{2}->[1];#"Tide Detergent";
					$wash_hash{$row}[2] = 2;
					$row++;
					$wash_hash{$row}[0] = $price{3}->[1];
					$wash_hash{$row}[1] = $inventory{3}->[1];#"Downy Fabric Softener";
					$wash_hash{$row}[2] = 3;
				}
			}
			else
			{
				$row= 2;
				$wash_hash{$row}[0] = $price{0}->[1];
				$wash_hash{$row}[1] = $inventory{0}->[1];
				$wash_hash{$row}[2] = 1;
			}

			foreach my $rowid (sort keys %wash_hash)
			{
				$sellthru{$txn_date}->[($wash_hash{$rowid}[2])][0] =
										($wash_hash{$rowid}[2]+1); # button id
				$sellthru{$txn_date}->[($wash_hash{$rowid}[2])][1] += 1;
																	# sell thru
				$sellthru{$txn_date}->[($wash_hash{$rowid}[2])][2] += ($wash_hash{$rowid}[0]); # sales (in pennies)
				$sellthru{$txn_date}->[($wash_hash{$rowid}[2])][3]  = 0; # sold out
				$sellthru{$txn_date}->[($wash_hash{$rowid}[2])][4]  = $inventory{($wash_hash{$rowid}[2])}->[1]; # price
				$sellthru{$txn_date}->[($wash_hash{$rowid}[2])][5]  = $price{($wash_hash{$rowid}[2])}->[1]; # inv num
			}
		}
		else
		{
			#--dryer cycle--#
			$sellthru{$txn_date}->[$button_index][0]  = $button_id; # button id
			$sellthru{$txn_date}->[$button_index][1] += 1; # sell thru
			$sellthru{$txn_date}->[$button_index][2] += $price{$button_index}->[1]; # sales (in pennies)
			$sellthru{$txn_date}->[$button_index][3]  = 0; # sold out
			$sellthru{$txn_date}->[$button_index][4]  = $inventory{$button_index}->[1]; # inv num
			$sellthru{$txn_date}->[$button_index][5]  = $price{$button_index}->[1]; # price
		}
	}

	if ($dialedIn{$machine})
	{
		print "Dialed in\n";
		if (scalar(keys(%sellthru)) == 0)
		{
			# create ZERO-record if no transactions...
			print "Adding zero record for yesterday, $yesterday\n";
			$sellthru{$yesterday} = undef;
		}
	}

	# fill in info for zero columns...
	foreach my $date (keys %sellthru)
	{
		for (my $button_index = 0;
				$button_index < $num_buttons;
				$button_index++)
		{
			if (!defined($sellthru{$date}->[$button_index]))
			{
				$sellthru{$date}->[$button_index][0]  = $price{$button_index}->[0]; # button id
				$sellthru{$date}->[$button_index][1]  = 0; # sell thru
				$sellthru{$date}->[$button_index][2]  = 0; # sales (in pennies)
				$sellthru{$date}->[$button_index][3]  = 0; # sold out
				$sellthru{$date}->[$button_index][4]  = $inventory{$button_index}->[1]; # inv num
				$sellthru{$date}->[$button_index][5]  = $price{$button_index}->[1]; # price
			}
		}
	}

	my $count = 0;

	foreach my $date (keys %sellthru)
	{
		print "DATE2: $date\n";
      
		foreach my $button (@{$sellthru{$date}})
		{
			print "$button->[0]\t$button->[1]\t$button->[2]\t$button->[3]\t$button->[4]\t$button->[5]\n";
		}
		$count++;
	}

	# print "num insertions into seelthru is ", $count, "\n";
	# print "size is ", scalar(@$txns), "\n";

	foreach my $date (keys %sellthru)
	{
		$dbase_oracle->insert(table      => 'Rerix_Sell_Through',
							insert_columns    => 'machine_id, SALES_DATE, BASELINE, SELL_THROUGH',
							insert_values     => [$machine, $date, 'N', $sellthru{$date}] );
	}

	$dbase_oracle->update(  table          => 'TRANS_DTL_RPT',
							update_columns => 'Sell_Thru',
							update_values  => ['1'],
							where_columns  => ['Machine_Id = ?',
												"Sell_Thru = '0'"],
							where_values   => [$machine] );
}


#--------------------------------------------------------------------------------------------------#
# KOdak Digital Kiosk thingy
#--------------------------------------------------------------------------------------------------#

foreach my $machine (@kdkdk_list)
{
   #do something
   if($dialedIn{$machine})
   {
      print $machine."\n";
      my %sellthru;
   
    
      my $txns = $dbase_oracle->select( table          => 'TRANS_DTL_RPT',
                                        select_columns => 'Trans_Date,
                                                           Button_Id,
                                                           Transaction_Amt,
                                                           Inventory_Number',
                                        where_columns  => ['Machine_Id = ?', "Sell_Thru = '0'"],
                                        where_values   => [$machine] );
   
      print "got ", scalar(@$txns), " txns for machine $machine\n";
   
   
   
      foreach my $txn (@$txns) 
      {
         my ($txn_date, $button_id, $txn_amt, $inv_num) = @$txn;
         $sellthru{$txn_date}{$txn_amt}[0][0]= $button_id;
         $sellthru{$txn_date}{$txn_amt}[0][1]++;
         $sellthru{$txn_date}{$txn_amt}[0][2]= $txn_amt *100;
         $sellthru{$txn_date}{$txn_amt}[0][3]= 0;
         $sellthru{$txn_date}{$txn_amt}[0][4]= $inv_num;
         $sellthru{$txn_date}{$txn_amt}[0][5]= $txn_amt *100;
      }
   
      
   
   
      my $count = 0;
   
     if (scalar keys %sellthru) {
      foreach my $date (keys %sellthru) 
      {
         my $buttonid =0;
         my %insert_hash;
         foreach my $txnamt (keys %{$sellthru{$date}})
         {
   
           $insert_hash{$date}->[$buttonid][0] = $buttonid;
           $insert_hash{$date}->[$buttonid][1] = $sellthru{$date}{$txnamt}[0][1]; #--sellthru--#
           $insert_hash{$date}->[$buttonid][2] = ($sellthru{$date}{$txnamt}[0][2] * $sellthru{$date}{$txnamt}[0][1]); #--total sales--#
           $insert_hash{$date}->[$buttonid][3] = 0;#--sold out flag--#
           $insert_hash{$date}->[$buttonid][4] = $sellthru{$date}{$txnamt}[0][4];
           $insert_hash{$date}->[$buttonid][5] = $sellthru{$date}{$txnamt}[0][5];
   
   
           
   
           $buttonid++;
         }
   
         for (my $i=0; $i<$buttonid; $i++) 
         {
            print qq($date : $insert_hash{$date}->[$i][0] $insert_hash{$date}->[$i][1] $insert_hash{$date}->[$i][2] $insert_hash{$date}->[$i][3] $insert_hash{$date}->[$i][4] $insert_hash{$date}->[$i][5]\n);
         }
         
          $dbase_oracle->insert(table      => 'Rerix_Sell_Through',
                insert_columns    => 'machine_id, SALES_DATE, BASELINE, SELL_THROUGH',
                insert_values     => [$machine, $date, 'N', $insert_hash{$date}] );
   
   
   
         $count++;
      }

   
   
   
        $dbase_oracle->update(  table          => 'TRANS_DTL_RPT',
                             update_columns => 'Sell_Thru',
                             update_values  => ['1'],
                             where_columns  => ['Machine_Id = ?', "Sell_Thru = '0'"],
                             where_values   => [$machine] );
   
    }
    else
    {
    my $date = $dbase_oracle->select(table=>"Dual",
                          select_columns=>"YESTERDAY",
                          );

    #---make a zero record for this machine--#
    my %insert_hash;
    $insert_hash{$date->[0][0]}->[0][0] = 0;
    $insert_hash{$date->[0][0]}->[0][1] = 0;
    $insert_hash{$date->[0][0]}->[0][2] = 0;
    $insert_hash{$date->[0][0]}->[0][3] = 0;
    $insert_hash{$date->[0][0]}->[0][4] = 'INV336';
    $insert_hash{$date->[0][0]}->[0][5] = '300';


    $dbase_oracle->insert(table => 'Rerix_Sell_Through',
                   insert_columns=>'machine_id, Sales_Date, Baseline, Sell_Through',
                   insert_values=>[$machine, $date->[0][0], 'N', $insert_hash{$date->[0][0]}]);
    }

 }


}









$dbase_oracle->close();
