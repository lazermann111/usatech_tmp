#!/usr/bin/perl

use strict;

use Evend::Database::Database;
use CardUtils;
use IO::Socket;
use IO::Select;

require 'pss_interface.pm';

my $DATABASE = Evend::Database::Database->new(	print_query => 0, 
												execute_flatfile=>0, 
												debug => 0, 
												debug_die => 0);

my $min_settle_hours 			= 22;
my $min_settle_transactions 	= 500;

my $merchants_stmt = $DATABASE->select(
		table			=> 'merchant@pss_tulsap1',
		select_columns	=> 'client_type, merchant_desc' );

foreach(@$merchants_stmt)
{
	my ($client_type, $merchant_desc) = ($_->[0], $_->[1]);
	
	output("--- PROCESSING: $merchant_desc ($client_type)");
	
	my $client_stmt = $DATABASE->select(
						table			=> 'quick_pss_merchant@pss_tulsap1',
						select_columns	=> 'max(client_id)',
						where_columns	=> ['client_type = ?'],
						where_values	=> [$client_type] );
	
	if(!($client_stmt->[0][0]))
	{
		output("Cannot find a client_id for $client_type: Settlement not proceeding!\n");
		next;
	}
	
	my $client_id = $client_stmt->[0][0];
	output("Using $client_id for $client_type");
	
	my ($result, $result_text) = &pss_BatchSettleConditional($client_id,'',$min_settle_hours,$min_settle_transactions);	
	
	output("$client_type BATCH_SETTLE - Result     : $result");
	output("$client_type BATCH_SETTLE - Response   : $result_text\n");
}

sub output
{
	my $line = shift;
	print "[" . localtime() . "] $line\n";
}

