package DatabaseConfig;

use strict;
use USAT::Database::Config;
use vars qw(@ISA);
@ISA = qw(USAT::Database::Config);

sub new {
	my $type = shift;
	my $self = USAT::Database::Config->new(
		debug 				=> 1, 
		debug_die 			=> 0, 
		print_query 		=> 1,
		retries				=> 1,
		connection_lifetime	=> 60 * 60 * 24,
		@_
	);
	bless $self, __PACKAGE__;
	return $self;
}

1;
