#!/usr/local/bin/perl
$VERSION = 1.000003;	#[major].[nnn minor][nnn maintenance]

use strict;
use warnings;

use Getopt::Long qw(VersionMessage);
use Pod::Usage;

use Time::HiRes qw(time);
use IO::Poll;
use POE::strict qw(
	Component::SimpleLog
);
use USAT::POE::Component::Server::SOAP;
use USAT::SOAP::Util;
use USAT::App::ECCS::Config;
use USAT::App::ECCS::Gateway;
use USAT::App::ECCS::SOAP::ComplexType::hostItem;
use USAT::App::ECCS::SOAP::ComplexType::ResponseMessage;

#use Cache::BerkeleyDB;
use Cache::FileCache;	#switch to BerkeleyDB if performance is slow
use USAT::Database;
use Data::Dumper;

use lib LIBDIR;
use DatabaseConfig;

use lib LIBDIR_LOGIC_ENGINE;
use ReRix::ESuds;

use constant {
	SESSION__MAIN			=> 'ECCS',
	SESSION__SIMPLELOG		=> 'MyLogger',
	SESSION__SERVER_SOAP	=> 'MySOAP',
	SOAP_SERVER_VERSION	=> 1.1,
	SQL__GET_HOST_PARAM	=> q{
		SELECT device_id, host_port_num, host_position_num
		FROM host
		WHERE host_id = ?
	}
};

use constant {
	EXCEPTION__INVALID_REQUEST_PARAMETER	=> -100,
	SOAP_SUCCESS 							=> 0,
	EXCEPTION__UNKNOWN_ERROR				=> 1
};

use File::Pid;
my $pidfile = File::Pid->new({file => EXECDIR.'/'.PIDFILE});
die "ECCS already running: ".$pidfile->pid." (".$pidfile->file.")\n" if $pidfile->running;
$pidfile->write;

########################################################################
package My::SOAP::ComplexType::hostItemRetry;
use strict;
use warnings;
use USAT::App::ECCS::SOAP::ComplexType::hostItem;
use vars qw(@ISA);
@ISA = qw(USAT::App::ECCS::SOAP::ComplexType::hostItem);

use constant OBJ_URI 	=> USAT::App::ECCS::SOAP::ComplexType::hostItem::OBJ_URI;
use constant OBJ_TYPE	=> 'SOAP-ENV:hostItemRetry';
use constant OBJ_FIELDS	=> {
	retryAttempts	=> ['int', undef, undef, {required => 1}]
};

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;
	my $data = shift;
	my $obj_fields = shift;
	$obj_fields = defined $obj_fields && ref($obj_fields) eq 'HASH' ? {%{+OBJ_FIELDS}, %{$obj_fields}} : OBJ_FIELDS;
	my $self = $class->SUPER::new($data, $obj_fields);
	return bless($self, $class);
}

########################################################################
# Main program
########################################################################
package main;
sub main {
	my $oldfh = select(STDOUT); $| = 1; select(STDERR); $| = 1; select($oldfh);	#force auto-flush; otherwise, lines may be out of order
	
	### handle arguments ###
	my ($version, $help);
	Getopt::Long::Configure(qw(no_ignore_case));
	GetOptions(
		'help'				=> \$help,
		'version'			=> \$version,
	) || pod2usage(1);

	### handle version or help ###
	if ($version) {
	  VersionMessage(
		-exitval => 'NOEXIT',
		-verbose => 1,
	  );
	  pod2usage(
		-exitval => 0,
		-sections => 'COPYRIGHT',
		-verbose => 99,
	  );
	}
	pod2usage(-exitval => 0, -verbose => 1) if $help;

	### init components ###
	POE::Component::SimpleLog->new(
		'ALIAS'			=> SESSION__SIMPLELOG,
		'PRECISION'		=> 1
	) or die "Failed to create a POE::Component::SimpleLog session\n";

	USAT::POE::Component::Server::SOAP->new(
		'ALIAS'			=> SESSION__SERVER_SOAP,
		'ADDRESS'		=> SOAP_SERVER_LISTEN_LOCALHOST_ONLY ? 'localhost' : 0,
		'PORT'			=> SOAP_SERVER_PORT,
		'SOAPVERSION'	=> SOAP_SERVER_VERSION
	) or die "Failed to create a USAT::POE::Component::Server::SOAP session\n";

	### init main session ###
	POE::Session->create(
		inline_states	=> {
			### system methods ###
			'_start'	=> \&service_start,
			'_stop'		=> \&service_stop,
			
			'sigint'						=> \&sigint,
			'shutdown_service'				=> \&shutdown_service,
			'shutdown_service_immediatly'	=> \&shutdown_service_immediatly,
			
			### SOAP server methods (keys of %USAT::App::ECCS::Gateway::methods) ###
			'schedule'			=> \&soap_do_schedule_hosts,
			'scheduleCancel'	=> \&soap_do_schedule_cancel_hosts,
			'shutdownService'	=> \&soap_do_shutdown_service,
			
			### internal app methods ###
			'do_schedule_host'				=> \&do_schedule_host,
			'do_schedule_cancel_host'		=> \&do_schedule_cancel_host,
			'handle_host_cycle_complete'	=> \&handle_host_cycle_complete,
			
			### misc methods ###
			'_validate_soap_schedule_input'	=> \&_validate_soap_schedule_input,
			'_do_soap_response'				=> \&_do_soap_response,
			'do_log'						=> \&do_log,
		},
	);

	$poe_kernel->run;
	exit 0;
}

########################################################################
# SOAP Server Methods
########################################################################
sub soap_do_schedule_hosts {
	my ($kernel, $heap, $response) = @_[KERNEL, HEAP, ARG0];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	my $obj = $kernel->call( SESSION__MAIN, '_validate_soap_schedule_input', $response );
	return undef unless defined $obj;

	$kernel->post( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'Found 1 host event to schedule.' );

	### schedule events ###
	$kernel->yield( 'do_schedule_host', $obj, $response );
}

sub soap_do_schedule_cancel_hosts {
	my ($kernel, $heap, $response) = @_[KERNEL, HEAP, ARG0];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	my $obj = $kernel->call( SESSION__MAIN, '_validate_soap_schedule_input', $response );
	return undef unless defined $obj;

	$kernel->post( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'Received request to cancel host event.' );

	### cancel events ###
	$kernel->yield( 'do_schedule_cancel_host', $obj, $response );
}

sub soap_do_shutdown_service {
	my ($kernel, $heap, $response) = @_[KERNEL, HEAP, ARG0];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	$kernel->yield( 'shutdown_service' );
	$kernel->yield( '_do_soap_response', $response, SOAP_SUCCESS, "Server has been scheduled to shutdown." );
}

########################################################################
# Internal App Methods
########################################################################
sub do_schedule_host {
	my ($kernel, $heap, $obj, $response) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	### validate object ###
	unless (UNIVERSAL::isa($obj, 'USAT::App::ECCS::SOAP::ComplexType::hostItem')) {
		$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'Invalid object: '.ref($obj) );
		return undef;
	}
	print Dumper($obj) if DEBUG;
	
	### validate timestamp ###
	my $cycle_start_epoch_ts = USAT::SOAP::Util::convert_soap_datetime_to_epoch_sec($obj->cycleStartTime);
	unless (defined $cycle_start_epoch_ts) {
		my $result = 'Invalid input time: '.$obj->cycleStartTime;
		$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', $result );
		$kernel->yield( '_do_soap_response', $response, EXCEPTION__UNKNOWN_ERROR, $result ) if defined $response;
		return undef;
	}
	
	### set alarm and save object if event has not already expired ###
	my $expires_epoch_ts = $obj->isa('My::SOAP::ComplexType::hostItemRetry') && time <= ($cycle_start_epoch_ts + EVENT_RESCHEDULE_MAX_SEC)
		? time + EVENT_RESCHEDULE_INTERVAL_SEC
		: $cycle_start_epoch_ts + ($obj->estCycleMin * 60);
	if (time < $expires_epoch_ts) {
		if (my $alarm_id = $kernel->alarm_set( 'handle_host_cycle_complete', $expires_epoch_ts, $obj )) {
			### remove old alarm, if it exists ###
			$kernel->alarm_remove( $heap->{host_alarms}->{$obj->hostId} ) if defined $heap->{host_alarms}->{$obj->hostId};
			
			### set new alarm and save object ###
			$heap->{host_alarms}->{$obj->hostId} = $alarm_id;
			$heap->{host_objects}->set($obj->hostId, $obj, $expires_epoch_ts);
			
			### log event and send optional response ###
			my $result = "Alarm set for hostId ".$obj->hostId.": Cycle complete @ ".localtime($expires_epoch_ts);
			$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', $result );
			$kernel->yield( '_do_soap_response', $response, SOAP_SUCCESS, $result ) if defined $response;
		}
		else {
			my $result = "Error setting alarm for hostId ".$obj->hostId.": $!";
			$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', $result );
			$kernel->yield( '_do_soap_response', $response, EXCEPTION__UNKNOWN_ERROR, $result ) if defined $response;
		}
	}
	else {
		my $result = "Alarm NOT set for host ".$obj->hostId.": Time ".localtime($expires_epoch_ts)." already expired";
		$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', $result );
		$kernel->yield( '_do_soap_response', $response, EXCEPTION__UNKNOWN_ERROR, $result ) if defined $response;
		$heap->{host_objects}->remove($obj->hostId);	#remove object, if it exists
	}
}

sub do_schedule_cancel_host {
	my ($kernel, $heap, $obj, $response) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	### validate object ###
	unless (UNIVERSAL::isa($obj, 'USAT::App::ECCS::SOAP::ComplexType::hostItem')) {
		my $result = 'Invalid object: '.ref($obj);
		$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', $result );
		$kernel->yield( '_do_soap_response', $response, EXCEPTION__UNKNOWN_ERROR, $result ) if defined $response;
		return undef;
	}
	
	### remove alarm and object ###
	$heap->{host_objects}->remove($obj->hostId);
	if (defined $heap->{host_alarms}->{$obj->hostId}) {
		$kernel->alarm_remove( $heap->{host_alarms}->{$obj->hostId} );
		my $result = 'Removed alarm for hostId '.$obj->hostId;
		$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', $result );
		$kernel->yield( '_do_soap_response', $response, SOAP_SUCCESS, $result ) if defined $response;
	}
	else {
		my $result = 'Remove alarm failed: No alarm exists for hostId '.$obj->hostId;
		$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', $result );
		$kernel->yield( '_do_soap_response', $response, EXCEPTION__UNKNOWN_ERROR, $result ) if defined $response;
	}
}

sub handle_host_cycle_complete {
	my ($kernel, $heap, $obj) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	unless (UNIVERSAL::isa($obj, 'USAT::App::ECCS::SOAP::ComplexType::hostItem')) {
		$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'Invalid object: '.ref($obj) );
		return undef;
	}
	
	### validate that database is available; otherwise, try to reschedule event ###
	My::Util::validate_dbh($heap);
	if ($heap->{dbh}->ping) {
		### prepare parameters for ESuds.pm method call ###
		my $c_get_host_param = $heap->{dbh}->query(
			query	=> SQL__GET_HOST_PARAM,
			values	=> [ $obj->hostId ]
		);
		my ($device_id, $port, $position) = (@{$c_get_host_param->[0]});
		
		### call ESuds.pm method ###
		$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'ESuds method: ReRix::ESuds::_room_status_update_host('
			.join(', ', ($heap->{dbh}, 0, $device_id, $obj->hostId, $port, $position, ReRix::ESuds::EQUIP_STATUS_IDLE_NOT_AVAILABLE)).')' 
		);
		my $log = eval { ReRix::ESuds::_room_status_update_host($heap->{dbh}, 0, $device_id, $obj->hostId, $port, $position, ReRix::ESuds::EQUIP_STATUS_IDLE_NOT_AVAILABLE); };
		if (defined $log && ref($log) eq 'ARRAY') {
			$kernel->post( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', "ESuds: $_" ) foreach @{$log};
		}
		else {
			$kernel->post( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', "ESuds: Error handling room status update: $@" );
		}
	}
	else {
		### reschedule if still within retry window ###
		my $cycle_start_epoch_ts = USAT::SOAP::Util::convert_soap_datetime_to_epoch_sec($obj->cycleStartTime);
		if (time > ($cycle_start_epoch_ts + EVENT_RESCHEDULE_MAX_SEC)) {
			$kernel->post( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'Cycle event cancelled: Retry time expired for hostId '.$obj->hostId );
		}
		else {
			my $new_obj = My::SOAP::ComplexType::hostItemRetry->new({
				hostId			=> $obj->hostId, 
				cycleStartTime	=> $obj->cycleStartTime, 
				estCycleMin		=> $obj->estCycleMin,
				retryAttempts	=> 1 + ($obj->isa('My::SOAP::ComplexType::hostItemRetry') ? $obj->retryAttempts : 0)
			});
			$kernel->yield( 'do_schedule_host', $new_obj );
		}
	}

	### clean up ###
	$heap->{host_objects}->remove($obj->hostId);
}

########################################################################
# System Methods
########################################################################
sub service_start {
	my ($kernel, $heap, $session) = @_[KERNEL, HEAP, SESSION, ARG0..$#_];
	print STDOUT "*** Service started at ".My::Util::get_date_stamp()."\n";
	$kernel->alias_set( SESSION__MAIN );
	$kernel->sig( INT => 'sigint' );
	
	### populate heap with session data ###
	$heap->{dbh} = undef;
	$heap->{dbh_dateformat} = 'MM/DD/YYYY HH24:MI:SS';
	$heap->{dbh_config} = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 1);

	$heap->{host_alarms} = undef;
	
	### prepare logging ###
	$kernel->post( SESSION__SIMPLELOG, 'REGISTER', LOGNAME => 'APP_LOG', SESSION => $session, EVENT => 'do_log' );
	$kernel->post( SESSION__SIMPLELOG, 'REGISTER', LOGNAME => 'APP_EVENT_LOG', SESSION => $session, EVENT => 'do_log' );
	$kernel->post( SESSION__SIMPLELOG, 'REGISTER', LOGNAME => 'APP_ERR_LOG', SESSION => $session, EVENT => 'do_log' );
	
	### load persistent cache && re-schedule events if any are still pending ###
	$heap->{host_objects} = Cache::FileCache->new({
		namespace			=> 'HostObjects',
		auto_purge_on_get	=> 1,
		auto_purge_on_set	=> 1,
		cache_root			=> EXECDIR
	});
	foreach ($heap->{host_objects}->get_keys()) {
		my $obj = $heap->{host_objects}->get($_);
		$kernel->post( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'Rescheduling cycle complete event for hostId '.$obj->hostId );
		$kernel->yield( 'do_schedule_host', $obj ) if defined $obj;
	}

	### register SOAP server methods ###
	$kernel->post( SESSION__SERVER_SOAP, 'ADDMETHOD', SESSION__MAIN, $_ ) foreach keys %USAT::App::ECCS::Gateway::methods;
}

sub service_stop {
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	print STDERR "*** Service shutdown at ".localtime(time())."\n";
	eval { $pidfile->remove; }
}

sub shutdown_service {
	my ($kernel, $heap, $cur_delay_cnt) = @_[KERNEL, HEAP, ARG0];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	my $max_delay_cnt = 12;
	$cur_delay_cnt = 0 unless defined $cur_delay_cnt;
	
	$kernel->alarm_remove_all();
	$kernel->post( SESSION__SERVER_SOAP, 'DELSERVICE', SESSION__MAIN );
	$kernel->post( SESSION__SERVER_SOAP, 'SHUTDOWN', 'GRACEFUL' );

	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'Service shutting down NOW...' );
	$heap->{dbh}->close() if defined $heap->{dbh};
	$kernel->post( SESSION__SIMPLELOG, 'SHUTDOWN', SESSION__SIMPLELOG );
#	$kernel->alias_remove( SESSION__MAIN );
}

sub shutdown_service_immediatly {
	my ($kernel, $heap, $cur_delay_cnt) = @_[KERNEL, HEAP, ARG0];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_LOG', 'Service shutting down NOW...' );

	$kernel->alarm_remove_all();		
	$kernel->post( SESSION__SERVER_SOAP, 'DELSERVICE', SESSION__MAIN );
	$kernel->post( SESSION__SERVER_SOAP, 'SHUTDOWN' );
	$heap->{dbh}->close() if defined $heap->{dbh};
}

sub sigint {
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	$kernel->yield( 'shutdown_service_immediatly' );
	$kernel->sig_handled();
	return 0;
}

########################################################################
# Misc Methods
########################################################################
sub _validate_soap_schedule_input {	#must use call()--returns object if input valid
	my ($kernel, $heap, $response) = @_[KERNEL, HEAP, ARG0];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	warn Dumper($response->soapbody) if DEBUG;
	my $obj = eval { My::Util::reconstitute_soap_complextype_object($response->soapbody->{host}, 'USAT::App::ECCS::SOAP::ComplexType::hostItem'); };
	if (defined $obj && $obj->isa('USAT::App::ECCS::SOAP::ComplexType::hostItem')) {
		eval { my $t = $obj->hostId; $t = $obj->cycleStartTime; $t = $obj->estCycleMin; };	#future: SOAP::Data::ComplexType promises to validate XML element properties, like required
		if ($@) {
			$kernel->yield( '_do_soap_response', $response, EXCEPTION__INVALID_REQUEST_PARAMETER, "One or more required parameters not defined: $@" );
			return undef;
		}
		return $obj;
	}
	else {
		### return SOAP fault - unknown object error ###
		$kernel->yield( '_do_soap_response', $response, EXCEPTION__INVALID_REQUEST_PARAMETER, "Error in reqest parameter 'hosts': $@" );
	}
	
	return undef;
}

sub _do_soap_response {
	my ($kernel, $heap, $response, $code, $detail) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( SESSION__SIMPLELOG, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	if ($code) {	#if non-zero
		$kernel->post( SESSION__SERVER_SOAP, 'FAULT', $response, $code, undef, $detail );
	}
	else {
		my $obj = SOAP::Data->new(name => 'result', type => USAT::App::ECCS::SOAP::ComplexType::ResponseMessage::OBJ_TYPE, value =>
			\SOAP::Data->value(USAT::App::ECCS::SOAP::ComplexType::ResponseMessage->new({code => $code, detail => $detail})->as_soap_data)
		);
		$response->content($obj);
		$kernel->post( SESSION__SERVER_SOAP, 'DONE', $response );
	}

	return 1;
}

sub do_log {
	my ($kernel, $heap, $file, $line, $time, $name, $msg) = @_[KERNEL, HEAP, ARG0..$#_];
	my $date_stamp = My::Util::get_date_stamp(@{$time});
	if (ref($msg) eq 'ARRAY' && scalar @{$msg} > 0) {
		print STDOUT "$date_stamp [".sprintf("%5d", ($$))."] $_\n" foreach @{$msg};
	}
	else {
		print STDOUT "$date_stamp [".sprintf("%5d", ($$))."] $msg\n" if defined $msg;
	}
}

########################################################################
main();

########################################################################
# Utility Subs
########################################################################
package My::Util;
use strict;
use warnings;
use Time::HiRes qw(gettimeofday);
use Time::Local;
use Data::Dumper;

sub validate_dbh ($) {
	my $heap = shift;
	my $dbh = $heap->{dbh};
	unless (defined $dbh && $dbh->ping) {
		$dbh->close if defined $dbh->{handle};
		$dbh = USAT::Database->new(config => DatabaseConfig->new());
		$dbh->do(query => "ALTER SESSION SET NLS_DATE_FORMAT = '$heap->{dbh_dateformat}'");
	}
	$heap->{dbh} = $dbh;
}

sub dbts_to_epoch_sec ($) {
	my $timestamp = shift;
	return undef unless defined $timestamp;
	my @ts = split(/\W/, $timestamp);	#MM DD YYYY HH24 MI SS
	my $ts_epoch = eval { timelocal($ts[5], $ts[4], $ts[3], $ts[1], ($ts[0] - 1), $ts[2]); };
	return $ts_epoch;
}

sub get_date_stamp () {
	my @epoch_sec = @_;
	@epoch_sec = gettimeofday() unless @epoch_sec;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = eval { localtime($epoch_sec[0]); };
	defined $epoch_sec[1]
		? return sprintf("%04d-%02d-%02d %02d:%02d:%02d.%06d", ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec, $epoch_sec[1])
		: return sprintf("%04d-%02d-%02d %02d:%02d:%02d", ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec);
}

sub logline ($) {
	my $msg = shift;
	my $date_stamp = get_date_stamp();
	my $stamp = defined $INC{'threads.pm'} ? sprintf("%5d:%4d", ($$, threads->tid)) : sprintf("%5d", ($$));
	if (ref($msg) eq 'ARRAY' && scalar @{$msg} > 0) { print "$date_stamp [$stamp] $_\n" foreach @{$msg}; }
	else { print "$date_stamp [$stamp] $msg\n" if defined $msg; }
}

sub reconstitute_soap_complextype_object {
	my $obj = shift;
	my @allowed_pkg = @_;
	
	my $obj_type_regex = quotemeta ref($obj);
	my @package;
	die "Unknown SOAP object type '".ref($obj)."' received" unless (@package = grep(/^(\w+::)*$obj_type_regex$/, sort @allowed_pkg));	#only supports first match
	$package[0] =~ m/(.+)(::\w+)/o;
	my $class = defined $1 ? Symbol::qualify(ref($obj), $1) : ref($obj);
	my $qualified_obj = $class->new($obj);
	
	return $qualified_obj;
}

__END__

=pod

=head1 NAME

ECCS - eSuds Cycle Complete Simulator Daemon

=head1 SYNOPSIS

esuds_cycle_complete_simulator.pl  [--help|--version]

=head1 DESCRIPTION

Provides SOAP interface to register simulated eSuds cycle complete events.
Intended for use for laundry machine types that do not have

=head1 SOAP INTERFACE

Assuming you have started the daemon, the following code snippet exemplifies
common usage of this application:

	use USAT::App::ECCS::Gateway;
	use USAT::App::ECCS::SOAP::ComplexType::hostItem;
	use USAT::App::ECCS::SOAP::ComplexType::ResponseMessage;
	
	my $obj = eval { 
		USAT::App::ECCS::SOAP::ComplexType::hostItem->new({ 
			hostId => $hostId, 
			cycleStartTime => USAT::SOAP::Util::convert_epoch_sec_to_soap_datetime($cycleStartTime), 
			estCycleMin => $estCycleMin 
		}); 
	};
	die "Cycle complete event NOT scheduled: $@" if ($@);
	
	my $response = USAT::App::ECCS::Gateway
		-> proxy("http://localhost:32081/?session=ECCS", timeout => $ReRix_ESuds{eccs_timeout})
		-> schedule(\SOAP::Data->value($obj->as_soap_data));
	my $result = eval { $response->result || $response->fault; };
	
	if (defined $result) {
		if ($response->fault) {
			die "Cycle complete event NOT scheduled: Error ".$response->faultcode.": ".$response->faultstring.": ".$response->faultdetail;
		}
		elsif (UNIVERSAL::isa($result, 'ResponseMessage')) {
			my $obj = eval {
				USAT::App::ECCS::SOAP::ComplexType::ResponseMessage->new($result);
			};
			return "Cycle complete event NOT scheduled: $@" if ($@);
			if ($obj->code == 0) {
				print "Cycle complete event scheduled"
			}
			else {
				die "Cycle complete event NOT scheduled: Error ".$obj->code.": ".$obj->detail;
			}
		}
		else {
			die "Cycle complete event scheduler: Unknown response: $result";
		}
	}
	else {
		die "Cycle complete event scheduler: Unknown error: $@";
	}

=head1 OPTIONS

=over 8

=item B<--help>

Display this help and exit.

=item B<--version>

Output version information and exit.

=back

=head1 COPYRIGHT

Copyright (c) 2006 USA Technologies, Inc. All rights reserved.

=cut

