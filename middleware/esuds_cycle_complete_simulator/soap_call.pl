#!/usr/local/bin/perl
use strict;
use warnings;

use USAT::App::ECCS::Config qw();
use USAT::App::ECCS::Gateway;
use USAT::App::ECCS::SOAP::ComplexType::hostItem;
use Data::Dumper;

#print Dumper(\@INC);
my $action = shift @ARGV;
my @param;
if (scalar @ARGV > 1) {	#assume we're processing a complex object
	my @data;
	my ($hostID, $cycleStartTime, $estCycleMin) = (shift @ARGV, shift @ARGV, shift @ARGV);
	my $obj = USAT::App::ECCS::SOAP::ComplexType::hostItem->new({hostID => $hostID, cycleStartTime => $cycleStartTime, estCycleMin => $estCycleMin });
	push @param, $obj->as_soap_data;
}
else {
	@param = @ARGV;
}

my $port = USAT::App::ECCS::Config::SOAP_SERVER_PORT;
my $response = USAT::App::ECCS::Gateway
	-> proxy("http://localhost:$port/?session=ECCS")
	-> $action(\SOAP::Data->value(@param));
my $result = $response->result || $response->fault;

print Dumper($result);
