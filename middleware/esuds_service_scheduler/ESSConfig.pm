package ESSConfig;

use strict;
use warnings;

BEGIN {
	use constant DEBUG => 1;	#0=prod, 1=prod + debug info, 2=simulation mode + debug info, 3=simulation mode (no DBI) + debug info
	use constant {
		EVENT_FREQUENCY__process_active_devices			=> DEBUG  >= 2 ? 60 : '30 2 * * *',	#format: seconds or crontab
		EVENT_FREQUENCY__do_poll_current_activity_log	=> 30,	#format: seconds or crontab
		
		MAX_CONCURRENT_DEVICES				=> 50,	#max number of devices to process concurrently at any time
		DEVICE_FORCE_REACTIVATION_PERIOD	=> 30 * 60,	#seconds

		SERVICE_EVENT__REPEAT				=> 0,	#boolean: indicates whether or not service events should repeat indefinately
		
		SERVICE_EVENT__MESSAGE_CD			=> '83',
		SERVICE_EVENT__MESSAGE_CONTENT		=> '0F',
		SERVICE_EVENT__MESSAGE_RECIEVED		=> 'LOG_STARTING_UP',
		
		REACTIVATION_EVENT__MESSAGE_CD		=> undef,
		REACTIVATION_EVENT__MESSAGE_CONTENT	=> undef,
		REACTIVATION_EVENT__MESSAGE_RECEIVED=> undef,
	};
	use constant SCHOOLS => (
#		'USA Technologies School',	#for debugging
		'American University',
		'Bluffton University',
		'Carnegie Mellon University',
		'Case Western Reserve University',
		'Cedarville University',
		'Elizabethtown College',
		'Goucher College',
		'Keystone College',
		'May-Tides Laundries Inc',
		'Rutgers University',
		'Stratford Heights',
		'Temple University',
		'University of Cincinnati',
		'University Park',
		'Villa Julie College',
	);
	use constant DEVICE_SN_IGNORE_LIST	=> ();

	my $regex = quotemeta __PACKAGE__;
	sub declared (;$) {
		use constant 1.01;              # don't omit this!
		my $name = shift;
		if (defined $name) {
			$name =~ s/^::/main::/o;
			my $pkg = caller;
			my $full_name = $name =~ m/::/o ? $name : "${pkg}::$name";
			return $constant::declared{$full_name};
		}
		else {
			return grep(/^$regex\::\w+$/o, keys %constant::declared);
		}
	}
}

BEGIN {
	use Exporter ();
	use vars		qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
	@ISA			= qw(Exporter);
	@EXPORT			= ();
	@EXPORT_OK		= ();
	%EXPORT_TAGS	= (
		GLOBALS			=> [map((split('::', $_))[-1], declared())],
	);
	Exporter::export_tags('GLOBALS');
	Exporter::export_ok_tags('GLOBALS');
}

1;
