#!/usr/local/bin/perl

$VERSION = 1.00100;

use strict;
use warnings;

use Time::HiRes qw(time);
use IO::Poll;
use POE::strict qw(
	Component::SimpleLog 
);

use USAT::Database;
use USAT::Util::Datetime qw(convert_epoch_sec_to_db_timestamp convert_db_timestamp_to_epoch_sec);
use Data::Dumper;

use lib '/opt/ESS';
use ESSConfig;
use DatabaseConfig;

use constant {
	ALIAS__main			=> 'MyServer',
	ALIAS__SimpleLog	=> 'MyLogger',

	DB_FORMAT__DATE 		=> 'YYYY-MM-DD HH24:MI:SS',
	DB__EXCEPTION_CODE_ID	=> 10000,

	DEVICE_ALARM	=> 'device_noresponse_alarm',
	DEVICES			=> 'device_list',
	DEVICES_BEING_PROCESSED_CNT	=> 'device_in_service_cnt',
	LOCATIONS		=> 'locations_being_handled',
};
use constant EVENT_CD_GROUP => map(defined $_ ? $_ : (), SERVICE_EVENT__MESSAGE_CD, REACTIVATION_EVENT__MESSAGE_CD);
use constant SERVICE_EVENT_GROUP => map(defined $_ ? $_ : (), SERVICE_EVENT__MESSAGE_CD, SERVICE_EVENT__MESSAGE_CONTENT);
use constant REACTIVATION_EVENT_GROUP => map(defined $_ ? $_ : (), REACTIVATION_EVENT__MESSAGE_CD, REACTIVATION_EVENT__MESSAGE_CONTENT);
use constant {
	SQL__current_date => q{
		SELECT TO_CHAR(SYSDATE, ?) FROM DUAL
	},
	SQL__monitor_activity_log	=> q{
		SELECT created_ts server_ts, additional_information device_ts, server_name device_name, object_name event
		FROM exception_data
		WHERE exception_code_id = ?
		AND created_ts >= TO_DATE(?, ?)
	},
	SQL__select_active_esuds_devices	=> q{
		SELECT school.location_id, dorm.location_id, d.device_name, d.device_serial_cd, room.location_name
		FROM device d, pos p, vw_location_hierarchy vlh, location.location school, location.location dorm, location.location room
		WHERE vlh.ancestor_location_id = school.location_id
		AND vlh.descendent_location_id = dorm.parent_location_id
		AND LOWER(school.location_name) IN (...)
		AND vlh.depth = 1
		AND room.parent_location_id = dorm.location_id
		AND p.location_id = room.location_id
		AND p.device_id = d.device_id
		AND d.device_active_yn_flag = 'Y'
		AND d.device_type_id = 5	--only esuds devices
		AND dorm.location_type_id = 17
		AND d.last_activity_ts >= SYSDATE - 30/(60 * 24)	/* last activity no earlier than 30 minutes ago */
		AND d.device_name NOT IN (
			SELECT machine_id
			FROM machine_command_pending
			WHERE data_type IN (}."'".join("','", EVENT_CD_GROUP)."'".q{)	/* hex codes for service/reactivate msgs */
		)
		ORDER BY vlh.ancestor_location_id, dorm.location_id
	},
	SQL__network_schedule_service_event	=> q{
		INSERT INTO machine_command_pending (
			machine_id, data_type, command, execute_cd, execute_order
		) VALUES (
			?, }."'".join("','", SERVICE_EVENT_GROUP)."'".q{, 'P', 1
		)
	},
	SQL__network_activate_device	=> q{
		INSERT INTO machine_command_pending (
			machine_id, data_type, command, execute_cd, execute_order
		) VALUES (
			?, }."'".join("','", REACTIVATION_EVENT_GROUP)."'".q{, 'P', 1
		)
	}
};

########################################################################
# Main program
########################################################################
sub main {
	my $oldfh = select(STDOUT); $| = 1; select(STDERR); $| = 1; select($oldfh);	#force auto-flush; otherwise, lines may be out of order
	
#	unless (scalar @ARGV) {
#		warn 'Usage: $0 <school name 1> [<school name 2> ... <school name N>]'."\n";
#		exit 0;
#	}
	
	### init components ###
	POE::Component::SimpleLog->new(
		'ALIAS'			=> ALIAS__SimpleLog,
		'PRECISION'		=> 1
	) or die "Failed to create a POE::Component::SimpleLog session\n";

	### init main session ###
	POE::Session->create(
		args	=> scalar @ARGV ? \@ARGV : [SCHOOLS],
		inline_states	=> {
			### system ###
			'_start'	=> \&service_start,
			'_stop'		=> \&service_stop,
			'_child'	=> sub {},
			
			'sigint'			=> \&sigint,
			'shutdown_service'	=> \&shutdown_service,

			### session methods ###
			'do_log'							=> \&do_log,
			'process_active_devices'			=> \&process_active_devices,
			'do_poll_current_activity_log'		=> \&do_poll_current_activity_log,
			'handle_poll_current_activity_log'	=> \&handle_poll_current_activity_log,
			'do_handle_next_devices'			=> \&do_handle_next_devices,
			'do_device_schedule_service_event'	=> \&do_device_schedule_service_event,
			'do_device_reactivate'				=> \&do_device_reactivate,
			'handle_device_reactivation'		=> \&handle_device_reactivation,
		},
		package_states	=> [
			'My::POE::SyncEvent' => My::POE::SyncEvent::POE_PKG_STATES()	#required to use My::POE::SyncEvent
		]
	);

	$poe_kernel->run;
	exit 0;
}

########################################################################
# POE Methods
########################################################################
sub process_active_devices {
	my ($kernel, $heap, @target_schools) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	### load list of devices to reset ###
	My::Util::validate_dbh($heap) unless DEBUG >= 3;
	my $sql = SQL__select_active_esuds_devices;
	my $sql_ph = join(',', ('LOWER(?)') x scalar @target_schools);
	$sql =~ s/\.{3}/$sql_ph/o;
	my $res_aref = DEBUG >= 3 ? 
		[
			[
				'10',
				'84',
				'EV033358',
				'101400000143',
				'QA Laundry 2nd Floor'
			],
			[
				'10',
				'84',
				'EV033395',
				'101200000068',
				'QA Laundry 3rd Floor'
			],
			[
				'10',
				'88',
				'EV021053',
				'101400000151',
				'QA Laundry 1st Floor'
			]
		] 
		: $heap->{dbh}->query(
		'query'		=> $sql,
		'values'	=> [@target_schools]
	);
print STDOUT Dumper($res_aref);
	### prepare internal queues and lookup tables ###
	if (defined $res_aref->[0]->[0]) {
		foreach (@{$res_aref}) {
			my ($school, $dorm, $device_name, $device_sn, $location_name) = @{$_};
			print STDOUT "*** $dorm, $device_name, $device_sn, $location_name ***\n";
			my $device_sn_regex = quotemeta $device_sn;
			unless (grep(/^$device_sn_regex$/, DEVICE_SN_IGNORE_LIST) || defined $heap->{+DEVICES}->{$device_name}) {	#unless we're already processing this device or device in ignore list
				#need to throttle per location
				$heap->{+DEVICES}->{$device_name} = {
					school		=> $school,
					dorm		=> $dorm,
					room_name	=> $location_name,
					start_ts	=> undef,
					handled_ts	=> undef
				};
				$heap->{+LOCATIONS}->{$school}->{$dorm}->{$device_name} = undef;
			}
		}
	}
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', (caller(0))[3]." data:\n".Dumper($heap->{+LOCATIONS}, $heap->{+DEVICES}) ) if DEBUG;
	
	### immediately begin processing devices ###
	$kernel->yield( 'sevent_yield', 'do_handle_next_devices' );
	
	### reset synchronous scheduled event ###
	$kernel->call( ALIAS__main, 'sevent_done_reset', 'process_active_devices' ) if SERVICE_EVENT__REPEAT;
}

sub do_poll_current_activity_log {
	my ($kernel, $heap, $last_poll_epoch_ts) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	### get database timestamp ###
	My::Util::validate_dbh($heap) unless DEBUG >= 3;
	my $c_current_date = DEBUG >= 3 ? [[convert_db_timestamp_to_epoch_sec(time())]] : $heap->{dbh}->query(
		query	=> SQL__current_date,
		values	=> [ DB_FORMAT__DATE ]
	);
	unless (defined $c_current_date->[0]->[0]) {
		$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_ERR_LOG', 'ERROR: Unable to obtain current date from database. Current activity poll cancelled.' );
		$kernel->call( ALIAS__main, 'sevent_done_reset', 'do_poll_current_activity_log', $last_poll_epoch_ts );
		return 0;
	}
	
	### poll database ###
	my $c_db_poll_event = DEBUG >= 3 ? undef : $heap->{dbh}->query(
		query	=> SQL__monitor_activity_log,
		values	=> [
			DB__EXCEPTION_CODE_ID,
			defined $last_poll_epoch_ts ? convert_epoch_sec_to_db_timestamp($last_poll_epoch_ts) : $c_current_date->[0]->[0],
			DB_FORMAT__DATE
		]
	);
	
	### handle poll results ###
	if (defined $c_db_poll_event->[0]->[0]) {
		$kernel->post( ALIAS__main, 'handle_poll_current_activity_log', @{$_} ) foreach (@{$c_db_poll_event}); 
	}
	
	### reset synchronous scheduled event ###
	$kernel->call( ALIAS__main, 'sevent_done_reset', 'do_poll_current_activity_log', convert_db_timestamp_to_epoch_sec($c_current_date->[0]->[0]) );
}

sub handle_poll_current_activity_log {
	my ($kernel, $heap, @data) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	my ($date_recv, $date_event, $device_name, $msg) = @data;
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "Received msg from device $device_name: ".join(',', @data) );
	if (defined $heap->{+DEVICES}->{$device_name} && defined $heap->{+DEVICES}->{$device_name}->{handled_ts}) {
		if ($heap->{+DEVICES}->{$device_name}->{handled_ts} > 0) {	#log received after service event queued
			if ($msg eq REACTIVATION_EVENT__MESSAGE_RECEIVED) {
				$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "DONE: Reactivation notice received for device $device_name!" );
				$kernel->call( ALIAS__main, 'handle_device_reactivation', $device_name );
			}
		}
		elsif ($heap->{+DEVICES}->{$device_name}->{handled_ts} == 0) {	#log received: no startup detected so far
			if ($msg eq SERVICE_EVENT__MESSAGE_RECIEVED) {
				$kernel->post( ALIAS__main, 'do_device_reactivate', $device_name );
			};
		}
		else {	#(should never happen)
			$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_ERR_LOG', "ERROR: invalid handled_ts for device $device_name!" );
		}
	}
}

sub do_handle_next_devices {
	my ($kernel, $heap) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );

	### choose one device per dorm per school to deactivate; do nothing for dorm if any device at that dorm is current being processed ###
	if ($heap->{+DEVICES_BEING_PROCESSED_CNT} <= MAX_CONCURRENT_DEVICES) {
		my $is_device_flagged = 0;
		foreach my $school (sort keys %{$heap->{+LOCATIONS}}) {
			DORMLOOP: foreach my $dorm (sort keys %{$heap->{+LOCATIONS}->{$school}}) {
				next DORMLOOP if (sort {$b <=> $a} map(defined $_ ? $_ : '', values %{$heap->{+LOCATIONS}->{$school}->{$dorm}}))[0];	#skip this dorm if any device at this location currently being processed
				my @devices = (sort keys %{$heap->{+LOCATIONS}->{$school}->{$dorm}});
				DEVICELOOP: while (my $device_name = shift @devices) {
					if (defined $heap->{+DEVICES}->{$device_name} && !defined $heap->{+LOCATIONS}->{$school}->{$dorm}->{$device_name}) {
						$is_device_flagged = 1;
						$heap->{+LOCATIONS}->{$school}->{$dorm}->{$device_name} = $heap->{+DEVICES}->{$device_name};	#flag device as being handled (semaphore)
						$kernel->yield( 'do_device_schedule_service_event', $device_name );
						$heap->{+DEVICES_BEING_PROCESSED_CNT}++;
						last DEVICELOOP;
					}
				}
			}
		}
		$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', (caller(0))[3]." data:\n".Dumper($heap->{+LOCATIONS}, $heap->{+DEVICES}) ) if DEBUG && $is_device_flagged;
	}
	else {
		$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "Max number of devices (".MAX_CONCURRENT_DEVICES.") currently being processed.  Action cancelled." )
	}

	### reset synchronous scheduled event ###
	$kernel->call( ALIAS__main, 'sevent_done_reset', 'do_handle_next_devices' );
}

sub do_device_schedule_service_event {
	my ($kernel, $heap, $device_name) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );

	if (defined $heap->{+DEVICES}->{$device_name} && !defined $heap->{+DEVICES}->{$device_name}->{handled_ts}) {
		My::Util::validate_dbh($heap) unless DEBUG >= 3;
		my $res = DEBUG >= 2 ? 1 : $heap->{dbh}->do(
			'query'		=> SQL__network_schedule_service_event,
			'values'	=> [$device_name]
		);
		if ($res) {
			my ($school, $dorm) = ($heap->{+DEVICES}->{$device_name}->{school}, $heap->{+DEVICES}->{$device_name}->{dorm});
			$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "START: Queued service event for device $device_name.");
			$heap->{+DEVICES}->{$device_name}->{start_ts} = time();
			$heap->{+DEVICES}->{$device_name}->{handled_ts} = 0;
			if (defined DEVICE_FORCE_REACTIVATION_PERIOD) {	#prevent unintended long-term maintenance outage of a device
				$heap->{+DEVICE_ALARM}->{$device_name} = $kernel->alarm_set( 'do_device_reactivate', time() + DEVICE_FORCE_REACTIVATION_PERIOD, $device_name, time() );
				if (!$heap->{+DEVICE_ALARM}->{$device_name}) {
					$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_ERR_LOG', "ERROR: Error enabling alarm for device $device_name: $!" );
					delete $heap->{+DEVICE_ALARM}->{$device_name};
				}
			}
		}
		else {	
			$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_ERR_LOG', "ERROR: Error inserting command to deactivate device $device_name.  Action cancelled." );

			### reset device flag ###
			my ($school, $dorm) = ($heap->{+DEVICES}->{$device_name}->{school}, $heap->{+DEVICES}->{$device_name}->{dorm});
			$heap->{+LOCATIONS}->{$school}->{$dorm}->{$device_name} = undef;
		}
	}
	else {
		$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_ERR_LOG', "ERROR: Service event already queued or invalid service event call for device $device_name.  Action cancelled." );
	}
}

sub do_device_reactivate {
	my ($kernel, $heap, $device_name, $forced_reactivation) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	if (defined $heap->{+DEVICES}->{$device_name} && defined $heap->{+DEVICES}->{$device_name}->{handled_ts} && $heap->{+DEVICES}->{$device_name}->{handled_ts} == 0) {
		if (defined REACTIVATION_EVENT__MESSAGE_CD) {
			My::Util::validate_dbh($heap) unless DEBUG >= 3;
			my $res = DEBUG >= 2 ? 1 : $heap->{dbh}->do(
				'query'		=> SQL__network_activate_device,
				'values'	=> [$device_name]
			);
			if ($res) {
				$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "NOTICE: Queued reactivation for device $device_name.");
				$heap->{+DEVICES}->{$device_name}->{handled_ts} = time();
				if ($forced_reactivation) {
					$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "WARNING: No startup notice received for device $device_name in last ".sprintf("%.3f", (time() - $forced_reactivation) / 60)." minutes. Forcing reactivation NOW." );
					$kernel->call( ALIAS__main, 'handle_device_reactivation', $device_name );
				}
			}
			else {
				$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_ERR_LOG', "ERROR: Error inserting command to reactivate device $device_name.  Action cancelled." );
			}
		}
		else {
			$heap->{+DEVICES}->{$device_name}->{handled_ts} = time();
			$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "NOTICE: No reactivation event required. Handling simulated reactivation NOW." );
			$kernel->call( ALIAS__main, 'handle_device_reactivation', $device_name );
		}
	}
	else {
		$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_ERR_LOG', "ERROR: Reactivation already queued or invalid reactivation call for device $device_name.  Action cancelled." );
	}
}

sub handle_device_reactivation {
	my ($kernel, $heap, $device_name) = @_[KERNEL, HEAP, ARG0..$#_];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	if (defined $heap->{+DEVICES}->{$device_name} && defined $heap->{+DEVICES}->{$device_name}->{handled_ts} && defined $heap->{+DEVICES}->{$device_name}->{start_ts}) {
		$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "NOTICE: Handling reactivation for device $device_name (total time = ".sprintf("%.3f", ($heap->{+DEVICES}->{$device_name}->{handled_ts} - $heap->{+DEVICES}->{$device_name}->{start_ts}) / 60)." min).");

		### disable unused reactivation alarm for this device ###
		eval { $kernel->alarm_remove($heap->{+DEVICE_ALARM}->{$device_name}) if defined $heap->{+DEVICE_ALARM}->{$device_name}; };
		delete $heap->{+DEVICE_ALARM}->{$device_name};

		### remove device from queue ###
		my ($school, $dorm) = ($heap->{+DEVICES}->{$device_name}->{school}, $heap->{+DEVICES}->{$device_name}->{dorm});
		delete $heap->{+LOCATIONS}->{$school}->{$dorm}->{$device_name};
		delete $heap->{+DEVICES}->{$device_name};
		
		### decrement concurrent device count ###
		$heap->{+DEVICES_BEING_PROCESSED_CNT}--;

		### recursively purge school tree as appropriate ###
		delete $heap->{+LOCATIONS}->{$school}->{$dorm} unless scalar values %{$heap->{+LOCATIONS}->{$school}->{$dorm}} > 0;
		delete $heap->{+LOCATIONS}->{$school} unless scalar values %{$heap->{+LOCATIONS}->{$school}} > 0;
		
		### exit application if all service events have been completed ###
		if (!SERVICE_EVENT__REPEAT && scalar keys %{$heap->{+LOCATIONS}} == 0) {
			$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', "NOTICE: All service events completed. Application will now terminate.");
			$kernel->yield( 'shutdown_service' );
		}
		else {
			### immediately check for more devices to process ###
			$kernel->yield( 'sevent_yield', 'do_handle_next_devices' );
		}
	}
	else {
		$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_ERR_LOG', "ERROR: Reactivation already handled or invalid handler for device $device_name.  Action cancelled." );
	}
}

sub service_start {
	my ($kernel, $heap, $session, @schools) = @_[KERNEL, HEAP, SESSION, ARG0..$#_];
	print STDOUT "*** Service started at ".My::Util::get_date_stamp()."\n";
	$kernel->alias_set( ALIAS__main );
	$kernel->sig( INT => 'sigint' );
	
	### populate heap with session data ###
	$heap->{dbh} = undef;
	$heap->{dbh_dateformat} = DB_FORMAT__DATE;
	$heap->{dbh_config} = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 1);
	$heap->{schools} = \@schools;
	
	### prepare logging ###
	$kernel->post( ALIAS__SimpleLog, 'REGISTER', LOGNAME => 'APP_LOG', SESSION => $session, EVENT => 'do_log' );
	$kernel->post( ALIAS__SimpleLog, 'REGISTER', LOGNAME => 'APP_EVENT_LOG', SESSION => $session, EVENT => 'do_log' );
	$kernel->post( ALIAS__SimpleLog, 'REGISTER', LOGNAME => 'APP_ERR_LOG', SESSION => $session, EVENT => 'do_log' );

	### set scheduled events ###
	my %sevents = (
		process_active_devices			=> [ EVENT_FREQUENCY__process_active_devices, @{$heap->{schools}} ],
		do_handle_next_devices			=> [ 60 ],
		do_poll_current_activity_log	=> [ EVENT_FREQUENCY__do_poll_current_activity_log ]
	);
	$kernel->yield( 'sevent_alarm_init', \%sevents );
	$kernel->yield( 'sevent_yield', 'process_active_devices', @{$heap->{schools}} ) if DEBUG >= 1;
}

sub service_stop {
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	
	### shutdown remaining app pieces ###
	$heap->{dbh}->close() if defined $heap->{dbh};
	$kernel->call( ALIAS__SimpleLog, 'SHUTDOWN', ALIAS__SimpleLog );
	
	print STDOUT "*** Service shutdown at ".My::Util::get_date_stamp()."\n";
}

sub shutdown_service {
	my ($kernel, $heap, $cur_delay_cnt) = @_[KERNEL, HEAP, ARG0];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_LOG', 'Service shutting down NOW...' );

	### remove all alarms ###
	$kernel->alarm_remove_all();		

	### immediately reactivate any deactivated (or scheduled to be deactivated) devices ###
	foreach my $device_name (keys %{$heap->{+DEVICES}}) {
		$kernel->yield( 'do_device_reactivate', $device_name ) if defined $heap->{+DEVICES}->{$device_name}->{handled_ts} && $heap->{+DEVICES}->{$device_name}->{handled_ts} == 0;
	}
}

sub do_log {
	my ($kernel, $heap, $file, $line, $time, $name, $msg) = @_[KERNEL, HEAP, ARG0..$#_];
	my $date_stamp = My::Util::get_date_stamp(@{$time});
	if (ref($msg) eq 'ARRAY' && scalar @{$msg} > 0) {
		print STDOUT "$date_stamp [".sprintf("%5d", ($$))."] $_\n" foreach @{$msg};
	}
	else {
		print STDOUT "$date_stamp [".sprintf("%5d", ($$))."] $msg\n" if defined $msg;
	}
}

sub sigint {
	my ($kernel, $heap, $cur_delay_cnt) = @_[KERNEL, HEAP, ARG0];
	$kernel->call( ALIAS__SimpleLog, 'LOG', 'APP_EVENT_LOG', 'Executing '.(caller(0))[3]."..." );
	$kernel->yield( 'shutdown_service' );
	$kernel->sig_handled();
	return 0;
}

main();

########################################################################
# My::POE::SyncEvent
########################################################################
package My::POE::SyncEvent;

use strict;
use warnings;
use POE;
use Schedule::Cron::Events;
use Time::Local;
use Data::Dumper;

use constant {
	DEBUG				=> main::DEBUG,
	LOGGER				=> main::ALIAS__SimpleLog,
	POE_PKG_STATES		=> [	#intended for import by implementing POE::Session
		'sevent_alarm_init',
		'sevent_alarm',
		'sevent_alarm_reset',
		'sevent_call',
		'sevent_yield',
		'sevent_done',
		'sevent_done_reset',
		'sevent_isactive'
	],
	ALARM_SCHEDULE_KEY	=> 'sevent_schedule',
	ALARM_PERIOD_KEY	=> 'period',	#can be integer (seconds) or crontab-format (* * * * *)
	ALARM_DEF_ARGS_KEY	=> 'default_args',
	ALARM_ID_KEY		=> 'alarm_id',
	ALARM_NEXT_TS_KEY	=> 'next_ts',
	ALARM_ISACTIVE_KEY	=> 'sevent_isactive'
};

########################################################################
#Purpose:	Sets synchronous alarm for a given event
#----------------------------------------------------------------------#
sub sevent_alarm_init {
	my ($kernel, $heap, $event_href) = @_[KERNEL, HEAP, ARG0];

	if (defined $event_href && ref($event_href) eq 'HASH') {
		foreach my $event (keys %{$event_href}) {
			my ($event_period, @event_default_args) = @{$event_href->{$event}};
			unless ($event_period =~ m/^\d+$/) {
				$event_period = eval { new Schedule::Cron::Events( $event_period, Seconds => time() ); };
				if ($@) {
					$event_period = undef;
					print STDERR "Invalid event period: $@\n";
				}
			}
			
			if (defined $event_period) {
				%{$heap->{+ALARM_SCHEDULE_KEY}->{$event}} = (
					ALARM_PERIOD_KEY()		=> $event_period,
					ALARM_DEF_ARGS_KEY()	=> \@event_default_args,
					ALARM_ID_KEY()			=> undef,
					ALARM_NEXT_TS_KEY()		=> undef,
					ALARM_ISACTIVE_KEY()	=> 0
				);
				$kernel->yield( 'sevent_alarm', $event );
			}
		}
	}
	$kernel->call( LOGGER, 'LOG', 'APP_LOG', (caller(0))[3]." data:\n".Dumper($heap->{+ALARM_SCHEDULE_KEY}) ) if DEBUG;
}

########################################################################
#Purpose:	Sets synchronous alarm for a given event
#----------------------------------------------------------------------#
sub sevent_alarm {
	my ($kernel, $heap, $event, @event_args) = @_[KERNEL, HEAP, ARG0..$#_];
	my @event_args_final = scalar @event_args ? @event_args : @{$heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_DEF_ARGS_KEY}};
	
	### delete old alarm if exists ###
	$kernel->alarm_remove($heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ID_KEY}) if defined $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ID_KEY};
	
	### create new alarm ###
	$heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_NEXT_TS_KEY} = UNIVERSAL::isa($heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_PERIOD_KEY}, 'Schedule::Cron::Events')
		? timelocal($heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_PERIOD_KEY}->nextEvent)
		: int(time()) + $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_PERIOD_KEY};
	$heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ID_KEY} = $kernel->alarm_set( 'sevent_yield', $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_NEXT_TS_KEY}, $event, @event_args_final );
	
	$kernel->post( LOGGER, 'LOG', 'APP_LOG', "Event '$event' will reoccur at ".localtime($heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_NEXT_TS_KEY})." using ".(scalar @event_args_final)." ".(scalar @event_args ? " custom" : "default")." args" );
}

########################################################################
#Purpose:	Sets synchronous alarm for a given event
#----------------------------------------------------------------------#
sub sevent_alarm_reset {
	my ($kernel, $heap, $session, $event, @event_args) = @_[KERNEL, HEAP, SESSION, ARG0..$#_];
	$kernel->call( $session, 'sevent_alarm', $event, @event_args) if defined $heap->{+ALARM_SCHEDULE_KEY}->{$event};
}

########################################################################
#Purpose:	Mark a non-running synchronous event active and call the event
#----------------------------------------------------------------------#
sub sevent_call {
	my ($kernel, $heap, $session, $event, @event_args) = @_[KERNEL, HEAP, SESSION, ARG0..$#_];
	my $res = 0;	#did not start event
	unless (defined $heap->{+ALARM_SCHEDULE_KEY}->{$event} && defined $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} && $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} == 1) {
		$heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} = 1;
		$kernel->call( LOGGER, 'LOG', 'APP_LOG', "Initiated callback of synchronized event '$event'..." );
		$kernel->call( $session, $event, @event_args );
		$res = 1;
	}
	else {
		#should do callback to error sub if defined here
		$kernel->call( LOGGER, 'LOG', 'APP_LOG', "Unable to initiated postback of synchronized event '$event'. Instance currently executing." );
	}
	return $res;
}

########################################################################
#Purpose:	Mark a non-running synchronous event active and yield the event
#----------------------------------------------------------------------#
sub sevent_yield {
	my ($kernel, $heap, $event, @event_args) = @_[KERNEL, HEAP, ARG0..$#_];
	my $res = 0;	#did not start event
	unless (defined $heap->{+ALARM_SCHEDULE_KEY}->{$event} && defined $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} && $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} == 1) {
		$heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} = 1;
		$kernel->yield( $event, @event_args );
		$kernel->call( LOGGER, 'LOG', 'APP_LOG', "Initiated postback of synchronized event '$event'..." );
		$res = 1;
	}
	else {
		#should do callback to error sub if defined here
		$kernel->call( LOGGER, 'LOG', 'APP_LOG', "Unable to initiated postback of synchronized event '$event'. Instance currently executing." );
	}
	return $res;
}

########################################################################
#Purpose:	Mark a synchronous event complete
#----------------------------------------------------------------------#
sub sevent_done {
	my ($kernel, $heap, $event) = @_[KERNEL, HEAP, ARG0];
	$heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} = 0 if defined $heap->{+ALARM_SCHEDULE_KEY}->{$event} && defined $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY};
}

########################################################################
#Purpose:	Mark a synchronous event complete and reset alarm
#----------------------------------------------------------------------#
sub sevent_done_reset {
	my ($kernel, $heap, $session, $event, @event_args) = @_[KERNEL, HEAP, SESSION, ARG0..$#_];
	$kernel->call( $session, 'sevent_done', $event );
	$kernel->call( $session, 'sevent_alarm_reset', $event, @event_args );
}

########################################################################
#Purpose:	Returns list of still active synchronous events
#Pre:		POE caller (note: only useful with call())
#----------------------------------------------------------------------#
sub sevent_isactive {
	my ($kernel, $heap) = @_[KERNEL, HEAP];
	my @active_events;
	if (ref($heap->{+ALARM_SCHEDULE_KEY}) eq 'HASH') {
		foreach my $event (keys %{$heap->{+ALARM_SCHEDULE_KEY}}) {
			push @active_events, $event if defined $heap->{+ALARM_SCHEDULE_KEY}->{$event} && defined $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} && $heap->{+ALARM_SCHEDULE_KEY}->{$event}->{+ALARM_ISACTIVE_KEY} == 1;
		}
	}
	return @active_events ? \@active_events : undef;
}

########################################################################
# Utility Subs
########################################################################
package My::Util;
use strict;
use Time::HiRes qw(gettimeofday);
use Time::Local;

sub validate_dbh ($) {
	my $heap = shift;
	my $dbh = $heap->{dbh};
	unless (defined $dbh && $dbh->ping) {
		$dbh->close if defined $dbh->{handle};
		$dbh = USAT::Database->new(debug => 1, debug_die => 0, print_query => 1);
		$dbh->query_nonselect(query => "alter session set nls_date_format = '$heap->{dbh_dateformat}'");
	}
	$heap->{dbh} = $dbh;
}

sub get_date_stamp () {
	my @epoch_sec = @_;
	@epoch_sec = gettimeofday() unless @epoch_sec;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($epoch_sec[0]);
	if (defined $epoch_sec[1]) {
		return sprintf("%04d-%02d-%02d %02d:%02d:%02d.%06d", ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec, $epoch_sec[1]);
	}
	else {
		return sprintf("%04d-%02d-%02d %02d:%02d:%02d", ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec);
	}
}

sub logline ($) {
	my $msg = shift;
	my $date_stamp = get_date_stamp();
	if (ref($msg) eq 'ARRAY' && scalar @{$msg} > 0) { print "$date_stamp [".sprintf("%5d:%4d", ($$, threads->self->tid))."] $_\n" foreach @{$msg}; }
	else { print "$date_stamp [".sprintf("%5d", ($$))."] $msg\n" if defined $msg; }
}

1;


