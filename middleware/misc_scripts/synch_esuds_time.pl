#!/usr/bin/perl

use strict;
use DBI;
use USAT::Database;
use DatabaseConfig;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not $dbh)
{
        print localtime() . "\tFailed to connect to the database!\n";
        exit();
}

my $insert_stmt = $dbh->prepare( q{
insert into machine_command_pending(machine_id, data_type, command, execute_cd, execute_order)
select device_name, '83', '0F', 'P', 1
from device
where device_type_id = 5
and device_active_yn_flag = 'Y'
});

$insert_stmt->execute();
$insert_stmt->finish();

$dbh->disconnect;

