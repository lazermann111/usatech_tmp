#!/usr/bin/perl
#
# This script will create a pending request for firmware revision for every active
# G4 and G5 device, but only if there is not already a pending request.
# 
# It's expected this will run via cron once every 30 or 60 days.
#
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		06/10/2004	pcowan		Initial Creation
# 1.10		10/05/2004	pcowan		Adapted from request_esuds_diagnostics.pl
#

use DBI;
use USAT::Database;
use DatabaseConfig;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not $dbh)
{
        print localtime() . "\tFailed to connect to the database!\n";
        exit();
}

my $data_type = '87';
my $command = '4400007F200000000F';
my $setting_name = 'Firmware Version';
my $days_old = 60;

my $list_devices_stmt = $dbh->prepare("select device_name from device where device.device_type_id in (0,1) and device.device_active_yn_flag = 'Y' and not exists( select 1 from machine_command_pending where machine_command_pending.data_type = :data_type and machine_command_pending.execute_cd in ('P', 'S') and machine_command_pending.command = :command and device.device_name = machine_command_pending.machine_id ) and not exists ( select 1 from device_setting where device_setting.device_id = device.device_id and device_setting.device_setting_parameter_cd = :setting_name and device_setting.last_updated_ts > (sysdate-:days_old) ) ");

if(not $list_devices_stmt)
{
	print localtime() . "\tFailed to prepare select statement!\n";
	exit();
}

$list_devices_stmt->bind_param(":data_type", $data_type);
$list_devices_stmt->bind_param(":command", $command);
$list_devices_stmt->bind_param(":setting_name", $setting_name);
$list_devices_stmt->bind_param(":days_old", $days_old);
$list_devices_stmt->execute();

while (my @ev_array = $list_devices_stmt->fetchrow_array()) 
{
	print localtime() . "\tCreating Firmwave Version Request for $ev_array[0]\n";

	my $insert_pending_stmt = $dbh->prepare("insert into machine_command_pending(machine_id, data_type, command, execute_cd, execute_order) values (:machine_id, :data_type, :command, :execute_cd, :execute_order)");
	$insert_pending_stmt->bind_param(":machine_id", $ev_array[0]);
	$insert_pending_stmt->bind_param(":data_type", $data_type);
	$insert_pending_stmt->bind_param(":command", $command);
	$insert_pending_stmt->bind_param(":execute_cd", 'P');
	$insert_pending_stmt->bind_param(":execute_order", '99');
	$insert_pending_stmt->execute();
	$insert_pending_stmt->finish();
}

$list_devices_stmt->finish();
$dbh->disconnect;
