#!/usr/bin/perl

#
#	Change History
#	--------------
#	Version		Date			Programmer		Description
#	-------		----------		----------		-------------------------------
#	1.00		09/13/2004		P Cowan			Job to normalize G4/G5 call-in times
#

use strict;
use DBI;
use USAT::Database;
use DatabaseConfig;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not $dbh)
{
        print localtime() . "\tFailed to connect to the database!\n";
        exit();
}

my $debug		= 1;

my @call_in_times 	= ('0005','0010','0015','0020','0025','0030','0035','0040','0045','0050','0055','0100','0105','0110','0115','0120','0125','0130','0135','0140','0145','0150','0155','0200','0205','0210','0215','0220','0225','0230','0235','0240','0245','0250','0255','0300','0305','0310','0315','0320','0325','0330','0335','0340','0345','0350','0355','0400','0405','0410','0415','0420','0425');

# get a list of the most frequent call-in times
my $device_list_stmt = $dbh -> prepare(q{
	select device.device_id, device.device_name, device.device_serial_cd, file_transfer.file_transfer_id, file_transfer.file_transfer_name, device.device_type_id, device.FILE_CONTENT_SUBSTR(file_transfer.rowid, 172, 2)
        from device, file_transfer
        where device.device_name = substr(file_transfer.file_transfer_name, 0, 8)
        and device.device_type_id in (0,1)
        and device.device_active_yn_flag = 'Y'
        and file_transfer.file_transfer_type_cd = 1
        and device.last_activity_ts > (sysdate - 30)
        and 
        (
            device.FILE_CONTENT_SUBSTR(file_transfer.rowid, 172, 2) like '043%' OR
            device.FILE_CONTENT_SUBSTR(file_transfer.rowid, 172, 2) like '044%' OR
            device.FILE_CONTENT_SUBSTR(file_transfer.rowid, 172, 2) like '045%'
        )
});

$device_list_stmt -> execute();
my $device_list_ref = $device_list_stmt -> fetchall_arrayref();
$device_list_stmt -> finish();
my $counter = 0;
foreach my $device_list_array (@{$device_list_ref})
{
	my $device_id 		= $device_list_array->[0];
	my $ev_number	 	= $device_list_array->[1];
	my $serial_number 	= $device_list_array->[2];
	my $file_id 		= $device_list_array->[3];
	my $file_name 		= $device_list_array->[4];
	my $device_type		= $device_list_array->[5];
	my $call_in_time	= $device_list_array->[6];
		
	my $new_index = ($counter % scalar(@call_in_times));
	my $new_call_in = $call_in_times[$new_index];
		
	&output(" Updating deviceID $device_id ($serial_number): old call-in = $call_in_time, new call-in = $new_call_in");
		
	my ($return_code,$return_msg,$db_err_str);
	my $update_ref = $dbh->prepare("BEGIN sp_update_config (:device_id, :offset, :new_data, :data_type, :device_type, :debug, :return_code, :return_msg); END;");
	$update_ref->bind_param(":device_id", $device_id);
	$update_ref->bind_param(":offset", 172);
	$update_ref->bind_param(":new_data", $new_call_in);
	$update_ref->bind_param(":data_type", 'H');
	$update_ref->bind_param(":device_type", $device_type);
	$update_ref->bind_param(":debug", $debug);
	$update_ref->bind_param_inout(":return_code", \$return_code, 40);
	$update_ref->bind_param_inout(":return_msg", \$return_msg, 2048);
	$update_ref->execute;
	$db_err_str = $dbh->errstr;
	$update_ref->finish();
	
	if($return_code < 0)
	{
		&output("  Update of deviceID $device_id call-in failed: Error creating Poke! $return_msg, $db_err_str");
	}
	elsif($return_code == 1)
	{
		&output("  Update of deviceID $device_id call-in succeeded: $return_msg");
	}
	else
	{
		&output("  Update of deviceID $device_id call-in skipped: no change ($return_msg)");
	}
	
	$counter++;
}

sub output
{
	my $line = shift;
	print "[" . localtime() . "] $line\n";
}

1;

