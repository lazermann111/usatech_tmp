#!/usr/local/USAT/bin/perl
#
# This script will create a pending request for Multitech GSM/GPRS information for 
# G4 and G5 device, but only if there is not already a pending request.
# 
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		06/10/2004	pcowan		Initial Creation
# 1.10		10/05/2004	pcowan		Adapted from request_esuds_diagnostics.pl
# 1.20		11/15/2004	pcowan		Adapted from request_gx_rev.pl
# 1.30		03/18/2005	pcowan		Merged insert/select into 1 statement, added MEI statement
# 1.31		08/30/2005	pcowan		Modified sql statements to impose a max rows limit
# 1.32		05/21/2008	jbradley	Added command line param for $max_requests, changed MEI query to reflect eport query
# 1.32		12/19/2008	jbradley	Updated queries to reflect the use of the AT&T APNs (new IP range)
# 1.33		09/13/2010	dkouznetsov	Fixed issue with deprecated net layer names, improved database query performance
#

use strict;
use DBI;
use USAT::Database;
use DatabaseConfig;
use Getopt::Long;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if (not $dbh)
{
	print localtime() . "\tFailed to connect to the database!\n";
	exit();
}

my $data_type = '87';
my $eport_days_old = 14;
my $mei_days_old = 14;
my $multitech_command = '4400802C0000000190';
my $mei_command	= '4200000000000000FF';
my $max_requests; # = 100;
my $mei_max_requests = 100;

# Check for input from command line; validate
my $argv_result = GetOptions(
	"maxreq=i" => \$max_requests
);

# Check for maxreq
if (!(defined $max_requests)) {
	$max_requests = 1000;
}

my $insert_multitech_peek_stmt = $dbh->prepare( q{

insert into engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order) 
select /*+ NO_CPU_COSTING */
device_name, :data_type, :multitech_command, 'P', 99
from
(
    select d.device_name
    from device.device d
    where d.last_activity_ts > sysdate - :days_old
	and d.device_active_yn_flag = 'Y'
	and d.device_type_id in (0, 1)
	and d.comm_method_cd = 'G'
    and not exists
    (
        select 1
        from engine.machine_cmd_pending mcp
        where mcp.machine_id = d.device_name
		and mcp.data_type = :data_type
        and mcp.command = :multitech_command
        and mcp.execute_cd in ('P', 'S')
    )
    and not exists
    (
        select 1
        from engine.machine_cmd_pending_hist mcph
        where mcph.machine_id = d.device_name
		and mcph.execute_date > sysdate - :days_old
		and mcph.data_type = :data_type
        and mcph.command = :multitech_command
    )
    and not exists
    (
        select 1
        from device.gprs_device gd
        where gd.device_id = d.device_id
        and gd.assigned_ts > sysdate - :days_old
    )
    order by d.device_name desc
)
where rownum < :max_requests

});

if (not $insert_multitech_peek_stmt)
{
	print localtime() . "\tFailed to prepare Multitech statement!\n";
	exit();
}

$insert_multitech_peek_stmt->bind_param(":data_type", $data_type);
$insert_multitech_peek_stmt->bind_param(":multitech_command", $multitech_command);
$insert_multitech_peek_stmt->bind_param(":days_old", $eport_days_old);
$insert_multitech_peek_stmt->bind_param(":max_requests", $max_requests);
$insert_multitech_peek_stmt->execute();
$insert_multitech_peek_stmt->finish();

my $insert_mei_peek_stmt = $dbh->prepare( q{

insert into engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order) 
select /*+ NO_CPU_COSTING */
device_name, :data_type, :mei_command, 'P', 99
from
(
    select d.device_name
    from device.device d
    where d.last_activity_ts > sysdate - :days_old
	and d.device_active_yn_flag = 'Y'
	and d.device_type_id = 6
	and d.comm_method_cd = 'G'
    and not exists
    (
        select 1
        from engine.machine_cmd_pending mcp
        where mcp.machine_id = d.device_name
		and mcp.data_type = :data_type
        and mcp.command = :mei_command
        and mcp.execute_cd in ('P', 'S')
    )
    and not exists
    (
        select 1
        from engine.machine_cmd_pending_hist mcph
        where mcph.machine_id = d.device_name
		and mcph.execute_date > sysdate - :days_old
		and mcph.data_type = :data_type
        and mcph.command = :mei_command
    )
    and not exists
    (
        select 1
        from device.gprs_device gd
        where gd.device_id = d.device_id
        and gd.assigned_ts > sysdate - :days_old
    )
    order by d.device_name desc
)
where rownum < :max_requests

});

if (not $insert_mei_peek_stmt)
{
        print localtime() . "\tFailed to prepare MEI statement!\n";
        exit();
}

$insert_mei_peek_stmt->bind_param(":data_type", $data_type);
$insert_mei_peek_stmt->bind_param(":mei_command", $mei_command);
$insert_mei_peek_stmt->bind_param(":days_old", $mei_days_old);
$insert_mei_peek_stmt->bind_param(":max_requests", $mei_max_requests);
$insert_mei_peek_stmt->execute();
$insert_mei_peek_stmt->finish();

$dbh->disconnect;

