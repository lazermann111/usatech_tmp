#!/bin/bash

JOBSPATH="/opt/USAT/jobs"

PARAMS=""
RUNIT=1
while getopts "m:h?" optname 
do
	case "$optname" in
	  "m")
		PARAMS="-maxreq $OPTARG "
		;;
	  "h")
		RUNIT=0
		echo "Optional agrs: '-m (int)' where (int) is the maximum number of devices to request modem information from."
		;;
	  "?")
		RUNIT=0
		echo "Optional agrs: '-m (int)' where (int) is the maximum number of devices to request modem information from."
		;;
	  ":")
		RUNIT=0
		echo "No argument value for option $OPTARG"
		;;
	  *)
	  # Should not occur
		RUNIT=0
		echo "Unknown error while processing options"
		;;
	esac
done

if [ $RUNIT -eq 1 ]; then
	cd $JOBSPATH
	#pwd
	#echo "perl request_multitech_info.pl ${PARAMS}1>> ${JOBSPATH}/request_multitech_info.log 2>&1"
	perl request_multitech_info.pl ${PARAMS}1>> ${JOBSPATH}/request_multitech_info.log 2>&1
fi
