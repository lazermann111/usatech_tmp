package DatabaseConfig;

use strict;
use USAT::Database::Config;
use vars qw(@ISA);
@ISA = qw(USAT::Database::Config);

sub new {
	my $type = shift;
	my $self = USAT::Database::Config->new(
		retries			=> 1,
		connection_lifetime	=> 60 * 60 * 24,
		@_
	);
	return $self;
}

1;
