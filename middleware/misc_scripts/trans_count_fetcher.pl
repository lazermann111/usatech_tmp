#!/usr/bin/perl

use strict;
use warnings;

use DBI;
use Time::HiRes qw(gettimeofday time);

my $oldfh = select(STDOUT); $| = 1; select($oldfh); #force auto-flush of STDOUT; otherwise, lines may be out of order


print_log("Process started ...");


## connect to the database
print_log("Connecting to DB ...");
my $dbh = DBI->connect('dbi:Oracle:USARDB', 'report', 'usatech');


## prepare the tran count query
my $sth = $dbh->prepare(q{
SELECT 
TO_CHAR(TRUNC(SYSDATE) - 1, 'YYYY-MM-DD') count_date,
'Start Count' count_type,
COUNT(1) tran_count
FROM report.trans
WHERE trans_type_id IN (16, 19) -- debit and credit only
AND server_date < TRUNC(SYSDATE) - 1

UNION ALL

SELECT
TO_CHAR(TRUNC(SYSDATE) - 1, 'YYYY-MM-DD') count_date,
'Process Count' count_type,
COUNT(1) tran_count
FROM report.trans
WHERE trans_type_id IN (16, 19) -- debit and credit only
AND server_date >= TRUNC(SYSDATE) - 1
AND server_date < TRUNC(SYSDATE)
});


## execute the query
print_log("Fetching transaction count data ...");
$sth->execute();


## get the resulting data
my ($start_count, $process_count, $count_date);
while (my @data_row = $sth->fetchrow_array()) {
	print_log("Data: $data_row[0]  $data_row[1]  $data_row[2]");
	if ($data_row[1] eq 'Start Count') {
		$start_count = $data_row[2];
	} elsif ($data_row[1] eq 'Process Count') {
		$process_count = $data_row[2];
		$count_date = $data_row[0];
	}
}
$sth->finish();


## format and then write the data to a file
print_log("Writing data to file ...");
open TRANCOUNTDATA, ">tran_count_data" or die $!;
print TRANCOUNTDATA "$start_count\n$process_count\n$count_date";
close TRANCOUNTDATA;


## get the raw list of servers from the servers file
print_log("Fetching the list of servers to copy file ...");
my @servers;
open SERVERS, "<trans_count_fetcher.servers" or die $!;
chomp(@servers = <SERVERS>);
close(SERVERS);


## parse the list of servers
my @server_list;
my $server;
foreach $server (@servers) {
	$server =~ s/\#.*?$//g; # remove Perl-style comments
	$server =~ s/^\s+|\s+$//g; # remove whitespace
	# check for empty strings
	if ($server !~ m/^$/) {
		push(@server_list, $server);
	}
}


## scp the file to the list of servers
my $scpcmd;
my $output;
foreach $server (@server_list) {
	print_log("Copying file to $server ...");
	# /usr/local/bin/scp -pq /home/webmaster/test_file usawsp01:/home/html/trans_count/	print_log("Command: $scpcmd");
	$scpcmd = "scp -pq /opt/jobs/TransCount/tran_count_data $server:/home/html/trans_count/";
	$output = system($scpcmd);
	
	if ($output == 0) {
		print_log("Result: file copied.");
	} else {
		print_log("Result: error - code: $output");
	}:wq
}


sub print_log {
	my $message = shift;
	print log_date_stamp() .  " [".sprintf("%5d", $$)."] $message\n";
}

sub log_date_stamp {
	my @epoch_sec = gettimeofday();
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($epoch_sec[0]);
	return sprintf('%04d-%02d-%02d %02d:%02d:%02d.', ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec).substr(sprintf('%06d', $epoch_sec[1]), 0, 3);
}
