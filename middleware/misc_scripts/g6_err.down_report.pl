#!/usr/local/bin/perl

use strict;
use warnings;

use Net::SMTP;
use OOCGI::Query;
use OOCGI::OOCGI;
use OOCGI::NTable;

########################################################################

our $smtp_server = 'mailhost';
#our $to_email = 'pcowan@usatech.com,tmann@usatech.com,aseymour@usatech.com,bpatrizzi@usatech.com,dbothner@usatech.com,jdettinger@usatech.com,jsimpkins@usatech.com,gharrum@usatech.com';
our $to_email = 'G6Error@usatech.com';
#our $to_email = 'pcowan@usatech.com';
our $from_email = 'reports@usatech.com';
our $subject = 'G6 Err.DOWN Report - ' . localtime();

########################################################################

# Connect to server, DL file header description
sub main {

	my $table = OOCGI::NTable->new("cellspacing=0 cellpadding=1 border=1");
	my $rn = 0;

	$table->insert($rn, 0, B($subject), 'bgcolor=#C0C0C0 colspan=11 align=center');
	$rn++;

	$table->insert($rn, 0, "G6 errors reported in the last 3 days", 'colspan=11 align=center');
	$rn++;

	$table->insert($rn, 0, B("Serial Number"), 'align=center nowrap');
	$table->insert($rn, 1, B("Customer"), 'align=center nowrap');
	$table->insert($rn, 2, B("Location"), 'align=center nowrap');
	$table->insert($rn, 3, B("Last Reported Date"), 'align=center nowrap');
	$table->insert($rn, 4, B("Number of Errors Reported"), 'align=center nowrap');
	$table->insert($rn, 5, B("EEPROM State"), 'align=center nowrap');
	$table->insert($rn, 6, B("System Service State"), 'align=center nowrap');
	$table->insert($rn, 7, B("MDB OR Pulse State"), 'align=center nowrap');
	$table->insert($rn, 8, B("COMMs State"), 'align=center nowrap');
	$table->insert($rn, 9, B("UI State"), 'align=center nowrap');
	$table->insert($rn, 10, B("Raw Message"), 'align=center nowrap');
	$rn++;
	
	my $sql = q|
		select 
	    export_error.express as "Serial Number",
	    customer.customer_name as "Customer", 
	    export_error.location as "Location", 
	    to_char(max(export_error.create_dt), 'MM/DD/YYYY HH:MI:SS AM') as "Last Reported Date", 
	    count(1) as "Number of Errors Reported", 
	    decode(substr(msg, 9,1), '0', 'EEPROM Unconfigured', '1', 'EEPROM Configured', 'Unknown') as "EEPROM State",
	    decode(substr(msg, 10,1), 'I', 'In Service', 'C', 'In Service - Diag Mode', 'T', 'Out of Service by Client', 'O', 'Out of Service by Server', 'Unknown') as "System Service State",
	    decode(substr(msg, 11,1), 'I', 'STATE_MDB_INACTIVE', 'D', 'STATE_MDB_DISABLED', 'E', 'STATE_MDB_ENABLED', 'S', 'STATE_MDB_SESSION_IDLE', 'V', 'STATE_MDB_SESSION_VEND', 'R', 'STATE_MDB_SESSION_REVALUE', 'X', 'Invalid Value', '0', 'PULSE_INTERFACE_STATE_DISABLED', '1', 'PULSE_INTERFACE_STATE_ENABLED', 'B', 'PULSE_INTERFACE_STATE_BUSY', 'Z', 'PULSE_INTERFACE_STATE_IMPROPER_CONFIGURATION', 'Unknown') as "MDB OR Pulse State",
	    decode(substr(msg, 12,1), '0', 'COMMs Down', '1', 'COMMs Up', 'Unknown') as "COMMs State",
	    decode(substr(msg, 13,1), 'A', 'UI_UNIT_STATES_DISABLED', 'B', '= UI_UNIT_STATES_PLEASE_SWIPE_CARD', 'C', 'UI_UNIT_STATES_CARD_AUTHORIZING', 'D', 'UI_UNIT_STATES_CARD_DENIED', 'E', 'UI_UNIT_STATES_CARD_APPROVED', 'F', 'UI_UNIT_STATES_WAITING_FOR_SELECTION', 'G', 'UI_UNIT_STATES_SELECTION_MADE', 'H', 'UI_UNIT_STATES_CREDIT_CANCELED', 'I', 'UI_UNIT_STATES_SESSION_TIMEOUT', 'J', 'UI_UNIT_STATES_SERVICE_MENU', 'K', 'UI_UNIT_STATES_NOT_CONFIGURED', 'X', 'INVALID UI STATE', 'Unknown') as "UI State",
	    export_error.msg as "Raw Message"
		from export_error, device, pos, customer
		where export_error.express = device.device_serial_cd
		and device.device_id = pos.device_id
		and pos.customer_id = customer.customer_id
		and device.device_active_yn_flag = 'Y'
		and export_error.create_dt > trunc(sysdate - 3)
		and export_error.export_batch_id > (select (max(export_error.export_batch_id) - 1000) from export_error)
		and export_error.msg like 'Err.Down%'
		group by export_error.express, customer.customer_name, export_error.location, export_error.msg
		order by 
	    customer.customer_name, export_error.express, export_error.msg |;

	my $format = 'align=LEFT nowrap';

	my $qo = OOCGI::Query->new($sql);
	while(my @arr = $qo->next)
	{
		my $cn = 0;
		$table->put($rn,$cn++,qq|<a href="http://device-admin-beta.usatech.com/profile.cgi?serial_number=$arr[0]">$arr[0]</a>|, $format);
		$table->put($rn,$cn++,$arr[1], $format);
		$table->put($rn,$cn++,$arr[2], $format);
		$table->put($rn,$cn++,$arr[3], $format);
		$table->put($rn,$cn++,$arr[4], $format);
		$table->put($rn,$cn++,$arr[5], $format);
		$table->put($rn,$cn++,$arr[6], $format);
		$table->put($rn,$cn++,$arr[7], $format);
		$table->put($rn,$cn++,$arr[8], $format);
		$table->put($rn,$cn++,$arr[9], $format);
		$table->put($rn,$cn++,$arr[10], $format);
		$rn++;
	}
	
	if($rn == 2)
	{
		$table->put($rn,0,B('No errors to report!', 'GREEN'), 'colspan=11 align=center');
		$rn++;
	}
	
	display $table;
	send_email($from_email, $to_email, $subject, $table);
}

########################################################################
sub send_email ($$$$)
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = $smtp_server;
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("Content-Type: text/html\n");
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}


main();
