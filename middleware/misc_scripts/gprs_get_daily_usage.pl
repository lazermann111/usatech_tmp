#!/usr/local/bin/perl

use strict;
use warnings;

use DBI;
use Archive::Zip;
use Text::CSV;
use Net::SMTP;
use Sys::Hostname;
use Date::Calc;
use Getopt::Long;

use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::Database;

########################################################################
#App defaults
my $process_name = $0; $process_name =~ s/\W/_/go; $process_name =~ s/^_+//o;
our $smtp_server = 'mailhost';
our $to_email = 'pcowan@usatech.com';
our $from_email = "$process_name@".hostname().".usatech.com";

my $file_name_param;
my $file_prefix = 'USA';
my $file_ext = 'zip';
my $archive_path = '/opt/jobs/gprsdata';

########################################################################
# Check for input from command line; validate
my $argv_result = GetOptions(
	"file=s" => \$file_name_param
);

# Connect to server, DL file header description
sub main {
	# Determine file to download (default: yesterday's file)
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $file_name_txt = sprintf("%d%02d%02d", Date::Calc::Add_Delta_Days($year+1900,$mon+1,$mday, -1));
	my $file_name;
	if(defined $file_name_param)
	{
 		$file_name = $file_name_param;
		my @file_name_split = split(/\./, $file_name_param);
		$file_name_txt = $file_name_split[0];
	}
	else
	{
	 	$file_name = "$file_prefix$file_name_txt\.$file_ext";
	}

	print "file_name = $file_name\n";
	print "file_name_txt = $file_name_txt\n";
	
	# Load data file header format
	my $url = "$USAT::DeviceAdmin::GPRS::Cingular::protcol$USAT::DeviceAdmin::GPRS::Cingular::server/USA/sub_detail_definitions.asp";
	print "Attempting to download $url ...\n";
	my $response = $USAT::DeviceAdmin::GPRS::Cingular::browser->get($url);
	if ($response->is_error)
	{
		die "Error accessing '$url' at Cingular.";
	}
	my $file_header; #= 'ICCID,MSISDN,PLAN_CODE,ACT_ID,INCLUDED_USAGE,ON_NET_KB_USAGE,ROAMING_KB_USAGE,CAN_KB_USAGE,INT_KB_USAGE,TOTAL_KB_USAGE,ACCESS_CHG,ACTIVATION_CHG,BEGPERIOD,ENDPERIOD,CUST_PREFIX,CUST_FIELD_1,CUST_FIELD_2,CUST_FIELD_3,CUST_FIELD_4,CUST_FIELD_5';
	my $header_data = $response->content;
	$header_data =~ s/\r\n|\n//go;
	if ($header_data =~ m/Header for Imsi Detail file:<[^>]+><div[^>]+>(([^<]|<[br|BR]+>)+)<\/div>/)
	{
		$file_header = $1;
		$file_header =~ s/<[^>]+>//go;	#remove any remaining html tags (yuck!)
		foreach (
			'BEGPERIOD', 
			'ICCID', 
			'MSISDN', 
			'INCLUDED_USAGE', 
			'ON_NET_KB_USAGE', 
			'ROAMING_KB_USAGE', 
			'CAN_KB_USAGE', 
			'INT_KB_USAGE', 
			'TOTAL_KB_USAGE'
		)
		{
			die "Unable to find required header field name '$_'" unless $file_header =~ m/\b$_\b/;
		}
	}
	else
	{
		print "Unable to determine CVS header from Cingular web site. Data import process cancelled. Please validate page content.\n";
		send_email(
			$from_email, 
			$to_email, 
			"Error processing input GPRS header file", 
			"Unable to determine CVS header from Cingular web site. Please validate page content.\n"
		);
		exit 0;
	}

	my $idx = 0;
	my %header_hash = map(($_, $idx++), split(/,/, $file_header));

	# Connect to server, DL file
	$url = "$USAT::DeviceAdmin::GPRS::Cingular::protcol$USAT::DeviceAdmin::GPRS::Cingular::server/USA/GPRSDailyUsage/$file_name";
	print "Attempting to download $url ...\n";
	$response = $USAT::DeviceAdmin::GPRS::Cingular::browser->get($url);
	if ($response->is_error)
	{
		die "Error downloading '$file_name' from Cingular.";
	}
	my $file_content = $response->content;
	if (open(FILEOUT, ">$archive_path/$file_name"))
	{
		print FILEOUT $file_content;
		close FILEOUT;
	}
	else
	{
		die "Unable to create temporary file '$archive_path/$file_name'";
	}


	# Parse file & Write to database
	my $zip_file = Archive::Zip->new("$archive_path/$file_name");
	my @textFileMembers = $zip_file->membersMatching("$file_name_txt.txt");
	if (scalar @textFileMembers > 1)
	{
		die "Multiple files found in archive '$file_name'.  Process currently can handle only one file.";
	}

	my $dbh = USAT::Database->new(AutoCommit => 0);
	my @data = split(/\r\n|\n/, $zip_file->contents($textFileMembers[0]));
	my $csv = Text::CSV->new();
	my $error_occured = 0;
	my $insert_sth;
	foreach (@data)
	{
		$_ =~ s/\000//go;	#null characters invalid byte in CVS files?
		if ($csv->parse($_)) {
			my @data_row = $csv->fields();
			$dbh->do(
				query	=> q{
					INSERT INTO device.gprs_daily_usage (
						report_date,
						iccid,
						msisdn,
						included_usage,
						aws_kb_usage,
						roaming_kb_usage,
						can_kb_usage,
						int_kb_usage,
						total_kb_usage
					) VALUES (
						TO_DATE(?, 'YYYY-MM-DD'),
						?, ?, ?, ?, ?, ?, ?, ?
					)
				},
				values	=> [
					sprintf("%04d-%02d-%02d", split(/[^0-9]/,$data_row[$header_hash{BEGPERIOD}])),
					$data_row[$header_hash{ICCID}],
					$data_row[$header_hash{MSISDN}],
					$data_row[$header_hash{INCLUDED_USAGE}],
					$data_row[$header_hash{ON_NET_KB_USAGE}],
					$data_row[$header_hash{ROAMING_KB_USAGE}],
					$data_row[$header_hash{CAN_KB_USAGE}],
					$data_row[$header_hash{INT_KB_USAGE}],
					$data_row[$header_hash{TOTAL_KB_USAGE}],
				],
				sth	=> \$insert_sth
			);
		}
		else {
			$error_occured = 1;
			print "Invalid line found: ".$csv->error_input()."\n";
			send_email(
				$from_email, 
				$to_email, 
				"Error processing input GPRS data file", 
				"Invalid line found: ".$csv->error_input()."\n"
			);
			$dbh->rollback();
			last;
		}
	}
	$dbh->commit();
	$dbh->close();
	print ($error_occured ? "Error during processing. Database unchanged." : "Done! Inserted ".scalar @data." rows.\n");
}

########################################################################
sub send_email ($$$$)
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = $smtp_server;
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}

sub send_email_detailed
{
	my ($from_addr, $from_name, $to_addrs, $to_names, $subject, $content) = @_;
	my @to_addr_array = split(/,/, $to_addrs);
	my @to_name_array = split(/,/, $to_names);
	
	# check that every address has a name
	if($#to_addr_array ne $#to_name_array)
	{
		warn "send_email_detailed: Length of address array not equald to length of name array!\n";
		return 0;
	}

	my $smtp_server = $smtp_server;
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_addr_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 

	my $to_line;
	for(my $index = 0; $index <= $#to_addr_array; $index++)
	{
		$to_line = $to_line . "\"$to_name_array[$index]\" <$to_addr_array[$index]>";
		if($index < $#to_addr_array)
		{
			$to_line = $to_line . ", ";
		}
	}
	
	$smtp->datasend("To: $to_line\n");
	$smtp->datasend("From: $from_name <$from_addr>\n");
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("Content-Type: text/plain\n");
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();
	
	return 1;
}

main();
