#!/usr/bin/perl
#
# This script will create a pending request for machine diagnostics for every active
# eSuds device, but only if there is not already a pending diagnostics request.
# 
# It's expected this will run via cron once a night to get fresh diagnostics for the morning.
#
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		06/10/2004	pcowan		Initial Creation
#

use strict;

use DBI;
use USAT::Database;
use DatabaseConfig;

# First get a connection to the database
my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not $dbh)
{
	print localtime() . "\tFailed to connect to the database!\n";
	exit();
}

my $data_type = '9A61';

my $esuds_devices_stmt = $dbh->prepare("select device_name from device where device.device_type_id = 5 and device.device_active_yn_flag = 'Y' and device.device_name not in (select machine_command_pending.machine_id from machine_command_pending where machine_command_pending.data_type = :data_type and machine_command_pending.execute_cd in ('P', 'S'))");

if(not $esuds_devices_stmt)
{
	print localtime() . "\tFailed to prepare select statement!\n";
	exit();
}

$esuds_devices_stmt->bind_param(":data_type", $data_type);
$esuds_devices_stmt->execute();

my @ev_array;

while (my @row_array = $esuds_devices_stmt->fetchrow_array()) 
{
	push (@ev_array, $row_array[0]);
}

$esuds_devices_stmt->finish();

print localtime() . "\tFound " . scalar(@ev_array) . " room controllers that need diagnostics...\n";

for my $ev (@ev_array)
{
	print localtime() . "\tCreating Diagnostics Request for $ev\n";

	my $insert_pending_stmt = $dbh->prepare("insert into machine_command_pending(machine_id, data_type, execute_cd, execute_order) values (:machine_id, :data_type, :execute_cd, :execute_order)");
	$insert_pending_stmt->bind_param(":machine_id", $ev);
	$insert_pending_stmt->bind_param(":data_type", $data_type);
	$insert_pending_stmt->bind_param(":execute_cd", 'P');
	$insert_pending_stmt->bind_param(":execute_order", '1');
	$insert_pending_stmt->execute();
	$insert_pending_stmt->finish();

	sleep 60;
}

$dbh->disconnect;

