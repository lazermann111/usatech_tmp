#!/usr/bin/perl

# This script pulls the ICCID out of the MEI configuration files in the file_transfer_table
# and correlates the data with the gprs_device table.
#
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		03/15/2005	pcowan		Initial Version
#

use strict;
use DBI;
use USAT::Database;
use DatabaseConfig;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not defined $dbh)
{
	print "Failed to get a connection to the database!\n";
	exit 1;
}

# query for a list of devices that have reported GPRS modem information recently
my $lookup_mei_iccids_stmt = $dbh->prepare("select device.device_id,  device.file_content_substr(file_transfer.rowid, 28, 10) from file_transfer, device where file_transfer.file_transfer_name = device.device_name || '-CFG'  and device.device_type_id = 6 and file_transfer.last_updated_ts > (sysdate-:days_old)") or die "Couldn't prepare statement: " . $dbh->errstr;
$lookup_mei_iccids_stmt->bind_param(":days_old", 60);
$lookup_mei_iccids_stmt->execute() or die "Couldn't execute statement: " . $lookup_mei_iccids_stmt->errstr;
while(my @data = $lookup_mei_iccids_stmt->fetchrow_array())
{
	my $device_id = $data[0];
	my $iccid = $data[1];
	
	print "\nProcessing Device ID $device_id, ICCID $iccid...\n";
	
	if(length($iccid) != 20)
	{
		print "ICCID \"$iccid\" failed validation check!\n";
		next;
	}
	
	if(!($iccid =~ /^8931/))
	{
		print "ICCID \"$iccid\" failed validation check!\n";
		next;
	}
	
	# query for the current modem info in gprs_device table
	my $current_data_stmt = $dbh->prepare("select gprs_device_state_id, device_id from gprs_device where iccid = :iccid") or die "Couldn't prepare statement: " . $dbh->errstr;
	$current_data_stmt->bind_param(":iccid", $iccid);
	$current_data_stmt->execute() or die "Couldn't execute statement: " . $current_data_stmt->errstr;
	my @current_data = $current_data_stmt->fetchrow_array();
	$current_data_stmt->finish();
	
	my $current_device_id 	= $current_data[1];
	
	if($device_id != $current_device_id)
	{
		print "Updating $iccid, old device_id = $current_device_id, new device_id = $device_id\n";
		
		my $update_stmt = $dbh->prepare("update gprs_device set device_id = :device_id, gprs_device_state_id = :gprs_device_state_id, assigned_by = :assigned_by, assigned_ts = sysdate where iccid = :iccid") or die "Couldn't prepare statement: " . $dbh->errstr;
		$update_stmt->bind_param(":device_id", $device_id);
		$update_stmt->bind_param(":gprs_device_state_id", 5);
		$update_stmt->bind_param(":assigned_by", 'SYSTEM');
		$update_stmt->bind_param(":iccid", $iccid);
		$update_stmt->execute() or die "Couldn't execute statement: " . $update_stmt->errstr;
		$update_stmt->finish();
	}
	else
	{
		print "$iccid is up-to-date!\n";
	}
}

$lookup_mei_iccids_stmt->finish();
$dbh->disconnect;

