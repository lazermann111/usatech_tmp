#!/usr/bin/perl
use strict;
use USAT::POS::API::PTA::Util;

use USAT::Database;
my $DATABASE = USAT::Database->new(print_query => 0, PrintError => 1, RaiseError => 1, AutoCommit => 1);

my $template_id = 341;
my $import_mode = 'O';

my $result = $DATABASE->query(
query	=> q{

select distinct device_id from (
select device.device_id, device.device_serial_cd, merchant.merchant_name
from device, pos, pos_pta, payment_subtype, terminal, merchant
where device.device_id = pos.device_id
and pos.customer_id = 330
and pos.pos_id = pos_pta.pos_id
and pos_pta.payment_subtype_id = payment_subtype.payment_subtype_id
and payment_subtype.payment_subtype_id = 1
and (pos_pta.pos_pta_activation_ts < sysdate and pos_pta.pos_pta_deactivation_ts is null)
and pos_pta.payment_subtype_key_id = terminal.terminal_id
and terminal.merchant_id = merchant.merchant_id
and merchant.merchant_id != 6063
and device.device_type_id != 6
order by merchant.merchant_id
)

},);

foreach my $result_ref (@{$result})
{
	my $err_ref;
	my $res = USAT::POS::API::PTA::Util::import_template($DATABASE, $result_ref->[0], $template_id, $import_mode, \$err_ref);
	if(!$res)
	{
		print "Import failed for device_id $result_ref->[0]: $err_ref\n";
	}
	else
	{
		print "OK: http://device-admin.usatech.com/device_profile.cgi?device_id=$result_ref->[0]\n";
	}
}

