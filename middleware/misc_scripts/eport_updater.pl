#!/usr/bin/perl

use strict;

use USAT::Database;
use DatabaseConfig;
use Config::Properties::Simple;

my $in      = shift(@ARGV);
my $config = Config::Properties::Simple->new(
	file => $in,
    validate => { 
	
		debug							=> 'boolean',
		print_query						=> 'boolean',

		diag_protocol					=> 'string',
		diag_server_ip					=> 'string',
		diag_port_num					=> 'integer',
		diag_login_name 				=> 'string',
		diag_password					=> 'string',
		diag_file_format				=> 'string',
		diag_encapsulation				=> 'string',
		diag_crc_type					=> 'string',
		diag_path_name					=> 'string',
		diag_crc						=> 'string',
		diag_key						=> 'string',
		diag_exp_days					=> 'integer',
		diag_retries					=> 'integer',
		diag_retry_interval				=> 'integer',
		
		mainapp_protocol				=> 'string',
		mainapp_server_ip				=> 'string',
		mainapp_port_num				=> 'integer',
		mainapp_login_name 				=> 'string',
		mainapp_password				=> 'string',
		mainapp_file_format				=> 'string',
		mainapp_encapsulation			=> 'string',
		mainapp_crc_type				=> 'string',
		mainapp_path_name				=> 'string',
		mainapp_crc						=> 'string',
		mainapp_key						=> 'string',
		mainapp_exp_days				=> 'integer',
		mainapp_retries					=> 'integer',
		mainapp_retry_interval			=> 'integer',
		
		last_activity_days				=> 'number',
		last_updated_days				=> 'number',
		recent_transfer_days			=> 'number',
		
		max_out_of_date_peek			=> 'integer',
		max_diag_transfer				=> 'integer',
		max_diag_sent_peek				=> 'integer',
		max_mainapp_transfer			=> 'integer',
		max_recent_transfers			=> 'integer',
		
		old_mainapp_revs				=> 'string',
		new_mainapp_revs				=> 'string',
		
		old_diag_revs					=> 'string',
		new_diag_revs					=> 'string'
    },
    required => [qw{debug print_query diag_protocol diag_server_ip diag_port_num diag_login_name diag_password diag_file_format diag_encapsulation diag_crc_type diag_path_name diag_crc diag_key diag_exp_days diag_retries diag_retry_interval mainapp_protocol mainapp_server_ip mainapp_port_num mainapp_login_name mainapp_password mainapp_file_format mainapp_encapsulation mainapp_crc_type mainapp_path_name mainapp_crc mainapp_key mainapp_exp_days mainapp_retries mainapp_retry_interval last_activity_days last_updated_days recent_transfer_days max_out_of_date_peek max_diag_transfer max_diag_sent_peek max_mainapp_transfer old_mainapp_revs new_mainapp_revs old_diag_revs new_diag_revs}]
   );

my $debug					= $config->getProperty('debug');
my $print_query				= $config->getProperty('print_query');
my $diag_protocol			= $config->getProperty('diag_protocol');
my $diag_server_ip			= $config->getProperty('diag_server_ip');
my $diag_port_num			= $config->getProperty('diag_port_num');
my $diag_login_name 		= $config->getProperty('diag_login_name');
my $diag_password			= $config->getProperty('diag_password');
my $diag_file_format		= $config->getProperty('diag_file_format');
my $diag_encapsulation		= $config->getProperty('diag_encapsulation');
my $diag_crc_type			= $config->getProperty('diag_crc_type');
my $diag_path_name			= $config->getProperty('diag_path_name');
my $diag_crc				= $config->getProperty('diag_crc');
my $diag_key				= $config->getProperty('diag_key');
my $diag_exp_days			= $config->getProperty('diag_exp_days');
my $diag_retries			= $config->getProperty('diag_retries');
my $diag_retry_interval		= $config->getProperty('diag_retry_interval');
my $mainapp_protocol		= $config->getProperty('mainapp_protocol');
my $mainapp_server_ip		= $config->getProperty('mainapp_server_ip');
my $mainapp_port_num		= $config->getProperty('mainapp_port_num');
my $mainapp_login_name 		= $config->getProperty('mainapp_login_name');
my $mainapp_password		= $config->getProperty('mainapp_password');
my $mainapp_file_format		= $config->getProperty('mainapp_file_format');
my $mainapp_encapsulation	= $config->getProperty('mainapp_encapsulation');
my $mainapp_crc_type		= $config->getProperty('mainapp_crc_type');
my $mainapp_path_name		= $config->getProperty('mainapp_path_name');
my $mainapp_crc				= $config->getProperty('mainapp_crc');
my $mainapp_key				= $config->getProperty('mainapp_key');
my $mainapp_exp_days		= $config->getProperty('mainapp_exp_days');
my $mainapp_retries			= $config->getProperty('mainapp_retries');
my $mainapp_retry_interval	= $config->getProperty('mainapp_retry_interval');
my $last_activity_days		= $config->getProperty('last_activity_days');
my $last_updated_days		= $config->getProperty('last_updated_days');
my $recent_transfer_days	= $config->getProperty('recent_transfer_days');
my $max_out_of_date_peek	= $config->getProperty('max_out_of_date_peek');
my $max_diag_transfer		= $config->getProperty('max_diag_transfer');
my $max_diag_sent_peek		= $config->getProperty('max_diag_sent_peek');
my $max_mainapp_transfer	= $config->getProperty('max_mainapp_transfer');
my $max_recent_transfers	= $config->getProperty('max_recent_transfers');
my $old_mainapp_revs		= $config->getProperty('old_mainapp_revs');
my $new_mainapp_revs		= $config->getProperty('new_mainapp_revs');
my $old_diag_revs			= $config->getProperty('old_diag_revs');
my $new_diag_revs			= $config->getProperty('new_diag_revs');

my @old_diag_values = split(',', $old_diag_revs);
my @new_diag_values = split(',', $new_diag_revs);
my @old_mainapp_values = split(',', $old_mainapp_revs);
my @new_mainapp_values = split(',', $new_mainapp_revs);

my $rev_peek_order = 1;
my $rev_peek_datatype = '87';
my $rev_peek_command = '4400007F200000000F';
my $ext_file_trans_datatype = 'A9';
my $diag_trans_order = 1;
my $mainapp_trans_order = 1;  

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => $print_query);
my $dbh = USAT::Database->new(config => $db_config);

&output("ePort Updater running!");
&output("Upgrading diagnostic app from $old_diag_revs to $new_diag_revs");
&output("Upgrading main app from $old_mainapp_revs to $new_mainapp_revs");

&output("----------------------------------------------------");
&do_out_of_date_peek();
&output("----------------------------------------------------");
&do_diag_transfer();
&output("----------------------------------------------------");
&do_diag_recently_sent_peek();
&output("----------------------------------------------------");
&do_mainapp_app_transfer();
&output("----------------------------------------------------");

sub do_out_of_date_peek
{
	&output("CRITERIA : devices where rev info is out-of-date");
	&output("ACTION   : peek for main app version, will also get booloader and diagnostic app versions");

	my $out_of_date_sql = 
	q{
		select device.device_id, device.device_name, device.device_serial_cd
		from device, pos, device_setting mainapp
		where device.device_id = pos.device_id
		and device.device_active_yn_flag = 'Y'
		and pos.customer_id > 1
		and device.device_type_id = 1
		and device.last_activity_ts > (sysdate - ?)
		and mainapp.device_id = device.device_id
		and mainapp.device_setting_parameter_cd = 'Firmware Version'
		and mainapp.device_setting_value like 'USA-G51v5%'
		and mainapp.last_updated_ts < (sysdate - ?)
		and not exists
		(
		    select 1
		    from machine_command_pending
		    where machine_command_pending.machine_id = device.device_name
		    and machine_command_pending.data_type = ?
		    and machine_command_pending.command = ?
		)
		and rownum <= ?
	};

	my $out_of_date_query = $dbh->query(
		query	=> $out_of_date_sql,
		values	=>
		[
			$last_activity_days,
			$last_updated_days,
			$rev_peek_datatype,
			$rev_peek_command,
			$max_out_of_date_peek
		]
	);

	my $out_of_date_count = 0;
	my @out_of_date_list = (); 
	my @out_of_date_names;
	foreach my $out_of_date_ref (@{$out_of_date_query})
	{
		push(@out_of_date_list, [$out_of_date_ref->[0], $out_of_date_ref->[1], $out_of_date_ref->[2]]);
		push(@out_of_date_names, $out_of_date_ref->[2]);
		$out_of_date_count++;
	}
	
	if($out_of_date_count > 0)
	{
		&output("RESULT   : Creating $out_of_date_count firmware peeks for out-of-date devices: " . join(",", @out_of_date_names));
		
		if(!$debug)
		{
			foreach my $out_of_date_ref (@out_of_date_list)
			{
				my $out_of_date_insert = $dbh->query(
					query	=>
						q{ 
							insert into machine_command_pending(machine_id, execute_cd, execute_order, data_type, command)
							values (?, ?, ?, ?, ?)
						},
					values	=>
					[
						$out_of_date_ref->[1],
						'P',
						$rev_peek_order,
						$rev_peek_datatype,
						$rev_peek_command
					]
				);
			}
		}
		else
		{
			&output("RESULT   : Skipped; DEBUG ON");
		}
	}
	else
	{
		&output("RESULT   : No devices have out-of-date rev info");
	}
}

sub do_diag_transfer
{
	&output("CRITERIA : devices with remote flashable main app and old diagnostic app ");
	&output("ACTION   : send new diagnostic app");

	my $diag_transfer_sql = 
	q{
		select device.device_id, device.device_name, device.device_serial_cd, location.location_time_zone_cd
		from device, pos, device_setting mainapp, device_setting diag, location.location
		where device.device_id = pos.device_id
		and pos.location_id = location.location_id
		and device.device_active_yn_flag = 'Y'
		and pos.customer_id > 1
		and device.device_type_id = 1
		and device.last_activity_ts > (sysdate - ?)
		and mainapp.device_id = device.device_id
		and mainapp.device_setting_parameter_cd = 'Firmware Version'
		and mainapp.device_setting_value like 'USA-G51v5%'
		and mainapp.last_updated_ts > (sysdate - ?)
		and diag.device_id = device.device_id
		and diag.device_setting_parameter_cd = 'Diagnostic App Rev'
		and diag.device_setting_value in (} . join(',', ('?') x scalar(@old_diag_values)) .q{)
		and diag.last_updated_ts > (sysdate - ?)
		and not exists
		(
		    select 1
		    from machine_command_pending
		    where machine_command_pending.machine_id = device.device_name
		    and upper(machine_command_pending.data_type) = ?
		)
		and not exists
		(
		    select 1
		    from machine_command_pending_hist
		    where machine_command_pending_hist.machine_id = device.device_name
		    and upper(machine_command_pending_hist.data_type) = ?
		    and machine_command_pending_hist.execute_date > (sysdate - ?)
		)
                and 
                (
                    select count(1)
                    from machine_command_pending_hist
                    where machine_command_pending_hist.machine_id = device.device_name
                    and upper(machine_command_pending_hist.data_type) = ?
                    and machine_command_pending_hist.execute_date > (sysdate - ?)
                ) <= ?
		and rownum <= ?
	};

	my $diag_transfer_query = $dbh->query(
		query	=> $diag_transfer_sql,
		values	=>
		[
			$last_activity_days,
			$last_updated_days,
			(@old_diag_values),
			$last_updated_days,
			$ext_file_trans_datatype,
			$ext_file_trans_datatype,
			$recent_transfer_days,
			$ext_file_trans_datatype,
			$last_activity_days,
			$max_recent_transfers,
			$max_diag_transfer
		]
	);

	my $diag_transfer_count = 0;
	my @diag_transfer_list = ();
	my @diag_transfer_names = ();  
	foreach my $diag_transfer_ref (@{$diag_transfer_query})
	{
		push(@diag_transfer_list, [$diag_transfer_ref->[0], $diag_transfer_ref->[1], $diag_transfer_ref->[2], $diag_transfer_ref->[3]]);
		push(@diag_transfer_names, $diag_transfer_ref->[2]);
		$diag_transfer_count++;
	}
	
	if($diag_transfer_count > 0)
	{
		&output("RESULT   : Creating $diag_transfer_count diagnostic file transfers: " . join(",", @diag_transfer_names));
		
		if(!$debug)
		{
			my ($octet1, $octet2, $octet3, $octet4) = split(/\./,$diag_server_ip);
			$octet1 = chr($octet1);
			$octet2 = chr($octet2);
			$octet3 = chr($octet3);
			$octet4 = chr($octet4);
	
			my $login_name_length = chr(length($diag_login_name));
			my $login_password_length = chr(length($diag_password));
			my $file_path_name_length = chr(length($diag_path_name));
			my $encryption_key_length = chr(length($diag_key)/2);
		
			foreach my $diag_transfer_ref (@diag_transfer_list)
			{
				my $transfer_start_gmtime = &getAdjustedTimestamp($diag_transfer_ref->[3]);
				my $transfer_end_gmtime = $transfer_start_gmtime + ($diag_exp_days * 86400);
		
				my $message = pack("aaaaa",$diag_protocol, $octet1, $octet2, $octet3, $octet4);
				$message .= pack("n", $diag_port_num);
				$message .= pack("aa*", $login_name_length, $diag_login_name);
				$message .= pack("aa*", $login_password_length, $diag_password);
				$message .= pack("aa*", $file_path_name_length, $diag_path_name);
				$message .= pack("H*", $diag_file_format);
				$message .= pack("H*", $diag_encapsulation);
				$message .= pack("H*", $diag_crc_type);
				$message .= pack("H*", $diag_crc);
				$message .= pack("a", $encryption_key_length);
				$message .= pack("H*", $diag_key);
				$message .= pack("NN", $transfer_start_gmtime, $transfer_end_gmtime);
				$message .= pack("aa*", chr($diag_retries), chr($diag_retry_interval));
				
				&output(" Diagnostic transfer command: " . $diag_transfer_ref->[1] . ": " . unpack("H*",$message));
				
				my $diag_transfer_insert = $dbh->query(
					query	=>
						q{ 
							insert into machine_command_pending(machine_id, execute_cd, execute_order, data_type, command)
							values (?, ?, ?, ?, ?)
						},
					values	=>
					[
						$diag_transfer_ref->[1],
						'P',
						$diag_trans_order,
						$ext_file_trans_datatype,
						unpack("H*",$message)
					]
				);
			}
		}
		else
		{
			&output("RESULT   : Skipped; DEBUG ON");
		}
	}
	else
	{
		&output("RESULT   : No devices need a diagnostic app update");
	}
}

sub do_diag_recently_sent_peek
{
	&output("CRITERIA : devices with remote flashable main app and old diagnostic app and diagnostic app was sent more recently than the peek");
	&output("ACTION   : peek for main app version, will also get booloader and diagnostic app versions");

	my $diag_sent_sql = 
	q{
		select device.device_id, device.device_name, device.device_serial_cd
		from device, pos, device_setting mainapp, device_setting diag
		where device.device_id = pos.device_id
		and device.device_active_yn_flag = 'Y'
		and pos.customer_id > 1
		and device.device_type_id = 1
		and device.last_activity_ts > (sysdate - ?)
		and mainapp.device_id = device.device_id
		and mainapp.device_setting_parameter_cd = 'Firmware Version'
		and mainapp.device_setting_value like 'USA-G51v5%'
		and mainapp.last_updated_ts > (sysdate - ?)
		and diag.device_id = device.device_id
		and diag.device_setting_parameter_cd = 'Diagnostic App Rev'
		and diag.device_setting_value in (} . join(',', ('?') x scalar(@old_diag_values)) .q{)
		and diag.last_updated_ts > (sysdate - ?)
		and not exists
		(
		    select 1
		    from machine_command_pending
		    where machine_command_pending.machine_id = device.device_name
		    and machine_command_pending.data_type = ?
		    and machine_command_pending.command = ?
		)
		and exists
		(
		    select 1
		    from machine_command_pending_hist
		    where machine_command_pending_hist.machine_id = device.device_name
		    and upper(machine_command_pending_hist.data_type) = ?
		    and machine_command_pending_hist.execute_cd = 'A'
		    and machine_command_pending_hist.execute_date > (sysdate - ?)
		)
		and
		(
		    select max(machine_command_pending_hist.execute_date)
		    from machine_command_pending_hist
		    where machine_command_pending_hist.machine_id = device.device_name
		    and upper(machine_command_pending_hist.data_type) = ?
		    and machine_command_pending_hist.execute_cd = 'A'
		    and machine_command_pending_hist.execute_date > (sysdate - ?)
		) > diag.last_updated_ts 
		and rownum <= ?
	};

	my $diag_sent_sql_query = $dbh->query(
		query	=> $diag_sent_sql,
		values	=>
		[
			$last_activity_days,
			$last_updated_days,
			(@old_diag_values),
			$last_updated_days,
			$rev_peek_datatype,
			$rev_peek_command,
			$ext_file_trans_datatype,
			$last_updated_days,
			$ext_file_trans_datatype,
			$last_updated_days,
			$max_diag_sent_peek
		]
	);

	my $diag_sent_sql_count = 0;
	my @diag_sent_list = (); 
	my @diag_sent_names;
	foreach my $diag_sent_ref (@{$diag_sent_sql_query})
	{
		push(@diag_sent_list, [$diag_sent_ref->[0], $diag_sent_ref->[1], $diag_sent_ref->[2]]);
		push(@diag_sent_names, $diag_sent_ref->[2]);
		$diag_sent_sql_count++;
	}
	
	if($diag_sent_sql_count > 0)
	{
		&output("RESULT   : Creating $diag_sent_sql_count firmware peeks for recently sent diagnostics: " . join(",", @diag_sent_names));
		
		if(!$debug)
		{
			foreach my $diag_sent_ref (@diag_sent_list)
			{
				my $diag_sent_sql_insert = $dbh->query(
					query	=>
						q{ 
							insert into machine_command_pending(machine_id, execute_cd, execute_order, data_type, command)
							values (?, ?, ?, ?, ?)
						},
					values	=>
					[
						$diag_sent_ref->[1],
						'P',
						$rev_peek_order,
						$rev_peek_datatype,
						$rev_peek_command
					]
				);
			}
		}
		else
		{
			&output("RESULT   : Skipped; DEBUG ON");
		}
	}
	else
	{
		&output("RESULT   : No devices have been recently sent a diagnostic app update");
	}
}

sub do_mainapp_app_transfer
{
	&output("CRITERIA : devices with old main app but new diagnostic app");
	&output("ACTION   : send new main app");

	my $mainapp_transfer_sql = 
	q{
		select device.device_id, device.device_name, device.device_serial_cd, location.location_time_zone_cd
		from device, pos, device_setting mainapp, device_setting diag, location.location
		where device.device_id = pos.device_id
		and device.device_id = diag.device_id
		and pos.location_id = location.location_id
		and device.device_active_yn_flag = 'Y'
		and pos.customer_id > 1
		and device.device_type_id = 1
		and device.last_activity_ts > (sysdate - ?)
		and mainapp.device_id = device.device_id
		and mainapp.device_setting_parameter_cd = 'Firmware Version'
		and mainapp.device_setting_value in (} . join(',', ('?') x scalar(@old_mainapp_values)) .q{)
		and mainapp.last_updated_ts > (sysdate - ?)
		and diag.device_id = device.device_id
		and diag.device_setting_parameter_cd = 'Diagnostic App Rev'
		and diag.device_setting_value in (} . join(',', ('?') x scalar(@new_diag_values)) .q{)
		and diag.last_updated_ts > (sysdate - ?)
		and not exists
		(
		    select 1
		    from machine_command_pending
		    where machine_command_pending.machine_id = device.device_name
		    and upper(machine_command_pending.data_type) = ?
		)
		and not exists
		(
		    select 1
		    from machine_command_pending_hist
		    where machine_command_pending_hist.machine_id = device.device_name
		    and machine_command_pending_hist.data_type = ?
		    and machine_command_pending_hist.execute_date > (sysdate - ?)
		)
	        and
                (
                    select count(1)
                    from machine_command_pending_hist
                    where machine_command_pending_hist.machine_id = device.device_name
                    and upper(machine_command_pending_hist.data_type) = ?
                    and machine_command_pending_hist.execute_date > (sysdate - ?)
                ) <= ?
		and rownum <= ?
	};

	my $mainapp_transfer_query = $dbh->query(
		query	=> $mainapp_transfer_sql,
		values	=>
		[
			$last_activity_days,
			(@old_mainapp_values),
			$last_updated_days,
			(@new_diag_values),
			$last_updated_days,
			$ext_file_trans_datatype,
			$ext_file_trans_datatype,
			$recent_transfer_days,
                        $ext_file_trans_datatype,
                        $last_activity_days,
                        $max_recent_transfers,
			$max_mainapp_transfer
		]
	);

	my $mainapp_transfer_count = 0;
	my @mainapp_transfer_list = ();
	my @mainapp_transfer_names = ();  
	foreach my $mainapp_transfer_ref (@{$mainapp_transfer_query})
	{
		push(@mainapp_transfer_list, [$mainapp_transfer_ref->[0], $mainapp_transfer_ref->[1], $mainapp_transfer_ref->[2], $mainapp_transfer_ref->[3]]);
		push(@mainapp_transfer_names, $mainapp_transfer_ref->[2]);
		$mainapp_transfer_count++;
	}
	
	if($mainapp_transfer_count > 0)
	{
		&output("RESULT   : Creating $mainapp_transfer_count main app file transfers: " . join(",", @mainapp_transfer_names));
		
		if(!$debug)
		{
			my ($octet1, $octet2, $octet3, $octet4) = split(/\./,$mainapp_server_ip);
			$octet1 = chr($octet1);
			$octet2 = chr($octet2);
			$octet3 = chr($octet3);
			$octet4 = chr($octet4);
	
			my $login_name_length = chr(length($mainapp_login_name));
			my $login_password_length = chr(length($mainapp_password));
			my $file_path_name_length = chr(length($mainapp_path_name));
			my $encryption_key_length = chr(length($mainapp_key)/2);
		
			foreach my $mainapp_transfer_ref (@mainapp_transfer_list)
			{
				my $transfer_start_gmtime = &getAdjustedTimestamp($mainapp_transfer_ref->[3]);
				my $transfer_end_gmtime = $transfer_start_gmtime + ($mainapp_exp_days * 86400);
		
				my $message = pack("aaaaa",$mainapp_protocol, $octet1, $octet2, $octet3, $octet4);
				$message .= pack("n", $mainapp_port_num);
				$message .= pack("aa*", $login_name_length, $mainapp_login_name);
				$message .= pack("aa*", $login_password_length, $mainapp_password);
				$message .= pack("aa*", $file_path_name_length, $mainapp_path_name);
				$message .= pack("H*", $mainapp_file_format);
				$message .= pack("H*", $mainapp_encapsulation);
				$message .= pack("H*", $mainapp_crc_type);
				$message .= pack("H*", $mainapp_crc);
				$message .= pack("a", $encryption_key_length);
				$message .= pack("H*", $mainapp_key);
				$message .= pack("NN", $transfer_start_gmtime, $transfer_end_gmtime);
				$message .= pack("aa*", chr($mainapp_retries), chr($mainapp_retry_interval));
				
				&output(" Main app transfer command: " . $mainapp_transfer_ref->[1] . ": " . unpack("H*",$message));
		
				my $mainapp_transfer_insert = $dbh->query(
					query	=>
						q{ 
							insert into machine_command_pending(machine_id, execute_cd, execute_order, data_type, command)
							values (?, ?, ?, ?, ?)
						},
					values	=>
					[
						$mainapp_transfer_ref->[1],
						'P',
						$mainapp_trans_order,
						$ext_file_trans_datatype,
						unpack("H*",$message)
					]
				);
			}
		}
		else
		{
			&output("RESULT   : Skipped; DEBUG ON");
		}
	}
	else
	{
		&output("RESULT   : No devices need a main app update");
	}
}

sub output
{
	my $line = shift;
	print "[" . localtime() . "] $line\n";
}

sub getAdjustedTimestamp
{
	my ($timezone, $timestamp) = @_;

	# we need to know if its daylight savings time; if so move the timestamp 'forward' an additional hour
	my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;

	# if a timestamp is not provided grab the system time (which is in GMT)
	if ((!defined $timestamp) or ($timestamp == 0))
	{
		$timestamp = time();
    }

	SWITCH: 
	{
		if ($timezone eq 'CST') {$timestamp -= (3600 * (6 - $isdst)); last SWITCH;}
		if ($timezone eq 'MT') {$timestamp -= (3600 * (7 - $isdst)); last SWITCH;}
		if ($timezone eq 'MST') {$timestamp -= (3600 * (7 - $isdst)); last SWITCH;}
		if ($timezone eq 'PT') {$timestamp -= (3600 * (8 - $isdst)); last SWITCH;}
		if ($timezone eq 'PST') {$timestamp -= (3600 * (8 - $isdst)); last SWITCH;}
		# if the time zone is not identified (or IS set to "EST") adjust the time back
		# only 5 hours (normal time)to adjust to EST
		$timestamp -= (3600 * (5 - $isdst));
	}
	
    return $timestamp;
}

1;
