#!/usr/local/bin/perl -w

#legacy handler auth-debit-refund test (assumes 1 refund can be attached to same transaction as the sale-settlement

use SOAP::Lite +trace;
use strict;
use Data::Dumper;
use USAT::Database;

sub main {
	### input args ###
	my ($device_id, $host_id, $client_payment_type_cd, $acct_cd) 
		= (shift @ARGV || 13192, shift @ARGV || 2419, shift @ARGV || 'S', shift @ARGV || '4001');
	my ($error_id, $response, $result);
	
	my $amt = 3.00;
	my $device_tran_cd = time();
	print "Process will attempt to auth and debit \$$amt against account '$acct_cd'\n\n";
	push @ARGV, (
		
		$acct_cd
		,3			#magnetic stripe
		,undef
		,$device_id
		,$device_tran_cd
		,$client_payment_type_cd
		,$amt
		,'Q'		#result code--generic success case
		,$device_tran_cd
		
		,2419
		,$amt / 3
		,0
		,1
		,'Regular Wash Cycle, Washer 1'
		,1
		,undef
	
		,2420
		,$amt / 3
		,0
		,1
		,'Regular Wash Cycle, Washer 2'
		,1
		,undef
	
		,2421
		,$amt / 3
		,0
		,1
		,'Regular Wash Cycle, Washer 3'
		,1
		,undef
	
	);
	
	### parse input args ###
	my @parameters;
	push @parameters, SOAP::Data->name('AcctData')			-> type('string')	-> value(shift @ARGV);
	push @parameters, SOAP::Data->name('EntryMethod')		-> type('string')	-> value(shift @ARGV);
	push @parameters, SOAP::Data->name('PinData')			-> type('integer')	-> value(shift @ARGV);
	push @parameters, SOAP::Data->name('DeviceId')			-> type('integer')	-> value(shift @ARGV);
	push @parameters, SOAP::Data->name('DeviceTranCd')		-> type('integer')	-> value(shift @ARGV);
	push @parameters, SOAP::Data->name('CardType')			-> type('string')	-> value(shift @ARGV);
	push @parameters, SOAP::Data->name('Amount')			-> type('double')	-> value(shift @ARGV);
	push @parameters, SOAP::Data->name('DeviceResultTypeCd')-> type('string')	-> value(shift @ARGV);
	push @parameters, SOAP::Data->name('TsLocal')			-> type('date')		-> value(epoch_sec_to_wdsl_datetime((shift @ARGV) - (3600 * (5 - (localtime())[8]))));
	my $line_item_num = 0;
	my @line_items;
	while (@ARGV) {
		my @values;
		$line_item_num++;
		push @values, SOAP::Data->name('HostId')		-> type('integer')	-> value(shift @ARGV);
		push @values, SOAP::Data->name('Amount')		-> type('double')	-> value(shift @ARGV);
		push @values, SOAP::Data->name('Tax')			-> type('double')	-> value(shift @ARGV);
		push @values, SOAP::Data->name('ItemTypeId')	-> type('integer')	-> value(shift @ARGV);
		push @values, SOAP::Data->name('Desc')			-> type('string')	-> value(shift @ARGV);
		push @values, SOAP::Data->name('Quantity')		-> type('integer')	-> value(shift @ARGV);
		push @values, SOAP::Data->name('PositionCd')	-> type('string')	-> value(shift @ARGV);
		push @line_items, \SOAP::Data->value(@values);
	}
	push @parameters, SOAP::Data->name("LineItem" => \SOAP::Data->value(@line_items));
	
	### call POSM and do auth-sale batch ###
	$response = SOAP::Lite
		-> uri('http://localhost:32080/')
		-> proxy('http://localhost:32080/?session=POSM')
		-> AuthSaleBatch(@parameters);
	$result = $response->result;
	print Dumper($result);
	$error_id = $result->[0]->[0];
	my $tran_id = $result->[0]->[1];
	die "Error $error_id during initial creation of transaction.\n" if $error_id;

	### force immediate sale of transaction ###
	$response = SOAP::Lite
		-> uri('http://localhost:32080/')
		-> proxy('http://localhost:32080/?session=POSM')
		-> SaleAuthorized($tran_id);
	$result = $response->result;
	print Dumper($result);
	
	$error_id = $result->[0]->[0];
	die "Error $error_id during initial sale of transaction.\n" if $error_id;
}

main();

########################################################################
sub epoch_sec_to_wdsl_datetime ($) {
	my $epoch_sec = shift;
	my $timestamp_xml_format;
	if ($SOAP::Lite::VERSION <= 0.60) {
		my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime($epoch_sec);
		$timestamp_xml_format = sprintf("%04d-%02d-%02dT%02d:%02d:%02d", ($year+1900), ($mon+1), $mday, $hour, $min, $sec);
	}
	else {
		$timestamp_xml_format = SOAP::Utils::format_datetime(gmtime($epoch_sec));
	}
	return $timestamp_xml_format;
}
