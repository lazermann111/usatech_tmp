#!/usr/bin/perl
#
# This script will create a pending Peek request for the entire config of E-Ports.
# 
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		09/16/2005	pcowan		Adapted from request_mei_info.pl
#

use strict;

use DBI;
use USAT::Database;
use DatabaseConfig;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not $dbh)
{
        print localtime() . "\tFailed to connect to the database!\n";
        exit();
}

my $data_type		= '87';
my $command		= '4200000000000000FF';
my $days_old		= 30;
my $max_requests	= 300; 

my $insert_peek_stmt = $dbh->prepare( q{

insert into machine_command_pending(machine_id, data_type, command, execute_cd, execute_order) 
select device.device_name, :data_type, :command, 'P', 99
from device 
where device.device_type_id in (0,1)
and device.device_active_yn_flag = 'Y' 
and device.last_activity_ts > (sysdate - :days_old)
and not exists 
(
     select 1     
     from machine_command_pending     
     where machine_command_pending.data_type = :data_type
     and machine_command_pending.command = :command
     and machine_command_pending.execute_cd in ('P', 'S')   
     and device.device_name = machine_command_pending.machine_id 
) 
and not exists 
(
    select 1
    from device_file_transfer, file_transfer
    where device_file_transfer.file_transfer_id = file_transfer.file_transfer_id
    and file_transfer.file_transfer_type_cd = 1
    and device_file_transfer.device_file_transfer_direct = 'I'
    and device_file_transfer.device_file_transfer_status_cd = 1
    and file_transfer.last_updated_ts > (sysdate - :days_old)
    and device.device_id = device_file_transfer.device_id
)
and rownum <= :max_requests

});

if(not $insert_peek_stmt)
{
        print localtime() . "\tFailed to prepare select statement!\n";
        exit();
}

$insert_peek_stmt->bind_param(":data_type", $data_type);
$insert_peek_stmt->bind_param(":command", $command);
$insert_peek_stmt->bind_param(":days_old", $days_old);
$insert_peek_stmt->bind_param(":max_requests", $max_requests);
$insert_peek_stmt->execute();
$insert_peek_stmt->finish();

$dbh->disconnect;

