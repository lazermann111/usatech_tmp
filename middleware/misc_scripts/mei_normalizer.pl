#!/usr/bin/perl

use strict;
use DBI;
use USAT::Database;
use DatabaseConfig;
use Getopt::Long;
use Time::HiRes qw(gettimeofday time);


my $oldfh = select(STDOUT); $| = 1; select($oldfh); #force auto-flush of STDOUT; otherwise, lines may be out of order

my $debug;

# Check for input from command line; validate
my $argv_result = GetOptions(
	"debug=i" => \$debug
);

# Check for debug
if (!(defined $debug)) {
	$debug = 0;
}


print_log("Process started ...");
## DEBUG
if ($debug != 0) {
	print_log("Running in Debug mode (devices will not be updated).");
}

my $db_config = DatabaseConfig->new();
my $db = USAT::Database->new(config => $db_config);


#-- Fill the temp table with actual call in times
print_log("Fetching the actual call in times into tmp_device_call_in_time ...");

my $num_rows = $db->do(
	query => q{
INSERT INTO tracking.tmp_device_call_in_time 
(device_id, device_type_id, call_in_hour, call_in_min, retry_interval, tz_offset, actual_call_in_time, last_full_config, last_activity_ts, new_call_in_time, adjusted)
SELECT
cci.device_id,
cci.device_type_id,
cci.call_in_hour,
cci.call_in_min,
cci.retry_interval,
NVL(tz.time_zone_offset, 0) tz_offset,
(MOD(
	cci.call_in_hour + 
	NVL2(tz.time_zone_offset, tz.time_zone_offset, 0) + 
	TRUNC((cci.call_in_min) / 60) + 24, 24
) * 100)
+ MOD(cci.call_in_min, 60) actual_call_in_time,
cci.last_full_config,
cci.last_activity_ts,
0 new_call_in_time,
'N' adjusted
FROM (
	SELECT 
	machc.device_id,
	machc.device_type_id,
	machc.last_activity_ts,
	machc.pending_cmd_count,
	machc.last_phone_in_update,
	machc.last_full_config,
	FLOOR(TO_NUMBER(device.FILE_CONTENT_SUBSTR(ft.rowid, 17, 2)) / 60) call_in_hour,
	MOD(TO_NUMBER(device.FILE_CONTENT_SUBSTR(ft.rowid, 17, 2)), 60) call_in_min,
	TO_NUMBER(device.FILE_CONTENT_SUBSTR(ft.rowid, 19, 1)) retry_interval
	FROM file_transfer ft
	JOIN (
		SELECT 
		dev.device_id, 
		dev.device_type_id, 
		dev.device_name,
		dev.last_activity_ts,
		NVL(mcpc.pending_command_count, 0) pending_cmd_count,
		GREATEST(NVL(MAX(mcphp.execute_date), SYSDATE - 365), NVL(MAX(mcphpi.execute_date), SYSDATE - 365)) last_phone_in_update,
		MAX(mcphc.execute_date) last_full_config
		FROM device.device dev
		
		LEFT OUTER JOIN machine_command_pending_hist mcphp -- this gets the last phone in time update
		ON mcphp.machine_id = dev.device_name
		AND mcphp.data_type = 88
		AND mcphp.command = '420000000000000039'
		
		LEFT OUTER JOIN machine_command_pending_hist mcphpi -- this gets the other last phone in time update
		ON mcphpi.machine_id = dev.device_name
		AND mcphpi.data_type = 88
		AND mcphpi.command = '420000000000000043'
				
		LEFT OUTER JOIN machine_command_pending_hist mcphc -- this gets the last full config update
		ON mcphc.machine_id = dev.device_name
		AND mcphc.data_type = 87
		AND mcphc.command = '4200000000000000FF'
		
		LEFT OUTER JOIN ( -- this gets any pending phone in time update commands
			SELECT mcp.machine_id, COUNT(1) pending_command_count
			FROM machine_command_pending mcp
			WHERE mcp.execute_cd IN ('P', 'S')
			AND mcp.data_type = '88'
			AND (mcp.command = '420000000000000039' OR mcp.command = '420000000000000043')
			GROUP BY mcp.machine_id
		) mcpc
		ON mcpc.machine_id = dev.device_name
		WHERE dev.device_type_id = 6
		AND dev.device_active_yn_flag = 'Y'
		AND dev.last_activity_ts > (SYSDATE - 30)
		GROUP BY
		dev.device_id,
		dev.device_type_id,
		dev.device_name,
		dev.last_activity_ts,
		NVL(mcpc.pending_command_count, 0)
	) machc
	ON machc.device_name = SUBSTR(ft.file_transfer_name, 0, 8)
	AND ft.file_transfer_type_cd = 1
	AND (machc.last_full_config IS NULL OR machc.last_full_config > (SYSDATE - 45))
	AND (machc.last_phone_in_update IS NULL OR machc.last_full_config > machc.last_phone_in_update)
	AND machc.pending_cmd_count = 0
) cci
LEFT OUTER JOIN (
	SELECT 
	dev.device_id,
	(CASE 
	    WHEN loc.location_time_zone_cd = 'CST' THEN 1
	    WHEN loc.location_time_zone_cd = 'CDT' THEN 1
	    WHEN loc.location_time_zone_cd = 'CET' THEN 1
	    WHEN loc.location_time_zone_cd = 'MST' THEN 2
	    WHEN loc.location_time_zone_cd = 'MDT' THEN 2
	    WHEN loc.location_time_zone_cd = 'PST' THEN 3
	    WHEN loc.location_time_zone_cd = 'PDT' THEN 3
	    
	    
	    WHEN loc.location_time_zone_cd = 'HST' THEN 6
	    WHEN loc.location_time_zone_cd = 'HDT' THEN 6
	    
	    WHEN loc.location_time_zone_cd = 'AST' THEN -1
	    WHEN loc.location_time_zone_cd = 'ADT' THEN -1
	    
	    WHEN loc.location_time_zone_cd = 'WET' THEN -5
	    
	    WHEN loc.location_time_zone_cd = 'ACST' THEN -15
	    WHEN loc.location_time_zone_cd = 'ACDT' THEN -15
	    
	    ELSE -24
	END) time_zone_offset
	FROM device.device dev
	JOIN pss.pos pos
	ON pos.device_id = dev.device_id
	AND pos.pos_active_yn_flag = 'Y'
	AND dev.device_type_id = 6
	AND dev.device_active_yn_flag = 'Y'
	JOIN location.location loc
	ON loc.location_id = pos.location_id
	AND loc.location_active_yn_flag = 'Y'
	AND loc.location_time_zone_cd IS NOT NULL
	AND loc.location_time_zone_cd != 'EST'
	AND loc.location_time_zone_cd != 'EDT'
) tz
ON tz.device_id = cci.device_id
}
);

if (defined $num_rows) {
        # query was successful, even if no rows affected
		print_log("Inserted $num_rows rows.");
} else {
        # sql or database error
		print_log("Unable to execute insert into tmp_device_call_in_time (database or query error)!");
}


## DEBUG
#-- Check how many devices we have that can be altered
if ($debug != 0) {
	my $device_count = 0;
	
	my $device_count_rs = $db->query(
		query => q{
	SELECT COUNT(1) device_count
	FROM tracking.tmp_device_call_in_time
	}
	);
	
	if (defined $device_count_rs->[0]) {
		$device_count = $device_count_rs->[0][0];
	}
	
	print_log("Devices that can be altered: $device_count");
}
## END DEBUG


print_log("Fetching the maximum average device per minute for the high priority zone ...");
#-- Get the maximum average device per minute for the high priority zone (between 11 PM and 8 AM)
my $max_dev_per_min = 0;

my $avg_device_count_rs = $db->query(
	query => q{
SELECT CEIL((COUNT(1) / 540)) device_per_min
FROM device.device dev
WHERE dev.device_type_id = 6
AND dev.device_active_yn_flag = 'Y'
AND dev.last_activity_ts > (SYSDATE - 30)
}
);

if (defined $avg_device_count_rs->[0]) {
	$max_dev_per_min = $avg_device_count_rs->[0][0];
}

$max_dev_per_min++; # Add one to this number to give us a little wiggle room 

print_log("Max devices per min: $max_dev_per_min");


print_log("Fetching the allowed calls per slot in the high priority zone ...");
#-- Get the allowed calls per slot in the high priority zone
my @hp_device_counts;
my $hp_device_count_rs = $db->query(
	query => q{
SELECT 
call_in_time,
? - CEIL(((time_priority / 10) - .5) * ?) allowed_callins,
0 actual_callins
FROM (
    SELECT 
	SUBSTR('00' || TO_CHAR(hrs.cntr - 1), -2, 2) || SUBSTR('00' || TO_CHAR(mins.cntr - 1), -2, 2) call_in_time,
	(CASE
	    WHEN hrs.cntr - 1 = 22 THEN 2 
	    WHEN hrs.cntr - 1 = 23 THEN 3
	    WHEN hrs.cntr - 1 = 0 THEN 4
	    WHEN hrs.cntr - 1 = 1 THEN 5
	    WHEN hrs.cntr - 1 = 2 THEN 6
	    WHEN hrs.cntr - 1 = 3 THEN 5
	    WHEN hrs.cntr - 1 = 4 THEN 4
	    WHEN hrs.cntr - 1 = 5 THEN 3
	    WHEN hrs.cntr - 1 = 6 THEN 2
	    ELSE 0
	END) +
	(CASE 
	    WHEN hrs.cntr - 1 > 6 AND hrs.cntr - 1 < 22 THEN 0
	    WHEN MOD(mins.cntr - 1, 60) < 5 THEN 4
	    WHEN MOD(mins.cntr - 1, 5) = 0 THEN 3
	    WHEN MOD(mins.cntr - 1, 5) = 1 THEN 2
	    WHEN MOD(mins.cntr - 1, 5) = 4 THEN 1
	    WHEN MOD(mins.cntr - 1, 5) = 3 THEN 0
	    WHEN MOD(mins.cntr - 1, 5) = 2 THEN 0
	END) 
	 time_priority
	FROM (
	    SELECT LEVEL cntr
	    FROM dual 
	    CONNECT BY LEVEL <= 24
	) hrs, (
	    SELECT LEVEL cntr
	    FROM dual 
	    CONNECT BY LEVEL <= 60
	) mins
)
WHERE time_priority > 0
ORDER BY call_in_time
},
	values => [$max_dev_per_min, $max_dev_per_min]      #optional argument, list of bind values
);

if (defined $hp_device_count_rs->[0]) {

	@hp_device_counts = @$hp_device_count_rs;
	
	print_log("Fetching the actual calls per slot ...");
	#-- Get the actual calls per slot
	my $act_device_count_rs = $db->query(
		query => q{
SELECT actual_call_in_time, COUNT(1) device_count
FROM (
	SELECT
	SUBSTR('0000' ||
		TO_CHAR((
			MOD(
				cci.call_in_hour + 
				NVL2(tz.time_zone_offset, tz.time_zone_offset, 0) + 
				TRUNC((cci.call_in_min) / 60) + 24, 24
			) * 100)
			+ MOD(cci.call_in_min, 60)
		), -4, 4
	) actual_call_in_time
	FROM (
		SELECT 
		device.device_serial_cd,
        FLOOR(TO_NUMBER(device.FILE_CONTENT_SUBSTR(file_transfer.rowid, 17, 2)) / 60) call_in_hour,
        MOD(TO_NUMBER(device.FILE_CONTENT_SUBSTR(file_transfer.rowid, 17, 2)), 60) call_in_min,
        TO_NUMBER(device.FILE_CONTENT_SUBSTR(file_transfer.rowid, 19, 1)) retry_interval
		FROM device, file_transfer 
		WHERE device.device_name = SUBSTR(file_transfer.file_transfer_name, 0, 8) 
		AND device.device_type_id = 6
		AND device.device_active_yn_flag = 'Y'
		AND file_transfer.file_transfer_type_cd = 1 
		--AND file_transfer.last_updated_ts > (SYSDATE - 45)
		AND device.last_activity_ts > (SYSDATE - 30)
	) cci
	LEFT OUTER JOIN (
		SELECT 
		dev.device_serial_cd,
		(CASE 
		    WHEN loc.location_time_zone_cd = 'CST' THEN 1
		    WHEN loc.location_time_zone_cd = 'CDT' THEN 1
		    WHEN loc.location_time_zone_cd = 'CET' THEN 1
		    WHEN loc.location_time_zone_cd = 'MST' THEN 2
		    WHEN loc.location_time_zone_cd = 'MDT' THEN 2
		    WHEN loc.location_time_zone_cd = 'PST' THEN 3
		    WHEN loc.location_time_zone_cd = 'PDT' THEN 3
		    
		    
		    WHEN loc.location_time_zone_cd = 'HST' THEN 6
		    WHEN loc.location_time_zone_cd = 'HDT' THEN 6
		    
		    WHEN loc.location_time_zone_cd = 'AST' THEN -1
		    WHEN loc.location_time_zone_cd = 'ADT' THEN -1
		    
		    WHEN loc.location_time_zone_cd = 'WET' THEN -5
		    
		    WHEN loc.location_time_zone_cd = 'ACST' THEN -15
		    WHEN loc.location_time_zone_cd = 'ACDT' THEN -15
		    
		    ELSE -24
		END) time_zone_offset
		FROM device.device dev
		JOIN pss.pos pos
		ON pos.device_id = dev.device_id
		AND pos.pos_active_yn_flag = 'Y'
		AND dev.device_type_id = 6
		AND dev.device_active_yn_flag = 'Y'
		JOIN location.location loc
		ON loc.location_id = pos.location_id
		AND loc.location_active_yn_flag = 'Y'
		AND loc.location_time_zone_cd IS NOT NULL
		AND loc.location_time_zone_cd != 'EST'
		AND loc.location_time_zone_cd != 'EDT'
	) tz
	ON tz.device_serial_cd = cci.device_serial_cd
)
WHERE (TO_NUMBER(actual_call_in_time) < 700 OR TO_NUMBER(actual_call_in_time) >= 2200)
GROUP BY actual_call_in_time
	}
	);
	
	if (defined $act_device_count_rs->[0]) {
		print_log("Updating the hp_device_counts Array ...");
		foreach my $act_device_count (@{$act_device_count_rs}) {
			my $actual_call_in_time = $act_device_count->[0];
			my $actual_device_count = $act_device_count->[1];
			
			for (my $i = 0; $i < scalar(@hp_device_counts); $i++) {
				if ($hp_device_counts[$i][0] eq $actual_call_in_time) {
					$hp_device_counts[$i][2] = $actual_device_count;
					last;
				}
			}
		}
	}
	
}


## DEBUG
if ($debug != 0) {
	print_log("Printing the Main Array ...");
	for (my $i = 0; $i < scalar(@hp_device_counts); $i++) {
		print "$hp_device_counts[$i][0],$hp_device_counts[$i][1],$hp_device_counts[$i][2]\n";
	}
}
## END DEBUG


print_log("Creating and sorting the Opens Array ...");
#-- Create a new 2 dim array of slots with opens (Opens Array)
my @hp_opens = ();
my $j = 0;
for (my $i = 0; $i < scalar(@hp_device_counts); $i++) {
	if ($hp_device_counts[$i][1] > $hp_device_counts[$i][2]) {
		my @temp = ($hp_device_counts[$i][0], $hp_device_counts[$i][1] - $hp_device_counts[$i][2]);
		push(@hp_opens, \@temp);
		$j++;
	}
}
#-- Sort the Opens Array by the number of available slots and reverse it
my $pos = 1;
@hp_opens = sort {$a->[$pos] <=> $b->[$pos]} @hp_opens;
@hp_opens = reverse(@hp_opens);


## DEBUG
if ($debug != 0) {
	print_log("Printing the Opens Array ...");
	for (my $i = 0; $i < scalar(@hp_opens); $i++) {
		print "$hp_opens[$i][0],$hp_opens[$i][1]\n";
	}
}
## END DEBUG


#-- setup the cutoff for smoother load balancing
#-- the cutoff is used to get slots with at least a given number of opens
#-- the cutoff is lowered 5 times (5th time reaching zero)
my $cutoff_factor = int(($max_dev_per_min / 5) + .5);
my $cutoff_counter = 1;
my $cutoff = $max_dev_per_min - ($cutoff_factor * $cutoff_counter);
my $opens_pointer = 0;

my $reassigned_counter = 0;
my $error_counter = 0;
my $skipped_counter = 0;

print_log("Correcting slots in Overage ...");
#-- Correct slots in Overage
for (my $i = 0; $i < scalar(@hp_device_counts); $i++) {
	if ($hp_device_counts[$i][1] < $hp_device_counts[$i][2]) {
		
		my $current_call_in_time = $hp_device_counts[$i][0];
		my $current_call_in_time_int = int($current_call_in_time);
		my $devices_to_move = $hp_device_counts[$i][2] - $hp_device_counts[$i][1];
		
		my $device_count = 0;
		my $device_to_correct_rs = $db->query(
			query => q{
SELECT dcit.device_id, 
dcit.device_type_id,
dcit.retry_interval,
dcit.tz_offset
FROM tracking.tmp_device_call_in_time dcit
WHERE dcit.actual_call_in_time = ?
AND ROWNUM <= ?
ORDER BY dcit.last_full_config, dcit.last_activity_ts DESC
		},
			values => [$current_call_in_time_int, $devices_to_move]
		);
		
		if (defined $device_to_correct_rs->[0]) {
			foreach my $device_to_correct (@{$device_to_correct_rs}) {
				my $device_id = $device_to_correct->[0];
				my $device_type_id = $device_to_correct->[1];
				my $retry_interval = $device_to_correct->[2];
				my $tz_offset = $device_to_correct->[3];
				
				my $new_call_in_time = "";
				
				while ($new_call_in_time eq "") {
					# find the next open slot to move this device to
					if ($hp_opens[$opens_pointer][1] >= $cutoff) {
						# we have open slots here
						$new_call_in_time = $hp_opens[$opens_pointer][0];
						$hp_opens[$opens_pointer][1]--;
					} else {
						# move to the next slot and check the cutoff
						$opens_pointer++;
						if ($opens_pointer >= scalar(@hp_opens)) {
							if ($cutoff_counter < 5) {
								# we need to lower the cutoff and move back to the top of the array
								$cutoff_counter++;
								$opens_pointer = 0;
								# recalculate the cutoff
								$cutoff = $max_dev_per_min - ($cutoff_factor * $cutoff_counter);
								# go to next itteration
								next;
							} elsif ($cutoff_counter == 5) {
								# set cutoff to 1 to catch the last set of opens and move back to the top of the array
								$cutoff_counter++;
								$opens_pointer = 0;
								$cutoff = 1;
								# go to next itteration
								next;
							} else {
								# we have moved through the entire opens array so break out of the loop
								last;
							}
						}
					}
				} # end while ($new_call_in_time eq "")
				
				
				if ($new_call_in_time eq "") {
					# we couldn't find an open slot
					# we are done
					print_log("No more open slots, last cutoff: $cutoff (this shouldn't happen).");
					last;
				} else {
					# we need to calculate the corrected call in time from the TZ
					# pull the mins and hours out
					my $new_call_in_hour_int = int(substr($new_call_in_time, 0, 2));
					my $new_call_in_min_int = int(substr($new_call_in_time, 2, 2));
					
					my $corrected_call_in_hour_int = ($new_call_in_hour_int - $tz_offset + 24) % 24;
					my $corrected_call_in_time = substr("0000" . (($corrected_call_in_hour_int * 60) + $new_call_in_min_int), -4);
					
					my $return_code = update_device_config($db, $device_id, $device_type_id, $corrected_call_in_time, $new_call_in_time, $current_call_in_time, $tz_offset, $retry_interval, $debug);
					if ($return_code == 1) {
						$reassigned_counter++;
					} elsif ($return_code < 0) {
						$error_counter++;
					} else {
						$skipped_counter++;
					}
					
				} # end if ($new_call_in_time eq "")
				
			} # end foreach
			
			
			
		} # end if (defined $device_to_correct_rs->[0])
		
		
	}
}

print_log("Finished, reassigned: $reassigned_counter, skipped: $skipped_counter, errors: $error_counter.");



sub update_device_config {
	my($db, $device_id, $device_type_id, $corrected_call_in_time, $new_call_in_time, $current_call_in_time, $tz_offset, $retry_interval, $debug) = @_;
	
	# we will do the update here (using a stored proc)...
	my ($return_code, $return_msg, $db_err_str);
	my $sp_update_config_ref = $db->callproc(
		name => 'sp_update_config',
		bind => [':p_device_id', ':p_offset', ':p_new_data', ':p_data_type', ':p_device_type_id', ':p_debug', ':p_return_code', ':p_return_msg'],
		in => [$device_id, 17, $corrected_call_in_time, 'H', $device_type_id, $debug],
		inout => [40, 2048],
	);
	
	$db_err_str = $db->errstr;
	
	$return_code = $sp_update_config_ref->[0];
	$return_msg = $sp_update_config_ref->[1];
	
	if($return_code < 0) {
		print_log("Update of deviceID $device_id call-in failed: Error creating Poke! $return_msg, $db_err_str");
	} elsif($return_code == 1) {
		print_log("Update of deviceID $device_id call-in succeeded (Current Adjusted: $current_call_in_time, New Raw: $new_call_in_time, TZ: $tz_offset, Retry: $retry_interval): $return_msg");
	} else {
		print_log("Update of deviceID $device_id call-in skipped (Current Adjusted: $current_call_in_time, New Raw: $new_call_in_time, TZ: $tz_offset, Retry: $retry_interval): no change ($return_msg)");
	}
	
	return $return_code;
}



sub print_log {
	my $message = shift;
	print log_date_stamp() .  " [".sprintf("%5d", $$)."] $message\n";
}

sub log_date_stamp {
	my @epoch_sec = gettimeofday();
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($epoch_sec[0]);
	return sprintf('%04d-%02d-%02d %02d:%02d:%02d.', ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec).substr(sprintf('%06d', $epoch_sec[1]), 0, 3);
}


