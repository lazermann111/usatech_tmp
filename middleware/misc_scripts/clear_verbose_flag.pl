#!/usr/bin/perl

use strict;
use DBI;
use USAT::Database;
use DatabaseConfig;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not $dbh)
{
        print localtime() . "\tFailed to connect to the database!\n";
        exit();
}

my $insert_stmt = $dbh->prepare( q{
insert into machine_command_pending(machine_id, data_type, command, execute_cd, execute_order) 
select device.device_name, '88', '4400807FE001', 'P', 2
from device, pos, customer, location.location
where device.device_id = pos.device_id
and pos.customer_id = customer.customer_id
and pos.location_id = location.location_id
and customer.customer_id = 571
and location.location_name like 'Kings%'
and not exists
(
	select 1
	from machine_command_pending
	where machine_command_pending.machine_id = device.device_name
	and machine_command_pending.data_type = '88'
	and machine_command_pending.command = '4400807FE001'
)
});

$insert_stmt->execute();
$insert_stmt->finish();

$dbh->disconnect;

