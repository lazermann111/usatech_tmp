#!/usr/bin/perl
#
# This script will create a pending Peek request for the entire config of MEI devices.
# 
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		09/08/2005	pcowan		Adapted from request_multitech_info.pl
#

use strict;

use DBI;
use USAT::Database;
use DatabaseConfig;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not $dbh)
{
        print localtime() . "\tFailed to connect to the database!\n";
        exit();
}

my $data_type		= '87';
my $mei_command		= '4200000000000000FF';
my $days_old		= 30;
my $max_requests	= 50;

my $insert_mei_peek_stmt = $dbh->prepare( q{

insert into machine_command_pending(machine_id, data_type, command, execute_cd, execute_order) 
select device.device_name, :data_type, :mei_command, 'P', 99
from device 
where device.device_type_id = 6
and device.device_active_yn_flag = 'Y' 
and device.last_activity_ts > (sysdate - :days_old)
and not exists 
(
     select 1     
     from machine_command_pending     
     where machine_command_pending.data_type = :data_type
     and machine_command_pending.command = :mei_command
     and machine_command_pending.execute_cd in ('P', 'S')   
     and device.device_name = machine_command_pending.machine_id 
) 
and not exists 
(
    select 1
    from file_transfer
    where file_transfer.file_transfer_name = device.device_name || '-CFG'
    and file_transfer.last_updated_ts > (sysdate - :days_old)
)
and rownum <= :max_requests

});

if(not $insert_mei_peek_stmt)
{
        print localtime() . "\tFailed to prepare MEI statement!\n";
        exit();
}

$insert_mei_peek_stmt->bind_param(":data_type", $data_type);
$insert_mei_peek_stmt->bind_param(":mei_command", $mei_command);
$insert_mei_peek_stmt->bind_param(":days_old", $days_old);
$insert_mei_peek_stmt->bind_param(":max_requests", $max_requests);
$insert_mei_peek_stmt->execute();
$insert_mei_peek_stmt->finish();

$dbh->disconnect;

