#!/usr/bin/perl

use OOCGI::Query;
use USAT::DeviceAdmin::DBObj;
use Getopt::Long qw(VersionMessage);

my $customer_name = "";
Getopt::Long::Configure(qw(no_ignore_case));
GetOptions(
   'c|customer_name=s' => \$customer_name
);

my $sql = q|
    SELECT distinct d.device_id
      FROM device d 
          INNER JOIN device_setting ds 
             ON d.device_id = ds.device_id
            AND ds.device_setting_parameter_cd = 'Firmware Version'
            AND ds.device_setting_value like '%v5.0.0%'
          INNER JOIN pos p
             ON p.device_id = d.device_id
          INNER JOIN customer c 
             ON p.customer_id = c.customer_id
            AND LOWER(c.customer_name) like ?
     WHERE device_type_id = 1
       AND device_active_yn_flag = 'Y'|;

print "CUSTOMER_NAME = $customer_name \n";
my $qo = OOCGI::Query->new($sql, { bind => [ lc($customer_name).'%'] } );

while(my @arr = $qo->next_array) {
    my $device = USAT::DeviceAdmin::DBObj::Device->new($arr[0]);

    my $device_name = $device->device_name;
    my $device_name_length = length($device_name);

=pod
    print $device->ID;
    print " ";
    print $device_name;
    print " ";
    print $device->Customer->customer_name;
    print "\n";
=cut

    if(defined $device_name && $device_name ne '' && $device_name_length eq '8') {
       my $mcp = USAT::DeviceAdmin::DBObj::Machine_cmd_pending->new;
       $mcp->machine_id($device_name);
       $mcp->data_type('87');
       $mcp->command('4400807EB20000004C');
       $mcp->execute_cd('P');
       $mcp->execute_order('18');
       $mcp->insert;
       print "Success $device_name \n";
    } else {
       # skip this record;
       print "Failure $device_name \n";
    }
}
