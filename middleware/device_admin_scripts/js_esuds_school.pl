#!/usr/local/USAT/bin/perl

use OOCGI::Query;

# this section generates the list which starts with 
# lower or upper case letters
# ------------------------------------------------------------
my @arr = ('A','B','C','D','E','F','G','H','I','J','K','L','M',
           'N','O','Q','P','R','S','T','U','V','W','X','Y','Z');

my $string1 = q|
with(milonic=new menuname("esuds_school")) {
style=HeadersStyle;
overflow="scroll";
align="center";
aI("text=School;type=header;");
|;

my $string2 = '';
foreach my $key ( @arr ) {
   my $sql = qq{
          SELECT location_id,
                 location_name
            FROM location.location
           WHERE location_active_yn_flag = 'Y'
             AND location_type_id = 6
             AND UPPER(location_name) like ?
        ORDER BY location_name};

   my $qo = OOCGI::Query->new($sql, { bind => "$key%" });

   if($qo->rowNumber > 0 ) {
   my $lc_key = lc($key);
   $string1 .= qq|aI("showmenu=esud_|.$lc_key.qq|;text=$key;");\n|;
   $string2 .= qq|with(milonic=new menuname("esud_|.lc($key).q|")){|."\n";
   $string2 .= qq|overflow="scroll";|."\n";
   $string2 .= qq|style=menuStyle;|."\n";
   #aI("text=American University;url=javascript:esuds(1391);")
   while(my @arr = $qo->next) {
      $string2 .= qq|aI("text=|
         .quotemeta($arr[1]);
#        qq|;url=javascript:esuds(|.$arr[0].q|);")|;
         if($arr[0] > 0) {
            $string2 .= qq|;url=/location_tree.cgi?action=Search&source_id=$arr[0];")|;
	     }
      $string2 .= "\n";
   }
   $string2 .= "}\n\n";
   }
}

$string1 .= q{aI("text=Public Web Sites;type=header;url=/esuds_public_web_site.cgi");};
print $string1;
print "}\n\n";
print $string2;

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
