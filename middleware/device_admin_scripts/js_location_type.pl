#!/usr/local/USAT/bin/perl

use USAT::DeviceAdmin::DBObj;

my $sql = qq{
       SELECT location_type_id,
              location_type_desc
         FROM location.location_type
     ORDER BY location_type_desc};

my $qo = OOCGI::Query->new($sql);

print q|
with(milonic=new menuname("loca_type")) {
style=HeadersStyle;
overflow="scroll";
aI("align=center;text=Location Name:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/location_list.cgi name=search><table><tr><td><input name=location_name size=16 onmouseover='showtip(tooltip_sm_locations_search_location_name)' onmouseout='hidetip()'> <input type=submit value=Search></td></tr></table></form>;type=form;");
aI("align=center;text=Location City:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/location_list.cgi name=search><table><tr><td><input name=location_city size=16 onmouseover='showtip(tooltip_sm_locations_search_location_city)' onmouseout='hidetip()'> <input type=submit value=Search></td></tr></table></form>;type=form;");
aI("align=center;text=New Location Name:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/edit_location.cgi name=search><table><tr><td><input name=location_name size=16 onmouseover='showtip(tooltip_sm_locations_new_location)' onmouseout='hidetip()'> <input type=submit value=Next></td></tr></table></form>;type=form;");
aI("align=center;text=Location Type;type=header;");
aI("showmenu=esuds_school;text=eSuds School;");
|;

#aI("text=Airport;url=/location_list.cgi?location_type=10&action=Search;");
while(my %hash = $qo->next_hash) {
   print q|aI("text=|,$hash{location_type_desc},
         q|;url=/location_list.cgi?location_type=|,$hash{location_type_id},
         q|&action=Search;");|
      ;
   print "\n";
}
print "}\n\n";

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
