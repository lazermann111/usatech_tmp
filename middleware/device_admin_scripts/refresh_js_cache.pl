#!/usr/local/USAT/bin/perl

use File::Pid;
use File::Spec;
use USAT::DeviceAdmin::DBObj;
use USAT::App::DeviceAdmin::Config ( :globals ) 1.000001;

### wait for running instances, if any exist ###
use constant EXEC_PATH     => JS_MENU__CRON_SCRIPT_PATH;
use constant TMP_PATH      => File::Spec->tmpdir();
use constant PIDFILE       => TMP_PATH.'/refresh_js_cache.lock';
use constant LOCK_WAIT_SLEEP_SEC => 1;
use constant LOCK_WAIT_MAX_SEC => 180;
use constant USAT_TARGET_FILE_PATH => JS_MENU__JS_SCRIPT_PATH;
use constant MEI_TARGET_FILE_PATH  => JS_MENU__JS_MEI_SCRIPT_PATH;
use constant USAT_TMP_FILE_PATH    => TMP_PATH;

our $pidfile = File::Pid->new({ file => PIDFILE });
my $pidtime  = time() + LOCK_WAIT_MAX_SEC;
while ( my $pid = $pidfile->running ) {
    if(time() < $pidtime) {
	   print 'Lock file exists.  Waiting '.LOCK_WAIT_SLEEP_SEC()." seconds to acquire lock...\n";
	   sleep LOCK_WAIT_SLEEP_SEC;	#wait for process to finish
    } else {
	   die "Wait time expired: Existing process ($pid) still running"
		if $pidfile->running;
       print "Removing stale PID file: ",$pidfile->file,"\n";
    }
}
$pidfile->remove;
$pidfile = File::Pid->new({ file => PIDFILE });
die "Unable to write pidfile '".PIDFILE."': $!"
	unless $pidfile->write;

&script_result_to_file('js_customer.pl',           'DA', JS_MENU__CUSTOMER);
&script_result_to_file('js_location.pl',           'DA', JS_MENU__LOCATION);
&script_result_to_file('js_firmware.pl',           'DA', JS_MENU__FIRMWARE);
&script_result_to_file('js_location_type.pl',      'DA', JS_MENU__LOCATION_TYPE);
&script_result_to_file('js_file_transfer_type.pl', 'DA', JS_MENU__FILE_TRANSFER_TYPE);
&script_result_to_file('js_esuds_school.pl',       'DA', JS_MENU__ESUDS_SCHOOL);
&script_result_to_file('js_authority_payment_mask_bref.pl', 'DA', JS_MENU__AUTHORITY_PAYMENT_MASK_BREF);

#mei
&script_result_to_file('js_mei_customer.pl', 'MEI', JS_MENU__MEI_CUSTOMER);
&script_result_to_file('js_mei_location.pl', 'MEI', JS_MENU__MEI_LOCATION);
&script_result_to_file('js_mei_dates.pl',    'MEI', JS_MENU__MEI_DATES);

END {
	eval { $pidfile->remove; };
}
	

#===================================================================
sub script_result_to_file()    #execute a script, store STDOUT to a file
#===================================================================
# Arg_1 = script to execute; stdout will contain result for file
# Arg_2 = file to save (absolute path)
{
    my $SCRIPT   = shift;
    my $PATHFLAG = shift;
    my $TARGFILE = shift;

    my $TARGPATH;
    my $local_dir;
    my $js_file;
    if($PATHFLAG eq 'DA') {
       $TARGPATH = USAT_TARGET_FILE_PATH;
       ($local_dir, $js_file ) = split(/\//, $TARGFILE); 
    } elsif($PATHFLAG eq 'MEI') {
       $TARGPATH = MEI_TARGET_FILE_PATH;
       $js_file  = $TARGFILE; 
    } else {
       ;
    } 
    my $MYERR = 0;
    chdir EXEC_PATH;
  
    my $oldfile  = $TARGPATH.$TARGFILE;
    my $tempfile = USAT_TMP_FILE_PATH.'/'.$js_file;
    system("./$SCRIPT > $tempfile");
    my $ret = $? >> 8;
    if ( $ret gt 0 ) {
        $MYERR = $ret;
    } else {
        system("diff $oldfile $tempfile > /dev/null");
        my $err = $? >> 8;
        if($err gt 0) {
#          print "DIFFERENT \n";
           system("mv $tempfile $oldfile");
           if($? gt 0) {
              $MYERR = $? >> 8;
           } else {
              my $file_cache = USAT::DeviceAdmin::DBObj::Da_file_cache->new({ da_file_cache_obj_path => $oldfile});
              $file_cache->da_file_cache_last_mod_ts(OOCGI::Query->SYSDATE);
              if(defined $file_cache->ID && $file_cache->ID > 0) {
                 $file_cache->update;
              } else {
                 $file_cache->da_file_cache_obj_path($oldfile);
                 $file_cache->insert;
              }
              
              my $user = 'nobody';
              my ($login,$pass,$uid,$gid) = getpwnam($user) or warn "$user not in passwd file";
              chown $uid,$gid, $oldfile;
              my $mode = 0644;
              chmod $mode, $oldfile;
           }
        } else {
           system("rm $tempfile");
        }
    }
    
    return $MYERR
}
