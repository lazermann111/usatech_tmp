#!/usr/local/USAT/bin/perl

use strict;
use warnings;

use DBI;
use Archive::Zip;
use Text::CSV;
use Net::SMTP;
use Sys::Hostname;
use Date::Calc;
use Getopt::Long;
use Time::HiRes qw(gettimeofday time);

use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::Database;

my $oldfh = select(STDOUT); $| = 1; select($oldfh); #force auto-flush of STDOUT; otherwise, lines may be out of order

########################################################################
#App defaults
my $process_name = $0; #$process_name =~ s/\W/_/go; $process_name =~ s/^_+//o;
our $smtp_server = 'mailhost';
our $to_email = 'monitorallbyemail@usatech.com'; #'erybski@usatech.com';
our $from_email = 'monitor@usatech.com'; #"$process_name@".hostname().".usatech.com";

my $file_name_param;
my $file_prefix = 'USA';
my $file_ext = 'zip';
my $archive_path = '/opt/jobs/DeviceAdmin/gprsdata';
my $debug;

########################################################################
# Check for input from command line; validate
my $argv_result = GetOptions(
	"file=s" => \$file_name_param,
	"debug=i" => \$debug
);

# Connect to server, DL file header description
sub main {
	
	print_log("Process started...");
	
	# Check for debug
	if(!(defined $debug))
	{
		$debug = 0;
	}
	
	if ($debug != 0)
	{
		print_log("Running in debug mode (data will not be inserted and error emails are disabled)...");
	}
	
	# Determine file to download (default: yesterday's file)
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $file_name_txt = sprintf("%d%02d%02d", Date::Calc::Add_Delta_Days($year+1900,$mon+1,$mday, -1));
	my $file_name;
	my $file_date;
	if(defined $file_name_param)
	{
 		$file_name = $file_name_param;
		my @file_name_split = split(/\./, $file_name_param);
		$file_name_txt = $file_name_split[0];
	}
	else
	{
	 	$file_name = "$file_prefix$file_name_txt\.$file_ext";
		$file_name_txt = "$file_prefix$file_name_txt";
	}
	
	$file_date = substr($file_name_txt, 3);
	
	print_log("file_name = $file_name");
	print_log("file_name_txt = $file_name_txt");
	print_log("file_date = $file_date");
	
	
	# Check to see if data has already been inserted to the DB for this day
	my $dbh = USAT::Database->new(AutoCommit => 0);
	my $date_count_rs = $dbh->query(
		query => q{
SELECT COUNT(1) date_count
FROM device.gprs_daily_usage
WHERE report_date = TO_DATE(?, 'YYYYMMDD')
		},
			values => [$file_date]
	);
	if (defined $date_count_rs->[0]) {
		#sql query was successful & returned some data
		my $date_count = int($date_count_rs->[0][0]);
		if ($date_count > 0) {
			# we have already loaded data for this date
			if ($debug == 0) {
				send_email(
					$from_email, 
					$to_email, 
					"GPRS Get Daily Usage - Error", 
					"Data was already loaded for date: $file_date (record count: $date_count).\n\n$process_name is running on " . hostname() . ".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
				);
			}
			die "Data was already loaded for date: $file_date (record count: $date_count).";
		}
	} else {
		#sql query was unsuccessful
		if ($debug == 0) {
			send_email(
				$from_email, 
				$to_email, 
				"GPRS Get Daily Usage - Error", 
				"Unable to determine current record count for date: $file_date (database or query error).\n\n$process_name is running on " . hostname() . ".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
			);
		}
		die "Unable to determine current record count for date: $file_date (database or query error).";
	}
	
	
	# Load data file header format
	my $url = "$USAT::DeviceAdmin::GPRS::Cingular::protcol$USAT::DeviceAdmin::GPRS::Cingular::server/USA/sub_detail_definitions.asp";
	print_log("Attempting to download $url ...");
	my $response = $USAT::DeviceAdmin::GPRS::Cingular::browser->get($url);
	if ($response->is_error)
	{
		if ($debug == 0) {
			send_email(
				$from_email, 
				$to_email, 
				"GPRS Get Daily Usage - Error", 
				"Error accessing '$url' at AT&T EOD web site.\nResponse Code: " .$response->code . "\nResponse Message: " . $response->message . "\n\n$process_name is running on " . hostname() . ".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
			);
		}
		die "Error accessing '$url' at AT&T EOD  web site - Response Code: " .$response->code . " Response Message: " . $response->message . ".";
	}
	my $file_header; #= 'ICCID,MSISDN,PLAN_CODE,ACT_ID,INCLUDED_USAGE,ON_NET_KB_USAGE,ROAMING_KB_USAGE,CAN_KB_USAGE,INT_KB_USAGE,TOTAL_KB_USAGE,ACCESS_CHG,ACTIVATION_CHG,BEGPERIOD,ENDPERIOD,CUST_PREFIX,CUST_FIELD_1,CUST_FIELD_2,CUST_FIELD_3,CUST_FIELD_4,CUST_FIELD_5';
	my $header_data = $response->content;
	$header_data =~ s/\r\n|\n//go;
	if ($header_data =~ m/Header for Imsi Detail file:<[^>]+><div[^>]+>(([^<]|<[br|BR]+>)+)<\/div>/)
	{
		$file_header = $1;
		$file_header =~ s/<[^>]+>//go;	#remove any remaining html tags (yuck!)
		foreach (
			'BEGPERIOD', 
			'ICCID', 
			'MSISDN', 
			'INCLUDED_USAGE', 
			'ON_NET_KB_USAGE', 
			'ROAMING_KB_USAGE', 
			'CAN_KB_USAGE', 
			'INT_KB_USAGE', 
			'TOTAL_KB_USAGE'
		)
		{
			#die "Unable to find required header field name '$_'" unless $file_header =~ m/\b$_\b/;
			if (!($file_header =~ m/\b$_\b/)) {
				if ($debug == 0) {
					send_email(
						$from_email, 
						$to_email, 
						"GPRS Get Daily Usage - Error", 
						"Unable to find required header field name '$_'.\n\n$process_name is running on ".hostname().".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
					);
				}
				die "Unable to find required header field name '$_'";
			}
		}
	}
	else
	{
		print_log("Unable to determine CVS header from Cingular web site. Data import process cancelled. Please validate page content.");
		if ($debug == 0) {
			send_email(
				$from_email, 
				$to_email, 
				#"Error processing input GPRS header file", 
				"GPRS Get Daily Usage - Error",
				"Unable to determine CVS header from AT&T EOD web site. Please validate page content.\n\n$process_name is running on ".hostname().".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
			);
		}
		exit 0;
	}

	my $idx = 0;
	my %header_hash = map(($_, $idx++), split(/,/, $file_header));

	# Connect to server, DL file
	$url = "$USAT::DeviceAdmin::GPRS::Cingular::protcol$USAT::DeviceAdmin::GPRS::Cingular::server/USA/GPRSDailyUsage/$file_name";
	print_log("Attempting to download $url ...");
	$response = $USAT::DeviceAdmin::GPRS::Cingular::browser->get($url);
	if ($response->is_error)
	{
		if ($debug == 0) {
			send_email(
				$from_email, 
				$to_email, 
				"GPRS Get Daily Usage - Error", 
				"Error downloading '$file_name' from AT&T EOD web site.\nResponse Code: " .$response->code . "\nResponse Message: " . $response->message . "\n\n$process_name is running on " . hostname() . ".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
			);
		}
		die "Error downloading '$file_name' from AT&T EOD web site ($url) - Response Code: " .$response->code . " Response Message: " . $response->message . ".";
	}
	my $file_content = $response->content;
	if (open(FILEOUT, ">$archive_path/$file_name"))
	{
		print FILEOUT $file_content;
		close FILEOUT;
	}
	else
	{
		if ($debug == 0) {
			send_email(
				$from_email, 
				$to_email, 
				"GPRS Get Daily Usage - Error", 
				"Unable to create temporary file '$archive_path/$file_name'.\n\n$process_name is running on ".hostname().".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
			);
		}
		die "Unable to create temporary file '$archive_path/$file_name'";
	}


	# Parse file & Write to database
	my $zip_file = Archive::Zip->new("$archive_path/$file_name");
	my @textFileMembers = $zip_file->membersMatching("$file_name_txt.txt");
	if (scalar @textFileMembers < 1)
	{
		if ($debug == 0) {
			send_email(
				$from_email, 
				$to_email, 
				"GPRS Get Daily Usage - Error", 
				"File ('$file_name_txt.txt') not found in archive '$file_name'.\n\n$process_name is running on ".hostname().".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
			);
		}
		die "File ('$file_name_txt.txt') not found in archive '$file_name'.";
	}
	if (scalar @textFileMembers > 1)
	{
		if ($debug == 0) {
			send_email(
				$from_email, 
				$to_email, 
				"GPRS Get Daily Usage - Error", 
				"Multiple files found in archive '$file_name'.  Process currently can handle only one file.\n\n$process_name is running on ".hostname().".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
			);
		}
		die "Multiple files found in archive '$file_name'.  Process currently can handle only one file.";
	}
	
	my @data = split(/\r\n|\n/, $zip_file->contents($textFileMembers[0]));
	my $csv = Text::CSV->new();
	my $error_occured = 0;
	my $insert_sth;
	foreach (@data)
	{
		$_ =~ s/\000//go;	#null characters invalid byte in CVS files?
		if ($csv->parse($_)) {
			my @data_row = $csv->fields();
			if ($debug == 0) {
				$dbh->do(
					query	=> q{
						INSERT INTO device.gprs_daily_usage (
							report_date,
							iccid,
							msisdn,
							included_usage,
							aws_kb_usage,
							roaming_kb_usage,
							can_kb_usage,
							int_kb_usage,
							total_kb_usage
						) VALUES (
							TO_DATE(?, 'YYYY-MM-DD'),
							?, ?, ?, ?, ?, ?, ?, ?
						)
					},
					values	=> [
						sprintf("%04d-%02d-%02d", split(/[^0-9]/,$data_row[$header_hash{BEGPERIOD}])),
						$data_row[$header_hash{ICCID}],
						$data_row[$header_hash{MSISDN}],
						$data_row[$header_hash{INCLUDED_USAGE}],
						$data_row[$header_hash{ON_NET_KB_USAGE}],
						$data_row[$header_hash{ROAMING_KB_USAGE}],
						$data_row[$header_hash{CAN_KB_USAGE}],
						$data_row[$header_hash{INT_KB_USAGE}],
						$data_row[$header_hash{TOTAL_KB_USAGE}],
					],
					sth	=> \$insert_sth
				);
			}
		}
		else {
			$error_occured = 1;
			print_log("Invalid line found: ".$csv->error_input());
			if ($debug == 0) {
				send_email(
					$from_email, 
					$to_email, 
					#"Error processing input GPRS data file", 
					"GPRS Get Daily Usage - Error",
					"Invalid line found: ".$csv->error_input()."\n\n$process_name is running on ".hostname().".usatech.com\n\nhttp://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=GPRSGetDailyUsageError\n"
				);
			}
			$dbh->rollback();
			last;
		}
	}
	$dbh->commit();
	$dbh->close();
	print_log($error_occured ? "Error during processing. Database unchanged." : "Done! Inserted ".scalar @data." rows.");
}

########################################################################
sub send_email ($$$$)
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_addr_array = split(/,/, $to_addrs);

	my $smtp_server = $smtp_server;
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_addr_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 

	my $to_line;
	for(my $index = 0; $index <= $#to_addr_array; $index++)
	{
		$to_line = $to_line . "$to_addr_array[$index]";
		if($index < $#to_addr_array)
		{
			$to_line = $to_line . ", ";
		}
	}
	
	$smtp->datasend("To: $to_line\n");
	$smtp->datasend("From: $from_addr\n");
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("Content-Type: text/plain\n");
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend();
	
	$smtp->quit();   
	
	return 1;
}

sub send_email_detailed
{
	my ($from_addr, $from_name, $to_addrs, $to_names, $subject, $content) = @_;
	my @to_addr_array = split(/,/, $to_addrs);
	my @to_name_array = split(/,/, $to_names);
	
	# check that every address has a name
	if($#to_addr_array ne $#to_name_array)
	{
		warn "send_email_detailed: Length of address array not equald to length of name array!\n";
		return 0;
	}

	my $smtp_server = $smtp_server;
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_addr_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 

	my $to_line;
	for(my $index = 0; $index <= $#to_addr_array; $index++)
	{
		$to_line = $to_line . "\"$to_name_array[$index]\" <$to_addr_array[$index]>";
		if($index < $#to_addr_array)
		{
			$to_line = $to_line . ", ";
		}
	}
	
	$smtp->datasend("To: $to_line\n");
	$smtp->datasend("From: $from_name <$from_addr>\n");
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("Content-Type: text/plain\n");
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();
	
	return 1;
}


sub print_log
{
	my $message = shift;
	print log_date_stamp() .  " [".sprintf("%5d", $$)."] $message\n";
}

sub log_date_stamp
{
	my @epoch_sec = gettimeofday();
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($epoch_sec[0]);
	return sprintf('%04d-%02d-%02d %02d:%02d:%02d.', ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec).substr(sprintf('%06d', $epoch_sec[1]), 0, 3);
}


main();
