#!/usr/local/USAT/bin/perl

# The script should synchronize our GPRS_DEVICE table with the Cingular "Enterprise on Demand"
# self-service website.   It also will update the 5 Cingular customer defined fields to keep 
# them synchronized with our database.
#
# This script requires Cingular.pm; our library to communicate with the Cingular website

use strict;

use USAT::DeviceAdmin::GPRS;
use USAT::DeviceAdmin::GPRS::Cingular;

# Moved the following to its own cron due to race condition
#print "Checking for SIMs in USAT rate plan that need to be updated to USAA...\n";
#USAT::DeviceAdmin::GPRS::update_rate_plan();
print "Checking for SIMs that are Activated but have a device assigned...\n";
USAT::DeviceAdmin::GPRS::update_assigned();

print "Requesting Active SIMs file refresh...\n";
my ($code, $msg) = USAT::DeviceAdmin::GPRS::Cingular::refresh_active_sims_file();
my $code = 1;
my $msg =  "";
if($code)
{
	print "Request accepted!  I will now wait 15 minutes for the refresh to occur...\n";
	sleep(960);
#	sleep(10);
	USAT::DeviceAdmin::GPRS::synch_all();
}
else
{
	print "Failed to request active SIMs file refresh: $msg\n";
}

