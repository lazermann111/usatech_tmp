#!/usr/local/USAT/bin/perl

use OOCGI::Query;
use USAT::DeviceAdmin::UI::USAPopups;

print qq|with(milonic=new menuname("file_type")) {
style=HeadersStyle;
overflow="scroll";
aI("align=center;text=File Name:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/file_list.cgi name=search><table><tr><td><input name=file_name size=32 onmouseover='showtip(tooltip_sm_files_search_file_name)' onmouseout='hidetip()'> <input type=submit value=Search></td></tr></table></form>;type=form;");
aI("align=center;text=File ID:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/file_list.cgi name=search><table><tr><td><input name=file_id onmouseover='showtip(tooltip_sm_files_search_file_name)' onmouseout='hidetip()'> <input type=submit value=Search></td></tr></table></form>;type=form;");
aI("align=center;text=Upload File to Server:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/new_file.cgi name=search><table><tr><td><input type=submit value=Next></td></tr></table></form>;type=form;align=center;");
aI("align=center;text=Create External File Transfer;type=header;");
aI("text=<FORM METHOD=GET ACTION=/create_external_file_transfer.cgi name=search><table><tr><td><input name=device_name size=16 onmouseover='showtip(tooltip_sm_files_create_ext_file_transfer)' onmouseout='hidetip()'> <input type=submit value=Next></td></tr></table></form>;type=form;");
aI("align=center;text=File Type;type=header;");\n|;

my $sql = USAT::DeviceAdmin::UI::USAPopups->sql_file_transfer_type;

my $qo = OOCGI::Query->new($sql);


while(my @arr = $qo->next) {
   my ($text, $file_type) = (quotemeta $arr[1], $arr[0]);
   print qq|aI("text=$text;url=/file_list.cgi?file_type=$file_type&action=Search;");\n|;
}

print qq|}\n|;

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
