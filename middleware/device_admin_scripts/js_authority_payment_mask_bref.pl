#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::Query;

### load backref settings ###
my %regex_bref_types;
my @regex_bref_codes;
my @regex_bref_names;
my @regex_bref_descs;

my $sql = 'SELECT authority_payment_mask_bref_id, regex_bref_name, regex_bref_desc
            FROM authority_payment_mask_bref';

my $qo = OOCGI::Query->new($sql);
while(my @arr = $qo->next_array) {
	$regex_bref_types{$arr[0]} = { name => $arr[1], desc => $arr[2] };
}

### compile settings into javascript-readable arrays ###
foreach my $bref_code (sort keys %regex_bref_types)
{
	push @regex_bref_codes, $bref_code;
	push @regex_bref_names, quotemeta $regex_bref_types{$bref_code}->{name};
	push @regex_bref_descs, quotemeta $regex_bref_types{$bref_code}->{desc};
}
my $regex_bref_codes_str = "'".join("','", @regex_bref_codes)."'";
my $regex_bref_names_str = "'".join("','", @regex_bref_names)."'";
my $regex_bref_descs_str = "'".join("','", @regex_bref_descs)."'";

print <<"EOHTML";
function testRegex(ID) {
    var regexid = 'regex_';
    var backrid = 'backr_';
    var testvid = 'testv_';
	if(ID) {
       regexid = 'regex_'+ID;
       backrid = 'backr_'+ID;
       testvid = 'testv_'+ID;
    }
    var regex = document.getElementById(regexid).value;
    var backr = document.getElementById(backrid).value;
    var testv = document.getElementById(testvid).value;

	var regexBrefCodes = new Array($regex_bref_codes_str);
	var regexBrefNames = new Array($regex_bref_names_str);
	var regexBrefDescs = new Array($regex_bref_descs_str);
	var regexBrefTypes = new Array();
	for (var i = 0; i < regexBrefCodes.length; i++) {
		regexBrefTypes[regexBrefCodes[i]] = regexBrefNames[i];
	}

	var myRegex = new RegExp(regex, 'i');
	var myResArr = myRegex.exec(testv);
	var myRegexBref = backr;	
	if (myResArr == null) {
		alert("Invalid Match.\\n\\nPlease check that you have selected the correct method\\nand have entered a correct sample string.");
	}
	else {
		var brefResStr = "";
		if (myRegexBref != '') {
			brefResStr = "\\n\\nMatched substring results are as follows:\\n";
			var myBrefCodeArr = myRegexBref.split('|');
			for (var i = 0; i < myBrefCodeArr.length; i++) {
				var myBrefPartsArr = myBrefCodeArr[i].split(':');	//0=num, 1=db id
				//alert("myBrefPartsArr="+myBrefPartsArr+"\\nmyResArr="+myResArr+"\\nregexBrefTypes="+regexBrefTypes);
				brefResStr = brefResStr + " - " + regexBrefTypes[myBrefPartsArr[1]] + ": " + myResArr[myBrefPartsArr[0]] + "\\n";
			}
		}
		alert('Successful Match!' + brefResStr);
	}
}
EOHTML
