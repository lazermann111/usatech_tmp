package conf::Config;
$VERSION = 1.01;

use strict;
use warnings;
use FindBin;

### configurable characteristics of system ###
use constant DEBUG => 0;

use constant {
	PIDFILE    => "$FindBin::Bin/debug/sony_log_monitor.pid",
	CACHE_ROOT => "$FindBin::Bin/debug",
	
	# File transfer policies
	MAX_DAYS_TO_CHECK => 1,	#max num of days of log files to review
	MAX_FILE_SIZE     => 1048576, #max size in bytes allowed per file
	
	# E-mail settings
	SMTP_MAILHOST       => 'mailhost', #SMTP server
	ALERT_EMAIL_FROM    => 'sony_log_monitor@usatech.com', # from address in email header
	ALERT_EMAIL_TO      => 'rita.lin@am.sony.com', # address to send logs
	ALERT_EMAIL_SUBJECT => 'Sony debug files received', # e-mail subject line

	# Leave SOURCE_DATABASE blank to use default OOCGI::Query::Config settings for DNS, user, and password
	SOURCE_DATABASE    => '',	#i.e. dbi:Oracle:usadbp
	SOURCE_DB_USER     => '',
	SOURCE_DB_PASSWORD => '',
	
	# Internal date formatting
	DBTS_FORMAT => 'YYYY-MM-DD HH24:MI:SS',
};

# SQL 'like' parameters to match specific Sony PS uploaded files USADBP DEVICE.FILE_TRANSFER.FILE_TRANSFER_NAME
use constant MATCH_FILENAME_SQL_LIKE => (
	'%USATCom%', '%USATData%', '%connection.log%', '%data.log%'
);

if (DEBUG) {
	eval <<EVAL;
use constant {
	MAX_DAYS_TO_CHECK => 365,	#nax num of days of log files to review
	SMTP_MAILHOST    => 'mail.usatech.com', #SMTP server
	ALERT_EMAIL_TO   => 'erybski\@usatech.com', # address to send deubg logs
	SOURCE_DATABASE    => 'dbi:Oracle:(DESCRIPTION = (ADDRESS_LIST = (FAILOVER = TRUE) (LOAD_BALANCE = TRUE) (ADDRESS = (PROTOCOL = TCP)(HOST = USADBP21-vip.trooper.usatech.com)(PORT = 1510)) (ADDRESS = (PROTOCOL = TCP)(HOST = USADBP22-vip.trooper.usatech.com)(PORT = 1510))) (CONNECT_DATA = (SERVICE_NAME = USADBP.world) (FAILOVER_MODE = (TYPE=SELECT)(METHOD=BASIC)(RETRIES=1)(DELAY=1))))',	#dbi:Oracle:usadbp
	SOURCE_DB_USER     => 'erybski',
};
EVAL
}

# -------------------------------------------------------------------- #

BEGIN { #internal subroutine to simplify generation of exporter defaults
	my $regex = quotemeta __PACKAGE__;
	sub declared (;$) {
		use constant 1.01;	      # don't omit this!
		my $name = shift;
		if (defined $name) {
			$name =~ s/^::/main::/o;
			my $pkg = caller;
			my $full_name = $name =~ m/::/o ? $name : "${pkg}::$name";
			return $constant::declared{$full_name};
		}
		else {
			return grep(/^$regex\::\w+$/o, keys %constant::declared);
		}
	}
}

BEGIN {
	use Exporter ();
	use vars		qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
	@ISA		    = qw(Exporter);
	@EXPORT		 = ();
	@EXPORT_OK	      = (map((split('::', $_))[-1], declared()));
	%EXPORT_TAGS    = (
			globals		 => [@EXPORT_OK],
	);
}

1;
