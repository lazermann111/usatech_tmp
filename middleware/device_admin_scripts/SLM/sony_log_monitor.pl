#!/usr/local/USAT/bin/perl

$VERSION = 1.001000; #[major].[nnn minor][nnn maintenance]

# Script sends recent Sony PictureStation logs to Sony

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/";

use OOCGI::Query;
use conf::Config qw(:globals);
use File::Pid;
use Getopt::Long qw(VersionMessage);
use Net::SMTP;
use MIME::Lite;
use File::Temp;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Data::Dumper;
use Cache::FileCache;

#use IO::Scalar;

our $pidfile;
our $time;

sub main {
	{ my $oldfh = select(STDOUT); $|=1; select($oldfh); }   #auto-flush STDOUT
	
	# Script input option handling
	Getopt::Long::Configure(qw(no_ignore_case));
#	GetOptions(
#	);

	# PID file management
	# store process id in a file and use that file as a semaphore to check
	# whether process is running or not. 
	$pidfile = File::Pid->new({ file => PIDFILE });
	if ( my $pid = $pidfile->running ) {
		print STDERR "Application instance PID $pid already appears to be running.  Attempting to shut down existing instance...\n";
		kill('SIGINT', $pid);
		sleep 5;	#wait for process to settle
		if ($pidfile->running) {
			kill('SIGKILL', $pid);
			sleep 2;	#wait for process to settle
		}
		if ($pidfile->running) {
			print STDERR "Existing process ($pid) refused to terminate\n";
			exit 1;
		}
		print STDERR "Removing stale PID file: ",$pidfile->file,"\n";
	}
	$pidfile->remove;
	$pidfile = File::Pid->new({ file => PIDFILE });
	unless ($pidfile->write) {
		print STDERR "Unable to write pidfile '".PIDFILE."': $!\n";
		exit 1;
	}

	# Load db password
	my $password = SOURCE_DB_PASSWORD;
	if (SOURCE_DATABASE && !$password) {
		require Term::ReadKey;
		print 'Please enter password for '.SOURCE_DB_USER.'@'.SOURCE_DATABASE.': ';
		Term::ReadKey::ReadMode('noecho');
		$password = Term::ReadKey::ReadLine(0);
		Term::ReadKey::ReadMode('normal');
		chomp $password;
		print "\n";
	}

	# Start timing this program.
	$time = time;
	print "--------------------------------------------\n";
	print "start time: ".localtime($time)."\n";
	
	# Load cache
	my $cache = new Cache::FileCache({
		'cache_root'	=> CACHE_ROOT.'/cache',
		'namespace'		=> 'sony_log_monitor'
	});

	# Prepare some runtime variables for period to check
	my $last_successful_runtime = (time - ($cache->get('last_successful_runtime') || 0));
	my $days_to_check = $last_successful_runtime / (60*60*24);
	$days_to_check = MAX_DAYS_TO_CHECK if $days_to_check > MAX_DAYS_TO_CHECK;
	my $time_offset_str = localtime(time - ($days_to_check * (60*60*24)));

	# Connect to source database (if required) and run query
	OOCGI::Query->connect( SOURCE_DATABASE, SOURCE_DB_USER, $password ) if SOURCE_DATABASE;

	my $qo = OOCGI::Query->new(q{
		SELECT   ft.file_transfer_id, ft.file_transfer_name,
				 TO_CHAR(dft.device_file_transfer_ts, '}.DBTS_FORMAT.q{') device_file_transfer_ts,
				 d.device_serial_cd, dcl.customer_name, dcl.location_name, ft.file_transfer_content,
				 TO_CHAR(SYSDATE, '}.DBTS_FORMAT.q{') now
			FROM (SELECT d.device_id
					FROM device.device d JOIN device.device_setting ds ON d.device_id =
																			ds.device_id
																	 AND ds.device_setting_parameter_cd =
																			'HostType'
																	 AND ds.device_setting_value =
																				'SONY'
				  UNION
				  SELECT device_id
					FROM device.device
				   WHERE device_type_id = 4) x,
				 device.device_file_transfer dft,
				 device.file_transfer ft,
				 device.device d,
				 device.device_cust_loc dcl
		   WHERE x.device_id = dft.device_id
			 AND x.device_id = d.device_id
			 AND ft.file_transfer_id = dft.file_transfer_id
			 AND dft.device_file_transfer_ts >= SYSDATE - ?
			 AND dcl.device_serial_cd = d.device_serial_cd
			 AND dft.device_file_transfer_direct = 'I'
			 AND (}.join(' OR ', ("file_transfer_name LIKE ?") x (scalar(my @a=MATCH_FILENAME_SQL_LIKE))).q{)
		ORDER BY d.device_serial_cd
		}, 
		{
		  bind         => [ $days_to_check, MATCH_FILENAME_SQL_LIKE ],
		  data_as_hash => 1,
		  cache        => 0,
		  LongReadLen  => MAX_FILE_SIZE, #1M max log size
		  typemap => { DBI::SQL_TYPE_DATE      => DBTS_FORMAT,
					   DBI::SQL_TYPE_TIMESTAMP => DBTS_FORMAT },
		},
	);
	eval { $qo->dbh->{LongTruncOk} = 1; };

	# Create a Zip file
#	my $zipContents = '';
#	my $SH = IO::Scalar->new( \$zipContents );
	my $zip = Archive::Zip->new();

	# Add files to zip
	my %file_list;
	my %file_cnt;
	while (my %h = $qo->next_hash) {
		$file_cnt{$h{device_serial_cd}}++;
		
		# Prepare file
		my $fn = $h{file_transfer_name};
		$fn =~ s/.*(\/|\\)//go;	# Strip qualified path name, if exists

		my $dir = "/logs/$h{customer_name}/$h{location_name}/$h{device_serial_cd}/$h{device_file_transfer_ts}";
		$dir =~ s/[^a-zA-Z0-9\/]/_/go;

		push @{$file_list{$h{device_serial_cd}}}, " $file_cnt{$h{device_serial_cd}}. $dir/$fn\n    - Original file: '$h{file_transfer_name}'\n    - received at $h{device_file_transfer_ts} EDT";

		# Add a file from a string with compression
		my $string_member = $zip->addString( pack("H*", $h{file_transfer_content}), "/$dir/$fn" );
		$string_member->desiredCompressionMethod( COMPRESSION_DEFLATED );
	}

	my $total_files = 0;
	$total_files += $file_cnt{$_} foreach keys %file_cnt;
	print "Found $total_files total files since $time_offset_str.\n";
	if ($total_files) {
		# Save the Zip file
#		unless ( (my $result = $zip->writeToFileHandle($SH)) == AZ_OK ) {
#		   die "Couldn't write to temp file ".$SH.": $result";
#		}
#		$SH->close;
		my $tmp = File::Temp->new( SUFFIX => '.zip' );
		unless ( (my $result = $zip->writeToFileHandle($tmp)) == AZ_OK ) {
		   die "Couldn't write to temp file ".$tmp->filename.": $result";
		}
		$tmp->seek( 0, SEEK_END );

		# send the email
		my $email_zip_name = "logs since $time_offset_str.zip";
		$email_zip_name =~ s/[^a-zA-Z0-9_.\-]/_/go;
		my $result = send_email(
			ALERT_EMAIL_FROM,
			ALERT_EMAIL_TO,
			ALERT_EMAIL_SUBJECT,
			"New PictureStation log files were received since $time_offset_str.\n"
				."The following files are included in the attached Zip archive:\n\n"
				 .join("\n", map {"$_:\n" . join("\n", @{$file_list{$_}}) } sort keys %file_list),
			({
				Type        => 'application/x-zip',
#	    		Encoding    => "base64",
#				Data        => \$zipContents,
				Path        => $tmp->filename,
				Filename    => $email_zip_name,
				Disposition => 'attachment'		
			})
		);
		
		if ($result) {
			print "E-mail sent to: ". ALERT_EMAIL_TO . "\n";
		} else {
			die "Error sending e-mail: $result";
		}
	} else {
		print "No e-mail sent.\n";
	}

	$cache->set('last_successful_runtime', $time) unless DEBUG > 1;
}
main();

END {
	print "end time: ".localtime(time())." (".(time-$time)." seconds)\n";
	eval { $pidfile->remove; };
}

sub send_email {
	my ($from_address, $to_address, $subject, $message_body, @attachments) = @_;

	### Create the multipart container
	my $msg = MIME::Lite->new (
		From    => $from_address,
		To      => $to_address,
		Subject => $subject,
		Type    =>'multipart/mixed'
	) or die "Error creating multipart container: $!\n";

	### Add the text message part
	$msg->attach (
		Type => 'TEXT',
		Data => $message_body
	) or die "Error adding the text message part: $!\n";

	foreach my $href (@attachments) {
		### Add the ZIP file
		$msg->attach (
#			Type        => 'application/zip',
#			Path        => $my_file_zip,
#			Filename    => $your_file_zip,
#			Disposition => 'attachment'
			%$href
		) or die "Error attaching file: $!:".Dumper($href)."\n";
	}

	### Send the Message
	MIME::Lite->send('smtp', SMTP_MAILHOST, Timeout => 300);
	return $msg->send;
}
