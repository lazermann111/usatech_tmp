#!/usr/local/USAT/bin/perl

use USAT::DeviceAdmin::DBObj;

# this section generates the list which starts with a number
# ----------------------------------------------------------
my $sql = qq{
       SELECT customer_id,
              customer_name || '-' || customer_city as name_city, customer_state_cd
         FROM location.customer
        WHERE customer_active_yn_flag = 'Y'
          AND ASCII(SUBSTR(customer_name,1,1)) BETWEEN 48 AND 57 
     ORDER BY customer_name
};

my @customers = USAT::DeviceAdmin::DBObj::Customer->objects($sql);

print q|with(milonic=new menuname("cust__")){|,"\n";  # for customer starting with number
print q|overflow="scroll";|,"\n";
print q|style=menuStyle;|,"\n";

foreach my $obj ( @customers ) {
   my $count_sql = q{
        SELECT COUNT(1)
        FROM pss.pos p, device.device d
        WHERE p.customer_id = ?
            AND p.device_id = d.device_id
            AND d.device_active_yn_flag = 'Y'
	};

   my $count = OOCGI::Query->new($count_sql, { bind => [ $obj->ID ] } )->node(0,0);
   if($count > 0) {
      print q|aI("text=|,
      quotemeta($obj->name_city.' '.$obj->customer_state_cd." ( $count )"),
      q|;url=/device_list.cgi?customer_id=|,$obj->ID,q|;")|;
   } else {
      print q|aI("text=|,
      quotemeta($obj->name_city.' '.$obj->customer_state_cd),
      q|;")|;
   }
   print "\n";
}
print "}\n\n";

# this section generates the list which starts with
# lower or upper case letters
# ------------------------------------------------------------
my @arr = ('A','B','C','D','E','F','G','H','I','J','K','L','M',
           'N','O','Q','P','R','S','T','U','V','W','X','Y','Z');

foreach my $key ( @arr ) {
   my $sql = qq{
          SELECT customer_id,
                 customer_name || '-' || customer_city as name_city, customer_state_cd
            FROM location.customer
           WHERE customer_active_yn_flag = 'Y'
             AND UPPER(customer_name) like '$key%'
        ORDER BY customer_name};

   my @customers = USAT::DeviceAdmin::DBObj::Customer->objects($sql);

   print q|with(milonic=new menuname("cust_|,lc($key),q|")){|,"\n";
   print q|overflow="scroll";|,"\n";
   print q|style=menuStyle;|,"\n";

   foreach my $obj ( @customers ) {
       my $count_sql = q{
        SELECT COUNT(1)
        FROM pss.pos p, device.device d
        WHERE p.customer_id = ?
            AND p.device_id = d.device_id
            AND d.device_active_yn_flag = 'Y'
	};

      my $count = OOCGI::Query->new($count_sql, { bind => [ $obj->ID ] } )->node(0,0);
	  if($count > 0) {
         print q|aI("text=|,
         quotemeta($obj->name_city.' '.$obj->customer_state_cd." ( $count )"),
         q|;url=/device_list.cgi?customer_id=|,$obj->ID,q|;")|;
	  } else {
         print q|aI("text=|,
         quotemeta($obj->name_city.' '.$obj->customer_state_cd),
         q|;")|;
	  }
      print "\n";
   }
   print "}\n\n";
}

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
