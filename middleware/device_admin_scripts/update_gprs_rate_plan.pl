#!/usr/local/USAT/bin/perl

# The script should update the SIM rate plan with the Cingular "Enterprise on Demand"
# self-service website for devices that need it (migrate from USAT to USAA).
#
# This script was moved from the synch_gprs_device.pl script.
# It should be run approximately 2 hours after the synch_gprs_device.pl runs.
#
# This script requires USAT::DeviceAdmin::GPRS;

use strict;
use USAT::DeviceAdmin::GPRS;

print "Checking for SIMs in USAT rate plan that need to be updated to USAA...\n";
USAT::DeviceAdmin::GPRS::update_rate_plan();
