#!/usr/local/USAT/bin/perl

use USAT::DeviceAdmin::DBObj;

my @device_types = USAT::DeviceAdmin::DBObj::Device_type->objects;

foreach my $obj ( @device_types ) {
   my $ID = $obj->ID;
   my $sql = qq|
   select distinct ds.device_setting_value 
     from device_setting ds, device_type dt, device d 
    where dt.device_type_id = ?
      and dt.device_type_id = d.device_type_id 
      and d.device_id = ds.device_id 
      and ds.device_setting_parameter_cd = 'Firmware Version'
    order by ds.device_setting_value
   |;

#print "SQL $sql <br>";
   my $qo = OOCGI::Query->new($sql, { bind=>$ID });
   if($qo->rowNumber) {
      print qq|with(milonic=new menuname("firw_$ID")){\n|;
      print qq|overflow="scroll";\n|;
      print qq|style=HeadersStyle;\n|;
      print qq|aI("align=center;text=Firmware Version:;type=header;");\n|;
      while(my @arr = $qo->next ) {
         my $data = quotemeta $arr[0];
#        print  qq|aI("text=$data;url=javascript:frw('$data',$ID);")\n|;
         print  qq|aI("text=$data;url=/device_list.cgi?device_type=$ID&action=Search&firmware_version=$data&enabled=%27Y%27;")\n|;
      }
      print qq|}\n\n|;
   }
}

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
