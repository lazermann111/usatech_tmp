#!/usr/local/USAT/bin/perl

use USAT::DeviceAdmin::DBObj;

# this section generates the list which starts with a number
# ----------------------------------------------------------

my $sql = qq{
       SELECT
     DISTINCT loc.location_id,
              loc.location_name|| '-' || loc.location_city as NAME_CITY,
              loc.location_state_cd,
              loc.location_country_cd
         FROM location.location loc,
              pss.pos pos,
              device.device dev
        WHERE pos.device_id = dev.device_id
          AND pos.location_id = loc.location_id
          AND dev.device_type_id = 6
          AND loc.location_active_yn_flag = 'Y'
          AND ASCII(SUBSTR(location_name,1,1)) BETWEEN 48 AND 57
     ORDER BY NAME_CITY
};
=pod
       SELECT location_id,
              location_name || '-' || location_city as NAME_CITY,
              location_state_cd,
              location_country_cd
         FROM location.location
        WHERE location_active_yn_flag = 'Y'
          AND ASCII(SUBSTR(location_name,1,1)) BETWEEN 48 AND 57 
     ORDER BY location_name
=cut

my @locations = USAT::DeviceAdmin::DBObj::Location->objects($sql);

print q|with(milonic=new menuname("loca__")){|,"\n";  # for location starting with number
print q|overflow="scroll";|,"\n";
print q|style=menuStyle;|,"\n";

foreach my $obj ( @locations ) {
   print q|aI("text=|,
         quotemeta($obj->name_city.' '.$obj->location_state_cd.' '.$obj->location_country_cd),
         q|;url=javascript:loc(|,$obj->ID,q|);")|;
   print "\n";
}
print "}\n\n";

# this section generates the list which starts with 
# lower or upper case letters
# ------------------------------------------------------------
my @arr = ('A','B','C','D','E','F','G','H','I','J','K','L','M',
           'N','O','Q','P','R','S','T','U','V','W','X','Y','Z');

foreach my $key ( @arr ) {
   my $sql = qq{
       SELECT
     DISTINCT loc.location_id,
              loc.location_name|| '-' || loc.location_city as NAME_CITY,
              loc.location_state_cd,
              loc.location_country_cd
         FROM location.location loc,
              pss.pos pos,
              device.device dev
        WHERE pos.device_id = dev.device_id
          AND pos.location_id = loc.location_id
          AND dev.device_type_id = 6
          AND loc.location_active_yn_flag = 'Y'
          AND UPPER(location_name) like '$key%'
     ORDER BY NAME_CITY
   };
=pod
          SELECT location_id,
                 location_name || '-' || location_city as NAME_CITY,
                 location_state_cd,
                 location_country_cd
            FROM location.location
           WHERE location_active_yn_flag = 'Y'
             AND UPPER(location_name) like '$key%'
        ORDER BY location_name
=cut

   my @locations = USAT::DeviceAdmin::DBObj::Location->objects($sql);

   print q|with(milonic=new menuname("loca_|,lc($key),q|")){|,"\n";
   print q|overflow="scroll";|,"\n";
   print q|style=menuStyle;|,"\n";
   #aI("text=A to Z Vending-Farmington Hills MI;url=javascript:usasf(238);")
   foreach my $obj ( @locations ) {
      print q|aI("text=|,
            quotemeta($obj->name_city.' '.$obj->location_state_cd.' '.$obj->location_country_cd),
            q|;url=javascript:loc(|,$obj->ID,q|);")|;
      print "\n";
   }
   print "}\n\n";
}

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
