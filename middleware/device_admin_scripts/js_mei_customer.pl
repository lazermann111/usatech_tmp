#!/usr/local/USAT/bin/perl

use USAT::DeviceAdmin::DBObj;

# this section generates the list which starts with a number
# ----------------------------------------------------------
my $sql = q{
SELECT DISTINCT cus.customer_id,
                cus.customer_name || '-' || cus.customer_city as name_city,
                cus.customer_state_cd
           FROM location.customer cus,
                pss.pos ps,
                device.device dev
          WHERE ps.device_id = dev.device_id
            AND ps.customer_id = cus.customer_id
            AND dev.device_type_id = 6
            AND cus.customer_active_yn_flag = 'Y'
            AND ASCII(SUBSTR(cus.customer_name,1,1)) BETWEEN 48 AND 57 
       ORDER BY name_city};

my @customers = USAT::DeviceAdmin::DBObj::Customer->objects($sql);

print q|with(milonic=new menuname("cust__")){|,"\n";  # for customer starting with number
print q|overflow="scroll";|,"\n";
print q|style=menuStyle;|,"\n";

foreach my $obj ( @customers ) {
   print q|aI("text=|,
      (quotemeta $obj->name_city.' '.$obj->customer_state_cd.' '),
      q|;url=javascript:cus(|,$obj->ID,q|);")|;
   print "\n";
}
print "}\n\n";

# this section generates the list which starts with
# lower or upper case letters
# ------------------------------------------------------------
my @arr = ('A','B','C','D','E','F','G','H','I','J','K','L','M',
           'N','O','Q','P','R','S','T','U','V','W','X','Y','Z');

foreach my $key ( @arr ) {
   my $sql = qq{
         SELECT
       DISTINCT cus.customer_id,
                cus.customer_name || '-' || cus.customer_city as name_city,
                cus.customer_state_cd
           FROM location.customer cus,
                pss.pos ps,
                device.device dev
          WHERE ps.device_id = dev.device_id
            AND ps.customer_id = cus.customer_id
            AND dev.device_type_id = 6
            AND cus.customer_active_yn_flag = 'Y'
            AND UPPER(cus.customer_name) like '$key%'
       ORDER BY name_city
   };

   my @customers = USAT::DeviceAdmin::DBObj::Customer->objects($sql);

   if($#customers >= 0) {
   print q|with(milonic=new menuname("cust_|,lc($key),q|")){|,"\n";
   print q|overflow="scroll";|,"\n";
   print q|style=menuStyle;|,"\n";

   foreach my $obj ( @customers ) {
      print q|aI("text=|,
         (quotemeta $obj->name_city.' '.$obj->customer_state_cd.' '),
         q|;url=javascript:cus(|,$obj->ID,q|);")|;
      print "\n";
   }
   print "}\n\n";
   }
}

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
