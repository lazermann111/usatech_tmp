#!/usr/local/USAT/bin/perl

use strict;

use Date::Calc qw(Today Add_Delta_Days);

my @dates;

print q|
with(milonic=new menuname("batc_succ")) {
style=HeadersStyle;
overflow="scroll";
aI("text=report may take 20+ seconds to load;type=header;");
|;
for (my $i = 1; $i <= 30; $i++) {
   @dates = Add_Delta_Days(Today(), -$i);
   print qq|aI("text=$dates[1]/$dates[2]/$dates[0];url=batch_success.cgi?action=View&batch_date=$dates[1]%2F$dates[2]%2F$dates[0];");\n|;
}
print qq|}\n\n|;

print q|
with(milonic=new menuname("sess_excp")) {
style=HeadersStyle;
overflow="scroll";
aI("text=report may take 20+ seconds to load;type=header;");
|;

for (my $i = 1; $i <= 30; $i++) {
   @dates = Add_Delta_Days(Today(), -$i);
   print qq|aI("text=$dates[1]/$dates[2]/$dates[0];url=session_expt.cgi?action=View&expt_date=$dates[1]%2F$dates[2]%2F$dates[0];");\n|;
}
print qq|}\n\n|;

print q|
with(milonic=new menuname("usat_trip")) {
style=HeadersStyle;
overflow="scroll";
aI("text=USAT Round Trip Time;type=header;");
|;

for (my $i = 1; $i <= 30; $i++) {
   @dates = Add_Delta_Days(Today(), -$i);
   print qq|aI("text=$dates[1]/$dates[2]/$dates[0];url=auth_rt_usat.cgi?action=View&auth_date=$dates[1]%2F$dates[2]%2F$dates[0];");\n|;
}
print qq|}\n\n|;

print q|
with(milonic=new menuname("gprs_trip")) {
style=HeadersStyle;
overflow="scroll";
aI("text=GPRS Remote Device Round Trip Time;type=header;");
|;

for (my $i = 1; $i <= 30; $i++) {
   @dates = Add_Delta_Days(Today(), -$i);
   print qq|aI("text=$dates[1]/$dates[2]/$dates[0];url=auth_rt_remote.cgi?action=View&auth_date=$dates[1]%2F$dates[2]%2F$dates[0];");\n|;
}
print qq|}\n\n|;

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
