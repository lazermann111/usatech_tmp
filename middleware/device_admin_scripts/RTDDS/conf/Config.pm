package conf::Config;
$VERSION = 1.00;

use strict;
use warnings;
use Sys::Hostname;

### configurable characteristics of system ###
use constant PIDFILE => '/opt/jobs/DeviceAdmin/RTDDS/rtdds.pid';

use constant CREATE_TEMP_TABLE_IF_REQUIRED => 0;

use constant SOURCE_DATABASE    => 'dbi:Oracle:';
use constant SOURCE_DB_USER     => '';
use constant SOURCE_DB_PASSWORD => '';

use constant TARGET_DATABASE    => 'dbi:Oracle:';
use constant TARGET_DB_USER     => '';
use constant TARGET_DB_PASSWORD => '';

use constant SOURCE_TABLE      => 'REPORT.VW_CURRENT_DEVICE_LOCATION';
use constant TARGET_TABLE      => 'DEVICE.DEVICE_CUST_LOC';
use constant TEMP_TARGET_TABLE => 'DEVICE.DEVICE_CUST_LOC_TMP';
use constant TEMP_TARGET_TABLE_TABLESPACE => 'REPL_DEVICE_DATA';

use constant ALLOW_UPDATE => 5; # change should not be larger than 5 percent
use constant DBTS_FORMAT  => 'MM/DD/YYYY HH24:MI:SS';

use constant INSERT_ROW_CHUNK => 500;

use constant ALERT_EMAIL_FROM => 'rtdds-cronjob@'.hostname().'usatech.com';
#use constant ALERT_EMAIL_TO   => 'monitorallbyemail@usatech.com';
use constant ALERT_EMAIL_TO   => '';

BEGIN { #internal subroutine to simplify generation of exporter defaults
        my $regex = quotemeta __PACKAGE__;
        sub declared (;$) {
                use constant 1.01;              # don't omit this!
                my $name = shift;
                if (defined $name) {
                        $name =~ s/^::/main::/o;
                        my $pkg = caller;
                        my $full_name = $name =~ m/::/o ? $name : "${pkg}::$name";
                        return $constant::declared{$full_name};
                }
                else {
                        return grep(/^$regex\::\w+$/o, keys %constant::declared);
                }
        }
}

BEGIN {
        use Exporter ();
        use vars                qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
        @ISA                    = qw(Exporter);
        @EXPORT                 = ();
        @EXPORT_OK              = (map((split('::', $_))[-1], declared()));
        %EXPORT_TAGS    = (
                globals                 => [@EXPORT_OK],
        );
}

1;

