#!/usr/local/USAT/bin/perl

$VERSION = 1.001000; #[major].[nnn minor][nnn maintenance]

use strict;
use OOCGI::Business;
use conf::Config qw(:globals);
use File::Pid;
use Getopt::Long qw(VersionMessage);
use Net::SMTP;

use constant DEBUG => 0;

our $pidfile;

sub main {
    # store process id in a file and use that file as a semaphore to check
    # whether process is running or not. 

    { my $oldfh = select(STDOUT); $|=1; select($oldfh); }   #auto-flush STDOUT

    ### PID file management ###
    $pidfile = File::Pid->new({ file => PIDFILE });
    if ( my $pid = $pidfile->running ) {
        print STDERR "Application instance PID $pid already appears to be running.  Attempting to shut down existing instance...\n";
        kill('SIGINT', $pid);
        sleep 2;    #wait for process to settle
        if ($pidfile->running) {
            kill('SIGKILL', $pid);
            sleep 2;    #wait for process to settle
        }
        if ($pidfile->running) {
            print STDERR "Existing process ($pid) refused to terminate\n";
            exit 1;
        }
        print STDERR "Removing stale PID file: ",$pidfile->file,"\n";
    }
    $pidfile->remove;
    $pidfile = File::Pid->new({ file => PIDFILE });
    unless ($pidfile->write) {
        print STDERR "Unable to write pidfile '".PIDFILE."': $!\n";
        exit 1;
    }

    ### Script input option handling ###
    my $force_percentage = 0;
    Getopt::Long::Configure(qw(no_ignore_case));
    GetOptions(
        'f|force' => \$force_percentage
    );
    if($force_percentage > 0) {
        $force_percentage = 100; 
    } else {
        $force_percentage = ALLOW_UPDATE;
    }

    # @names array will be used to store column names of target table to use it later in the program
    my @names = qw(
        eport_id
        device_serial_cd
        customer_id
        customer_name
        terminal_id
        terminal_nbr
        asset_nbr
        region_name
        location_name
        install_ts
        activation_ts
        product_type_name
        product_type_specify
        location_type_name
        location_type_specify
        auth_mode
        timezone_offset
        timezone_name
        timezone_abbrev
        intended_avg_trans_amt
        dex_flag
        receipt_flag
        vend_mode
        location_details
        address1
        address2
        city
        state
        zip
    ) if DEBUG;

    get_cols_compare_ddl(\@names) unless DEBUG;

    # Good there is no problem with column info, proceed now.

    ### Connect to source database and prepare query ###
    OOCGI::Query->connect( SOURCE_DATABASE, SOURCE_DB_USER, SOURCE_DB_PASSWORD );

    # Get everything from source view and store it in $qo object for use later in the program.
    my $qo = OOCGI::Query->select(
        SOURCE_TABLE, \@names,
        undef, undef,
        { cache => 0,
          data_as_hash => 1,
          typemap => { DBI::SQL_TYPE_DATE      => DBTS_FORMAT,
                       DBI::SQL_TYPE_TIMESTAMP => DBTS_FORMAT },
        }
    );

    ### Connect to target database and prepare temp table ###
    OOCGI::Query->connect( TARGET_DATABASE, TARGET_DB_USER, TARGET_DB_PASSWORD );

    # check whether TEMP_TARGET_TABLE exist or not. If it exists delete it.
    if(CREATE_TEMP_TABLE_IF_REQUIRED == 1) {
        if(OOCGI::Query->exists_table(TEMP_TARGET_TABLE)) {
            print "DELETING records from temporary table ".TEMP_TARGET_TABLE."\n";
            OOCGI::Query->modify('DELETE FROM '.TEMP_TARGET_TABLE);
        } else {
            # create fresh TEMP_TARGET_TABLE by using TARGET_TABLE as a template.
            OOCGI::Query->modify('CREATE table '.TEMP_TARGET_TABLE
                .' TABLESPACE '.TEMP_TARGET_TABLE_TABLESPACE
                .' AS SELECT * FROM '.TARGET_TABLE.' WHERE 1=2');
        }
    } else {
        OOCGI::Query->modify('DELETE FROM '.TEMP_TARGET_TABLE);
    }
    # start timing this program. this is not actual time of the process, but time it takes copying the data
    my $time = time;
    print "--------------------------------------------\n";
    print "Bulk insert start time: ".localtime($time)."\n";

    my @big_array  = ();
    my $count = 0;
    while(my %hash = $qo->next_hash) {
        # following line makes sure that we only put columns which exist in old_table
        my @values = map { $hash{lc($_)} } @names; 
        push(@big_array, \@values);

        # This is memory efficent insert method which uses DB Tuple.
        if (@big_array && scalar(@big_array) % INSERT_ROW_CHUNK == 0) {
            if(DEBUG) {
                print "Inserting data after row number $count as ".INSERT_ROW_CHUNK." chunk of rows\n";
            }
            &bulk_insert(TEMP_TARGET_TABLE, \@names, \@big_array);
            @big_array = ();
            $count += INSERT_ROW_CHUNK;
        }
    }

    # THIS IS TO STORE LEFT OVER DATA FROM PREVIOUS INSERTS
    # previosly we inserted the data which contains 500 rows chunks
    # if you have 2749 rows, previous while loop will insert 5 500rows chunks
    # and the following statements will insert remaining 249 rows.
    if (@big_array) {
        ### Highly-efficient bulk inserts
        &bulk_insert(TEMP_TARGET_TABLE, \@names, \@big_array);
    }

    my $sql_old   = 'SELECT count(1) FROM '.TARGET_TABLE;
    my $sql_new   = 'SELECT count(1) FROM '.TEMP_TARGET_TABLE;
    my $old_count = OOCGI::Query->new($sql_old)->node(0,0);
    my $new_count = OOCGI::Query->new($sql_new)->node(0,0);

    my $change = abs($new_count - $old_count);
    my $big_count  = ($new_count > $old_count) ? $new_count : $old_count; 
    my $percentage = 100 * $change / $big_count;
    print "OLD COUNT $old_count NEW COUNT $new_count CHANGE $change CHANGE PERCENTAGE $percentage\n";
    print "PERCENTAGE LIMIT is $force_percentage \n";

    if(CREATE_TEMP_TABLE_IF_REQUIRED == 1) {
        &ddl_finalize_data($sql_old, $new_count,$percentage,$force_percentage);
    } else {
        &dml_finalize_data($sql_old, $new_count,$percentage,$force_percentage);
    }

    print "Bulk insert end time: ".localtime(time())." (".(time-$time)." seconds)\n";
}
main();

END {
    eval { $pidfile->remove; };
}

sub send_email {
    my ($from_addr, $to_addrs, $subject, $content) = @_;
    my @to_array = split(/ |,/, $to_addrs);

    my $smtp = Net::SMTP->new('mailhost');

    if(not defined $smtp) {
        warn "send_email: Failed to connect to SMTP server mailhost!\n";
        return 0;
    }
    $smtp->mail($from_addr);
    foreach my $to_addr (@to_array) {
        $smtp->to($to_addr);
    }
    $smtp->data();
    $smtp->datasend("Subject: $subject\n");
    $smtp->datasend("\n");
    $smtp->datasend("$content\n\n");
    $smtp->dataend();
    $smtp->quit();
    return 1;
}

sub get_cols_compare_ddl {
    my $arr_ref = shift;

    if(DEBUG) {
        print "Hope that your computer is not slow, otherwise please wait, initial processing might take time ....\n";
    }
    ### Connect to source database get table column info.
    ### This will be used to test data integrity against target table.
    OOCGI::Query->connect( SOURCE_DATABASE, SOURCE_DB_USER, SOURCE_DB_PASSWORD );
    my $source_fields_with_col_info = OOCGI::Query->get_table_fields_with_column_info(SOURCE_TABLE);

    ### Connect to target database and prepare temp table ###
    ### This will be used to test data integrity against source table.
    OOCGI::Query->connect( TARGET_DATABASE, TARGET_DB_USER, TARGET_DB_PASSWORD );
    my $target_fields_with_col_info = OOCGI::Query->get_table_fields_with_column_info(TARGET_TABLE);

    my $subject = 'Data integrity'; # Subject line for data integrity mail 
    my $content;   # will be used for email content

    foreach my $column ( keys %{$target_fields_with_col_info} ) {
        # first store column names for later use.
        push(@{$arr_ref}, lc $column);
        # Get column info both for source and target.
        # Most important fields for data integrity is given below.
        # Make sure that source did not change those fields.
        my $source_sql_data_type = $source_fields_with_col_info->{$column}->{SQL_DATA_TYPE};
        my $target_sql_data_type = $target_fields_with_col_info->{$column}->{SQL_DATA_TYPE};
        my $source_nullable      = $source_fields_with_col_info->{$column}->{NULLABLE};
        my $target_nullable      = $target_fields_with_col_info->{$column}->{NULLABLE};
        my $source_column_size   = $source_fields_with_col_info->{$column}->{COLUMN_SIZE};
        my $target_column_size   = $target_fields_with_col_info->{$column}->{COLUMN_SIZE};
        my $source_num_prec_radix= $source_fields_with_col_info->{$column}->{NUM_PREC_RADIX};
        my $target_num_prec_radix= $target_fields_with_col_info->{$column}->{NUM_PREC_RADIX};
        # check each column and each field so that data integrity is preserved
        # otherwise send an email and stop processing data to prevent corruption in the data.
        if($target_sql_data_type ne $source_sql_data_type) {
            $content = "SQL DATA TYPE $source_sql_data_type for SOURCE does not match target $target_sql_data_type for Column '$column'\n";
            &send_email(ALERT_EMAIL_FROM, ALERT_EMAIL_TO, $subject, $content);
            print "Terminating the process because $content";
            exit(0);
        }
        if($source_nullable ne $target_nullable) {
            $content = "NULLABLE $source_nullable for SOURCE does not match target $target_nullable for Column '$column'\n";
            &send_email(ALERT_EMAIL_FROM, ALERT_EMAIL_TO, $subject, $content);
            print "Terminating the process because $content";
            exit(0);
        }
        if($source_column_size > $target_column_size) {
            $content = "COLUMN_SIZE $source_column_size for SOURCE does not match target $target_column_size for Column '$column'\n";
            &send_email(ALERT_EMAIL_FROM, ALERT_EMAIL_TO, $subject, $content);
            print "Terminating the process because $content";
            exit(0);
        }
        if($source_num_prec_radix > $target_num_prec_radix) {
            $content = "NUM PREC RADIXE $source_num_prec_radix for SOURCE does not match target $target_num_prec_radix for Column '$column'\n";
            &send_email(ALERT_EMAIL_FROM, ALERT_EMAIL_TO, $subject, $content);
            print "Terminating the process because $content";
            exit(0);
        }

        # SQL type debugging
        if (DEBUG) {
            print "$column|SQL_DATA_TYPE | SOURCE = $source_sql_data_type | TARGET = $target_sql_data_type\n";
            print "$column|NULLABLE      | SOURCE = $source_nullable      | TARGET = $target_nullable\n";
            print "$column|COLUMN_SIZE   | SOURCE = $source_column_size   | TARGET = $target_column_size\n";
            print "$column|NUM_PREC_RADIX| SOURCE = $source_num_prec_radix| TARGET = $target_num_prec_radix\n" if DEBUG;
        }
    }
}

sub ddl_finalize_data($$$$) {
    if (dml_finalize_data(@_)) {
        OOCGI::Query->modify('DROP TABLE '.TEMP_TARGET_TABLE);
        return 1;
    }
    return 0;
}

sub dml_finalize_data($$$$) {
    # this case for DELETING all data from TARGET table which is least efficient, however
    # user will NOT lose dependent objects. 
    my $old_sql   = shift;
    my $new_count = shift;
    my $percentage= shift;
    my $force_percentage = shift;
    if($percentage <= $force_percentage) {
        #  start transaction
        OOCGI::Query->begin_work();
        OOCGI::Query->modify('DELETE FROM '.TARGET_TABLE);
        OOCGI::Query->modify('INSERT INTO '.TARGET_TABLE.' (SELECT * FROM '.TEMP_TARGET_TABLE.')');
        my $old_count = OOCGI::Query->new($old_sql)->node(0,0);
        if($old_count == $new_count) {
            print "Everything seems OK, committing the operation.\n";
            OOCGI::Query->modify('DELETE FROM '.TEMP_TARGET_TABLE);
            OOCGI::Query->commit(TARGET_TABLE. ' table populated successfully');
        } else {
            my $content = "During copy operation ".TARGET_TABLE." count ($old_count) did not match processed records ($new_count)"; 
            OOCGI::Query->rollback($content);
            my $subject = "Counts Do Not Match";
            &send_email(ALERT_EMAIL_FROM, ALERT_EMAIL_TO, $subject, $content);
        }
        return 1;
    } else {
        my $content = "
Percentage of allowable change limit ($force_percentage) has been exceeded,
system found $percentage Percent difference.
Please compare ".TARGET_TABLE. ' and '.TEMP_TARGET_TABLE;
        my $subject = 'Percentage Limit Exceeded';
        &send_email(ALERT_EMAIL_FROM, ALERT_EMAIL_TO, $subject, $content);
    }
    return 0;
}

sub bulk_insert($$$) {
    my $table = shift;
    my $names = shift;
    my $bigarray = shift;
    OOCGI::Query->insert( $table,
        OOCGI::Query::ParamTuple->new(
            fields => $names,
            values => $bigarray
        )
   );
}
