#!/usr/local/USAT/bin/perl

# This script will looks for GPRS modem information in the device_setting table and populate
# it into the gprs_device table.

use strict;
use USAT::Database;

# Define a bunch of counters to print out at the end to keep track of what happened during execution
my $cingular_file_count 			= 0;

# First get a connection to the database
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
my $dbh = $DATABASE->{handle};
if(not defined $dbh)
{
	print "Failed to get a connection to the database!\n";
	exit 1;
}

# query for a list of devices that have reported GPRS modem information recently
my $lookup_device_iccid_stmt = $dbh->prepare("select device_id, device_setting_value from device_setting where device_setting_parameter_cd = :iccid and last_updated_ts > (sysdate - :days_old) order by last_updated_ts asc") or die "Couldn't prepare statement: " . $dbh->errstr;
$lookup_device_iccid_stmt->bind_param(":iccid", 'CCID');
$lookup_device_iccid_stmt->bind_param(":days_old", 120);
$lookup_device_iccid_stmt->execute() or die "Couldn't execute statement: " . $lookup_device_iccid_stmt->errstr;
while(my @data = $lookup_device_iccid_stmt->fetchrow_array())
{
	my $device_id = $data[0];
	my $iccid = $data[1];
	
	print "Processing Device ID $device_id, ICCID $iccid...\n";
	
	if(length($iccid) != 20)
	{
		print "ICCID \"$iccid\" failed validation check!\n";
		next;
	}
	
	# query for the current modem info in gprs_device table
	my $current_data_stmt = $dbh->prepare("select gprs_device_state_id, phone_number, device_id, imei, device_type_name, device_firmware_name, rssi from gprs_device where iccid = :iccid") or die "Couldn't prepare statement: " . $dbh->errstr;
	$current_data_stmt->bind_param(":iccid", $iccid);
	$current_data_stmt->execute() or die "Couldn't execute statement: " . $current_data_stmt->errstr;
	my @current_data = $current_data_stmt->fetchrow_array();
	$current_data_stmt->finish();
	
	my $current_state 		= $current_data[0];
	my $current_phone 		= $current_data[1];
	my $current_device_id 	= $current_data[2];
	my $current_imei 		= $current_data[3];
	my $current_device_type	= $current_data[4];
	my $current_firmware 	= $current_data[5];
	my $current_rssi	 	= $current_data[6];
	
	print "Current GPRS_DEVICE Data: state=$current_state, phone=$current_phone, device_id=$current_device_id, imei=$current_imei, device_type=$current_device_type, firmware=$current_firmware, rssi=$current_rssi\n";
	
	# now query for all the modem information to populate into gprs_device table
	my $settings_data_stmt = $dbh->prepare("select device_setting_parameter_cd, device_setting_value from device_setting where device_id = :device_id and device_setting_parameter_cd in ('CGMI', 'CGMR', 'CGSN', 'CNUM', 'CSQ') and device_setting_value <> 'OK'") or die "Couldn't prepare statement: " . $dbh->errstr;
	$settings_data_stmt->bind_param(":device_id", $device_id);
	$settings_data_stmt->execute() or die "Couldn't execute statement: " . $settings_data_stmt->errstr;
	
	my %settings_hash;
	
	while (my @settings_data = $settings_data_stmt->fetchrow_array())
	{
		print "Current device_setting data: $settings_data[0] = $settings_data[1]\n";
		$settings_hash{$settings_data[0]} = $settings_data[1];
	}
	
	$settings_data_stmt->finish();
	
	my $update_gprs_device_sql = "update gprs_device set ";
	my $params = 0;
	my %bind_params;
	
	if($current_state ne '5')
	{
		print "GPRS Device is Assigned: setting state = 5!\n";
		$update_gprs_device_sql .= ", " unless $params == 0;
		$update_gprs_device_sql .= 'gprs_device_state_id = :state, assigned_by = :assigned_by, assigned_ts = sysdate ';
		$bind_params{'state'} = 5;
		$bind_params{'assigned_by'} = 'SYSTEM';
		$params++;
	}
	
	if(defined $settings_hash{'CGMI'} && $settings_hash{'CGMI'} ne $current_device_type)
	{
		$update_gprs_device_sql .= ", " unless $params == 0;
		$update_gprs_device_sql .= 'device_type_name = :CGMI ';
		$bind_params{'CGMI'} = $settings_hash{'CGMI'};
		$params++;
	}
	
	if(defined $settings_hash{'CGMR'} && $settings_hash{'CGMR'} ne $current_firmware)
	{
		$update_gprs_device_sql .= ", " unless $params == 0;
		$update_gprs_device_sql .= 'device_firmware_name = :CGMR ';
		$bind_params{'CGMR'} = $settings_hash{'CGMR'};
		$params++;
	}
	
	if(defined $settings_hash{'CGSN'} && $settings_hash{'CGSN'} != $current_imei)
	{
		$update_gprs_device_sql .= ", " unless $params == 0;
		$update_gprs_device_sql .= 'imei = :CGSN ';
		$bind_params{'CGSN'} = $settings_hash{'CGSN'};
		$params++;
	}
	
	if(defined $settings_hash{'CNUM'} && $settings_hash{'CNUM'} ne $current_phone)
	{
		$update_gprs_device_sql .= ", " unless $params == 0;
		$update_gprs_device_sql .= 'phone_number = :CNUM ';
		$bind_params{'CNUM'} = $settings_hash{'CNUM'};
		$params++;
	}
	
	if(defined $settings_hash{'CSQ'} && $settings_hash{'CSQ'} ne $current_rssi)
	{
		$update_gprs_device_sql .= ", " unless $params == 0;
		$update_gprs_device_sql .= 'rssi = :CSQ ';
		$bind_params{'CSQ'} = $settings_hash{'CSQ'};
		$params++;
	}
	
	if($device_id ne $current_device_id)
	{
		$update_gprs_device_sql .= ", " unless $params == 0;
		$update_gprs_device_sql .= 'device_id = :device_id ';
		$bind_params{'device_id'} = $device_id;
		$params++;
	}
	
	$update_gprs_device_sql .= ' where iccid = :iccid';
	
	if($params == 0)
	{
		print "GPRS_DEVICE $iccid is up-to-date!\n";
	}
	else
	{
		print "Update SQL: $update_gprs_device_sql\n";
		
		my $update_gprs_device_stmt = $dbh->prepare($update_gprs_device_sql) or die "Couldn't prepare statement: " . $dbh->errstr;
		
		foreach my $key (keys %bind_params)
		{
			print "Update Binding: $key = $bind_params{$key}\n";
			$update_gprs_device_stmt->bind_param(":$key", $bind_params{$key});
		}
	
		$update_gprs_device_stmt->bind_param(":iccid", $iccid);
	
		$update_gprs_device_stmt->execute() or die "Couldn't execute statement: " . $update_gprs_device_sql->errstr;
		$update_gprs_device_stmt->finish();
	}
}

$lookup_device_iccid_stmt->finish();
$dbh->disconnect;

