#!/usr/local/USAT/bin/perl

use USAT::DeviceAdmin::DBObj;

my $sql = qq|
   SELECT dt.device_type_id, dt.device_type_desc 
     FROM device.device_type dt
    ORDER BY UPPER(dt.device_type_desc)
|;

#print "SQL $sql <br>";
my $qo = OOCGI::Query->new($sql);
if($qo->rowNumber) {
   print qq|with(milonic=new menuname("device_type")) {\n|;
   print qq|style=style=submenuStyleG;\n|;
   print qq|overflow="scroll";\n|;

   while(my @arr = $qo->next_array ) {
      my $ID   = $arr[0];
      my $data = quotemeta $arr[1];
      if($ID == 0) {
         print  qq|aI("showmenu=firw_0;url=/device_list.cgi?device_type=$ID&enabled='Y'&action=Search;text=$arr[1];")\n|;
      } elsif($ID == 1) {
         print  qq|aI("showmenu=firw_1;url=/device_list.cgi?device_type=$ID&enabled='Y'&action=Search;text=$arr[1];")\n|;
      } else {
         print  qq|aI("url=/device_list.cgi?device_type=$ID&enabled='Y'&action=Search;text=$arr[1];")\n|;
      }
   }
   print qq|}\n\n|;
}

print q|
drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
|;
