#!/usr/local/USAT/bin/perl

use OOCGI::Query;

# delete any divice admin session older than one day

my $SQL = "DELETE FROM da_session WHERE created_ts < (SYSDATE - 1)";
OOCGI::Query->new->modify($SQL);
