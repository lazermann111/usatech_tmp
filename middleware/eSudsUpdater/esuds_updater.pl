#!/usr/local/USAT/bin/perl

use strict;

use USAT::Database;
use DatabaseConfig;
use Config::Properties::Simple;

my $in      = shift(@ARGV);
my $config = Config::Properties::Simple->new(
	file => $in,
    validate => { 
    	file_id 						=> 'integer',
    	parameter_name 					=> 'string',
    	parameter_values 				=> 'string',
    	anti_parameter_name 			=> 'string',
    	anti_parameter_values 			=> 'string',
    	last_activity_days				=> 'integer',
    	version_days_old				=> 'integer',
    	max_pending_version_requests 	=> 'integer',
    	max_pending_file_transfers		=> 'integer',
    	needs_reboot					=> 'boolean',
    	file_transfer_packet_size		=> 'integer',
    	post_upgrade_version_request	=> 'boolean'
    },
    required => [qw{file_id parameter_name parameter_values last_activity_days version_days_old  max_pending_version_requests max_pending_file_transfers needs_reboot file_transfer_packet_size post_upgrade_version_request}]
   );

my $file_id							= $config->getProperty('file_id');
my $parameter_name 					= $config->getProperty('parameter_name');
my $parameter_values_str			= $config->getProperty('parameter_values');

my $anti_parameter_name 			= $config->getProperty('anti_parameter_name');
my $anti_parameter_values_str		= $config->getProperty('anti_parameter_values');

my $last_activity_days				= $config->getProperty('last_activity_days');
my $version_days_old 				= $config->getProperty('version_days_old');
my $max_pending_version_requests 	= $config->getProperty('max_pending_version_requests');
my $max_pending_file_transfers		= $config->getProperty('max_pending_file_transfers');

my $file_transfer_packet_size		= $config->getProperty('file_transfer_packet_size');

my $needs_reboot					= $config->getProperty('needs_reboot');
my $post_upgrade_version_request	= $config->getProperty('post_upgrade_version_request');

my @parameter_values = split(',', $parameter_values_str);
my @anti_parameter_values = split(',', $anti_parameter_values_str);

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $dbh = USAT::Database->new(config => $db_config);

&output("eSuds Updater running: file_id = $file_id");

&output("Counting existing version requests...");
my $count_existing_version_requests_aref = $dbh->query(
	query	=> 
	q{
		select count(1)
		from machine_command_pending, device, device_setting
		where machine_command_pending.machine_id = device.device_name
		and device.device_id = device_setting.device_id
		and device.device_type_id = 5
		and device.device_active_yn_flag = 'Y'
	    and device.last_activity_ts > (sysdate - ?)
	    and device_setting.device_setting_parameter_cd = ?
	    and device_setting.device_setting_value in (}.join(',', ('?') x scalar(@parameter_values)).q{)
	    } . (defined $anti_parameter_name ? " and not exists ( select 1 from device_setting ds where device.device_id = ds.device_id and ds.device_setting_parameter_cd = ? and ds.device_setting_value in (" . join(',', ('?') x scalar(@anti_parameter_values)) . "))" : " ") . q{ 
	    and device_setting.last_updated_ts < (sysdate - ?)
		and machine_command_pending.data_type = '83'
		and machine_command_pending.command = '0B'
		and machine_command_pending.execute_cd in ('P','S')
		and machine_command_pending.created_ts > (sysdate - 1)
	},
	values =>
	[
		$last_activity_days,
		$parameter_name,
		(@parameter_values),
		(defined $anti_parameter_name ? $anti_parameter_name : ()), 
		(defined $anti_parameter_name ? (@anti_parameter_values) : ()),
		$version_days_old
	]
);
	
my $existing_version_request_count = $count_existing_version_requests_aref->[0][0];
&output("There are $existing_version_request_count existing version requests... ");

my $version_request_count = ($max_pending_version_requests - $existing_version_request_count);
&output("Creating up to $version_request_count requests for version info for out-of-date devices...");
if($version_request_count > 0)
{
	my $version_requests_aref = $dbh->query(
		query => 
		q{
			select device_id, device_name
			from
			(
			    select device.device_id, device.device_name
			    from device, device_setting
			    where device.device_id = device_setting.device_id
			    and device.device_type_id = 5
			    and device.device_active_yn_flag = 'Y'
			    and device.last_activity_ts > (sysdate - ?)
			    and device_setting.device_setting_parameter_cd = ?
			    and device_setting.device_setting_value in (}.join(',', ('?') x scalar(@parameter_values)).q{)
			    } . (defined $anti_parameter_name ? " and not exists ( select 1 from device_setting ds where device.device_id = ds.device_id and ds.device_setting_parameter_cd = ? and ds.device_setting_value in (" . join(',', ('?') x scalar(@anti_parameter_values)) . "))" : " ") . q{ 
			    and device_setting.last_updated_ts < (sysdate - ?)
			    and not exists
			    (
			        select 1
			        from machine_command_pending
			        where machine_command_pending.machine_id = device.device_name
			        and machine_command_pending.data_type = '83'
			        and machine_command_pending.command = '0B'
			        and machine_command_pending.execute_cd in ('P','S')
			    )
			    order by device_setting.last_updated_ts desc
			)
			where rownum <= ?	
		},
		values => 
		[
			$last_activity_days,
			$parameter_name,
			(@parameter_values),
			(defined $anti_parameter_name ? $anti_parameter_name : ()), 
			(defined $anti_parameter_name ? (@anti_parameter_values) : ()),
			$version_days_old,
			$version_request_count
		]
	);
	
	foreach my $version_ref (@{$version_requests_aref})
	{
		my $device_id = $version_ref->[0];
		my $ev_number = $version_ref->[1];

		output("Creating version request for " . $device_id . ", " . $ev_number);
		
		$dbh->do(
			query => 
			q{
				insert into machine_command_pending(machine_id, data_type, command, execute_cd, execute_order) 
				values (?, ?, ?, ?, ?)
			},
			values =>
			[
				$ev_number, 
				'83', 
				'0B', 
				'P', 
				1
			]
		);
	}
}

&output("Counting existing file transfers...");
my $count_existing_file_transfer_aref = $dbh->query(
	query	=> 
	q{
		select count(1)
		from device_file_transfer, machine_command_pending, device, device_setting
		where machine_command_pending.machine_id = device.device_name
		and device.device_id = device_setting.device_id
		and device.device_type_id = 5
		and device.device_active_yn_flag = 'Y'
	    and device.last_activity_ts > (sysdate - ?)
	    and device_setting.device_setting_parameter_cd = ?
	    and device_setting.device_setting_value in (}.join(',', ('?') x scalar(@parameter_values)).q{)
	    } . (defined $anti_parameter_name ? " and not exists ( select 1 from device_setting ds where device.device_id = ds.device_id and ds.device_setting_parameter_cd = ? and ds.device_setting_value in (" . join(',', ('?') x scalar(@anti_parameter_values)) . "))" : " ") . q{ 
	    and device_setting.last_updated_ts > (sysdate - ?)
		and machine_command_pending.data_type = '7C' 
		and machine_command_pending.command  = device_file_transfer.device_file_transfer_id
		and machine_command_pending.execute_cd in ('P','S')
		and machine_command_pending.created_ts > (sysdate - (2/24))
		and device_file_transfer.device_id = device.device_id
		and device_file_transfer.file_transfer_id = ?
		and device_file_transfer.device_file_transfer_status_cd = 0
		and device_file_transfer.device_file_transfer_direct = 'O' 
		and machine_command_pending.machine_id = device.device_name
	},
	values =>
	[
		$last_activity_days,
		$parameter_name,
		(@parameter_values),
		(defined $anti_parameter_name ? $anti_parameter_name : ()), 
		(defined $anti_parameter_name ? (@anti_parameter_values) : ()),
		$version_days_old,
		$file_id
	]
);
	
my $existing_file_transfer_count = $count_existing_file_transfer_aref->[0][0];
&output("There are $existing_file_transfer_count pending file_transfers... ");

my $file_transfer_count = ($max_pending_file_transfers - $existing_file_transfer_count);
&output("Creating up to $file_transfer_count files transfers to upgrade devices...");

if($file_transfer_count > 0)
{
	my $upgrade_aref = $dbh->query(
		query	=> 
		q{
		    select device.device_id, device.device_name
	    	from device, device_setting
	    	where device.device_id = device_setting.device_id
	    	and device.device_type_id = 5
	    	and device.device_active_yn_flag = 'Y'
		    and device.last_activity_ts > (sysdate - ?)
		    and device_setting.device_setting_parameter_cd = ?
		    and device_setting.device_setting_value in (}.join(',', ('?') x scalar(@parameter_values)).q{)
		    } . (defined $anti_parameter_name ? " and not exists ( select 1 from device_setting ds where device.device_id = ds.device_id and ds.device_setting_parameter_cd = ? and ds.device_setting_value in (" . join(',', ('?') x scalar(@anti_parameter_values)) . "))" : " ") . q{ 
		    and device_setting.last_updated_ts > (sysdate - ?)
	    	and not exists
	    	(
	        	select 1
	        	from device_file_transfer dft, machine_command_pending mcp 
	        	where mcp.data_type = '7C' 
	        	and mcp.command  = dft.device_file_transfer_id
	        	and dft.device_id = device.device_id
	        	and dft.file_transfer_id = ? 
	        	and dft.device_file_transfer_status_cd = 0
	        	and dft.device_file_transfer_direct = 'O' 
	        	and mcp.machine_id = device.device_name
	        	and mcp.execute_cd in ('P','S')
	    	)
	    	and not exists
	    	(
	        	select 1
	        	from device_file_transfer dft, machine_command_pending_hist mcph
	        	where mcph.data_type = '7C' 
	        	and mcph.command  = dft.device_file_transfer_id
	        	and dft.device_id = device.device_id
	        	and dft.file_transfer_id = ?
	        	and dft.device_file_transfer_status_cd = 1
	        	and dft.device_file_transfer_direct = 'O' 
	        	and mcph.machine_id = device.device_name
	        	and mcph.execute_cd = 'A'
	        	and mcph.execute_date > (sysdate - 30)
	    	)
	    	order by device_setting.last_updated_ts desc
		},
		values	=> 
		[
			$last_activity_days,
			$parameter_name,
			(@parameter_values),
			(defined $anti_parameter_name ? $anti_parameter_name : ()), 
			(defined $anti_parameter_name ? (@anti_parameter_values) : ()),
			$version_days_old,
			$file_id,
			$file_id
		]
	);
	
	my $upgrade_counter = 0;
	foreach my $upgrade_ref (@{$upgrade_aref})
	{
		my $device_id = $upgrade_ref->[0];
		my $ev_number = $upgrade_ref->[1];
		
		output("Creating file transfer for " . $device_id . ", " . $ev_number);
		
		my $transfer_id_aref = $dbh->query(query => "select SEQ_DEVICE_FILE_TRANSFER_ID.nextval from dual");
		my $transfer_id = $transfer_id_aref->[0][0];
		
		$dbh->do(
			query => 
			q{
				insert into device_file_transfer
				(
					device_file_transfer_id, 
					device_id, 
					file_transfer_id, 
					device_file_transfer_direct, 
					device_file_transfer_status_cd, 
					device_file_transfer_pkt_size
				) 
				values (?, ?, ?, ?, ?, ?)
			},
			values =>
			[
				$transfer_id,
				$device_id,
				$file_id,
				'O',
				'0',
				$file_transfer_packet_size
			]
		);
		
		$dbh->do(
			query => 
			q{
				insert into machine_command_pending
				(
					machine_id, 
					data_type, 
					command, 
					execute_cd, 
					execute_order
				) 
				values (?, ?, ?, ?, ?)
			},
			values =>
			[
				$ev_number,
				'7C',
				$transfer_id,
				'P',
				'1'
			]
		);
		
		last if($upgrade_counter++ >= $file_transfer_count);
	}
}

if($needs_reboot)
{
	&output("Creating any pending reboot requests...");
	my $reboot_requests_aref = $dbh->query(
		query => 
		q{
			select device.device_id, device.device_name
			from device, device_file_transfer, file_transfer
			where device.device_id = device_file_transfer.device_id
			and device_file_transfer.file_transfer_id = file_transfer.file_transfer_id
			and device.device_type_id = 5
			and device.device_active_yn_flag = 'Y'
			and file_transfer.file_transfer_id = ?
			and device_file_transfer.device_file_transfer_direct = 'O' 
			and device_file_transfer.device_file_transfer_status_cd = 1
			and not exists
			(
			    select 1
			    from machine_command_pending_hist
			    where machine_command_pending_hist.machine_id = device.device_name
			    and machine_command_pending_hist.data_type = '83'
			    and machine_command_pending_hist.command = '01'
			    and machine_command_pending_hist.execute_date > device_file_transfer.device_file_transfer_ts
			)
			and not exists
			(
			    select 1
			    from machine_command_pending
			    where machine_command_pending.machine_id = device.device_name
			    and machine_command_pending.data_type = '83'
			    and machine_command_pending.command = '01'
			    and machine_command_pending.execute_cd in ('P','S')
			)
		},
		values =>
		[
			$file_id
		]
	);
	
	foreach my $reboot_ref (@{$reboot_requests_aref})
	{
		my $device_id = $reboot_ref->[0];
		my $ev_number = $reboot_ref->[1];

		output("Creating reboot request for " . $device_id . ", " . $ev_number);
		
		$dbh->do(
			query => 
			q{
				insert into machine_command_pending(machine_id, data_type, command, execute_cd, execute_order) 
				values (?, ?, ?, ?, ?)
			},
			values =>
			[
				$ev_number, 
				'83', 
				'01', 
				'P', 
				1
			]
		);
	}
}

if($post_upgrade_version_request)
{
	&output("Creating any pending version requests...");
	
	my $version_requests_aref;
	
	if($needs_reboot)
	{
		$version_requests_aref = $dbh->query(
			query => 
			q{
				select device.device_id, device.device_name
				from device, device_file_transfer, file_transfer
				where device.device_id = device_file_transfer.device_id
				and device_file_transfer.file_transfer_id = file_transfer.file_transfer_id
				and device.device_type_id = 5
				and device.device_active_yn_flag = 'Y'
				and file_transfer.file_transfer_id = ?
				and device_file_transfer.device_file_transfer_direct = 'O' 
				and device_file_transfer.device_file_transfer_status_cd = 1
				and not exists
				(
				    select 1
				    from machine_command_pending_hist
				    where machine_command_pending_hist.machine_id = device.device_name
				    and machine_command_pending_hist.data_type = '83'
				    and machine_command_pending_hist.command = '0B'
				    and machine_command_pending_hist.execute_date > device_file_transfer.device_file_transfer_ts
				)
				and not exists
				(
				    select 1
				    from machine_command_pending
				    where machine_command_pending.machine_id = device.device_name
				    and machine_command_pending.data_type = '83'
				    and machine_command_pending.command = '0B'
				    and machine_command_pending.execute_cd in ('P','S')
				)
				and exists
				(
				    select 1
				    from machine_command_pending_hist
				    where machine_command_pending_hist.machine_id = device.device_name
				    and machine_command_pending_hist.data_type = '83'
				    and machine_command_pending_hist.command = '01'
				    and machine_command_pending_hist.execute_date > device_file_transfer.device_file_transfer_ts
				)
			},
			values =>
			[
				$file_id
			]
		);
	}
	else
	{
		$version_requests_aref = $dbh->query(
			query => 
			q{
				select device.device_id, device.device_name
				from device, device_file_transfer, file_transfer
				where device.device_id = device_file_transfer.device_id
				and device_file_transfer.file_transfer_id = file_transfer.file_transfer_id
				and device.device_type_id = 5
				and device.device_active_yn_flag = 'Y'
				and file_transfer.file_transfer_id = ?
				and device_file_transfer.device_file_transfer_direct = 'O' 
				and device_file_transfer.device_file_transfer_status_cd = 1
				and not exists
				(
				    select 1
				    from machine_command_pending_hist
				    where machine_command_pending_hist.machine_id = device.device_name
				    and machine_command_pending_hist.data_type = '83'
				    and machine_command_pending_hist.command = '0B'
				    and machine_command_pending_hist.execute_date > device_file_transfer.device_file_transfer_ts
				)
				and not exists
				(
				    select 1
				    from machine_command_pending
				    where machine_command_pending.machine_id = device.device_name
				    and machine_command_pending.data_type = '83'
				    and machine_command_pending.command = '0B'
				    and machine_command_pending.execute_cd in ('P','S')
				)
			},
			values =>
			[
				$file_id
			]
		);
	}
	
	foreach my $version_ref (@{$version_requests_aref})
	{
		my $device_id = $version_ref->[0];
		my $ev_number = $version_ref->[1];

		output("Creating version request for " . $device_id . ", " . $ev_number);
		
		$dbh->do(
			query => 
			q{
				insert into machine_command_pending(machine_id, data_type, command, execute_cd, execute_order) 
				values (?, ?, ?, ?, ?)
			},
			values =>
			[
				$ev_number, 
				'83', 
				'0B', 
				'P', 
				1
			]
		);
	}
}

sub output
{
	my $line = shift;
	print "[" . localtime() . "] $line\n";
}
		
1;
