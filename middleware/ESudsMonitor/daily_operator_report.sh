RUN_DIR=/opt/eSudsMonitor
LOG_DIR=$RUN_DIR
MULTILOG=/usr/local/bin/multilog

cd $RUN_DIR
java -cp $RUN_DIR:$RUN_DIR/esudsmonitor.jar:$RUN_DIR/common.jar:$RUN_DIR/mail.jar:$RUN_DIR/activation.jar:$RUN_DIR/smtp.jar:$RUN_DIR/ojdbc14.jar:$RUN_DIR/commons-logging.jar:$RUN_DIR/log4j-1.2.8.jar  com.usatech.server.esuds.util.DiagnosticReportSender $RUN_DIR/dailydiagreport.properties 2>&1 | $MULTILOG s104857600 n9 $LOG_DIR

