package com.usatech.server.esuds.util;

/**
 * @author pcowan
 */

import com.usatech.db.*;
import com.usatech.util.*;
import com.usatech.util.mail.*;

import org.apache.commons.logging.*;

import java.text.*;
import java.util.*;

public class DiagnosticReportSender
{
	private static Log log = LogFactory.getLog(DiagnosticReportSender.class);
	
	protected String 			smtpServer;
	protected int 				smtpPort;
	protected String			fromAddress;
	protected List<String> 		adminEmails 	= new ArrayList<String>();
	protected List<String> 		ccEmails 		= new ArrayList<String>();
	
	protected SMTPConnection 	smtp;
	
	public static void main(String args[])
	{
		if(args.length != 1)
		{
			System.out.println("Usage: java com.usatech.server.esuds.util.DiagnosticReportSender <props file>");
			System.exit(1);
		}
		
		Properties props = Util.loadProperties(args[0]);
		if(props == null)
		{
			System.out.println("Usage: java com.usatech.server.esuds.util.DiagnosticReportSender <props file>");
			System.exit(1);
		}
		
		new DiagnosticReportSender(props);
	}
	
	public DiagnosticReportSender(Properties props)
	{
		log.info("eSuds DiagnosticReportSender is Running @ " + Util.getTimestamp());
		
		// get our smtp server and port
		smtpServer = Util.getStringOrExit(props, "smtp.server");
		smtpPort = Util.getIntOrExit(props, "smtp.port");
		fromAddress = Util.getStringOrExit(props, "from.address");
		
		log.debug("SMTP Server: "+smtpServer);
		log.debug("SMPT Port: "+smtpPort);
		
		int reportDays = Util.getIntOrExit(props, "report.days");

		// load the list of admin emails
		String adminsStr = Util.getStringOrExit(props, "admin.email.addresses");
		Scanner adminsScanner = new Scanner(adminsStr);
		adminsScanner.useDelimiter(",");
		
		while(adminsScanner.hasNext())
		{
			String email = adminsScanner.next();
			log.debug("Adding admin email: "+email);
			adminEmails.add(email);
		}

		// load the list of CC emails
		String ccStr = Util.getStringOrExit(props, "cc.email.addresses");
		Scanner ccScanner = new Scanner(ccStr);
		ccScanner.useDelimiter(",");
		
		while(ccScanner.hasNext())
		{
			String email = ccScanner.next();
			log.debug("Adding CC email: "+email);
			ccEmails.add(email);
		}
		
		// load the list of customer ids
		String idsStr = Util.getStringOrExit(props, "customer.ids");
		Scanner idsScanner = new Scanner(idsStr);
		idsScanner.useDelimiter(",");
		
		List<Integer> customerIDs = new ArrayList<Integer>();
		while(idsScanner.hasNextInt())
		{
			int customerID = idsScanner.nextInt();
			log.debug("Loading customer ID: "+customerID);
			customerIDs.add(customerID);
		}
		
		DateFormat subjectDateFormat = new SimpleDateFormat("EEE, MMM dd, yyyy");
		String subjectDateStamp = subjectDateFormat.format(new Date());
		log.debug("Report datestamp: " + subjectDateStamp);
		
		DateFormat fileDateFormat = new SimpleDateFormat("MMddyyyy");
		String fileDateStamp = fileDateFormat.format(new Date());
		
		StringBuilder textBody = new StringBuilder();
		textBody.append("Please see attached reports in HTML and CSV formats.");
		textBody.append(Util.WINDOWS_LB);
		textBody.append(Util.WINDOWS_LB);
		textBody.append("This report includes data from the last " + reportDays + " day(s).");
		textBody.append(Util.WINDOWS_LB);
		textBody.append(Util.WINDOWS_LB);
		textBody.append("The eSuds System Installation Manual is available online at: ");
		textBody.append(Util.WINDOWS_LB);
		textBody.append("http://www.usatech.com/manuals/eSuds_Installation_Manual.pdf");
		textBody.append(Util.WINDOWS_LB);
		textBody.append(Util.WINDOWS_LB);
		textBody.append("USA Technologies Customer Care may be reached at:");
		textBody.append(Util.WINDOWS_LB);
		textBody.append(Util.WINDOWS_LB);
		textBody.append(" 24 Spring Mill Drive");
		textBody.append(Util.WINDOWS_LB);
		textBody.append(" Malvern, PA 19355");
		textBody.append(Util.WINDOWS_LB);
		textBody.append(" Phone: 888.561.4748   Fax: 610.989.9695");
		textBody.append(Util.WINDOWS_LB);
		textBody.append(" Email: customersupport@usatech.com");
		textBody.append(Util.WINDOWS_LB);
		textBody.append(Util.WINDOWS_LB);
		
		// create a list to store the result of each report generation for sending after all reports have been generated
		List<ReportMessage> messages = new ArrayList<ReportMessage>();
		
		// keep a count of errors encountered so we know how bad the issue is
		int errorCount = 0;

		// now iterate through each customer id
		for(int customerID : customerIDs)
		{
			ReportMessage reportMessage = new ReportMessage();
			reportMessage.customerID = customerID;
			
			// load the list of emails to send to for this customer
			String emailsStr = Util.getStringOrExit(props, "customer." + customerID + ".email.addresses");
			Scanner emailsScanner = new Scanner(emailsStr);
			emailsScanner.useDelimiter(",");
			
			List<String> emails = new ArrayList<String>();
			while(emailsScanner.hasNext())
			{
				String email = emailsScanner.next();
				log.debug(customerID + ":\t" + "Adding Operator email: "+email);
				emails.add(email);
			}
			
			reportMessage.emails = emails;
			
			// generate the report
			try
			{
				log.info(customerID + ":\t" + "Generating Operator Diagnostic Report...");
				OperatorDiagnosticReport rept = new OperatorDiagnosticReport(customerID, reportDays);
				log.info(customerID + ":\t" + "Report generated successfully!");

				reportMessage.success = true;
				reportMessage.subject = "eSuds Operator Daily Diagnostic Report for " + subjectDateStamp;
				reportMessage.textBody = textBody.toString();
				reportMessage.htmlBody = rept.getResultsAsHTML();
				reportMessage.attachmentName = "esuds-diag-" + fileDateStamp + ".csv";
				reportMessage.attachment = rept.getResultsAsCSV();
				
				messages.add(reportMessage);
			}
			catch(Exception e)
			{
				errorCount++;
				
				// there was a problem generating the report
				
				log.error(customerID + ":\t" + "Report generation failed: " + e.getMessage(), e);

				StringBuilder sb = new StringBuilder();
				sb.append("Operator Diagnostic Report Generation Failed for customer " + customerID + "!" + Util.WINDOWS_LB);
				sb.append(e.getLocalizedMessage() + Util.WINDOWS_LB);
				sb.append(Util.getStackTraceAsString(e));
				
				alertAdmin(sb.toString());
			}
		}
		
		log.debug("Sending " + messages.size() + " reports...");
		
		// initialize the SMTP connection; do this after report generation so the connection doesn't get closed due to
		// a timeout while we're querying the database to generate report which can take a while...
		try
		{
			smtp = new SMTPConnection(smtpServer, smtpPort);
		}
		catch(Exception e)
		{
			log.fatal("Failed to connect to SMTP server!", e);
			System.exit(1);
		}
		
		if(errorCount == customerIDs.size())
		{
			// there were no successfull reports generated... must be a major problem like a database outage
			// notify admin emails only
			
			log.fatal("No reports were generated successfully!  Notifying admin...");
			alertAdmin("No reports were generated successfully!  Please check the logs.");
			
			System.exit(1);
		}
		
		for(ReportMessage message : messages)
			message.send();
	}
		
	private void alertAdmin(String body)
	{
		if(smtp == null)
		{
			try
			{
				smtp = new SMTPConnection(smtpServer, smtpPort);
			}
			catch(Exception e)
			{
				log.fatal("Failed to connect to SMTP server!", e);
				System.exit(1);
			}
		}
		
		for(String toAddress : adminEmails)
			smtp.send(fromAddress, toAddress, null, "Fatal Error Generating eSuds Operator Diagnostic Reports!", body, null, SMTPConnection.TEXT_FORMAT);
	}
	
	protected class ReportMessage
	{
		protected int customerID;
		protected List<String> emails;
		protected String subject;
		protected String textBody;
		protected String htmlBody;
		protected String attachment;
		protected String attachmentName;
		protected boolean success;
		
		protected void send()
		{
			if(success)
			{
				// if generation was successfull send the message to the operator
				for(String toAddress : emails)
					smtp.send(fromAddress, toAddress, null, subject, textBody, htmlBody, attachment, "text/csv", attachmentName);
				
				// also send to anyone on the CC list
				for(String toAddress : ccEmails)
					smtp.send(fromAddress, toAddress, null, subject, textBody, htmlBody, attachment, "text/csv", attachmentName);
			}
			else
			{
				// if report generation failed only send to the admins
				for(String toAddress : adminEmails)
					smtp.send(fromAddress, toAddress, null, subject, textBody, htmlBody, attachment, "text/csv", attachmentName);
			}
		}
	}
}
