package com.usatech.server.esuds.util;

import com.usatech.db.*;
import com.usatech.util.*;
import com.usatech.util.mail.*;

import org.apache.commons.logging.*;

import java.sql.SQLException;
import java.text.*;
import java.util.*;

public class OperatorDiagnosticReport
{
	private static Log log = LogFactory.getLog(OperatorDiagnosticReport.class);

	private static int NUM_COLUMNS = 5;
	
	private ArrayList<String[]> results = new ArrayList<String[]>();
	
	public ArrayList<String[]>  getResults()
	{
		return results;
	}
	
	public String getResultsAsCSV()
	{
		StringBuilder msg = new StringBuilder();
		
		msg.append("\"SCHOOL\",\"RESIDENCE HALL\",\"LAUNDRY ROOM\",\"MACHINE\",\"DIAGNOSTIC\"");
		msg.append(Util.WINDOWS_LB);
		
		for(String[] line : results)
		{
			for(int i=0; i<NUM_COLUMNS; i++)
			{
				msg.append("\"");
				msg.append(line[i]);
				msg.append("\"");
				
				if(i < (NUM_COLUMNS-1))
					msg.append(",");
			}
			msg.append(Util.WINDOWS_LB);
		}
		
		return msg.toString();
	}
	
	public String getResultsAsHTML()
	{
		StringBuilder msg = new StringBuilder();
		
		msg.append("<html><body>");
		msg.append(Util.WINDOWS_LB);
		msg.append("<table cellspacing=\"0\" cellpadding=\"1\" border=\"1\">");
		msg.append(Util.WINDOWS_LB);
		msg.append(" <tr><th nowrap>SCHOOL</th><th nowrap>RESIDENCE HALL</th><th nowrap>LAUNDRY ROOM</th><th nowrap>MACHINE</th><th nowrap>DIAGNOSTIC</th></tr>");
		msg.append(Util.WINDOWS_LB);
		
		for(String[] line : results)
		{
			msg.append(" <tr>");
			
			for(int i=0; i<NUM_COLUMNS; i++)
			{
				msg.append("<td nowrap>");
				msg.append(line[i]);
				msg.append("</td>");
			}
			
			msg.append("</tr>");
			msg.append(Util.WINDOWS_LB);
		}
		
		msg.append("</table");
		msg.append(Util.WINDOWS_LB);
		msg.append("</body></html>");
		msg.append(Util.WINDOWS_LB);
		
		return msg.toString();

	}
	
	public OperatorDiagnosticReport(int customerID, int days) throws Exception
	{
		/*
		String customerInfoSQL = "select customer.customer_name from customer where customer_id = ?";
		Query customerInfoQuery = new Query(customerInfoSQL, args);
		customerInfoQuery.execute();
		if(customerInfoQuery.getException() != null)
		{
			customerInfoQuery.close();
			throw customerInfoQuery.getException();
		}
		
		if(!customerInfoQuery.next())
		{
			customerInfoQuery.close();
			throw new Exception("Customer not found: " + customerID);
		}
		
		String customerName = customerInfoQuery.getString(1);
		customerInfoQuery.close();
		*/
		
		String improperSettingsSQL = 
			"select school.location_name, dorm.location_name, room.location_name, " + 
			"host_type.host_type_desc || ' ' || host.host_label_cd, " +
			"'Improper Settings (see eSuds Manual: Configuring Options on Maytag Gen 2 Washers/Dryers)' " +
			"from host_diag_status, host, device, pos, location.location school, location.location campus, location.location dorm, location.location room, host_type " +
			"where host_diag_status.host_id = host.host_id " +
			"and host.device_id = device.device_id " +
			"and device.device_id = pos.device_id " +
			"and pos.location_id = room.location_id " +
			"and room.parent_location_id = dorm.location_id " +
			"and dorm.parent_location_id = campus.location_id " +
			"and campus.parent_location_id = school.location_id " +
			"and host.host_type_id = host_type.host_type_id " +
			"and device.device_active_yn_flag = 'Y' " +
			"and device.device_type_id = 5 " +
			"and host_diag_status.host_diag_cd = 251 " +
			"and host_diag_status.host_diag_start_ts > (sysdate-?) " + 
			"and pos.customer_id = ? " +
			"and host_diag_status.host_diag_clear_ts is null " +
			"order by school.location_name, campus.location_name, dorm.location_name, room.location_name, host.host_label_cd ";
		
		Vector improperSettingsArgs = new Vector();
		improperSettingsArgs.add(new Integer(days));
		improperSettingsArgs.add(new Integer(customerID));

		String machineDiagSQL = 
			"select school.location_name, dorm.location_name, room.location_name, " +
			"host_type.host_type_desc || ' ' || host.host_label_cd, " +
			"	'Code ' || host_diag_status.host_diag_cd  || ' @ ' || to_char(host_diag_status.host_diag_start_ts, 'MM/DD/YYYY HH:MI:SS AM') " +
			"from host_diag_status, host, device, pos, location.location school, location.location campus, location.location dorm, location.location room, host_type " +
			"where host_diag_status.host_id = host.host_id  " +
			"and host.device_id = device.device_id " +
			"and device.device_id = pos.device_id " +
			"and pos.location_id = room.location_id " +
			"and room.parent_location_id = dorm.location_id " +
			"and dorm.parent_location_id = campus.location_id " +
			"and campus.parent_location_id = school.location_id " +
			"and host.host_type_id = host_type.host_type_id " +
			"and device.device_active_yn_flag = 'Y' " +
			"and device.device_type_id = 5 " +
			"and host_diag_status.host_diag_cd < 200 " +
			"and host_diag_status.host_diag_start_ts > (sysdate-?) " + 
			"and pos.customer_id = ? " +
			"and host_diag_status.host_diag_clear_ts is null " +
			"order by school.location_name, campus.location_name, dorm.location_name, room.location_name, host.host_label_cd ";
		
		Vector machineDiagArgs = new Vector();
		machineDiagArgs.add(new Integer(days));
		machineDiagArgs.add(new Integer(customerID));

		String noUsageSQL = 
			"select school.location_name, dorm.location_name, room.location_name, " +  
			"host_type.host_type_desc || ' ' || host.host_label_cd, " + 
			"	'No Usage Since ' || to_char(host.host_last_start_ts, 'MM/DD/YYYY HH:MI:SS AM') " + 
			"from host, device, pos, location.location school, location.location campus, location.location dorm, location.location room, host_type " + 
			"where host.device_id = device.device_id " + 
			"and device.device_id = pos.device_id " + 
			"and pos.location_id = room.location_id " + 
			"and room.parent_location_id = dorm.location_id " + 
			"and dorm.parent_location_id = campus.location_id " + 
			"and campus.parent_location_id = school.location_id " + 
			"and host.host_type_id = host_type.host_type_id " + 
			"and host.host_last_start_ts < (sysdate - (?+4)) " + 
			"and device.last_activity_ts > (sysdate - ?) " + 
			"and device.device_active_yn_flag = 'Y' " + 
			"and device.device_type_id = 5 " + 
			"and pos.customer_id = ? " + 
			"order by school.location_name, campus.location_name, dorm.location_name, room.location_name, host.host_label_cd ";

		Vector noUsageArgs = new Vector();
		noUsageArgs.add(new Integer(days));
		noUsageArgs.add(new Integer(days));
		noUsageArgs.add(new Integer(customerID));

		String roomOfflineSQL = 		
			"select school.location_name, dorm.location_name, room.location_name, '-', 'Room Controller Offline Since ' || to_char(device.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM') " +
			"from device, pos, location.location school, location.location campus, location.location dorm, location.location room " +
			"where device.device_id = pos.device_id " +
			"and pos.location_id = room.location_id " +
			"and room.parent_location_id = dorm.location_id " +
			"and dorm.parent_location_id = campus.location_id " +
			"and campus.parent_location_id = school.location_id " +
			"and device.last_activity_ts > (sysdate - (?+1)) " +
//			"and device.last_activity_ts < (sysdate - ?) " +
			"and device.last_activity_ts < (sysdate - (3/24)) " +
			"and device.device_active_yn_flag = 'Y' " +
			"and device.device_type_id = 5 " +
			"and pos.customer_id = ? " +
			"order by school.location_name, campus.location_name, dorm.location_name, room.location_name ";

		Vector roomOfflineArgs = new Vector();
		roomOfflineArgs.add(new Integer(days));
//		roomOfflineArgs.add(new Integer(days));
		roomOfflineArgs.add(new Integer(customerID));

		results.addAll(execute(roomOfflineSQL, roomOfflineArgs, "Room Offline"));
		results.addAll(execute(machineDiagSQL, machineDiagArgs, "Maytag Diagnostics"));
		results.addAll(execute(improperSettingsSQL, improperSettingsArgs, "Improper Settings"));
		results.addAll(execute(noUsageSQL, noUsageArgs, "No Machine Usage"));

		// now create the CSV file
		
		/*
		messageContent.append("\"CUSTOMER NAME\",\"" + customerName + "\",\"\",\"\",\"\"");
		messageContent.append(Util.WINDOWS_LB);
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String dateStamp = dateFormat.format(new Date());
		messageContent.append("\"REPORT DATE\",\"" + dateStamp + "\",\"\",\"\",\"\"");
		messageContent.append(Util.WINDOWS_LB);
		*/
		
	}		
	
	private List<String[]> execute(String sql, Vector args, String title) throws Exception
	{
		log.info("Executing : " + title);
		log.info("SQL       : " + SQL.queryToString(sql, args));
		
		Query query = new Query(sql, args);
		if(!query.execute())
		{
			query.close();
			
			if(query.getException() != null)
				throw query.getException();
			else
				throw new SQLException("Failed to execute \"" + title + "\"");
		}
		
		List<String[]> rows = new ArrayList<String[]>();
		
		while(query.next())
		{
			String[] columns = new String[NUM_COLUMNS];
		
			for(int i=0; i<NUM_COLUMNS; i++)
				columns[i] = query.getString(i+1);
			
			rows.add(columns);
		}
		
		query.close();
		
		log.info("Results   : " + rows.size());
		
		return rows;
	}
}
