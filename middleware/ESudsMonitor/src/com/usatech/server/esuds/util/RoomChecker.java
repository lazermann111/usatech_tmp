package com.usatech.server.esuds.util;

/**
 * This class implements some semi-complex database checks that can not easily 
 * be performed via IPSentry.  Any results produced will be emailed to the 
 * provided addresses.
 * 
 * @author pcowan
 *  
 */

import com.usatech.db.*;
import com.usatech.util.*;
import com.usatech.util.mail.*;
import java.util.*;

public class RoomChecker
{
	private static final String[] HOST_STATUS_CODES = {"No Status Available", "Idle, Available", "In 1st Cycle", "Out Of Service", "Nothing on Port", 	"Idle, Not Available", "Manual Service Mode", "In 2nd Cycle", "Transaction In Progress"};
	
	private Vector emails = new Vector();
	private String smtpServer;
	private int smtpPort;
	
	private StringBuffer messageContent = new StringBuffer();
	
	public static void main(String args[])
	{
		if(args.length != 1)
		{
			System.out.println("Usage: java com.usatech.server.esuds.util.RoomChecker <props file>");
			System.exit(1);
		}
		
		Properties props = Util.loadProperties(args[0]);
		if(props == null)
		{
			System.out.println("Usage: java com.usatech.server.esuds.util.RoomChecker <props file>");
			System.exit(1);
		}
		
		new RoomChecker(props);
	}
	
	public RoomChecker(Properties props)
	{
		emails = Util.split(Util.getStringOrExit(props, "email.addresses"), ",");
		smtpServer = Util.getStringOrExit(props, "smtp.server");
		smtpPort = Util.getIntOrExit(props, "smtp.port");
		
		boolean foundIssue = false;
		
		boolean check254 = Util.getBooleanOrExit(props, "diag254.check");
		if(check254)
		{
			int includeMinutes = Util.getIntOrExit(props, "diag254.include.minutes");
			int minMinutes = Util.getIntOrExit(props, "diag254.min.minutes");
			
			String result = checkDiag254(includeMinutes, minMinutes);
			if(result != null)
			{
				foundIssue = true;
				messageContent.append(result);
			}
		}
		
		boolean check251 = Util.getBooleanOrExit(props, "diag251.check");
		if(check251)
		{
			int includeMinutes = Util.getIntOrExit(props, "diag251.include.minutes");
			
			String result = checkDiag251(includeMinutes);
			if(result != null)
			{
				foundIssue = true;
				messageContent.append(result);
			}
		}

		boolean check255 = Util.getBooleanOrExit(props, "diag255.check");
		if(check255)
		{
			String result = checkDiag255();
			if(result != null)
			{
				foundIssue = true;
				messageContent.append(result);
			}
		}

		boolean checkTranFlood = Util.getBooleanOrExit(props, "tranflod.check");
		if(checkTranFlood)
		{
			int includeMinutes = Util.getIntOrExit(props, "tranflod.include.minutes");
			int maxTrans = Util.getIntOrExit(props, "tranflod.max.transactions");
			
			String result = checkTranFlood(includeMinutes, maxTrans);
			if(result != null)
			{
				foundIssue = true;
				messageContent.append(result);
			}
		}

		boolean checkNoStatus = Util.getBooleanOrExit(props, "nostatus.check");
		if(checkNoStatus)
		{
			int activeDaysAgo = Util.getIntOrExit(props, "nostatus.active.days");
			int includeMinutes = Util.getIntOrExit(props, "nostatus.include.minutes");
			
			String result = checkNoStatus(activeDaysAgo, includeMinutes);
			if(result != null)
			{
				foundIssue = true;
				messageContent.append(result);
			}
		}
		
		if(foundIssue)
		{
			String styleBlock = 
				"<style type=\"text/css\">" +
				"	.base     { font-size:10pt; color:#000000; font-weight:normal; font-family:'Courier New', monospace; }" + 
				"	.heading  { font-size:10pt; color:#000000; font-weight:bold; font-family:'Courier New', monospace; }" +
				"</style>";

			messageContent.insert(0, "<html><body>" + Util.LB + styleBlock);
			messageContent.append("</body></html>" + Util.LB);
			
			try
			{
				SMTPConnection smtp = new SMTPConnection(smtpServer, smtpPort);
				Enumeration e = emails.elements();
				while(e.hasMoreElements())
				{
					String email = (String) e.nextElement();
					smtp.send("rerixengine@usatech.com", email, null, "eSuds Problem Detected", null, messageContent.toString(), SMTPConnection.HTML_FORMAT);  
				}
			}
			catch(Exception e)
			{
				System.out.println("Failed to connect to SMTP server at " + smtpServer + ":" + smtpPort + ": " + e);
				System.exit(1);
			}
		}
	}
	
	private String checkDiag254(int includeMinutes, int minMinutes)
	{
		String sql = 
			"select customer.customer_name, location.location_name, device.device_serial_cd, device.device_name, host.host_port_num, decode(host.host_position_num, 0, 'Bottom', 1, 'Top'), host.host_label_cd, " + 
			"decode(host.host_status_cd, '0', 'No Status Available', " +
			"	'1', 'Idle, Available', " +
			"	'2', 'In 1st Cycle', " +
			"	'3', 'Out Of Service', " +
			"	'4', 'Nothing on Port', " +
			"	'5', 'Idle, Not Available', " +
			"	'6', 'Manual Service Mode', " +
			"	'7', 'In 2nd Cycle',  " +
			"	'8', 'Transaction In Progress'), " +
			"to_char(host_diag_status.host_diag_start_ts, 'MM/DD/YY HH24:MI:SS') " + 
			"from device, pos, location.location, customer, device_setting, host, host_diag_status " +
			"where device.device_id = pos.device_id " +
			"and pos.location_id = location.location_id " +
			"and pos.customer_id = customer.customer_id " + 
			"and host.device_id = device.device_id " +
			"and host_diag_status.host_id = host.host_id " +
			"and device_setting.device_id = device.device_id " + 
			"and device.device_active_yn_flag = 'Y' " + 
			"and device.device_type_id = 5 " +
			"and device_setting.device_setting_parameter_cd = 'Monitor Room' " + 
			"and device_setting.device_setting_value = 'Y' " + 
			"and host_diag_status.host_diag_cd = 254 " +
			"and host_diag_status.host_diag_start_ts < (sysdate-(?/1440)) " + 
			"and host_diag_status.host_diag_start_ts > (sysdate-(?/1440)) " + 
			"and host_diag_status.host_diag_clear_ts is null ";
		
		Vector args = new Vector();
		args.addElement(new Integer(minMinutes));
		args.addElement(new Integer(includeMinutes));
		
		String[] columns = {"Customer", "Location", "Serial Number", "EV Number", "Port", "Position", "Label", "Status", "Diagnostic Start Time"};
		return executeCheck(sql, args, columns, "The Following Machines Have Been Reporting Diagnostic Code 254 For At Least " + minMinutes + " Minutes!");
	}
	
	private String checkDiag251(int includeMinutes)
	{
		String sql = 
			"select customer.customer_name, location.location_name, device.device_serial_cd, device.device_name, host.host_port_num, decode(host.host_position_num, 0, 'Bottom', 1, 'Top'), host.host_label_cd, " + 
			"decode(host.host_status_cd, '0', 'No Status Available', " +
			"	'1', 'Idle, Available', " +
			"	'2', 'In 1st Cycle', " +
			"	'3', 'Out Of Service', " +
			"	'4', 'Nothing on Port', " +
			"	'5', 'Idle, Not Available', " +
			"	'6', 'Manual Service Mode', " +
			"	'7', 'In 2nd Cycle',  " +
			"	'8', 'Transaction In Progress'), " +
			"to_char(host_diag_status.host_diag_start_ts, 'MM/DD/YY HH24:MI:SS') " +  
			"from device, pos, location.location, customer, device_setting, host, host_diag_status " +
			"where device.device_id = pos.device_id " +
			"and pos.location_id = location.location_id " + 
			"and pos.customer_id = customer.customer_id " +
			"and host.device_id = device.device_id " +
			"and host_diag_status.host_id = host.host_id " + 
			"and device_setting.device_id = device.device_id " + 
			"and device.device_active_yn_flag = 'Y' " +
			"and device.device_type_id = 5 " +
			"and device_setting.device_setting_parameter_cd = 'Monitor Room' " + 
			"and device_setting.device_setting_value = 'Y' " +
			"and host_diag_status.host_diag_cd = 251 " +
			"and host_diag_status.host_diag_start_ts > (sysdate-(?/1440)) " + 
			"and host_diag_status.host_diag_clear_ts is null ";

		Vector args = new Vector();
		args.add(new Integer(includeMinutes));
		
		String[] columns = {"Customer", "Location", "Serial Number", "EV Number", "Port", "Position", "Label", "Status", "Diagnostic Start Time"};
		return executeCheck(sql, args, columns, "The Following Machines Have Reported Diagnostic Code 251 In The Last " + includeMinutes + " Minutes!");
	}
	
	private String checkDiag255()
	{
		String sql = 
			"select distinct customer.customer_name, location.location_name, device.device_serial_cd, device.device_name " + 
			"from host, device, pos, location.location, customer, device_setting " +
			"where device.device_id = pos.device_id " +
			"and pos.location_id = location.location_id " + 
			"and pos.customer_id = customer.customer_id " +
			"and host.device_id = device.device_id " +
			"and device_setting.device_id = device.device_id " + 
			"and device.device_active_yn_flag = 'Y' " +
			"and device.device_type_id = 5 " +
			"and device_setting.device_setting_parameter_cd = 'Monitor Room' " + 
			"and device_setting.device_setting_value = 'Y' " +
			"and host.host_position_num = 0 " +
			"and host.host_id not in " +
			"( " +
			"    select host_diag_status.host_id " + 
			"    from host_diag_status " +
			"    where host_diag_cd = '255' " + 
			"    and host_diag_clear_ts is null " +
			")";
		
		Vector args = new Vector();
		
		String[] columns = {"Customer", "Location", "Serial Number", "EV Number"};
		return executeCheck(sql, args, columns, "The Following Devices Are NOT Currently Reporting Diagnostic Code 255 For At Least One Machine!");
	}
	
	private String checkTranFlood(int includeMinutes, int maxTrans)
	{
		String sql = 
			"select customer.customer_name, location.location_name, device.device_serial_cd, device.device_name, count(1) " + 
			"from device, pos, pos_pta, tran, customer, location.location, device_setting " +
			"where device.device_id = pos.device_id " +
			"and pos.pos_id = pos_pta.pos_id " +
			"and pos_pta.pos_pta_id = tran.pos_pta_id " +
			"and pos.customer_id = customer.customer_id " + 
			"and pos.location_id = location.location_id " +
			"and device_setting.device_id = device.device_id " + 
			"and device.device_active_yn_flag = 'Y' " +
			"and device.device_type_id = 5 " +
			"and device_setting.device_setting_parameter_cd = 'Monitor Room' " + 
			"and device_setting.device_setting_value = 'Y' " +
			"and tran.tran_start_ts > (sysdate-(?/1440)) " +
			"group by customer.customer_name, location.location_name, device.device_serial_cd, device.device_name " + 
			"having count(1) > ? ";

		Vector args = new Vector();
		args.addElement(new Integer(includeMinutes));
		args.addElement(new Integer(maxTrans));
		
		String[] columns = {"Customer", "Location", "Serial Number", "EV Number", "Transaction Count"};
		return executeCheck(sql, args, columns, "The Following Devices Are Flooding The Server With At Least " + maxTrans + " Transaction In The Last " + includeMinutes + " Minutes!");
	}
	
	private String checkNoStatus(int activeDays, int includeMinutes)
	{
		String sql = 
			"select customer.customer_name, location.location_name, device.device_serial_cd, device.device_name " + 
			"from device, pos, location.location, customer, device_setting " +
			"where device.device_id = pos.device_id " +
			"and pos.location_id = location.location_id " + 
			"and pos.customer_id = customer.customer_id " +
			"and device_setting.device_id = device.device_id " + 
			"and device.device_active_yn_flag = 'Y' " +
			"and device.device_type_id = 5 " +
			"and device.last_activity_ts > (sysdate-?) " +
			"and device_setting.device_setting_parameter_cd = 'Monitor Room' " + 
			"and device_setting.device_setting_value = 'Y' " +
			"and device.device_name not in " +
			"( " +
			"    select distinct(machine_id) " + 
			"    from MACHINE_COMMAND_INBOUND_HIST " + 
			"    where inbound_date > (sysdate-(?/1440)) " + 
			"    and inbound_command like '9A47%' " +
			") " +
			"order by customer_name, location_name ";

		Vector args = new Vector();
		args.addElement(new Integer(activeDays));
		args.addElement(new Integer(includeMinutes));
		
		String[] columns = {"Customer", "Location", "Serial Number", "EV Number"};
		return executeCheck(sql, args, columns, "The Following Devices Have Communicated In The Last " + activeDays + " Days But Have Not Reported Washer/Dryer Status in the last " + includeMinutes + " Minutes!");
	}
	
	private String executeCheck(String sql, Vector args, String[] columns, String title)
	{
		Util.output("Executing : " + title);
		Util.output("SQL       : " + SQL.queryToString(sql, args));
		
		Query query = new Query(sql, args);
		if(!query.execute())
		{
			System.out.println("Failed to execute \"" + title + "\": " + query.getException());
			if(query.getException() != null)
			{
				query.getException().printStackTrace(System.out);
			}
			System.exit(1);
		}
		
		int resultCount = 0;
		
		StringBuffer body = new StringBuffer();
		while(query.next())
		{
			body.append(" <tr>" + Util.LB);

			for(int i=1; i<=columns.length; i++)
			{
				if(i == 4)
				{
					body.append("  <td nowrap class=\"base\"><a href=\"http://device-admin.usatech.com/device_profile.cgi?ev_number=" + query.getString(i) + "\">" + (query.getString(i) == null ? "&nbsp;" : query.getString(i)) + "</a></td>" + Util.LB);
				}
				else
				{
					body.append("  <td nowrap class=\"base\">" + (query.getString(i) == null ? "&nbsp;" : query.getString(i)) + "</td>" + Util.LB);
				}
			}
			
			body.append(" </tr>" + Util.LB);
			
			resultCount++;
		}
		
		query.close();
		
		Util.output("Results   : " + resultCount);
		
		if(body.length() > 0)
		{
			StringBuffer header = new StringBuffer();
			header.append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">" + Util.LB);
			header.append("<tr><td class=\"heading\" colspan=\"" + columns.length + "\" bgcolor=\"#C0C0C0\">" + title + "</td></tr>" + Util.LB);
			header.append("<tr>");
			
			for(int i=0; i<columns.length; i++)
			{
				header.append("<th class=\"heading\">" + columns[i] + "</th>");
			}
			
			header.append("</tr>");
			
			body.insert(0, header.toString());
			body.append("</table><br>" + Util.LB);
			
			return body.toString();
		}
		
		return null;
	}
}
