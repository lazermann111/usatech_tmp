#!/usr/bin/perl -w

use strict;
use Net::SMTP;
use Evend::Database::Database;

my $DATABASE = Evend::Database::Database->new(	print_query => 1, 
												execute_flatfile=>1, 
												debug => 1, 
												debug_die => 0,
												print_query => 0);
print localtime()." Purging existing inbound command queue...\n";
$DATABASE->update(
		table			=> 'machine_command_inbound',
		update_columns	=> 'Execute_Cd',
		update_values	=> ['P'],
		where_columns	=> ['Execute_Cd in (?,?)'],
		where_values	=> ['N','F']);
$DATABASE->close();
$DATABASE = {};
undef $DATABASE;

my $engine = '/opt/ReRixEngine2/ReRixEngine.pl';
my ($runtime, $starttime, $exit_value, $dumped_core, $sleeptime, $loop_count);
$loop_count = 0;
$sleeptime = 5;

while(1)
{
	$loop_count++;
	$starttime = time();

	print (localtime() . " [run_rerix] ReRixEngine Starting ($loop_count) *********************\n");
	system($engine);
	print (localtime() . " [run_rerix] ReRixEngine Exiting  ($loop_count) *********************\n");

	$exit_value = $? >> 8;
	$dumped_core = $? & 128;
	$runtime = time() - $starttime;
	
	if($runtime > 60*60 || $exit_value == 1)
	{
		# if it ran for more than an hour or self-terminated safely, just restart it
		$sleeptime = 0;
	}
	elsif($runtime < 60*5)
	{
		# if it ran for less than 5 minutes, exponentially increase sleep time
		$sleeptime = $sleeptime * 2;
	}
	else
	{
		# between 5 minutes and an hour of run time, sleep 1 minutes
		$sleeptime = 60;
	}
	
	my $msg = localtime() . "\n\nExit Value: $exit_value\nDumped Core: $dumped_core\nRun Time: $runtime\nLoop Count: $loop_count\n\n";

	# don't do this on the dev server
	if ($exit_value == 0 || $exit_value == 255 || $dumped_core != 0)
	{
#		&send_email('rerix_engine@usatech.com','pcowan@usatech.com,4844671281@mobile.att.net,mheilman@usatech.com,4844674506@mobile.att.net','ReRix Engine Restarting', $msg);
	}
	print ($msg);
	
	sleep($sleeptime);
}

sub send_email
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = 'mail.usatech.com';
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}
