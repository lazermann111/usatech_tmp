#!/usr/bin/perl -w

use lib '/home/mknoll/ReRixEngine/';

use strict;

use Evend::Database::Database;

my $DATABASE = Evend::Database::Database->new(  print_query => 1,
												execute_flatfile=>1,
												debug => 1,
												debug_die => 0,
											);

print "SCU Commands sent at " . localtime() . "\n";

$DATABASE->query(query => "alter session set nls_date_format = 'MM/DD/YYYY HH24:MI:SS'");

#select * from machine a, rerix_inventory b
#where a.machine_arch = 'SCU' and
#a.machine_id = b.machine_id and
#nvl(a.inventory_date, to_date('01/01/2002','MM/DD/YYYY')) != b.inventory_start_date

my $array_ref = $DATABASE->query(query =>
						"select a.machine_id, b.inventory_start_date,count(*) ".
						"from machine a, rerix_inventory b, " .
							"table(b.inventory) c " .
						"where a.machine_arch = 'SCU' and " .
							"a.machine_id = b.machine_id and " .
							"((a.inventory_date is null) or " .
							"(a.inventory_date != b.inventory_start_date)) ".
						"group by a.machine_id, b.inventory_start_date");

foreach( @$array_ref )
{
	my ($machine_id, $inventory_start_date, $product_cnt) =@$_;

	print "Machine ID: $machine_id\n";
	print "Updated Start Date: $inventory_start_date\n";

	for(my $i = 1; $i <= $product_cnt; $i++)
	{
		$DATABASE->insert(
					table           => 'scu_command',
					insert_columns  => 'machine_id, command',
					insert_values   => [$machine_id, "product $i"],
						);
	}

	$DATABASE->update(
					table			=> 'machine',
					update_columns	=> 'inventory_date',
					update_values	=> [$inventory_start_date],
					where_columns	=> ['machine_id = ?'],
					where_values	=> [$machine_id]
						);

}

1;
