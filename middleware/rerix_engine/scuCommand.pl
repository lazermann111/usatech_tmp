#!/usr/bin/perl -w

use lib '/home/mknoll/ReRixEngine/';

use strict;

use Evend::Database::Database;

my $TIMEOUT = 3*60;
#my $RETRIES = (60*60*24)/$TIMEOUT;		# one day of retries
my $RETRIES = 5;

# hash key is command name, [ <ReRix command>, <Parameter number>,
#								< servicing subroutine>, <param1>, <param2>]

my %command_fetch = (
					AUTH_TYPE			=> ['3', 11, \&scu_template,
											'auth_type', 'c'],
					AUTH_PREFIXES		=> ['3', 12, \&scu_template,
											'auth_prefixes', 'a*'],
					AUTH_READ_TRACKS	=> ['3', 13, \&scu_template,
											'auth_read_tracks', 'c'],
					AUTH_TIMEOUT		=> ['3', 14, \&scu_template,
											'auth_timeout', 'c'],
					MAX_LOCAL_AUTHS		=> ['3', 15, \&scu_template,
											'max_local_auths', 'v'],
					MAX_VENDS			=> ['3', 16, \&scu_template,
											'max_vends', 'c'],
					MULTI_VEND_TIMEOUT	=> ['3', 17, \&scu_template,
											'multi_vend_timeout', 'c'],

					USE_PRINTER			=> ['3', 21, \&scu_template_flags,
											'use_printer', 'c'],
					USE_RFID			=> ['3', 22, \&scu_template_flags,
											'use_rfid', 'c'],
					USE_CARD_READER		=> ['3', 23, \&scu_template_flags,
											'use_card_reader', 'c'],
					ASK_RECEIPT_QUESTION=> ['3', 24, \&scu_template_flags,
											'ask_receipt_question', ''],
					VEND_TIMEOUT		=> ['3', 25, \&scu_template,
											'vend_timeout', 'v'],
					RFID_TIMEOUT		=> ['3', 26, \&scu_template,
											'rfid_timeout', 'v'],

					INVENTORY_KEYPAD_CODE	=> ['3', 33, \&scu_template,
											'inventory_keypad_code', 'a*'],

					TAX_TYPE				=> ['3', 1, \&tax_type,
											'', ''],
					TAX_PERCENTAGE			=> ['3', 2, \&tax_percentage,
											'', ''],
					COST_OF_GOODS_PERC		=> ['3', 3, \&cost_of_goods_perc,
											'', ''],



					AUTH_FAILED				=> ['1', 2, \&scu_messages,
											'AUTH_FAILED', 1],
					AUTH_AUTHORIZING		=> ['1', 3, \&scu_messages,
											'AUTH_AUTHORIZING', 1],
					AUTH_REMOVE_CARD		=> ['1', 4, \&scu_messages,
											'AUTH_REMOVE_CARD', 1],
					AUTH_INSERT_CARD		=> ['1', 5, \&scu_messages,
											'AUTH_INSERT_CARD', 1],

					AUTH_CARD_NOT_VALID		=> ['1', 6, \&scu_messages,
											'AUTH_CARD_NOT_VALID', 1],
					AUTH_CARD_NOT_READ		=> ['1', 7, \&scu_messages,
											'AUTH_CARD_NOT_READ', 1],
					AUTH_RFID_NOT_VALID		=> ['1', 8, \&scu_messages,
											'AUTH_RFID_NOT_VALID', 1],
					AUTH_RFID_NOT_READ		=> ['1', 9, \&scu_messages,
											'AUTH_RFID_NOT_READ', 1],

					AUTH_FAILED_NETWORK		=> ['1', 10, \&scu_messages,
											'AUTH_FAILED_NETWORK', 1],
					AUTH_OVER_LOCAL_LIMIT	=> ['1', 11, \&scu_messages,
											'AUTH_OVER_LOCAL_LIMIT', 1],

					AD_MESSAGE				=> ['1', 21, \&scu_messages,
											'AD_MESSAGE', 1],

					RECEIPT_PRINTING		=> ['1', 31, \&scu_messages,
											'RECEIPT_PRINTING', 1],
					RECEIPT_QUESTION		=> ['1', 32, \&scu_messages,
											'RECEIPT_QUESTION', 1],
					RECEIPT_TAKE			=> ['1', 33, \&scu_messages,
											'RECEIPT_TAKE', 1],
					RECEIPT_ERROR_PRE		=> ['1', 34, \&scu_messages,
											'RECEIPT_ERROR_PRE', 1],
					RECEIPT_ERROR_POST		=> ['1', 35, \&scu_messages,
											'RECEIPT_ERROR_POST', 1],
					RECEIPT_HEADER			=> ['1', 36, \&scu_messages,
											'RECEIPT_HEADER', 0],
					RECEIPT_FOOTER			=> ['1', 37, \&scu_messages,
											'RECEIPT_FOOTER', 0],

					VEND_SELECT				=> ['1', 40, \&scu_messages,
											'VEND_SELECT', 1],
					VEND_VENDING			=> ['1', 41, \&scu_messages,
											'VEND_VENDING', 1],
					VEND_TIME_OUT			=> ['1', 42, \&scu_messages,
											'VEND_TIME_OUT', 1],
					VEND_FAILED				=> ['1', 43, \&scu_messages,
											'VEND_FAILED', 1],
					VEND_CANCELLED			=> ['1', 44, \&scu_messages,
											'VEND_CANCELLED', 1],
					VEND_COMPLETE			=> ['1', 45, \&scu_messages,
											'VEND_COMPLETE', 1],
					VEND_AGAIN				=> ['1', 46, \&scu_messages,
											'VEND_AGAIN', 1],

					UNIT_FILL_ACK			=> ['1', 50, \&scu_messages,
											'UNIT_FILL_ACK', 1],
					UNIT_OUT_OF_SERVICE		=> ['1', 51, \&scu_messages,
											'UNIT_OUT_OF_SERVICE', 1],

					LED_AUTH_AUTHORIZING	=> ['1', 60, \&scu_messages,
											'LED_AUTH_AUTHORIZING', 1],
					LED_AUTH_FAILED			=> ['1', 61, \&scu_messages,
											'LED_AUTH_FAILED', 1],
					LED_VEND_SELECT			=> ['1', 62, \&scu_messages,
											'LED_VEND_SELECT', 1],
					LED_FAILED_NETWORK		=> ['1', 63, \&scu_messages,
											'LED_FAILED_NETWORK', 1],
					LED_CARD_NOT_VALID		=> ['1', 64, \&scu_messages,
											'LED_AUTH_CARD_NOT_VALID', 1],
					LED_CARD_NOT_READ		=> ['1', 65, \&scu_messages,
											'LED_AUTH_CARD_NOT_READ', 1],
					LED_VEND_CANCELLED		=> ['1', 66, \&scu_messages,
											'LED_VEND_CANCELLED', 1],

					);

my $DATABASE = Evend::Database::Database->new(  print_query => 1,
												execute_flatfile=>1,
												debug => 1,
												debug_die => 0,
											);

print "SCU Commands sent at " . localtime() . "\n";

my $array_ref = $DATABASE->select(
							table			=> 'scu_command',
							select_columns	=> 'command_id, machine_id, ' .
												'command, command_status, ' .
												'nvl(round((sysdate-sent_date)'.
															'*60*60*24), 0), ' .
												'retry',
							order			=> 'machine_id, command, ' .
												'decode(command_status, ' .
													"'S', 1, " .
													"'N', 2, 3)"
						);

# Check list of commands, if there's any 'N's for commands that have not been
# sent, send them

my $prev_machine_id = '';
my $prev_command = '';

foreach( @$array_ref )
{
#	sleep 4;

	my ($command_id, $machine_id, $command, $command_status, $age, $retry) =@$_;

	print "\n---------------------------------------------\n";
	print "Command ID: $command_id Machine: $machine_id Command: $command\n";
	print "Command Status: $command_status Age: $age Retry: $retry\n";

	# if it's a new machine_id and command, see if it's been sent

	if( ($prev_machine_id ne $machine_id) or
		($prev_command ne $command) )
	{
		if( $command_status ne 'S' )
		{
			# send the update, mark it as pending

			if ( exists $command_fetch{$command} )
			{
				my $message = &{$command_fetch{$command}->[2]} (
												$DATABASE,
												$machine_id,
												$command_fetch{$command}->[3],
												$command_fetch{$command}->[4] );

				print MakeHex( $command_fetch{$command}->[0] .
								chr($command_fetch{$command}->[1]) .
								$message ) . "\n";

				$DATABASE->insert(
							table           => 'Machine_Command',
							insert_columns  => 'Modem_ID, Command',
							insert_values   => [$machine_id,
											MakeHex(
											$command_fetch{$command}->[0] .
											chr($command_fetch{$command}->[1]) .
											$message )]
						);

				$DATABASE->update(
								table			=> 'scu_command',
								update_columns	=> 'command_status',
								update_values	=> ['S'],
								where_columns	=> ['command_id = ?'],
								where_values	=> [$command_id]
							);
			}
			elsif ( $command =~ /product (\d*)/ )
			{

				print "Product ID $1 requested\n";

				my $inv_ref = $DATABASE->select(
							table			=> 'rerix_inventory a, ' .
												'table(a.inventory) b, ' .
												'inventory_item c',
							select_columns	=> 'item_abbrev, item_description',
							where_columns	=> ['b.inventory_item_number = ' .
													'c.inventory_item_number',
												'machine_id = ?',
												'button_id = ?'],
							where_values	=> [$machine_id, $1],
						);

				if( defined $inv_ref->[0][0] )
				{
					$DATABASE->insert(
							table           => 'Machine_Command',
							insert_columns  => 'Modem_ID, Command',
							insert_values   => [$machine_id,
												MakeHex(pack('Cva*',
														0x32,
														($1 - 1),
														$inv_ref->[0][0]))]
						);

					$DATABASE->update(
								table			=> 'scu_command',
								update_columns	=> 'command_status',
								update_values	=> ['S'],
								where_columns	=> ['command_id = ?'],
								where_values	=> [$command_id]
							);
				}
				else
				{
					print "No inventory found\n";

					$DATABASE->update(
								table			=> 'scu_command',
								update_columns	=> 'command_status',
								update_values	=> ['E'],
								where_columns	=> ['command_id = ?'],
								where_values	=> [$command_id]
							);
				}
			}
			elsif ( $command eq 'all' )
			{
				foreach( keys %command_fetch )
				{
					$DATABASE->insert(
							table           => 'scu_command',
							insert_columns  => 'machine_id, command',
							insert_values   => [$machine_id, $_]
						);
				}

				$DATABASE->update(
								table			=> 'scu_command',
								update_columns	=> 'command_status',
								update_values	=> ['Y'],
								where_columns	=> ['command_id = ?'],
								where_values	=> [$command_id]
							);
			}
			else
			{

				print "Don't know how to handle command\n";

				$DATABASE->update(
							table			=> 'scu_command',
							update_columns	=> 'command_status',
							update_values	=> ['E'],
							where_columns	=> ['command_id = ?'],
							where_values	=> [$command_id]
						);
			}
		}
		elsif( ($command_status eq 'S') and
				(defined $age) and
				($age > $TIMEOUT) )
		{
			# the sent update has timed out, send another

			print "Command Time Out, resending\n";

			$DATABASE->update(
							table			=> 'scu_command',
							update_columns	=> 'command_status',
							update_values	=> ['T'],
							where_columns	=> ['command_id = ?'],
							where_values	=> [$command_id]
						);

			if( $retry < $RETRIES )
			{
				$DATABASE->insert(
							table           => 'scu_command',
							insert_columns  => 'machine_id, command, retry',
							insert_values   => [$machine_id, $command, $retry+1]
						);
			}
			else
			{
				print "Retry limit exceed, command will be lost\n";
			}
		}
	}

	$prev_machine_id = $machine_id;
	$prev_command = $command;
}


sub scu_template
{
	my ($DATABASE, $machine_id, $column, $packtype) = @_;

	print "scu_template(dbh, $machine_id, $column, $packtype)\n";

	my $array_ref = $DATABASE->select(
							table			=> 'machine a, scu_template b',
							select_columns	=> "$column",
							where_columns	=> ['a.scu_template_no = ' .
															'b.scu_template_no',
												'a.machine_id = ?'],
							where_values	=> [$machine_id],
						);

	if( $array_ref->[0][0] )
	{
		return( pack $packtype, $array_ref->[0][0] );
	}
	else
	{
		return '';
	}
}

sub scu_template_flags
{
	my ($DATABASE, $machine_id, $column, undef) = @_;

	print "scu_template_flags(dbh, $machine_id, $column, undef)\n";

	my $array_ref = $DATABASE->select(
							table			=> 'machine a, scu_template b',
							select_columns	=> "$column",
							where_columns	=> ['a.scu_template_no = ' .
															'b.scu_template_no',
												'a.machine_id = ?'],
							where_values	=> [$machine_id],
						);

	if( $array_ref->[0][0] )
	{
		return( pack 'c', ($array_ref->[0][0] eq 'Y')?1:0 );
	}
	else
	{
		return '';
	}
}



sub tax_percentage
{
	my ($DATABASE, $machine_id, undef, undef) = @_;

	print "tax_percentage(dbh, $machine_id, undef, undef)\n";

	my $array_ref = $DATABASE->select(
							table			=> 'machine a, location b',
							select_columns	=> "tax_percentage",
							where_columns	=> ['a.location_number = ' .
															'b.location_number',
												'a.machine_id = ?'],
							where_values	=> [$machine_id],
						);

	if( $array_ref->[0] )
	{
		print "Tax Rate: $array_ref->[0][0]%\n";
		return( pack 'v', ($array_ref->[0][0] * 1000) );
	}
	else
	{
		print "Tax Rate Not Found\n";

		return '';
	}
}


sub cost_of_goods_perc
{
	my ($DATABASE, $machine_id, undef, undef) = @_;

	print "cost_of_goods_perc(dbh, $machine_id, undef, undef)\n";

	my $array_ref = $DATABASE->select(
							table			=> 'machine a, location b',
							select_columns	=> "cost_of_goods_perc",
							where_columns	=> ['a.location_number = ' .
															'b.location_number',
												'a.machine_id = ?'],
							where_values	=> [$machine_id],
						);

	if( $array_ref->[0][0] )
	{
		print "Cost of Goods Percent: " . ($array_ref->[0][0]) . "\n";
		return( pack 'v', ($array_ref->[0][0]) );
	}
	else
	{
		print "Cost of Goods Percent Not Found\n";

		return '';
	}
}


sub tax_type
{
	my ($DATABASE, $machine_id, undef, undef) = @_;

	print "tax_type(dbh, $machine_id, undef, undef)\n";

	my $array_ref = $DATABASE->select(
							table			=> 'machine a, location b',
							select_columns	=> "tax_type",
							where_columns	=> ['a.location_number = ' .
															'b.location_number',
												'a.machine_id = ?'],
							where_values	=> [$machine_id],
						);

	if( $array_ref->[0][0] )
	{
		print "Tax Type: $array_ref->[0][0]\n";
		return( pack 'c', $array_ref->[0][0] );
	}
	else
	{
		print "Cost of Goods Percent Not Found\n";

		return '';
	}
}


sub scu_messages
{
	my ($DATABASE, $machine_id, $column, $extras) = @_;

	print "scu_messages(dbh, $machine_id, $column, $extras)\n";

	my $array_ref = $DATABASE->select(
							table			=> 'machine a, scu_messages b',
							select_columns	=> $extras?("$column, $column" .
														"_time, $column" .
														"_beep, $column"."_centered")
													:"$column",
							where_columns	=> ['a.message_set_no = ' .
															'b.message_set_no',
												'a.machine_id = ?'],
							where_values	=> [$machine_id],
						);

	if( $array_ref->[0][0] )
	{
		print "$column: $array_ref->[0][0]\n";

		my $message = $array_ref->[0][0];

		$message =~ s/\\n/\n/g;

		# if there are substution tags, process them
		if( $message =~ /\^/ )
		{
			my $location = $DATABASE->select(
							table			=> 'machine a, location b',
							select_columns	=> 'b.location_name, ' .
												'b.city, ' .
												'b.state',
							where_columns	=> ['a.location_number = ' .
														'b.location_number',
												'a.machine_id = ?'],
							where_values	=> [$machine_id],
						);

			$message =~ s/\^loc_name/$location->[0][0]/g;
			$message =~ s/\^loc_city_state/$location->[0][1] $location->[0][2]/g;
			$message =~ s/\^loc_city/$location->[0][1]/g;
			$message =~ s/\^loc_state/$location->[0][2]/g;
		}

		if( $extras )
		{
			my $type = ($array_ref->[0][2] eq 'Y' ? 0x01 : 0x00) |
						($array_ref->[0][3] eq 'Y' ? 0x02 : 0x00);

			return( pack 'cca*', $array_ref->[0][1], $type, $message);
		}
		else
		{
			return( pack 'cca*', 0, 0, $message );
		}
	}
	else
	{
		print "$column Not Found\n";

		return '';
	}
}

sub MakeHex
{
   my ($message) = @_;
   my @chars = unpack  'H*', $message;
   return join '', @chars;
}

1;
