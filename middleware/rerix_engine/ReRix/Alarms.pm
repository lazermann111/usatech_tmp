#!/usr/bin/perl -w

use strict;

package ReRix::Alarms;

use ReRix::Shared;
use ReRix::Send;

# SCU Alarms

sub parse_scu
{
	my ($DATABASE, $command_hashref) = @_;
	my @logs;
	
	push @logs, "Kodak parse_scu   : Disabled as of 12/24/2003";
	return(\@logs);

	#my $alarmbyte = ord(MakeString(substr $command_hashref->{inbound_command}, 2));

	#my $insert_columns = 'machine_id';

	#my @insert_values = ($command_hashref->{machine_id});

	#if ( $alarmbyte & 0x01 )
	#{
	#	$insert_columns .= ', card_swiper';
	#	push @insert_values, '1';
	#	push @logs, '     Alarm: Card Swiper Error';
	#}
	#if ( $alarmbyte & 0x02 )
	#{
	#	$insert_columns .= ', keypad';
	#	push @insert_values, '1';
	#	push @logs, '     Alarm: Keypad Error';
	#}
	#if ( $alarmbyte & 0x04 )
	#{
	#	$insert_columns .= ', mdb';
	#	push @insert_values, '1';
	#	push @logs, '     Alarm: MDB Error';
	#}
	#if ( $alarmbyte & 0x08 )
	#{
	#	$insert_columns .= ', frontboard';
	#	push @insert_values, '1';
	#	push @logs, '     Alarm: Frontboard Error';
	#}
	#if ( $alarmbyte & 0x10 )
	#{
	#	$insert_columns .= ', printer_not_responding';
	#	push @insert_values, '1';
	#	push @logs, '     Alarm: Printer Not Responding';
	#}
	#if ( $alarmbyte & 0x20 )
	#{
	#	$insert_columns .= ', printer_no_paper';
	#	push @insert_values, '1';
	#	push @logs, '     Alarm: Printer Has No Paper';
	#}
	#if ( $alarmbyte & 0x40 )
	#{
	#	$insert_columns .= ', printer_jam';
	#	push @insert_values, '1';
	#	push @logs, '     Alarm: Printer Jam';
	#}
	#if ( $alarmbyte & 0x80 )
	#{
	#	$insert_columns .= ', rfid';
	#	push @insert_values, '1';
	#	push @logs, '     Alarm: RFID Error';
	#}

	#$DATABASE->insert(
	#			table			=> 'scu_alarms',
	#			insert_columns	=> $insert_columns,
	#			insert_values	=> \@insert_values,
	#			);

	#return(\@logs);
}

sub parse_dex
{
	my ($DATABASE, $command_hashref) = @_;
	my @logs;
	
	push @logs, "Kodak parse_dex   : Disabled as of 12/24/2003";
	return(\@logs);

	#my ($workingbyte, $fields, $values);

	#my %errors;

	#my $message = MakeString($command_hashref->{inbound_command});

	#if( length($message) > 1 )
	#{
	#	$workingbyte = ord(substr($message,1,1));
	#}
	#else
	#{
	#	$workingbyte = 0;
	#}

	# column jammed 
	#(0x01 & $workingbyte) && do { $errors{COLUMN_JAMMED} = 1;};

	# Select Switch Error
	#(0x02 & $workingbyte) && do { $errors{SELECT_SWITCH_ERROR} = 1;};

	# Door Sense Error
	#(0x04 & $workingbyte) && do { $errors{DOOR_SENSE_ERROR} = 1;};

	# Coin Change Error
	#(0x08 & $workingbyte) && do { $errors{COIN_COMM_ERROR} = 1;};

	# Coin Changer Tube Sensor Error
	#(0x10 & $workingbyte) && do { $errors{COIN_TUBE_SENSOR_ERROR} = 1;};

	# Coin Changer Inlet Chute Error
	#(0x20 & $workingbyte) && do { $errors{COIN_INLET_CHUTE_ERROR} = 1;};

	# Coin Changer Tube Jammer
	#(0x40 & $workingbyte) && do { $errors{COIN_TUBE_JAMMED} = 1;};

	# Coin Changer Excessive Escrow
	#(0x80 & $workingbyte) && do { $errors{COIN_EXCESSIVE_ESCROW} = 1;};


	#  Byte 3
	#if( length($message) > 2 )
	#{
	#	$workingbyte = ord(substr($message,2,1));
	#}
	#else
	#{
	#	$workingbyte = 0;
	#}

	# Coin Changer Coin Jammed
	#(0x01 & $workingbyte) && do { $errors{COIN_JAMMED} = 1;};

	# Coin Changer Low Acceptance Rate
	#(0x02 & $workingbyte) && do { $errors{COIN_LOW_ACCEPTANCE} = 1;};

	# Bill Validator Stacker Full
	#(0x04 & $workingbyte) && do { $errors{BILL_STACKER_FULL} = 1;};

	# Bill Validator Motor Defective
	#(0x08 & $workingbyte) && do { $errors{BILL_MOTOR_DEFECTIVE} = 1;};

	# Bill Validator Staker Opened
	#(0x10 & $workingbyte) && do { $errors{BILL_STACKER_OPENED} = 1;};

	# Bill Validator Sensor Error
	#(0x20 & $workingbyte) && do { $errors{BILL_SENSOR_ERROR} = 1;};

	# Control Board Failure
	#(0x40 & $workingbyte) && do { $errors{CONTROL_BOARD_FAILURE} = 1;};

	# Control Board Enabled Error
	#(0x80 & $workingbyte) && do { $errors{CONTROL_BOARD_ENABLE_ERROR} = 1;};


	#  Byte 4
	#if( length($message) > 3 )
	#{
	#	$workingbyte = ord(substr($message,3,1));
	#}
	#else
	#{
	#	$workingbyte = 0;
	#}

	# Modem Quality Poor
	#(0x01 & $workingbyte) && do { $errors{MODEM_QUALITY_POOR} = 1;};

	# Modem Went Out of Coverage
	#(0x02 & $workingbyte) && do { $errors{MODEM_COVERAGE} = 1;};

	# Machine Power Outage
	#(0x04 & $workingbyte) && do { $errors{MACHINE_POWER_OUTAGE} = 1;};

	# Door Opened
	#(0x08 & $workingbyte) && do { $errors{DOOR_OPENED} = 1;};

	# Price Changed
	#(0x10 & $workingbyte) && do { $errors{PRICE_CHANGED} = 1;};

	# Serial Number Changed
	#(0x20 & $workingbyte) && do { $errors{SERIAL_NUMBER_CHANGED} = 1;};

	# Ver 1.71 DEX Clock
	#(0x40 & $workingbyte) && do { $errors{DEX_CLOCK_FAILURE} = 1;};

	# Multi-Door Openings
	#(0x80 & $workingbyte) && do { $errors{MULT_DOOR_OPEN} = 1;};

	#  Byte 5
	#if( length($message) > 4 )
	#{
	#	$workingbyte = ord(substr($message,4,1));
	#}
	#else
	#{
	#	$workingbyte = 0;
	#}

	# Temperature Error
	#(0x01 & $workingbyte) && do { $errors{TEMPERATURE_ERROR} = 1;};

	# Card Swipe
	#(0x02 & $workingbyte) && do { $errors{CARD_SWIPE} = 1;};

	# Printer Respond
	#(0x04 & $workingbyte) && do { $errors{PRINTER_RESPOND} = 1;};

	# Paper Out
	#(0x08 & $workingbyte) && do { $errors{PAPER_OUT} = 1;};

	# Handle Column Jam list
	#if( length($message) > 5 )
	#{
	#	$message = substr($message,5);

	#	while( length($message) )
	#	{
	#		$errors{COLUMNS_JAMMED} .= ord(substr $message,0,1) . ",";
	#		$message = substr($message,1);
	#	}

	#	if( length($errors{COLUMNS_JAMMED}) > 0 )
	#	{
	#		$errors{COLUMNS_JAMMED} = substr($errors{COLUMNS_JAMMED}, 0, -1);
	#	}
	#}

	#$fields = join ', ', ('MACHINE_ID', keys %errors);
	#$values = [$command_hashref->{machine_id}];

	#foreach (keys %errors)
	#{
	#	push @{$values}, $errors{$_};
	#	push @logs, "    Alarm: $_";
	#}

	#$DATABASE->insert(
	#				table			=> 'RERIX_ALARMS',
	#				insert_columns	=> $fields,
	#				insert_values	=> $values
	#			);

	#return(\@logs);
} 

1;
