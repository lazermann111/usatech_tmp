#!/usr/bin/perl -w

use strict;

package ReRix::Settings;

use ReRix::Shared;
use ReRix::Send;

# SCU Settings

my %number_command = (
						'01' => 'TAX_TYPE',
						'02' => 'TAX_PERCENTAGE',
						'03' => 'COST_OF_GOODS_PERC',
						'0B' => 'AUTH_TYPE',
						'0C' => 'AUTH_PREFIXES',
						'0D' => 'AUTH_READ_TRACKS',
						'0E' => 'AUTH_TIMEOUT',
						'0F' => 'MAX_LOCAL_AUTHS',
						'10' => 'MAX_VENDS',
						'11' => 'MULTI_VEND_TIMEOUT',
						'15' => 'USE_PRINTER',
						'16' => 'USE_RFID',
						'17' => 'USE_CARD_READER',
						'18' => 'ASK_RECEIPT_QUESTION',
						'19' => 'VEND_TIMEOUT',
						'1A' => 'RFID_TIMEOUT',
						'21' => 'INVENTORY_KEYPAD_CODE',
					);

sub parse_req
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	# Remove the command letter, then generate each setting requested

	my $request_list = substr $command_hashref->{inbound_command}, 2;

	my $request;

	while( $request = substr( $request_list, 0, 2 ) )
	{

		push @logs, "Request: $request ($number_command{$request})";

		$DATABASE->insert(
					table			=> 'scu_command',
					insert_columns	=> 'machine_id, command',
					insert_values	=> [ $command_hashref->{machine_id},
										$number_command{$request} ]
					);

		$request_list = substr $request_list, 2;
	}

	return(\@logs);
}

sub parse_ack
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $ack_command_number = uc(substr $command_hashref->{inbound_command}, 2);

	my $ack_command = $number_command{ $ack_command_number };

	if( ! exists $number_command{ $ack_command_number } )
	{
		return ( ["     ACK Setting Unknown Setting ($ack_command_number)"] );
	}

	push @logs, "ACK Setting: $ack_command";

	$DATABASE->update(
				table			=> 'scu_command',
				update_columns	=> 'command_status',
				update_values	=> ['Y'],
				where_columns	=> ["command_status = 'S'",
									'command = ?',
									'machine_id = ?'],
				where_values	=> [ $ack_command,
										$command_hashref->{machine_id} ],
				);

	return( \@logs );
}


1;
