package ReRix::Update;

use strict;

use Evend::Database::Database;        # Access to the Database
use Evend::ReRix::Shared;
use ReRix::Send;
use ReRix::CallInRecord;

# Gx devices can't handle throwing any pending command at it except when it passes this command
# to say it's ready to accept an update command
sub update_status_req
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $response_no = $command_hashref->{response_no};
	
	if(not defined $device_id)
	{
		ReRix::Utils::send_alert("Update: No active device record found for $machine_id", $command_hashref);
		push @logs, "No active device record found for $machine_id";
		return(\@logs);
	}

	# client is asking if we have any updates to send it
	# query machine_command_pending to see if there's anything to send it
	
	my $count_pending_ref = $DATABASE->select(
									table			=> 'machine_command_pending',
									select_columns	=> 'count(1)',
									order			=> 'execute_order',
									where_columns	=> ['machine_id = ?', '(execute_cd = ? or (execute_cd = ? and execute_date < (sysdate-(90/86400))))'],
									where_values	=> [$machine_id, 'P', 'S'] );
								
	my $pending_count = $count_pending_ref->[0][0];
	my $updates_pending = 0;
	if($pending_count > 0)
	{
		# 1 means there are updates
		# would be nicer to respond with this number since we have it, so client will know how many more are coming...
		$updates_pending = 1;
	}

	push @logs, "Updates Pending   : $pending_count";

	my $response = pack('CHC', $response_no, '90', $updates_pending);
	push @logs, "Response          : " . unpack("H*", $response);

	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*", $response)] );

	if($updates_pending == 0)
	{
		&ReRix::CallInRecord::set_status($raw_handle, $machine_id, 'S');
		return(\@logs);	
	}
	
	# sleep a few seconds to make sure the command was sent
	sleep(3);

	# send 1 update at a time	
	my $return_ref = &send_pending($DATABASE, $command_hashref, 1);
	
	foreach(@{$return_ref})
	{
		push(@logs, $_);
	}
	
	&ReRix::CallInRecord::server_sent_config($raw_handle, $machine_id);

	return(\@logs);	
}

sub send_pending
{
	my (@logs);
	my ($DATABASE, $command_hashref, $num_to_send) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	
	push @logs, "Checking for pending commands for $machine_id";
	
	# this should give us anything that is (P)ending, or anything that was (S)ent 
	# but not acked and is older than 5 minutes
	
	my $pending_command_ref = $DATABASE->select(
									table			=> 'machine_command_pending',
									select_columns	=> 'machine_command_pending_id, data_type, command',
									order			=> 'execute_order',
									where_columns	=> ['machine_id = ?', '(execute_cd = ? or (execute_cd = ? and execute_date < (sysdate-(90/86400))))'],
									where_values	=> [$machine_id, 'P', 'S'] );
	
	if(not defined $pending_command_ref->[0])
	{
		push @logs, "No commands are pending!";
		return(\@logs);
	}

	my $num_sent = 0;
	foreach(@$pending_command_ref)
	{
		my $pending_id = $_->[0];
		my $data_type = $_->[1];
		my $command = $_->[2];
		if(not defined $command)
		{
			$command = "";
		}
		
		push @logs, "Pending Type      : " . $data_type;

		my $reponse_no = ReRix::Utils::get_message_number($DATABASE);
	
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'message_number, execute_cd',
							update_values	=> [$reponse_no, 'S'],
							where_columns	=> ['machine_command_pending_id = ?'],
							where_values	=> [$pending_id] );

		if(($data_type eq '7C') || ($data_type eq '7c'))
		{
			# 7C is a special case - the file transfer command needs to be constructed on the fly
			# command should contain device_file_transfer_id
			my $device_file_transfer_id = $command;
			push @logs, "File Transfer     : $device_file_transfer_id";
			foreach(@{ReRix::FileTransferOutgoing::send_file($DATABASE, $command_hashref, $reponse_no, $pending_id, $device_file_transfer_id)})
			{
				push(@logs, $_);
			}
		}
		elsif($data_type eq '88')
		{
			# 88 is a special case - the Poke command needs to be constructed on the fly
			# command should contain all the data except for the actual data to poke
			my $poke_info = $command;
			$poke_info = pack("H*", $poke_info);
			my ($memory_code, $memory_location, $poke_size) = unpack("aNN", $poke_info);
			
			if($memory_code eq 'B')
			{
				push @logs, "EEROM Poke        : code:$memory_code location:$memory_location size:$poke_size";
				foreach(@{ReRix::Gx::make_poke($DATABASE, $command_hashref, $reponse_no, $pending_id, $memory_code, $memory_location, $poke_size)})
				{
					push(@logs, $_);
				}
			}
			else
			{
				push @logs, "Other Poke        : $poke_info";
				
				my $msg = chr($reponse_no) . pack("H*",$data_type) . pack("H*", $command);
				push @logs, "Response          : " . unpack("H*", $msg);
		
				$DATABASE->insert(	table=> 'machine_command',
									insert_columns=> 'modem_id, command',
									insert_values=> [$machine_id, unpack("H*", $msg)] );
			}
		}
		else
		{
			# for all other commands, just contruct the message and add it to the outgoing queue
			my $msg = chr($reponse_no) . pack("H*",$data_type) . pack("H*", $command);
			push @logs, "Response          : " . unpack("H*", $msg);
	
			$DATABASE->insert(	table=> 'machine_command',
								insert_columns=> 'modem_id, command',
								insert_values=> [$machine_id, unpack("H*", $msg)] );
		}
		
		if(++$num_sent >= $num_to_send)
		{
			return(\@logs);
			push @logs, "Finished          : Sent max of $num_to_send messages";
		}
	}

	push @logs, "Finished          : Sent all $num_sent pending messages";
	return(\@logs);	
}

1;
