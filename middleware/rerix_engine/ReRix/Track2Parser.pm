#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		03/04/2004	pcowan		Initial concept design and creation
#									Addition of parser: parse_USATechTest_v1
#

package ReRix::Track2Parser;

use strict;
use Evend::Database::Database;        # Access to the Database

#
#	This will simply return the 1st 9 characters of the Track2 data.
#	This is typically the social security number
#
sub first_9_characters_parser
{
	my ($DATABASE, $command_hashref, $auth_data) = @_;
	return substr($auth_data, 0, 9);
}

1;