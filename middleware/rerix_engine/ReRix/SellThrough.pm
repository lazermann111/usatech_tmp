#!/usr/bin/perl -w

use strict;

package ReRix::SellThrough;

use ReRix::Shared;
use ReRix::Send;

# Fill/Sell Thru Complex/Simple

sub parse_baselines
{
	my ($DATABASE, $command_hashref) = @_;

	my (@logs);

	my ($button, $sellthrough, $soldout, $soldcash, $workinglong);
	my ($list, $row);

	my $message = MakeString($command_hashref->{inbound_command});

	my $command = substr($message, 0, 1);
	my $type;

	if( $command eq "\x22" or $command eq "\x23" )
	{
		$type = 'S';
	}
	else
	{
		$type = 'C';
	}

	my $fill = 'N';

	if( $command eq "\x23" or $command eq "\x25" )
	{
		$fill = 'F';
		update_inventory($DATABASE, $command_hashref->{machine_id});
	}

	my $timestamp;
	if( $command_hashref->{timestamp} )
	{
		$timestamp = $command_hashref->{timestamp};
	}
	else
	{
		$timestamp = $command_hashref->{inbound_date};
	}

	$message = substr($message,1);

	my %buttonHash;

	$button = 1;
	$list=[];
	my $BevMax= 0;

#	if (length $message > 240)
#	{
#		$BevMax = 1; 
#	}
#	elsif ( (length $message > 60) and ($type eq 'S') )
#	{
#		$BevMax = 1;
#	}

	my $bevbut; 

	# Get the Price into the Hash Price (button -> price)
	my %price;

	my $array_ref = $DATABASE->select(
						table			=> 'rerix_inventory A, ' .
											'table(a.inventory) B, ' .
											'rerix_pricing C,  ' .
											'table(c.rerix_price) D ',
						select_columns	=> 'b.button_id, ' .
											'b.inventory_item_number, d.price',
						where_columns	=> ['a.machine_id = c.machine_id',
											'b.button_id = d.button_id',
											'a.machine_id = ?'],
						where_values	=> [$command_hashref->{machine_id}]
					);

	foreach (@$array_ref)
	{
		$price{$_->[0]} = [$_->[1], (defined $_->[2])?$_->[2]:0];
	}                                                             

	# Ok that out of the way do the rest
	while( (length $message) > 1 )
	{
		if( $type eq 'C' )
		{
			($workinglong, $soldcash) = unpack "NN", $message;
			$message = substr($message, 8);

			$sellthrough =  $workinglong & 0x7fffffff;
			$soldout = ($workinglong & 0x80000000)?1:0;
		}
		else
		{
			($workinglong) = unpack "n", $message;
			$message = substr($message, 2);

			$sellthrough =  $workinglong & 0x7fff;
			$soldout = ($workinglong & 0x8000)?1:0;

			$soldcash = 0;
		}

		if (not exists $price{$button})
		{
			$price{$button} = ["NO INV",0];
		}

		if (not $BevMax)
		{
			$buttonHash{$button} = [$sellthrough,
									$soldcash,
									$soldout,
									$price{$button}->[0],
									$price{$button}->[1]];
		}
		else
		{
			$bevbut = BevMax_Num_To_Name($button);
			if (not exists $price{$bevbut})
			{
				$price{$bevbut} = ['NA', 0]; 
			}
			if (not $price{$bevbut}->[0])
			{
				$price{$bevbut}->[0] = 'NA'; 
			}
			if (not $price{$bevbut}->[1])
			{
				$price{$bevbut}->[1] = 0; 
			}

			$buttonHash{$bevbut} = [$sellthrough,
									$soldcash,
									$soldout,
									$price{$bevbut}->[0],
									$price{$bevbut}->[1]];
		}
		$button++;
	}

	my @sell_thru;
	my %utterhack;

	if (scalar(keys %buttonHash) >0)
	{
		# fetch old baseline, create deltas for rerix_sell_through

		my $sellthru = $DATABASE->select(
						table			=> "Rerix_Sell_through_Baseline A,".
											" table(a.sell_through) B",
						select_columns	=> "b.button_id, b.sell_through, " .
											"b.sales, b.sold_out, " .
											"b.inventory_item_number, " .
											"b.price, " .
											"a.baseline, a.sales_date",
						where_columns	=>
								["a.machine_id=? order by a.sales_date"],
						where_values	=> [$command_hashref->{machine_id}],
					);


		if (scalar @$sellthru)
		{
			if( ($fill eq 'F') and
				($sellthru->[0][7] eq $timestamp) )
			{
				return([['duplicate fill']]);
			}

			# $col = pop(@$sellthru);
			foreach my $col (@$sellthru)
			{
				my ($button_id, $total, $cash, $so, $inv, $price) = @$col;
				if (exists $utterhack{$button_id})
				{
					next;
				}

				if( $type eq 'S' )
				{
					$buttonHash{$button_id}->[0] |= ($total & 0xffff0000);
					$buttonHash{$button_id}->[1] = $cash +
									$buttonHash{$button_id}->[4] *
									($buttonHash{$button_id}->[0] - $total);
				}

				if( (	($command_hashref->{machine_id} eq 'EV14420') or
						($command_hashref->{machine_id} eq 'EV14523') or
						($command_hashref->{machine_id} eq 'EV14429') or
						($command_hashref->{machine_id} eq 'EV14562') )
					and $type eq 'C' )
				{
					$buttonHash{$button_id}->[1] = $cash +
									$buttonHash{$button_id}->[4] *
									($buttonHash{$button_id}->[0] - $total);
				}

				push @sell_thru, [
									$button_id,
									$buttonHash{$button_id}->[0] - $total,
									$buttonHash{$button_id}->[1] - $cash,
									$buttonHash{$button_id}->[2],
									$buttonHash{$button_id}->[3],
									$buttonHash{$button_id}->[4]
								];

				$utterhack{$button_id}++;
			}

			$DATABASE->query(query => "alter session set nls_date_format = 'MM/DD/YYYY HH24:MI:SS'");

			$DATABASE->insert(
					table			=> 'Rerix_sell_through',
					insert_columns	=> 'machine_id, BASELINE, ' .
										'sales_date, ' .
										'sell_through',
					insert_values	=> [$command_hashref->{machine_id},
										$fill,
										$timestamp,
										\@sell_thru]
					);
		}

		foreach (keys %buttonHash)
		{
			push @$list, [$_,	$buttonHash{$_}->[0] >= 0
											?$buttonHash{$_}->[0]:0,
								$buttonHash{$_}->[1] >= 0
											?$buttonHash{$_}->[1]:0,
								$buttonHash{$_}->[2] >= 0
											?$buttonHash{$_}->[2]:0,
								$buttonHash{$_}->[3] eq ''
											?'NO INV':$buttonHash{$_}->[3],
								$buttonHash{$_}->[4] >= 0
											?$buttonHash{$_}->[4]:0];
		}

		if( ((scalar @$sellthru) and ($type eq 'S')) or ($type eq 'C') )
		{
			$DATABASE->query(query => "alter session set nls_date_format = 'MM/DD/YYYY HH24:MI:SS'");

			$DATABASE->insert(
						table			=> 'Rerix_Sell_Through_baseline',
						insert_columns	=> 'machine_id, BASELINE, sales_date,
											 SELL_THROUGH',
						insert_values	=> [$command_hashref->{machine_id},
											$fill,
											$timestamp,
											$list]
										);

			if( $type eq 'C' )
			{
				$DATABASE->insert(
					table           => 'Machine_Command',
					insert_columns  => 'Modem_ID, Command, Execute_cd',
					insert_values   => [$command_hashref->{machine_id},
											'2600', 'N']
       		         );
			}
		}
	}


	return (\@logs);
}

sub BevMax_Num_To_Name
{
	my ($num) = @_;
	$num--;
	return (chr(ord('A') + int($num/9)) . (($num % 9) + 1));
}

sub BevMax_Name_To_Num
{
	my ($name) = @_;
	return( 9* (ord(substr($name,0,1)) - ord('A')) + substr($name,1,1) );
}

sub update_inventory()
{
	my $DATABASE = shift;
	my $machine_id = shift;

	my $ref = $DATABASE->select(
					table			=> 'INVENTORY_FILL A, ' .
										'table(a.INV_FILL) b',
					select_columns 	=> "a.MACHINE_ID, " .
										"to_char(a.FILL_CREATE_DATE, " .
												"'MM/DD/YYYY HH24:MI:SS')," .
										"a.FILL_CD, a.FILL_DATE, b.COLUMN_ID, ".
										"b.INVENTORY_ITEM_NUMBER, " .
										"b.FILL_LEVEL, b.EXPIRE_DATE",
					where_columns	=> ['a.MACHINE_ID = ?'],
					where_values	=> [$machine_id],
					order			=> 'a.FILL_CREATE_DATE',
                             );


	if ( !defined $ref->[0] )  #in case there isn't a fill tix
	{
		return;
	}

	my $data_set;
	my $fill_date = $$ref[0][1];
	foreach my $row (@$ref)
	{
		if ($$row[1] ne $fill_date)   #then we've gotten to the next
		{                            #row and should move on
			last;  
		}
		my $col = $$row[4];
		my $item = $$row[5];
		my $level = $$row[6];
		push @$data_set, [$col, $item, $level]
	}

	$DATABASE->insert(	table			=> 'INVENTORY_LEVEL',
						insert_columns	=> 'MACHINE_ID, INV_LEVEL',
						insert_values	=> [$machine_id, $data_set],
					);

	$fill_date = $$ref[0][1];

	$DATABASE->query (	query	=> "update INVENTORY_FILL " .
									"set FILL_CD='A' " .
									"where machine_id = ? and " .
									"FILL_CREATE_DATE = " .
										"to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
						values	=> [$machine_id, $fill_date]
					);
}

1;
