#--------------------------------------------------------------------------------
# Description
#	Provides functionality to export G4 data from USANetwork System to other
#	processing systems
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		11/05/2003	T Shannon	First version
#									
# 1.5		02/24/2004	P Cowan		Quite a few changes have happened since version was increased.
#									In this case I made a small change to createG4ExportDEXRec
#									to check for 3 0x04's instead of just 1
#
# 1.51		04/29/2004	E Rybski	Made card number logging more secure (only partial card numbers shown)
#
# 1.52		05/10/2004	P Cowan		Bug fix for timestamp adjust problem.
#
# 1.53		08/06/04	P Cowan		Cleanup of Vended Items Description Block parsing.  Fixing a bug where the
#									the Amount value and sum/column was not being calculated correctly.
#

use strict;

package ReRix::G4Export;

use Evend::ReRix::Shared;
use DateTime;
use Time::Local;
use ReRix::Utils;

sub createG4ExportRec
{
	my @logs;
	my ($DATABASE, $machine_id, $device_id, $machine_trans_no, $vend_qty, $detail_data, $timestamp, $amt, $card, $card_type, $vend_col_bytes) = @_;
	
	my $device_serial_cd;
	my $ftype;
	my $dbh = $DATABASE->{handle};
	my @data;
	my $sth;
	#my $isdst;
	#my $tzone = 'EST';	#default to EST
	my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy);
	
	# get the timezone for the terminal's location
	#$sth = $dbh->prepare("select LOCATION_TIMEZONE from pss.vw_location_by_device_id where device_id = ?");
	#$sth->execute($device_id);
	#@data = $sth->fetchrow_array();
	#$tzone = $data[0] if defined $data[0];
	
	# determine if we have all of the data elements - if we didn't receive a card and card type
	# we're probably being called by net_batch so use the machine_trans_no to obtain that data
	if (!defined $card)
	{
		# get the card and card_type from the transaction_record_hist table; if not present try the
		# transaction_record
		$sth = $dbh->prepare("select card_number, card_type, to_char(transaction_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd') from tazdba.transaction_record_hist a where a.machine_trans_no = ?");
		$sth->execute($machine_trans_no);
		
		@data = $sth->fetchrow_array();
		
		# find out if we're in DST
		my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
		
		if (defined $data[0])
		{
			$card = $data[0];	
			if (defined $data[2])
			{ 	
				($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $data[2]);
				#$timestamp = ReRix::Utils::getAdjustedTimestampReverse($tzone, Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst));
				$timestamp = Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);
			}
			else
			{
				$timestamp = time();
				push @logs, "!!--TIMESTAMP NOT AVAILABLE FROM THE DATABASE - GENERATING A TIMESTAMP USING time()--!!";
			}
			push @logs, "Timestamp = $timestamp";
		
			# if the card_type was not passed (i.e., net_batch() doesn't pass it) then 
			# set the indicator here	
			if (!defined $card_type)
			{
				if ($data[1] eq 'SP')
				{
					$card_type = 'S';
				}
				elsif (defined $data[1])
				{
					# all other card types should be credit/debit cards
					$card_type = 'C';
				}
				else
				{
					push @logs, "Could not identify the card type - '" . $card_type ."'";
				}
					
			}
		}
		else
		{
			# if we reached here the record may not have been moved to the history table
			push @logs, "Could not find the transaction in transaction_record_hist...";
			$sth = $dbh->prepare("select card_number, card_type, to_char(transaction_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd') from tazdba.transaction_record_hist a where a.machine_trans_no = ?");
			$sth->execute($machine_trans_no);
			@data = $sth->fetchrow_array();

			if (defined $data[0])
			{
				$card = $data[0];
				if (defined $data[2])
				{ 	
					($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $data[2]);
					#$timestamp = ReRix::Utils::getAdjustedTimestampReverse($tzone, Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst));
					$timestamp = Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);
				}
				else
				{
					$timestamp = time();
					push @logs, "!!--TIMESTAMP NOT AVAILABLE FROM THE DATABASE - GENERATING A TIMESTAMP USING time()--!!";
				}
				push @logs, "Timestamp = $timestamp";

				# if the card_type was not passed (i.e., net_batch() doesn't pass it) then 
				# set the indicator here	
				if (!defined $card_type)
				{
					if ($data[1] eq 'SP')
					{
						$card_type = 'S';
					}
					elsif (defined $data[1])
					{
						# all other card types should be credit/debit cards
						$card_type = 'C';
					}
					else
					{
						push @logs, "Could not identify the card type - '" . $card_type ."'";
					}
				}
			}
			else
			{
				push @logs, "Could not find the transaction in transaction_record table either...";
				push @logs, "Aborting the process and writing a partial rec to the ERROR directory";
			}
		}
	}
		
		
	# the following loop will pull the column id's and vend amounts from the vended items block and place them in a hash
	my %vended_items;
		
	for (my $i = 0;  $i < $vend_qty; $i++)
	{
		my $item_tmp = substr($detail_data, (3 + $vend_col_bytes) * $i, (3 + $vend_col_bytes));

		my $item_nbr = unpack("H*",substr($item_tmp, 0, $vend_col_bytes));
		my $item_amt = substr($item_tmp, $vend_col_bytes, 3);

		$item_amt = (unpack("N", ("\x00".$item_amt))*.01);

		# add/update item in %vended_items hash - this allows us to accumulate quantities
		# by item number so we can insert summed col#/price/quantity rows into the TRANS_ITEM table.
		$vended_items{$item_nbr}{AMT} = sprintf("%.2f", ($vended_items{$item_nbr}{AMT} + $item_amt));
		$vended_items{$item_nbr}{QTY} += 1;
	}
	
	# adjust the amount
	$amt *= 100;
	
	# determine the kind of G4 record to build (CREDIT, MISC)
	if ('SR' =~ m/$card_type/)
	{
		# Special = S, Cash = R
		$ftype = 'MISC';
	}
	else
	{
		$ftype = 'CREDIT'
	}
	
	#my $cardreader = CardUtils->new();
	#my ($card_number, undef, undef, undef) = $cardreader->parse($card);
	push @logs, "--------- Export Details ---------";
	push @logs, "Machine ID      : $machine_id";
	push @logs, "Trans Number    : $machine_trans_no";
	push @logs, "Vend Col Bytes  : $vend_col_bytes";
	push @logs, "Vend Qty        : $vend_qty";
	push @logs, "Tran Date/Time  : $timestamp";
	push @logs, "Tran Amount     : $amt";
	push @logs, "Card            : ".secure_data_log($card);
	#push @logs, "Card            : $card";
	push @logs, "Card Type       : $card_type";
	push @logs, "G4 Category     : $ftype";
	push @logs, "Detail Data     : " . unpack("H*",$detail_data) . "\n";
	
	# get the location name
	$sth = $dbh->prepare("select location_name, device_serial_cd from pss.vw_location_by_device_id a where a.device_id = ?");
	$sth->execute($device_id);
	
	@data = $sth->fetchrow_array();
	# the legacy USALive limits the location name to 16 chars
	my $locname;
	if (defined $data[0]){
		$locname = substr($data[0],0,16);
	}
	else
	{
		$locname = "UNKNOWN";
	}
	
	# obtain the device serial number... if not found use the machine id and 
	# note it in the logs
	if (defined $data[1]){
		$device_serial_cd = $data[1];
	}
	else
	{
		$device_serial_cd = $machine_id;
		push @logs, "************ ALERT *****************";
		push @logs, "COULD NOT OBTAIN THE DEVICE_SERIAL_CD";
		push @logs, "FROM THE DATABASE USING THE DEVICE_ID.";
		push @logs, "WRITING THE DEVICE_ID TO THE G4 RECORD";
		push @logs, "INSTEAD.";
		push @logs, "*************************************";		
	}
	
	# if we haven't set the card type by now it's because we couldn't find
	# the batched record in the database or didn't recognize what was passed, 
	# so note it in the logs
	if (!defined $card_type)
	{
		push @logs, "************ ALERT *****************";
		push @logs, "THE TRANSACTION WAS EITHER A BATCHED";
		push @logs, "TRANSACTION NOT FOUND IN THE";
		push @logs, "DATABASE OR THE CARD TYPE COULD NOT BE";
		push @logs, "IDENTIFIED. THE RECORD WAS WRITTEN TO"; 
		push @logs, "THE ERROR DIRECTORY.";
		push @logs, "*************************************";		
		$ftype = 'FAILED';
	}

	# start building the G4 rec
	my $g4rec = "$device_serial_cd,$locname,";

	# format the trandate,trantime info
	($sec, $min, $hr, $dy, $mon, undef, undef, undef, undef) = gmtime($timestamp);
	$mon = ($mon+1);
	
	$g4rec .= sprintf("%02d/%02d,%02d:%02d,", $mon, $dy, $hr, $min);
	
	# the G4 Category affects how we build the middle of the record
	if ($ftype eq 'CREDIT')
	{
		# add the apcode, sale, data1, merch, card, data2
		$g4rec .= sprintf("APGReRix,%05d,0000,E.179001415993577953,$card,~,","$amt");
		
		# add the startdate,starttime,data3,data4
		$g4rec .= sprintf("%02d/%02d,%02d:%02d,0000,~,", $mon, $dy, $min, $sec);
	}
	else	# passcard, cash
	{
		# add the card, sale, data3, data4
		$g4rec .= sprintf("$card,%05d,0000,~,","$amt");
		
		# add the stopdate,stoptime,data5,data6
		$g4rec .= sprintf("%02d/%02d,%02d:%02d,0000,~,", $mon, $dy, $min, $sec);
	}
			
	# -- build the vend column string in G4 format --
	# first, create an array containing the 'denormalized' vend list of columns
	my ($key, @vends);
	foreach $key (%vended_items)
	{		
		if (defined($vended_items{$key}{QTY}))
		{
			for (my $i = 0; $i < $vended_items{$key}{QTY}; $i++)
			{
				# for each item vended create a placeholder in the array; $key is the 
				# column number we want to place in the final string
				push @vends, $key;
			}
		}
	}

	# finish building the record
	$g4rec .=  buildSrvCntString($vend_qty, @vends);
	
	# prepare to create a file to write the record
	my ($pathdat, $pathlog, $fname, $ext) = ("/opt/ReRixEngine2/ReRix/G4Export/data/$ftype/",
						"/opt/ReRixEngine2/ReRix/G4Export/logs/",
						$device_serial_cd . sprintf("_%08d", $timestamp), 
						".new");
	if (open (G4EXPORT, "> $pathdat$fname$ext"))
	{
		# write the record and immediately close the file
		printf G4EXPORT "%s", $g4rec;
		close G4EXPORT;
		
		# after closing the file change its extension to 'log' so the batching process 
		# will pick it up **after** the file is completely written and closed
		rename $pathdat . $fname . $ext, $pathdat . $fname . ".log";
		push @logs, "Saved G4 Export file as $fname.log";
		
		# log the result of the write
		my $logname = "g4export.log";
		if (open (G4LOG, ">> $pathlog$logname"))
		{
			printf G4LOG "[%s", localtime() . "] Successfully created $pathdat$fname.log (createG4ExportRec)\n";
		}
		else
		{
			push @logs, "Could not write entry to g4export.log";
		}	
	}
	else
	{
		push @logs, "An error occurred trying to create G4 Export file $pathdat$fname.$ext";
	}
	
	return (\@logs);
	
}

sub createG4ExportCashRec
{
	my @logs;
	my ($DATABASE, $machine_id, $device_id, $amt, $column, $timestamp) = @_;
	
	my $item_nbr;
	my $item_amt;
	my $item_tmp;
	my $card = 'Currency Vend';
	my $i;
	my $device_serial_cd;
	my $ftype;
	my $dbh = $DATABASE->{handle};
	my @data;
	my $sth;
	#my $isdst;
	#my $tzone = 'EST';	#default to EST
	my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy);

	# get the timezone for the terminal's location
	#$sth = $dbh->prepare("select LOCATION_TIMEZONE from pss.vw_location_by_device_id where device_id = ?");
	#$sth->execute($device_id);
	#@data = $sth->fetchrow_array();
	#$tzone = $data[0] if defined $data[0];
				
	# adjust the amount
	$amt *= 100;
		
	push @logs, "--------- Cash Export Details ---------";
	push @logs, "Machine ID      : $machine_id";
	push @logs, "Vend Col        : $column";
	push @logs, "Tran Date/Time  : $timestamp";
	push @logs, "Tran Amount     : $amt";
	push @logs, "G4 Category     : $card";
	
	# get the location name
	$sth = $dbh->prepare("select location_name, device_serial_cd from pss.vw_location_by_device_id a where a.device_id = ?");
	$sth->execute($device_id);
	
	@data = $sth->fetchrow_array();
	# the legacy USALive limits the location name to 16 chars
	my $locname;
	if (defined $data[0]){
		$locname = substr($data[0],0,16);
	}
	else
	{
		$locname = "UNKNOWN";
	}
	
	# obtain the device serial number... if not found use the machine id and 
	# note it in the logs
	if (defined $data[1]){
		$device_serial_cd = $data[1];
	}
	else
	{
		$device_serial_cd = $machine_id;
		push @logs, "************ ALERT *****************";
		push @logs, "COULD NOT OBTAIN THE DEVICE_SERIAL_CD";
		push @logs, "FROM THE DATABASE USING THE DEVICE_ID.";
		push @logs, "WRITING THE DEVICE_ID TO THE G4 RECORD";
		push @logs, "INSTEAD.";
		push @logs, "*************************************";		
	}
	
	# start building the G4 rec
	my $g4rec = "$device_serial_cd,$locname,";

	# format the trandate,trantime info
	($sec, $min, $hr, $dy, $mon, undef, undef, undef, undef) = gmtime($timestamp);
	$g4rec .= sprintf("%02d/%02d,%02d:%02d,", $mon + 1, $dy, $hr, $min);

	# add the card, sale, data3, data4
	$g4rec .= sprintf("$card,%05d,0000,~,","$amt");
	
	# add the stopdate,stoptime,data5,data6
	$g4rec .= sprintf("%02d/%02d,%02d:%02d,0000,~,", $mon + 1, $dy, $min, $sec);

	my @vends;
	#$i = 0;
	#foreach $key (%vended_items)
	#{		
	#	if ( defined($vended_items{$key}{QTY}) )
	#	{
	#		for ($i = 0; $i < $vended_items{$key}{QTY}; $i++)
	#		{
	#			# for each item vended create a placeholder in the array; $key is the 
	#			# column number we want to place in the final string
	#			push @vends, $key;
	#		}
	#	}
	#}
	push @vends, $column;

	# finish building the record
	$g4rec .=  buildSrvCntString(sprintf("%02d", 1), @vends);
			
	# prepare to create a file to write the record
	my ($pathdat, $pathlog, $fname, $ext) = ("/opt/ReRixEngine2/ReRix/G4Export/data/MISC/",
						"/opt/ReRixEngine2/ReRix/G4Export/logs/",
						$device_serial_cd . sprintf("_%08d", $timestamp), 
						".new");
	if (open (G4EXPORT, "> $pathdat$fname$ext"))
	{
		# write the record and immediately close the file
		printf G4EXPORT "%s", $g4rec;
		close G4EXPORT;
		
		# after closing the file change its extension to 'log' so the batching process 
		# will pick it up **after** the file is completely written and closed
		rename $pathdat . $fname . $ext, $pathdat . $fname . ".log";
		push @logs, "Saved G4 Export file as $fname.log";
		
		# log the result of the write
		my $logname = "g4export.log";
		if (open (G4LOG, ">> $pathlog$logname"))
		{
			printf G4LOG "[%s", localtime() . "] Successfully created $pathdat$fname.log (createG4ExportCashRec)\n";
		}
		else
		{
			push @logs, "Could not write entry to g4export.log";
		}	
	}
	else
	{
		push @logs, "An error occurred trying to create G4 Export file $pathdat$fname.$ext";
	}
	
	return (\@logs);
	
}

sub createG4ErrorRec
{
	my @logs;
	my ($DATABASE, $machine_id, $device_id, $device_serial_cd, $timestamp, $card) = @_;
	
	my $dbh = $DATABASE->{handle};

	push @logs, "--------- Error Export Details ---------";
	push @logs, "Machine ID             : $machine_id";
	push @logs, "Timestamp              : $timestamp";
	push @logs, "Error Message          : $card";
	
	# get the location name
	my $sth = $dbh->prepare("select location_name, device_serial_cd from pss.vw_location_by_device_id a where a.device_id = ?");
	$sth->execute($device_id);
	
	my @data = $sth->fetchrow_array();
	# the legacy USALive limits the location name to 16 chars
	my $locname;
	if (defined $data[0]){
		$locname = substr($data[0],0,16);
	}
	else
	{
		$locname = "UNKNOWN";
	}
	
	# process the different types of error messages and format them properly...
	# 
	if($card =~ m/^Err\.DEX/)
	{
		$card = "Err.DEX Fill";
	}
	elsif($card =~ m/^ErrorSystem/)
	{
		$card = "Error System";
	}
	else
	{
		$card = substr($card, 0, index($card, "-->"));
	}
	
	push @logs, "Formatted Message      : $card";
	
	# start building the G4 rec
	my $g4rec = "$device_serial_cd,$locname,";

	# format the trandate,trantime info
	my ($sec, $min, $hr, $dy, $mon, undef, undef, undef, undef) = gmtime($timestamp);
	$g4rec .= sprintf("%02d/%02d,%02d:%02d,", $mon + 1, $dy, $hr, $min);

	# add the card, sale, data3, data4
	$g4rec .= $card;
	
	# prepare to create a file to write the record
	my ($pathdat, $pathlog, $fname, $ext) = ("/opt/ReRixEngine2/ReRix/G4Export/data/ERROR/", "/opt/ReRixEngine2/ReRix/G4Export/logs/", $device_serial_cd . sprintf("_%08d", $timestamp), ".new");
	if (open (G4EXPORT, "> $pathdat$fname$ext"))
	{
		# write the record and immediately close the file
		printf G4EXPORT "%s", $g4rec;
		close G4EXPORT;
		
		# after closing the file change its extension to 'log' so the batching process 
		# will pick it up **after** the file is completely written and closed
		rename $pathdat . $fname . $ext, $pathdat . $fname . ".log";
		push @logs, "Saved G4 Error file as $fname.log";
		
		# log the result of the write
		my $logname = "g4export.log";
		if (open (G4LOG, ">> $pathlog$logname"))
		{
			printf G4LOG "[%s", localtime() . "] Successfully created $pathdat$fname.log (createG4ErrorRec)\n";
		}
		else
		{
			push @logs, "Could not write error entry to g4export.log";
		}	
	}
	else
	{
		push @logs, "An error occurred trying to create G4 Error file $pathdat$fname.$ext";
	}
	
	return (\@logs);
}

sub createG4ExportDEXRec
{
	my @logs;
	my ($DATABASE, $machine_id, $device_id, $SSN, $data) = @_;
	
	# g4 may send 3 0x04 characters because that marks the end of the file in memory.  if it's there delete it
	if($data =~ m/\x04{3}/)
	{
		push @logs, "DEX Cleanup       : Removing EOT (04h) characters that where left by the G4";
		$data =~ s/\x04{3}//g;
	}
	
	# the first line of the DEX file is a header to identify the DEX data...  earlier versions of the 
	# G4 didn't put DEX- in front of the E4 number, and it screwed of some legacy apps...  so we have
	# to insert it if it's not there
	
	if(substr($data, 0, 4) ne 'DEX-')
	{
		push @logs, "DEX Cleanup       : G4 ommited DEX- header, inserting in to compensate";
		$data = 'DEX-' . $data;
	}
	
	# prepare to create a file to write the record
	my ($pathdat, $pathlog, $fname, $ext) = ("/opt/ReRixEngine2/ReRix/G4Export/data/DEX_DATA/", "/opt/ReRixEngine2/ReRix/G4Export/logs/", ('DEX-' . $SSN . '-' . time()), ".new");
						
	if (open (G4EXPORT, "> $pathdat$fname$ext"))
	{
		# write the record and immediately close the file
		print G4EXPORT $data;
		close G4EXPORT;
		
		# after closing the file change its extension to 'log' so the batching process 
		# will pick it up **after** the file is completely written and closed
		rename $pathdat . $fname . $ext, $pathdat . $fname . ".log";
		push @logs, "Saved G4 DEX file as $fname.log";
		
		# log the result of the write
		my $logname = "g4export.log";
		if (open (G4LOG, ">> $pathlog$logname"))
		{
			printf G4LOG "[%s", localtime() . "] Successfully created $pathdat$fname.log (createG4ExportDEXRec)\n";
			close(G4LOG);
		}
		else
		{
			push @logs, "Could not write entry to g4export.log";
		}	
	}
	else
	{
		push @logs, "An error occurred trying to create G4 Export file $pathdat$fname.$ext";
	}
	
	return (\@logs);
}


sub buildSrvCntString
{
	my ($vend_qty, @vends) = @_;
	# the first 4 chars are = 2 chars for total vend count + 2 chars for the first column vended
	my $vendstr = "~," . sprintf("%02d", $vend_qty) . sprintf("%02d",$vends[0]) . ",~,";
	my ($i, $j);
	for ($i=1; $i<10; )
	{
		for ($j=0; $j<2; $j++)
		{
			if (defined $vends[$i])
			{
				$vendstr .= sprintf("%02d",$vends[$i]);
			}
			else
			{
				$vendstr .= "00";
			}
			$i++;
			
		}
		if ($i < 10)
		{
			$vendstr .= ",~,";
		}
	}
	return $vendstr;	
	
}

sub secure_data_log ($)
{
	my $data = shift;
	my ($min_num_length, $max_seq_shown) = (8, 4);	#min num of chars that requires security; max num of chars in any sequence shown
	my $data_out = '';
	if (length $data > 0)
	{
		$data_out = substr($data, 0, length $data > $min_num_length?4:length $data).(length $data > $min_num_length?(length $data > (4 + 1)?'...':'').substr($data, (length $data) - int((length $data) / 4)):'');
	}
	return $data_out.' ('.(length $data).' char)';
}
1;
