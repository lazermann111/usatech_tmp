#!/usr/bin/perl -w

use strict;

package ReRix::ProductIDs;

use ReRix::Shared;
use ReRix::Send;

# SCU Product ID Messages

sub parse_req
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	# Remove the command letter, then generate each message requested

	my $request_list = substr $command_hashref->{inbound_command}, 2;

	my $request;

	while( $request = substr( $request_list, 0, 2 ) )
	{
		$request = ord(pack 'H*', $request)+1;

		push @logs, "Request: product $request";

		$DATABASE->insert(
					table			=> 'scu_command',
					insert_columns	=> 'machine_id, command',
					insert_values	=> [ $command_hashref->{machine_id},
										"product $request" ]
					);

		$request_list = substr $request_list, 2;
	}

	return(\@logs);
}

sub parse_ack
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $message = MakeString($command_hashref->{inbound_command});

	my $ack_command = unpack('v', substr($message, 1)) + 1;

	push @logs, "ACK Product: $ack_command";

	$DATABASE->update(
				table			=> 'scu_command',
				update_columns	=> 'command_status',
				update_values	=> ['Y'],
				where_columns	=> ["command_status = 'S'",
									'command = ?',
									'machine_id = ?'],
				where_values	=> [ "product $ack_command",
										$command_hashref->{machine_id} ],
				);

	return( \@logs );
}


1;
