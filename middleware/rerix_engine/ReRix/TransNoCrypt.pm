#!/usr/bin/perl -w

use strict;

package ReRix::TransNoCrypt;

use ReRix::Shared;
use ReRix::Send;

use ReRix::AuthClient;
use ReRix::CardUtils;

# Authorization Request: 6

my @button_map_vt96663_mdb_to_dex = (
    65535,  	# mdb id 0 == 0x0000 has no dex id
    65535,  	# mdb id 1 == 0x0001 has no dex id
    1,  	# mdb id 2 == 0x0002
    2,  	# mdb id 3 == 0x0003
    3,  	# mdb id 4 == 0x0004
    4,  	# mdb id 5 == 0x0005
    5,  	# mdb id 6 == 0x0006
    6,  	# mdb id 7 == 0x0007
    7,  	# mdb id 8 == 0x0008
    8,  	# mdb id 9 == 0x0009
    9,  	# mdb id 10 == 0x000a
    65535,  	# mdb id 11 == 0x000b has no dex id
    65535,  	# mdb id 12 == 0x000c has no dex id
    65535,  	# mdb id 13 == 0x000d has no dex id
    10,  	# mdb id 14 == 0x000e
    11,  	# mdb id 15 == 0x000f
    12,  	# mdb id 16 == 0x0010
    13,  	# mdb id 17 == 0x0011
    14,  	# mdb id 18 == 0x0012
    15,  	# mdb id 19 == 0x0013
    65535,  	# mdb id 20 == 0x0014 has no dex id
    65535,  	# mdb id 21 == 0x0015 has no dex id
    65535,  	# mdb id 22 == 0x0016 has no dex id
    65535,  	# mdb id 23 == 0x0017 has no dex id
    65535,  	# mdb id 24 == 0x0018 has no dex id
    65535,  	# mdb id 25 == 0x0019 has no dex id
    16,  	# mdb id 26 == 0x001a
    17,  	# mdb id 27 == 0x001b
    18,  	# mdb id 28 == 0x001c
    19,  	# mdb id 29 == 0x001d
    20,  	# mdb id 30 == 0x001e
    21,  	# mdb id 31 == 0x001f
    65535,  	# mdb id 32 == 0x0020 has no dex id
    65535,  	# mdb id 33 == 0x0021 has no dex id
    65535,  	# mdb id 34 == 0x0022 has no dex id
    65535,  	# mdb id 35 == 0x0023 has no dex id
    65535,  	# mdb id 36 == 0x0024 has no dex id
    65535,  	# mdb id 37 == 0x0025 has no dex id
    22,  	# mdb id 38 == 0x0026
    23,  	# mdb id 39 == 0x0027
    24,  	# mdb id 40 == 0x0028
    25,  	# mdb id 41 == 0x0029
    26,  	# mdb id 42 == 0x002a
    27,  	# mdb id 43 == 0x002b
    65535,  	# mdb id 44 == 0x002c has no dex id
    65535,  	# mdb id 45 == 0x002d has no dex id
    65535,  	# mdb id 46 == 0x002e has no dex id
    65535,  	# mdb id 47 == 0x002f has no dex id
    65535,  	# mdb id 48 == 0x0030 has no dex id
    65535,  	# mdb id 49 == 0x0031 has no dex id
    28,  	# mdb id 50 == 0x0032
    29,  	# mdb id 51 == 0x0033
    30,  	# mdb id 52 == 0x0034
    65535,  	# mdb id 53 == 0x0035 has no dex id
    65535,  	# mdb id 54 == 0x0036 has no dex id
    65535,  	# mdb id 55 == 0x0037 has no dex id
    65535,  	# mdb id 56 == 0x0038 has no dex id
    65535,  	# mdb id 57 == 0x0039 has no dex id
    65535,  	# mdb id 58 == 0x003a has no dex id
    65535,  	# mdb id 59 == 0x003b has no dex id
    65535,  	# mdb id 60 == 0x003c has no dex id
    65535,  	# mdb id 61 == 0x003d has no dex id
    65535,  	# mdb id 62 == 0x003e has no dex id
    65535,  	# mdb id 63 == 0x003f has no dex id
    65535,  	# mdb id 64 == 0x0040 has no dex id
    65535,  	# mdb id 65 == 0x0041 has no dex id
    65535,  	# mdb id 66 == 0x0042 has no dex id
    65535,  	# mdb id 67 == 0x0043 has no dex id
    65535,  	# mdb id 68 == 0x0044 has no dex id
    65535,  	# mdb id 69 == 0x0045 has no dex id
    65535,  	# mdb id 70 == 0x0046 has no dex id
    65535,  	# mdb id 71 == 0x0047 has no dex id
    65535,  	# mdb id 72 == 0x0048 has no dex id
    65535,  	# mdb id 73 == 0x0049 has no dex id
    65535,  	# mdb id 74 == 0x004a has no dex id
    65535,  	# mdb id 75 == 0x004b has no dex id
    65535,  	# mdb id 76 == 0x004c has no dex id
    65535,  	# mdb id 77 == 0x004d has no dex id
    65535,  	# mdb id 78 == 0x004e has no dex id
    65535,  	# mdb id 79 == 0x004f has no dex id
    65535,  	# mdb id 80 == 0x0050 has no dex id
    65535,  	# mdb id 81 == 0x0051 has no dex id
    65535,  	# mdb id 82 == 0x0052 has no dex id
    65535,  	# mdb id 83 == 0x0053 has no dex id
    65535,  	# mdb id 84 == 0x0054 has no dex id
    65535,  	# mdb id 85 == 0x0055 has no dex id
    65535,  	# mdb id 86 == 0x0056 has no dex id
    65535,  	# mdb id 87 == 0x0057 has no dex id
    65535,  	# mdb id 88 == 0x0058 has no dex id
    65535,  	# mdb id 89 == 0x0059 has no dex id
    65535,  	# mdb id 90 == 0x005a has no dex id
    65535,  	# mdb id 91 == 0x005b has no dex id
    65535,  	# mdb id 92 == 0x005c has no dex id
    65535,  	# mdb id 93 == 0x005d has no dex id
    65535,  	# mdb id 94 == 0x005e has no dex id
    65535,  	# mdb id 95 == 0x005f has no dex id
    65535,  	# mdb id 96 == 0x0060 has no dex id
    65535,  	# mdb id 97 == 0x0061 has no dex id
    65535,  	# mdb id 98 == 0x0062 has no dex id
    65535,  	# mdb id 99 == 0x0063 has no dex id
    65535,  	# mdb id 100 == 0x0064 has no dex id
    65535,  	# mdb id 101 == 0x0065 has no dex id
    65535,  	# mdb id 102 == 0x0066 has no dex id
    65535,  	# mdb id 103 == 0x0067 has no dex id
    65535,  	# mdb id 104 == 0x0068 has no dex id
    65535,  	# mdb id 105 == 0x0069 has no dex id
    65535,  	# mdb id 106 == 0x006a has no dex id
    65535,  	# mdb id 107 == 0x006b has no dex id
    65535,  	# mdb id 108 == 0x006c has no dex id
    65535,  	# mdb id 109 == 0x006d has no dex id
    65535,  	# mdb id 110 == 0x006e has no dex id
    65535,  	# mdb id 111 == 0x006f has no dex id
    65535,  	# mdb id 112 == 0x0070 has no dex id
    65535,  	# mdb id 113 == 0x0071 has no dex id
    65535,  	# mdb id 114 == 0x0072 has no dex id
    65535,  	# mdb id 115 == 0x0073 has no dex id
    65535,  	# mdb id 116 == 0x0074 has no dex id
    65535,  	# mdb id 117 == 0x0075 has no dex id
    65535,  	# mdb id 118 == 0x0076 has no dex id
    65535,  	# mdb id 119 == 0x0077 has no dex id
    65535,  	# mdb id 120 == 0x0078 has no dex id
    );  # end of @button_map_vt96663_mdb_to_dex


my @button_map_vt96663_dex_to_mdb = (
    65535,  	# dex id 0 has no mdb id
    2,  	# dex id 1 
    3,  	# dex id 2 
    4,  	# dex id 3 
    5,  	# dex id 4 
    6,  	# dex id 5 
    7,  	# dex id 6 
    8,  	# dex id 7 
    9,  	# dex id 8 
    10,  	# dex id 9 
    14,  	# dex id 10 
    15,  	# dex id 11 
    16,  	# dex id 12 
    17,  	# dex id 13 
    18,  	# dex id 14 
    19,  	# dex id 15 
    26,  	# dex id 16 
    27,  	# dex id 17 
    28,  	# dex id 18 
    29,  	# dex id 19 
    30,  	# dex id 20 
    31,  	# dex id 21 
    38,  	# dex id 22 
    39,  	# dex id 23 
    40,  	# dex id 24 
    41,  	# dex id 25 
    42,  	# dex id 26 
    43,  	# dex id 27 
    50,  	# dex id 28 
    51,  	# dex id 29 
    52,  	# dex id 30 
    );  # end of @button_map_vt96663_dex_to_mdb


my @button_map_vt96663_mdb_to_name = (
    '', 	# mdb id 0 == 0x0000 has no name
    '', 	# mdb id 1 == 0x0001 has no name
    'A1', 	# mdb id 2 == 0x0002
    'A2', 	# mdb id 3 == 0x0003
    'A3', 	# mdb id 4 == 0x0004
    'A4', 	# mdb id 5 == 0x0005
    'A5', 	# mdb id 6 == 0x0006
    'A6', 	# mdb id 7 == 0x0007
    'A7', 	# mdb id 8 == 0x0008
    'A8', 	# mdb id 9 == 0x0009
    'A9', 	# mdb id 10 == 0x000a
    'A10', 	# mdb id 11 == 0x000b
    'A11', 	# mdb id 12 == 0x000c
    'A12', 	# mdb id 13 == 0x000d
    'B1', 	# mdb id 14 == 0x000e
    'B2', 	# mdb id 15 == 0x000f
    'B3', 	# mdb id 16 == 0x0010
    'B4', 	# mdb id 17 == 0x0011
    'B5', 	# mdb id 18 == 0x0012
    'B6', 	# mdb id 19 == 0x0013
    'B7', 	# mdb id 20 == 0x0014
    'B8', 	# mdb id 21 == 0x0015
    'B9', 	# mdb id 22 == 0x0016
    'B10', 	# mdb id 23 == 0x0017
    'B11', 	# mdb id 24 == 0x0018
    'B12', 	# mdb id 25 == 0x0019
    'C1', 	# mdb id 26 == 0x001a
    'C2', 	# mdb id 27 == 0x001b
    'C3', 	# mdb id 28 == 0x001c
    'C4', 	# mdb id 29 == 0x001d
    'C5', 	# mdb id 30 == 0x001e
    'C6', 	# mdb id 31 == 0x001f
    'C7', 	# mdb id 32 == 0x0020
    'C8', 	# mdb id 33 == 0x0021
    'C9', 	# mdb id 34 == 0x0022
    'C10', 	# mdb id 35 == 0x0023
    'C11', 	# mdb id 36 == 0x0024
    'C12', 	# mdb id 37 == 0x0025
    'D1', 	# mdb id 38 == 0x0026
    'D2', 	# mdb id 39 == 0x0027
    'D3', 	# mdb id 40 == 0x0028
    'D4', 	# mdb id 41 == 0x0029
    'D5', 	# mdb id 42 == 0x002a
    'D6', 	# mdb id 43 == 0x002b
    'D7', 	# mdb id 44 == 0x002c
    'D8', 	# mdb id 45 == 0x002d
    'D9', 	# mdb id 46 == 0x002e
    'D10', 	# mdb id 47 == 0x002f
    'D11', 	# mdb id 48 == 0x0030
    'D12', 	# mdb id 49 == 0x0031
    'E1', 	# mdb id 50 == 0x0032
    'E2', 	# mdb id 51 == 0x0033
    'E3', 	# mdb id 52 == 0x0034
    'E4', 	# mdb id 53 == 0x0035
    'E5', 	# mdb id 54 == 0x0036
    'E6', 	# mdb id 55 == 0x0037
    'E7', 	# mdb id 56 == 0x0038
    'E8', 	# mdb id 57 == 0x0039
    'E9', 	# mdb id 58 == 0x003a
    'E10', 	# mdb id 59 == 0x003b
    'E11', 	# mdb id 60 == 0x003c
    'E12', 	# mdb id 61 == 0x003d
    'F1', 	# mdb id 62 == 0x003e
    'F2', 	# mdb id 63 == 0x003f
    'F3', 	# mdb id 64 == 0x0040
    'F4', 	# mdb id 65 == 0x0041
    'F5', 	# mdb id 66 == 0x0042
    'F6', 	# mdb id 67 == 0x0043
    'F7', 	# mdb id 68 == 0x0044
    'F8', 	# mdb id 69 == 0x0045
    'F9', 	# mdb id 70 == 0x0046
    'F10', 	# mdb id 71 == 0x0047
    'F11', 	# mdb id 72 == 0x0048
    'F12', 	# mdb id 73 == 0x0049
    'G1', 	# mdb id 74 == 0x004a
    'G2', 	# mdb id 75 == 0x004b
    'G3', 	# mdb id 76 == 0x004c
    'G4', 	# mdb id 77 == 0x004d
    'G5', 	# mdb id 78 == 0x004e
    'G6', 	# mdb id 79 == 0x004f
    'G7', 	# mdb id 80 == 0x0050
    'G8', 	# mdb id 81 == 0x0051
    'G9', 	# mdb id 82 == 0x0052
    'G10', 	# mdb id 83 == 0x0053
    'G11', 	# mdb id 84 == 0x0054
    'G12', 	# mdb id 85 == 0x0055
    'H1', 	# mdb id 86 == 0x0056
    'H2', 	# mdb id 87 == 0x0057
    'H3', 	# mdb id 88 == 0x0058
    'H4', 	# mdb id 89 == 0x0059
    'H5', 	# mdb id 90 == 0x005a
    'H6', 	# mdb id 91 == 0x005b
    'H7', 	# mdb id 92 == 0x005c
    'H8', 	# mdb id 93 == 0x005d
    'H9', 	# mdb id 94 == 0x005e
    'H10', 	# mdb id 95 == 0x005f
    'H11', 	# mdb id 96 == 0x0060
    'H12', 	# mdb id 97 == 0x0061
    'I1', 	# mdb id 98 == 0x0062
    'I2', 	# mdb id 99 == 0x0063
    'I3', 	# mdb id 100 == 0x0064
    'I4', 	# mdb id 101 == 0x0065
    'I5', 	# mdb id 102 == 0x0066
    'I6', 	# mdb id 103 == 0x0067
    'I7', 	# mdb id 104 == 0x0068
    'I8', 	# mdb id 105 == 0x0069
    'I9', 	# mdb id 106 == 0x006a
    'I10', 	# mdb id 107 == 0x006b
    'I11', 	# mdb id 108 == 0x006c
    'I12', 	# mdb id 109 == 0x006d
    'J1', 	# mdb id 110 == 0x006e
    'J2', 	# mdb id 111 == 0x006f
    'J3', 	# mdb id 112 == 0x0070
    'J4', 	# mdb id 113 == 0x0071
    'J5', 	# mdb id 114 == 0x0072
    'J6', 	# mdb id 115 == 0x0073
    'J7', 	# mdb id 116 == 0x0074
    'J8', 	# mdb id 117 == 0x0075
    'J9', 	# mdb id 118 == 0x0076
    'J10', 	# mdb id 119 == 0x0077
    'J11', 	# mdb id 120 == 0x0078
    );  # end of @button_map_vt96663_mdb_to_name

sub parse_auth_wamt
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $array_ref;

	$array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'modem_id, network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $command_hashref->{machine_id} ]
				);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_it and machine_index in rerix_modem_to_serial'] );
	}

	my $modem_id = $array_ref->[0][0];
	my $network = $array_ref->[0][1];

	my $message = MakeString( $command_hashref->{inbound_command} );

	my (undef, $trans_id, $card_type, $auth_amt_cents) = unpack "cVav", $message;

	my $card = substr($message, 8);
	my $result = 1;

	if( substr($card, -2, 1) eq '?' )
	{
		$card = substr( $card, 0, -2 );
	}
	elsif( substr($card, -1, 1) eq '?' )
	{
		$card = substr( $card, 0, -1 );
	}

	my $cardreader = CardUtils->new();
	my ($card_number, undef, undef, undef) = $cardreader->parse($card);
	my $machine_trans_no = "$network:$modem_id:$trans_id";
	push @logs, "Machine Trans ID  : $machine_trans_no";
	push @logs, "Card Type         : $card_type";
	push @logs, "Card              : ".secure_data_log($card_number);

	my $auth_amt = sprintf("%.02f", ($auth_amt_cents * .01));

	push @logs, "Auth Amt          : $auth_amt";

	if( $result != 1 )
	{
		return [@logs, "Decryption Failure"];
	}

	my $trans;

	$trans = ReRix::AuthClient->connect();

	$trans->send("t7,$command_hashref->{machine_id},$card," .
				",$auth_amt,,,$card_type,$machine_trans_no");

	my $auth_code = $trans->get_code();

	push @logs, "Auth Code         : $auth_code";

	$trans->disconnect();

	my $response = '6';	# Auth response command is an ascii '6'

#	$response .= PackEncrypted( pack("VC", $trans_id, ($auth_code eq 'A'?1:0) ),
#	$response .= PackEncrypted( pack("VC", $trans_id, 1 ),

	$response = pack 'ACc', '7', ($trans_id & 0x000000FF), 1;

	$DATABASE->insert(
					table=> 'Machine_Command',
					insert_columns=> 'Modem_ID, Command',
					insert_values=> [$command_hashref->{machine_id},
					&MakeHex($response)]
				);

	return(\@logs);
}

sub parse_net_batch
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $array_ref;

	$array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'modem_id, network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $command_hashref->{machine_id} ]
				);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_it and machine_index in rerix_modem_to_serial'] );
	}

	my $modem_id = $array_ref->[0][0];
	my $network = $array_ref->[0][1];

	my $message = MakeString( $command_hashref->{inbound_command} );

	my (undef, $trans_id, $column, $button, $amt, $tax, $trans_result) =
												unpack "cVvvvvA", $message;

	if( ($command_hashref->{machine_id} eq 'EV13820') or
		($command_hashref->{machine_id} eq 'EV13284') )
	{
		$button = $column = $button_map_vt96663_mdb_to_dex[$column];
	}
	else
	{
		$column++;		# make column_id base 1
	}

	$amt *= .01;
	$tax *= .01;

	my $machine_trans_no = "$network:$modem_id:$trans_id";
	push @logs, "Machine Trans ID : $machine_trans_no";
	push @logs, "Column           : $column";
	push @logs, "Button           : $button";
	push @logs, "Amount           : $amt";
	push @logs, "Tax              : $tax";
	push @logs, "Result           : $trans_result";


	$array_ref = $DATABASE->select(
					table			=> 'rerix_inventory a, ' .
										'table(a.inventory)',
					select_columns	=> 'inventory_item_number',
					where_columns	=> [ 'machine_id = ?', 'button_id = ?' ],
					where_values	=> [ $command_hashref->{machine_id},
											$column ]
				);

	my $inv_num;
	if( $array_ref->[0][0] )
	{
		$inv_num = $array_ref->[0][0];
	}
	else
	{
		$inv_num = 'NO INV';
	}

	push @logs, "Inventory        : $inv_num";

	my $trans;
#	if( $command_hashref->{machine_id} eq 'EV13288' )
#	{
#		$trans = ReRix::AuthClient->connect(
#											processor => 'testtransd',
#											host => '10.74.163.213',
#											port => '10104');
#	}
#	else
#	{
#		$trans = ReRix::AuthClient->connect();
#	}

	$trans = ReRix::AuthClient->connect();

	$trans->send("t8,$command_hashref->{machine_id},$machine_trans_no,".
				"$column,$inv_num,$amt,$tax,,,$button,$trans_result");

	if( defined (my $auth_code = $trans->get_code() ) )
	{
		# Batch response command is an ascii '7' and the least significant byte
		# of the transaction ID
		my $response = pack 'AC', '7', ($trans_id & 0x000000FF);

		$DATABASE->insert(
						table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$command_hashref->{machine_id},
						&MakeHex($response)]
					);

		push @logs, "Auth Code         : $auth_code";

		my $trans_no = $trans->authorization_number();

		push @logs, "Trans No          : $trans_no";

		# or ($command_hashref->{machine_id} ne 'EV13288') )

		if( $trans->get_inv() )
		{
			my $upd_inv = Update_Inventory( $DATABASE,
											$command_hashref->{machine_id},
											$column,
											$trans_no );

			push @logs, "APPLY_INVENTORY   : $upd_inv";
		}
		else
		{
			push @logs, "APPLY_INVENTORY   : skipped";
		}
	}
	else
	{
		push @logs, "Transaction Server unreachable";
	}

	return(\@logs);
}

sub parse_local_batch
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $array_ref;

	$array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'modem_id, network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $command_hashref->{machine_id} ]
				);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_it and machine_index in rerix_modem_to_serial'] );
	}

	my $modem_id = $array_ref->[0][0];
	my $network = $array_ref->[0][1];

	my $message = MakeString( $command_hashref->{inbound_command} );

	my (undef, $trans_id, $timestamp, $card_type, $column, $button, $amt, $tax,
			$trans_result, $encrypted_card) =
												unpack "cVVavvvvAa*", $message;

	if( ($command_hashref->{machine_id} eq 'EV13820') or
		($command_hashref->{machine_id} eq 'EV13284') )
	{
		$button = $column = $button_map_vt96663_mdb_to_dex[$column];
	}
	else
	{
		$column++;		# make column_id base 1
	}

	$amt *= .01;
	$tax *= .01;

	my $card = $encrypted_card;
	my $result = 1;

	if( substr($card, -2, 1) eq '?' )
	{
		$card = substr( $card, 0, -2 );
	}
	elsif( substr($card, -1, 1) eq '?' )
	{
		$card = substr( $card, 0, -1 );
	}

	my $cardreader = CardUtils->new();
	my ($card_number, undef, undef, undef) = $cardreader->parse($card);
	my $machine_trans_no = "$network:$modem_id:$trans_id";
	push @logs, "Machine Trans ID  : $machine_trans_no";
	push @logs, "Timestamp         : $timestamp";
	push @logs, "Card Type         : $card_type";
	push @logs, "Column            : $column";
	push @logs, "Button            : $button";
	push @logs, "Amount            : $amt";
	push @logs, "Tax               : $tax";
	push @logs, "Result            : $trans_result";
	push @logs, "Card              : ".secure_data_log($card_number);

	if( $result != 1 )
	{
		return [@logs, "Decryption Failure: $result"];
	}

	$array_ref = $DATABASE->select(
					table			=> 'rerix_inventory a, ' .
										'table(a.inventory)',
					select_columns	=> 'inventory_item_number',
					where_columns	=> [ 'machine_id = ?', 'button_id = ?' ],
					where_values	=> [ $command_hashref->{machine_id},
											$column ]
				);

	my $inv_num;
	if( $array_ref->[0][0] )
	{
		$inv_num = $array_ref->[0][0];
	}
	else
	{
		$inv_num = 'NO INV';
	}

	push @logs, "Inventory         : $inv_num";

	if( ($timestamp == 0) or ($timestamp < (31*24*60*60)) )
	{
		# if we get a bad timestamp, use the current time
		$timestamp = time() - 60*60*5;
	}

	my ($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);

	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);

	my $time = sprintf("%02d:%02d:%02d", $hour, $min, $sec);

	my $trans;
#	if( $command_hashref->{machine_id} eq 'EV13288' )
#	{
#		$trans = ReRix::AuthClient->connect(
#											processor => 'testtransd',
#											host => '10.74.163.213',
#											port => '10104');
#	}
#	else
#	{
#		$trans = ReRix::AuthClient->connect();
#	}

	$trans = ReRix::AuthClient->connect();

	$trans->send("t9,$command_hashref->{machine_id},$time,$date,$card," .
				"$card_type,$column,$inv_num,$amt,$tax,$button," .
				"$trans_result,$machine_trans_no");

	if( defined (my $auth_code = $trans->get_code() ) )
	{
		# Batch response command is an ascii '7' and the least significant byte
		# of the transaction ID
		my $response = pack 'AC', '7', ($trans_id & 0x000000FF);

		$DATABASE->insert(
						table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$command_hashref->{machine_id},
						&MakeHex($response)]
					);

		push @logs, "Auth Code         : $auth_code";

		my $trans_no = $trans->authorization_number();

		push @logs, "Trans No          : $trans_no";

		# or ($command_hashref->{machine_id} ne 'EV13288') )
		if( $trans->get_inv() )
		{
			my $upd_inv = Update_Inventory( $DATABASE,
											$command_hashref->{machine_id},
											$column,
											$trans_no );

			push @logs, "APPLY_INVENTORY   : $upd_inv";
		}
		else
		{
			push @logs, "APPLY_INVENTORY   : skipped";
		}
	}
	else
	{
		push @logs, "Transaction Server unreachable";
	}

	return(\@logs);
}

sub Update_Inventory()
{
	my ($DATABASE, $mach_id, $col_num, $trans_no) = @_;

	my $raw_handle = $DATABASE->{handle};
	my $out_msg = 0;

	#returns in the Msg field a "BAD" for failure and a "GOOD" for success...
	my $csr = $raw_handle->prepare(q{BEGIN
				APPLY_INVENTORY(:MACHINE_ID,:COLUMN_ID,:OUT_MSG,:TRANS_NO);
					END;
				});

	$csr->bind_param(":MACHINE_ID", $mach_id);
	$csr->bind_param(":COLUMN_ID",  $col_num);
	$csr->bind_param_inout(":OUT_MSG", \$out_msg, 1);
	$csr->bind_param(":TRANS_NO", $trans_no, 1);
	$csr->execute;

	return $out_msg;
}

sub secure_data_log ($)
{
	my $data = shift;
	my ($min_num_length, $max_seq_shown) = (8, 4);	#min num of chars that requires security; max num of chars in any sequence shown
	my $data_out = '';
	if (length $data > 0)
	{
		$data_out = substr($data, 0, length $data > $min_num_length?4:length $data).(length $data > $min_num_length?(length $data > (4 + 1)?'...':'').substr($data, (length $data) - int((length $data) / 4)):'');
	}
	return $data_out.' ('.(length $data).' char)';
}


1;
