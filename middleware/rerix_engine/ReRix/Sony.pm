#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		unknown		tshannon	Initial release
#
# 1.01		04/05/2004	pcowan		Cleaned up indents and alignment
#									Removed commit from direct DBI calls and added finish

use strict;

package ReRix::Sony;
use lib '/opt/ReRixEngine2';
use Evend::ReRix::Shared;
use ReRix::Send;

# Sony Info packet
sub parse_info
{
	my ( $DATABASE, $command_hashref ) = @_;
	my @logs;
	my $raw_handle = $DATABASE->{handle};
	$raw_handle->{RaiseError} = 1;

	push @logs, "Inbound Sony Command: $command_hashref->{'inbound_command'}";

	my $info = $command_hashref->{'inbound_command'};
	eval 
	{
		if ( $info =~ s/^5f530101// )
		{
			# Sales Summary

			printf("Sony Sales Summary received\n");
			push @logs, "Sony Sales Summary received";

			# MDH: Added 0's to the mac_id, local auths, and local auth declines until
			# sony finished the spec.
		
			my $csr = $raw_handle->prepare(	q{BEGIN	INSERT_SONY_SALES(	?, ?, 0,
											to_date(?, 'YYYY/MM/DD'),
											?, ?, ?, ?,
											?, ?, ?, ?, ?, ?,
											?, ?, ?, ?, ?, ?, ?,
											?, ?, ?, ?, 0); END; } );

			my $i = 1;
			foreach ( $command_hashref->{'machine_id'}, split( ',', MakeString($info) ) )
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^5f5504ef// )
		{
			# Prints Remaining
			printf("Sony Prints Remaining Received\n");
			push @logs, "Sony Prints Remaining Received";

			# my $prints_remaining = ord(MakeString(substr($info, 0, 4)));
			# my $printer_num = ord(MakeString(substr($info, 4, 2)));
			my $prints_remaining = hex( substr( $info, 0, 4 ) );
			my $printer_num	  = hex( substr( $info, 4, 2 ) );

			my $csr = $raw_handle->prepare(
						q{BEGIN UPDATE_PRINTER_REMAINING( ?, ?, ?); END; });
			my $i = 1;
			foreach (($command_hashref->{'machine_id'}, $prints_remaining, $printer_num))
			{
				push @logs, "   Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^5f5504f3// )
		{
			# Total Prints
			printf("Sony Total Prints Received\n");
			push @logs, "Sony Total Prints Received";

			#my $total_prints = ord(MakeString(substr($info, 0, 6)));
			#my $printer_num = ord(MakeString(substr($info, 6, 2)));
			my $total_prints = hex( substr( $info, 0, 6 ) );
			my $printer_num  = hex( substr( $info, 6, 2 ) );

			my $csr = $raw_handle->prepare(
					 	q{BEGIN UPDATE_PRINTER_ITEMS_PRINTED(?, ?, ?); END; });
			my $i = 1;
			foreach (($command_hashref->{'machine_id'}, $total_prints, $printer_num))
			{
				push @logs, "   Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}

			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^5f5521..// )
		{
			# Sales Summary
			push @logs, " Sony Error Message received";

			my $alert_context = ord( MakeString( substr( $info, 0, 2 ) ) );
			my $alert_msg	 = ord( MakeString( substr( $info, 2, 2 ) ) );
			my $printer_id	= ord( MakeString( substr( $info, 4, 2 ) ) );

			my $csr = $raw_handle->prepare(
				q{	BEGIN INSERT_ALERT( ?, ?, ?, to_date(?, 'MM/DD/YYYY HH24:MI:SS'), ?, ?); END; } );

			my $i = 1;
			foreach ( (	$command_hashref->{'machine_id'},
					  	$printer_id, '2', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context))
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^5f5503..// )
		{
			# Sales Summary
			push @logs, "   Sony  Error Message Received";

			my $alert_context 	= ord( MakeString( substr( $info, 0, 2 ) ) );
			my $alert_msg	 	= ord( MakeString( substr( $info, 2, 2 ) ) );
			my $printer_id		= ord( MakeString( substr( $info, 4, 2 ) ) );

			my $csr = $raw_handle->prepare(	q{BEGIN INSERT_ALERT(	?, ?, ?,
											to_date(?, 'MM/DD/YYYY HH24:MI:SS'),
											?, ?); END; } );

			my $i = 1;
			foreach ( ( $command_hashref->{'machine_id'},
						$printer_id, '3', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context ) )
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}

		my $sony_info = substr( $command_hashref->{'inbound_command'}, 2 );
		push @logs, "  Insert Sony   Info: $info";

		$DATABASE->insert(	table		  	=> 'SONY_INFO',
							insert_columns 	=> 'MACHINE_ID, INFO',
							insert_values 	=> [ $command_hashref->{'machine_id'}, $sony_info ] );

		return 1;
	}
	or push @logs, "Error Occurred $!: $@" . ( $raw_handle and $raw_handle->err
					and "\n\t[" . $raw_handle->err . "] (" . $raw_handle->state . "): " . $raw_handle->errstr );

	return (\@logs);
}

sub parse_info_V2
{
	my ( $DATABASE, $command_hashref ) = @_;
	my @logs;
	my $raw_handle = $DATABASE->{handle};
	$raw_handle->{RaiseError} = 1;
	push @logs, "Inbound Sony Command: $command_hashref->{'inbound_command'}";

	my $info = $command_hashref->{'inbound_command'};

	# we need to get the msgnbr for this command to ACK the sender
	my $array_msg_no = $DATABASE->select(
							  table		  => 'Machine_Command_Inbound',
							  select_columns => 'inbound_msg_no',
							  where_columns  => ['inbound_id = ?'],
							  where_values => [ $command_hashref->{inbound_id} ] );
							  
	my $msg_nbr = $array_msg_no->[0][0];

	eval 
	{
		if ( $info =~ s/^b9530101// )
		{

			# Sales Summary
			printf("Sony Sales Summary received\n");
			push @logs, "Sony Sales Summary received";

			#MDH: Added 0's to the mac_id, local auths, and local auth declines until
			#sony finished the spec.

			my $csr = $raw_handle->prepare(	q{BEGIN INSERT_SONY_SALES(	?, ?, 0,
											to_date(?, 'YYYY/MM/DD'),
											?, ?, ?, ?,
											?, ?, ?, ?, ?, ?,
											?, ?, ?, ?, ?, ?, ?,
											?, ?, ?, ?, 0); END; } );

			my $i = 1;
			foreach ( $command_hashref->{'machine_id'}, split( ',', MakeString($info) ) )
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^b95504ef// )
		{
			# Prints Remaining
			printf("Sony Prints Remaining Received\n");
			push @logs, "Sony Prints Remaining Received";

			#my $prints_remaining = ord(MakeString(substr($info, 0, 4)));
			#my $printer_num = ord(MakeString(substr($info, 4, 2)));
			my $prints_remaining = hex( substr( $info, 0, 4 ) );
			my $printer_num	  = hex( substr( $info, 4, 2 ) );

			my $csr = $raw_handle->prepare(	q{BEGIN UPDATE_PRINTER_REMAINING( ?, ?, ?); END; } );

			my $i = 1;
			foreach (($command_hashref->{'machine_id'}, $prints_remaining, $printer_num))
			{
				push @logs, "   Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^b95504f3// )
		{
			# Total Prints
			printf("Sony Total Prints Received\n");
			push @logs, "Sony Total Prints Received";

			#my $total_prints = ord(MakeString(substr($info, 0, 6)));
			#my $printer_num = ord(MakeString(substr($info, 6, 2)));
			my $total_prints = hex( substr( $info, 0, 6 ) );
			my $printer_num  = hex( substr( $info, 6, 2 ) );

			my $csr = $raw_handle->prepare(q{BEGIN UPDATE_PRINTER_ITEMS_PRINTED(?, ?, ?); END;});
			my $i = 1;
			foreach (($command_hashref->{'machine_id'}, $total_prints, $printer_num))
			{
				push @logs, "   Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		#elsif($info =~ s/^b95502..//)
		elsif ( $info =~ s/^b95502// )
		{
			# Sales Summary
			push @logs, " Sony Error Message received";
			push @logs, "	 Info (asc)  : $info";

			#my $alert_context = substr($info, 0, 2);
			my $alert_context = hex( substr( $info, 0, 2 ) );

			#my $alert_msg = substr($info, 2, 2);	needs to be interpreted as numeric
			#my $alert_msg = ord(pack("H*",substr($info, 2, 2)));
			my $alert_msg  = hex( substr( $info, 2, 2 ) );
			my $printer_id = hex( substr( $info, 4, 2 ) );

			push @logs, "Command code $alert_context";
			push @logs, "Message $alert_msg";
			push @logs, "Printer $printer_id";

			my $csr = $raw_handle->prepare(q{BEGIN INSERT_ALERT(	?, ?, ?,
											to_date(?, 'MM/DD/YYYY HH24:MI:SS'),
											?, ?); END; } );

			my $i = 1;
			foreach ((	$command_hashref->{'machine_id'},
						$printer_id, '2', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context ))
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		#elsif($info =~ s/^b95503..//)
		elsif ( $info =~ s/^b95503// )
		{
			# Sales Summary
			push @logs, "   Sony  Status Message Received";

			my $alert_context = hex( substr( $info, 0, 2 ) );
			my $alert_msg	 = hex( substr( $info, 2, 2 ) );
			my $printer_id	= hex( substr( $info, 4, 2 ) );

			my $csr = $raw_handle->prepare(	q{BEGIN INSERT_ALERT(	?, ?, ?,
											to_date(?, 'MM/DD/YYYY HH24:MI:SS'),
											?, ?); END; });

			my $i = 1;
			foreach ((	$command_hashref->{'machine_id'},
						$printer_id, '3', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context))
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}

			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^b95505// )
		{
			# Sales Summary
			push @logs, "   Sony  Other System Error Message Received";

			my $alert_context = hex( substr( $info, 0, 2 ) );
			my $alert_msg	 = hex( substr( $info, 2, 2 ) );
			my $printer_id	= hex( substr( $info, 4, 2 ) );

			my $csr = $raw_handle->prepare(	q{BEGIN
											INSERT_ALERT(	   ?, ?, ?,
											to_date(?, 'MM/DD/YYYY HH24:MI:SS'),
											?, ?); END; });
			my $i = 1;
			foreach ((	$command_hashref->{'machine_id'},
						$printer_id, '5', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context))
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}

		my $sony_info = substr( $command_hashref->{'inbound_command'}, 2 );

		push @logs, "  Insert Sony   Info: $info";
		push @logs, "  Message Nbr	   : " . unpack( "H*", $msg_nbr );

		$DATABASE->insert(
			   table		  => 'SONY_INFO',
			   insert_columns => 'MACHINE_ID, INFO',
			   insert_values => [ $command_hashref->{'machine_id'}, $sony_info ] );

		# create the generic ACK packet
		my $response = pack("CCC", 0x01, 0x2f, $msg_nbr);

		$DATABASE->insert(	table		  	=> 'Machine_Command',
					   		insert_columns => 'Modem_ID, Command',
					   		insert_values  => [ $command_hashref->{machine_id}, &MakeHex($response) ] );

		return 1;
	}
	or push @logs, "Error Occurred $!: $@" . ( $raw_handle and $raw_handle->err
					and "\n\t[" . $raw_handle->err . "] (" . $raw_handle->state . "): " . $raw_handle->errstr );

	return (\@logs);
}

1;
