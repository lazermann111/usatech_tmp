#!/usr/bin/perl -w

use strict;

package ReRix::SoftDnld;

use ReRix::Shared;
use ReRix::Send;

# Software Download Commands: L, M, w, W, x, X, y, Y

# Software Version Number: V

sub parse_V
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;
	my $array_ref;

	$array_ref = $DATABASE->select(
						table			=> 'rerix_modem_to_serial',
						select_columns	=> 'network, modem_id, machine_index',
						where_columns	=> [ 'machine_id = ?'],
						where_values	=> [ $command_hashref->{machine_id} ]
					);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_it and machine_index in rerix_modem_to_serial'] );
	}

	my $network = $array_ref->[0][0];
	my $modem_id = $array_ref->[0][1];
	my $machine_index = $array_ref->[0][2];

	my $version = substr(&MakeString($command_hashref->{inbound_command}), 1, 10);

	# Now to get what the version number should be
	$array_ref = $DATABASE->select(
							table			=> 'rerix_machine',
							select_columns	=> 'SOFTWARE_VERSION',
							where_columns	=> [ 'MODEM_ID = ?'],
							where_values	=> [$modem_id]
					);

	push @logs, "Version Reported: $version";

	if (defined @{$array_ref} && defined $array_ref->[0][0])
	{
		push @logs, "Version Expected: $array_ref->[0][0]";

		if ($array_ref->[0][0] eq $version)
		{
			# No Worries the software version matches

			push @logs, "Versions match";

			$DATABASE->update(
							table			=> 'rerix_machine',
							update_columns	=> 'REPORTED_SOFTWARE_VERSION',
							update_values	=> [$version],
							where_columns	=> ['MODEM_ID = ?'],
							where_values	=> [$modem_id]
			);
		}
		else
		{
			# Wrong Version report it to the database

			push @logs, "Versions different, pinging to start download";

			$DATABASE->update(
							table			=> 'rerix_machine',
							update_columns	=> 'REPORTED_SOFTWARE_VERSION, ' .
												'SOFTWARE_DNLD_COMPLETE, ' .
												'SOFTWARE_DNLD_REQUEST',
							update_values	=> [$version, 'N' , 'Y'],
							where_columns	=> ['MODEM_ID = ?'],
							where_values	=> [$modem_id]
						);

			# Tell the machine to start a software download
#			$DATABASE->insert(
#							table			=> 'Machine_Command',
#							insert_columns	=> 'Modem_ID, Command',
#							insert_values	=> [$command_hashref->{machine_id},
#												&MakeHex('Z' . chr(128))]
#						);
		}
	}
	else
	{
		$array_ref = $DATABASE->select(
							table			=> 'rerix_machine',
							select_columns	=> 'count(*)',
							where_columns	=> [ 'MODEM_ID = ?'],
							where_values	=> [$modem_id]
						);

		if (defined $array_ref &&
			defined $array_ref->[0][0] &&
			($array_ref->[0][0] != 0) )
		{
			# Record exists but No software listed

			push @logs, "Version not in ReRix_Machine entry";

			$DATABASE->update(
						table			=> 'rerix_machine',
						update_columns	=> 'SOFTWARE_VERSION, ' .
											'REPORTED_SOFTWARE_VERSION, ' .
											'SOFTWARE_DNLD_COMPLETE',
						update_values	=> [$version, $version, 'S'],
						where_columns	=> ['MODEM_ID = ?'],
						where_values	=> [$modem_id]
					);
		}
		else
		{
			# No record, lets make one

			push @logs, "No ReRix_Machine entry, inserting one";

			$DATABASE->insert(
						table=> 'rerix_machine',
						insert_columns=> 'MODEM_ID, SOFTWARE_VERSION, ' .
											'REPORTED_SOFTWARE_VERSION, ' .
											'SOFTWARE_DNLD_COMPLETE, ' .
											'COMM_FREQUENCY, ' .
											'STARTING_COMM_TIME',
						insert_values=> [$modem_id, $version, $version,
											'S', 24, int(rand(37)) ]
						); 
		}
	}

	return (\@logs);
}

# Start Software Download at Line (X/Y)

sub parse_XY
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;
	my $array_ref;

	$array_ref = $DATABASE->select(
						table			=> 'rerix_modem_to_serial',
						select_columns	=> 'network, modem_id, machine_index',
						where_columns	=> [ 'machine_id = ?'],
						where_values	=> [ $command_hashref->{machine_id} ]
					);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_it and machine_index in rerix_modem_to_serial'] );
	}

	my $network = $array_ref->[0][0];
	my $modem_id = $array_ref->[0][1];
	my $machine_index = $array_ref->[0][2];

	my ($start, $number, $crc);

	my $message = MakeString( $command_hashref->{inbound_command} );

	if( substr($message, 0, 1) eq 'X' )
	{
		# X's start at 0
		$start = ord(substr($message,1,1));
	}
	else
	{
		# Y's start at 256
		$start = ord(substr($message,1,1)) + 256;
	}

	$number = ord(substr($message,2,1));

	if( length($message) >= 5)
	{
		$crc = ord(substr($message,3,1))<<8 | ord(substr($message,4,1));
	}

	my $crccomp = 0;

	my ($version, $code, $row);

	# Figure out how to get which version here
	$array_ref = $DATABASE->select(
							table			=> 'rerix_machine',
							select_columns	=> 'SOFTWARE_VERSION',
							where_columns	=> [ 'MODEM_ID = ?'],
							where_values	=> [$modem_id]
                                      );

	if (defined @$array_ref  && defined $array_ref->[0][0])
	{
		$version = $array_ref->[0][0];
	}
	else
	{
		$version = 'current';
	}

	$array_ref = $DATABASE->select(
						table			=> 'software_download',
						select_columns	=> 'LINE_NO,  SOFTWARE',
						order			=> 'LINE_NO',    
						where_columns	=> ['SOFTWARE_VERSION = ?',
											'LINE_NO >= ?',
											'LINE_NO < ?'],
						where_values=> [$version, $start, ($start+$number)]
					);

	foreach $row (@$array_ref)
	{
		# Building the code into 1 block for use in CRC
		$code.=&MakeString($row->[1]);
	}

	if (defined $code)
	{
		push @logs, ("     Size of code: " . length($code));

		$crccomp = getcrc16i($code);
		$code = '';
		push @logs, sprintf('Got Checksum: 0x%4.4X Computed: 0x%4.4X' ."\n",
											(defined $crc)?$crc:0, $crccomp);
	}
	else
	{
		push @logs, "Code block empty\n";
	}

	# OK, I got the CRC

	# used to contain a weblink network specific condition
	if (defined($code) && ((!defined $crc) or ($crc != $crccomp)))
	{
		foreach $row (@$array_ref)
		{
			if ($row->[0] < 256)
			{
				$DATABASE->insert(
							table			=> 'Machine_Command',
							insert_columns	=> 'Modem_ID, Command',
							insert_values	=> [$command_hashref->{machine_id},
												&MakeHex('L' . chr($row->[0]) .
												&MakeString($row->[1]))]
						);
			}
			else
			{
				$DATABASE->insert(
							table			=> 'Machine_Command',
							insert_columns	=> 'Modem_ID, Command',
							insert_values	=> [$command_hashref->{machine_id},
												&MakeHex('M' .
														chr($row->[0] - 256) .
												&MakeString($row->[1]))]
							);
			}
		}
	}
	else
	{
		# we are assuming that the code is the same

		push @logs, "CRCs Match\n";
	}

	# Push the "Done" Command
	# used to contain a condition to not send if on weblink
	$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, Command',
				insert_values	=> [$command_hashref->{machine_id},
									&MakeHex('w')]
				);

	return (\@logs);   
}

# Software Download Complete (x)

sub parse_x
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;
	my $array_ref;

	$array_ref = $DATABASE->select(
						table			=> 'rerix_modem_to_serial',
						select_columns	=> 'network, modem_id, machine_index',
						where_columns	=> [ 'machine_id = ?'],
						where_values	=> [ $command_hashref->{machine_id} ]
					);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_id and machine_index in rerix_modem_to_serial'] );
	}

	my $network = $array_ref->[0][0];
	my $modem_id = $array_ref->[0][1];
	my $machine_index = $array_ref->[0][2];

	$DATABASE->update(
					table			=> 'rerix_machine',
					update_columns	=> 'SOFTWARE_DNLD_COMPLETE',
					update_values	=> ['Y'],
					where_columns	=> ['MODEM_ID = ?'],
					where_values	=> [$modem_id]
				);

	return (['     Download complete flag set']);
}

# Stop Software Download

#sub reRIX_parse_W
#{
#   my ($message, $unit)=@_;
#   print "Stopping Download for $unit\n";
#   $DATABASE->insert(
#                    table           => 'Machine_Command',
#                    insert_columns  => 'Modem_ID, Command, Execute_Cd',
#                    insert_values   => [$unit, &MakeHex('STOP DOWNLOAD'), 'S']
#                    );
#   return (1);
#}

1;
