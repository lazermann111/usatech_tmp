#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		09/19/2003	pcowan		Creation of commands:
# 1.01		06/02/2004	pcowan		Added gen_random_key
#

package ReRix::Utils;

use strict;
use Evend::Database::Database;        # Access to the Database
use Net::SMTP;
use DateTime;

use Math::Rand48 qw(lrand48 nrand48);

sub get_device_data
{
	my ($machine_id, $DATABASE) = @_;
	
	my $array_ref = $DATABASE->select(
						table          => 'device',
						select_columns => 'device_id, device_serial_cd, device_type_id',
						where_columns  => ['device_name = ?', 'device_active_yn_flag=?'],
						where_values   => [ $machine_id, 'Y' ]
				);

	if(not $array_ref->[0])
	{
		return (undef, undef, undef);
	}
	
	return ($array_ref->[0][0], $array_ref->[0][1], $array_ref->[0][2]);
}

sub get_message_number
{
	my ($DATABASE) = @_;
	
	my $message_no_ref = $DATABASE->select(
				table			=> 'dual',
				select_columns	=> 'tazdba.rerix_message_num_seq.nextval');
				
	return $message_no_ref->[0][0];
}

sub blob_select
{
	my ($DATABASE, $sqlStmt) = @_;
	my $db = $DATABASE->{handle};	

	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB

	my $stmt = $db->prepare(q{$sqlStmt});

	$stmt->execute();
	
	if(defined $db->errstr)
	{
		print "blob_select error: " . $db->errstr . "\n";
		return undef;
	}

	my $row = 0;
	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}

sub blob_insert
{
	my ($DATABASE, $sqlStmt, $buf) = @_;
	my $db = $DATABASE->{handle};	
	my $LONG_RAW_TYPE=24;
	my $stmt = $db->prepare(q{$sqlStmt});
	my %attrib;
	$attrib{'ora_type'} = $LONG_RAW_TYPE;
	$stmt->bind_param(":blob", $buf, \%attrib);  
	$stmt->execute();
	if(defined $db->errstr)
	{
		print "blob_insert error: " . $db->errstr . "\n";
	}
}

sub get_random_int
{
	my ($min,$max,$in_seed) = @_;
	my $len = 2;
	my $seed;
	my $key;
	my $counter = 1;
	
	while($key < $min || $key > $max)
	{
		while(length($key) < length($max))
		{
			$seed = $counter . time() . $in_seed;
			$key = $key . nrand48($seed);
			$counter++;
		}
		
		$key = substr($key, 0, $len);
	}
		
	return $key;
}

sub send_alert
{
	my ($content, $command_hashref) = @_;
	
	my $data = "$content\n\n";
	my ($key,$value);
	if(defined $command_hashref)
	{
		while(($key,$value) = each(%$command_hashref))
		{
			$data = $data . "$key => $value\n";
		}
	}
	
	#return &send_email('rerix_engine@usatech.com','pcowan@usatech.com,4844671281@mobile.att.net','Rerix Engine Issue', $data);
	#return &send_email('rerix_engine@usatech.com','pcowan@usatech.com','Rerix Engine Issue', $data);
}

sub send_email
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = 'mailhost';
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}

sub send_email_detailed
{
	my ($from_addr, $from_name, $to_addrs, $to_names, $subject, $content) = @_;
	my @to_addr_array = split(/,/, $to_addrs);
	my @to_name_array = split(/,/, $to_names);
	
	# check that every address has a name
	if($#to_addr_array ne $#to_name_array)
	{
		warn "send_email_detailed: Length of address array not equald to length of name array!\n";
		return 0;
	}

	my $smtp_server = 'mailhost';
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_addr_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 

	my $to_line;
	for(my $index = 0; $index <= $#to_addr_array; $index++)
	{
		$to_line = $to_line . "\"$to_name_array[$index]\" <$to_addr_array[$index]>";
		if($index < $#to_addr_array)
		{
			$to_line = $to_line . ", ";
		}
	}
	
	$smtp->datasend("To: $to_line\n");
	$smtp->datasend("From: $from_name <$from_addr>\n");
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("Content-Type: text/plain\n");
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();
	
	return 1;
}

sub getAdjustedTimestamp
{
#----------------------------------------------------------------------------------------------------------
# 	Function: 		: getAdjustedTimestamp ($timezone, $timestamp)
# 	Description 		: This function returns an adjusted 32-bit timestamp value based upon GMT
#	Params:
#		$timezone 	: The timezone to adjust **TO**. If one is not provided
#				  EST will be the default
#		$timestamp	: The timestamp to adjust. If one is not provided then localtime
#				  EST will be used as the default
#	Returns:
#		The adjusted 32-bit timestamp
#
#	Change History		:
#		Version		Developer	Date		Description
#		1.0		T Shannon	10/15/03	first rev
#				
#----------------------------------------------------------------------------------------------------------

	my ($timezone, $timestamp) = @_;
	
	# we need to know if its daylight savings time; if so move the timestamp 'forward' an additional hour
	my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
	# for testing normal time during DST ONLY!!
	#$isdst = 0;

	# if a timestamp is not provided grab the system time (which is in GMT)
	if ((!defined $timestamp) or ($timestamp == 0))
	{
		$timestamp = time();
		print "Utils::getAdjustedTimestamp() - timestamp not provided. Self-generated timestamp = $timestamp (" . localtime($timestamp) . ")\n";
	}
	
	SWITCH: {
		if ($timezone eq 'CST') {$timestamp -= (3600 * (6 - $isdst)); last SWITCH;}	
		if ($timezone eq 'MT') {$timestamp -= (3600 * (7 - $isdst)); last SWITCH;}	
		if ($timezone eq 'MST') {$timestamp -= (3600 * (7 - $isdst)); last SWITCH;}	
		if ($timezone eq 'PT') {$timestamp -= (3600 * (8 - $isdst)); last SWITCH;}
		if ($timezone eq 'PST') {$timestamp -= (3600 * (8 - $isdst)); last SWITCH;}
		# if the time zone is not identified (or IS set to "EST") adjust the time back 
		# only 5 hours (normal time)to adjust to EST
		$timestamp -= (3600 * (5 - $isdst));
		}
	return $timestamp;
}

sub getAdjustedTimestampReverse
{
#----------------------------------------------------------------------------------------------------------
# 	Function: 		: getAdjustedTimestampReverse ($timezone, $timestamp)
# 	Description 		: This function returns an adjusted 32-bit timestamp value that reverses the
#				: the value created by getAdjustedTimestamp()
#	Params:
#		$timezone 	: The timezone to adjust **TO**. If one is not provided
#				  EST will be the default
#		$timestamp	: The timestamp to adjust. If one is not provided then localtime
#				  EST will be used as the default
#	Returns:
#		The adjusted 32-bit timestamp
#
#	Change History		:
#		Version		Developer	Date		Description
#		1.0		T Shannon	11/19/03	first rev
#				
#----------------------------------------------------------------------------------------------------------

	my ($timezone, $timestamp) = @_;
	
	# we need to know if its daylight savings time; if so move the timestamp 'forward' an additional hour
	my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
	# for testing normal time during DST ONLY!!
	#$isdst = 0;

	# if a timestamp is not provided grab the system time (which is in GMT)
	if ((!defined $timestamp) or ($timestamp == 0))
	{
		$timestamp = time();
		print "Utils::getAdjustedTimestamp() - timestamp not provided. Self-generated timestamp = $timestamp (" . localtime($timestamp) . ")\n";
	}
	
	SWITCH: {
		if ($timezone eq 'CST') {$timestamp += (3600 * (6 + $isdst)); last SWITCH;}	
		if ($timezone eq 'MT') {$timestamp += (3600 * (7 + $isdst)); last SWITCH;}	
		if ($timezone eq 'MST') {$timestamp += (3600 * (7 + $isdst)); last SWITCH;}	
		if ($timezone eq 'PT') {$timestamp += (3600 * (8 + $isdst)); last SWITCH;}
		if ($timezone eq 'PST') {$timestamp += (3600 * (8 + $isdst)); last SWITCH;}
		# if the time zone is not identified (or IS set to "EST") adjust the time back 
		# only 5 hours (normal time)to adjust to EST
		$timestamp += (3600 * (5 + $isdst));
		}
	return $timestamp;
}
sub convert_bcd_time
{
	my ($bcd_str) = @_;
	my ($bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year) = unpack("H2H2H2H2H2H4", $bcd_str);
	
	my $dt = DateTime->new(	year   => $bcd_year,
							month  => $bcd_mon,
							day    => $bcd_mday,
							hour   => $bcd_hour,
							minute => $bcd_min,
							second => $bcd_sec);

	return($dt->epoch());
}

sub gen_random_key
{
	my ($SSN) = @_;
	my $len = 16;
	my $seed;
	my $key;
	my $counter = 1;
	
	while(length($key) < $len)
	{
		$seed = $counter . time() . $SSN;
		$key = $key . nrand48($seed);
		$counter++;
	}
	
	$key = substr($key, 0, $len);
		
	return $key;
}

1;
