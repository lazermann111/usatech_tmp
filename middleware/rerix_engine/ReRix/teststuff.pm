#!/usr/bin/perl -w
#
#--------------------------------------------------------------------------------
# Description
#	Provides functionality to export G4 data from USANetwork System to other
#	processing systems
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		11/05/2003	T Shannon	First version
#									

use strict;


	my $vend_qty = '3';
	my @vends=('28', '19', '28');
	my $timestamp = 12345678;
	print "vend_qty = $vend_qty\n";
	print "vends = $vends[0], $vends[1], $vends[2] \n";
	# the first 4 chars are = 2 chars for total vend count + 2 chars for the first column vended
	my $vendstr = "~," . sprintf("%02d", $vend_qty) . sprintf("%02d",$vends[0]) . ",~,";
	my ($i, $j);
	for ($i=1; $i<8; )
	{
		for ($j=0; $j<2; $j++)
		{
			if (defined $vends[$i])
			{
				$vendstr .= sprintf("%02d",$vends[$i]);
			}
			else
			{
				$vendstr .= "00";
			}
			$i++;
			
		}
		if ($i < 8)
		{
			$vendstr .= ",~,";
		}
	}
	print "vendstr = $vendstr\n";
	
	my ($sec, $min, $hr, undef, $mon, $yr, undef, undef, undef) = localtime();
	$yr = sprintf("%02d", $yr);
	print "sec = $sec, min = $min, hr = $hr, mon = $mon, yr = $yr\n";
	my ($path, $fname, $ext) = ("/opt/ReRixEngine2/ReRix/G4Export/data/",
				"EV0199999" . sprintf("_%08d", $timestamp), 
				"new");
	print "The pathname = $path$fname.$ext\n";
	open G4LOG, "> $path$fname$ext";
	printf G4LOG "%s", "EV017579,B102,10/07,15:55,APGReRix,00003,0000,E.123456789,00100007,~,10/07,55:11,0000,~,~,0305,~,0606,~,0000,~,0000,~,0000~,0305,~,0606,~,0000,~,0000,~,0000";
	close G4LOG;
	# after closing the file change its extension to 'log' so the batching process will pick it up
	rename $path . $fname . $ext, $path . $fname . "log";


1;
