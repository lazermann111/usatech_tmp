#!/usr/bin/perl -w

use strict;

package ReRix::Cash;

use ReRix::Shared;
use ReRix::Send;

# Client to Server Ping: I

sub parse
{
    my ($DATABASE, $command_hashref) = @_;

	my (@logs);

    my $message = substr(MakeString($command_hashref->{inbound_command}),1);

	my (	$cash_fill,
			$cash_cashbox,
			$cash_tubes,
			$cash_bills,
			$cash_dispensed,
			$cash_manual,
			$card,
			$sales,
			$vends);

	($cash_fill, $message) = &parse_4byteblock($message);
	($cash_cashbox, $message) = &parse_4byteblock($message);
	($cash_tubes, $message) = &parse_4byteblock($message);
	($cash_bills, $message) = &parse_4byteblock($message);
	($cash_dispensed, $message) = &parse_4byteblock($message);
	($cash_manual, $message) = &parse_4byteblock($message);
	($card, $message) = &parse_4byteblock($message);
	($sales, $message) = &parse_4byteblock($message);
	($vends, $message) = &parse_4byteblock($message);

	push @logs, "Cash Since Last Fill             : $cash_fill";
	push @logs, "Cash to Cashbox Since Last Fill  : $cash_cashbox";
	push @logs, "Cash to Tubes Since Last Fill    : $cash_tubes";
	push @logs, "Bills in Since Last Fill         : $cash_bills";
	push @logs, "Cash Dispensed Since Last Fill   : $cash_dispensed";
	push @logs, "Cash Manually Dispensed S.L.F.   : $cash_manual";
	push @logs, "Card Sales S.L.F                 : $card";
	push @logs, "Sales S.L.F                      : $sales";
	push @logs, "Vends S.L.F                      : $vends";

    my $timestamp;
    if( $command_hashref->{timestamp} )
    {
        $timestamp = $command_hashref->{timestamp};
    }
    else
    {
        $timestamp = $command_hashref->{inbound_date};
    }

	$DATABASE->query(query => "alter session set nls_date_format = 'MM/DD/YYYY HH24:MI:SS'");

	$DATABASE->insert(
					table           => 'rerix_acct_baseline',
					insert_columns  => 'MACHINE_ID, CASH_IN, CASH_BOX, ' .
										'CASH_TUBES, ' .
					                    'BILLS_IN, CASH_DISP, CASH_MAN_DISP, ' .
										'CARD_SALES, ' .
				         	            'SALES, VENDS, ACCT_DATE', 
					insert_values   => [$command_hashref->{machine_id},
										$cash_fill, $cash_cashbox, $cash_tubes,
										$cash_bills, $cash_dispensed,
										$cash_manual, $card,
										$sales, $vends, $timestamp]
		);

	return (\@logs);
}

sub parse_4byteblock
{
   my ($message) = @_;
   my $out = 0;
   $out = ($out<<8) | ord($message); $message = substr($message,1);
   $out = ($out<<8) | ord($message); $message = substr($message,1);
   $out = ($out<<8) | ord($message); $message = substr($message,1);
   $out = ($out<<8) | ord($message); $message = substr($message,1);

   return ($out, $message);

}

1;
