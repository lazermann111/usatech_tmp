#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		07/30/2003	pcowan		Creation of commands: 9A45, 9A47, 9A41, 9A5E, 9A5F
#
# 2.00		01/16/2004	pcowan		New commands: 9A63, 9A6A, 9A62
#									Deprecated: 9A45
#									Format changed: 9A5E, 9A5F
#
# 2.01		03/03/2004	pcowan		I'm sure a bunch of changes have occured since the last entry... 
#									however I will try to record every change going forward.
#
#									Change Network Auth Batch to match changes ReRix spec which 
#									includes a Transaction Result code to tell the server if the
#									transaction was cancelled or failed or timed-out.
#
#									Modified Local and Network batches to call slightly changed stored
#									procedure that now accepts the Transaction Result code
#
# 2.02		03/04/2004	pcowan		Added Track2 parsing for Network Auth and Local Batch
#
# 2.03		03/10/2004	pcowan		Changed room_model_layout_v2 to create new device and hosts if 
#									layout is the same but a host is missing.  Business rule is to ignore this
#									type of change but that has been taken care of by the room controller
#									and is not needed by the server.  Server should now obey any layout
#									sent by the room controller.
#
# 2.04		03/22/2004	pcowan		Emergency update to fix race condition with sending cycle finished emails.
#
# 2.05		03/23/2004	pcowan		Reducing/compressing the amount of logging; log files are growing rapidly
#
# 2.06		03/26/2004	pcowan		Small fix to make sure cycle time can not exceed 99 minutes for top-offs
#
# 2.07		04/07/2004	erybski		Added cycle complete status e-mail notification for future users that requested it
#
# 2.08		04/16/2004	erybski		Added host cycle price subroutine to simplify free-vend mode checking
#									Corrected issue with missing location in e-mail to future users in free-vend mode
#									Updated estimated time to reflect average cycle time for each cycle type in both regular and free-vend modes
#									On cycle complete, last and average cycle time now updated for every host/tran line item type
#									Numerous small changes to meet current business rules, fix typos, improve SQL performance, improve logging, etc.
#
# 2.09		04/19/2004	erybski		Changed all pack c->C statements to insure handling of unsigned bit numbers (should have been this way to begin with)
#									Avg cycle time reset on host cycle time change
#
# 2.10		04/26/2004	pcowan		Changed estimated time remaining business rules to use estimated time from table in most cases
#									Changed some logging a bit
#
# 2.11		06/03/2004	pcowan		Changed stored proc to take amount = amt/qty, because client sends it as
#									the total rather than per item.  Also fixed parser bug when assigned 
#									parser doesn't exst.
#

package ReRix::ESuds;

use strict;
use Evend::Database::Database;        # Access to the Database
use Evend::ReRix::Shared;
use ReRix::Send;
use ReRix::AuthClient;
use Evend::CardUtils;
use ReRix::Utils;
use ReRix::Track2Parser;
use DateTime;

my %command_parsers =	
(
	'45'	=>	[\&ReRix::ESuds::room_model_layout,					'(45h) Room Model Info/Layout'],
	'47'	=>	[\&ReRix::ESuds::room_status,						'(47h) Room Status'],
	'41'	=>	[\&ReRix::ESuds::room_status_starting_port,			'(41h) Room Status w/ Starting Port'],
	'5E'	=>	[\&ReRix::ESuds::esuds_network_auth_batch,			'(5Eh) eSuds Network Authorization Batch'],
	'5F'	=>	[\&ReRix::ESuds::esuds_local_auth_batch,			'(5Fh) eSuds Local Authorization Batch'],
	'63'	=>	[\&ReRix::ESuds::room_model_layout_v2,				'(63h) Room Model Info/Layout V2'],
	'6A'	=>	[\&ReRix::ESuds::washer_dryer_labels,				'(6Ah) Washer/Dryer Labels'],
	'62'	=>	[\&ReRix::ESuds::washer_dryer_diagnostics,			'(62h) Washer/Dryer Diagnostics'],
);

my %track2_parsers =	
(
	'first_9_characters_parser'	=>	[\&ReRix::Track2Parser::first_9_characters_parser,	'1st 9 Characters Track2 Parser'],
);

sub parse
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	my $command_code = uc( substr($command_hashref->{inbound_command},2,2) );

	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	
	if(not defined $device_id)
	{
		push @logs, "No active device record found for $machine_id";
		ReRix::Utils::send_alert("ESuds: No active device record found for $machine_id", $command_hashref);
		return(\@logs);
	}

	if(defined $command_parsers{$command_code})
	{
		my $return_ref = $command_parsers{$command_code}->[0]($DATABASE, $command_hashref);
		push(@logs, ($command_parsers{$command_code}->[1] . " ---"));
		push(@logs, @{$return_ref});
	}
	else
	{
		$DATABASE->update(	table			=> 'Machine_Command_Inbound',
							update_columns	=> 'Execute_Cd',
							update_values	=> ['E'],
							where_columns	=> ['Inbound_ID = ?'],
							where_values	=> [$command_hashref->{inbound_id}] );
					
		print "****************** ESuds Command Not Found: $command_code\n"; 
		ReRix::Utils::send_alert("ESuds: Command Not Found: $command_code\n\n", $command_hashref);
	}
	
	#push @logs, "ESuds Command Execution Complete ---";
	return(\@logs);
}	

sub washer_dryer_labels
{
	# return logs for info and errors for logging
	my (@logs);
	
	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	
	# chop the first 2 byte Data Type
	$message = substr($message, 2);
	
	# labels are in 5 byte chunks, 1 per port
	my $num_ports = length($message)/5;

	if(length($num_ports) > 2)
	{
		push @logs, "Data looks corrupt!  num_ports = $num_ports";
		ReRix::Utils::send_alert("ESuds: washer_dryer_labels: Data looks corrupt! num_ports = $num_ports", $command_hashref);
		return(\@logs);
	}

	push @logs, "Num Ports         : $num_ports";
	
	# eat away at the message until it's gone
	while(length($message) > 0)
	{
		my $chunk = substr($message, 0, 5);
		$message = substr($message, 5);
		
		my $port = ord(substr($chunk, 0, 1));
		my $label = substr($chunk, 1, 3);
		my $top_bottom = substr($chunk, 4, 1);
		
		$label =~ s/\s//g;
		
		# translate T/B into position number
		my $position = 0;
		if($top_bottom eq 'T')
		{
			$position = 1;
		}
		
		if(length($label) == 0)
		{
			push @logs, "Skipping          : Empty label for Port $port";
			next;
		}

		# lookup the host in the database
		my $get_host_ref = $DATABASE->select(
							table			=> 'host',
							select_columns	=> 'host.host_id',
							where_columns	=> [ 'host.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
							where_values	=> [ $device_id, $port, $position ] );
							
		if(not defined $get_host_ref->[0])
		{
			push @logs, @{&request_room_layout($DATABASE, $machine_id)};
			push @logs, "No host record found for device_id $device_id, port $port, position $position";
			ReRix::Utils::send_alert("ESuds: washer_dryer_labels: No host record found for device_id $device_id, port $port, position $position", $command_hashref);
			return(\@logs);	
		}

		my $host_id = $get_host_ref->[0][0];

		# now update the host's label
		$DATABASE->update(	table            => 'host',
							update_columns   => 'host_label_cd',
							update_values    => [$label],
							where_columns    => ['host_id = ?'],
							where_values     => [$host_id] );

		push @logs, "Label             : Port $port, Pos $position; Host $host_id Label = $label";
	}

	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	$DATABASE->insert(	table=> 'machine_command',
						insert_columns=> 'modem_id, command',
						insert_values=> [$machine_id, unpack("H*", $response)] );
	
	return(\@logs);	
}

sub washer_dryer_diagnostics
{
	# return logs for info and errors for logging
	my (@logs);
	
	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	
	my $port = ord(substr($message, 2, 1));
	my $diag_block = substr($message, 3);

	# we are receiving diagnostic, which may have been sent as a result of a pending message
	# even if it wasn't, we're going to remove any pending diagnostic request messages	
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'execute_cd',
						update_values	=> ['A'],
						where_columns	=> ['machine_id = ?', 'data_type = ?', 'execute_cd = ?'],
						where_values	=> [$machine_id, '9A61', 'S'] );
	
	# lookup the host in the database
	# diagnostics for a stack do not report top/bottom, so we are just always linking to bottom
	my $get_host_ref = $DATABASE->select(
						table			=> 'host',
						select_columns	=> 'host.host_id',
						where_columns	=> [ 'host.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
						where_values	=> [ $device_id, $port, 0 ] );
						
	if(not defined $get_host_ref->[0])
	{
		push @logs, @{&request_room_layout($DATABASE, $machine_id)};
		push @logs, "No host record found for device_id $device_id, port $port, position 0";
		ReRix::Utils::send_alert("ESuds: washer_dryer_diagnostics: No host record found for device_id $device_id, port $port, position 0", $command_hashref);
		return(\@logs);	
	}
	
	my $host_id = $get_host_ref->[0][0];
	push @logs, "Port/Host         : $port/$host_id";
	push @logs, "Diagnostic Block  : " . unpack("H*", $diag_block);

	# get any active diagnostic codes
	my $get_actives_ref = $DATABASE->select(
							table			=> 'host_diag_status',
							select_columns	=> 'host_diag_status_id, host_diag_cd',
							where_columns	=> [ 'host_id = ? and host_diag_clear_ts is null' ],
							where_values	=> [ $host_id ] );
	
	my %actives_hash;
	if(not defined $get_actives_ref->[0])
	{
		# there are no active diagnostics
		push @logs, "Active Codes      : NONE";
	}
	else
	{
		# load the active codes into a hashtable
		foreach my $row (@$get_actives_ref)
		{
			my $diag_id = $row->[0];
			my $code = $row->[1];
			push @logs, "Active Code       : ID=$diag_id CODE=$code";
			$actives_hash{$code} = $diag_id;
		}
	}
	
	# now step through the incoming codes and insert/update them in the database
	for(my $index=0; $index<length($diag_block); $index++)
	{
		my $code = ord(substr($diag_block, $index, 1));
		
		# check if this code is currently active for this device
		my $diagnostic_id = $actives_hash{$code};
		
		if(not defined $diagnostic_id)
		{
			# code does not exist, insert it
			$DATABASE->insert(	table=> 'host_diag_status',
								insert_columns=> 'host_id, host_diag_cd',
								insert_values=> [$host_id, $code] );
								
			push @logs, "New Code          : Port=$port CODE=$code";
		}
		else
		{
			# code exists, update it
			# this stupid Evend::Database package put every update value in quotes, which doesn't work for sysdate
	
			my $stmt = $raw_handle->prepare('UPDATE host_diag_status SET host_diag_last_reported_ts = sysdate WHERE host_diag_status_id = ?');
			$stmt->execute($diagnostic_id);
			$stmt->finish();

			push @logs, "Updated Code      : Port=$port CODE=$code";
			
			# remove it from the hash
			delete($actives_hash{$code});
		}
	}
	
	# anything left in the hash is a cleared code!
	while(my ($code, $diagnostic_id) = each(%actives_hash))
	{
		# set the cleared date
		my $stmt = $raw_handle->prepare('UPDATE host_diag_status SET host_diag_clear_ts = sysdate WHERE host_diag_status_id = ?');
		$stmt->execute($diagnostic_id);
		$stmt->finish();

		push @logs, "Cleared Code      : Port= $port CODE=$code";
	}
	
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	$DATABASE->insert(	table=> 'machine_command',
						insert_columns=> 'modem_id, command',
						insert_values=> [$machine_id, unpack("H*", $response)] );
	
	return(\@logs);	
}

sub room_model_layout_v2
{
	# return logs for info and errors for logging
	my (@logs);
	
	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};

	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	
	# chop the first 2 bytes Data Type
	$message = substr($message, 2);
	
	my $db_unit_count = 0;
	my $incoming_unit_count = 0;
	
	# eat away at the message, pull out sent data, lookup in host table
	my @units;
	while(length($message) > 0)
	{
		#push @logs, "Message           : " . unpack("H*", $message);
		
		my $port = substr($message, 0, 1);
		my $serial_number = substr($message, 1, 10);
		my $model_code = substr($message, 11, 1);
		my $firmware = substr($message, 12, 1);
		my $max_price = unpack("n", substr($message, 13, 2));
		my $unit_type = substr($message, 15, 1);
		my $num_supported_cycles = substr($message, 16, 1);
		
		$max_price = sprintf("%.02f", ($max_price * .01));

		$num_supported_cycles = ord($num_supported_cycles);
		my $cycle_block = substr($message, 17, ($num_supported_cycles*4));
		
		# chop this data from the message
		my $unit_block_length = (17 + ($num_supported_cycles*4));
		$message = substr($message, $unit_block_length);
		
		if($unit_type eq 'N')
		{
			push @logs, "Port $port        : Nothing on port, skipping!";
			next;
		}

		# convert port and num_supported_cycles to an integer
		$port = ord($port);
		$model_code = ord($model_code);
		$firmware = ord($firmware);
		
		# convert max price to a number, formatted like a dollar value
		
		# serial_number is left aligned, padded with spaces.  Trim the spaces
		$serial_number =~ s/\s+$//;

		push @logs, "Port              : $port";
		push @logs, "Serial Number     : $serial_number";
		push @logs, "Model Code        : $model_code";
		push @logs, "Firmware Version  : $firmware";
		push @logs, "Max Price         : $max_price";
		push @logs, "Unit Type         : $unit_type";
		push @logs, "Supported Cycles  : $num_supported_cycles";
		push @logs, "Cycle Block       : " . unpack("H*", $cycle_block);

		my @suppored_cycles;
		# parse the suppored cycles block into an array
		# should be in 4 bytes chunks
		for (my $index=0; $index<$num_supported_cycles; $index++)
		{
			my $cycle_chunk = substr($cycle_block, ($index*4), 4);
			my $cycle_type = ord(substr($cycle_chunk, 0, 1));
			my $cycle_price = unpack("n", substr($cycle_chunk, 1, 2));
			my $cycle_time = ord(substr($cycle_chunk, 3, 1));
			$cycle_price = sprintf("%.02f", ($cycle_price * .01));
			
			push(@suppored_cycles, [$cycle_type, $cycle_price, $cycle_time]);
			push @logs, "   Cycle          : Type= $cycle_type, Price=  $cycle_price, Time= $cycle_time";
		}

		my $host_type_id;

		# first use the model_id and unit_type to lookup the host_type
		my $host_type_ref = $DATABASE->select(
								table			=> 'host_type',
								select_columns	=> 'host_type_id',
								where_columns	=> [ 'host_type_cd = ?', 'host_model_cd = ?' ],
								where_values	=> [ $unit_type, $model_code ] );
									
		if(not defined $host_type_ref->[0])
		{
			# this host_type doesn't exist so add a new one
			push @logs, "DB Host Type      : $unit_type,$model_code does not exist, adding...";
				
			# get a new id from the sequence
			my $host_type_seq_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'device.seq_HOST_TYPE_id.nextval' );
		
			if(not defined $host_type_seq_ref->[0])
			{
				push @logs, "Failed to query seq_HOST_TYPE_id sequence for a new ID!";
				ReRix::Utils::send_alert("ESuds: Failed to query seq_HOST_TYPE_id sequence for a new ID!", $command_hashref);
				return(\@logs);	
			}
				
			$host_type_id = $host_type_seq_ref->[0][0];
			push @logs, "New Host Type ID  : $host_type_id";

			# get the default name for this unit type
			my $host_type_name = &get_host_type_name($unit_type);
			if(not defined $host_type_name)
			{
				push @logs, "No host_type_desc found for unit_type $unit_type!";
				ReRix::Utils::send_alert("ESuds: No host_type_desc found for unit_type $unit_type!", $command_hashref);
				return(\@logs);	
			}
				
			# now add the host_type
			$DATABASE->insert(
				table			=> 'host_type',
				insert_columns	=> 'host_type_id, host_type_desc, host_type_manufacturer, host_model_cd, host_default_complete_minut, host_type_cd',
				insert_values	=> [ $host_type_id, $host_type_name, 'Unknown', $model_code, 0, $unit_type]);
					
			push @logs, "New Host Type     : $host_type_id, $host_type_name, Unknown, $model_code, 0, $unit_type";
		}
		else
		{
			# this host_type already existed, so use it
			$host_type_id = $host_type_ref->[0][0];
			push @logs, "Existing Host Type: $host_type_id = $unit_type,$model_code";
		}
		
		# now that we have all the relevant info, save in an array for later use
		# max_price is getting thrown away for now, maybe we'll use it later
		if($unit_type eq 'S' || $unit_type eq 'U')
		{
			# this is a stack, break it into 2 separate hosts
			
			# 0 is bottom
			push(@units, [$model_code, $unit_type, $port, '0', $host_type_id, $firmware, $serial_number, \@suppored_cycles]);
			push @logs, "Received stacked client equip record: $model_code, $unit_type, $port, 0, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;

			# 1 is top
			push(@units, [$model_code, $unit_type, $port, '1', $host_type_id, $firmware, $serial_number, \@suppored_cycles]);
			push @logs, "Received stacked client equip record: $model_code, $unit_type, $port, 1, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;
		}
		else
		{
			push(@units, [$model_code, $unit_type, $port, '0', $host_type_id, $firmware, $serial_number, \@suppored_cycles]);
			push @logs, "Received non-stacked client equip record: $model_code, $unit_type, $port, 0, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;
		}
	}
	
	push @logs, "Incoming Hosts   : $incoming_unit_count";
	
	# we should now have a nice array of incoming host records
	
	# now we lookup any already existing host records
	my $existing_hosts_ref = $DATABASE->select(
						table			=> 'host',
						select_columns	=> 'host_id, host_type_id, host_port_num, host_position_num, host_serial_cd',
						where_columns	=> [ 'device_id = ?' ],
						where_values	=> [ $device_id ] );
						
	# these flags let us know what needs to be done depending on what alreay exists in the database
	my $insert_device = 'N';
	my $insert_host = 'N';

	my %db_units;
	if(defined $existing_hosts_ref->[0])
	{
		# at least one host record exists

		# load up the db records, put in a hash indexed by port number
		foreach my $row (@$existing_hosts_ref)
		{
			$db_units{("$row->[2]-$row->[3]")} = [$row->[1], $row->[4], $row->[0]];
			push @logs, "Existing Host     : ID=$row->[0], Port=$row->[2], Position=$row->[3], host_type_id=$row->[1]";
			$db_unit_count++;
		}
		
		# compare the db records to the client records
		foreach(@units)
		{
			#       0          1       2       3           4           5            6                7
			# [model_code, unit_type, port, position, host_type_id, firmware, serial_number, suppored_cycles]
			
			my $incoming_model_code = $_->[0];
			my $incoming_unit_type = $_->[1];
			my $incoming_port = $_->[2];
			my $incoming_position = $_->[3];
			my $incoming_host_type_id = $_->[4];
			my $incoming_firmware = $_->[5];
			my $incoming_serial_number = $_->[6];
			my $suppored_cycles_ref = $_->[7];
			
			my $key = "$incoming_port-$incoming_position";
			my $db_host_type_id = $db_units{$key}->[0];
			
			if(not defined $db_host_type_id)
			{
				# client record exists but db does not;  this triggers a new device to be inserted
				$insert_device = 'Y';
				$insert_host = 'Y';
				push @logs, "Room Setup Status : Port $incoming_port, Position $incoming_position Does Not Exist";
			}
			else
			{
				if($db_host_type_id eq $incoming_host_type_id)
				{
					# records are equal, don't trigger a new device insert
					push @logs, "Room Setup Status : Port $incoming_port, Position $incoming_position is Identical: $db_host_type_id";
				}
				else
				{
					# records are different;  this causes a new device to be inserted
					$insert_device = 'Y';
					$insert_host = 'Y';
					push @logs, "Room Setup Status :  Port $incoming_port, Position $incoming_position is Changed! ($db_host_type_id != $incoming_host_type_id)";
				}
			}
		}
		
		if($incoming_unit_count < $db_unit_count)
		{
			push @logs, "Room Setup Status : Fewer incoming hosts than existing ($incoming_unit_count < $db_unit_count).   Layout is Changed!";
			$insert_device = 'Y';
			$insert_host = 'Y';
		}
	}
	else
	{
		# no host records exist
		$insert_host = 'Y';
		push @logs, "Room Setup Status : No host records exist in the database";
	}
	
	if($insert_device eq 'Y')
	{
		# make the old device inactive, insert a new one.  Stored proc does this nicely.
		
		my $old_device_id = $device_id;
		my $new_device_id;
		my $return_code;
		my $error_message;
		my $db_err_str;
		
		my $sp_assign_device_ref = $raw_handle->prepare(q{ BEGIN PKG_DEVICE_MAINT.SP_ASSIGN_NEW_DEVICE_ID (:old_id, :new_id, :return_code, :error_message); END; });
		$sp_assign_device_ref->bind_param(":old_id", $old_device_id);
		$sp_assign_device_ref->bind_param_inout(":new_id", \$new_device_id, 20);
		$sp_assign_device_ref->bind_param_inout(":return_code", \$return_code, 20);
		$sp_assign_device_ref->bind_param_inout(":error_message", \$error_message, 40);
		$sp_assign_device_ref->execute;
		$db_err_str = $raw_handle->errstr;
		$sp_assign_device_ref->finish();
		
		if(not defined $new_device_id)
		{
			push @logs, "Failed to call stored procedure PKG_DEVICE_MAINT.SP_ASSIGN_NEW_DEVICE_ID!";
			
			push @logs, "DBI errstr        : $db_err_str";
			push @logs, "Return Code       : $return_code";
			push @logs, "Error Message     : $error_message";

			ReRix::Utils::send_alert("ESuds: Failed to call stored procedure PKG_DEVICE_MAINT.SP_ASSIGN_NEW_DEVICE_ID!\n$error_message\n$db_err_str", $command_hashref);
			return(\@logs);	
		}
	
		# remove spaces
		$new_device_id =~ s/\s//g;

		$device_id = $new_device_id;
		push @logs, "New device_id     : $device_id";
	}
	
	# create a hash to store cycles for later use
	my %cycles_hash;

	# if there was a new device, or no host records existed, insert new ones
	if($insert_host eq 'Y')
	{
		foreach(@units)
		{
			#       0          1       2       3           4           5            6                7
			# [model_code, unit_type, port, position, host_type_id, firmware, serial_number, suppored_cycles]
			
			my $incoming_model_code = $_->[0];
			my $incoming_unit_type = $_->[1];
			my $incoming_port = $_->[2];
			my $incoming_position = $_->[3];
			my $incoming_host_type_id = $_->[4];
			my $incoming_firmware = $_->[5];
			my $incoming_serial_number = $_->[6];
			my $suppored_cycles_ref = $_->[7];

			# insert the host
			# first get the host_id from a sequence
			my $host_seq_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'device.seq_HOST_id.nextval' );
		
			if(not defined $host_seq_ref->[0])
			{
				push @logs, "Failed to query seq_HOST_id sequence for a new ID!";
				ReRix::Utils::send_alert("ESuds: Failed to query seq_HOST_id sequence for a new ID!", $command_hashref);
				return(\@logs);	
			}
				
			my $host_id = $host_seq_ref->[0][0];

			$DATABASE->insert(
				table			=> 'host',
				insert_columns	=> 'host_id, device_id, host_serial_cd, host_status_cd, host_est_complete_minut, host_type_id, host_port_num, host_position_num, HOST_SETTING_UPDATED_YN_FLAG',
				insert_values	=> [$host_id, $device_id, $incoming_serial_number, '0', '0', $incoming_host_type_id, $incoming_port, $incoming_position , 'N']);
			
			push @logs, "New Host          : $host_id, $device_id, $incoming_serial_number, 0, 0, $incoming_host_type_id, $incoming_port, $incoming_position , N";
			
			# store the suppored cycles with host_id as the key
			$cycles_hash{$host_id} = $suppored_cycles_ref;
		}
	}
	else
	{
		# we don't need to insert hosts, but we still want to update the cycles times
		foreach(@units)
		{
			my $incoming_port = $_->[2];
			my $incoming_position = $_->[3];
			my $suppored_cycles_ref = $_->[7];
			
			my $key = "$incoming_port-$incoming_position";
			my $db_host_id = $db_units{$key}->[2];
		
			# store the suppored cycles with host_id as the key
			$cycles_hash{$db_host_id} = $suppored_cycles_ref;
		}
	}
	
	if(($insert_host eq 'N') && ($insert_device eq 'N'))
	{
		push @logs, "Identical Setup   : $device_id";
	}
	
	push @logs, "Processing cycle price/times";
	
	# now iterate through each host and update the incoming cycle times
	while(my($host_id, $cycles_ref) = each (%cycles_hash))
	{
		my @cycles_array = @$cycles_ref;
		foreach my $cycle_ref (@cycles_array)
		{
			my $cycle_type = $cycle_ref->[0];
			my $cycle_price = $cycle_ref->[1];
			my $cycle_time = $cycle_ref->[2];

			my $cycle_desc = &get_item_desc($DATABASE, $cycle_type);
			if(not defined $cycle_desc)
			{
				push @logs, "Failed to lookup tran_line_item_type for item_id $cycle_type";
				ReRix::Utils::send_alert("ESuds: Failed to lookup tran_line_item_type for item_id $cycle_type", $command_hashref);
				return(\@logs);	
			}

			#push @logs, "Cycle Time/Price  : Host $host_id, Type $cycle_type($cycle_desc), Time $cycle_time, Price $cycle_price";
			
			if($cycle_time > 0)	#no machine should allow cycle time of 0, so we assume undefined in db to ease error checking
			{
				# lookup the cycle time in host_setting table
				my $cycle_time_ref = $DATABASE->select(
								table			=> 'host_setting',
								select_columns	=> 'host_setting_value',
								where_columns	=> [ 'host_id = ?', 'HOST_SETTING_PARAMETER = ?' ],
								where_values	=> [ $host_id, "$cycle_desc Time" ] );
				
				if(not defined $cycle_time_ref->[0])
				{
					push @logs, "Insert Cycle      : $host_id, \"$cycle_desc Time\", $cycle_time";
					# cycle time has not been recorded, insert it
					$DATABASE->insert(
						table			=> 'host_setting',
						insert_columns	=> 'host_id, HOST_SETTING_PARAMETER, host_setting_value',
						insert_values	=> [$host_id, "$cycle_desc Time", $cycle_time]);
				}
				else
				{
					if($cycle_time_ref->[0][0] ne $cycle_time)
					{
						push @logs, "Update Cycle      : $host_id, \"$cycle_desc Time\", $cycle_time";
						# cycle time is changed, update it
						$DATABASE->update(table      => 'host_setting',
										update_columns   => 'host_setting_value',
										update_values    => [$cycle_time],
										where_columns	=> [ 'host_id = ?', 'HOST_SETTING_PARAMETER = ?' ],
										where_values	=> [ $host_id, "$cycle_desc Time" ] );
										
						### reset average time (if exists) for this host & cycle_type
						push @logs, "Update Cycle Est. : $host_id, $cycle_type, 0";
						# cycle time is changed, update it
						$DATABASE->update(
							table			=> 'est_tran_complete_time'
							,update_columns	=> 'avg_tran_complete_minut, num_cycle_in_avg'
							,update_values	=> [ 0, 0 ]
							,where_columns	=> [ 'tran_line_item_type_id = ?', 'host_id = ?' ]
							,where_values	=> [ $cycle_type, $host_id ]
						);
					}
					else
					{
						push @logs, "Cycle Up-To-Date  : $host_id, \"$cycle_desc Time\", $cycle_time";
					}
				}
			}
			#else
			#{
			#	push @logs, "Skipping          : Time = $cycle_time";
			#}

#			if($cycle_price > 0)
#			{
			# lookup the cycle price in host_setting table
			my $cycle_price_ref = $DATABASE->select(
							table			=> 'host_setting',
							select_columns	=> 'host_setting_value',
							where_columns	=> [ 'host_id = ?', 'HOST_SETTING_PARAMETER = ?' ],
							where_values	=> [ $host_id, "$cycle_desc Price" ] );

			if(not defined $cycle_price_ref->[0])
			{
				push @logs, "Insert Price      : $host_id, \"$cycle_desc Price\", $cycle_price";
				# cycle price has not been recorded, insert it
				$DATABASE->insert(
					table			=> 'host_setting',
					insert_columns	=> 'host_id, HOST_SETTING_PARAMETER, host_setting_value',
					insert_values	=> [$host_id, "$cycle_desc Price", $cycle_price]);
			}
			else
			{
				if($cycle_price_ref->[0][0] ne $cycle_price)
				{
					push @logs, "Update Price      : $host_id, \"$cycle_desc Price\", $cycle_price";
					# cycle price is changed, update it
					$DATABASE->update(table      => 'host_setting',
									update_columns   => 'host_setting_value',
									update_values    => [$cycle_price],
									where_columns	=> [ 'host_id = ?', 'HOST_SETTING_PARAMETER = ?' ],
									where_values	=> [ $host_id, "$cycle_desc Price" ] );
				}
				else
				{
					push @logs, "Cycle Up-To-Date  : $host_id, \"$cycle_desc Price\", $cycle_price";
				}
			}
#			}
			#else
			#{
			#	push @logs, "Skipping          : Price = $cycle_price";
			#}
		}
	}
	
	# pack a response and save it
	# response command is a hex '9A,46'
	my $response = pack("C*C*C*", $response_no, 0x9A, 0x46);
	push @logs, "Response          : " . unpack("H*", $response);
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*",$response)] );
	
	return(\@logs);	
}

sub room_model_layout
{
	# return logs for info and errors for logging
	my (@logs);
	
	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	
	push @logs, "room_model_layout (9Ah45h) : Deprecated! No response sent.";
	
	#my $response = pack("C*C*C*", $response_no, 0x9A, 0x46);
	#push @logs, "Response          : " . unpack("H*", $response);
	#$DATABASE->insert(	table=> 'Machine_Command',
	#					insert_columns=> 'Modem_ID, Command',
	#					insert_values=> [$machine_id, unpack("H*",$response)] );
	
	return(\@logs);	
}
	
sub room_status
{
	# return logs for info and errors for logging
	my (@logs);
	
	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 

	#my $starting_port = ord(substr($message, 2, 1));
	#my $bytes = substr($message, 3);
	
	my $starting_port = 1;
	my $bytes = substr($message, 2);
	
	for(my $portIndex = 0; $portIndex<length($bytes); $portIndex++)
	{
		my $port = ($portIndex+$starting_port);
		my $status = unpack("H*", substr($bytes, $portIndex, 1));
		
		if($status eq '04')
		{
			push @logs, "Skipping          : Port $port is empty";
			next;
		}
		
		# hack
		my $temp_status = $status;
		$status = substr($temp_status,1,1) . substr($temp_status,0,1);
		
		for(my $position = 0; $position <= (length($status)-1); $position++)
		{
			my $position_status = substr($status, $position, 1);
			$position_status =~ s/\s+$//;
			
			# lookup the host in the database
			my $get_host_ref = $DATABASE->select(
								table			=> 'host, host_type',
								select_columns	=> "host.host_id, host.host_status_cd, host_type.host_type_desc, host_type.host_model_cd, host_type_cd, host_label_cd, to_char(host.host_last_start_ts, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
								where_columns	=> [ 'host.host_type_id = host_type.host_type_id and host.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
								where_values	=> [ $device_id, $port, $position ] );
		
			if(defined $get_host_ref->[0])
			{
				push @logs, "Port/Pos/Status   : $port/$position/$position_status -----";
				
				my $host_id = $get_host_ref->[0][0];
				my $host_status_flag = $get_host_ref->[0][1];
				my $host_type_name = $get_host_ref->[0][2];
				my $host_model_cd = $get_host_ref->[0][3];
				my $host_type_cd = $get_host_ref->[0][4];
				my $host_label_cd = $get_host_ref->[0][5];
				my $last_start_str = $get_host_ref->[0][6];
				
				# for free-vend mode usage
				my $base_cycle_type_id = &get_host_base_cycle_type_id($host_type_cd);
				
				$host_status_flag =~ s/\s+$//;
				
				push @logs, "Database Host     : $host_id($host_label_cd) status = $host_status_flag";

				if($host_status_flag eq $position_status)
				{
					push @logs, "No Change!        : Port $port, Position $position";
				}
				else
				{
					my $description = $host_type_name . " " . $host_label_cd;
					if(($host_type_cd eq 'S' || $host_type_cd eq 'U') && $position eq '0')
					{
						$description = "Bottom " . $description;
					}
					elsif(($host_type_cd eq 'S' || $host_type_cd eq 'U') && $position eq '1')
					{
						$description = "Top " . $description;
					}
					
					my $cycle_started = 0;
	
					# compare the status
					if(($host_status_flag eq '2' || $host_status_flag eq '7') && ($position_status ne '2' && $position_status ne '7' && $position_status ne '8'))
					{
						# cycle finished
						push @logs, "Cycle Finished!   : $description";
						push @logs, "Email             : Sending cycle finished alert for $description";
						my $email_result = &send_cycle_finished_email($device_id, $host_id, $description, $DATABASE);
						push @logs, "Email Result      : $email_result";
						
						# get the last_start_time as seconds since the epoch
						my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
						my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $last_start_str);
						my $last_start_ts = Time::Local::timelocal($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);

						# get the current seconds since the epoch
						my $dt = DateTime->now;
						my $current_ts = $dt->epoch();

						# find the difference and round to the nearest minute
						my $cycle_et_orig = (($current_ts - $last_start_ts)/60);
						my $cycle_et = sprintf("%.0f", $cycle_et_orig<0?int($cycle_et_orig-0.5):int($cycle_et_orig+0.5));	#sprintf rounding not reliable for floating-point round arithmetic

						push @logs, "Last Start Time   : $last_start_str";
						push @logs, "Last Start TS     : $last_start_ts";
						push @logs, "Current TS        : $current_ts";
						push @logs, "Calculated Time   : $cycle_et Minutes (exact: $cycle_et_orig)";

						# do some sanity checks on the calculated time
						if($cycle_et < 15 || $cycle_et > 120)
						{
							push @logs, "Cycle Sanity Check: $cycle_et looks goofy, ingoring it";
						}
						else
						{
							# TODO: this should probably use the calculated run time from above, and query for the minimum
							# tran_line_item_id where the line item timestamp is greater than (current time - run minutes)
							# because top-offs could screw us up
							# now lookup the last recorded transaction to get what kinda cycle this was
							my $get_cycle_type_ref = $DATABASE->select(
												table			=> 'tran_line_item, tran_line_item_type',
												select_columns	=> "tran_line_item_type.tran_line_item_type_desc, tran_line_item_type.tran_line_item_type_id",
												where_columns	=> [ 'tran_line_item.tran_line_item_type_id = tran_line_item_type.tran_line_item_type_id and tran_line_item.tran_line_item_id = (select max(tran_line_item.tran_line_item_id) from tran_line_item where tran_line_item.host_id = ?)' ],
												where_values	=> [ $host_id ] );

							if(not defined $get_cycle_type_ref->[0])
							{
								push @logs, "Cycle Type        : Not found for host_id $host_id";
							}
							else
							{
								my $cycle_desc = $get_cycle_type_ref->[0][0];
								my $cycle_type_id = $get_cycle_type_ref->[0][1];
								
								push @logs, "Cycle Type/Time   : Host $host_id, Type $cycle_desc, Type ID $cycle_type_id, Time $cycle_et";

								### determine whether or not we need to insert or update last cycle time
								my $last_tran_cycle_time_ref = $DATABASE->select(
									table			=> 'est_tran_complete_time',
									select_columns	=> 'tran_line_item_type_id, last_tran_complete_minut',
									where_columns	=> ['tran_line_item_type_id = ?' ,'host_id = ?'],
									where_values	=> [ $cycle_type_id, $host_id ]
								);
								
								if (not defined $last_tran_cycle_time_ref->[0])	#insert
								{
									push @logs, "Insert Cycle Est. : $cycle_type_id, $host_id, $cycle_et";
									$DATABASE->insert(
										table			=> 'est_tran_complete_time'
										,insert_columns	=> 'tran_line_item_type_id, host_id, last_tran_complete_minut'
										,insert_values	=> [$cycle_type_id, $host_id, $cycle_et]
									);
								}
								else	#update
								{
									push @logs, "Update Cycle Est. : $cycle_type_id, $host_id, $cycle_et";
									$DATABASE->update(
										table			=> 'est_tran_complete_time'
										,update_columns	=> 'last_tran_complete_minut'
										,update_values	=> [ $cycle_et ]
										,where_columns	=> [ 'tran_line_item_type_id = ?', 'host_id = ?' ]
										,where_values	=> [ $cycle_type_id, $host_id ]
									);
								}
							}
						}
					}
					elsif($position_status eq '2' && $host_status_flag ne '2')
					{
						# initial cycle started
						push @logs, "Cycle Started!    : $description";
						
						### check if we are in free-vend or regular mode
						my $host_cycle_price = &_host_base_cycle_price($DATABASE, $host_id);
						if ($host_cycle_price == -1)	#error
						{
							push @logs, "Unable to determine free-vend mode on/off flag for device $device_id host $host_id. Skipping estimated time setting for this host.";
						}
						elsif ($host_cycle_price == 0)	#free-vend mode
						{
							### if in new cycle, determine the appropriate completion minutes and set it
							
							# if we have an average for this host, use it as the est completion time
							# if this is a new host, use the reported time
							# if we don't have either, set est completion minutes to zero
							
							my $host_est_complete_minut;
							
							# get the average we have been keeping
							my $host_avg_time_ref = $DATABASE->select(
								table			=> 'est_tran_complete_time',
								select_columns	=> 'avg_tran_complete_minut',
								where_columns	=> ['host_id = ?', 'tran_line_item_type_id = ?'],
								where_values	=> [ $host_id, $base_cycle_type_id]);
								
							$host_est_complete_minut = $host_avg_time_ref->[0][0];
							
							if(not defined $host_est_complete_minut)
							{
								# we failed to get an average, now try to get the reported time
								my $host_reported_time_ref = $DATABASE->select(
									table			=> 'host_setting',
									select_columns	=> 'host_setting_value',
									where_columns	=> ['host_id = ?',
										"host_setting_parameter = ( 
										 select 	tran_line_item_type_desc || ' Time'
										 from	tran_line_item_type
										 where 	tran_line_item_type_id = ?)" ],
									where_values	=> [ $host_id, $base_cycle_type_id ]);
								
								$host_est_complete_minut = $host_reported_time_ref->[0][0];
							}
							
							if(not defined $host_est_complete_minut)
							{
								$host_est_complete_minut = 0;
							}
							
							### set the estimated time
							$DATABASE->update(	table            => 'host',
												update_columns   => 'HOST_EST_COMPLETE_MINUT',
												update_values    => [$host_est_complete_minut],
												where_columns    => ['host_id = ?'],
												where_values     => [$host_id] );
							push @logs, "Setting estimated minutes to $host_est_complete_minut for host $host_id port $port";
						}
						else	#regular mode
						{
							# if we started a new cycle, zero out the completion minutes.  it should get populated
							# when we receive the batch
							# but what if we already received the batch?  then the status should be transaction in 
							# progress, so check for that and don't zero out the time if it is
							if($host_status_flag ne '8')
							{
								$DATABASE->update(	table            => 'host',
													update_columns   => 'HOST_EST_COMPLETE_MINUT',
													update_values    => [0],
													where_columns    => ['host_id = ?'],
													where_values     => [$host_id] );
								push @logs, "Setting estimated minutes to 0 for host $host_id port $port";
							}
						}
					}
					
					### update the host status 
					$DATABASE->update(	table            => 'host',
										update_columns   => 'host_status_cd',
										update_values    => [$position_status],
										where_columns    => ['host_id = ?'],
										where_values     => [$host_id] );
					push @logs, "Updated Port $port Position $position status to $position_status";
				}
			}
			else
			{
				if($position_status ne '0')
				{
					push @logs, @{&request_room_layout($DATABASE, $machine_id)};
					ReRix::Utils::send_alert("ESuds: room_status: No host record found for device_id $device_id, port $port, position $position", $command_hashref);
					push @logs, "No host record found for device_id $device_id, port $port, position $position";
					return(\@logs);	
				}
			}
		}
	}
		
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	$DATABASE->insert(	table=> 'machine_command',
						insert_columns=> 'modem_id, command',
						insert_values=> [$machine_id, unpack("H*", $response)] );

	return(\@logs);	
}

sub room_status_starting_port
{
	# return logs for info and errors for logging
	my (@logs);
	
	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 

	my $starting_port = ord(substr($message, 2, 1));
	my $bytes = substr($message, 3);
	
	#my $starting_port = 1;
	#my $bytes = substr($message, 2);
	
	for(my $portIndex = 0; $portIndex<length($bytes); $portIndex++)
	{
		my $port = ($portIndex+$starting_port);
		my $status = unpack("H*", substr($bytes, $portIndex, 1));
		
		if($status eq '04')
		{
			push @logs, "Skipping          : Port $port is empty";
			next;
		}
		
		# hack
		my $temp_status = $status;
		$status = substr($temp_status,1,1) . substr($temp_status,0,1);
		
		for(my $position = 0; $position <= (length($status)-1); $position++)
		{
			my $position_status = substr($status, $position, 1);
			$position_status =~ s/\s+$//;
			
			# lookup the host in the database
			my $get_host_ref = $DATABASE->select(
								table			=> 'host, host_type',
								select_columns	=> "host.host_id, host.host_status_cd, host_type.host_type_desc, host_type.host_model_cd, host_type_cd, host_label_cd, to_char(host.host_last_start_ts, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
								where_columns	=> [ 'host.host_type_id = host_type.host_type_id and host.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
								where_values	=> [ $device_id, $port, $position ] );
		
			if(defined $get_host_ref->[0])
			{
				push @logs, "Port/Pos/Status   : $port/$position/$position_status -----";
				
				my $host_id = $get_host_ref->[0][0];
				my $host_status_flag = $get_host_ref->[0][1];
				my $host_type_name = $get_host_ref->[0][2];
				my $host_model_cd = $get_host_ref->[0][3];
				my $host_type_cd = $get_host_ref->[0][4];
				my $host_label_cd = $get_host_ref->[0][5];
				my $last_start_str = $get_host_ref->[0][6];
				
				# for free-vend mode usage
				my $base_cycle_type_id = &get_host_base_cycle_type_id($host_type_cd);
				
				$host_status_flag =~ s/\s+$//;
				
				push @logs, "Database Host     : $host_id($host_label_cd) status = $host_status_flag";

				if($host_status_flag eq $position_status)
				{
					push @logs, "No Change!        : Port $port, Position $position";
				}
				else
				{
					my $description = $host_type_name . " " . $host_label_cd;
					if(($host_type_cd eq 'S' || $host_type_cd eq 'U') && $position eq '0')
					{
						$description = "Bottom " . $description;
					}
					elsif(($host_type_cd eq 'S' || $host_type_cd eq 'U') && $position eq '1')
					{
						$description = "Top " . $description;
					}
					
					my $cycle_started = 0;
	
					# compare the status
					if(($host_status_flag eq '2' || $host_status_flag eq '7') && ($position_status ne '2' && $position_status ne '7' && $position_status ne '8'))
					{
						# cycle finished
						push @logs, "Cycle Finished!   : $description";
						push @logs, "Email             : Sending cycle finished alert for $description";
						my $email_result = &send_cycle_finished_email($device_id, $host_id, $description, $DATABASE);
						push @logs, "Email Result      : $email_result";
						
						# get the last_start_time as seconds since the epoch
						my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
						my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $last_start_str);
						my $last_start_ts = Time::Local::timelocal($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);

						# get the current seconds since the epoch
						my $dt = DateTime->now;
						my $current_ts = $dt->epoch();

						# find the difference and round to the nearest minute
						my $cycle_et_orig = (($current_ts - $last_start_ts)/60);
						my $cycle_et = $cycle_et_orig<0?int($cycle_et_orig-0.5):int($cycle_et_orig+0.5);	#sprintf rounding not reliable for floating-point round arithmetic

						push @logs, "Last Start Time   : $last_start_str";
						push @logs, "Last Start TS     : $last_start_ts";
						push @logs, "Current TS        : $current_ts";
						push @logs, "Calculated Time   : $cycle_et Minutes (exact: $cycle_et_orig)";

						# do some sanity checks on the calculated time
						if($cycle_et < 15 || $cycle_et > 120)
						{
							push @logs, "Cycle Sanity Check: $cycle_et looks goofy, ingoring it";
						}
						else
						{
							# TODO: this should probably use the calculated run time from above, and query for the minimum
							# tran_line_item_id where the line item timestamp is greater than (current time - run minutes)
							# because top-offs could screw us up
							# now lookup the last recorded transaction to get what kinda cycle this was
							my $get_cycle_type_ref = $DATABASE->select(
												table			=> 'tran_line_item, tran_line_item_type',
												select_columns	=> "tran_line_item_type.tran_line_item_type_desc, tran_line_item_type.tran_line_item_type_id",
												where_columns	=> [ 'tran_line_item.tran_line_item_type_id = tran_line_item_type.tran_line_item_type_id and tran_line_item.tran_line_item_id = (select max(tran_line_item.tran_line_item_id) from tran_line_item where tran_line_item.host_id = ?)' ],
												where_values	=> [ $host_id ] );

							if(not defined $get_cycle_type_ref->[0])
							{
								push @logs, "Cycle Type        : Not found for host_id $host_id";
							}
							else
							{
								my $cycle_desc = $get_cycle_type_ref->[0][0];
								my $cycle_type_id = $get_cycle_type_ref->[0][1];
								
								push @logs, "Cycle Type/Time   : Host $host_id, Type $cycle_desc, Type ID $cycle_type_id, Time $cycle_et";

								### determine whether or not we need to insert or update last cycle time
								my $last_tran_cycle_time_ref = $DATABASE->select(
									table			=> 'est_tran_complete_time',
									select_columns	=> 'tran_line_item_type_id, last_tran_complete_minut',
									where_columns	=> ['tran_line_item_type_id = ?' ,'host_id = ?'],
									where_values	=> [ $cycle_type_id, $host_id ]
								);
								
								if (not defined $last_tran_cycle_time_ref->[0])	#insert
								{
									push @logs, "Insert Cycle Est. : $cycle_type_id, $host_id, $cycle_et";
									$DATABASE->insert(
										table			=> 'est_tran_complete_time'
										,insert_columns	=> 'tran_line_item_type_id, host_id, last_tran_complete_minut'
										,insert_values	=> [$cycle_type_id, $host_id, $cycle_et]
									);
								}
								else	#update
								{
									push @logs, "Update Cycle Est. : $cycle_type_id, $host_id, $cycle_et";
									$DATABASE->update(
										table			=> 'est_tran_complete_time'
										,update_columns	=> 'last_tran_complete_minut'
										,update_values	=> [ $cycle_et ]
										,where_columns	=> [ 'tran_line_item_type_id = ?', 'host_id = ?' ]
										,where_values	=> [ $cycle_type_id, $host_id ]
									);
								}
							}
						}
					}
					elsif($position_status eq '2' && $host_status_flag ne '2')
					{
						# initial cycle started
						push @logs, "Cycle Started!    : $description";
						
						### check if we are in free-vend or regular mode
						my $host_cycle_price = &_host_base_cycle_price($DATABASE, $host_id);
						if ($host_cycle_price == -1)	#error
						{
							push @logs, "Unable to determine free-vend mode on/off flag for device $device_id host $host_id. Skipping estimated time setting for this host.";
						}
						elsif ($host_cycle_price == 0)	#free-vend mode
						{
							### if in new cycle, determine the appropriate completion minutes and set it
							
							# if we have an average for this host, use it as the est completion time
							# if this is a new host, use the reported time
							# if we don't have either, set est completion minutes to zero
							
							my $host_est_complete_minut;
							
							# get the average we have been keeping
							my $host_avg_time_ref = $DATABASE->select(
								table			=> 'est_tran_complete_time',
								select_columns	=> 'avg_tran_complete_minut',
								where_columns	=> ['host_id = ?', 'tran_line_item_type_id = ?'],
								where_values	=> [ $host_id, $base_cycle_type_id]);
								
							$host_est_complete_minut = $host_avg_time_ref->[0][0];
							
							if(not defined $host_est_complete_minut)
							{
								# we failed to get an average, now try to get the reported time
								my $host_reported_time_ref = $DATABASE->select(
									table			=> 'host_setting',
									select_columns	=> 'host_setting_value',
									where_columns	=> ['host_id = ?',
										"host_setting_parameter = ( 
										 select 	tran_line_item_type_desc || ' Time'
										 from	tran_line_item_type
										 where 	tran_line_item_type_id = ?)" ],
									where_values	=> [ $host_id, $base_cycle_type_id ]);
								
								$host_est_complete_minut = $host_reported_time_ref->[0][0];
							}
							
							if(not defined $host_est_complete_minut)
							{
								$host_est_complete_minut = 0;
							}
							
							### set the estimated time
							$DATABASE->update(	table            => 'host',
												update_columns   => 'HOST_EST_COMPLETE_MINUT',
												update_values    => [$host_est_complete_minut],
												where_columns    => ['host_id = ?'],
												where_values     => [$host_id] );
							push @logs, "Setting estimated minutes to $host_est_complete_minut for host $host_id port $port";
						}
						else	#regular mode
						{
							# if we started a new cycle, zero out the completion minutes.  it should get populated
							# when we receive the batch
							# but what if we already received the batch?  then the status should be transaction in 
							# progress, so check for that and don't zero out the time if it is
							if($host_status_flag ne '8')
							{
								$DATABASE->update(	table            => 'host',
													update_columns   => 'HOST_EST_COMPLETE_MINUT',
													update_values    => [0],
													where_columns    => ['host_id = ?'],
													where_values     => [$host_id] );
								push @logs, "Setting estimated minutes to 0 for host $host_id port $port";
							}
						}
					}
					
					### update the host status 
					$DATABASE->update(	table            => 'host',
										update_columns   => 'host_status_cd',
										update_values    => [$position_status],
										where_columns    => ['host_id = ?'],
										where_values     => [$host_id] );
					push @logs, "Updated Port $port Position $position status to $position_status";
				}
			}
			else
			{
				if($position_status ne '0')
				{
					push @logs, @{&request_room_layout($DATABASE, $machine_id)};
					ReRix::Utils::send_alert("ESuds: room_status: No host record found for device_id $device_id, port $port, position $position", $command_hashref);
					push @logs, "No host record found for device_id $device_id, port $port, position $position";
					return(\@logs);	
				}
			}
		}
	}
		
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	$DATABASE->insert(	table=> 'machine_command',
						insert_columns=> 'modem_id, command',
						insert_values=> [$machine_id, unpack("H*", $response)] );

	return(\@logs);	
}

sub authorize
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};
	
	if($device_type ne '5')
	{
		push @logs, "Huh!?  ESuds::authorize received a request to authorize a non-esuds device!";
		ReRix::Utils::send_alert("ESuds: ESuds::authorize received a request to authorize a non-esuds device!\n\n$device_id", $command_hashref);
		return(\@logs);
	}
	
	push @logs, "Authorize         : Processing ESuds Authorization Request!";

	my (undef, $trans_id, $card_type, $auth_amt, $auth_data) = unpack("CNaH6A*", $message);

	# }:-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
	$auth_amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $auth_amt)))));
	$auth_amt = sprintf("%.02f", ($auth_amt * .01));
	
	if($card_type ne 'S')
	{
		# eSuds should not send a credit card at this time
		# TODO: Add support for credit cards and RFID (?) in eSuds
		
		push @logs, "Huh!?  ESuds::authorize received a request to authorize a non-special card: $card_type";
		ReRix::Utils::send_alert("Huh!?  ESuds::authorize received a request to authorize a non-special card: $card_type", $command_hashref);
		return(\@logs);
	}

	push @logs, "Trans ID          : $trans_id";
	push @logs, "Card Type         : $card_type";
	push @logs, "Auth Data         : $auth_data";
	push @logs, "Amount            : $auth_amt";
	
	# NOTE:  We need to adjust this timestamp to the timezone where this device is located so
	# it shows up correctly on the reports
	
	my $time_zone;

	# Use the device_id as a rerefence to lookup the time zone
	my $lookup_tz_ref =	$DATABASE->select(	
							table          => 'location.location, pos',
							select_columns => 'location.location_time_zone_cd',
							where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
							where_values   => [$device_id] );
							
	$time_zone = $lookup_tz_ref->[0][0];
	
	if(not defined $time_zone)
	{
		$time_zone = 'EST';
		push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
	}
	else
	{
		push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
	}

	my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
	$Month = $Month+1;
    $Year  = $Year+1900;
    
    my $start_ts = "$Month/$Day/$Year $Hour:$Minute:$Second";
	push @logs, "Transaction Time  : $start_ts";
	
	# if this device is one that uses a card reader, we need to parse the track2 data and pull out the student ID
	my $student_id;

	if($card_type eq 'S')
	{
		# lookup the card parser for this device

		my $lookup_parser_ref =	$DATABASE->select(	
							table          => 'device_setting',
							select_columns => 'device_setting.device_setting_value',
							where_columns  => ['device_setting.device_setting_parameter_cd = ?', 'device_setting.device_id = ?'],
							where_values   => ['Card Data Parser', $device_id] );
	
		if(not defined $lookup_parser_ref->[0][0])
		{
			push @logs, "Track2 Parser     : Not found for device $device_id.  Using unmodifed: $auth_data";
			$student_id = $auth_data;
		}
		else
		{
			my $parser_key = $lookup_parser_ref->[0][0];
			$student_id = $track2_parsers{$parser_key}->[0]($DATABASE, $command_hashref, $auth_data);
			
			if(defined $student_id)
			{
				push @logs, "Track2 Parser     : \"$track2_parsers{$parser_key}->[1]\" returned studentID = $student_id";
			}
			else
			{
				push @logs, "Track2 Parser     : \"$track2_parsers{$parser_key}->[1]\" Failed! Using unmodifed: $auth_data";
				$student_id = $auth_data;
			}
		}
	}

	my $approved_flag;
	my $return_code;
	my $error_message;
	my $approved;
	my $db_err_str;
	
	# call a stored proc to authorize the tran
	my $raw_handle = $DATABASE->{handle};
	my $sp_auth_ref = $raw_handle->prepare(q{ BEGIN PKG_PROCESS_TRANSACTION.sp_authorize (:account_id, :device_id, :tran_id, :card_type, :amount, to_date(:start_ts, 'MM/DD/YYYY HH24:MI:SS'), :local_auth_flag, :approved_flag, :return_code, :error_message); END; });
	$sp_auth_ref->bind_param(":account_id", $student_id);
	$sp_auth_ref->bind_param(":device_id", $device_id);
	$sp_auth_ref->bind_param(":tran_id", $trans_id);
	$sp_auth_ref->bind_param(":card_type", $card_type);
	$sp_auth_ref->bind_param(":amount", $auth_amt);
	$sp_auth_ref->bind_param(":start_ts", $start_ts);
	$sp_auth_ref->bind_param(":local_auth_flag", 'N');
	$sp_auth_ref->bind_param_inout(":approved_flag", \$approved_flag, 1);
	$sp_auth_ref->bind_param_inout(":return_code", \$return_code, 40);
	$sp_auth_ref->bind_param_inout(":error_message", \$error_message, 256);
	$sp_auth_ref->execute;
	$db_err_str = $raw_handle->errstr;
	$sp_auth_ref->finish();
	
	push @logs, "Stored Procedure  : PKG_PROCESS_TRANSACTION.sp_authorize($student_id, $device_id, $trans_id, $card_type, $auth_amt, $start_ts, N)";
	
	if(defined $db_err_str)
	{
		push @logs, "DBI errstr        : $db_err_str";
	}
	
	if(defined $error_message)
	{
		push @logs, "Error Message     : $error_message";
	}
	
	push @logs, "Return Code       : $return_code";
	push @logs, "Approved Flag     : $approved_flag";
	
	# 0  is success, 
	# 9  is user doesn't exist
	# 10 is card not accepted at point of sale
	# 17 is account is not active
	# 18 is student is authing at wrong location
	# 19 is insufficent account balance
	
	if($return_code ne "0" && $return_code ne "9" && $return_code ne "10" && $return_code ne "17" && $return_code ne "18" && $return_code ne "19")  
	{
		push @logs, "Procedure   : PKG_PROCESS_TRANSACTION.sp_authorize failed! return_code = $return_code";
		ReRix::Utils::send_alert("ESuds: Procedure   : PKG_PROCESS_TRANSACTION.sp_authorize failed!\n$error_message\n$db_err_str", $command_hashref);
		return(\@logs);	
	}
	
	# DBI returns a bunch of spaces after the approved flag, so just compare the first character
	if(substr($approved_flag,0,1) eq 'Y')
	{
		$approved = 1;
	}
	else
	{
		$approved = 0;
	}
	
 	# Auth response command is a hex '60' (decimal 96)
	my $response = pack("CCNC", $response_no, 96, $trans_id, $approved);

	push @logs, "Response          : " . unpack("H*", $response);
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*", $response)]);
						
	return(\@logs);
}

sub esuds_network_auth_batch
{
	my (@logs);
	
	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	
	# card_number should be the student ID

	my $trans_id = unpack("N", substr($message,2,4));
	my $num_line_items = ord(substr($message,6,1));
	my $tran_result = 'Q';

	if($num_line_items == 0)
	{
		$tran_result = 'C';
	}

	my $item_start_pos = 7;

	if($device_type ne '5')
	{
		push @logs, "Huh!?  ESuds::esuds_network_auth_batch received a batch for a non-esuds device!";
		ReRix::Utils::send_alert("ESuds: Huh!?  ESuds::esuds_network_auth_batch received a batch for a non-esuds device!", $command_hashref);
		return(\@logs);
	}

	push @logs, "Trans ID          : $trans_id";
	push @logs, "Transaction Result: $tran_result";
	push @logs, "Num Line Items    : $num_line_items";
	
	my $total = 0;
	my $data = substr($message, $item_start_pos);
	
	my $db_err_str;
	my $raw_handle;

	# BUG FIX - Lookup and use the device_id of the device as it existed at the time of the transaction (most recent trans only to make sure unique in case of multiple transactions in less that 1 second)
	$raw_handle = $DATABASE->{handle};
	my $get_device_id_stmt = $raw_handle->prepare(q{ select device_id from ( select device.device_id from tran, pos, device where tran.pos_id = pos.pos_id and pos.device_id = device.device_id and device.device_name = :ev_number and tran.device_tran_cd = :device_tran_cd order by tran.tran_auth_ts desc ) where rownum = 1 });
	if(not defined $get_device_id_stmt)
	{
		push @logs, "ESuds::esuds_network_auth_batch: Failed to prepare get_device_id_stmt statmement";
		ReRix::Utils::send_alert("ESuds::esuds_network_auth_batch: Failed to prepare get_device_id_stmt statmement", $command_hashref);
		return(\@logs);
	} 
	
	$get_device_id_stmt->bind_param(":ev_number", $machine_id);
	$get_device_id_stmt->bind_param(":device_tran_cd", $trans_id);
	$get_device_id_stmt->execute();
	
	$db_err_str = $get_device_id_stmt->errstr;
	if(defined $db_err_str)
	{
		push @logs, "ESuds: Failed to lookup device_id for $machine_id and $trans_id: $db_err_str";
		ReRix::Utils::send_alert("ESuds: Failed to lookup device_id for $machine_id and $trans_id: $db_err_str", $command_hashref);
		return(\@logs);
	}
	
	my @device_id_row = $get_device_id_stmt->fetchrow_array();
	if(not defined $device_id_row[0])
	{
		push @logs, "ESuds: Failed to lookup device_id for $machine_id and $trans_id";
		ReRix::Utils::send_alert("ESuds: Failed to lookup device_id for $machine_id and $trans_id", $command_hashref);
		return(\@logs);
	}

	$device_id = $device_id_row[0];
	$get_device_id_stmt->finish();
	
	$db_err_str = undef;

	push @logs, "Stored Device ID  : $device_id";
	
	my @line_items;
	for(my $item_num=0; $item_num<$num_line_items; $item_num++)
	{
		push @logs, "Line Item Num     : $item_num ------------";

		my $chunk = substr($data, ($item_num*6), 6);
		push @logs, "Chunk             : " . unpack("H*", $chunk);
		my $port_num = ord(substr($chunk, 0, 1));
		my $top_bottom = unpack("a", substr($chunk, 1, 1));
		my $item = ord(substr($chunk, 2, 1));
		my $qty = ord(substr($chunk, 3, 1));
		my $amt = unpack("H4", substr($chunk, 4, 2));
		
		my $position = 0;
		if($top_bottom eq 'T')
		{
			$position = 1;
		}
		elsif($top_bottom eq 'U')
		{
			push @logs, "Port $port_num = unknown position!, using bottom.";
			$position = 0;
		}

		$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
		$amt = sprintf("%.02f", ($amt * .01));
		
		$total += $amt;
		
		# construct a description; should look like "Standard Cycle, Dryer on Port 7"
		my $cycle_desc = &get_item_desc($DATABASE, $item);
		if(not defined $cycle_desc)
		{
			push @logs, "Failed to lookup tran_line_item_type for item_id $item";
			ReRix::Utils::send_alert("ESuds: Failed to lookup tran_line_item_type for item_id $item", $command_hashref);
			return(\@logs);	
		}

		my $description = $cycle_desc . ', ';

		my $lookup_host_type_ref = $DATABASE->select(
							table			=> 'host, host_type',
							select_columns	=> 'host_type.host_type_desc, host_type.host_model_cd, host_type_cd, host.host_id, host.host_label_cd',
							where_columns	=> [ 'host.host_type_id = host_type.host_type_id and host.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
							where_values	=> [ $device_id, $port_num, $position ] );
	
		if(not defined $lookup_host_type_ref->[0])
		{
			push @logs, "Failed to find host_type_desc for device $device_id, Port $port_num, Position $position";
			push @logs, "Storing failed transaction and sending positive response...";
			my $resp = &log_exception($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE);
			push @logs, $resp;
			ReRix::Utils::send_alert("ESuds: Failed to find host_type_desc for device $device_id, Port $port_num, Position $position", $command_hashref);
			return(\@logs);	
		}
		
		my $host_type_name = $lookup_host_type_ref->[0][0];
		my $host_model_cd = $lookup_host_type_ref->[0][1];
		my $host_type_cd = $lookup_host_type_ref->[0][2];
		my $host_id = $lookup_host_type_ref->[0][3];
		my $host_label = $lookup_host_type_ref->[0][4];
		
		push @logs, "HostID/Type/Label : $host_id/$host_type_name/$host_label";
		
		if($host_type_cd eq 'S' || $host_type_cd eq 'U')
		{
			if($top_bottom eq 'T')
			{
				$description = $description . 'Top ';
			}
			else
			{
				$description = $description . 'Bottom ';
			}
		}
		
		$description = $description . $host_type_name . " " . $host_label;
		
		push @logs, "Item Port/Pos     : $port_num/$position";
		push @logs, "Item ID           : $item";
		push @logs, "Qty/Amt           : $qty/$amt";
		push @logs, "Top/Bottom        : $top_bottom";
		push @logs, "Description       : $description";

		push(@line_items, [$item_num, $port_num, $position, $amt, $item, $top_bottom, $description, $host_id, $qty]);
		
		### check if we are in free-vend or regular mode
		my $host_cycle_price = &_host_base_cycle_price($DATABASE, $host_id);
		if ($host_cycle_price == -1)	#error
		{
			push @logs, "Unable to determine free-vend mode on/off flag for device $device_id host $host_id line item $item. Skipping cycle-time estimate set/adjust for this line item transaction.";
		}
		elsif ($host_cycle_price > 0)	#regular mode
		{
			# TODO: Adjust the hosts's cycle completion minutes based on the incoming cycle and the 
			# cycle time stored in the host_setting table, or if this host_setting value does not exist,
			# try using the avg cycle time or last cycle time, whichever is most accurate here.
			
			my $host_cycle_time;
			my $item_desc = &get_item_desc($DATABASE, $item);
			my $lookup_cycle_time_ref = $DATABASE->select(
								table			=> 'host_setting',
								select_columns	=> 'host_setting_value',
								where_columns	=> ['host_id = ?', 'HOST_SETTING_PARAMETER = ?'],
								where_values	=> [$host_id, "$item_desc Time"]);

			if(defined $lookup_cycle_time_ref->[0])	#use cycle time parameter as reported by host
			{
				$host_cycle_time = $lookup_cycle_time_ref->[0][0];
				push @logs, "Cycle Time Lookup : Found REPORTED time $host_cycle_time for host $host_id item $item_desc";
			}
			else	#try using avg instead
			{
				# get the average we have been keeping
				my $host_avg_time_ref = $DATABASE->select(
					table			=> 'est_tran_complete_time',
					select_columns	=> 'avg_tran_complete_minut',
					where_columns	=> ['host_id = ?', 'tran_line_item_type_id = ?'],
					where_values	=> [ $host_id, $item]);
								
				if(defined $host_avg_time_ref->[0])
				{
					$host_cycle_time = $host_avg_time_ref->[0][0];
					push @logs, "Cycle Time Lookup : Found ESTIMATED time $host_cycle_time for host $host_id item $item_desc";
				}
			}
			
			if (defined $host_cycle_time)	#fine in the case of undefined, as 0 minutes is set for the estimate on cycle start for regular cycles
			{
				my $cycle_time = $host_cycle_time;
				my $total_cycle_time = ($cycle_time*$qty);
				if($item eq '42')
				{
					if($top_bottom ne 'U')
					{
						# upgrade
						# can't do this update with Evend::Database
						push @logs, "Cycle Time Adjust : Top-Off! Increasing cycle time by $total_cycle_time for host $host_id, $item_desc Time";
						#my $top_off_stmt = $raw_handle->prepare("UPDATE host SET HOST_EST_COMPLETE_MINUT=(HOST_EST_COMPLETE_MINUT+:cycle_time) WHERE host_id = :host_id");
						my $top_off_stmt = $raw_handle->prepare("UPDATE host SET HOST_EST_COMPLETE_MINUT=(case when (host_est_complete_minut+:cycle_time) > 99 then 99 else (host_est_complete_minut+:cycle_time) end) WHERE host_id = :host_id");
						$top_off_stmt->bind_param(":cycle_time", $total_cycle_time);
						$top_off_stmt->bind_param(":cycle_time", $total_cycle_time);
						$top_off_stmt->bind_param(":host_id", $host_id);
						$top_off_stmt->execute();
						my $top_off_err_str = $raw_handle->errstr;
						$top_off_stmt->finish();
						if(defined $top_off_err_str)
						{
							push @logs, "Top-Off Failed    : $top_off_err_str";
						}
					}
					else
					{
						push @logs, "Cycle Time Adjust : Skipping adjust for unknown top-off position";
					}
				}
				else
				{
					# new cycle
					push @logs, "Cycle Time Adjust : Setting cycle time to $total_cycle_time for host $host_id, $item_desc Time";
					$DATABASE->update(	table            => 'host',
										update_columns   => 'HOST_EST_COMPLETE_MINUT',
										update_values    => [$total_cycle_time],
										where_columns    => ['host_id = ?'],
										where_values     => [$host_id]);
				}
			}
			else
			{
				push @logs, "Cycle Time Lookup : Failed to find stored cycle time for host $host_id item $item_desc";
			}
		}
		#else we are in free-vend mode--not adjusting estimate here, at least for now....
	}
	
	push @logs, "Total Sale        : $total";
	
	my $tran_return_code;
	my $error_message;
	my $db_tran_id;
	
	push @logs, "Storing Tran      : $trans_id";
	
	# NOTE:  We need to adjust this timestamp to the timezone where this device is located so
	# it shows up correctly on the reports
	
	my $time_zone;
	
	# Use the device_id as a rerefence to lookup the time zone
	my $lookup_tz_ref =	$DATABASE->select(	
							table          => 'location.location, pos',
							select_columns => 'location.location_time_zone_cd',
							where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
							where_values   => [$device_id] );
							
	$time_zone = $lookup_tz_ref->[0][0];
	if(not defined $time_zone)
	{
		$time_zone = 'EST';
		push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
	}
	else
	{
		push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
	}

	my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
	$Month = $Month+1;
    $Year  = $Year+1900;
    
    my $start_ts = "$Month/$Day/$Year $Hour:$Minute:$Second";
	push @logs, "Device Tran Time  : $start_ts";

	# call a stored proc to store the tran
	$raw_handle = $DATABASE->{handle};
	my $sp_auth_ref = $raw_handle->prepare(q{ BEGIN PKG_PROCESS_TRANSACTION.sp_esuds_batch_auth (:account_id, :device_id, :tran_id, :tran_result, :card_type, :local_flag, to_date(:start_ts, 'MM/DD/YYYY HH24:MI:SS'), to_date(:end_ts, 'MM/DD/YYYY HH24:MI:SS'), :db_tran_id, :return_code, :error_message); END; });
	$sp_auth_ref->bind_param(":account_id", undef);
	$sp_auth_ref->bind_param(":device_id", $device_id);
	$sp_auth_ref->bind_param(":tran_id", $trans_id);
	$sp_auth_ref->bind_param(":tran_result", $tran_result);
	$sp_auth_ref->bind_param(":card_type", undef);
	$sp_auth_ref->bind_param(":local_flag", 'N');
	$sp_auth_ref->bind_param(":start_ts", $start_ts);
	$sp_auth_ref->bind_param(":end_ts", $start_ts);
	$sp_auth_ref->bind_param_inout(":db_tran_id", \$db_tran_id, 20);
	$sp_auth_ref->bind_param_inout(":return_code", \$tran_return_code, 40);
	$sp_auth_ref->bind_param_inout(":error_message", \$error_message, 256);

	push @logs, "Calling Procedure : PKG_PROCESS_TRANSACTION.sp_esuds_batch_auth(undef, $device_id, $trans_id, $tran_result, undef, N, $start_ts, $start_ts)";

	$sp_auth_ref->execute;
	$db_err_str = $raw_handle->errstr;
	$sp_auth_ref->finish();
	
	if(defined $db_err_str)
	{
		push @logs, "DBI errstr        : $db_err_str";
	}
	
	if(defined $error_message)
	{
		push @logs, "Error Message     : $error_message";
	}
	
	push @logs, "Return Code       : $tran_return_code";
	push @logs, "DB Trans ID       : $db_tran_id";
	
	if($tran_return_code ne "0")
	{
		push @logs, "Procedure         : sp_esuds_batch_auth failed! return_code ne 0";
		push @logs, "Storing failed transaction and sending positive response...";
		my $resp = &log_exception($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE);
		push @logs, $resp;
		ReRix::Utils::send_alert("ESuds: Procedure         : sp_esuds_batch_auth failed!\n$error_message\n$db_err_str", $command_hashref);
		return(\@logs);	
	}
	
	if(not defined $db_tran_id)
	{
		push @logs, "Procedure         : sp_esuds_batch_auth failed! db_tran_id is undefined";
		push @logs, "Storing failed transaction and sending positive response...";
		my $resp = &log_exception($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE);
		push @logs, $resp;
		ReRix::Utils::send_alert("ESuds: Procedure         : sp_esuds_batch_auth failed!\n$error_message\n$db_err_str", $command_hashref);
		return(\@logs);	
	}

	my $item_ref;
	
	#       0          1         2        3      4       5              6            7      8
	# [$item_num, $port_num, $position, $amt, $item, $top_bottom, $description, $host_id, $qty]
	
	foreach $item_ref (@line_items)
	{
		my $item_return_code;
		
		push @logs, "Storing Line Item : " . $item_ref->[0] . " -----";
		# call a stored proc to store each line item
		my $sp_batch_ref = $raw_handle->prepare(q{ BEGIN PKG_PROCESS_TRANSACTION.sp_esuds_batch_add_detail (:db_tran_id, :host_id, :item_amount, :item_tax, :item, :description, :qty, :return_code, :error_message); END; });
		$sp_batch_ref->bind_param(":db_tran_id", $db_tran_id);
		$sp_batch_ref->bind_param(":host_id", $item_ref->[7]);
		$sp_batch_ref->bind_param(":item_amount", ($item_ref->[3]/$item_ref->[8]));
		$sp_batch_ref->bind_param(":item_tax", '0');
		$sp_batch_ref->bind_param(":item", $item_ref->[4]);
		$sp_batch_ref->bind_param(":description", $item_ref->[6]);
		$sp_batch_ref->bind_param(":qty", $item_ref->[8]);
		$sp_batch_ref->bind_param_inout(":return_code", \$item_return_code, 40);
		$sp_batch_ref->bind_param_inout(":error_message", \$error_message, 256);
		
		push @logs, "Calling Procedure : PKG_PROCESS_TRANSACTION.sp_esuds_batch_add_detail($db_tran_id, $item_ref->[7], $item_ref->[3], 0, $item_ref->[4], $item_ref->[6], $item_ref->[8])";

		$sp_batch_ref->execute;
		$db_err_str = $raw_handle->errstr;
		$sp_batch_ref->finish();
		
		if(defined $db_err_str)
		{
			push @logs, "DBI errstr        : $db_err_str";
		}
		
		if(defined $error_message)
		{
			push @logs, "Error Message     : $error_message";
		}
		
		push @logs, "Return Code       : $item_return_code";
		
		if($item_return_code ne "0")
		{
			push @logs, "Procedure         : sp_esuds_batch_add_detail failed! ";
			push @logs, "Storing failed transaction and sending positive response...";
			my $resp = &log_exception($message, 'esuds_network_auth_batch', '16', $response_no, $trans_id, $machine_id, $DATABASE);
			push @logs, $resp;
			ReRix::Utils::send_alert("ESuds: Procedure         : sp_esuds_batch_add_detail failed!\n$error_message\n$db_err_str", $command_hashref);
			return(\@logs);	
		}
	}

	my $response = pack("CH2N", $response_no, '71', $trans_id);
	push @logs, "Response          : " . unpack("H*", $response);
	
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$command_hashref->{machine_id}, unpack("H*", $response)]);
							
	return(\@logs);	
}

sub esuds_local_auth_batch
{
	# return logs for info and errors for logging
	my (@logs);
	
	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	
	my $trans_id = unpack("N", substr($message,2,4));
	my $timestamp = unpack("N", substr($message,6,4));
	my $tran_type = unpack("a", substr($message,10,1));
	my $card_length = ord(substr($message,11,1));
	my $auth_data = unpack("a*", substr($message,12,$card_length));
	my $num_line_items = ord(substr($message,(12+$card_length),1));

	my $item_start_pos = (12+$card_length+1);
	
	if($device_type ne '5')
	{
		push @logs, "Huh!?  ESuds::esuds_local_auth_batch received a batch for a non-esuds device!";
		ReRix::Utils::send_alert("ESuds: Huh!?  ESuds::esuds_local_auth_batch received a batch for a non-esuds device!", $command_hashref);
		return(\@logs);
	}

	if($tran_type ne 'S' && $tran_type ne 'M')
	{
		# eSuds should not send a credit card at this time
		# TODO: Add support for credit cards and others
		
		push @logs, "Huh!?  ESuds::esuds_local_auth_batch received a batch for an unsuported card type: $tran_type";
		ReRix::Utils::send_alert("ESuds: Huh!?  ESuds::esuds_local_auth_batch received a batch for an unsuported card type: $tran_type", $command_hashref);
		return(\@logs);
	}

	push @logs, "Trans ID          : $trans_id";
	push @logs, "Num Line Items    : $num_line_items";
	push @logs, "Auth Data         : $auth_data";
	
	if($tran_type eq 'M')
	{
		$auth_data = undef;
		push @logs, "Cash Transaction! : Setting card number to undef";
	}
	
	# BIG FAT NOTE:   Since the room controller sets it's clock using an adjusted time, we don't 
	# need to adjust this timestamp like we do for a network auth
	
	my ($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);
	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	my $time = sprintf("%02d:%02d:%02d", $hour, $min, $sec);
	my $date_time = "$date $time";
	push @logs, "Transaction Time  : $date_time";

	my $db_err_str;
	my $raw_handle;

	# BUG FIX - Lookup and use the device_id of the device as it existed at the time of the transaction (in case port layout changed & transactions occured while network was down)
	$raw_handle = $DATABASE->{handle};
	my $get_device_id_stmt = $raw_handle->prepare(q{ select max(device.device_id) from pos, device where pos.device_id = device.device_id and device.device_name = :ev_number and pos.created_ts < to_date(:tran_ts, 'MM/DD/YYYY HH24:MI:SS') });
	
	if(not defined $get_device_id_stmt)
	{
		push @logs, "ESuds::esuds_local_auth_batch: Failed to prepare get_device_id_stmt statmement";
		ReRix::Utils::send_alert("ESuds::esuds_local_auth_batch: Failed to prepare get_device_id_stmt statmement", $command_hashref);
		return(\@logs);
	} 
	
	$get_device_id_stmt->bind_param(":ev_number", $machine_id);
	$get_device_id_stmt->bind_param(":tran_ts", $date_time);
	$get_device_id_stmt->execute();
	
	$db_err_str = $get_device_id_stmt->errstr;
	if(defined $db_err_str)
	{
		push @logs, "ESuds: Failed to lookup device_id for $machine_id and $date_time: $db_err_str";
		ReRix::Utils::send_alert("ESuds: Failed to lookup device_id for $machine_id and $date_time: $db_err_str", $command_hashref);
		return(\@logs);
	}
	
	my @device_id_row = $get_device_id_stmt->fetchrow_array();
	if(not defined $device_id_row[0])
	{
		push @logs, "ESuds: Failed to lookup device_id for $machine_id and $date_time";
		ReRix::Utils::send_alert("ESuds: Failed to lookup device_id for $machine_id and $date_time", $command_hashref);
		return(\@logs);
	}

	$device_id = $device_id_row[0];
	$get_device_id_stmt->finish();
	
	$db_err_str = undef;

	push @logs, "Stored Device ID  : $device_id";

	my $total = 0;
	my $data = substr($message, $item_start_pos);
	
	# if this device is one that uses a card reader, we need to parse the track2 data and pull out the student ID
	my $student_id;

	# don't need to parse Cash transactions
	if($tran_type eq 'S')
	{
		# lookup the card parser for this device

		my $lookup_parser_ref =	$DATABASE->select(	
							table          => 'device_setting',
							select_columns => 'device_setting.device_setting_value',
							where_columns  => ['device_setting.device_setting_parameter_cd = ?', 'device_setting.device_id = ?'],
							where_values   => ['Card Data Parser', $device_id] );
	
		if(not defined $lookup_parser_ref->[0][0])
		{
			push @logs, "Track2 Parser     : Not found for device $device_id.  Using unmodifed: $auth_data";
			$student_id = $auth_data;
		}
		else
		{
			my $parser_key = $lookup_parser_ref->[0][0];
			$student_id = $track2_parsers{$parser_key}->[0]($DATABASE, $command_hashref, $auth_data);
			
			if(defined $student_id)
			{
				push @logs, "Track2 Parser     : \"$track2_parsers{$parser_key}->[1]\" returned studentID = $student_id";
			}
			else
			{
				push @logs, "Track2 Parser     : \"$track2_parsers{$parser_key}->[1]\" Failed! Using unmodifed: $auth_data";
				$student_id = $auth_data;
			}
		}
	}
	
	my @line_items;
	for(my $item_num=0; $item_num<$num_line_items; $item_num++)
	{
		push @logs, "Line Item Num     : $item_num ------------";

		my $chunk = substr($data, ($item_num*6), 6);
		push @logs, "Chunk             : " . unpack("H*", $chunk);
		my $port_num = ord(substr($chunk, 0, 1));
		my $top_bottom = unpack("a", substr($chunk, 1, 1));
		my $item = ord(substr($chunk, 2, 1));
		my $qty = ord(substr($chunk, 3, 1));
		my $amt = unpack("H4", substr($chunk, 4, 2));
		
		my $position = 0;
		if($top_bottom eq 'T')
		{
			$position = 1;
		}
		elsif($top_bottom eq 'U')
		{
			push @logs, "Port $port_num = unknown position!, using bottom.";
			$position = 0;
		}

		$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
		$amt = sprintf("%.02f", ($amt * .01));
		
		$total += $amt;
		
		# construct a description; should look like "Standard Cycle, Dryer on Port 7"
		
		my $cycle_desc = &get_item_desc($DATABASE, $item);
		if(not defined $cycle_desc)
		{
			push @logs, "Failed to lookup tran_line_item_type for item_id $item";
			ReRix::Utils::send_alert("ESuds: Failed to lookup tran_line_item_type for item_id $item", $command_hashref);
			return(\@logs);	
		}

		my $description = $cycle_desc . ', ';
		my $lookup_host_type_ref = $DATABASE->select(
							table			=> 'host, host_type',
							select_columns	=> 'host_type.host_type_desc, host_type.host_model_cd, host_type_cd, host.host_id, host.host_label_cd',
							where_columns	=> [ 'host.host_type_id = host_type.host_type_id and host.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
							where_values	=> [ $device_id, $port_num, $position ] );
	
		if(not defined $lookup_host_type_ref->[0])
		{
			push @logs, "Failed to find host_type_desc for device $device_id, Port $port_num, Position $position";
			push @logs, "Storing failed transaction and sending positive response...";
			my $resp = &log_exception($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE);
			push @logs, $resp;
			ReRix::Utils::send_alert("ESuds: Failed to find host_type_desc for device $device_id, Port $port_num, Position $position", $command_hashref);
			return(\@logs);	
		}
		
		my $host_type_name = $lookup_host_type_ref->[0][0];
		my $host_model_cd = $lookup_host_type_ref->[0][1];
		my $host_type_cd = $lookup_host_type_ref->[0][2];
		my $host_id = $lookup_host_type_ref->[0][3];
		my $host_label = $lookup_host_type_ref->[0][4];
		
		push @logs, "HostID/Type/Label : $host_id/$host_type_name/$host_label";

		if($host_type_cd eq 'S' || $host_type_cd eq 'U')
		{
			if($top_bottom eq 'T')
			{
				$description = $description . 'Top ';
			}
			else
			{
				$description = $description . 'Bottom ';
			}
		}
		
		$description = $description . $host_type_name . " " . $host_label;
		
		push @logs, "Item Port/Pos     : $port_num/$position";
		push @logs, "Item ID           : $item";
		push @logs, "Qty/Amt           : $qty/$amt";
		push @logs, "Top/Bottom        : $top_bottom";
		push @logs, "Description       : $description";

		push(@line_items, [$item_num, $port_num, $position, $amt, $item, $top_bottom, $description, $host_id, $qty]);

		### check if we are in free-vend or regular mode
		my $host_cycle_price = &_host_base_cycle_price($DATABASE, $host_id);
		if ($host_cycle_price == -1)	#error
		{
			push @logs, "Unable to determine free-vend mode on/off flag for device $device_id host $host_id line item $item. Skipping cycle-time estimate set/adjust for this line item transaction.";
		}
		elsif ($host_cycle_price > 0)	#regular mode
		{
			# TODO: Adjust the hosts's cycle completion minutes based on the incoming cycle and the 
			# cycle time stored in the host_setting table, or if this host_setting value does not exist,
			# try using the avg cycle time or last cycle time, whichever is most accurate here.
			
			my $host_cycle_time;
			my $item_desc = &get_item_desc($DATABASE, $item);
			my $lookup_cycle_time_ref = $DATABASE->select(
								table			=> 'host_setting',
								select_columns	=> 'host_setting_value',
								where_columns	=> ['host_id = ?', 'HOST_SETTING_PARAMETER = ?'],
								where_values	=> [$host_id, "$item_desc Time"]);

			if(defined $lookup_cycle_time_ref->[0])	#use cycle time parameter as reported by host
			{
				$host_cycle_time = $lookup_cycle_time_ref->[0][0];
				push @logs, "Cycle Time Lookup : Found REPORTED time $host_cycle_time for host $host_id item $item_desc";
			}
			else	#try using avg instead
			{
				# get the average we have been keeping
				my $host_avg_time_ref = $DATABASE->select(
					table			=> 'est_tran_complete_time',
					select_columns	=> 'avg_tran_complete_minut',
					where_columns	=> ['host_id = ?', 'tran_line_item_type_id = ?'],
					where_values	=> [ $host_id, $item]);
								
				if(defined $host_avg_time_ref->[0])
				{
					$host_cycle_time = $host_avg_time_ref->[0][0];
					push @logs, "Cycle Time Lookup : Found ESTIMATED time $host_cycle_time for host $host_id item $item_desc";
				}
			}
			
			if (defined $host_cycle_time)	#fine in the case of undefined, as 0 minutes is set for the estimate on cycle start for regular cycles
			{
				my $cycle_time = $host_cycle_time;
				my $total_cycle_time = ($cycle_time*$qty);
				if($item eq '42')
				{
					if($top_bottom ne 'U')
					{
						# upgrade
						# can't do this update with Evend::Database
						push @logs, "Cycle Time Adjust : Top-Off! Increasing cycle time by $total_cycle_time for host $host_id, $item_desc Time";
						#my $top_off_stmt = $raw_handle->prepare("UPDATE host SET HOST_EST_COMPLETE_MINUT=(HOST_EST_COMPLETE_MINUT+:cycle_time) WHERE host_id = :host_id");
						my $top_off_stmt = $raw_handle->prepare("UPDATE host SET HOST_EST_COMPLETE_MINUT=(case when (host_est_complete_minut+:cycle_time) > 99 then 99 else (host_est_complete_minut+:cycle_time) end) WHERE host_id = :host_id");
						$top_off_stmt->bind_param(":cycle_time", $total_cycle_time);
						$top_off_stmt->bind_param(":cycle_time", $total_cycle_time);
						$top_off_stmt->bind_param(":host_id", $host_id);
						$top_off_stmt->execute();
						my $top_off_err_str = $raw_handle->errstr;
						$top_off_stmt->finish();
						if(defined $top_off_err_str)
						{
							push @logs, "Top-Off Failed    : $top_off_err_str";
						}
					}
					else
					{
						push @logs, "Cycle Time Adjust : Skipping adjust for unknown top-off position";
					}
				}
				else
				{
					# new cycle
					push @logs, "Cycle Time Adjust : Setting cycle time to $total_cycle_time for host $host_id, $item_desc Time";
					$DATABASE->update(	table            => 'host',
										update_columns   => 'HOST_EST_COMPLETE_MINUT',
										update_values    => [$total_cycle_time],
										where_columns    => ['host_id = ?'],
										where_values     => [$host_id]);
				}
			}
			else
			{
				push @logs, "Cycle Time Lookup : Failed to find stored cycle time for host $host_id item $item_desc";
			}
		}
		#else we are in free-vend mode--not adjusting estimate here, at least for now....
	}
	
	push @logs, "Total Sale        : $total";
	
	my $tran_return_code;
	my $error_message;
	my $db_tran_id;
	
	push @logs, "Storing Tran      : $trans_id";

	# call a stored proc to store the tran
	$raw_handle = $DATABASE->{handle};
	my $sp_auth_ref = $raw_handle->prepare(q{ BEGIN PKG_PROCESS_TRANSACTION.sp_esuds_batch_auth (:card_number, :device_id, :tran_id, :tran_result, :tran_type, :local_flag, to_date(:start_ts, 'MM/DD/YYYY HH24:MI:SS'), to_date(:end_ts, 'MM/DD/YYYY HH24:MI:SS'), :db_tran_id, :return_code, :error_message); END; });
	$sp_auth_ref->bind_param(":card_number", $student_id); # should be undef for cash trans
	$sp_auth_ref->bind_param(":device_id", $device_id);
	$sp_auth_ref->bind_param(":tran_id", $trans_id);
	$sp_auth_ref->bind_param(":tran_result", undef);	# undef should default to batch success
	$sp_auth_ref->bind_param(":tran_type", $tran_type);
	$sp_auth_ref->bind_param(":local_flag", 'Y');
	$sp_auth_ref->bind_param(":start_ts", $date_time);
	$sp_auth_ref->bind_param(":end_ts", $date_time);
	$sp_auth_ref->bind_param_inout(":db_tran_id", \$db_tran_id, 20);
	$sp_auth_ref->bind_param_inout(":return_code", \$tran_return_code, 40);
	$sp_auth_ref->bind_param_inout(":error_message", \$error_message, 256);

	push @logs, "Calling Procedure : PKG_PROCESS_TRANSACTION.sp_esuds_batch_auth($student_id, $device_id, $trans_id, undef, $tran_type, Y, $date_time, $date_time)";

	$sp_auth_ref->execute;
	$db_err_str = $raw_handle->errstr;
	$sp_auth_ref->finish();
	
	if(defined $db_err_str)
	{
		push @logs, "DBI errstr        : $db_err_str";
	}
	
	if(defined $error_message)
	{
		push @logs, "Error Message     : $error_message";
	}
	
	push @logs, "Return Code       : $tran_return_code";
	push @logs, "DB Trans ID       : $db_tran_id";

	if($tran_return_code ne "0")
	{
		push @logs, "Procedure         : sp_esuds_batch_auth failed! return_code ne 0";
		push @logs, "Storing failed transaction and sending positive response...";
		my $resp = &log_exception($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE);
		push @logs, $resp;
		ReRix::Utils::send_alert("ESuds: Procedure        : sp_esuds_batch_auth failed!\n$error_message\n$db_err_str", $command_hashref);
		return(\@logs);	
	}
	
	if(not defined $db_tran_id)
	{
		push @logs, "Procedure         : sp_esuds_batch_auth failed! db_tran_id is undefined";
		push @logs, "Storing failed transaction and sending positive response...";
		my $resp = &log_exception($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE);
		push @logs, $resp;
		ReRix::Utils::send_alert("ESuds: Procedure        : sp_esuds_batch_auth failed!\n$error_message\n$db_err_str", $command_hashref);
		return(\@logs);	
	}

	#        0          1       2        3      4       5            6             7        8
	# [$item_num, $port_num, $position, $amt, $item, $top_bottom, $description, $host_id, $qty]
	
	foreach my $item_ref (@line_items)
	{
		my $item_return_code;
		
		push @logs, "Storing Line Item : " . $item_ref->[0];
		# call a stored proc to store each line item
		my $sp_batch_ref = $raw_handle->prepare(q{ BEGIN PKG_PROCESS_TRANSACTION.sp_esuds_batch_add_detail (:db_tran_id, :host_id, :item_amount, :item_tax, :item, :description, :qty, :return_code, :error_message); END; });
		$sp_batch_ref->bind_param(":db_tran_id", $db_tran_id);
		$sp_batch_ref->bind_param(":host_id", $item_ref->[7]);
		$sp_batch_ref->bind_param(":item_amount", ($item_ref->[3]/$item_ref->[8]));
		$sp_batch_ref->bind_param(":item_tax", 0);
		$sp_batch_ref->bind_param(":item", $item_ref->[4]);
		$sp_batch_ref->bind_param(":description", $item_ref->[6]);
		$sp_batch_ref->bind_param(":qty", $item_ref->[8]);
		$sp_batch_ref->bind_param_inout(":return_code", \$item_return_code, 40);
		$sp_batch_ref->bind_param_inout(":error_message", \$error_message, 256);
		
		push @logs, "Calling Procedure : PKG_PROCESS_TRANSACTION.sp_esuds_batch_add_detail($db_tran_id, $item_ref->[7], $item_ref->[3], 0, $item_ref->[4], $item_ref->[6], $item_ref->[8])";

		$sp_batch_ref->execute;
		$db_err_str = $raw_handle->errstr;
		$sp_batch_ref->finish();
		
		if(defined $db_err_str)
		{
			push @logs, "DBI errstr        : $db_err_str";
		}
		
		if(defined $error_message)
		{
			push @logs, "Error Message     : $error_message";
		}
		
		push @logs, "Return Code       : $item_return_code";

		if($item_return_code ne "0")
		{
			push @logs, "Procedure         : sp_esuds_batch_add_detail failed! ";
			push @logs, "Storing failed transaction and sending positive response...";
			my $resp = &log_exception($message, 'esuds_network_auth_batch', 16, $response_no, $trans_id, $machine_id, $DATABASE);
			push @logs, $resp;
			ReRix::Utils::send_alert("ESuds: Procedure         : sp_esuds_batch_add_detail failed!\n$error_message\n$db_err_str", $command_hashref);
			return(\@logs);	
		}
	}

	my $response = pack("CH2N", $response_no, '71', $trans_id);
	push @logs, "Response          : " . unpack("H*", $response);
	
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$command_hashref->{machine_id}, unpack("H*", $response)]);
	
	return(\@logs);	
}

sub get_item_desc
{
	my ($DATABASE, $item_id) = @_;
	my $lookup_item_desc_ref = $DATABASE->select(
						table			=> 'tran_line_item_type',
						select_columns	=> 'tran_line_item_type_desc',
						where_columns	=> [ 'tran_line_item_type_id = ?' ],
						where_values	=> [ $item_id ] );

	if(not defined $lookup_item_desc_ref->[0])
	{
		return undef;
	}
	else
	{
		return $lookup_item_desc_ref->[0][0];
	}
}

sub get_host_type_name
{
	my ($host_type_cd) = shift;
	my $name;
	if($host_type_cd eq 'W')
	{
		$name = 'Washer';
	}
	elsif($host_type_cd eq 'D')
	{
		$name = 'Dryer';
	}
	elsif($host_type_cd eq 'S')
	{
		$name = 'Stacked Dryer';
	}
	elsif($host_type_cd eq 'U')
	{
		$name = 'Stacked Washer/Dryer';
	}
	else
	{
		return undef;
	}
	return $name;
}

sub send_cycle_finished_email
{
	my ($device_id, $host_id, $machine, $DATABASE) = @_;
	
	my $max_hours_since_upload_for_alert = 3;
	my $upload_lag_adjust_minutes = 5;
	
	### check if we are in free-vend or regular mode
	my (@to_email_addrs, $location_name);
	my $raw_handle = $DATABASE->{handle};
	my $subject = "eSuds.net Cycle Complete";
	my $from_addr = 'admin@esuds.net';
	my $from_name = 'eSuds.net Administrator';
	my $host_cycle_price = &_host_base_cycle_price($DATABASE, $host_id);
	if ($host_cycle_price == -1)	#error
	{
		return "Unable to determine free-vend mode on/off flag for device $device_id host $host_id. No emails sent.";
	}
	elsif ($host_cycle_price == 0)	#free-vend mode
	{
		### lookup the location
		my $stmt = $raw_handle->prepare(q{ 
			select 	location.location_name 
			from 	location.location, 
					pos
			where 	location.location_id = pos.location_id
			and 	pos.device_id = :device_id
		});

		if(not defined $stmt)
		{
			ReRix::Utils::send_alert("ESuds: send_cycle_finished_email: Failed to prepare SQL statement!");
			return "Failed to prepare SQL statement! (.".$raw_handle->errstr.")";
		}

		$stmt->bind_param(":device_id", $device_id);
		$stmt->execute();

		my $db_err_str = $raw_handle->errstr;
		if(defined $db_err_str)
		{
			ReRix::Utils::send_alert("ESuds: send_cycle_finished_email: SQL execution failed: $db_err_str");
			return $db_err_str;
		}

		my @row = $stmt->fetchrow_array();
		$stmt->finish();

		$location_name = $row[0];
	}
	else	#regular mode
	{
		# lookup the email address
		#my $stmt = $raw_handle->prepare(q{ select tran.tran_id, consumer.consumer_email_addr2, consumer.consumer_fname, consumer.consumer_lname, location.location_name from consumer, consumer_acct, tran, pos, location.location where tran.consumer_acct_id = consumer_acct.consumer_acct_id and consumer_acct.consumer_id = consumer.consumer_id and tran.pos_id = pos.pos_id and pos.location_id = location.location_id and tran.tran_start_ts > (sysdate-(:max_hours/24)) and tran.tran_id = (select max(tran.tran_id) from tran, tran_line_item, host where tran.tran_id = tran_line_item.tran_id and tran_line_item.host_id = host.host_id and tran.tran_upload_ts > host.host_last_start_ts and host.host_id = :host_id) });
		my $stmt = $raw_handle->prepare(q{ select tran.tran_id, consumer.consumer_email_addr2, consumer.consumer_fname, consumer.consumer_lname, location.location_name, tran.tran_start_ts, tran.tran_upload_ts, host.host_last_start_ts from consumer, consumer_acct, tran, pos, location.location, tran_line_item, host where tran.consumer_acct_id = consumer_acct.consumer_acct_id and consumer_acct.consumer_id = consumer.consumer_id and tran.tran_id = tran_line_item.tran_id and tran_line_item.host_id = host.host_id and tran.pos_id = pos.pos_id and pos.location_id = location.location_id and tran.tran_start_ts > (sysdate-(:max_hours/24)) and tran.tran_upload_ts > (host.host_last_start_ts-(:tran_upload_lag_adjust/1440)) and tran_line_item.tran_line_item_id = (select max(tran_line_item.tran_line_item_id) from tran_line_item where tran_line_item.host_id = :host_id) });

		if(not defined $stmt)
		{
			ReRix::Utils::send_alert("ESuds: send_cycle_finished_email: Failed to prepare SQL statement!");
			return "Failed to prepare SQL statement!  (".$raw_handle->errstr.")";
		}

		$stmt->bind_param(":max_hours", $max_hours_since_upload_for_alert);
		$stmt->bind_param(":tran_upload_lag_adjust", $upload_lag_adjust_minutes);
		$stmt->bind_param(":host_id", $host_id);
		$stmt->execute();

		my $db_err_str = $raw_handle->errstr;
		if(defined $db_err_str)
		{
			ReRix::Utils::send_alert("ESuds: send_cycle_finished_email: SQL execution failed: $db_err_str");
			return $db_err_str;
		}

		my @row = $stmt->fetchrow_array();
		$stmt->finish();

		my $to_addr = $row[1];
		my $first_name = $row[2];
		my $last_name = $row[3];
		$location_name = $row[4];

		### process e-mail for user that started the transaction (unless if in monitor mode, then there is no e-mail defined)
		my $message = "Hi $first_name,\n\nYour laundry is finished. Please pick up your laundry from $machine in $location_name.\n\nThank you!";
		my $to_name = "$first_name $last_name";
		if(defined $to_addr)
		{
			$DATABASE->insert(	table=> 'ob_email_queue',
								insert_columns=> 'OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG',
								insert_values=> [$from_addr, $from_name, $to_addr,  $to_name, $subject, $message]);
			push @to_email_addrs, $to_addr;
		}
		else
		{
			#ReRix::Utils::send_alert("ESuds: send_cycle_finished_email: No email address found for device $device_id");
		}
	}
	
	# this should get done regardless of free-vend mode or not, although it most likely won't get used
	# in regular mode because the website won't support it, although it may if the customer wants it
	
	### lookup email addresses of other users who have requested cycle complete status
	my $get_emails_ref = $DATABASE->select(
		table			=> 'host_status_notif_queue',
		select_columns	=> 'host_status_notif_queue_id, host_status_notif_q_email_addr',
		where_columns	=> [ 'host_id = ?', 'host_status_notif_type_id = ?' ],
		where_values	=> [ $host_id, 2 ]	#2=eSuds Laundry Machine Cycle Completed Notification
	);
	
	if(defined $get_emails_ref->[0])
	{
		my $message = "Hi!\n\nThis is the notification you requested from $machine in $location_name.\n\nThank you!";
		foreach my $row_aref (@{$get_emails_ref})
		{
			my ($host_status_notif_queue_id, $host_status_notif_q_email_addr) = @{$row_aref};
			if (defined $host_status_notif_queue_id)
			{
				unless ($host_status_notif_q_email_addr eq '')
				{
					push @to_email_addrs, $host_status_notif_q_email_addr;

					### send e-mail
					$DATABASE->insert(
						table			=> 'ob_email_queue'
						,insert_columns	=> 'OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG'
						,insert_values	=> [$from_addr, $from_name, $host_status_notif_q_email_addr, $host_status_notif_q_email_addr, $subject, $message]
					);
				}

				### delete from queue
				my $stmt = $raw_handle->prepare('delete from host_status_notif_queue where host_status_notif_queue_id = :host_status_notif_queue_id');
				$stmt->bind_param(":host_status_notif_queue_id", $host_status_notif_queue_id);
				$stmt->execute();
				$stmt->finish();
			}
		}
	}
	
	if (scalar @to_email_addrs > 0)
	{
		return "Inserted emails to ".join(',', @to_email_addrs);
	}
	else
	{
		return "No emails sent for device $device_id";
	}
}

sub log_exception
{
	my ($rerix_command, $function_name, $error_code, $response_no, $trans_id, $machine_id, $DATABASE) = @_;
	
	#my $server_name = `uname -n`;
	my $server_name = 'usaapd01';

	my $raw_handle = $DATABASE->{handle};
	my $sp_log_exception_ref = $raw_handle->prepare(q{ BEGIN pkg_exception_processor.sp_log_exception(:error_code, :rerix_cmd, :server_name, :function_name); END; });
	
	warn "sp_log_exception_ref = $sp_log_exception_ref\n";
	
	$sp_log_exception_ref->bind_param(":error_code", $error_code);
	$sp_log_exception_ref->bind_param(":rerix_cmd", unpack("H*",$rerix_command));
	$sp_log_exception_ref->bind_param(":server_name", $server_name);
	$sp_log_exception_ref->bind_param(":function_name", $function_name);
	
	$sp_log_exception_ref->execute;
	my $db_err_str = $raw_handle->errstr;
	$sp_log_exception_ref->finish();
	
	if(defined $db_err_str)
	{
		warn "log_exception: $db_err_str\n";
	}
		
	my $response = pack("CH2N", $response_no, '71', $trans_id);

	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*", $response)]);
						
	return "Response          : " . unpack("H*", $response);
}	

# This procedure will create a pending Server to Client request for Room Layout.  We'll do 
# this a case where we receive host information that doesn't look consistent with the 
# layout that's on the server.
sub request_room_layout
{
	my @logs;
	my ($DATABASE, $machine_id) = @_;
	
	# first check if a pending request is already queued
	my $lookup_pending_ref = $DATABASE->select(
						table			=> 'machine_command_pending',
						select_columns	=> 'count(1)',
						where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?', 'execute_cd in (?,?)'],
						where_values	=> [$machine_id, '83', '0C', 'P', 'S']);
		
	if(not defined $lookup_pending_ref->[0])
	{
		push @logs, "Room Layout Req   : Failed to check for pending request for $machine_id in sub request_room_layout";
		return (\@logs);
	}
	
	if($lookup_pending_ref->[0][0] eq '0')
	{
		push @logs, "Room Layout Req   : Queuing for $machine_id";
		$DATABASE->insert(	table=> 'machine_command_pending',
						insert_columns=> 'machine_id, data_type, command, execute_cd, execute_order',
						insert_values=> [$machine_id, '83', '0C', 'P', 1]);
	}
	else
	{
		push @logs, "Room Layout Req   : Queued request already exists for $machine_id";
	}

	return (\@logs);
}

########################################################################
sub _host_base_cycle_price ($$) {
#Purpose:	Return base (default required cycle) host cycle price from host_setting table depending on the type of host (washer or dryer).
#			If return = 0, then host is in free-vend mode.
#			If return > 0, then host is in 
#
#Pre:		-
#Requires:	hashref: db handle
#			scalar: valid value in host.host_id
#Returns: 	success: non-negative price of base host cycle; error: -1
#Post:		-
	my ($DATABASE, $host_id) = @_;
	
	### note: this is a temporary solution to insure we can reliably determine free-vend or regular cycle mode
	#	'D'	=> 40	#dryer
	#	'S'	=> 40	#stacked dryer
	#	'U'	=> 1	#stacked washer/washer
	#	'W'	=> 1	#washer
	my $host_price_ref = $DATABASE->select(
		table			=> 'host_setting'
		,select_columns	=> 'host_setting_value'
		,where_columns	=> [
			'host_id = ?'
			,"host_setting_parameter = ( 
				select 	tran_line_item_type_desc || ' Price'
				from	tran_line_item_type
				where 	tran_line_item_type_id = (
					select	case ht.host_type_cd
						when 'D'	then 40
						when 'S'	then 40
						when 'U'	then 1
						when 'W'	then 1
					end
					from	host, host_type ht
					where	host.host_type_id = ht.host_type_id
					and		host.host_id = ?
				)
			)"
		]
		,where_values	=> [ $host_id, $host_id ]
	);
	if (not defined $host_price_ref->[0])
	{
		return -1;	#error: price could not be queried
	}
	
	return $host_price_ref->[0]->[0];
}

sub get_host_base_cycle_type_id
{
	my ($host_model_cd) = @_;
	
	#	'D'	=> 40	#dryer
	#	'S'	=> 40	#stacked dryer
	#	'U'	=> 1	#stacked washer/washer
	#	'W'	=> 1	#washer
	
	if($host_model_cd eq 'D')
	{
		return 40;
	}
	elsif($host_model_cd eq 'S')
	{
		return 40;
	}
	elsif($host_model_cd eq 'U')
	{
		return 1;
	}
	elsif($host_model_cd eq 'W')
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

1;
