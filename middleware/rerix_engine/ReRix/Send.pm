use strict;

package ReRix::Send;

use Evend::ReRix::Shared;

sub Time
{
	my ($DATABASE, $machine_id, $start, $freq, $cur_time) = @_;

	if( not defined $cur_time )
	{
		$cur_time = time;
	}

	#send the message
	my $message = &encode_T($cur_time, $start, $freq);
	$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, Command',
				insert_values	=> [$machine_id, &MakeHex($message)]
			);

	$message = &encode_x28($start, $freq);
	$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, Command',
				insert_values	=> [$machine_id, &MakeHex($message)]
			);
}

sub encode_T
{
	my ($time, $comtime, $frequency)=@_;

	my $message = 'T';

	my ($sec,$min,$hour) = localtime($time);
	$min += 60 * $hour;
	$message .= chr (($min & 0xff00) >> 8) . chr($min & 0xff);
	$message .= chr($comtime) . chr($frequency);
}   

sub encode_x28
{
	my ($comtime, $frequency)=@_;

	my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) =
																localtime;

	my $time;

	# calculate EDT time in seconds since epoc(unix time on EDT)
	if( $isdst )
	{
		# It's Daylight Savings Time, EDT is GMT-4

		$time = time() - 60*60*4;
	}
	else
	{
		# It's Not Daylight Savings Time, EDT is GMT-5

		$time = time() - 60*60*5;
	}

	pack("CVCC", 0x28, $time, $comtime, $frequency);
}

sub sendPrices
{
	my ($DATABASE, $machine_id) = @_;

	# Get Prices
	my $array_ref = $DATABASE->query(
							query	=> "select a.price_change_number " .
										"from rerix_pricing_master a " .
										"where price_date = " .
										"(select max(price_date) " .
										"from rerix_pricing_master b " .
										"where b.machine_id = a.machine_id) " .
										"and a.machine_id = ?",
					values	=> [$machine_id],
					);

	my $pcnumber=0;

	if ( defined $array_ref->[0][0] )
	{
		$pcnumber = $array_ref->[0][0];
	}


	if ($pcnumber != 0)
	{
		$array_ref = $DATABASE->select(
								table			=> 'rerix_pricing_master A, ' .
													'TABLE(a.rerix_price) B',
								select_columns	=> 'b.button_id, b.column_id, '.
													'b.price',
								where_columns	=> ['a.machine_id = ?',
													'a.price_change_number =?'],
								where_values	=> [$machine_id, $pcnumber ]
						);
	}
	else
	{
		# If there was no pending price push(prices haven't changed),
		# generate a price push of the currect prices PC = 0

		$array_ref = $DATABASE->select(
								table			=> 'rerix_pricing A, ' .
													'TABLE(a.rerix_price) B',
								select_columns	=> 'b.button_id, b.column_id, '.
													'b.price',
								where_columns	=> ['a.machine_id = ?'],
								where_values	=> [$machine_id ]
						);
	}

	my %pricelist;
	my $complex = 0;
	my ($but, $column, $price);

	foreach my $row (@$array_ref)
	{
		($but, $column, $price) = @$row;

		if (($price % 5 != 0) or ($price >1100))
		{
			$complex = 1;
		}
		if (not defined $pricelist{$but})
		{
			$pricelist{$but} = [$price, $column];
		}
		else
		{
			push @{$pricelist{$but}}, $column;
		}
	}

	foreach (sort {($a=~/[A-Z]/)&&($b=~/[A-Z]/)?$a cmp $b : $a <=> $b}
					keys %pricelist)
	{
		my @list = @{$pricelist{$_}};
		$price = shift @list;
	}

	# Ok, pricelist now looks like a hash by button of ($price, $col1, $col2...)
	#check if we can use set price simple
	if (not $complex)
	{
		foreach (keys %pricelist)
		{
			if (@{$pricelist{$_}} > 3)  # 3 = price + 2 columns
			{
				$complex = 1;
				last;
			}
		}
	}

	my $message = '';
	if ($complex)
	{
		# Can't use simple
		foreach (sort {($a=~/[A-Z]/)&&($b=~/[A-Z]/)?$a cmp $b : $a <=> $b}
							keys %pricelist)
		{
			my (@list) = @{$pricelist{$_}};
			my $price = shift @list;
			$price = $price;
			$message.=chr(($price & 0xff00) >> 8) . chr($price & 0xff);
			while (@list)
			{
				my $col = shift @list;
				if (@list)
				{
					$col &= 0x7f;
				}
				else
				{
					$col |= 0x80;
				}
				$message.=chr($col);
			}

		}

		$message.=chr($pcnumber & 0xff);

		my $serial_ref = $DATABASE->select(
								table			=> 'Rerix_modem_to_serial',
								select_columns	=> 'modem_id',
								where_columns	=> ['machine_id = ?'],
								where_values	=> [$machine_id]
							);

		$message = &PackEncrypted($message, $serial_ref->[0][0], $DATABASE);

		$message = 'p' . $message;
	}
	else
	{
		# use simple
		$message = '';
		foreach (sort {($a=~/[A-Z]/)&&($b=~/[A-Z]/)?$a cmp $b : $a <=> $b}
								keys %pricelist)
		{
			# assuming that all buttons are represented
			my (@list) = @{$pricelist{$_}};
			my $price = shift @list;
			$price = $price/5;
			$message .= chr($price & 0xff);
			my $col1 = shift @list;
			my $col2 = shift @list;

			if (not defined $col2)
			{
				$col2=0;
			}
			$message .= chr($col1 | ($col2 <<4));
		}
		$message .= chr($pcnumber & 0xff);

		my $serial_ref = $DATABASE->select(
									table			=> 'Rerix_modem_to_serial',
									select_columns	=> 'modem_id',
									where_columns	=> ['machine_id = ?'],
									where_values	=> [$machine_id]
								);

		$message = &PackEncrypted($message, $serial_ref->[0][0], $DATABASE);

		$message = 'P' . $message;
	}

	$DATABASE->insert(
						table			=> 'Machine_Command',
						insert_columns	=> 'Modem_ID, Command',
						insert_values	=> [$machine_id,  &MakeHex($message)]
					);

	if( $pcnumber != 0 )
	{
		$DATABASE->update (
						table			=> 'rerix_pricing_master',
						update_columns	=> 'PRICE_CHANGE_SENT',
						update_values	=> ['Y'],
						where_columns	=> ['price_change_number = ?',
											'machine_id = ?'],
						where_values	=> [$pcnumber, $machine_id]
					);
	}

}

1;
