#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 2.00		01/08/2003	T Shannon	Addition of 2.0 commands:
#									[5Eh] - Authorization Request (with amount)
#									[60h] - Authorization Response (the Sony [60h] command was obsoleted so we reused this command number)
#									[2Ah] - Network Authorization Batch 
#									[2Bh] - Local Authorization Batch
#									[71h] - Batch Ack (Server to Client)
#
# 2.01		09/03/03	P Cowan		Added hook for eSuds Authorizations
#
# 3.0		11/03/03	P Cowan		Addition of BCD and 3.0 Commands
#									[93h] - Local Authorization Batch 2.0 (BCD Time Format)
#									[94h] - Cash Sale Detail 2.0 (BCD Time Format)
#
# 3.1		12/22/03	P Cowan		Code cleanup; 
#										- using response_no, removed pointless database queries for msg_no
#										- added conditionals for unpacking and packing trans ID because
#										  endian-ness varies between clients
#
# 3.2		01/19/04	T Shannon	Added auth command [BAh] that can return 'Server Not Responding' code to client
#
# 3.3		01/28/04	P Cowan		- Deprecated parse_cash_sale [5Bh] - wasn't being actively used
#									- Added/improved duplicate checking for batch & cash commands
#
# 3.4		02/03/04	P Cowan		A bug was found in the BCD time transmitted by new G4s.  Month was being
#									inserted into the seconds spot instead of the seconds.  This makes the 
#									seconds on the transaction timestamp for 94h and 94h commands useless.  
#									The bug has been fixed in the client, but to compensate for this I am going 
#									to substitute the current time seconds.  This will keep transactions from 
#									being filtered out as duplicates in other systems.
#
# 3.5		02/04/04	P Cowan		Changed export portions to export $0 value transactions.
#
# 3.6		03/02/04	P Cowan		Changed duplicate checks for G4 to use batch transmission time to determine 
#									duplicates.  This is needed because of a G4 bug that transmits duplicate
#									transaction id's when multiple transactions occur in the same minute.
#
# 3.7		03/08/04	P Cowan		Changed G4Export conditional to prevent exporting cancelled or failed transactions
#
# 3.8		03/26/04	P Cowan		Added transaction timestamp checking to the 2 BCD commands.  Invalid timestamps
#									are causing an error and the terminal can't complete a session.
#
# 4.0		05/18/04	P Cowan		- Improved timestamp check to prevent crashes when time = 0
#									- Added code to handle different device types - G4/G5/LegacyG4/Transact
#									- Added code to prevent processing error transaction and to export them
#
# 4.1		06/02/04	P Cowan		Changed invalid timestamp handling to use the current time if a time part
#									is invalid, instead of throwing away the transaction.
#
# 4.2		08/02/04	P Cowan		Added handling for MEI Telemeter device type
#
# 4.3		08/06/04	P Cowan		Cleanup of Vended Items Description Block parsing.  Fixing a bug where the
#									the Amount value and sum/column was not being calculated correctly.
#

use strict;

package ReRix::Trans20;

use Evend::ReRix::Shared;
use ReRix::Send;
use ReRix::AuthClient;
use Evend::CardUtils;
use ReRix::G4Export;
use DateTime;
use ReRix::CallInRecord;

sub parse_auth_v2
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};
	
	# if it's an eSuds device, break off and do my own thing
	if($device_type eq '5')
	{
		@logs = @{ReRix::ESuds::authorize($DATABASE, $command_hashref)};
		return(\@logs);
	}

	my $array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'modem_id, network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $machine_id ] );

	if(not $array_ref->[0])
	{
		push @logs, "Cannot find network and modem_id in rerix_modem_to_serial for machine_id $machine_id";
		return(\@logs);
	}

	my $modem_id = $array_ref->[0][0];
	my $network = $array_ref->[0][1];

	my ($trans_id, $card_type, $auth_amt, $card);
	if($device_type =~ m/0|1|6|9|10/)
	{
		# G4 sends the trans ID in big-endian order
		(undef, $trans_id, $card_type, $auth_amt, $card) = unpack("CNaH6A*", $message);
		push @logs, "G4 Generated ID   : $trans_id";
	}
	else
	{
		# sony,etc sends the trans ID in little-endian order
		(undef, $trans_id, $card_type, $auth_amt, $card) = unpack("CVaH6A*", $message);
		push @logs, "Generated ID     : $trans_id";
	}

	# }:-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
	$auth_amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $auth_amt)))));
	$auth_amt = sprintf("%.02f", ($auth_amt * .01));

	my $card_number;
	if($card_type =~ m/S|C/)
	{
		my $cr = Evend::CardUtils->new();
		$card =  $cr->cleanSwipeData($card);

		if( substr($card, 0, 1) eq '%' )
		{
			$card = substr( $card, 1 );
		}
		if( substr($card, -2, 1) eq '?' )
		{
			$card = substr( $card, 0, -2 );
		}
		elsif( substr($card, -1, 1) eq '?' )
		{
			$card = substr( $card, 0, -1 );
		}
	
		my $cardreader = Evend::CardUtils->new();
		($card_number, undef, undef, undef) = $cardreader->parse($card);
	}
	elsif($card_type =~ /E/)
	{
		$card_number = $card;
	}
	else
	{
		push @logs, "Received unrecognized card type! : $card_type";
	}
	
	my $machine_trans_id = "$network:$modem_id:$trans_id";
	
	push @logs, "Machine Trans ID  : $machine_trans_id";
	push @logs, "Card Type         : $card_type";
	push @logs, "Card              : ".secure_data_log($card_number);
	push @logs, "Amount            : $auth_amt";

 	# -2.02- if this is a PictureStation adjust the timestamp to set it to it's local timezone because the PMI
 	#	app doesn't do this for us	

 	#  test to see if this is a Sony PictureStation
	my $sony_test = $DATABASE->select(
					table			=> "rerix_initialization",
					select_columns	=> "count(model_number)",
					where_columns	=> [ "model_number = 'SonyProd'" , "serial_number = ?" ],
					where_values	=> [$machine_id] );
				
	my $timestamp;
	
	if($sony_test->[0][0] > 0)
	{
		# this is a Sony picture station so get its timezone
		my $dbh = $DATABASE->{handle};
		my $sth = $dbh->prepare("select a.time_zone from tazdba.location a, tazdba.machine b " .
					" where a.group_cd = 'SONY'" .
					" and b.machine_id = ?" .
					" and a.location_number = b.location_number");
					
		$sth->execute($machine_id);
		my @data = $sth->fetchrow_array();
		my $tz = $data[0];

		# we should show if its daylight savings time
		my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
		# for testing normal time during DST ONLY!!
		#$isdst = 0;

		$timestamp = time();
		push @logs, "     Server Time           : ". localtime($timestamp);
		
		$timestamp = ReRix::Utils::getAdjustedTimestamp($tz, $timestamp);		
		
		push @logs, "Adjusting Timestamp back because this is a Sony PictureStation";
		push @logs, "Terminal Timezone     : $tz";
		push @logs, "Daylight Savings time : $isdst - (0 = No, 1 = Yes)";
		push @logs, "The adjusted time     : " . gmtime($timestamp);
	}
	
	# this is a G4, so adjust the time to its timezone
	if(defined $device_id && $device_type =~ m/0|1|6|9|10/)
	{
		my $time_zone;
		
		# Use the device_id as a rerefence to lookup the time zone
		
		my $lookup_tz_ref =	$DATABASE->select(	
								table          => 'location.location, pos',
								select_columns => 'location.location_time_zone_cd',
								where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
								where_values   => [$device_id] );
								
		$time_zone = $lookup_tz_ref->[0][0];
		
		push @logs, "Adjusting Timestamp to compensate for Time Zone differences...";

		if(not defined $time_zone)
		{
			$time_zone = 'EST';
			push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
		}
		else
		{
			push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
		}
		
		push @logs, "Server Time       : " . localtime();

		$timestamp = ReRix::Utils::getAdjustedTimestamp($time_zone);
		push @logs, "Adjusted Time     : " . gmtime($timestamp);
	}

	my $trans = ReRix::AuthClient->connect();
	
	$trans->send("t6,$machine_id,$card,$auth_amt,$card_type,$machine_trans_id,$timestamp");
	my $auth_code = $trans->get_code();
	my $trans_no = $trans->authorization_number();
	push @logs, "Auth Code         : $auth_code";
	push @logs, "Trans No          : $trans_no";
	$trans->disconnect();

	my $response;
	if($device_type =~ m/0|1|6|9|10/)
	{
		# g4 sends the trans ID in big-endian order
		$response = pack("CCNC", $response_no, 96, $trans_id, ($auth_code eq 'A' ? 1:0));
	}
	else
	{
		# sony,etc sends the trans ID in little-endian order
		$response = pack("CCVC", $response_no, 96, $trans_id, ($auth_code eq 'A' ? 1:0));
	}

	push @logs, "Response          : " . unpack("H*", $response);
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*", $response)]);
						
	&ReRix::CallInRecord::add_auth($raw_handle, $machine_id, ($card_type eq 'S' ? 'P' : $card_type), $auth_amt, ($auth_code eq 'A' ? 'Y':'N'));
	&ReRix::CallInRecord::set_status($raw_handle, $machine_id, 'S');
	
	return(\@logs);
}

sub parse_auth_v3
{
	# this version of the auth returns 3 possible values:
	#	0 - Auth Declined
	#	1 - Auth Approved
	#	2 - Server Not Responding (authorization indeterminate)
	
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};
	
	my $array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'modem_id, network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $machine_id ] );

	if(not $array_ref->[0])
	{
		push @logs, "Cannot find network and modem_id in rerix_modem_to_serial for machine_id $machine_id";
		return(\@logs);
	}

	my $modem_id = $array_ref->[0][0];
	my $network = $array_ref->[0][1];

	my ($trans_id, $card_type, $auth_amt, $card);
	if($device_type =~ m/0|1|6|9|10/)
	{
		# G4 sends the trans ID in big-endian order
		(undef, $trans_id, $card_type, $auth_amt, $card) = unpack("CNaH6A*", $message);
		push @logs, "G4 Generated ID   : $trans_id";
	}
	else
	{
		# sony,etc sends the trans ID in little-endian order
		(undef, $trans_id, $card_type, $auth_amt, $card) = unpack("CVaH6A*", $message);
		push @logs, "Generated ID     : $trans_id";
	}

	# }:-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
	$auth_amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $auth_amt)))));
	$auth_amt = sprintf("%.02f", ($auth_amt * .01));

	my $card_number;
	if($card_type =~ m/S|C/)
	{
		my $cr = Evend::CardUtils->new();
		$card =  $cr->cleanSwipeData($card);

		if( substr($card, 0, 1) eq '%' )
		{
			$card = substr( $card, 1 );
		}
		if( substr($card, -2, 1) eq '?' )
		{
			$card = substr( $card, 0, -2 );
		}
		elsif( substr($card, -1, 1) eq '?' )
		{
			$card = substr( $card, 0, -1 );
		}
		
		my $cardreader = Evend::CardUtils->new();
		($card_number, undef, undef, undef) = $cardreader->parse($card);
	}
	elsif($card_type =~ /E/)
	{
		$card_number = $card;
	}
	else
	{
		push @logs, "Received unrecognized card type! : $card_type";
	}
	
	my $machine_trans_id = "$network:$modem_id:$trans_id";
	
	push @logs, "Machine Trans ID  : $machine_trans_id";
	push @logs, "Card Type         : $card_type";
	push @logs, "Card              : ".secure_data_log($card_number);
	push @logs, "Amount            : $auth_amt";

 	# -2.02- if this is a PictureStation adjust the timestamp to set it to it's local timezone because the PMI
 	#	app doesn't do this for us	

 	#  test to see if this is a Sony PictureStation
	my $sony_test = $DATABASE->select(
					table			=> "rerix_initialization",
					select_columns	=> "count(model_number)",
					where_columns	=> [ "model_number = 'SonyProd'" , "serial_number = ?" ],
					where_values	=> [$machine_id] );
				
	my $timestamp;

	if($sony_test->[0][0] > 0)
	{
		# this is a Sony picture station so get its timezone
		my $dbh = $DATABASE->{handle};
		my $sth = $dbh->prepare("select a.time_zone from tazdba.location a, tazdba.machine b " .
					" where a.group_cd = 'SONY'" .
					" and b.machine_id = ?" .
					" and a.location_number = b.location_number");
					
		$sth->execute($machine_id);
		my @data = $sth->fetchrow_array();
		my $tz = $data[0];

		# we should show if its daylight savings time
		my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
		# for testing normal time during DST ONLY!!
		#$isdst = 0;

		$timestamp = time();
		push @logs, "     Server Time           : ". localtime($timestamp);
		
		$timestamp = ReRix::Utils::getAdjustedTimestamp($tz, $timestamp);		
		
		push @logs, "Adjusting Timestamp back because this is a Sony PictureStation";
		push @logs, "Terminal Timezone     : $tz";
		push @logs, "Daylight Savings time : $isdst - (0 = No, 1 = Yes)";
		push @logs, "The adjusted time     : " . gmtime($timestamp);
	}
	
	if(defined $device_id && $device_type =~ m/0|1|6|9|10/)
	{
		# this is a G4, so adjust the time to its timezone
		my $time_zone;
		
		# Use the device_id as a rerefence to lookup the time zone
		
		my $lookup_tz_ref =	$DATABASE->select(	
								table          => 'location.location, pos',
								select_columns => 'location.location_time_zone_cd',
								where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
								where_values   => [$device_id] );
								
		$time_zone = $lookup_tz_ref->[0][0];
		
		push @logs, "Adjusting Timestamp to compensate for Time Zone differences...";

		if(not defined $time_zone)
		{
			$time_zone = 'EST';
			push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
		}
		else
		{
			push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
		}
		
		push @logs, "Server Time       : " . localtime();

		$timestamp = ReRix::Utils::getAdjustedTimestamp($time_zone);
		push @logs, "Adjusted Time     : " . gmtime($timestamp);

	}

	my $trans = ReRix::AuthClient->connect();
	
	#!! FOR TEST PURPOSES ONLY!!
	#$trans->send("t6,$machine_id,$card,$auth_amt,$card_type,$machine_trans_id,$timestamp");
	#my $auth_code = $trans->get_code();
	
	my $auth_code = 'E';
	
	push @logs, "Auth Code         : $auth_code";
	$trans->disconnect();

	my $response;
	if($device_type =~ m/0|1|6|9|10/)
	{
		# g4 sends the trans ID in big-endian order
		$response = pack("CCNC", $response_no, 187, $trans_id, 2); #($auth_code eq 'A' ? 1:0));
	}
	else
	{
		# sony,etc sends the trans ID in little-endian order
		$response = pack("CCVC", $response_no, 187, $trans_id, 2); #($auth_code eq 'A' ? 1:0));
	}

	push @logs, "Response          : " . unpack("H*", $response);
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*", $response)]);
						
	&ReRix::CallInRecord::add_auth($raw_handle, $machine_id, ($card_type eq 'S' ? 'P' : $card_type), $auth_amt, ($auth_code eq 'A' ? 'Y':'N'));
	&ReRix::CallInRecord::set_status($raw_handle, $machine_id, 'S');
	
	return(\@logs);
}

sub parse_net_batch_v2
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};

	my $array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'modem_id, network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $machine_id ] );

	if(not $array_ref->[0])
	{
		push @logs, "Cannot find network and modem_id in rerix_modem_to_serial for machine_id $machine_id";
		return(\@logs);
	}

	my $modem_id = $array_ref->[0][0];
	my $network = $array_ref->[0][1];

	my ($trans_id, $amt, $tax, $trans_result, $detail_data);
	
	if($device_type =~ m/0|1|6|9|10/)
	{
		# G4 sends the trans ID in big-endian order
		(undef, $trans_id, $amt, $tax, $trans_result, $detail_data) = unpack("aNH6vaa*", $message);
		push @logs, "G4 Generated ID   : $trans_id";
	}
	else
	{
		# sony sends the trans ID in little-endian order
		(undef, $trans_id, $amt, $tax, $trans_result, $detail_data) = unpack("aVH6vaa*", $message);
		push @logs, "Generated ID     : $trans_id";
	}

	# }:-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
	$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
	$amt = sprintf("%.02f", ($amt * .01));
	$tax = sprintf("%.02f", ($tax * .01));

	my $machine_trans_no = "$network:$modem_id:$trans_id";
	
	push @logs, "Machine ID       : $machine_id";
	push @logs, "Machine Trans ID : $machine_trans_no";
	push @logs, "Amount           : $amt";
	push @logs, "Tax              : $tax";
	push @logs, "Result           : $trans_result";
	push @logs, "Detail (hex)     : " . unpack("H*",$detail_data);
	
	# check for duplicates
	my $dupe_detected = 0;
	
	my $dupe_check_ref = $DATABASE->select(
					table			=> 'transaction_record_hist',
					select_columns	=> 'count(1)',
					where_columns	=> [ 'machine_trans_no = ?'],
					where_values	=> [ $machine_trans_no ] );

	if($dupe_check_ref->[0][0] > 0)
	{
		$dupe_detected = 1;
	}
	
	if($dupe_detected)
	{
		push @logs, "Duplicate Check   : $machine_trans_no: Duplicate Detected! Not storing or exporting record.";

		my $response;
		if($device_type =~ m/0|1|6|9|10/)
		{
			# g4 sends the trans ID in big-endian order
			$response = pack('CH2NC', $response_no, '71', $trans_id, '1');
		}
		else
		{
			# sony,etc sends the trans ID in little-endian order
			$response = pack('CH2VC', $response_no, '71', $trans_id, '1');
		}

		push @logs, "Response          : " . unpack("H*", $response);
		$DATABASE->insert(	table=> 'Machine_Command',
							insert_columns=> 'Modem_ID, Command',
							insert_values=> [$machine_id, &MakeHex($response)] );
		return(\@logs);
	}
	else
	{
		push @logs, "Duplicate Check   : $machine_trans_no: New! Storing and exporting record.";
	}		

	# get the bytes per vend column from the last 2 bits
	my $vend_col_bytes = (ord(substr($detail_data, 0, 1)) >> 6) + 1;

	# get the vend quantity from the first 6 bits
	my $vend_qty = (ord(substr($detail_data, 0, 1)) & 0x3f);
	
	# chop off the first byte; should now have a 0-n Vended Items Description Blocks
	$detail_data = substr($detail_data, 1);
	
	# the following loop will pull the column id's and vend amounts from the vended items block and place them in a hash
	my %vended_items;
	
	for (my $i = 0;  $i < $vend_qty; $i++)
	{
		my $item_tmp = substr($detail_data, (3 + $vend_col_bytes) * $i, (3 + $vend_col_bytes));

		my $item_nbr = unpack("H*",substr($item_tmp, 0, $vend_col_bytes));
		my $item_amt = substr($item_tmp, $vend_col_bytes, 3);

		$item_amt = (unpack("N", ("\x00".$item_amt))*.01);

		# add/update item in %vended_items hash - this allows us to accumulate quantities
		# by item number so we can insert summed col#/price/quantity rows into the TRANS_ITEM table.
		$vended_items{$item_nbr}{AMT} = sprintf("%.2f", $item_amt);
		$vended_items{$item_nbr}{QTY} += 1;
	}
	
	push @logs, "--------- Column Totals ---------";
	push @logs, "Vend Col Bytes  : $vend_col_bytes";
	push @logs, "Vend Qty        : $vend_qty";
	#display the column/qty/amt in the log
	foreach my $key (%vended_items)
	{
		if (defined($vended_items{$key}{QTY}))
		{
			push @logs, "\t\tColumn : $key";
			push @logs, "\t\t\tAMT    : $vended_items{$key}{AMT}";
			push @logs, "\t\t\tQTY    : $vended_items{$key}{QTY}";
		}
	}	
      
	my $trans = ReRix::AuthClient->connect();
	$trans->send("t10,$machine_id,$machine_trans_no,$amt,$tax,$trans_result,$vend_qty,%vended_items");

	my $auth_code;
	if(defined ($auth_code = $trans->get_code()))
	{
		# Batch response command is an ascii '7' and the least significant byte
		# of the transaction ID
		
		my $trans_no = $trans->authorization_number();
		push @logs, "Auth Code         : $auth_code";
		push @logs, "Trans No          : $trans_no";

		my $response;
		if($device_type =~ m/0|1|6|9|10/)
		{
			# g4 sends the trans ID in big-endian order
			$response = pack('CH2NC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
		}
		else
		{
			# sony,etc sends the trans ID in little-endian order
			$response = pack('CH2VC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
		}

		push @logs, "Response          : " . unpack("H*", $response);
		$DATABASE->insert(	table=> 'Machine_Command',
							insert_columns=> 'Modem_ID, Command',
							insert_values=> [$machine_id, &MakeHex($response)] );
	}
	else
	{
		push @logs, "Transaction Server unreachable";
	}
	
	#if the terminal is a G4/G5 and the auth succeeded export the data to a flat file for legacy systems
	if ($device_type =~ m/0|1|6/ && $trans_result =~ m/S|R|N|Q/)
	{
		push @logs, "--Exporting G4/G5 Record for $machine_id--";
		push @logs, @{&ReRix::G4Export::createG4ExportRec($DATABASE, $machine_id, $device_id, $machine_trans_no, $vend_qty, $detail_data, undef, $amt, undef, undef, $vend_col_bytes)}; 
	}
	
	# need to lookup the card type here to write the log
	
	my $card_type = 'U';
	
	my $sth = $raw_handle->prepare("select card_type from tazdba.transaction_record_hist where machine_trans_no = ?");
	$sth->execute($machine_trans_no);
	my @data = $sth->fetchrow_array();
	if(defined $data[0])
	{
		$card_type = $data[0];
	}
	else
	{
		$sth = $raw_handle->prepare("select card_type from tazdba.transaction_failures where machine_trans_no = ?");
		$sth->execute($machine_trans_no);
		@data = $sth->fetchrow_array();
		if(defined $data[0])
		{
			$card_type = $data[0];
		}
	}		
	
	if(defined $card_type)
	{
		if($card_type eq 'SP')
		{
			$card_type = 'P' 	# P = Passcard
		}
		else
		{
			$card_type = 'C'	# if it was in the DB and was not SP, then it should be a credit card (?)
		}
	}
	
	&ReRix::CallInRecord::add_trans($raw_handle, $machine_id, $card_type, ($amt+$tax));

	return(\@logs);
}

sub parse_local_batch_v2
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};

	my $array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'modem_id, network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $machine_id ] );

	if(not $array_ref->[0])
	{
		push @logs, "Cannot find network and modem_id in rerix_modem_to_serial for machine_id $machine_id";
		return(\@logs);
	}

	my $modem_id = $array_ref->[0][0];
	my $network = $array_ref->[0][1];

	my (undef, $trans_id, $timestamp, $card_type, $amt, $tax, $maglength, $reqvariable);
	if($device_type =~ m/0|1|6|9|10/)
	{
		# G4 sends the batch ID in big-endian order
		(undef, $trans_id, $timestamp, $card_type, $amt, $tax, $maglength, $reqvariable) = unpack("aNNaH6vCa*", $message);
		push @logs, "G4 Generated ID   : $trans_id";
	}
	else
	{
		# sony sends the batch ID in little-endian order
		(undef, $trans_id, $timestamp, $card_type, $amt, $tax, $maglength, $reqvariable) = unpack("aVNaH6vCa*", $message);
		push @logs, "Generated ID     : $trans_id";
	}

	# }:-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
	$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
	$amt = sprintf("%.02f", ($amt * .01));
	$tax = sprintf("%.02f", ($tax * .01));;

	my $card;
	($card, $reqvariable) = (substr($reqvariable, 0, $maglength), substr($reqvariable, $maglength));
	my ($trans_result, $detail_data) = unpack("aa*", $reqvariable);

	my $card_number;
	if($card_type =~ m/S|C/)
	{
		my $cr = Evend::CardUtils->new();
		$card =  $cr->cleanSwipeData($card);
	
		if( substr($card, 0, 1) eq '%' )
		{
			$card = substr( $card, 1 );
		}
		if( substr($card, -2, 1) eq '?' )
		{
			$card = substr( $card, 0, -2 );
		}
		elsif( substr($card, -1, 1) eq '?' )
		{
			$card = substr( $card, 0, -1 );
		}
	
		my $cardreader = Evend::CardUtils->new();
		($card_number, undef, undef, undef) = $cardreader->parse($card);
	}
	elsif($card_type =~ /E/)
	{
		$card_number = $card;
	}
	else
	{
		push @logs, "Received unrecognized card type! : $card_type";
	}
	
	push @logs, "Client Timestamp  : $timestamp";
	if (((time() - (60*24*60*60)) > $timestamp) || ((time() + (60*24*60*60)) < $timestamp))
	{
		push @logs, "Received invalid timestamp! : $timestamp, using current time: ";
		$timestamp = time();
	}

	#  test to see if this is a Sony PictureStation
	my $sony_test = $DATABASE->select(
					table			=> "rerix_initialization",
					select_columns	=> "count(model_number)",
					where_columns	=> [ "model_number = 'SonyProd'" , "serial_number = ?" ],
					where_values	=> [$machine_id] );

	# -2.02- if this is a PictureStation adjust the timestamp to set it to it's local timezone because the PMI app doesn't do this for us
	if( $sony_test->[0][0] > 0)
	{
		# this is a Sony picture station get its timezone
		my $dbh = $DATABASE->{handle};
		my $sth = $dbh->prepare("select a.time_zone from tazdba.location a, tazdba.machine b " .
					" where a.group_cd = 'SONY'" .
					" and b.machine_id = ?" .
					" and a.location_number = b.location_number");
		$sth->execute($machine_id);
		my @data = $sth->fetchrow_array();
		my $tz = $data[0];

		# we should show if its daylight savings time
		my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
		# for testing normal time during DST ONLY!!
		#$isdst = 0;

		push @logs, "     Client Timestamp      : ". localtime($timestamp);
		
		$timestamp = ReRix::Utils::getAdjustedTimestamp($tz, $timestamp);

		push @logs, "     Adjusting Timestamp back because this is a Sony PictureStation";
		push @logs, "     Terminal Timezone     : $tz";
		push @logs, "     Daylight Savings time : $isdst - (0 = No, 1 = Yes)";
		push @logs, "     The adjusted time     : " . gmtime($timestamp);
	}

	my ($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);
	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	my $timestr = sprintf("%02d:%02d:%02d", $hour, $min, $sec);

	my $machine_trans_no = "$network:$modem_id:$trans_id";
	push @logs, "Machine Trans ID  : $machine_trans_no";
	push @logs, "Machine ID        : $machine_id";
	push @logs, "Timestamp         : $timestamp";
	push @logs, "Date/Time         : $date  $timestr";
	push @logs, "Card Type         : $card_type";
	push @logs, "Amount            : $amt";
	push @logs, "Tax               : $tax";
	push @logs, "Result            : $trans_result";
	push @logs, "Card Length       : $maglength";
	push @logs, "Card              : ".secure_data_log($card_number);
	push @logs, "Detail Data       : " . unpack("H*", $detail_data);

	# check for duplicates
	# don't need the special G4 logic in this one, because G4s use the BCD local batch below
	my $dupe_detected = 0;
	
	my $dupe_check_ref = $DATABASE->select(
					table			=> 'transaction_record',
					select_columns	=> 'count(1)',
					where_columns	=> [ 'machine_trans_no = ?'],
					where_values	=> [ $machine_trans_no ] );

	if($dupe_check_ref->[0][0] > 0)
	{
		$dupe_detected = 1;
	}
	else
	{
		my $dupe_check_2_ref = $DATABASE->select(
						table			=> 'transaction_record_hist',
						select_columns	=> 'count(1)',
						where_columns	=> [ 'machine_trans_no = ?'],
						where_values	=> [ $machine_trans_no ] );
	
		if($dupe_check_2_ref->[0][0] > 0)
		{
			$dupe_detected = 1;
		}
	}
	
	if($dupe_detected)
	{
		push @logs, "Duplicate Check   : $machine_trans_no: Duplicate Detected! Not storing or exporting record.";

		my $response;
		if($device_type =~ m/0|1|6|9|10/)
		{
			# g4 sends the trans ID in big-endian order
			$response = pack('CH2NC', $response_no, '71', $trans_id, '1');
		}
		else
		{
			# sony,etc sends the trans ID in little-endian order
			$response = pack('CH2VC', $response_no, '71', $trans_id, '1');
		}

		push @logs, "Response          : " . unpack("H*", $response);
		$DATABASE->insert(	table=> 'Machine_Command',
							insert_columns=> 'Modem_ID, Command',
							insert_values=> [$machine_id, &MakeHex($response)] );
		return(\@logs);
	}
	else
	{
		push @logs, "Duplicate Check   : $machine_trans_no: New! Storing and exporting record.";
	}
	
	my ($vend_qty, $vend_col_bytes);
	if($card_type =~ m/S|C/)
	{
		# get the bytes per vend column from the last 2 bits
		$vend_col_bytes = (ord(substr($detail_data, 0, 1)) >> 6) + 1;
	
		# get the vend quantity from the first 6 bits
		$vend_qty = (ord(substr($detail_data, 0, 1)) & 0x3f);
		
		# chop off the first byte; should now have a 0-n Vended Items Description Blocks
		$detail_data = substr($detail_data, 1);
		
		# the following loop will pull the column id's and vend amounts from the vended items block and place them in a hash
		my %vended_items;
		
		for (my $i = 0;  $i < $vend_qty; $i++)
		{
			my $item_tmp = substr($detail_data, (3 + $vend_col_bytes) * $i, (3 + $vend_col_bytes));
	
			my $item_nbr = unpack("H*",substr($item_tmp, 0, $vend_col_bytes));
			my $item_amt = substr($item_tmp, $vend_col_bytes, 3);
	
			$item_amt = (unpack("N", ("\x00".$item_amt))*.01);
	
			# add/update item in %vended_items hash - this allows us to accumulate quantities
			# by item number so we can insert summed col#/price/quantity rows into the TRANS_ITEM table.
			$vended_items{$item_nbr}{AMT} = sprintf("%.2f", ($vended_items{$item_nbr}{AMT} + $item_amt));
			$vended_items{$item_nbr}{QTY} += 1;
		}
		
		push @logs, "--------- Column Totals ---------";
		push @logs, "Vend Col Bytes  : $vend_col_bytes";
		push @logs, "Vend Qty        : $vend_qty";
		#display the column/qty/amt in the log
		foreach my $key (%vended_items)
		{
			if (defined($vended_items{$key}{QTY}))
			{
				push @logs, "\t\tColumn : $key";
				push @logs, "\t\tAMT    : $vended_items{$key}{AMT}";
				push @logs, "\t\tQTY    : $vended_items{$key}{QTY}";
			}
		}	
		
		my $trans = ReRix::AuthClient->connect();
		$trans->send("t11,$machine_id,$timestr,$date,$card,$card_type,$amt,$tax,$trans_result,$machine_trans_no,$timestamp,$vend_qty,$detail_data");
	
		my $auth_code;
		if(defined ($auth_code = $trans->get_code()))
		{
			# Batch response command is an ascii '7' and the least significant byte
			# of the transaction ID
			
			my $trans_no = $trans->authorization_number();
			push @logs, "Auth Code         : $auth_code";
			push @logs, "Trans No          : $trans_no";
			
			my $response;
			if($device_type =~ m/0|1|6|9|10/)
			{
				# g4 sends the trans ID in big-endian order
				$response = pack('CH2NC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
			}
			else
			{
				# sony,etc sends the trans ID in little-endian order
				$response = pack('CH2VC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
			}
	
			push @logs, "Response          : " . unpack("H*", $response);
			$DATABASE->insert(	table=> 'Machine_Command',
								insert_columns=> 'Modem_ID, Command',
								insert_values=> [$machine_id, &MakeHex($response)] );
		}
		else
		{
			push @logs, "Transaction Server unreachable";
		}
	}
	elsif($card_type =~ m/E/)
	{
		my $response;
		if($device_type =~ m/0|1|6|9|10/)
		{
			# g4 sends the trans ID in big-endian order
			$response = pack('CH2NC', $response_no, '71', $trans_id, '1');
		}
		else
		{
			# sony,etc sends the trans ID in little-endian order
			$response = pack('CH2VC', $response_no, '71', $trans_id, '1');
		}

		push @logs, "Response          : " . unpack("H*", $response);
		$DATABASE->insert(	table=> 'Machine_Command',
							insert_columns=> 'Modem_ID, Command',
							insert_values=> [$machine_id, &MakeHex($response)] );
	}

	#if the terminal is a G4/G5 and the auth succeeded export the data to a flat file for other processes
	if ($device_type =~ m/0|1|6/ && $trans_result =~ m/S|R|N|Q/)
	{
		push @logs, "--Exporting G4/G5 Record for $machine_id--";
		push @logs, @{&ReRix::G4Export::createG4ExportRec($DATABASE, $machine_id, $device_id, $machine_trans_no, $vend_qty, $detail_data, $timestamp, $amt, $card, $card_type, $vend_col_bytes)}; 
	}
	elsif ($device_type =~ m/0|1|6/ && $card_type eq 'E')
	{
		push @logs, "--Exporting G4/G5 Error Record for $machine_id--";
		push @logs, @{&ReRix::G4Export::createG4ErrorRec($DATABASE, $machine_id, $device_id, $SSN, $card)}; 
	}

	&ReRix::CallInRecord::add_trans($raw_handle, $machine_id, ($card_type eq 'S' ? 'P' : $card_type), ($amt+$tax));

	return(\@logs);
}

sub parse_local_batch_v2_bcd
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};

	my $array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'modem_id, network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $machine_id ] );

	if(not $array_ref->[0])
	{
		push @logs, "Cannot find network and modem_id in rerix_modem_to_serial for machine_id $machine_id";
		return(\@logs);
	}

	my $modem_id = $array_ref->[0][0];
	my $network = $array_ref->[0][1];

	my (undef, $trans_id, $bcd_str, $card_type, $amt, $tax, $maglength, $reqvariable);
	if($device_type =~ m/0|1|6|9|10/)
	{
		# G4 sends the batch ID in big-endian order
		(undef, $trans_id, $bcd_str, $card_type, $amt, $tax, $maglength, $reqvariable) = unpack("aNa7aH6vCa*", $message);
		push @logs, "G4 Generated ID   : $trans_id";
	}
	else
	{
		# sony sends the batch ID in little-endian order
		(undef, $trans_id, $bcd_str, $card_type, $amt, $tax, $maglength, $reqvariable) = unpack("aVa7aH6vCa*", $message);
		push @logs, "Generated ID     : $trans_id";
	}

	# }:-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
	$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
	$amt = sprintf("%.02f", ($amt * .01));
	$tax = sprintf("%.02f", ($tax * .01));;

	my $card;
	($card, $reqvariable) = (substr($reqvariable, 0, $maglength), substr($reqvariable, $maglength));
	my ($trans_result, $detail_data) = unpack("aa*", $reqvariable);

	my $card_number;
	if($card_type =~ m/S|C/)
	{
		my $cr = Evend::CardUtils->new();
		$card =  $cr->cleanSwipeData($card);
	
		if( substr($card, 0, 1) eq '%' )
		{
			$card = substr( $card, 1 );
		}
		if( substr($card, -2, 1) eq '?' )
		{
			$card = substr( $card, 0, -2 );
		}
		elsif( substr($card, -1, 1) eq '?' )
		{
			$card = substr( $card, 0, -1 );
		}
	
		my $cardreader = Evend::CardUtils->new();
		($card_number, undef, undef, undef) = $cardreader->parse($card);
	}
	elsif($card_type =~ /E/)
	{
		$card_number = $card;
	}
	else
	{
		push @logs, "Received unrecognized card type! : $card_type";
	}
	
	#  test to see if this is a Sony PictureStation
	my $sony_test = $DATABASE->select(
					table			=> "rerix_initialization",
					select_columns	=> "count(model_number)",
					where_columns	=> [ "model_number = 'SonyProd'" , "serial_number = ?" ],
					where_values	=> [$machine_id] );
	
	# The only difference between this command and parse_local_batch_v2 is the following part to convert
	# the BCD date to a timestamp
	
	if($device_type eq '0')
	{
		push @logs, "> Bug Fix         : Calling fix_g4_bcd_seconds_bug_discovered_02_03_2004";
		my $log_ref;
		($bcd_str, $log_ref) = &fix_g4_bcd_seconds_bug_discovered_02_03_2004($bcd_str, $machine_id);
		push @logs, @{$log_ref};
	}
	
	my $machine_trans_no = "$network:$modem_id:$trans_id";
	
	# check the transaction timestamp to make sure it's not screwed up
	my ($bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year) = unpack("H2H2H2H2H2H4", $bcd_str);
	#push @logs, "Tran BCD Timestamp : $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year";
	
	# create current time bcd string to check against
	my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
	my $current_bcd_str = sprintf("%02d%02d%02d%02d%02d%04d", $hour, $min, $sec, ($mon + 1), $mday, ($year + 1900));
	
	my ($current_bcd_hour, $current_bcd_min, $current_bcd_sec, $current_bcd_mon, $current_bcd_mday, $current_bcd_year) = unpack("a2a2a2a2a2a4", $current_bcd_str);
	#push @logs, "Current BCD Timestamp : $current_bcd_hour, $current_bcd_min, $current_bcd_sec, $current_bcd_mon, $current_bcd_mday, $current_bcd_year";
	my $time_validation_failed = 0;

	if($bcd_hour > 24)
	{
		push @logs, "ERROR             : Time Validation Failed! Hour = $bcd_hour: $SSN";
		$bcd_hour = $current_bcd_hour;
		$time_validation_failed = 1;
	}

	if($bcd_min > 60)
	{
		push @logs, "ERROR             : Time Validation Failed! Minute = $bcd_min: $SSN";
		$bcd_min = $current_bcd_min;
		$time_validation_failed = 1;
	}
	
	if($bcd_sec > 60)
	{
		push @logs, "ERROR             : Time Validation Failed! Second = $bcd_sec: $SSN";
		$bcd_sec = $current_bcd_sec;
		$time_validation_failed = 1;
	}

	if($bcd_mon > 12 || $bcd_mon == 0)
	{
		push @logs, "ERROR             : Time Validation Failed! Month = $bcd_mon: $SSN";
		$bcd_mon = $current_bcd_min;
		$time_validation_failed = 1;
	}

	if($bcd_mday > 31 || $bcd_mday == 0)
	{
		push @logs, "ERROR             : Time Validation Failed! Day = $bcd_mday: $SSN";
		$bcd_mday = $current_bcd_mday;
		$time_validation_failed = 1;
	}

	if($bcd_year > ($current_bcd_year+1) || $bcd_year < ($current_bcd_year-1))
	{
		push @logs, "ERROR             : Time Validation Failed! Year = $bcd_year: $SSN";
		$bcd_year = $current_bcd_year;
		$time_validation_failed = 1;
	}
	
	if($time_validation_failed)
	{
		$bcd_str = pack("H2H2H2H2H2H4", $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year);
		push @logs, "ERROR             : Substituting BCD String: " . unpack("H*", $bcd_str);
	}
		
	my $timestamp = &ReRix::Utils::convert_bcd_time($bcd_str);
	push @logs, "Client Timestamp  : " . gmtime($timestamp);

 	# -2.02- if this is a PictureStation adjust the timestamp to set it to it's local timezone because the PMI
 	# app doesn't do this for us	
	if( $sony_test->[0][0] > 0)
	{
		# this is a Sony picture station get its timezone
		my $dbh = $DATABASE->{handle};
		my $sth = $dbh->prepare("select a.time_zone from tazdba.location a, tazdba.machine b " .
					" where a.group_cd = 'SONY'" .
					" and b.machine_id = ?" .
					" and a.location_number = b.location_number");
		$sth->execute($machine_id);
		my @data = $sth->fetchrow_array();
		my $tz = $data[0];

		# we should show if its daylight savings time
		my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
		# for testing normal time during DST ONLY!!
		#$isdst = 0;

		push @logs, "     Client Timestamp      : ". localtime($timestamp);
		
		$timestamp = ReRix::Utils::getAdjustedTimestamp($tz, $timestamp);

		push @logs, "     Adjusting Timestamp back because this is a Sony PictureStation";
		push @logs, "     Terminal Timezone     : $tz";
		push @logs, "     Daylight Savings time : $isdst - (0 = No, 1 = Yes)";
		push @logs, "     The adjusted time     : " . gmtime($timestamp);
	}

	($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);

	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	my $timestr = sprintf("%02d:%02d:%02d", $hour, $min, $sec);

	push @logs, "Machine Trans ID  : $machine_trans_no";
	push @logs, "Machine ID        : $machine_id";
	push @logs, "Timestamp         : $timestamp";
	push @logs, "Date/Time         : $date  $timestr";
	push @logs, "Card Type         : $card_type";
	push @logs, "Amount            : $amt";
	push @logs, "Tax               : $tax";
	push @logs, "Result            : $trans_result";
	push @logs, "Card Length       : $maglength";
	push @logs, "Card              : ".secure_data_log($card_number);
	push @logs, "Detail Data       : " . unpack("H*", $detail_data);
	
	# check for duplicates
	# account for G4 duplicate trans ID bug explained at top
	my $processed_time_str;
	my $dupe_detected = 0;

	my $dupe_check_ref = $DATABASE->select(
					table			=> 'transaction_record',
					select_columns	=> "to_char(batch_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
					where_columns	=> [ 'machine_trans_no = ?'],
					where_values	=> [ $machine_trans_no ] );
					
	if(defined $dupe_check_ref->[0][0])
	{
		$processed_time_str = $dupe_check_ref->[0][0];
		$dupe_detected = 1;
	}
	else
	{
		my $dupe_check_2_ref = $DATABASE->select(
						table			=> 'transaction_record_hist',
						select_columns	=> "to_char(batch_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
						where_columns	=> [ 'machine_trans_no = ?'],
						where_values	=> [ $machine_trans_no ] );
	
		if(defined $dupe_check_2_ref->[0][0])
		{
			$processed_time_str = $dupe_check_2_ref->[0][0];
			$dupe_detected = 1;
		}
	}
	
	if(defined $processed_time_str && $device_type eq '0')
	{
		# this is a G4 and we've found an existing record...
		# because of a G4 bug, we must examine the batch time to decide if this is really a duplicate
		
		my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
		my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $processed_time_str);
		my $processed_ts = Time::Local::timelocal($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);
		
		my $dt = DateTime->now;
		my $current_ts = $dt->epoch();
		
		# find the difference and round to the nearest minute
		my $et = (($current_ts - $processed_ts)/60);
		$et = sprintf("%d", $et);
		
		if($et > 5)
		{
			# we processed the previous one more than 5 minutes ago, so call this one a dupe
			$dupe_detected = 1;
			push @logs, "G4 Duplicate Check: $machine_trans_no exists, $et min old; Duplicate Detected!";
		}
		else
		{
			$dupe_detected = 0;
			
			# contruct a different machine_trans_no to store this one as
			my $new_machine_trans_no = $machine_trans_no . ":" . $current_ts;

			# we processed the previous one less than 5 minutes ago, so don't call this one a dupe
			push @logs, "G4 Duplicate Check: $machine_trans_no exists, $et min old; Saving as $new_machine_trans_no";
			
			$machine_trans_no = $new_machine_trans_no;
		}
	}
	
	if($dupe_detected)
	{
		push @logs, "Duplicate Check   : $machine_trans_no: Duplicate Detected! Not storing or exporting record.";

		my $response;
		if($device_type =~ m/0|1|6|9|10/)
		{
			# g4 sends the trans ID in big-endian order
			$response = pack('CH2NC', $response_no, '71', $trans_id, '1');
		}
		else
		{
			# sony,etc sends the trans ID in little-endian order
			$response = pack('CH2VC', $response_no, '71', $trans_id, '1');
		}

		push @logs, "Response          : " . unpack("H*", $response);
		$DATABASE->insert(	table=> 'Machine_Command',
							insert_columns=> 'Modem_ID, Command',
							insert_values=> [$machine_id, &MakeHex($response)] );
		return(\@logs);
	}
	else
	{
		push @logs, "Duplicate Check   : $machine_trans_no: New! Storing and exporting record.";
	}
	
	my ($vend_qty, $vend_col_bytes);
	if($card_type =~ m/S|C/)
	{
		# get the bytes per vend column from the last 2 bits
		$vend_col_bytes = (ord(substr($detail_data, 0, 1)) >> 6) + 1;
	
		# get the vend quantity from the first 6 bits
		$vend_qty = (ord(substr($detail_data, 0, 1)) & 0x3f);
		
		# chop off the first byte; should now have a 0-n Vended Items Description Blocks
		$detail_data = substr($detail_data, 1);
		
		# the following loop will pull the column id's and vend amounts from the vended items block and place them in a hash
		my %vended_items;
		
		for (my $i = 0;  $i < $vend_qty; $i++)
		{
			my $item_tmp = substr($detail_data, (3 + $vend_col_bytes) * $i, (3 + $vend_col_bytes));
	
			my $item_nbr = unpack("H*",substr($item_tmp, 0, $vend_col_bytes));
			my $item_amt = substr($item_tmp, $vend_col_bytes, 3);
	
			$item_amt = (unpack("N", ("\x00".$item_amt))*.01);
	
			# add/update item in %vended_items hash - this allows us to accumulate quantities
			# by item number so we can insert summed col#/price/quantity rows into the TRANS_ITEM table.
			$vended_items{$item_nbr}{AMT} = sprintf("%.2f", ($vended_items{$item_nbr}{AMT} + $item_amt));
			$vended_items{$item_nbr}{QTY} += 1;
		}
		
		push @logs, "--------- Column Totals ---------";
		push @logs, "Vend Col Bytes  : $vend_col_bytes";
		push @logs, "Vend Qty        : $vend_qty";
		#display the column/qty/amt in the log
		foreach my $key (%vended_items)
		{
			if (defined($vended_items{$key}{QTY}))
			{
				push @logs, "\t\tColumn : $key";
				push @logs, "\t\tAMT    : $vended_items{$key}{AMT}";
				push @logs, "\t\tQTY    : $vended_items{$key}{QTY}";
			}
		}	
		
		my $trans = ReRix::AuthClient->connect();
		$trans->send("t11,$machine_id,$timestr,$date,$card,$card_type,$amt,$tax,$trans_result,$machine_trans_no,$timestamp,$vend_qty,$detail_data");
	
		my $auth_code;
		if( defined ($auth_code = $trans->get_code() ) )
		{
			# Batch response command is an ascii '7' and the least significant byte
			# of the transaction ID
			
			my $trans_no = $trans->authorization_number();
			push @logs, "Auth Code         : $auth_code";
			push @logs, "Trans No          : $trans_no";
			
			my $response;
			if($device_type =~ m/0|1|6|9|10/)
			{
				# g4 sends the trans ID in big-endian order
				$response = pack('CH2NC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
			}
			else
			{
				# sony,etc sends the trans ID in little-endian order
				$response = pack('CH2VC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
			}
	
			push @logs, "Response          : " . unpack("H*", $response);
			$DATABASE->insert(	table=> 'Machine_Command',
								insert_columns=> 'Modem_ID, Command',
								insert_values=> [$machine_id, &MakeHex($response)] );
		}
		else
		{
			push @logs, "Transaction Server unreachable";
		}
	}
	elsif($card_type =~ m/E/)
	{
		my $response;
		if($device_type =~ m/0|1|6|9|10/)
		{
			# g4 sends the trans ID in big-endian order
			$response = pack('CH2NC', $response_no, '71', $trans_id, '1');
		}
		else
		{
			# sony,etc sends the trans ID in little-endian order
			$response = pack('CH2VC', $response_no, '71', $trans_id, '1');
		}

		push @logs, "Response          : " . unpack("H*", $response);
		$DATABASE->insert(	table=> 'Machine_Command',
							insert_columns=> 'Modem_ID, Command',
							insert_values=> [$machine_id, &MakeHex($response)] );
	}
	
	#if the terminal is a G4/G5 and the auth succeeded export the data to a flat file for other processes
	#if ($command_hashref->{device_type} eq '0') 
	#if (($amt > 0) && ($device_type eq '0'))
	if ($device_type =~ m/0|1|6/ && $trans_result =~ m/S|R|N|Q/)
	{
		push @logs, "--Exporting G4/G5 Record for $machine_id--";
		push @logs, @{&ReRix::G4Export::createG4ExportRec($DATABASE, $machine_id, $device_id, $machine_trans_no, $vend_qty, $detail_data, $timestamp, $amt, $card, $card_type, $vend_col_bytes)}; 
	}
	elsif ($device_type =~ m/0|1|6/ && $card_type eq 'E')
	{
		push @logs, "--Exporting G4/G5 Error Record for $machine_id--";
		push @logs, @{&ReRix::G4Export::createG4ErrorRec($DATABASE, $machine_id, $device_id, $SSN, $timestamp, $card)}; 
	}

	&ReRix::CallInRecord::add_trans($raw_handle, $machine_id, ($card_type eq 'S' ? 'P' : $card_type), ($amt+$tax));

	return(\@logs);
}

sub parse_cash_sale
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};
	
	push @logs, "Deprecated        : parse_cash_sale";

	return(\@logs);
}

sub parse_cash_sale_v2_bcd
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};
	
	my $network_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'network',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $machine_id ] );

	if(not defined $network_ref->[0])
	{
		push @logs, "Cannot find network in rerix_modem_to_serial for machine_id $machine_id";
		return(\@logs);
	}

	my $network = $network_ref->[0][0];

	my (undef, $trans_id, $bcd_str, $amt, $column);
	if($device_type =~ m/0|1|6|9|10/)
	{
		(undef, $trans_id, $bcd_str, $amt, $column) = unpack("aNa7H6H2", $message);
		push @logs, "G4 Generated ID   : $trans_id";
	}
	else
	{
		(undef, $trans_id, $bcd_str, $amt, $column) = unpack("aVa7H6H2", $message);
		push @logs, "Generated ID      : $trans_id";
	}

	my $machine_trans_no = "$network:$machine_id:$trans_id";
	
	# }:-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
	$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
	$amt = sprintf("%.02f", ($amt * .01));
	
	if($device_type eq '0')
	{
		push @logs, "> Bug Fix         : Calling fix_g4_bcd_seconds_bug_discovered_02_03_2004";
		my $log_ref;
		($bcd_str, $log_ref) = &fix_g4_bcd_seconds_bug_discovered_02_03_2004($bcd_str, $machine_id);
		push @logs, @{$log_ref};
	}
	
	# check the transaction timestamp to make sure it's not screwed up
	my ($bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year) = unpack("H2H2H2H2H2H4", $bcd_str);
	#push @logs, "Tran BCD Timestamp : $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year";
	
	# create current time bcd string to check against
	my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
	my $current_bcd_str = sprintf("%02d%02d%02d%02d%02d%04d", $hour, $min, $sec, ($mon + 1), $mday, ($year + 1900));
	
	my ($current_bcd_hour, $current_bcd_min, $current_bcd_sec, $current_bcd_mon, $current_bcd_mday, $current_bcd_year) = unpack("a2a2a2a2a2a4", $current_bcd_str);
	#push @logs, "Current BCD Timestamp : $current_bcd_hour, $current_bcd_min, $current_bcd_sec, $current_bcd_mon, $current_bcd_mday, $current_bcd_year";
	my $time_validation_failed = 0;

	if($bcd_hour > 24)
	{
		push @logs, "ERROR             : Time Validation Failed! Hour = $bcd_hour: $SSN";
		$bcd_hour = $current_bcd_hour;
		$time_validation_failed = 1;
	}

	if($bcd_min > 60)
	{
		push @logs, "ERROR             : Time Validation Failed! Minute = $bcd_min: $SSN";
		$bcd_min = $current_bcd_min;
		$time_validation_failed = 1;
	}
	
	if($bcd_sec > 60)
	{
		push @logs, "ERROR             : Time Validation Failed! Second = $bcd_sec: $SSN";
		$bcd_sec = $current_bcd_sec;
		$time_validation_failed = 1;
	}

	if($bcd_mon > 12 || $bcd_mon == 0)
	{
		push @logs, "ERROR             : Time Validation Failed! Month = $bcd_mon: $SSN";
		$bcd_mon = $current_bcd_min;
		$time_validation_failed = 1;
	}

	if($bcd_mday > 31 || $bcd_mday == 0)
	{
		push @logs, "ERROR             : Time Validation Failed! Day = $bcd_mday: $SSN";
		$bcd_mday = $current_bcd_mday;
		$time_validation_failed = 1;
	}

	if($bcd_year > ($current_bcd_year+1) || $bcd_year < ($current_bcd_year-1))
	{
		push @logs, "ERROR             : Time Validation Failed! Year = $bcd_year: $SSN";
		$bcd_year = $current_bcd_year;
		$time_validation_failed = 1;
	}
	
	if($time_validation_failed)
	{
		$bcd_str = pack("H2H2H2H2H2H4", $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year);
		push @logs, "ERROR             : Substituting BCD String: " . unpack("H*", $bcd_str);
	}
		
	my $timestamp = &ReRix::Utils::convert_bcd_time($bcd_str);
	push @logs, "Client Timestamp  : " . gmtime($timestamp);

	($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);
	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	my $time = sprintf("%02d:%02d:%02d", $hour, $min, $sec);

	push @logs, "Date/Time         : $date $time" ;
	push @logs, "Amount            : $amt";
	push @logs, "Column            : $column";
	
	# check if this cash transaction has been saved yet
	my $check_cash_ref = $DATABASE->select(
					table			=> 'cash_transaction_record',
					select_columns	=> "to_char(processed_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
					where_columns	=> [ 'machine_trans_no = ?'],
					where_values	=> [ $machine_trans_no ] );
	
	if(not defined $check_cash_ref->[0][0])
	{
		# this is a new cash transaction
		push @logs, "Duplicate Check   : $machine_trans_no: New! Storing and exporting record.";
		
		my $insert_cash_ref = $raw_handle->prepare(q{ INSERT INTO cash_transaction_record(transaction_date, machine_id, machine_trans_no, amount, column_id) VALUES (to_date(:transaction_date, 'MM/DD/YYYY HH24:MI:SS'), :machine_id, :machine_trans_no, :amount, :column_id) });
		$insert_cash_ref->bind_param(":transaction_date", "$date $time");
		$insert_cash_ref->bind_param(":machine_id", $machine_id);
		$insert_cash_ref->bind_param(":machine_trans_no", $machine_trans_no);
		$insert_cash_ref->bind_param(":amount", $amt);
		$insert_cash_ref->bind_param(":column_id", $column);
		$insert_cash_ref->execute;
		my $db_err_str = $raw_handle->errstr;
		$insert_cash_ref->finish();
		
		if(defined $db_err_str)
		{
			push @logs, "Insert Cash Transaction Failed: $db_err_str";
			return(\@logs);
		} 
		
		# export data for USALive
		if ($device_type =~ m/0|1|6/)
		{
			push @logs, "--Exporting G4/G5 Cash Record for $machine_id--";
			push @logs, @{&ReRix::G4Export::createG4ExportCashRec($DATABASE, $machine_id, $device_id, $amt, $column, $timestamp)};
		}
	}
	else
	{
		# this is a duplicate cash transaction, but due to a client bug we can't necessairly assume it's a dupe

		my $processed_time_str = $check_cash_ref->[0][0];
		my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
		my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $processed_time_str);
		my $processed_ts = Time::Local::timelocal($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);
		
		my $dt = DateTime->now;
		my $current_ts = $dt->epoch();
		
		# find the difference and round to the nearest minute
		my $et = (($current_ts - $processed_ts)/60);
		$et = sprintf("%d", $et);
		
		if($et <= 5)
		{
			# contruct a different machine_trans_no to store this one as
			my $new_machine_trans_no = $machine_trans_no . ":" . $current_ts;

			# we processed the previous one less than 5 minutes ago, so don't call this one a dupe
			push @logs, "G4 Duplicate Check: $machine_trans_no exists, $et min old; Saving as $new_machine_trans_no";
			
			my $insert_cash_ref = $raw_handle->prepare(q{ INSERT INTO cash_transaction_record(transaction_date, machine_id, machine_trans_no, amount, column_id) VALUES (to_date(:transaction_date, 'MM/DD/YYYY HH24:MI:SS'), :machine_id, :machine_trans_no, :amount, :column_id) });
			$insert_cash_ref->bind_param(":transaction_date", "$date $time");
			$insert_cash_ref->bind_param(":machine_id", $machine_id);
			$insert_cash_ref->bind_param(":machine_trans_no", $new_machine_trans_no);
			$insert_cash_ref->bind_param(":amount", $amt);
			$insert_cash_ref->bind_param(":column_id", $column);
			$insert_cash_ref->execute;
			my $db_err_str = $raw_handle->errstr;
			$insert_cash_ref->finish();
			
			if(defined $db_err_str)
			{
				push @logs, "Insert Cash Transaction Failed: $db_err_str";
				return(\@logs);
			} 
			
			# export data for USALive
			if ($device_type =~ m/0|1|6/)
			{
				push @logs, "--Exporting G4/G5 Cash Record for $machine_id--";
				push @logs, @{&ReRix::G4Export::createG4ExportCashRec($DATABASE, $machine_id, $device_id, $amt, $column, $timestamp)};
			}
		}
		else
		{
			# we processed the previous one more than 5 minutes ago, so call this one a dupe
			push @logs, "G4 Duplicate Check: $machine_trans_no exists, $et min old; Duplicate Detected!";
		}
	}

	my $response;
	if($device_type =~ m/0|1|6|9|10/)
	{
		$response = pack("CH2N", $response_no, '95', $trans_id);
	}
	else
	{
		$response = pack("CH2V", $response_no, '95', $trans_id);
	}

	push @logs, "Response          : " . unpack("H*", $response);
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*", $response)]);

	&ReRix::CallInRecord::add_trans($raw_handle, $machine_id, 'M', $amt);

	return(\@logs);
}

sub fix_g4_bcd_seconds_bug_discovered_02_03_2004
{
	my @logs;
	my ($bcd_str, $machine_id) = @_;
	my ($bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year) = unpack("H2H2H2H2H2H4", $bcd_str);
	
	# may want to put some additional checks here to look at the machine ID and decide if it's a version
	# that contains this bug
	
	#push @logs, "> Input BCD Time  : " . unpack("H*", $bcd_str);
	#push @logs, "> Input Seconds   : $bcd_sec";
	
	if($bcd_sec eq $bcd_mon)
	{
		my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);

		if(length($sec) == 1)
		{
			$sec = "0$sec";
		}
		
		push @logs, "> Fixing Seconds  : Using $sec instead of $bcd_sec";

		$bcd_sec = $sec;
	}
	else
	{
		push @logs, "> Seconds OK      : Seconds $bcd_sec != Month $bcd_mon";
	}
	
	$bcd_str = pack("H2H2H2H2H2H4", $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year);
	
	#push @logs, "> Output Seconds  : $bcd_sec";
	#push @logs, "> Output BCD Time : " . unpack("H*", $bcd_str);

	return ($bcd_str, \@logs);
}

sub secure_data_log ($)
{
	my $data = shift;
	my ($min_num_length, $max_seq_shown) = (8, 4);	#min num of chars that requires security; max num of chars in any sequence shown
	my $data_out = '';
	if (length $data > 0)
	{
		$data_out = substr($data, 0, length $data > $min_num_length?4:length $data).(length $data > $min_num_length?(length $data > (4 + 1)?'...':'').substr($data, (length $data) - int((length $data) / 4)):'');
	}
	return $data_out.' ('.(length $data).' char)';
}

1;
