#!/usr/bin/perl -w

use strict;

package ReRix::Counters;

use ReRix::Shared;
use ReRix::Send;

# SCU Counters

my @count_columns = (
						'card_swipes',
						'bad_swipes',
						'no_data_swipes',
						'vends',
						'failed_auth_comm',
						'vend_auth_limit_hit',
						'num_of_attempted_comm',
						'invalid_card_bad_number',
						'invalid_card_expire',
						'invalid_card_not_valid',
						'checksum_errors',
						'cancelled_trans',
						'denied_cards',
						'fallbacks',
				);

my @count_desc = (
					'Card Swipes',
					'Bad Swipes',
					'No Data Swipes',
					'Vends',
					'Failed Auth Communications',
					'Vend Auth Hit Limit',
					'Number of Attempted Coms',
					'Invalid Cards - Bad Number',
					'Invalid Cards - Expired',
					'Invalid Cards - Not Valid',
					'Checksum Errors',
					'Cancelled Trans',
					'Denied Cards',
					'Fallbacks',
				);


sub parse_complete
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $message = substr MakeString($command_hashref->{inbound_command}), 1;

	my @counters;
	my @counts;

	my $j = 0;
	while( length($message) >= 4 )
	{
		push @counts, (unpack 'V', $message);
		$message = substr $message, 4;

		push @counters, $count_columns[$j++];
	}

	my $array_ref = $DATABASE->select(
				table			=> 'scu_counters_baseline',
				select_columns	=> join(', ', @counters),
				where_columns	=> [ 'machine_id = ?'],
				where_values	=> [ $command_hashref->{machine_id} ]
					);

	if( defined @$array_ref )
	{
		my @deltas;

		for( my $i = 0; $i < (@counters); $i++)
		{
			if( defined $array_ref->[0][$i] )
			{
				push @deltas, ($counts[$i] - $array_ref->[0][$i]);

				push @logs, "$count_desc[$i]: $counts[$i] - $array_ref->[0][$i] = $deltas[-1]";
			}
			else
			{
				push @deltas, '';

				push @logs, "$count_desc[$i]: $counts[$i] - X = X";
			}
		}

		$DATABASE->insert(
				table			=> 'scu_counters',
				insert_columns	=> join(', ', 'machine_id', @counters),
				insert_values	=> [ $command_hashref->{machine_id},
									map {length>5?'':$_} @deltas ]
				);
	}
	else
	{
		for( my $i = 0; $i < (@counters); $i++)
		{
			push @logs, "$count_desc[$i]: $counts[$i]";
		}
	}

	$DATABASE->insert(
				table			=> 'scu_counters_baseline',
				insert_columns	=> join(', ', 'machine_id', @counters),
				insert_values	=> [ $command_hashref->{machine_id},
									map {length>10?'':$_} @counts ]
				);

	$DATABASE->insert(
				table			=> 'machine_command',
				insert_columns	=> 'modem_id, command',
				insert_values	=> [ $command_hashref->{machine_id}, '332801']
				);

	return(\@logs);
}

sub parse_abbrev
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $message = substr MakeString($command_hashref->{inbound_command}), 1;

	my @counters;
	my @counts;

	my $j = 0;
	while( $message )
	{
		push @counts, (unpack 'C', $message);
		$message = substr $message, 1;

		push @counters, $count_columns[$j++];
	}

	my $array_ref = $DATABASE->select(
				table			=> 'scu_counters_baseline',
				select_columns	=> join(', ', @counters),
				where_columns	=> [ 'machine_id = ?'],
				where_values	=> [ $command_hashref->{machine_id} ]
					);

	if( defined @$array_ref )
	{
		my @deltas;

		for( my $i = 0; $i < (@counters); $i++)
		{
			if( defined $array_ref->[0][$i] )
			{
				$counts[$i] |= ($array_ref->[0][$i] & 0xffffff00);

				push @deltas, ($counts[$i] - $array_ref->[0][$i]);

				push @logs, "$count_desc[$i]: $counts[$i] - $array_ref->[0][$i] = $deltas[-1]";
			}
			else
			{
				push @logs, "Unable to complete abbreviated data";

				$DATABASE->insert(
							table			=> 'machine_command',
							insert_columns	=> 'modem_id, command',
							insert_values	=> [ $command_hashref->{machine_id},
												'5a0002']
					);

				return(\@logs);
			}
		}

		$DATABASE->insert(
				table			=> 'scu_counters',
				insert_columns	=> join(', ', 'machine_id', @counters),
				insert_values	=> [ $command_hashref->{machine_id},
									map {length>5?'':$_} @deltas ]
				);

		$DATABASE->insert(
				table			=> 'scu_counters_baseline',
				insert_columns	=> join(', ', 'machine_id', @counters),
				insert_values	=> [ $command_hashref->{machine_id},
									map {length>10?'':$_} @counts ]
				);
	}
	else
	{
		push @logs, "Unable to complete abbreviated data";

		$DATABASE->insert(
				table			=> 'machine_command',
				insert_columns	=> 'modem_id, command',
				insert_values	=> [ $command_hashref->{machine_id}, '5a0004']
				);
	}

	return(\@logs);
}

1;
