#!/usr/bin/perl -w

use strict;

package ReRix::Pricing;

use ReRix::Shared;
use ReRix::Send;

# Price Push Responses: g, G

sub parse_pos
{
	my ($DATABASE, $command_hashref) = @_;

	my (@logs);

	my $updater = ord( substr( MakeString( $command_hashref->{inbound_command})
											, 1, 1 ) );

	push @logs, "Price Change Recieved: $updater";

	$DATABASE->update(
					table			=> 'rerix_pricing_master',
					update_columns	=> 'PRICE_CHANGE_COMPLETE',
					update_values	=> ['Y'],
					where_columns	=> ['MACHINE_ID = ?',
										'PRICE_CHANGE_NUMBER = ?'],
					where_values	=> [$command_hashref->{machine_id},
										$updater]
				);

	return (\@logs);
}

sub parse_neg
{
	my ($DATABASE, $command_hashref) = @_;

	my (@logs);

	my $updater = ord( substr( MakeString( $command_hashref->{inbound_command})
											, 1, 1 ) );

	$DATABASE->update(
					table			=> 'rerix_pricing_master',
					update_columns	=> 'PRICE_CHANGE_COMPLETE',
					update_values	=> ['E'],
					where_columns	=> ['MACHINE_ID = ?',
										'PRICE_CHANGE_NUMBER = ?'],
					where_values	=> [$command_hashref->{machine_id},
										$updater]
				);
   
	my $message = 'Z' . chr(2);

	$DATABASE->insert(
					table			=> 'Machine_Command',
					insert_columns	=> 'Modem_ID, Command',
					insert_values	=> [$command_hashref->{machine_id},
										&MakeHex($message)]
				);

	return (\@logs);
}

sub parse_simple
{
    my ($DATABASE, $command_hashref) = @_;

    my (@logs);

	my $message = MakeString($command_hashref->{inbound_command});

	my (%columnprices);
	my ($price, $col1, $col2, $workingword);
	my ($list, $butcount);

	$message = 'x' . $message; # a hack to make the following loop work

	# The following messy line chops a 2byte word of $message (discarded) and 
	# then puts the first word in $workingbyte until there are no words
	# in message (So, the first two chars [the command and the 'x' above] are
	# discarded then one
	# at a time workingword is filled with each successive word until the
	# end of the message

	$butcount=1;
	$list = [];
	while ($workingword = 
				substr( $message = substr($message,2), 0, 2) )
	{
		# Price in simple mode is expressed as a number of nickels
		# We have to convert it to pennies here

		$price =  ord(substr($workingword,0,1)) * 5; 

		$workingword =ord(substr($workingword,1,1));     # dump price char

		# Each column ID is a 4 bits
		$col1 = $workingword & 0x0F;

		if ( defined $col1 ) 
		{
			$columnprices{$col1} = $price;
			push @{$list}, [$butcount, $col1, $price ];

			push @logs, "     But/Col/Price: $butcount/$col1/$price";
		}

		$col2 = (($workingword ) & 0xF0) >>4;
		if ( defined $col2) 
		{
			$columnprices{$col2} = $price;

			if( $col2 != $col1 )
			{
				push @{$list}, [$butcount, $col2, $price];
				push @logs, "     But/Col/Price: $butcount/$col2/$price";
			}
		}

		$butcount++;
	}

	if( (@$list) > 0 )
	{
		$DATABASE->insert(	table			=> 'Rerix_Pricing',
							insert_columns	=> 'machine_id, rerix_price',
							insert_values	=> [$command_hashref->{machine_id},
												$list] );
	}

	return (\@logs);
}

sub parse_complex
{
	my ($DATABASE, $command_hashref) = @_;

	my (@logs);

	my $message = MakeString($command_hashref->{inbound_command});

	my ($column, $workingbyte, $price);
	my ($list, $butcount);

	$butcount=1;
	$list = [];
	$message = substr($message,1); # chop off the command

	while (length $message)        # not at the end of the message
	{
		# 2 bytes are the price

		$price = (ord(substr($message,0,1)) << 8) + (ord(substr($message,1,1)));
		$message = substr($message,2);

		# until a column has the magic 1
		do 
		{
			$column = ord(substr($message,0,1));
			if ( defined($column & 0x7f) )
			{
				my $realcolumn = $column & 0x7f;

				push @{$list}, [$butcount, $realcolumn, $price];
				push @logs, "     But/Col/Price: $butcount/$realcolumn/$price";
			}
			$message = substr($message,1);
		} while ((not ($column & 0x80)) and length $message);#stop endless loops

		$butcount++;
	}

	if( (@$list) > 0 )
	{
		$DATABASE->insert(	table 			=> 'Rerix_Pricing',
							insert_columns	=> 'machine_id, rerix_price',
							insert_values	=> [$command_hashref->{machine_id},
												$list] );
	}

	return (\@logs);
}


1;
