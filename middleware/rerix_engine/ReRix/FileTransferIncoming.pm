#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		07/30/2003	pcowan		Creation of commands
#
#

package ReRix::FileTransferIncoming;

use strict;

use Evend::Database::Database;        # Access to the Database
use Evend::ReRix::Shared;
use ReRix::Send;
use ReRix::Utils;
use ReRix::CallInRecord;

my $incoming_root = "/opt/ReRixEngine2/incoming_files/";

sub start
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 
	my $device_id = $command_hashref->{device_id}; 
	my $device_type = $command_hashref->{device_type}; 
	
	my (undef, $num_packets, $num_bytes, $group_num, $file_type, $file_name) = unpack("CCNCCA*", $message);
	
    push @logs, "Num Packets       : $num_packets";
    push @logs, "Num Bytes         : $num_bytes";
    push @logs, "Group Number      : $group_num";
    push @logs, "File Type         : $file_type";
    push @logs, "File Name         : $file_name";

	my $time_now = time();
	
	# create a unique temp directory to hold the transfer
	my $dir = $incoming_root . $machine_id . "_" . $group_num;
	mkdir($dir, 0755);

	&startDataFile($dir);
	&startControlFile($dir, time(), $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, "-1");
	push @logs, "Started Data and Control File; expecting $num_packets packets";

	if($device_type =~ /0|1/ && $file_type eq '2')
	{
		# this is the start of a Peek, which was requested from a pending message...
		# mark the pending message ack'd
		
		push @logs, "Peek Response     : Removing any pending Peek requests";
		# we don't know the pending message ID, so just update any pending pokes
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['A'],
							where_columns	=> ['machine_id = ?', 'data_type = ?'],
							where_values	=> [$machine_id, '87'] );
	}

	if($device_type =~ /0|1/ && $file_type eq '1')
	{
		# this could be the result of a Peek for the entire config
		# mark the pending message ack'd
		
		push @logs, "Config Transfer   : Removing any pending Peeks for entire config";
		# we don't know the pending message ID, so just update any pending pokes
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['A'],
							where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?'],
							where_values	=> [$machine_id, '87', '4200000000000000FF'] );
	}

	if($device_type eq '5' && $file_type eq '4')
	{
		# if this is an eSuds device and the incoming file is an eSuds generic file, it may have been 
		# sent as a response to an eSuds file request message (9B)
		# so remove any 9B messages for this file name
		
		my $command = unpack("H*", $file_name);
		
		push @logs, "eSuds File        : Removing any requests for file: $file_name";
		# we don't know the pending message ID, so just update any pending pokes
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['A'],
							where_columns	=> ['machine_id = ?', 'data_type = ?', 'upper(command) = ?'],
							where_values	=> [$machine_id, '9B', uc($command)] );
	}

	# pack a response and save it
	my $response = pack("CCC", $response_no, 0x7D, $group_num);
	push @logs, "Response          : " . unpack("H*", $response);
	
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, &MakeHex($response)] );
	
	return(\@logs);
}

sub transfer
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 
	my $device_type = $command_hashref->{device_type}; 
	my $ssn = $command_hashref->{SSN}; 

	my (undef, $group_num, $incoming_packet_num, $payload) = unpack("CCCa*", $message);
	
	push @logs, "Group Number      : $group_num";
	push @logs, "Packet Number     : $incoming_packet_num";
	#push @logs, "Payload           : " . unpack("H*", $payload);
	
	my $dir = $incoming_root . $machine_id . "_" . $group_num;
	my ($start_time, $device_id, $stored_machine_id, $num_packets, $num_bytes, $stored_group_num, $file_type, $file_name, $stored_packet_num) = &readControlFile($dir);
	if(not defined $start_time)
	{
		# problem reading control file
		ReRix::Utils::send_alert("FileTransferIncoming: ERROR: Problem reading control file for $dir", $command_hashref);
		push @logs, "ERROR: Problem reading control file for $dir";
		return(\@logs);
	}
	
	push @logs, "File Name         : $file_name";
	
	my $ctrl_file = "$dir/control";

	my $response;
	my $success = 0;
	
	if($stored_packet_num >= $incoming_packet_num)
	{
		# duplicate packet - we've already see this packet, so ack it but don't write the 
		# data and don't update control file
		push @logs, "Dupe packet       : $stored_packet_num >= $incoming_packet_num";
		$response = pack("CCCC", $response_no, 0x7F, $group_num, $incoming_packet_num);
	}
	elsif($stored_packet_num == ($incoming_packet_num-1))
	{
		# next packet in sequence - append the data, update the control file to this packet 
		# num, ack this packet num
		push @logs, "Packet received   : " . ($incoming_packet_num+1) . " of " . $num_packets;
		$response = pack("CCCC", $response_no, 0x7F, $group_num, $incoming_packet_num);
		&updateControlFile($dir, $incoming_packet_num);
		&updateDataFile($dir, $payload);
		$success = 1;
	}
	elsif($stored_packet_num < 0)
	{
		# we missed a packet, and have never received a packet - resend the file transfer start ACK
		push @logs, "Missed a transfer packet: $stored_packet_num < $incoming_packet_num";
		push @logs, "Resending File Transfer Start ACK";
		$response = pack("CCC", $response_no, 0x7D, $group_num);
	}
	else
	{
		# we missed a packet - don't write the data, don't update control file, ack the stored
		# packet num to get the client back in sync
		push @logs, "Missed a transfer packet: $stored_packet_num < $incoming_packet_num";
		$response = pack("CCCC", $response_no, 0x7F, $group_num, $stored_packet_num);
	}
	
	if((($incoming_packet_num+1) == $num_packets) && ($success == 1))
	{
		# This was the last packet.  
		# Packet numbers must start at zero.  
		# Write file to database.  
		# Cleanup temp files and directory
		
		push @logs, "Last packet       : Transfer Is Complete!";
		
		# Check if all the data was received
		
		my $data_file = "$dir/data";
		my ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev, $size, $atime, $mtime, $ctime, $blksize, $blocks) = stat($data_file);
		
		if($num_bytes == $size)
		{
			push @logs, "Correct num bytes : $size";
		}
		else
		{
			push @logs, "WARNING: Received $size bytes, but expecting $num_bytes bytes";
		}
		
		# get the accumulated data
		my $data = &readDataFile($dir);
		
		if($device_type =~ /0|1/ && $file_type eq '1')
		{
			# special case for G4
			# check if there are pending Pokes for this unit and don't save if there are - if we 
			# store the incoming config but we have pokes pending, we will poke back the config 
			# data it just transfered
			
			# for changes to take effect on the unit, the operator must call it in first then make changes
			
			my $check_for_pokes_ref = $DATABASE->select(
									table			=> 'machine_command_pending',
									select_columns	=> 'count(1)',
									where_columns	=> ['machine_id = ?', 'data_type = ?', '(execute_cd = ? or (execute_cd = ? and execute_date < (sysdate-(90/86400))))'],
									where_values	=> [$machine_id, '88', 'P', 'S'] );
			
			my $poke_count = $check_for_pokes_ref->[0][0];
			if($poke_count > 0)
			{
				push @logs, "Gx Config         : Exists... NOT Overwriting $file_name; Pokes already exists for current config";
				
				rename($ctrl_file, ($ctrl_file .'.'. time()));
				rename($data_file, ($data_file .'.'. time()));
				
				push @logs, "Response          : " . unpack("H*", $response);
				
				$DATABASE->insert(	table=> 'Machine_Command',
							insert_columns=> 'Modem_ID, Command',
							insert_values=> [$machine_id, &MakeHex($response)] );
							
				return(\@logs);	
			}
		}
		
		if($device_type =~ /0|1/ && $file_type eq '2')
		{
			# special case for Peeks from a G4 - if it's sending version info, write it to the 
			# device_setting table so an admin can see it on the website
			
			if(substr($data, 0, 4) eq 'USA-')
			{
				my $version = $data;
				
				# trim spaces
				$version =~ s/\s+$//;
				$version =~ s/^\s+//;
				
				my $setting_name = 'Firmware Version';
			
				# first check if Firmware Version setting exists
				my $check_setting_ref = $DATABASE->select(
										table			=> 'device_setting',
										select_columns	=> 'count(1)',
										where_columns	=> ['device_id = ?', 'device_setting_parameter_cd = ?'],
										where_values	=> [$device_id, $setting_name] );
			
				if($check_setting_ref->[0][0] == 0)
				{
					push @logs, "G4 Version        : Inserting $setting_name = $version";
					
					# insert version
					$DATABASE->insert(	table=> 'device_setting',
										insert_columns=> 'device_id, device_setting_parameter_cd, device_setting_value',
										insert_values=> [$device_id, $setting_name, $version] );
				}
				else
				{
					push @logs, "G4 Version        : Updating $setting_name = $version";
					
					# update version
					$DATABASE->update(	table			=> 'device_setting',
										update_columns	=> 'device_setting_value',
										update_values	=> [$version],
										where_columns	=> ['device_id = ?', 'device_setting_parameter_cd = ?'],
										where_values	=> [$device_id, $setting_name] );
				}
			}
		}
		
		if($file_type eq '12')
		{
			# email this file out; addresses are listed on the first line, comma delimited
			my @data_array = split(/\n/, $data);
			my @emails_array = split(/,/, $data_array[0]);
			my $message = join("\n", @data_array[1..$#data_array]);

			my $subject = "File received from $ssn on " . localtime();
			
			foreach my $to_addr (@emails_array)
			{
				$DATABASE->insert(	table=> 'ob_email_queue',
						insert_columns=> 'OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG',
						insert_values=> ['rerixengine@usatech.com', 'ePort Network Protocol Server', $to_addr, $to_addr, $subject, $message]);
				
				push @logs, "Auto-Email File   : Inserted email to $to_addr: $subject";
			}
		}
		
		# check if the file exists
		my $file_transfer_id;

		my $check_exists_ref = $DATABASE->select(
					table			=> 'file_transfer',
					select_columns	=> 'file_transfer_id',
					where_columns	=> ['file_transfer_name = ?'],
					where_values	=> [$file_name] );
		
		if($device_type =~ /0|1/ && $file_type eq '1' && (defined $check_exists_ref->[0][0]))
		{
			# special case for g4's - if it just transfered it's config, overwrite the current config 
			# file machine_id-CFG with this new config file
			
			$file_transfer_id = $check_exists_ref->[0][0];
			push @logs, "Gx Config         : Exists... Overwriting file_transfer_id $file_transfer_id";
		}
		else
		{
			# Get a new File_Transfer ID
			my $file_transfer_seq_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'device.SEQ_FILE_TRANSFER_ID.NEXTVAL' );
	
			if(not $file_transfer_seq_ref->[0])
			{
				push @logs, "Failed to query file_transfer sequence for a new ID!";
				return(\@logs);
			}
			
			$file_transfer_id = $file_transfer_seq_ref->[0][0];
			push @logs, "New Transfer ID   : $file_transfer_id";
		
			# insert the file, don't insert the content because we need to use blob_insert
			$DATABASE->insert(	table=> 'device.file_transfer',
								insert_columns=> 'file_transfer_id, file_transfer_name, file_transfer_type_cd',
								insert_values=> [$file_transfer_id, $file_name, $file_type]);
		}
	
		if($device_type =~ /0|1/ && $file_type eq '0')
		{
			# export the DEX file
			push @logs, "-- Exporting G4 DEX Record for $machine_id --";
			push @logs, @{&ReRix::G4Export::createG4ExportDEXRec($DATABASE, $machine_id, $device_id, $ssn, $data)}; 
		}
		
		# 01/09/04 - pwc - Added call_in_record logging
		if($file_type eq '0')
		{
			&ReRix::CallInRecord::dex_received($raw_handle, $machine_id, length($data));
		}
		elsif($file_type eq '1')
		{
			&ReRix::CallInRecord::device_sent_config($raw_handle, $machine_id);
		}
		
		# insert doesn't work right for blobs, so do an update with using blobInsert
		&blob_insert($raw_handle, "update device.file_transfer set file_transfer_content = :blob where file_transfer_id = $file_transfer_id", $data);
		
		# insert the device_file_transfer
		$DATABASE->insert(	table=> 'device.device_file_transfer',
							insert_columns=> 'device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd',
							insert_values=> [$device_id, $file_transfer_id, 'I', '1']);
		
		push @logs, "Stored file       : $file_transfer_id $file_name";
				
		# do we really want to delete the files?
		# just rename them for now
		
		my $ctrl_file = "$dir/control";
		
		rename($ctrl_file, ($ctrl_file .'.'. time()));
		rename($data_file, ($data_file .'.'. time()));
	}
	
	push @logs, "Response          : " . unpack("H*", $response);
	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, &MakeHex($response)] );
	
	return(\@logs);
}

sub kill_transfer
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 
	my $device_id = $command_hashref->{device_id}; 

	my (undef, $group_num) = unpack("CC", $message);

	push @logs, "Group Number      : $group_num";
	
	my $dir = $incoming_root . $machine_id . "_" . $group_num;
	my $ctrl_file = "$dir/control";
	my $data_file = "$dir/data";		
	
	rename($ctrl_file, ($ctrl_file .'.'. time()));
	rename($data_file, ($data_file .'.'. time()));

	# pack a response and save it
	my $response = pack("CCC", $response_no, 0x81, $group_num);

	push @logs, "Response          : " . unpack("H*", $response);

	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, &MakeHex($response)] );
	
	return(\@logs);
}

sub readControlFile
{
	my ($dir) = @_;
	my $ctrl_file = "$dir/control";
	if(not -f $ctrl_file)
	{
		return (undef);
	}
	
	open(CTRLFILE, "<$ctrl_file");
	
	my $start_time = readline(CTRLFILE);
	my $device_id = readline(CTRLFILE);
	my $machine_id = readline(CTRLFILE);
	my $num_packets = readline(CTRLFILE);
	my $num_bytes = readline(CTRLFILE);
	my $group_num = readline(CTRLFILE);
	my $file_type = readline(CTRLFILE);
	my $file_name = readline(CTRLFILE);
	my $packet_num = readline(CTRLFILE);
	
	chomp($start_time);
	chomp($device_id);
	chomp($machine_id);
	chomp($num_packets);
	chomp($num_bytes);
	chomp($group_num);
	chomp($file_type);
	chomp($file_name);
	chomp($packet_num);
	
	close(CTRLFILE);
	
	return ($start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num);
}

sub startControlFile
{
	my ($dir, $start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num) = @_;

	my $ctrl_file = "$dir/control";
	if(-f $ctrl_file)
	{
		rename($ctrl_file, ($ctrl_file .'.'. time()));
	}
	
	# write the control file to hold temp data
	open(CTRLFILE, ">$ctrl_file");
	print CTRLFILE "$start_time\n";		# start time of transfer
	print CTRLFILE "$device_id\n";		# device id
	print CTRLFILE "$machine_id\n";		# machine EV number
	print CTRLFILE "$num_packets\n";	# number of packets to expect
	print CTRLFILE "$num_bytes\n";		# number of bytes to expect
	print CTRLFILE "$group_num\n";		# transfer group number
	print CTRLFILE "$file_type\n";		# file type ID
	print CTRLFILE "$file_name\n";		# real file name - may include fully qualified path
	print CTRLFILE "$packet_num\n";		# last packet num transfered
	close(CTRLFILE);
}

sub updateControlFile
{
	my ($dir, $new_packet_num) = @_;
	
	my ($start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num) = readControlFile($dir);
	if(not defined $start_time)
	{
		return undef;
	}
	
	$packet_num = $new_packet_num;
	
	my $ctrl_file = "$dir/control";
	open(CTRLFILE, ">$ctrl_file");
	print CTRLFILE "$start_time\n";		# start time of transfer
	print CTRLFILE "$device_id\n";		# device ID
	print CTRLFILE "$machine_id\n";		# machine EV number
	print CTRLFILE "$num_packets\n";	# number of packets to expect
	print CTRLFILE "$num_bytes\n";		# number of bytes to expect
	print CTRLFILE "$group_num\n";		# transfer group number
	print CTRLFILE "$file_type\n";		# file type ID
	print CTRLFILE "$file_name\n";		# real file name - may include fully qualified path
	print CTRLFILE "$packet_num\n";		# last packet num transfered
	close(CTRLFILE);
}

sub startDataFile
{
	my ($dir) = @_;

	my $data_file = "$dir/data";
	if(-f $data_file)
	{
		rename($data_file, ($data_file .'.'. time()));
	}
	
	open(DATAFILE, ">$data_file");
	close(DATAFILE);
}

sub updateDataFile
{
	my ($dir, $payload) = @_;

	my $data_file = "$dir/data";
	
	open(DATAFILE, ">>$data_file");
	#binmode(DATAFILE);
	#syswrite(DATAFILE, $payload, length($payload), -1);
	print DATAFILE $payload;
	close(DATAFILE);
}

sub readDataFile
{
	my ($dir) = @_;
	my $data_file = "$dir/data";	
	my $buf;
	open(DATAFILE, "<$data_file");
	#binmode(DATAFILE);
	#sysread(DATAFILE, $buf, 5000000);
	read(DATAFILE, $buf, 5000000);
	close(DATAFILE);
	return $buf;
}

sub blob_insert
{
	my ($raw_handle, $sql, $blob) = @_;
	
	my $LONG_RAW_TYPE=24;
	my %attrib;
	$attrib{'ORA_TYPE'} = $LONG_RAW_TYPE;

	my $stmt = $raw_handle->prepare($sql);
	$stmt->bind_param(":blob", unpack("H*", $blob), \%attrib); 
	$stmt->execute();
	
	if(defined $raw_handle->errstr)
	{
		print "blob_insert error: " . $raw_handle->errstr . " ($sql)\n";
	}
}

1;
