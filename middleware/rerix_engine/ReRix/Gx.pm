#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		07/30/2003	pcowan		Creation of commands:
#

package ReRix::Gx;

use strict;

use Evend::Database::Database;        # Access to the Database
use Evend::ReRix::Shared;
use ReRix::Send;

sub counters
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no}; 
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $response_no = $command_hashref->{response_no}; 
	
	if(not defined $device_id)
	{
		push @logs, "No device record found for $machine_id";
		return(\@logs);	
	}
	
	my $currency_transaction_counter = &pbcd2num(substr($message, 1, 4));
	my $currency_money_counter       = &pbcd2num(substr($message, 5, 4));
	
	my $cashless_transaction_counter = &pbcd2num(substr($message, 9, 4));
	my $cashless_money_counter       = &pbcd2num(substr($message, 13, 4));
	
	my $passcard_transaction_counter = &pbcd2num(substr($message, 17, 4));
	my $passcard_money_counter       = &pbcd2num(substr($message, 21, 4));

	my $total_bytes = &pbcd2num(substr($message, 25, 4));
	my $total_sessions_attempted = &pbcd2num(substr($message, 29, 4));

	#my $reserved = &pbcd2num(substr($message, 33, 4));
	
	push @logs, "Currency Transactions     : $currency_transaction_counter";
	push @logs, "Currency Money            : $currency_money_counter";
	push @logs, "Cashless Transactions     : $cashless_transaction_counter";
	push @logs, "Cashless Money            : $cashless_money_counter";
	push @logs, "Passcard Transactions     : $passcard_transaction_counter";
	push @logs, "Passcard Money            : $passcard_money_counter";
	push @logs, "Total Bytes               : $total_bytes";
	push @logs, "Total Sessions Attempted  : $total_sessions_attempted";
	
	&insert_counter($device_id, "CURRENCY_TRANSACTION", $currency_transaction_counter, $DATABASE);
	&insert_counter($device_id, "CURRENCY_MONEY", $currency_money_counter, $DATABASE);
	&insert_counter($device_id, "CASHLESS_TRANSACTION", $cashless_transaction_counter, $DATABASE);
	&insert_counter($device_id, "CASHLESS_MONEY", $cashless_money_counter, $DATABASE);
	&insert_counter($device_id, "PASSCARD_TRANSACTION", $passcard_transaction_counter, $DATABASE);
	&insert_counter($device_id, "PASSCARD_MONEY", $passcard_money_counter, $DATABASE);
	&insert_counter($device_id, "TOTAL_BYTES", $total_bytes, $DATABASE);
	&insert_counter($device_id, "TOTAL_SESSIONS", $total_sessions_attempted, $DATABASE);
	
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	$DATABASE->insert(	table=> 'machine_command',
						insert_columns=> 'modem_id, command',
						insert_values=> [$machine_id, unpack("H*", $response)] );
	
	return(\@logs);	
}

sub make_poke
{
	my (@logs);
	my ($DATABASE, $command_hashref, $response_no, $pending_id, $memory_code, $memory_location, $poke_size) = @_;

	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id};
	
	# use blobSelect because the column is a LONG and it won't come back otherwise
	my $raw_handle = $DATABASE->{handle};
	my $poke_data = &blob_select_config($raw_handle, $machine_id);
	
	if(not defined $poke_data)
	{
		push @logs, "Config            : Config not found for $machine_id" ;
		
		$poke_data = &blob_select_config($raw_handle, "GX-DEFAULT");
		if(not defined $poke_data)
		{
			ReRix::Utils::send_alert("Gx: Config            : Config not found for GX-DEFAULT!", $command_hashref);
			push @logs, "Config            : Config not found for GX-DEFAULT!" ;
			return(\@logs);
		}
		else
		{
			push @logs, "Config            : Using GX-DEFAULT config" ;
		}
	}

	# Data in file_transfer_content coulumn is stored HEX encoded
	$poke_data = pack("H*", $poke_data);
	
	my $total_bytes = length($poke_data);
	
	# the G4 config file uses 2 byte memory addressing
	my $pos = ($memory_location*2);
	
	push @logs, "Memory Location   : $memory_location";
	push @logs, "File Location     : $pos";
	
	my $chunk;
	
	if(($pos+$poke_size) > $total_bytes)
	{
		# this should be the last Poke
		$chunk = substr($poke_data, $pos);
	}
	else
	{
		$chunk = substr($poke_data, $pos, $poke_size);
	}
		
	my $response = pack('CCCNa*', $response_no, 0x88, ord("B"), $memory_location, $chunk);
	push @logs, "Response          : " . unpack("H*", $response);

	$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*", $response)] );
						
	return(\@logs);	
}

sub insert_counter
{
	my ($device_id, $name, $value, $DATABASE) = @_;
	
	$DATABASE->insert(	table=> 'device_counter',
						insert_columns=> 'device_id, device_counter_parameter, device_counter_value',
						insert_values=> [$device_id, $name, $value] );
}

sub pbcd2num
{
	my $pbcd = shift;
	
	if(not $pbcd)
	{
		return 0;
	}
	
	my $num = 0;
	foreach my $digit (unpack("C4",$pbcd))
	{
		my ($lsn,$msn) = ($digit % 16,int $digit / 16);
		$num = ($num * 10 + $msn) * 10 + $lsn;
	};
	return $num;
};

sub num2pbcd
{
	my ($num,$pair) = (shift,undef);
	my @digits = ();
	foreach my $i (0..3)
	{
		($num,$pair) = (int $num/100,$num % 100);
		my ($lsn,$msn) = ($pair % 10,int $pair/10);
		unshift(@digits,$msn * 16 + $lsn);
	};
	return pack('C4',@digits);
}; 

sub blob_select_config
{
	my ($db, $machine_id) = @_;
	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB
	my $stmt = $db->prepare(q{select file_transfer_content from file_transfer where file_transfer_name = :file_name });
	$stmt->bind_param(":file_name", "$machine_id-CFG");

	$stmt->execute();

	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}


1;
