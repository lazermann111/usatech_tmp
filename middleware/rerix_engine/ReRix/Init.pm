#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00
# 1.10		04/01/2004	erybski		Device firmware version query (G4, eSuds)
# 1.11		06/02/2004	pcowan		Moved gen_random_key to Utils.pm
# 1.12		08/09/2004	pcowan		Prepend "M1" to MEI serial numbers
#

use strict;

package ReRix::Init;

use Evend::ReRix::Shared;
use ReRix::Send;
use ReRix::CallInRecord;
use ReRix::ClientVersion;
use ReRix::Utils;

# Initialization Command: I

sub parse
{
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my @logs;

	my ($serial, $model, $build, $config, $workingbyte, $uniq);
	my ($start, $freq);
	my $array_ref;

	$array_ref = $DATABASE->select(
					table          => 'rerix_modem_to_serial',
					select_columns => 'network, modem_id, machine_index',
					where_columns  => [ 'machine_id = ?'],
					where_values => [ $command_hashref->{machine_id} ]
				);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_id and machine_index in rerix_modem_to_serial'] );
	}

	my $network = $array_ref->[0][0];
	my $modem_id = $array_ref->[0][1];
	my $machine_index = $array_ref->[0][2];

	my $message = MakeString( $command_hashref->{inbound_command} );

	$message = substr($message,1);        # chop 'I'

	$config = ord(substr($message,0,1));  # Get config

	$message = substr($message,1);        # chop config

	($serial, $message) =  &parse_byteblock($message); # get serial number
	($model, $message) =   &parse_byteblock($message); # Get Model Number
	($build, $message) =   &parse_byteblock($message); # get Build number

	$uniq =  $message;    # get the Silicon Serial Number

	push @logs, "Supplied Serial Number : $serial";

	if (substr($uniq,0,4) eq chr(0)x4)
	{
		# there has been some weird problem with the init of the SSN check
		# to see if it is a repeat
		$array_ref = $DATABASE->select(
						table			=> 'rerix_initialization',
						select_columns	=> 'SILICON_SERIAL_NUMBER',
						where_columns	=> [ 'MODEM_ID = ?'],
						where_values	=> [ $modem_id ]
					); 

		if (not substr($array_ref->[0][0],0,4) eq '0000') 
		{
			# FIXME: inserts to machine_command using the modem_id don't work
			$DATABASE->insert(
						table           => 'Machine_Command',
						insert_columns  => 'Modem_ID, Command, execute_cd',
						insert_values   => [$command_hashref->{machine_id},
											&MakeHex('e' . chr(1)), 'N']
					);

			if (not length $uniq)
			{
				$uniq = chr(0)x8; 
			}

			$DATABASE->insert(
							table          => 'rerix_initialization',
							insert_columns => 'MODEM_ID, SERIAL_NUMBER, ' .
												'SILICON_SERIAL_NUMBER',
							insert_values  => [$modem_id, $serial,
												&MakeHex($uniq)]
						);
			return;
		}
	}


	if( !( (length($serial) > 2) and (substr($serial, 0, 2) eq 'EV') ) )
	{
		# If the machine did not send us an EV number, get one for it

		push @logs, "Getting New Serial number for " .
							"$network:$modem_id:$machine_index";

		$array_ref = $DATABASE->select(
					table			=> 'dual',
					select_columns	=> 'rerix_machine_id');

		$serial = $array_ref->[0][0];
	}
	else
	{
		# If we're taking the serial number from another network, HUP it
		$array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'network, modem_id, machine_index',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $serial ]
				);

		if( $array_ref->[0][0] ne $network )
		{
			$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, execute_cd',
				insert_values	=> [$serial, "H$array_ref->[0][0]"]
				);
		}
	}

	$DATABASE->insert(
				table			=> 'Rerix_modem_to_serial',
				insert_columns	=> 'modem_id, machine_id, ' .
									'network,  MACHINE_INDEX',
				insert_values	=> [$modem_id, $serial,
									$network, $machine_index]);

	# HUP the server for this network, since we might have changed
	# rerix_modem_to_serial
	$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, execute_cd',
				insert_values	=> [$command_hashref->{machine_id}, "H$network"]
				);

	if (length $uniq > 8)
	{
		# Trime the SSN to 8 bytes
		$uniq = substr($uniq,0,8);
	}
	elsif (length $uniq < 8)
	{
		# add starting 0s to a short SSN
		$uniq = (chr(0) x (8 - length $uniq)) . $uniq;
	}

	if ( $model eq "\x00" )
	{
		$model = ''; 
	}

	if ( $build eq "\x00" )
	{
		$build = ''; 
	}

	$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, Command, execute_cd',
				insert_values	=> [$serial,
									&MakeHex( chr(ord('i')) . "$serial" ),
									'N']
				);

	$DATABASE->insert(
				table          => 'rerix_initialization',
				insert_columns => 'MODEM_ID, SERIAL_NUMBER, MODEL_NUMBER, ' .
									'BUILD_STANDARD, SILICON_SERIAL_NUMBER, ' .
									'DECIMAL_POINT_POSITION, COIN_MECH_TYPE, ' .
									'ESCROW_MODE',
				insert_values  => [$modem_id, $serial, $model, $build,
									&MakeHex($uniq),
									((0x01 | 0x02) & $config),
									((0x04 & $config)?1:0),
									((0x08|0x10) & $config) >> 4 ]
			); 

	# Got all the data - We now need to return the 'T' command
	$array_ref = $DATABASE->select(
					table			=> 'rerix_machine',
					select_columns	=> 'STARTING_COMM_TIME,  COMM_FREQUENCY',
					where_columns	=> [ 'MODEM_ID= ?'],
					where_values	=> [$modem_id]
				);

	if (defined @{$array_ref})
	{
		$start = $array_ref->[0][0];
		$freq =  $array_ref->[0][1];

		# Set up some defaults
		if (not defined $start)
		{
			$start = int(rand 37);		# default to a random dialin time
		}

		if (not defined $freq)
		{
			$freq=24;					# default to once a day
		}
	}
	else
	{
		# Set up some defaults

		$start = int(rand 37);
		$freq=24; 

		$DATABASE->insert(
					table			=> 'rerix_machine',
					insert_columns	=> 'MODEM_ID, STARTING_COMM_TIME, ' .
										'COMM_FREQUENCY',
					insert_values	=> [$modem_id, $start, $freq],
					);
	}

	ReRix::Send::Time($DATABASE, $serial, $start, $freq);

	$message = "Z" . chr(91);
	$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, Command',
				insert_values	=> [$serial,
									&MakeHex($message)]
				);

	push @logs, "Assigned Serial Number : $serial";
	push @logs, "Machine Model Number   : $model";
	push @logs, ("     Machine Build Number   : " . (&MakeHex($build)));
	push @logs, ("     Machine SS Number      : " . (&MakeHex($uniq)));
	
	&ReRix::CallInRecord::device_initialized($raw_handle, $serial);

	return (\@logs);
}

sub parse_V2
{
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my @logs;

	my ($serial, $model, $build, $config, $workingbyte, $uniq);
	my ($start, $freq);
	my $array_ref;

	$array_ref = $DATABASE->select(
					table          => 'rerix_modem_to_serial',
					select_columns => 'network, modem_id, machine_index',
					where_columns  => [ 'machine_id = ?'],
					where_values => [ $command_hashref->{machine_id} ]
				);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_it and machine_index in rerix_modem_to_serial'] );
	}


	my $network = $array_ref->[0][0];
	my $modem_id = $array_ref->[0][1];
	my $machine_index = $array_ref->[0][2];

	my $message = MakeString( $command_hashref->{inbound_command} );

	$message = substr($message,1);        # chop 0xb1

	$config = ord(substr($message,0,1));  # Get config

	$message = substr($message,1);        # chop config

	push @logs, "The message before using parse_byteblock (asc) : $message";
	push @logs, "The message before using parse_byteblock (hex) : " . unpack("H*",$message);
	
	($serial, $message) =  &parse_byteblock($message); # get serial number
	push @logs, "   serial (asc) : $serial";
	push @logs, "   serial (hex) : " . unpack("H*",$serial);
	
	($model, $message) =   &parse_byteblock($message); # Get Model Number
	push @logs, "    model (asc) : $model";
	push @logs, "    model (hex) : " . unpack("H*",$model);

	($build, $message) =   &parse_byteblock($message); # get Build number
	push @logs, "    build (asc) : $build";
	push @logs, "    build (hex) : " . unpack("H*",$build);

	#$uniq =  $message;    # get the Silicon Serial Number
	#($uniq, $message) =   &parse_byteblock($message); # get Silicon Serial Number
	$uniq =  $message;    # get the Silicon Serial Number

	push @logs, "Supplied Serial Number : $serial";

	if( !( (length($serial) > 2) and (substr($serial, 0, 2) eq 'EV') ) )
	{
		# If the machine did not send us an EV number, get one for it

		push @logs, "Getting New Serial number for " .
							"$network:$modem_id:$machine_index";

		$array_ref = $DATABASE->select(
					table			=> 'dual',
					select_columns	=> 'tazdba.rerix_machine_id_v2');

		$serial = $array_ref->[0][0];
		push @logs, "The new serial number is $serial";
	}
	else
	{
		# If we're taking the serial number from another network, HUP it
		push @logs, "We're taking the serial number ($serial) from another network, HUP it " .
							"$network:$modem_id:$machine_index";
		$array_ref = $DATABASE->select(
					table			=> 'rerix_modem_to_serial',
					select_columns	=> 'network, modem_id, machine_index',
					where_columns	=> [ 'machine_id = ?'],
					where_values	=> [ $serial ]
				);

		if( $array_ref->[0][0] ne $network )
		{
			$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, execute_cd',
				insert_values	=> [$serial, "H$array_ref->[0][0]"]
				);
		}
	}

	$DATABASE->insert(
				table			=> 'Rerix_modem_to_serial',
				insert_columns	=> 'modem_id, machine_id, ' .
									'network,  MACHINE_INDEX',
				insert_values	=> [$modem_id, $serial,
									$network, $machine_index]);

	# HUP the server for this network, since we might have changed
	# rerix_modem_to_serial
	$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, execute_cd',
				insert_values	=> [$command_hashref->{machine_id}, "H$network"]
				);

	if (length $uniq > 8)
	{
		# Trime the SSN to 8 bytes
		$uniq = substr($uniq,0,8);
	}
	elsif (length $uniq < 8)
	{
		# add starting 0s to a short SSN
		$uniq = (chr(0) x (8 - length $uniq)) . $uniq;
	}

	if ( $model eq "\x00" )
	{
		$model = ''; 
	}

	if ( $build eq "\x00" )
	{
		$build = ''; 
	}

	my $msg_no = 1;	#the msgnbr has only a trivial implementation currently
	my $datatype = 0x69;
	my $response = &MakeHex(pack("CCA*", $msg_no, $datatype, $serial)) ;
	

	$DATABASE->insert(
				table			=> 'Machine_Command',
				insert_columns	=> 'Modem_ID, Command, execute_cd',
				insert_values	=> [$serial,
									($response),
									'N']
				);

	$DATABASE->insert(
				table          => 'rerix_initialization',
				insert_columns => 'MODEM_ID, SERIAL_NUMBER, MODEL_NUMBER, ' .
									'BUILD_STANDARD, SILICON_SERIAL_NUMBER, ' .
									'DECIMAL_POINT_POSITION, COIN_MECH_TYPE, ' .
									'ESCROW_MODE',
				insert_values  => [$modem_id, $serial, $model, $build,
									&MakeHex($uniq),
									((0x01 | 0x02) & $config),
									((0x04 & $config)?1:0),
									((0x08|0x10) & $config) >> 4 ]
			); 

	# if this is a Sony PictureStation insert a record into the Machine_Hierarchy table 
	if($model =~ m/Sony/)
	{
		$DATABASE->insert(
				table          => 'machine_hierarchy',
				insert_columns => 'machine_id, group_cd, level_0, level_1, level_2, level_3',
				insert_values  => [$serial,'SONY','ALL0','ALL1','ALL2','SONY']
			);
	}

	
	push @logs, "Message Number (asc)        : $msg_no";
	push @logs, "Message Number (hex)        : " . unpack("H", $msg_no);
	push @logs, "Assigned Serial Number      : $serial";
	push @logs, "Machine Model Number        : $model";
	push @logs, "     Machine Build Number   : " . (&MakeHex($build));
	push @logs, "     Machine SS Number      : " . (&MakeHex($uniq));
	push @logs, "     Response (hex)         : " . unpack("H*", $response);
	push @logs, "     Response (asc)         : $response";

	&ReRix::CallInRecord::device_initialized($raw_handle, $serial);

	return (\@logs);
}

sub parse_V3
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $msg_no = $command_hashref->{msg_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $incoming_id = $command_hashref->{machine_id}; 
	
	my $device_type = ord(substr($message, 1, 1));
	my $SSN = substr($message, 2);
	
	my $device_id;
	my $ev_number;
	my $key;
	my $active_flag;

	push @logs, "Device Type       : $device_type";
	push @logs, "SSN               : $SSN";
	
	if($device_type eq '6' && length($SSN) == 6)
	{
		$SSN = 'M1' . $SSN;
		push @logs, "SSN               : $SSN (Adjusted for MEI Telemeter)";
	}
	
	my $lookup_device_ref = $DATABASE->select(
								table			=> 'device',
								select_columns	=> 'device_id, device_name, device_type_id, encryption_key, device_active_yn_flag',
								order			=> 'device_active_yn_flag desc',
								where_columns	=> [ 'device_serial_cd = ?' ],
								where_values	=> [ $SSN ] );
								
	if(not $lookup_device_ref->[0])
	{
		push @logs, "No device record found for $SSN";
		
		# generate new EV
		my $new_ev_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'rerix_machine_id_v3');

		$ev_number = $new_ev_ref->[0][0];
		push @logs, "New EV Number     : $ev_number";
		
		# generate new encryption key
		$key = &ReRix::Utils::gen_random_key($SSN);
		
		push @logs, "New Encryption Key: $key";
		
		$active_flag = 'Y';
		if($device_type eq 5)
		{
			# ESuds devices can initialize, but they will not be set active and will not get a response until
			# customer service sets the active flag = Y, then we will respond to the device 
			
			# Changed 03/02/2004 - eSuds devices are now immediatly
			#$active_flag = 'N';
			$active_flag = 'Y';
		}
		
		$DATABASE->insert(
					table			=> 'device',
					insert_columns	=> 'device_name, device_type_id, device_serial_cd, device_active_yn_flag, encryption_key',
					insert_values	=> [$ev_number, $device_type, $SSN, $active_flag, $key]);
					
		# do this query again, it should work now
		$lookup_device_ref = $DATABASE->select(
								table			=> 'device',
								select_columns	=> 'device_id',
								where_columns	=> [ 'device_serial_cd = ?' ],
								where_values	=> [ $SSN ] );
								
		if(not $lookup_device_ref->[0])
		{
			ReRix::Utils::send_alert("Init: ERROR: Failed to lookup device for serial $SSN after doing insert!  Insert must have failed!", $command_hashref);
			push @logs, "ERROR: Failed to lookup device for serial $SSN after doing insert!  Insert must have failed!";
			return (\@logs);
		}

		$device_id = $lookup_device_ref->[0][0];
		push @logs, "New Device ID     : $device_id";
	}
	else
	{
		$device_id = $lookup_device_ref->[0][0];
		push @logs, "DB Device ID      : $device_id";
		
		$ev_number = $lookup_device_ref->[0][1];
		push @logs, "EV Number         : $ev_number";

		my $db_device_type = $lookup_device_ref->[0][2];
		if($db_device_type ne $device_type)
		{
			ReRix::Utils::send_alert("Init: WARNING: Stored device type $db_device_type not equal to received type $device_type", $command_hashref);
			push @logs, "WARNING: Stored device type $db_device_type not equal to received type $device_type";
		}
		
		$key = $lookup_device_ref->[0][3];
		push @logs, "Encryption Key    : $key";

		$active_flag = $lookup_device_ref->[0][4];
		push @logs, "Active Flag       : $active_flag";
	}
	
	# we should now have an initialized device that exists in the device table, and have the following info;
	# device id, ev number, encryption key, silicon serial number, device type
	
	# now make sure the POS exists to it can record transactions and be linked to a customer and location
	
	my $lookup_pos_ref = $DATABASE->select(
						table			=> 'pos',
						select_columns	=> 'pos_id, location_id, customer_id',
						where_columns	=> ['device_id = ?'],
						where_values	=> [$device_id] );
						
	my $pos_id = $lookup_pos_ref->[0][0];
	if(not defined $pos_id)
	{
		push @logs, "POS               : inserting $device_id";

		$DATABASE->insert(
					table			=> 'pos',
					insert_columns	=> 'device_id',
					insert_values	=> [$device_id]);
	}
	else
	{
		push @logs, "POS               : exists; $pos_id";
	}
	
	if($device_type eq '5')
	{
		# if this is an esuds device, check that the pos_payment_type_authority is setup
		
		# get the POS ID
		if(not defined $pos_id)
		{
			$lookup_pos_ref = $DATABASE->select(
								table			=> 'pos',
								select_columns	=> 'pos_id',
								where_columns	=> ['device_id = ?'],
								where_values	=> [$device_id] );
			
			$pos_id = $lookup_pos_ref->[0][0];
			push @logs, "POS               : new $pos_id";
		}
		
		if(not defined $pos_id)
		{
			ReRix::Utils::send_alert("Init: ERROR: Failed to lookup pos for device_id $device_id after doing insert!  Insert must have failed!", $command_hashref);
			push @logs, "ERROR: Failed to lookup pos for device_id $device_id after doing insert!  Insert must have failed!";
			return (\@logs);
		}

		my $lookup_payment_auth_ref = $DATABASE->select(
							table			=> 'pos_payment_type_authority',
							select_columns	=> 'authority_id, payment_type_id',
							where_columns	=> ['pos_id = ?'],
							where_values	=> [$pos_id] );
		
		my $authority_id = $lookup_payment_auth_ref->[0][0];
		my $payment_type_id = $lookup_payment_auth_ref->[0][1];
		
		if(not defined $authority_id)
		{
			push @logs, "POS               : Inserting pos_payment_type_authority";
			
			# special card
			$DATABASE->insert(
						table			=> 'pos_payment_type_authority',
						insert_columns	=> 'pos_id, authority_id, payment_type_id',
						insert_values	=> [$pos_id, '138', '2']);

			# cash
			$DATABASE->insert(
						table			=> 'pos_payment_type_authority',
						insert_columns	=> 'pos_id, authority_id, payment_type_id',
						insert_values	=> [$pos_id, '138', '4']);

		}
		else
		{
			push @logs, "POS               : pos_payment_type_authority exists ";
		}
	}

	# make sure it exists in the quick_pss schema so transactions can be processed
	
	my $lookup_merchant_ref = $DATABASE->select(
						table			=> 'quick_pss_merchant@pss_tulsap1',
						select_columns	=> 'count(1)',
						where_columns	=> ['client_id = ?'],
						where_values	=> [$ev_number] );

	# For now we're putting everything in PSS as an EPORT, but we could break that out later based on device_type
	my $client_type = 'EPORT';  
		
	if($lookup_merchant_ref->[0][0] eq '0')
	{
		push @logs, "quick_pss merchant: inserting $client_type, $ev_number";

		$DATABASE->insert(
					table			=> 'quick_pss_merchant@pss_tulsap1',
					insert_columns	=> 'client_type, client_id',
					insert_values	=> [$client_type, $ev_number]);
	}
	else
	{
		push @logs, "quick_pss merchant: exists" ;
	}
	
	# now check if a config file exists for this device in the file_transfer table, and if not create one
	my $lookup_config_ref = $DATABASE->select(
						table			=> 'file_transfer',
						select_columns	=> 'count(1)',
						where_columns	=> ['file_transfer_name = ?'],
						where_values	=> ["$ev_number-CFG"] );
	
	if($device_type =~ /0|1/)
	{
		# for a g4, make sure the config exists

		if($lookup_config_ref->[0][0] eq '1')
		{
			push @logs, "Config Init       : $ev_number-CFG exists";
		}
		else
		{
			push @logs, "Config Init       : Does not exist; Using Default";

			my $default_config = 'GX-DEFAULT-CFG';
			my $default_config_data = &blob_select_file($DATABASE->{handle}, $default_config);
			if(not defined $default_config_data)
			{
				ReRix::Utils::send_alert("Init: ERROR: Failed to load $default_config", $command_hashref);
				push @logs, "Init: ERROR: Failed to load $default_config";
				return (\@logs);
			}
			
			$default_config_data = pack("H*", $default_config_data);
		
			$DATABASE->insert(	table			=> 'file_transfer',
								insert_columns	=> 'file_transfer_name, file_transfer_type_cd',
								insert_values	=> ["$ev_number-CFG", "1"]);
			
			&blob_insert($raw_handle, "update file_transfer set file_transfer_content = :blob where file_transfer_name = :name", $default_config_data, "$ev_number-CFG");
		}
	}
	
	# now make sure the device exists in the legacy tables; rerix_modem_to_serial and rerix_initialization

	my $lookup_existing_ref = $DATABASE->select(
						table			=> 'rerix_modem_to_serial',
						select_columns	=> 'modem_id, network',
						where_columns	=> ['machine_id = ?'],
						where_values	=> [$ev_number] );
	
	if(defined $lookup_existing_ref->[0])
	{
		push @logs, "Modem Init        : previously initialized";

		my $modem_id = $lookup_existing_ref->[0][0];
		my $network =  $lookup_existing_ref->[0][1];
		
		push @logs, "Modem ID          : $modem_id";
		push @logs, "Network           : $network";
		
		if(($modem_id ne $ev_number) || ($network ne 'X'))
		{
			push @logs, "Updating          : $ev_number";

			$DATABASE->update(
						table			=> 'rerix_modem_to_serial',
						update_columns	=> 'modem_id, network',
						update_values	=> [$ev_number, 'X'],
						where_columns	=> ['machine_id = ?'],
						where_values	=> [$ev_number] );
		}
	}
	else
	{
		push @logs, "Modem Init        : new initialization";

		push @logs, "Inserting         : $ev_number, $ev_number, X, 1";

		$DATABASE->insert(
					table			=> 'rerix_modem_to_serial',
					insert_columns	=> 'modem_id, machine_id, network, machine_index',
					insert_values	=> [$ev_number, $ev_number, 'X', '1']);
	}
	
	# rerix_modem_to_serial should now be ready
	
	# check for the record in rerix_initialization
	
	push @logs, "Checking rerix_initialization for : $SSN";

	my $lookup_initialization_ref = $DATABASE->select(
							table          => 'rerix_initialization',
							select_columns => 'modem_id, serial_number',
							where_columns  => ['silicon_serial_number = ?'],
							where_values   => [$SSN]);

	if(not defined $lookup_initialization_ref->[0])
	{
		push @logs, "Inserting         : $ev_number, $ev_number, $SSN";

		$DATABASE->insert(
					table			=> 'rerix_initialization',
					insert_columns	=> 'modem_id, serial_number, silicon_serial_number',
					insert_values	=> [$ev_number, $ev_number, $SSN]);		
	}
	else
	{
		my $init_modem_id = 	$lookup_initialization_ref->[0][0];
		my $init_ev_number = 	$lookup_initialization_ref->[0][1];
		
		push @logs, "Existing          : $init_modem_id, $init_ev_number";

		# if any of the values are different, update the record
		
		if(($init_modem_id ne $ev_number) || ($init_ev_number ne $ev_number))
		{
			push @logs, "Updating          : $SSN";

			$DATABASE->update(
						table			=> 'rerix_initialization',
						update_columns	=> 'modem_id, serial_number',
						update_values	=> [$ev_number, $ev_number],
						where_columns  => ['silicon_serial_number = ?'],
						where_values   => [$SSN]);
		}
	}
	
	if($active_flag eq 'Y')
	{
		### device firmware version query
		if ($device_type =~ /0|1/) {	#G4
			push @logs, @{(ReRix::ClientVersion::request_g4($DATABASE, $ev_number))[0]};
		}
		elsif ($device_type eq '5') {	#eSuds
			push @logs, @{(ReRix::ClientVersion::request($DATABASE, $ev_number))[0]};
		}

		# eSuds wants a device reactivate message to startup correctly
		if($device_type eq '5')
		{
			push @logs, "Queued for ESuds  : Device Reactive Message";
			$DATABASE->insert(	table=> 'machine_command_pending',
							insert_columns=> 'machine_id, data_type, execute_cd, execute_order',
							insert_values=> [$ev_number, '76', 'P', 1]);
		}
		
		my $response = pack("CH2a*a*", $msg_no, '8F', $key, $ev_number);
		push @logs, "Response (hex)    : " . unpack("H*", $response);
		# pass back the message using the original EV number
		$DATABASE->insert(
					table			=> 'Machine_Command',
					insert_columns	=> 'Modem_ID, Command, execute_cd',
					insert_values	=> [$incoming_id, unpack("H*", $response), 'N']);
					
					
		&ReRix::CallInRecord::device_initialized($raw_handle, $ev_number);
	}
	else
	{
		push @logs, "Response          : Device is NOT active; no reponse";
	}
	
	return (\@logs);
}

####################################################################
# NOTE:  A byte block is defined as a series of bytes with the last
#        byte's 8th bit set to 1.  No other byte in the block has 
#        the 8th bit set to 1
####################################################################
sub parse_byteblock 
{
	my($message) = @_;
	my($byteblock, $whatisleft);
	my($workingbyte);
   
	$byteblock = '';
	# There will always be at least one byte in a block
	do 
	{
		$workingbyte = ord(substr($message,0,1));
		$message = substr($message,1);
		$byteblock .=  chr($workingbyte & 0x7F); # make sure to remove the high 8th bit
	}
	while ((not ($workingbyte & 0x80)) and length $message);

	if (length $message == 0) 
	{
		# Remove the high 8th bit is it is at the end of the message
		$byteblock .=  chr($workingbyte & 0x7F);
	}

	return ($byteblock, $message);
}

sub blob_select_file
{
	my ($db, $file_name) = @_;
	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB
	my $stmt = $db->prepare(q{select file_transfer_content from file_transfer where file_transfer_name = :file_name });
	$stmt->bind_param(":file_name", $file_name);

	$stmt->execute();
	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}

sub blob_insert
{
	my ($db, $sqlStmt, $buf, $name) = @_;
	my $LONG_RAW_TYPE=24;
	my $stmt = $db->prepare($sqlStmt);
	my %attrib;
	$attrib{'ora_type'} = $LONG_RAW_TYPE;
	$stmt->bind_param(":blob", $buf, \%attrib);
	$stmt->bind_param(":name", $name);
	$stmt->execute();
	if(defined $db->errstr)
	{
		print "blob_insert error: " . $db->errstr . "\n";
	}
	$stmt->finish();
}

1;
