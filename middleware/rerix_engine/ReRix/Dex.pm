#!/usr/bin/perl -w

use strict;

package ReRix::Dex;

use ReRix::Shared;
use ReRix::Send;

# Dex Response: R

sub parse
{
    my ($DATABASE, $command_hashref) = @_;

    my @logs;

	my $message = substr(MakeString($command_hashref->{inbound_command}),1);

	$message =~ s/\x0D\x0A/\n/g;
	$message =~ s/\x0D/\n/g;
	$message =~ s/\x0A/\n/g;

	$DATABASE->insert(
						table			=> 'dex_Command_Response',
						insert_columns	=> 'MACHINE_ID, DEX_RESPONSE',
						insert_values	=> [$command_hashref->{machine_id},
											$message ]
					);

	return [$message];
}

1;
