package ReRix::AuthClient;

use strict;
use IO::Socket;

BEGIN
{
	use Exporter    ();
	use vars        qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    
	$VERSION        = '0.01';

	@ISA            = qw(Exporter);

	#export by default
    @EXPORT         = qw();

	#export on demand
    @EXPORT_OK      = qw();

	#global/constants
	use vars        qw();

	sub connect 
	{
		my $type = shift;
	
		my $object = {};
		%$object = @_;
	
		$object->{timeout} ||= 10;
		$object->{processor} ||= 'paymentech';
	
		$object->{host} = '127.0.0.1';
		$object->{port} = '10104';
		
		$object->{socket} = IO::Socket::INET->new(Proto => 'tcp', PeerAddr => $object->{host}, PeerPort => $object->{port}, Timeout => 15);
	
		bless $object,$type;
	}
	
	sub send
	{
		my $object = shift;
		my $string = shift;
		my $socket = $object->{socket};
	
		print $socket "$string\n" if $socket;
	
		my $line =  <$socket>;
		chomp $line;
	
		my ($code,$authnumber,$account_balance, $inv) = split /\,/, $line;
		$account_balance =~ s/\s//g;
		$object->{authorization_number} = $authnumber;
		$object->{code} = $code;
		$object->{account_balance} = "\$$account_balance" if $account_balance ne '';
	
		$object->{$authnumber}->{code} = $code;
		$object->{$authnumber}->{account_balance} = "\$$account_balance" if $account_balance ne '';
		$object->{inv} = $inv;
		
		return 1;
	}
	
	sub authorization_number
	{
		my($object) = shift;
		return $object->{authorization_number};
	}
	
	sub get_code
	{
		my($object) = shift;
		my $authorization_number = shift;
		$authorization_number ||= $object->authorization_number();
		return $object->{$authorization_number}->{code};
	}
	
	sub account_balance
	{
		my ($object) = shift;
		my $authorization_number = shift;
		$authorization_number ||= $object->authorization_number();
		return $object->{$authorization_number}->{account_balance};
	}
	
	sub disconnect
	{
		my($object) = shift;
		my $socket = $object->{socket};
	
		$socket->close();
		undef $object;
	}
	
	# returns 1 if inventory needs to be removed for this transaction, 0 if it
	# already has
	sub get_inv
	{
	        my($object) = shift;
			if( !defined $object->{inv} )
			{
				return 0;
			}
	        return $object->{inv};
	}

	END { }
}

1;
