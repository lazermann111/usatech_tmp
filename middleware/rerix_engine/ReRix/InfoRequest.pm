use strict;

package ReRix::InfoRequest;

use Evend::ReRix::Shared;
use ReRix::Send;

# File Transfer stuff
use ReRix::FileTransferIncoming;
use ReRix::FileTransferOutgoing;

use ReRix::Utils;

use POSIX qw(ceil);
use Time::Local;

sub parse
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;

	my $message = MakeString($command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no};
	my $machine_id = $command_hashref->{machine_id};

	my $reqnbr = unpack("C", substr($message,1,1));
	
	push @logs, "Request Number    : $reqnbr";

	my $response;

	if($reqnbr == 0) # Unix Time (84h)
	{
		push @logs, "Request           : Unix Time";
		push(@logs, @{&unix_time($DATABASE,$command_hashref)});
	}
	elsif($reqnbr == 1) # BCD Time (85h)
	{
		push @logs, "Request           : BCD Time";
		push(@logs, @{&bcd_time($DATABASE,$command_hashref)});
	}
	elsif($reqnbr == 2) # Gx Configuration in Gx Poke (88h)
	{
		push @logs, "Request           : Gx Configuration Poke";
		push(@logs, @{&gx_config_poke($DATABASE,$command_hashref)});
	}
	
	return(\@logs);
}

sub unix_time
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;

	my $message = MakeString($command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no};
	my $machine_id = $command_hashref->{machine_id};
	my $device_id = $command_hashref->{device_id};
	my $response_no = $command_hashref->{response_no}; 

	my $time_zone;
	
	if(defined $device_id)
	{
		# Use the device_id as a rerefence to lookup the time zone
	
		my $lookup_tz_ref =	$DATABASE->select(	
								table          => 'location.location, pos',
								select_columns => 'location.location_time_zone_cd',
								where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
								where_values   => [$device_id] );
								
		$time_zone = $lookup_tz_ref->[0][0];
	}
	
	if(not defined $time_zone)
	{
		$time_zone = 'EST';
		push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
	}
	else
	{
		push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
	}

	my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
	
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
	$Month = $Month+1;
    $Year  = $Year+1900;
	
	push @logs, "Time              : $Month/$Day/$Year $Hour:$Minute:$Second ";

	my $response = pack('CCN', $response_no, 0x84, $time);
	push @logs, "Response          : " . unpack("H*", $response);
	
	$DATABASE->insert(	table			=> 'Machine_Command',
						insert_columns	=> 'Modem_ID, Command, execute_cd',
						insert_values	=> [$machine_id, unpack("H*", $response), 'N'] );

	return(\@logs);
}

sub bcd_time
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;

	my $message = MakeString($command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no};
	my $machine_id = $command_hashref->{machine_id}; # this should be the default
	my $device_id = $command_hashref->{device_id};
	my $response_no = $command_hashref->{response_no}; 
	
	my $time_zone;
	
	if(defined $device_id)
	{
		# Use the device_id as a rerefence to lookup the time zone
	
		my $lookup_tz_ref =	$DATABASE->select(	
								table          => 'location.location, pos',
								select_columns => 'location.location_time_zone_cd',
								where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
								where_values   => [$device_id] );
								
		$time_zone = $lookup_tz_ref->[0][0];
	}
	
	if(not defined $time_zone)
	{
		$time_zone = 'EST';
		push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
	}
	else
	{
		push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
	}


	my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
	
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
	my ($Year1, $Year2);

	$Month = $Month+1;
    $Year  = $Year+1900;
    
	$Year1 = substr($Year,0,2);
	$Year2 = substr($Year,2,2);
	
	# make sure these are all 2 characters long, if not pad with a zero
	#if(length($Second) < 2)	{	$Second = '0' . $Second;	}
	#if(length($Minute) < 2)	{	$Minute = '0' . $Minute;	}
	#if(length($Hour) < 2)	{	$Hour = '0' . $Hour;		}
	#if(length($Day) < 2)	{	$Day = '0' . $Day;			}
	#if(length($Month) < 2)	{	$Month = '0' . $Month;		}
	
	push @logs, "Time              : $Month/$Day/$Year $Hour:$Minute:$Second ";

	#my $response = pack('C9', $response_no, 0x85, $Hour, $Minute, $Second, $Month, $Day, $Year1, $Year2);
	my $response = pack('C9', $response_no, 0x85, $Hour, $Minute, $Second, $Month, $Day, $Year1, $Year2);
	#my $response = pack('CCH2H2H2H2H2H2H2', $response_no, 0x85, $Hour, $Minute, $Second, $Month, $Day, $Year1, $Year2);
	push @logs, "Response          : " . unpack("H*", $response);
	
	$DATABASE->insert(	table			=> 'Machine_Command',
						insert_columns	=> 'Modem_ID, Command, execute_cd',
						insert_values	=> [$machine_id, unpack("H*", $response), 'N'] );

	return(\@logs);
}

# This command should insert Poke commands into machine_command_pending table that will overwrite the 
# entire Gx configuration in nv-ram.  
sub gx_config_poke
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;

	my $message = pack("H*", $command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no};
	my $machine_id = $command_hashref->{machine_id}; # this should be the default
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $response_no = $command_hashref->{response_no}; 
	my $device_type = $command_hashref->{device_type}; 

	if(not defined $device_id)
	{
		push @logs, "No active device record found for $machine_id";
		return(\@logs);
	}
	
	# remove any existing pokes 
	
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'execute_cd',
						update_values	=> ['C'],
						where_columns	=> ['machine_id = ?', 'data_type = ?', '(execute_cd = ? or (execute_cd = ? and execute_date < (sysdate-(90/86400))))'],
						where_values	=> [$machine_id, '88', 'P', 'S'] );

	# get the config file from the database
	
	# use blobSelect because the column is a LONG and it won't come back otherwise
	my $raw_handle = $DATABASE->{handle};
	my $poke_data = &blob_select_config($raw_handle, $machine_id);
	
	if(not defined $poke_data)
	{
		push @logs, "Config            : Config not found for $machine_id" ;
		
		$poke_data = &blob_select_config($raw_handle, "GX-DEFAULT");
		if(not defined $poke_data)
		{
			ReRix::Utils::send_alert("InfoRequest: Config            : Config not found for GX-DEFAULT!", $command_hashref);
			push @logs, "Config            : Config not found for GX-DEFAULT!" ;
			return(\@logs);
		}
		else
		{
			push @logs, "Config            : Using GX-DEFAULT config" ;
		}
	}
	else
	{
		push @logs, "Config            : Using $machine_id-CFG config" ;
	}

	# Data in file_transfer_content coulumn is stored HEX encoded
	$poke_data = pack("H*", $poke_data);
	
	my $poke_size = 50;
	if(defined $device_type)
	{
		if($device_type eq '0')
		{
			# make the poke size bigger for a G4, small for MEI, etc
			$poke_size = 200;
			#$poke_size = 50;
		}
		if($device_type eq '1')
		{
			$poke_size = 200;
		}			
		# may want to add other conditionals here in the future
	}
	
	# figure out how many Pokes it will take to transmit the entire file
	my $total_bytes = length($poke_data);
	my $num_parts = ceil($total_bytes/$poke_size);
	
	push @logs, "Poke Size         : $poke_size";
	push @logs, "Total Bytes       : $total_bytes";
	push @logs, "Num Parts         : $num_parts";
	
	# create each Poke and put it in the machine_command_pending table	

	# G4 EEROM uses weird memory addressing, where each address = 16 bits.  So memory 
	# addresses is always = (file_position/2)
	
	for(my $counter=0; $counter<$num_parts; $counter++)
	{
		my $pos = ($counter*$poke_size);
		my $chunk;
		if(($pos+$poke_size) > $total_bytes)
		{
			# this should be the last Poke
			$chunk = substr($poke_data, $pos);
		}
		else
		{
			$chunk = substr($poke_data, $pos, $poke_size);
		}
		
		my $memory_location = ($pos/2);
		push @logs, "File Location     : $pos";
		push @logs, "Memory Location   : $memory_location";
		
		# changed - don't put actual poke data in the pending table, just the poke info.  We'll construct
		# the poke on the fly when the command is sent - this is done in case there are changes made to the
		# config file between the time we create the pokes here and they are sent
		#my $poke = pack('CNa*', ord("B"), $memory_location, $chunk);
		my $poke = pack('CNN', ord("B"), $memory_location, length($chunk));

		$DATABASE->insert(	table=> 'machine_command_pending',
							insert_columns=> 'machine_id, data_type, command, execute_cd, execute_order',
							insert_values=> [$machine_id, '88', unpack("H*", $poke), 'P', ($counter+1)]);
		
		push @logs, "POKE              : $machine_id, 88, " . unpack("H*", $poke) . ", P, " . ($counter+1);
	}
		
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	$DATABASE->insert(	table=> 'machine_command',
						insert_columns=> 'modem_id, command',
						insert_values=> [$machine_id, unpack("H*", $response)] );

	return(\@logs);	
}

sub blob_select_config
{
	my ($db, $machine_id) = @_;
	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB
	my $stmt = $db->prepare(q{select file_transfer_content from file_transfer where file_transfer_name = :file_name });
	$stmt->bind_param(":file_name", "$machine_id-CFG");

	$stmt->execute();

	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}

1;
