#!/usr/bin/perl -w

use strict;

package ReRix::Signal;

use ReRix::Shared;
use ReRix::Send;

# Motient Signal Stats: H

sub parse
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $array_ref;

	$array_ref = $DATABASE->select(
						table			=> 'rerix_modem_to_serial',
						select_columns	=> 'network, modem_id, machine_index',
						where_columns	=> [ 'machine_id = ?'],
						where_values	=> [ $command_hashref->{machine_id} ]
					);

	if( not $array_ref->[0] )
	{
		return( ['    Cannot find network, modem_it and machine_index in rerix_modem_to_serial'] );
	}

	my $modem_id = $array_ref->[0][1];

	my $message = MakeString( $command_hashref->{inbound_command} );

	if (length $message == 3)
	{
		$message .= chr(0)x2;
	}
	my %out = (	strength	=> ord(substr($message,1,1)) , 
				quality		=> ord(substr($message,2,1)),
				channel		=> (ord(substr($message,3,1))<<8) |
											ord(substr($message,4,1))
			);

	push @logs, "Modem ID: $modem_id";
	push @logs, "Strength: $out{strength}";
	push @logs, "Quality : $out{quality}";
	push @logs, "Channel : $out{channel}";

	$DATABASE->insert(
				table			=> 'MODEM_STATUS',
				insert_columns	=> 'MODEM_ID, STRENGTH, QUALITY, CHANNEL',
				insert_values	=> [$modem_id, $out{strength}, $out{quality},
									$out{channel}]
				);

	return(\@logs);
}

1;
