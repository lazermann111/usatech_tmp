use strict;

package ReRix::Ping;

use Evend::ReRix::Shared;
use ReRix::Send;
use ReRix::CallInRecord;

# Client to Server Ping: I

sub parse
{
	my ($DATABASE, $command_hashref) = @_;
	my (@logs);

	my $messagestr = substr(MakeString($command_hashref->{inbound_command}),1);

	my $message = ord($messagestr);

	push @logs, "Client Message Byte(hex): $messagestr\n";
	
	my $network;
	my $modem_id;
	my $machine_index;

	if ( (0x04 & $message) or (0x08 & $message) )
	{
		my $array_ref = $DATABASE->select(
						table	=> 'rerix_modem_to_serial',
						select_columns	=> 'network, modem_id, machine_index',
						where_columns	=> [ 'machine_id = ?'],
						where_values	=> [ $command_hashref->{machine_id} ]
		);
		if( not $array_ref->[0] )
		{
			return( ['    Cannot find network, modem_id and machine_index in rerix_modem_to_serial'] );
		}

		$network = $array_ref->[0][0];
		$modem_id = $array_ref->[0][1];
		$machine_index = $array_ref->[0][2];
	}


	my ($version, $count);

	if ( 0x01 & $message)
	{
		push @logs, '     Price/Column Setup';
		&ReRix::Send::sendPrices($DATABASE, $command_hashref->{machine_id});
	}
	if ( 0x02 & $message)
	{
		push @logs, '     Price/Column Setup Complex (No Response)';
	}
	if ( 0x04 & $message)
	{
		##############################################
		# I was asked to count how many lines of software
		###############################################
		push @logs, '     Send Count of Software';

		# First Find out which version
		my $array_ref = $DATABASE->select(
								table			=> 'rerix_machine',
								select_columns	=> 'SOFTWARE_VERSION',
								where_columns	=> [ 'MODEM_ID = ?'],
								where_values	=> [$modem_id]
							);

		if (defined @$array_ref && defined $array_ref->[0][0])
		{
			$version = $array_ref->[0][0];
			# Then get the count
			$array_ref = $DATABASE->select(
								table			=> 'software_download',
								select_columns	=> 'count(LINE_NO)',
								where_columns	=> [ 'SOFTWARE_VERSION = ?'],
								where_values	=> [$version]
							);
			if (defined @$array_ref && defined $array_ref->[0][0])
			{
				$count = $array_ref->[0][0]; 
			}
		}
		if (not defined $count) { $count=0;}
		# OK, I got a count of the lines

		$DATABASE->insert(
						table			=> 'Machine_Command',
						insert_columns	=> 'Modem_ID, Command',
						insert_values	=> [$command_hashref->{machine_id},
											 &MakeHex('v' .
												chr(($count & 0xff00) >>8 ) .
												chr($count & 0xff))]
					);
	}
	if ( 0x08 & $message)
	{
		##############################################
		# I was asked to send Date/Time Management Setup
		###############################################

		push @logs, '     Sending Date/Time Management Setup';

		my $array_ref = $DATABASE->select(
						table          => 'rerix_machine',
						select_columns => 'STARTING_COMM_TIME,  COMM_FREQUENCY',
						where_columns  => [ 'MODEM_ID= ?'],
						where_values => [$modem_id]
						);

		my $start;
		my $freq;

		if (defined @{$array_ref})
		{
			$start = $array_ref->[0][0];
			$freq =  $array_ref->[0][1];

			# Set up some defaults
			if (not defined $start)
			{
				$start = int(rand 37);  # Machines "Dial in" at random
			}

			if (not defined $freq)
			{
				$freq=24;
			}
		}
		else
		{
			$start = int(rand 37);
			$freq=24;

			$DATABASE->insert(
						table          => 'rerix_machine',
						insert_columns => 'MODEM_ID, STARTING_COMM_TIME, ' .
											'COMM_FREQUENCY',
						insert_values  => [$modem_id, $start, $freq],
						);
		}

		my $datetimeman = &ReRix::Send::encode_x28($start, $freq);
		$DATABASE->insert(
					table           => 'Machine_Command',
					insert_columns  => 'Modem_ID, Command',
					insert_values   => [$command_hashref->{machine_id},
											&MakeHex($datetimeman)]
					);
	}
	if ( 0x10 & $message )
	{
		push @logs, '     Send Time';

		my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) =
																	localtime;

		my $time;

		# calculate EDT time in seconds since epoc(unix time on EDT)
		if( $isdst )
		{
			# It's Daylight Savings Time, EDT is GMT-4

			$time = time() - 60*60*4;
		}
		else
		{
			# It's Not Daylight Savings Time, EDT is GMT-5

			$time = time() - 60*60*5;
		}

		$DATABASE->insert(
						table           => 'Machine_Command',
						insert_columns  => 'Modem_ID, Command',
						insert_values   => [$command_hashref->{machine_id},
											&MakeHex(pack("CV", 0x29, $time))]
					);
	}
	if ( 0x20 & $message )
	{
		push @logs, '     Unused';
	}
	if ( 0x40 & $message )
	{
		push @logs, '     Unused';
	}
	if ( 0x80 & $message )
	{
		push @logs, '     Unused';
	}

	return (\@logs);
}

sub alive_alert_v20 ()
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $device_id = $command_hashref->{device_id};
	my $raw_handle = $DATABASE->{handle};

	push @logs, "Machine Is Alive  : $machine_id";
	
	if(defined $device_id)
	{
		my $dbh = $DATABASE->{handle};
		my $stmt = $dbh->prepare('UPDATE device SET last_activity_ts = sysdate WHERE  device_id = ?');
		$stmt->execute($device_id);
		$stmt->finish();
	}

	&ReRix::CallInRecord::set_type($raw_handle, $machine_id, 'B');

	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	$DATABASE->insert(	table=> 'machine_command',
						insert_columns=> 'modem_id, command',
						insert_values=> [$machine_id, unpack("H*", $response)] );

	return(\@logs);
}

sub ack
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	
	my $acked_msg_no = ord(substr($message, 1, 1));
	
	push @logs, "ACK'd Message     : $acked_msg_no";
	
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'execute_cd',
						update_values	=> ['A'],
						where_columns	=> ['machine_id = ?', 'message_number = ?', 'execute_cd = ?'],
						where_values	=> [$machine_id, $acked_msg_no, 'S'] );

	return(\@logs);
}

sub device_control_ack
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'execute_cd',
						update_values	=> ['A'],
						where_columns	=> ['machine_id = ?', 'data_type = ? or data_type = ?', 'execute_cd = ?'],
						where_values	=> [$machine_id, '73', '76', 'S'] );

	return(\@logs);
}

sub nak
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my (undef, $code) = unpack("C2", $message);	
	push @logs, "NAK!              : $code";

	return(\@logs);
}

sub nak_V2
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $max_retries = 3;
	my %nak_type =	(
		#NAK type => ['description', resend message boolean (0|1)]
		0	=> [
			'Failed (Unspecified)'
			,1
		]
		,1	=> [
			'Message Not Implemented'
			,0
		]
		,2	=> [
			'CRC Failure'
			,1
		]
	);

	### extract command info
	my $message = pack("H*",$command_hashref->{inbound_command});
	my (undef, $type, $msg_num) = unpack("C3", $message);
	
	### determine the number of times this message has been resent already
	my $array_ref = $DATABASE->select(
		table			=> 'machine_command_hist'
		,select_columns	=> 'COUNT(*), command'
		,where_columns	=> [
			'modem_id = ?'
			,'execute_date >= (sysdate-(60/1440))'
			,qq{command = 
				(
					SELECT command
					FROM (
						SELECT command
						FROM machine_command_hist
						WHERE modem_id = ?
						AND LOWER(SUBSTR(command, 0, 2)) = LOWER(?)
						ORDER BY execute_date DESC, command_id DESC
					)
					WHERE rownum <= 1
				) GROUP BY command
			}
		]
		,where_values	=> [
			$command_hashref->{machine_id}
			,$command_hashref->{machine_id}
			,unpack("H*", pack("C", $msg_num))
		]
	);

	my ($num_retries, $command) = (0, undef);
	if (defined @{$array_ref})
	{
		($num_retries, $command) = (@{$array_ref->[0]})[0..1];
	}
	
	### queue message to be resent
	unless ($num_retries == 0 || $num_retries > $max_retries)
	{
		$DATABASE->insert(
			table			=> 'machine_command'
			,insert_columns	=> 'modem_id, command'
			,insert_values	=> [
				$command_hashref->{machine_id}
				,$command
			]
		);
	}

	### log result
	push @logs, "NAK V2!      Type : ".(
		defined $nak_type{$type}?
		$nak_type{$type}->[0].(
			$nak_type{$type}->[1]?(
				$num_retries > $max_retries?
				". Msg not resent (max num $max_retries retries exceeded)."
				:(
					$num_retries > 0?
					". Resending command msg num \#$msg_num (attempt $num_retries/$max_retries)"
					:". ERR: no message found in history! Nothing to resend!"
				)
			)
			:". Msg not resent."
		)
		:"Reserved NAK type \#$type received. Msg not resent."
	);

	return(\@logs);
}

1;
