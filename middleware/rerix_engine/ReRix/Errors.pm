#!/usr/bin/perl -w

use strict;

package ReRix::Errors;

use ReRix::Shared;
use ReRix::Send;

# Client Errors: E

sub parse
{
    my ($DATABASE, $command_hashref) = @_;

    my @logs;

    my $array_ref;

    $array_ref = $DATABASE->select(
                    table          => 'rerix_modem_to_serial',
                    select_columns => 'modem_id',
                    where_columns  => [ 'machine_id = ?'],
                    where_values => [ $command_hashref->{machine_id} ]
				);

    if( not $array_ref->[0] )
    {
        return( ['    Cannot find network, modem_it and machine_index in rerix_m
odem_to_serial'] );
    }

    my $modem_id = $array_ref->[0][0];

    my $message = MakeString( $command_hashref->{inbound_command} );

	my (@errors);
	my $workingbyte = ord(substr($message,1,1));

	(0x01 & $workingbyte) && do { push @errors, 'RERIX_DECRYPT_FAILURE' };
	(0x02 & $workingbyte) && do { push @errors, 'RERIX_BAD_FORMAT' };
	(0x04 & $workingbyte) && do { push @errors, 'DEX_PORT_FAILURE' };
	(0x08 & $workingbyte) && do { push @errors, 'HARDWARE_FAILURE' };
	(0x10 & $workingbyte) && do { push @errors, 'SOFTWARE_FAILURE' };
	(0x20 & $workingbyte) && do { push @errors, 'DOWNLOAD_COMMAND_FAILURE' };
	(0x40 & $workingbyte) && do { push @errors, 'SYSTEM_WATCHDOG_FIRED' };
	(0x80 & $workingbyte) && do { push @errors, 'Unused' };

	my $fields = join ', ', ('MACHINE_ID, MODEM_ID', @errors);
	my $values = [$command_hashref->{machine_id}, $modem_id];

	foreach (@errors)
	{
		push @{$values}, 1;
	}

	$DATABASE->insert(
					table			=> 'RERIX_CLIENT_ERRORS',
					insert_columns	=> $fields,
					insert_values	=> $values
				);

	return( [ ("     Errors: " . join(", ", @errors)) ] );
}

1;
