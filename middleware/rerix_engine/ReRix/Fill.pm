#!/usr/bin/perl -w

use strict;

package ReRix::Fill;

use ReRix::Shared;
use ReRix::Send;

# Complete Fill Information Command

sub parse
{
    my ($DATABASE, $command_hashref) = @_;

    my (@logs);

	my $message = MakeString($command_hashref->{inbound_command});

	$message = substr $message, 1;

	my $timestamp = unpack 'V', $message;

	$DATABASE->insert(
					table			=> 'machine_command',
					insert_columns	=> 'modem_id, command',
					insert_values	=> [$command_hashref->{machine_id},
										MakeHex('o' . substr($message, 0, 4))]
					);

	if( $timestamp == 0 )
	{
		# if we get a bad timestamp, use the current time

		push @logs, "Bad time stamp, using current time";

		$timestamp = time() - 60*60*5;
	}

	my ($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);

	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);

	my $time = sprintf("%02d:%02d:%02d", $hour, $min, $sec);

	$message = substr $message, 4;

	# Cash accounting

	$DATABASE->query(query => "alter session set nls_date_format = 'MM/DD/YYYY HH24:MI:SS'");

	$DATABASE->insert(
					table			=> 'Machine_Command_Inbound',
					insert_columns	=> 'machine_id, inbound_command, timestamp',
					insert_values	=> [$command_hashref->{machine_id},
										MakeHex("O" . substr($message, 0, 36)),
										"$date $time"]
					);

	$message = substr $message, 36;

	# Sell Through

	$DATABASE->query(query => "alter session set nls_date_format = 'MM/DD/YYYY HH24:MI:SS'");

	$DATABASE->insert(
					table			=> 'Machine_Command_Inbound',
					insert_columns	=> 'machine_id, inbound_command, timestamp',
					insert_values	=> [$command_hashref->{machine_id},
										MakeHex($message),
										"$date $time"]
					);

	return(\@logs);
}

1;
