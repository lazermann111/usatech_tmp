#!/usr/bin/perl -w

use strict;

package ReRix::Messages;

use ReRix::Shared;
use ReRix::Send;

# SCU Messages

my %number_command = (
						'02' => 'AUTH_FAILED',
						'03' => 'AUTH_AUTHORIZING',
						'04' => 'AUTH_REMOVE_CARD',
						'05' => 'AUTH_INSERT_CARD',
						'06' => 'AUTH_CARD_NOT_VALID',
						'07' => 'AUTH_CARD_NOT_READ',
						'08' => 'AUTH_RFID_NOT_VALID',
						'09' => 'AUTH_RFID_NOT_READ',
						'0A' => 'AUTH_FAILED_NETWORK',
						'0B' => 'AUTH_OVER_LOCAL_LIMIT',
						'15' => 'AD_MESSAGE',

						'1F' => 'RECEIPT_PRINTING',
						'20' => 'RECEIPT_QUESTION',
						'21' => 'RECEIPT_TAKE',
						'22' => 'RECEIPT_ERROR_PRE',
						'23' => 'RECEIPT_ERROR_POST',
						'24' => 'RECEIPT_HEADER',
						'25' => 'RECEIPT_FOOTER',

						'28' => 'VEND_SELECT',
						'29' => 'VEND_VENDING',
						'2A' => 'VEND_TIME_OUT',
						'2B' => 'VEND_FAILED',
						'2C' => 'VEND_CANCELLED',
						'2D' => 'VEND_COMPLETE',
						'2E' => 'VEND_AGAIN',

						'32' => 'UNIT_FILL_ACK',
						'33' => 'UNIT_OUT_OF_SERVICE',

						'3C' => 'LED_AUTH_AUTHORIZING',
						'3D' => 'LED_AUTH_FAILED',
						'3E' => 'LED_VEND_SELECT',
						'3F' => 'LED_FAILED_NETWORK',
						'40' => 'LED_CARD_NOT_VALID',
						'41' => 'LED_CARD_NOT_READ',
						'42' => 'LED_VEND_CANCELLED',
					);

sub parse_req
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	# Remove the command letter, then generate each message requested

	my $request_list = substr $command_hashref->{inbound_command}, 2;

	my $request;

	while( $request = substr( $request_list, 0, 2 ) )
	{

		push @logs, "Request: $request ($number_command{$request})";

		$DATABASE->insert(
					table			=> 'scu_command',
					insert_columns	=> 'machine_id, command',
					insert_values	=> [ $command_hashref->{machine_id},
										$number_command{$request} ]
					);

		$request_list = substr $request_list, 2;
	}

	return(\@logs);
}

sub parse_ack
{
	my ($DATABASE, $command_hashref) = @_;

	my @logs;

	my $ack_command_number = uc(substr $command_hashref->{inbound_command}, 2);

	my $ack_command = $number_command{ $ack_command_number };

	if( ! exists $number_command{ $ack_command_number } )
	{
		return ( ["     ACK Setting Unknown Messages ($ack_command_number)"] );
	}

	push @logs, "ACK Messages: $ack_command";

	$DATABASE->update(
				table			=> 'scu_command',
				update_columns	=> 'command_status',
				update_values	=> ['Y'],
				where_columns	=> ["command_status = 'S'",
									'command = ?',
									'machine_id = ?'],
				where_values	=> [ $ack_command,
										$command_hashref->{machine_id} ],
				);

	return( \@logs );
}


1;
