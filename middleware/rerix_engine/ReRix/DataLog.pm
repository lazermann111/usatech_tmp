#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		08/04/2003	pcowan		Created
#

package ReRix::DataLog;

use strict;

use Evend::Database::Database;        # Access to the Database
use Evend::ReRix::Shared;
use ReRix::Send;

my $log_root = '/opt/ReRixEngine2/data_log';
#open(DATALOG, ">>$logfile");

my %esuds_log_types = 
(
	'1'	 	=> 	'LOG_STARTING_UP',
	'2' 	=> 	'LOG_ENTERING_MAINTENANCE',
	'3' 	=> 	'LOG_LEAVING_MAINTENANCE',
	'4' 	=> 	'LOG_RECEIVED_MANUAL_ACTIVATION',
	'5' 	=> 	'LOG_RECEIVED_NETWORK_ACTIVATION',
	'6' 	=> 	'LOG_RECEIVED_CONFIG_FILE',
	'7' 	=> 	'LOG_RECEIVED_APP_UPGRADE',
	'8' 	=> 	'LOG_SYSTEM_TIME_CHANGED',
	'9' 	=> 	'LOG_RECEIVED_UNKNOWN_REMOTE_COMMAND',
	'10' 	=> 	'LOG_SHUTDOWN_NORMAL',
	'11' 	=> 	'LOG_SHUTDOWN_REBOOT',
	'12' 	=> 	'LOG_SHUTDOWN_POWER_DOWN',
	'13' 	=> 	'LOG_SHUTDOWN_RESTART', 
	'14'	=>	'LOG_SHUTDOWN_EXTERNAL',
	'19' 	=> 	'LOG_SHUTDOWN_UNKNOWN',
	'20' 	=> 	'LOG_MANUAL_CONFIG_CHANGE',
	'21' 	=> 	'LOG_UNKNOWN_PROGRAM_ERROR',
	'22' 	=> 	'LOG_CONSECUTIVE_AUTH_FAILURES',
	'23' 	=> 	'LOG_DECRYPTION_FAILURE',
	'24' 	=> 	'LOG_ENCRYPTION_FAILURE',
	'99' 	=> 	'LOG_FLASH_WRITE_FAILURE',
	'100' 	=> 	'LOG_DEAD_THREAD_KEYPAD',
	'101' 	=> 	'LOG_DEAD_THREAD_UI', 
	'102' 	=> 	'LOG_DEAD_THREAD_MULTIPLEXOR',
	'103' 	=> 	'LOG_DEAD_THREAD_NETWORK',
	'104' 	=> 	'LOG_DEAD_THREAD_MAIN',
	'105' 	=> 	'LOG_DEAD_THREAD_MONITOR',
	'106' 	=> 	'LOG_DEAD_THREAD_SCHEDULED_TASKS',
	'107' 	=> 	'LOG_DEAD_THREAD_CCR',
	'120' 	=> 	'LOG_EQUIP_CHANGE',
	'121' 	=> 	'LOG_MULTIPLEXOR_COMM_LOST',
);

sub parse
{
	my ($DATABASE, $command_hashref) = @_;
	my @logs;
	my $array_ref;
	
	my $msg_no = $command_hashref->{msg_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $response_no = $command_hashref->{response_no};
	my $device_type_code = $command_hashref->{device_type};
	
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = localtime();

	$Month = $Month+1;
    $Year  = $Year+1900;
    
    if(length($Day) < 2)
    {
    	$Day = "0$Day";
    }
    
    if(length($Month) < 2)
    {
    	$Month = "0$Month";
    }
    
    my $timestamp = $Day . $Month . $Year;
    my $logfile = $log_root;
    
    my $data = substr($message, 1);
	
	if(not defined $device_type_code)
	{
		$logfile = $logfile . "/other/" . $timestamp;
		push @logs, "Log File          : $logfile";
		open(LOG, ">>$logfile");
		print LOG '[' . localtime() . "] $machine_id $data\n";	
		close(LOG);
	}
	elsif($device_type_code eq '0')
	{
		$logfile = $logfile . "/G4/" . $timestamp;
		push @logs, "Log File          : $logfile";
		open(LOG, ">>$logfile");
		print LOG '[' . localtime() . "] $machine_id $data\n";	
		close(LOG);
	}
	elsif($device_type_code eq '1')
	{
		$logfile = $logfile . "/G5/" . $timestamp;
		push @logs, "Log File          : $logfile";
		open(LOG, ">>$logfile");
		print LOG '[' . localtime() . "] $machine_id $data\n";	
		close(LOG);
	}
	elsif($device_type_code eq '3')
	{
		$logfile = $logfile . "/brick/" . $timestamp;
		push @logs, "Log File          : $logfile";
		open(LOG, ">>$logfile");
		print LOG '[' . localtime() . "] $machine_id $data\n";	
		close(LOG);
	}
	elsif($device_type_code eq '4')
	{
		$logfile = $logfile . "/sony/" . $timestamp;
		push @logs, "Log File          : $logfile";
		open(LOG, ">>$logfile");
		print LOG '[' . localtime() . "] $machine_id $data\n";	
		close(LOG);
	}
	elsif($device_type_code eq '5')
	{
		$logfile = $logfile . "/esuds/" . $timestamp;
		push @logs, "Log File          : $logfile";
		&logESuds($machine_id, $logfile, $data);
	}
	elsif($device_type_code eq '6')
	{
		$logfile = $logfile . "/mei/" . $timestamp;
		push @logs, "Log File          : $logfile";
		open(LOG, ">>$logfile");
		print LOG '[' . localtime() . "] $machine_id $data\n";	
		close(LOG);
	}
	else
	{
		$logfile = $logfile . "/other/" . $timestamp;
		push @logs, "Log File          : $logfile";
		open(LOG, ">>$logfile");
		print LOG '[' . localtime() . "] $machine_id $data\n";	
		close(LOG);
	}
	
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	$DATABASE->insert(	table=> 'machine_command',
						insert_columns=> 'modem_id, command',
						insert_values=> [$machine_id, unpack("H*", $response)] );

	return (\@logs);
}

sub logESuds
{
	my ($machine_id, $logfile, $data) = @_;
	my $diag_code = unpack("n", substr($data, 0, 2));
	my $timestamp = substr($data, 2);
	my $diag_msg = $esuds_log_types{$diag_code};
	if(not defined $diag_msg)
	{
		$diag_msg = "UNKNOWN_CODE_$diag_code";
	}
	
	open(LOG, ">>$logfile");
	print LOG localtime() . ',' . unpack("H*",$timestamp) . ",$machine_id,$diag_msg\n";	
	close(LOG);
}

