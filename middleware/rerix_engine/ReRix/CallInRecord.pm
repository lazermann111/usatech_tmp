package ReRix::CallInRecord;

use strict;

use Evend::Database::Database;        # Access to the Database
use ReRix::Utils;

my $status_incomplete 	= 'I';
my $status_complete 	= 'S';
my $status_failed 		= 'U';
my $status_unknown 		= 'X';

my $call_type_unknown 	= 'U';
my $call_type_auth 		= 'A';
my $call_type_batch 	= 'B';

my $auth_approved 		= 'Y';
my $auth_denied 		= 'N';
my $auth_failed 		= 'F';

my $credit_trans		= 'C';
my $passcard_trans		= 'P';
my $cash_trans			= 'M';

my $message_inbound		= 'I';
my $message_outbound	= 'O';

sub start
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}
	
	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_start';
	my @proc_args = (":ev_number", ":network_layer", ":ip_address", ":modem_id", ":rssi");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}


sub finish
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_finish';
	my @proc_args = (":ev_number");
	my @proc_values = @_[1..$#_];
 	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub set_status
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_set_status';
	my @proc_args = (":ev_number", ":status");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub set_type
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_set_type';
	my @proc_args = (":ev_number", ":type");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub add_auth
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_add_auth';
	my @proc_args = (":ev_number", ":card_type", ":auth_amount", ":approved_flag");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub add_message
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_add_message';
	my @proc_args = (":ev_number", ":direction", ":num_bytes");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub add_trans
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_add_trans';
	my @proc_args = (":ev_number", ":card_type", ":trans_amount");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub device_sent_config
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_set_device_sent_config';
	my @proc_args = (":ev_number");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub server_sent_config
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_set_server_sent_config';
	my @proc_args = (":ev_number");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub dex_received
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_set_dex_received';
	my @proc_args = (":ev_number", ":file_size");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub device_initialized
{
	if(not defined $_[1])
	{
		print("[" . localtime() . "] Failed to write call-in record: machine_id is undefined");
		return;
	}

	if($_[1] eq 'EV017289')
	{
		# EV017289 is the heartbeat, and we don't want to record logs for the heartbeat because it 
		# will create a ton of records in the database
		return;
	}

	my ($database_handle) = @_;

	my $proc_name = 'pkg_call_in_record_maint.sp_set_initialized';
	my @proc_args = (":ev_number");
	my @proc_values = @_[1..$#_];
	
	&call_proc($database_handle, $proc_name, \@proc_args, \@proc_values);
}

sub call_proc
{
	my ($database_handle, $proc_name, $args_ref, $values_ref) = @_;
	
	my @proc_args = @$args_ref;
	my @proc_values = @$values_ref;
	
	my $proc_arg_list = join(", ", @proc_args);
	my $proc_ref = $database_handle->prepare("BEGIN $proc_name($proc_arg_list); END;");

	print("[" . localtime() . "] Executing: $proc_name(" . join(", ", @proc_values) . ")\n");

	for(my $i = 0; $i <= $#proc_args; $i++)
	{
		$proc_ref->bind_param($proc_args[$i], $proc_values[$i]);
	}
	
	$proc_ref->execute;
	my $db_err_msg = $database_handle->errstr;
	$proc_ref->finish();
	
	if(defined $db_err_msg)
	{
		print("[" . localtime() . "] $proc_name(" . join(", ", @proc_values) . ") FAILED! $db_err_msg\n");
	}
}

1;
