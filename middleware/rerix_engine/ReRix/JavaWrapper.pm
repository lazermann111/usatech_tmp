use strict;

package ReRix::JavaWrapper;

use Evend::ReRix::Shared;
use ReRix::Send;

sub parse
{
	my ($DATABASE, $command_hashref) = @_;
	my @logs;

	my $message = pack("H*",$command_hashref->{inbound_command});
	my $msgnbr = $command_hashref->{msg_no};
	my $machineID = $command_hashref->{machine_id}; 
	my $inboundID = $command_hashref->{inbound_id}; 

	push @logs, "Machine ID        : $machineID";
	push @logs, "Message Number    : $msgnbr";
	push @logs, "Inbound ID        : $inboundID";
	push @logs, ("    	Message    	    : ".unpack("H*",$message));

	$DATABASE->update(
				table			=> 'MACHINE_COMMAND_INBOUND',
				update_columns	=> 'Execute_Cd',
				update_values	=> ['J'],
				where_columns	=> ['Inbound_ID = ?'],
				where_values	=> [$inboundID]
			);
			
	push @logs, " -J- Executing command via Java";
	
	return(\@logs);
}

1;
