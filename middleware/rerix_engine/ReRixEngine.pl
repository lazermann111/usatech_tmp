#!/usr/bin/perl -w

use strict;

use Evend::Database::Database;        # Access to the Database

use POSIX ":sys_wait_h";
use Time::HiRes qw(gettimeofday);

use lib '/opt/ReRixEngine2';

use ReRix::Init;					# Init version 1, 2, 3
##use ReRix::SoftDnld;
use ReRix::Ping;

##use ReRix::Alarms;

##use ReRix::Errors;
##use ReRix::Signal;

##use ReRix::Trans;
use ReRix::Trans20;					# Version 2.0 trans commands

# The following use statements are version 3 dependent
use ReRix::ESuds;					# ESuds Commands
use ReRix::DataLog;					# Generic Data Log
use ReRix::Gx;						# Gx Peek & Poke
use ReRix::Update;					# Update status request processing
use ReRix::FileTransferIncoming;	# File Transfer stuff
use ReRix::FileTransferOutgoing;	# File Transfer stuff
use ReRix::InfoRequest;				# UNIX time, BCD time, GX Config reqest
use ReRix::ClientVersion;			# Client Version stuff (only eSuds supported currently)

##use ReRix::TransNoCrypt;
##use ReRix::Settings;
##use ReRix::Messages;
##use ReRix::Counters;
##use ReRix::ProductIDs;
##use ReRix::Cash;

##use ReRix::SellThrough;
##use ReRix::Fill;
##use ReRix::Pricing;
##use ReRix::Dex;

use ReRix::Sony;

use ReRix::Utils;

# hash describes parsers.  the hex value of the command is the key.
# the value is a arrayref [ &<parser>, 'description', fork status ]

my %command_parsers = (
##						'22'	=>	[\&ReRix::SellThrough::parse_baselines,
##										'(*) Simple Sell Through', 0],
##						'23'	=>	[\&ReRix::SellThrough::parse_baselines,
##										'(#) Simple Fill Through', 0],
##						'24'	=>	[\&ReRix::SellThrough::parse_baselines,
##										'(*) Complex Sell Through', 0],
##						'25'	=>	[\&ReRix::SellThrough::parse_baselines,
##										'(#) Complex Fill Through', 0],
##						'27'	=>	[\&ReRix::Fill::parse,
##										'(\') Complete Fill Information', 0],
						'2A'	=>	[\&ReRix::Trans20::parse_net_batch_v2,
										'(2Ah) Net Authorization Batch V2', 1],
						'2B'	=>	[\&ReRix::Trans20::parse_local_batch_v2,
										'(2Bh) Local Authorization Batch V2', 1],
						'2E'	=>	[\&ReRix::DataLog::parse,
										'(2Eh) Generic Data Log', 0],
						'2F'	=>	[\&ReRix::Ping::ack,
										'(2Fh) Generic ACK', 1],
##						'31'	=>	[\&ReRix::Messages::parse_req,
##										'(1) User Messages Request', 0],
##						'32'	=>	[\&ReRix::ProductIDs::parse_req,
##										'(2) Product ID Messages Request', 0],
##						'33'	=>	[\&ReRix::Settings::parse_req,
##										'(3) Settings Request', 0],
##						'35'	=>	[\&ReRix::Trans::parse_auth_wamt,
##										'(5) Authorization w/ Amnt Request', 1],
##						'36'	=>	[\&ReRix::Trans::parse_auth,
##										'(6) Authorization Request', 1],
##						'37'	=>	[\&ReRix::Trans::parse_net_batch,
##										'(7) Net Authorization Batch', 1],
##						'38'	=>	[\&ReRix::Trans::parse_local_batch,
##										'(8) Local Authorization Batch', 1],
##						'3A'	=>	[\&ReRix::Messages::parse_ack,
##										'(:) User Messages ACK', 0],
##						'3B'	=>	[\&ReRix::ProductIDs::parse_ack,
##										'(;) Product ID Messages ACK', 0],
##						'3C'	=>	[\&ReRix::Settings::parse_ack,
##										'(<) Settings ACK', 0],
##						'3D'	=>	[\&ReRix::Counters::parse_abbrev,
##										'(=) SCU Counters Abbreviated', 0],
##						'3E'	=>	[\&ReRix::Alarms::parse_scu,
##										'(>) SCU Alarms', 0],
##						'3F'	=>	[\&ReRix::Counters::parse_complete,
##										'(?) SCU Counters Complete', 0],
##						'41'	=>	[\&ReRix::Alarms::parse_dex,
##										'(A) Alarms', 0],
##						'43'	=>	[\&ReRix::Pricing::parse_simple,
##										'(C) Price/Column Info Simple', 0],
##						'45'	=>	[\&ReRix::Errors::parse,
##										'(E) Client Errors', 0],
##						'47'	=>	[\&ReRix::Pricing::parse_pos,
##										'(G) Price Change Complete', 0],
##						'48'	=>	[\&ReRix::Signal::parse,
##										'(H) Signal Stats', 0],
						'49'	=>	[\&ReRix::Init::parse,
										'(I) Initialize', 0],
##						'4F'	=>	[\&ReRix::Cash::parse,
##										'(O) Cash Accounting', 0],
##						'52'	=>	[\&ReRix::Dex::parse,
##										'(R) DEX Command Response', 0],
##						'56'	=>	[\&ReRix::SoftDnld::parse_V,
##										'(V) Version', 0],
##						'58'	=>	[\&ReRix::SoftDnld::parse_XY,
##										'(X) Start Software Download', 0],
##						'59'	=>	[\&ReRix::SoftDnld::parse_XY,
##										'(Y) Start Software Download', 0],
						'5B'	=>	[\&ReRix::Trans20::parse_cash_sale,
										'(5Bh) Cash Sales Detail', 1],
						'5C'	=>	[\&ReRix::Ping::alive_alert_v20,
										'(5Ch) Alive Alert', 1],
						'5E'	=>	[\&ReRix::Trans20::parse_auth_v2,
										'(5Eh) Authorization Request V2', 1],
						'5F'	=>	[\&ReRix::Sony::parse_info,
										'(`) Sony Info', 0],
						'60'	=>	[\&ReRix::Sony::parse_info,
										'(`) Sony Info Old', 0],
##						'63'	=>	[\&ReRix::Pricing::parse_complex,
##										'(C) Price/Column Info Complex', 0],
##						'67'	=>	[\&ReRix::Pricing::parse_neg,
##										'(G) Price Change Complete', 0],
						'75'	=>	[\&ReRix::Ping::device_control_ack,
										'(75h) Device Control ACK', 0],
##						'78'	=>	[\&ReRix::SoftDnld::parse_x,
##										'(x) Software Download Complete', 0],
						'7A'	=>	[\&ReRix::Ping::parse,
										'(z) Client to Server Ping', 0],
						'7C'	=>	[\&ReRix::FileTransferIncoming::start,
										'(7Ch) File Transfer Incoming Start', 1],
						'7D'	=>	[\&ReRix::FileTransferOutgoing::start_ack,
										'(7Dh) File Transfer Outgoing Start ACK', 1],
						'7E'	=>	[\&ReRix::FileTransferIncoming::transfer,
										'(7Eh) File Transfer Incoming', 1],
						'7F'	=>	[\&ReRix::FileTransferOutgoing::transfer_ack,
										'(7Fh) File Transfer Outgoing ACK', 1],
						'80'	=>	[\&ReRix::FileTransferIncoming::kill_transfer,
										'(80h) File Transfer Incoming Kill', 1],
						'81'	=>	[\&ReRix::FileTransferOutgoing::kill_transfer_ack,
										'(81h) File Transfer Outgoing Kill ACK', 1],
						'82'	=>	[\&ReRix::InfoRequest::parse,
										'(82h) Client to Server Request (Info)', 1],
						'86'	=>	[\&ReRix::Gx::counters,
										'(86h) Gx Counters', 1],
						'87'	=>	[\&ReRix::Gx::peek,
										'(87h) Gx Peek', 1],
						'88'	=>	[\&ReRix::Gx::poke,
										'(88h) Gx Poke', 1],
						'8E'	=>	[\&ReRix::Init::parse_V3,
										'(8Eh) Initialize V3', 1],
						'91'	=>	[\&ReRix::Ping::nak,
										'(91h) Generic NAK', 0],
						'92'	=>	[\&ReRix::Update::update_status_req,
										'(92h) Update Status Req', 1],
						'93'	=>	[\&ReRix::Trans20::parse_local_batch_v2_bcd,
										'(93h) Local Authorization Batch V2 (BCD)', 1],
						'94'	=>	[\&ReRix::Trans20::parse_cash_sale_v2_bcd,
										'(94h) Cash Sales Detail V2 (BCD)', 1],
#						'96'	=>	[\&ReRix::Trans20::parse_cash_sale_v3,
#										'(96h) Cash Sales Detail V3', 1],
#						'97'	=>	[\&ReRix::Trans20::parse_cash_sale_v3,
#										'(96h) Cash Sales Detail V3 (BCD)', 1],
#						'98'	=>	[\&ReRix::Ping::nak_V2,
#										'(98h) Generic NAK V2', 1],
						'99'	=>	[\&ReRix::ClientVersion::parse,
										'(99h) Client Version', 0],
						'9A'	=>	[\&ReRix::ESuds::parse,
										'(9Ah) ESuds', 1],
						'B1'	=>	[\&ReRix::Init::parse_V2,
										'(I) Initialize V2', 0],
##						'B5'	=>	[\&ReRix::TransNoCrypt::parse_auth_wamt,
##								'(debug) No Crypt Authorization Request', 1],
##						'B7'	=>	[\&ReRix::TransNoCrypt::parse_net_batch,
##								'(debug) No Crypt Net Authorization Batch', 1],
##						'B8'	=>	[\&ReRix::TransNoCrypt::parse_local_batch,
##								'(debug) No Crypt Local Authorization Batch', 1],
						'B9'	=>	[\&ReRix::Sony::parse_info_V2,
										'(`) Sony Info V2', 1],
					);

my $process_commands = 1;
my $sig_int_cnt = 0;
my $last_inbound_id = 0;
my $command_counter = 0;
my $min_frk_allowed = 8;	#always allow at least N forks at all times
my $max_mem_use_parent = 25000;	#max Kb of main process before restart
my $max_mem_use_total = 300000;	#max Kb allowed for all processing
my $DATABASE = Evend::Database::Database->new(	print_query => 1, 
												execute_flatfile=>1, 
												debug => 1, 
												debug_die => 0,
												print_query => 0);

sub SIGINT
{ 
	$SIG{INT} = \&SIGINT;
	$sig_int_cnt++;
	print "ReRixEngine: caught control-c (#$sig_int_cnt)\n";
	if ($sig_int_cnt == 1)
	{
		print "ReRixEngine: attempting to shut down safely...\n";
		$process_commands = 0;
	}
	else
	{
		print "ReRixEngine: attempting to shut down immediately...\n";
		$DATABASE->close();
		exit 1;
	}
};

my %Kid_Status;
#sub REAPER {
#	my $child;
#	while (($child = waitpid(-1,WNOHANG)) > 0) {
#		$Kid_Status{$child} = $?;
#	}
#	$SIG{CHLD} = \&REAPER;
#}
#$SIG{CHLD} = \&REAPER;
$SIG{CHLD} = 'IGNORE';

$DATABASE->{print_query} = 0;
$DATABASE->query(query => "alter session set nls_date_format = 'MM/DD/YYYY HH24:MI:SS'");

#logline("Purging existing inbound command queue...");
#$DATABASE->update(
#		table			=> 'machine_command_inbound',
#		update_columns	=> 'Execute_Cd',
#		update_values	=> ['P'],
#		where_columns	=> ['Execute_Cd in (?,?)'],
#		where_values	=> ['N','F']);

my ($vm_mem_free, $phys_mem_free, $mem_inuse, $ps_cnt, $ps_href) = &get_mem_stats(['ReRixEngine.pl']);
while($process_commands)
{       
	$SIG{INT} = \&SIGINT;
	# workaround for problem with losing date format
	$DATABASE->query(query => "alter session set nls_date_format = 'MM/DD/YYYY HH24:MI:SS'");

	my $array_ref = $DATABASE->select(
		table			=> qq{
			(
				SELECT MIN(TO_NUMBER(inbound_id)) inbound_id, machine_id
				FROM machine_command_inbound t2
				WHERE execute_cd = DECODE((
					SELECT COUNT(1)
					FROM machine_command_inbound t3
					WHERE t2.machine_id = t3.machine_id
					AND execute_cd = 'F'
					AND execute_date >= (sysdate-(5/1440))
				), 0, 'N', NULL)
				GROUP BY machine_id
			) x, machine_command_inbound t1
		}
		,select_columns	=>	't1.inbound_id, t1.machine_id, t1.inbound_date, t1.timestamp, t1.inbound_command, t1.execute_date, t1.execute_cd, t1.inbound_msg_no'
		,where_columns	=> [
			'x.inbound_id = t1.inbound_id ORDER BY t1.inbound_date'
		]
		,where_values	=> undef
	);
								
	my @commands;
	if($array_ref->[0][0])
	{ 
		foreach(@$array_ref )
		{
			my %tmp_command;
	
			# get the message data
			$tmp_command{inbound_id} = $_->[0];
			$tmp_command{machine_id} = $_->[1];
			$tmp_command{inbound_date} = $_->[2];
			$tmp_command{timestamp} = $_->[3];
			$tmp_command{inbound_command} = $_->[4];
			$tmp_command{execute_date} = $_->[5];
			$tmp_command{execute_cd} = $_->[6];
			$tmp_command{msg_no} = $_->[7];
			
			if( $last_inbound_id < $tmp_command{inbound_id} )
			{
				$last_inbound_id = $tmp_command{inbound_id};
			}
			
			# get the device data
			($tmp_command{device_id}, $tmp_command{SSN}, $tmp_command{device_type}) = ReRix::Utils::get_device_data($_->[1], $DATABASE);
			
			my $response_no = ReRix::Utils::get_message_number($DATABASE);
			if(not defined $response_no)
			{
				$response_no = $_->[7];
			}
			
			$tmp_command{response_no} = $response_no;
			
			logline("Loading msg $tmp_command{inbound_id} from $tmp_command{machine_id}");
			push @commands, \%tmp_command;
		}
	}
	else
	{
		#logit("No Data\n");
	}

	$process_commands = 0 if ($ps_href->{$$}->{mem} > $max_mem_use_parent);
#	while(my $command = shift @commands)
	while(( (($ps_cnt - 1) < $min_frk_allowed) || ((($ps_cnt - 1) >= $min_frk_allowed) && ($mem_inuse < $max_mem_use_total)) ) && (my $command = shift @commands))
	{
		($vm_mem_free, $phys_mem_free, $mem_inuse, $ps_cnt, $ps_href) = &get_mem_stats(['ReRixEngine.pl']);
		logline("avg swap free=$vm_mem_free, avg real free=$phys_mem_free, ps mem used=$mem_inuse, ps_cnt=$ps_cnt; min_frk_allowed=$min_frk_allowed, ps_href->{$$}->{mem}=".$ps_href->{$$}->{mem}.", max_mem_use_parent=$max_mem_use_parent");
		my @logs;
		
		#logline("Executing msg $command->{inbound_id} from $command->{machine_id}");
		
		my $command_code = uc( substr($command->{inbound_command},0,2) );
		
		$DATABASE->query ( query => qq{UPDATE machine_command_inbound
										SET num_times_executed = num_times_executed + 1
										WHERE inbound_id = ?},
										values => [$command->{inbound_id}]);

		if(defined $command_parsers{$command_code})
		{
			push(@logs, $command_counter++ . " - " . $command_parsers{$command_code}->[1] . " --------------------------");
		}
		else
		{
			logit($command->{inbound_id}, $command->{machine_id}, "****************** Command Not Found: $command_code");
			#ReRix::Utils::send_alert("ReRixEngine: ****************** Command Not Found: $command_code");
		}
		
		if(defined $command_parsers{$command_code})
		{
			push(@logs, "Machine ID        : $command->{machine_id}");
			if(defined $command->{device_id})
			{
				push(@logs, "Device ID         : $command->{device_id}");
				push(@logs, "SSN               : $command->{SSN}");
				push(@logs, "Device Type       : " . &get_device_desc($command->{device_type}));
			}
			push(@logs, "Message Number    : $command->{msg_no}");
			push(@logs, "Message           : " . $command->{inbound_command});
			
			if( $command_parsers{$command_code}->[2] == 0 )
			{
				# don't need to fork to execute this command
				push(@logs, @{$command_parsers{$command_code}->[0] ($DATABASE,$command)});
				logline("NOT forking for msg $command->{inbound_id} from $command->{machine_id}");
				
				foreach(@logs)
				{
					logit($command->{inbound_id}, $command->{machine_id}, $_);
				}
				
				$DATABASE->update(
						table			=> 'machine_command_inbound',
						update_columns	=> 'Execute_Cd',
						update_values	=> ['Y'],
						where_columns	=> ['Inbound_ID = ?'],
						where_values	=> [$command->{inbound_id}] );
			}
			else
			{
				# fork to handle this command
				logline("Forking for msg $command->{inbound_id} from $command->{machine_id}");
				$DATABASE->update(
							table			=> 'Machine_Command_Inbound',
							update_columns	=> 'Execute_Cd',
							update_values	=> ['F'],
							where_columns	=> ['Inbound_ID = ?'],
							where_values	=> [$command->{inbound_id}] );

				$DATABASE->clear_statement_handles();
#				$DATABASE->{handle}->disconnect();

				$SIG{INT} = 'DEFAULT';
#				$SIG{CHLD} = \&REAPER;
				$SIG{CHLD} = 'IGNORE';

				my $pid = fork;
				# push(@logs, "Fork PID = $pid"); # ok
				if( !defined $pid )
				{
					logline("Failed to fork for msg $command->{inbound_id} from $command->{machine_id}");
#					$DATABASE->database_connect();
				}
				elsif( $pid )
				{
					# parent
					$SIG{INT} = \&SIGINT;
					#logline("Parent $pid executing msg $command->{inbound_id} from $command->{machine_id}");

#					$DATABASE->database_connect();

					#$DATABASE->update(
					#	table			=> 'Machine_Command_Inbound',
					#	update_columns	=> 'Execute_Cd',
					#	update_values	=> ['E'],
					#	where_columns	=> ['Inbound_ID = ?'],
					#	where_values	=> [$command->{inbound_id}]
					#	);
				}
				elsif( $pid == 0 )
				{
					# child

					$DATABASE->{handle}->{InactiveDestroy} = 1;
#					$DATABASE->close();

#					$DATABASE->close_child();

					#logline("Child executing msg $command->{inbound_id} from $command->{machine_id}");

					my $DATABASE2 = Evend::Database::Database->new(
												print_query			=> 1, 
												execute_flatfile	=> 1, 
												debug				=> 1, 
												debug_die			=> 0);

					$SIG{TERM} = sub {$DATABASE2->close();print "Child TERMed\n"; exit 0;};
					$SIG{INT}  = sub {$DATABASE2->close();print "Child INTed\n"; exit 0;};

					# push(@logs, "Child executing...");	# ok
					push(@logs, @{$command_parsers{$command_code}->[0] ($DATABASE2,$command)});
					
					foreach(@logs)
					{
						logit($command->{inbound_id}, $command->{machine_id}, $_);
					}
					
					$DATABASE2->update(
							table			=> 'machine_command_inbound',
							update_columns	=> 'Execute_Cd',
							update_values	=> ['Y'],
							where_columns	=> ['Inbound_ID = ?'],
							where_values	=> [$command->{inbound_id}] );
					
					#logline("Child finished msg $command->{inbound_id} from $command->{machine_id}");
					$DATABASE2->close;

					exit;
				}
			}
		}
		else
		{
			$DATABASE->update(
						table			=> 'Machine_Command_Inbound',
						update_columns	=> 'Execute_Cd',
						update_values	=> ['E'],
						where_columns	=> ['Inbound_ID = ?'],
						where_values	=> [$command->{inbound_id}] );
		}
		
		#logline("Finished msg $command->{inbound_id} from $command->{machine_id}");

		# now send pending messages depending on what kind of device this is
		if(not defined $command->{device_type})
		{
			# Legacy device
		}
		elsif($command->{device_type} eq '0')
		{
			# G4
		}
		elsif($command->{device_type} eq '1')
		{
			# G5
		}
		elsif($command->{device_type} eq '3')
		{
			# Brick
		}
		elsif($command->{device_type} eq '4')
		{
			# Sony PictureStation
		}
		elsif($command->{device_type} eq '5')
		{
			# eSuds Room Controller
			foreach(@{ReRix::Update::send_pending($DATABASE, $command, 999)})
			{
				logit($command->{inbound_id}, $command->{machine_id}, $_);
			}
		}
		elsif($command->{device_type} eq '6')
		{
			# MEI Telemeter
		}
	}

	$SIG{INT} = \&SIGINT;
	sleep(1);
}

logline("ReRixEngine: ****************** Shutting Down: disconnecting parent from database");
$DATABASE->close();

($vm_mem_free, $phys_mem_free, $mem_inuse, $ps_cnt, $ps_href) = &get_mem_stats(['ReRixEngine.pl']);
my $sleep_timeout = 60;	#second before force disconnect occurs IF no children exit within this time limit
my $last_inuse_diff_time = time();
my $last_inuse_num = $ps_cnt;
logline("ReRixEngine: ****************** Shutting Down: checking for any child processes still running...");
while ((($sleep_timeout + $last_inuse_diff_time) >= time()) && ($ps_cnt > 1))
{
	if ($ps_cnt != $last_inuse_num)
	{
		$last_inuse_num = $ps_cnt;
		$last_inuse_diff_time = time();
	}

	logline("ReRixEngine: ****************** Shutting Down: waiting for #".($ps_cnt - 1)." child processes to complete and exit (timeout in ".($sleep_timeout + $last_inuse_diff_time - time())." sec)");
	sleep 1;
	($vm_mem_free, $phys_mem_free, $mem_inuse, $ps_cnt, $ps_href) = &get_mem_stats(['ReRixEngine.pl']);
}
logline("ReRixEngine: ****************** Shutting Down: shut down");
exit 1;




##### subroutines #####
sub logit
{
	my $num = shift;
	my $machine = shift;
	my $line = shift;
	my @epoch_sec = gettimeofday();
	my $ltime = localtime($epoch_sec[0]);
	my @localtime = split(/\s+/, $ltime);
	print sprintf("%s %s %2s %s.%06d %04d", @localtime[0..3], $epoch_sec[1], $localtime[4])." $machine [$num] $line\n";
}

sub logline
{
	my $msg = shift;
	my @epoch_sec = gettimeofday();
	my $ltime = localtime($epoch_sec[0]);
	my @localtime = split(/\s+/, $ltime);
	print sprintf("%s %s %2s %s.%06d %04d", @localtime[0..3], $epoch_sec[1], $localtime[4])." $msg\n";
}

sub get_device_desc
{
	my ($device_type_code) = @_;
	if(not defined $device_type_code)
	{
		return "Unknown (Legacy)";
	}
	elsif($device_type_code eq '0')
	{
		return "G4";
	}
	elsif($device_type_code eq '1')
	{
		return "G5";
	}
	elsif($device_type_code eq '3')
	{
		return "Brick";
	}
	elsif($device_type_code eq '4')
	{
		return "Sony PictureStation";
	}
	elsif($device_type_code eq '5')
	{
		return "eSuds Room Controller";
	}
	elsif($device_type_code eq '6')
	{
		return "MEI Telemeter";
	}
	else
	{
		return "Unknown (Other)";
	}
}

sub get_mem_stats ($)
{	
	my $ps_list_aref = $_[0];	#list of process names to return summed mem use results for (if undef, then returns sum of all)
	my $ps_list_regex;
	if (defined $ps_list_aref && scalar @{$ps_list_aref} > 0)
	{
		my @ps_list_regex;
		foreach my $ps (@{$ps_list_aref})
		{
			push @ps_list_regex, quotemeta $ps;
		}
		$ps_list_regex = join('|', @ps_list_regex);
	}
	local $SIG{CHLD} = 'DEFAULT';
	
	### determine total physical memory free ###
	my @mem_free_cmd;
	if (open(IPCCMD, 'vmstat |'))
	{
		@mem_free_cmd = <IPCCMD>;
		close IPCCMD || warn "bad vmstat: $! $?";
	}
	else
	{
		warn "can't fork: $!";
	}
	my ($free_swap, $free_phys) = ((split(/ /, $mem_free_cmd[2]))[4..5]);
	
	### determine total memory in use ###
	my @mem_use_cmd;
	if (open(IPCCMD, 'ps -opid -oppid -eovsz -oargs |'))	#solaris-only command, can be rewritten per os as necesary
	{
		@mem_use_cmd = <IPCCMD>;
		close IPCCMD || warn "bad ps: $! $?";
	}
	else
	{
		warn "can't fork: $!";
	}
	chomp @mem_use_cmd;
	my %mem_hsh;
	my %ps_hsh;
	for (my $i = 1; $i < scalar @mem_use_cmd; $i++)
	{
		if ($mem_use_cmd[$i] =~ m/^\s*(\d+)\s+(\d+)\s+(\d+)\s+(.*)$/)
		{
			my ($pid, $ppid, $mem, $name) = ($1, $2, $3, $4);
			if ( (!defined $ps_list_regex) || ($name =~ m/($ps_list_regex)/) )
			{
				unless ($name eq '')	#check for defunct processes
				{
					$mem_hsh{$name} = [0, 0] unless (defined $mem_hsh{$name});
					$mem_hsh{$name}->[0]++;	#num of process instances
					$mem_hsh{$name}->[1] += $mem;
				}
				$ps_hsh{$pid} = {
					'ppid'	=> $ppid
					,'mem'	=> $mem
					,'name'	=> $name
				};
			}
		}
		else
		{
			warn "*** [warning] get_mem_stats: found unknown format ps line '$mem_use_cmd[$i]'.\n";
		}
	}
	my ($ps_cnt, $inuse) = (0, 0);
	$ps_cnt += $mem_hsh{$_}->[0] foreach (keys %mem_hsh);
	$inuse += $mem_hsh{$_}->[1] foreach (keys %mem_hsh);

	return ($free_swap, $free_phys, $inuse, $ps_cnt, \%ps_hsh);
}
