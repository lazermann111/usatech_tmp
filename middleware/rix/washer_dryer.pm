#This package is used to handle routines
#concerning the washers and dryers.

#Testing right now so don't use this line till stable
#package Evend::Washer_Dryer::Washer_Dryer;
package washer_dryer;

#I assume that database.pm has already been 
#added in otherwise I won't work.
use strict;

use Exporter    ();
use vars        qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
$VERSION        = '0.01';
@ISA            = qw(Exporter);
@EXPORT         = qw(rix_Send27Rec rix_Get28Rec Laundry_Status rix_Get_Mach_User rix_Page_User is_a_washer);
@EXPORT_OK      = qw();

use Evend::Mail::Mail;

########################################
#
# rix_Send27Rec
#
########################################
# Description: Dumps the 27 setup record to the washer dryer.
# 
# Uses Globals: @DATA_LIST
#
# Written By: Chris Donohue
# Created: 28Nov2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Send27Rec
{
  my ($mach_num) = @_;
  my $format27 = "H2Ca10Ca3CCCCCCCCCh2h2h2CCCCCCCCCCCCCxC";
  my $col_list_27 = "RECORD_TYPE, MODEL_CD, MACHINE_NUMBER, POSITION_CD, LOCATION_NUMBER";
  $col_list_27 .= ", REG_PRICE, REG_COLD_PRICE, REG_WARM_PRICE, REG_HOT_PRICE, SP_PRICE";
  $col_list_27 .= ", SP_COLD_PRICE, SP_WARM_PRICE, SP_HOT_PRICE, SUPER_CYC_PRICE_ADD";
  $col_list_27 .= ", SUPER_CYC_TYPE, FEATURE_1, FEATURE_2, FEATURE_3, SP_PRICE_DAYS";
  $col_list_27 .= ", SP_PRICE_BEGIN_TIME, SP_PRICE_END_TIME, REG_DRYER_TIME, SP_DRYER_TIME";
  $col_list_27 .= ", WASH_TIME, RINSES_PER_CYC, RINSE_AGIT_TIME, TOP_SPIN_SPEED";
  $col_list_27 .= ", COIN_1, COIN_2, PENNY_OFFSET, ACTION_CD";

  #CPD - right now this is gonna be hardcoded.
  # Vals should get the 28 rec and treat it like a 27
  #my @Vals = unpack $format27, $DATA_LIST[0];
  
  my $OracleDB = Evend::Database::Database->new( print_query=>0, primary => 'taz3', backup => 'taz3' );
  my $ref = $OracleDB->select (
                    table => "laundry",
                    select_columns => $col_list_27,
                    where_columns => ['MACHINE_ID = ?'],
                    where_values => [$mach_num]
                    );

  my @Vals;
  foreach my $row (@$ref) { 
    #Translate Values
    $Vals[0]  = sprintf "%lx", $$row[0];
    $Vals[1]  = $$row[1];
    $Vals[2]  = $$row[2];
    $Vals[3]  = ord($$row[3]);
    $Vals[4]  = $$row[4];
    $Vals[5]  = sprintf("%.0f", ($$row[5] / 0.05));
    $Vals[6]  = sprintf("%.0f", ($$row[6] / 0.05));
    $Vals[7]  = sprintf("%.0f", ($$row[7] / 0.05));
    $Vals[8]  = sprintf("%.0f", ($$row[8] / 0.05));
    $Vals[9]  = sprintf("%.0f", ($$row[9] / 0.05));
    $Vals[10]  = sprintf("%.0f", ($$row[10] / 0.05));
    $Vals[11]  = sprintf("%.0f", ($$row[11] / 0.05));
    $Vals[12]  = sprintf("%.0f", ($$row[12] / 0.05));
    $Vals[13]  = sprintf("%.0f", ($$row[13] / 0.05));
    $Vals[14] = ord($$row[14]);
    $Vals[15] = $$row[15];
    $Vals[16] = $$row[16];
    $Vals[17] = $$row[17];
    $Vals[18] = $$row[18];
    $Vals[19] = $$row[19];
    $Vals[20] = $$row[20];
    $Vals[21] = $$row[21];
    $Vals[22] = $$row[22];
    $Vals[23] = $$row[23];
    $Vals[24] = $$row[24];
    $Vals[25] = $$row[25];
    $Vals[26] = $$row[26];
    $Vals[27]  = sprintf("%.0f", ($$row[27] / 0.05));
    $Vals[28]  = sprintf("%.0f", ($$row[28] / 0.05));
    $Vals[29] = $$row[29];
    $Vals[30] = ord($$row[30]);
  }

  my $ins_str = pack($format27, @Vals);
  $OracleDB->close();
  return $ins_str;
}

########################################
#
# rix_Get28Rec
#
########################################
# Description: Gets the 28 rec and inserts the data
# 
# Uses Globals: @DATA_LIST
#
# Written By: Chris Donohue
# Created: 28Nov2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Get28Rec
{
  my ($mach_num, $line) = @_;
  #print STDERR "CPD Rec28 ";
  #print STDERR $line;
  #print STDERR "\n";
  #print STDERR "CPD HEX ";
  foreach my $tmp (split //, $line) 
  { 
    #print STDERR "$tmp=";
    my $mumble = sprintf "%lx", $tmp;
    #print STDERR $mumble; 
    #print STDERR " ";
  }

  #print STDERR "\n";

  my $OracleDB = Evend::Database::Database->new( print_query=>1, primary => 'taz3', backup => 'taz3' );

  my $format28 = "H2Ca10Ca3CCCCCCCCCh2h2h2CCCCCCCCCCCCCxCCCCCH4H4H4H4H4H4H4H4xxxxH4H6CCCxxCCH4H4CCCCCCCCH4H4H4H4H4H4H4H4H6CCCCCC";

  my $col_list_28  = "RECORD_TYPE, "; #1
  $col_list_28 .= "MODEL_CD, "; 
  $col_list_28 .= "MACHINE_NUMBER, "; 
  $col_list_28 .= "POSITION_CD, "; 
  $col_list_28 .= "LOCATION_NUMBER, "; 
  $col_list_28 .= "REG_PRICE, "; 
  $col_list_28 .= "REG_COLD_PRICE, "; 
  $col_list_28 .= "REG_WARM_PRICE, "; 
  $col_list_28 .= "REG_HOT_PRICE, "; 
  $col_list_28 .= "SP_PRICE, ";  #10
  $col_list_28 .= "SP_COLD_PRICE, ";
  $col_list_28 .= "SP_WARM_PRICE, ";
  $col_list_28 .= "SP_HOT_PRICE, ";
  $col_list_28 .= "SUPER_CYC_PRICE_ADD, ";
  $col_list_28 .= "SUPER_CYC_TYPE, ";
  $col_list_28 .= "FEATURE_1, ";
  $col_list_28 .= "FEATURE_2, ";
  $col_list_28 .= "FEATURE_3, ";
  $col_list_28 .= "SP_PRICE_DAYS, ";
  $col_list_28 .= "SP_PRICE_BEGIN_TIME, "; #20
  $col_list_28 .= "SP_PRICE_END_TIME, ";
  $col_list_28 .= "REG_DRYER_TIME, ";
  $col_list_28 .= "SP_DRYER_TIME, ";
  $col_list_28 .= "WASH_TIME, ";
  $col_list_28 .= "RINSES_PER_CYC, ";
  $col_list_28 .= "RINSE_AGIT_TIME, ";
  $col_list_28 .= "TOP_SPIN_SPEED, ";
  $col_list_28 .= "COIN_1, ";
  $col_list_28 .= "COIN_2, ";
  $col_list_28 .= "PENNY_OFFSET, "; #30
  $col_list_28 .= "FACTORY_TEST_1, "; 
  $col_list_28 .= "FACTORY_TEST_2, ";
  $col_list_28 .= "FACTORY_TEST_3, ";
  $col_list_28 .= "FACTORY_THERM_CAL_1, ";
  $col_list_28 .= "FACTORY_THERM_CAL_2, ";
  $col_list_28 .= "CUM_REG_PRICE_COLD_CYC, ";
  $col_list_28 .= "CUM_REG_PRICE_DRY_CYC, ";
  $col_list_28 .= "CUM_REG_PRICE_WARM_CYC, ";
  $col_list_28 .= "CUM_REG_DRYER_TOPOFF_CYC, ";
  $col_list_28 .= "CUM_SP_PRICE_COLD_CYC, "; #40
  $col_list_28 .= "CUM_SP_PRICE_DRY_CYC, "; 
  $col_list_28 .= "CUM_SP_PRICE_HOT_CYC, ";

  $col_list_28 .= "CUM_SERVICE_CYC, ";
  $col_list_28 .= "CUM_MONEY_PENNY_OFFSET, ";
  $col_list_28 .= "CUM_MINUTE_TICKER, ";
  $col_list_28 .= "CUM_PROBE_READS, ";
  $col_list_28 .= "FACTORY_CUM_1, ";
  $col_list_28 .= "FACTORY_CUM_2, ";
  $col_list_28 .= "INT_SUDS_CYC, ";
  $col_list_28 .= "INT_SUDS_RECV_CYC, "; #50 here?

  $col_list_28 .= "INT_REG_PRICE_CYC, ";
  $col_list_28 .= "INT_SP_PRICE_CYC, ";
  $col_list_28 .= "INT_NO_RUNS, ";
  $col_list_28 .= "INT_SRV_CYC, ";
  $col_list_28 .= "INT_TROUBLE_CYC, ";
  $col_list_28 .= "INT_POWER_DOWN, ";
  $col_list_28 .= "INT_UNAUTH_SERV_DOOR_OPEN, ";
  $col_list_28 .= "INT_AUTH_SERV_DOOR_OPEN, ";
  $col_list_28 .= "INT_UNAUTH_COIN_VAULT_OPEN, "; 
  $col_list_28 .= "INT_AUTH_COIN_VAULT_OPEN, "; #60
  $col_list_28 .= "TIME_LAST_SERV_DOOR_OPEN, ";
  $col_list_28 .= "TIME_LAST_COIN_VAULT_OPEN, ";
  $col_list_28 .= "TIME_LAST_TROUBLE_CYC, ";
  $col_list_28 .= "TIME_LAST_POWER_DOWN, ";
  $col_list_28 .= "TIME_PREV_SERV_DOOR_OPEN, ";
  $col_list_28 .= "TIME_PREV_COIN_VAULT_OPEN, ";
  $col_list_28 .= "TIME_PREV_TROUBLE_CYC, ";
  $col_list_28 .= "TIME_PREV_POWER_DOWN, ";
  $col_list_28 .= "TIME_LAST_SETUP, "; 
  $col_list_28 .= "CURRENT_STATUS_1, "; #70
  $col_list_28 .= "CURRENT_STATUS_2, ";
  $col_list_28 .= "CURRENT_STATUS_3, ";
  $col_list_28 .= "DIAG_CD_1, ";
  $col_list_28 .= "DIAG_CD_2, ";
  $col_list_28 .= "DIAG_CD_3, ";
  $col_list_28 .= "FACTORY_DIAG_HIST_1, ";
  $col_list_28 .= "FACTORY_DIAG_HIST_2, ";
  $col_list_28 .= "FACTORY_DIAG_HIST_3, ";
  $col_list_28 .= "FACTORY_FIRMWARE"; #79

  #@Vals = unpack $format28, $line;
  #print STDERR "CPD Cols = $col_list_28\n";
  my @temp = split //, $line;
  #print STDERR "CPD vals = ";

  my @Vals;
  $Vals[0] = ord($temp[0]);
  $Vals[1] = ord($temp[1]);
  $Vals[2] = ($temp[2]);
  $Vals[2] .= ($temp[3]);
  $Vals[2] .= ($temp[4]);
  $Vals[2] .= ($temp[5]);
  $Vals[2] .= ($temp[6]);
  $Vals[2] .= ($temp[7]);
  $Vals[2] .= ($temp[8]);
  $Vals[2] .= ($temp[9]);
  $Vals[2] .= ($temp[10]);
  $Vals[2] .= ($temp[11]);
  #$Vals[2] = "1250000000";
  $Vals[3] = chr($temp[12]+33);
  $Vals[4] = chr($temp[13]+33);
  $Vals[4] .= chr($temp[14]+33);
  $Vals[4] .= chr($temp[15]+33);
  $Vals[5] = ord($temp[16]) * 0.05;
  $Vals[6] = ord($temp[17]) * 0.05;
  $Vals[7] = ord($temp[18]) * 0.05;
  $Vals[8] = ord($temp[19])* 0.05;
  $Vals[9] = ord($temp[20])* 0.05;
  $Vals[10] = ord($temp[21])* 0.05;
  $Vals[11] = ord($temp[22])* 0.05;
  $Vals[12] = ord($temp[23])* 0.05;
  $Vals[13] = ord($temp[24])* 0.05;
  $Vals[14] = chr($temp[25]+33);
  $Vals[15] = chr($temp[26]+33);
  $Vals[16] = chr($temp[27]+33);
  $Vals[17] = chr($temp[28]+33);
  $Vals[18] = chr($temp[29]+33);
  $Vals[19] = chr($temp[30]+33);
  $Vals[20] = chr($temp[31]+33);
  $Vals[21] = ord($temp[32]);
  $Vals[22] = ord($temp[33]);
  $Vals[23] = ord($temp[34]);
  $Vals[24] = ord($temp[35]);
  $Vals[25] = ord($temp[36]);
  $Vals[26] = ord($temp[37]);
  $Vals[27] = ord($temp[38]) * 0.05;
  $Vals[28] = ord($temp[39]) * 0.05;
  $Vals[29] = ord($temp[40]) * 0.05;
  $Vals[30] = chr($temp[42]+33);
  $Vals[31] = chr($temp[43]+33);
  $Vals[32] = chr($temp[44]+33);
  $Vals[33] = chr($temp[45]+33);
  $Vals[34] = chr($temp[46]+33);
  $Vals[35] = ord($temp[47])*256 + ord($temp[48]);
  $Vals[36] = ord($temp[49])*256 + ord($temp[50]);
  $Vals[37] = ord($temp[51])*256 + ord($temp[52]);
  $Vals[38] = ord($temp[53])*256 + ord($temp[54]);
  $Vals[39] = ord($temp[55])*256 + ord($temp[56]);
  $Vals[40] = ord($temp[57])*256 + ord($temp[58]);
  $Vals[41] = ord($temp[59])*256 + ord($temp[60]);
  $Vals[42] = ord($temp[61])*256 + ord($temp[62]);
  $Vals[43] = ord($temp[63])*256 + ord($temp[64]);
  $Vals[44] = ord($temp[65])*65536 + ord($temp[66])*256 + ord($temp[67]);
  $Vals[45] = ord($temp[72]);
  $Vals[46] = ord($temp[73]);
  $Vals[47] = ord($temp[74]);
  $Vals[48] = ord($temp[77]);
  $Vals[49] = ord($temp[78]);
  $Vals[50] = ord($temp[79])*256 + ord($temp[80]);
  $Vals[51] = ord($temp[81])*256 + ord($temp[82]);
  $Vals[52] = ord($temp[83]);
  $Vals[53] = ord($temp[84]);
  $Vals[54] = ord($temp[85]);
  $Vals[55] = ord($temp[86]);
  $Vals[56] = ord($temp[87]);
  $Vals[57] = ord($temp[88]);
  $Vals[58] = ord($temp[89]);
  $Vals[59] = ord($temp[90]);
  $Vals[60] = ord($temp[91])*256 + ord($temp[92]);
  $Vals[61] = ord($temp[93])*256 + ord($temp[94]);
  $Vals[62] = ord($temp[95])*256 + ord($temp[96]);
  $Vals[63] = ord($temp[97])*256 + ord($temp[98]);
  $Vals[64] = ord($temp[99])*256 + ord($temp[100]);
  $Vals[65] = ord($temp[101])*256 + ord($temp[102]);
  $Vals[66] = ord($temp[103])*256 + ord($temp[104]);
  $Vals[67] = ord($temp[105])*256 + ord($temp[106]);
  $Vals[68] = ord($temp[107])*65536 + ord($temp[108])*256 + ord($temp[109]);
  $Vals[69] = ord($temp[110]);
  $Vals[70] = ord($temp[111]);
  $Vals[71] = ord($temp[112]);
  $Vals[72] = ord($temp[113]);
  $Vals[73] = ord($temp[114]);
  $Vals[74] = ord($temp[115]);
  $Vals[75] = ord($temp[116]);
  $Vals[76] = ord($temp[117]);
  $Vals[77] = ord($temp[118]);
  $Vals[78] = ord($temp[119]);

  my @cols = split /,/, $col_list_28;
  #print STDERR "CPD \n";
  foreach my $i (0..$#cols)
  {
    print STDERR "CPD - $cols[$i] = $Vals[$i]\n";
  }
  $OracleDB->insert(
                   table => "laundry2",
                   insert_columns => "machine_id, $col_list_28",
                   insert_values => [$mach_num, @Vals]
                   );
  $OracleDB->close();
}

########################################
#
# rix_Laundry_Status
#
########################################
# Description: Used to deal with P3 and P4 commands
#
# Uses Globals:
# Passed Vars : OnOff - wather you are turning the machine on
#                       or off.
#
# Written By: Chris Donohue
# Created: 7Dec2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub Laundry_Status()
{
  my($OnOff, $mach_num, $fob) = @_;
  my ($user_id) = &rix_Get_Mach_User($fob);
  my $OracleDB = Evend::Database::Database->new( print_query=>0 , primary => 'taz3', backup => 'taz3');
       
  my $top  = &top_decide($mach_num, $OnOff);
  #print STDERR "CPD - top is $top\n";
  if ($OnOff == 1)  #This may get changed we'll see
  {
    $OracleDB->update ( table => 'Laundry_Events',
                        update_columns => 'End_Time',
                        update_values => ['1/1/2000'],  #this causes an oracle trigger to fire
                        where_columns => ['machine_id = ?', 'top = ?', 'End_Time is null'],
                        where_values => [$mach_num, $top]
                        );  #first make sure the cycle is done.  Then we'll insert a new start
    $OracleDB->insert ( table => 'Laundry_Events',
                        insert_columns => 'machine_id, customer_name, top',
                        insert_values => [$mach_num, $user_id, $top]
                        );
  }
  else {
    my $row = $OracleDB->select ( table => 'Laundry_Events',
                                  select_columns => 'customer_name',
                                  where_columns => ['End_Time is null', 'Machine_id = ?', 'top = ?'],
                                  where_values => [$mach_num, $top],
                                  order => 'Start_Time DESC'
                                  );
    $user_id = $$row[0][0];

    $OracleDB->update ( table => 'Laundry_Events',
                        update_columns => 'End_Time',
                        update_values => ['1/1/2000'],  #this causes an oracle trigger to fire
                        where_columns => ['machine_id = ?', 'top = ?', 'End_Time is null'],
                        where_values => [$mach_num, $top]
                        );  
    $row = $OracleDB->select ( table => 'Laundry_Events',
                               select_columns => 'count(*)',
                               where_columns => ['End_Time is null', 'customer_name = ?'],
                               where_values => [$user_id]
                             );

    my $count = $$row[0][0];
    
    $row = $OracleDB->select ( table => 'laundry',
                               select_columns => 'model_cd',
                               where_columns => ['Machine_id = ?'],
                               where_values => [$mach_num],
                             );
    my $machine_type = $$row[0][0];

    &rix_Page_User($user_id, $count, $machine_type);
  }

  $OracleDB->close();
}

########################################
#
# rix_Get_Mach_User
#
########################################
# Description: Used to get the machine and userid
#
# Uses Globals:  DATA_LIST?  MACHINE_ID?
#
# Written By: Chris Donohue
# Created: 7Dec2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Get_Mach_User()
{
  my ($fob_num) = @_;
  #somehow we gotta get the fob out then we
  #can fetch the user_id.

  my $OracleDB = Evend::Database::Database->new( print_query=>0, primary => 'taz3', backup => 'taz3' );
  my $row = $OracleDB->select ( table => 'promo_esuds1',
                                select_columns => 'Customer_name',
                                where_columns => ['card_number = ?'],
                                where_values => [$fob_num]
                              );

  return ($$row[0][0]);
}

########################################
#
# rix_PageUser
#
########################################
# Description: Pages a washing machine user depending on their preference
#
# Uses Globals: 
# Passed Vars : User_id - the user's name
#               count - this is a count of records in laundry
#                       events with the current user_id
#
# Written By: Chris Donohue
# Created: 7Dec2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Page_User()
{
  my ($user_id, $count, $machine_type) = @_;
  my $OracleDB = Evend::Database::Database->new( print_query=>0, primary => 'taz3', backup => 'taz3' );

  my $row = $OracleDB->select ( table => 'PROMO_ESUDS1',
                                 select_columns => 'CARD_NUMBER, CONTACT_EMAIL, CONTACT_METHOD',
                                 where_columns => ['customer_name = ?'],
                                 where_values => [$user_id]
                                 );
  my $email = $$row[0][1];
  #my $pager = $$row[0][2];
  my $cont_meth = $$row[0][2]; # || 2;
  my $cont_when =  2; #$$row[0][3];

  if (($cont_meth != 0) && ($cont_when != 0))
  {
    if ((($cont_when == 1) && ($count == 0)) || ($cont_when == 2))
    {
      if (($cont_meth == '3') || ($cont_meth == '2'))
      {
        #email
        #system ("/bin/mailx -s \"Your laundry is done.\" $email \< \/home\/donohue\/blank");
        my $email_sender = 'esuds@e-vend.net'; 
        my $email_subject;
        if ($machine_type eq '6')
        { 
          $email_subject = 'Your dryer cycle is complete.';
        }
        else {
          $email_subject = 'Your washer cycle is complete.';
        }
        my $email_server = 'mail.usatech.com';
        my $email_messg = '';

        Evend::Mail::Mail::send(
                                from => $email_sender,
                                to => $email,
                                subject => $email_subject,
                                server => $email_server,
                                message => $email_messg
                               );
        warn "EMAIL to $email  subject $email_subject\n";

      }
      if (($cont_meth == '3') || ($cont_meth == '1'))
      {
        #page
        $ENV{'LD_LIBRARY_PATH'} = '/usr/local/lib';
        #$system "/usr/local/bin/sendpage -h perceptor -p $pager \"Laundry is done.\"";
      }
    }
  }
    
  $OracleDB->close();
}

################
#CPD
################
sub is_a_washer()
{
  my ($mach_id) = @_;
  my $OracleDB = Evend::Database::Database->new( print_query=>0, primary => 'taz3', backup => 'taz3');
  my $row = $OracleDB->select ( table => 'machine',
                                select_columns => 'machine_type_number',
                                where_columns => ['machine_id = ?'],
                                where_values => [$mach_id]
                              );
  if (($$row[0][0] eq '1023') || ($$row[0][0] eq '1024'))
  {
    $OracleDB->close();
    return 1;
  }
  else {
    $OracleDB->close();
    return 0;
  }
}

########################################
#
# top_decide
#
########################################
# Description: decides weather this should refer to the top
#              or bottom machine
#
# Uses Globals:
# Passed Vars : mach_num - the machine id number
#               onoff - weather we are turning the machine on or off
#
# Written By: Chris Donohue
# Created: 7Dec2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub top_decide()
{
  my ($mach_num, $onoff) = @_;
  my $top = 0;
  my $OracleDB = Evend::Database::Database->new( print_query=>0 , primary => 'taz3', backup => 'taz3');

  my $row = $OracleDB->select ( table => 'laundry',
                                 select_columns => 'model_cd',
                                 where_columns => ['machine_id = ?'],
                                 where_values => [$mach_num]
                              );
  if ($$row[0][0] == '6') {
    #deal with a stacked washer dryer
    my $ref = $OracleDB->select ( table => 'laundry_events',
                               select_columns => 'to_char(start_time, \'J-SSSSS\'), in_use, top',
                               where_columns => ['machine_id = ?'],
                               where_values => [$mach_num]
                             );
    my $count = 0;
    my ($b_start_day, $b_start_sec, $b_use, $t_start_day, $t_start_sec, $t_use) = (0,0,0,0,0,0);
    foreach my $row (@$ref) {
      $count++;
      if ($$row[2] == '0'){
        ($b_start_day, $b_start_sec) = split /-/, $$row[0];
        $b_use = $$row[1];
      }
      else {
        ($t_start_day, $t_start_sec) = split /-/, $$row[0];
        $t_use = $$row[1];
      }
    } #assigned temporary vars for everything

    if ($count < 2)
    {
      if (!($t_use)) { $t_use = 'N'; }
      if (!($b_use)) { $b_use = 'N'; }
    }
    
    if ($onoff == 1) {
      #use whichever dryer is available
      if (($b_use eq 'Y') && ($t_use eq 'N'))
      {
        $top = 1;
      }
      if ($b_use eq 'N')
      {
        $top = 0;
      }
      #if both are in use then decide which should be shut off first
      if (($b_use eq 'Y') && ($t_use eq 'Y'))
      {
        $top = &top_decide($mach_num, 0);
      }
    } 
    else {
      #deal with shutting one off
      if (($b_use eq 'Y') && ($t_use eq 'N'))
      {
        $top = 0;
      }
      if ($b_use eq 'N')
      {
        $top = 1;
      }
      #if both are in use then decide which should be shut off first
      if (($b_use eq 'Y') && ($t_use eq 'Y'))
      {
         if (((($t_start_day - $b_start_day) * 24*60*60) + ($t_start_sec - $b_start_sec)) > 0)
         {
            $top = 0;
         }
         else {
            $top = 1;
         }
      }

    }
  } #else top is always 0

  $OracleDB->close();
  return $top;
}

1;
