#!/opt/bin/perl

use lib '/opt/rixd';
use rix;
use strict;
use Evend::Database::Database;
use DBI;
use DBD::Oracle;
use Evend::Transaction::AuthClient;

my $server = rix->new({ localport => 10105, logfile => '/opt/rixd/rixd.log', pidfile => '/opt/rixd/rixd.pid'});
$server->Bind();


