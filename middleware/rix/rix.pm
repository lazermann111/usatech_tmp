package rix;
use Net::Daemon;
use Evend::Transaction::AuthClient;
use Evend::Database::Database;

@ISA = qw(Net::Daemon);
my ($RECORD_28, $RECORD_28_ENC); 

sub Run
{
	($notself) = @_;
	$sock = $notself->{socket};
	autoflush $sock 1;
	$localtime = localtime();

	warn("connect at $localtime\n");

push(@INC,"/opt/rixd/socketlib");
use lib '/websites/e-vend/rixd/';
use washer_dryer;

#################################################################
#
# rix6.cgi
# 
# Version 6.0
#
#################################################################
#
# Written By: Scott Wasserman
# Created:  8/30/1999
# 
# (c)1999 e-Vend.net Corporation
#
#
#################################################################
# Revision History
#################################################################
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
#
#
#
#
#
#################################################################
# Setup
#################################################################

#################################################################
# Required Libraries
#################################################################
require("rix_os-lib.pl");
# require("rix_transaction-lib.pl");
require("rix_checksum-lib.pl");
require("rix_response-lib.pl");
# require("rix_inventory_card-lib.pl");
require("rix_inventory.pl");
require("rix_tracer-lib.pl");
require("rix_debug-lib.pl");
require("rix_dex-lib.pl");
require("washer_dryer.pm");

#################################################################
# Globals
#################################################################
# Should we override default SEPARATOR character?
#CHANGED FROM NE TO EQ
if ($ARGV[0] eq "")
{
   #$separator_ascii_num = $ARGV[0];
	$separator_ascii_num = 224;
   $rix_os_ENV{"SEPARATOR"} = sprintf("%c",$separator_ascii_num);

   if ($ARGV[1] ne "")
   {
      # The 2nd command line argument is a 2 bit value.
      # The lower bit represents the state of the DEBUG_MODE.
      # The higher bit represents the state of the FAUX_MODE.
      # i.e. (0|1)(0|1) or (FAUX_MODE)(DEBUG_MODE)

      # 1 - debug only
      # 2 - faux mode only
      # 3 - faux and debug

      $ARGV[1] = &rix_checksum_Dec_To_Bin($ARGV[1]);

      if ($ARGV[1] & 0x1)
      {
         $rix_os_ENV{"DEBUG_MODE"} = 1;
      }

      if (($ARGV[1]>>1) & 0x1)
      {
         $rix_os_ENV{"FAUX_MODE"} = 1;
      }

      &rix_debug("FAUX_MODE_ON");

      &rix_debug("DEBUG_ON");

      &rix_debug("CURRENT_SEPARATOR");

      if ($rix_os_ENV{"FAUX_MODE"})
      {
         # If we are in faux mode, then we will require a file containing the RIX commands to simulate.
         # These are stored in a file,  which we copy into a list. Doing so will limit
         # file access to a single read.
         # the input filename is the 3rd command line argument.
         $rix_debug_GLOBAL{"FAUX_INPUT_FILE"} = $ARGV[2];

         $rix_debug_GLOBAL{"FAUX_CMDS"} = [ &rix_debug_Get_Faux_Commands() ];

         #&dump_List(@{$rix_debug_GLOBAL{"FAUX_CMDS"}});
         
         # We might also want to specify an output file to record the faux session, in addition to sending it to
         # standard output.
         # if specified, the output filename is the 4th command line argument.
         if ($ARGV[3] ne "")
         {
            $rix_debug_GLOBAL{"FAUX_OUTPUT_FILE"} = $ARGV[3];
         }

      }
   }
}

####################################
# NOTE TO SELF
####################################
#
# Add timeout logic to code!
#
####################################

# Init Global Variables
&rix_Init_Globals();

if ($rix_os_ENV{"FAUX_MODE"})
{
   # Start Faux Command Shell
   &rix_Faux_Command_Shell();
}
else
{
   # Start Command Shell
   &rix_Command_Shell();
}


########################################
#
# rix_Init_Globals
#
########################################
# Description: Init global variables needed in RIX
# 
# Uses Globals: 
#
# Written By: Scott Wasserman
# Created: 10/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Init_Globals
{	
	
	$CONTINUE_RIX_SESSION = 1;
	
	
	# RIX Ack/Nak Responses
	$ACK = 0;
	$NAK = 1; # Retransmit Data
	$FATAL_ERROR = 2;
	
	# Default Message Options
	$DEFAULT_MESSAGE_DELAY = 5;
	$DEFAULT_MESSAGE_OPTION_BEEP = 0;
	$DEFAULT_MESSAGE_OPTION_CENTERED = 1;
	$DEFAULT_MESSAGE_OPTION_IMMEDIATE = 0;
	
	# Message option values
	$MESSAGE_OPTION_BEEP_VALUE = 4;
	$MESSAGE_OPTION_CENTERED_VALUE = 2;
	$MESSAGE_OPTION_IMMEDIATE_VALUE = 1;
	
	# RIX Session Timeout in seconds
	$RIX_SESSION_TIMEOUT = 1800;
	
	# RIX Session Checksum Count
	$CHECKSUM_ERROR_COUNTER = 0;
	
	# The number of checksums RIX will allow before quitting
	$CHECKSUM_ERROR_THRESHOLD = 10;
	
	# Line breach character
	$LINE_BREAK = "\n";

        # Constants associated with inter-process communication (shared memory, semaphore locks, etc.)
        $IPC_PRIVATE = 0;
        $IPC_RMID = 0;
        $IPC_PID = -1;

        # Maximum size of the shared memory location
        $SHM_SIZE = 100;
        $SHM_KEY = 0;

        # used in transaction-lib, to signal auth approval/denial
        $APPROVED = 1;
        $DENIED   = 0;

        # Identifier used to mark "empty"ness of the command line buffer
        $CMD_LN_BUFF_EMPTY = 0;

        # Flag to indicate that first cmd has been executed - needed to differentiate PRI transaction auth
        # from standard auth
        $PRI_LOGIN = 1;
}

########################################
#
# rix_Command_Shell
#
########################################
# Description: The rix command shell loop
# 
# Uses Globals: 
#
# Written By: Scott Wasserman
# Created: 10/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Command_Shell
{

	# Init Watchdog
	&rix_Watchdog_Reset();

        $| = 1;
	#&rix_response_Raw(">\n");

	while ($CONTINUE_RIX_SESSION && $sock->connected)
	{
		# Command prompt
		&rix_response_Raw(">");
		
		# Get command line buffer
		$command_line_buffer = &rix_Get_Command_Line();
		warn("$command_line_buffer") if $command_line_buffer ne "";

		my $hidden_command_line_buffer = $command_line_buffer;

                #JHR 6.20.2001 Added t5
		if ($hidden_command_line_buffer =~ /t1|t4|t5/)
		{
			$hidden_command_line_buffer = replace_magstrip($command_line_buffer);	
		}
		&rix_tracer_Save_Line(">".$hidden_command_line_buffer."\n");

		# Get command line data
		(	$DEVICE_ID,
                        $COMMAND_GROUP,
			$COMMAND_NUMBER,
			$STRING_TO_CHECKSUM,
			$CHECKSUM,
			@DATA_LIST) = &rix_Parse_Command_Line($command_line_buffer);
		&rix_debug("COMMAND_DATA");
		
		if ($COMMAND_GROUP ne "")
		{
			# Execute command if possible
			&rix_Execute_Command();
			
			&rix_Watchdog_Reset();
		}
	}

        #sleep 1;
}

########################################
#
# rix_Faux_Command_Shell
#
########################################
# Description: The rix command shell loop
# 
# Uses Globals: 
#
# Written By: Salim Khan
# Created: 11/30/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Faux_Command_Shell
{
   if (defined($rix_debug_GLOBAL{"FAUX_OUTPUT_FILE"}))
   {
      open(FAUX_OUT,"+<".$rix_debug_GLOBAL{"FAUX_OUTPUT_FILE"});
   }

   while (scalar(@{$rix_debug_GLOBAL{"FAUX_CMDS"}}) > 0)
   {
      $command_line_buffer = shift(@{$rix_debug_GLOBAL{"FAUX_CMDS"}}); 

      $| = 1; # make STDOUT "hot", i.e. auto-flush STDOUT

      # Command prompt
      &rix_response_Raw(">".$command_line_buffer);

      &rix_tracer_Save_Line(">".$command_line_buffer);

      # Get command line data
      (     $DEVICE_ID,
            $COMMAND_GROUP,
            $COMMAND_NUMBER,
            $STRING_TO_CHECKSUM,
            $CHECKSUM,
            @DATA_LIST) = &rix_Parse_Command_Line($command_line_buffer);

      &rix_debug("COMMAND_DATA");

      if ($COMMAND_GROUP ne "")
      {
            # Execute command if possible
            &rix_Execute_Command();
      }
   }

   if (defined($rix_debug_GLOBAL{"FAUX_OUTPUT_FILE"}))
   {
      close(FAUX_OUT);
   }

}

              
              
              
 

########################################
#
# rix_Get_Command_Line
#
########################################
# Description: Get RIX Command Line
# 
# Uses Globals: 
#
# Written By: Scott Wasserman
# Created: 9/20/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Get_Command_Line
{
	local(	$command_line_buffer,
			$curr_key);
			
# 	#(&rix_Watchdog_Ok())
# 	$command_line_buffer = "";
# 
#    print "> ";
# 
#    $curr_key = "";
#     
#    while ($curr_key ne "\n")
#    {
#       $command_line_buffer .= $curr_key;
#       $curr_key = getc;
#    }
	
	#$command_line_buffer = <STDIN>;
	$command_line_buffer = <$sock>;
        
	return $command_line_buffer;
}

########################################
#
# rix_Execute_Command
#
########################################
# Description: Execute Current RIX Command
# 
# Uses Globals: $COMMAND_GROUP, $COMMAND_NUMBER, $STRING_TO_CHECKSUM, $CHECKSUM
#
# Written By: Scott Wasserman
# Created: 8/31/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Execute_Command
{

	&rix_debug_Execute_Commands();

	# Check checksum
        #if (($rix_os_ENV{"MACHINE_ID"} eq "0039") || # tmp - want 0039 to dump its DEX output, even though checksums are going to be wrong

#Change for checksum errors
	if (true) 

  # if (&rix_checksum_Valid($STRING_TO_CHECKSUM,$CHECKSUM))
	{
		if ($COMMAND_GROUP eq "x")
		{
			###################################
			#>
			#> Exit command group
			#>
			###################################
			&rix_Exit();
		}
		else
		{
			###################################
			# The l and o command groups can be 
			# used without having a Device ID set.
			###################################
			if ($COMMAND_GROUP eq "l")
			{
				###################################
				#>
				#> Login command group
				#>
				###################################
		
				&rix_Login();

                                $PRI_LOGIN = 0;
			}
                       #Modified JHR 6.21.2001
                       # elsif ( ($COMMAND_GROUP eq "t") && ($COMMAND_NUMBER eq "1") && ($PRI_LOGIN == 1) )
                        elsif ( ($COMMAND_GROUP eq "t") && ($COMMAND_NUMBER =~ /1|5/) && ($PRI_LOGIN == 1) )
                        {
                                ###################################
                                #>
                                #> Transaction command group
                                #>
                                ###################################
                                &rix_Transaction_PRI_Authorize();
                        }
			elsif ($COMMAND_GROUP eq "o")
			{
				###################################
				#>
				#> Override Separator command group
				#>
				###################################
				&rix_Override_Separator();
			}
			elsif ($COMMAND_GROUP eq "h")
			{
				
			}
			else
			{
				# Is there a DEVICE_ID entered
				if ($rix_os_ENV{"DEVICE_ID"} ne "")
				{
					if ($COMMAND_GROUP eq "s")
					{
						###################################
						#>
						#> Software command group
						#>
						###################################
						&rix_Software();
					}
					elsif ($COMMAND_GROUP eq "v")
					{
						###################################
						#>
						#>  VMC Settings command group
						#>
						###################################
						&rix_Vmc_Settings();
					}
					elsif ($COMMAND_GROUP eq "c")
					{
						###################################
						#>
						#> CCU Settings command group
						#>
						###################################
						&rix_Ccu_Settings();
					}
					elsif ($COMMAND_GROUP eq "w")
					{
						###################################
						#>
						#> Swipe Settings command group
						#>
						###################################
						&rix_Swipe_Settings();
					}
					elsif ($COMMAND_GROUP eq "t")
					{
						###################################
						#>
						#> Transaction command group
						#>
						###################################
						&rix_Transaction();
					}
					elsif ($COMMAND_GROUP eq "r")
					{
						###################################
						#>
						#> Receipt command group
						#>
						###################################
						&rix_Receipt_Settings();
					}
					elsif ($COMMAND_GROUP eq "u")
					{
						###################################
						#>
						#> CCU Status command group
						#>
						###################################
						&rix_Ccu_Status();
					}
					elsif ($COMMAND_GROUP eq "i")
					{
						###################################
						#>
						#> Inventory Card command group
						#>
						###################################
						&rix_Inventory_Card();
					}
                                        elsif ($COMMAND_GROUP eq "d")
					{
						###################################
						#>
						#> DEX processing command group
						#>
						###################################
						&rix_Dex_Command();
					}
                                        elsif ($COMMAND_GROUP eq "p")
					{
						###################################
						#>
						#> Washer and Dryer command group
						#>
						###################################
						&rix_Washer_Dryer_Settings();
					}
                                        elsif ($COMMAND_GROUP eq "f")
					{
						###################################
						#>
						#> Sales & Usage Tax Processing
						#>
						###################################
						&rix_Tax_Settings();
					}
					else
					{
						###################################
						#>
						#> No valid command group found
						#>
						###################################
						&rix_Invalid_Command();
					}
				}
				else
				{
					&rix_Misc_Error("No Valid","Device ID Entered");
				}
			}
			
		}
	}
	else
	{
#		&rix_Ack(); #CRC
		&rix_Checksum_Error();
	}
}


########################################
#
# rix_Watchdog_Reset
#
########################################
# Description: Reset Rix watchdog
# 
# Uses Globals: $WATCHDOG_TIMER
#
# Written By: Scott Wasserman
# Created: 9/20/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Watchdog_Reset
{
	local(	$hold_watchdog_timer);
	
# 	if ($DEBUG_MODE)
# 	{
# 		print ">>WATCHDOG RESET<<\n";
# 		$hold_watchdog_timer = $WATCHDOG_TIMER;
# 		print ">>WATCHDOG (IN): ".$WATCHDOG_TIMER."\n";
# 	}
	
	$WATCHDOG_TIMER = (time)+$RIX_SESSION_TIMEOUT;
	
# 	if ($DEBUG_MODE)
# 	{
# 		print ">>WATCHDOG (OUT): ".$WATCHDOG_TIMER."\n";
# 		print ">>WATCHDOG (ELAPSED): ".($WATCHDOG_TIMER-$hold_watchdog_timer)."\n";
# 	}
}

########################################
#
# rix_Watchdog_Ok
#
########################################
# Description: Check if $WATCHDOG_TIMER has expired
# 
# Uses Globals: $WATCHDOG_TIMER, $CONTINUE_RIX_SESSION
#
# Written By: Scott Wasserman
# Created: 9/20/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Watchdog_Ok
{
	local(	$curr_timer);
	
	if ($DEBUG_MODE)
	{
		return 1;
	}
	else
	{
		$curr_timer = time;
		if ($WATCHDOG_TIMER > $curr_timer)
	 	{
	 		return 1;
	 	}
	 	else
	 	{
	 		# Kill rix session
	 		$CONTINUE_RIX_SESSION = 0;
	 		return 0;
	 	}
		
	}
	
}
########################################
#
# rix_Vmc_Settings
#
########################################
# Description: Rix VMC settings procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER, %rix_os_ENV
#
# Written By: Scott Wasserman
# Created: 9/?/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Vmc_Settings
{
	local(	@single_data_command_number_list,
			$vmc_data_item_number,
			$vmc_data,
			$vmc_data_packed,
			@vmc_data_list);
	
	@single_data_command_number_list = (1,2);

	if (grep(/$COMMAND_NUMBER/, @single_data_command_number_list))	
	#if (&on_List($COMMAND_NUMBER,@single_data_command_number_list))
	{
		######################################
		#> Action: 	1) numOfColumns
		#>		2) columnIndexOrigin
		#>
		#> Note: Single data item per command number
		######################################
		
		$vmc_data_item_number = $COMMAND_NUMBER;
		$vmc_data = &rix_os_Get_Vmc_Setting_Data($vmc_data_item_number).$rix_os_ENV{"SEPARATOR"};
		
		&rix_response($ACK,$vmc_data);
	}
	else 
	{
		######################################
		#> Action: 	4) ColumnName
		#>			5) ColumnPrice
		#>			6) ColumnInventoryId
		#>
		#> Note: Multiple data items per command number
		######################################
		$column_number = $DATA_LIST[0];
		
		if ( ($column_number ne "") && ($column_number > 0) )
		{
                      $vmc_data_item_number = $COMMAND_NUMBER;
		      $vmc_data_packed = &rix_os_Get_Vmc_Setting_Data($vmc_data_item_number);
		      @vmc_data_list = split(/$rix_os_ENV{"SEPARATOR"}/,$vmc_data_packed);
		      $column_number = $DATA_LIST[0];
		      $vmc_data = $vmc_data_list[($column_number-1)].$rix_os_ENV{"SEPARATOR"};
		        
		      &rix_response($ACK,$vmc_data);
		}
		else
		{
			&rix_Invalid_Data();
		}
		
	}
	
}

########################################
#
# rix_Ccu_Settings
#
########################################
# Description: Rix CCU settings procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER, %rix_os_ENV
#
# Written By: Scott Wasserman
# Created: 9/?/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Ccu_Settings
{
	local(	$local_date,
			$local_time,
			$ccu_data_item_number,
			$ccu_data,
			$prefix_number,
			@prefix_list);
			
	if ($COMMAND_NUMBER eq "100")
	{		
		######################################
		#> Action: Get Valid prefixes
		######################################
		if ($DATA_LIST[0] ne "")
		{	
			$ccu_data_item_number = $COMMAND_NUMBER;
			$prefix_number = ($DATA_LIST[0])-1;
			@prefix_list = split(/$rix_os_ENV{"SEPARATOR"}/,&rix_os_Get_Ccu_Setting_Data($ccu_data_item_number));
			$curr_prefix_data = $prefix_list[$prefix_number].$rix_os_ENV{"SEPARATOR"};
			
			&rix_response($ACK,$curr_prefix_data);
			
		}
		else
		{
			&rix_Invalid_Data();
		}		
	}
	elsif ($COMMAND_NUMBER eq "70")
	{
		######################################
		#> Action: Get Current Date & Time (Timezone Adjusted)
		######################################
		&rix_os_Update_Ccu_Setting_Date_And_Time();
		
		$ccu_data_item_number = $COMMAND_NUMBER;
		$ccu_data = &rix_os_Get_Ccu_Setting_Data($ccu_data_item_number).$rix_os_ENV{"SEPARATOR"};
		
		
		warn("ccu data: $ccu_data\n");

		&rix_response($ACK, $ccu_data);
	}
	elsif ($COMMAND_NUMBER eq '146')
	{
		my $ORACLE = Evend::Database::Database->new(print_query => 1);
		my $ref = $ORACLE->select( table => 'rix_Machine',
			select_columns => 'DISABLE_PASSWORD',
			where_columns => ['machine_id = ?'],
			where_values => [$rix_os_ENV{"MACHINE_ID"}],
		);

		&rix_response($ACK,"$ref->[0][0]�");
	}
	else
	{
		######################################
		#> Action: Get CCU settings data
		######################################
		$ccu_data_item_number = $COMMAND_NUMBER;
		$ccu_data = &rix_os_Get_Ccu_Setting_Data($ccu_data_item_number).$rix_os_ENV{"SEPARATOR"};
		
		&rix_response($ACK,$ccu_data);
	}
}

########################################
#
# rix_Swipe_Settings
#
########################################
# Description: Rix Swipe Settings procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER, %rix_os_ENV
#
# Written By: Scott Wasserman
# Created: 9/?/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Swipe_Settings
{
	######################################
	#> Action: Get Swipe settings data
	######################################
	$swipe_data_item_number = $COMMAND_NUMBER;
	$swipe_data = &rix_os_Get_Swipe_Setting_Data($swipe_data_item_number).$rix_os_ENV{"SEPARATOR"};
	
	&rix_response($ACK,$swipe_data);	
}


########################################
#
# rix_Transaction
#
########################################
# Description: Rix Transation procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER 
#
# Written By: Scott Wasserman
# Created: 9/27/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Transaction
{
   local (  $number_of_data_items_needed,
            $auth_return_data,
            $amt_left,
            $auth_return_msg1,
            $auth_return_msg2,
            @authorization_failure_code,
            $authorization_failure_code );

	my $ORACLE_AUTH = Evend::Database::Database->new(print_query => 1);

	if ($COMMAND_NUMBER eq "1")
	{ 
                ######################################
		#> Action: RIX authorization
		######################################  
                $number_of_data_items_needed = 6; # changed from 4

		

		# t1


                if (scalar(@DATA_LIST) == $number_of_data_items_needed)
                {
			my $trans = Evend::Transaction::AuthClient->connect();
			my $payload = join ',', @DATA_LIST;
			$trans->send("t1,$rix_os_ENV{MACHINE_ID},$payload");

			my $auth_code = $trans->get_code();
			my $txn_num = $trans->authorization_number();
			$auth_return_msg1 = $trans->account_balance();
			warn("AUTH RETURN MESSAGE1 = $auth_return_msg1\n");
			$trans->disconnect();


            if (($auth_code eq 'A') || ($auth_code eq 'T')) {
               $txn_num = "$txn_num $txn_num".$rix_os_ENV{SEPARATOR};
            }
            elsif ($auth_code eq 'E') {
               $txn_num = '0'.$rix_os_ENV{SEPARATOR};
            }

		&rix_response($ACK,$txn_num,$auth_return_msg1,$auth_return_msg2);

                }

                else
                {
                    &rix_Invalid_Data();
                }

        }


        elsif ($COMMAND_NUMBER eq "5")
        {
                ######################################
                #> Action: RIX authorization with Sequence Number
                ######################################
                $number_of_data_items_needed = 7; # changed from 4

                # t5

                if (scalar(@DATA_LIST) == $number_of_data_items_needed)
                {

                        my $trans = Evend::Transaction::AuthClient->connect();
                        my $payload = join ',', @DATA_LIST;
                        $trans->send("t1,$rix_os_ENV{MACHINE_ID},$payload");

                        my $auth_code = $trans->get_code();
                        my $txn_num = $trans->authorization_number();
                        $auth_return_msg1 = $trans->account_balance();
                        warn("AUTH RETURN MESSAGE1 = $auth_return_msg1\n");
                        $trans->disconnect();

                        #send the sequence number back to the client
                        if (($auth_code eq 'A') || ($auth_code eq 'T')) {
                           $txn_num = "$txn_num $txn_num".$rix_os_ENV{SEPARATOR}.$DATA_LIST[6].$rix_os_ENV{SEPARATOR};
                        }
                        elsif ($auth_code eq 'E') {
                           $txn_num = '0'.$rix_os_ENV{SEPARATOR}.$DATA_LIST[6].$rix_os_ENV{SEPARATOR};
                        }

                        &rix_response($ACK,$txn_num,$auth_return_msg1,$auth_return_msg2);

                }

                else
                {
                    &rix_Invalid_Data();
                }

        }

	elsif ($COMMAND_NUMBER eq "2")
	{
		######################################
		#> Action: RIX batch
		######################################

		if ($DATA_LIST[0] eq 'MASTER')
		{
			my @time = localtime();
			my $time = "$time[2]:$time[1]:$time[0]";
			$time[4]++;
			$time[5] += 1900;

			my $date = "$time[4]/$time[3]/$time[5]";

			warn("master code card detected\n");
			my $trans = Evend::Transaction::AuthClient->connect();

			$trans->send("t4,$rix_os_ENV{MACHINE_ID},$time,$date,SKDKDK1MASTER^CHAMPION CARD^^,1,LOCAL,$DATA_LIST[1],$DATA_LIST[2],$DATA_LIST[3],$DATA_LIST[4],,,$DATA_LIST[7]");


			$trans->disconnect();
			&rix_Ack();
			return;
		}

		my $size_of_data_list = scalar(@DATA_LIST);

		warn("size of list: $size_of_data_list\n");

		$number_of_data_items_needed = 9; # changed from 8

		my $ptref = $ORACLE_AUTH->query(query => "select
								trans_no
							from
								transaction_record_hist
							where
								trans_no = ?
								and machine_id = ?",
			values => [$DATA_LIST[0], $rix_os_ENV{MACHINE_ID}],
		);

		my $pt2ref = $ORACLE_AUTH->query(query => "select
								force_action_cd
							from
								transaction_record
							where
								trans_no = ?
								and machine_id = ?",
			values => [$DATA_LIST[0], $rix_os_ENV{MACHINE_ID}],
		);

		warn("ptref is $ptref->[0][0], $pt2ref->[0][0]\n");

		#don't continue if the transaction is in transaction_record_hist
		#or if it's an approved transaction
		if( ($ptref->[0][0]) || ($pt2ref->[0][0] eq 'A') )
		{
			&rix_Ack();
			warn("$ptref->[0][0] transaction exists, aborting\n");
			return;	
		}

		my $machine_type = Get_Machine_Type($ORACLE_AUTH,$rix_os_ENV{MACHINE_ID});

		if (scalar(@DATA_LIST) == $number_of_data_items_needed)
		{
			if (length($DATA_LIST[$#DATA_LIST]) > 2)
			{
				$DATA_LIST[$#DATA_LIST] = substr($DATA_LIST[$#DATA_LIST], 0, 2);
			}
			$payload = join ',', @DATA_LIST;

			if ($machine_type eq 'APC')
			{
				$payload = Recreate_APC_Button_Info('t2', $ORACLE_AUTH,$rix_os_ENV{MACHINE_ID} , $payload);
			}
			elsif ($machine_type eq 'CRANE')
			{
				$payload = Recreate_CRANE_Button_Info('t2', $ORACLE_AUTH, $rix_os_ENV{MACHINE_ID}, $payload);
			}

			my $trans = Evend::Transaction::AuthClient->connect();

			if ( !( ($DATA_LIST[0] =~ /^CANCEL/) ||
					($DATA_LIST[0] =~ /^VENDTOOLONG/) ||
					($DATA_LIST[0] =~ /DENIED/) ||
					($DATA_LIST[3] == 0) ) )
			{
				warn("selecting card type for t2 transaction\n");
				my $cardtyperef = $ORACLE_AUTH->select(
									table => 'transaction_record',
									select_columns => 'card_type,trans_no',
									where_columns => ['trans_no = ?'],
									where_values => [$DATA_LIST[0]],
								);
			
				my $cardtypetrans;
				$cardtypetrans = '1' if $cardtyperef->[0][0] eq 'SP';

				if ( $cardtyperef->[0][1]
					and !(	($rix_os_ENV{MACHINE_ID} eq 'EV11082') or
							($rix_os_ENV{MACHINE_ID} eq 'EV11080') or
							($rix_os_ENV{MACHINE_ID} eq 'EV10956') or
							($rix_os_ENV{MACHINE_ID} eq 'EV10955') or
							($rix_os_ENV{MACHINE_ID} eq 'EV11083')
								) )
				{
					&Update_Inventory(	$rix_os_ENV{MACHINE_ID},
										$DATA_LIST[1],
										$cardtypetrans,
										$DATA_LIST[0])
				}
			}

			#to here CPD

			$trans->send("t2,$rix_os_ENV{MACHINE_ID},$payload");

			if(	$trans->get_inv() and (
				($rix_os_ENV{MACHINE_ID} eq 'EV11082') or
				($rix_os_ENV{MACHINE_ID} eq 'EV11080') or
				($rix_os_ENV{MACHINE_ID} eq 'EV10956') or
				($rix_os_ENV{MACHINE_ID} eq 'EV10955') or
				($rix_os_ENV{MACHINE_ID} eq 'EV11083') ) )
			{
					&Update_Inventory(	$rix_os_ENV{MACHINE_ID},
										$DATA_LIST[1],
										$cardtypetrans,
										$DATA_LIST[0])
			}

			$trans->disconnect();

			&rix_Ack();
		}
		else
		{
			&rix_Invalid_Data();
		}		
	}
	elsif ($COMMAND_NUMBER eq "3")
	{
		######################################
		#> Action: Save batch data (direct FD client auth & batch)
		#> NOTE: Will be obsolute when all system are transitioned to 
		#>		 fully operational RIX server with leased-line FD auth
		######################################
		$number_of_data_items_needed = 15;
		
		if (scalar(@DATA_LIST) == $number_of_data_items_needed)
		{
			&rix_transaction_Save_Data(@DATA_LIST);
			&rix_Ack();
		}
		else
		{
			&rix_Invalid_Data();
		}		
		
	}
        elsif ($COMMAND_NUMBER eq "4")
	{
		######################################
		#> Action: Save batch data for transactions that have been authorized locally, i.e. by the machine itself.
		#> 
		#> 
		######################################
		$number_of_data_items_needed = 12;

		my $machine_type = Get_Machine_Type($ORACLE_AUTH,$rix_os_ENV{MACHINE_ID});

		my $ptref = $ORACLE_AUTH->query(
							query => "select trans_no from transaction_record_hist where transaction_date = to_date(?,'MM/DD/YYYY HH24:MI:SS') and machine_id = ?",
			values => ["$DATA_LIST[1] $DATA_LIST[0]",$rix_os_ENV{MACHINE_ID}],
		) if length($DATA_LIST[0]) == 8;

		my $pt2ref = $ORACLE_AUTH->query(query => "select trans_no from transaction_record where transaction_date = to_date(?,'MM/DD/YYYY HH24:MI:SS') and machine_id = ?",
		values => ["$DATA_LIST[1] $DATA_LIST[0]",$rix_os_ENV{MACHINE_ID}],
		) if length($DATA_LIST[0]) == 8;


		warn("ptref is $ptref->[0][0], $pt2ref->[0][0]\n");

		if ( ($ptref->[0][0]) || ($pt2ref->[0][0]) )
		{	
			&rix_Ack();
			warn("$ptref->[0][0] transaction exists, aborting\n");
			return;	
		}
	
		if (1)	
		#if (scalar(@DATA_LIST) >= $number_of_data_items_needed)
		{
			my $trans = Evend::Transaction::AuthClient->connect();

			$payload = join ',', @DATA_LIST;

			if ($machine_type eq 'APC') { $payload = Recreate_APC_Button_Info('t4', $ORACLE_AUTH, $rix_os_ENV{MACHINE_ID}, $payload); }
			elsif ($machine_type eq 'CRANE') { $payload = Recreate_CRANE_Button_Info('t4', $ORACLE_AUTH, $rix_os_ENV{MACHINE_ID}, $payload); }

			$trans->send("t4,$rix_os_ENV{MACHINE_ID},$payload");

			my $txn_num = $trans->authorization_number();

			if(	$trans->get_inv() and (
				($rix_os_ENV{MACHINE_ID} eq 'EV11082') or
				($rix_os_ENV{MACHINE_ID} eq 'EV11080') or
				($rix_os_ENV{MACHINE_ID} eq 'EV10956') or
				($rix_os_ENV{MACHINE_ID} eq 'EV10955') or
				($rix_os_ENV{MACHINE_ID} eq 'EV11083') ) )
			{
					&Update_Inventory(	$rix_os_ENV{MACHINE_ID},
										$DATA_LIST[5],
										$DATA_LIST[3],
										$txn_num);
			}

			$trans->disconnect();


			#Change stuff here CPD
			if( (!(	($DATA_LIST[4] =~ /^CANCEL/) ||
					($DATA_LIST[4] =~ /^VENDTOOLONG/) ||
					($DATA_LIST[4] =~ /DENIED/) ||
					($DATA_LIST[7] == 0) )) and
				!(($rix_os_ENV{MACHINE_ID} eq 'EV11082') or
				($rix_os_ENV{MACHINE_ID} eq 'EV11080') or
				($rix_os_ENV{MACHINE_ID} eq 'EV10956') or
				($rix_os_ENV{MACHINE_ID} eq 'EV10955') or
				($rix_os_ENV{MACHINE_ID} eq 'EV11083') )
				)
			{
				&Update_Inventory($rix_os_ENV{MACHINE_ID}, $DATA_LIST[5], $DATA_LIST[3],
						$txn_num);
			}
			#to here CPD

			&rix_Ack();
		}
		else
		{
			&rix_Invalid_Data();
		}		
		
	}
	else
	{
		&rix_Invalid_Command();
	}
}



########################################
#
# rix_Transaction_PRI_Authorize
#
########################################
# Description: Rix Transation procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER 
#
# Written By: Salim Khan
# Created: 5/30/2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Transaction_PRI_Authorize
{
   local ( );
   if ($DEVICE_ID ne "")
   {
      # Check device id
      if (&rix_os_Init_Session($DEVICE_ID))
      {
         &rix_debug("MACHINE_ID");
         
         ##################################################################################################################
         # L2 command
         ##################################################################################################################
         ######################################
         #> Action: Login for Authorization
         ######################################
         
         # Init all global data needed for Authorization
      
         # Log Start of Rix Session
         &rix_os_Log_Session_Start();
         
         # Init all global data needed for Authorize
         # &rix_os_Init_Transaction_Authorize_Data_Lists();
         &rix_os_Init_TransAuth_Login_Data();
      
         # Open tracer if needed
         &rix_tracer_Open();
      
         # Save Session Login Information in Tracer
         &rix_tracer_Save_Line("#####################################\n");
         &rix_tracer_Save_Line("# PRI LOGIN: ".$rix_os_ENV{"DEVICE_ID"}." - ".$rix_os_ENV{"MACHINE_ID"}."\n");
         &rix_tracer_Save_Line("# ".$rix_os_ENV{"SESSION_START_DATE"}." - ".$rix_os_ENV{"SESSION_START_TIME"}."\n");
         &rix_tracer_Save_Line("#####################################\n");
         # $command_line_buffer is carried over from the shell

                my $hidden_command_line_buffer = $command_line_buffer;

                if ($hidden_command_line_buffer =~ /t1|t4|t5/)
                {
                        $hidden_command_line_buffer = replace_magstrip($command_line_buffer);
                }
                &rix_tracer_Save_Line(">".$hidden_command_line_buffer."\n");


if ($hidden_command_line_buffer =~ /t1/)
{
         ##################################################################################################################
         # T1 command
         ##################################################################################################################
         ######################################
         #> Action: RIX authorization
         ######################################
         $number_of_data_items_needed = 6; # changed from 4
         
         if (scalar(@DATA_LIST) == $number_of_data_items_needed)
         {
		# MEK 8/1/01, now checks for the highest priced item
		# in the machine to auth, overriding the auth amount
		# the mahcine sent us


		my $ORACLE_AUTH =Evend::Database::Database->new(print_query=>1);

	        my $group_cdref = $ORACLE_AUTH->select(
				table => 'machine_hierarchy',
				select_columns => 'group_cd',
				where_columns =>  ['machine_id = ?'],
		                where_values =>   [$rix_os_ENV{MACHINE_ID}] );

		if( $group_cdref->[0][0] eq 'KODAK' )
		{
			warn("Kodak machine, altering auth_amt\n");

	        	my $auth_amtref = $ORACLE_AUTH->select(
				table => qq{rerix_pricing,
						table(rerix_price)},
				select_columns => 'max(price)',
				where_columns =>  ['machine_id = ?'],
		                where_values =>   [$rix_os_ENV{MACHINE_ID}] );


		        warn("Old Auth Amount: $DATA_LIST[2]\n");
			# add 10% to cover any taxes, and adjust to dollars
			# from cents
			$DATA_LIST[2] = sprintf("%.02f", ($auth_amtref->[0][0] * .011));
		}

	        warn("New: $DATA_LIST[2]\n");

		$payload = join ',', @DATA_LIST;
		my $trans = Evend::Transaction::AuthClient->connect();

		$trans->send("t1,$rix_os_ENV{MACHINE_ID},$payload\n");
                        my $auth_code = $trans->get_code();
                        my $txn_num = $trans->authorization_number();
			$auth_return_msg1 = $trans->account_balance();	
			warn("AUTH RETURN MESSAGE1 = $auth_return_msg1\n");

			$trans->disconnect();

            if (($auth_code eq 'A') || ($auth_code eq 'T')) {
               $txn_num = "$txn_num $txn_num".$rix_os_ENV{SEPARATOR};

               #CPD - this is the start washer/dryer if transaction code
               #my $mach_num = $rix_os_ENV{MACHINE_ID};
               #my $washer = &is_a_washer($mach_num);
               #if ($washer)
               #{
               #  my $fob = 1;
               #  my $fob = $DATA_LIST[0];
               #  $fob = substr($fob, 7);
               #  chop $fob;
               #  print STDERR "CPD fob = $fob\n";
               #  &Laundry_Status(1, $mach_num, $fob);
               #}
            }
            elsif ($auth_code eq 'E') {
               $txn_num = '0'.$rix_os_ENV{SEPARATOR};
            }
 &rix_response($ACK,$txn_num,$auth_return_msg1,$auth_return_msg2);
&rix_response_Raw(">\n");
		
         }
         else
         {
                 &rix_Invalid_Data();
         }
   }     
elsif ($hidden_command_line_buffer =~ /t5/)
   {
         ##################################################################################################################
         # T5 command
         ##################################################################################################################
         ######################################
         #> Action: RIX authorization
         ######################################

         $number_of_data_items_needed = 7; # changed from 4

         if (scalar(@DATA_LIST) == $number_of_data_items_needed)
         {

                $payload = join ',', @DATA_LIST;
                my $trans = Evend::Transaction::AuthClient->connect();

                $trans->send("t1,$rix_os_ENV{MACHINE_ID},$payload\n");
                        my $auth_code = $trans->get_code();
                        my $txn_num = $trans->authorization_number();
                        $auth_return_msg1 = $trans->account_balance();
                        warn("AUTH RETURN MESSAGE1 = $auth_return_msg1\n");

                        $trans->disconnect();

            if (($auth_code eq 'A') || ($auth_code eq 'T')) {
               $txn_num = "$txn_num $txn_num".$rix_os_ENV{SEPARATOR}.$DATA_LIST[6].$rix_os_ENV{SEPARATOR};

               #CPD - this is the start washer/dryer if transaction code
               #my $mach_num = $rix_os_ENV{MACHINE_ID};
               #my $washer = &is_a_washer($mach_num);
               #if ($washer)
               #{
               #  my $fob = 1;
               #  my $fob = $DATA_LIST[0];
               #  $fob = substr($fob, 7);
               #  chop $fob;
               #  print STDERR "CPD fob = $fob\n";
               #  &Laundry_Status(1, $mach_num, $fob);
               #}
            }
            elsif ($auth_code eq 'E') {
               $txn_num = '0'.$rix_os_ENV{SEPARATOR}.$DATA_LIST[6].$rix_os_ENV{SEPARATOR};
            }
 &rix_response($ACK,$txn_num,$auth_return_msg1,$auth_return_msg2);
&rix_response_Raw(">\n");

         }
         else
         {
                 &rix_Invalid_Data();
         }
   }
         ##################################################################################################################
         # X2 command
         ##################################################################################################################
         ######################################
         #> Action: Exit RIX from Authorization
         ######################################
      
         # Log end of RIX session
         &rix_os_End_Session();
         
         # Save Session Exit Information in Tracer
         &rix_tracer_Save_Line("#####################################\n");
         &rix_tracer_Save_Line("# PRI EXIT: ".$rix_os_ENV{"DEVICE_ID"}." - ".$rix_os_ENV{"MACHINE_ID"}."\n");
         &rix_tracer_Save_Line("# ".$rix_os_ENV{"SESSION_END_DATE"}." - ".$rix_os_ENV{"SESSION_END_TIME"}."\n");
         &rix_tracer_Save_Line("#####################################\n");
         
         # Close Tracer
         &rix_tracer_Close();
                         
         # this is not needed for PRI auth
         # &rix_Ack();
         
         $CONTINUE_RIX_SESSION = 0;
      }
      else
      {
              &rix_Fatal_Error("Invalid Device ID","");
      }
   }
   else
   {
           ######################################
           #> Action: Invalid Command
           ######################################
           &rix_Invalid_Data();
   }




}

########################################
#
# rix_Ccu_Status
#
########################################
# Description: Rix status procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER, 
#
# Written By: Scott Wasserman
# Created: 9/27/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Ccu_Status
{
	if ($COMMAND_NUMBER eq "1")
	{
		# Was there data sent?
		if (scalar(@DATA_LIST) > 0)
		{
			######################################
			#> Action: Add counter data to @CCU_STATUS
			######################################
			$counter_number = $DATA_LIST[0];
			$status_data = $DATA_LIST[1];
			
			&rix_os_Add_Ccu_Status_Item(1,$counter_number,$status_data);
			&rix_Ack();
		}
		else
		{
			&rix_Invalid_Data();
		}
	}
	elsif ($COMMAND_NUMBER eq "2")
	{
		# Was there data sent?
		if (scalar(@DATA_LIST) > 0)
		{
			######################################
			#> Action: Add status data to @CCU_STATUS
			######################################
			$status_number = $DATA_LIST[0];
			$status_data = $DATA_LIST[1];
			
			&rix_os_Add_Ccu_Status_Item(2,$status_number,$status_data);
			&rix_Ack();
		}
		else
		{
			&rix_Invalid_Data();
		}
	}
	elsif ($COMMAND_NUMBER eq "3")
	{
		######################################
		#> Action: Write out data in @CCU_STATUS
		######################################
		#&rix_os_Save_Ccu_Status();
		&rix_Ack();
	}
        elsif ($COMMAND_NUMBER eq "4") 
	{
           
           ########################
           # add composite info to @CCU_STATUS
           ########################
           for(my $counter_number = 1; $counter_number < 18; $counter_number++)
           {
              $status_data = $DATA_LIST[$counter_number-1];
              &rix_os_Add_Ccu_Status_Item(1,$counter_number,$status_data);
           }
           
           &rix_Ack();
	}
    elsif ($COMMAND_NUMBER eq "5")
        {

           ########################
           # add composite info to @CCU_STATUS
           ########################
           for(my $counter_number = 1; $counter_number < 4; $counter_number++)
           {
              $status_data = $DATA_LIST[$counter_number-1];
              &rix_os_Add_Ccu_Status_Item(2,$counter_number,$status_data);
           }

           # we are doing away with the u3 cmd, so, for the new CCU software, the CCU status
           # buffer is saved as soon as the u4 cmd is executed. SAK 10/19/2000
           #&rix_os_Save_Ccu_Status();

           &rix_Ack();
        }
	elsif ($COMMAND_NUMBER eq "6")
	{

		my $uhandle = Evend::Database::Database->new(print_query => 1);

		$uhandle->query(
			query => 'update rix_machine set placement_date = SYSDATE where machine_id = ?',
			values => [$rix_os_ENV{MACHINE_ID}],
			);

		$uhandle->update(
			table => 'rix_machine',
			update_columns => 'DISABLE_FLAG',
			update_values => ['N'],
			where_columns => ['Machine_ID = ?'],
			where_values => [$rix_os_ENV{MACHINE_ID}],
		);
	
		&rix_Ack();
	}
	else
	{
	    &rix_Invalid_Data();
	}
}

########################################
#
# rix_Inventory_Card
#
########################################
# Description: Rix Receipt Settings procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER, 
#
# Written By: Scott Wasserman
# Created: 9/28/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Inventory_Card
{
	local(	$inventory_card_number,
			$process_inventory_card_success,
			$curr_local_date,
			$dummy,
			$report_number,
			$new_promotion_name);
			
	if ($COMMAND_NUMBER eq "1")
	{
		######################################
		#> Action: Process inventory card
		######################################
                #In the new form we should uncomment the line below
                &rix_update_inventory();
                #and comment all the others out

		#$inventory_card_number = $DATA_LIST[0];

                ## ($curr_local_date,$dummy) = &rix_os_Get_Local_Date_And_Time();
		##($curr_local_date,$dummy) = &rix_os_Get_Date_And_Time();
		
		##$report_number = &rix_inventory_card_Get_Report_Number_From_Card_Number($inventory_card_number);

                #$process_inventory_card_success = &rix_inventory_card_Process($inventory_card_number);
		
		## Save a status record with the inventory card processing info
		#&rix_os_Save_Ccu_Status();
		
		#if ($process_inventory_card_success)
		#{
		#	# Update machine info
		#	&rix_os_Update_Machine_Info();
			
			## Update data lists if successful update
			#&rix_os_Relogin_Current_Machine();
			#$new_promotion_name = &rix_inventory_card_Get_New_Promotion_Name();
			#$new_promotion_name = substr($new_promotion_name,0,20);
			#&rix_response($ACK,"",$new_promotion_name,"Inventory Updated");
		#}
		#else
                #{
                #        &rix_response($ACK,"","Pick Ticket #".$report_number,"Not Found!",10,1);
		#}
		
	}
	elsif ($COMMAND_NUMBER eq "2")
	{
		######################################
		#> Action: Process next availible fill 
		#>			without inventory card
		######################################
		#($curr_local_date,$dummy) = &rix_os_Get_Local_Date_And_Time();

		my $ref = $oracle->select( table => 'inventory_fill_record',
			select_columns => 'max(inventory_card_number)',
			where_columns => ['machine_id = ?'],
			where_values => [$rix_os_ENV{MACHINE_ID}],
		);

		$process_inventory_card_success = &rix_inventory_card_Process($ref->[0][0]);
		
		# Save a status record with the inventory card processing info
		&rix_os_Save_Ccu_Status();
		
		if ($process_inventory_card_success)
		{
			# Update machine info
			&rix_os_Update_Machine_Info();
			
			# Update data lists if successful update
			&rix_os_Relogin_Current_Machine();
			$new_promotion_name = &rix_inventory_card_Get_New_Promotion_Name();
			$new_promotion_name = substr($new_promotion_name,0,20);
			&rix_response($ACK,"",$new_promotion_name,"Inventory Updated");
		}
		else
		{
			&rix_response($ACK,"","Open Pick Ticket","Not Found!",10,1);
		}

	}
	else
	{
		&rix_Invalid_Command();
	}


}



########################################
#
# rix_Receipt_Settings
#
########################################
# Description: Rix Receipt Settings procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER, %rix_os_ENV
#
# Written By: Scott Wasserman
# Created: 9/23/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Receipt_Settings
{
	######################################
	#> Action: Get receipt settings data
	######################################
	$receipt_data_item_number = $COMMAND_NUMBER;
	$receipt_data = &rix_os_Get_Receipt_Setting_Data($receipt_data_item_number).$rix_os_ENV{"SEPARATOR"};
	
	&rix_response($ACK,$receipt_data);	
}


########################################
#
# rix_Software
#
########################################
# Description: Rix software download procedures
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER 
#				@SOFTWARE_BUFFER, @SOFTWARE_UPDATE_LOG_BUFFER
#
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Software
{
	local(	$software_line_num);

		
	if (($COMMAND_NUMBER eq "1") || ($COMMAND_NUMBER eq "2"))
	{
				
		######################################
		#> Action: Get Software Line
		######################################
		
		$software_line_num = $DATA_LIST[0];

		if ( ($software_line_num < scalar(@SOFTWARE_BUFFER)) &&
				($software_line_num ne "") )
		{
			&rix_response_Raw($SOFTWARE_BUFFER[$software_line_num]);
		}
		else
		{
			&rix_Misc_Error("Invalid Software","Line Number");
		}
	}
	else
	{
		######################################
		#> Action: Invalid Command
		######################################
		&rix_Invalid_Command();
	}
	
}



########################################
#
# rix_Dex_Command
#
########################################
# Description: Sends DEX commands over to the client
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER
#
# Written By: Salim Khan
# Created: 02/22/2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Dex_Command
{
   local(   $number_of_data_items_needed,
            $dex_status,
            $dex_recv_data,
            $dex_next_cmd,
            $server_response_data );

   $dex_server_send_cmd = 0;
   $dex_server_recv_data = 1;
   $dex_server_session_finish = 2;

   if ($COMMAND_NUMBER eq "1")
   {
      ######################################
      #> Action: Process DEX command
      ######################################
      $number_of_data_items_needed = 2;


      if (scalar(@DATA_LIST) == $number_of_data_items_needed)
      {
         (  $dex_status,
            $dex_recv_data ) = @DATA_LIST;

         if ($rix_os_ENV{"DEX_IN_PROGRESS"} == 0)    
         {
            # initialize the SET and GET modes
            $rix_os_ENV{"DEX_SET_MODE"} = 0;
            $rix_os_ENV{"DEX_GET_MODE"} = 0;
            $rix_os_ENV{"DEX_GET_MODE_NEEDED"} = 0;
            
            $rix_os_ENV{"DEX_SLAVE_RESPONSES"} = "";


            # DEX interaction initiated. At this point, the req'd information is 
            # prepared in the background.
            # $rix_os_ENV{"DEX_COMMANDS"} contains an array consisting of lines of DEX commands
            if ( ($rix_os_ENV{"MACHINE_ID"} eq "SCOT") ||
                 ($rix_os_ENV{"MACHINE_ID"} eq "PPS1") )
            {
               ( $rix_os_ENV{"DEX_GET_MODE_NEEDED"}, 
                 @{ $rix_os_ENV{"DEX_COMMANDS"} } ) = (  "0");
            }
            else
            {
               ( $rix_os_ENV{"DEX_GET_MODE_NEEDED"}, 
                 @{ $rix_os_ENV{"DEX_COMMANDS"} } ) =  &dex_Master_Generate_Commands($rix_os_ENV{"MACHINE_ID"});
            }
            

            if (scalar(@{ $rix_os_ENV{"DEX_COMMANDS"} }) == 0)
            {
               # no DEX commands to send over to client. send over "DEX session finished"
               $server_response_data = $dex_server_session_finish.$rix_os_ENV{"SEPARATOR"}.$rix_os_ENV{"SEPARATOR"};
            }
            else
            {
               # DEX session is now in progress...
               $rix_os_ENV{"DEX_IN_PROGRESS"} = 1;
               # set DEX_SET_MODE for future set instructions
               $rix_os_ENV{"DEX_SET_MODE"} = 1;
   
               # assuming a single/multiple DEX cmd(s) in same line (\r\n delimited)
               $dex_next_cmd = shift(@{ $rix_os_ENV{"DEX_COMMANDS"} });
   
               # this is the first line of DEX cmds sent over
               $server_response_data = $dex_server_send_cmd.$rix_os_ENV{"SEPARATOR"}.$dex_next_cmd.$rix_os_ENV{"SEPARATOR"};  
            }
         }
         elsif ($rix_os_ENV{"DEX_IN_PROGRESS"} == 1)
         {
            if ($rix_os_ENV{"DEX_SET_MODE"} == 1)
            {
               # we are in SETting mode...continue ripping DEX cmds from array, and sending
               # them over to client
               if (scalar(@{ $rix_os_ENV{"DEX_COMMANDS"} }) > 0)
               {
                  # we can continue sending lines of DEX cmds over
                  $dex_next_cmd = shift(@{ $rix_os_ENV{"DEX_COMMANDS"} });

                  $server_response_data = $dex_server_send_cmd.$rix_os_ENV{"SEPARATOR"}.$dex_next_cmd.$rix_os_ENV{"SEPARATOR"}; 
               }
               elsif (scalar(@{ $rix_os_ENV{"DEX_COMMANDS"} }) == 0)
               {
                  $dex_next_cmd = ""; # the empty payload 

                  # we are done sending commands over, so at this 
                  # point, we tell client that we are ready to receive data
                  $rix_os_ENV{"DEX_SET_MODE"} = 0;

                  if ($rix_os_ENV{"DEX_GET_MODE_NEEDED"} eq "RECV")
                  {
                     $rix_os_ENV{"DEX_GET_MODE"} = 1;
                     
                     $server_response_data = $dex_server_recv_data.$rix_os_ENV{"SEPARATOR"}.$dex_next_cmd.$rix_os_ENV{"SEPARATOR"};
                  }
                  else
                  {
                     $server_response_data = $dex_server_session_finish.$rix_os_ENV{"SEPARATOR"}.$dex_next_cmd.$rix_os_ENV{"SEPARATOR"};
                  }
               }
               else
               {
                  # should NOT get here!
               }
            }
            elsif ($rix_os_ENV{"DEX_GET_MODE"} == 1)
            {
               # we are in GETting mode...so, tell client that server is ready to receive
               # data

               if ( ($dex_status == 0) ||
                    ($dex_status == 1) ) # remove this 

               {
                  # OK received 
                  if ($dex_recv_data eq "")
                  {
                     # an empty payload indicates end of DEX session
                     $rix_os_ENV{"DEX_SET_MODE"} = 0;
                     $rix_os_ENV{"DEX_GET_MODE"} = 0;
                     $rix_os_ENV{"DEX_IN_PROGRESS"} = 0;

                     # temp - REMOVE 
                     &rix_tracer_Save_Line("slave response is...\n");
                     &rix_tracer_Save_Line($rix_os_ENV{"DEX_SLAVE_RESPONSES"});
                     &rix_tracer_Save_Line("\n");
		

                     &dex_Slave_Accept_Responses($rix_os_ENV{"MACHINE_ID"}, $rix_os_ENV{"DEX_SLAVE_RESPONSES"});

                     $server_response_data = $dex_server_session_finish.$rix_os_ENV{"SEPARATOR"}.$rix_os_ENV{"SEPARATOR"};
                  }
                  else
                  {
                     # the transmission of DEX blocks is incomplete, and it means that the
                     # server needs to continue in DEX_GET_MODE, i.e. $dex_recv_data contains
                     # valuable data
                     $rix_os_ENV{"DEX_SLAVE_RESPONSES"} .= &rix_dex_Process_Recv_Data($dex_recv_data);
   
                     $server_response_data = $dex_server_recv_data.$rix_os_ENV{"SEPARATOR"}.$rix_os_ENV{"SEPARATOR"};
                  }
               }
               # THIS IS TEMP
#                elsif ($dex_status == 1)
#                {
#                   # we've got DEX communication problems..fatal error. stop!
#                   # only DEX is stopped, the RIX session continues
#                   # we send a "DEX session finished"
#                   $rix_os_ENV{"DEX_SET_MODE"} = 0;
#                   $rix_os_ENV{"DEX_GET_MODE"} = 0;
#                   $rix_os_ENV{"DEX_IN_PROGRESS"} = 0;
#
#                   $server_response_data = $dex_server_session_finish.$rix_os_ENV{"SEPARATOR"}.$rix_os_ENV{"SEPARATOR"};
#
#                }
               else
               {
                  # should NOT get here!
                  # server needs to continue in DEX_GET_MODE, i.e. $dex_recv_data contains
                  # valuable data
                  &dex_Slave_Accept_Responses($dex_recv_data);

                  $server_response_data = $dex_recv_data.$rix_os_ENV{"SEPARATOR"}.$rix_os_ENV{"SEPARATOR"}; 
               }


            }
         }
         else
         {
            # Should not get here. $rix_os_ENV{"DEX_IN_PROGRESS"} set to val other than 0 or 1
         }

         &rix_response_DEX($ACK,$server_response_data);
      }
      else
      {
         &rix_Invalid_Data();
      }
   }
   else
   {
      ######################################
      #> Action: Invalid Command
      ######################################
      &rix_Invalid_Command();
   }

}




########################################
#
# rix_Washer_Dryer_Settings
#
########################################
# Description: Perform Maytag washer and dryer info exchange
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER
#
# Written By: Salim Khan
# Created: 09/05/2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Washer_Dryer_Settings
{
   my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;
   my $mach_num = $rix_os_ENV{"MACHINE_ID"};
   #print STDERR "CPD - \n";
   print STDERR "CPD - Machine =  $mach_num\n";
   print STDERR "CPD - Time = $hour\:$min\:$sec $mon\-$mday\-$year\n";
   print STDERR "CPD - Command = $COMMAND_NUMBER\n";
   #print STDERR "CPD - Data String = ***@DATA_LIST***\n";
   my $fob = $DATA_LIST[0];
   $fob = substr($fob, 7);
   chop $fob;
   chop $fob;
   print STDERR " CPD - Fob = ***$fob***\n";

   my ($rec27, @rec27); #my ($RECORD_28, $RECORD_28_ENC);
   if ($COMMAND_NUMBER eq "1")
   {
      ######################################
      #> Action: Record 27 Data Transfer to Washer
      ######################################
      $rec27 = &rix_Send27Rec($mach_num);
      #print STDERR "CPD Rec 27 = *-*$rec27*-*\n";
      @rec27 = split(//,$rec27);
      @rec27 = rix_uuencode(@rec27);
      $rec27 = join('', @rec27);
      $rec27 .= $rix_os_ENV{'SEPARATOR'};
      &rix_response($ACK, $rec27);

      #&rix_Ack();
   }
   elsif ($COMMAND_NUMBER eq "2")
   {
      ######################################
      #> Action: Record 28 Data Transfer from Washer
      ######################################
      $number_of_data_items_needed = 1;
      #print STDERR "CPD Num = " . scalar(@DATA_LIST) . "\n";

      #if (scalar(@DATA_LIST) > $number_of_data_items_needed)
      #{
      #  my $tmp;
      #  foreach $str (@DATA_LIST)
      #  {
      #    $tmp .= $str;
      #  }
      #  @DATA_LIST = ($tmp);
      #}

      if (scalar(@DATA_LIST) >= $number_of_data_items_needed)
      {
         my ($encstring) = join(",", @DATA_LIST);
         #print STDERR "CPD -- *-*$encstring*-*\n";
         #print STDERR "CPD -- " . length($encstring) . "\n";

         if ($encstring eq '')
         {
            #$RECORD_28 = rix_uudecode($RECORD_28_ENC);
            #print STDERR "CPD -**$RECORD_28_ENC**-\n";
            $RECORD_28 = uudec($RECORD_28_ENC);
            #print STDERR "CPD in 2 - $RECORD_28\n";
            #print STDERR "CPD in 2 - $RECORD_28_ENC\n";

            &rix_Get28Rec($mach_num, $RECORD_28);
            #open(DUMP, ">>maytag.dump");
            #print DUMP "*********\n";
            #print DUMP $rix_os_ENV{"MACHINE_ID"}.' '.$rix_os_ENV{'SERVER_TODAY'} .' '.$rix_os_ENV{'SERVER_NOW'}."\n";
            my @res = split(/:/, $RECORD_28);

            rix_DumpWasherDryerInfo(@res);
            #foreach my $item (@res)
            #{
            #   print DUMP sprintf("%02x", $item)." ";
            #}
            #print DUMP "\n";
            #print DUMP "*********\n";
            #close(DUMP);

         }
         else
         {
            #$encstring =~ s/([\\\|\(\)\[\]\{\^\$\*\+\?\.\,\#])/\\$1/g;
            $RECORD_28_ENC .= $encstring;
            print STDERR "CPD enc = $encstring\n";
            print STDERR "CPD REC = $RECORD_28_ENC\n";
         }
      }
      &rix_Ack();
   }
   elsif ($COMMAND_NUMBER eq "3")
   {
      # TMP SAK 10/13/2000
      ######################################
      #> Machine start
      ######################################
      &Laundry_Status(0, $mach_num, $fob);
      &rix_Ack();
   }

   elsif ($COMMAND_NUMBER eq "4")
   {
      # TMP SAK 10/13/2000
      ######################################
      #> machine end
      ######################################
      &Laundry_Status(1, $mach_num, $fob);
      &rix_Ack();
   }




}




########################################
#
# rix_Login
#
########################################
# Description: Rix login procedures
# 
# Uses Globals: %rix_os_ENV, @DATA_LIST, $COMMAND_NUMBER, @SOFTWARE_BUFFER
#
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Login
{
	local(	$device_id_check );
	
	# Is there data in @DATA_LIST?
#         if (scalar(@DATA_LIST) > 0)
	#{
        if ($DEVICE_ID ne "")
        {
		
		# Check device id
		#$device_id_check = $DATA_LIST[0];	
                $device_id_check = $DEVICE_ID;
		if (&rix_os_Init_Session($device_id_check))
		{
			&rix_debug("MACHINE_ID");
							
			if ($COMMAND_NUMBER eq "1")
			{
				######################################
				#> Action: Login for Batch, Boot, or Inventory Card
				######################################
						
				# Log Start of Rix Session
				&rix_os_Log_Session_Start();
				
				# Init all global data needed for Batch, Boot, or Inventory Card
				# &rix_os_Init_Global_Data_Lists();
                                &rix_os_Init_Standard_Login_Data();
				
				# Open tracer if needed
				&rix_tracer_Open();
                                 

                                   $category_flags  = 0; $shift = 1;
                                   $category_flags += $MACHINE_INFO_BUFFER[34]*$shift; $shift *= 2;
                                   $category_flags += $MACHINE_INFO_BUFFER[35]*$shift; $shift *= 2;
                                   $category_flags += $MACHINE_INFO_BUFFER[36]*$shift; $shift *= 2;
                                   $category_flags += $MACHINE_INFO_BUFFER[37]*$shift; $shift *= 2;
                                   $category_flags += $MACHINE_INFO_BUFFER[38]*$shift; $shift *= 2;
                                   $category_flags += $MACHINE_INFO_BUFFER[39]*$shift;
                                   $category_flags .= $rix_os_ENV{"SEPARATOR"}; 
	
		               $category_flags .= $CCU_SETTING_DATA[72];
                                $category_flags .= $rix_os_ENV{"SEPARATOR"};
				$category_flags .= $MACHINE_TEMPLATE_INFO_BUFFER[20];
                                #$category_flags .= 60;
                                $category_flags .= $rix_os_ENV{"SEPARATOR"};
   

				# Save Session Login Information in Tracer
				&rix_tracer_Save_Line("#####################################\n");
				&rix_tracer_Save_Line("# LOGIN: ".$rix_os_ENV{"DEVICE_ID"}." - ".$rix_os_ENV{"MACHINE_ID"}."\n");
				&rix_tracer_Save_Line("# ".$rix_os_ENV{"SESSION_START_DATE"}." - ".$rix_os_ENV{"SESSION_START_TIME"}."\n");
				&rix_tracer_Save_Line("#####################################\n");
				# $command_line_buffer is carried over from the shell
				&rix_tracer_Save_Line(">".$command_line_buffer);
				&rix_response($ACK,$category_flags);

                        }
			elsif ($COMMAND_NUMBER eq "2")
			{
				######################################
				#> Action: Login for Authorization
				######################################
				
				# Init all global data needed for Authorization

                                # Log Start of Rix Session
				&rix_os_Log_Session_Start();
				
				# Init all global data needed for Authorize
                                # &rix_os_Init_Transaction_Authorize_Data_Lists();
                                &rix_os_Init_TransAuth_Login_Data();

				# Open tracer if needed
				&rix_tracer_Open();
	
				# Save Session Login Information in Tracer
				&rix_tracer_Save_Line("#####################################\n");
				&rix_tracer_Save_Line("# LOGIN: ".$rix_os_ENV{"DEVICE_ID"}." - ".$rix_os_ENV{"MACHINE_ID"}."\n");
				&rix_tracer_Save_Line("# ".$rix_os_ENV{"SESSION_START_DATE"}." - ".$rix_os_ENV{"SESSION_START_TIME"}."\n");
				&rix_tracer_Save_Line("#####################################\n");
				# $command_line_buffer is carried over from the shell
				&rix_tracer_Save_Line(">".$command_line_buffer);

                                &rix_Ack();
			}
			elsif ($COMMAND_NUMBER eq "3")
			{
				######################################
				#> Action: Login for Software Update
				######################################
						
				# Create Software Update Log
				&rix_os_Log_Software_Download_Start();
			
				# Init all global data needed for Software Update
				&rix_os_Get_Software();

                                # Open tracer if needed
				# &rix_tracer_Open();
				
				# Turn tracer off for software downloads
				$rix_os_ENV{"TRACE_ON"} = 0;

                                &rix_Ack();
			}
                        
		}
		else
		{
			&rix_Fatal_Error("Invalid Device ID","");
		}
	}
	else
	{
		######################################
		#> Action: Invalid Command
		######################################
		&rix_Invalid_Data();
	}
}

########################################
#
# rix_Exit
#
########################################
# Description: Rix exit procedures
# 
# Uses Globals: $CONTINUE_RIX_SESSION, $COMMAND_NUMBER
#
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Exit
{

	if ($COMMAND_NUMBER eq "1")
	{
		######################################
		#> Action: Exit RIX from Batch, Boot, or Inventory Card session
		######################################
                # The Cancellations module goes in here
                #&rix_txndatabase_Write_Cancelled_Transactions();

		# Log end of RIX session
		&rix_os_End_Session();
		
		# Save Session Exit Information in Tracer
		&rix_tracer_Save_Line("#####################################\n");
		&rix_tracer_Save_Line("# EXIT: ".$rix_os_ENV{"DEVICE_ID"}." - ".$rix_os_ENV{"MACHINE_ID"}."\n");
		&rix_tracer_Save_Line("# ".$rix_os_ENV{"SESSION_END_DATE"}." - ".$rix_os_ENV{"SESSION_END_TIME"}."\n");
		&rix_tracer_Save_Line("#####################################\n");

                if (($rix_os_ENV{'MACHINE_ID'} eq '002L') || 
                    ($rix_os_ENV{'MACHINE_ID'} eq 'APC9') ||
                    ($rix_os_ENV{'MACHINE_ID'} eq 'CRANE1'))
                {
                   # need to add machine_id's to the script too
                   `perl /websites/e-vend/lib/multiDialupSellThru.cgi`;
                }
		
		# Close Tracer
		&rix_tracer_Close();
	
		my $xhandle = Evend::Database::Database->new(print_query => 1);

		my $ref = $xhandle->select( table => 'rix_machine',
			select_columns => 'DISABLE_FLAG',
			where_columns => ['machine_id = ?'],
			where_values => [$rix_os_ENV{'MACHINE_ID'}],
		);

		if ($ref->[0][0] eq 'Y')	
		{			
                	&rix_response();
		}
		else
		{	
			&rix_Ack();
		}
		
		$xhandle->close();
                
                # rix_txndatabase_WriteSellThru();

                if ($rix_os_ENV{'MACHINE_ID'} eq 'SCOT') {
                   #$MACHINE_INFO_BUFFER[34] = 0;
                   #$MACHINE_INFO_BUFFER[35] = 0;
                   #$MACHINE_INFO_BUFFER[36] = 0;
                   #$MACHINE_INFO_BUFFER[37] = 0;
                   #$MACHINE_INFO_BUFFER[38] = 0;
                   #$MACHINE_INFO_BUFFER[39] = 0;
                }

                $MACHINE_INFO_BUFFER[34] = 0;
                $MACHINE_INFO_BUFFER[35] = 0;
                #$MACHINE_INFO_BUFFER[36] = 0; # send_dex_session
                $MACHINE_INFO_BUFFER[37] = 0;
                $MACHINE_INFO_BUFFER[38] = 0;
                $MACHINE_INFO_BUFFER[39] = 0;

                rix_os_Update_Machine_Info();
		&rix_os_Save_Ccu_Status();
			
		$CONTINUE_RIX_SESSION = 0;
	}
	elsif ($COMMAND_NUMBER eq "2")
	{
		######################################
		#> Action: Exit RIX from Authorization
		######################################

                # Log end of RIX session
		&rix_os_End_Session();
		
		# Save Session Exit Information in Tracer
		&rix_tracer_Save_Line("#####################################\n");
		&rix_tracer_Save_Line("# EXIT: ".$rix_os_ENV{"DEVICE_ID"}." - ".$rix_os_ENV{"MACHINE_ID"}."\n");
		&rix_tracer_Save_Line("# ".$rix_os_ENV{"SESSION_END_DATE"}." - ".$rix_os_ENV{"SESSION_END_TIME"}."\n");
		&rix_tracer_Save_Line("#####################################\n");
		
		# Close Tracer
		&rix_tracer_Close();
				
		&rix_Ack();
                
		$CONTINUE_RIX_SESSION = 0;
	}
	elsif ($COMMAND_NUMBER eq "3")
	{
		######################################
		#> Action: Exit RIX from Software Update
		######################################
		
		# Log end of software download
		&rix_os_Log_Software_Download_End();

                # set "update software" to false. 
                # SAK 6/12/2000
                if ($MACHINE_INFO_BUFFER[31])
                {
                   $MACHINE_INFO_BUFFER[31] = 0;

                   #&avc_Database_Update_Record(1,"",@MACHINE_INFO_BUFFER);

			$oracle->update(
				table => 'Machine',
				update_columns => 'Update_Software',
				update_values => [$MACHINE_INFO_BUFFER[31]],
				where_columns => ['Device_ID_FDC = ?'],
				where_values => [$rix_os_ENV{DEVICE_ID}],
			);

                }
		
		&rix_Ack();

		$CONTINUE_RIX_SESSION = 0;
	}
        else
	{
		######################################
		#> Action: Invalid Command
		######################################
		&rix_Invalid_Command();
	}


        # CAUTION: 
        # this has been put in to avoid the race condition between the server and the phone line
        # it has been done for testing purposes ONLY
        # sleep 2;
       #$database->close(); 
	$oracle->close();
}

########################################
#
# rix_Ack
#
########################################
# Description: Display a RIX Ack response
# 
#
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Ack
{
	&rix_response($ACK);
}

########################################
#
# rix_Misc_Error
#
########################################
# Description: Display a RIX error response
# 
#
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Misc_Error
{
	local(	$error_message_1,
			$error_message_2);
			
	$error_message_1 = $_[0];
	$error_message_2 = $_[1];
	
	&rix_response($NAK,"",$error_message_1,$error_message_2);
}

########################################
#
# rix_Fatal_Error
#
########################################
# Description: Display a fatal RIX error response
# 
#
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Fatal_Error
{
	local(	$error_message_1,
			$error_message_2);
			
	$error_message_1 = $_[0];
	$error_message_2 = $_[1];
	
	&rix_response($NAK,"",$error_message_1,$error_message_2);
}

########################################
#
# rix_Checksum_Error
#
########################################
# Description: Display a RIX checksum error response
# 
#
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Checksum_Error
{
	&rix_response($NAK,"","Invalid Checksum","");
}

########################################
#
# rix_Invalid_Command
#
########################################
# Description: Display a RIX invalid command error response
# 
#
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Invalid_Command
{
	&rix_response($FATAL_ERROR,"","Not a Valid","RIX Command");
}

########################################
#
# rix_Invalid_Data
#
########################################
# Description: Display a RIX invalid data error response
# 
#
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Invalid_Data
{
	&rix_response($FATAL_ERROR,"","Invalid Data","");
}

########################################
#
# rix_Process_Message_Data
#
########################################
# Description: Process response messages before sending
# 
# Uses Globals: $MESSAGE_OPTION_BEEP_VALUE, $MESSAGE_OPTION_CENTERED_VALUE,
#				$MESSAGE_OPTION_IMMEDIATE_VALUE, %rix_os_ENV
#
# Written By: Scott Wasserman
# Created: 9/28/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Process_Message_Data
{
	local(	$response_message_1,
			$response_message_2,
			$response_message_delay,
			$response_message_option_beep,
			$response_message_option_centered,
			$response_message_option_immediate,
			$response_message_processed,
			$response_option_processed);
			
	(	$response_message_1,
		$response_message_2,
		$response_message_delay,
		$response_message_option_beep,
		$response_message_option_centered,
		$response_message_option_immediate) = @_;
		
		
	if ( ($response_message_1 eq "") && ($response_message_2 eq "") )
	{
		$response_message_processed = "".$rix_os_ENV{"SEPARATOR"}."";
	}
	else
	{	
		# Force response delay data to 2 digits	
		if ($response_message_delay < 10)
		{
			$response_message_delay = "0".$response_message_delay;
		}
		
		# Calculate response option
		$response_option_processed = 0;
		$response_option_processed += ($response_message_option_beep * $MESSAGE_OPTION_BEEP_VALUE);
		$response_option_processed += ($response_message_option_centered * $MESSAGE_OPTION_CENTERED_VALUE);
		$response_option_processed += ($response_message_option_immediate * $MESSAGE_OPTION_IMMEDIATE_VALUE);
		
		# Force response option data to 2 digits	
		if ($response_option_processed < 10)
		{
			$response_option_processed = "0".$response_option_processed;
		}
		
		$response_message_processed = $response_message_delay.$response_option_processed.$response_message_1.$rix_os_ENV{"SEPARATOR"}.$response_message_2;
		
	}
	
	return $response_message_processed;

}

########################################
#
# rix_Parse_Command_Line
#
########################################
# Description: Get Current RIX Command
# 
# Uses Globals: %rix_os_ENV
#
# NOTE: Assumes $command_group, $command_number, @data_list, 
#		$string_to_checksum are globals to be inited and filled
#
# Written By: Scott Wasserman
# Created: 8/30/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Parse_Command_Line
{
	local(	        $device_id,
                        $command_line_buffer,
			$checksum_break,
			$command_group,
			$command_number,
			$string_to_checksum,
			$checksum,
			@command_data_list,
			$command_chunk);
			
	$command_line_buffer = $_[0];
		
	# Eat the CR
                $ten = sprintf("%c",10);
                $tteen = sprintf("%c",13);
                $command_line_buffer =~ s/$ten//g;
                $command_line_buffer =~ s/$tteen//g;
	chomp($command_line_buffer);

	$command_group = "";
	$command_number = "";
	$string_to_checksum = "";
	$checksum = "";
	
	if ($command_line_buffer ne "")
	{
		#########################
		# Parse Command Line
		#########################
		@command_data_list = split(/[$rix_os_ENV{"SEPARATOR"}]/,$command_line_buffer);

                # Get Device Id
                $device_id = shift(@command_data_list);
		
		# Grab Command
		$command_chunk = shift(@command_data_list);
		# First char is the command group
		$command_group = substr($command_chunk,0,1);
		# All remain chars are command number
		$command_number = substr($command_chunk,1,length($command_chunk)-1);
		
		# Grab checksum
		$checksum = pop(@command_data_list);
		
		#########################
		# Make string_to_checksum so enable checking of checksum
		#########################
		$checksum_break = rindex($command_line_buffer,$rix_os_ENV{"SEPARATOR"});
		$string_to_checksum = substr($command_line_buffer,0,$checksum_break+1);
		
	}
	
	return (	        $device_id,
                                $command_group,
				$command_number,
				$string_to_checksum,
				$checksum,
				@command_data_list);

}


########################################
#
# rix_Override_Separator
#
########################################
# Description: Override separator character
# 
# Uses Globals: %rix_os_ENV, @DATA_LIST
#
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Override_Separator
{
	if ($COMMAND_NUMBER eq "1")
	{
		######################################
		#> Action: Create separator from ascii number
		######################################
		$separator_ascii_num = $DATA_LIST[0];
		$rix_os_ENV{"SEPARATOR"} = sprintf("%c",$separator_ascii_num);
		&rix_Ack();
	}
	elsif ($COMMAND_NUMBER eq "2")
	{
		######################################
		#> Action: Create separator from character
		######################################
		$rix_os_ENV{"SEPARATOR"} =  $DATA_LIST[0];
		&rix_Ack();
	}
	else
	{
		######################################
		#> Action: Invalid Command
		######################################
		&rix_Invalid_Command();
	}
}



#sub rix_uuencode
#{
#   my @byte = split(//, $_[0]);
#   my (@out, @final, $thisbyte, $need, $mask, $nextval, $val, $fill);
#
#   $need = 6;
#   for (;;) {
#      if ($need && scalar(@byte)) {
#         $thisbyte = ord pop @byte;
#         $mask = 2**$need - 1;
#
#        $nextval = ($thisbyte & $mask) << (6-$need);
#        $val += $nextval + 32;
#        $out[$fill++] = $val;
#        
#        $val = ($thisbyte & ~$mask) >> $need;
#        $need = $need - 2; # 6 - (8 - $need)
#     }
#     else {
#        $out[$fill++] = $val + 32;
#        
#        $val = 0;
#        $need = 6;
#
#        if (!scalar(@byte)) {
#           foreach my $c (@out)
#           {
#              unshift @final , sprintf("%c", $c);
#           }
#           return join '', @final;
#        }
#     }
#  }
#
sub rix_uuencode
{
   my (@byte) = @_;

   my $len = scalar @byte;

   for($i = 0; $i < $len; $i++)
   {
      $byte[$i] = ord($byte[$i]);
   }

   my (@final, $ptr);
   for(my $i = 0; $i < $len; $i += 3)
   {
      $final[$ptr++] = sprintf("%c", 32 + ($byte[$i] & 0x3f));
      $final[$ptr++] = sprintf("%c", 32 + ((($byte[$i]>>6) & 3) + (($byte[$i+1]& 0xf)*4)));
      $final[$ptr++] = sprintf("%c", 32 + ((($byte[$i+1]>>4) & 0xf) + (($byte[$i+2]&0x3)*16)));
      $final[$ptr++] = sprintf("%c", 32 + (($byte[$i+2]>>2) & 0x3f));
   }

   return @final;
}
    

sub uudec
{
   my ($message)=@_;
   my @sixbits;
  
   while (length $message)
   {
      push @sixbits, ord($message) - 0x20;
      $message=substr($message,1);
   }
   my $byte =0;
   my $mask;
   my $buts;
   my $first;
   my $second;
   my $third;
   my $fourth;

   while (@sixbits)
   {
      $first = shift @sixbits;
      $second = shift @sixbits;
      $third = shift @sixbits;
      $fourth = shift @sixbits;

      $message .= chr($first | (($second & 3) * 64 ));
      $message .= chr((($third & 0xf) *16)  |  ((($second & 0x3C) >> 2) & 0x0f));
      $message .= chr(($fourth * 4) | ((($third & 0x30) >> 4) & 0x0f));
   }
   #return join ":", (split //, $message);
   return $message;
}
sub rix_uudecode 
{

   # reverse the input string
   my @byte = split //, $_[0];

   
   my $block = 0;
   
   # add 32 (= 0, after subtraction) to make the last chunk a 4-byte block
   my $mod = scalar(@byte)%4;
   for (my $i = 0; ($mod && ($i<(4-$mod))); $i++) {
      push @byte, 32;
   }

   for(my $i = 0; $i < @byte; $i++)
   {
      $mod = $i % 4;
      my $info = ord($byte[$i]) - 32;

      if ($mod == 0) {
         $val[0] = ($info & 0x3f);
      }
      elsif ($mod == 1) {
         $val[0] |= ($info & 0x03) << 6;
         $val[1]  = ($info & 0x3c) >> 2;
      }
      elsif ($mod == 2) {
         $val[1] |= ($info & 0x0f) << 4;
         $val[2]  = ($info & 0x30) >> 4;
      }
      elsif ($mod == 3) {
         $val[2] |= ($info & 0x3f) << 2;

         @final[$block..($block+2)] = @val;
         @val = ();
         $block += 3;
      }
   }

   #foreach my $c (@out)
   #{
      #unshift @final, unpack("B8", sprintf('%c', $c)); # sprintf('%c', $c); 
      #print  sprintf('%c', $c), unpack("B8", sprintf('%c', $c)), unpack("H2", sprintf('%c', $c)), "\n";
      #unshift @final, unpack("H2", $c); #, unpack("B8", sprintf('%d', $c)); # sprintf('%c', $c); 
   #}
   # @final = reverse @out;
   
   return join ':', @final;

}

sub rix_DumpWasherDryerInfo
{
#    $pos = 0;
#    $record_type = substr
#
#    $model_code = $_[$pos++];
#
#    $machine_ID_number = join(//, @_[$pos..$pos+9]);
#    $pos += 10;
#
#    $position_code = $_[$pos++];
#
#    $location_number = join(//, @_[$pos..$pos+2]);
#    $pos += 3;
#
#    $regular_price = $_[$pos++];
#
#    $regular_cold_price = $_[$pos++];
#
#    $regular_warm_price = $_[$pos++];
#
#    $regular_hot_price = $_[$pos++];
#
#    $special_price = $_[$pos++];
#
#    $special_cold_price = $_[$pos++];
#
#    $special_warm_price = $_[$pos++];
#
#    $special_hot_price = $_[$pos++];
#
#    $super_cycle_price_adder = $_[$pos++];
#
#    $super_cycle_type =

   ################################ REAL CODE STARTS HERE ####################################
#
#    $info = shift;
#
#    $record_type = substr($info, 0, 1);
#
#    $model_code = substr($info, 1, 1);
#
#    $machine_ID_number = substr($info, 2, 10);
#
#    $position_code = substr($info, 12, 1);
#
#    $location_number = substr($info, 13, 3);
#
#    $regular_price = substr($info, 16, 1);
#
#    $regular_cold_price = substr($info, 17, 1);
#
#    $regular_warm_price = substr($info, 18, 1);
#
#    $regular_hot_price = substr($info, 19, 1);
#
#    $special_price = substr($nfo, 20, 1);
#
#    $special_cold_price = substr($info, 21, 1);
#
#    $special_warm_price = substr($info, 22, 1);
#
#    $special_hot_price = substr($info, 23, 1);
#
#    $super_cycle_price_adder = substr($info, 24, 1);
#
#    $super_cycle_type = substr($info, 25, 1);
#
#    $features_1 = substr($info, 26, 1);
#
#    $features_2 = substr($info, 27, 1);
#
#    $features_3 = substr($info, 28, 1);
#
#    $special_price_days = substr($info, 29, 1);
#
#    $special_price_days_begin_time = substr($info, 30, 1);
#
#    $special_price_days_end_time = substr($info, 31, 1);
#
#    $prewash_time = substr($info, 32, 1);
#
#    $final_spin_time = substr($info, 33, 1);
#
#    $wash_time = substr($info, 34, 1);
#
#    $number_rinses_per_cycle = substr($info, 35, 1);
#
#    $minutes_rinse_agitate = substr($info, 36, 1);
#
#    $top_spin_speed = substr($info, 37, 1);
#
#    $value_of_coin_1 = substr($info, 38, 1);
#
#    $value_of_coin_2 = substr($info, 39, 1);
#
#    $penny_increment_pricing_offset = substr($info, 40, 1);
#
#    # jumping one byte (unused)
#
#    $factory_test_states_1 = substr($info, 42, 1);
#
#    $factory_test_states_2 = substr($info, 43, 1);
#
#    $factory_test_states_3 = substr($info, 44, 1);
#
#    $factory_thermistor_calibration_1 = substr($info, 45, 1);
#
#    $factory_thermistor_calibration_2 = substr($info, 46, 1);
#
#    $cum_regular_priced_cold_cycles = substr($info, 47, 2);
#
#    $cum_regular_priced_warm_cycles = substr($info, 49, 2);
#
#    $cum_regular_priced_hot_cycles = substr($info, 51, 2);
#
#    $cum_special_priced_cold_cycles = substr($info, 53, 2);
#
#    $cum_special_priced_warm_cycles = substr($nfo, 55, 2);
#
#    $cum_special_priced_hot_cycles = substr($info, 57, 2);
#
#    $cum_super_cycle_upgrades = substr($info, 59, 2);
#
#    $cum_service_cycles = substr($info, 61, 2);
#
#    # some chars are missing here
#    $cum_money_penny_offset = substr($info, 66, 2);
#
#    $cum_money = substr($info, 68, 3);
#
#    $cum_probe_reads = substr($info, 71, 1);
#
#    $factory_cum_possum_count = substr($info, 72, 1);
#
#    $factory_cum_warm_E2_count = substr($info, 73, 1);
#
#    # unused
#
#    $interval_suds_cycle = substr($info, 77, 1);
#
#    $interval_suds_recovery_cycles = substr($info, 78, 1);
#
#    $interval_regular_priced_cycles = substr($info, 79, 2);
#
#    $interval_special_priced_cycles = substr($info, 81, 2);
#
#    $interval_no_runs = substr($info, 83, 1);
#
#    $interval_service_cycles = substr($info, 84, 1);
#
#    $interval_trouble_cycles = substr($info, 85, 1);
#
#    $interval_power_downs = substr($info, 86, 1);
#
#    $interval_unauth_service_door_openings = substr($info, 87, 1);
#
#    $interval_auth_service_door_openings = substr($info, 88, 1);
#
#    $interval_unauth_coin_vault_openings = substr($info, 89, 1);
#
#    $interval_auth_coin_vault_openings = substr($info, 90, 1);
#
#    $last_service_door_opening_time = substr($info, 91, 2);
#
#    $last_coin_vault_opening_time = substr($info, 93, 2);
#
#    $last_trouble_cycle_time = substr($info, 95, 2);
#
#    $last_power_down_time = substr($info, 97, 2);
#
#    $previous_service_door_opening_time = substr($info, 99, 2);
#
#    $previous_coin_vault_opening_time = substr($info, 101, 2);
#
#    $previous_trouble_cycle_time = substr($info, 103, 2);
#
#    $previous_power_down_time = substr($info, 105, 2);
#
#    $last_setup_time = substr($info, 107, 3);
#
#    $current_status_1 = substr($info, 110, 1);
#
#    $current_status_2 = substr($info, 111, 1);
#
#    $current_status_3 = substr($info, 112, 1);
#
#    $diagnostic_codes_1 = substr($info, 113, 1);
#
#    $diagnostic_codes_2 = substr($info, 114, 1);
#
#    $diagonstic_codes_2 = substr($info,  115, 1);
#
#    $factory_diagnostic_history_1 = substr($info, 116, 1);
#
#    $factory_diagnostic_history_2 = substr($info, 117, 1);
#
#    $factory_diagnostic_history_3 = substr($info, 118, 1);
#
#    $factory_firmware_version = substr($info, 119, 1);
#
#
#    $DBASE_ORACLE->insert( table          => 'Rerix_Sell_Through',
#                           insert_columns => 'MACHINE_ID,
#                                              RECORD_TYPE,
#                                              MODEL_CD,
#                                              MACHINE_NUMBER,
#                                              POSITION_CD,
#                                              LOCATION_NUMBER,
#                                              REG_PRICE,
#                                              REG_COLD_PRICE,
#                                              REG_WARM_PRICE,
#                                              REG_HOT_PRICE,
#                                              SP_PRICE,
#                                              SP_COLD_PRICE,
#                                              SP_WARM_PRICE,
#                                              SP_HOT_PRICE,
#                                              SUPER_CYC_PRICE_ADD,
#                                              SUPER_CYC_TYPE,
#                                              FEATURE_1,
#                                              FEATURE_2,
#                                              FEATURE_3,
#                                              SP_PRICE_DAYS,
#                                              SP_PRICE_BEGIN_TIME,
#                                              SP_PRICE_END_TIME,
#                                              REG_DRYER_TIME,
#                                              SP_DRYER_TIME,
#                                              WASH_TIME,
#                                              RINSES_PER_CYC,
#                                              RINSE_AGIT_TIME,
#                                              TOP_SPIN_SPEED,
#                                              COIN_1,
#                                              COIN_2,
#                                              PENNY_OFFSET,
#                                              FACTORY_TEST_1,
#                                              FACTORY_TEST_2,
#                                              FACTORY_TEST_3,
#                                              FACTORY_THERM_CAL_1,
#                                              FACTORY_THERM_CAL_2,
#                                              FIELD_37,
#                                              FIELD_38,
#                                              FIELD_39,
#                                              FIELD_40,
#                                              FIELD_41,
#                                              FIELD_42,
#                                              FIELD_43,
#                                              CUM_SERVICE_CYC,
#                                              CUM_MONEY_PENNY_OFFSET,
#                                              CUM_MINUTE_TICKER,
#                                              CUM_PROBE_READS,
#                                              FACTORY_CUM_1,
#                                              FACTORY_CUM_2,
#                                              INT_SUDS_CYC,
#                                              INT_REG_TOPOFF,
#                                              INT_SUDS_RECV_CYC,
#                                              INT_SP_TOPOFF,
#                                              INT_REG_PRICE_CYC,
#                                              INT_SP_PRICE_CYC,
#                                              INT_NO_RUNS,
#                                              INT_SRV_CYC,
#                                              INT_TROUBLE_CYC,
#                                              INT_POWER_DOWN,
#                                              INT_UNAUTH_SERV_DOOR_OPEN,
#                                              INT_AUTH_SERV_DOOR_OPEN,
#                                              INT_UNAUTH_COIN_VAULT_OPEN,
#                                              INT_AUTH_COIN_VAULT_OPEN,
#                                              TIME_LAST_SERV_DOOR_OPEN,
#                                              TIME_LAST_COIN_VAULT_OPEN,
#                                              TIME_LAST_TROULBE_CYC,
#                                              TIME_LAST_POWER_DOWN,
#                                              TIME_PREV_SERV_DOOR_OPEN,
#                                              TIME_PREV_COIN_VAULT_OPEN,
#                                              TIME_PREV_TROUBLE_CYC,
#                                              TIME_PREV_POWER_DOWN,
#                                              TIME_LAST_SETUP,
#                                              CURRENT_STATUS_1,
#                                              CURRENT_STATUS_2,
#                                              CURRENT_STATUS_3,
#                                              DIAG_CD_1,
#                                              DIAG_CD_2,
#                                              DIAG_CD_3,
#                                              FACTORY_DIAG_HIST_1,
#                                              FACTORY_DIAG_HIST_2,
#                                              FACTORY_DIAG_HIST_3,
#                                              FACTORY_FIRMWARE',
#                           insert_values =>  [
#
#
#
#
#
#                           insert_values  => [$mymachine, $date, 'X', $sellthru_ref->{$date}]
#                         );
   
}


########################################
#
# rix_Tax_Settings
#
########################################
# Description: Returns server settings for sales and usage tax processing
# 
# Uses Globals: @DATA_LIST, $COMMAND_NUMBER, %rix_os_ENV
#
# Written By: Salim Khan
# Created: 9/15/2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_Tax_Settings
{

   my( $tax_data_item_num,
       $tax_data );

   ######################################
   #> Action: Get tax settings data
   ######################################
   $tax_data_item_num = $COMMAND_NUMBER;
   $tax_data = &rix_os_Get_Tax_Setting_Data($tax_data_item_num).$rix_os_ENV{"SEPARATOR"};

   &rix_response($ACK,$tax_data);
}

########################################
#
# Update_Inventory
#
########################################
# Description: Updates the inventory for a txn
#
# Written By: Chris Donohue
# Rewritten by : LInda MInnich
# Created: 2/6/2001
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 2/13/01      LPM          Changed the script to run APPLY_INVENTORY
#
########################################
sub Update_Inventory()
{
  my ($mach_id, $col_num,$card_type, $trans_no) = @_;

  my $dbptr = Evend::Database::Database->new();
  
  my $raw_handle = $dbptr->{handle};
  my $out_msg = 0;

  #returns in the Msg field a "BAD" for failure and a "GOOD" for success...
  my $csr = $raw_handle->prepare(q{BEGIN
                                   APPLY_INVENTORY(:MACHINE_ID,:COLUMN_ID,:OUT_MSG,:TRANS_NO);
                                   END;
                                  });

  $csr->bind_param(":MACHINE_ID", $mach_id);
  $csr->bind_param(":COLUMN_ID",  $col_num);
  $csr->bind_param_inout(":OUT_MSG", \$out_msg, 1);
  $csr->bind_param(":TRANS_NO", $trans_no, 1);
  $csr->execute;

  warn("OUT MESSAGE ", $out_msg);
  warn("Success in Applying Inventory to $mach_id and $col_num out message is ".$out_msg."\n") if ($out_msg eq "GOOD");
  warn("FAILURE in Applying Inventory to $mach_id and $col_num out message is ".$out_msg."\n") if ($out_msg eq "BAD");
  $dbptr->close();
  return;

}

sub Get_Machine_Type
{

	my $ORACLE = shift;
	my $machine = shift;

	my $machine_rec = $ORACLE->select( table => 'rix_machine A, Machine_type B',
				select_columns => 'b.column_translation_Type',
				where_columns => ['a.machine_id = ?','a.machine_type_number = b.machine_type_number'],
				where_values => [$machine],
			);

	if ($machine_rec->[0][0] == 1) { return 'APC'; }
	if ($machine_rec->[0][0] == 2) { return 'CRANE'; }
	return undef;
}

sub Recreate_APC_Button_Info
{
   my ($command, $ORACLE, $machine, $payload) = @_;

   my ($txn_num,
       $txn_time,
       $txn_date,
       $magstrip,
       $card_type,
       $auth_num,
       $col_num,
       $inv_num,
       $txn_amt,
       $tax_amt,
       $q1_data,
       $q2_data,
       $button_num,
       $txn_status);
   if ($command eq 't2') {
      ($txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status) = split /,/, $payload;
   } elsif ($command eq 't4') {
      ($txn_time, $txn_date, $magstrip, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num) = split /,/, $payload;
   }

$button_num =~ s/^0+//g; if ($button_num eq '') { $button_num = 0; } # SAK 01/18/01


	$ORACLE->{print_query} = 1;
	

   my $machine_rec = $ORACLE->select( table     => 'rix_Machine',
                                       select_columns => 'Location_Number',
                                        where_columns => ['Machine_ID=?'],
                                         where_values => [$machine] );


   my $location_rec = $ORACLE->select( table    => 'Location',
                                       select_columns => 'Tax_Percentage',
                                        where_columns => ['Location_Number=?'],
                                         where_values => [$machine_rec->[0][0]]
);

   my $tax_pct = $location_rec->[0][0];

   my $inv_id_ref = $ORACLE->select(   table          => "rerix_inventory A, TABLE(a.Inventory) B",
                                       select_columns => "b.Inventory_Item_Number",
                                       where_columns  => ["machine_id = ?", "b.Button_ID = ?"],
                                       where_values   => [$machine, $button_num]
 );

   $inv_num = $inv_id_ref->[0][0];

   my $inv_price_ref = $ORACLE->select(   table          => "rerix_pricing A, TABLE(a.Rerix_Price) B",
                                          select_columns => "b.Price",
                                          where_columns  => ["machine_id = ?", "b.Button_ID = ?"],
                                          where_values   => [$machine, $button_num] );

   $txn_amt  = sprintf("%.2f", $inv_price_ref->[0][0]/100);;
   $tax_amt = sprintf("%.2f", $txn_amt*$tax_pct);

   if ($command eq 't2') {
      $payload = join(',', $txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status);
   }
   elsif ($command eq 't4')  {
      $payload = join(',', $txn_time, $txn_date, $magstrip, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num);
   }

   return $payload;
}

sub Recreate_CRANE_Button_Info
{
   my ($command, $ORACLE, $machine, $payload) = @_;

   my ($txn_num,
       $txn_time,
       $txn_date,
       $magstrip,
       $card_type,
       $auth_num,
       $col_num,
       $inv_num,
       $txn_amt,
       $tax_amt,
       $q1_data,
       $q2_data,
       $button_num,
       $txn_status);

   if ($command eq 't2') {
      ($txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status) = split /,/, $payload;

   }
   elsif ($command eq 't4') {
      ($txn_time, $txn_date, $magstrip, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num) = split /,/,$payload;
   }

$button_num =~ s/^0+//g; if ($button_num eq '') { $button_num = 0; } # SAK 01/18/01


   my $machine_rec = $ORACLE->select( table     => 'rix_Machine',
                                       select_columns => 'Location_Number',
                                        where_columns => ['Machine_ID=?'],
                                         where_values => [$machine] );


   my $location_rec = $ORACLE->select( table    => 'Location',
                                       select_columns => 'Tax_Percentage',
                                        where_columns => ['Location_Number=?'],
                                         where_values => [$machine_rec->[0][0]]
);

   my $tax_pct = $location_rec->[0][0];
   my $inv_id_ref = $ORACLE->select( table          => "rerix_inventory A, TABLE(a.Inventory) B",
                                       select_columns => "b.Inventory_Item_Number",
                                       where_columns  => ["machine_id = ?", "b.Button_ID = ?"],
                                       where_values   => [$machine, $button_num]);

   $inv_num = $inv_id_ref->[0][0];


   my $inv_price_ref = $ORACLE->select( table          => "rerix_pricing A, TABLE(a.Rerix_Price) B",
                                          select_columns => "b.Price",
                                          where_columns  => ["machine_id = ?", "b.Button_ID = ?"],
                                          where_values   => [$machine, $button_num] );


   $txn_amt  = sprintf("%.2f", $inv_price_ref->[0][0]/100);;
   $tax_amt = sprintf("%.2f", $txn_amt*$tax_pct);
   if ($command eq 't2') {
      $payload = join(',', $txn_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num, $txn_status);
   }
   elsif ($command eq 't4')  {
      $payload = join(',', $txn_time, $txn_date, $magstrip, $card_type, $auth_num, $col_num, $inv_num, $txn_amt, $tax_amt, $q1_data, $q2_data, $button_num);
   }

	return $payload;
}



#CPDHERE - for I1 commands.
sub rix_update_inventory()
{
  warn "In I1\n";
  my $machine_id = $rix_os_ENV{"MACHINE_ID"};

  my $OracleDB = Evend::Database::Database->new( print_query=>1 );

  my $ref = $OracleDB->select( table => 'INVENTORY_FILL A, table(a.INV_FILL) b',
                               select_columns => "a.MACHINE_ID, to_char(a.FILL_CREATE_DATE, 'MM/DD/YYYY HH24:MI:SS'), a.FILL_CD, a.FILL_DATE, b.COLUMN_ID, b.INVENTORY_ITEM_NUMBER, b.FILL_LEVEL, b.EXPIRE_DATE",
                               where_columns => ['a.MACHINE_ID = ?'],
                               where_values => [$machine_id],
                               order => 'a.FILL_CREATE_DATE'
                             );


  if ($ref->[0] == NULL)  #in case there isn't a fill tix
  {
	&rix_response($ACK); return;
  }

#  my ($j,$j,$j,$day,$mon,$year,$j,$j,$j) = localtime;
#  $mon++;
#  $year += 1900;
  
  my $data_set;
  my $fill_date = $$ref[0][1];
  foreach my $row (@$ref)
  {
    #warn "$fill_date, $$row[1]\n";

    if ($$row[1] ne $fill_date)   #then we've gotten to the next
    {                            #row and should move on
      last;  
    }
    my $col = $$row[4];
    my $item = $$row[5];
    my $level = $$row[6];
    push @$data_set, [$col, $item, $level]
  }
  $OracleDB->insert( table => 'INVENTORY_LEVEL',
                     insert_columns => 'MACHINE_ID, INV_LEVEL',
                     insert_values => [$machine_id, $data_set],
                   );

  my $fill_date = $$ref[0][1];

  #$OracleDB->update ( table => 'INVENTORY_FILL',
                      #update_columns => 'FILL_CD',
                      #update_values => ['A'],
                      #where_columns => ['MACHINE_ID = ?', 'to_char(FILL_CREATE_DATE, \'MM\/DD\/YYYY\') = ?'],
                      #where_values => [$machine_id, $fill_date]
                    #);

  $OracleDB->query ( query => "update INVENTORY_FILL set FILL_CD='A' where machine_id = ? and FILL_CREATE_DATE =  to_date(?, 'MM/DD/YYYY HH24:MI:SS')",
                     values => [$machine_id, $fill_date]
                    );

  $OracleDB->close();
  &rix_response($ACK);
}

sub replace_magstrip
{

	warn("in replace magstrip\n");

        my $in = shift;
        my $char = chr(224);
        my $magstrip;

        my @inarray = split /$char/, $in;

        if ($inarray[1] eq 't1')
        {
                $magstrip = $inarray[2];
                $magstrip =~ /(\d{15,16})\^/;
                $magstrip =~ s/$1/****************/g;
                $inarray[2] = $magstrip;

        }
        if ($inarray[1] eq 't4')
        {
                $magstrip = $inarray[4];
                $magstrip =~ /(\d{15,16})\^/;
                $magstrip =~ s/$1/****************/g;
                $inarray[4] = $magstrip;

        }
        if ($inarray[1] eq 't5')
        {
                $magstrip = $inarray[2];
                $magstrip =~ /(\d{15,16})\^/;
                $magstrip =~ s/$1/****************/g;
                $inarray[2] = $magstrip;
    
        }
        return join($char,@inarray);
}


}

1;
