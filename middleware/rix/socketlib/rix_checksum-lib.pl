#!/opt/bin/perl
#################################################################
#
# rix_checksum-lib.pl
# 
# Version 1.00
#
#################################################################
#
# Written By: Scott Wasserman
# Created: 8/30/1999
# (c)1999 e-Vend.net Corporation
#
#
#
#################################################################
# Functions Availible:
#################################################################
#
#
# 
#
# 
#
#
#
#################################################################
# Required Libraries
#################################################################

#################################################################
# Setup
#################################################################

#################################################################
# Globals 
#################################################################

########################################
#
# rix_checksum_Dec_To_Bin
#
########################################
# Description: Convert a decimal number to a binary number
# 
# Written By: Salim A. Khan
# Created: 11/30/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_checksum_Dec_To_Bin
{
   local (  $number,
            $return_value );

   $number = $_[0];

   $return_value = unpack("B32", pack("N", $number));

   $return_value =~ s/^0+(?=\d)//; # else you'll get leading zeroes

   return $return_value;
}

########################################
#
# rix_checksum_Bin_To_Dec
#
########################################
# Description: Convert a binary number to a decimal number.
# 
# Written By: Salim A. Khan
# Created: 11/30/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_checksum_Bin_To_Dec
{
   local (  $number,
            $return_value );

   $number = $_[0];

   $return_value = unpack("N", pack("B32", substr("0" x 32 . $number, -32)));

   return $return_value;
}


sub rix_checksum_Dec_To_Hex    
{
   local(); 
my $value = sprintf "%lx", $_[0];

   return sprintf "%lx", $_[0];
}

########################################
#
# rix_checksum_Hex_To_Dec
#
########################################
# Description: Convert hex number to decimal number
# 
# Written By: Salim A. Khan
# Created: 8/31/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_checksum_Hex_To_Dec
{
   local ( $number );

   	$number = $_[0];

  	return hex($number);
}



########################################
#
# rix_checksum_Make_From_String
#
########################################
# Description: Create a 2 digit Hex checksum from a string
# 
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_checksum_Make_From_String
{
	local(	$string_in,
			$checksum_out_hex,
			$checksum_out_decimal);
			
	$string_in = $_[0];
	
	$checksum_out_decimal = &rix_checksum_Make_Binary(&rix_checksum_Count_Integer_Values($string_in));

	
	
	$checksum_out_hex = &rix_checksum_Dec_To_Hex($checksum_out_decimal);
	
	# Force it to 2 digits
	if (length($checksum_out_hex) == 1)
	{
		$checksum_out_hex = "0".$checksum_out_hex;
	}
	
	if (length($checksum_out_hex) == 3)
	{
		# If we have 3 digits then we have to kill the first digit because we can only return
		# 2 bytes of checksum.
		$checksum_out_hex = substr($checksum_out_hex,1,length($checksum_out_hex));
	}
	
	return $checksum_out_hex;
}

########################################
#
# rix_checksum_Valid
#
########################################
# Description: Is the checksum valid for the string?
# 
# Written By: Scott Wasserman
# Created: 8/30/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_checksum_Valid
{
	local(	$string_to_checksum,
			$checksum);
			
	$string_to_checksum = $_[0];
	$checksum = $_[1];

        if ( ($COMMAND_GROUP eq "d") && ($COMMAND_NUMBER eq "1"))
        {
           # Substitutions are made for characters that can cause disruption of RIX transmission
           $TAB = chr(9);
           $LF = chr(10);
           $CR = chr(13);

           # DLE and ETB substitutions are temp. 6/22/2000 SAK
           $DLE = chr(16);
           $ETB = chr(23);
          
           $ESC = chr(128);
   
           $ZER0  = chr(0x30);  # 0x30 = "0"
           $ONE   = chr(0x31);  # 0x31 = "1"
           $TWO   = chr(0x32);  # 0x32 = "2"
           $THREE = chr(0x33);  # 0x33 = "3"

           # DLE and ETB substitutions are temp. 6/22/2000 SAK
           $FOUR = chr(0x34);  # 0x34 = "4"
           $FIVE = chr(0x35);  # 0x35 = "5"

           $curr_filename = "/home/salim/sltf.txt.KODA";

           open(SLTF,">>$curr_filename");

           select(SLTF);
           $| = 1;


           @ary = split(//, $string_to_checksum);
           print SLTF "before re-sub: ";
           foreach $char (@ary)
              {
                 print SLTF "<".ord($char).">"; 

              }

           print SLTF "\n";
                           
           # (ESC)(ONE) -> (TAB)
           $string_to_checksum =~ s/$ESC$ONE/$TAB/g;
   
           # (ESC)(TWO) -> (LF)
           $string_to_checksum =~ s/$ESC$TWO/$LF/g;

           # (ESC)(THREE) -> (CR)
           $string_to_checksum =~ s/$ESC$THREE/$CR/g;

           # (ESC)(FOUR) -> (DLE)
           $string_to_checksum =~ s/$ESC$FOUR/$DLE/g;

           # (ESC)(FIVE) -> (ETB)
           $string_to_checksum =~ s/$ESC$FIVE/$ETB/g;

           # (ESC)(ZERO) -> (ESC)
           $string_to_checksum =~ s/$ESC$ZERO/$ESC/g;

           @ary = split(//, $string_to_checksum);
           print SLTF "after re-sub : ";
           foreach $char (@ary)
              {
                 print SLTF "<".ord($char).">"; 

              }

           print SLTF "\n";

           close SLTF;

           select(STDOUT);
           $| = 1;

        }

	my $new_checksum = rix_checksum_Make_From_String($string_to_checksum);
	if ((&rix_checksum_Make_From_String($string_to_checksum)) == $checksum)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

########################################
#
# rix_checksum_Count_Integer_Values
#
########################################
# Description: Add up the integer values in a string
# 
# Written By: Scott Wasserman
# Created: 8/30/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_checksum_Count_Integer_Values
{
	local(	$string_in,
			@string_split,
			$byte_counter,
			$char_counter,
			$line_char_int,
			$line_char);
	
	$string_in = $_[0];
	
	@string_split = split(//,$string_in);
	
	$byte_counter = 0;
	for ($char_counter = 0;$char_counter < scalar(@string_split);$char_counter++)
	{
		 $line_char = $string_split[$char_counter];
		 $line_char_int = ord($line_char);
		 $byte_counter += $line_char_int;
	}
	
	return $byte_counter;
}


########################################
#
# rix_checksum_Make_Binary
#
########################################
# Description: Return an 8-bit checksum
# 
# Written By: Scott Wasserman
# Created: 8/30/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_checksum_Make_Binary
{		
	return 256 - ($_[0] - (256 * (int($_[0]/256)) ) );
}



1; #return true
