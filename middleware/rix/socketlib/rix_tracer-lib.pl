#!/opt/bin/perl
#################################################################
#
# rix_tracer-lib.pl
# 
# Version 1.00
#
#################################################################
#
# Written By: Scott Wasserman
# Created: 10/11/1999
# (c)1999 e-Vend.net Corporation
#
#
#
#################################################################
# Functions Availible:
#################################################################
#
# 
#
# 
#
# 
#
#
#
#################################################################
# Required Libraries
#################################################################
# require("avcbridge_templates-lib.pl");

#################################################################
# Setup
#################################################################
$home_dir = '/rix';
########################################
#
# rix_tracer_Save_Line
#
########################################
# Description: Save trace line
#
# Uses Globals: TRACE_FILE
# 
# Written By: Scott Wasserman
# Created: 10/11/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################		
sub rix_tracer_Save_Line
{
	local(	$trace_line_in);


	$trace_line_in = $_[0];
	
	if ($rix_os_ENV{"TRACE_ON"})
	{
		print TRACE_FILE $trace_line_in;
	}
}


########################################
#
# rix_tracer_Open
#
########################################
# Description: Open trace file
#
# Uses Globals: %rix_os_ENV, TRACE_FILE
# 
# Written By: Scott Wasserman
# Created: 10/11/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_tracer_Open
{
	local(	$trace_file_name,
			$trace_file_tweak);

	if ($rix_os_ENV{"TRACE_ON"})
	{
		$trace_file_name = $home_dir."/traces/".$rix_os_ENV{"MACHINE_ID"}.".tracer";
		$rix_os_ENV{"TRACE_FILE_NAME"} = $trace_file_name;
		open(TRACE_FILE,">>".$trace_file_name);
		chmod(0777,$trace_file_name);
		print TRACE_FILE "\n";
		#CHANGED
		#&avc_Database_Set_File_Protection($trace_file_name);
		
		# Turn off file buffering
		$trace_file_tweak = select(TRACE_FILE);
		$| = 1;
		select($trace_file_tweak);
	}
}

########################################
#
# rix_tracer_Close
#
########################################
# Description: Close trace file
#
# Uses Globals:  TRACE_FILE
# 
# Written By: Scott Wasserman
# Created: 10/11/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_tracer_Close
{
	if ($rix_os_ENV{"TRACE_ON"})
	{	
		close(TRACE_FILE);
	}
}

1; # return true



