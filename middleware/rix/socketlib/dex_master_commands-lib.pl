#!/opt/bin/perl

################################################################################
#
# dex_master_commands-lib.pl
#
################################################################################
#
# Written By: Josh Richards
# Created: 2/15/2000
# Last Modified: 2/15/2000
# (c)2000 Goodvest Corporation
#
################################################################################
#push(@INC, "/websites/e-vend/lib/");
#require("avcbridge_database_core-lib.pl");
#require("avcbridge_database_query-lib.pl");
require("dex_build_commands-lib.pl");
#require("nama_inventory-lib.pl");

use Evend::Database::Database;
%dbdexhash;
$database_dex = Evend::Database::Database->new(print_query => $print_query);


# use strict;

################################################################################
my (@SETTINGS,
    @BUTTON_SETTINGS,
    $BUTTONS,
    $COLUMNS);

################################################################################
# dex_Master_Generate_Commands
################################################################################
sub dex_Master_Generate_Commands
{
	return 0;
}

################################################################################
# dex_Append_Join_Chars_To_Array
################################################################################
sub dex_Append_Join_Chars_To_Array
{
    my (@in_commands,
	@out_commands,
	$join_vals,
	$count);

    @in_commands = @_;
    
    ## 0x0A is ASCII for LineFeed and
    ## 0X0D is ASCII for CarriageReturn
    $join_vals = chr(0x0D).chr(0x0A);

    for ($count=0; $count<scalar(@in_commands); $count++)
    {
	$out_commands[$count] = $in_commands[$count].$join_vals;
    }

    return @out_commands;
}

################################################################################
# dex_Initialize_Master_Globals
################################################################################
sub dex_Initialize_Master_Globals
{
    my ($machine_num,
	$key,
	$record,
	@records,
	@unpacked,
	@machine_record,
	@machine_type_record,
	@button_vals);

    $machine_num = $_[0];

    if ($machine_num ne "")
    {
        $mrref = $database_dex->select( table => 'Machine',
                                select_columns => '*',
                                where_columns => ['Machine_ID = ?'],
                                where_values => [$machine_num],
                        );
        @machine_record = @{$mrref->[0]};

        $mtcref = $database_dex->select( table => 'Machine_Type',
                        select_columns => '*',
                        where_columns => ['Machine_Type_Number = ?'],
                        where_values => [$machine_record[29]],
                );
        @machine_type_rec = @{$mtcref->[0]};
#CHANGED
	#@machine_record = &avc_Database_Get_Keyed_Record(1, $machine_num);
	#@machine_type_record = &avc_Database_Get_Keyed_Record(10, $machine_record[29]);
	$BUTTONS = $machine_type_record[9];
	$COLUMNS = $machine_type_record[10];

       $middbcheck = $rix_os_ENV{"MACHINE_ID"};
        if (!$dbdexhash{$middbcheck})
        {
                $dbdexhash{$middbcheck} = Evend::Database::Database->new(print_query => $print_query );
        }

	$kfref = $dbdexhash{$middbcheck}->select( table => 'DEX_Master_Settings_Record',
					select_columns => 'max(Key_Field)',
				);

	$dmsrref =  $dbdexhash{$middbcheck}->select( table => 'DEX_Master_Settings_Record',
				select_columns => '*',
				where_columns => ['Key_Field = ?'],
				where_values => [$kfref->[0][0]],
			);

	@SETTINGS = @{$dmsrref->[0]};
	
#CHANGED
	#@SETTINGS = &avc_Database_Get_Last_Record(62, $machine_num);


#CHANGED

       $kfref = $dbdexhash{$middbcheck}->select( table => 'DEX_Master_Button_Settings_Record',
                                    select_columns => 'max(Key_Field)',
                                );

	$dmbsrref = $dbdexhash{$middbcheck}->select( table => 'DEX_Master_Button_Settings_Record',
			select_columns => '*',
			where_columns => ['Key_Field = ?'],
			where_values => [$kfref->[0][0]],
		);
	@BUTTON_SETTINGS = @{$dmbsrref->[0]};
	
	#@BUTTON_SETTINGS = &avc_Database_Get_Last_Record(63, $machine_num);

	## Check if we received any content back from database query
	## if either @SETTINGS or @BUTTON_SETTINGS had data, then
	## return 1 (true content flag)
	
	## Special developer flag.. fake it for SCOT and JOSH
	if ($machine_num eq "SCOT") ## or $machine_num eq "JOSH")
	{
	    return 1;
	}
	elsif ($SETTINGS[0] ne "" or $BUTTON_SETTINGS[0] ne "")
	{
	    return 1;
	}
	## Otherwise, no content to generate
	else
	{
	    return 0;
	}
    }
    ## We did not get a machine_num passed in, no content here either
    else
    {
	return 0;
    }
}

################################################################################
# dex_generate_command_group_trailer
################################################################################
sub dex_Generate_Command_Group_Trailer
{
    my ($command1,
	$command2,
	$line_count);

    $line_count = $_[0];

    $command1 = &dex_Build_Commands_SE($line_count, "1");
    $command2 = &dex_Build_Commands_DXE("1", "1");

    return ($command1, $command2);
}

################################################################################
# dex_Generate_Master_Group_OPTIONS
################################################################################
sub dex_Generate_Master_Group_OPTIONS
{
    my ($command1, 
	$command2,
	$date,
	@dates,
	$hours,
	$mins,
	$command_datetime,
	$time,
	@return_commands,
	@all_commands);
    
    
    ## Check if we should reset all errors
    if ($SETTINGS[24] == 1)
    {
	$command1 = &dex_Build_Commands_MC5("RESET");
	push(@return_commands, $command1);
    }

    ## Check if we should reset all fields
    if ($SETTINGS[25] == 1)
    {
	$command2 = &dex_Build_Commands_MC5("CF");
	push(@return_commands, $command2);
    }

    ## Check if we should sync datetime
    if ($SETTINGS[26] == 1)
    {
	@dates = split("/", &get_Extended_Date());
	$date = substr($dates[2], -2).$dates[0].$dates[1];

	($hours, $mins) = &get_Extended_Time() =~ m/(\d+):(\d+).*/;
	$hours = "0".$hours if($hours < 10);

	$command_datetime = &dex_Build_Commands_IC5($date, $hours.$mins);
	
	push(@return_commands, $command_datetime);
    }

    return @return_commands;
}

################################################################################
# dex_Generate_Master_Group_DISCOUNT
################################################################################
sub dex_Generate_Master_Group_DISCOUNT
{
    my (@days_selected,
	$start_hour,
	$start_mins,
	$stop_hour,
	$stop_mins,
	$command_ls,
	$counter,
	$command_lc,
	@commands_all_lcs,
	$command_le,
	$day,
	@prices,
	$prod_id,
	$command_ec_on,
	$command_ec_off,
	@commands_all_ecs);
    
    ## Days selected for discounts
    @days_selected = split(",", $SETTINGS[22]);

    ## Start and stop times for discounts
    ($start_hour, $start_mins) = split(":", $SETTINGS[20]);
    ($stop_hour, $stop_mins) = split(":", $SETTINGS[21]);

    ## Discounted button prices
    for ($count=0; $count<$BUTTONS; $count++)
    {
	$prices[$count] = $BUTTON_SETTINGS[($count+1)*4];
    }
     
    $command_ls = &dex_Build_Commands_LS("0001");
    
    for ($counter=1; $counter<=scalar(@prices); $counter++)
    {
	$command_lc = &dex_Build_Commands_LC1(2, $counter, $prices[$counter-1]);
	push(@commands_all_lcs, $command_lc);
    }
    
    $command_le =  &dex_Build_Commands_LE("0001");
    
    foreach $day (@days_selected)
    {
	$command_ec_on = &dex_Build_Commands_EC2("PL2 ON", $day, $start_hour, $start_mins);
	$command_ec_off = &dex_Build_Commands_EC2("PL2 OFF", $day, $stop_hour, $stop_mins);
	push(@commands_all_ecs, $command_ec_on, $command_ec_off);
    }
    
    return ($command_ls, @commands_all_lcs, $command_le, @commands_all_ecs);
}

################################################################################
# dex_Generate_Master_Group_LIMITED
################################################################################
sub dex_Generate_Master_Group_LIMITED
{
    my (@days_selected,
	$col_item,
	$hex_map,
	$period,
	$day,
	$event,
	$start_hour,
	$start_mins,
	$command1,
	$stop_hour,
	$stop_mins,
	$start_index,
	$stop_index,
	$command2,
	@all_commands);
    
    ## Get the days that are to have limited access on
    @days_selected = split(",", $SETTINGS[17]);

    ## Limited access uses a column bit map to mark which columns
    ## are to be limited during specified times
    $hex_map = $SETTINGS[18];

    ## There are 2 limited access periods
    for ($period=1; $period<=2; $period++)
    {
	if ($period == 1)
	{
	    $start_index = 13;
	    $stop_index = 14;
	}
	else
	{
	    $start_index = 15;
	    $stop_index = 16;
	}
	
	## Do for all days selected...
	foreach $day (@days_selected)
	{
	    $event = "LA ON".$period;
	    ($start_hour, $start_mins) = split(":", $SETTINGS[$start_index]);
	    $command1 = &dex_Build_Commands_EC2($event, $day, $start_hour, $start_mins, "", $hex_map);

	    $event = "LA OFF".$period;
	    ($stop_hour, $stop_mins) = split(":", $SETTINGS[$stop_index]);
	    $command2 = &dex_Build_Commands_EC2($event, $day, $stop_hour, $stop_mins, "", $hex_map);
	    
	    push(@all_commands, $command1, $command2);
	}
    }

    return @all_commands;
}

################################################################################
# dex_Generate_Master_Group_STS
################################################################################
sub dex_Generate_Master_Group_STS
{
    my ($coin_type,
	$escrow_mode,
	$command_ls,
	$command_le,
	$button_counter,
	$command_pc,
	@commands_pc,
	$col_map,
	$price,
	$prod_id,
	$column_selected,
	@commands_mc,
	$command_mc);
    
    $coin_type = $SETTINGS[10];
    $escrow_mode = $SETTINGS[11];

    $command_ls = &dex_Build_Commands_LS("0001");
    
    for ($button_counter=1; $button_counter <= $BUTTONS; $button_counter++)
    {
	$prod_id = $BUTTON_SETTINGS[($button_counter-1)*4 + 1];
	$price = $BUTTON_SETTINGS[($button_counter-1)*4 + 2];
	$column_selected = $BUTTON_SETTINGS[($button_counter-1)*4 + 3];

	$command_pc = &dex_Build_Commands_PC1($button_counter, $price, $prod_id);
	push(@commands_pc, $command_pc);
	
	$col_map = &dex_Get_Bitmap_From_Binary_Array( ("")x($column_selected-1), ($column_selected ? 1 : 0));
	$command_mc = &dex_Build_Commands_MC5($button_counter, $col_map, $button_counter, $coin_type, $escrow_mode);
	push(@commands_mc, $command_mc);
    }

    $command_le = &dex_Build_Commands_LE("0001");

    return ($command_ls, @commands_pc, $command_le, @commands_mc);
}

################################################################################
# dex_Generate_Master_Group_PASS
################################################################################
sub dex_Generate_Master_Group_PASS
{
    my ($cur_password,
	$new_password,
	$command);

    $cur_password = &dex_Clean_String($SETTINGS[7]);
    $new_password = &dex_Clean_String($SETTINGS[8]);

    $command = &dex_Build_Commands_SD1($cur_password, $new_password);

    return $command;
}


################################################################################
# dex_Generate_Master_Group_ID
################################################################################
sub dex_Generate_Master_Group_ID
{
    my ($asset,
	$location,
	$model,
	$serial,
	$command);

    $asset    = &dex_Clean_String($SETTINGS[5]);
    $location = &dex_Clean_String($SETTINGS[4]);				  
    $model    = &dex_Clean_String($SETTINGS[3]);
    $serial   = &dex_Clean_String($SETTINGS[2]);

    $command = &dex_Build_Commands_IC1($serial, $model, "", $location, "", $asset);

    return $command;
}

################################################################################
# dex_Generate_Command_Group_Header
################################################################################
sub dex_Generate_Master_Group_Header
{
    my ($command1, 
	$command2);

    $command1 = &dex_Build_Commands_DXS("0000000000","VA", "V1/1", "1");
    $command2 = &dex_Build_Commands_ST("001", "0001");				  

    return ($command1, $command2);
}


################################################################################
# dex_Clean_String
################################################################################
sub dex_Clean_String
{
    my ($out) = $_[0];

    $out =~ s/\*//g;

    return $out;
}

################################################################################
# dex_Get_Bitmap_From_Selection
################################################################################
sub dex_Get_Bitmap_From_Binary_Array
{
    my ($bit_map,
	$counter,
	$bit_map,
	@in_array,
	$hex_map);
    
    @in_array = @_;

    $bit_map = 0;

    for ($counter=0; $counter<scalar(@in_array); $counter++)
    {
	if ($in_array[$counter] == 1)
	{
	    $bit_map += $in_array[$counter] << ($counter);
	}
    }
    
    $hex_map = sprintf("%0.4X", $bit_map);

    return $hex_map;
}

################################################################################
# dex_Generate_54_Button_Commands
################################################################################
sub dex_Generate_54_Button_Commands
{
    my ($machine_num,
	@data,
	$command_ls,
	$button_counter,
	$prod_id,
	$price,
	$command_pc,
	$command_le,
	@commands);
    
    $machine_num = $_[0];
   
      $middbcheck = $rix_os_ENV{"MACHINE_ID"};
        if (!$dbdexhash{$middbcheck})
        {
                $dbdexhash{$middbcheck} = Evend::Database::Database->new(print_query => $print_query);
        }

#CHANGED

       $kfref = $dbdexhash{$middbcheck}->select( table => 'DEX_Master_Button_Settings_Record',
                                    select_columns => 'max(Key_Field)',
                                );

        $dmbsrref = $dbdexhash{$middbcheck}->select( table => 'DEX_Master_Button_Settings_Record',
                        select_columns => '*',
                        where_columns => ['Key_Field = ?'],
                        where_values => [$kfref->[0][0]],
                );
        @data = @{$dmbsrref->[0]};
#CHANGED 
#    @data = &avc_Database_Get_Last_Record(63, $machine_num);
    ## Read in the list of price change flags
    open(PC, "</websites/e-vend/databases/nama_price_changes.txt");
    @flags = <PC>;
    close(PC);

    for ($button_counter=1; $button_counter<=54; $button_counter++)
    {
	if ($flags[$button_counter-1] == 1)
	{
	    $prod_id = $data[($button_counter-1)*4 + 1];
	    $price = $data[($button_counter-1)*4 + 2];
	    
	    if ($price eq "")
	    {
		$price = 0;
	    }
	    
	    $row = int($button_counter/9);
	    $col = $button_counter%9;
	    
	    if ($col == 0)
	    {
		$col = 9;
		$row--;
	    }
	    
	    $item = chr(65+$row).$col;
	    ##$price = ($row+1)*$col*100;
            ##JHR Added ending *
	    $command_pc = "PC1*$item*$price";
	    if (scalar(@commands) < 5)
	    {
		push(@commands, $command_pc);
	    }
	}
    }
    
    if (scalar(@commands) == 0)
    {
       ##JHR Added ending *
	@commands = ("PC1*A1*".$data[2]);
	## @commands = ("PC1*A1*");
    }

    &nama_Init_Price_Change_Flags();
    return @commands;
}

################################################################################
# dex_Generate_Std_Button_Commands
################################################################################
sub dex_Generate_Std_Button_Commands
{
    my ($machine_num,
	@data,
	$command_ls,
	$button_counter,
	$prod_id,
	$price,
	$command_pc,
	$command_le,
	@commands,
        $num_buttons,
        $item);
    
    ($machine_num, $num_buttons) = @_;

    #&rix_tracer_Save_Line("num buttons: $num_buttons\n"); 
         $middbcheck = $rix_os_ENV{"MACHINE_ID"};
        if (!$dbdexhash{$middbcheck})
        {
                $dbdexhash{$middbcheck} = Evend::Database::Database->new(print_query => $print_query);
        }

#CHANGED

       $kfref = $dbdexhash{$middbcheck}->select( table => 'DEX_Master_Button_Settings_Record',
                                    select_columns => 'max(Key_Field)',
                                );

        $dmbsrref = $dbdexhash{$middbcheck}->select( table => 'DEX_Master_Button_Settings_Record',
                        select_columns => '*',
                        where_columns => ['Key_Field = ?'],
                        where_values => [$kfref->[0][0]],
                );
        @data = @{$dmbsrref->[0]};
#CHANGED
 
    #@data = &avc_Database_Get_Last_Record(63, $machine_num);
    ## Read in the list of price change flags
    # open(PC, "</websites/e-vend/databases/nama_price_changes.txt");
    # @flags = <PC>;
    # close(PC);
    @commands = ();

    for ($button_counter=1; $button_counter<=$num_buttons; $button_counter++)
    {
	# if ($flags[$button_counter-1] == 1)
        # this is temprarily set to TRUE, and will be changed once we have the capability 
        # to detect price changes in the DB.
        if (1)
	{
	    #$prod_id = $data[($button_counter-1)*4 + 1];
	    $price = $data[($button_counter-1)*4 + 2];
	    
	    if ($price eq "")
	    {
		$price = 0;
	    }
	    
	    # $row = int($button_counter/9);
	    # $col = $button_counter%9;
	    
	    # if ($col == 0)
	    # {
	    # 	$col = 9;
	    # 	$row--;
	    # }
	    
	    $item = $button_counter;
	    ##$price = ($row+1)*$col*100;
            ##JHR Added ending *
	    $command_pc = "PC1*$item*$price";
	    
            #Commented the limiting of Price changes to 5 out JHR 6.21.2000
            #if (scalar(@commands) < 5)
	    #{
	    push(@commands, $command_pc);
	    #}
	}
    }
    
    if (scalar(@commands) == 0)
    {
       ##JHR Added ending *
	@commands = ("PC1*1*".$data[2]);
	## @commands = ("PC1*A1*");
    }

    #&nama_Init_Price_Change_Flags();
    return @commands;
}


################################################################################
## dex_Special_NAMA_Commands
################################################################################
##
## This does the last minute formatting of the DEX commands to smush less than 245
## characters onto one line. This way we can send just 3 or 4 commands (complete
## sessions btw) instead of 50 or 60 separate ones. cool.
## yes i know this code is crappy.. but you know what? i churned it out in a few
## minutes. will fix later.. for now it is working.
sub dex_Merge_Commands
{ 
    my (@header_commands,
	@chars,
	@body_commands,
	$last_flag,
	$too_big,
	$this_line,
	$commands_so_far,
	$command,
	$count,
	@com_letters,
	@line_letters,
	$machine_num,
	@all_commands);

    ($machine_num, @body_commands) = @_;

    @header_commands = &dex_Generate_Master_Group_Header();
    @header_commands = &dex_Append_Join_Chars_To_Array(@header_commands);
    
    $headers = join("", @header_commands);
    @chars = split("", $headers);
    ## print "There are ", scalar(@chars), " chars in the header\n";
     
    @body_commands = &dex_Append_Join_Chars_To_Array(@body_commands);
    ## &dump_List(@body_commands);
    
    $last_flag = 0;
    $too_big = 0;
    $this_line = $headers;
    $commands_so_far = 2;
    
    for ($count=0; $count<scalar(@body_commands); $count++)
    {
	$command = $body_commands[$count];
	## print "working on $command\n";
	
	@com_letters = split("", $command);
	@line_letters = split("", $this_line);
	
	$char_total_with_new_line = scalar(@com_letters)+scalar(@line_letters);
	if ($char_total_with_new_line < 227)
	{
	    $this_line .= $command;
	    $commands_so_far++;
	}
	else
	{
	    $too_big = 1;
	}
	
	## Make sure we force the write out on the last one
	if ($count == scalar(@body_commands)-1)
	{
	    $last_flag = 1;
	}
	
	## Check if we should start a new command
	if ($too_big or $last_flag)
	{
	    ## If this is not the last one, we need to redo it
	    if ($last_flag == 0 or
		($last_flag == 1 and $too_big == 1))
	    {
		$count--;
	    }
	    
	    $too_big = 0;
	    ## Then we have to end this one.
	    @trailer_commands = &dex_Generate_Command_Group_Trailer($commands_so_far);
	    @trailer_commands = &dex_Append_Join_Chars_To_Array(@trailer_commands);
	    $trailers = join("", @trailer_commands);
	    
	    @trails = split("", $trailers);
	    ## print "Trailer has ", scalar(@trails), " chars in it\n";
	    $this_line .= $trailers;
	    
	    @line_letters = split("", $this_line);
	    
	    push(@all_commands, $this_line);
	    
	    $commands_so_far = 2; ## Consider ST and SE
	    $this_line = $headers;		    
	}
    }
    return  @all_commands;
}



################################################################################
1; # return true
