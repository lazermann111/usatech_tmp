#!/opt/bin/perl
#################################################################
#
# rix_response-lib.pl
# 
# Version 1.00
#
#################################################################
#
# Written By: Scott Wasserman
# Created: 10/5/1999
# (c)1999 e-Vend.net Corporation
#
#
#
#################################################################
# Functions Availible:
#################################################################
#
# 
#
# 
#
# 
#
#
#
#################################################################
# Required Libraries
#################################################################
# require("rix_checksum-lib.pl");
# require("rix_tracer-lib.pl");

#################################################################
# Setup
#################################################################

#################################################################
# Globals 
#################################################################
########################################
#
# rix_response_Raw
#
########################################
# Description: Return data raw.  Used for returning software lines
# 
#
# Written By: Scott Wasserman
# Created: 9/27/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_response_Raw
{
	local(	$data_in);
	
	$data_in = $_[0];

	print $sock $data_in;
}

########################################
#
# rix_response
#
########################################
# Description: Display a RIX response
# 
# Uses Globals: $DEFAULT_MESSAGE_DELAY, $DEFAULT_MESSAGE_OPTION_BEEP
#				$DEFAULT_MESSAGE_OPTION_CENTERED, $DEFAULT_MESSAGE_OPTION_IMMEDIATE,
#				$LINE_BREAK, %rix_inv_ENV
#
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_response
{
	local(	$rix_response_num,
			$response_message_1,
			$response_message_2,
			$response_message_delay,
			$response_message_option_beep,
			$response_message_option_centered,
			$response_message_option_immediate,
			$response_message_processed,
			$rix_response,
			$rix_response_output);
	
	
	$rix_response_num = $_[0];
	$response_data = $_[1];
	$response_message_1 = $_[2];
	$response_message_2 = $_[3];
	$response_message_delay = $_[4];
$LINE_BREAK = "\r\n";
	
	if ($response_message_delay eq "")
	{
		$response_message_delay = $DEFAULT_MESSAGE_DELAY;
	}
	
	$response_message_option_beep = $_[5];
	if ($response_message_option_beep eq "")
	{
		$response_message_option_beep = $DEFAULT_MESSAGE_OPTION_BEEP;
	}
	
	$response_message_option_centered = $_[6];
	if ($response_message_option_centered eq "")
	{
		$response_message_option_centered = $DEFAULT_MESSAGE_OPTION_CENTERED;
	}
	
	$response_message_option_immediate = $_[7];
	if ($response_message_option_immediate eq "")
	{
		$response_message_option_immediate = $DEFAULT_MESSAGE_OPTION_IMMEDIATE;
	}
	
	# Process response message data
	$response_message_processed = &rix_Process_Message_Data($response_message_1,
															$response_message_2,
															$response_message_delay,
													$response_message_option_beep,
															$response_message_option_centered,
															$response_message_option_immediate);
	
	$rix_response = $rix_os_ENV{"DEVICE_ID"}.$rix_os_ENV{"SEPARATOR"}.$rix_response_num.$rix_os_ENV{"SEPARATOR"}.$response_message_processed.$rix_os_ENV{"SEPARATOR"}.$response_data;
	
        $rix_response_output = $rix_response.&rix_checksum_Make_From_String($rix_response).$LINE_BREAK;
	
	&rix_tracer_Save_Line("$rix_response_output\n");

	warn("$rix_response_output");	
	print $sock $rix_response_output;
}


########################################
#
# rix_response_DEX
#
########################################
# Description: Display a RIX server DEX response
# 
# Uses Globals: 
#
# Written By: Salim Khan
# Created: 2/23/2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_response_DEX
{
	local(	$rix_response_num,
			$response_message_1,
			$response_message_2,
			$response_message_delay,
			$response_message_option_beep,
			$response_message_option_centered,
			$response_message_option_immediate,
			$response_message_processed,
			$rix_response,
			$rix_response_output,
                        $TAB,
                        $LF,
                        $ESC,
                        $ZERO,
                        $ONE,
                        $TWO,
                        $THREE );
	
	
	$rix_response_num = $_[0];
	$response_data = $_[1];
	$response_message_1 = $_[2];
	$response_message_2 = $_[3];
	$response_message_delay = $_[4];

$LINE_BREAK = "\r\n";
	
	if ($response_message_delay eq "")
	{
		$response_message_delay = $DEFAULT_MESSAGE_DELAY;
	}
	
	$response_message_option_beep = $_[5];
	if ($response_message_option_beep eq "")
	{
		$response_message_option_beep = $DEFAULT_MESSAGE_OPTION_BEEP;
	}
	
	$response_message_option_centered = $_[6];
	if ($response_message_option_centered eq "")
	{
		$response_message_option_centered = $DEFAULT_MESSAGE_OPTION_CENTERED;
	}
	
	$response_message_option_immediate = $_[7];
	if ($response_message_option_immediate eq "")
	{
		$response_message_option_immediate = $DEFAULT_MESSAGE_OPTION_IMMEDIATE;
	}

        # Process response message data
	$response_message_processed = &rix_Process_Message_Data($response_message_1,
                                                                $response_message_2,
                                                                $response_message_delay,
                                                                $response_message_option_beep,
                                                                $response_message_option_centered,
                                                                $response_message_option_immediate);
	
	$rix_response = $rix_os_ENV{"DEVICE_ID"}.$rix_os_ENV{"SEPARATOR"}.$rix_response_num.$rix_os_ENV{"SEPARATOR"}.$response_message_processed.$rix_os_ENV{"SEPARATOR"}.$response_data;
	
        $rix_response_output = $rix_response.&rix_checksum_Make_From_String($rix_response);

        # Substitutions are made for characters that can cause disruption of RIX transmission
        $TAB = chr(9);
        $LF = chr(10);
        $CR = chr(13);
       
        $ESC = chr(128);

        $ZER0  = chr(0x30);  # 0x30 = "0"
        $ONE   = chr(0x31);  # 0x31 = "1"
        $TWO   = chr(0x32);  # 0x32 = "2"
        $THREE = chr(0x33);  # 0x33 = "3"

        # (ESC) -> (ESC)(ZERO)
        $rix_response_output =~ s/$ESC/$ESC$ZERO/g;

        # (TAB) -> (ESC)(ONE)
        $rix_response_output =~ s/$TAB/$ESC$ONE/g;

        # (LF) -> (ESC)(TWO)
        $rix_response_output =~ s/$LF/$ESC$TWO/g;

        # (CR) -> (ESC)(THREE)
        $rix_response_output =~ s/$CR/$ESC$THREE/g;

        $rix_response_output .= $LINE_BREAK;
	
	&rix_tracer_Save_Line("$rix_response_output\n");
	
	print $sock $rix_response_output;
}


1; # return true
