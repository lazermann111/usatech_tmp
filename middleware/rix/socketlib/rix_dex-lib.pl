#!/opt/bin/perl
#################################################################
#
# rix_dex-lib.pl
# 
# Version 1.00
#
#################################################################
#
# Written By: Salim Khan
# Created: 12/23/1999
# (c)1999 e-Vend.net Corporation
#
#
#
#################################################################
# Functions Availible:
#################################################################
#  rix_dex_Process_Command
# 
#
# 
#
# 
#
#
#
#################################################################
# Required Libraries
#################################################################

#################################################################
# Setup
#################################################################

#################################################################
# Globals 
#################################################################

########################################
#
# rix_dex_Process_Recv_Data
#
########################################
# Description: Re-subsitutes all the escape chars in the data 
# received from the client.
# 
#
# Written By: Salim Khan
# Created: 3/28/2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_dex_Process_Recv_Data
{  
   local(   $dex_recv_data );

   $dex_recv_data = $_[0];

   $TAB = chr(9);
   $LF = chr(10);
   $CR = chr(13);
  
   $ESC = chr(128);

   $ZER0  = chr(0x30);  # 0x30 = "0"
   $ONE   = chr(0x31);  # 0x31 = "1"
   $TWO   = chr(0x32);  # 0x32 = "2"
   $THREE = chr(0x33);  # 0x33 = "3"

   

   # (ESC)(ONE) -> (TAB) 
   $dex_recv_data =~ s/$ESC$ONE/$TAB/g;

   # (ESC)(TWO) -> (LF)  
   $dex_recv_data =~ s/$ESC$TWO/$LF/g;

   # (ESC)(THREE) -> (CR) 
   $dex_recv_data =~ s/$ESC$THREE/$CR/g;

   # (ESC)(ZERO) -> (ESC)
   $dex_recv_data =~ s/$ESC$ZERO/$ESC/g;

   return $dex_recv_data;
}

1; #return true

