#!/opt/bin/perl

use Evend::Database::Database;
$print_query = 1;
#$database = Evend::Database::Database->new(print_query => $print_query, mysql => 1, database => 'global');
$oracle = Evend::Database::Database->new(print_query => $print_query, primary => 'usadbp01', backup => 'usadbp01');
#################################################################
#
# rix_os-lib.pl
#
#################################################################
#
# Written By: Scott Wasserman
# Created: 8/31/1999
#
# (c)1999 e-Vend.net Corporation
#
#
#
#################################################################
# Functions Availible:
#################################################################
#
#
#################################################################
# Required Libraries
#################################################################
#require("avcbridge_database_query-lib.pl");
#require("slw-lib.pl");

#push(@INC, "/home/goodvest/evend/rix6_os");
#require("rix_transaction-lib.pl");

# new avcdatabase increment and decrement date in here
require("rix_date-lib.pl");

$home_dir = '/home/rmayberry/rix6/';
require("rix_checksum-lib.pl");
require("rix_response-lib.pl");
#require("rix_inventory_card-lib.pl");
require("rix_tracer-lib.pl");
require("rix_debug-lib.pl");
#require("rix_paymentech-lib.pl");
#require("rix_dex-lib.pl");
require("dex_build_commands-lib.pl");
require("dex_master_commands-lib.pl");
require("dex_slave_commands-lib.pl");

#################################################################
# Setup
#################################################################

#################################################################
# Globals 
#################################################################
$rix_os_ENV{"DEVICE_ID_RECORD_SIZE"} = 20;
$rix_os_ENV{"DEVICE_ID_FILENAME"} = "deviceid.rec";

# This only re-initializes the hash. Need to find a global initializer (if needed).
# Init rix_os_ENV
# %rix_os_ENV = ();

# Default SEPARATOR character
$separator_ascii_num = 224;
$rix_os_ENV{"SEPARATOR"} = sprintf("%c",$separator_ascii_num);

########################################
#
# rix_os_Get_Ccu_Setting_Data
#
########################################
# Description: Get CCU setting data from a data item number
#
# Uses Globals: @CCU_SETTING_DATA
# 
# Written By: Scott Wasserman
# Created: 9/21/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Get_Ccu_Setting_Data
{
	local(	$data_item_number);
	
	$data_item_number = $_[0];

	if ((($data_item_number<70) && ($data_item_number!=25) && ($data_item_number!=35) && ($data_item_number!=64)) || 
           ($data_item_number==90) || 
           ($data_item_number==91) ||
           ($data_item_number==92) || 
           ($data_item_number==93) ||
           ($data_item_number==94) ||
           ($data_item_number==109)||
           ($data_item_number==110))
        {
            return "0000".$CCU_SETTING_DATA[$data_item_number];
        }
        else
        {
           return $CCU_SETTING_DATA[$data_item_number];
        }

}

########################################
#
# rix_os_Get_Swipe_Setting_Data
#
########################################
# Description: Get swipe setting data from a data item number
#
# Uses Globals: @SWIPE_SETTING_DATA
# 
# Written By: Scott Wasserman
# Created: 9/23/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Get_Swipe_Setting_Data
{
	local(	$data_item_number);
	
	$data_item_number = $_[0];
	
	return $SWIPE_SETTING_DATA[$data_item_number];

}

########################################
#
# rix_os_Get_Vmc_Setting_Data
#
########################################
# Description: Get VMC setting data from a data item number
#
# Uses Globals: @VMC_SETTING_DATA
# 
# Written By: Scott Wasserman
# Created: 9/23/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Get_Vmc_Setting_Data
{
	local(	$data_item_number);
	
	$data_item_number = $_[0];
	
	return $VMC_SETTING_DATA[$data_item_number];

}

########################################
#
# rix_os_Get_Receipt_Setting_Data
#
########################################
# Description: Get VMC setting data from a data item number
#
# Uses Globals: @RECEIPT_SETTING_DATA
# 
# Written By: Scott Wasserman
# Created: 9/23/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Get_Receipt_Setting_Data
{
	local(	$data_item_number);
	
	$data_item_number = $_[0];
	
	return $RECEIPT_SETTING_DATA[$data_item_number];

}

########################################
#
# rix_os_Update_Ccu_Setting_Date_And_Time
#
########################################
# Description: Update current local date and time in @CCU_SETTING_DATA
#
# Uses Globals:  @CCU_SETTING_DATA
# 
# Written By: Scott Wasserman
# Created: 10/5/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Update_Ccu_Setting_Date_And_Time
{
	local(	$local_date,
			$local_time);
			
	($local_date,$local_time) = &rix_os_Get_Local_Date_And_Time();
	
	# Update current local date and time in @CCU_SETTING_DATA item 70
	$CCU_SETTING_DATA[70] = $local_date." ".$local_time;
 $CCU_SETTING_DATA[72] = $CCU_SETTING_DATA[70].$rix_os_ENV{"SEPARATOR"}.$CCU_SETTING_DATA[71];

	
}

########################################
#
# rix_os_Init_Global_Data_Lists
#
########################################
# Description: Fill global data lists
#
# Uses Globals: %rix_os_ENV, @LOCATI0N_INFO_BUFFER, @CCU_SETTING_DATA, 
#				@VMC_SETTING_DATA, @SWIPE_SETTING_DATA, @RECEIPT_SETTING_DATA,
#				@CCU_STATUS_BUFFER,%CCU_STATUS_RECORD_LAYOUT
# 
# Written By: Scott Wasserman
# Created: 9/20/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
# sub rix_os_Init_Global_Data_Lists
# {
#         local(  $machine_template_number,
#                         @machine_template_info,
#                         @setting_data_fill_list,
#                         $setting_data_fill_item,
#                         $setting_data_item_number,
#                         $database_record_number,
#                         $database_item_number,
#                         $database_item_separator,
#                         $number_of_items_to_return,
#                         $data_to_store);
#
#
#
#         ####################################################################################
#         # Collect data that will be needed to fill create global data lists
#         ####################################################################################
#         $machine_template_number = $MACHINE_INFO_BUFFER[1];
#         @machine_template_info = &avc_Database_Get_Keyed_Record(48,$machine_template_number);
#         @machine_type_info = &avc_Database_Get_Keyed_Record(10,$MACHINE_INFO_BUFFER[29]);
#         @promo_template_info = &avc_Database_Get_Keyed_Record(11,$MACHINE_INFO_BUFFER[4]);
#         @LOCATION_INFO_BUFFER = &avc_Database_Get_Keyed_Record(18,$MACHINE_INFO_BUFFER[6]);
#
#         ####################################################################################
#         # Set $TIME_ZONE in rix_os_ENV
#         ####################################################################################
#         $rix_os_ENV{"TIME_ZONE"} = $LOCATION_INFO_BUFFER[12];
#         $rix_os_ENV{"EXPECTED_DIALUP_TIME"} = $MACHINE_INFO_BUFFER[7];
#         $rix_os_ENV{"EXPECTED_DIALUP_DATE"} = $MACHINE_INFO_BUFFER[8];
#
#
#
# ########################################################################################
# #> Create @CCU_SETTING_DATA
# ########################################################################################
# #       @CCU_SETTING_DATA layout:
# #
# #       1       msgAuthBad
# #       2       msgAuthCard
# #       3       msgAuthOk
# #
# #       10      msgAvcPleaseWait
# #       11      msgAvcCloseBatch
# #
# #       20      msgCardInsert
# #       21      msgCardRemove
# #       22      msgCardCantRead
# #       23      msgCardWrongType
# #       24      msgCardInvalid
# #       25      msgCardMasterCode
# #
# #       30      msgInventoryErrorClose
# #       31      msgInventoryInit
# #       32      msgInventoryPleaseWait
# #       33      msgInventoryTryAgain
# #       34      msgInventoryUpdateComplete
# #       35      msgInventoryCode
# #
# #       42      msgReceiptPrinting
# #       43      msgReceiptQuestion
# #       44      msgReceiptTake
# #       45      msgReceiptNoPaper
# #
# #       50      msgSystemBusyPleaseWait
# #       51      msgSystemBusyTryAgain
# #       52      msgSystemFatalError
# #       53      msgSystemFlashInit
# #       54      msgSystemSelfTest
# #       55      msgSystemOutOfService
# #       56      msgSystemNoDialtone
# #       57      msgSystemPhonePrefix
# #
# #       60      msgVendCancel
# #       61      msgVendSelect
# #       62      msgVendLoser
# #       63      msgVendVending
# #
# #       70      Current Date and Time
# #               c70,checksum
# #               returns <current local date><space><current local time>,checksum
# #       71      Next Dialup Date and Time
# #               c71,checksum
# #               returns <dialup date><space><dialup time>,checksum
# #
# #       80      authorizationType
# #       82      probability
# #       83      printerUsed
# #       84      cancelUsed
# #       85      inventoryCardEnabled
# #       86      loserHasNoData
# #       87      initializeDatabase
# #       88      taxPercentage
# #       89      softwareDownloadNeeded
# #
# #
# #       90      question 1 information
# #               c90,checksum
# #               return <RIX Ack/Nak>,<question line 1>,<question line 2>,<min length>,<max length>,<question type>,checksum
# #       91      question 2 information
# #               c91,checksum
# #               return <RIX Ack/Nak>,<question line 1>,<question line 2>,<min length>,<max length>,<question type>,checksum
# #
# #       92	  new question 1 information
# #         	  <device id>,c92,checksum
# #               return <RIX Ack/Nak>,<question line 1>,<question line 2>,<min length>,<max length>,<question type>,<question function>,<timeout length>,<timeout action>,<default answer>,checksum
# #       93	  new question 2 information
# #               <device id>,c93,checksum
# #               return <RIX Ack/Nak>,<question line 1>,<question line 2>,<min length>,<max length>,<question type>,<question function>,<timeout length>,<timeout action>,<default answer>,checksum
# #	
# #       100     >Valid Prefixes
# #               c100,<valid prefix number (1-5)>,checksum
# #               return <valid prefix>,checksum
# #               NOTE: Assume will always read prefixes 1-5
# #
# #       110     promotionAd
# #       111     promotionName
# #
# #       120 FirstDataDeviceID
# #       121 FirstDataMerchantID
# #
# ########################################################################################
#
#         ####################################################################################
#         # List for creating @CCU_SETTING_DATA
#         ####################################################################################
#         @ccu_setting_data_fill_list = ( "1:48:14:|:2",
#                                                                         "2:48:15:|:2",
#                                                                         "3:48:16:|:2",
#                                                                         "10:48:24:|:2",
#                                                                         "11:48:25:|:2",
#                                                                         "20:48:34:|:2",
#                                                                         "21:48:35:|:2",
#                                                                         "22:48:36:|:2",
#                                                                         "23:48:37:|:2",
#                                                                         "24:48:38:|:2",
#                                                                         "25:48:39:|:1",
#                                                                         "30:48:44:|:2",
#                                                                         "31:48:45:|:2",
#                                                                         "32:48:46:|:2",
#                                                                         "33:48:47:|:2",
#                                                                         "34:48:48:|:2",
#                                                                         "35:48:49:|:1",
#                                                                         "42:48:56:|:2",
#                                                                         "43:48:57:|:2",
#                                                                         "44:48:58:|:2",
#                                                                         "45:48:59:|:2",
#                                                                         "50:48:64:|:2",
#                                                                         "51:48:65:|:2",
#                                                                         "52:48:66:|:2",
#                                                                         "53:48:67:|:2",
#                                                                         "54:48:68:|:2",
#                                                                         "55:48:69:|:2",
#                                                                         "56:48:70:|:2",
#                                                                         "57:48:71:|:2",
#                                                                         "60:48:74:|:2",
#                                                                         "61:48:75:|:2",
#                                                                         "62:48:76:|:2",
#                                                                         "63:48:77:|:2",
#                                                                         "80:48:3:|:1",
#                                                                         "81:48:120:|:1",
#                                                                         "83:48:9:|:1",
#                                                                         "84:48:10:|:1",
#                                                                         "85:48:11:|:1");
#
#         ####################################################################################
#         # Fill in @CCU_SETTING_DATA with the data requested in @setting_data_fill_list
#         ####################################################################################
#         foreach $ccu_setting_data_fill_item (@ccu_setting_data_fill_list)
#         {
#                 (       $setting_data_item_number,
#                         $database_record_number,
#                         $database_item_number,
#                         $database_item_separator,
#                         $number_of_items_to_return) = split(/:/,$ccu_setting_data_fill_item);
#
#                 # This is kind of stupid.
#                 # I couldn't get split(/\$database_item_separator/) working with a | seperator
#                 if ($database_item_separator eq "\|")
#                 {
#                         @database_item_split = split(/\|/,$machine_template_info[$database_item_number]);
#                 }
#                 else
#                 {
#                         @database_item_split = split(/$database_item_separator/,$machine_template_info[$database_item_number]);
#                 }
#
#                 $data_to_store = "";
#                 for ($data_item_counter = 0;$data_item_counter < $number_of_items_to_return;$data_item_counter++)
#                 {
#                         $data_to_store .= &rix_os_Fill_In_Message_Info($database_item_split[$data_item_counter]).$rix_os_ENV{"SEPARATOR"};
#                 }
#		chop($data_to_store = $data_to_store);
#
#
#
#                 $CCU_SETTING_DATA[$setting_data_item_number] = $data_to_store;
#         }
#
#         ####################################################################################
#         # Fill in @CCU_SETTING_DATA with other misc data that has to be hand extracted
#         ####################################################################################
#
#         ########################
#         #> Mark @CCU_SETTING_DATA inited
#         ########################
#         $CCU_SETTING_DATA[0] = "Inited";
#
#         ########################
#         #> One-use promotions
#         ########################
#
#         # the split SHOULD take place at the commas instead of the \0 character.
#         # @one_use_promotions = split(/\0/,$machine_template_info[5]);
#         @one_use_promotions = split(/,/,$machine_template_info[5]);
#
#         @valid_prefixes = ();
#         $probability = 0;
#         $loser_has_no_data = 0;
#         $init_daily = 0;
#
#         foreach $one_use_promotion_num (@one_use_promotions)
#         {
#
#
#                 @one_use_promotion_info = &avc_Database_Get_Keyed_Record(49,$one_use_promotion_num);
#                 $one_use_init_daily = $one_use_promotion_info[2];
#                 $one_use_probability = $one_use_promotion_info[4];
#                 $one_use_loser_has_no_data = $one_use_promotion_info[5];
#
#                 @one_use_prefixes = split(/\|/,$one_use_promotion_info[3]);
#
#                 foreach $one_use_prefix (@one_use_prefixes)
#                 {
#                         if (!grep (/$one_use_prefix/, @valid_prefixes))
#                         {
#                                 push(@valid_prefixes,$one_use_prefix);
#                         }
#                 }
#
#                 # Take highest probability
#                 if ($one_use_probability > $probability)
#                 {
#                         $probability = $one_use_probability;
#                 }
#
#                 # If one has init daily, all have init daily
#                 if ( ($one_use_init_daily == 1) && ($init_daily == 0) )
#                 {
#                         $init_daily = 1;
#                 }
#
#                 # If one loser has no data, all losers have no data
#                 if ( ($one_use_loser_has_no_data == 1) && ($loser_has_no_data == 0) )
#                 {
#                         $loser_has_no_data = 1;
#                 }
#         }
#
#         if ($probability == 0)
#         {
#                 $probability = 100;
#         }
#
#         $CCU_SETTING_DATA[82] = $probability;
#         $CCU_SETTING_DATA[86] = $loser_has_no_data;
#         $CCU_SETTING_DATA[87] = $init_daily;
#
#         $valid_prefix_data = "";
#         foreach $valid_one_use_prefix (@valid_prefixes)
#         {
#                 $valid_prefix_data .= $valid_one_use_prefix.$rix_os_ENV{"SEPARATOR"};
#         }
#	chop($valid_prefix_data = $valid_prefix_data);
#
#
#         $CCU_SETTING_DATA[100] = $valid_prefix_data;
#
#         ########################
#         #> Is init database flag set?
#         ########################
#         if ($MACHINE_INFO_BUFFER[30])
#         {
#                 $CCU_SETTING_DATA[87] = 1;
#         }
#         ########################
#         #> Tax Percentage
#         ########################
#         if ($machine_template_info[119])
#         {  # tax included in price...then, tax %age = 0.00
#            $CCU_SETTING_DATA[88] = 0.00;
#         }
#         else
#         {
#            $CCU_SETTING_DATA[88] = $LOCATION_INFO_BUFFER[11];
#         }
#
#         ########################
#         #> Download new software?
#         ########################
#         $CCU_SETTING_DATA[89] = $MACHINE_INFO_BUFFER[31];
#
#
#         ########################
#         #> Display Ad & Promo Name
#         ########################
#         $promotion_name = &rix_os_Fill_In_Message_Info($promo_template_info[1]);
#         $promotion_ad = &rix_os_Fill_In_Message_Info($promo_template_info[2]).$rix_os_ENV{"SEPARATOR"}.&rix_os_Fill_In_Message_Info($promo_template_info[3]);
#         $CCU_SETTING_DATA[110] = $promotion_ad;
#         $CCU_SETTING_DATA[111] = $promotion_name;
#
#         ########################
#         #> Find Next Dialup Date & Time and save it in @MACHINE_INFO_BUFFER
#         ########################
#
#         # Update current local date and time in @CCU_SETTING_DATA
#         &rix_os_Update_Ccu_Setting_Date_And_Time();
#
#         ($server_next_dialup_date,$server_next_dialup_time) = &rix_os_Calculate_Next_Dialup();
#
#         $MACHINE_INFO_BUFFER[8] = $server_next_dialup_date;
#
#         # Update machine info
#         &rix_os_Update_Machine_Info();
#
#         ($local_next_dialup_date,$local_next_dialup_time) = &rix_os_Make_Local_Date_And_Time($server_next_dialup_date,$server_next_dialup_time);
#
#         $CCU_SETTING_DATA[71] = $local_next_dialup_date." ".$local_next_dialup_time;
#
#         ########################
#         #> Question Data
#         ########################
#         $question1_number = $machine_template_info[84];
#         $question2_number = $machine_template_info[85];
#
#         @question1_info = &avc_Database_Get_Keyed_Record(58,$question1_number);
#         @question2_info = &avc_Database_Get_Keyed_Record(58,$question2_number);
#
#         $question1_data = $question1_info[1].$rix_os_ENV{"SEPARATOR"}.$question1_info[2].$rix_os_ENV{"SEPARATOR"}.$question1_info[3].$rix_os_ENV{"SEPARATOR"}.$question1_info[4].$rix_os_ENV{"SEPARATOR"}.$question1_info[5].$rix_os_ENV{"SEPARATOR"}.$question1_info[6].$rix_os_ENV{"SEPARATOR"}.$question1_info[7].$rix_os_ENV{"SEPARATOR"}.$question1_info[8];
#         $question2_data = $question2_info[1].$rix_os_ENV{"SEPARATOR"}.$question2_info[2].$rix_os_ENV{"SEPARATOR"}.$question2_info[3].$rix_os_ENV{"SEPARATOR"}.$question2_info[4].$rix_os_ENV{"SEPARATOR"}.$question2_info[5].$rix_os_ENV{"SEPARATOR"}.$question2_info[6].$rix_os_ENV{"SEPARATOR"}.$question2_info[7].$rix_os_ENV{"SEPARATOR"}.$question2_info[8];
#
#         $CCU_SETTING_DATA[90] = $question1_data;
#         $CCU_SETTING_DATA[91] = $question2_data;
#
#         ########################
#         #> Update rix_os_ENV for question info
#         ########################
#         $rix_os_ENV{"QUESTION1_NUMBER"} = $question1_number;
#         $rix_os_ENV{"QUESTION2_NUMBER"} = $question2_number;
#
#         ########################
#         #> Set First Data Device ID
#         ########################
#         $CCU_SETTING_DATA[120] = $rix_os_ENV{"MACHINE_ID"};
#         $CCU_SETTING_DATA[121] = $rix_os_ENV{"MERCHANT_ID"};
#
#         ########################
#         #> FDC & RIX phone number info
#         ########################
#         $CCU_SETTING_DATA[130] = $machine_template_info[7];
#         $CCU_SETTING_DATA[131] = $machine_template_info[8];
#
#
# ########################################################################################
# #> Create @VMC_SETTING_DATA
# ########################################################################################
# #       @CCU_SETTING_DATA layout:
# #
# #       1       numOfColumns
# #       2       columnIndexOrigin
# #       3       >columnName:
# #               v3,<column number>,checksum
# #               returns <string>,checksuum
# #       4       >columnPrice:
# #               v4,<column number>,checksum
# #               return <integer>,checksum
# #       5       >columnInventoryId:
# #               v5,<column number>,checksum
# #               return <string>,checksum
# #
# ########################################################################################
#
#         ###################################################################################
#         # Fill in @VMC_SETTING_DATA with other misc data that has to be hand extracted
#         ####################################################################################
#
#         ########################
#         #> Mark @VMC_SETTING_DATA inited
#         ########################
#         $VMC_SETTING_DATA[0] = "Inited";
#
#         ########################
#         #> 1    numOfColumns
#         ########################
#         $number_of_columns = $machine_type_info[10];
#         $VMC_SETTING_DATA[1] = $number_of_columns;
#
#         ########################
#         #> 2    columnIndexOrigin
#         ########################
#         $VMC_SETTING_DATA[2] = $machine_type_info[11];
#
#         ########################
#         #> 3    columnName
#         #> 4    columnPrice
#         #> 5    columnInventoryId
#         ########################
#         %inventory_item_name_buffer = ();
#         $column_name_list = "";
#         $column_price_list = "";
#         $column_inventory_id_list = "";
#
#         $needs_max_price_override = $machine_type_info[2];
#
#         for ($column_num = 1;$column_num <= $number_of_columns;$column_num++)
#         {
#                 $curr_column_inventory_id = $promo_template_info[(44+$column_num-1)];
#
#                 if ($curr_column_inventory_id ne "")
#                 {
#                         if ($inventory_item_name_buffer{$curr_column_inventory_id} eq "")
#                         {
#                                 # Fill %inventory_info_buffer
#                                 @inventory_item_info = &avc_Database_Get_Keyed_Record(24,$curr_column_inventory_id);
#                                 $inventory_item_name_buffer{$curr_column_inventory_id} = $inventory_item_info[1];
#                         }
#
#                         $curr_column_name = $inventory_item_name_buffer{$curr_column_inventory_id};
#                 }
#                 else
#                 {
#                         $curr_column_name = "ITEM ".$column_num;
#                 }
#
#                 # Does it need max price override?
#                 if ($needs_max_price_override)
#                 {
#                         $curr_column_price = $promo_template_info[(14+$column_num-1)]*100; # assuming that the prices are set in dollar format on server
#                 }
#                 else
#                 {
#                         $curr_column_price = "";
#                 }
#
#                 $column_name_list .= $curr_column_name.$rix_os_ENV{"SEPARATOR"};
#                 $column_price_list .= $curr_column_price.$rix_os_ENV{"SEPARATOR"};
#                 $column_inventory_id_list .= $curr_column_inventory_id.$rix_os_ENV{"SEPARATOR"};
#         }
#
#	chop($column_name_list = $column_name_list);
#	chop($column_price_list = $column_price_list);
#	chop($column_inventory_id_list = $column_inventory_id_list);
#
#         $VMC_SETTING_DATA[3] = $column_name_list;
#         $VMC_SETTING_DATA[4] = $column_price_list;
#         $VMC_SETTING_DATA[5] = $column_inventory_id_list;
#
# ########################################################################################
# #> Create @SWIPE_SETTING_DATA
# ########################################################################################
# #       @SWIPE_SETTING_DATA layout:
# #
# #       1       trackNumber
# #       2       pinEnabled
# #
# ########################################################################################
#
#         ########################
#         #> Mark @SWIPE_SETTING_DATA inited
#         ########################
#         $SWIPE_SETTING_DATA[0] = "Inited";
#
#         ########################
#         #> 1    trackNumber
#         ########################
#         $SWIPE_SETTING_DATA[1] = $machine_template_info[2];
#
#         ########################
#         #> 2    pinEnabled (unused)
#         ########################
#         $SWIPE_SETTING_DATA[2] = 0;
#
# ########################################################################################
# #> Create @RECEIPT_SETTING_DATA
# ########################################################################################
# #       @RECEIPT_SETTING_DATA layout:
# #
# #       1       parReceiptAskQuestion
# #       2       parReceiptLoser
# #       3       receiptCoupon
# #       4       receiptInfo
# #
# ########################################################################################
#
#         ########################
#         #> Mark @RECEIPT_SETTING_DATA inited
#         ########################
#         $RECEIPT_SETTING_DATA[0] = "Inited";
#
#         ########################
#         #> 1    parReceiptAskQuestion
#         ########################
#         $RECEIPT_SETTING_DATA[1] = $machine_template_info[114];
#
#         ########################
#         #> 2    parReceiptLoser
#         ########################
#         $RECEIPT_SETTING_DATA[2] = $machine_template_info[115];
#
#         ########################
#         #> 3    receiptCoupon
#         ########################
#         $receipt_coupon_data = "";
#         @receipt_coupon_list = split(/\n/,$machine_template_info[54]);
#         foreach $receipt_coupon_item (@receipt_coupon_list)
#         {
#                 $receipt_coupon_item_adjusted = &rix_os_Fill_In_Message_Info($receipt_coupon_item);
#                 $receipt_coupon_data .= $receipt_coupon_item_adjusted.$rix_os_ENV{"SEPARATOR"};
#         }
#
#	chop( $receipt_coupon_data =  $receipt_coupon_data);
#
#         $RECEIPT_SETTING_DATA[3] = $receipt_coupon_data;
#
#         ########################
#         #> 4    receiptInfo
#         ########################
#         $receipt_info_data = "";
#         @receipt_info_list = split(/\|/,$machine_template_info[55]);
#         foreach $receipt_info_item (@receipt_info_list)
#         {
#                 $receipt_info_item_adjusted = &rix_os_Fill_In_Message_Info($receipt_info_item);
#                 $receipt_info_data .= $receipt_info_item_adjusted.$rix_os_ENV{"SEPARATOR"};
#         }
#
#	chop($receipt_info_data = $receipt_info_data);
#
#         $RECEIPT_SETTING_DATA[4] = $receipt_info_data;
#
#
#         ####################################################################################
#         # Init @CCU_STATUS_BUFFER and %CCU_STATUS_RECORD_LAYOUT
#         ####################################################################################
#         @CCU_STATUS_BUFFER = ();
#         # $curr_local_date, $curr_local_time, $expected_dialup_date & $expected_dialup_time
#         # were created when filling in CCU_SETTING_DATA.
#         # Re-use them here:
#         $CCU_STATUS_BUFFER[0] = &avc_Database_Get_Record_Number(51,$rix_os_ENV{"MACHINE_ID"});
#
#         ($curr_local_date,$curr_local_time) = split(/ /,$CCU_SETTING_DATA[70]);
#
#         $CCU_STATUS_BUFFER[1] = $curr_local_date;
#         $CCU_STATUS_BUFFER[2] = $curr_local_time;
#         $CCU_STATUS_BUFFER[3] = $rix_os_ENV{"EXPECTED_DIALUP_DATE"};
#         $CCU_STATUS_BUFFER[4] = $rix_os_ENV{"EXPECTED_DIALUP_TIME"};
#         $CCU_STATUS_BUFFER[5] = $LOCATION_INFO_BUFFER[0];
#         $CCU_STATUS_BUFFER[6] = $MACHINE_INFO_BUFFER[1];
#         $CCU_STATUS_BUFFER[7] = $MACHINE_INFO_BUFFER[33];
#
#         @tmp_ccu_status_record_layout = (       "1:1","10",
#                                                                                 "1:2","11",
#                                                                                 "1:3","12",
#                                                                                 "1:4","13",
#                                                                                 "1:5","14",
#                                                                                 "1:6","15",
#                                                                                 "1:7","16",
#                                                                                 "1:8","17",
#                                                                                 "1:9","18",
#                                                                                 "1:10","19",
#                                                                                 "1:11","9",
#                                                                                 "1:12","24",
#                                                                                 "2:1","20",
#                                                                                 "2:2","21",
#                                                                                 "2:3","8");
#
#         %CCU_STATUS_RECORD_LAYOUT = @tmp_ccu_status_record_layout;
#
# }



########################################
#
#
#
# ########################################
# #
# # rix_os_Init_Transaction_Authorize_Data_Lists
# #
# ########################################
# # Description: Fill global data lists
# #
# # Uses Globals: %rix_os_ENV,@LOCATION_INFO_BUFFER, @MACHINE_INFO_BUFFER, @machine_template_info,
# #               @one_use_promotions, @CCU_SETTING_DATA
# #
# # Written By: Salim Khan
# # Created: 10/27/1999
# #
# #  DATE        NAME          CHANGES
# # ---------   ------------  ---------------------------------------
# #
# ########################################
# sub rix_os_Init_Transaction_Authorize_Data_Lists
# {
#         local(          @valid_prefixes,
#                         $probability,
#                         $loser_has_no_data,
#                         $init_daily,
#                         $one_use_init_daily,
#                         $one_use_probability,
#                         $one_use_loser_has_no_data,
#                         @one_use_prefixes,
#                         $one_use_prefix,
#                         $valid_one_use_prefix,
#                         $valid_prefix_data
#
#                                         );
#
#         $machine_template_number = $MACHINE_INFO_BUFFER[1];
#         @machine_template_info = &avc_Database_Get_Keyed_Record(48,$machine_template_number);
#
#         @LOCATION_INFO_BUFFER = &avc_Database_Get_Keyed_Record(18,$MACHINE_INFO_BUFFER[6]);
#         $location_number = $LOCATION_INFO_BUFFER[0];
#
#         ########################
#         #> One-use promotions
#         ########################
#         @one_use_promotions = split(/\0/,$machine_template_info[5]);
#
#         @valid_prefixes = ();
#         $server_probability = 0;
#
#         foreach $one_use_promotion_num (@one_use_promotions)
#         {
#                 @one_use_promotion_info = &avc_Database_Get_Keyed_Record(49,$one_use_promotion_num);
#                 $one_use_server_probability = $one_use_promotion_info[6];
#
#                 @one_use_prefixes = split(/\|/,$one_use_promotion_info[3]);
#
#                 foreach $one_use_prefix (@one_use_prefixes)
#                 {
#                         if (!grep (/$one_use_prefix/, @valid_prefixes))
#                         {
#                                 push(@valid_prefixes,$one_use_prefix);
#                         }
#                 }
#
#                 # Take highest probability
#                 if ($one_use_server_probability > $server_probability)
#                 {
#                         $server_probability = $one_use_server_probability;
#                 }
#         }
#
#         $valid_prefix_data = "";
#         foreach $valid_one_use_prefix (@valid_prefixes)
#         {
#                 $valid_prefix_data .= $valid_one_use_prefix.$rix_os_ENV{"SEPARATOR"};
#         }
#	chop($valid_prefix_data = $valid_prefix_data);
#
#         $CCU_SETTING_DATA[100] = $valid_prefix_data;
#
#         # Update current local date and time in @CCU_SETTING_DATA
#         &rix_os_Update_Ccu_Setting_Date_And_Time(); # $CCU_SETTING_DATA[70]
#
#         # Update server probability
#         $rix_os_ENV{"SERVER PROBABILITY"} = $server_probability;
#
#         $rix_transaction_ENV{"MASTERCODE"} = $machine_template_info[39];
# }

sub rix_os_Get_Next_Id
{
   local (  $LOCK_SH,
            $LOCK_EX,
            $LOCK_NB,
            $LOCK_UN,
            $id_num_file, 
            $next_id, 
            $new_next_id, 
            $id_length );

   ($id_num_file, $id_length)  = @_;

   $LOCK_SH = 1; # shared lock
   $LOCK_EX = 2; # exclusive lock 
   $LOCK_NB = 4; # non-blocking ORed with shared or exclusive locking
   $LOCK_UN = 8; # unlock


   if ($id_length eq "")
   {
      $id_length = 1; # default MAX value = 10
   }
   
   flock FILE, $LOCK_EX; # lock file access
   
   open FILE, "+<$id_num_file" or die "can't open $id_num_file\n";

   seek FILE, 0, 0; 
   read FILE,  $next_id, $id_length; 

   if ($next_id eq "") 
   {
      $next_id = 0;

      seek FILE, 0, 0; 
      print FILE $next_id;
   }

   $new_next_id =  $next_id + 1;
   if (length($new_next_id) > $id_length) 
   {
      $new_next_id = 0;
   }
      
   seek FILE, 0, 0; 
   print FILE $new_next_id;

   close FILE;

   flock FILE, $LOCK_UN; # unlock file access
   
   return  $next_id;
}

# sub rix_os_Read_Record_In_File
# {
#    local ( $curr_filename, $record_num, $record_length, $return_record );
#
#    ( $curr_filename, $record_num, $record_length  ) = @_;
#
#    open(DATABASE,"+<$curr_filename");
#    seek(DATABASE,($record_num*$record_length),0);
#    read(DATABASE,$return_record,$record_length);
#    close(DATABASE);
#
#    return $return_record;
# }
#
# sub rix_os_Update_Record_In_File
# {
#    local ( $curr_filename, $record_num, $record_length, $record, $return_record );
#
#    ( $curr_filename, $record_num, $record_length, $record ) = @_;
#
#    open(DATABASE,"+<$curr_filename");
#    seek(DATABASE,($record_num*$record_length),0);
#
#    if (length($record) <= $record_length)
#    {
#       print DATABASE $record;
#    }
#
#    close(DATABASE);
# }

########################################
#
# rix_os_Read_Record_In_File
#
########################################
# Description: Returns a record from a file specified by record number.
#
# Uses Globals: 
#
# Written By: Salim Khan
# Created: 10/27/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Read_Record_In_File
{
   local ( $curr_filename, $record_num, $record_length, $return_record ); 

   ( $curr_filename, $record_num, $record_length  ) = @_;

   open(DATABASE,"+<$curr_filename");
   seek(DATABASE,($record_num*$record_length),0);
   read(DATABASE,$return_record,$record_length);
   close(DATABASE);

   return $return_record;
}



########################################
#
# rix_os_Update_Record_In_File
#
########################################
# Description: Writes a record from a file specified by record number.
#
# Uses Globals: 
#
# Written By: Salim Khan
# Created: 10/27/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 12/27/99    SAK           added space padding to writes that dont fill the entire field
# 
########################################
sub rix_os_Update_Record_In_File
{
   local ( $curr_filename, $record_num, $record_length, $record, $return_record ); 

   ( $curr_filename, $record_num, $record_length, $record ) = @_;

   open(DATABASE,"+<$curr_filename");
   seek(DATABASE,($record_num*$record_length),0);

   if (length($record) < $record_length)
   {
      print DATABASE &pad_String( $record, 
                                  $record_length - length($record));
   }
   elsif (length($record) == $record_length)
   {
      print DATABASE $record;
   }

   close(DATABASE);
}


########################################
#
# rix_os_Calculate_Next_Dialup
#
########################################
# Description: Calculate the next dialup date and time
#
# Uses Globals: @CCU_SETTING_DATA, @MACHINE_INFO_BUFFER
#
# Written By: Scott Wasserman
# Created: 10/5/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 02/07/2000   SAK         added two dial-ins/day mechanism for Killington machine(0014)
########################################
sub rix_os_Calculate_Next_Dialup
{
	local(	$next_dialup_date,
			$next_dialup_time,
			$curr_local_date,
			$curr_local_time,
                        $num,
                        $second_num,
                        $secondary_dialup_time,
                        $secondary_before );
			
	#($curr_local_date,$curr_local_time) = split(/ /,$CCU_SETTING_DATA[70]);
	
        $curr_server_date = &get_Date();

        $curr_server_time = &get_Time();

        	warn("TIME DEBUG:  in calc, next dialup time: $MACHINE_INFO_BUFFER[1]\n");                                
	$next_dialup_time = $MACHINE_INFO_BUFFER[7];

        if (($rix_os_ENV{"MACHINE_ID"} eq "0014") ||
            ($rix_os_ENV{"MACHINE_ID"} eq "GDV3") ) # testing double dialins for e-Vend Soda machine, Killington machine
        {
           $num = &time_To_Num($next_dialup_time);

           if ( ($num-1200) < 0 )
           {
              $second_num = $num+1200;
   
              $secondary_dialup_time = &time_Fix(substr($second_num,0,2).":".substr($second_num,2,2));
   
              $secondary_before = 0;
           }
           else
           {
              $second_num = $num-1200;
   
              if (length($second_num)==3)
              {
                 $secondary_dialup_time = "0".substr($second_num,0,1).":".substr($second_num,1,2);
   
              }
              else
              {
                 $secondary_dialup_time = substr($second_num,0,2).":".substr($second_num,2,2);
              }
              
              $secondary_before = 1;
           }
           if ($secondary_before)
           {
              if ( (&avc_Date_eq($rix_os_ENV{"EXPECTED_DIALUP_DATE"},$curr_server_date)) &&
   	        (&avc_Time_lt($curr_server_time,$secondary_dialup_time)) )
              {
                 $next_dialup_date = $curr_server_date;
                 $next_dialup_time = $secondary_dialup_time;
              }
              elsif ( (&avc_Date_eq($rix_os_ENV{"EXPECTED_DIALUP_DATE"},$curr_server_date)) &&
   	           (&avc_Time_lt($curr_server_time,$next_dialup_time)) )
              {
                 $next_dialup_date = $curr_server_date;
                 # $next_dialup_time = $next_dialup_time; - done implicitly
              }
              elsif ( (&avc_Date_eq($rix_os_ENV{"EXPECTED_DIALUP_DATE"},$curr_server_date)) )  # assuming that this catches dialups on the same day itself
              {
                 $next_dialup_date = &avc_Database_Increment_Date($curr_server_date,$oracle);
                 $next_dialup_time = $secondary_dialup_time;
              }
              else
              {
                 $next_dialup_date = &avc_Database_Increment_Date($curr_server_date,$oracle);
                 $next_dialup_time = $secondary_dialup_time;
              }
           }
           else
           {
              if ( (&avc_Date_eq($rix_os_ENV{"EXPECTED_DIALUP_DATE"},$curr_server_date)) &&
   	        (&avc_Time_lt($curr_server_time,$next_dialup_time)) )
              {
                 $next_dialup_date = $curr_server_date;
                 # $next_dialup_time = $next_dialup_time; - done implicitly
              }
              elsif ( (&avc_Date_eq($rix_os_ENV{"EXPECTED_DIALUP_DATE"},$curr_server_date)) &&
   	           (&avc_Time_lt($curr_server_time,$secondary_dialup_time)) )
              {
   
                 $next_dialup_date = $curr_server_date;
                 $next_dialup_time = $secondary_dialup_time;
              }
              elsif ( (&avc_Date_eq($rix_os_ENV{"EXPECTED_DIALUP_DATE"},$curr_server_date)) )  # assuming that this catches dialups on the same day itself
              {
   
                 $next_dialup_date = &avc_Database_Increment_Date($curr_server_date,$oracle);
                 # $next_dialup_time = $next_dialup_time; - done implicitly
              }
              else
              {
                 $next_dialup_date = &avc_Database_Increment_Date($curr_server_date,$oracle);
                 # $next_dialup_time = $next_dialup_time; - done implicitly
              }
           }
        }
        else
        {
           # $expected_dialup_date changed to $rix_os_ENV{"EXPECTED_DIALUP_DATE"}
	warn("TIME DEBUG: in this big else\n");
           if ( (&avc_Date_eq($rix_os_ENV{"EXPECTED_DIALUP_DATE"},$curr_server_date)) &&
                    (&avc_Time_lt($curr_server_time,$next_dialup_time)) )
           {
                   $next_dialup_date = $curr_server_date;
			warn("TIME DEBUG: in if\n");
           }
           else
           {
                   $next_dialup_date = &avc_Database_Increment_Date($curr_server_date,$oracle);
		warn("TIME DEBUG: in else\n");
           }
        }
	
        
	return ($next_dialup_date,$next_dialup_time);
}

########################################
#
# rix_os_Fill_In_Receipt_Info
#
########################################
# Description: Fill in macro info on receipt data
#
# Uses Globals: @LOCATION_INFO_BUFFER
#
# NOTE: If addresses migrate to a rolodex in the location record we must update this!
#
# Written By: Scott Wasserman
# Created: 9/23/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
########################################
sub rix_os_Fill_In_Message_Info
{
	local(	$curr_message,
			$curr_data,
			$line_feed,
			$return);
			
	$curr_message = $_[0];
	
	$line_feed = sprintf("%c",10);
	$return = sprintf("%c",13);
				
	$curr_message =~ s/$line_feed//gi;
	$curr_message =~ s/$return//gi;
	$curr_message =~ s/\~/ /gi;
	
	if ($curr_message =~ /\^/)
	{
		if ($curr_message =~ /loc_name/)
		{
			$curr_data = $LOCATION_INFO_BUFFER[1];
			$curr_message =~ s/\^loc_name/$curr_data/gi;
		}
		elsif ($curr_message =~ /loc_city_state/)
		{
			$curr_data = $LOCATION_INFO_BUFFER[5].", ".$LOCATION_INFO_BUFFER[6];
			$curr_message =~ s/\^loc_city_state/$curr_data/gi;
		}
		elsif ($curr_message =~ /phone_number/)
		{
			$curr_data = $LOCATION_INFO_BUFFER[9];
			$curr_message =~ s/\^phone_number/$curr_data/gi;
		}
	}
	
	return $curr_message

}

########################################
#
# rix_os_Get_Local_Date_And_Time
#
########################################
# Description: Return current date at current location and time zone in the following formats:
#				Date: 00/00/0000
#				Time: 00:00
#
# Uses Globals: $rix_os_ENV
#
# Written By: Scott Wasserman
# Created: 9/21/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
########################################
sub rix_os_Get_Local_Date_And_Time
{
	local(	$curr_local_date,
			$curr_local_time,
			@curr_local_time_list,
			$curr_local_hour,
			$adjusted_hour,
			$adjusted_time,
			$adjusted_date);
		
	$curr_local_date = &get_Date();	
	$curr_local_time = &get_Time();
	@curr_local_time_list = split(/:/,$curr_local_time);
	$curr_local_hour = $curr_local_time_list[0];
	$adjusted_hour = $curr_local_hour-$rix_os_ENV{"TIME_ZONE"};
	
	if ($adjusted_hour < 0)
	{
		$adjusted_hour += 24;
		$adjusted_time = $adjusted_hour.":".$curr_local_time_list[1];
		$adjusted_date = &avc_Database_Decrement_Date($curr_local_date,$oracle);
	}
        elsif ($adjusted_hour >= 24)
        {
                $adjusted_hour -= 24;
		$adjusted_time = $adjusted_hour.":".$curr_local_time_list[1];
		$adjusted_date = &avc_Database_Increment_Date($curr_local_date,$oracle);
        }
	else
	{
		$adjusted_time = $adjusted_hour.":".$curr_local_time_list[1];
		$adjusted_date = $curr_local_date;
	}
	
	return ($adjusted_date,$adjusted_time);
}



########################################
#
# rix_os_Make_Local_Date_And_Time
#
########################################
# Description: Return current date at current location and time zone in the following formats:
#				Date: 00/00/0000
#				Time: 00:00
#
# Uses Globals: $rix_os_ENV
#
# Written By: Scott Wasserman
# Created: 9/21/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
########################################
sub rix_os_Make_Local_Date_And_Time
{
	local(	$server_date,
                $server_time,
                $server_hour,
                @server_time_list,
                $adjusted_hour,
                $local_time,
                $local_date );

        ($server_date, $server_time) = @_;

	$server_time = '00:00' if !$server_time;

	warn("DISPLAY:: $server_date, $server_time\n");
	
        @server_time_list = split(/:/,$server_time);
	$server_hour = $server_time_list[0];
	$adjusted_hour = $server_hour-$rix_os_ENV{"TIME_ZONE"};
	
	if ($adjusted_hour < 0)
	{
		$adjusted_hour += 24;
		$local_time = $adjusted_hour.":".$server_time_list[1];
		$local_date = &avc_Database_Decrement_Date($server_date,$oracle);
	}
        elsif ($adjusted_hour >= 24)
        {
                $adjusted_hour -= 24;
		$local_time = $adjusted_hour.":".$server_time_list[1];
		$local_date = &avc_Database_Increment_Date($server_date,$oracle);
        }
	else
	{
		$local_time = $adjusted_hour.":".$server_time_list[1];
		$local_date = $server_date;
	}

	warn ("DISPLAY::: $local_date,$local_time\n");
	return ($local_date,$local_time);
}

########################################
#
# rix_Init_Session
#
########################################
# Description: Init the globals for a RIX session is machine info is availible
#
# Uses Globals:@MACHINE_INFO_BUFFER, %rix_os_ENV
# 
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_os_Init_Session
{	
	local(	$device_id_check,
			@device_id_record,
			$machine_id_check,
			$machine_info_check);
	
	$device_id_check = $_[0];

	# Get Machine ID
	$midref = $oracle->select(	table => 'RIX_Device_ID',
				select_columns => 'Machine_ID',
				where_columns => ['Device_ID = ?'],
				where_values => [$device_id_check],
			);
	$machine_id_check = $midref->[0][0];

	# Get Machine Info
	my $micref = $oracle->select(
					table => 'RIX_MACHINE',
					select_columns => 'MACHINE_ID, MACHINE_TEMPLATE_NUMBER, DEVICE_ID, MERCHANT_ID, PROMOTION_TEMPLATE, IN_USE, LOCATION_NUMBER, DIALUP_TIME, DIALUP_DATE, UNUSED_FIELD, TRACE_ON, DEBUG_ON, UNUSED_FIELD2, UNUSED_FIELD3, UNUSED_FIELD4, UNUSED_FIELD5, SERIAL_NUMBER, KEY_NUMBER, PHONE_NUMBER, COUNTER_HOLDER, AUTO_AUTH, SPECIAL_NOTES, UNPLUGGED_NIGHTLY, MACHINE_RATING, SOFTWARE_VERSION, UNUSED_FIELD6, UNUSED_FIELD7, UNUSED_FIELD8, UNUSED_FIELD9, MACHINE_TYPE_NUMBER, INITIALIZE_FLASH_DAILY, UPDATE_SOFTWARE, MINUTES_UNTIL_NEXT_DIALUP, CURRENT_SOFTWARE, SEND_VMC_COLUMN_INFO, SEND_SALES_TAX, SEND_DEX_SESSION, SEND_QUESTIONS, SEND_MESSAGES, SEND_OTHER, INSTALL_DATE',
					where_columns => ['Machine_ID = ?'],
					where_values => [$machine_id_check],
				);
	 @machine_info_check = @{$micref->[0]};

	$machine_info_check[5] = reformat_enums($machine_info_check[5]);
	$machine_info_check[10] = reformat_enums($machine_info_check[10]);
	$machine_info_check[11] = reformat_enums($machine_info_check[11]);
	$machine_info_check[20] = reformat_enums($machine_info_check[20]);
	$machine_info_check[22] = reformat_enums($machine_info_check[22]);
	$machine_info_check[30] = reformat_enums($machine_info_check[30]);
	$machine_info_check[31] = reformat_enums($machine_info_check[31]);
	$machine_info_check[34] = reformat_enums($machine_info_check[34]);
	$machine_info_check[35] = reformat_enums($machine_info_check[35]);
	$machine_info_check[36] = reformat_enums($machine_info_check[36]);
	$machine_info_check[37] = reformat_enums($machine_info_check[37]);
	$machine_info_check[38] = reformat_enums($machine_info_check[38]);
	$machine_info_check[39] = reformat_enums($machine_info_check[39]);

	

	if (scalar(@machine_info_check) > 0)
	{
		$rix_os_ENV{"DEVICE_ID"} = $device_id_check;
		$rix_os_ENV{"MACHINE_ID"} = $machine_id_check;
		$rix_os_ENV{"SESSION_START_DATE"} = &get_Date();
		$rix_os_ENV{"SESSION_START_TIME"} = &get_Time();
		$rix_os_ENV{"TRACE_ON"} = $machine_info_check[10];
		$rix_os_ENV{"MERCHANT_ID"} = $machine_info_check[3];
                $rix_os_ENV{"DEX_IN_PROGRESS"} = 0;
                $rix_os_ENV{"DEX_COMMANDS"} = ();    # not sure about this
		
                # If no merchant id is set, load default
		if ($rix_os_ENV{"MERCHANT_ID"} eq "")
		{
			$rix_os_ENV{"MERCHANT_ID"} = $DEFAULT_MERCHANT_ID;
		}

#                 if ($rix_os_ENV{"MACHINE_ID"} eq "GDV2")
#                 {
#                    require("avcbridge_database_query-lib2.pl");
#
#                    # ideally, the below needs to be there. but, the client times out, so its been
#                    # temporarily disabled.
#
#                    # Get Machine Info
#                    @machine_info_check = &avc_Database_Get_Keyed_Record(1,$machine_id_check);
#
#                    $rix_os_ENV{"TRACE_ON"} = $machine_info_check[10];
#                    $rix_os_ENV{"MERCHANT_ID"} = $machine_info_check[3];
#                    # If no merchant id is set, load default
#                    if ($rix_os_ENV{"MERCHANT_ID"} eq "")
#                    {
#                            $rix_os_ENV{"MERCHANT_ID"} = $DEFAULT_MERCHANT_ID;
#                    }
#                 }
		
		
		@MACHINE_INFO_BUFFER = @machine_info_check;

                #&dump_List(@MACHINE_INFO_BUFFER);
		
		# Run watcher
		&rix_os_Watcher();
		
		return 1;
	}
	else
	{
		return 0;
	}	
}

########################################
#
# rix_os_Relogin_Current_Machine
#
########################################
# Description: Reinits current machine info and data list after machine
#				or promo update
#
# Uses Globals: 
# 
# Written By: Scott Wasserman
# Created: 9/28/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Relogin_Current_Machine
{
	&rix_os_Init_Standard_Login_Data();
}

########################################
#
# rix_os_Watcher
#
########################################
# Description: Watches machine activity and warns of problems
#
# Uses Globals: 
# 
# Written By: Scott Wasserman
# Created: 9/27/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Watcher
{



}

########################################
#
# rix_os_Update_Machine_Info
#
########################################
# Description: Updates machine info with the current info in @MACHINE_INFO_BUFFER 
#
# Uses Globals:@MACHINE_INFO_BUFFER
# 
# Written By: Scott Wasserman
# Created: 9/21/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Update_Machine_Info
{

       $MACHINE_INFO_BUFFER[5] = reformat_enums($MACHINE_INFO_BUFFER[5]);
        $MACHINE_INFO_BUFFER[10] = reformat_enums($MACHINE_INFO_BUFFER[10]);
        $MACHINE_INFO_BUFFER[11] = reformat_enums($MACHINE_INFO_BUFFER[11]);
        $MACHINE_INFO_BUFFER[20] = reformat_enums($MACHINE_INFO_BUFFER[20]);
        $MACHINE_INFO_BUFFER[22] = reformat_enums($MACHINE_INFO_BUFFER[22]);
        $MACHINE_INFO_BUFFER[30] = reformat_enums($MACHINE_INFO_BUFFER[30]);
        $MACHINE_INFO_BUFFER[31] = reformat_enums($MACHINE_INFO_BUFFER[31]);
        $MACHINE_INFO_BUFFER[34] = reformat_enums($MACHINE_INFO_BUFFER[34]);
        $MACHINE_INFO_BUFFER[35] = reformat_enums($MACHINE_INFO_BUFFER[35]);
        $MACHINE_INFO_BUFFER[36] = reformat_enums($MACHINE_INFO_BUFFER[36]);
        $MACHINE_INFO_BUFFER[37] = reformat_enums($MACHINE_INFO_BUFFER[37]);
        $MACHINE_INFO_BUFFER[38] = reformat_enums($MACHINE_INFO_BUFFER[38]);
        $MACHINE_INFO_BUFFER[39] = reformat_enums($MACHINE_INFO_BUFFER[39]);

#$MACHINE_INFO_BUFFER[8] = mysql_date_format($MACHINE_INFO_BUFFER[8]);


# set to y/n's before update

my $date_time = "$MACHINE_INFO_BUFFER[8] $MACHINE_INFO_BUFFER[7]";
	$oracle->update( table => 'rix_Machine',
			update_columns => 'MACHINE_ID, MACHINE_TEMPLATE_NUMBER, DEVICE_ID, MERCHANT_ID, PROMOTION_TEMPLATE, IN_USE, LOCATION_NUMBER, DIALUP_TIME, DIALUP_DATE, UNUSED_FIELD, TRACE_ON, DEBUG_ON, UNUSED_FIELD2, UNUSED_FIELD3, UNUSED_FIELD4, UNUSED_FIELD5, SERIAL_NUMBER, KEY_NUMBER, PHONE_NUMBER, COUNTER_HOLDER, AUTO_AUTH, SPECIAL_NOTES, UNPLUGGED_NIGHTLY, MACHINE_RATING, SOFTWARE_VERSION, UNUSED_FIELD6, UNUSED_FIELD7, UNUSED_FIELD8, UNUSED_FIELD9, MACHINE_TYPE_NUMBER, INITIALIZE_FLASH_DAILY, UPDATE_SOFTWARE, MINUTES_UNTIL_NEXT_DIALUP, CURRENT_SOFTWARE, SEND_VMC_COLUMN_INFO, SEND_SALES_TAX, SEND_DEX_SESSION, SEND_QUESTIONS, SEND_MESSAGES, SEND_OTHER, INSTALL_DATE',
			update_values => [$MACHINE_INFO_BUFFER[0],$MACHINE_INFO_BUFFER[1],$MACHINE_INFO_BUFFER[2],$MACHINE_INFO_BUFFER[3],$MACHINE_INFO_BUFFER[4],$MACHINE_INFO_BUFFER[5],$MACHINE_INFO_BUFFER[6],$MACHINE_INFO_BUFFER[7],"to_date(?,'MM/DD/YYYY HH24:MI:SS')",$MACHINE_INFO_BUFFER[9],$MACHINE_INFO_BUFFER[10],$MACHINE_INFO_BUFFER[11],$MACHINE_INFO_BUFFER[12],$MACHINE_INFO_BUFFER[13],$MACHINE_INFO_BUFFER[14],$MACHINE_INFO_BUFFER[15],$MACHINE_INFO_BUFFER[16],$MACHINE_INFO_BUFFER[17],$MACHINE_INFO_BUFFER[18],$MACHINE_INFO_BUFFER[19],$MACHINE_INFO_BUFFER[20],$MACHINE_INFO_BUFFER[21],$MACHINE_INFO_BUFFER[22],$MACHINE_INFO_BUFFER[23],$MACHINE_INFO_BUFFER[24],$MACHINE_INFO_BUFFER[25],$MACHINE_INFO_BUFFER[26],$MACHINE_INFO_BUFFER[27],$MACHINE_INFO_BUFFER[28],$MACHINE_INFO_BUFFER[29],$MACHINE_INFO_BUFFER[30],$MACHINE_INFO_BUFFER[31],$MACHINE_INFO_BUFFER[32],$MACHINE_INFO_BUFFER[33],$MACHINE_INFO_BUFFER[34],$MACHINE_INFO_BUFFER[35],$MACHINE_INFO_BUFFER[36],$MACHINE_INFO_BUFFER[37],$MACHINE_INFO_BUFFER[38],$MACHINE_INFO_BUFFER[39],$MACHINE_INFO_BUFFER[40]],
			where_columns => ['Machine_ID = ?'],
			where_values => [$MACHINE_INFO_BUFFER[0]],
			noquote_columns => [8],
			noquote_values => [$date_time],
		);

#$MACHINE_INFO_BUFFER[8] = slw_date_format($MACHINE_INFO_BUFFER[8]);
#CHANGED
	#&avc_Database_Update_Record(1,"",@MACHINE_INFO_BUFFER);

#switch back now that we've updated
# so lame but its crap i inhherited
       $MACHINE_INFO_BUFFER[5] = reformat_enums($MACHINE_INFO_BUFFER[5]);
        $MACHINE_INFO_BUFFER[10] = reformat_enums($MACHINE_INFO_BUFFER[10]);
        $MACHINE_INFO_BUFFER[11] = reformat_enums($MACHINE_INFO_BUFFER[11]);
        $MACHINE_INFO_BUFFER[20] = reformat_enums($MACHINE_INFO_BUFFER[20]);
        $MACHINE_INFO_BUFFER[22] = reformat_enums($MACHINE_INFO_BUFFER[22]);
        $MACHINE_INFO_BUFFER[30] = reformat_enums($MACHINE_INFO_BUFFER[30]);
        $MACHINE_INFO_BUFFER[31] = reformat_enums($MACHINE_INFO_BUFFER[31]);
        $MACHINE_INFO_BUFFER[34] = reformat_enums($MACHINE_INFO_BUFFER[34]);
        $MACHINE_INFO_BUFFER[35] = reformat_enums($MACHINE_INFO_BUFFER[35]);
        $MACHINE_INFO_BUFFER[36] = reformat_enums($MACHINE_INFO_BUFFER[36]);
        $MACHINE_INFO_BUFFER[37] = reformat_enums($MACHINE_INFO_BUFFER[37]);
        $MACHINE_INFO_BUFFER[38] = reformat_enums($MACHINE_INFO_BUFFER[38]);
        $MACHINE_INFO_BUFFER[39] = reformat_enums($MACHINE_INFO_BUFFER[39]);

}

########################################
#
# rix_os_Save_Ccu_Status
#
########################################
# Description: Writes the current info in @CCU_STATUS_BUFFER
#
# Uses Globals: @CCU_STATUS_BUFFER, @MACHINE_INFO_BUFFER
# 
# Written By: Scott Wasserman
# Created: 9/27/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Save_Ccu_Status
{
	local(	$machine_num);
	
	$machine_num = $MACHINE_INFO_BUFFER[0];

my $size = 35;
#$CCU_STATUS_BUFFER[1] = mysql_date_format($CCU_STATUS_BUFFER[1]);
$CCU_STATUS_BUFFER[2] = mysql_time_format($CCU_STATUS_BUFFER[2]);

foreach(0..($size - scalar(@CCU_STATUS_BUFFER) - 1))
{
	push(@CCU_STATUS_BUFFER,'');
}

my @ORC_CCU_STATUS_BUFFER = @CCU_STATUS_BUFFER[3..34];
#$ORC_CCU_STATUS_BUFFER[0] = oracle_date_format($ORC_CCU_STATUS_BUFFER[0]);
my $date_time = "$ORC_CCU_STATUS_BUFFER[0] $ORC_CCU_STATUS_BUFFER[1]";

      $oracle->insert(   table          => 'MACHINE_STATUS',
                           insert_columns => 'Machine_ID,
                                              Expected_Dialup_Date,
                                              Expected_Dialup_Time,
                                              Location_Number,
                                              Machine_Template_Number,
                                              Current_Software_Version,
                                              Dialing_Prefix,
                                              Number_Of_Cancels,
                                              Card_Swipes,
                                              Bad_Card_Swiped,
                                              No_Data_Card_Swipes,
                                              Cards_Reused,
                                              Vends_Winners,
                                              Losers,
                                              Failed_Connections,
                                              Failed_Auth_FD,
                                              Failed_Auth_Comm,
                                              Checksum_Errors,
                                              Software_Version,
                                              Call_In_Type,
                                              Inventory_Card_Number,
                                              Inventory_Swap_Result,
                                              Number_Attempted_Dialups,
                                              Mod_10_Failures,
                                              Expired_Cards,
                                              Vend_Timeouts,
                                              Printer_Error,
                                              Printer_Paper_Out,
                                              Swiper_Switch_Jam,
                                              Keypad_Btn_Stuck,
                                              Mdb_Stuck,
                                              Invalid_Cards,
                                              Vend_Limit_Failures',
		insert_values => [$rix_os_ENV{"MACHINE_ID"},"to_date(?,'MM/DD/YYYY HH24:MI:SS')",$ORC_CCU_STATUS_BUFFER[1],$ORC_CCU_STATUS_BUFFER[2],$ORC_CCU_STATUS_BUFFER[3],$ORC_CCU_STATUS_BUFFER[4],$ORC_CCU_STATUS_BUFFER[5],$ORC_CCU_STATUS_BUFFER[6],$ORC_CCU_STATUS_BUFFER[7],$ORC_CCU_STATUS_BUFFER[8],$ORC_CCU_STATUS_BUFFER[9],$ORC_CCU_STATUS_BUFFER[10],$ORC_CCU_STATUS_BUFFER[11],$ORC_CCU_STATUS_BUFFER[12],$ORC_CCU_STATUS_BUFFER[13],$ORC_CCU_STATUS_BUFFER[14],$ORC_CCU_STATUS_BUFFER[15],$ORC_CCU_STATUS_BUFFER[16],$ORC_CCU_STATUS_BUFFER[17],$ORC_CCU_STATUS_BUFFER[18],$ORC_CCU_STATUS_BUFFER[19],$ORC_CCU_STATUS_BUFFER[20],$ORC_CCU_STATUS_BUFFER[21],$ORC_CCU_STATUS_BUFFER[22],$ORC_CCU_STATUS_BUFFER[23],$ORC_CCU_STATUS_BUFFER[24],$ORC_CCU_STATUS_BUFFER[25],$ORC_CCU_STATUS_BUFFER[26],$ORC_CCU_STATUS_BUFFER[27],$ORC_CCU_STATUS_BUFFER[28],$ORC_CCU_STATUS_BUFFER[29],$ORC_CCU_STATUS_BUFFER[30],$ORC_CCU_STATUS_BUFFER[31]],
			noquote_columns => [1],
			noquote_values => [$date_time],
	);


#$CCU_STATUS_BUFFER[1] = slw_date_format($CCU_STATUS_BUFFER[1]);
$CCU_STATUS_BUFFER[2] = slw_time_format($CCU_STATUS_BUFFER[2]);

#CHANGED
#	&avc_Database_Write_Record(51,$machine_num,@CCU_STATUS_BUFFER);
}

########################################
#
# rix_os_Save_Ccu_Status
#
########################################
# Description: Adds a data item to the @CCU_STATUS_BUFFER
#
# Uses Globals: @CCU_STATUS_BUFFER, %CCU_STATUS_RECORD_LAYOUT
# 
# Written By: Scott Wasserman
# Created: 9/28/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Add_Ccu_Status_Item
{
	local(	$data_list_number,
			$data_item_number,
			$ccu_status_list_item_number,
			$status_data);
			
	$data_list_number = $_[0];
	$data_item_number = $_[1];
	$status_data = $_[2];

        ########################
        # cmd U2-2
        ########################
        if (($data_list_number == 2) && ($data_item_number == 2))
        {

           ############## DIALUP TYPE INFO #################
           my $dialup_type = $status_data & 0x000f;
           $CCU_STATUS_BUFFER[21] = $dialup_type;

           ############## ERROR CODES ######################
           my $mask = 2**4;

           ### PRINTER PROBLEM ###
          if ($status_data & $mask) {
              $CCU_STATUS_BUFFER[28] = 1;
           }
           $mask *= 2;
           ### PRINTER OUT OF PAPER ###
           if ($status_data & $mask) {
              $CCU_STATUS_BUFFER[29] = 1;
           }
           $mask *= 2;
           ### CARD SWIPER SWITCH JAMMED ###
           if ($status_data & $mask) {
              $CCU_STATUS_BUFFER[30] = 1;
           }
           $mask *= 2;
           ### KEYPAD BUTTON STUCK ###
           if ($status_data & $mask) {
              $CCU_STATUS_BUFFER[31] = 1;
           }
           $mask *= 2;
           ### MDB STUCK ###
           if ($status_data & $mask) {
              $CCU_STATUS_BUFFER[32] = 1;
           }
        }
        else
        {
           $ccu_status_list_item_number = $CCU_STATUS_RECORD_LAYOUT{$data_list_number.":".$data_item_number};

           $CCU_STATUS_BUFFER[$ccu_status_list_item_number] = $status_data;
        }

}

########################################
#
# rix_End_Session
#
########################################
# Description: Closes out the RIX session and performs any neccessary housekeeping
#
# Uses Globals: $rix_os_ENV
# 
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_End_Session
{
   # &rix_transaction_Move_Cancelled_Transactions(@cancels);
	
	&rix_os_Log_Session_End();	
				
}

########################################
#
# rix_Log_Session_Start
#
########################################
# Description: Gets software for download
# 
# Uses Globals: @SESSION_LOG_BUFFER, %rix_os_ENV
# 
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Log_Session_Start
{
	my $slbref = $oracle->select(table => 'dual',
			select_columns => 'RIX_DIALUP_LOG_RECORD_SEQ.nextval',
		);

	@SESSION_LOG_BUFFER = (			$slbref->[0][0],
							$rix_os_ENV{"DEVICE_ID"},
							$rix_os_ENV{"MACHINE_ID"},
							&mysql_date_format(&get_Date()),
							&mysql_time_format(&get_Time()),
							"",
							"");
				
$oracle->insert( table => 'RIX_Dialup_Log_Record',
		insert_columns => 'Machine_Dialup_Log_Record_Num,Device_ID, Machine_Number',
		insert_values => [$SESSION_LOG_BUFFER[0],$SESSION_LOG_BUFFER[1],$SESSION_LOG_BUFFER[2]],
		);

# will populate start date
$oracle->update( table => 'RIX_Dialup_Log_Record',
			update_columns => 'STARTED',
			update_values => ['Y'],
			where_columns => ['Machine_Dialup_Log_Record_Num = ?'],
			where_values => [$SESSION_LOG_BUFFER[0]],
		);
			

	#&avc_Database_Write_Record(60,"",@SESSION_LOG_BUFFER);
						
}

########################################
#
# rix_Log_Session_End
#
########################################
# Description: Gets software for download
# 
# Uses Globals: @SESSION_LOG_BUFFER, %rix_os_ENV
# 
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Log_Session_End
{
	
	$SESSION_LOG_BUFFER[5] = mysql_date_format(get_Date());
	$SESSION_LOG_BUFFER[6] = get_Time();

	$SESSION_LOG_BUFFER[4] = mysql_time_format($SESSION_LOG_BUFFER[4]);
	$SESSION_LOG_BUFFER[6] = mysql_time_format($SESSION_LOG_BUFFER[6]);

	$oracle->update( table => 'RIX_Dialup_Log_Record',
			update_columns => 'ENDED',
			update_values => ['Y'],
			where_columns => ['MACHINE_DIALUP_LOG_RECORD_NUM = ?'],
			where_values => [$SESSION_LOG_BUFFER[0]],
		);

	$SESSION_LOG_BUFFER[4] = slw_time_format($SESSION_LOG_BUFFER[4]);
	$SESSION_LOG_BUFFER[6] = slw_time_format($SESSION_LOG_BUFFER[6]);
	$SESSION_LOG_BUFFER[5] = slw_date_format($SESSION_LOG_BUFFER[5]);

#CHANGED	
#	&avc_Database_Update_Record(60,"",@SESSION_LOG_BUFFER);
}

########################################
#
# rix_Get_Software
#
########################################
# Description: Gets software for download
# 
# Uses Globals: %rix_os_ENV, @MACHINE_INFO_BUFFER, @SOFTWARE_UPDATE_LOG_BUFFER
# 
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Log_Software_Download_Start
{
#CHANGED
my $csbref = $oracle->select( table => 'dual',
		select_columns => 'SOFTWARE_UPDATE_LOG_RECORD_SEQ.nextval',
	);
	
	@SOFTWARE_UPDATE_LOG_BUFFER = (	$csbref->[0][0],
									&mysql_date_format(&get_Date()),
									mysql_time_format(&get_Time()),
									"",
									"",
									$MACHINE_INFO_BUFFER[33]);
	

	$oracle->insert( table => 'Software_Update_Log_Record',
			insert_columns => 'Software_Update_Log_Record_Num,Software_Version_Number,Machine_ID',
			insert_values => [$SOFTWARE_UPDATE_LOG_BUFFER[0],$SOFTWARE_UPDATE_LOG_BUFFER[5],$rix_os_ENV{"MACHINE_ID"}],
			);

	# will populate date started
	$oracle->update( table => 'Software_Update_Log_Record',
			update_columns => 'STARTED',
			update_values => ['Y'],
			where_columns => ['Software_Update_Log_Record_Num = ?'],
			where_values => [$SOFTWARE_UPDATE_LOG_BUFFER[0]],
		);

	$SOFTWARE_UPDATE_LOG_BUFFER[2] = slw_time_format($SOFTWARE_UPDATE_LOG_BUFFER[2]);


#CHANGED									
#	&avc_Database_Write_Record(54,$rix_os_ENV{"MACHINE_ID"},@SOFTWARE_UPDATE_LOG_BUFFER);

}

########################################
#
# rix_Get_Software
#
########################################
# Description: Gets software for download
# 
# Uses Globals:@SOFTWARE_UPDATE_LOG_BUFFER, %rix_os_ENV
# 
# Written By: Scott Wasserman
# Created: 9/2/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Log_Software_Download_End
{
#------
	$SOFTWARE_UPDATE_LOG_BUFFER[3] = &mysql_date_format(&get_Date());
	$SOFTWARE_UPDATE_LOG_BUFFER[4] = mysql_time_format(&get_Time());

	# will populate end date

	$oracle->update( table => 'Software_Update_Log_Record',
			update_columns => 'ENDED',
			update_values => ['Y'],
			where_columns => ['SOFTWARE_UPDATE_LOG_RECORD_NUM = ?'],
			where_values => [$SOFTWARE_UPDATE_LOG_BUFFER[0]],
			);

$SOFTWARE_UPDATE_LOG_BUFFER[3] = slw_date_format($SOFTWARE_UPDATE_LOG_BUFFER[3]);
$SOFTWARE_UPDATE_LOG_BUFFER[4] = slw_time_format($SOFTWARE_UPDATE_LOG_BUFFER[4]);

	#CHANGED	
	#&avc_Database_Update_Record(54,$rix_os_ENV{"MACHINE_ID"},@SOFTWARE_UPDATE_LOG_BUFFER);
}

########################################
#
# rix_Get_Software
#
########################################
# Description: Gets software for download
# 
# Uses Globals: @MACHINE_INFO_BUFFER, @SOFTWARE_BUFFER
# 
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Get_Software
{
	local(	$software_name,
			@software_out,
			@software_info,
			$software_version_num,
			$bin_line,
			$bytes_to_read);
	
	$software_version_num = $MACHINE_INFO_BUFFER[33];
	
	if ($software_version_num eq "")
	{
		$software_version_num = $DEFAULT_SOFTWARE_VERSION_NUM;
	}


	my $siref = $oracle->select( table => 'Software_Version_Record',
				select_columns => 'Filename',
				where_columns => ['Software_Version_Record_Number = ?'],
				where_values => [$software_version_num],
			);
	$software_filename = $siref->[0][0];
	
	$LINE_FEED = sprintf("%c",10);
	
	if ($software_filename ne "")
	{
		open(SOFTWARE_IN,"/rix/software/$software_filename");
		binmode(SOFTWARE_IN);
		@SOFTWARE_BUFFER = ();
		
		while (read(SOFTWARE_IN,$bytes_to_read_char,1))
		{	
			$bytes_to_read = ord($bytes_to_read_char);
			
			read(SOFTWARE_IN,$bin_line,$bytes_to_read);
			push(@SOFTWARE_BUFFER,$bin_line);
		}
		close(SOFTWARE_IN);

	}
	else
	{
		@SOFTWARE_BUFFER = ();
	}

}

########################################
#
# rix_Update_Device_Id_Record
#
########################################
# Description: Updates a Device ID record			
#
# NOTE: This will need to be changed if the database engine changes
#		It was created to speed up access to Device ID's.
# 
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 2/4/2000    Josh Richards Rewritten to use SQL
########################################
sub rix_os_Update_Device_Id_Record
{
    local($device_id,
	  $machine_num);	 
    
    $device_id = &rix_os_Get_Device_Id_Value($_[0]);
    $machine_num = $_[1];

    ## Check if we already have a record with this device_id
    if(&rix_os_Get_Machine_Id_From_Device_Id($device_id))
    {
	#$changed = &avc_Database_Update_Record(61, "", $device_id, $machine_num, "");
	$changed = $oracle->update(	table => 'RIX_Device_ID',
			update_columns => 'Device_ID,Machine_Number',
			update_values => [$device_id,$machine_num],
			where_columns => ['Device_ID = ?'],
			where_values => [$device_id],
		);
    }
    ## Otherwise, INSERT a new item...
    else
    {
	$oracle->insert( table => 'RIX_Device_ID',
			insert_columns => 'Device_ID, Machine_Number',
			insert_values => [$device_id,$machine_num],
		);
#CHANGED
#	&avc_Database_Write_Record(61, "", $device_id, $machine_num, "");
    }
}

########################################
#
# rix_os_Get_Machine_Id_From_Device_Id
#
########################################
# Description: Gets a Device ID record			
#
# NOTE: It was created to speed up access to Device ID's.
# 
# Written By: Scott Wasserman
# Created: 9/1/1999
# Modified: 2/3/2000
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 2/3/2000    Josh Richards Converted function to use SQL db
########################################
sub rix_os_Get_Machine_Id_From_Device_Id
{
    local($device_id,
	  $machine_id,
	  @the_record);
    
    $device_id = &rix_os_Get_Device_Id_Value($_[0]);

	 $diref = $oracle->select( table => 'RIX_Device_ID',
			select_columns => 'DEVICE_ID,MACHINE_ID,0',
			where_columns => ['Device_ID = ?'],
			where_values => [$device_id],
		);

	@the_record = @{$diref->[0]};

#CHANGED 
#    @the_record = &avc_Database_Get_Keyed_Record(61, $device_id);
    
    if(@the_record)
    {
	$machine_id = $the_record[1];
	$machine_id =~ s/ //gi;
    }
    else
    {
	$machine_id = "";
    }

    return $machine_id;
}

########################################
#
# rix_Get_Device_Id_Value
#
########################################
# Description: 	Converts a Device ID number to a numeric value for the purposes
#				of file seeks. 			
#
# NOTE: This will need to be changed if the database engine changes.
#		It was created to speed up access to Device ID's.
# 
# Written By: Scott Wasserman
# Created: 9/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 9/27/1999    SAK            added the perl substitution function to replace the digit-by-digit replace.
#
########################################
sub rix_os_Get_Device_Id_Value
{

	local(	$device_id);
			
	$device_id = $_[0];
        
    $device_id =~ s/^0+//gi;
	
	# Fix for the case of all zeros	
	if ($device_id eq "")
	{
		$device_id = "0";
	}
	
	return $device_id;

}



########################################
#
# rix_os_Init_Standard_Login_Data
#
########################################
# Description: Initializes all the data that will be required by the client in order to successfully 
#              conduct a RIX session. The standard login currently covers the cases when the client logs 
#              in for the following reasons:
#              1. batch transactions
#              2. client boot/reboot 
#              3. an inventory card session
#
#
# Uses Globals: @CCU_SETTING_DATA, @VMC_SETTING_DATA 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Standard_Login_Data
{
   # initialize the database records (necessary for standard login) associated with the current client
   &rix_os_Init_Standard_Login_Database_Lists();

   # initialize all the messages to be displayed by the CCU
   &rix_os_Init_Ccu_Messages_Data();

   # initialize all one-use specific data
   &rix_os_Init_Standard_Login_Ccu_One_Use_Data();

   # initialize any other information required by the standardized login
   &rix_os_Init_Ccu_Misc_Data();


   # update current local date and time in @CCU_SETTING_DATA
   &rix_os_Update_Ccu_Setting_Date_And_Time();

   # fill out expected dialup date and time values into CCU
   &rix_os_Init_Ccu_Dialup_Timestamp_Data();


   # initialize all information pertaining to questions asked of consumer
   &rix_os_Init_Ccu_Question_Data();

   # initialize FDC-related information
   &rix_os_Init_Ccu_FDC_Data();
&rix_os_Update_Ccu_Setting_Date_And_Time();

   # at this stage all the information in the CCU necessary for standard login has been initialized
   # we indicate this by setting a flag
   ########################
   #> Mark @CCU_SETTING_DATA inited
   ########################
   $CCU_SETTING_DATA[0] = "Inited";

   # initialize all data the goes into the VMC
   &rix_os_Init_Vmc_Setting_Data();

   # at this stage all the information in the VMC necessary for standard login has been initialized
   # we indicate this by setting a flag
   ########################
   #> Mark @VMC_SETTING_DATA inited
   ########################
   $VMC_SETTING_DATA[0] = "Inited";

	 &rix_os_Init_Tax_Setting_Data();
   ########################
   #> Mark @TAX_SETTING_DATA inited
   ########################
   $TAX_SETTING_DATA[0] = "Inited";


   # initialize all the swipe setting data on client
   &rix_os_Init_Swipe_Setting_Data();

   # initialize all the receipt setting data on client
   &rix_os_Init_Receipt_Setting_Data();

   # initializes the CCU status buffer and the CCU status record layout
   &rix_os_Init_Ccu_Status_Data();

   # Initialize machine-specific transaction_GLOBAL variables
   # mastercode
	$rix_transaction_GLOBAL{'SELLTHRU_REF'} = \%dummy;
   $rix_transaction_GLOBAL{"MASTERCODE"} = $MACHINE_TEMPLATE_INFO_BUFFER[39];

   # Insert current date and time into Machine Initialization Record [70] for current machine
   &rix_os_Initialize_Machine();
}


sub rix_os_Initialize_Machine
{
   local( @machines_inited );

	$ref = $oracle->select(
			table => 'Machine_Initialization',
			select_columns => 'Machine_ID',
		);
	foreach $yyrow (@$ref)
	{
		push (@machines_inited,$yyrow->[0]);
	}
   #@machines_inited = &avc_Database_Get_Key_Fields_List(70);

   # &dump_List(@machines_inited);
#CHANGED
   if (!grep (/$rix_os_ENV{"MACHINE_ID"}/, @machines_inited))
   {
      # print "wrote ".$rix_os_ENV{"MACHINE_ID"}."\n";
	$oracle->insert( table => 'Machine_Initialization',
		insert_columns => 'Machine_ID, Init_Date, Init_Time',
		#insert_values => [$rix_os_ENV{"MACHINE_ID"},&mysql_date_format(&get_Date()), mysql_time_format(&get_Time())],
		insert_values => [$rix_os_ENV{"MACHINE_ID"},&get_Date(), mysql_time_format(&get_Time())],
		);

#      &avc_Database_Write_Record( 70,
                                  # Machine Number (NONE)  since global record
                                  "",
                                  # Transaction Record List
#                                  $rix_os_ENV{"MACHINE_ID"},
#                                  &get_Date(),
#                                  &get_Time() );
   }
}



########################################
#
# rix_os_Init_Standard_Login_Database_Lists
#
########################################
# Description: Fill global database lists needed by the login script during standard login. Creates
#              1) MACHINE_TEMPLATE_INFO_BUFFER [48]
#              2) MACHINE_TYPE_INFO_BUFFER [10]
#              3) PROMO_TEMPLATE_INFO_BUFFER [11]
#              4) LOCATION_INFO_BUFFER [18]
#
# NOTE: MACHINE_INFO_BUFFER is created in rix_os_Init_Session.
#       
# Uses Globals: %rix_os_ENV, @MACHINE_TEMPLATE_INFO_BUFFER, @MACHINE_TYPE_INFO_BUFFER, 
#               @PROMO_TEMPLATE_INFO_BUFFER, @LOCATION_INFO_BUFFER
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Standard_Login_Database_Lists
{
   local ( $machine_template_number );
   
   $machine_template_number = $MACHINE_INFO_BUFFER[1];
   
   # machine template is of record type 48 in AVC database
	my $mtibref = $oracle->select( table => 'Machine_Template_Record',
			select_columns => 'MACHINE_TEMPLATE_NUMBER, MACHINE_TEMPLATE_NAME, CARD_TRACK_TYPE, AUTHORIZATION_TYPE, DAILY_ACCOUNTING_TYPE, ONE_USE_PROMOTIONS, AUTH_PHONE, FIRST_DATA_PHONE, RIX_PHONE, PRINTER_USED, CANCEL_USED, INVENTORY_CARD_ENABLED, LOCAL_AUTH_VEND_LIMIT, AUTH_MODEM_SPEED, MSG_AUTH_BAD, MSG_AUTH_CARD, MSG_AUTH_OK, GATEKEEPER_NEEDED, MGS_GATEKEEPER, MSG_AUTH_LIMIT, PING_INTERVAL, UNUSED_FIELD, UNUSED_FIELD1, UNUSED_FIELD2, MSG_AVC_BUSY_PLEASE_WAIT, MSG_AVC_CLOSE_BATCH, UNUSED_FIELD3, UNUSED_FIELD4, UNUSED_FIELD5, UNUSED_FIELD6, UNUSED_FIELD7, UNUSED_FIELD8, UNUSED_FIELD9, UNUSED_FIELD10, MSG_CARD_INSERT, MSG_CARD_REMOVE, MSG_CARD_CANT_READ, MSG_CARD_WRONG_TYPE, MSG_CARD_INVALID, CARD_MASTER_CODE, CARD_SERVICE_CODE, MAX_VEND_PER_SWIPE, FOB_MIN_DURATION, UNUSED_FIELD14, MSG_INVENTORY_ERROR_CLOSE, MSG_INVENTORY_INIT, MSG_INVENTORY_PLEASE_WAIT, MSG_INVENTORY_TRY_AGAIN, MSG_INVENTORY_UPDATE_COMPLETE, MSG_INVENTORY_CODE, UNUSED_FIELD15, UNUSED_FIELD16, UNUSED_FIELD17, UNUSED_FIELD18, MSG_RECEIPT_COUPON, MSG_RECEIPT_INFO, MSG_RECEIPT_PRINTING, MSG_RECEIPT_QUESTION, MSG_RECEIPT_TAKE, MSG_RECEIPT_NO_PAPER, UNUSED_FIELD19, UNUSED_FIELD20, UNUSED_FIELD21, UNUSED_FIELD22, MSG_SYSTEM_BUSY_PLEASE_WAIT, MSG_SYSTEM_BUSY_TRY_AGAIN, MSG_SYSTEM_FATAL_ERROR, MSG_SYSTEM_FLASH_INIT, MSG_SYSTEM_SELF_TEST, MSG_SYSTEM_OUT_OF_SERVICE, MSG_SYSTEM_NO_DIALTONE, MSG_SYSTEM_PHONE_PREFIX, UNUSED_FIELD23, UNUSED_FIELD24, MSG_VEND_CANCEL, MSG_VEND_SELECT, MSG_VEND_LOSER, MSG_VEND_VENDING, UNUSED_FIELD25, UNUSED_FIELD26, UNUSED_FIELD27, UNUSED_FIELD28, UNUSED_FIELD29, UNUSED_FIELD30, QUESTION1, QUESTION2, UNUSED_FIELD31, UNUSED_FIELD32, UNUSED_FIELD33, UNUSED_FIELD34, UNUSED_FIELD35, UNUSED_FIELD36, UNUSED_FIELD37, UNUSED_FIELD38, UNUSED_FIELD39, UNUSED_FIELD40, UNUSED_FIELD41, UNUSED_FIELD42, UNUSED_FIELD43, UNUSED_FIELD44, UNUSED_FIELD45, UNUSED_FIELD46, UNUSED_FIELD47, UNUSED_FIELD48, UNUSED_FIELD49, UNUSED_FIELD50, UNUSED_FIELD51, UNUSED_FIELD52, UNUSED_FIELD53, UNUSED_FIELD54, UNUSED_FIELD55, UNUSED_FIELD56, UNUSED_FIELD57, UNUSED_FIELD58, PAR_RECEIPT_ASK_QUESTION, PAR_RECEIPT_ALWAYS, UNUSED_FIELD59, UNUSED_FIELD60, UNUSED_FIELD61, PRICE_INCLUDES_TAX, CREDIT_CARD_ACCEPTED,AUTH_SWITCH_TIMEOUT, VEND_TIMEOUT,CARD_SERVICE_CODE,MAX_VEND_PER_SWIPE,FOB_MIN_DURATION,QUESTION3,MULTI_VEND_LIMIT,MULTI_VEND_COUNTDOWN',
			where_columns => ['Machine_Template_Number = ?'],
			where_values => [$machine_template_number],
		);
	@MACHINE_TEMPLATE_INFO_BUFFER = @{$mtibref->[0]};

$MACHINE_TEMPLATE_INFO_BUFFER[9] = reformat_enums($MACHINE_TEMPLATE_INFO_BUFFER[9]);
$MACHINE_TEMPLATE_INFO_BUFFER[10] = reformat_enums($MACHINE_TEMPLATE_INFO_BUFFER[10]);
$MACHINE_TEMPLATE_INFO_BUFFER[11] = reformat_enums($MACHINE_TEMPLATE_INFO_BUFFER[11]);
$MACHINE_TEMPLATE_INFO_BUFFER[17] = reformat_enums($MACHINE_TEMPLATE_INFO_BUFFER[17]);
$MACHINE_TEMPLATE_INFO_BUFFER[114] = reformat_enums($MACHINE_TEMPLATE_INFO_BUFFER[114]);
$MACHINE_TEMPLATE_INFO_BUFFER[115] = reformat_enums($MACHINE_TEMPLATE_INFO_BUFFER[115]);
$MACHINE_TEMPLATE_INFO_BUFFER[119] = reformat_enums($MACHINE_TEMPLATE_INFO_BUFFER[119]);
$MACHINE_TEMPLATE_INFO_BUFFER[120] = reformat_enums($MACHINE_TEMPLATE_INFO_BUFFER[120]);



#CHANGED
   #@MACHINE_TEMPLATE_INFO_BUFFER = &avc_Database_Get_Keyed_Record(48,$machine_template_number);
  
	 $mtib2ref = $oracle->select( table => 'rix_Machine_Type',
				select_columns => 'MACHINE_TYPE_NUMBER, MACHINE_DESCRIPTION, NEEDS_MAX_PRICE_OVERRIDE, UNUSED_FIELD1, UNUSED_FIELD2, UNUSED_FIELD3, UNUSED_FIELD4, UNUSED_FIELD5, UNUSED_FIELD6, NUMBER_OF_BUTTONS, NUMBER_OF_COLUMNS, COLUMN_INDEX_ORIGIN, COLUMN_TRANSLATION_TYPE, UNUSED_FIELD7, UNUSED_FIELD8, UNUSED_FIELD9, UNUSED_FIELD10, UNUSED_FIELD11, UNUSED_FIELD12, UNUSED_FIELD13, UNUSED_FIELD14, UNUSED_FIELD15, UNUSED_FIELD16, UNUSED_FIELD17, UNUSED_FIELD18, UNUSED_FIELD19, UNUSED_FIELD20, UNUSED_FIELD21, UNUSED_FIELD22, UNUSED_FIELD23',
				where_columns => ['Machine_Type_Number = ?'], 
				where_values => [$MACHINE_INFO_BUFFER[29]],
			);

	my $mporef = $oracle->select( table => 'Machine_Template_Record',
			select_columns => 'NEEDS_MAX_PRICE_OVERRIDE',
			where_columns => ['Machine_Template_Number = ?'],
			where_values => [$MACHINE_INFO_BUFFER[1]],
		);


@MACHINE_TYPE_INFO_BUFFER = @{$mtib2ref->[0]};

$MACHINE_TYPE_INFO_BUFFER[2] = $mporef->[0][0];
$MACHINE_TYPE_INFO_BUFFER[2] =  reformat_enums($MACHINE_TYPE_INFO_BUFFER[2]);


   # machine type is of record type 10 in AVC database
#CHANGED
   #@MACHINE_TYPE_INFO_BUFFER = &avc_Database_Get_Keyed_Record(10,$MACHINE_INFO_BUFFER[29]);
   
   # promotional template is of record type 11 in AVC database
	$ptibref = $oracle->select(table => 'Promotion_Template',
				select_columns => 'TEMPLATE_NUMBER, PROMOTIION_NAME, DISPLAY_ADD_LINE1, DISPLAY_ADD_LINE2, DISPLAY_ADD1_LINE1, DISPLAY_ADD2_LINE2, UNUSED_FIELD1, UNUSED_FIELD2, UNUSED_FIELD3, UNUSED_FIELD4, UNUSED_FIELD5, UNUSED_FIELD6, UNUSED_FIELD7, UNUSED_FIELD8, ITEM1_PRICE, ITEM2_PRICE, ITEM3_PRICE, ITEM4_PRICE, ITEM5_PRICE, ITEM6_PRICE, ITEM7_PRICE, ITEM8_PRICE, ITEM9_PRICE, ITEM10_PRICE, ITEM1_PROPERTY_LIST, ITEM2_PROPERTY_LIST, ITEM3_PROPERTY_LIST, ITEM4_PROPERTY_LIST, ITEM5_PROPERTY_LIST, ITEM6_PROPERTY_LIST, ITEM7_PROPERTY_LIST, ITEM8_PROPERTY_LIST, ITEM9_PROPERTY_LIST, ITEM10_PROPERTY_LIST, UNUSED_FIELD9, UNUSED_FIELD10, UNUSED_FIELD11, UNUSED_FIELD12, UNUSED_FIELD13, UNUSED_FIELD14, UNUSED_FIELD15, UNUSED_FIELD16, UNUSED_FIELD17, UNUSED_FIELD18, ITEM1_INVENTORY_ITEM_NUM, ITEM2_INVENTORY_ITEM_NUM, ITEM3_INVENTORY_ITEM_NUM, ITEM4_INVENTORY_ITEM_NUM, ITEM5_INVENTORY_ITEM_NUM, ITEM6_INVENTORY_ITEM_NUM, ITEM7_INVENTORY_ITEM_NUM, ITEM8_INVENTORY_ITEM_NUM, ITEM9_INVENTORY_ITEM_NUM, ITEM10_INVENTORY_ITEM_NUM, RECEIPT_DATA_STRING, DATE_STARTED, MACHINE_TEMPLATE_NUMBER',
				where_columns => ['Template_Number = ?'],
				where_values => [$MACHINE_INFO_BUFFER[4]],
			);
	@PROMO_TEMPLATE_INFO_BUFFER = @{$ptibref->[0]};


# CHANGED
   #@PROMO_TEMPLATE_INFO_BUFFER = &avc_Database_Get_Keyed_Record(11,$MACHINE_INFO_BUFFER[4]);
  
	$libref = $oracle->select( table => 'Location',
			select_columns => 'LOCATION_NUMBER,LOCATION_NAME,0,STREET_ADDRESS_1,STREET_ADDRESS_2,CITY,STATE,ZIP,COUNTRY,PHONE,FAX,TAX_PERCENTAGE,TIME_ZONE,0,0,0,ROYALTY_RENT_PERCENTAGE,LOCATION_CONTACT_PERSON,0,0,0,DIRECTIONS,0,OPENS,CLOSES,FILL_CONTACT_PERSON,SERVICE_COMPANY_CONTACT_PERSON,NOTES,TAX_TYPE,COST_OF_GOODS_PERC',
			where_columns => ['Location_Number = ?'],
			where_values => [$MACHINE_INFO_BUFFER[6]],
		);

	@LOCATION_INFO_BUFFER = @{$libref->[0]};



   # location information is of record type 18 in AVC database
#CHANGED
   #@LOCATION_INFO_BUFFER = &avc_Database_Get_Keyed_Record(18,$MACHINE_INFO_BUFFER[6]);

   ####################################################################################
   # Set $TIME_ZONE in rix_os_ENV
   ####################################################################################
   $rix_os_ENV{"TIME_ZONE"} = $LOCATION_INFO_BUFFER[12];
   $rix_os_ENV{"EXPECTED_DIALUP_TIME"} = $MACHINE_INFO_BUFFER[7];
   $rix_os_ENV{"EXPECTED_DIALUP_DATE"} = $MACHINE_INFO_BUFFER[8];


}


########################################
#
# rix_os_Init_Ccu_Messages_Data
#
########################################
# Description: Initializes all the Ccu messages (see below) 
#
# 	1	msgAuthBad
# 	2	msgAuthCard
# 	3	msgAuthOk
# 	
# 	10	msgAvcPleaseWait
# 	11	msgAvcCloseBatch
# 	
# 	20	msgCardInsert
# 	21	msgCardRemove
# 	22	msgCardCantRead
# 	23	msgCardWrongType
# 	24	msgCardInvalid
# 	25	msgCardMasterCode
#       26      msgDriversLicenseInsert
#       27      msgDriversLicenseREmove
# 	
# 	30	msgInventoryErrorClose
# 	31	msgInventoryInit
# 	32	msgInventoryPleaseWait
# 	33	msgInventoryTryAgain
# 	34	msgInventoryUpdateComplete
# 	35	msgInventoryCode
# 	
# 	42	msgReceiptPrinting
# 	43	msgReceiptQuestion
# 	44	msgReceiptTake
# 	45	msgReceiptNoPaper
# 	
# 	50	msgSystemBusyPleaseWait
# 	51	msgSystemBusyTryAgain
# 	52	msgSystemFatalError
# 	53	msgSystemFlashInit
# 	54	msgSystemSelfTest
# 	55	msgSystemOutOfService
# 	56 	msgSystemNoDialtone
# 	57	msgSystemPhonePrefix
# 	
# 	60	msgVendCancel
# 	61	msgVendSelect
# 	62	msgVendLoser
# 	63	msgVendVending
# 	
#
#
#
#
# Uses Globals: @CCU_SETTING_DATA 

# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Ccu_Messages_Data
{
   local (  @ccu_setting_data_fill_list,
            $ccu_setting_data_fill_item,
            $data_item_counter,
            $database_record_number,
            $database_item_number,
            $database_item_separator,
            $number_of_items_to_return,
            $data_to_store );


   ####################################################################################
   # List for creating @CCU_SETTING_DATA
   ####################################################################################
   @ccu_setting_data_fill_list = (	"1:48:14:|:2",
                                        "2:48:15:|:2",
                                        "3:48:16:|:2",
					"4:48:19:|:2",
                                        "10:48:24:|:2",
                                        "11:48:25:|:2",
                                        "20:48:34:|:2",
                                        "21:48:35:|:2",
                                        "22:48:36:|:2",
                                        "23:48:37:|:2",
                                        "24:48:38:|:2",
                                        "25:48:39:|:1",
                                        "26:48:18:|:2",
                                        "30:48:44:|:2",
                                        "31:48:45:|:2",
                                        "32:48:46:|:2",
                                        "33:48:47:|:2",
                                        "34:48:48:|:2",
                                        "35:48:49:|:1",
                                        "42:48:56:|:2",
                                        "43:48:57:|:2",
                                        "44:48:58:|:2",
                                        "45:48:59:|:2",
                                        "50:48:64:|:2",
                                        "51:48:65:|:2",
                                        "52:48:66:|:2",
                                        "53:48:67:|:2",
                                        "54:48:68:|:2",
                                        "55:48:69:|:2",
                                        "56:48:70:|:2",
                                        "57:48:71:|:2",
                                        "60:48:74:|:2",
                                        "61:48:75:|:2",
                                        "62:48:76:|:2",
                                        "63:48:77:|:2",
					"64:48:40:|:1",);
                                                                   
   ####################################################################################
   # Fill in @CCU_SETTING_DATA with the data requested in @setting_data_fill_list
   ####################################################################################
   foreach $ccu_setting_data_fill_item (@ccu_setting_data_fill_list)
   {
           (	$setting_data_item_number,
                   $database_record_number,
                   $database_item_number,
                   $database_item_separator,
                   $number_of_items_to_return) = split(/:/,$ccu_setting_data_fill_item);

           # This is kind of stupid.  
           # I couldn't get split(/\$database_item_separator/) working with a | seperator
           if ($database_item_separator eq "\|")
           {
                   @database_item_split = split(/\|/,$MACHINE_TEMPLATE_INFO_BUFFER[$database_item_number]);
           }
           else
           {
                   @database_item_split = split(/$database_item_separator/,$MACHINE_TEMPLATE_INFO_BUFFER[$database_item_number]);
           }

           $data_to_store = "";
           for ($data_item_counter = 0;$data_item_counter < $number_of_items_to_return;$data_item_counter++)
           {
                   $data_to_store .= &rix_os_Fill_In_Message_Info($database_item_split[$data_item_counter]).$rix_os_ENV{"SEPARATOR"};
           }
	chop($data_to_store = $data_to_store);

           $CCU_SETTING_DATA[$setting_data_item_number] = $data_to_store;
   }
}




########################################
#
# rix_os_Init_Standard_Login_Ccu_One_Use_Data
#
########################################
# Description: Fills into the Ccu, all information pertaining to one use promotions, i.e., fields
#              
# 	82 	probability
# 	86	loserHasNoData
# 	87	initializeDatabase
#       100	Valid Prefixes
# 		c100,<valid prefix number (1-5)>,checksum
# 		return <valid prefix>,checksum
# 		NOTE: Assume will always read prefixes 1-5        
#
#
# Uses Globals: @CCU_SETTING_DATA 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Standard_Login_Ccu_One_Use_Data
{
   local (  @one_use_promotions,
            @valid_prefixes,
            $one_use_promotion_num,
            $probability,
            $loser_has_no_data,
            $init_daily,
            $one_use_probability,
            $one_use_init_daily,
            $one_use_loser_has_no_data,
            @one_use_promotion_info,
            @one_use_prefixes,
            $one_use_prefix,
            $valid_one_use_prefix,
            %prefix_to_promo,
            %prefix_to_DBtable,
            $promo_DBtable );


   # the split SHOULD take place at the commas instead of the \0 character.
   # @one_use_promotions = split(/\0/,$machine_template_info[5]);
   @one_use_promotions = split(/,/,$MACHINE_TEMPLATE_INFO_BUFFER[5]);	
   
   @valid_prefixes = ();
   %prefix_to_promo = ();
   %prefix_to_DBtable = ();

   $probability = 0;
   $loser_has_no_data = 0;
   $init_daily = 0;
     
   foreach $one_use_promotion_num (@one_use_promotions)
   {
	my $oupiref = $oracle->select( table => 'One_Use_Promo_Record',
			select_columns => 'ONE_USE_PROMO_NUMBER, ONE_USE_PROMO_NAME, INIT_DAILY, PREFIXES, CLIENT_PROBABILITY, LOSER_HAS_NO_DATA, SERVER_PROBABILITY, TABLE_NUMBER, TABLE_NAME',
			where_columns => ['One_Use_Promo_Number = ?'],
			where_values => [$one_use_promotion_num],
		);
	@one_use_promotion_info = @{$oupiref->[0]};
	$one_use_promotion_info[2] = reformat_enums($one_use_promotion_info[2]);
	$one_use_promotion_info[5] = reformat_enums($one_use_promotion_info[5]);

	#CHANGED
      #@one_use_promotion_info = &avc_Database_Get_Keyed_Record(49,$one_use_promotion_num);
      $one_use_init_daily = $one_use_promotion_info[2];
      $one_use_probability = $one_use_promotion_info[4];
      $one_use_loser_has_no_data = $one_use_promotion_info[5];
      $promo_DBtable = $one_use_promotion_info[7];
      
      @one_use_prefixes = split(/\|/,$one_use_promotion_info[3]);  # this split on | wont be necessary in the future because single prefix only 
      
      foreach $one_use_prefix (@one_use_prefixes)
      {
            if (!grep (/$one_use_prefix/,@valid_prefixes))
            {
                    push(@valid_prefixes,$one_use_prefix);

                    $prefix_to_promo{$one_use_prefix} = $one_use_promotion_num;
                    $prefix_to_DBtable{$one_use_prefix} = $promo_DBtable;

            }
      }
      
      # Take highest probability
      if ($one_use_probability > $probability)
      {
            $probability = $one_use_probability;
      }
      
      # If one has init daily, all have init daily		
      if ( ($one_use_init_daily == 1) && ($init_daily == 0) )
      {
            $init_daily = 1;
      }
                    
      # If one loser has no data, all losers have no data
      if ( ($one_use_loser_has_no_data == 1) && ($loser_has_no_data == 0) )
      {
            $loser_has_no_data = 1;
      }
   }
   
   if ($probability == 0)
   {
      $probability = 100;
   }
   
   $CCU_SETTING_DATA[82] = $probability;
   $CCU_SETTING_DATA[86] = $loser_has_no_data;
   $CCU_SETTING_DATA[87] = $init_daily;

   $valid_prefix_data = "";
   foreach $valid_one_use_prefix (@valid_prefixes)
   {
      $valid_prefix_data .= $valid_one_use_prefix.$rix_os_ENV{"SEPARATOR"};
   }
	chop($valid_prefix_data = $valid_prefix_data);
   
   $CCU_SETTING_DATA[100] = $valid_prefix_data;

   # We probably DONT need this
   $rix_transaction_GLOBAL{"PREFIX_TO_PROMO_REF"} = [ %prefix_to_promo ];

   $rix_transaction_GLOBAL{"PREFIX_TO_DBTABLE_REF"} = [ %prefix_to_DBtable ];

}




########################################
#
# rix_os_Init_Ccu_Misc_Data
#
########################################
# Description: Fills all miscellaneous data into the CCU settings. Fields input are 
# 
#       80	authorizationType
# 	82 	probability
# 	83	printerUsed
# 	84	cancelUsed
# 	85	inventoryCardEnabled
#
#       87	initializeDatabase
# 	88	taxPercentage
#	89	softwareDownloadNeeded
# 
# 	110	promotionAd
# 	111	promotionName
#      
#
# Uses Globals: @CCU_SETTING_DATA, @MACHINE_INFO_BUFFER, @LOCATION_INFO_BUFFER, @PROMO_TEMPLATE_INFO_BUFFER,
#               @MACHINE_TEMPLATE_INFO_BUFFER
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Ccu_Misc_Data
{
   local (  @ccu_setting_data_fill_list, 
            $ccu_setting_data_fill_item,
            $setting_data_item_number,
            $data_item_counter,
            $database_record_number,
            $database_item_number,
            $database_item_separator,
            $number_of_items_to_return,
            $data_to_store );

            
   ##################
   # NOTE:  $CCU_SETTING_DATA[81] added. $CCU_SETTING_DATA = CreditCardAccepted
   ##################
                     
   @ccu_setting_data_fill_list = ("79:48:17:|:1",
                                  "80:48:3:|:1",
                                  "81:48:120:|:1",
                                  "83:48:9:|:1",
                                  "84:48:10:|:1",
                                  "85:48:11:|:1",
				"141:48:121:|:1",
				"142:48:122:|:1",
				"143:48:41:|:1",
				"144:48:127:i:1",
				"145:48:128:|:1",
				"150:48:42:|:1" );
   ####################################################################################
   # Fill in @CCU_SETTING_DATA with the data requested in @setting_data_fill_list
   ####################################################################################
   foreach $ccu_setting_data_fill_item (@ccu_setting_data_fill_list)
   {
      (	$setting_data_item_number,
                   $database_record_number,
                   $database_item_number,
                   $database_item_separator,
                   $number_of_items_to_return) = split(/:/,$ccu_setting_data_fill_item);

      # This is kind of stupid.  
      # I couldn't get split(/\$database_item_separator/) working with a | seperator
      if ($database_item_separator eq "\|")
      {
             @database_item_split = split(/\|/,$MACHINE_TEMPLATE_INFO_BUFFER[$database_item_number]);
      }
      else
      {
             @database_item_split = split(/$database_item_separator/,$MACHINE_TEMPLATE_INFO_BUFFER[$database_item_number]);
      }
      
      $data_to_store = "";
      for ($data_item_counter = 0;$data_item_counter < $number_of_items_to_return;$data_item_counter++)
      {
             $data_to_store .= &rix_os_Fill_In_Message_Info($database_item_split[$data_item_counter]).$rix_os_ENV{"SEPARATOR"};
      }
	chop($data_to_store = $data_to_store);
      
      $CCU_SETTING_DATA[$setting_data_item_number] = $data_to_store;
   }

   ########################
   #> Is init database flag set?
   ########################
   if ($MACHINE_INFO_BUFFER[30])
   {
      $CCU_SETTING_DATA[87] = 1;
   }

   ########################
   #> Tax Percentage
   ########################
   if ($machine_template_info[119])
   {  # tax included in price...then, tax %age = 0.00
      $CCU_SETTING_DATA[88] = 0.00;
   }
   else
   {
      $CCU_SETTING_DATA[88] = $LOCATION_INFO_BUFFER[11];
   }

   ########################
   #> Download new software?
   ########################
   $CCU_SETTING_DATA[89] = $MACHINE_INFO_BUFFER[31];
   
   ########################
   #> Display Ad & Promo Name
   ########################
   $promotion_name = &rix_os_Fill_In_Message_Info($PROMO_TEMPLATE_INFO_BUFFER[1]);
   $promotion_ad1 = &rix_os_Fill_In_Message_Info($PROMO_TEMPLATE_INFO_BUFFER[2]).$rix_os_ENV{"SEPARATOR"}.&rix_os_Fill_In_Message_Info($PROMO_TEMPLATE_INFO_BUFFER[3]);
   $promotion_ad2 = &rix_os_Fill_In_Message_Info($PROMO_TEMPLATE_INFO_BUFFER[4]).$rix_os_ENV{"SEPARATOR"}.&rix_os_Fill_In_Message_Info($PROMO_TEMPLATE_INFO_BUFFER[5]);
   $CCU_SETTING_DATA[109] = $promotion_ad1;
   $CCU_SETTING_DATA[110] = $promotion_ad2;
   $CCU_SETTING_DATA[111] = $promotion_name;

   ########################
   #> FDC & RIX phone number info
   ########################
   $CCU_SETTING_DATA[130] = $MACHINE_TEMPLATE_INFO_BUFFER[7];
   $CCU_SETTING_DATA[131] = $MACHINE_TEMPLATE_INFO_BUFFER[8];
   $CCU_SETTING_DATA[132] = $MACHINE_TEMPLATE_INFO_BUFFER[6];

   ########################
   #> Local Auth info
   ########################
   $CCU_SETTING_DATA[140] = $MACHINE_TEMPLATE_INFO_BUFFER[12];


}





########################################
#
# rix_os_Init_Ccu_Timestamp_Data
#
########################################
# Description: Fills out expected dialup date and time values into CCU. Specifically, the following
#              field is initialized.
# 
#       71	Next Dialup Date and Time
# 		c71,checksum
# 		returns <dialup date><space><dialup time>,checksum

#      
#
# Uses Globals: @CCU_SETTING_DATA, @MACHINE_INFO_BUFFER
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Ccu_Dialup_Timestamp_Data
{
   ########################
   #> Find Next Dialup Date & Time and save it in @MACHINE_INFO_BUFFER
   ########################
   ($server_next_dialup_date,$server_next_dialup_time) = &rix_os_Calculate_Next_Dialup();

	warn("TIME DEBUG out of calc: $server_next_dialup_date,$server_next_dialup_time\n");

   # in the future, we could have the machines dial in more than once per day, 
   # so we need to update expected dialup time as well
   $MACHINE_INFO_BUFFER[7] = $server_next_dialup_time;
   
   # in the future, we could have the machines dial in more than once per day, so we need to update time as
   # well
   $MACHINE_INFO_BUFFER[8] = $server_next_dialup_date;
   
   # Update machine info
   #&rix_os_Update_Machine_Info();	
   
   ($local_next_dialup_date,$local_next_dialup_time) = &rix_os_Make_Local_Date_And_Time($server_next_dialup_date,$server_next_dialup_time);	
	warn("TIME DEBUG: $local_next_dialup_time\n");
   
   $CCU_SETTING_DATA[71] = $local_next_dialup_date." ".$local_next_dialup_time;

}





########################################
#
# rix_os_Init_Ccu_Question_Data
#
########################################
# Description: Fills CCU slots with questions intended for the user. Fills in CCU fields
# 
#       90	question 1 information
# 		c90,checksum
# 		return <RIX Ack/Nak>,<question line 1>,<question line 2>,<min length>,<max length>,<question type>,checksum
# 	91	question 2 information
# 		c91,checksum
# 		return <RIX Ack/Nak>,<question line 1>,<question line 2>,<min length>,<max length>,<question type>,checksum
#
#       92	new question 1 information
#        	<device id>,c92,checksum
#        	return <RIX Ack/Nak>,<question line 1>,<question line 2>,<min length>,<max length>,<question type>,<question function>,<timeout length>,<timeout action>,<default answer>,checksum
#       93	new question 2 information
#        	<device id>,c93,checksum
#        	return <RIX Ack/Nak>,<question line 1>,<question line 2>,<min length>,<max length>,<question type>,<question function>,<timeout length>,<timeout action>,<default answer>,checksum

#		
#
# Uses Globals: @CCU_SETTING_DATA, @$MACHINE_TEMPLATE_INFO_BUFFER, %$rix_os_ENV 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Ccu_Question_Data
{
   local (  $question1_number,
            $question2_number,
            @question1_info,
            @question2_info,
            $question1_data,
            $question2_data,
            $new_question1_data,
            $new_question2_data );
   
   ########################
   #> Question Data
   ########################
   $question1_number = $MACHINE_TEMPLATE_INFO_BUFFER[84];
   $question2_number = $MACHINE_TEMPLATE_INFO_BUFFER[85];
	$question3_number = $MACHINE_TEMPLATE_INFO_BUFFER[126];
  
	my $qi1ref = $oracle->select( table => 'Question_Record',
			select_columns => 'QUESTION_NUMBER, QUESTION_STRING1, QUESTION_STRING2, MIN_ANSWER_LENGTH, MAX_ANSWER_LENGTH, QUESTION_TYPE, TIMEOUT_LENGTH, TIMEOUT_ACTION, DEFAULT_ANSWER, NEW_QUESTION_TYPE, NEW_QUESTION_FUNCTION',
			where_columns => ['Question_Number = ?'],
			where_values => [$question1_number],
		); 
        my $qi2ref = $oracle->select( table => 'Question_Record',
                        select_columns => 'QUESTION_NUMBER, QUESTION_STRING1, QUESTION_STRING2, MIN_ANSWER_LENGTH, MAX_ANSWER_LENGTH, QUESTION_TYPE, TIMEOUT_LENGTH, TIMEOUT_ACTION, DEFAULT_ANSWER, NEW_QUESTION_TYPE, NEW_QUESTION_FUNCTION',
                        where_columns => ['Question_Number = ?'],
                        where_values => [$question2_number],
                );

        my $qi3ref = $oracle->select( table => 'Question_Record',
                        select_columns => 'QUESTION_NUMBER, QUESTION_STRING1, QUESTION_STRING2, MIN_ANSWER_LENGTH, MAX_ANSWER_LENGTH, QUESTION_TYPE, TIMEOUT_LENGTH, TIMEOUT_ACTION, DEFAULT_ANSWER, NEW_QUESTION_TYPE, NEW_QUESTION_FUNCTION',
                        where_columns => ['Question_Number = ?'],
                        where_values => [$question3_number],
                );




	@question1_info = @{$qi1ref->[0]} if $qi1ref->[0];
	@question2_info = @{$qi2ref->[0]} if $qi2ref->[0];
	@question3_info = @{$qi3ref->[0]} if $qi3ref->[0];


#CHANGED
   #@question1_info = &avc_Database_Get_Keyed_Record(58,$question1_number);
   #@question2_info = &avc_Database_Get_Keyed_Record(58,$question2_number);
   
   $question1_data = $question1_info[1].$rix_os_ENV{"SEPARATOR"}.$question1_info[2].$rix_os_ENV{"SEPARATOR"}.$question1_info[3].$rix_os_ENV{"SEPARATOR"}.$question1_info[4].$rix_os_ENV{"SEPARATOR"}.$question1_info[5].$rix_os_ENV{"SEPARATOR"}.$question1_info[6].$rix_os_ENV{"SEPARATOR"}.$question1_info[7].$rix_os_ENV{"SEPARATOR"}.$question1_info[8];
   $question2_data = $question2_info[1].$rix_os_ENV{"SEPARATOR"}.$question2_info[2].$rix_os_ENV{"SEPARATOR"}.$question2_info[3].$rix_os_ENV{"SEPARATOR"}.$question2_info[4].$rix_os_ENV{"SEPARATOR"}.$question2_info[5].$rix_os_ENV{"SEPARATOR"}.$question2_info[6].$rix_os_ENV{"SEPARATOR"}.$question2_info[7].$rix_os_ENV{"SEPARATOR"}.$question2_info[8];
   
   $CCU_SETTING_DATA[90] = $question1_data;
   $CCU_SETTING_DATA[91] = $question2_data;

   ########################
   #> New Questions 
   ######################## 
   $new_question1_data = $question1_info[1].$rix_os_ENV{"SEPARATOR"}.$question1_info[2].$rix_os_ENV{"SEPARATOR"}.$question1_info[3].$rix_os_ENV{"SEPARATOR"}.$question1_info[4].$rix_os_ENV{"SEPARATOR"}.$question1_info[9].$rix_os_ENV{"SEPARATOR"}.$question1_info[10].$rix_os_ENV{"SEPARATOR"}.$question1_info[6].$rix_os_ENV{"SEPARATOR"}.$question1_info[7].$rix_os_ENV{"SEPARATOR"}.$question1_info[8];
   $new_question2_data = $question2_info[1].$rix_os_ENV{"SEPARATOR"}.$question2_info[2].$rix_os_ENV{"SEPARATOR"}.$question2_info[3].$rix_os_ENV{"SEPARATOR"}.$question2_info[4].$rix_os_ENV{"SEPARATOR"}.$question2_info[9].$rix_os_ENV{"SEPARATOR"}.$question2_info[10].$rix_os_ENV{"SEPARATOR"}.$question2_info[6].$rix_os_ENV{"SEPARATOR"}.$question2_info[7].$rix_os_ENV{"SEPARATOR"}.$question2_info[8];

	$new_question3_data = $question3_info[1].$rix_os_ENV{"SEPARATOR"}.$question3_info[2].$rix_os_ENV{"SEPARATOR"}.$question3_info[3].$rix_os_ENV{"SEPARATOR"}.$question3_info[4].$rix_os_ENV{"SEPARATOR"}.$question3_info[9].$rix_os_ENV{"SEPARATOR"}.$question3_info[10].$rix_os_ENV{"SEPARATOR"}.$question3_info[6].$rix_os_ENV{"SEPARATOR"}.$question3_info[7].$rix_os_ENV{"SEPARATOR"}.$question3_info[8];


   $CCU_SETTING_DATA[92] = $new_question1_data;
   $CCU_SETTING_DATA[93] = $new_question2_data;
	$CCU_SETTING_DATA[94] = $new_question3_data;
   
   ########################
   #> Update rix_os_ENV for question info
   ########################	
   $rix_os_ENV{"QUESTION1_NUMBER"} = $question1_number;
   $rix_os_ENV{"QUESTION2_NUMBER"} = $question2_number;
	$rix_os_ENV{"QUESTION3_NUMBER"} = $question3_number;

}



########################################
#
# rix_os_Init_Ccu_FDC_Data
#
########################################
# Description: Fills CCU slots with information pertaining to First Data Corporation. Specifically, the
#              the following fields are filled
# 
#       120     FirstDataDeviceID
#	121     FirstDataMerchantID
#
# Uses Globals: @CCU_SETTING_DATA, %rix_os_ENV 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Ccu_FDC_Data
{
   $CCU_SETTING_DATA[120] = $rix_os_ENV{"MACHINE_ID"};
   $CCU_SETTING_DATA[121] = $rix_os_ENV{"MERCHANT_ID"};

}



########################################
#
# rix_os_Init_Vmc_Setting_Data
#
########################################
# Description: Initialize all VMC settings.
#
# @CCU_SETTING_DATA layout:
#
# 	1 	numOfColumns
# 	2 	columnIndexOrigin
# 	3 	>columnName:
# 		v3,<column number>,checksum
# 		returns <string>,checksuum
# 	4	>columnPrice:
# 		v4,<column number>,checksum
# 		return <integer>,checksum
# 	5	>columnInventoryId:
# 		v5,<column number>,checksum
# 		return <string>,checksum

# 
# Uses Globals: @VMC_SETTING_DATA, @MACHINE_TYPE_INFO_BUFFER, @PROMO_TEMPLATE_INFO_BUFFER, %rix_os_ENV 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Vmc_Setting_Data
{
   local (  $number_of_columns,
            %inventory_item_name_buffer,
            $column_name_list,
            $column_price_list,
            $column_inventory_id_list,
            $needs_max_price_override,
            $column_num,
            $curr_column_inventory_id,
            @inventory_item_info,
            $curr_column_name,
            $curr_column_price );

   ########################
   #> Mark @VMC_SETTING_DATA inited
   ########################
   $VMC_SETTING_DATA[0] = "Inited";	
   
   ########################
   #> 1 numOfColumns
   ########################
   $number_of_columns = $MACHINE_TYPE_INFO_BUFFER[10];
   $VMC_SETTING_DATA[1] = $number_of_columns;
   
   ########################
   #> 2 columnIndexOrigin
   ########################
   $VMC_SETTING_DATA[2] = $MACHINE_TYPE_INFO_BUFFER[11];
   
   ########################
   #> 3	columnName
   #> 4	columnPrice
   #> 5	columnInventoryId
   ########################
  if (rix_os_IsAPCMachine() ||
       rix_os_IsCRANEMachine())
   {
      my $dbh = new Evend::Database::Database();
      my $inv_array_ref =  $dbh->select( table          => "rerix_inventory A, TABLE(a.Inventory) B",
                                         select_columns => "b.Button_ID, b.Inventory_Item_Number",
                                         where_columns  => ["machine_id = ?"],
                                         where_values   => [$rix_os_ENV{'MACHINE_ID'}] );

      my $price_array_ref =  $dbh->select( table          => "rerix_pricing A, TABLE(a.Rerix_Price) B",
                                           select_columns => "b.Button_ID, b.Price",
                                           where_columns  => ["machine_id = ?" ],
                                           where_values   => [$rix_os_ENV{'MACHINE_ID'}] );

      foreach $button_ref (@$inv_array_ref)
      {
         my $inv_name_ref =  $dbh->select( table          => "inventory_item",
                                           select_columns => "Item_Description",
                                           where_columns  => ["inventory_item_number = ?"],
                                           where_values   => [$button_ref->[1]]);

         push(@{$VMC_hash{$button_ref->[0]}}, $inv_name_ref->[0][0]); # inv_name
         push(@{$VMC_hash{$button_ref->[0]}}, $button_ref->[1]); # inv_id
	 }
     $dbh->close();

      foreach $button_ref (@$price_array_ref)
      {
         push(@{$VMC_hash{$button_ref->[0]}}, $button_ref->[1]); # price
      }

      foreach $button (sort keys %VMC_hash)
      {
         ($inv_name, $inv_id, $inv_price) = @{$VMC_hash{$button}};

         $MDB_name = sprintf("%04d", $button);

         $MDB_name_list .= $MDB_name.$rix_os_ENV{"SEPARATOR"};
         $inv_name_list .= $inv_name.$rix_os_ENV{"SEPARATOR"};
         $inv_id_list   .= $inv_id.$rix_os_ENV{"SEPARATOR"};
         $inv_price_list.= $inv_price.$rix_os_ENV{"SEPARATOR"};
         $all_list      .= $inv_name.'|'.$inv_price.'|'.$inv_id.'|'.$MDB_name.$rix_os_ENV{"SEPARATOR"};

      }
      #print($MDB_name_list,"\n");
      #print($inv_name_list,"\n");
      #print($inv_id_list,"\n");
      #print($inv_price_list,"\n");

      # overwrite the num of columns
      $VMC_SETTING_DATA[1] = scalar(keys(%VMC_hash));

      $VMC_SETTING_DATA[3] = $inv_name_list;
      $VMC_SETTING_DATA[4] = $inv_price_list;
      $VMC_SETTING_DATA[5] = $inv_id_list;
      $VMC_SETTING_DATA[6] = $MDB_name_list;
      $VMC_SETTING_DATA[7] = $all_list;
   }
else
{




   %inventory_item_name_buffer = ();
   $column_name_list = "";
   $column_price_list = "";
   $column_inventory_id_list = "";
   
   $needs_max_price_override = $MACHINE_TYPE_INFO_BUFFER[2];
   
   for ($column_num = 1;$column_num <= $number_of_columns;$column_num++)
   {
       $curr_column_inventory_id = $PROMO_TEMPLATE_INFO_BUFFER[(44+$column_num-1)];
       
       if ($curr_column_inventory_id ne "")
       {
               if ($inventory_item_name_buffer{$curr_column_inventory_id} eq "")
               {
                       # Fill %inventory_info_buffer
		$iiiref = $oracle->select( table => 'Inventory_Item',
			select_columns => 'INVENTORY_ITEM_NUMBER,ITEM_DESCRIPTION,0,0,0',
			where_columns => ['Inventory_Item_Number = ?'],
			where_values => [$curr_column_inventory_id],
			);
		@inventory_item_info = @{$iiiref->[0]};

#CHANGED
#                       @inventory_item_info = &avc_Database_Get_Keyed_Record(24,$curr_column_inventory_id);
                       $inventory_item_name_buffer{$curr_column_inventory_id} = $inventory_item_info[1];
               }
               
               $curr_column_name = $inventory_item_name_buffer{$curr_column_inventory_id};
       }
       else
       {	
               $curr_column_name = "ITEM ".$column_num;
       }
       
       # Does it need max price override?
       if ($needs_max_price_override)
       {
               $curr_column_price = $PROMO_TEMPLATE_INFO_BUFFER[(14+$column_num-1)]*100; # assuming that the prices are set in dollar format on server;
       }
       else
       {
               $curr_column_price = "";
       }
      
       $column_name_list .= $curr_column_name.$rix_os_ENV{"SEPARATOR"};
       $column_price_list .= $curr_column_price.$rix_os_ENV{"SEPARATOR"};
       $column_inventory_id_list .= $curr_column_inventory_id.$rix_os_ENV{"SEPARATOR"};
$column_MDB_name_list .=  $column_num.$rix_os_ENV{"SEPARATOR"};
$all_list .= $curr_column_name.'|'.$curr_column_price.'|'.$curr_column_inventory_id.'|'.sprintf("%04X", $column_num-(1-$VMC_SETTING_DATA[2])).$rix_os_ENV{"SEPARATOR"};

   }

   
	chop($column_name_list = $column_name_list);
	chop($column_price_list = $column_price_list);
	chop($column_inventory_id_list = $column_inventory_id_list);
   
   $VMC_SETTING_DATA[3] = $column_name_list;
   $VMC_SETTING_DATA[4] = $column_price_list;
   $VMC_SETTING_DATA[5] = $column_inventory_id_list;
      $VMC_SETTING_DATA[6] = $column_MDB_name_list;
      $VMC_SETTING_DATA[7] = $all_list;
        }

}





########################################
#
# rix_os_Init_Swipe_Setting_Data
#
########################################
# Description: Initialize all swipe settings on the client.
#
# @SWIPE_SETTING_DATA layout:
#
# 	1 	trackNumber
# 	2	pinEnabled
# 
# Uses Globals: @SWIPE_SETTING_DATA, @MACHINE_TEMPLATE_INFO_BUFFER 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Swipe_Setting_Data
{
   ########################
   #> Mark @SWIPE_SETTING_DATA inited
   ########################
   $SWIPE_SETTING_DATA[0] = "Inited";
   
   ########################
   #> 1 trackNumber
   ########################
   $SWIPE_SETTING_DATA[1] = $MACHINE_TEMPLATE_INFO_BUFFER[2];
   
   ########################
   #> 2	pinEnabled (unused)
   ########################
   $SWIPE_SETTING_DATA[2] = 0;

}
   
   
   
########################################
#
# rix_os_Init_Receipt_Setting_Data
#
########################################
# Description: Initialize all receipt settings on the client.
#
# @RECEIPT_SETTING_DATA layout:
#
# 	1	parReceiptAskQuestion
# 	2	parReceiptLoser
# 	3	receiptCoupon
# 	4	receiptInfo
# 
# Uses Globals: @RECEIPT_SETTING_DATA, @MACHINE_TEMPLATE_INFO_BUFFER, %rix_os_ENV 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Receipt_Setting_Data
{
   local (  $receipt_coupon_data,
            @receipt_coupon_list,
            $receipt_coupon_item,
            $receipt_info_data,
            @receipt_info_list,
            $receipt_info_item,
            $receipt_coupon_item_adjusted,
            $receipt_info_item_adjusted );


   ########################
   #> Mark @RECEIPT_SETTING_DATA inited
   ########################
   $RECEIPT_SETTING_DATA[0] = "Inited";
               
   ########################
   #> 1	parReceiptAskQuestion
   ########################
   $RECEIPT_SETTING_DATA[1] = $MACHINE_TEMPLATE_INFO_BUFFER[114];
   
   ########################
   #> 2	parReceiptLoser
   ########################
   $RECEIPT_SETTING_DATA[2] = $MACHINE_TEMPLATE_INFO_BUFFER[115];
   
   ########################
   #> 3	receiptCoupon
   ########################
   $receipt_coupon_data = "";
   @receipt_coupon_list = split(/\n/,$MACHINE_TEMPLATE_INFO_BUFFER[54]);
   foreach $receipt_coupon_item (@receipt_coupon_list)
   {
       $receipt_coupon_item_adjusted = &rix_os_Fill_In_Message_Info($receipt_coupon_item);
       $receipt_coupon_data .= $receipt_coupon_item_adjusted.$rix_os_ENV{"SEPARATOR"};
   }
       
	chop($receipt_coupon_data = $receipt_coupon_data);
   
   $RECEIPT_SETTING_DATA[3] = $receipt_coupon_data;
   
   ########################
   #> 4	receiptInfo
   ########################
   $receipt_info_data = "";
   @receipt_info_list = split(/\|/,$MACHINE_TEMPLATE_INFO_BUFFER[55]);
   foreach $receipt_info_item (@receipt_info_list)
   {	
       $receipt_info_item_adjusted = &rix_os_Fill_In_Message_Info($receipt_info_item);
       $receipt_info_data .= $receipt_info_item_adjusted.$rix_os_ENV{"SEPARATOR"};
   }
   
	chop($receipt_info_data = $receipt_info_data);
   
   $RECEIPT_SETTING_DATA[4] = $receipt_info_data;

}





########################################
#
# rix_os_Init_Ccu_Status_Data
#
########################################
# Description: Initializes the CCU status buffer and the CCU status record layout.
#
# @RECEIPT_SETTING_DATA layout:
#
# 	1	parReceiptAskQuestion
# 	2	parReceiptLoser
# 	3	receiptCoupon
# 	4	receiptInfo
# 
# Uses Globals: @CCU_STATUS_BUFFER, CCU_STATUS_RECORD_LAYOUT, @MACHINE_INFO_BUFFER, 
#               @LOCATION_INFO_BUFFER, %rix_os_ENV 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_Ccu_Status_Data
{
   local (  $curr_local_date,
            $curr_local_time,
            @tmp_ccu_status_record_layout );


   ####################################################################################
   # Init @CCU_STATUS_BUFFER and %CCU_STATUS_RECORD_LAYOUT
   ####################################################################################	
   #@CCU_STATUS_BUFFER = ();
   # $curr_local_date, $curr_local_time, $expected_dialup_date & $expected_dialup_time 
   # were created when filling in CCU_SETTING_DATA.
   # Re-use them here:

	my $mshhbref = $oracle->select( table => 'dual',
		select_columns => 'MACHINE_STATUS_SEQ.nextval',);

$CCU_STATUS_BUFFER[0] = $mshhbref->[0][0];	

#CHANGED		
   #$CCU_STATUS_BUFFER[0] = &avc_Database_Get_Record_Number(51,$rix_os_ENV{"MACHINE_ID"});
   
   ($curr_local_date,$curr_local_time) = split(/ /,$CCU_SETTING_DATA[70]);
   
   $CCU_STATUS_BUFFER[1] = $curr_local_date;  
   $CCU_STATUS_BUFFER[2] = $curr_local_time;
   $CCU_STATUS_BUFFER[3] = $rix_os_ENV{"EXPECTED_DIALUP_DATE"};  
   $CCU_STATUS_BUFFER[4] = $rix_os_ENV{"EXPECTED_DIALUP_TIME"};
   $CCU_STATUS_BUFFER[5] = $LOCATION_INFO_BUFFER[0];
   $CCU_STATUS_BUFFER[6] = $MACHINE_INFO_BUFFER[1];
   $CCU_STATUS_BUFFER[7] = $MACHINE_INFO_BUFFER[33];
   
      @tmp_ccu_status_record_layout = (    "1:1","10",
                                        "1:2","11",
                                        "1:3","12",
                                        "1:4","13",
                                        "1:5","14",
                                        "1:6","15",
                                        "1:7","16",
                                        "1:8","17",
                                        "1:9","18",
                                        "1:10","19",
                                        "1:11","9",
                                        "1:12","24",
                                        "1:13","25",
                                        "1:14","26",
                                        "1:15","27",
					"1:16","34",
					"1:17","33",
                                        "2:1","20",
                                        "2:2","21",
                                        "2:3","8");
 
   %CCU_STATUS_RECORD_LAYOUT = @tmp_ccu_status_record_layout;
   
}




########################################
#
# rix_os_Init_TransAuth_Login_Data
#
########################################
# Description: Initializes all the data that will be required by the client in order to successfully 
#              conduct a RIX session for authorizing a transaction through the server.
#
#
# Uses Globals: @CCU_SETTING_DATA, @VMC_SETTING_DATA 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_TransAuth_Login_Data
{
   # initialize the database records (necessary for transaction authorization login) associated with 
   # the current client
   &rix_os_Init_TransAuth_Login_Database_Lists();

   # initialize all one-use specific data
   &rix_os_Init_TransAuth_Login_Ccu_Promo_Data();

   # update current local date and time in @CCU_SETTING_DATA
   &rix_os_Update_Ccu_Setting_Date_And_Time();

   # Initialize machine-specific transaction_GLOBAL variables
   # mastercode
   $rix_transaction_GLOBAL{"MASTERCODE"} = $MACHINE_TEMPLATE_INFO_BUFFER[39];

}




########################################
#
# rix_os_Init_TransAuth_Login_Database_Lists
#
########################################
# Description: Fill global database lists needed by the login script during transaction auth login. Creates
#              1) MACHINE_TEMPLATE_INFO_BUFFER [48]
#              2) LOCATION_INFO_BUFFER [18]
#
# NOTE: MACHINE_INFO_BUFFER is created in rix_os_Init_Session.
#       
# Uses Globals: %rix_os_ENV, @MACHINE_TEMPLATE_INFO_BUFFER, @LOCATION_INFO_BUFFER
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_TransAuth_Login_Database_Lists
{
   local ( $machine_template_number );
   
   $machine_template_number = $MACHINE_INFO_BUFFER[1];
  
#not the important machine_template_Record select 
   # machine template is of record type 48 in AVC database
	$mtib3ref = $oracle->select( table => 'Machine_Template_Record',
		select_columns => 'MACHINE_TEMPLATE_NUMBER, MACHINE_TEMPLATE_NAME, CARD_TRACK_TYPE, AUTHORIZATION_TYPE, DAILY_ACCOUNTING_TYPE, ONE_USE_PROMOTIONS, AUTH_PHONE, FIRST_DATA_PHONE, RIX_PHONE, PRINTER_USED, CANCEL_USED, INVENTORY_CARD_ENABLED, LOCAL_AUTH_VEND_LIMIT, AUTH_MODEM_SPEED, MSG_AUTH_BAD, MSG_AUTH_CARD, MSG_AUTH_OK, GATEKEEPER_NEEDED, MGS_GATEKEEPER, MSG_AUTH_LIMIT, PING_INTERVAL, UNUSED_FIELD, UNUSED_FIELD1, UNUSED_FIELD2, MSG_AVC_BUSY_PLEASE_WAIT, MSG_AVC_CLOSE_BATCH, UNUSED_FIELD3, UNUSED_FIELD4, UNUSED_FIELD5, UNUSED_FIELD6, UNUSED_FIELD7, UNUSED_FIELD8, UNUSED_FIELD9, UNUSED_FIELD10, MSG_CARD_INSERT, MSG_CARD_REMOVE, MSG_CARD_CANT_READ, MSG_CARD_WRONG_TYPE, MSG_CARD_INVALID, CARD_MASTER_CODE, UNUSED_FIELD11, UNUSED_FIELD12, UNUSED_FIELD13, UNUSED_FIELD14, MSG_INVENTORY_ERROR_CLOSE, MSG_INVENTORY_INIT, MSG_INVENTORY_PLEASE_WAIT, MSG_INVENTORY_TRY_AGAIN, MSG_INVENTORY_UPDATE_COMPLETE, MSG_INVENTORY_CODE, UNUSED_FIELD15, UNUSED_FIELD16, UNUSED_FIELD17, UNUSED_FIELD18, MSG_RECEIPT_COUPON, MSG_RECEIPT_INFO, MSG_RECEIPT_PRINTING, MSG_RECEIPT_QUESTION, MSG_RECEIPT_TAKE, MSG_RECEIPT_NO_PAPER, UNUSED_FIELD19, UNUSED_FIELD20, UNUSED_FIELD21, UNUSED_FIELD22, MSG_SYSTEM_BUSY_PLEASE_WAIT, MSG_SYSTEM_BUSY_TRY_AGAIN, MSG_SYSTEM_FATAL_ERROR, MSG_SYSTEM_FLASH_INIT, MSG_SYSTEM_SELF_TEST, MSG_SYSTEM_OUT_OF_SERVICE, MSG_SYSTEM_NO_DIALTONE, MSG_SYSTEM_PHONE_PREFIX, UNUSED_FIELD23, UNUSED_FIELD24, MSG_VEND_CANCEL, MSG_VEND_SELECT, MSG_VEND_LOSER, MSG_VEND_VENDING, UNUSED_FIELD25, UNUSED_FIELD26, UNUSED_FIELD27, UNUSED_FIELD28, UNUSED_FIELD29, UNUSED_FIELD30, QUESTION1, QUESTION2, UNUSED_FIELD31, UNUSED_FIELD32, UNUSED_FIELD33, UNUSED_FIELD34, UNUSED_FIELD35, UNUSED_FIELD36, UNUSED_FIELD37, UNUSED_FIELD38, UNUSED_FIELD39, UNUSED_FIELD40, UNUSED_FIELD41, UNUSED_FIELD42, UNUSED_FIELD43, UNUSED_FIELD44, UNUSED_FIELD45, UNUSED_FIELD46, UNUSED_FIELD47, UNUSED_FIELD48, UNUSED_FIELD49, UNUSED_FIELD50, UNUSED_FIELD51, UNUSED_FIELD52, UNUSED_FIELD53, UNUSED_FIELD54, UNUSED_FIELD55, UNUSED_FIELD56, UNUSED_FIELD57, UNUSED_FIELD58, PAR_RECEIPT_ASK_QUESTION, PAR_RECEIPT_ALWAYS, UNUSED_FIELD59, UNUSED_FIELD60, UNUSED_FIELD61, PRICE_INCLUDES_TAX, CREDIT_CARD_ACCEPTED, AUTH_SWITCH_TIMEOUT, VEND_TIMEOUT, CARD_SERVICE_CODE,MAX_VEND_PER_SWIPE,FOB_MIN_DURATION',
		where_columns => ['Machine_Template_Number = ?'],
		where_values => [$machine_template_number],
		);

@MACHINE_TEMPLATE_INFO_BUFFER = @{$mtib3ref->[0]};

#CHANGED
   #@MACHINE_TEMPLATE_INFO_BUFFER = &avc_Database_Get_Keyed_Record(48,$machine_template_number);
   
   # location information is of record type 18 in AVC database

	$lib2ref = $oracle->select( table => 'Location',
		select_columns => 'LOCATION_NUMBER,LOCATION_NAME,0,STREET_ADDRESS_1,STREET_ADDRESS_2,CITY,STATE,ZIP,COUNTRY,PHONE,FAX,TAX_PERCENTAGE,TIME_ZONE,0,0,0,ROYALTY_RENT_PERCENTAGE,LOCATION_CONTACT_PERSON,0,0,0,DIRECTIONS,0,OPENS,CLOSES,FILL_CONTACT_PERSON,SERVICE_COMPANY_CONTACT_PERSON,NOTES,TAX_TYPE,COST_OF_GOODS_PERC',
		where_columns => ['Location_Number = ?'],
		where_values => [$MACHINE_INFO_BUFFER[6]],
		);
	@LOCATION_INFO_BUFFER = @{$lib2ref->[0]};
# CHANGED
   #@LOCATION_INFO_BUFFER = &avc_Database_Get_Keyed_Record(18,$MACHINE_INFO_BUFFER[6]);
}



########################################
#
#  rix_os_Init_TransAuth_Login_Ccu_Promo_Data
#
########################################
# Description: Valid prefixes for a machine are stored in the CCU. In addition, %ONE_USE_PREFIX2PROMO is
#              created. 
#
# 	        100	Valid Prefixes
# 		c100,<valid prefix number (1-5)>,checksum
# 		return <valid prefix>,checksum
# 		NOTE: Assume will always read prefixes 1-5        
#
#
# Uses Globals: @CCU_SETTING_DATA, %ONE_USE_PREFIX2PROMO 
# 
# Written By: Salim Khan
# Created: 11/15/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_os_Init_TransAuth_Login_Ccu_Promo_Data
{
   
   local (  @one_use_promotions,
            @valid_prefixes,
            $server_probability,
            $one_use_promotion_num,
            @one_use_promotion_info,
            $one_use_server_probability,
            @one_use_prefixes,
            $valid_prefix_data,
            $valid_one_use_prefix,
            %prefix_to_promo,
            %prefix_to_DBtable );
   

   # the split SHOULD take place at the commas instead of the \0 character.
   # @one_use_promotions = split(/\0/,$machine_template_info[5]);
   @one_use_promotions = split(/,/,$MACHINE_TEMPLATE_INFO_BUFFER[5]);
   
   @valid_prefixes = ();
   $server_probability = 0;
   %prefix_to_promo = ();
   %prefix_to_DBtable = ();
   
   foreach $one_use_promotion_num (@one_use_promotions)
   {
	$oupi2ref = $oracle->select( table => 'One_Use_Promo_Record',
				select_columns => 'ONE_USE_PROMO_NUMBER, ONE_USE_PROMO_NAME, INIT_DAILY, PREFIXES, CLIENT_PROBABILITY, LOSER_HAS_NO_DATA, SERVER_PROBABILITY, TABLE_NUMBER, TABLE_NAME',
				where_columns => ['One_Use_Promo_Number = ?'],
				where_values => [$one_use_promotion_num],
			);
	@one_use_promotion_info = @{$oupi2ref->[0]};
	# CHANGED
          #@one_use_promotion_info = &avc_Database_Get_Keyed_Record(49,$one_use_promotion_num);
          $one_use_server_probability = $one_use_promotion_info[6];
          $promo_DBtable = $one_use_promotion_info[7];
                          
          @one_use_prefixes = split(/\|/,$one_use_promotion_info[3]);
          
          foreach $one_use_prefix (@one_use_prefixes)
          {
                  if (!grep (/$one_use_prefix/, @valid_prefixes))
                  {
                          push(@valid_prefixes,$one_use_prefix);
   
                          $prefix_to_promo{$one_use_prefix} = $one_use_promotion_num;
                          $prefix_to_DBtable{$one_use_prefix} = $promo_DBtable;
                  }
          }
   
          # Take highest probability
          if ($one_use_server_probability > $server_probability)
          {
                  $server_probability = $one_use_server_probability;
          }
   }
   
   #&dump_List(@valid_prefixes);
   
   #$valid_prefix_data = "";
   #foreach $valid_one_use_prefix (@valid_prefixes)
   #{
   #       $valid_prefix_data .= $valid_one_use_prefix.$rix_os_ENV{"SEPARATOR"};
   #}
	# chop($valid_prefix_data = $valid_prefix_data);
   #$CCU_SETTING_DATA[100] = $valid_prefix_data;

   # Update server probability
   $rix_os_ENV{"SERVER_PROBABILITY"} = $server_probability;

   # We probably DONT need this
   $rix_transaction_GLOBAL{"PREFIX_TO_PROMO_REF"} = [ %prefix_to_promo ];

   $rix_transaction_GLOBAL{"PREFIX_TO_DBTABLE_REF"} = [ %prefix_to_DBtable ];
}

sub pad_String
{
        local(  $string_in,
                        $pad_depth,
                        $string_out,
                        $add_padding,
                        $pad_counter,
                        $pad_with,
                        $chop_string,
                        $pad_on_right);

        $string_in = $_[0];
        $pad_depth = $_[1];
        $pad_with = $_[2];
        if ($pad_with eq "")
        {
                $pad_with = " ";
        }
        $chop_string = $_[3];
        if ($chop_string eq "")
        {
                $chop_string = 0;
        }
        $pad_on_right = $_[4];
        if ($pad_on_right eq "")
        {
                $pad_on_right = 1;
        }

        $string_out = $string_in;
        if (length($string_out) <= $pad_depth)
        {
                $add_padding = $pad_depth-(length($string_in));
                for ($pad_counter=0;$pad_counter < $add_padding;$pad_counter++)
                {
                        if ($pad_on_right)
                        {
                                $string_out .= $pad_with;
                        }
                        else
                        {
                                $string_out = $pad_with.$string_out;
                       }
                }

        }
        else
        {
                if ($chop_string)
                {
                        $string_out = substr($string_in,0,$pad_depth);
                }
        }

        return $string_out;
}

sub get_Date
{

        local(  @currtimeinfo,
                        $currdate);

        @currtimeinfo = localtime(time);

        $currtimeinfo[5] += 1900;

        $currdate=((sprintf("%02d",$currtimeinfo[4])+1)) . "/" . sprintf("%02d",$currtimeinfo[3]) . "/" . $currtimeinfo[5];


        return $currdate;
}

sub get_Time
{

        local(  @currtimeinfo,
                        $currtime);

        @currtimeinfo = localtime(time);
        if ($currtimeinfo[1] < 10)
        {
                $currtime=$currtimeinfo[2] . ":0" . $currtimeinfo[1];
        }
        else
        {
                $currtime=$currtimeinfo[2] . ":" . $currtimeinfo[1];
        }

        return $currtime;

}


sub avc_Date_To_Num
{
        local(  @date_split,
                        $date_num,
                        $date_in,
                        $dont_fix_date);

        $fix_date = $_[1];
        if ($dont_fix_date)
        {
                $date_in = $_[0];
        }
        else
        {
                $date_in = $_[0];
        }

        @date_split = split(/\//,$date_in);

        $date_num = $date_split[2].$date_split[0].$date_split[1];
        return $date_num;
}

sub avc_Date_eq
{
        local(  $date_a,
                        $date_b);

        $date_a = $_[0];
        $date_b = $_[1];

        if (&avc_Date_To_Num($date_a) == &avc_Date_To_Num($date_b))
        {
                return 1;
        }
        else
        {
                return 0;
        }

}

sub rix_os_Get_Tax_Setting_Data
{
   my($data_item_num);

   $data_item_number = $_[0];

   return $TAX_SETTING_DATA[$data_item_number];
}

sub mysql_date_format
{

	($slwdate) = @_;
	($slwmon,$slwday,$slwyear) = split /\//, $slwdate;
	return join("-", ($slwyear,$slwmon,$slwday));
	
}

sub reformat_enums
{
       ($char_in) = @_;

        return "1" if $char_in eq "Y";
        return "0" if $char_in eq "N";
	return "Y" if $char_in eq "1";
	return "N" if $char_in eq "0";
	return "0";
}

sub rix_os_Init_Tax_Setting_Data
{
   ######## SALES TAX TYPE ########
   ## 1 = tax from total
   ## 2 = tax on sale
   $TAX_SETTING_DATA[1] =  $LOCATION_INFO_BUFFER[28];


   ######## SALES TAX RATE ########
   $TAX_SETTING_DATA[2] =  $LOCATION_INFO_BUFFER[11];


   ######## COST OF GOODS ########
   $TAX_SETTING_DATA[3] =  $LOCATION_INFO_BUFFER[29];
}

sub avc_Date_To_Num
{
        local(  @date_split,
                        $date_num,
                        $date_in,
                        $dont_fix_date);

        $fix_date = $_[1];
        if ($dont_fix_date)
        {
                $date_in = $_[0];
        }
        else
        {
                $date_in = $_[0];
        }

        @date_split = split(/\//,$date_in);

        $date_num = $date_split[2].$date_split[0].$date_split[1];
        return $date_num;

}

sub avc_Time_To_Num
{
        local(  @time_split,
                        $time_num,
                        $time_in,
                        $dont_fix_time);

        $fix_time = $_[1];
        if ($dont_fix_time)
        {
                $time_in = $_[0];
        }
        else
        {
                $time_in = $_[0];
        }


        @time_split = split(/:/,$time_in);

        $time_num = $time_split[0].$time_split[1];

        return $time_num;
}

sub avc_Time_lt
{
        local(  $time_a,
                        $time_b);

        $time_a = $_[0];
        $time_b = $_[1];

        if (&avc_Time_To_Num($time_a) < &avc_Time_To_Num($time_b))
        {
                return 1;
        }
        else
        {
                return 0;
        }

}

sub avc_Date_eq
{
        local(  $date_a,
                        $date_b);

        $date_a = $_[0];
        $date_b = $_[1];

        if (&avc_Date_To_Num($date_a) == &avc_Date_To_Num($date_b))
        {
                return 1;
        }
        else
        {
                return 0;
        }

}

sub slw_date_format
{
	($date_in) = @_;
	($myear,$mmon,$mday) = split /\-/, $date_in;
	$outstring = "$mmon/$mday/$myear";
	return $outstring;
}

sub mysql_time_format
{
	($time_in) = @_;
	($mhour,$mminute) = split /:/, $time_in;
	$outstring = "$mhour:$mminute:00";
	return $outstring;
}

sub slw_time_format
{

	($time_in) = @_;
	($shour,$sminute,$ssecond) = split /:/, $time_in;
	$outstring = "$shour:$sminute";
	return $outstring;
}

sub time_To_Num
{
        local(  @time_split,
                        $time_num,
                        $time_in);

        $time_in = $_[0];


        $time_in = &time_Fix($time_in);

        @time_split = split(/:/,$time_in);

        $time_num = $time_split[0].$time_split[1];

        return $time_num;
}

sub time_Fix
{
        local(  $time_in,
                        $time_out,
                        @time_split);

        $time_in = $_[0];


        @time_split = split(/:/,$time_in);

        if ( ($time_split[0] < 10) && (length($time_split[0]) < 2) )
        {
                $time_split[0] = "0".$time_split[0];
        }

        if ( ($time_split[1] < 10) && (length($time_split[1]) < 2) )
        {
                $time_split[1] = "0".$time_split[1];
        }

        $time_out = $time_split[0].":".$time_split[1];


        return $time_out;

}

sub oracle_date_format
{

        my $date_in = shift;
        my($oy,$om,$od) = split /-/, $date_in;
        return "$om/$od/$oy";


}

sub rix_os_IsAPCMachine
{
   if ($MACHINE_TYPE_INFO_BUFFER[12] == 1)
   {
      return 1;
   }
   else
   {
      return 0;
   }
}

sub rix_os_IsCRANEMachine
{
   if ($MACHINE_TYPE_INFO_BUFFER[12] == 2)
   {
      return 1;
   }
   else
   {
      return 0;
   }
}

1; #return true
