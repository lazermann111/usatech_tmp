#!/opt/bin/perl
use Evend::Database::Database;
#################################################################
#
# rix_inventory_card-lib.pl
#
#################################################################
#
# Written By: Scott Wasserman
# Created: 9/28/1999
#
# (c)1999 e-Vend.net Corporation
#
#
#
#################################################################
# Functions Availible:
#################################################################
#
#
#################################################################
# Required Libraries
#################################################################
# require("avcbridge_database_query-lib.pl");

#################################################################
# Setup
#################################################################

#################################################################
# Globals 
#################################################################



########################################
#
# rix_inventory_card_Get_New_Promotion_Name
#
########################################
# Description: Get inventory card prefix for current machine
#
# Uses Globals: @CCU_SETTING_DATA
# 
# Written By: Scott Wasserman
# Created: 9/28/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 
########################################
sub rix_inventory_card_Get_New_Promotion_Name
{
	local(	$promotion_name);
	
	$promotion_name = $CCU_SETTING_DATA[111];
	
	return $promotion_name;
}

########################################
#
# rix_inventory_card_Get_Prefix
#
########################################
# Description: Get inventory card prefix for current machine
#
# Uses Globals: @CCU_SETTING_DATA
# 
# Written By: Scott Wasserman
# Created: 9/28/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 2/29/2000    SAK           Made adjustment for "B"(format code) in inv card
########################################
sub rix_inventory_card_Get_Report_Number_From_Card_Number
{
	local(	$inventory_card_number,
			$inventory_prefix_size,
			$card_number_size,
			$report_number_size,
			$report_number);
		
	$inventory_card_number = $_[0];
	
	$inventory_prefix =  $CCU_SETTING_DATA[35];
	$inventory_prefix_size = length($inventory_prefix); # 1 added to adjust for card format code
	$card_number_size = length($inventory_card_number);
	$report_number_size = $card_number_size - $inventory_prefix_size;
	$report_number = substr($inventory_card_number,$inventory_prefix_size,$report_number_size);
		
	return $report_number;
}

################################################################################
#>
#> This needs to be rewritten at some point
#>
################################################################################
########################################
#
# rix_inventory_card_Process (modified from avc_Swap_Template, do not use in its place)
#
# NOTE: Uses @MACHINE_INFO_BUFFER, @CCU_STATUS_BUFFER
#
# NOTE: previously used @global_machine_data, $expected_dialup_date, $expected_dialup_time
#			 $local_dialup_date, $local_dialup_time
#       previously modified @machine_template_data & @global_machine_data
# 
########################################
# Description:
# 
# Created: 10/02/1998
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
# 12/7/1998    	SLW			Removal history had wrong template
# 1/4/1999    	SLW			$today is passed as a parameter to allow a manual swap 
#							 to happen at a later date if it is forgetten
# 1/8/1999		SLW			Now returns $return_status to tell whether there was anything to swap
# 6/3/1999		SLW			Now updates the machine template [48] when updating the promo template
#							Saves a machine status history [51] instead of dialup history [8]
#							No longer saves a manager swipe log [34] the info is now in 
#							 machine status history [51]
#							Added $raw_card_number as input
# 9/28/1999		SLW			Altered to temporarily work in RIX
#
########################################
sub rix_inventory_card_Process
{
	local(	@fill_history,
			$last_fill_info_packed,
			@last_fill_info,
			$machine_num,
			$today,
			$column_num,
			$fill_counter,
			@fill_list,
			$fill_type,
			$inventory_num,
			@last_template_history,
			$last_template_history_packed,
			@machine_status_list,
			@new_template_history,
			@removal_history,
			@remove_list,
			$status_list_packed,
			@status_list,
			@template_history,
			$current_template_num,
			@current_template_info,
			$promo_template_log_record_num,
			$curr_time,
			$template_removed,
			$return_status,
			$report_number,
			$data_number,
			$raw_card_number);


	$machine_num = $MACHINE_INFO_BUFFER[0];
	$today = $_[0];
	$report_number = $_[1];
	$raw_card_number = $_[2];
	
	$curr_time = &get_Time();
	
	########################################
	# Save Status Record
	########################################
	
	# Clear expected Date & Time
	$CCU_STATUS_BUFFER[3] = "";
	$CCU_STATUS_BUFFER[4] = "";
	
	for ($data_number = 8;$data_number <= 20;$data_number++)
	{
		$CCU_STATUS_BUFFER[$data_number] = "";
	}

	if ($raw_card_number =~ "MAN")
	{
		# Dial in type of 7 - Manual Template Swap
		$CCU_STATUS_BUFFER[21] = "7"; 	
	}
	else
	{
		# Dial in type of 6 - Inventory Card Swipe
		$CCU_STATUS_BUFFER[21] = "6"; 
	}
	
		
	# Mark Inventory Card Number
	$CCU_STATUS_BUFFER[22] = $raw_card_number;
	
	
	########################################
	# Find Last Fill Information
	########################################

	       $middbcheck = $rix_os_ENV{"MACHINE_ID"};
        if (!$dbhash{$middbcheck})
        {
                $dbhash{$middbcheck} = Evend::Database::Database->new(print_que
ry => 1);
        }
	$firef = $dbhash{$middbcheck}->select(select_columns => '*',
			table => 'Fill_History',
		);

	@fill_history = @{$firef->[0]};
	#@fill_history = &avc_Database_Get_Record_List(4,$machine_num);
	@fill_history = reverse(@fill_history);

	if (scalar(@fill_history) > 0)
	{
		if ($report_number eq "")
		{
		
			#######################################
			# No report number sent so just grab
			# the last unfilled inventory in queue
			#######################################
			$not_found = 1;
			$history_counter = 0;
			while ( ($history_counter < scalar(@fill_history)) && ($not_found) )
			{
				$tmp_last_fill_info_packed = $fill_history[$history_counter];
				@tmp_last_fill_info = &avc_Database_Unpack_Data(4,$tmp_last_fill_info_packed);
		
				if ($tmp_last_fill_info[15] ne "")
				{
					$not_found = 0;
					$template_removed = @tmp_last_fill_info[3];
				}
	
				$history_counter++;
			}
			
			if ($not_found)
			{
				if ($history_counter == scalar(@fill_history))
				{
					$last_fill_info_packed = pop(@fill_history);
					@last_fill_info = &avc_Database_Unpack_Data(4,$last_fill_info_packed);
				}
				else
				{
					$last_fill_info_packed = "";
				}
			}
			else
			{
				if (($history_counter-2) < 0)
				{
					$last_fill_info_packed = "";
				}
				else
				{
					$last_fill_info_packed = $fill_history[$history_counter-2];
					@last_fill_info = &avc_Database_Unpack_Data(4,$last_fill_info_packed);
				}
			}
		}
		else
		{
			#######################################
			# Search for report number
			#######################################
			$not_found = 1;
			$history_counter = 0;
			while ( ($history_counter < scalar(@fill_history)) && ($not_found) )
			{
				$tmp_last_fill_info_packed = $fill_history[$history_counter];
				@tmp_last_fill_info = &avc_Database_Unpack_Data(4,$tmp_last_fill_info_packed);
		
				#print $tmp_last_fill_info[18]." = ".$report_number."\n";
				if ( ($tmp_last_fill_info[18] eq $report_number) && ($tmp_last_fill_info[15] eq "") )
				{
					$not_found = 0;
					if ($history_counter < scalar(@fill_history))
					{
						$template_removed_info_packed = $fill_history[$history_counter+1];
						@template_removed_info = &avc_Database_Unpack_Data(4,$template_removed_info_packed);
						$template_removed = @template_removed_info[3];
					}
					else
					{
						$template_removed = "";
					
					}
				}
	
				$history_counter++;
			}
		
			if ($not_found)
			{
				$last_fill_info_packed = "";
			}
			else
			{
				
				$last_fill_info_packed = $tmp_last_fill_info_packed;
				@last_fill_info = @tmp_last_fill_info;
			}
		
		}
		
	}
	else
	{
		$last_fill_info_packed = "";
	}

	if ( ($last_fill_info_packed ne "") && ($last_fill_info[15] eq "") )
	{
		# Fill Report Information
		@fill_report_info = &avc_Database_Get_Keyed_Record(44,$last_fill_info[18]);
		
		# Vending Inventory Addition Information
		@vending_inventory_addition_info = &avc_Database_Get_Keyed_Record(45,$fill_report_info[14]);
		
		# Current template Information
		$current_template_num = $last_fill_info[3];
		@current_template_info = &avc_Database_Get_Keyed_Record(11,$current_template_num);
		
		# Current fill information
		$fill_type = $last_fill_info[14];
		@fill_list = ();
		for ($fill_counter = 0;$fill_counter < 10;$fill_counter++)
		{
			push(@fill_list,$last_fill_info[4+$fill_counter]);
		}
		
		########################################
		# Update Promo Template
		########################################
		$MACHINE_INFO_BUFFER[4] = $last_fill_info[3];
		
		########################################
		# Update Machine Template
		########################################
		$current_machine_template = $current_template_info[56];
		

# removed by SAK - 2/10/2000 on instructions from Scott 		
#                 if ($raw_card_number =~ "MAN")
#                 {
#                         # Set the init flag in the database for next
#                         @MACHINE_INFO_BUFFER[30] = 1;
#                 }
#                 else
#                 {
#                         # Don't set the init flag we are going to init in this session
#                         $MACHINE_INFO_BUFFER[30] = 1;
#                 }
		
		# Update curr globals about machine template
		@machine_template_data = &avc_Database_Get_Keyed_Record(48,$current_machine_template);
		
		########################################
		# If it's a fill modify Template History
		########################################
		if ($fill_type eq "F")
		{
			########################################
			# Update Last Template History
			########################################
			@template_history = &avc_Database_Get_Record_List(6,$machine_num);
			$last_template_history_packed = pop(@template_history);
			@last_template_history = &avc_Database_Unpack_Data(6,$last_template_history_packed);
			if ( ($last_template_history_packed ne "") && ($last_template_history[2] eq "") )
			{
				$last_template_history[2] = $today;
				$last_template_history[4] = "0";
				&avc_Database_Update_Record(6,$machine_num,@last_template_history);
			}
			
			########################################
			# Save New Template History
			########################################
			$template_history_record_num = &avc_Database_Get_Record_Number(6,$machine_num);
			@new_template_history = ($template_history_record_num,$today,"",$current_template_num,"0");
			&avc_Database_Write_Record(6,$machine_num,@new_template_history);
			
			########################################
			# Save New Template Log
			# 
			# Note: This assumes that machine records have
			#       updated location information 
			#
			########################################
			$promo_template_log_record_num = &rix_inventory_card_Write_Promo_Log($machine_num,$current_template_num,$MACHINE_INFO_BUFFER[6],$new_template_history[0]);
		}

		########################################
		# Update Machine Status
		########################################
		@machine_status_list = &avc_Database_Get_Record_List(3,$machine_num);
		$status_list_packed = pop(@machine_status_list);
		@status_list = &avc_Database_Unpack_Data(3,$status_list_packed);
		
		if ($fill_type eq "F")
		{
			# Save info about items removed & update status list
			@remove_list = ();
			for ($fill_counter = 0;$fill_counter < 10;$fill_counter++)
			{
				push(@remove_list,$status_list[1+$fill_counter]);
				
				$status_list[1+$fill_counter] = "";
				
				if ($fill_list[$fill_counter] ne "")
				{
					$status_list[1+$fill_counter] = $fill_list[$fill_counter];
				}
			}
		}
		elsif ($fill_type eq "R")
		{

			# Update status list
			for ($fill_counter = 0;$fill_counter < 10;$fill_counter++)
			{
				if ($fill_list[$fill_counter] ne "")
				{
					$status_list[1+$fill_counter] += $fill_list[$fill_counter];
				}
			}
		
		}
		&avc_Database_Update_Record(3,$machine_num,@status_list);
		
		
		########################################
		# If it's a fill save removal history
		########################################
		if ($fill_type eq "F")
		{
			########################################
			# Mark Removal History
			########################################
			$removal_history_record_num = &avc_Database_Get_Record_Number(5,$machine_num);
			@removal_history = ($removal_history_record_num,$today,"",$template_removed,@remove_list,"0");
			&avc_Database_Write_Record(5,$machine_num,@removal_history);
		
		}
		
		########################################
		# Transfer to vending inventory
		########################################
		for ($column_num = 0;$column_num < 10;$column_num++)
	 	{
	 		if ( ($fill_list[$column_num] != 0) || ($fill_list[$column_num] ne ""))
 			{
		 		#$inventory_num = $current_template_info[44+$column_num];
		 		
		 		if (scalar(@vending_inventory_addition_info) > 0)
		 		{
		 			# If a Vending Inventory Record exists
		 			
			 		@inventory_addition_chunk = split(/::/,$vending_inventory_addition_info[3+$column_num]);
					$inventory_num = $inventory_addition_chunk[0];
					$inventory_record_num = $inventory_addition_chunk[1];
					@curr_inventory_record =  &avc_Database_Get_Keyed_Record(30,$inventory_record_num,$inventory_num);
					$curr_inventory_record[2] += $fill_list[$column_num];
					$curr_inventory_record[3] = $today;
					&avc_Database_Update_Record(30,$inventory_num,@curr_inventory_record);
				}
				else
				{
					# If a Vending Inventory Record doesn't exist
					
					$inventory_num = $current_template_info[44+$column_num];
					&avc_Database_Write_Record(30,$inventory_num,(&avc_Database_Get_Record_Number(30,$inventory_num)),$machine_num,$fill_list[$column_num],$today,$last_fill_info[0]);
				}
			}
		}
		
		########################################
		# Update Fill Information
		########################################
		
		# Save Fill Date
		$last_fill_info[15] = $today;
		
		# Save Manager Number
		$last_fill_info[2] = $MACHINE_INFO_BUFFER[27];
		
		if ($fill_type eq "F")
		{
			# Save Promo Template History Record Number
			$last_fill_info[20] = $template_history_record_num;
			
			# Save Promo Template Log Record Number
			$last_fill_info[21] = $promo_template_log_record_num;
			
			# Save Removal History Record Number
			$last_fill_info[22] = $removal_history_record_num;
		}
		
		&avc_Database_Update_Record(4,$machine_num,@last_fill_info);

		$return_status = 1;
	}
	else
	{
		# Record Error
		
		$return_status = 0;
	}

	########################################
	# Modify Dialup Record with swap result
	########################################
	$CCU_STATUS_BUFFER[23] = $return_status;
		

	return $return_status;
}

########################################
#
# rix_inventory_card_Write_Promo_Log
#
########################################
# Description:
# 
# Created: 10/7/1998
# Last Modifed: 10/7/1998
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
########################################
sub rix_inventory_card_Write_Promo_Log
{
	local(	$curr_record_num,
			$machine_num,
			$location_num,
			$template_num,
			$history_record_num);
			
	$machine_num = $_[0];
	$template_num = $_[1];
	$location_num = $_[2];
	$history_record_num = $_[3];
	
	$curr_record_num = &pad_String($template_num,8,"_").$machine_num.&pad_String($location_num,8,"_");
	
	&avc_Database_Write_Record(40,"",$curr_record_num,$machine_num,$template_num,$location_num,$history_record_num);

	return $curr_record_num;
}

sub unpack_Database_Record
{
        local(  $database_template,
                        $packed_data,
                        @unpacked_data);

        $database_template = $_[0];
        $packed_data = $_[1];

        if (($database_template ne "") && ($packed_data ne ""))
        {
                @unpacked_data = unpack($database_template,$packed_data);
        }
        else
        {
                @unpacked_data = ();
        }

        return @unpacked_data;
}

sub avc_Database_Unpack_Data
{
    local($template_num,
          $packed_data);

    $template_num = $_[0];
    $packed_data = $_[1];

    return &fix_Key_Field(&avc_Database_Unpack_Database_Record($template_num,$packed_data));
}

sub fix_Key_Field
{
    local(@tmp_list);

    @tmp_list = @_;
    $tmp_list[0] = substr($tmp_list[0],1,length($tmp_list[0])-2);

    return @tmp_list;
}

sub avc_Database_Unpack_Database_Record
{
    local($template_num,
          $packed_data);

    $template_num = $_[0];
    $packed_data = $_[1];

    return &unpack_Database_Record($avc_record_template_list[$template_num],$packed_data);
}



1; #return true
