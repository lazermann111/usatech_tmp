#!/opt/bin/perl

sub avc_Database_Increment_Date
{
	($date_in,$dbh) = @_;

	($month_in,$day_in,$year_in) = split /\//, $date_in;

	#$ref = 	$oracle->select(
	#		table => 'dual',
	#		select_columns => qq{DATE_FORMAT(DATE_ADD('$year_in-$month_in-$day_in', INTERVAL 1 DAY), '%m/%d/%Y')},
	#	);

	$ref = $oracle->select(
			table => 'dual',
			select_columns => qq{to_date('$month_in/$day_in/$year_in','MM/DD/YYYY')+1},
		);

	warn(" TIME DEBUG in date-lib: $ref->[0][0]\n");
	return $ref->[0][0];

}

sub avc_Database_Decrement_Date
{

	($date_in,$dbh) = @_;
	($month_in,$day_in,$year_in) = split /\//, $date_in;

        #$ref =  $oracle->select(
        #                table => 'dual',
        #                select_columns => qq{DATE_FORMAT(DATE_SUB('$year_in-$month_in-$day_in', INTERVAL 1 DAY), '%m/%d/%Y')},
        #        );

        $ref = $oracle->select(
                        table => 'dual',
                        select_columns => qq{to_date('$month_in/$day_in/$year_in','MM/DD/YYYY')-1},
                );


	return $ref->[0][0];

}

1;
