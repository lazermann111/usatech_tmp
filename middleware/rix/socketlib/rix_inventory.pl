sub rix_inventory_card_Process
{

	my $inventory_card_number = shift;
	
	my $ref = $oracle->select( table => 'INVENTORY_FILL_RECORD',
		select_columns => 'FILL_TYPE_FLAG,INVENTORY_COLUMN1,INVENTORY_COLUMN2,INVENTORY_COLUMN3,INVENTORY_COLUMN4,INVENTORY_COLUMN5,INVENTORY_COLUMN6,INVENTORY_COLUMN7,INVENTORY_COLUMN8,INVENTORY_COLUMN9,INVENTORY_COLUMN10,MACHINE_ID',
		where_columns => ['INVENTORY_CARD_NUMBER = ?'],
		where_values => [$inventory_card_number],
		);

	my @array = @{$ref->[0]};
	my $fill_type = shift @array;
	warn("fill type: $fill_type\n");
	my $machine_id = $array[10];

	
	$oracle->insert( table => 'INVENTORY_STATUS',
			insert_columns => 'COLUMN1_INVENTORY,COLUMN2_INVENTORY,COLUMN3_INVENTORY,COLUMN4_INVENTORY,COLUMN5_INVENTORY,COLUMN6_INVENTORY,COLUMN7_INVENTORY,COLUMN8_INVENTORY,COLUMN9_INVENTORY,COLUMN10_INVENTORY,MACHINE_ID',
			insert_values => \@array,
		) if $fill_type eq 'F';

	return 1 if $fill_type eq 'F';

	my $invref = $oracle->select( table => 'INVENTORY_STATUS',
		select_columns => 'COLUMN1_INVENTORY,COLUMN2_INVENTORY,COLUMN3_INVENTORY,COLUMN4_INVENTORY,COLUMN5_INVENTORY,COLUMN6_INVENTORY,COLUMN7_INVENTORY,COLUMN8_INVENTORY,COLUMN9_INVENTORY,COLUMN10_INVENTORY',
		where_columns => ['MACHINE_ID = ?'],
		where_values => [$machine_id],
		);

	my @inv = @{$invref->[0]};
	foreach (0..9)
	{
		$inv[$_] = $inv[$_] + $array[$_];
	}

	$oracle->insert( table => 'INVENTORY_STATUS',
			insert_columns => 'COLUMN1_INVENTORY,COLUMN2_INVENTORY,COLUMN3_INVENTORY,COLUMN4_INVENTORY,COLUMN5_INVENTORY,COLUMN6_INVENTORY,COLUMN7_INVENTORY,COLUMN8_INVENTORY,COLUMN9_INVENTORY,COLUMN10_INVENTORY,MACHINE_ID',
			insert_values => [@inv,$machine_id],
		) if $fill_type eq 'R';

	return 1 if $fill_type eq 'R';
	return 0;
}

sub rix_inventory_card_Get_New_Promotion_Name
{
        local(  $promotion_name);

        $promotion_name = $CCU_SETTING_DATA[111];

        return $promotion_name;
}


1;

