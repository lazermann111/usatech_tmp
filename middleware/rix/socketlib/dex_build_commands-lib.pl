#!/opt/bin/perl
 ################################################################################
#
# dex_Build_Commands-lib.pl
#
 ################################################################################
#
# Written By: Josh Richards
# Created: 2/1/2000
# Last Modified: 2/10/2000
# (c)2000 Goodvest Corporation
#
################################################################################

use strict;

################################################################################
# dex_Build_Commands_PC1
################################################################################
sub dex_Build_Commands_PC1
{
    my ($product_number,
	$product_price,
	$product_id,
	$command);

    $product_number = $_[0];
    $product_price = $_[1];
    $product_id = $_[2];
    
    $command = "PC1*$product_number*$product_price*$product_id";
    
    return $command;
}

################################################################################
# dex_Build_Commands_LC1
################################################################################
sub dex_Build_Commands_LC1
{
    my ($price_list_number,
	$product_number,
	$price,
	$command);

    $price_list_number = $_[0];
    $product_number = $_[1];
    $price = $_[2];

    $command = "LC1*$price_list_number*$product_number*$price";

    return $command;
}

################################################################################
# dex_Build_Commands_LE
################################################################################
sub dex_Build_Commands_LE
{
    my ($loop_number,
	$command);

    $loop_number = $_[0];

    $command = "LE*$loop_number";

    return $command;
}

################################################################################
# dex_Build_Commands_LS
################################################################################
sub dex_Build_Commands_LS
{
    my ($loop_number,
	$command);

    $loop_number = $_[0];

    $command = "LS*$loop_number";

    return $command;
}

################################################################################
# dex_Build_Commands_SE
################################################################################
sub dex_Build_Commands_SE
{
    my ($number_of_included_sets,
	$control_number,
	$command);

    $number_of_included_sets = $_[0];
    $control_number = $_[1];

    $command = "SE*$number_of_included_sets*$control_number";

    return $command;
}

################################################################################
# dex_Build_Commands_DXE
################################################################################
sub dex_Build_Commands_DXE
{
    my ($control_number,
	$number_of_included_sets,
	$command);

    $control_number = $_[0];
    $number_of_included_sets = $_[1];

    $command = "DXE*$control_number*$number_of_included_sets";

    return $command;
}

################################################################################
# dex_Build_Commands_MC5
################################################################################
sub dex_Build_Commands_MC5
{
    my ($block_number,
	$bit_map,
	$joined,
	$config_number,
	$coin_mech_type,
	$escrow_mode,
	$command);

    #    $block_number = $_[0];
    #    $bit_map = $_[1];
    #    $config_number = $_[2];
    #    $coin_mech_type = $_[3];
    #    $escrow_mode = $_[4];

    $joined = join("*", @_);
    #    $command = "MC5*$block_number*$bit_map*$config_number*$coin_mech_type*$escrow_mode";
    $command = "MC5*$joined";
    return $command;	  
}

################################################################################
# dex_Build_Commands_G85
################################################################################
sub dex_Build_Commands_G85
{
    my (@data,
	$number_of_bytes,
	$dex_crc_remainder,
	$hex_remainder,
	$generator_polynomial,
	$DLE,
	$SYN,
	$byte_count,
	$working_data,
	$bit_count,
	$lsb_xor_msg_bit,
	@chars);

    ## To generate G85 checksums, we ignore the
    ## DXS command (the first one) from the data list
    if ($_[0] =~ /^DXS\*.*/)
    {
	@data = @_[1..$#_];
    }
    else
    {
	@data = @_;
    }

    @chars = unpack("C*", join("", @data));
    $number_of_bytes = scalar(@chars);

    ## Inits
    $dex_crc_remainder = 0x00;
    $generator_polynomial = 0xA001;
    $DLE = 0x10;
    $SYN = 0x16;

    ## Loop through all the data in the block
    for ($byte_count=0; $byte_count<$number_of_bytes; $byte_count++)
    {
	$working_data = $chars[$byte_count];
	
	if ($working_data ne $DLE and $working_data ne $SYN)
	{
	    ## Loop through each bit of the data
	    for ($bit_count=0; $bit_count<8; $bit_count++)
	    {
		## Check the LSB in the remainder and in the data
		$lsb_xor_msg_bit = (($dex_crc_remainder ^ $working_data) & 0x01) ? $generator_polynomial : 0;
		
		## Shift the remainder over 1 bit to the right
		$dex_crc_remainder >>= 1;
		
		## xor the new bit into the remainder
		$dex_crc_remainder = ($dex_crc_remainder & ~$generator_polynomial) | ($dex_crc_remainder ^ $lsb_xor_msg_bit);
		
		## Shift the data over 1 bit to the right
		$working_data >>= 1;	      
	    }
	}
    }

    $hex_remainder = sprintf("%0.4X", $dex_crc_remainder);

    return "G85*".$hex_remainder;
}

################################################################################
# dex_Build_Commands_EC2
################################################################################
sub dex_Build_Commands_EC2
{
    my ($event_id,
	$day,
	$hour,
	$minute,
	$millisecond,
	$user_defined,
	$command);

    $event_id = $_[0];
    $day = $_[1];
    $hour = $_[2];
    $minute = $_[3];
    $millisecond = $_[4];
    $user_defined = $_[5];

    $command = "EC2*$event_id*$day*$hour*$minute*$millisecond*$user_defined";

    return $command;
}

################################################################################
# dex_Build_Commands_IC5
################################################################################
sub dex_Build_Commands_IC5
{
    my ($date,
	$time, 
	$command);

    $date = $_[0];
    $time = $_[1];

    $command = "IC5*$date*$time";

    return $command;
}

################################################################################
# dex_Build_Commands_SD1
################################################################################
sub dex_Build_Commands_SD1
{
    my ($current_password,
	$new_password,
	$command);

    $current_password = $_[0];
    $new_password = $_[1];
    
    $command = "SD1*$current_password*$new_password";

    return $command;
}

################################################################################
# dex_Build_Commands_ST
################################################################################
sub dex_Build_Commands_ST
{
    my ($transaction_set_id,
	$transaction_set_control_number,
	$command);
    
    $transaction_set_id = $_[0];
    $transaction_set_control_number = $_[1];

    $command = "ST*$transaction_set_id*$transaction_set_control_number";

    return $command;    
}

################################################################################
# dex_Build_Commands_DXS
################################################################################
sub dex_Build_Commands_DXS
{
    my ($communication_id,
	$functional_id,
	$version,
	$transmission_control_number,
	$command);

    $communication_id = $_[0];
    $functional_id = $_[1];
    $version = $_[2];
    $transmission_control_number = $_[3];

    $command = "DXS*$communication_id*$functional_id*$version*$transmission_control_number";

    return $command;    
}

################################################################################
# dex_Build_Commands_IC1
################################################################################
sub dex_Build_Commands_IC1
{
    my ($serial,
	$model,
	$build_standard,
	$location,
	$user_defined_field,
	$asset,
	$command);

    $serial = $_[0];
    $model = $_[1];
    $build_standard = $_[2];
    $location = $_[3];
    $user_defined_field = $_[4];
    $asset = $_[5];

    $command = "IC1*$serial*$model*$build_standard*$location*$user_defined_field*$asset";

    return $command;
}

################################################################################
#return 1;  # True ##############################################################
1;


