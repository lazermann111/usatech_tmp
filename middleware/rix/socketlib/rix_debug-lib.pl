#!/opt/bin/perl
#################################################################
#
# rix_debug-lib.pl
# 
# Version 1.00
#
#################################################################
#
# Written By: Scott Wasserman
# Created: 10/5/1999
# (c)1999 e-Vend.net Corporation
#
#
#
#################################################################
# Functions Availible:
#################################################################
#
# 
#
# 
#
# 
#
#
#
#################################################################
# Required Libraries
#################################################################
#require("rix_os-lib.pl");
#require("slw-lib.pl");
#require("rix_checksum-lib.pl");

#################################################################
# Setup
#################################################################
		

# # Should we override default SEPARATOR character?
# if ($ARGV[0] ne "")
# {
#    $separator_ascii_num = $ARGV[0];
#    $rix_os_ENV{"SEPARATOR"} = sprintf("%c",$separator_ascii_num);
#
#
#    if ($ARGV[1] ne "")
#    {
#       # The 2nd command line argument is a 2 bit value.
#       # The lower bit represents the state of the DEBUG_MODE.
#       # The higher bit represents the state of the FAUX_MODE.
#       # i.e. (0|1)(0|1) or (FAUX_MODE)(DEBUG_MODE)
#
#       # 1 - debug only
#       # 2 - faux mode only
#       # 3 - faux and debug
#
#       $ARGV[1] = &rix_checksum_Dec_To_Bin($ARGV[1]);
#
#       if ($ARGV[1] & 0x1)
#       {
#          $rix_os_ENV{"DEBUG_MODE"} = 1;
#       }
#
#       if (($ARGV[1]>>1) & 0x1)
#       {
#          $rix_os_ENV{"FAUX_MODE"} = 1;
#       }
#
#       &rix_debug("FAUX_MODE_ON");
#
#       &rix_debug("DEBUG_ON");
#
#       &rix_debug("CURRENT_SEPARATOR");
#
#       if ($rix_os_ENV{"FAUX_MODE"})
#       {
#          # If we are in faux mode, then we will require a file containing the RIX commands to simulate.
#          # These are stored in a file,  which we copy into a list. Doing so will limit
#          # file access to a single read.
#          # the input filename is the 3rd command line argument.
#          $rix_debug_GLOBAL{"FAUX_INPUT_FILE"} = $ARGV[2];
#
#          $rix_debug_GLOBAL{"FAUX_CMDS"} = [ &rix_debug_Get_Faux_Commands() ];
#
#          #&dump_List(@{$rix_debug_GLOBAL{"FAUX_CMDS"}});
#
#          # We might also want to specify an output file to record the faux session, in addition to sending it to
#          # standard output.
#          # if specified, the output filename is the 4th command line argument.
#          if ($ARGV[3] ne "")
#          {
#             $rix_debug_GLOBAL{"FAUX_OUTPUT_FILE"} = $ARGV[3];
#          }
#
#       }
#    }
# }
#


#################################################################
# Globals 
#################################################################



	
########################################
#
# rix_debug
#
########################################
# Description: Debug commands
# 
# Commands Availible:
# DEBUG_ON) Show debug is on
# CURRENT_SEPERATOR) Show the current seperator
# COMMAND_DATA) Show command data
# CHECKSUM) Show overridden checksum
# MACHINE_ID) Show machine id
# ENV) Show environment globals
# 
# Uses Globals: $DEBUG_MODE, Any
#
# Written By: Scott Wasserman
# Created: 10/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_debug
{
	local(	$debug_command);
	
	$debug_command = $_[0];
	
	if ($rix_os_ENV{"DEBUG_MODE"})
	{
		if ($debug_command eq "DEBUG_ON")
		{
			######################################
			#> Action: Show debug is on
			######################################
			&rix_debug_Response(">>Debug mode is ON\n");
		}
		elsif ($debug_command eq "CURRENT_SEPARATOR")
		{		
			######################################
			#> Action: Show the current seperator
			######################################
			&rix_debug_Response(">>The SEPARATOR is ".$rix_os_ENV{"SEPARATOR"}."\n");
		}
		elsif ($debug_command eq "COMMAND_DATA")
		{
			######################################
			#> Action: Show command data
			######################################
                        &rix_debug_Response("device_id: $DEVICE_ID\n");
			&rix_debug_Response("command_group: $COMMAND_GROUP\n");
			&rix_debug_Response("command_number: $COMMAND_NUMBER\n");
			&rix_debug_Response("string_to_checksum: $STRING_TO_CHECKSUM\n");
			&rix_debug_Response("checksum: $CHECKSUM\n");
			&rix_debug_Response("data_list: ");
			for ($debug_item_counter = 0;$debug_item_counter < scalar(@DATA_LIST);$debug_item_counter++)
			{
				&rix_debug_Response($debug_item_counter.") ".$DATA_LIST[$debug_item_counter]."\n");
			}
			&rix_debug_Response("\n");
		}
		elsif ($debug_command eq "CHECKSUM")
		{
			######################################
			#> Action: Show overridden checksum
			######################################
			&rix_debug_Response(">>>Overriding checksum to ".$CHECKSUM."\n");
		}
		elsif ($debug_command eq "MACHINE_ID")
		{
			&rix_debug_Response("Machine id: ".$rix_os_ENV{"MACHINE_ID"}."\n");
		}
		elsif ($debug_command eq "ENV")
		{
			&dump_Relational_List_Sorted(%rix_os_ENV);
		}
	
	}

        if ($rix_os_ENV{"FAUX_MODE"})
	{
		if ($debug_command eq "FAUX_MODE_ON")
		{
			######################################
			#> Action: Show debug is on
			######################################
			&rix_debug_Response(">>Faux mode is ON\n");
		}

        }
        
}

########################################
#
# rix_debug_Execute_Commands
#
########################################
# Description: Rix Debug Commands
# 
# Uses Globals: 
#
# Written By: Scott Wasserman
# Created: 10/5/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_debug_Execute_Commands
{
	####################################
	#> Checksum show for debugging
	####################################	
	if ($COMMAND_GROUP eq "h")
	{
		&rix_debug_Checksum();
	}
	
	####################################
	#> Checksum override for debugging
	####################################
	if ($CHECKSUM eq "666")
	{
		$CHECKSUM = &rix_checksum_Make_From_String($STRING_TO_CHECKSUM);
		&rix_debug("CHECKSUM");
	}

	####################################
	#> Dump rix_os_ENV for debugging
	####################################
	if ($COMMAND_GROUP eq "z")
	{
		&rix_debug("ENV");
	}

}


########################################
#
# rix_Show_Checksum
#
########################################
# Description: Debug commands
# 
# Uses Globals: @DATA_LIST
#
# Written By: Scott Wasserman
# Created: 10/1/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_debug_Checksum
{
	local(	$assembled_checksum_check);
	
	######################################
	#> Action: Show checksum
	######################################
	$assembled_checksum_check = "";
	foreach $data_item (@DATA_LIST)
	{
		&rix_debug_Response("$data_item\n");
		$assembled_checksum_check = $assembled_checksum_check.$data_item.$rix_os_ENV{"SEPARATOR"};
	}
	&rix_debug_Response("$checksum\n"); 
	$assembled_checksum_check = $assembled_checksum_check.$checksum.$rix_os_ENV{"SEPARATOR"};
	
	&rix_debug_Response("Checksum for ".$assembled_checksum_check.": ".&rix_checksum_Make_From_String($assembled_checksum_check)."\n");
}

########################################
#
# rix_debug_Response
#
########################################
# Description: Return debug data
# 
#
# Written By: Scott Wasserman
# Created: 10/5/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_debug_Response
{
	local(	$data_in);
	
	$data_in = $_[0];

        if ($rix_os_ENV{"FAUX_MODE"} && defined($rix_debug_GLOBAL{"FAUX_OUTPUT_FILE"}))
        {
           print FAUX_OUT $data_in;
        }

	print $data_in;
}



########################################
#
# rix_debug_Get_Faux_Commands
#
########################################
# Description: Return faux commands contained in the input file  
#
# Written By: Salim Khan
# Created: 11/30/1999
#
#  DATE        NAME          CHANGES
# ---------   ------------  ---------------------------------------
#
#
########################################
sub rix_debug_Get_Faux_Commands
{
   local (  @return_array,
            @tmp_array );

   $filename = $rix_debug_GLOBAL{"FAUX_INPUT_FILE"};

   open(FAUX_IN,"+<$filename");
         
   @tmp_array = <FAUX_IN>;

   close(FAUX_IN);

   @return_array = ();

   foreach $line (@tmp_array)
   {
      #chop($line);
      
      $line =~ s/,/$rix_os_ENV{"SEPARATOR"}/g;

      push(@return_array, $line);
   }
   
   return @return_array;
}










1; #return true
