#!/opt/bin/perl
################################################################################
#
# dex_slave_commands-lib.pl
#
# last touched 3/27/00 at 10:29am
#
################################################################################
#
# Written By: Josh Richards
# Created: 2/15/2000
# Last Modified: 2/15/2000
#                6/8/2000  JHR - Fixed Error Responses Record- Door Openings were not incr.            
# (c)2000 Goodvest Corporation
#
################################################################################
push(@INC, "/websites/e-vend/lib/");
#require("avcbridge_database_core-lib.pl");
#require("avcbridge_database_query-lib.pl");
require("dex_build_commands-lib.pl");

## For NAMA
#require("nama_inventory-lib.pl");

## use strict;

my ($now, $today);

################################################################################
## dex_Slave_Accept_Responses
################################################################################
sub dex_Slave_Accept_Responses
{
    my ($dex_string,
	$CR_LF,
	@dex_lines,
	%record_64,
	%record_65,
	%record_66);
    
    my ($machine_num, $dex_string) = @_;

    # This was commented out to allow DEX to occur for all machines. The dex_Master_Generate_Cmds()
    # will indicate whether RIX needs to enter GET_MODE. 
    #if ($machine_num ne "JOSH" and $machine_num ne "GDV6")
    #{
    #	return;
    #}
    
    $now = &get_Extended_Time();
    $today = &sql_Format_Date_For_Sql(&get_Extended_Date());    
    
    ## DEBUG STUFF ###################################
    my $file = "/tmp/$machine_num.".$now.".dump";
    open(DEBUG, ">$file");
    print DEBUG $dex_string;
    close(DEBUG);
    `chmod 666 $file 2>/dev/null`;
    ##################################################

    ## SAL FIX ################################################
    #    $cr = chr(13);
    #    $lf = chr(10);
    #
    #    $sal1 = chr(128).chr(51);
    #    $sal2 = chr(128).chr(50);
    #    $dex_string =~ s/$sal1/$cr/gi;
    #    $dex_string =~ s/$sal2/$lf/gi;
    ###########################################################

    my $CR_LF = chr(13).chr(10);  ## Split on CR+LF chars
    my @dex_lines = split($CR_LF, $dex_string);
    
    ################################################################################
    ## START PARSE DEX COMMANDS
    ################################################################################
    foreach $dex_line (@dex_lines)
    {
	if ($dex_line =~ /\w+\*/)
	{
	    @fields = undef;
	    ($command_id, @fields) = split(/\*/, $dex_line);
	    
	    ## Loop counter START ##########################################################
	    if ($command_id eq "LS")
	    {
		$dex_line =~ /\w+\*(\d+)/;
		$current_loop = $1;
		
		## Initialize the current loop counter
		$loop_nums{$current_loop} = 0;
	    }
	    ## Loop counter END ############################################################
	    elsif ($command_id eq "LE")
	    {
		delete $loop_nums{$current_loop};
	    }
	    ## Machine Identification ######################################################
	    elsif ($command_id eq "ID1")
	    {
		$response_64{"ID101_Serial_Number"} = $fields[0];
		$response_64{"ID102_Model_Number"} = $fields[1];
		$response_64{"ID103_Build_Standard"} = $fields[2];
	    }
	    ## Machine Information #########################################################
	    elsif ($command_id eq "ID4")
	    {
		$response_64{"ID401_Decimal_Point_Position"} = $fields[0];
		$response_64{"ID402_Telephone_Country_Code"} = $fields[1];
	    }
	    ## Event Reporting - Door Openings #############################################
	    elsif ($command_id eq "EA2" and $fields[0] eq "DO")
	    {
		$door_openings = $fields[2];
	    }
	    ## Event Reporting - Counter Resets
	    elsif ($command_id eq "EA2" and $fields[0] eq "CR")
	    {
		$counter_resets = $fields[2];
	    }
	    ## Power outages ###############################################################
	    elsif ($command_id eq "EA7")
	    {
		$power_outages = $fields[1];
	    }
	    ## Machine Error Reporting #####################################################
	    elsif ($command_id eq "MA5" and $fields[0] eq "ERROR")
	    {
		## Lookup the name of the error
		$error_type = &dex_Slave_Error_Lookup($fields[1]);
		
		## If we are tracking this error, mark it as found
		if ($error_type)
		{
		    $error_65{$error_type} = 1;
		}		
	    }
	    ## Column Select Mask and Escrow mode+Coin mech ################################
	    elsif ($command_id eq "MA5" and $fields[0] =~ /^(\d\d?)$/)
	    {
		## Coin mech and Escrow mode information should
		## be attached with the first MA5 command
		if ($1 == 1 and $fields[3] and $fields[4])
		{
		    $response_64{"MA504_Coin_Mech_Type"} = $fields[3];
		    $response_64{"MA505_Escrow_Mode"} = $fields[4];
		}
		
		## Set the column select mask for this button in record 66
		$button_num = $1;
		$select_mask[$1 - 1] = $fields[1];
	    }
	    ## Machine's reported price ####################################################
	    elsif ($command_id eq "PA1")
	    {
		## Mark the current loop counter for this item
		$loop_nums{$current_loop} = &dex_Slave_Button_To_Number($fields[0]);
		
		## Set the price for this button in record 66
		$button_num = $loop_nums{$current_loop};
		$prices[$loop_nums{$current_loop} - 1] = $fields[1];
	    }
	    ## Sell through and Sales ######################################################
	    elsif ($command_id eq "PA2")
	    {
		$button_num = $loop_num{$current_loop};

		## Here is where history comes in, for now just grab
		$sell_through[$loop_nums{$current_loop} - 1] = $fields[0];
		$sales[$loop_nums{$current_loop} - 1] = $fields[1];
	    }
	    ## Sold out information ########################################################
	    elsif ($command_id eq "PA5")
	    {
		## This needs to change as well to take history into consideration
		$sold_out[$loop_nums{$current_loop} - 1] = join("", @fields[0..2]);
	    }
	}
	else
	{
	    ## print "dex_Slave_Accept_Responses: $dex_line - Not a DEX command\n";
	}
    }

    ################################################################################
    ## END PARSE DEX COMMANDS
    ################################################################################

##    &dump_Relational_List(%response_64);
##    &dump_Relational_List(%error_65);
##    &dump_Relational_List(%sell_through_66);

##    print "sell_through\n"; &dump_List(@sell_through);
##    print "sales\n";        &dump_List(@sales);
##    print "sold outs\n";    &dump_List(@sold_out);


    ## CHECK HISTORY FOR SIGNS OF NEW PROBLEMS #################################
    #@history_record = &avc_Database_Get_Last_Record(67, $machine_num);
    if ($history_record[0] eq "")
    {
	## Zero these fields
	@sold_out_flags = (0) x scalar(@sold_out); ## ** WHAT TO DO HERE? **
	@norm_sell_through = (0) x scalar(@sell_through);
	@norm_sales = (0) x scalar(@sales);
    }
    else
    {
       
	## Use history to calculate real values for these fields
	@sold_out_flags = &dex_Slave_Check_For_Sold_Outs(\@history_record, \@sold_out);
	@norm_sell_through = &dex_Slave_Normalize_Sell_Through(\@history_record, \@sell_through);
	@norm_sales = &dex_Slave_Normalize_Sales(\@history_record, \@sales);

	## open(RON, ">/tmp/ron_eats_it.txt");
	## Write out nama changes ##########################################
	if ($machine_num eq "GDV6") ## Only for NAMA Machine ###############
	{
	    ## print RON "Yes, this is machine GDV6\n";
	    ## print RON "Here are the norms:\n";
	    ## foreach $norm (@norm_sell_through)
	    ## {
		## print RON "$norm\n";
	    ## }

	    for ($counter=0; $counter<scalar(@norm_sell_through); $counter++)
	    {
		## print RON "Working with counter=$counter\n";

		if ($norm_sell_through[$counter] > 0)
		{
		    ## print RON "norm sales at $counter is ", $norm_sell_through[$counter], "\n";
		    $old_val = &nama_Get_Inventory($counter+1);

		    ## print RON "old value is $old_val\n";
		    $new_val = $old_val - $norm_sell_through[$counter];
		    ##print RON "new value is $new_val\n";
		    ##if ($new_val < 0)
		    ##{
		    ##$new_val = 0;
		    ##}
		    &nama_Set_Inventory($counter+1, $new_val);
		    ##print RON "wrote $new_val into inventory $counter (+1)\n";
		}
		else
		{
		    ## print RON "Norm sales was <= 0 at counter $counter\n";
		}
	    }
	}
	## close(RON);
	## `chmod 666 /tmp/ron_eats_it.txt 2>/dev/null`;
	####################################################################

	## Clear the old record, we'll insert a new row soon
	## &avc_Database_Initialize_Record($machine_num, 67);
    }

    ## WRITE OUT NEW DEX_Slave_History_Record ###################################
    &dex_Slave_Write_History($machine_num, $counter_resets, $door_openings, $power_outages,
			     \@sell_through, \@sales, \@sold_out);			     

    ## WRITE OUT DEX_Slave_Sell_Through_Responses ###############################
    &dex_Slave_Write_Sell_Through($machine_num, \@norm_sell_through, \@norm_sales,
				  \@sold_out_flags, \@prices, \@select_mask);

    ## WRITE OUT DEX_Slave_Responses ############################################
    #$response_64{"Key_Field"} = &avc_Database_Get_Record_Number(64, $machine_num);
    $response_64{"Date_Reported"} = $today;
    $response_64{"Time_Reported"} = $now;
    #&avc_Database_Write_Record_Hash(64, $machine_num, %response_64);

    
     ## WRITE OUT DEX_Slave_Error_Responses #####################################
     #$error_65{"Key_Field"} =  &avc_Database_Get_Record_Number(65, $machine_num);
     $error_65{"Date_Reported"} = $today;
     $error_65{"Time_Reported"} = $now;
     ## Note: refs to $history_record are the previous history, not the newly
     ## inserted one.

     ##Changed from > to < for Reset,Door Opening, Power Outage JHR. 6.8.2000
     $error_65{"Counter_Reset"} = ($history_record[1] < $counter_reset ? 1 : 0);
     $error_65{"Door_Opened"} = ($history_record[2] < $door_openings ? 1 : 0);
     $error_65{"Machine_Power_Outage"} = ($history_record[3] < $power_outages ? 1 : 0);
     $error_65{"Price_Changed"} = &dex_Slave_Check_Price_Change($machine_num);

     #&avc_Database_Write_Record_Hash(65, $machine_num, %error_65);
}


################################################################################
## dex_Slave_Accept_xml_Responses
## Added 6/26/00 by SAH  (modified from dex_Slave_Accept Responses
##  for the purpose of using a different date and time
################################################################################
sub dex_Slave_Accept_xml_Responses
{
    my ($dex_string,
	$CR_LF,
	@dex_lines,
	%record_64,
	%record_65,
	%record_66,
        %record_76,
        $machine_num,
        $dex_string);
    
    ($machine_num, $dex_string, $now, $today) = @_;
        # This was commented out to allow DEX to occur for all machines. The dex_Master_Generate_Cmds()
    # will indicate whether RIX needs to enter GET_MODE. 
    #if ($machine_num ne "JOSH" and $machine_num ne "GDV6")
    #{
    #	return;
    #}
    
    # These lines no longer necessary
    #$now = &get_Extended_Time();
    #$today = &sql_Format_Date_For_Sql(&get_Extended_Date());    
    
    ## DEBUG STUFF ###################################
    my $file = "/tmp/$machine_num.".$now.".dump";
    open(DEBUG, ">$file");
    print DEBUG $dex_string;
    close(DEBUG);
    `chmod 666 $file 2>/dev/null`;
    ##################################################

    ## SAL FIX ################################################
    #    $cr = chr(13);
    #    $lf = chr(10);
    #
    #    $sal1 = chr(128).chr(51);
    #    $sal2 = chr(128).chr(50);
    #    $dex_string =~ s/$sal1/$cr/gi;
    #    $dex_string =~ s/$sal2/$lf/gi;
    ###########################################################

    my $CR_LF = chr(13).chr(10);  ## Split on CR+LF chars
    my @dex_lines = split($CR_LF, $dex_string);
    
    ################################################################################
    ## START PARSE DEX COMMANDS
    ################################################################################
    foreach $dex_line (@dex_lines)
    {
	if ($dex_line =~ /\w+\*/)
	{
	    @fields = undef;
	    ($command_id, @fields) = split(/\*/, $dex_line);
	    
	    ## Loop counter START ##########################################################
	    if ($command_id eq "LS")
	    {
		$dex_line =~ /\w+\*(\d+)/;
		$current_loop = $1;
		
		## Initialize the current loop counter
		$loop_nums{$current_loop} = 0;
	    }
	    ## Loop counter END ############################################################
	    elsif ($command_id eq "LE")
	    {
		delete $loop_nums{$current_loop};
	    }
	    ## Machine Identification ######################################################
	    elsif ($command_id eq "ID1")
	    {
		$response_64{"ID101_Serial_Number"} = $fields[0];
		$response_64{"ID102_Model_Number"} = $fields[1];
		$response_64{"ID103_Build_Standard"} = $fields[2];
	    }
	    ## Machine Information #########################################################
	    elsif ($command_id eq "ID4")
	    {
		$response_64{"ID401_Decimal_Point_Position"} = $fields[0];
		$response_64{"ID402_Telephone_Country_Code"} = $fields[1];
	    }
	    ## Event Reporting - Door Openings #############################################
	    elsif ($command_id eq "EA2" and $fields[0] eq "DO")
	    {
		$door_openings = $fields[2];
	    }
	    ## Event Reporting - Counter Resets
	    elsif ($command_id eq "EA2" and $fields[0] eq "CR")
	    {
		$counter_resets = $fields[2];
	    }
	    ## Power outages ###############################################################
	    elsif ($command_id eq "EA7")
	    {
		$power_outages = $fields[1];
	    }
	    ## Machine Error Reporting #####################################################
	    elsif ($command_id eq "MA5" and $fields[0] eq "ERROR")
	    {
		## Lookup the name of the error
		$error_type = &dex_Slave_Error_Lookup($fields[1]);
		
		## If we are tracking this error, mark it as found
		if ($error_type)
		{
		    $error_65{$error_type} = 1;
		}		
	    }
	    ## Column Select Mask and Escrow mode+Coin mech ################################
	    elsif ($command_id eq "MA5" and $fields[0] =~ /^(\d\d?)$/)
	    {
		## Coin mech and Escrow mode information should
		## be attached with the first MA5 command
		if ($1 == 1 and $fields[3] and $fields[4])
		{
		    $response_64{"MA504_Coin_Mech_Type"} = $fields[3];
		    $response_64{"MA505_Escrow_Mode"} = $fields[4];
		}
		
		## Set the column select mask for this button in record 66
		$button_num = $1;
		$select_mask[$1 - 1] = $fields[1];
	    }
	    ## Machine's reported price ####################################################
	    elsif ($command_id eq "PA1")
	    {
		## Mark the current loop counter for this item
		$loop_nums{$current_loop} = &dex_Slave_Button_To_Number($fields[0]);
		
		## Set the price for this button in record 66
		$button_num = $loop_nums{$current_loop};
		$prices[$loop_nums{$current_loop} - 1] = $fields[1];
	    }
	    ## Sell through and Sales ######################################################
	    elsif ($command_id eq "PA2")
	    {
		$button_num = $loop_num{$current_loop};

		## Here is where history comes in, for now just grab
		$sell_through[$loop_nums{$current_loop} - 1] = $fields[0];
		$sales[$loop_nums{$current_loop} - 1] = $fields[1];
	    }
	    ## Sold out information ########################################################
	    elsif ($command_id eq "PA5")
	    {
		## This needs to change as well to take history into consideration
		$sold_out[$loop_nums{$current_loop} - 1] = join("", @fields[0..2]);
	    }
	}
	else
	{
	    ## print "dex_Slave_Accept_Responses: $dex_line - Not a DEX command\n";
	}
    }

    ################################################################################
    ## END PARSE DEX COMMANDS
    ################################################################################

##    &dump_Relational_List(%response_64);
##    &dump_Relational_List(%error_65);
##    &dump_Relational_List(%sell_through_66);

##    print "sell_through\n"; &dump_List(@sell_through);
##    print "sales\n";        &dump_List(@sales);
##    print "sold outs\n";    &dump_List(@sold_out);


    ## CHECK HISTORY FOR SIGNS OF NEW PROBLEMS #################################
    #@history_record = &avc_Database_Get_Last_Record(67, $machine_num);
    if ($history_record[0] eq "")
    {
	## Zero these fields
	@sold_out_flags = (0) x scalar(@sold_out); ## ** WHAT TO DO HERE? **
	@norm_sell_through = (0) x scalar(@sell_through);
	@norm_sales = (0) x scalar(@sales);
    }
    else
    {
       
	## Use history to calculate real values for these fields
	@sold_out_flags = &dex_Slave_Check_For_Sold_Outs(\@history_record, \@sold_out);
	@norm_sell_through = &dex_Slave_Normalize_Sell_Through(\@history_record, \@sell_through);
	@norm_sales = &dex_Slave_Normalize_Sales(\@history_record, \@sales);

	## open(RON, ">/tmp/ron_eats_it.txt");
	## Write out nama changes ##########################################
	if ($machine_num eq "GDV6") ## Only for NAMA Machine ###############
	{
	    ## print RON "Yes, this is machine GDV6\n";
	    ## print RON "Here are the norms:\n";
	    ## foreach $norm (@norm_sell_through)
	    ## {
		## print RON "$norm\n";
	    ## }

	    for ($counter=0; $counter<scalar(@norm_sell_through); $counter++)
	    {
		## print RON "Working with counter=$counter\n";

		if ($norm_sell_through[$counter] > 0)
		{
		    ## print RON "norm sales at $counter is ", $norm_sell_through[$counter], "\n";
		    $old_val = &nama_Get_Inventory($counter+1);

		    ## print RON "old value is $old_val\n";
		    $new_val = $old_val - $norm_sell_through[$counter];
		    ##print RON "new value is $new_val\n";
		    ##if ($new_val < 0)
		    ##{
		    ##$new_val = 0;
		    ##}
		    &nama_Set_Inventory($counter+1, $new_val);
		    ##print RON "wrote $new_val into inventory $counter (+1)\n";
		}
		else
		{
		    ## print RON "Norm sales was <= 0 at counter $counter\n";
		}
	    }
	}
	## close(RON);
	## `chmod 666 /tmp/ron_eats_it.txt 2>/dev/null`;
	####################################################################

	## Clear the old record, we'll insert a new row soon
	## &avc_Database_Initialize_Record($machine_num, 67);
    }

    ## WRITE OUT NEW DEX_Slave_History_Record ###################################
    &dex_Slave_Write_History($machine_num, $counter_resets, $door_openings, $power_outages,
			     \@sell_through, \@sales, \@sold_out);			     

    ## WRITE OUT DEX_Slave_Sell_Through_Responses ###############################
 
    &dex_Slave_Write_Sell_Through($machine_num, \@norm_sell_through, \@norm_sales,
				  \@sold_out_flags, \@prices, \@select_mask);

    ## WRITE OUT DEX_Slave_Responses ############################################
    #$response_64{"Key_Field"} = &avc_Database_Get_Record_Number(64, $machine_num);
    $response_64{"Date_Reported"} = $today;
    $response_64{"Time_Reported"} = $now;
    #&avc_Database_Write_Record_Hash(64, $machine_num, %response_64);

    
     ## WRITE OUT DEX_Slave_Error_Responses #####################################
     #$error_65{"Key_Field"} =  &avc_Database_Get_Record_Number(65, $machine_num);
     $error_65{"Date_Reported"} = $today;
     $error_65{"Time_Reported"} = $now;
     ## Note: refs to $history_record are the previous history, not the newly
     ## inserted one.

     ##Changed from > to < for Reset,Door Opening, Power Outage JHR. 6.8.2000
     $error_65{"Counter_Reset"} = ($history_record[1] < $counter_reset ? 1 : 0);
     $error_65{"Door_Opened"} = ($history_record[2] < $door_openings ? 1 : 0);
     $error_65{"Machine_Power_Outage"} = ($history_record[3] < $power_outages ? 1 : 0);
     $error_65{"Price_Changed"} = &dex_Slave_Check_Price_Change($machine_num);

     #&avc_Database_Write_Record_Hash(65, $machine_num, %error_65);


     ## Write out Dex Dump (Coke only)
	      $middbcheck = $rix_os_ENV{"MACHINE_ID"};
        if (!$dbhash{$middbcheck})
        {
                $dbhash{$middbcheck} = Evend::Database::Database->new(print_query => $print_query);
        }

	$kfref = $dbhash{$middbcheck}->select( table => 'DEX_Slave_Dump',
			select_columns => 'max(Key_Field+1)',
		);
#CHANGED
    # $record_76{"Key_Field"} = &avc_Database_Get_Record_Number(76, $machine_num);
	$record_76{"Key_Field"} = $kfref->[0][0];

     $record_76{"Reported_Date"} = $today;
     $record_76{"Reported_Time"} = $now;
     $record_76{"DEX_Dump"} = $dex_string;
# SHOULD BE FIXED< TABLE EXISTS
     #&avc_Database_Write_Record_Hash(76,$machine_num,%record_76);
}

################################################################################
## dex_Slave_Normalize_Sell_Through
################################################################################
sub dex_Slave_Normalize_Sell_Through
{
    my ($count, @sell_through_out);

    my ($hist_ref, $sell_through_ref) = @_;

    for ($count=0; $count<scalar(@{$sell_through_ref}); $count++)
    {
	$sell_through_out[$count] = $ {$sell_through_ref}[$count] - $ {$hist_ref}[$count*3 + 4];
    }

##    print "Sell through\n";
##    &dump_List(@sell_through_out);

    return @sell_through_out;
}

################################################################################
## dex_Slave_Normalize_Sales
################################################################################
sub dex_Slave_Normalize_Sales
{
    my ($count, @sales_out);

    my ($hist_ref, $sales_ref) = @_;

    for ($count=0; $count<scalar(@{$sales_ref}); $count++)
    {
	$sales_out[$count] = $ {$sales_ref}[$count] - $ {$hist_ref}[$count*3 + 5];
    }

##    print "Sales out\n";
##    &dump_List(@sales_out);

    return @sales_out;
}

################################################################################
## dex_Slave_Check_For_Sold_Outs
################################################################################
sub dex_Slave_Check_For_Sold_Outs
{
    my ($count, @flags);

    my ($hist_ref, $sold_out_ref) = @_;

    if (scalar(@{$sold_out_ref})== 0) 
    {
        for ($count=0;$count<54; $count++)
        {
           
            if (@{$hist_ref}[$count*3 +6]) 
            {
                 $flags[$count] = 1;
            }
            else
            {
                 $flags[$count] = 0;
            }
        }
    }
    
    else
    {
      for ($count=0; $count<scalar(@{$sold_out_ref}); $count++)
      {
           #if ((($ {$sold_out_ref}[$count] eq "") or ($ {$sold_out_ref}[$count] == 0)) 
           #   and (($ {$hist_ref}[$count*3 + 6] eq "") or ($ {$hist_ref}[$count*3 + 6] ==0)))
           #{
  	   #    $flags[$count] = 0;
  	   #}

           &rix_tracer_Save_Line("Hist Sold Out:".$count." ".@{$hist_ref}[$count*3 + 6]."\n"); 
   
   
           #Sold Out if current sold out stamp exists
           if (($ {$sold_out_ref}[$count] ne "") or ($ {$sold_out_ref}[$count] != 0)) 
           {
               $flags[$count] = 1;
           }
           #Sold Out if History Record was sold out
           elsif ((@{$hist_ref}[$count*3 + 6] ne "") or (@{$hist_ref}[$count*3 + 6] !=0))
           {
               $flags[$count] = 1;
           }
           else
      	   {
              #if current sold out = hist sold out set flag 1 else set flag 0
      	      #Commented below line out JHR 6.11.2000
              #$flags[$count] = $ {$sold_out_ref}[$count] eq $ {$hist_ref}[$count*3 + 6] ? 1 : 0;
              $flags[$count] = 0;
      	   }
      }
    }

##    print "Soldout flags\n";
##    &dump_List(@flags);

    return @flags;
}

################################################################################
## dex_Slave_Write_Sell_Through
################################################################################
sub dex_Slave_Write_Sell_Through
{
    my ($count);

    my ($machine_num, $sell_through_ref, $sales_ref,
	$sold_out_flags_ref, $prices_ref, $select_mask_ref) = @_;

	
    #$record_66{"Key_Field"} = &avc_Database_Get_Record_Number(66, $machine_num);

    $record_66{"Date_Reported"} = $today;
    $record_66{"Time_Reported"} = $now;

    for ($count=1; $count<=scalar(@{$sell_through_ref}); $count++)
    {
	$record_66{"Button".$count."_Sell_Through"} = $ {$sell_through_ref}[$count-1];
    }
    
    for ($count=1; $count<=scalar(@{$sales_ref}); $count++)
    {
	$record_66{"Button".$count."_Sales"} = $ {$sales_ref}[$count-1];
    }

    for ($count=1; $count<=scalar(@{$sold_out_flags_ref}); $count++)
    {
	$record_66{"Button".$count."_Sold_Out"} = ${$sold_out_flags_ref}[$count-1];
    }

    for ($count=1; $count<=scalar(@{$prices_ref}); $count++)
    {
	$record_66{"Button".$count."_Price"} = $ {$prices_ref}[$count-1];
    }
    
    for ($count=1; $count<=scalar(@{$select_mask_ref}); $count++)
    {
	$record_66{"Button".$count."_Column_Select_Mask"} = $ {$select_mask_ref}[$count-1];
    }


##    print "Dumping Sell Through record66\n";
##    &dump_Relational_List(%record_66);
    #&avc_Database_Write_Record_Hash(66, $machine_num, %record_66);
}
################################################################################
## dex_Slave_Write_History
################################################################################
sub dex_Slave_Write_History
{
    my (%history_67, $count);
    
    my ($machine_num, $counter_resets, $door_openings, $power_outages, 
	$sell_through_ref, $sales_ref, $sold_out_ref) = @_;
    
    #$history_67{"Key_Field"} = &avc_Database_Get_Record_Number(67, $machine_num);
    $history_67{"EA202_Counter_Resets_Since_Init"} = $counter_resets;
    $history_67{"EA202_Door_Openings_Since_Init"} = $door_openings;
    $history_67{"EA702_Power_Outages_Since_Init"} = $power_outages;

    ## Update with latest Sell Through info
    for ($count=1; $count<=scalar(@{$sell_through_ref}); $count++)
    {
	$history_67{"Button".$count."_Last_Sell_Through"} =  $ {$sell_through_ref}[$count-1];
    }
    
    ## Update with latest Sales info
    for ($count=1; $count<=scalar(@{$sales_ref}); $count++)
    {
	$history_67{"Button".$count."_Last_Sales"} =  $ {$sales_ref}[$count-1];
    }
    
    ## Update with latest Sold-out info
    for ($count=1; $count<=scalar(@{$sold_out_ref}); $count++)
    {
	## Sold-out info is not guaranteed to be non-null
	if ($ {$sold_out_ref}[$count-1] eq "")
	{
	    $ {$sold_out_ref}[$count-1] = 0;
	}
	
	$history_67{"Button".$count."_Sold_Out_Stamp"} =  $ {$sold_out_ref}[$count-1];
    }

##    &dump_Relational_List(%history_67);
    #&avc_Database_Write_Record_Hash(67, $machine_num, %history_67);
}

################################################################################
## dex_Slave_Check_Price_Change
################################################################################
sub dex_Slave_Check_Price_Change
{
    my $machine_num = $_[0];
    
    #my ($count, $packed_1, $packed_2) = &avc_Database_Get_Record_List_Nth_Range(66, $machine_num, 1, 2, "DESC");

    if ($count < 2)
    {
	## print "less than 2 records in 66, false\n";
	return 0;  ## No changes
    }
    else
    {
	## print "at least 2 records.. checking\n";
	my $price_changed = 0;

	## Note: Record1 is more recent than Record2
	#my @record1 = &dvc_Database_Unpack_Database_Record(66, $packed_1);
	#my @record2 = &avc_Database_Unpack_Database_Record(66, $packed_2);

	for (my $counter=3; $counter<scalar(@record1); $counter+=5)
	{
	    if ($record1[$counter] != $record2[$counter])
	    {
		## print "yup. changed\n";
		$price_changed = 1;  ## There was a price change, flag it
	    }
	}

	return $price_changed;
    }
}

################################################################################
## dex_Slave_Error_Lookup
################################################################################
## Description: This function accepts a error identification field from a DEX
## session and returns the SQL name of the column that it matches. If error is
## not found, null is returned.
################################################################################
sub dex_Slave_Error_Lookup
{
    my $dex_error = $_[0];
    
    ## Column Jammed error  ###################################
    if ($dex_error =~ /CJ(\d\d)/)
    {
	return "Column_Jammed";
    }
    ## Select Switch error  ###################################
    elsif ($dex_error =~ /SS(\d\d)/)
    {
	return "Select_Switch_Error";
    }
    ## Home sense error  ######################################
    elsif ($dex_error eq "HS" )
    {
	return "Home_Sense_Error";
    }
    ## Space to sales, double assignment error ###############
    elsif ($dex_error =~ /DA(\d\d)/)
    {
	return "";	## Ignoring for now
    }
    ## Space to sales, unassigned column error ###############
    elsif ($dex_error =~ /UN(\d\d)/)
    {
	return "";	## Ignoring for now
    }
    ## Door sense error ######################################
    elsif ($dex_error eq "DS")
    {
	return "Door_Sense_Error";
    }
    ## Coin changer comm error ###############################
    elsif ($dex_error eq "CC")
    {
	return "Coin_Changer_Comm_Error";
    }
    ## coin changer tube sensor error ########################
    elsif ($dex_error eq "TS")
    {
	return "Coin_Changer_Tube_Sensor_Error";
    }
    ## coin changer inlet chute blocked #######################
    elsif ($dex_error eq "IC")
    {
	return "Coin_Changer_Inlet_Chute_Error";
    }
    ## coin changer tube jammed ###############################
    elsif ($dex_error eq "TJ")
    {
	return "Coin_Changer_Tube_Jammed";
    }
    ## coin changer excessive escrow #########################
    elsif ($dex_error eq "EE")
    {
	return "Coin_Changer_Excessive_Escrow";
    }
    ## coin changer coin jammed ##############################
    elsif ($dex_error eq "NJ")
    {
	return "Coin_Changer_Coin_Jammed";
    }
    ## low acceptance rate ###################################
    elsif ($dex_error eq "LA")
    {
	return "Coin_Changer_Low_Acceptance_Rate";
    }
    ## bill validator stacker full ###########################
    elsif ($dex_error eq "BFUL")
    {
	return "Bill_Validator_Stacker_Full";
    }
    ## bill validator motor defective #########################
    elsif ($dex_error eq "BILL")
    {
	return "Bill_Validator_Motor_Defective";
    }
    ## bill validator jammed ##################################
    elsif ($dex_error eq "BJ")
    {
	return "Bill_Validator_Jammed";
    }
    ## bill validator stacker opened ##########################
    elsif ($dex_error eq "BOPN")
    {
	return "Bill_Validator_Stacker_Opened";
    }
    ## bill validator sensor error ############################
    elsif ($dex_error eq "BS")
    {
	return "Bill_Validator_Sensor_Error";
    }
    ## card reader error #####################################
    elsif ($dex_error =~ /CR(\d\d)/)
    {
	return "";	## Ignoring for now
    }
    ## control board failure #################################
    elsif ($dex_error eq "PB")
    {
	return "Control_Board_Failure";
    }
    ## control board enable error ############################
    elsif ($dex_error eq "EN")
    {
	return "Control_Board_Enable_Error";
    }
    ## Error not found
    else
    {
	return "";
    }  
}

################################################################################
## dex_Slave_Button_To_Number
################################################################################
## Description: This function converts a button number (ex: A1, 1, C7, 9) and
## converts it into its index number. Buttons can be digits or combination of 
## one alpha (A-F) and one digit. Hrm.. this dscription needs work... later.
sub dex_Slave_Button_To_Number
{
    my $button_number = $_[0];

    ## Check for A1...F9 buttons
    if ($button_number =~ /([A-F])(\d)/)
    {
	## print "button number is $button_number ";
	my $row = (ord($1) - 65)*9;
	my $col = $2;
	## print "= ", $row+$col, "\n";
	return $row + $col;
    }
    elsif ($button_number =~ /\d+/)
    {
	return $button_number;
    }
    else
    {
	return 0;
    }
}

################################################################################
#return 1;
1;
