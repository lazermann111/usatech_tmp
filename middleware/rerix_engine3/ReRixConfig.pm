package ReRixConfig;
use strict;
# TODO: convert to using Config::Properties::Simple to load and validate from
# separate file; retain same export behavior

use FindBin;

BEGIN
{
	use Exporter    ();
	use vars        qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
	@ISA            = qw(Exporter);
	@EXPORT         = qw($debug %ReRix_global);
	@EXPORT_OK      = qw(
		%run_rerix
		%ReRixEngine
		%ReRix_AuthClient
		%ReRix_DataLog
		%ReRix_ESuds
		%ReRix_FileTransferIncoming
		%ReRix_FileTransferOutgoing
		%ReRix_Functions
		%ReRix_G4Export
		%ReRix_KeyManager
		%ReRix_Init
		%ReRix_SSL
		%ReRix_Utils
	);
	%EXPORT_TAGS = (FIELDS => [@EXPORT_OK, @EXPORT]);
}

our $debug = 0;	#debug mode: 0=off, 1=on(loud), 2=on(quiet)

my $hostname = `hostname`;
chomp $hostname;
our %ReRix_global = (
	base_path            => "$FindBin::Bin",
	log_format_key_width => 18, #char min width of key-value pairs
	hostname             => $hostname,
	pid_file             => 'ReRixEngine.pid',
);

our %run_rerix = (
	alert_email_enabled     => 1, #alert e-mail toggle: 1=on, 0=off
	alert_email_smtp_server => 'mailhost.usatech.com',
	alert_email_addr_from   => '"rerix_engine@' . $ReRix_global{hostname}
	                         . '" <rerix_engine@usatech.com>',
	alert_email_addr_to     => 'ReRixAlerts@usatech.com', # comma-separated
);

our %ReRixEngine = (
	max_mem_use_parent => 85000, # max vsz memory allowed for main thread
	max_mem_use_thr    => 115000, # max vsz memory allowed for handler threads
	                             # (should never be smaller than main process)

	maintenance_thr_poll_freq => 30,  # seconds between database queue
	                                  # maintenance checks
	maintenance_thr_max_rows  => 600, # max rows allowed before database queue
	                                  # cleanup is performed
	maintenance_thr_max_sec   => 600, # max seconds allowed before database
	                                  # queue cleanup is performed
	                                  # (superceding maintenance_thr_max_rows)

	stat_report_interval => 900, #seconds between automatic statistics report

	pool_min             => 20, # min handlers threads to run at all times
	pool_max             => 90, # max for pool (see pool_max_type for units)
	pool_max_type        => 'thread', # type of data that pool_max represents
	                                  # use 'thread' or 'memory'
	pool_growby_min      => 1,    # min num of threads to grow by
	                              # each time growth required
	pool_growby_max      => 2,   # max num of threads to grow by
	                              # each time growth required
	pool_growby_delay    => 0.05, # seconds between creation of
	                              # new handler threads
	pool_queue_max       => 1000, # max allowed items at any time in internal
	                              # rerix message queue that handlers read from
	pool_handler_timeout => 1020, # seconds before pool downsize logic is
	                              # checked after a recent peak queue load

	dist_engine_sn_path => $ReRix_global{base_path}.'/ReRixEngine_serialnum.dat',
	# if unable to register unique rerix engine id, allow rerix_engine_id to
	# default to 0 and process messages saved in shared message bin
	dist_engine_allow_reg_failure => 0,
);

our %ReRix_AuthClient = ( #settings for transd process
	host    => '127.0.0.1',
	port    => 10104,
	timeout => 15,
);

our %ReRix_DataLog = (
	log_root_path => "$ReRix_global{base_path}/data_log",
);

our %ReRix_ESuds = (
	eccs_host    => '127.0.0.1',
	eccs_port    => 32081,
	eccs_timeout => 15,
);

our %ReRix_FileTransferIncoming = (
	data_root_path => "$ReRix_global{base_path}/temp_file_trans",
	# regex for incoming modem info file content
	MODEM_INFO_REGEX => 'HARDWAREVERSION|^<MODEM>|CGMI:TELIT',
);

our %ReRix_FileTransferOutgoing = (
	data_root_path      => "$ReRix_global{base_path}/temp_file_trans",
	default_packet_size => 105, #bytes
);

our %ReRix_G4Export = (
	data_root_path => "$ReRix_global{base_path}/ReRix/G4Export/data",
	log_root_path  => "$ReRix_global{base_path}/ReRix/G4Export/logs",
);

our %ReRix_Utils = (
	alert_email_enabled     => $run_rerix{alert_email_enabled}, # 1=on, 0=off
	alert_email_smtp_server => 'mailhost',
	alert_email_addr_from   => $run_rerix{alert_email_addr_from},
	alert_email_addr_to     => 'ReRixAlerts@usatech.com', # comma-separated
);

our %ReRix_Init = (
	init_email_enabled   => 1, #init e-mail toggle: 1=on, 0=off
	init_email_addr_from => $run_rerix{alert_email_addr_from},
	init_email_addr_to   => 'KioskAlerts@usatech.com', #comma-separated list
);

our %ReRix_SSL = (
	HTTPS_CA_FILE         => "$ReRix_global{base_path}/SSL/USATechRootCA.cer",
	HTTPS_PKCS12_FILE     => "$ReRix_global{base_path}/SSL/ReRix.pfx",
	HTTPS_PKCS12_PASSWORD => '<password>',
);

our %ReRix_KeyManager = (
	KEYSERVER_PROXY         => 'https://<server>/KeyManager/soapservice/KeyManager',
	KEYSERVER_PROXY_TIMEOUT => 5, #seconds
	DEFAULT_DUKPT_KEY_ID    => 1222874644089,
);

our %ReRix_Functions = (
	# _fix_g4_bcd_seconds_bug_discovered_02_03_2004() toggle: 1=on, 0=off
	G4_BCD_SECONDS_BUG_FIX        => 0,
	# reactivation of disabled devices on reinit toggle: 1=on, 0=off
	DEVICE_REACTIVATION_ON_REINIT => 0,
	# if send_alert() content matches this regex, alert will not be sen
	# set to empty string to send all alerts
	NO_ALERT_REGEX                => 'A duplicate transaction exists: ',
);

1;
