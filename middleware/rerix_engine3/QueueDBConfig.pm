package QueueDBConfig;

use strict;
use USAT::Database::Config;
use vars qw(@ISA);
@ISA = qw(USAT::Database::Config);

sub new {
	my $type = shift;
	my $self = USAT::Database::Config->new(
		driver 			=> 'Pg', 
		alias			=> 'usadbn01',
		primary 		=> 'dbname=usadbn;host=<primary>;port=1521',
		backup			=> 'dbname=usadbn;host=<backup>;port=1521',
		username 		=> '<username>',
		password 		=> '<password>',
		debug				=> 1,
		debug_die			=> 1,
		print_query			=> 1,
		retries				=> 1,
		connection_lifetime	=> 60 * 60 * 24,
		@_
	);
	bless $self, __PACKAGE__;
	return $self;
}

1;
