#!/usr/local/USAT/bin/perl
#usage: LoadTest.pl <num iterations before session complete> EV#0 [EV#1 EV#2 ... EV#N]
use strict;
use warnings;
use sigtrap qw(die untrapped normal-signals stack-trace error-signals);

use forks::BerkeleyDB;
use forks::BerkeleyDB::shared;
use Time::HiRes qw(time);
use IO::Poll;
use POE::strict qw(
	Component::Client::TCP
	Filter::Line
);

use USAT::NetLayer::Database;
use USAT::NetLayer::Filter::UU qw(uuencode uudecode);
use USAT::NetLayer::Utils qw(LogIt);

use DatabaseConfig;

use constant {
	DEBUG		=> 0,
	LOG_LEVEL	=> 'INFO',
	
	NUM_SESSIONS_PER_THR					=> 20,	#num tcp sessions to manage per perl thread
	RUNTIME_STAT_REPORT_INTERVAL			=> 15,	#seconds
	PRECACHE_MSG_AND_RESP					=> 1,	#1=create all messages before starting sessions; 0=create on the fly (note: may adversely affect statistics)
	SIMULATE_CONNECT_FIRST_MESSAGE_DELAY	=> 10,	#max seconds (randomized 0-N); 0=disabled
	SIMULATE_WAN_TCP_COMM_DELAY				=> 1,	#seconds; 0=disabled
	VALIDATE_RESPONSE_WITH_REQUEST 			=> 1,	#1=enabled; 0=disabled
	DISCONNECT_SESSION_AFTER_LAST_MESSAGE	=> 0,	#1=disconnect; 0=wait for server timeout

	HOST	=> '127.0.0.1',	#'10.0.0.42',	#'10.0.0.63',	#'129.41.132.162'
	PORT	=> 14107,
};

our $my_session_lock : shared;
our $active_sessions : shared = 0;
our $total_sessions_started : shared = 0;

our $my_stat_lock : shared;
our $msg_process_time_total : shared = 0;
our $msg_process_time_total_num : shared = 0;
our $msg_process_time_max : shared = undef;
our $msg_process_time_min : shared = undef;
our %msg_process_time_histogram : shared = ();
our %sessions_with_errors : shared = (
	no_connection			=> undef,
	input_timeout			=> undef,
	incorrect_num_responses	=> undef,
	incorrect_responses		=> undef
);
{
	my %no_connection : shared;
	my %input_timeout : shared;
	my %incorrect_num_responses : shared;
	my %incorrect_responses : shared;
	$sessions_with_errors{no_connection} = \%no_connection;
	$sessions_with_errors{input_timeout} = \%input_timeout;
	$sessions_with_errors{incorrect_num_responses} = \%incorrect_num_responses;
	$sessions_with_errors{incorrect_responses} = \%incorrect_responses;
}

our $running = 1;
our $SIGINT_CNT = 0;
$SIG{INT} = sub {
	$SIGINT_CNT++;
	warn "Caught SIGINT \#$SIGINT_CNT: ".
		($SIGINT_CNT == 1 ? "attempting to exit gracefully..." : "attempting to exit immediately");
	if ($SIGINT_CNT > 1) {
		report_global_stats();
		exit();
	}
	$running = 0;
};

sub report_global_stats {
	lock $my_stat_lock;
	LogIt( "Overall Execution statistics (so far): " );
	LogIt( "\tTotal messages successfully processed: ". $msg_process_time_total_num );
	LogIt( "\tOverall Avg Msg Time for all sessions: "
		.($msg_process_time_total_num ? $msg_process_time_total / $msg_process_time_total_num : 0) );
	LogIt( "\tOverall Max Msg Time: ". $msg_process_time_max );
	LogIt( "\tOverall Min Msg Time: ". $msg_process_time_min );
	LogIt( "\tOverall Execution histogram: " );
	LogIt( $_ ) foreach map("\t\t".sprintf('% 7s', $msg_process_time_histogram{$_})." @ $_ sec", sort { $a <=> $b } keys %msg_process_time_histogram);
	foreach (keys %sessions_with_errors) {
		my @session_err_keys = keys %{$sessions_with_errors{$_}};
		LogIt( "\tSessions with $_ errors (".scalar @session_err_keys."): ".join(',', @session_err_keys) );
	}
}

sub create_msg_tuplet ($$$) {	#msg, resp, encoded msg
	my ($device_name, $device_key, $msg_num) = @_;
	my $rand = substr(rand(), 2) % 4294967295;
	my $msg = pack('CCN', $msg_num % 256, 0xFE, $rand);
	my $resp = pack('CCN', $msg_num % 256, 0xFF, $rand);
	return ( unpack("H*", $msg), unpack("H*", $resp), uuencode($device_name . Logic::PackEncrypted($msg, $device_key, LOG_LEVEL, *STDOUT, HOST.':'.PORT)) );
}

sub main () {
	my $num_loops = shift @ARGV;
	die "Number of loops param isn't a valid positive integer" unless defined $num_loops && $num_loops =~ m/^\d+$/o;
	my @devices = @ARGV;
	die "No devices specified" unless @devices;
	my @keys;
	my $DATABASE = USAT::NetLayer::Database->new(
		config 				=> new DatabaseConfig,
		log_level 			=> LOG_LEVEL,
		debug_file_handle	=> *STDOUT,
	);
	push @keys, (Logic::getKey($_, $DATABASE, HOST.':'.PORT, LOG_LEVEL, *STDOUT))[0] foreach @devices;

	my (@messages, @responses, @messages_encoded);
	if (PRECACHE_MSG_AND_RESP) {
		for (my $idx = 0; $idx < scalar @devices; $idx++) {
			for (0..($num_loops - 1)) {
				my ($msg, $resp, $msg_encoded) = create_msg_tuplet($devices[$idx], $keys[$idx], $_);
				push @{$messages[$idx]}, $msg;
				push @{$responses[$idx]}, $resp;
				push @{$messages_encoded[$idx]}, $msg_encoded;
			}
		}
	}

	# Spawn a new client for each EV number.
	for (my $i = 0; $i < scalar @devices; $i += NUM_SESSIONS_PER_THR) {
		my $thr = threads->new(\&handler_thr, 
			$i,
			NUM_SESSIONS_PER_THR > scalar @devices
				? scalar @devices
				: scalar(@devices) - NUM_SESSIONS_PER_THR >= $i ? NUM_SESSIONS_PER_THR : scalar(@devices) - $i,
			$num_loops,
			\@devices,
			\@keys,
			\@messages,
			\@responses,
			\@messages_encoded
		);
		LogIt("Created thread \#".$thr->tid);
	}
	threads->new(\&report_global_stats_thr);
	$_->join() foreach threads->list();
	report_global_stats();
	exit 0;
}

sub report_global_stats_thr {
	while (1) {
		sleep RUNTIME_STAT_REPORT_INTERVAL;
		report_global_stats();
		return if $active_sessions <= 0;
	}
}

sub handler_thr {
	my $start_offset = shift;
	my $num_to_handle = shift;
	my $num_loops = shift;
	my @devices = @{shift(@_)};
	my @keys = @{shift(@_)};
	my @messages = @{shift(@_)};
	my @responses = @{shift(@_)};
	my @messages_encoded = @{shift(@_)};
	
	for (my $idx = $start_offset; $idx < $start_offset + $num_to_handle; $idx++) {

		POE::Component::Client::TCP->new(
			Args			=> [$devices[$idx], $keys[$idx], $idx],
		    RemoteAddress	=> HOST,
			RemotePort		=> PORT,
			Filter			=> ['POE::Filter::Line', Literal => "\x0A"],
			
			# The session started.
			Started => sub {
				$_[HEAP]->{device_name} = $_[ARG0] || die 'Unknown device name';
				$_[HEAP]->{device_key} = $_[ARG1] || die "Unknown device key for device $_[HEAP]->{device_name}";
				$_[HEAP]->{device_idx} = defined $_[ARG2] ? $_[ARG2] : die 'Unknown device index';
				$_[HEAP]->{buffer} = [];
				$_[HEAP]->{msg_idx} = 0;
				$_[HEAP]->{msg_process_start_ts} = 0;
				$_[HEAP]->{msg_process_time} = [];
				$_[HEAP]->{input_timeout_alarm_id} = undef;
				$_[HEAP]->{msg_process_time_total} = 0;
				$_[HEAP]->{msg_process_time_total_num} = 0;
				$_[HEAP]->{msg_process_time_histogram} = {};	#store count of msgs per 0.1 second interval
				$_[HEAP]->{running} = 1;
			},

			# The connection was successful. Send a message.
			Connected => sub {
				$_[KERNEL]->yield( log => "$_[HEAP]->{device_name} connected to ".HOST.':'.PORT.' ...' );
				if (SIMULATE_CONNECT_FIRST_MESSAGE_DELAY) {
					my $delay = int(rand()*SIMULATE_CONNECT_FIRST_MESSAGE_DELAY);
					$_[KERNEL]->delay( 'send_message', $delay );
					$_[KERNEL]->yield( log => "Simulating $delay second client delay before first message..." );
				} else {
					$_[KERNEL]->yield( 'send_message' );
				}
				{
					lock $my_session_lock;
					$active_sessions++;
					$total_sessions_started++;
				}
			},

			# The connection failed.
			ConnectError => sub {
				$_[KERNEL]->yield( log => 'ERROR: could not connect to '.HOST.':'.PORT.' ...' );
				$sessions_with_errors{no_connection}->{$_[HEAP]->{device_idx}} = undef;
			},

			# The output buffer flushed.
			ServerFlushed => sub {
				$_[HEAP]->{msg_process_start_ts} = time();
			},
			
			# The server has sent us something.
			ServerInput => sub {
				my $input = $_[ARG0];
				$_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1] = time() - $_[HEAP]->{msg_process_start_ts};
				$_[HEAP]->{msg_process_time_total} += $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1];
				$_[HEAP]->{msg_process_time_total_num}++;
				my $hist_time = sprintf('%.1f', $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1]);
				$_[HEAP]->{msg_process_time_histogram}->{$hist_time}++;

				{
					lock $my_stat_lock;
					$msg_process_time_total += $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1];
					$msg_process_time_total_num++;
					$msg_process_time_histogram{$hist_time}++;
					$msg_process_time_max = $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1]
						if !defined $msg_process_time_max || $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1] > $msg_process_time_max;
					$msg_process_time_min = $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1]
						if !defined $msg_process_time_min || $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1] < $msg_process_time_min;
				}
				
				$_[KERNEL]->yield( log => 'got input from '.HOST.':'.PORT.' ...' ) if DEBUG;
				push @{ $_[HEAP]->{buffer} }, $input;
				if ($_[HEAP]->{running} && $_[HEAP]->{msg_idx} < $num_loops) {
					$_[KERNEL]->delay( 'send_message', SIMULATE_WAN_TCP_COMM_DELAY );
					$_[KERNEL]->alarm_remove($_[HEAP]->{input_timeout_alarm_id}) if defined $_[HEAP]->{input_timeout_alarm_id};
					$_[HEAP]->{input_timeout_alarm_id} = $_[KERNEL]->alarm_set( input_timeout => time() + 30 );
				}
				else {
					$_[KERNEL]->alarm_remove($_[HEAP]->{input_timeout_alarm_id}) if defined $_[HEAP]->{input_timeout_alarm_id};
					$_[KERNEL]->yield( 'disconnect', SIMULATE_WAN_TCP_COMM_DELAY );
				}
			},
			
			Disconnected => sub {
				$_[KERNEL]->alarm_remove($_[HEAP]->{input_timeout_alarm_id}) if defined $_[HEAP]->{input_timeout_alarm_id};
				$_[KERNEL]->yield( 'disconnect' )
					if $_[HEAP]->{running};
			},
			
			InlineStates => {

				# Send a message.
				send_message => sub {
					unless ($running) {
						$_[KERNEL]->yield( log => "Immediate termination requested: Exiting gracefully ..." );
						$_[KERNEL]->yield( 'disconnect' );
						return;
					}

					my @logs;
					push @logs, "sending message \#$_[HEAP]->{msg_idx} on ".HOST.':'.PORT.' ...'
						if DEBUG && $_[HEAP]->{msg_idx} % 10 == 0;
					
					my ($msg, $resp, $msg_encoded);
					unless (PRECACHE_MSG_AND_RESP) {
						my ($msg, $resp, $msg_encoded) = create_msg_tuplet(
							$devices[$_[HEAP]->{device_idx}],
							$keys[$_[HEAP]->{device_idx}],
							$_[HEAP]->{msg_idx}
						);
						push @{$messages[$_[HEAP]->{device_idx}]}, $msg;
						push @{$responses[$_[HEAP]->{device_idx}]}, $resp;
						push @{$messages_encoded[$_[HEAP]->{device_idx}]}, $msg_encoded;
					}
					
					if (DEBUG) {
						push @logs, "Machine ID : ".$_[HEAP]->{device_name};
						push @logs, "Input      : ".$messages[$_[HEAP]->{device_idx}][$_[HEAP]->{msg_idx}];
						push @logs, "Key        : ".$_[HEAP]->{device_key};
						push @logs, "Output     : ".$messages_encoded[$_[HEAP]->{device_idx}][$_[HEAP]->{msg_idx}];
					}
					$_[KERNEL]->yield( log => @logs );

					$_[HEAP]->{server}->put($messages_encoded[$_[HEAP]->{device_idx}][$_[HEAP]->{msg_idx}]);
					$_[HEAP]->{msg_idx}++;
					$_[KERNEL]->alarm_remove($_[HEAP]->{input_timeout_alarm_id}) if defined $_[HEAP]->{input_timeout_alarm_id};
					$_[HEAP]->{input_timeout_alarm_id} = $_[KERNEL]->alarm_set( input_timeout => time() + 30 );
				},

				# The server sent us something already, but it has become idle.
				# Display what the server sent us so far, and shut down.
				input_timeout => sub {
					$_[KERNEL]->yield( log => 'ERROR: got input timeout from '.HOST.':'.PORT.' ...' );
					$sessions_with_errors{input_timeout}->{$_[HEAP]->{device_idx}} = undef;
					$_[KERNEL]->yield( 'disconnect' )
						if $_[HEAP]->{running};
				},
				
				disconnect => sub {
					unless ($_[HEAP]->{running}) {
						$_[KERNEL]->yield( log => "ERROR: Received multiple disconnect requests" );
						if ($_[HEAP]->{msg_process_time_total_num} != $num_loops) {
							$sessions_with_errors{incorrect_num_responses}->{$_[HEAP]->{device_idx}} = undef;
						} else {
							delete $sessions_with_errors{incorrect_num_responses}->{$_[HEAP]->{device_idx}};
						}

						return;
					}
					$_[HEAP]->{running} = 0;

					my @logs;
					
					### session stats ###
					push @logs, "Session Execution statistics: ";
					push @logs, "\tTotal messages successfully processed: ". $_[HEAP]->{msg_process_time_total_num};
					push @logs, "\tAvg Msg Time for this session: "
						.($_[HEAP]->{msg_process_time_total_num} ? $_[HEAP]->{msg_process_time_total} / $_[HEAP]->{msg_process_time_total_num} : 0);
					push @logs, "Session Execution histogram: ";
					push @logs, map("\t".sprintf('% 7s', $_[HEAP]->{msg_process_time_histogram}->{$_})." @ $_ sec", sort { $a <=> $b } keys %{$_[HEAP]->{msg_process_time_histogram}});
					
					if ($_[HEAP]->{msg_process_time_total_num} != $num_loops) {
						$sessions_with_errors{incorrect_num_responses}->{$_[HEAP]->{device_idx}} = undef;
					} else {
						delete $sessions_with_errors{incorrect_num_responses}->{$_[HEAP]->{device_idx}};
					}

					if (DEBUG) {
						my $detail = '';
						$detail .= "\n\t\#$_: $_[HEAP]->{msg_process_time}->[$_] seconds" for (0..scalar @{$_[HEAP]->{msg_process_time}} - 1);
						push @logs, $detail if $detail;
					}

					$_[KERNEL]->delay( validate_response_loop => 1, 0 )
						if VALIDATE_RESPONSE_WITH_REQUEST;

					push @logs, "session shutting down...";
					{
						lock $my_session_lock;
						$active_sessions--;
						push @logs, "active sessions remaining: ".$active_sessions." of $total_sessions_started total)";
					}

					$_[KERNEL]->yield( log => @logs );
					$_[KERNEL]->yield( 'shutdown' ) if DISCONNECT_SESSION_AFTER_LAST_MESSAGE;
				},
				
				validate_response_loop => sub {
					my $msg_idx = $_[ARG0];

					my @logs;
					push @logs, ',----- Responses from '.HOST.':'.PORT if DEBUG && $msg_idx == 0;
					if (exists $_[HEAP]->{buffer}->[$msg_idx]) {
						push @logs, "Message \#$msg_idx:";
						push @logs, "\tResponse   : " . unpack("H*",$_[HEAP]->{buffer}->[$msg_idx]);
						
						my $decoded = uudecode($_[HEAP]->{buffer}->[$msg_idx]);
						my ($result, $decrypted) = @{Logic::UnpackEncrypted($decoded, $_[HEAP]->{device_key}, LOG_LEVEL, *STDOUT, HOST.':'.PORT)};
						my $decrypted_hex = unpack("H*",$decrypted);
						if ($result && $decrypted_hex eq $responses[$_[HEAP]->{device_idx}][$msg_idx]) {
							push @logs, "\tResult     : Good";
							push @logs, "\tExpected   : ".$responses[$_[HEAP]->{device_idx}][$msg_idx];
						} else {
							push @logs, "\tResult     : ERROR";
							push @logs, "\tGot        : " . $decrypted_hex;
							push @logs, "\tExpected   : ".$responses[$_[HEAP]->{device_idx}][$msg_idx];
							$sessions_with_errors{incorrect_responses}->{$_[HEAP]->{device_idx}} = undef;
						}
						
						$_[KERNEL]->delay( validate_response_loop => 1, ++$msg_idx )
							if $msg_idx < (scalar @{$messages_encoded[$_[HEAP]->{device_idx}]} - 1);
					}
					push @logs, "`-----" if DEBUG && $msg_idx == 0;
					$_[KERNEL]->yield( log => @logs );
				},
				
				log => sub {
					LogIt("Session \#$_[HEAP]->{device_idx}: $_") foreach (@_[ARG0..$#_]);
				},
			  },
		  );
	}

	# Run the clients until the last one has shut down.

	$poe_kernel->run();
}

#####
main();

package Logic;

use strict;
use warnings;

use USAT::NetLayer::Utils qw(LogIt SecureLogIt);
use Evend::Crypt::TEA;
use Evend::ReRix::Shared qw(getcrc16s);

use constant {
	DEFAULT_MACHINE_ID 		=> 'EV000000',
	DEFAULT_ENCRYPTION_KEY	=> '9265027739274401'
};

sub PackEncrypted ($$$$;$)
{
	my ($message, $key, $logLevel, $debugFile, $call_id) = @_;
	my ($len, $crc, @list_key, $tea);
    
	if (not defined $key)
	{
		# default key used in alot of places
		$key = "E4059999";
	}

	if(length($key) == 8)
	{
		@list_key =(
			(ord(substr($key,3,1))<<24) |
			(ord(substr($key,2,1))<<16) |
			(ord(substr($key,1,1))<<8)  |
			 ord(substr($key,0,1))
			,
			(ord(substr($key,7,1))<<24) |
			(ord(substr($key,6,1))<<16) |
			(ord(substr($key,5,1))<<8)  |
			 ord(substr($key,4,1))
		);
		
        # Duplicate it to get 16 bytes
	    push @list_key, @list_key;
	}
	elsif(length($key) == 16)
	{
		@list_key =(
			(ord(substr($key,3,1))<<24) |
			(ord(substr($key,2,1))<<16) |
			(ord(substr($key,1,1))<<8)  |
			 ord(substr($key,0,1))
			,
			(ord(substr($key,7,1))<<24) |
			(ord(substr($key,6,1))<<16) |
			(ord(substr($key,5,1))<<8)  |
			 ord(substr($key,4,1))
			,
			(ord(substr($key,11,1))<<24) |
			(ord(substr($key,10,1))<<16) |
			(ord(substr($key,9,1))<<8)  |
			 ord(substr($key,8,1))
			,
			(ord(substr($key,15,1))<<24) |
			(ord(substr($key,14,1))<<16) |
			(ord(substr($key,13,1))<<8)  |
			 ord(substr($key,12,1))
		);
	}
	else
	{
		SecureLogIt("PackEncrypted: Invalid Key Length: $key", $logLevel, $debugFile);
		return 0;
	}
	
    # set the key
    $tea = Evend::Crypt::TEA->new(Key=>\@list_key);
    
    # get the length of the message as a character
    $len = chr(length $message);
    
    # Compute the CRC16 of the message
    $crc = getcrc16s($message);
    
    SecureLogIt("$call_id\t" . "Encrypt: [".unpack("H*",ord($len))."][".unpack("H*",$message)."][".unpack("H*",$crc)."]", $logLevel, $debugFile);

    # build the message with the length and the crc
    $message = $len . $message . $crc;
    
    # Do magic
    return $tea->EncodeStream($message);
}

sub UnpackEncrypted ($$$$;$)
{
	my ($message, $key, $logLevel, $debugFile, $call_id) = @_;
	my ($out, $len, $crc, $ccrc, @list_key, $tea);
    
	if (not defined $key)
	{
		# default key used in a lot of places
		$key = "E4059999";
	}

	if(length($key) == 8)
	{
		@list_key =(
			(ord(substr($key,3,1))<<24) |
			(ord(substr($key,2,1))<<16) |
			(ord(substr($key,1,1))<<8)  |
			 ord(substr($key,0,1))
			,
			(ord(substr($key,7,1))<<24) |
			(ord(substr($key,6,1))<<16) |
			(ord(substr($key,5,1))<<8)  |
			 ord(substr($key,4,1))
		);
		
        # Duplicate it to get 16 bytes
	    push @list_key, @list_key;
	}
	elsif(length($key) == 16)
	{
		@list_key =(
			(ord(substr($key,3,1))<<24) |
			(ord(substr($key,2,1))<<16) |
			(ord(substr($key,1,1))<<8)  |
			 ord(substr($key,0,1))
			,
			(ord(substr($key,7,1))<<24) |
			(ord(substr($key,6,1))<<16) |
			(ord(substr($key,5,1))<<8)  |
			 ord(substr($key,4,1))
			,
			(ord(substr($key,11,1))<<24) |
			(ord(substr($key,10,1))<<16) |
			(ord(substr($key,9,1))<<8)  |
			 ord(substr($key,8,1))
			,
			(ord(substr($key,15,1))<<24) |
			(ord(substr($key,14,1))<<16) |
			(ord(substr($key,13,1))<<8)  |
			 ord(substr($key,12,1))
		);
	}
	else
	{
		SecureLogIt("UnpackEncrypted: Invalid Key Length: $key", $logLevel, $debugFile);
		return 0;
	}
	
	# set the key
	$tea = Evend::Crypt::TEA->new(Key=>\@list_key);
    
	# decrypt
	$out = $tea->DecodeStream($message);
    
	# first byte is length    
	$len = ord($out);
	
	$out = substr($out,1);
	
	# last 2 bytes are CRC
	$crc = substr($out,$len,2);
	
	# string may be padded because that's what Tea does...
	$out = substr($out,0,$len);
	
	SecureLogIt("$call_id\t" . "Decrypt: [".unpack("H*",$len)."][", $logLevel, $debugFile, unpack("H*",$out), "][".unpack("H*",$crc)."]");
	
	# calculate our own crc to compare to saved one
	$ccrc = getcrc16s($out);
    
	return [($crc eq $ccrc)?1:0, $out];
}

sub getKey ($$$$$)
{
	my ($machineID, $DATABASE, $call_id, $logLevel, $debugFile) = @_;
	SecureLogIt("$call_id\t" . "Looking up encryption key for $machineID", $logLevel, $debugFile);
	
	if($machineID eq DEFAULT_MACHINE_ID)
	{
		SecureLogIt("$call_id\t" . "Returning DEFAULT encryption key", $logLevel, $debugFile);
		return DEFAULT_ENCRYPTION_KEY;
	}
	
	my $array_ref = $DATABASE->select(
							table          => 'device',
							select_columns => 'encryption_key, device_serial_cd',
							where_columns  => [ 'device_name = ?', 'device_active_yn_flag = ?' ],
							where_values   => [ $machineID, 'Y' ] );
							
	if(not $array_ref->[0])
	{
		return (0,undef);
	}
	else
	{
		return ($array_ref->[0][0],$array_ref->[0][1]);
	}
}
