#--------------------------------------------------------------------------------
# Description
#	Provides functionality to export G4 data from USANetwork System to other
#	processing systems
#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		11/05/2003	T Shannon	First version
#									
# 1.5		02/24/2004	P Cowan		Quite a few changes have happened since version was increased.
#									In this case I made a small change to createG4ExportDEXRec
#									to check for 3 0x04's instead of just 1
#
# 1.51		04/29/2004	E Rybski	Made card number logging more secure (only partial card numbers shown)
#
# 1.52		05/10/2004	P Cowan		Bug fix for timestamp adjust problem.
#
# 1.53		08/06/04	P Cowan		Cleanup of Vended Items Description Block parsing.  Fixing a bug where the
#									the Amount value and sum/column was not being calculated correctly.
#


package ReRix::G4Export;

use strict;
use warnings;
use ReRixConfig qw(%ReRix_G4Export);
use Time::Local;
use ReRix::Utils;
use USAT::Security::StringMask qw(mask_credit_card);
use USAT::Database::SensitiveData;

sub createG4ExportRec
{
	my @logs;
	my ($DATABASE, $machine_id, $device_id, $machine_trans_no, $vend_qty, $timestamp, $amt, $card, $card_type, $vended_items) = @_;
	
	my $device_serial_cd;
	my $ftype;
	my @data;
	#my $isdst;
	#my $tzone = 'EST';	#default to EST
	my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy);
	
#	# get the timezone for the terminal's location
#	my $c_location_timezone = $DATABASE->query(
#		query	=> q{ select LOCATION_TIMEZONE from pss.vw_location_by_device_id where device_id = ? },
#		values	=> [ $device_id ]
#	);
#	@data = @{$c_location_timezone->[0]} if defined $c_location_timezone->[0]->[0];
#	$tzone = $data[0] if defined $data[0];
	
	# determine if we have all of the data elements - if we didn't receive a card and card type
	# we're probably being called by net_batch so use the machine_trans_no to obtain that data
	if (!defined $card)
	{
		# get the card and card_type from the transaction_record_hist table; if not present try the transaction_record
		my $c_card_properties_tran_hist = $DATABASE->query(
			query	=> q{ select mag_stripe, card_type, to_char(transaction_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd') from tazdba.transaction_record_hist a where a.machine_trans_no = ? },
			values	=> [ $machine_trans_no ]
		);
		@data = @{$c_card_properties_tran_hist->[0]} if defined $c_card_properties_tran_hist->[0]->[0];
		
		# find out if we're in DST
		my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
		
		if (defined $data[0])
		{
			$card = $data[0];	
			if (defined $data[2])
			{ 	
				($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $data[2]);
				#$timestamp = ReRix::Utils::getAdjustedTimestampReverse($tzone, Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst));
				$timestamp = Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);
			}
			else
			{
				$timestamp = time();
				push @logs, "!!--TIMESTAMP NOT AVAILABLE FROM THE DATABASE - GENERATING A TIMESTAMP USING time()--!!";
			}
			push @logs, "Timestamp = $timestamp";
		
			# if the card_type was not passed (i.e., net_batch() doesn't pass it) then 
			# set the indicator here	
			if (!defined $card_type)
			{
				if ($data[1] eq 'SP')
				{
					$card_type = 'S';
				}
				elsif (defined $data[1])
				{
					# all other card types should be credit/debit cards
					$card_type = 'C';
				}
				else
				{
					push @logs, "Could not identify the card type - '" . $card_type ."'";
				}
					
			}
		}
		else
		{
			# if we reached here the record may not have been moved to the history table
			push @logs, "Could not find the transaction in transaction_record_hist...";
			my $c_card_properties = $DATABASE->query(
				query	=> q{ select mag_stripe, card_type, to_char(transaction_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd') from tazdba.transaction_record a where a.machine_trans_no = ? },
				values	=> [ $machine_trans_no ]
			);
			@data = @{$c_card_properties->[0]} if defined $c_card_properties->[0]->[0];

			if (defined $data[0])
			{
				$card = $data[0];
				if (defined $data[2])
				{ 	
					($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $data[2]);
					#$timestamp = ReRix::Utils::getAdjustedTimestampReverse($tzone, Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst));
					$timestamp = Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);
				}
				else
				{
					$timestamp = time();
					push @logs, "!!--TIMESTAMP NOT AVAILABLE FROM THE DATABASE - GENERATING A TIMESTAMP USING time()--!!";
				}
				push @logs, "Timestamp = $timestamp";

				# if the card_type was not passed (i.e., net_batch() doesn't pass it) then 
				# set the indicator here	
				if (!defined $card_type)
				{
					if ($data[1] eq 'SP')
					{
						$card_type = 'S';
					}
					elsif (defined $data[1])
					{
						# all other card types should be credit/debit cards
						$card_type = 'C';
					}
					else
					{
						push @logs, "Could not identify the card type - '" . $card_type ."'";
					}
				}
			}
			else
			{
				push @logs, "Could not find the transaction in transaction_record table either...";
				push @logs, "Aborting the process and writing a partial rec to the ERROR directory";
			}
		}
	}
	
	# adjust the amount
	$amt *= 100;
	
	# determine the kind of G4 record to build (CREDIT, MISC)
	if ('SR' =~ m/$card_type/)
	{
		# Special = S, Cash = R
		$ftype = 'MISC';
	}
	else
	{
		$ftype = 'CREDIT'
	}
	
	#my $cardreader = CardUtils->new();
	#my ($card_number, undef, undef, undef) = $cardreader->parse($card);
	push @logs, "--------- Export Details ---------";
	push @logs, "Machine ID      : $machine_id";
	push @logs, "Trans Number    : $machine_trans_no";
	push @logs, "Vend Qty        : $vend_qty";
	push @logs, "Tran Date/Time  : $timestamp";
	push @logs, "Tran Amount     : $amt";
	push @logs, "Card            : ".mask_credit_card($card);
	push @logs, "Card Type       : $card_type";
	push @logs, "G4 Category     : $ftype";
	
	# get the location name
	my $c_get_location_name = $DATABASE->query(
		query => q{ select location_name, device_serial_cd from pss.vw_location_by_device_id a where a.device_id = ? },
		values => [ $device_id ]
	);
	@data = @{$c_get_location_name->[0]} if defined $c_get_location_name->[0]->[0];
	
	# the legacy USALive limits the location name to 16 chars
	my $locname;
	if (defined $data[0]){
		$locname = substr($data[0],0,16);
		$locname =~ s/,//g;
	}
	else
	{
		$locname = "UNKNOWN";
	}
	
	# obtain the device serial number... if not found use the machine id and 
	# note it in the logs
	if (defined $data[1]){
		$device_serial_cd = $data[1];
	}
	else
	{
		$device_serial_cd = $machine_id;
		push @logs, "************ ALERT *****************";
		push @logs, "COULD NOT OBTAIN THE DEVICE_SERIAL_CD";
		push @logs, "FROM THE DATABASE USING THE DEVICE_ID.";
		push @logs, "WRITING THE DEVICE_ID TO THE G4 RECORD";
		push @logs, "INSTEAD.";
		push @logs, "*************************************";		
	}
	
	# if we haven't set the card type by now it's because we couldn't find
	# the batched record in the database or didn't recognize what was passed, 
	# so note it in the logs
	if (!defined $card_type)
	{
		push @logs, "************ ALERT *****************";
		push @logs, "THE TRANSACTION WAS EITHER A BATCHED";
		push @logs, "TRANSACTION NOT FOUND IN THE";
		push @logs, "DATABASE OR THE CARD TYPE COULD NOT BE";
		push @logs, "IDENTIFIED. THE RECORD WAS WRITTEN TO"; 
		push @logs, "THE ERROR DIRECTORY.";
		push @logs, "*************************************";
		$ftype = 'FAILED';
	}

	# start building the G4 rec
	my $str_tmp = "$device_serial_cd,$locname,";
	my $g4rec = $str_tmp;
	my $g4rec_secured = $str_tmp;

	# format the trandate,trantime info
	($sec, $min, $hr, $dy, $mon, undef, undef, undef, undef) = gmtime($timestamp);
	$mon = ($mon+1);
	
	$str_tmp = sprintf("%02d/%02d,%02d:%02d,", $mon, $dy, $hr, $min);
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;
	
	# the G4 Category affects how we build the middle of the record
	if ($ftype eq 'CREDIT')
	{
		# add the apcode, sale, data1, merch, card, data2
		$g4rec .= sprintf("APGReRix,%05d,0000,E.179001415993577953,$card,~,","$amt");
		$g4rec_secured .= sprintf("APGReRix,%05d,0000,E.179001415993577953,".mask_credit_card($card).",~,","$amt");
		
		# add the startdate,starttime,data3,data4
		$str_tmp = sprintf("%02d/%02d,%02d:%02d,0000,~,", $mon, $dy, $min, $sec);
		$g4rec .= $str_tmp;
		$g4rec_secured .= $str_tmp;
	}
	else	# passcard, cash
	{
		# add the card, sale, data3, data4
		$g4rec .= sprintf("$card,%05d,0000,~,","$amt");
		$g4rec_secured .= sprintf(mask_credit_card($card).",%05d,0000,~,","$amt");
		
		# add the stopdate,stoptime,data5,data6
		$str_tmp = sprintf("%02d/%02d,%02d:%02d,0000,~,", $mon, $dy, $min, $sec);
		$g4rec .= $str_tmp;
		$g4rec_secured .= $str_tmp;
	}
			
	# -- build the vend column string in G4 format --
	# first, create an array containing the 'denormalized' vend list of columns
	my @vends;
	my $vend_counter = 0;
	foreach my $key (sort {$vended_items->{$a}{ORDER} <=> $vended_items->{$b}{ORDER}} keys %$vended_items)
	{		
		if (defined($vended_items->{$key}{QTY}))
		{
			for (my $i = 0; $i < $vended_items->{$key}{QTY}; $i++)
			{
				$vend_counter++;
				# for each item vended create a placeholder in the array; $key is the 
				# column number we want to place in the final string
				push @vends, $key;
				push @logs, "Vend $vend_counter          : $key";
			}
		}
	}
	
	# finish building the record
	$str_tmp = buildSrvCntString($vend_qty, @vends);
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;
		
	# append machine trans no
	$str_tmp = ",$machine_trans_no";
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;
	
	### write this data to the database (raw string parsed by insert trigger) ###
	if ($ftype eq 'CREDIT' || $ftype eq 'MISC')
	{
		my $insert_data_table;
		if ($ftype eq 'CREDIT')
		{
			$insert_data_table = 'export_credit';
		}
		elsif ($ftype eq 'MISC')	# passcard, cash
		{
			$insert_data_table = 'export_passaccess';
		}
		my $insert_data = $DATABASE->do(
			query	=> qq{INSERT INTO $insert_data_table (raw_data) VALUES (?)}
			,values	=> [ USAT::Database::SensitiveData->new($g4rec, $g4rec_secured) ]
		);
	}
	
	return (\@logs);
	
}

sub createG4ExportCashRec
{
	my @logs;
	my ($DATABASE, $machine_id, $device_id, $machine_trans_no, $amt, $column, $timestamp) = @_;
	
	my $item_nbr;
	my $item_amt;
	my $item_tmp;
	my $card = 'Currency Vend';
	my $i;
	my $device_serial_cd;
	my $ftype = 'MISC';
	my @data;
	#my $isdst;
	#my $tzone = 'EST';	#default to EST
	my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy);

	# get the timezone for the terminal's location
	#$sth = $dbh->prepare("select LOCATION_TIMEZONE from pss.vw_location_by_device_id where device_id = ?");
	#$sth->execute($device_id);
	#@data = $sth->fetchrow_array();
	#$tzone = $data[0] if defined $data[0];
				
	# adjust the amount
	$amt *= 100;
		
	push @logs, "--------- Cash Export Details ---------";
	push @logs, "Machine ID      : $machine_id";
	push @logs, "Vend Col        : $column";
	push @logs, "Tran Date/Time  : $timestamp";
	push @logs, "Tran Amount     : $amt";
	push @logs, "G4 Category     : $card";
	
	# get the location name
	my $c_get_location_name = $DATABASE->query(
		query => q{ select location_name, device_serial_cd from pss.vw_location_by_device_id a where a.device_id = ? },
		values => [ $device_id ]
	);
	@data = @{$c_get_location_name->[0]} if defined $c_get_location_name->[0]->[0];

	# the legacy USALive limits the location name to 16 chars
	my $locname;
	if (defined $data[0]){
		$locname = substr($data[0],0,16);
		$locname =~ s/,//g;
	}
	else
	{
		$locname = "UNKNOWN";
	}
	
	# obtain the device serial number... if not found use the machine id and 
	# note it in the logs
	if (defined $data[1]){
		$device_serial_cd = $data[1];
	}
	else
	{
		$device_serial_cd = $machine_id;
		push @logs, "************ ALERT *****************";
		push @logs, "COULD NOT OBTAIN THE DEVICE_SERIAL_CD";
		push @logs, "FROM THE DATABASE USING THE DEVICE_ID.";
		push @logs, "WRITING THE DEVICE_ID TO THE G4 RECORD";
		push @logs, "INSTEAD.";
		push @logs, "*************************************";		
	}
	
	# start building the G4 rec
	my $str_tmp = "$device_serial_cd,$locname,";
	my $g4rec = $str_tmp;
	my $g4rec_secured = $str_tmp;

	# format the trandate,trantime info
	($sec, $min, $hr, $dy, $mon, undef, undef, undef, undef) = gmtime($timestamp);
	$str_tmp = sprintf("%02d/%02d,%02d:%02d,", $mon + 1, $dy, $hr, $min);
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;

	# add the card, sale, data3, data4
	$g4rec .= sprintf("$card,%05d,0000,~,","$amt");
	$g4rec_secured .= sprintf(mask_credit_card($card).",%05d,0000,~,","$amt");
	
	# add the stopdate,stoptime,data5,data6
	$str_tmp = sprintf("%02d/%02d,%02d:%02d,0000,~,", $mon + 1, $dy, $min, $sec);
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;

	my @vends;
	push @vends, $column;

	# finish building the record
	$str_tmp = buildSrvCntString(sprintf("%02d", 1), @vends);
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;
			
	# append machine trans no
	$str_tmp = ",$machine_trans_no";
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;
	
	### write this data to the database (raw string parsed by insert trigger) ###
	my $insert_data = $DATABASE->do(
		query	=> q{INSERT INTO export_passaccess (raw_data) VALUES (?)}
		,values	=> [ USAT::Database::SensitiveData->new($g4rec, $g4rec_secured) ]
	);

	return (\@logs);
	
}

sub createG4ErrorRec
{
	my @logs;
	my ($DATABASE, $machine_id, $device_id, $machine_trans_no, $device_serial_cd, $timestamp, $card) = @_;
	
	my $ftype = 'ERROR';

	push @logs, "--------- Error Export Details ---------";
	push @logs, "Machine ID             : $machine_id";
	push @logs, "Timestamp              : $timestamp";
	push @logs, "Error Message          : $card";
	
	# get the location name
	my @data;
	my $c_get_location_name = $DATABASE->query(
		query => q{ select location_name, device_serial_cd from pss.vw_location_by_device_id a where a.device_id = ? },
		values => [ $device_id ]
	);
	@data = @{$c_get_location_name->[0]} if defined $c_get_location_name->[0]->[0];
	
	# the legacy USALive limits the location name to 16 chars
	my $locname;
	if (defined $data[0]){
		$locname = substr($data[0],0,16);
		$locname =~ s/,//g;
	}
	else
	{
		$locname = "UNKNOWN";
	}
	
	# process the different types of error messages and format them properly...
	# 
	if($card =~ m/^Err\.DEX/)
	{
		$card = "Err.DEX Fill";
	}
	elsif($card =~ m/^ErrorSystem/)
	{
		$card = "Error System";
	}
	else
	{
		$card = substr($card, 0, index($card, "-->"));
	}
	
	push @logs, "Formatted Message      : $card";
	
	# start building the G4 rec
	my $str_tmp = "$device_serial_cd,$locname,";
	my $g4rec = $str_tmp;
	my $g4rec_secured = $str_tmp;

	# format the trandate,trantime info
	my ($sec, $min, $hr, $dy, $mon, undef, undef, undef, undef) = gmtime($timestamp);
	$str_tmp = sprintf("%02d/%02d,%02d:%02d,", $mon + 1, $dy, $hr, $min);
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;

	# add the card, sale, data3, data4
	$g4rec .= $card;
	$g4rec_secured .= mask_credit_card($card);
	
	# append machine trans no
	$str_tmp = ",$machine_trans_no";
	$g4rec .= $str_tmp;
	$g4rec_secured .= $str_tmp;
		
	### write this data to the database (raw string parsed by insert trigger) ###
	my $insert_data = $DATABASE->do(
		query	=> q{INSERT INTO export_error (raw_data) VALUES (?)}
		,values	=> [ USAT::Database::SensitiveData->new($g4rec, $g4rec_secured) ]
	);

	return (\@logs);
}

sub createG4ExportDEXRec
{
	my @logs;
	my ($DATABASE, $machine_id, $device_id, $SSN, $data) = @_;
	
	my $ftype = 'DEX_DATA';
	
	# g4 may send 3 0x04 characters because that marks the end of the file in memory.  if it's there delete it
	if($data =~ m/\x04{3}/)
	{
		push @logs, "DEX Cleanup       : Removing EOT (04h) characters that where left by the G4";
		$data =~ s/\x04{3}//g;
	}
	
	# the first line of the DEX file is a header to identify the DEX data...  earlier versions of the 
	# G4 didn't put DEX- in front of the E4 number, and it screwed of some legacy apps...  so we have
	# to insert it if it's not there
	
	if(substr($data, 0, 4) ne 'DEX-')
	{
		push @logs, "DEX Cleanup       : G4 ommited DEX- header, inserting in to compensate";
		$data = 'DEX-' . $data;
	}
	
#	# prepare to create a file to write the record
#	my ($fsec,$fmin,$fhour,$fmday,$fmon,$fyear,$fwday,$fyday,$fisdst) = localtime(time);
#	$fyear += 1900;
#	$fmon++;
#	my ($pathlog, $fname, $ext) = (
#		"$ReRix_G4Export{log_root_path}/",
#		join('_', ($ftype, $fyear, $fmon, $fmday)),
#		'log'
#	);
#	if (open (G4EXPORT, ">>$pathlog$fname.$ext"))
#	{
#		# write the record and immediately close the file
#		print G4EXPORT ''.time().'_DEX-'."$SSN,$data\n";
#		close G4EXPORT;
#		push @logs, "Appended to G4 Export file $fname.$ext";
#	}
#	else
#	{
#		push @logs, "An error occurred trying to create or append to G4 Export file $pathlog$fname.$ext";
#	}
	# prepare to create a file to write the record
	my ($pathdat, $pathlog, $fname, $ext) = ("$ReRix_G4Export{data_root_path}/DEX_DATA/", "$ReRix_G4Export{log_root_path}/", ('DEX-' . $SSN . '-' . time()), ".new");
						
	if (open (G4EXPORT, "> $pathdat$fname$ext"))
	{
		# write the record and immediately close the file
		print G4EXPORT $data;
		close G4EXPORT;
		
		# after closing the file change its extension to 'log' so the batching process 
		# will pick it up **after** the file is completely written and closed
		rename $pathdat . $fname . $ext, $pathdat . $fname . ".log";
		push @logs, "Saved G4 DEX file as $fname.log";
		
		# log the result of the write
		my $logname = "g4export.log";
		if (open (G4LOG, ">> $pathlog$logname"))
		{
			printf G4LOG "[%s", localtime() . "] Successfully created $pathdat$fname.log (createG4ExportDEXRec)\n";
			close(G4LOG);
		}
		else
		{
			push @logs, "Could not write entry to g4export.log";
		}	
	}
	else
	{
		push @logs, "An error occurred trying to create G4 Export file $pathdat$fname.$ext";
	}
	
	return (\@logs);
}


sub buildSrvCntString
{
	my ($vend_qty, @vends) = @_;
	# the first 4 chars are = 2 chars for total vend count + 2 chars for the first column vended
	my $vendstr = "~," . sprintf("%02d", $vend_qty) . sprintf("%02d",$vends[0]) . ",~,";
	my ($i, $j);
	for ($i=1; $i<10; )
	{
		for ($j=0; $j<2; $j++)
		{
			if (defined $vends[$i])
			{
				$vendstr .= sprintf("%02d",$vends[$i]);
			}
			else
			{
				$vendstr .= "00";
			}
			$i++;
			
		}
		if ($i < 10)
		{
			$vendstr .= ",~,";
		}
	}
	return $vendstr;	
	
}

1;
