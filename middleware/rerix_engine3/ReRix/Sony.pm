package ReRix::Sony;

use strict;
use Evend::ReRix::Shared;
use ReRix::Utils;

# Sony Info packet
sub parse_info
{
	my ( $DATABASE, $command_hashref ) = @_;
	my @logs;
	my $raw_handle = $DATABASE->{handle};
	$raw_handle->{RaiseError} = 1;

	push @logs, "Inbound Sony Command: $command_hashref->{'inbound_command'}";

	my $info = lc($command_hashref->{'inbound_command'});
	
	eval 
	{
		if ( $info =~ s/^5f530101// )
		{
			# Sales Summary

			printf("Sony Sales Summary received\n");
			push @logs, "Sony Sales Summary received";

			# MDH: Added 0's to the mac_id, local auths, and local auth declines until
			# sony finished the spec.
		
			my $csr = $raw_handle->prepare(	q{BEGIN	INSERT_SONY_SALES(	?, ?, 0,
											to_date(?, 'YYYY/MM/DD'),
											?, ?, ?, ?,
											?, ?, ?, ?, ?, ?,
											?, ?, ?, ?, ?, ?, ?,
											?, ?, ?, ?, 0); END; } );

			my $i = 1;
			foreach ( $command_hashref->{'machine_id'}, split( ',', MakeString($info) ) )
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^5f5504ef// )
		{
			# Prints Remaining
			printf("Sony Prints Remaining Received\n");
			push @logs, "Sony Prints Remaining Received";

			# my $prints_remaining = ord(MakeString(substr($info, 0, 4)));
			# my $printer_num = ord(MakeString(substr($info, 4, 2)));
			my $prints_remaining = hex( substr( $info, 0, 4 ) );
			my $printer_num	  = hex( substr( $info, 4, 2 ) );

			my $csr = $raw_handle->prepare(
						q{BEGIN UPDATE_PRINTER_REMAINING( ?, ?, ?); END; });
			my $i = 1;
			foreach (($command_hashref->{'machine_id'}, $prints_remaining, $printer_num))
			{
				push @logs, "   Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^5f5504f3// )
		{
			# Total Prints
			printf("Sony Total Prints Received\n");
			push @logs, "Sony Total Prints Received";

			#my $total_prints = ord(MakeString(substr($info, 0, 6)));
			#my $printer_num = ord(MakeString(substr($info, 6, 2)));
			my $total_prints = hex( substr( $info, 0, 6 ) );
			my $printer_num  = hex( substr( $info, 6, 2 ) );

			my $csr = $raw_handle->prepare(
					 	q{BEGIN UPDATE_PRINTER_ITEMS_PRINTED(?, ?, ?); END; });
			my $i = 1;
			foreach (($command_hashref->{'machine_id'}, $total_prints, $printer_num))
			{
				push @logs, "   Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}

			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^5f5521..// )
		{
			# Sales Summary
			push @logs, " Sony Error Message received";

			my $alert_context = ord( MakeString( substr( $info, 0, 2 ) ) );
			my $alert_msg	 = ord( MakeString( substr( $info, 2, 2 ) ) );
			my $printer_id	= ord( MakeString( substr( $info, 4, 2 ) ) );

			my $csr = $raw_handle->prepare(
				q{	BEGIN INSERT_ALERT( ?, ?, ?, to_date(?, 'MM/DD/YYYY HH24:MI:SS'), ?, ?); END; } );

			my $i = 1;
			foreach ( (	$command_hashref->{'machine_id'},
					  	$printer_id, '2', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context))
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^5f5503..// )
		{
			# Sales Summary
			push @logs, "   Sony  Error Message Received";

			my $alert_context 	= ord( MakeString( substr( $info, 0, 2 ) ) );
			my $alert_msg	 	= ord( MakeString( substr( $info, 2, 2 ) ) );
			my $printer_id		= ord( MakeString( substr( $info, 4, 2 ) ) );

			my $csr = $raw_handle->prepare(	q{BEGIN INSERT_ALERT(	?, ?, ?,
											to_date(?, 'MM/DD/YYYY HH24:MI:SS'),
											?, ?); END; } );

			my $i = 1;
			foreach ( ( $command_hashref->{'machine_id'},
						$printer_id, '3', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context ) )
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}

		my $sony_info = substr( $command_hashref->{'inbound_command'}, 2 );
		push @logs, "  Insert Sony   Info: $info";

		$DATABASE->insert(	table		  	=> 'SONY_INFO',
							insert_columns 	=> 'MACHINE_ID, INFO',
							insert_values 	=> [ $command_hashref->{'machine_id'}, $sony_info ] );

		return 1;
	}
	or push @logs, "Error Occurred $!: $@" . ( $raw_handle and $raw_handle->err
					and "\n\t[" . $raw_handle->err . "] (" . $raw_handle->state . "): " . $raw_handle->errstr );

	return (\@logs);
}

sub parse_info_V2
{
	my ( $DATABASE, $command_hashref ) = @_;
	my @logs;
	my $raw_handle = $DATABASE->{handle};
	$raw_handle->{RaiseError} = 1;
	push @logs, "Inbound Sony Command: $command_hashref->{'inbound_command'}";

	my $info = lc($command_hashref->{'inbound_command'});
	my $msg_nbr = $command_hashref->{msg_no};

	eval 
	{
		if ( $info =~ s/^b9530101// )
		{

			# Sales Summary
			printf("Sony Sales Summary received\n");
			push @logs, "Sony Sales Summary received";

			#MDH: Added 0's to the mac_id, local auths, and local auth declines until
			#sony finished the spec.

			my $csr = $raw_handle->prepare(	q{BEGIN INSERT_SONY_SALES(	?, ?, 0,
											to_date(?, 'YYYY/MM/DD'),
											?, ?, ?, ?,
											?, ?, ?, ?, ?, ?,
											?, ?, ?, ?, ?, ?, ?,
											?, ?, ?, ?, 0); END; } );

			my $i = 1;
			foreach ( $command_hashref->{'machine_id'}, split( ',', MakeString($info) ) )
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^b95504ef// )
		{
			# Prints Remaining
			printf("Sony Prints Remaining Received\n");
			push @logs, "Sony Prints Remaining Received";

			#my $prints_remaining = ord(MakeString(substr($info, 0, 4)));
			#my $printer_num = ord(MakeString(substr($info, 4, 2)));
			my $prints_remaining = hex( substr( $info, 0, 4 ) );
			my $printer_num	  = hex( substr( $info, 4, 2 ) );

			my $csr = $raw_handle->prepare(	q{BEGIN UPDATE_PRINTER_REMAINING( ?, ?, ?); END; } );

			my $i = 1;
			foreach (($command_hashref->{'machine_id'}, $prints_remaining, $printer_num))
			{
				push @logs, "   Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^b95504f3// )
		{
			# Total Prints
			printf("Sony Total Prints Received\n");
			push @logs, "Sony Total Prints Received";

			#my $total_prints = ord(MakeString(substr($info, 0, 6)));
			#my $printer_num = ord(MakeString(substr($info, 6, 2)));
			my $total_prints = hex( substr( $info, 0, 6 ) );
			my $printer_num  = hex( substr( $info, 6, 2 ) );

			my $csr = $raw_handle->prepare(q{BEGIN UPDATE_PRINTER_ITEMS_PRINTED(?, ?, ?); END;});
			my $i = 1;
			foreach (($command_hashref->{'machine_id'}, $total_prints, $printer_num))
			{
				push @logs, "   Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		#elsif($info =~ s/^b95502..//)
		elsif ( $info =~ s/^b95502// )
		{
			# Sales Summary
			push @logs, " Sony Error Message received";
			push @logs, "	 Info (asc)  : $info";

			#my $alert_context = substr($info, 0, 2);
			my $alert_context = hex( substr( $info, 0, 2 ) );

			#my $alert_msg = substr($info, 2, 2);	needs to be interpreted as numeric
			#my $alert_msg = ord(pack("H*",substr($info, 2, 2)));
			my $alert_msg  = hex( substr( $info, 2, 2 ) );
			my $printer_id = hex( substr( $info, 4, 2 ) );

			push @logs, "Command code $alert_context";
			push @logs, "Message $alert_msg";
			push @logs, "Printer $printer_id";

			my $csr = $raw_handle->prepare(q{BEGIN INSERT_ALERT(	?, ?, ?,
											to_date(?, 'MM/DD/YYYY HH24:MI:SS'),
											?, ?); END; } );

			my $i = 1;
			foreach ((	$command_hashref->{'machine_id'},
						$printer_id, '2', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context ))
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		#elsif($info =~ s/^b95503..//)
		elsif ( $info =~ s/^b95503// )
		{
			# Sales Summary
			push @logs, "   Sony  Status Message Received";

			my $alert_context = hex( substr( $info, 0, 2 ) );
			my $alert_msg	 = hex( substr( $info, 2, 2 ) );
			my $printer_id	= hex( substr( $info, 4, 2 ) );

			my $csr = $raw_handle->prepare(	q{BEGIN INSERT_ALERT(	?, ?, ?,
											to_date(?, 'MM/DD/YYYY HH24:MI:SS'),
											?, ?); END; });

			my $i = 1;
			foreach ((	$command_hashref->{'machine_id'},
						$printer_id, '3', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context))
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}

			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^b95505// )
		{
			# Sales Summary
			push @logs, "   Sony  Other System Error Message Received";

			my $alert_context = hex( substr( $info, 0, 2 ) );
			my $alert_msg	 = hex( substr( $info, 2, 2 ) );
			my $printer_id	= hex( substr( $info, 4, 2 ) );

			my $csr = $raw_handle->prepare(	q{BEGIN
											INSERT_ALERT(	   ?, ?, ?,
											to_date(?, 'MM/DD/YYYY HH24:MI:SS'),
											?, ?); END; });
			my $i = 1;
			foreach ((	$command_hashref->{'machine_id'},
						$printer_id, '5', $command_hashref->{'inbound_date'},
						$alert_msg, $alert_context))
			{
				push @logs, "	 Bind Param $i: $_";
				$csr->bind_param( $i++, $_ );
			}
			$csr->execute();
			$csr->finish();
		}
		elsif ( $info =~ s/^b9530601// )
		{
			push @logs, "   Sony Info Text ".MakeString($info);
			&insert_sony_sales($DATABASE, $command_hashref, $info, \@logs);
		}
		elsif ( $info =~ s/^b95507//)
		{
			push @logs, "   Error Message and Status Information $info";
			&insert_alert($DATABASE, $command_hashref, 7, $info, \@logs);
		}
		elsif ( $info =~ s/^b95508a0// )
		{
		# Printer Info 
			push @logs, "   Sony Printer Info Message Received $info";
			&update_printer_info($DATABASE, $command_hashref, $info, \@logs);
		}
		elsif ( $info =~ s/^b95508ef// )
		{
			# Printer Info
			push @logs, "   Sony Printer Info Message Received (Prints Remaining) $info";
			&update_printer_remaining($DATABASE, $command_hashref, $info, \@logs);
		}
		elsif ( $info =~ s/^b95509f3// )
		{
			#Total Thermal Head Prints
			push @logs, "  Message not implemented yet";
		}

		my $sony_info = substr( $command_hashref->{'inbound_command'}, 2 );

		push @logs, "  Insert Sony   Info: $info";
		push @logs, "  Message Nbr	   : " . unpack( "H*", $msg_nbr );

		$DATABASE->insert(
			   table		  => 'SONY_INFO',
			   insert_columns => 'MACHINE_ID, INFO',
			   insert_values => [ $command_hashref->{'machine_id'}, $sony_info ] );

		# create the generic ACK packet
		my $response = pack("CCC", 0x01, 0x2f, $msg_nbr);
					   		
		ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));

		return 1;
	}
	or push @logs, "Error Occurred $!: $@" . ( $raw_handle and $raw_handle->err
					and "\n\t[" . $raw_handle->err . "] (" . $raw_handle->state . "): " . $raw_handle->errstr );

	return (\@logs);
}

sub insert_alert($$$$$) {
	my $DATABASE           = shift;
	my $command_hashref    = shift;
	my $alert_msg_type     = shift;
	my $info               = shift;
	my $logs               = shift;

	my $machine_id         = $command_hashref->{'machine_id'};
	my $alert_ts           = $command_hashref->{'inbound_date'};
	my $alert_context_code = ord( MakeString( substr( $info, 0, 2 ) ) );
	my $alert_msg_code     = ord( MakeString( substr( $info, 2, 2 ) ) );
	my $printer_num        = ord( MakeString( substr( $info, 4, 2 ) ) );

	my $rows;
	my $alert_msg_id;
	my $alert_context_id;
	my $sony_printer_id;

	my $array_ref = $DATABASE->query(
		query => q{
			SELECT alert_msg_id
			  FROM alert_msg
			 WHERE alert_msg_code = ? 
			   AND alert_msg_type = ?
		},
		values => [ $alert_msg_code, $alert_msg_type ]
	);

	if(defined $array_ref->[0][0]) {
		$alert_msg_id = $array_ref->[0][0];
	} else {
		my $count = $DATABASE->do(
			query => q{
				INSERT INTO alert_msg
					( alert_msg_type, alert_display_msg, priority, alert_msg_code )
				VALUES ( ?, ?, ?, ? )
			},
			values => [ $alert_msg_type, 'Unknown Message #'.$alert_msg_code , 1, $alert_msg_code ]
		);
		if($count > 0) {
			$array_ref = $DATABASE->query(
				query => q{
					SELECT alert_msg_id
					  FROM alert_msg
					 WHERE alert_msg_code = ? 
					   AND alert_msg_type = ?
				},
				values => [ $alert_msg_code, $alert_msg_type ]
			);
			if(defined $array_ref->[0][0]) {
				$alert_msg_id = $array_ref->[0][0];
			} else {
				push @{$logs}, "   Sony Printer : Can't get alert_msg_id";
				my $message = "Sony: unexpected issue processing method insert_alert for Machine ID $machine_id (line ".(caller)[2].")";
				ReRix::Utils::send_alert($message,$command_hashref);
			}
		} else {
			push @{$logs}, "   Sony Printer : Can't insert into alert_msg";
			# error and email
			my $message = "Sony : unexpected issue processing method insert_alert for Machine ID $machine_id (line ".(caller)[2].")";
			ReRix::Utils::send_alert($message,$command_hashref);
		}
	}

	$array_ref = $DATABASE->query(
		query => q{
			SELECT alert_context_id
			  FROM alert_context
			 WHERE alert_context_code = ?
		},
		values => [ $alert_context_code ]
	);

	if(defined $array_ref->[0][0]) {
		$alert_context_id = $array_ref->[0][0];
	} else {
		my $count = $DATABASE->do(
			query => q{
				INSERT INTO alert_context
					( context_display_msg, alert_context_code )
				VALUES ( ?, ? )
			},
			values => [ 'Unknown Context #'.$alert_context_code, $alert_context_code ]
		);
		if($count > 0) {
			$array_ref = $DATABASE->query(
				query => q{
					SELECT alert_context_id
					  FROM alert_context
					 WHERE alert_context_code = ?
				},
				values => [ $alert_context_code ]
			);
			if(defined $array_ref->[0][0]) {
				$alert_context_id = $array_ref->[0][0];
			} else {
				push @{$logs}, "   Sony Printer : Can't get into alert_context_id";
				#error
				my $message = "Sony : unexpected issue processing method insert_alert for Machine ID $machine_id (line ".(caller)[2].")";
				ReRix::Utils::send_alert($message,$command_hashref);
			}
		} else {
			push @{$logs}, "   Sony Printer : Can't insert into alert_context";
			# error and email
			my $message = "Sony : unexpected issue processing method insert_alert for Machine ID $machine_id (line ".(caller)[2].")";
			ReRix::Utils::send_alert($message,$command_hashref);
		}
	}

	$array_ref = $DATABASE->query(
		query => q{
			SELECT sony_printer_id
			  FROM sony_printer
			 WHERE machine_id  = ?
			   AND printer_num = ?
		},
		values => [ $machine_id, $printer_num ]
	);

	if(defined $array_ref->[0][0]) {
		$sony_printer_id = $array_ref->[0][0];
	} else {
		my $count = $DATABASE->do(
			query => q{
				INSERT INTO sony_printer
					( machine_id, printer_num )
				VALUES ( ?, ? )
			},
			values => [ $machine_id, $printer_num ]
		);
		if($count > 0) {
			$array_ref = $DATABASE->query(
				query => q{
					SELECT sony_printer_id
					  FROM sony_printer
					 WHERE machine_id  = ?
					   AND printer_num = ?
				},
				values => [ $machine_id, $printer_num  ]
			);
			if(defined $array_ref->[0][0]) {
				$sony_printer_id = $array_ref->[0][0];
			} else {
				push @{$logs}, "   Sony Printer : Can't get into sony_printer_id";
				#error
				my $message = "Sony : unexpected issue processing method insert_alert for Machine ID $machine_id (line ".(caller)[2].")";
				ReRix::Utils::send_alert($message,$command_hashref);
			}
		} else {
			push @{$logs}, "   Sony Printer : Can't insert into sony_printer";
			# error and email
			my $message = "Sony : unexpected issue processing method insert_alert for Machine ID $machine_id (line ".(caller)[2].")";
			ReRix::Utils::send_alert($message,$command_hashref);
		}
	}
	
	my $alert_id;
	my $count = $DATABASE->do(
		query => q{
			INSERT INTO alert
				( machine_id, sony_printer_id, alert_ts, alert_msg_id, alert_context_id )
			VALUES ( ?, ?, ?, ?, ? )
		},
		values => [ $machine_id, $sony_printer_id, $alert_ts, $alert_msg_id, $alert_context_id ]
	);
	if($count <= 0) {
		push @{$logs}, "   Sony Printer : Can't insert into alert";
		# error and email
		my $message = "Sony : unexpected issue processing method insert_alert for Machine ID $machine_id (line ".(caller)[2].")";
		ReRix::Utils::send_alert($message,$command_hashref);
	}
}

sub update_printer_remaining($$$$) {
	my $DATABASE    = shift;
	my $command_hashref = shift;
	my $info        = shift;
	my $logs        = shift;

	my $machine_id  = $command_hashref->{'machine_id'};
	my $printer_num = hex( substr( $info,  0, 2 ) );
	my $remaining   = hex( substr( $info,  2, 4 ) );

	my $array_ref = $DATABASE->query(
		query => q{
			SELECT sony_printer_id
			FROM sony_printer
			WHERE machine_id  = ?
			AND printer_num = ?
		},
		values => [ $machine_id, $printer_num ]
	);

	if(defined $array_ref->[0][0]) {
		my $count = $DATABASE->do(
			query => q{
				UPDATE sony_printer
				   SET prints_remaining_upd_ts = SYSDATE,
				       total_prints_remaining = ?
				 WHERE sony_printer_id = ?
			},
			values => [ $remaining, $array_ref->[0][0] ]
		);
		if($count > 0) {
			push @{$logs}, "   Sony Printer : total_prints_remaining update succesful";
		} else {
			push @{$logs}, "   Sony Printer : total_prints_remaining update failure";
			my $message = "Sony : unexpected issue processing method update_printer_remaining for Machine ID $machine_id (line ".(caller)[2].")";
			ReRix::Utils::send_alert($message,$command_hashref);
		}
	} else {
		my $count = $DATABASE->do(
			query => q{
				INSERT INTO sony_printer
					( machine_id, printer_num, total_prints_remaining )
				VALUES ( ?, ?, ? )
			},
			values => [ $machine_id, $printer_num, $remaining ]
		);
		if($count > 0) {
			push @{$logs}, "   Sony Printer : total_prints_remaining insert succesful";
		} else {
			push @{$logs}, "   Sony Printer : total_prints_remaining insert failure";
			my $message = "Sony : unexpected issue processing method update_printer_remaining for Machine ID $machine_id (line ".(caller)[2].")";
			ReRix::Utils::send_alert($message,$command_hashref);
		}
	}
}

sub update_printer_info($$$$) {
	my $DATABASE    = shift;
	my $command_hashref = shift;
	my $info        = shift;
	my $logs        = shift;

	my $length    = 0;
	my $remaining = 0;
	my $machine_id           = $command_hashref->{'machine_id'};
	
	my $printer_num          = hex( substr( $info,  $length, 2 ) );
	$length += 2;
	
	my $printer_status_type  = hex( substr( $info,  $length, 2 ) );
	$length += 2;
	
	my $length_of_label      = 2*hex( substr( $info,  $length, 2 ) );
	$length += 2;
	
	my $printer_label        = hex_to_ascii( substr( $info, $length, $length_of_label ) );
	$length += $length_of_label;
	
	my $length_of_model      = 2*hex( substr( $info, $length, 2 ) );
	$length += 2;
	
	my $printer_model        = hex_to_ascii( substr( $info, $length, $length_of_model ) );
	$length += $length_of_model;
	
	my $length_of_firmware_version = 2*hex( substr( $info, $length, 2 ) );
	$length += 2;
	
	my $firmware_version     = hex_to_ascii( substr( $info, $length, $length_of_firmware_version ) );
	$length +=  $length_of_firmware_version;
	
	my $length_of_serial_number = 2*hex( substr( $info, $length, 2 ) );
	$length += 2;
	
	my $serial_number        = hex_to_ascii( substr( $info, $length, $length_of_serial_number ) );

	my $array_ref = $DATABASE->query(
		query => q{
			SELECT sony_printer_id
			  FROM sony_printer
			 WHERE machine_id  = ?
			   AND printer_num = ?
		},
		values => [ $machine_id, $printer_num ]
	);

	if(defined $array_ref->[0][0]) {
		my $count = $DATABASE->do(
			query => q{
				UPDATE sony_printer
				   SET prints_remaining_upd_ts  = SYSDATE,
					   printer_label            = ?,
					   printer_model            = ?,
					   sony_printer_status_type_id = ?,
					   printer_version          = ?,
					   printer_serial_cd        = ?
				 WHERE sony_printer_id = ?
			},
			values => [
				$printer_label, $printer_model, $printer_status_type,
				$firmware_version, $serial_number, $array_ref->[0][0]
			]
		);
		if($count > 0) {
			push @{$logs}, "   Sony Printer : sony_printer update succesful";
		} else {
			push @{$logs}, "   Sony Printer : sony_printer update failure";
			my $message = "Sony : unexpected issue processing method sony_printer for Machine ID $machine_id (line ".(caller)[2].")";
			ReRix::Utils::send_alert($message,$command_hashref);
		}
	} else {
		my $count = $DATABASE->do(
			query => q{
				INSERT INTO sony_printer
					( machine_id, printer_num, printer_label, printer_model,
					  sony_printer_status_type_id, printer_version, printer_serial_cd, prints_remaining_upd_ts )
				VALUES ( ?, ?, ?, ?, ?, ?, ?, SYSDATE )
			},
			values => [
				$machine_id, $printer_num, $printer_label, $printer_model,
				$printer_status_type, $firmware_version, $serial_number 
			]
		);
		if($count > 0) {
			push @{$logs}, "   Sony Printer : sony_printer insert succesful";
		} else {
			push @{$logs}, "   Sony Printer : sony_printer insert failure";
			my $message = "Sony : unexpected issue processing method update_printer_info for Machine ID $machine_id (line ".(caller)[2].")";
			ReRix::Utils::send_alert($message,$command_hashref);
		}
	}
}

sub insert_sony_sales($$$$) {
	my $DATABASE           = shift;
	my $command_hashref    = shift;
	my $info               = shift;
	my $logs               = shift;

	#general system data
	# $l_trans             sony_sales_summary.total_transactions%TYPE,
	# $l_sales_date        sony_sales_summary.sales_ts%TYPE,
	# $l_active            sony_sales_summary.total_minutes_active%TYPE,
	# $l_keep_shopping     sony_sales_summary.total_keep_shopping_selected%TYPE,
	# $l_selected          sony_sales_summary.total_pictures_selected%TYPE,
	# $l_printed           sony_sales_summary.total_pictures_printed%TYPE,
	#media type totals
	# $l_cds               sony_sales_summary.total_compact_disks_used%TYPE,
	# $l_floppies          sony_sales_summary.total_floppy_disks_used%TYPE,
	# $l_pcmcia            sony_sales_summary.total_pcmcia_cards_used%TYPE,
	# $l_compact_flash     sony_sales_summary.total_compact_flash_used%TYPE,
	# $l_smart_media       sony_sales_summary.total_smart_media_used%TYPE,
	# $l_mem_sticks        sony_sales_summary.total_memory_sticks_used%TYPE,
	#output type totals
	# $l_cdws              sony_sales_summary.total_compact_disk_writes_sold%TYPE,
	# $l_snapshots         sony_sales_summary.total_snap_shots_sold%TYPE,
	# $l_4x6               sony_sales_summary.total_four_by_sixes_sold%TYPE,
	# $l_5x7               sony_sales_summary.total_five_by_sevens_sold%TYPE,
	# $l_8x10              sony_sales_summary.total_eight_by_tens_sold%TYPE,
	# $l_wallets           sony_sales_summary.total_wallets_sold%TYPE,
	# $l_indexes           sony_sales_summary.total_indexes_sold%TYPE,
	#cash total
	# $l_subtotal          sony_sales_summary.sales_sub_total%TYPE,
	# $l_tax               sony_sales_summary.sales_tax%TYPE,
	# $l_total             sony_sales_summary.sales_total%TYPE,
	# $l_la                sony_sales_summary.total_local_auth%TYPE,
	# $l_la_declined       sony_sales_summary.total_local_auth_declined%TYPE

	my $l_machine_id       = $command_hashref->{'machine_id'}; # sony_sales_summary.machine_id%TYPE,
	my ($l_trans
		, $l_sales_date
		, $l_active
		, $l_keep_shopping
		, $l_selected
		, $l_printed
		, $l_cds
		, $l_floppies
		, $l_pcmcia
		, $l_compact_flash
		, $l_smart_media
		, $l_mem_sticks
		, $l_cdws
		, $l_snapshots
		, $l_4x6
		, $l_5x7
		, $l_8x10
		, $l_wallets
		, $l_indexes
		, $l_subtotal
		, $l_tax
		, $l_total
		, $l_la
		, $l_la_declined) = split( ',', MakeString($info));

	my $array_ref = $DATABASE->query(
		query => q{
			SELECT sony.seq_sony_sales_summary_id.NEXTVAL
			FROM DUAL 
		}
	);

	my $l_id;
	if(defined $array_ref->[0][0]) {
		$l_id = $array_ref->[0][0];
	} 

	my $count = $DATABASE->do(
		query => qq{
			DELETE FROM sony.sony_sales_summary
			 WHERE machine_id = ?
			   AND TRUNC (sales_ts, 'DD') = TRUNC (to_date(?,'YYYY/MM/DD'), 'DD')
		},
		values => [ $l_machine_id, $l_sales_date ]
	);

	$count = $DATABASE->do(
		query => qq{
			INSERT INTO sony.sony_sales_summary
			(sony_sales_summary_id,
				machine_id,
				sales_ts,
				total_transactions,
				total_memory_sticks_used,
				total_smart_media_used,
				total_compact_flash_used,
				total_pcmcia_cards_used,
				total_compact_disks_used,
				total_floppy_disks_used,
				total_minutes_active,
				total_keep_shopping_selected,
				total_wallets_sold,
				total_eight_by_tens_sold,
				total_five_by_sevens_sold,
				total_four_by_sixes_sold,
				total_snap_shots_sold,
				total_compact_disk_writes_sold,
				total_pictures_selected,
				total_pictures_printed,
				sales_sub_total,
				total_indexes_sold,
				total_local_auth_declined,
				total_local_auth,
				sales_total,
				sales_tax)
			VALUES (?, ?, to_date(?, 'YYYY/MM/DD'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, ?, ?, ? )
		},
		values => [
			$l_id,
			$l_machine_id,
			$l_sales_date,
			$l_trans,
			$l_mem_sticks,
			$l_smart_media,
			$l_compact_flash,
			$l_pcmcia,
			$l_cds,
			$l_floppies,
			$l_active,
			$l_keep_shopping,
			$l_wallets,
			$l_8x10,
			$l_5x7,
			$l_4x6,
			$l_snapshots,
			$l_cdws,
			$l_selected,
			$l_printed,
			$l_subtotal,
			$l_indexes,
			$l_la,
			$l_total,
			$l_tax
		]
	);
	if($count > 0) {
		push @{$logs}, "   Sony Printer : sony_sales_summary insert succesful";
	} else {
		push @{$logs}, "   Sony Printer : sony_sales_summary insert failure";
		my $message = "Sony : unexpected issue processing method insert_sony_sales for Machine ID $l_machine_id (line ".(caller)[2].")";
		ReRix::Utils::send_alert($message,$command_hashref);
	}
}

sub ascii_to_hex ($)
{
	## Convert each ASCII character to a two-digit hex number.
	(my $str = shift) =~ s/(.|\n)/sprintf("%02lx", ord $1)/eg;
	return $str;
}
sub hex_to_ascii ($)
{
	## Convert each two-digit hex number back to an ASCII character.
	(my $str = shift) =~ s/([a-fA-F0-9]{2})/chr(hex $1)/eg;
	return $str;
}

1;
