package ReRix::Utils;

$VERSION = 1.001000;

use strict;
use ReRixConfig qw(%ReRix_Utils %ReRix_KeyManager %ReRix_Functions);
use MIME::Base64;
use Net::SMTP;
use Time::Local;
use Sys::Hostname;
use Storable ();
use USAT::App::ReRix::Const qw(BOOLEAN DB ENGINE_MSG_PARSER_RULES EVENT_STATE);
use USAT::App::KeyManager::SOAP::Gateway;

use constant DEBUG => $ReRixConfig::debug;

use Math::Rand48 qw(nrand48);

sub get_device_data
{
	my ($DATABASE, $cmd) = @_;
	my $array_ref;
	
	if ($cmd->{machine_id} !~ m/^[0-9A-F]{8}(63){4}$/)
	{
		$array_ref = $DATABASE->select(
			table          	=> 'device.device d, device.host h',
			select_columns 	=> 'd.device_id, d.device_serial_cd, d.device_type_id, d.encryption_key, d.device_name, h.host_id, h.host_type_id',
			where_columns  	=> ['d.device_name = ?', 'd.device_active_yn_flag = ?', 'd.device_id = h.device_id (+)', 'h.host_port_num (+) = ?'],
			where_values   	=> [$cmd->{machine_id}, 'Y', 0]
		);
	}
	else
	{
		#Legacy Sony DLL
		$array_ref = $DATABASE->select(
			table          	=> 'device.device d, device.host h',
			select_columns 	=> 'd.device_id, d.device_serial_cd, d.device_type_id, d.encryption_key, d.device_name, h.host_id, h.host_type_id',
			where_columns  	=> ['d.device_serial_cd = ?', 'd.device_active_yn_flag = ?', 'd.device_type_id = ?', 'd.device_id = h.device_id (+)', 'h.host_port_num (+) = ?'],
			where_values   	=> [$cmd->{machine_id}, 'Y', 4, 0]
		);
	}
	
	($cmd->{device_id}, $cmd->{SSN}, $cmd->{device_type}, $cmd->{encryption_key}, $cmd->{machine_id}, $cmd->{base_host_id}, $cmd->{base_host_type_id}) = ($array_ref->[0] ? @{$array_ref->[0]} : (undef, undef, undef, undef, $cmd->{machine_id}, undef, undef));
}

sub get_message_number
{
	my ($DATABASE) = @_;
	my $message_no = $DATABASE->sequence_next(sequence => 'engine.rerix_message_num_seq');
	return $message_no;
}

sub blob_select
{
	my ($DATABASE, $sqlStmt) = @_;
	my $db = $DATABASE->{handle};	

	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB

	my $stmt = $db->prepare(q{$sqlStmt});

	$stmt->execute();
	
	if(defined $db->errstr)
	{
		print "blob_select error: " . $db->errstr . "\n";
		return undef;
	}

	my $row = 0;
	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}

sub blob_insert
{
	my ($DATABASE, $sqlStmt, $buf) = @_;
	my $db = $DATABASE->{handle};	
	my $LONG_RAW_TYPE=24;
	my $stmt = $db->prepare(q{$sqlStmt});
	my %attrib;
	$attrib{'ora_type'} = $LONG_RAW_TYPE;
	$stmt->bind_param(":blob", $buf, \%attrib);  
	$stmt->execute();
	if(defined $db->errstr)
	{
		print "blob_insert error: " . $db->errstr . "\n";
	}
}

sub get_random_int
{
	my ($min,$max,$in_seed) = @_;
	my $len = 2;
	my $seed;
	my $key;
	my $counter = 1;
	
	while($key < $min || $key > $max)
	{
		while(length($key) < length($max))
		{
			$seed = $counter . time() . $in_seed;
			$key = $key . nrand48($seed);
			$counter++;
		}
		
		$key = substr($key, 0, $len);
	}
		
	return $key;
}

sub mask_sensitive_inbound_message ($$$) {
	my ($inbound_message, $command_code, $command_subcode) = @_;
	my $value = $inbound_message;
	if (defined ENGINE_MSG_PARSER_RULES->{$command_code}
		&& defined ENGINE_MSG_PARSER_RULES->{$command_code}->{$command_subcode}
		&& defined ENGINE_MSG_PARSER_RULES->{$command_code}->{$command_subcode}->[4])
	{
		my $cmd_content_security_filter_sub = ENGINE_MSG_PARSER_RULES->{$command_code}->{$command_subcode}->[4];
		eval { $value = $cmd_content_security_filter_sub->($value) }
			if !DEBUG && defined $cmd_content_security_filter_sub && ref($cmd_content_security_filter_sub) eq 'CODE';
	}
	return $value;
}

sub send_alert
{
	my ($content, $command_hashref) = @_;
	
	return 0 if length($ReRix_Functions{NO_ALERT_REGEX}) > 0 && $content =~ m/$ReRix_Functions{NO_ALERT_REGEX}/;
	
	my $data = "$content\n\n";
	my ($key,$value);
	if(defined $command_hashref)
	{
		my ($command_code, $command_subcode) = ($command_hashref->{_command_code}, $command_hashref->{_command_subcode});
		while(($key,$value) = each(%$command_hashref))
		{
			$value = mask_sensitive_inbound_message($value, $command_code, $command_subcode) if $key eq 'inbound_command';
			$data = $data . "$key => $value\n";
		}
	}
	
	return &send_email($ReRix_Utils{alert_email_addr_from}, $ReRix_Utils{alert_email_addr_to}, 'Rerix Engine Issue ('.hostname().')', $data) if $ReRix_Utils{alert_email_enabled};
}

sub send_email
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = $ReRix_Utils{alert_email_smtp_server};
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}

sub send_email_detailed
{
	my ($from_addr, $from_name, $to_addrs, $to_names, $subject, $content) = @_;
	my @to_addr_array = split(/,/, $to_addrs);
	my @to_name_array = split(/,/, $to_names);
	
	# check that every address has a name
	if($#to_addr_array ne $#to_name_array)
	{
		warn "send_email_detailed: Length of address array not equald to length of name array!\n";
		return 0;
	}

	my $smtp_server = $ReRix_Utils{alert_email_smtp_server};
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_addr_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 

	my $to_line;
	for(my $index = 0; $index <= $#to_addr_array; $index++)
	{
		$to_line = $to_line . "\"$to_name_array[$index]\" <$to_addr_array[$index]>";
		if($index < $#to_addr_array)
		{
			$to_line = $to_line . ", ";
		}
	}
	
	$smtp->datasend("To: $to_line\n");
	$smtp->datasend("From: $from_name <$from_addr>\n");
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("Content-Type: text/plain\n");
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();
	
	return 1;
}

sub getAdjustedTimestamp
{
#----------------------------------------------------------------------------------------------------------
# 	Function: 		: getAdjustedTimestamp ($timezone, $timestamp)
# 	Description 		: This function returns an adjusted 32-bit timestamp value based upon GMT
#	Params:
#		$timezone 	: The timezone to adjust **TO**. If one is not provided
#				  EST will be the default
#		$timestamp	: The timestamp to adjust. If one is not provided then localtime
#				  EST will be used as the default
#	Returns:
#		The adjusted 32-bit timestamp
#
#	Change History		:
#		Version		Developer	Date		Description
#		1.0		T Shannon	10/15/03	first rev
#				
#----------------------------------------------------------------------------------------------------------

	my ($timezone, $timestamp) = @_;
	
	# we need to know if its daylight savings time; if so move the timestamp 'forward' an additional hour
	my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
	# for testing normal time during DST ONLY!!
	#$isdst = 0;

	# if a timestamp is not provided grab the system time (which is in GMT)
	if ((!defined $timestamp) or ($timestamp == 0))
	{
		$timestamp = time();
		print "Utils::getAdjustedTimestamp() - timestamp not provided. Self-generated timestamp = $timestamp (" . localtime($timestamp) . ")\n";
	}
	
	SWITCH: {
		if ($timezone eq 'CST') {$timestamp -= (3600 * (6 - $isdst)); last SWITCH;}	
		if ($timezone eq 'MT') {$timestamp -= (3600 * (7 - $isdst)); last SWITCH;}	
		if ($timezone eq 'MST') {$timestamp -= (3600 * (7 - $isdst)); last SWITCH;}	
		if ($timezone eq 'PT') {$timestamp -= (3600 * (8 - $isdst)); last SWITCH;}
		if ($timezone eq 'PST') {$timestamp -= (3600 * (8 - $isdst)); last SWITCH;}
		# if the time zone is not identified (or IS set to "EST") adjust the time back 
		# only 5 hours (normal time)to adjust to EST
		$timestamp -= (3600 * (5 - $isdst));
		}
	return $timestamp;
}

sub getAdjustedTimestampReverse
{
#----------------------------------------------------------------------------------------------------------
# 	Function: 		: getAdjustedTimestampReverse ($timezone, $timestamp)
# 	Description 		: This function returns an adjusted 32-bit timestamp value that reverses the
#				: the value created by getAdjustedTimestamp()
#	Params:
#		$timezone 	: The timezone to adjust **TO**. If one is not provided
#				  EST will be the default
#		$timestamp	: The timestamp to adjust. If one is not provided then localtime
#				  EST will be used as the default
#	Returns:
#		The adjusted 32-bit timestamp
#
#	Change History		:
#		Version		Developer	Date		Description
#		1.0		T Shannon	11/19/03	first rev
#				
#----------------------------------------------------------------------------------------------------------

	my ($timezone, $timestamp) = @_;
	
	# we need to know if its daylight savings time; if so move the timestamp 'forward' an additional hour
	my (undef, undef, undef, undef, undef, undef, undef, undef, $isdst) = localtime;
	# for testing normal time during DST ONLY!!
	#$isdst = 0;

	# if a timestamp is not provided grab the system time (which is in GMT)
	if ((!defined $timestamp) or ($timestamp == 0))
	{
		$timestamp = time();
		print "Utils::getAdjustedTimestamp() - timestamp not provided. Self-generated timestamp = $timestamp (" . localtime($timestamp) . ")\n";
	}
	
	SWITCH: {
		if ($timezone eq 'CST') {$timestamp += (3600 * (6 + $isdst)); last SWITCH;}	
		if ($timezone eq 'MT') {$timestamp += (3600 * (7 + $isdst)); last SWITCH;}	
		if ($timezone eq 'MST') {$timestamp += (3600 * (7 + $isdst)); last SWITCH;}	
		if ($timezone eq 'PT') {$timestamp += (3600 * (8 + $isdst)); last SWITCH;}
		if ($timezone eq 'PST') {$timestamp += (3600 * (8 + $isdst)); last SWITCH;}
		# if the time zone is not identified (or IS set to "EST") adjust the time back 
		# only 5 hours (normal time)to adjust to EST
		$timestamp += (3600 * (5 + $isdst));
		}
	return $timestamp;
}
sub convert_bcd_time
{
	my ($bcd_str) = @_;
	my ($bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year) = unpack("H2H2H2H2H2H4", $bcd_str);
	return(Time::Local::timegm($bcd_sec, $bcd_min, $bcd_hour, $bcd_mday, ($bcd_mon - 1), $bcd_year));
}

sub gen_random_key
{
	my ($SSN) = @_;
	my $len = 16;
	my $seed;
	my $key;
	my $counter = 1;
	
	while(length($key) < $len)
	{
		$seed = $counter . time() . $SSN;
		$key = $key . nrand48($seed);
		$counter++;
	}
	
	$key = substr($key, 0, $len);
		
	return $key;
}

sub secure_data_log ($)
{
	my $data = shift;
	my ($min_num_length, $max_seq_shown) = (8, 4);	#min num of chars that requires security; max num of chars in any sequence shown
	my $data_out = '';
	if (length $data > 0)
	{
		$data_out = substr($data, 0, length $data > $min_num_length?4:length $data).(length $data > $min_num_length?(length $data > (4 + 1)?'...':'').substr($data, (length $data) - int((length $data) / 4)):'');
	}
	return $data_out.' ('.(length $data).' char)';
}

sub store_setting
{
	my ($device_id, $name, $value, $DATABASE) = @_;
	
	my $check_setting_ref = $DATABASE->select(
							table			=> 'device_setting',
							select_columns	=> 'count(1)',
							where_columns	=> ['device_id = ?', 'device_setting_parameter_cd = ?'],
							where_values	=> [$device_id, $name] );

	if($check_setting_ref->[0][0] == 0)
	{
		$DATABASE->insert(	table=> 'device_setting',
							insert_columns=> 'device_id, device_setting_parameter_cd, device_setting_value',
							insert_values=> [$device_id, $name, $value] );
	}
	else
	{
		$DATABASE->update(	table			=> 'device_setting',
							update_columns	=> 'device_setting_value',
							update_values	=> [$value],
							where_columns	=> ['device_id = ?', 'device_setting_parameter_cd = ?'],
							where_values	=> [$device_id, $name] );
	}
}

sub parse_multitech_modem_info
{
	my ($data)  = @_;
	my @lines = split(/\r\n/, $data);
	my @cleanLines;

	foreach my $line (@lines)
	{
		if($line)
		{
			$line =~ s/^(\+|\s)+//;
			$line =~ s/(\s|\:)+$//;
			if($line)
			{
				push(@cleanLines, $line);
			}
		}
	}

	my ($csq, $cgsn, $cimi, $cgmr, $cgmm, $cgmi, $cnum, $ccid);

	for(my $i=0; $i<scalar(@cleanLines); $i++)
	{
		if($cleanLines[$i] =~ m/^CSQ$/)
		{
			$csq = $cleanLines[$i+1];
			$csq =~ s/(CSQ|:| |)//g;
			$i++;
		}
		elsif($cleanLines[$i] =~ m/^CGSN$/)
		{
			$cgsn = $cleanLines[$i+1];
			$i++;
		}
		elsif($cleanLines[$i] =~ m/^CIMI$/)
		{
			$cimi = $cleanLines[$i+1];
			$i++;
		}
		elsif($cleanLines[$i] =~ m/^CGMR$/)
		{
			$cgmr = $cleanLines[$i+1];
			$i++;
		}
		elsif($cleanLines[$i] =~ m/^CGMM$/)
		{
			$cgmm = $cleanLines[$i+1];
			$i++;
		}
		elsif($cleanLines[$i] =~ m/^CGMI/)
		{
			$cgmi = $cleanLines[$i+1];
			$i++;
		}
		elsif($cleanLines[$i] =~ m/^CNUM/)
		{
			$cnum = $cleanLines[$i+1];
			$cnum =~ s/(CNUM|:| |\")//g;
			$i++;
		}
		elsif($cleanLines[$i] =~ m/^CCID/)
		{
			$ccid = $cleanLines[$i+1];
			$ccid =~ s/(CCID|:| |\")//g;
			$i++;
		}
	}
	
	return ($csq, $cgsn, $cimi, $cgmr, $cgmm, $cgmi, $cnum, $ccid);
}

sub parse_multitech_modem_info_new
{
	my ($data)  = @_;
	my @lines = split(/\r|\n/, $data);

	my ($csq, $cgsn, $cimi, $cgmr, $cgmm, $cgmi, $cnum, $ccid);
	my $line;

	foreach $line (@lines)
	{
		my ($id,$val) = split(/:/, $line);
		
		if($id eq 'CSQ')
		{
			$csq = $val;
		}
		elsif($id eq 'CGSN')
		{
			$cgsn = $val;
		}
		elsif($id eq 'CIMI')
		{
			$cimi = $val;
		}
		elsif($id eq 'CGMR')
		{
			$cgmr = $val;
		}
		elsif($id eq 'CGMM')
		{
			$cgmm = $val;
		}
		elsif($id eq 'CGMI')
		{
			$cgmi = $val;
		}
		elsif($id eq 'CNUM')
		{
			$cnum = $val;
		}
		elsif($id eq 'CCID')
		{
			$ccid = $val;
		}
	}
	
	return ($csq, $cgsn, $cimi, $cgmr, $cgmm, $cgmi, $cnum, $ccid);
}

sub parse_bootloader_diag_rev
{
	my ($data)  = @_;
	my ($bootloader, $diag) = split(/\r|\n/, $data);
	
	$bootloader =~ s/\s+$//;
	$bootloader =~ s/^\s+//;
	
	$diag =~ s/\s+$//;
	$diag =~ s/^\s+//;
	
	return ($bootloader, $diag);
}

sub enqueue_outbound_command($$)
{
	my ($command_hashref, $response) = @_;
	my %hash = map { $_ => (ref($command_hashref->{$_}) ? undef : $command_hashref->{$_}) } keys %{$command_hashref};	#only pass scalar values to outbound queue handler
	my $val = Storable::freeze([undef, \%hash, $response]);
	push @{$command_hashref->{queue_ob}}, Storable::freeze([undef, \%hash, $response]);
	return 1;
}

sub bin2dec 
{
    return unpack("N", pack("B32", substr("0" x 32 . shift, -32)));
}

sub mask_response
{
	my ($response, $num_chars) = @_;
	return substr($response, 0, $num_chars) . "..." . (length($response) - $num_chars) . " chars"; # mask sensitive data
}

sub get_global_trans_cd($$)
{
	my ($device_name, $device_tran_cd) = @_;
	return "X:$device_name:$device_tran_cd";
}

sub create_event($$$$$$$$)
{
	my ($DATABASE, $logs, $cmd, $event_type_host_type_num, $event_state_id, $event_device_tran_cd, $event_end_flag, $event_upload_complete_flag) = @_;
	my $base_host_id = $cmd->{base_host_id};
	my $base_host_type_id = $cmd->{base_host_type_id};
	my ($event_id, $event_type_id, $event_global_trans_cd, $db_event_state_id, $db_event_end_ts, $db_event_upload_complete_ts);
	
	my $data = $DATABASE->query(
		query	=> q{
			SELECT event_type_id
			FROM device.event_type_host_type
			WHERE host_type_id = ?
				AND event_type_host_type_num = ?
		},
		values	=> [$base_host_type_id, $event_type_host_type_num]
	);
	
	if(defined $data->[0])
	{
		$event_type_id = $data->[0]->[0];
	}
	else
	{	
		push @{$logs}, "Unable to create event. No event_type_id exists for host_type_id: $base_host_type_id, event_type_host_type_num: $event_type_host_type_num in device.event_type_host_type.";
		return undef;
	}
	
	if (defined $event_device_tran_cd)
	{
		$event_global_trans_cd = get_global_trans_cd($cmd->{machine_id}, $event_device_tran_cd);
		
		my $data = $DATABASE->query(
			query	=> q{
				SELECT event_id, event_state_id, event_end_ts, event_upload_complete_ts
				FROM device.event
				WHERE event_global_trans_cd = ?
			},
			values	=> [$event_global_trans_cd]
		);
		
		if(defined $data->[0])
		{
			($event_id, $db_event_state_id, $db_event_end_ts, $db_event_upload_complete_ts) = @{$data->[0]};
			push @{$logs}, "Event $event_id already exists for $event_global_trans_cd in device.event";
			
			my $update_event_flag = BOOLEAN->{FALSE};
			my $update_event_state_flag = BOOLEAN->{FALSE};
			
			if ($db_event_state_id != $event_state_id
				&& $db_event_state_id != EVENT_STATE->{COMPLETE_FINAL}
				&& $db_event_state_id != EVENT_STATE->{COMPLETE_ERROR})
			{
				$update_event_flag = BOOLEAN->{TRUE};
				$update_event_state_flag = BOOLEAN->{TRUE};
			}
			elsif ($event_end_flag && !defined $db_event_end_ts)
			{
				$update_event_flag = BOOLEAN->{TRUE};
			}
			elsif ($event_upload_complete_flag && !defined $db_event_upload_complete_ts)
			{
				$update_event_flag = BOOLEAN->{TRUE};
			}
			
			if ($update_event_flag)
			{
				$DATABASE->do(
					query => q{
						UPDATE device.event
						SET event_state_id = CASE ? WHEN ? THEN TO_NUMBER(?) ELSE event_state_id END,
							event_end_ts = CASE ? WHEN ? THEN SYSDATE ELSE event_end_ts END,
							event_upload_complete_ts = CASE ? WHEN ? THEN SYSDATE ELSE event_upload_complete_ts END
						WHERE event_id = ?
					}
					,values => [
						$update_event_state_flag, BOOLEAN->{TRUE}, $event_state_id,
						$event_end_flag, BOOLEAN->{TRUE},
						$event_upload_complete_flag, BOOLEAN->{TRUE},
						$event_id
					]
				);			
			}
			
			return $event_id;
		}		
	}
	
	push @{$logs}, "Creating new device.event record for host: $base_host_id, event_type_id: $event_type_id...";
	$event_id = $DATABASE->sequence_next(sequence => 'device.seq_event_id');
	
	my $result = $DATABASE->do(
		query	=> q{
			INSERT INTO device.event (
				event_id,
				event_type_id,
				event_state_id,
				event_device_tran_cd,
				event_global_trans_cd,
				host_id,
				event_start_ts,
				event_end_ts,
				event_upload_complete_ts				
			) VALUES (
				?, ?, ?, ?, ?, ?, SYSDATE, 
				CASE ? WHEN ? THEN SYSDATE ELSE NULL END,
				CASE ? WHEN ? THEN SYSDATE ELSE NULL END
			)
		}
		,values => [
			$event_id,
			$event_type_id,
			$event_state_id,
			$event_device_tran_cd,
			$event_global_trans_cd,
			$base_host_id,
			$event_end_flag,
			BOOLEAN->{TRUE},
			$event_upload_complete_flag,
			BOOLEAN->{TRUE}
		]
	);
	
	$DATABASE->do(
		query	=> q{INSERT INTO device.event_nl_device_session (event_id, session_id) VALUES (?, ?)}
		,values => [$event_id, $cmd->{session_id}]
	);
	
	return $event_id;
}

sub create_event_detail($$$$$$)
{
	my ($DATABASE, $logs, $event_id, $event_detail_type_id, $event_detail_value, $event_detail_value_ts) = @_;
	return if !defined $event_id;
	
	my $data = $DATABASE->query(
		query	=> q{
			SELECT event_detail_id
			FROM device.event_detail
			WHERE event_id = ?
				AND event_detail_type_id = ?
		},
		values	=> [
			$event_id,
			$event_detail_type_id
		]
	);
	
	if(defined $data->[0])
	{
		push @{$logs}, "Event detail " . $data->[0]->[0] . " already exists for event_id: $event_id, event_detail_type_id: $event_detail_type_id in device.event_detail";
		return;
	}
	
	push @{$logs}, "Creating new device.event_detail record for event_id: $event_id, event_detail_type_id: $event_detail_type_id...";
	
	$DATABASE->do(
		query	=> q{
			INSERT INTO device.event_detail (
				event_id,
				event_detail_type_id,
				event_detail_value,
				event_detail_value_ts
			) VALUES (
				?, ?, ?, TO_DATE(?, ?)
			)
		}
		,values => [
			$event_id,
			$event_detail_type_id,
			$event_detail_value,
			$event_detail_value_ts,
			DB->{DEFAULT_DATE_FORMAT}
		]
	);
}

sub DUKPT_decrypt($$$$)
{
	my ($logs, $KSN, $encrypted, $decrypted_length) = @_;
	
	push @{$logs}, "Sending KeyManager DUKPTDecrypt request, KSN: " . unpack('H*', $KSN) . ", encrypted: " . length($encrypted) . " bytes, decrypted length: $decrypted_length...";
	
	my $response = USAT::App::KeyManager::SOAP::Gateway
		->soapversion(1.1)
		->proxy(
			$ReRix_KeyManager{KEYSERVER_PROXY},
			$ReRix_KeyManager{KEYSERVER_PROXY_TIMEOUT})
		->DUKPTDecrypt(
			"2TDES",
			$ReRix_KeyManager{DEFAULT_DUKPT_KEY_ID},
			encode_base64($KSN),
			encode_base64($encrypted));

	my $decrypted = undef;
	if (defined $response) {
		if ($response->fault || ($response->result_object
				&& eval { $response->result_object->isa('USAT::App::KeyManager::SOAP::ComplexType::CryptoException') })) {
			my $error = "KeyManager Fault detected";
			$error .= ": " . $response->faultstring if ($response->faultstring);
			push @{$logs}, $error;
		} elsif ($response->result_object) {	#error
			push @{$logs}, "KeyManager unexpected response";
		} else {
			# process non-object result
			$decrypted = $response->result;
			if ($decrypted_length > 0) {
				$decrypted = substr($decrypted, 0, $decrypted_length);
			} else {
				# remove padding
				$decrypted =~ s/\x00+$//;
			}
			push @{$logs}, "Received KeyManager DUKPTDecrypt response, decrypted: " . length($decrypted) . " bytes";
		}
	}
	else {
		push @{$logs}, "KeyManager request failed: Timeout occured";
	}
	
	return $decrypted;
}

1;
