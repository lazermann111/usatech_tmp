#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		07/30/2003	pcowan		Initial creation
# 1.01		09/20/2004	pcowan		Added support for MEI
#

package ReRix::Gx;

use strict;
use ReRix::Utils;
use ReRix::G4Export;
use USAT::App::ReRix::Const qw(BOOLEAN EVENT_DETAIL_TYPE EVENT_STATE EVENT_TYPE_HOST_NUM);

sub counters
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no}; 
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $response_no = $command_hashref->{response_no}; 
	my $session_id = $command_hashref->{session_id};
	
	if(not defined $device_id)
	{
		push @logs, "No device record found for $machine_id";
		return(\@logs);	
	}
	
	my $currency_transaction_counter = &pbcd2num(substr($message, 1, 4));
	my $currency_money_counter       = &pbcd2num(substr($message, 5, 4));
	
	my $cashless_transaction_counter = &pbcd2num(substr($message, 9, 4));
	my $cashless_money_counter       = &pbcd2num(substr($message, 13, 4));
	
	my $passcard_transaction_counter = &pbcd2num(substr($message, 17, 4));
	my $passcard_money_counter       = &pbcd2num(substr($message, 21, 4));

	my $total_bytes = &pbcd2num(substr($message, 25, 4));
	my $total_sessions_attempted = &pbcd2num(substr($message, 29, 4));

	#my $reserved = &pbcd2num(substr($message, 33, 4));
	
	push @logs, "Currency Transactions     : $currency_transaction_counter";
	push @logs, "Currency Money            : $currency_money_counter";
	push @logs, "Cashless Transactions     : $cashless_transaction_counter";
	push @logs, "Cashless Money            : $cashless_money_counter";
	push @logs, "Passcard Transactions     : $passcard_transaction_counter";
	push @logs, "Passcard Money            : $passcard_money_counter";
	push @logs, "Total Bytes               : $total_bytes";
	push @logs, "Total Sessions Attempted  : $total_sessions_attempted";
	
	my $event_id = ReRix::Utils::create_event($DATABASE, \@logs, $command_hashref, EVENT_TYPE_HOST_NUM->{BATCH}, EVENT_STATE->{COMPLETE_FINAL}, undef, BOOLEAN->{TRUE}, BOOLEAN->{TRUE});
	
	&insert_counter($DATABASE, \@logs, $command_hashref, "CURRENCY_TRANSACTION", $currency_transaction_counter, $event_id, 1);
	&insert_counter($DATABASE, \@logs, $command_hashref, "CURRENCY_MONEY", $currency_money_counter, $event_id, 1);
	&insert_counter($DATABASE, \@logs, $command_hashref, "CASHLESS_TRANSACTION", $cashless_transaction_counter, $event_id, 1);
	&insert_counter($DATABASE, \@logs, $command_hashref, "CASHLESS_MONEY", $cashless_money_counter, $event_id, 1);
	&insert_counter($DATABASE, \@logs, $command_hashref, "PASSCARD_TRANSACTION", $passcard_transaction_counter, $event_id, 1);
	&insert_counter($DATABASE, \@logs, $command_hashref, "PASSCARD_MONEY", $passcard_money_counter, $event_id, 1);
	&insert_counter($DATABASE, \@logs, $command_hashref, "TOTAL_BYTES", $total_bytes, $event_id, 1);
	&insert_counter($DATABASE, \@logs, $command_hashref, "TOTAL_SESSIONS", $total_sessions_attempted, $event_id, 1);
	
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	
	return(\@logs);	
}

sub counters_v2
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no}; 
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $response_no = $command_hashref->{response_no}; 
	my $session_id = $command_hashref->{session_id};
	
	if(not defined $device_id)
	{
		push @logs, "No device record found for $machine_id";
		return(\@logs);	
	}
	
	my $currency_transaction_counter = &pbcd2num(substr($message, 1, 4));
	my $currency_money_counter       = &pbcd2num(substr($message, 5, 4));
	
	my $cashless_transaction_counter = &pbcd2num(substr($message, 9, 4));
	my $cashless_money_counter       = &pbcd2num(substr($message, 13, 4));
	
	my $passcard_transaction_counter = &pbcd2num(substr($message, 17, 4));
	my $passcard_money_counter       = &pbcd2num(substr($message, 21, 4));

	my $total_bytes = &pbcd2num(substr($message, 25, 4));
	my $total_sessions_attempted = &pbcd2num(substr($message, 29, 4));

	push @logs, "Currency Transactions     : $currency_transaction_counter";
	push @logs, "Currency Money            : $currency_money_counter";
	push @logs, "Cashless Transactions     : $cashless_transaction_counter";
	push @logs, "Cashless Money            : $cashless_money_counter";
	push @logs, "Passcard Transactions     : $passcard_transaction_counter";
	push @logs, "Passcard Money            : $passcard_money_counter";
	push @logs, "Total Bytes               : $total_bytes";
	push @logs, "Total Sessions Attempted  : $total_sessions_attempted";
	
	my ($trans_id, $timestamp, $reason_code, $maglength, $magstripe) = unpack("NNaCa*", substr($message, 33));
	my $machine_trans_no = ReRix::Utils::get_global_trans_cd($machine_id, $trans_id);
	
	my ($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);
	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	my $timestr = sprintf("%02d:%02d:%02d", $hour, $min, $sec);
	my $datetime = "$date $timestr";

	push @logs, "Machine ID        : $machine_id";
	push @logs, "Timestamp         : $timestamp";
	push @logs, "Date/Time         : $datetime";
	push @logs, "Reason Code       : $reason_code";
	push @logs, "Trans ID          : $trans_id";
	push @logs, "Machine Trans ID  : $machine_trans_no";
	
	my $event_id = ReRix::Utils::create_event($DATABASE, \@logs, $command_hashref, ord($reason_code), EVENT_STATE->{COMPLETE_FINAL}, $trans_id, BOOLEAN->{TRUE}, BOOLEAN->{TRUE});
	ReRix::Utils::create_event_detail($DATABASE, \@logs, $event_id, EVENT_DETAIL_TYPE->{EVENT_TYPE_VERSION}, 2, undef);
	ReRix::Utils::create_event_detail($DATABASE, \@logs, $event_id, EVENT_DETAIL_TYPE->{HOST_EVENT_TIMESTAMP}, undef, $datetime);
	
	&insert_counter($DATABASE, \@logs, $command_hashref, "CURRENCY_TRANSACTION", $currency_transaction_counter, $event_id, 2);
	&insert_counter($DATABASE, \@logs, $command_hashref, "CURRENCY_MONEY", $currency_money_counter, $event_id, 2);
	&insert_counter($DATABASE, \@logs, $command_hashref, "CASHLESS_TRANSACTION", $cashless_transaction_counter, $event_id, 2);
	&insert_counter($DATABASE, \@logs, $command_hashref, "CASHLESS_MONEY", $cashless_money_counter, $event_id, 2);
	&insert_counter($DATABASE, \@logs, $command_hashref, "PASSCARD_TRANSACTION", $passcard_transaction_counter, $event_id, 2);
	&insert_counter($DATABASE, \@logs, $command_hashref, "PASSCARD_MONEY", $passcard_money_counter, $event_id, 2);
	&insert_counter($DATABASE, \@logs, $command_hashref, "TOTAL_BYTES", $total_bytes, $event_id, 2);
	&insert_counter($DATABASE, \@logs, $command_hashref, "TOTAL_SESSIONS", $total_sessions_attempted, $event_id, 2);
	
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	
	return(\@logs);	
}

sub make_poke
{
	my (@logs);
	my ($DATABASE, $command_hashref, $response_no, $pending_id, $memory_code, $memory_location, $poke_size, $device_type) = @_;

	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id};
	
	# use blobSelect because the column is a LONG and it won't come back otherwise
	my $raw_handle = $DATABASE->{handle};
	my $poke_data = &blob_select_config($raw_handle, $machine_id);
	
	if(not defined $poke_data)
	{
		ReRix::Utils::send_alert("Config                : Config not found for $machine_id!", $command_hashref);
		push @logs, "Config            : Config not found for $machine_id!" ;
		return(\@logs);
	}

	# Data in file_transfer_content coulumn is stored HEX encoded
	$poke_data = pack("H*", $poke_data);
	
	my $total_bytes = length($poke_data);
	
	my $pos = $memory_location;
	
	if($device_type =~ /^(0|1)$/)
	{
		# G4/G5 use 2 byte memory addressing
		$pos = ($memory_location*2);
	}
	
	push @logs, "Memory Location   : $memory_location";
	push @logs, "File Location     : $pos";
	
	my $chunk;
	
	if(($pos+$poke_size) > $total_bytes)
	{
		# this should be the last Poke
		$chunk = substr($poke_data, $pos);
	}
	else
	{
		$chunk = substr($poke_data, $pos, $poke_size);
	}
		
	my $response = pack('CCCNa*', $response_no, 0x88, ord("B"), $memory_location, $chunk);
	push @logs, "Response          : " . unpack("H*", $response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
						
	return(\@logs);	
}

sub insert_counter
{
	my ($DATABASE, $logs, $cmd, $name, $value, $event_id, $counter_version) = @_;
	
	if ($counter_version > 1)
	{
		my $data = $DATABASE->query(
			query	=> q{
				SELECT hc.host_counter_id
				FROM device.host_counter_event hce
				JOIN device.host_counter hc
					ON hce.host_counter_id = hc.host_counter_id
				WHERE hce.event_id = ?
					AND hc.host_counter_parameter = ?
			},
			values	=> [
				$event_id,
				$name
			]
		);

		if(defined $data->[0])
		{
			push @{$logs}, "Host counter " . $data->[0]->[0] . " already exists for event_id: $event_id, host_counter_parameter: $name in device.host_counter_event";
			return;
		}
	}
	
	my $host_counter_id = $DATABASE->sequence_next(sequence => 'device.seq_host_counter_id');
	
	$DATABASE->insert(	table => 'device.host_counter',
						insert_columns => 'host_counter_id, host_id, host_counter_parameter, host_counter_value',
						insert_values => [$host_counter_id, $cmd->{base_host_id}, $name, $value] );
						
	$DATABASE->insert(	table => 'device.host_counter_event',
						insert_columns => 'event_id, host_counter_id',
						insert_values => [$event_id, $host_counter_id] );
}

sub pbcd2num
{
	my $pbcd = shift;
	
	if(not $pbcd)
	{
		return 0;
	}
	
	my $num = 0;
	foreach my $digit (unpack("C4",$pbcd))
	{
		my ($lsn,$msn) = ($digit % 16,int $digit / 16);
		$num = ($num * 10 + $msn) * 10 + $lsn;
	};
	return $num;
};

sub num2pbcd
{
	my ($num,$pair) = (shift,undef);
	my @digits = ();
	foreach my $i (0..3)
	{
		($num,$pair) = (int $num/100,$num % 100);
		my ($lsn,$msn) = ($pair % 10,int $pair/10);
		unshift(@digits,$msn * 16 + $lsn);
	};
	return pack('C4',@digits);
}; 

sub blob_select_config
{
	my ($db, $machine_id) = @_;
	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB
	my $stmt = $db->prepare(q{select file_transfer_content from file_transfer where file_transfer_name = :file_name });
	$stmt->bind_param(":file_name", "$machine_id-CFG");

	$stmt->execute();

	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}

1;
