#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		08/04/2003	pcowan		Created
# 1.01		10/06/2004	pcowan		Added eSuds code 122
# 1.02		10/01/2005	erybski		Updated to support logging to database instead of flat files
#

package ReRix::DataLog;

use strict;
use ReRixConfig qw(%ReRix_DataLog);
use Config;
use Math::BigInt;
use ReRix::Utils;

#my $log_root = $ReRix_DataLog{log_root_path};

my %esuds_log_types = 
(
	1	=> 'LOG_STARTING_UP',
	2 	=> 'LOG_ENTERING_MAINTENANCE',
	3 	=> 'LOG_LEAVING_MAINTENANCE',
	4 	=> 'LOG_RECEIVED_MANUAL_ACTIVATION',
	5 	=> 'LOG_RECEIVED_NETWORK_ACTIVATION',
	6 	=> 'LOG_RECEIVED_CONFIG_FILE',
	7 	=> 'LOG_RECEIVED_APP_UPGRADE',
	8 	=> 'LOG_SYSTEM_TIME_CHANGED',
	9 	=> 'LOG_RECEIVED_UNKNOWN_REMOTE_COMMAND',
	10 	=> 'LOG_SHUTDOWN_NORMAL',
	11 	=> 'LOG_SHUTDOWN_REBOOT',
	12 	=> 'LOG_SHUTDOWN_POWER_DOWN',
	13 	=> 'LOG_SHUTDOWN_RESTART', 
	14	=> 'LOG_SHUTDOWN_EXTERNAL',
	15	=> 'LOG_SHUTDOWN_RESTORE_ORIG_CONFIG',
	16	=> 'LOG_SHUTDOWN_INTERNAL_FLASH_UPDATE',
	19 	=> 'LOG_SHUTDOWN_UNKNOWN',
	20 	=> 'LOG_MANUAL_CONFIG_CHANGE',
	21 	=> 'LOG_UNKNOWN_PROGRAM_ERROR',
	22 	=> 'LOG_CONSECUTIVE_AUTH_FAILURES',
	23 	=> 'LOG_DECRYPTION_FAILURE',
	24 	=> 'LOG_ENCRYPTION_FAILURE',
	25	=> 'LOG_LOCAL_AUTH_TABLE_CLEARED',
	26	=> 'LOG_CONSECUTIVE_AUTH_DECLINES',
	99 	=> 'LOG_FLASH_WRITE_FAILURE',
	100 => 'LOG_DEAD_THREAD_KEYPAD',
	101 => 'LOG_DEAD_THREAD_UI', 
	102 => 'LOG_DEAD_THREAD_MULTIPLEXOR',
	103 => 'LOG_DEAD_THREAD_NETWORK',
	104 => 'LOG_DEAD_THREAD_MAIN',
	105 => 'LOG_DEAD_THREAD_MONITOR',
	106 => 'LOG_DEAD_THREAD_SCHEDULED_TASKS',
	107 => 'LOG_DEAD_THREAD_CCR',
	120 => 'LOG_EQUIP_CHANGE',
	121 => 'LOG_MULTIPLEXOR_COMM_LOST',
	122 => 'LOG_MULTIPLEXOR_ESCROW_NO_RESPONSE',
	123	=> 'LOG_MULTIPLEXOR_ESCROW_NAK',
	124	=> 'LOG_MULTIPLEXOR_FAILED_TO_START_APP',
	125	=> 'LOG_MULTIPLEXOR_FAILED_TO_ENTER_BOOTLOADER',
	126	=> 'LOG_RECEIVED_MULTIPLEXOR_BOOTLOADER_UPGRADE',
	127	=> 'LOG_RECEIVED_MULTIPLEXOR_APPLICATION_UPGRADE',
	128	=> 'LOG_MULTIPLEXOR_BOOTLOADER_UPGRADE_SUCCESSFUL',
	129	=> 'LOG_MULTIPLEXOR_BOOTLOADER_UPGRADE_FAILED',
	130	=> 'LOG_MULTIPLEXOR_APPLICATION_UPGRADE_SUCCESSFUL',
	131	=> 'LOG_MULTIPLEXOR_APPLICATION_UPGRADE_FAILED',
	132	=> 'LOG_MULTIPLEXOR_COMM_NOT_ESTABLISHED',
	133	=> 'LOG_FLASH_INTERNAL_UPDATE_SUCCESSFUL',
	134	=> 'LOG_FLASH_INTERNAL_UPDATE_ERR'
);

sub parse
{
	my ($DATABASE, $command_hashref) = @_;
	my @logs;
	my $array_ref;
	
	my $msg_no = $command_hashref->{msg_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $response_no = $command_hashref->{response_no};
	my $device_type_code = $command_hashref->{device_type};
	
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = localtime();

	$Month = $Month+1;
    $Year  = $Year+1900;
    
    if(length($Day) < 2)
    {
    	$Day = "0$Day";
    }
    
    if(length($Month) < 2)
    {
    	$Month = "0$Month";
    }
    
    my $timestamp = $Day . $Month . $Year;
    
    my $data = substr($message, 1);
	
	if(not defined $device_type_code)
	{
		log_generic($DATABASE, $machine_id, 'other', $data);
	}
	elsif($device_type_code eq '0')
	{
		log_generic($DATABASE, $machine_id, 'G4', $data);
	}
	elsif($device_type_code eq '1')
	{
		log_generic($DATABASE, $machine_id, 'G5', $data);
	}
	elsif($device_type_code eq '3')
	{
		log_generic($DATABASE, $machine_id, 'brick', $data);
	}
	elsif($device_type_code eq '4')
	{
		log_generic($DATABASE, $machine_id, 'sony', $data);
	}
	elsif($device_type_code eq '5')
	{
		&logESuds($DATABASE, $machine_id, $data);
	}
	elsif($device_type_code eq '6')
	{
		log_generic($DATABASE, $machine_id, 'mei', $data);
	}
	elsif($device_type_code eq '11')
	{
		log_generic($DATABASE, $machine_id, 'kiosk', $data);
	}
	elsif($device_type_code eq '12')
	{
		log_generic($DATABASE, $machine_id, 'T2', $data);
	}
	else
	{
		log_generic($DATABASE, $machine_id, 'other', $data);
	}
	
	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return (\@logs);
}

sub log_generic ($$$$)
{
	my ($DATABASE, $machine_id, $event_name, $data) = @_;
	$DATABASE->procedure(
		'name'	=> 'PKG_EXCEPTION_PROCESSOR.sp_log_exception',
		'bind'	=> [ ':pn_exception_code_id', ':pv_additional_info', ':pv_server_name', ':pv_object_name' ],
		'in'	=> [ 10000, $data, $machine_id, $event_name ]
	);
}

sub logESuds
{
	my ($DATABASE, $machine_id, $data) = @_;
	my $diag_code = unpack("n", substr($data, 0, 2));
	my $timestamp = defined $Config{use64bitint} ? unpack("q", substr($data, 2)) : Math::BigInt->new("0x".unpack("H*", substr($data, 2)));
	my $diag_msg = $esuds_log_types{$diag_code};
	if(not defined $diag_msg)
	{
		$diag_msg = "UNKNOWN_CODE_$diag_code";
	}
	
	my @epoch_sec = ($timestamp / 1000, substr($timestamp, -3));
	my @ts_parts = split (/\s+/, gmtime($epoch_sec[0]));
	my $timestamp_str = join(' ', (@ts_parts)[0..3]).'.'.$epoch_sec[1].' '.$ts_parts[4];

	log_generic($DATABASE, $machine_id, $diag_msg, $timestamp_str);
}

1;
