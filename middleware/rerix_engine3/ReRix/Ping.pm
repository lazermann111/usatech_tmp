package ReRix::Ping;

use strict;
use Evend::ReRix::Shared;
use ReRix::Utils;
use USAT::Common::CallInRecord;

sub alive_alert
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $device_id = $command_hashref->{device_id};
	my $raw_handle = $DATABASE->{handle};

	push @logs, "Machine Is Alive  : $machine_id";
	
	USAT::Common::CallInRecord::set_type($DATABASE, $command_hashref, $command_hashref->{session_id}, USAT::Common::CallInRecord::CALL_TYPE_BATCH);

	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);
}

sub ack
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	
	my $acked_msg_no = ord(substr($message, 1, 1));
	
	push @logs, "ACK'd Message     : $acked_msg_no";
	
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'execute_cd',
						update_values	=> ['A'],
						where_columns	=> ['machine_id = ?', 'message_number = ?', 'execute_cd = ?'],
						where_values	=> [$machine_id, $acked_msg_no, 'S'] );

	return(\@logs);
}

sub device_control_ack
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'execute_cd',
						update_values	=> ['A'],
						where_columns	=> ['machine_id = ?', 'data_type = ? or data_type = ?', 'execute_cd = ?'],
						where_values	=> [$machine_id, '73', '76', 'S'] );

	return(\@logs);
}

sub nak
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	
	my (undef, $code) = unpack("C2", $message);	
	push @logs, "NAK!              : $code";

	USAT::Common::CallInRecord::set_status($DATABASE, $command_hashref, $command_hashref->{session_id}, USAT::Common::CallInRecord::STATUS_FAILED);

	return(\@logs);
}

sub nak_V2
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $max_retries = 3;
	my %nak_type =	(
		#NAK type => ['description', resend message boolean (0|1)]
		0	=> [
			'Failed (Unspecified)'
			,1
		]
		,1	=> [
			'Message Not Implemented'
			,0
		]
		,2	=> [
			'CRC Failure'
			,1
		]
	);

	### extract command info
	my $message = pack("H*",$command_hashref->{inbound_command});
	my (undef, $type, $msg_num) = unpack("C3", $message);
	
	my $machine_id = $command_hashref->{machine_id}; 
	USAT::Common::CallInRecord::set_status($DATABASE, $command_hashref, $command_hashref->{session_id}, USAT::Common::CallInRecord::STATUS_FAILED);
	
	### determine the number of times this message has been resent already	#TODO: future queue db action?
	my $array_ref;
	if ($command_hashref->{dbh}->{config}->{driver} =~ m/Pg$/)
	{
		$array_ref = $command_hashref->{dbh}->select(
			table			=> 'engine.machine_cmd_outbound_hist'
			,select_columns	=> 'COUNT(*), command'
			,where_columns	=> [
				'modem_id = ?'
				,"execute_date >= (NOW() - INTERVAL '1 hour')"
				,qq{command = 
					(
						SELECT command
						FROM (
							SELECT command
							FROM engine.machine_cmd_outbound_hist
							WHERE modem_id = ?
							AND LOWER(SUBSTR(command, 1, 2)) = LOWER(?)
							ORDER BY execute_date DESC, command_id DESC
						) AS cmd
						LIMIT 1
					) GROUP BY command
				}
			]
			,where_values	=> [
				$command_hashref->{machine_id}
				,$command_hashref->{machine_id}
				,unpack("H*", pack("C", $msg_num))
			]
		);	
	}
	else
	{
		$array_ref = $command_hashref->{dbh}->select(
			table			=> 'engine.machine_cmd_outbound_hist'
			,select_columns	=> 'COUNT(*), command'
			,where_columns	=> [
				'modem_id = ?'
				,'execute_date >= (sysdate-(60/1440))'
				,qq{command = 
					(
						SELECT command
						FROM (
							SELECT command
							FROM engine.machine_cmd_outbound_hist
							WHERE modem_id = ?
							AND LOWER(SUBSTR(command, 0, 2)) = LOWER(?)
							ORDER BY execute_date DESC, command_id DESC
						)
						WHERE rownum <= 1
					) GROUP BY command
				}
			]
			,where_values	=> [
				$command_hashref->{machine_id}
				,$command_hashref->{machine_id}
				,unpack("H*", pack("C", $msg_num))
			]
		);
	}

	my ($num_retries, $command) = (0, undef);
	if (defined @{$array_ref})
	{
		($num_retries, $command) = (@{$array_ref->[0]})[0..1];
	}
	
	### queue message to be resent
	unless ($num_retries == 0 || $num_retries > $max_retries)
	{		
		ReRix::Utils::enqueue_outbound_command($command_hashref, $command);
	}

	### log result
	push @logs, "NAK V2!      Type : ".(
		defined $nak_type{$type}?
		$nak_type{$type}->[0].(
			$nak_type{$type}->[1]?(
				$num_retries > $max_retries?
				". Msg not resent (max num $max_retries retries exceeded)."
				:(
					$num_retries > 0?
					". Resending command msg num \#$msg_num (attempt $num_retries/$max_retries)"
					:". ERR: no message found in history! Nothing to resend!"
				)
			)
			:". Msg not resent."
		)
		:"Reserved NAK type \#$type received. Msg not resent."
	);

	return(\@logs);
}

1;
