#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		04/01/2004	erybski		First release
#

package ReRix::ClientVersion;
use strict;
use ReRixConfig;

my $log_format_width = $ReRix_global{log_format_key_width};
my %version_string_layout =	(
	5	=> [	#eSuds
		'eSuds Application Version'
		,'Multiplexor Application Version'
		,'Display Firmware Version'
		,'Root File System Version'
		,'Init File System Version'
		,'eSuds File System Version'
		,'MAC Address'
		,'Multiplexor Bootloader Version'
	]
);

sub parse ($$) {
	my ($DATABASE, $command_hashref) = @_;
	my (@logs);
	
	### prepare inbound data
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $device_type = $command_hashref->{device_type};
	my $machine_id = $command_hashref->{machine_id}; 
#	my $msg_no = $command_hashref->{msg_no}; 
#	my $SSN = $command_hashref->{SSN}; 
#	my $response_no = $command_hashref->{response_no}; 
	
	### validate device_id
	if (not defined $device_id) {
		push @logs, "No device record found for $device_id";
		return(\@logs);	
	}
	
	### mark the original pending message ack'd

	push @logs, sprintf("%-".$log_format_width."s", "Client Ver. Resp.").": Removing any client version requests";
	$DATABASE->update(
		table			=> 'machine_command_pending'
		,update_columns	=> 'execute_cd'
		,update_values	=> ['A']
		,where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?', 'execute_cd in (?,?)']
		,where_values	=> [$machine_id, '83', '0B', 'P', 'S']
	);

	### unpack message data
	chomp $message;
#	my $device_type = substr($message, 1, 1);
	my @version_strings = split("\n", substr($message, 1));
	chomp @version_strings;	#eliminate trailing spaces, LFs
	push @logs, sprintf("%-".$log_format_width."s", "Client Ver. Type").": $device_type";
	for (my $i = 0; $i < scalar @version_strings; $i++) {
		push @logs, sprintf("%-".$log_format_width."s", "Version String ".sprintf("% 3u", $i + 1)).": $version_strings[$i]";
	}
	
	### validate device_type
	if ($device_type eq '5') {	#eSuds
		### store version data in database
		### check if insert or update required for each parameter
		my $ver_str_aref = $version_string_layout{$device_type};
		for (my $i = 0; $i < scalar @{$ver_str_aref}; $i++) {
			if (defined $version_strings[$i])
			{
				my $check_setting_ref = $DATABASE->select(
					table			=> 'device_setting'
					,select_columns	=> 'count(1)'
					,where_columns	=> ['device_id = ?', 'device_setting_parameter_cd = ?']
					,where_values	=> [$device_id, $ver_str_aref->[$i]]
				);
				if ($check_setting_ref->[0][0] == 0) {
					### insert
					push @logs, sprintf("%-".$log_format_width."s", "eSuds Version").": Inserting ".$ver_str_aref->[$i]." = $version_strings[$i]";
					$DATABASE->insert(
						table			=> 'device_setting'
						,insert_columns	=> 'device_id, device_setting_parameter_cd, device_setting_value'
						,insert_values	=> [$device_id, $ver_str_aref->[$i], $version_strings[$i]]
					);
				}
				else {
					### update
					push @logs, sprintf("%-".$log_format_width."s", "eSuds Version").": Updating ".$ver_str_aref->[$i]." = $version_strings[$i]";
					$DATABASE->update(
						table			=> 'device_setting'
						,update_columns	=> 'device_setting_value'
						,update_values	=> [$version_strings[$i]]
						,where_columns	=> ['device_id = ?', 'device_setting_parameter_cd = ?']
						,where_values	=> [$device_id, $ver_str_aref->[$i]]
					);
				}
			}
		}
	}
	else {
		push @logs, "Unknown/unimplemented client version type $device_type";
		return(\@logs);	
	}
	
	### no response to client expected; just return
	return(\@logs);	
}

sub request ($$) {
	my ($DATABASE, $ev_number) = @_;
	my @logs;

	### store version request in database
	### check if pending request is already queued
	my $lookup_pending_ref = $DATABASE->select(
		table			=> 'machine_command_pending'
		,select_columns	=> 'count(1)'
		,where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?', 'execute_cd in (?,?)']
		,where_values	=> [$ev_number, '83', '0B', 'P', 'S']
	);
	if (not defined $lookup_pending_ref->[0]) {
		push @logs, sprintf("%-".$log_format_width."s", "Client Ver. Req.").": Failed to check for pending request for $ev_number in sub request";
		return (\@logs);
	}
	if ($lookup_pending_ref->[0][0] eq '0') {
		push @logs, sprintf("%-".$log_format_width."s", "Client Ver. Req.").": Queuing for $ev_number";
		$DATABASE->insert(
			table			=> 'machine_command_pending'
			,insert_columns	=> 'machine_id, data_type, command, execute_cd, execute_order'
			,insert_values	=> [$ev_number, '83', '0B', 'P', 15]
		);
	}
	else {
		push @logs, sprintf("%-".$log_format_width."s", "Client Ver. Req.").": Queued request already exists for $ev_number";
	}

	return (\@logs);	
}

sub request_g4 ($$) {
	my ($DATABASE, $ev_number) = @_;
	my @logs;

	### store version request in database
	### check if pending request is already queued
	my $lookup_pending_ref = $DATABASE->select(
		table			=> 'machine_command_pending'
		,select_columns	=> 'count(1)'
		,where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?', 'execute_cd in (?,?)']
		,where_values	=> [$ev_number, '87', '4400007F200000000F', 'P', 'S']
	);
	if (not defined $lookup_pending_ref->[0]) {
		push @logs, sprintf("%-".$log_format_width."s", "Client Ver. Req.").": Failed to check for pending request for $ev_number in sub request";
		return (\@logs);
	}
	if ($lookup_pending_ref->[0][0] eq '0') {
		push @logs, sprintf("%-".$log_format_width."s", "Client Ver. Req.").": Queuing for $ev_number";
		$DATABASE->insert(
			table			=> 'machine_command_pending'
			,insert_columns	=> 'machine_id, data_type, command, execute_cd, execute_order'
			,insert_values	=> [$ev_number, '87', '4400007F200000000F', 'P', 15]
		);
	}
	else {
		push @logs, sprintf("%-".$log_format_width."s", "Client Ver. Req.").": Queued request already exists for $ev_number";
	}

	return (\@logs);	
}

sub request_gx_gprs_settings ($$) 
{
	my ($DATABASE, $ev_number) = @_;
	my @logs;

	### store GPRS info peek in database
	### check if pending request is already queued
	my $lookup_pending_ref = $DATABASE->select(
		table			=> 'machine_command_pending'
		,select_columns	=> 'count(1)'
		,where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?', 'execute_cd in (?,?)']
		,where_values	=> [$ev_number, '87', '4400802C0000000190', 'P', 'S']
	);
	if (not defined $lookup_pending_ref->[0]) 
	{
		push @logs, sprintf("%-".$log_format_width."s", "GPRS Info Req").": Failed to check for pending request for $ev_number in sub request";
		return (\@logs);
	}
	if ($lookup_pending_ref->[0][0] eq '0') 
	{
		push @logs, sprintf("%-".$log_format_width."s", "GPRS Info Req").": Queuing for $ev_number";
		$DATABASE->insert(
			table			=> 'machine_command_pending'
			,insert_columns	=> 'machine_id, data_type, command, execute_cd, execute_order'
			,insert_values	=> [$ev_number, '87', '4400802C0000000190', 'P', 16]
		);
	}
	else 
	{
		push @logs, sprintf("%-".$log_format_width."s", "GPRS Info Req").": Queued request already exists for $ev_number";
	}

	return (\@logs);	
}

1;
