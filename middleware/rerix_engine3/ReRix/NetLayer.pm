package ReRix::NetLayer;

use strict;
use warnings;

use USAT::Common::Const;
use USAT::Common::CallInRecord;
use USAT::Database::SensitiveData;
use USAT::App::ReRix::Const qw(NET_LAYER__INTERNAL_RESPONSE);
use USAT::Util::Datetime qw(convert_db_timestamp_to_epoch_sec);
use ReRix::Utils;
use OOCGI::Date 0.11;

sub handle_decryption_failure
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $device_id	= $command_hashref->{device_id};
	my $ev_number	= $command_hashref->{machine_id};
	
	my ($max_failures, $check_days) = (10, 2);
	my ($response, $response_hex);

	my $inserted_device_decode_fail_stat_count = $DATABASE->do(
		query => q{INSERT INTO device.device_decode_fail(device_id, device_decode_fail_type_id) VALUES(?, ?)},
		values => [$device_id, 1]);

    my $count = sql_run_func($DATABASE, 'count', $device_id, $check_days);

	if($count > $max_failures) {
    	# generate new encryption key
        my $key = ReRix::Utils::gen_random_key($command_hashref->{SSN});
        my $max_date = sql_run_func($DATABASE, 'max', $device_id, $check_days);
        my $min_date = sql_run_func($DATABASE, 'min', $device_id, $check_days);

		# update device.device table with this encryption key
		my $updated_device_decode_fail_stat_count = $DATABASE->do(
			query => q{UPDATE device.device SET encryption_key = ? WHERE device_id = ?},
			values => [USAT::Database::SensitiveData->new($key, '*' x length($key)), $device_id]);

		# enqueue an 8Fh "Set ID number and KEY" command
		$response = pack("CH2a*a*", $command_hashref->{response_no}, '8F', $key, $ev_number);
		$response_hex = unpack("H*", $response);
		push @logs, "Response          : " . ReRix::Utils::mask_response($response_hex, 4);
		push @logs, "$ev_number is on END bug list. Attempting to send 8Fh command with new encryption key!";
		
		# pass back the message using the original EV number				
		ReRix::Utils::enqueue_outbound_command($command_hashref, $response_hex);

		# send alert for encryption change
        my ($day, $hour, $minute, $second ) = $min_date->Delta_DHMS($max_date);
        my $message = "
        Attention !

        New encryption key has been generated for the device.
        Device id is $device_id and  EV number is $ev_number.
        Key changed due to $count decryption failures in the last
        $day days, $hour hours, $minute minutes and $second seconds
		";
        ReRix::Utils::send_alert($message);
	} else {		
		$response = pack("CH2H2", $command_hashref->{response_no}, NET_LAYER__INTERNAL_RESPONSE, $command_hashref->{_command_subcode});
		$response_hex = unpack("H*", $response);
		push @logs, "Response          : $response_hex";
		push @logs, "$ev_number is NOT on END bug list";
		ReRix::Utils::enqueue_outbound_command($command_hashref, $response_hex);
    }

	return(\@logs);
}

sub sql_run_func($$) {
    my $DATABASE  = shift;
    my $func      = shift;   # values min, max and count
    my $device_id = shift;
    my $days      = shift;

    my $func_sql;
    if($func eq 'count') {
       $func_sql = 'COUNT(1)';
    } elsif($func eq 'max') {
       $func_sql = "TO_CHAR(MAX(ddf.created_ts), 'MM-DD-YYYY HH24:MI:SS')";
    } elsif($func eq 'min') {
       $func_sql = "TO_CHAR(MIN(ddf.created_ts), 'MM-DD-YYYY HH24:MI:SS')";
    } else {
       return;
    }

    my $sql = qq{
		 SELECT $func_sql
		 FROM device.device d, device.device_decode_fail ddf
		 WHERE d.device_id = ?
		 	AND ddf.created_ts >
		 		CASE
		 			WHEN d.device_encr_key_gen_ts > SYSDATE - ? THEN d.device_encr_key_gen_ts
		 			ELSE SYSDATE - ?
		 		END
		    AND ddf.device_id = d.device_id
		    AND ddf.device_decode_fail_type_id = 1
	};

	my $data = $DATABASE->query(
			query => $sql,
            values  => [$device_id, $days, $days]
    );

    if($func eq 'count') {
       return $data->[0][0];
    } elsif( $func eq 'max' || $func eq 'min' ) {
       return OOCGI::Date->new($data->[0][0],'MM-DD-YYYY HH24:MI:SS');
    } else {
       return;
    }
}

sub encryption_key_request
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;

	my $encryption_key = $command_hashref->{encryption_key};
	if (!defined $encryption_key)
	{
		$encryption_key = '';
		push @logs, "Key Request       : Unable to find key";
	}
	
	my $response = pack("CH2H2a*", $command_hashref->{response_no}, NET_LAYER__INTERNAL_RESPONSE, $command_hashref->{_command_subcode}, $encryption_key);
	my $response_hex = unpack("H*", $response);
	push @logs, "Response          : " . ReRix::Utils::mask_response($response_hex, 6);
	ReRix::Utils::enqueue_outbound_command($command_hashref, $response_hex);
	return(\@logs);
}

sub net_layer_start
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $net_layer_id = $command_hashref->{net_layer_id};
	my $message = pack("H*", $command_hashref->{inbound_command});
	my $net_layer_desc = substr($message, 2);
	push @logs, "Net Layer         : $net_layer_desc";
	
	my $res_aref = $DATABASE->procedure(
		'name'	=> 'PKG_ENGINE_MAINT.SP_START_NET_LAYER',
		'bind'	=> [ ":stored_id", ":desc", ":new_id" ],
		'in'	=> [ $net_layer_id, $net_layer_desc ]
	);	
	
	my $new_layer_id = $res_aref->[0] if ref($res_aref) eq 'ARRAY';
	if ($new_layer_id !~ m/^\d+$/o)
	{
		push @logs, "PKG_ENGINE_MAINT.SP_START_NET_LAYER returned invalid net layer id: $new_layer_id";
	}	
	
	return(\@logs);
}	

sub net_layer_stop
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	my $net_layer_id = $command_hashref->{net_layer_id};
	
	$DATABASE->procedure(
		'name'	=> 'PKG_ENGINE_MAINT.SP_STOP_NET_LAYER',
		'bind'	=> [ ":stored_id" ],
		'in'	=> [ $net_layer_id ]
	);
	
	return(\@logs);
}

sub device_session_end
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	
	my @args = ($DATABASE, $command_hashref, $command_hashref->{session_id});
	push @args, split(/\|/, substr(pack("H*", $command_hashref->{inbound_command}), 2));
	USAT::Common::CallInRecord::finish(@args);
	
	$command_hashref->{session_expires_sec} = -1;
	return(\@logs);	
}

sub health_check
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;

	my $response_data;
	my $aref = $DATABASE->query(query => q{SELECT 1 FROM dual});
	if ($aref->[0] && $aref->[0]->[0] == 1)
	{
		$response_data = substr($command_hashref->{inbound_command}, 4);
		push @logs, "Health Check      : Succeeded";
	}
	else
	{
		$response_data = '';
		push @logs, "Health Check      : Failed";
	}

	my $response = pack("CH2H2H*", $command_hashref->{response_no}, NET_LAYER__INTERNAL_RESPONSE, $command_hashref->{_command_subcode}, $response_data);
	my $response_hex = unpack("H*", $response);
	push @logs, "Response          : $response_hex";
	ReRix::Utils::enqueue_outbound_command($command_hashref, $response_hex);
	return(\@logs);
}

1;
