package ReRix::ESuds;

use strict;
use ReRixConfig qw(%ReRix_ESuds);
use ReRix::Utils;
use Time::Local;
use USAT::POS;
use USAT::POS::Const ();
use USAT::SOAP::Util;
use USAT::App::ECCS::Gateway;
use USAT::App::ECCS::SOAP::ComplexType::hostItem;
use USAT::App::ECCS::SOAP::ComplexType::ResponseMessage;
use URI::Escape;
use Sys::Hostname;
use USAT::Security::StringMask qw(mask_credit_card);

use constant {
	EQUIP_TYPE_NOTHING_ON_PORT 								=> 'N',
	EQUIP_TYPE_MAYTAG_WASHER 								=> 'W',
	EQUIP_TYPE_MAYTAG_DRYER 								=> 'D',
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_DRYER 					=> 'S',
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_WASHER 					=> 'U',
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_TOP 			=> 'G',
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_BOTTOM 		=> 'H',
	EQUIP_TYPE_SPEEDQUEEN_GEN1_WASHER 						=> 'A',
	EQUIP_TYPE_SPEEDQUEEN_GEN1_DRYER 						=> 'R',
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_TOP 		=> 'I',
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_BOTTOM 	=> 'J',

	EQUIP_POSITION_UNKNOWN 	=> 'U',
	EQUIP_POSITION_TOP 		=> 'T',
	EQUIP_POSITION_BOTTOM 	=> 'B',

	EQUIP_STATUS_NO_STATUS_AVAILABLE		=> 0,
	EQUIP_STATUS_IDLE_AVAILABLE				=> 1,
	EQUIP_STATUS_IN_CYCLE_FIRST				=> 2,
	EQUIP_STATUS_OUT_OF_SERVICE				=> 3,
	EQUIP_STATUS_NO_WASHER_OR_DRYER_ON_PORT	=> 4,
	EQUIP_STATUS_IDLE_NOT_AVAILABLE			=> 5,
	EQUIP_STATUS_IN_MANUAL_SERVICE_MODE		=> 6,
	EQUIP_STATUS_IN_CYCLE_SECOND			=> 7,
	EQUIP_STATUS_TRANSACTION_IN_PROGRESS	=> 8,

	EQUIP_HOST_TYPE_GROUP_WASHER				=> 'ESUDS_WASHER',
	EQUIP_HOST_TYPE_GROUP_DRYER					=> 'ESUDS_DRYER',
	EQUIP_HOST_TYPE_GROUP_STACKED_DRYER_DRYER	=> 'ESUDS_STACKED_DRYER',
	EQUIP_HOST_TYPE_GROUP_STACKED_WASHER_DRYER	=> 'ESUDS_STACKED_WASHER_DRYER',

	MANUFACTURER_MAYTAG			=> 'Maytag',
	MANUFACTURER_SPEEDQUEEN		=> 'Speed Queen',
	
	TLI_REGULAR_WASH_CYCLE					=> 1,
	TLI_REGULAR_COLD_WASH_CYCLE				=> 2,
	TLI_REGULAR_WARM_WASH_CYCLE				=> 3,
	TLI_REGULAR_HOT_WASH_CYCLE				=> 4,
	TLI_SPECIAL_WASH_CYCLE					=> 5,
	TLI_SPECIAL_COLD_WASH_CYCLE				=> 6,
	TLI_SPECIAL_WARM_WASH_CYCLE				=> 7,
	TLI_SPECIAL_HOT_WASH_CYCLE				=> 8,
	TLI_SUPER_WASH_CYCLE					=> 9,
	TLI_REGULAR_DRY_CYCLE					=> 40,
	TLI_SPECIAL_DRY_CYCLE					=> 41,
	TLI_TOP_OFF_DRY							=> 42
};

use constant EQUIP_TYPE_DESCRIPTIONS => {
	EQUIP_TYPE_NOTHING_ON_PORT() 							=> 'No Washer/Dryer on Port',
	EQUIP_TYPE_MAYTAG_WASHER() 								=> 'Maytag Gen2 Washer',
	EQUIP_TYPE_MAYTAG_DRYER() 								=> 'Maytag Gen2 Dryer',
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_DRYER() 				=> 'Maytag Gen2 Stacked Dryer/Dryer',
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_WASHER() 				=> 'Maytag Gen2 Stacked Washer/Dryer',
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_TOP() 		=> 'Maytag Gen1 Stacked Dryer - Top',
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_BOTTOM() 	=> 'Maytag Gen1 Stacked Dryer - Bottom',
	EQUIP_TYPE_SPEEDQUEEN_GEN1_WASHER ()					=> 'Speed Queen Washer',
	EQUIP_TYPE_SPEEDQUEEN_GEN1_DRYER() 						=> 'Speed Queen Dryer',
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_TOP() 	=> 'Speed Queen Stacked Dryer - Top',
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_BOTTOM()	=> 'Speed Queen Stacked Dryer - Bottom'
};

use constant EQUIP_HOST_TYPE_GROUP_CD => {
	EQUIP_TYPE_NOTHING_ON_PORT() 							=> undef,
	EQUIP_TYPE_MAYTAG_WASHER() 								=> EQUIP_HOST_TYPE_GROUP_WASHER,
	EQUIP_TYPE_MAYTAG_DRYER() 								=> EQUIP_HOST_TYPE_GROUP_DRYER,
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_DRYER() 				=> EQUIP_HOST_TYPE_GROUP_STACKED_DRYER_DRYER,
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_WASHER() 				=> EQUIP_HOST_TYPE_GROUP_STACKED_WASHER_DRYER,
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_TOP() 		=> EQUIP_HOST_TYPE_GROUP_STACKED_DRYER_DRYER,
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_BOTTOM() 	=> EQUIP_HOST_TYPE_GROUP_STACKED_DRYER_DRYER,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_WASHER ()					=> EQUIP_HOST_TYPE_GROUP_WASHER,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_DRYER() 						=> EQUIP_HOST_TYPE_GROUP_DRYER,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_TOP() 	=> EQUIP_HOST_TYPE_GROUP_STACKED_DRYER_DRYER,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_BOTTOM()	=> EQUIP_HOST_TYPE_GROUP_STACKED_DRYER_DRYER
};

use constant EQUIP_GROUP_WASHER => (
	EQUIP_TYPE_MAYTAG_WASHER,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_WASHER
);
use constant EQUIP_GROUP_DRYER => (
	EQUIP_TYPE_MAYTAG_DRYER,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_DRYER
);
use constant EQUIP_GROUP_STACKED_DRYER => (
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_DRYER,
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_TOP,
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_BOTTOM,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_TOP,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_BOTTOM
);
use constant EQUIP_GROUP_STACKED_WASHER_DRYER => (
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_WASHER
);
use constant EQUIP_GROUP_SINGLE_TOP => (
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_TOP,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_TOP
);
use constant EQUIP_GROUP_STACKED => (
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_DRYER,
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_TOP,
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_BOTTOM,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_TOP,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_BOTTOM,
	EQUIP_TYPE_MAYTAG_STACKED_DRYER_WASHER
);
use constant EQUIP_GROUP_SPEEDQUEEN => (
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_TOP,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_BOTTOM,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_WASHER,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_DRYER
);
use constant EQUIP_GROUP_STACKED_GEN1 => (
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_TOP,
	EQUIP_TYPE_MAYTAG_GEN1_STACKED_DRYER_DRYER_BOTTOM,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_TOP,
	EQUIP_TYPE_SPEEDQUEEN_GEN1_STACKED_DRYER_DRYER_BOTTOM
);
use constant TLI_GROUP_NON_REGULAR_WASH_CYCLE => (
	TLI_REGULAR_COLD_WASH_CYCLE,
	TLI_REGULAR_WARM_WASH_CYCLE,
	TLI_REGULAR_HOT_WASH_CYCLE,
	TLI_SPECIAL_WASH_CYCLE,
	TLI_SPECIAL_COLD_WASH_CYCLE,
	TLI_SPECIAL_WARM_WASH_CYCLE,
	TLI_SPECIAL_HOT_WASH_CYCLE,
	TLI_SUPER_WASH_CYCLE
);

my $EQUIP_GROUP_WASHER_REGEX = '('.join('|', EQUIP_GROUP_WASHER).')';
my $EQUIP_GROUP_DRYER_REGEX = '('.join('|', EQUIP_GROUP_DRYER).')';
my $EQUIP_GROUP_STACKED_DRYER_REGEX = '('.join('|', EQUIP_GROUP_STACKED_DRYER).')';
my $EQUIP_GROUP_STACKED_WASHER_DRYER_REGEX = '('.join('|', EQUIP_GROUP_STACKED_WASHER_DRYER).')';
my $EQUIP_GROUP_SINGLE_TOP_REGEX = '('.join('|', EQUIP_GROUP_SINGLE_TOP).')';
my $EQUIP_GROUP_STACKED_REGEX = '('.join('|', EQUIP_GROUP_STACKED).')';
my $EQUIP_GROUP_SPEEDQUEEN_REGEX = '('.join('|', EQUIP_GROUP_SPEEDQUEEN).')';
my $EQUIP_GROUP_STACKED_GEN1_REGEX = '('.join('|', EQUIP_GROUP_STACKED_GEN1).')';
my $TLI_GROUP_NON_REGULAR_WASH_CYCLE_REGEX = '('.join('|', TLI_GROUP_NON_REGULAR_WASH_CYCLE).')';

sub washer_dryer_labels
{
	# return logs for info and errors for logging
	my (@logs);

	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id};
	my $device_id = $command_hashref->{device_id};
	my $SSN = $command_hashref->{SSN};

	# chop the first 2 byte Data Type
	$message = substr($message, 2);

	# labels are in 5 byte chunks, 1 per port
	my $num_ports = length($message)/5;

	if(length($num_ports) > 2)
	{
		push @logs, "Data looks corrupt!  num_ports = $num_ports";
		ReRix::Utils::send_alert("ESuds: washer_dryer_labels: Data looks corrupt! num_ports = $num_ports", $command_hashref);
		return(\@logs);
	}

	push @logs, "Num Ports         : $num_ports";

	# eat away at the message until it's gone
	while(length($message) > 0)
	{
		my $chunk = substr($message, 0, 5);
		$message = substr($message, 5);

		my $port = ord(substr($chunk, 0, 1));
		my $label = substr($chunk, 1, 3);
		my $top_bottom = substr($chunk, 4, 1);

		$label =~ s/\s//g;

		# translate T/B into position number
		my $position = 0;
		if($top_bottom eq EQUIP_POSITION_TOP)
		{
			$position = 1;
		}

		if(length($label) == 0)
		{
			push @logs, "Skipping          : Empty label for Port $port Position $position";
			next;
		}

		# lookup the host in the database
		my $get_host_ref = $DATABASE->select(
							table			=> 'host',
							select_columns	=> 'host.host_id',
							where_columns	=> [ 'host.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
							where_values	=> [ $device_id, $port, $position ] );

		if(not defined $get_host_ref->[0])
		{
			push @logs, @{_request_room_layout($DATABASE, $machine_id)};
			push @logs, "No host record found for device_id $device_id, port $port, position $position";
			ReRix::Utils::send_alert("ESuds: washer_dryer_labels: No host record found for device_id $device_id, port $port, position $position", $command_hashref);
			return(\@logs);
		}

		my $host_id = $get_host_ref->[0][0];

		# now update the host's label
		$DATABASE->update(	table            => 'host',
							update_columns   => 'host_label_cd',
							update_values    => [$label],
							where_columns    => ['host_id = ?'],
							where_values     => [$host_id] );

		push @logs, "Label             : Port $port, Pos $position; Host $host_id Label = $label";
	}

	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);
}

sub washer_dryer_diagnostics
{
	# return logs for info and errors for logging
	my (@logs);

	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id};
	my $device_id = $command_hashref->{device_id};
	my $SSN = $command_hashref->{SSN};

	my $port = ord(substr($message, 2, 1));
	my $diag_block = substr($message, 3);

	# we are receiving diagnostic, which may have been sent as a result of a pending message
	# even if it wasn't, we're going to remove any pending diagnostic request messages
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'execute_cd',
						update_values	=> ['A'],
						where_columns	=> ['machine_id = ?', 'data_type = ?', 'execute_cd = ?'],
						where_values	=> [$machine_id, '9A61', 'S'] );

	# lookup the host in the database
	# diagnostics for a stack do not report top/bottom
	# we're linking diagnostics to the bottom if there are 2 machines on the port
	my $get_host_ref = $DATABASE->select(
						table			=> 'host',
						select_columns	=> 'host.host_id',
						where_columns	=> [ 'host.device_id = ?', 'host.host_port_num = ?'],
						where_values	=> [ $device_id, $port],
						order			=> 'host.host_position_num ASC');

	if(not defined $get_host_ref->[0])
	{
		push @logs, @{_request_room_layout($DATABASE, $machine_id)};
		push @logs, "No host record found for device_id $device_id, port $port, position 0";
		ReRix::Utils::send_alert("ESuds: washer_dryer_diagnostics: No host record found for device_id $device_id, port $port, position 0", $command_hashref);
		return(\@logs);
	}

	my $host_id = $get_host_ref->[0][0];
	push @logs, "Port/Host         : $port/$host_id";
	push @logs, "Diagnostic Block  : " . unpack("H*", $diag_block);

	# get any active diagnostic codes
	my $get_actives_ref = $DATABASE->select(
							table			=> 'host_diag_status',
							select_columns	=> 'host_diag_status_id, host_diag_cd',
							where_columns	=> [ 'host_id = ? and host_diag_clear_ts is null' ],
							where_values	=> [ $host_id ] );

	my %actives_hash;
	if(not defined $get_actives_ref->[0])
	{
		# there are no active diagnostics
		push @logs, "Active Codes      : NONE";
	}
	else
	{
		# load the active codes into a hashtable
		foreach my $row (@$get_actives_ref)
		{
			my $diag_id = $row->[0];
			my $code = $row->[1];
			push @logs, "Active Code       : ID=$diag_id CODE=$code";
			$actives_hash{$code} = $diag_id;
		}
	}

	# now step through the incoming codes and insert/update them in the database
	for(my $index=0; $index<length($diag_block); $index++)
	{
		my $code = ord(substr($diag_block, $index, 1));

		# check if this code is currently active for this device
		my $diagnostic_id = $actives_hash{$code};

		if(not defined $diagnostic_id)
		{
			# code does not exist, insert it
			$DATABASE->insert(	table=> 'host_diag_status',
								insert_columns=> 'host_id, host_diag_cd',
								insert_values=> [$host_id, $code] );

			push @logs, "New Code          : Port=$port CODE=$code";
		}
		else
		{
			# code exists, update it
			$DATABASE->do(
				query => q{ UPDATE host_diag_status SET host_diag_last_reported_ts = sysdate WHERE host_diag_status_id = ? },
				values => [ $diagnostic_id ]
			);

			push @logs, "Updated Code      : Port=$port CODE=$code";

			# remove it from the hash
			delete($actives_hash{$code});
		}
	}

	# anything left in the hash is a cleared code!
	while(my ($code, $diagnostic_id) = each(%actives_hash))
	{
		# set the cleared date
		$DATABASE->do(
			query => q{ UPDATE host_diag_status SET host_diag_clear_ts = sysdate WHERE host_diag_status_id = ? },
			values => [ $diagnostic_id ]
		);

		push @logs, "Cleared Code      : Port= $port CODE=$code";
	}

	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);
}

sub room_model_layout_v2
{
	# return logs for info and errors for logging
	my (@logs);

	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id};
	my $device_id = $command_hashref->{device_id};
	my $SSN = $command_hashref->{SSN};

	# chop the first 2 bytes Data Type
	$message = substr($message, 2);

	my $db_unit_count = 0;
	my $incoming_unit_count = 0;

	# eat away at the message, pull out sent data, lookup in host table
	my @units;
	while(length($message) > 0)
	{
		#push @logs, "Message           : " . unpack("H*", $message);

		my $port = substr($message, 0, 1);
		my $serial_number = substr($message, 1, 10);
		my $model_code = substr($message, 11, 1);
		my $firmware = substr($message, 12, 1);
		my $max_price = unpack("n", substr($message, 13, 2));
		my $unit_type = substr($message, 15, 1);
		my $num_supported_cycles = substr($message, 16, 1);

		$max_price = sprintf("%.02f", ($max_price * .01));

		$num_supported_cycles = ord($num_supported_cycles);
		my $cycle_block = substr($message, 17, ($num_supported_cycles*4));

		# chop this data from the message
		my $unit_block_length = (17 + ($num_supported_cycles*4));
		$message = substr($message, $unit_block_length);

		if($unit_type eq EQUIP_TYPE_NOTHING_ON_PORT)
		{
			push @logs, "Port $port        : Nothing on port, skipping!";
			next;
		}

		# convert port and num_supported_cycles to an integer
		$port = ord($port);
		$model_code = ord($model_code);
		$firmware = ord($firmware);

		# convert max price to a number, formatted like a dollar value

		# serial_number is left aligned, padded with spaces.  Trim the spaces
		$serial_number =~ s/\s+$//;

		# only 2 manufacturers so far, if we get another this will need changes
		my $manufacturer = ($unit_type =~ /^$EQUIP_GROUP_SPEEDQUEEN_REGEX$/o ? MANUFACTURER_SPEEDQUEEN : MANUFACTURER_MAYTAG);

		push @logs, "Port              : $port";
		push @logs, "Serial Number     : $serial_number";
		push @logs, "Manufacturer      : $manufacturer";
		push @logs, "Model             : $model_code";
		push @logs, "Firmware Version  : $firmware";
		push @logs, "Max Price         : $max_price";
		push @logs, "Unit Type         : $unit_type (" . EQUIP_TYPE_DESCRIPTIONS->{$unit_type} . ")";
		push @logs, "Supported Cycles  : $num_supported_cycles";
		push @logs, "Cycle Block       : " . unpack("H*", $cycle_block);

		my @suppored_cycles;
		# parse the suppored cycles block into an array
		# should be in 4 bytes chunks
		for (my $index=0; $index<$num_supported_cycles; $index++)
		{
			my $cycle_chunk = substr($cycle_block, ($index*4), 4);
			my $cycle_type = ord(substr($cycle_chunk, 0, 1));
			my $cycle_price = unpack("n", substr($cycle_chunk, 1, 2));
			my $cycle_time = ord(substr($cycle_chunk, 3, 1));
			$cycle_price = sprintf("%.02f", ($cycle_price * .01));

			push(@suppored_cycles, [$cycle_type, $cycle_price, $cycle_time]);
			push @logs, "   Cycle          : Type= $cycle_type, Price=  $cycle_price, Time= $cycle_time";
		}

		my $host_type_ref = $DATABASE->select (
								table			=> 'host_type, device_type_host_type',
								select_columns	=> 'host_type.host_type_id',
								where_columns	=> [ 'host_type.host_type_id = device_type_host_type.host_type_id', 'device_type_host_type.device_type_host_type_cd = ?', 'device_type_host_type.device_type_id = ?' ],
								where_values	=> [ $unit_type, '5'] );

		if(not defined $host_type_ref->[0])
		{
			# this host_type doesn't exist, this is not an acceptable condition.  All host_type codes that devices use must known
			push @logs, "Unknown host type code: $unit_type!";
			ReRix::Utils::send_alert("ESuds: room_model_layout_v2: Data looks corrupt! Unknown host type code: $unit_type!", $command_hashref);
			return(\@logs);
		}

		my $host_type_id = $host_type_ref->[0][0];
		push @logs, "Existing Host Type: $host_type_id = $unit_type";

		my $host_equip_ref = $DATABASE->select (
								table			=> 'host_equipment',
								select_columns	=> 'host_equipment.host_equipment_id',
								where_columns	=> [ 'host_equipment.host_equipment_mfgr = ?', 'host_equipment.host_equipment_model = ?' ],
								where_values	=> [ $manufacturer, $model_code ] );

		my $host_equip_id;

		if(not defined $host_equip_ref->[0])
		{
			# this equipment doesn't exist, create a new record

			# first get the equip id from a sequence
			my $host_equip_seq_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'device.seq_host_equipment_id.NEXTVAL' );

			if(not defined $host_equip_seq_ref->[0])
			{
				push @logs, "Failed to query device.seq_host_equipment_id sequence for a new ID!";
				ReRix::Utils::send_alert("ESuds: Failed to query device.seq_host_equipment_id sequence for a new ID!", $command_hashref);
				return(\@logs);
			}

			$host_equip_id = $host_equip_seq_ref->[0][0];

			$DATABASE->insert(
				table			=> 'host_equipment',
				insert_columns	=> 'host_equipment_id, host_equipment_mfgr, host_equipment_model',
				insert_values	=> [$host_equip_id, $manufacturer, $model_code]);

			push @logs, "New Host Equip : $host_equip_id = ($manufacturer, $model_code)";

			$DATABASE->insert(
				table			=> 'host_type_host_equipment',
				insert_columns	=> 'host_type_id, host_equipment_id',
				insert_values	=> [$host_type_id, $host_equip_id]);
		}
		else
		{
			$host_equip_id = $host_equip_ref->[0][0];
			push @logs, "Existing Host Equip: $host_equip_id = ($manufacturer, $model_code)";
		}

		# now that we have all the relevant info, save in an array for later use
		# max_price is getting thrown away for now, maybe we'll use it later
		if($unit_type eq EQUIP_TYPE_MAYTAG_STACKED_DRYER_DRYER)
		{
			# this is a stack, break it into 2 separate hosts

			# 0 is bottom
			push(@units, [$model_code, $unit_type, $port, '0', $host_type_id, $firmware, $serial_number, \@suppored_cycles, $host_equip_id]);
			push @logs, "Received stacked client equip record: $model_code, $unit_type, $port, 0, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;

			# 1 is top
			push(@units, [$model_code, $unit_type, $port, '1', $host_type_id, $firmware, $serial_number, \@suppored_cycles, $host_equip_id]);
			push @logs, "Received stacked client equip record: $model_code, $unit_type, $port, 1, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;
		}
		elsif($unit_type eq EQUIP_TYPE_MAYTAG_STACKED_DRYER_WASHER)
		{
			# this is a stacked washer/dryer, break it into 2 separate hosts
			# bottom (0) is the washer
			# top (1) is the dryer

			# 0 is bottom
			push(@units, [$model_code, $unit_type, $port, '0', $host_type_id, $firmware, $serial_number, \@suppored_cycles, $host_equip_id]);
			push @logs, "Received stacked client equip record: $model_code, $unit_type, $port, 0, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;

			# 1 is top
			push(@units, [$model_code, $unit_type, $port, '1', $host_type_id, $firmware, $serial_number, \@suppored_cycles, $host_equip_id]);
			push @logs, "Received stacked client equip record: $model_code, $unit_type, $port, 1, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;
		}
		elsif ($unit_type =~ m/^$EQUIP_GROUP_SINGLE_TOP_REGEX$/o)
		{
			# this is a single unit in top position
			# top (1) is the dryer

			# 1 is top
			push(@units, [$model_code, $unit_type, $port, '1', $host_type_id, $firmware, $serial_number, \@suppored_cycles, $host_equip_id]);
			push @logs, "Received single top client equip record: $model_code, $unit_type, $port, 1, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;
		}
		else
		{
			# assume everything else is a single unit in bottom position

			push(@units, [$model_code, $unit_type, $port, '0', $host_type_id, $firmware, $serial_number, \@suppored_cycles, $host_equip_id]);
			push @logs, "Received single bottom client equip record: $model_code, $unit_type, $port, 0, $host_type_id, $firmware, $serial_number";
			$incoming_unit_count++;
		}
	}

	push @logs, "Incoming Hosts   : $incoming_unit_count";

	# we should now have a nice array of incoming host records

	# now we lookup any already existing host records
	my $existing_hosts_ref = $DATABASE->select(
						table			=> 'host',
						select_columns	=> 'host_id, host_type_id, host_port_num, host_position_num, host_serial_cd, host_equipment_id',
						where_columns	=> [ 'device_id = ?', 'host_port_num > ?' ],
						where_values	=> [ $device_id, '0' ] );

	# these flags let us know what needs to be done depending on what already exists in the database
	my $insert_device = 'N';
	my $insert_host = 'N';

	my %db_units;
	if(defined $existing_hosts_ref->[0])
	{
		# at least one host record exists

		# load up the db records, put in a hash indexed by port number
		foreach my $row (@$existing_hosts_ref)
		{
			$db_units{("$row->[2]-$row->[3]")} = [$row->[1], $row->[4], $row->[0], $row->[5]];
			push @logs, "Existing Host     : ID=$row->[0], Port=$row->[2], Position=$row->[3], host_type_id=$row->[1], host_type_id=$row->[5]";
			$db_unit_count++;
		}

		# compare the db records to the client records
		foreach(@units)
		{
			#       0          1       2       3           4           5            6                7              8
			# [model_code, unit_type, port, position, host_type_id, firmware, serial_number, suppored_cycles, host_equip_id]

			my $incoming_model_code = $_->[0];
			my $incoming_unit_type = $_->[1];
			my $incoming_port = $_->[2];
			my $incoming_position = $_->[3];
			my $incoming_host_type_id = $_->[4];
			my $incoming_firmware = $_->[5];
			my $incoming_serial_number = $_->[6];
			my $suppored_cycles_ref = $_->[7];
			my $incoming_host_equip_id = $_->[8];

			my $key = "$incoming_port-$incoming_position";
			my $db_host_type_id = $db_units{$key}->[0];
			my $db_host_equip_id = $db_units{$key}->[3];

			if(not defined $db_host_type_id)
			{
				# client record exists but db does not;  this triggers a new device to be inserted
				$insert_device = 'Y';
				$insert_host = 'Y';
				push @logs, "Room Setup Status : Port $incoming_port, Position $incoming_position Does Not Exist";
			}
			else
			{
				# this is the key check that decides if the room layout has changed!
				if($db_host_type_id == $incoming_host_type_id && $db_host_equip_id == $incoming_host_equip_id)
				{
					# records are equal, don't trigger a new device insert
					push @logs, "Room Setup Status : Port $incoming_port, Position $incoming_position is Identical: $db_host_type_id, $db_host_equip_id";
				}
				else
				{
					# records are different;  this causes a new device to be inserted
					$insert_device = 'Y';
					$insert_host = 'Y';
					push @logs, "Room Setup Status :  Port $incoming_port, Position $incoming_position is Changed! ($db_host_type_id = $incoming_host_type_id, $db_host_equip_id = $incoming_host_equip_id)";
				}
			}
		}

		if($incoming_unit_count < $db_unit_count)
		{
			push @logs, "Room Setup Status : Fewer incoming hosts than existing ($incoming_unit_count < $db_unit_count).   Layout is Changed!";
			$insert_device = 'Y';
			$insert_host = 'Y';
		}
	}
	else
	{
		# no host records exist
		$insert_host = 'Y';
		push @logs, "Room Setup Status : No host records exist in the database";
	}

	if($insert_device eq 'Y')
	{
		# make the old device inactive, insert a new one.  Stored proc does this nicely.

		my $old_device_id = $device_id;
		my $array_ref = $DATABASE->callproc(
			'name'	=> 'PKG_DEVICE_MAINT.SP_ASSIGN_NEW_DEVICE_ID',
			'bind'	=> [ qw(:old_id :new_id :return_code :error_message) ],
			'in'	=> [ $old_device_id ],
			'inout'	=> [ 20, 20, 40 ]
		);
		my ($new_device_id, $return_code, $error_message) = @{$array_ref} if defined $array_ref;

		if(not defined $new_device_id)
		{
			push @logs, "Failed to call stored procedure PKG_DEVICE_MAINT.SP_ASSIGN_NEW_DEVICE_ID!";

			push @logs, "DBI errstr        : ".$DATABASE->errstr;
			push @logs, "Return Code       : $return_code";
			push @logs, "Error Message     : $error_message" if defined $error_message;

			ReRix::Utils::send_alert("ESuds: Failed to call stored procedure PKG_DEVICE_MAINT.SP_ASSIGN_NEW_DEVICE_ID!\n$error_message\n".$DATABASE->errstr, $command_hashref);
			return(\@logs);
		}

		# remove spaces
		$new_device_id =~ s/\s//g;

		$device_id = $new_device_id;
		push @logs, "New device_id     : $device_id";
	}

	# create a hash to store cycles for later use
	my %cycles_hash;

	# if there was a new device, or no host records existed, insert new ones
	if($insert_host eq 'Y')
	{
		foreach(@units)
		{
			#       0          1       2       3           4           5            6                7              8
			# [model_code, unit_type, port, position, host_type_id, firmware, serial_number, suppored_cycles, host_equip_id]

			my $incoming_model_code = $_->[0];
			my $incoming_unit_type = $_->[1];
			my $incoming_port = $_->[2];
			my $incoming_position = $_->[3];
			my $incoming_host_type_id = $_->[4];
			my $incoming_firmware = $_->[5];
			my $incoming_serial_number = $_->[6];
			my $suppored_cycles_ref = $_->[7];
			my $incoming_host_equip_id = $_->[8];

			# insert the host
			# first get the host_id from a sequence
			my $host_seq_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'device.seq_HOST_id.nextval' );

			if(not defined $host_seq_ref->[0])
			{
				push @logs, "Failed to query seq_HOST_id sequence for a new ID!";
				ReRix::Utils::send_alert("ESuds: Failed to query seq_HOST_id sequence for a new ID!", $command_hashref);
				return(\@logs);
			}

			my $host_id = $host_seq_ref->[0][0];

			$DATABASE->insert(
				table			=> 'host',
				insert_columns	=> 'host_id, device_id, host_serial_cd, host_status_cd, host_est_complete_minut, host_type_id, host_port_num, host_position_num, HOST_SETTING_UPDATED_YN_FLAG, host_equipment_id',
				insert_values	=> [$host_id, $device_id, $incoming_serial_number, EQUIP_STATUS_NO_STATUS_AVAILABLE, '0', $incoming_host_type_id, $incoming_port, $incoming_position , 'N', $incoming_host_equip_id]);

			push @logs, "New Host          : $host_id, $device_id, $incoming_serial_number, ".EQUIP_STATUS_NO_STATUS_AVAILABLE.", 0, $incoming_host_type_id, $incoming_port, $incoming_position , N, $incoming_host_equip_id";

			if(defined $incoming_firmware && $incoming_firmware != '0')
			{
				$DATABASE->insert(
					table			=> 'host_setting',
					insert_columns	=> 'host_id, HOST_SETTING_PARAMETER, host_setting_value',
					insert_values	=> [$host_id, "Firmware Version", $incoming_firmware]);
			}

			# store the suppored cycles with host_id as the key
			$cycles_hash{$host_id} = $suppored_cycles_ref;
		}
	}
	else
	{
		# we don't need to insert hosts, but we still want to update the cycles times
		foreach(@units)
		{
			my $incoming_port = $_->[2];
			my $incoming_position = $_->[3];
			my $suppored_cycles_ref = $_->[7];

			my $key = "$incoming_port-$incoming_position";
			my $db_host_id = $db_units{$key}->[2];

			# store the suppored cycles with host_id as the key
			$cycles_hash{$db_host_id} = $suppored_cycles_ref;
		}
	}

	if(($insert_host eq 'N') && ($insert_device eq 'N'))
	{
		push @logs, "Identical Setup   : $device_id";
	}

	push @logs, "Processing cycle price/times";

	# now iterate through each host and update the incoming cycle times
	while(my($host_id, $cycles_ref) = each (%cycles_hash))
	{
		my @cycles_array = @$cycles_ref;
		foreach my $cycle_ref (@cycles_array)
		{
			my $cycle_type = $cycle_ref->[0];
			my $cycle_price = $cycle_ref->[1];
			my $cycle_time = $cycle_ref->[2];

			my $cycle_desc = _get_item_details($DATABASE, $cycle_type);
			if(not defined $cycle_desc)
			{
				push @logs, "Failed to lookup tran_line_item_type for item_id $cycle_type";
				ReRix::Utils::send_alert("ESuds: Failed to lookup tran_line_item_type for item_id $cycle_type", $command_hashref);
				return(\@logs);
			}

			#push @logs, "Cycle Time/Price  : Host $host_id, Type $cycle_type($cycle_desc), Time $cycle_time, Price $cycle_price";

			if($cycle_time > 0)	#no machine should allow cycle time of 0, so we assume undefined in db to ease error checking
			{
				# lookup the cycle time in host_setting table
				my $cycle_time_ref = $DATABASE->select(
								table			=> 'host_setting',
								select_columns	=> 'host_setting_value',
								where_columns	=> [ 'host_id = ?', 'HOST_SETTING_PARAMETER = ?' ],
								where_values	=> [ $host_id, "$cycle_desc Time" ] );

				if(not defined $cycle_time_ref->[0])
				{
					push @logs, "Insert Cycle      : $host_id, \"$cycle_desc Time\", $cycle_time";
					# cycle time has not been recorded, insert it
					$DATABASE->insert(
						table			=> 'host_setting',
						insert_columns	=> 'host_id, HOST_SETTING_PARAMETER, host_setting_value',
						insert_values	=> [$host_id, "$cycle_desc Time", $cycle_time]);
				}
				else
				{
					if($cycle_time_ref->[0][0] ne $cycle_time)
					{
						push @logs, "Update Cycle      : $host_id, \"$cycle_desc Time\", $cycle_time";
						# cycle time is changed, update it
						$DATABASE->update(table      => 'host_setting',
										update_columns   => 'host_setting_value',
										update_values    => [$cycle_time],
										where_columns	=> [ 'host_id = ?', 'HOST_SETTING_PARAMETER = ?' ],
										where_values	=> [ $host_id, "$cycle_desc Time" ] );

						### reset average time (if exists) for this host & cycle_type
						push @logs, "Update Cycle Est. : $host_id, $cycle_type, 0";
						# cycle time is changed, update it
						$DATABASE->update(
							table			=> 'est_tran_complete_time'
							,update_columns	=> 'avg_tran_complete_minut, num_cycle_in_avg'
							,update_values	=> [ 0, 0 ]
							,where_columns	=> [ 'tran_line_item_type_id = ?', 'host_id = ?' ]
							,where_values	=> [ $cycle_type, $host_id ]
						);
					}
					else
					{
						push @logs, "Cycle Up-To-Date  : $host_id, \"$cycle_desc Time\", $cycle_time";
					}
				}
			}
			#else
			#{
			#	push @logs, "Skipping          : Time = $cycle_time";
			#}

#			if($cycle_price > 0)
#			{
			# lookup the cycle price in host_setting table
			my $cycle_price_ref = $DATABASE->select(
							table			=> 'host_setting',
							select_columns	=> 'host_setting_value',
							where_columns	=> [ 'host_id = ?', 'HOST_SETTING_PARAMETER = ?' ],
							where_values	=> [ $host_id, "$cycle_desc Price" ] );

			if(not defined $cycle_price_ref->[0])
			{
				push @logs, "Insert Price      : $host_id, \"$cycle_desc Price\", $cycle_price";
				# cycle price has not been recorded, insert it
				$DATABASE->insert(
					table			=> 'host_setting',
					insert_columns	=> 'host_id, HOST_SETTING_PARAMETER, host_setting_value',
					insert_values	=> [$host_id, "$cycle_desc Price", $cycle_price]);
			}
			else
			{
				if($cycle_price_ref->[0][0] ne $cycle_price)
				{
					push @logs, "Update Price      : $host_id, \"$cycle_desc Price\", $cycle_price";
					# cycle price is changed, update it
					$DATABASE->update(table      => 'host_setting',
									update_columns   => 'host_setting_value',
									update_values    => [$cycle_price],
									where_columns	=> [ 'host_id = ?', 'HOST_SETTING_PARAMETER = ?' ],
									where_values	=> [ $host_id, "$cycle_desc Price" ] );
				}
				else
				{
					push @logs, "Cycle Up-To-Date  : $host_id, \"$cycle_desc Price\", $cycle_price";
				}
			}
#			}
			#else
			#{
			#	push @logs, "Skipping          : Price = $cycle_price";
			#}
		}
	}

	# pack a response and save it
	# response command is a hex '9A,46'
	my $response = pack("C*C*C*", $response_no, 0x9A, 0x46);
	push @logs, "Response          : " . unpack("H*", $response);
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);
}

sub room_model_layout
{
	# return logs for info and errors for logging
	my (@logs);

	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;

	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id};
	my $device_id = $command_hashref->{device_id};
	my $SSN = $command_hashref->{SSN};

	push @logs, "room_model_layout (9Ah45h) : Deprecated! No response sent.";

	#my $response = pack("C*C*C*", $response_no, 0x9A, 0x46);
	#push @logs, "Response          : " . unpack("H*", $response);
	#$DATABASE->insert(	table=> 'Machine_Command',
	#					insert_columns=> 'Modem_ID, Command',
	#					insert_values=> [$machine_id, unpack("H*",$response)] );

	return(\@logs);
}

sub room_status
{
	my ($DATABASE, $command_hashref) = @_;

	my $message = pack("H*",$command_hashref->{inbound_command});

	my $starting_port = 1;
	my $bytes = substr($message, 2);

	return _room_status($DATABASE, 1, $command_hashref, $starting_port, $bytes);
}

sub room_status_starting_port
{
	my ($DATABASE, $command_hashref) = @_;

	my $message = pack("H*",$command_hashref->{inbound_command});

	my $starting_port = ord(substr($message, 2, 1));
	my $bytes = substr($message, 3);

	return _room_status($DATABASE, 0, $command_hashref, $starting_port, $bytes);
}

sub _room_status_update_host ($$$$$$$)
{
	my ($DATABASE, $is_full_room_status, $device_id, $host_id, $port, $position, $position_status) = @_;
	my @logs;

	# lookup the host in the database
	my $get_host_ref = $DATABASE->select(
		table			=> 'device, host, host_type, device_type_host_type, host_type_host_group_type, host_group_type',
		select_columns	=> "host.host_id, host.host_status_cd, host.host_label_cd, to_char(host.host_last_start_ts, 'ss:mi:hh24:dd:mm:yyyy:d:ddd'), host_type.host_type_desc, device_type_host_type.device_type_host_type_cd, host_group_type.host_group_type_name",
		where_columns	=> [ 'device.device_id = host.device_id', 'host.host_type_id = host_type.host_type_id', 'host_type.host_type_id = device_type_host_type.host_type_id', 'device.device_type_id = device_type_host_type.device_type_id', 'host_type.host_type_id = host_type_host_group_type.host_type_id', 'host_type_host_group_type.host_group_type_id = host_group_type.host_group_type_id', 'host.host_id = ?' ],
		where_values	=> [ $host_id ] );

	if(defined $get_host_ref->[0])
	{
		push @logs, "Port/Pos/Status   : $port/$position/$position_status -----";

		my $host_id = $get_host_ref->[0][0];
		my $host_status_flag = $get_host_ref->[0][1];
		my $host_label_cd = $get_host_ref->[0][2];
		my $last_start_str = $get_host_ref->[0][3];
		my $host_type_name = $get_host_ref->[0][4];
		my $host_type_cd = $get_host_ref->[0][5];
		my $host_group_type_name = $get_host_ref->[0][6];

		if(not defined $host_label_cd)
		{
			$host_label_cd = "";
		}

		# for free-vend mode usage
		my $base_cycle_type_id = _get_host_base_cycle_type_id($host_type_cd, $position);

		$host_status_flag =~ s/\s+$//;

		push @logs, "Database Host     : $host_id($host_label_cd) status = $host_status_flag";

		if($host_status_flag eq $position_status)
		{
			push @logs, "No Change!        : Port $port, Position $position";
		}
		else
		{
			my $description = $host_group_type_name . " " . $host_label_cd;
			if($host_type_cd =~ m/^$EQUIP_GROUP_STACKED_REGEX$/o)
			{
				if($position eq '1')
				{
					$description = "Top " . $description;
				}
				else
				{
					$description = "Bottom " . $description;
				}
			}

			my $cycle_started = 0;

			# compare the status
			if(($host_status_flag eq EQUIP_STATUS_IN_CYCLE_FIRST || $host_status_flag eq EQUIP_STATUS_IN_CYCLE_SECOND)
				&& ($position_status ne EQUIP_STATUS_IN_CYCLE_FIRST && $position_status ne EQUIP_STATUS_IN_CYCLE_SECOND && $position_status ne EQUIP_STATUS_TRANSACTION_IN_PROGRESS))	# cycle finished
			{
				push @logs, "Cycle Finished!   : $description";

				### update the host status (also acts as semaphore control -- prevent potential race condition that would leave status in a sequentially-invalid state) ###
				my $c_update_host_status = $DATABASE->update(
					table            => 'host',
					update_columns   => 'host_status_cd',
					update_values    => [$position_status],
					where_columns    => ['host_id = ?', 'host_status_cd IN (?, ?)'],
					where_values     => [$host_id, EQUIP_STATUS_IN_CYCLE_FIRST, EQUIP_STATUS_IN_CYCLE_SECOND]
				);
				if (!defined $c_update_host_status) {
					push @logs, "Error updating Port $port Position $position status to $position_status";
					push @logs, "Email             : Sending cycle finished alert for $description";
					my $email_result = _send_cycle_finished_email($DATABASE, $device_id, $host_id, $description, $base_cycle_type_id);
					push @logs, "Email Result      : $email_result";
				}
				elsif ($c_update_host_status == 0) {
					push @logs, "Race condition avoided: host status NOT updated to '$position_status' and NO cycle finished alert sent for host_id $host_id, port $port, position $position";
				}
				else {
					push @logs, "Updated Port $port Position $position status to $position_status";
					push @logs, "Email             : Sending cycle finished alert for $description";
					my $email_result = _send_cycle_finished_email($DATABASE, $device_id, $host_id, $description, $base_cycle_type_id);
					push @logs, "Email Result      : $email_result";
				}

				# get the last_start_time as seconds since the epoch
				my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
				my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $last_start_str);
				my $last_start_ts = eval { timelocal($sec, $min, $hr, $dm, ($mon-1), $yr); };

				# get the current seconds since the epoch
				my $current_ts = time();

				# find the difference and round to the nearest minute
				my $cycle_et_orig = (($current_ts - $last_start_ts)/60);
				my $cycle_et = $cycle_et_orig<0?int($cycle_et_orig-0.5):int($cycle_et_orig+0.5);	#sprintf rounding not reliable for floating-point round arithmetic

				push @logs, "Last Start Time   : $last_start_str";
				push @logs, "Last Start TS     : $last_start_ts";
				push @logs, "Current TS        : $current_ts";
				push @logs, "Calculated Time   : $cycle_et Minutes (exact: $cycle_et_orig)";

				# do some sanity checks on the calculated time
				if($cycle_et < 15 || $cycle_et > 120)
				{
					push @logs, "Cycle Sanity Check: $cycle_et looks goofy, ignoring it";
				}
				else
				{
					# TODO: this should probably use the calculated run time from above, and query for the minimum
					# tran_line_item_id where the line item timestamp is greater than (current time - run minutes)
					# because top-offs could screw us up
					# now lookup the last recorded transaction to get what kinda cycle this was
					my $get_cycle_type_ref = $DATABASE->query(	#TODO: should order by date instead of tran_line_item_id
						query => q{
							select * from (
								select types.tran_line_item_type_desc, types.tran_line_item_type_id
								from pss.tran_line_item_type types, pss.tran_line_item_recent recent
								where recent.host_id = ?
								and recent.tran_line_item_type_id = types.tran_line_item_type_id
								order by recent.tran_line_item_id desc
							) where rownum = 1
						},
						values => [ $host_id ]
					);

					if(not defined $get_cycle_type_ref->[0])
					{
						push @logs, "Cycle Type        : Not found for host_id $host_id";
					}
					else
					{
						my $cycle_desc = $get_cycle_type_ref->[0][0];
						my $cycle_type_id = $get_cycle_type_ref->[0][1];

						push @logs, "Cycle Type/Time   : Host $host_id, Type $cycle_desc, Type ID $cycle_type_id, Time $cycle_et";
						
						if($cycle_type_id =~ m/^$TLI_GROUP_NON_REGULAR_WASH_CYCLE_REGEX$/)
						{
							$cycle_type_id = TLI_REGULAR_WASH_CYCLE;
							push @logs, "Adjusting cycle estimate for Regular Wash Cycle...";
						}

						### determine whether or not we need to insert or update last cycle time
						my $last_tran_cycle_time_ref = $DATABASE->select(
							table			=> 'est_tran_complete_time',
							select_columns	=> 'tran_line_item_type_id, last_tran_complete_minut',
							where_columns	=> ['tran_line_item_type_id = ?' ,'host_id = ?'],
							where_values	=> [ $cycle_type_id, $host_id ]
						);

						if (not defined $last_tran_cycle_time_ref->[0])	#insert
						{
							push @logs, "Insert Cycle Est. : $cycle_type_id, $host_id, $cycle_et";
							$DATABASE->insert(
								table			=> 'est_tran_complete_time'
								,insert_columns	=> 'tran_line_item_type_id, host_id, last_tran_complete_minut'
								,insert_values	=> [$cycle_type_id, $host_id, $cycle_et]
							);
						}
						else	#update
						{
							push @logs, "Update Cycle Est. : $cycle_type_id, $host_id, $cycle_et";
							$DATABASE->update(
								table			=> 'est_tran_complete_time'
								,update_columns	=> 'last_tran_complete_minut'
								,update_values	=> [ $cycle_et ]
								,where_columns	=> [ 'tran_line_item_type_id = ?', 'host_id = ?' ]
								,where_values	=> [ $cycle_type_id, $host_id ]
							);
						}
					}
				}
			}
			elsif($position_status eq EQUIP_STATUS_IN_CYCLE_FIRST && $host_status_flag ne EQUIP_STATUS_IN_CYCLE_FIRST)	# initial cycle started
			{
				### if this was part of a full room status, current state is cycle complete, and this is Speed Queen host, return immediately avoid race ECCS condition ###
				if ($is_full_room_status && $host_status_flag eq EQUIP_STATUS_IDLE_NOT_AVAILABLE && $host_type_cd =~ /^$EQUIP_GROUP_SPEEDQUEEN_REGEX$/o)
				{
					push @logs, "Race condition avoided: host status NOT updated to '$position_status' for host_id $host_id, port $port, position $position";
					return \@logs;
				}

				push @logs, "Cycle Started!    : $description";

				### update the host status (also acts as semaphore control -- prevent potential race condition that would leave status in a sequentially-invalid state) ###
				my $c_update_host_status = $DATABASE->update(
					table            => 'host',
					update_columns   => 'host_status_cd',
					update_values    => [$position_status],
					where_columns    => ['host_id = ?', 'host_status_cd != ?'],
					where_values     => [$host_id, EQUIP_STATUS_IN_CYCLE_FIRST]
				);
				if (!defined $c_update_host_status) {
					push @logs, "Error updating Port $port Position $position status to $position_status";
				}
				elsif ($c_update_host_status == 0) {
					push @logs, "Race condition avoided: host status NOT updated to '$position_status' for host_id $host_id, port $port, position $position";
				}
				else {
					push @logs, "Updated Port $port Position $position status to $position_status";
				}

				### check if we are in free-vend or regular mode
				my $host_cycle_price = _host_base_cycle_price($DATABASE, $host_id, $base_cycle_type_id);
				if (not defined $host_cycle_price)	#error
				{
					push @logs, "Unable to determine free-vend mode on/off flag for device $device_id host $host_id. Skipping estimated time setting for this host.";
				}
				elsif ($host_cycle_price == 0)	#free-vend mode
				{
					### if in new cycle, determine the appropriate completion minutes and set it

					# if we have an average for this host, use it as the est completion time
					# if this is a new host, use the reported time
					# if we don't have either, set est completion minutes to zero

					my $host_est_complete_minut;

					# get the average we have been keeping
					my $host_avg_time_ref = $DATABASE->select(
						table			=> 'est_tran_complete_time',
						select_columns	=> 'avg_tran_complete_minut',
						where_columns	=> ['host_id = ?', 'tran_line_item_type_id = ?'],
						where_values	=> [ $host_id, $base_cycle_type_id]);

					$host_est_complete_minut = $host_avg_time_ref->[0][0];

					if(not defined $host_est_complete_minut)
					{
						# we failed to get an average, now try to get the reported time
						my $host_reported_time_ref = $DATABASE->select(
							table			=> 'host_setting',
							select_columns	=> 'host_setting_value',
							where_columns	=> ['host_id = ?',
								"host_setting_parameter = (
								 select 	tran_line_item_type_desc || ' Time'
								 from	tran_line_item_type
								 where 	tran_line_item_type_id = ?)" ],
							where_values	=> [ $host_id, $base_cycle_type_id ]);

						$host_est_complete_minut = $host_reported_time_ref->[0][0];
					}

					if(not defined $host_est_complete_minut)
					{
						$host_est_complete_minut = 0;
					}

					### set the estimated time
					$DATABASE->update(	table            => 'host',
										update_columns   => 'HOST_EST_COMPLETE_MINUT',
										update_values    => [$host_est_complete_minut],
										where_columns    => ['host_id = ?'],
										where_values     => [$host_id] );
					push @logs, "Setting estimated minutes to $host_est_complete_minut for host $host_id port $port";

					if($host_type_cd =~ m/^$EQUIP_GROUP_SPEEDQUEEN_REGEX$/o && $host_est_complete_minut > 0)
					{
						# this is speed queen equipment which does not send a cycle complete status change notif
						# we need to schedule a simulated cycle change notif here

						# don't need to lookup start time since start time is right now, or relatively close to it
						my $start_ts = localtime();
						my $schedule_simulated_result = _schedule_simulated_cycle_complete($host_id, $start_ts, $host_est_complete_minut);
						push @logs, "Speed Queen Equip : (free-vend) $schedule_simulated_result";
					}
				}
				else	#regular mode
				{
					# if we started a new cycle, zero out the completion minutes.  it should get populated
					# when we receive the batch
					# but what if we already received the batch?  then the status should be transaction in
					# progress, so check for that and don't zero out the time if it is
					if($host_status_flag ne EQUIP_STATUS_TRANSACTION_IN_PROGRESS)
					{
						$DATABASE->update(	table            => 'host',
											update_columns   => 'HOST_EST_COMPLETE_MINUT',
											update_values    => [0],
											where_columns    => ['host_id = ?'],
											where_values     => [$host_id] );
						push @logs, "Setting estimated minutes to 0 for host $host_id port $port";
					}
				}
			}
			else	#just update the status
			{
				### update the host status ###
				my $c_update_host_status = $DATABASE->update(
					table            => 'host',
					update_columns   => 'host_status_cd',
					update_values    => [$position_status],
					where_columns    => ['host_id = ?'],
					where_values     => [$host_id]
				);
				if (!defined $c_update_host_status) {
					push @logs, "Error updating Port $port Position $position status to $position_status";
				}
				else {
					push @logs, "Updated Port $port Position $position status to $position_status";
				}
			}
		}
	}
	else
	{
		push @logs, "No host record found for device_id $device_id, port $port, position $position";
	}
	return (\@logs);
}

sub _room_status ($$$$$)
{
	# return logs for info and errors for logging
	my (@logs);

	# handle to rerix database
	my ($DATABASE, $is_full_room_status, $command_hashref, $starting_port, $bytes) = @_;

	my $msg_no = $command_hashref->{msg_no};
	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id};
	my $device_id = $command_hashref->{device_id};
	my $SSN = $command_hashref->{SSN};

	for(my $portIndex = 0; $portIndex<length($bytes); $portIndex++)
	{
		my $port = ($portIndex+$starting_port);
		my $status = unpack("H*", substr($bytes, $portIndex, 1));

		if($status eq '04')
		{
			push @logs, "Skipping          : Port $port is empty";
			next;
		}

		# hack
		my $temp_status = $status;
		$status = substr($temp_status,1,1) . substr($temp_status,0,1);

		for(my $position = 0; $position <= 1; $position++)
		{
			my $position_status = substr($status, $position, 1);
			$position_status =~ s/\s+$//;

			my $log;
			my $host_id;
			my $get_host_ref = $DATABASE->select(
				table			=> 'host',
				select_columns	=> "host.host_id",
				where_columns	=> [ 'host.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
				where_values	=> [ $device_id, $port, $position ]
			);

			if(defined $get_host_ref->[0])
			{
				$log = _room_status_update_host($DATABASE, $is_full_room_status, $device_id, $get_host_ref->[0]->[0], $port, $position, $position_status);
			}
			else
			{
				if($position_status ne '0')
				{
					push @logs, @{_request_room_layout($DATABASE, $machine_id)};
					ReRix::Utils::send_alert("ESuds: room_status: No host record found for device_id $device_id, port $port, position $position", $command_hashref);
					push @logs, "No host record found for device_id $device_id, port $port, position $position";
					return(\@logs);
				}
			}
			push @logs, @{$log} if defined $log && ref($log) eq 'ARRAY';
		}
	}

	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);

	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);
}

sub authorize
{
	my @logs;
	my ($DATABASE, $command_hashref, $trans_id, $card_type, $auth_amt, $pin_data, $auth_data) = @_;

	my $machine_id = $command_hashref->{machine_id};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id};
	my $SSN = $command_hashref->{SSN};
	my $device_type = $command_hashref->{device_type};
	my $response_no = $command_hashref->{response_no};

	if($device_type ne '5')
	{
		push @logs, "Huh!?  ESuds::authorize received a request to authorize a non-esuds device!";
		ReRix::Utils::send_alert("ESuds: ESuds::authorize received a request to authorize a non-esuds device!\n\n$device_id", $command_hashref);
		return(\@logs);
	}

	push @logs, "Authorize         : Processing ESuds Authorization Request!";
	push @logs, "Trans ID          : $trans_id";
	push @logs, "Card Type         : $card_type";
	push @logs, "Auth Data         : ".mask_credit_card($auth_data);
	push @logs, "Pin Data          : $pin_data";
	push @logs, "Amount            : $auth_amt";

	# NOTE:  We need to adjust this timestamp to the timezone where this device is located so
	# it shows up correctly on the reports

	my $time_zone;

	# Use the device_id as a rerefence to lookup the time zone
	my $lookup_tz_ref =	$DATABASE->select(
							table          => 'location.location, pos',
							select_columns => 'location.location_time_zone_cd',
							where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
							where_values   => [$device_id] );

	$time_zone = $lookup_tz_ref->[0][0];

	if(not defined $time_zone)
	{
		$time_zone = 'EST';
		push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
	}
	else
	{
		push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
	}

	my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
	$Month = $Month+1;
    $Year  = $Year+1900;

	my $start_ts = "$Month/$Day/$Year $Hour:$Minute:$Second";
	push @logs, "Transaction Time  : $start_ts";

	my $posh = USAT::POS->new($DATABASE);
	my $approved_flag;
	my $return_code;
	my $error_message;
	my $acct_entry_method = $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__cash
		? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__manual
		: $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__credit_card || $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__special_card
			? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__magnetic_stripe
			: $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__rfid_credit_card || $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__rfid_special_card
				? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__contactless
				: USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__unspecified;
	my $approved;
	my $approved_amt;
	my $authority_tran_cd;
	my $authority_auth_ref_cd;
	my $db_err_str;
	my $db_tran_id;

	# call a stored proc to authorize the tran
	$posh->authorize(
		$auth_data
		,$pin_data
		,$device_id
		,$trans_id
		,$card_type
		,$auth_amt
		,$start_ts
		,'N'
		,$auth_data
		,$acct_entry_method
		,\$approved_flag
		,\$approved_amt
		,\$authority_tran_cd
		,\$authority_auth_ref_cd
		,\$db_tran_id
		,\$return_code
		,\$error_message
	);

	push @logs, "POS Processor     : authorize(".mask_credit_card($auth_data).", $pin_data, $device_id, $trans_id, $card_type, $auth_amt, $start_ts, N, ".mask_credit_card($auth_data).", $acct_entry_method, ...)";

	push @logs, "Return Code       : $return_code";
	push @logs, "Error Message     : $error_message" if defined $error_message;
	push @logs, "Approved Flag     : $approved_flag";

	if ($approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__success)	#success
	{
		$approved = 1;
	}
	elsif ($approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__success_conditional
		&& $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__insufficient_balance)	#conditionally approved (due to insufficient funds)
	{
		$approved = 2;
		push @logs, "POS Processor     : authorize conditional success ($error_message)";
	}
	elsif ($approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__decline
		|| $approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__decline_permanent)	#decline or perm decline (currently mean the same in supported client responses)
	{
		$approved = 0;
		if ($return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__invalid_pin_entry)
		{
			$approved = 4;
			push @logs, "POS Processor     : invalid pin detected ($error_message)";
		}
		else
		{
			$approved_amt = $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__insufficient_balance ? 0 : undef;	#decline $0 amt ==> insuf funds; decline undef amt, ==> generic decline
		}
	}
	elsif ($approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__failure)	#failure
	{
		$approved = 3;
	}
	else
	{
		push @logs, "POS Processor     : authorize failed! return_code = $return_code ($error_message)!";
		ReRix::Utils::send_alert("ESuds: POS Processor     : authorize failed!\n$error_message", $command_hashref);
		$approved = 0;
	}

	return(\@logs, $approved, $approved_amt);
}

sub esuds_network_auth_batch_intended
{
	my ($DATABASE, $command_hashref) = @_;
	return _esuds_network_auth_batch($DATABASE, $command_hashref, 'I');
}
sub esuds_network_auth_batch_actual
{
	my ($DATABASE, $command_hashref) = @_;
	return _esuds_network_auth_batch($DATABASE, $command_hashref, 'A');
}

sub _esuds_network_auth_batch ($$$)
{
	my (@logs);

	# handle to rerix database
	my ($DATABASE, $command_hashref, $tran_batch_type_cd) = @_;

	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no};
	my $machine_id = $command_hashref->{machine_id};
	my $device_id = $command_hashref->{device_id};
	my $SSN = $command_hashref->{SSN};
	my $device_type = $command_hashref->{device_type};

	# card_number should be the student ID

	my $trans_id = unpack("N", substr($message,2,4));
	my $num_line_items = ord(substr($message,6,1));
	my $tran_result = 'Q';

	if($num_line_items == 0)
	{
		$tran_result = 'C';
	}

	if($device_type ne '5')
	{
		push @logs, "Huh!?  ESuds::esuds_network_auth_batch received a batch for a non-esuds device!";
		ReRix::Utils::send_alert("ESuds: Huh!?  ESuds::esuds_network_auth_batch received a batch for a non-esuds device!", $command_hashref);
		return(\@logs);
	}

	push @logs, "Trans ID          : $trans_id";
	push @logs, "Transaction Result: $tran_result";
	push @logs, "Num Line Items    : $num_line_items";

	my $total = 0;
	my $item_start_pos = 7;
	my $data = substr($message, $item_start_pos);

	# Lookup and use the device_id of the device as it existed at the time of the transaction (most recent trans only to make sure unique in case of multiple transactions in less that 1 second)
	my $get_device_id_stmt = $DATABASE->query(
		query	=> q{
			SELECT device_id
			FROM (
				SELECT d.device_id
				FROM tran t, auth a, pos_pta pp, pos p, device d
				WHERE t.tran_id = a.tran_id
				AND pp.pos_id = p.pos_id
				AND t.pos_pta_id = pp.pos_pta_id
				AND p.device_id = d.device_id
				AND d.device_name = ?
				AND t.tran_device_tran_cd = ?
				ORDER BY a.auth_ts DESC
			) x
			WHERE ROWNUM = 1
		},
		values 	=> [$machine_id, $trans_id]
	);

	unless (defined $get_device_id_stmt->[0]->[0])
	{
		push @logs, "ESuds: Failed to lookup device_id for $machine_id and $trans_id";
		push @logs, _log_exception_old_client_compatible($message, 'esuds_network_auth_batch', '1', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
		ReRix::Utils::send_alert("ESuds: Failed to lookup device_id for $machine_id and $trans_id", $command_hashref);
		return(\@logs);
	}
	$device_id = $get_device_id_stmt->[0]->[0];
#	$get_device_id_stmt->finish();

#	$db_err_str = undef;

	push @logs, "Stored Device ID  : $device_id";

	# NOTE:  We need to adjust this timestamp to the timezone where this device is located so
	# it shows up correctly on the reports

	my $time_zone;

	# Use the device_id as a rerefence to lookup the time zone
	my $lookup_tz_ref =	$DATABASE->select(
							table          => 'location.location, pos',
							select_columns => 'location.location_time_zone_cd',
							where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
							where_values   => [$device_id] );

	$time_zone = $lookup_tz_ref->[0][0];
	if(not defined $time_zone)
	{
		$time_zone = 'EST';
		push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
	}
	else
	{
		push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
	}

	my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
	$Month = $Month+1;
    $Year  = $Year+1900;

    my $start_ts = "$Month/$Day/$Year $Hour:$Minute:$Second";
	push @logs, "Device Tran Time  : $start_ts";

	my @line_items;
	LINE_ITEM_LOOP: for(my $item_num=0; $item_num<$num_line_items; $item_num++)
	{
		push @logs, "Line Item Num     : $item_num ------------";

		my $chunk = substr($data, ($item_num*6), 6);
		push @logs, "Chunk             : " . unpack("H*", $chunk);
		my $port_num = ord(substr($chunk, 0, 1));
		my $top_bottom = unpack("a", substr($chunk, 1, 1));
		my $item = ord(substr($chunk, 2, 1));
		my $qty = ord(substr($chunk, 3, 1));
		my $amt = unpack("H4", substr($chunk, 4, 2));

		my $position = 0;
		if($top_bottom eq EQUIP_POSITION_TOP)
		{
			$position = 1;
		}
		elsif($top_bottom eq EQUIP_POSITION_UNKNOWN)
		{
			push @logs, "Port $port_num = unknown position!";

			my $lookup_host_for_unknown_ref = $DATABASE->select(
							table			=> 'host',
							select_columns	=> 'host.host_position_num',
							where_columns	=> [ 'host.device_id = ?', 'host.host_port_num = ?', ],
							where_values	=> [ $device_id, $port_num ],
							order			=> 'host.host_position_num ASC');

			if(not defined $lookup_host_for_unknown_ref->[0])
			{
				push @logs, "Failed to find a host for $port_num and unknown position, using bottom!";
				$position = 0;
			}
			else
			{
				$position = $lookup_host_for_unknown_ref->[0][0];
			}
		}

		my $cycle_desc = _get_item_details($DATABASE, $item);
		my $cycle_amt_modifier = 0.01;
		$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
		$amt = sprintf("%.02f", ($amt * $cycle_amt_modifier));

		$total += $amt;

		# construct a description; should look like "Standard Cycle, Dryer on Port 7"
		if(not defined $cycle_desc)
		{
			push @logs, "Failed to lookup tran_line_item_type for item_id $item";
			push @logs, _log_exception_old_client_compatible($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
			ReRix::Utils::send_alert("ESuds: Failed to lookup tran_line_item_type for item_id $item", $command_hashref);
			return(\@logs);
		}

		my $description = $cycle_desc . ', ';
		my $lookup_host_type_ref = $DATABASE->select(
							table			=> 'device, host, host_type, device_type_host_type, host_type_host_group_type, host_group_type',
							select_columns	=> 'host_type.host_type_desc, device_type_host_type.device_type_host_type_cd, host.host_id, host.host_label_cd, host.host_status_cd, host_group_type.host_group_type_name',
							where_columns	=> [ 'device.device_id = host.device_id', 'host.host_type_id = host_type.host_type_id ', 'host_type.host_type_id = device_type_host_type.host_type_id', 'device_type_host_type.device_type_id = device.device_type_id', 'host_type.host_type_id = host_type_host_group_type.host_type_id', 'host_type_host_group_type.host_group_type_id = host_group_type.host_group_type_id', 'device.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
							where_values	=> [ $device_id, $port_num, $position ] );

		if(not defined $lookup_host_type_ref->[0])
		{
			push @logs, "Failed to find host_type_desc for device $device_id, Port $port_num, Position $position";
			$lookup_host_type_ref = $DATABASE->select(
							table			=> 'device, host, host_type, device_type_host_type, host_type_host_group_type, host_group_type',
							select_columns	=> 'host_type.host_type_desc, device_type_host_type.device_type_host_type_cd, host.host_id, host.host_label_cd, host.host_status_cd, host_group_type.host_group_type_name, host.host_position_num',
							where_columns	=> [ 'device.device_id = host.device_id', 'host.host_type_id = host_type.host_type_id ', 'host_type.host_type_id = device_type_host_type.host_type_id', 'device_type_host_type.device_type_id = device.device_type_id', 'host_type.host_type_id = host_type_host_group_type.host_type_id', 'host_type_host_group_type.host_group_type_id = host_group_type.host_group_type_id', 'device.device_id = ?', 'host.host_port_num = ?' ],
							where_values	=> [ $device_id, $port_num ] );
			if(not defined $lookup_host_type_ref->[0])
			{
				push @logs, "Failed to find host_type_desc for device $device_id, Port $port_num, Any Position";
				push @logs, _log_exception_old_client_compatible($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
				ReRix::Utils::send_alert("ESuds: Failed to find host_type_desc for device $device_id, Port $port_num, Position $position", $command_hashref);
				return(\@logs);
			}
			else
			{
				my $device_type_host_type_cd = $lookup_host_type_ref->[0]->[1];
				my $host_position_num = $lookup_host_type_ref->[0]->[6];
				if ($device_type_host_type_cd =~ m/^$EQUIP_GROUP_STACKED_GEN1_REGEX$/)
				{
					push @logs, "Altered position for Gen1 device $device_id, Port $port_num from Position $position to $host_position_num";
					$position = $host_position_num;
				}
				else
				{
					push @logs, "Invalid position for device $device_id, Port $port_num, Position $host_position_num: Not a Gen1 device: Can't correct position";
					push @logs, _log_exception_old_client_compatible($message, 'esuds_network_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
					ReRix::Utils::send_alert("ESuds: Failed to find host_type_desc for device $device_id, Port $port_num, Position $position", $command_hashref);
					return(\@logs);
				}
			}
		}

		my $host_type_name = $lookup_host_type_ref->[0][0];
		my $host_type_cd = $lookup_host_type_ref->[0][1];
		my $host_id = $lookup_host_type_ref->[0][2];
		my $host_label = $lookup_host_type_ref->[0][3];
		my $host_status_cd = $lookup_host_type_ref->[0][4];
		my $host_group_type_name = $lookup_host_type_ref->[0][5];

		push @logs, "HostID/Type/Label : $host_id/$host_type_name/$host_label";

		if($host_type_cd =~ m/^$EQUIP_GROUP_STACKED_REGEX$/o)
		{
			if($top_bottom eq EQUIP_POSITION_TOP)
			{
				$description = $description . "Top ";
			}
			elsif($top_bottom eq EQUIP_POSITION_BOTTOM)
			{
				$description = $description . "Bottom ";
			}
		}
		$description = $description . $host_group_type_name . " " . $host_label;

		push @logs, "Item Port/Pos     : $port_num/$position";
		push @logs, "Item ID           : $item";
		push @logs, "Qty/Amt           : $qty/$amt";
		push @logs, "Top/Bottom        : $top_bottom";
		push @logs, "Description       : $description";

		push(@line_items, [$item_num, $port_num, $position, $amt, $item, $top_bottom, $description, $host_id, $qty]);

		### adjust time estimates if this is actual batch ###
		if ($tran_batch_type_cd eq 'A')
		{
			if($item =~ m/^$TLI_GROUP_NON_REGULAR_WASH_CYCLE_REGEX$/)
			{
				push @logs, "Skipping cycle-time estimate set/adjust for non-regular wash cycle";
				last LINE_ITEM_LOOP;
			}
		
			### check if we are in free-vend or regular mode
			my $base_cycle_type_id = _get_host_base_cycle_type_id($host_type_cd, $position);
			my $host_cycle_price = _host_base_cycle_price($DATABASE, $host_id, $base_cycle_type_id);
			if (not defined $host_cycle_price)	#error
			{
				push @logs, "Unable to determine free-vend mode on/off flag for device $device_id host $host_id line item $item. Skipping cycle-time estimate set/adjust for this line item transaction.";
			}
			elsif ($host_cycle_price > 0)	#regular mode
			{
				# TODO: Adjust the hosts's cycle completion minutes based on the incoming cycle and the
				# cycle time stored in the host_setting table, or if this host_setting value does not exist,
				# try using the avg cycle time or last cycle time, whichever is most accurate here.

				my $host_cycle_time;
				my $item_desc = _get_item_details($DATABASE, $item);
				my $lookup_cycle_time_ref = $DATABASE->select(
									table			=> 'host_setting',
									select_columns	=> 'host_setting_value',
									where_columns	=> ['host_id = ?', 'HOST_SETTING_PARAMETER = ?'],
									where_values	=> [$host_id, "$item_desc Time"]);

				if(defined $lookup_cycle_time_ref->[0])	#use cycle time parameter as reported by host
				{
					$host_cycle_time = $lookup_cycle_time_ref->[0][0];
					push @logs, "Cycle Time Lookup : Found REPORTED time $host_cycle_time for host $host_id item $item_desc";
				}
				else	#try using avg instead
				{
					# get the average we have been keeping
					my $host_avg_time_ref = $DATABASE->select(
						table			=> 'est_tran_complete_time',
						select_columns	=> 'avg_tran_complete_minut',
						where_columns	=> ['host_id = ?', 'tran_line_item_type_id = ?'],
						where_values	=> [ $host_id, $item]);

					if(defined $host_avg_time_ref->[0])
					{
						$host_cycle_time = $host_avg_time_ref->[0][0];
						push @logs, "Cycle Time Lookup : Found ESTIMATED time $host_cycle_time for host $host_id item $item_desc";
					}
				}

				if (defined $host_cycle_time)	#fine in the case of undefined, as 0 minutes is set for the estimate on cycle start for regular cycles
				{
					my $cycle_time = $host_cycle_time;
					my $total_cycle_time = ($cycle_time*$qty);
					if($item == TLI_TOP_OFF_DRY)
					{
						if($top_bottom ne EQUIP_POSITION_UNKNOWN)
						{
							# upgrade
							push @logs, "Cycle Time Adjust : Top-Off! Increasing cycle time by $total_cycle_time for host $host_id, $item_desc Time";
							my $result = $DATABASE->do(
								query => q{
									UPDATE host
									SET HOST_EST_COMPLETE_MINUT = (
										CASE
											WHEN (host_est_complete_minut + ?) > 99 THEN 99
											ELSE (host_est_complete_minut + ?)
										END
									)
									WHERE host_id = ?
								},
								values => [ $total_cycle_time, $total_cycle_time, $host_id ]
							);
							unless ($result)
							{
								push @logs, "Top-Off Failed    : ".$DATABASE->errstr;
							}
						}
						else
						{
							push @logs, "Cycle Time Adjust : Skipping adjust for unknown top-off position";
						}
					}
					else
					{
						# new cycle
						push @logs, "Cycle Time Adjust : Setting cycle time to $total_cycle_time for host $host_id, $item_desc Time";
						$DATABASE->update(	table            => 'host',
											update_columns   => 'HOST_EST_COMPLETE_MINUT',
											update_values    => [$total_cycle_time],
											where_columns    => ['host_id = ?'],
											where_values     => [$host_id]);

						if($host_type_cd =~ m/^$EQUIP_GROUP_SPEEDQUEEN_REGEX$/o)
						{
							# this is speed queen equipment which does not send a cycle complete status change notif
							# we need to schedule a simulated cycle change notif here

							my $get_start_time_ref = $DATABASE->select(
								table			=> 'host',
								select_columns	=> 'to_char(host.host_last_start_ts, \'ss:mi:hh24:dd:mm:yyyy:d:ddd\')',
								where_columns	=> ['host_id = ?'],
								where_values	=> [ $host_id]);

							if(defined $get_start_time_ref->[0])
							{
								my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
								my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $get_start_time_ref->[0]->[0]);
								my $start_ts = eval { timelocal($sec, $min, $hr, $dm, ($mon-1), $yr); };
								my $event_ts = $time > $start_ts ? $time : $start_ts;	#if (tran ts > last host start ts), use tran ts (as the batch was received before status update)

								my $schedule_simulated_result = _schedule_simulated_cycle_complete($host_id, $event_ts, $total_cycle_time);
								push @logs, "Speed Queen Equip : $schedule_simulated_result";
							}
							else
							{
								push @logs, "Speed Queen Equip : Failed to lookup last_start_ts for host $host_id";
							}
						}
					}
				}
				else
				{
					push @logs, "Cycle Time Lookup : Failed to find stored cycle time for host $host_id item $item_desc";
				}
			}
			#else we are in free-vend mode--not adjusting estimate here, at least for now....
		}
	}

	push @logs, "Total Sale        : $total";

	my $tran_return_code;
	my $error_message;
	my $db_tran_id;
	my $tran_state_cd_batch;

	push @logs, "Storing Tran      : $trans_id";

	# call a stored proc to store the tran
	my $posh = USAT::POS->new($DATABASE);
	my $tran_local_auth_yn_flag = 'N';
	$posh->queue_debit_summary(
		$tran_batch_type_cd
		,undef						#card magstripe data
		,undef						#card pin entry
		,$device_id
		,$trans_id
		,$tran_result
		,undef						#tran card type
		,undef						#trans total amt (always undef for network batch)
		,$tran_local_auth_yn_flag	#local auth flag
		,undef						#parsed card data--always undef for network batch
		,undef						#acct entry method--always undef for network batch
		,$start_ts					#start ts
		,$start_ts					#end ts
		,\$db_tran_id
		,\$tran_state_cd_batch
		,\$tran_return_code
		,\$error_message
	);
	push @logs, "POS Processor     : queue_debit_summary($tran_batch_type_cd, undef, undef, $device_id, $trans_id, $tran_result, undef, undef, $tran_local_auth_yn_flag, undef, undef, $start_ts, $start_ts, ...)";

	push @logs, "Return Code       : $tran_return_code";
	push @logs, "Error Message     : $error_message" if defined $error_message;
	push @logs, "DB Trans ID       : $db_tran_id";

	if($tran_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
	{
		push @logs, "POS Processor     : queue_debit_summary failed! return_code ne ".USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
		push @logs, _log_exception_old_client_compatible($message, 'queue_debit_summary', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
		ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed!\n$error_message", $command_hashref);
		return(\@logs);
	}

	if(not defined $db_tran_id)
	{
		push @logs, "POS Processor     : queue_debit_summary failed! db_tran_id is undefined";
		push @logs, _log_exception_old_client_compatible($message, 'queue_debit_summary', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
		ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed!\n$error_message", $command_hashref);
		return(\@logs);
	}

	#       0          1         2        3      4       5              6            7      8
	# [$item_num, $port_num, $position, $amt, $item, $top_bottom, $description, $host_id, $qty]

	foreach my $item_ref (@line_items)
	{
		my $item_return_code;

		push @logs, "Storing Line Item : " . $item_ref->[0] . " -----";
		# call a stored proc to store each line item
		$posh->queue_tran_add_line_item(
			$tran_batch_type_cd
			,$db_tran_id
			,$tran_state_cd_batch
			,$item_ref->[7]						#host id
			,($item_ref->[3]/$item_ref->[8])	#item amount
			,0									#item tax
			,$item_ref->[4]						#item
			,$item_ref->[6]						#description
			,$item_ref->[8]						#qty
			,undef								#position cd
			,\$item_return_code
			,\$error_message
		);
		push @logs, "POS Processor     : queue_tran_add_line_item($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $item_ref->[7], ".($item_ref->[3]/$item_ref->[8]).", 0, $item_ref->[4], $item_ref->[6], $item_ref->[8], undef)";
		push @logs, "Return Code       : $item_return_code";
		push @logs, "Error Message     : $error_message" if defined $error_message;

		if($item_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
		{
			push @logs, "POS Processor     : queue_tran_add_line_item failed! ";
			push @logs, _log_exception_old_client_compatible($message, 'queue_tran_add_line_item', 16, $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
			ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_add_line_item failed!\n$error_message", $command_hashref);
			return(\@logs);
		}
	}

	### complete batch ###
	my $batch_complete_return_code;
	my $batch_complete_error_message;
	$posh->queue_tran_finalize(
		$tran_batch_type_cd
		,$db_tran_id
		,undef			#tran total amount (undef == don't validate)
		,\$tran_state_cd_batch
		,\$batch_complete_return_code
		,\$batch_complete_error_message
	);
	push @logs, "POS Processor     : queue_tran_finalize($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, undef)";
	push @logs, "Return Code       : $batch_complete_return_code";
	push @logs, "Error Message     : $batch_complete_error_message" if defined $batch_complete_error_message;
	if ($batch_complete_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
	{
		push @logs, "POS Processor     : queue_tran_finalize failed! ";
		push @logs, _log_exception_old_client_compatible($message, 'queue_tran_finalize', '16', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
		ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_finalize failed!\n$batch_complete_error_message", $command_hashref);
		return(\@logs);
	}

	### send response (actual batch message) ###
	if ($tran_batch_type_cd eq 'A')
	{
		my $response = pack("CH2NC", $response_no, '71', $trans_id, 1);
		push @logs, "Response          : " . unpack("H*", $response);

		ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	}

	### handle (prepare or execute) settlement ###
	if ($tran_batch_type_cd eq 'I')
	{
		my $handle_debit_intended_return_code;
		my $handle_debit_intended_error_message;
		$posh->handle_debit_intended(
			$tran_batch_type_cd
			,$db_tran_id
			,$tran_state_cd_batch
			,undef			#tran total amount (undef == don't validate)
			,$tran_local_auth_yn_flag
			,\$handle_debit_intended_return_code
			,\$handle_debit_intended_error_message
		);
		push @logs, "POS Processor     : handle_debit_intended($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, undef, $tran_local_auth_yn_flag)";
		push @logs, "Return Code       : $handle_debit_intended_return_code";
		push @logs, "Error Message     : $handle_debit_intended_error_message" if defined $handle_debit_intended_error_message;
		if ($handle_debit_intended_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
		{
			push @logs, "POS Processor     : handle_debit_intended failed! ";
			my $tran_response = 2;
			if ($handle_debit_intended_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__insufficient_balance
				|| $handle_debit_intended_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__auth_handler_request_denied
				|| $handle_debit_intended_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__auth_handler_error) {
				$tran_response = 0;
			}
			push @logs, _log_exception($message, 'handle_debit_intended', '16', $response_no, $trans_id, $machine_id, $DATABASE, $tran_response, $command_hashref);
			ReRix::Utils::send_alert("ESuds: POS Processor     : handle_debit_intended failed!\n$handle_debit_intended_error_message", $command_hashref);
			return(\@logs);
		}

		### send response (intended batch message) ###
		my $response = pack("CH2NC", $response_no, '71', $trans_id, 1);
		push @logs, "Response          : " . unpack("H*", $response);

		ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	}

	return(\@logs);
}

sub esuds_local_auth_batch
{
	# return logs for info and errors for logging
	my (@logs);

	# handle to rerix database
	my ($DATABASE, $command_hashref) = @_;
	my $tran_batch_type_cd = 'A';

	my $response_no = $command_hashref->{response_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $msg_no = $command_hashref->{msg_no};
	my $machine_id = $command_hashref->{machine_id};
	my $device_id = $command_hashref->{device_id};
	my $SSN = $command_hashref->{SSN};
	my $device_type = $command_hashref->{device_type};

	my $trans_id = unpack("N", substr($message,2,4));
	my $timestamp = unpack("N", substr($message,6,4));
	my $tran_type = unpack("a", substr($message,10,1));
	my $card_length = ord(substr($message,11,1));
	my $auth_data = unpack("a*", substr($message,12,$card_length));
	my $num_line_items = ord(substr($message,(12+$card_length),1));

	my $item_start_pos = (12+$card_length+1);

	my $tran_result = 'Q';

	if($num_line_items == 0)
	{
		$tran_result = 'C';
	}

	if($device_type ne '5')
	{
		push @logs, "Huh!?  ESuds::esuds_local_auth_batch received a batch for a non-esuds device!";
		ReRix::Utils::send_alert("ESuds: Huh!?  ESuds::esuds_local_auth_batch received a batch for a non-esuds device!", $command_hashref);
		return(\@logs);
	}

	push @logs, "Trans ID          : $trans_id";
	push @logs, "Num Line Items    : $num_line_items";
	push @logs, "Auth Data         : ".mask_credit_card($auth_data);

	if($tran_type eq 'M')
	{
		$auth_data = undef;
		push @logs, "Cash Transaction! : Setting card number to undef";
	}

	# BIG FAT NOTE:   Since the room controller sets it's clock using an adjusted time, we don't
	# need to adjust this timestamp like we do for a network auth

	my ($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);
	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	my $time = sprintf("%02d:%02d:%02d", $hour, $min, $sec);
	my $date_time = "$date $time";
	push @logs, "Transaction Time  : $date_time";

	# Lookup and use the device_id of the device as it existed at the time of the transaction (in case port layout changed & transactions occured while network was down)
	my $get_device_id_stmt = $DATABASE->query(
		query	=> q{
			SELECT device_id FROM (
				SELECT d.device_id
				FROM pos p, device d
				WHERE p.device_id = d.device_id
				AND d.device_name = ?
				AND p.pos_activation_ts < TO_DATE(?, 'MM/DD/YYYY HH24:MI:SS')
				ORDER BY p.pos_activation_ts DESC
			)
			WHERE ROWNUM = 1
		},
		values 	=> [$machine_id, $date_time]
	);

	unless (defined $get_device_id_stmt->[0]->[0])
	{
		push @logs, "ESuds: Failed to lookup device_id for $machine_id and $trans_id";
		push @logs, _log_exception_old_client_compatible($message, 'esuds_network_auth_batch', '1', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
		ReRix::Utils::send_alert("ESuds: Failed to lookup device_id for $machine_id and $trans_id", $command_hashref);
		return(\@logs);
	}
	$device_id = $get_device_id_stmt->[0]->[0];

	push @logs, "Stored Device ID  : $device_id";

	my $total = 0;
	my $data = substr($message, $item_start_pos);

	my $posh = USAT::POS->new($DATABASE);

	my @line_items;
	LINE_ITEM_LOOP: for(my $item_num=0; $item_num<$num_line_items; $item_num++)
	{
		push @logs, "Line Item Num     : $item_num ------------";

		my $chunk = substr($data, ($item_num*6), 6);
		push @logs, "Chunk             : " . unpack("H*", $chunk);
		my $port_num = ord(substr($chunk, 0, 1));
		my $top_bottom = unpack("a", substr($chunk, 1, 1));
		my $item = ord(substr($chunk, 2, 1));
		my $qty = ord(substr($chunk, 3, 1));
		my $amt = unpack("H4", substr($chunk, 4, 2));

		my $position = 0;
		if($top_bottom eq EQUIP_POSITION_TOP)
		{
			$position = 1;
		}
		elsif($top_bottom eq EQUIP_POSITION_UNKNOWN)
		{
			push @logs, "Port $port_num = unknown position!";

			my $lookup_host_for_unknown_ref = $DATABASE->select(
							table			=> 'host',
							select_columns	=> 'host.host_position_num',
							where_columns	=> [ 'host.device_id = ?', 'host.host_port_num = ?', ],
							where_values	=> [ $device_id, $port_num ],
							order			=> 'host.host_position_num ASC');

			if(not defined $lookup_host_for_unknown_ref->[0])
			{
				push @logs, "Failed to find a host for $port_num and unknown position, using bottom!";
				$position = 0;
			}
			else
			{
				$position = $lookup_host_for_unknown_ref->[0][0];
			}
		}

		my $cycle_desc = _get_item_details($DATABASE, $item);
		my $cycle_amt_modifier = 0.01;
		$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
		$amt = sprintf("%.02f", ($amt * $cycle_amt_modifier));

		$total += $amt;

		# construct a description; should look like "Standard Cycle, Dryer on Port 7"
		if(not defined $cycle_desc)
		{
			push @logs, "Failed to lookup tran_line_item_type for item_id $item";
			push @logs, _log_exception_old_client_compatible($message, 'esuds_local_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
			ReRix::Utils::send_alert("ESuds: Failed to lookup tran_line_item_type for item_id $item", $command_hashref);
			return(\@logs);
		}

		my $description = $cycle_desc . ', ';
		my $lookup_host_type_ref = $DATABASE->select(
							table			=> 'device, host, host_type, device_type_host_type, host_type_host_group_type, host_group_type',
							select_columns	=> 'host_type.host_type_desc, device_type_host_type.device_type_host_type_cd, host.host_id, host.host_label_cd, host.host_status_cd, host_group_type.host_group_type_name',
							where_columns	=> [ 'device.device_id = host.device_id', 'host.host_type_id = host_type.host_type_id ', 'host_type.host_type_id = device_type_host_type.host_type_id', 'device_type_host_type.device_type_id = device.device_type_id', 'host_type.host_type_id = host_type_host_group_type.host_type_id', 'host_type_host_group_type.host_group_type_id = host_group_type.host_group_type_id', 'device.device_id = ?', 'host.host_port_num = ?', 'host.host_position_num = ?' ],
							where_values	=> [ $device_id, $port_num, $position ] );

		if(not defined $lookup_host_type_ref->[0])
		{
			push @logs, "Failed to find host_type_desc for device $device_id, Port $port_num, Position $position";
			$lookup_host_type_ref = $DATABASE->select(
							table			=> 'device, host, host_type, device_type_host_type, host_type_host_group_type, host_group_type',
							select_columns	=> 'host_type.host_type_desc, device_type_host_type.device_type_host_type_cd, host.host_id, host.host_label_cd, host.host_status_cd, host_group_type.host_group_type_name, host.host_position_num',
							where_columns	=> [ 'device.device_id = host.device_id', 'host.host_type_id = host_type.host_type_id ', 'host_type.host_type_id = device_type_host_type.host_type_id', 'device_type_host_type.device_type_id = device.device_type_id', 'host_type.host_type_id = host_type_host_group_type.host_type_id', 'host_type_host_group_type.host_group_type_id = host_group_type.host_group_type_id', 'device.device_id = ?', 'host.host_port_num = ?' ],
							where_values	=> [ $device_id, $port_num ] );
			if(not defined $lookup_host_type_ref->[0])
			{
				push @logs, "Failed to find host_type_desc for device $device_id, Port $port_num, Any Position";
				push @logs, _log_exception_old_client_compatible($message, 'esuds_local_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
				ReRix::Utils::send_alert("ESuds: Failed to find host_type_desc for device $device_id, Port $port_num, Position $position", $command_hashref);
				return(\@logs);
			}
			else
			{
				my $device_type_host_type_cd = $lookup_host_type_ref->[0]->[1];
				my $host_position_num = $lookup_host_type_ref->[0]->[6];
				if ($device_type_host_type_cd =~ m/^$EQUIP_GROUP_STACKED_GEN1_REGEX$/)
				{
					push @logs, "Altered position for Gen1 device $device_id, Port $port_num from Position $position to $host_position_num";
					$position = $host_position_num;
				}
				else
				{
					push @logs, "Invalid position for device $device_id, Port $port_num, Position $host_position_num: Not a Gen1 device: Can't correct position";
					push @logs, _log_exception_old_client_compatible($message, 'esuds_local_auth_batch', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
					ReRix::Utils::send_alert("ESuds: Failed to find host_type_desc for device $device_id, Port $port_num, Position $position", $command_hashref);
					return(\@logs);
				}
			}
		}

		my $host_type_name = $lookup_host_type_ref->[0][0];
		my $host_type_cd = $lookup_host_type_ref->[0][1];
		my $host_id = $lookup_host_type_ref->[0][2];
		my $host_label = $lookup_host_type_ref->[0][3];
		my $host_status_cd = $lookup_host_type_ref->[0][4];
		my $host_group_type_name = $lookup_host_type_ref->[0][5];

		push @logs, "HostID/Type/Label : $host_id/$host_type_name/$host_label";

		if($host_type_cd =~ m/^$EQUIP_GROUP_STACKED_REGEX$/o)
		{
			if($top_bottom eq EQUIP_POSITION_TOP)
			{
				$description = $description . "Top ";
			}
			elsif($top_bottom eq EQUIP_POSITION_BOTTOM)
			{
				$description = $description . "Bottom ";
			}
		}
		$description = $description . $host_group_type_name . " " . $host_label;

		push @logs, "Item Port/Pos     : $port_num/$position";
		push @logs, "Item ID           : $item";
		push @logs, "Qty/Amt           : $qty/$amt";
		push @logs, "Top/Bottom        : $top_bottom";
		push @logs, "Description       : $description";

		push(@line_items, [$item_num, $port_num, $position, $amt, $item, $top_bottom, $description, $host_id, $qty]);
		
		if($item =~ m/^$TLI_GROUP_NON_REGULAR_WASH_CYCLE_REGEX$/)
		{
			push @logs, "Skipping cycle-time estimate set/adjust for non-regular wash cycle";
			last LINE_ITEM_LOOP;
		}

		my $base_cycle_type_id = _get_host_base_cycle_type_id($host_type_cd, $position);

		### check if we are in free-vend or regular mode
		my $host_cycle_price = _host_base_cycle_price($DATABASE, $host_id, $base_cycle_type_id);
		if (not defined $host_cycle_price)	#error
		{
			push @logs, "Unable to determine free-vend mode on/off flag for device $device_id host $host_id line item $item. Skipping cycle-time estimate set/adjust for this line item transaction.";
		}
		elsif ($host_cycle_price > 0)	#regular mode
		{
			# TODO: Adjust the hosts's cycle completion minutes based on the incoming cycle and the
			# cycle time stored in the host_setting table, or if this host_setting value does not exist,
			# try using the avg cycle time or last cycle time, whichever is most accurate here.

			my $host_cycle_time;
			my $item_desc = _get_item_details($DATABASE, $item);
			my $lookup_cycle_time_ref = $DATABASE->select(
								table			=> 'host_setting',
								select_columns	=> 'host_setting_value',
								where_columns	=> ['host_id = ?', 'HOST_SETTING_PARAMETER = ?'],
								where_values	=> [$host_id, "$item_desc Time"]);

			if(defined $lookup_cycle_time_ref->[0])	#use cycle time parameter as reported by host
			{
				$host_cycle_time = $lookup_cycle_time_ref->[0][0];
				push @logs, "Cycle Time Lookup : Found REPORTED time $host_cycle_time for host $host_id item $item_desc";
			}
			else	#try using avg instead
			{
				# get the average we have been keeping
				my $host_avg_time_ref = $DATABASE->select(
					table			=> 'est_tran_complete_time',
					select_columns	=> 'avg_tran_complete_minut',
					where_columns	=> ['host_id = ?', 'tran_line_item_type_id = ?'],
					where_values	=> [ $host_id, $item]);

				if(defined $host_avg_time_ref->[0])
				{
					$host_cycle_time = $host_avg_time_ref->[0][0];
					push @logs, "Cycle Time Lookup : Found ESTIMATED time $host_cycle_time for host $host_id item $item_desc";
				}
			}

			if (defined $host_cycle_time)	#fine in the case of undefined, as 0 minutes is set for the estimate on cycle start for regular cycles
			{
				my $cycle_time = $host_cycle_time;
				my $total_cycle_time = ($cycle_time*$qty);
				if($item == TLI_TOP_OFF_DRY || ($host_status_cd == 2 && $tran_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__cash))	#upgrade or already in cycle (special case for cash-enabled dryers with no top-offs enabled)
				{
					if($top_bottom ne EQUIP_POSITION_UNKNOWN)
					{
						# upgrade
						push @logs, "Cycle Time Adjust : Top-Off! Increasing cycle time by $total_cycle_time for host $host_id, $item_desc Time";
						my $result = $DATABASE->do(
							query => q{
								UPDATE host
								SET HOST_EST_COMPLETE_MINUT = (
									CASE
										WHEN (host_est_complete_minut + ?) > 99 THEN 99
										ELSE (host_est_complete_minut + ?)
									END
								)
								WHERE host_id = ?
							},
							values => [ $total_cycle_time, $total_cycle_time, $host_id ]
						);
						unless ($result)
						{
							push @logs, "Top-Off Failed    : ".$DATABASE->errstr;
						}
					}
					else
					{
						push @logs, "Cycle Time Adjust : Skipping adjust for unknown top-off position";
					}
				}
				else
				{
					# new cycle
					push @logs, "Cycle Time Adjust : Setting cycle time to $total_cycle_time for host $host_id, $item_desc Time";
					$DATABASE->update(	table            => 'host',
										update_columns   => 'HOST_EST_COMPLETE_MINUT',
										update_values    => [$total_cycle_time],
										where_columns    => ['host_id = ?'],
										where_values     => [$host_id]);

					if($host_type_cd =~ m/^$EQUIP_GROUP_SPEEDQUEEN_REGEX$/o)
					{
						# this is speed queen equipment which does not send a cycle complete status change notif
						# we need to schedule a simulated cycle change notif here

						my $get_start_time_ref = $DATABASE->select(
							table			=> 'host',
							select_columns	=> 'to_char(host.host_last_start_ts, \'ss:mi:hh24:dd:mm:yyyy:d:ddd\')',
							where_columns	=> ['host_id = ?'],
							where_values	=> [ $host_id]);

						if(defined $get_start_time_ref->[0])
						{
							my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
							my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $get_start_time_ref->[0]->[0]);
							my $start_ts = eval { timelocal($sec, $min, $hr, $dm, ($mon-1), $yr); };
							my $event_ts = $timestamp > $start_ts ? $timestamp : $start_ts;	#if (tran ts > last host start ts), use tran ts (as the batch was received before status update)

							my $schedule_simulated_result = _schedule_simulated_cycle_complete($host_id, $event_ts, $total_cycle_time);
							push @logs, "Speed Queen Equip : $schedule_simulated_result";
						}
						else
						{
							push @logs, "Speed Queen Equip : Failed to lookup last_start_ts for host $host_id";
						}
					}
				}
			}
			else
			{
				push @logs, "Cycle Time Lookup : Failed to find stored cycle time for host $host_id item $item_desc";
			}
		}
		#else we are in free-vend mode--not adjusting estimate here, at least for now....
	}

	push @logs, "Total Sale        : $total";

	my $tran_return_code;
	my $error_message;
	my $acct_entry_method = $tran_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__cash
		? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__manual
		: $tran_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__credit_card || $tran_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__special_card
			? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__magnetic_stripe
			: $tran_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__rfid_credit_card || $tran_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__rfid_special_card
				? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__contactless
				: USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__unspecified;
	my $db_tran_id;
	my $tran_state_cd_batch;

	push @logs, "Storing Tran      : $trans_id";

	# call a stored proc to store the tran
	my $tran_local_auth_yn_flag = 'Y';
	$posh->queue_debit_summary(
		$tran_batch_type_cd
		,$auth_data					#card magstripe data
		,undef						#card pin entry
		,$device_id
		,$trans_id
		,$tran_result				#tran result
		,$tran_type					#tran card type
		,$total						#trans total amt
		,$tran_local_auth_yn_flag	#local auth flag
		,$auth_data
		,$acct_entry_method
		,$date_time					#start ts
		,$date_time					#end ts
		,\$db_tran_id
		,\$tran_state_cd_batch
		,\$tran_return_code
		,\$error_message
	);
	push @logs, "POS Processor     : queue_debit_summary($tran_batch_type_cd, ".mask_credit_card($auth_data).", undef, $device_id, $trans_id, undef, $tran_type, undef, $tran_local_auth_yn_flag, ".mask_credit_card($auth_data).", $acct_entry_method, $date_time, $date_time)";

	push @logs, "Return Code       : $tran_return_code";
	push @logs, "Error Message     : $error_message" if defined $error_message;
	push @logs, "DB Trans ID       : $db_tran_id";

	if($tran_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
	{
		push @logs, "POS Processor     : queue_debit_summary failed! return_code ne ".USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
		push @logs, _log_exception_old_client_compatible($message, 'queue_debit_summary', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
		ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed!\n$error_message", $command_hashref);
		return(\@logs);
	}

	if(not defined $db_tran_id)
	{
		push @logs, "POS Processor     : queue_debit_summary failed! db_tran_id is undefined";
		push @logs, _log_exception_old_client_compatible($message, 'queue_debit_summary', '15', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
		ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed!\n$error_message", $command_hashref);
		return(\@logs);
	}

	#       0          1         2        3      4       5              6            7      8
	# [$item_num, $port_num, $position, $amt, $item, $top_bottom, $description, $host_id, $qty]

	foreach my $item_ref (@line_items)
	{
		my $item_return_code;

		push @logs, "Storing Line Item : " . $item_ref->[0] . " -----";
		# call a stored proc to store each line item
		$posh->queue_tran_add_line_item(
			$tran_batch_type_cd
			,$db_tran_id
			,$tran_state_cd_batch
			,$item_ref->[7]						#host id
			,($item_ref->[3]/$item_ref->[8])	#item amount
			,0									#item tax
			,$item_ref->[4]						#item
			,$item_ref->[6]						#description
			,$item_ref->[8]						#qty
			,undef								#position cd
			,\$item_return_code
			,\$error_message
		);
		push @logs, "POS Processor     : queue_tran_add_line_item($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $item_ref->[7], ".($item_ref->[3]/$item_ref->[8]).", 0, $item_ref->[4], $item_ref->[6], $item_ref->[8], undef)";
		push @logs, "Return Code       : $item_return_code";
		push @logs, "Error Message     : $error_message" if defined $error_message;

		if($item_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
		{
			push @logs, "POS Processor     : queue_tran_add_line_item failed! ";
			push @logs, _log_exception_old_client_compatible($message, 'queue_tran_add_line_item', 16, $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
			ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_add_line_item failed!\n$error_message", $command_hashref);
			return(\@logs);
		}
	}

	### complete batch ###
	my $batch_complete_return_code;
	my $batch_complete_error_message;
	$posh->queue_tran_finalize(
		$tran_batch_type_cd
		,$db_tran_id
		,undef			#tran total amount (undef == don't validate)
		,\$tran_state_cd_batch
		,\$batch_complete_return_code
		,\$batch_complete_error_message
	);
	push @logs, "POS Processor     : queue_tran_finalize($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, undef)";
	push @logs, "Return Code       : $batch_complete_return_code";
	push @logs, "Error Message     : $batch_complete_error_message" if defined $batch_complete_error_message;
	if ($batch_complete_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
	{
		push @logs, "POS Processor     : queue_tran_finalize failed! ";
		push @logs, _log_exception_old_client_compatible($message, 'queue_tran_finalize', '16', $response_no, $trans_id, $machine_id, $DATABASE, 2, $command_hashref);
		ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_finalize failed!\n$batch_complete_error_message", $command_hashref);
		return(\@logs);
	}

	### send response (actual batch message) ###
	if ($tran_batch_type_cd eq 'A')
	{
		my $response = pack("CH2NC", $response_no, '71', $trans_id, 1);
		push @logs, "Response          : " . unpack("H*", $response);

		ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	}

	### handle (prepare or execute) settlement ###
	if ($tran_batch_type_cd eq 'I')
	{
		my $handle_debit_intended_return_code;
		my $handle_debit_intended_error_message;
		$posh->handle_debit_intended(
			$tran_batch_type_cd
			,$db_tran_id
			,$tran_state_cd_batch
			,undef			#tran total amount (undef == don't validate)
			,$tran_local_auth_yn_flag
			,\$handle_debit_intended_return_code
			,\$handle_debit_intended_error_message
		);
		push @logs, "POS Processor     : handle_debit_intended($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, undef, $tran_local_auth_yn_flag)";
		push @logs, "Return Code       : $handle_debit_intended_return_code";
		push @logs, "Error Message     : $handle_debit_intended_error_message" if defined $handle_debit_intended_error_message;
		if ($handle_debit_intended_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
		{
			push @logs, "POS Processor     : handle_debit_intended failed! ";
			my $tran_response = 2;
			if ($handle_debit_intended_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__insufficient_balance
				|| $handle_debit_intended_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__auth_handler_request_denied
				|| $handle_debit_intended_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__auth_handler_error) {
				$tran_response = 0;
			}
			push @logs, _log_exception($message, 'handle_debit_intended', '16', $response_no, $trans_id, $machine_id, $DATABASE, $tran_response, $command_hashref);
			ReRix::Utils::send_alert("ESuds: POS Processor     : handle_debit_intended failed!\n$handle_debit_intended_error_message", $command_hashref);
			return(\@logs);
		}

		### send response (intended batch message) ###
		my $response = pack("CH2NC", $response_no, '71', $trans_id, 1);
		push @logs, "Response          : " . unpack("H*", $response);

		ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	}

	return(\@logs);
}

sub _get_item_details ($$)
{
	my ($DATABASE, $item_id) = @_;
	my $lookup_item_details_ref = $DATABASE->select(
						table			=> 'tran_line_item_type',
						select_columns	=> 'tran_line_item_type_desc',
						where_columns	=> [ 'tran_line_item_type_id = ?' ],
						where_values	=> [ $item_id ] );

	return $lookup_item_details_ref->[0]->[0];
}

sub _get_host_group_type_id ($$)
{
	my ($DATABASE, $host_group_type_cd) = @_;
	return undef unless defined $host_group_type_cd;
	my $c_host_group_type_id = $DATABASE->query(
		query => q{
			SELECT host_group_type_id
			FROM host_group_type
			where host_group_type_cd = ?
		},
		values => [ $host_group_type_cd ]
	);

	return $c_host_group_type_id->[0]->[0];
}

sub _get_host_type_name ($)
{
	my ($host_type_cd) = shift;
	my $name;

	if ($host_type_cd =~ m/^$EQUIP_GROUP_WASHER_REGEX$/o)
	{
		$name = 'Washer';
	}
	elsif ($host_type_cd =~ m/^$EQUIP_GROUP_DRYER_REGEX$/o)
	{
		$name = 'Dryer';
	}
	elsif ($host_type_cd =~ m/^$EQUIP_GROUP_STACKED_DRYER_REGEX$/o)
	{
		$name = 'Stacked Dryer';
	}
	elsif ($host_type_cd =~ m/^$EQUIP_GROUP_STACKED_WASHER_DRYER_REGEX$/o)
	{
		$name = 'Stacked Washer/Dryer';
	}
	else
	{
		return undef;
	}

	return $name;
}

sub _send_cycle_finished_email ($$$$$)
{
	my ($DATABASE, $device_id, $host_id, $machine, $base_cycle_type_id) = @_;
	my @logs;
	my $max_hours_since_upload_for_alert = 3;
	my $upload_lag_adjust_minutes = 5;

	### check if we are in free-vend or regular mode
	my (@to_email_addrs, $location_name);
	my $subject = "eSuds.net Cycle Complete";
	my $from_addr = 'admin@esuds.net';
	my $from_name = 'eSuds.net Administrator';
	my $host_cycle_price = _host_base_cycle_price($DATABASE, $host_id, $base_cycle_type_id);
	if (not defined $host_cycle_price)	#error
	{
		push @logs, "Unable to determine free-vend mode for device $device_id host $host_id: no cycle price found for item type $base_cycle_type_id";
	}

	### lookup the location
	my $array_ref = $DATABASE->query(
		query => q{
			SELECT 	location.location_name
			FROM 	location.location,
					pos
			WHERE 	location.location_id = pos.location_id
			AND 	pos.device_id = ?
		},
		values => [ $device_id ]
	);

	unless (defined $array_ref->[0]->[0])
	{
		ReRix::Utils::send_alert("ESuds: location not found for device_id=$device_id");
		return "ESuds: location not found for device_id=$device_id";
	}

	$location_name = $array_ref->[0]->[0];
	if (defined $host_cycle_price && $host_cycle_price != 0)	#regular mode
	{
		# lookup the email address
		my $array_ref = $DATABASE->query(
			query => q{
			select tran.tran_id,
				consumer.consumer_email_addr2,
				consumer.consumer_fname,
				consumer.consumer_lname,
				tran.tran_start_ts,
				tran.tran_upload_ts,
				host.host_last_start_ts,
				consumer.consumer_email_addr1,
				consumer.consumer_id
			from consumer,
				consumer_acct,
				tran,
				pos_pta,
				tran_line_item,
				host,
                pss.consumer_notif
			where tran.consumer_acct_id = consumer_acct.consumer_acct_id
			and consumer_acct.consumer_id = consumer.consumer_id
			and tran.tran_id = tran_line_item.tran_id
			and tran_line_item.host_id = host.host_id
			and tran.pos_pta_id = pos_pta.pos_pta_id
			and tran_line_item.tran_line_item_batch_type_cd = ?
			and tran.tran_start_ts > (sysdate-(?/24))
			and tran.tran_upload_ts > (host.host_last_start_ts-(?/1440))
			and tran_line_item.tran_line_item_id = (
				select tran_line_item_id
				from PSS.tran_line_item_recent
				where host_id = ?
				and tran_line_item_type_id = ?
			)
			and consumer.consumer_id = consumer_notif.consumer_id (+)
            and consumer_notif.host_status_notif_type_id (+) = ?
            and nvl(consumer_notif.notify_on, ?) = ?
		},
		values => [
			'A', # tran_batch_type_cd
			$max_hours_since_upload_for_alert, # max_hours
			$upload_lag_adjust_minutes, # tran_upload_lag_adjust
			$host_id, # host_id
			$base_cycle_type_id, # tran_line_item_type_id
			2, # host_status_notif_type_id
			'Y', # default_notify_on
			'Y' # qualifying_notify_on
		]);

		my $row = $array_ref->[0];

		if(defined $row) {
			my ($to_addr, $first_name, $last_name, $primary_addr, $consumer_id);
			(undef, $to_addr, $first_name, $last_name, undef, undef, undef, $primary_addr, $consumer_id) = @{$row};

			#create passcode for deregister
			push @logs, "Cycle Complete Notify : Creating pass code for $primary_addr (consumer_id = $consumer_id)";
			my $array_ref = $DATABASE->callproc(
				name	=> 'PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE',
				bind	=> [ qw(:consumer_id :passcode_type) ],
				in		=> [ $consumer_id, 'NOTIF_CONFIG' ],
				output	=> 50
			);
			unless (defined $array_ref) {
				ReRix::Utils::send_alert("ESuds: send_cycle_finished_email: SQL execution failed: ".$DATABASE->errstr);
				push @logs, $DATABASE->errstr;
				return(join("\n", @logs));
			}
			my $passcode = $array_ref->[0];

			### process e-mail for user that started the transaction (unless if in monitor mode, then there is no e-mail defined)
			my $message = "Hi $first_name,\n\nYour laundry is finished. ".
				"Please pick up your laundry from $machine in $location_name.\n\nThank you!\n\n".
				"P.S. - If you wish to stop receiving these notifications or if you would like to ".
				"change the email address to which these notifications are sent, please visit the following link:\n\n\t".
				"http://www.esuds.net/RoomStatus/notifyConfig.do?action=get&primaryEmailAddr=".URI::Escape::uri_escape($primary_addr)."&".
                "hostStatusNotifyTypeId=2&passCode=".URI::Escape::uri_escape($passcode)."\n";
			my $to_name = "$first_name $last_name";
			if(defined $to_addr) {
				$DATABASE->insert(	table=> 'ob_email_queue',
									insert_columns=> 'OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG',
									insert_values=> [$from_addr, $from_name, $to_addr,  $to_name, $subject, $message]);
				push @to_email_addrs, $to_addr;
			} else {
				#ReRix::Utils::send_alert("ESuds: send_cycle_finished_email: No email address found for device $device_id");
			}
		}
	}

	# this should get done regardless of free-vend mode or not, although it most likely won't get used
	# in regular mode because the website won't support it, although it may if the customer wants it

	### lookup email addresses of other users who have requested cycle complete status
	my $get_emails_ref = $DATABASE->select(
		table			=> 'host_status_notif_queue',
		select_columns	=> 'host_status_notif_queue_id, host_status_notif_q_email_addr',
		where_columns	=> [ 'host_id = ?', 'host_status_notif_type_id = ?' ],
		where_values	=> [ $host_id, 2 ]	#2=eSuds Laundry Machine Cycle Completed Notification
	);

	if(defined $get_emails_ref->[0])
	{
		my $message = "Hi!\n\nThis is the notification you requested from $machine in $location_name.\n\nThank you!";
		foreach my $row_aref (@{$get_emails_ref})
		{
			my ($host_status_notif_queue_id, $host_status_notif_q_email_addr) = @{$row_aref};
			if (defined $host_status_notif_queue_id)
			{
				unless ($host_status_notif_q_email_addr eq '')
				{
					push @to_email_addrs, $host_status_notif_q_email_addr;

					### send e-mail
					$DATABASE->insert(
						table			=> 'ob_email_queue'
						,insert_columns	=> 'OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG'
						,insert_values	=> [$from_addr, $from_name, $host_status_notif_q_email_addr, $host_status_notif_q_email_addr, $subject, $message]
					);
				}

				### delete from queue
				$DATABASE->do(
					query => q{DELETE FROM host_status_notif_queue WHERE host_status_notif_queue_id = ?},
					values => [ $host_status_notif_queue_id ]
				);
			}
		}
	}

	if (scalar @to_email_addrs > 0)
	{
		push @logs, "Inserted emails to ".join(',', @to_email_addrs);
	}
	else
	{
		push @logs, "No emails sent for device $device_id";
	}
	return(join("\n", @logs));
}

sub _log_exception_old_client_compatible ($$$$$$$$$)
{
	my ($rerix_command, $function_name, $error_code, $response_no, $trans_id, $machine_id, $DATABASE, $response_tran_result, $command_hashref) = @_;
	my @logs;

#	my $server_name = hostname();
#	$DATABASE->callproc(
#		'name'	=> 'PKG_EXCEPTION_PROCESSOR.sp_log_exception',
#		'bind'	=> [ ':error_code', ':rerix_cmd', ':server_name', ':function_name' ],
#		'in'	=> [ $error_code, unpack("H*",$rerix_command), $server_name, $function_name ]
#	);

	my $tran_result = defined $response_tran_result ? $response_tran_result : 0;
#	if ($tran_result == 2) {	#remove this case once all eSuds clients upgraded to 1.3.x or later!!
#		push @logs, "Storing failed transaction and sending no response ...";
#	}
#	else {
	my $response = pack("CH2NC", $response_no, '71', $trans_id, 1);	#sending positive response in all cases (for now)
#	my $response = pack("CH2NC", $response_no, '71', $trans_id, $tran_result);
	push @logs, "Storing failed transaction and sending tran result response '$tran_result'...";
	push @logs, "Response          : " . unpack("H*", $response);
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
#	}

	return @logs;
}

sub _log_exception ($$$$$$$$$)
{
	my ($rerix_command, $function_name, $error_code, $response_no, $trans_id, $machine_id, $DATABASE, $response_tran_result, $command_hashref) = @_;
	my @logs;

#	my $server_name = hostname();
#	$DATABASE->callproc(
#		'name'	=> 'PKG_EXCEPTION_PROCESSOR.sp_log_exception',
#		'bind'	=> [ ':error_code', ':rerix_cmd', ':server_name', ':function_name' ],
#		'in'	=> [ $error_code, unpack("H*",$rerix_command), $server_name, $function_name ]
#	);

	my $tran_result = defined $response_tran_result ? $response_tran_result : 0;
	my $response = pack("CH2NC", $response_no, '71', $trans_id, $tran_result);
	push @logs, "Storing failed transaction and sending tran result response '$tran_result'...";
	push @logs, "Response          : " . unpack("H*", $response);
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
#	}

	return @logs;
}

# This procedure will create a pending Server to Client request for Room Layout.  We'll do
# this a case where we receive host information that doesn't look consistent with the
# layout that's on the server.
sub _request_room_layout ($$)
{
	my @logs;
	my ($DATABASE, $machine_id) = @_;

	# first check if a pending request is already queued
	my $lookup_pending_ref = $DATABASE->select(
						table			=> 'machine_command_pending',
						select_columns	=> 'count(1)',
						where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?', 'execute_cd in (?,?)'],
						where_values	=> [$machine_id, '83', '0C', 'P', 'S']);

	if(not defined $lookup_pending_ref->[0])
	{
		push @logs, "Room Layout Req   : Failed to check for pending request for $machine_id";
		return (\@logs);
	}

	if($lookup_pending_ref->[0][0] eq '0')
	{
		push @logs, "Room Layout Req   : Queuing for $machine_id";
		$DATABASE->insert(	table=> 'machine_command_pending',
						insert_columns=> 'machine_id, data_type, command, execute_cd, execute_order',
						insert_values=> [$machine_id, '83', '0C', 'P', 1]);
	}
	else
	{
		push @logs, "Room Layout Req   : Queued request already exists for $machine_id";
	}

	return (\@logs);
}

########################################################################
sub _host_base_cycle_price ($$$) {
#Purpose:	Return base (default required cycle) host cycle price from host_setting table depending on the type of host (washer or dryer).
#			If return = 0, then host is in free-vend mode.
#			If return > 0, then host is in
#
#Pre:		-
#Requires:	hashref: db handle
#			scalar: valid value in host.host_id
#			scalar: valid cycle type id
#Returns: 	success: non-negative price of base host cycle; error: undef
#Post:		-
	my ($DATABASE, $host_id, $base_cycle_type_id) = @_;

	### note: this is a temporary solution to insure we can reliably determine free-vend or regular cycle mode
	my $host_price_ref = $DATABASE->select(
		table			=> 'host_setting'
		,select_columns	=> 'host_setting_value'
		,where_columns	=> [
			'host_id = ?'
			,"host_setting_parameter = (
				select 	tran_line_item_type_desc || ' Price'
				from	tran_line_item_type
				where 	tran_line_item_type_id = ?
			)"
		]
		,where_values	=> [ $host_id, $base_cycle_type_id ]
	);

	# return cycle price if it makes sense, otherwise return undef
	return (
		defined     $host_price_ref->[0][0]
		and length( $host_price_ref->[0][0] ) > 0
	) ? $host_price_ref->[0][0]
	  : undef;
}

sub _get_host_base_cycle_type_id ($$)
{
	my ($host_type_cd, $position) = @_;
	if ($host_type_cd =~ m/^$EQUIP_GROUP_WASHER_REGEX$/o)
	{
		return 1;
	}
	elsif ($host_type_cd =~ m/^$EQUIP_GROUP_DRYER_REGEX$/o)
	{
		return 40;
	}
	elsif ($host_type_cd =~ m/^$EQUIP_GROUP_STACKED_DRYER_REGEX$/o)
	{
		return 40;
	}
	elsif ($host_type_cd =~ m/^$EQUIP_GROUP_STACKED_WASHER_DRYER_REGEX$/o)
	{
		if($position eq '1')	#top
		{
			return 40;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return -1;
	}
}

sub _schedule_simulated_cycle_complete ($$$) {
	my ($hostId, $cycleStartTime, $estCycleMin) = @_;

	my $obj = eval {
		USAT::App::ECCS::SOAP::ComplexType::hostItem->new({
			hostId => $hostId,
			cycleStartTime => USAT::SOAP::Util::convert_epoch_sec_to_soap_datetime($cycleStartTime),
			estCycleMin => $estCycleMin
		});
	};
	return "Cycle complete event NOT scheduled: $@" if ($@);

	my $response = USAT::App::ECCS::Gateway
		-> proxy("http://$ReRix_ESuds{eccs_host}:$ReRix_ESuds{eccs_port}/?session=ECCS", timeout => $ReRix_ESuds{eccs_timeout})
		-> schedule(\SOAP::Data->value($obj->as_soap_data));
	my $result = eval { $response->result || $response->fault; };

	if (defined $result) {
		if ($response->fault) {
			return "Cycle complete event NOT scheduled: Error ".$response->faultcode.": ".$response->faultstring.": ".$response->faultdetail;
		}
		elsif (UNIVERSAL::isa($result, 'ResponseMessage')) {
			my $obj = eval {
				USAT::App::ECCS::SOAP::ComplexType::ResponseMessage->new($result);
			};
			return "Cycle complete event NOT scheduled: $@" if ($@);
			if ($obj->code == 0) {
				return "Cycle complete event scheduled"
			}
			else {
				return "Cycle complete event NOT scheduled: Error ".$obj->code.": ".$obj->detail;
			}
		}
		else {
			return "Cycle complete event scheduler: Unknown response: $result";
		}
	}
	else {
		return "Cycle complete event scheduler: Unknown error: $@";
	}

	return $result;
}

1;
