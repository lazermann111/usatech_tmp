package ReRix::Init;

use strict;
use Evend::ReRix::Shared;
use USAT::Common::CallInRecord;
use USAT::Database::SensitiveData;
use USAT::POS::API::PTA::Util;
use ReRix::ClientVersion;
use ReRix::Utils;
use ReRix::InfoRequest;
use ReRixConfig qw(%ReRix_Init %ReRix_Functions);

use constant {
	CMD_V2		=> 2.0,
	CMD_V3		=> 3.0,
	CMD_V3_1	=> 3.1
};

sub parse_V3 { return _parse_init(@_, CMD_V3); }
sub parse_V3_1 { return _parse_init(@_, CMD_V3_1); }

sub parse_V2
{
	my ($DATABASE, $command_hashref) = @_;
	
	my @logs;
	
	my $msg_no = $command_hashref->{msg_no};

	my ($ev_number, $ssn, $model, $build, $config, $array_ref);
	my ($device_id, $key, $lookup_device_ref, $db_device_type);

	my $message = MakeString($command_hashref->{inbound_command});

	$message = substr($message,1);        # chop 0xb1
	$config = ord(substr($message,0,1));  # Get config
	$message = substr($message,1);        # chop config

	push @logs, "The message before using parse_byteblock (hex) : " . unpack("H*",$message);
	
	($ev_number, $message) =  &parse_byteblock($message); # get EV number
	$ev_number = uc(substr($ev_number, 0, 8));
	push @logs, "ev_number (asc)   : $ev_number";
	push @logs, "ev_number (hex)   : " . unpack("H*",$ev_number);
	
	($model, $message) =   &parse_byteblock($message); # get Model Number
	push @logs, "model (asc)       : $model";
	push @logs, "model (hex)       : " . unpack("H*",$model);

	($build, $message) =   &parse_byteblock($message); # get Build number
	push @logs, "build (asc)       : $build";
	push @logs, "build (hex)       : " . unpack("H*",$build);

	$ssn = $message;    # get Silicon Serial Number
	$ssn = uc(unpack("H*", substr($ssn, 0, 8)));
	push @logs, "ssn               : $ssn";
	
	if ( $model eq "\x00" )
	{
		$model = ''; 
	}

	if ( $build eq "\x00" )
	{
		$build = ''; 
	}
	
	# InitV2 is used only by Sony PictureStations with the old DLL
	if($model !~ m/Sony/ || $ssn !~ m/^[0-9A-F]{8}(63){4}$/)
	{
		ReRix::Utils::send_alert("InitV2: Device identification failed validation tests! (Serial number: $ssn, model: $model)", $command_hashref);
		push @logs, "ERROR: Device identification failed validation tests! (Serial number: $ssn, model: $model)";
		return (\@logs);
	}
	
	my $device_type = '4';
	my $client_type = 'SONY';
	my $found_device = 0;
	my $valid_ev_number = 0;
	
	if ($ev_number =~ m/^EV(\d){5,6}$/)
	{
		$valid_ev_number = 1;
	
		push @logs, "Searching for device record with device_name $ev_number...";
	
		$lookup_device_ref = $DATABASE->select(
			table			=> 'device.device',
			select_columns	=> 'device_id, device_serial_cd, device_type_id',
			order			=> 'device_active_yn_flag desc',
			where_columns	=> ['device_name = ?'],
			where_values	=> [$ev_number] );

		if($lookup_device_ref->[0])
		{
			$found_device = 1;
		
			$device_id = $lookup_device_ref->[0][0];
			push @logs, "DB Device ID      : $device_id";

			my $db_ssn = $lookup_device_ref->[0][1];
			push @logs, "DB Serial Number  : $db_ssn";

			$db_device_type = $lookup_device_ref->[0][2];
			if($db_device_type ne $device_type)
			{
				ReRix::Utils::send_alert("Init: WARNING: Stored device type $db_device_type not equal to received type $device_type", $command_hashref);
				push @logs, "WARNING: Stored device type $db_device_type not equal to received type $device_type";
			}

			if ($db_ssn ne $ssn)
			{	
				push @logs, "Changing Serial Number device_serial_cd in device table to $ssn for $ev_number";

				$DATABASE->update(
						table			=> 'device.device',
						update_columns	=> 'device_serial_cd',
						update_values	=> [$ssn],
						where_columns	=> ['device_name = ?'],
						where_values	=> [$ev_number] );
			}
		}
		else
		{
			push @logs, "No device record found for device_name $ev_number";
		}
	}
		
	if ($found_device == 0)
	{
		push @logs, "Searching for device record with device_serial_cd $ssn...";

		$lookup_device_ref = $DATABASE->select(
							table			=> 'device.device',
							select_columns	=> 'device_id, device_name, device_type_id',
							order			=> 'device_active_yn_flag desc',
							where_columns	=> ['device_serial_cd = ?'],
							where_values	=> [$ssn] );

		if($lookup_device_ref->[0])
		{
			$device_id = $lookup_device_ref->[0][0];
			push @logs, "DB Device ID      : $device_id";

			$ev_number = $lookup_device_ref->[0][1];
			push @logs, "DB Serial Number  : $ev_number";

			$db_device_type = $lookup_device_ref->[0][2];
			if($db_device_type ne $device_type)
			{
				ReRix::Utils::send_alert("Init: WARNING: Stored device type $db_device_type not equal to received type $device_type", $command_hashref);
				push @logs, "WARNING: Stored device type $db_device_type not equal to received type $device_type";
			}
		}
		else
		{	
			push @logs, "No device record found for device_serial_cd $ssn, inserting new record...";

			if ($valid_ev_number == 0)
			{
				$array_ref = $DATABASE->select(
							table			=> 'dual',
							select_columns	=> 'tazdba.rerix_machine_id_v2');

				$ev_number = $array_ref->[0][0];
				push @logs, "New EV number     : $ev_number";
			}

			# generate new encryption key
			$key = &ReRix::Utils::gen_random_key($ssn);

			$DATABASE->insert(
						table			=> 'device.device',
						insert_columns	=> 'device_name, device_type_id, device_serial_cd, device_active_yn_flag, encryption_key',
						insert_values	=> [$ev_number, $device_type, $ssn, 'Y', USAT::Database::SensitiveData->new($key, '*' x length($key))] );

			# do this query again, it should work now
			$lookup_device_ref = $DATABASE->select(
									table			=> 'device.device',
									select_columns	=> 'device_id',
									where_columns	=> ['device_serial_cd = ?'],
									where_values	=> [$ssn] );

			if(not $lookup_device_ref->[0])
			{
				ReRix::Utils::send_alert("Init: ERROR: Failed to lookup device for serial $ssn after doing insert!  Insert must have failed!", $command_hashref);
				push @logs, "ERROR: Failed to lookup device for serial $ssn after doing insert!  Insert must have failed!";
				return (\@logs);
			}

			$device_id = $lookup_device_ref->[0][0];
			push @logs, "New Device ID     : $device_id";
		}
	}

	# update Firmware Version in DB
	my $device_setting_ref = $DATABASE->select(
								table			=> 'device_setting',
								select_columns	=> 'device_setting_value',
								where_columns	=> ['device_id = ?', 'device_setting_parameter_cd = ?'],
								where_values	=> [$device_id, 'Firmware Version'] );

	if(defined $device_setting_ref->[0][0])
	{	
		my $db_build = $device_setting_ref->[0][0];
		if($db_build ne $build)
		{
			push @logs, "Updating DB Firmware Version for Device ID $device_id from $db_build to $build";

			$DATABASE->update(table			=> 'device_setting',
							update_columns	=> 'device_setting_value',
							update_values	=> [$build],
							where_columns	=> ['device_id = ?', 'device_setting_parameter_cd = ?'],
							where_values	=> [$device_id, 'Firmware Version'] );
		}
	}
	else
	{
		push @logs, "Inserting Firmware Version $build into DB for Device ID $device_id";

		$DATABASE->insert(
					table			=> 'device_setting',
					insert_columns	=> 'device_id, device_setting_parameter_cd, device_setting_value',
					insert_values	=> [$device_id, 'Firmware Version', $build]);
	}

	# we should now have an initialized device that exists in the device table
	# now make sure the POS exists so it can record transactions and be linked to a customer and location

	my $lookup_device_type_ref = $DATABASE->select(
									table			=> 'device_type',
									select_columns	=> 'device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id',
									where_columns	=> [ 'device_type_id = ?' ],
									where_values	=> [ $device_type ] );

	if(!defined $lookup_device_type_ref->[0])
	{
		# this device type is not supported by Initialize 2.0
		ReRix::Utils::send_alert("Init: Device type $device_type is not supported by Initialize 2.0!", $command_hashref);
		push @logs, "ERROR: Device type $device_type is not supported by Initialize 2.0!";
		return (\@logs);
	}

	my $device_type_name = $lookup_device_type_ref->[0][0];
	my $device_type_serial_cd_regex = $lookup_device_type_ref->[0][1];
	my $device_type_pos_pta_tmpl = $lookup_device_type_ref->[0][2];

	push @logs, "Device Type Name  : $device_type_name";

	my $lookup_pos_ref = $DATABASE->select(
						table			=> 'pos',
						select_columns	=> 'pos_id, location_id, customer_id',
						where_columns	=> ['device_id = ?'],
						where_values	=> [$device_id] );

	my $pos_id = $lookup_pos_ref->[0][0];
	if(not defined $pos_id)
	{
		push @logs, "POS               : inserting $device_id";

		$DATABASE->insert(
					table			=> 'pos',
					insert_columns	=> 'device_id',
					insert_values	=> [$device_id]);
	}
	else
	{
		push @logs, "POS               : exists; $pos_id";
	}

	# get the POS ID
	if(not defined $pos_id)
	{
		$lookup_pos_ref = $DATABASE->select(
							table			=> 'pos',
							select_columns	=> 'pos_id',
							where_columns	=> ['device_id = ?'],
							where_values	=> [$device_id] );

		$pos_id = $lookup_pos_ref->[0][0];
		push @logs, "POS               : new $pos_id";
	}

	if(not defined $pos_id)
	{
		ReRix::Utils::send_alert("Init: ERROR: Failed to lookup pos for device_id $device_id after doing insert!  Insert must have failed!", $command_hashref);
		push @logs, "ERROR: Failed to lookup pos for device_id $device_id after doing insert!  Insert must have failed!";
		return (\@logs);
	}

	my $err;

	# Create hosts if they don't exist
	my $hosts_created_count = USAT::POS::API::PTA::Util::create_default_hosts_if_needed($DATABASE, $device_id, \$err);
	if(defined $err)
	{
		ReRix::Utils::send_alert("Init: ERROR: Failed to create default hosts: " . $err, $command_hashref);
		push @logs, "ERROR: Failed to create default hosts: " . $err;
		return (\@logs);
	}

	push @logs, "Default Hosts     : Created $hosts_created_count Hosts";

	if(defined $device_type_pos_pta_tmpl)
	{
		# Create pos_pta's if they don't exist
		my $import_pos_pta_success = USAT::POS::API::PTA::Util::import_template($DATABASE, $device_id, $device_type_pos_pta_tmpl, 'S', \$err);
		if(defined $err || $import_pos_pta_success == 0)
		{
			ReRix::Utils::send_alert("Init: ERROR: Failed to import pos_pta template $device_type_pos_pta_tmpl: " . $err, $command_hashref);
			push @logs, "ERROR: Failed to import pos_pta template $device_type_pos_pta_tmpl: " . $err;
			return (\@logs);
		}

		push @logs, "POS PTA Import    : Template $device_type_pos_pta_tmpl imported successfully in Safe Mode";
	}
	else
	{
		push @logs, "POS PTA Import    : Import Skipped: Default template ID not found!";
	}

	my $response = pack("CCA*", $msg_no, 0x69, $ev_number);
	push @logs, "Response          : " . unpack("H*", $response);
	
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	USAT::Common::CallInRecord::device_initialized($DATABASE, $command_hashref, $command_hashref->{session_id});
		
	send_init_email($DATABASE, $device_id, CMD_V2, undef, undef);

	return (\@logs);
}

sub _parse_init
{
	my (@logs);
	my ($DATABASE, $command_hashref, $command_version) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $msg_no = $command_hashref->{msg_no};
	my $message = pack("H*",$command_hashref->{inbound_command});
	
	my $device_type = ord(substr($message, 1, 1));
	my ($SSN, $device_info, $terminal_info);
	
	if($command_version eq CMD_V3_1)
	{
		my $ssn_len = unpack("C", substr($message, 2, 1));
		$SSN = substr($message, 3, $ssn_len);
		my $device_info_len = unpack("n", substr($message, 3 + $ssn_len, 2));
		$device_info = substr($message, 3 + $ssn_len + 2, $device_info_len);
		my $terminal_info_len = unpack("n", substr($message, 3 + $ssn_len + 2 + $device_info_len, 2));
		$terminal_info = substr($message, 3 + $ssn_len + 2 + $device_info_len + 2, $terminal_info_len);
	}
	else
	{
		$SSN = substr($message, 2);
	}
	
	my $device_id;
	my $ev_number;
	my $key;
	my $active_flag;

	push @logs, "Device Type       : $device_type";
	push @logs, "SSN               : $SSN";
	
	my $ssn_validated = 0;
	
	my $lookup_device_type_ref = $DATABASE->select(
									table			=> 'device.device_type',
									select_columns	=> 'device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id',
									where_columns	=> [ 'device_type_id = ?' ],
									where_values	=> [ $device_type ] );
									
	if(!defined $lookup_device_type_ref->[0])
	{
		# this device type is not supported by Initialize 3.0
		ReRix::Utils::send_alert("Init: Device type $device_type is not supported by Initialize 3.0!", $command_hashref);
		push @logs, "ERROR: Device type $device_type is not supported by Initialize 3.0!";
		return (\@logs);
	}
	
	my $device_type_name = $lookup_device_type_ref->[0][0];
	my $device_type_serial_cd_regex = $lookup_device_type_ref->[0][1];
	my $device_type_pos_pta_tmpl = $lookup_device_type_ref->[0][2];
	
	push @logs, "Device Type Name  : $device_type_name";
	
	# some device types need serial number pre-processing before validation
	if($device_type eq '4')
	{
		# Sony/DLL serial number is 8 binary bytes, so unpack to hex
		$SSN = uc(unpack("H*",$SSN));
		if($SSN !~ /^[0-9A-F]{8}(63){4}$/)
		{
			# dll generated Sony serial number needs to be prefixed
			$SSN = 'S1' . $SSN;
		}
	}
	elsif($device_type eq '6' && length($SSN) == 6)
	{
		# mei needs to be prefixed
		$SSN = 'M1' . $SSN;
	}
	elsif($device_type eq '11')
	{
		if($command_version eq CMD_V3)
		{
			# dll serial number is 8 binary bytes and needs to be prefixed
			$SSN = 'K1' . uc(unpack("H*",$SSN));
		}
		elsif($command_version eq CMD_V3_1)
		{
			$SSN = uc($SSN);
			
			my $device_info_line = $device_info;
			$device_info_line =~ s/\n/\\n/gs;
			push @logs, "Device Info       : $device_info_line";
			if ($device_info =~ m/^A\n/s)
			{
				$device_info = substr($device_info, 2);
			}
			else
			{
				$device_info = undef;
				push @logs, "Received unsupported Device Info format!";
			}
			
			my $terminal_info_line = $terminal_info;
			$terminal_info_line =~ s/\n/\\n/gs;
			push @logs, "Terminal Info     : $terminal_info_line";
			if ($terminal_info =~ m/^A\n/s)
			{
				$terminal_info = substr($terminal_info, 2);
			}
			else
			{
				$terminal_info = undef;
				push @logs, "Received unsupported Terminal Info format!";
			}			
		}
	}

	# validate the serial number, only allow initialization by devices with valid serial numbers
	# this prevents devices with corrupt memory from initializing
	if(!($SSN =~ $device_type_serial_cd_regex))
	{
		ReRix::Utils::send_alert("Init: Serial number failed validation tests! ($SSN)", $command_hashref);
		push @logs, "ERROR: Serial number failed validation tests! ($SSN)";
		return (\@logs);
	}

	push @logs, "SSN Validated     : $SSN";
	
	my $lookup_device_ref = $DATABASE->select(
								table			=> 'device.device',
								select_columns	=> 'device_id, device_name, device_type_id, encryption_key, device_active_yn_flag',
								order			=> 'device_active_yn_flag desc',
								where_columns	=> [ 'device_serial_cd = ?' ],
								where_values	=> [ $SSN ] );
								
	if(not $lookup_device_ref->[0])
	{
		push @logs, "No device record found for $SSN";
		
		# generate new EV
		my $new_ev_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'rerix_machine_id_v3');

		$ev_number = $new_ev_ref->[0][0];
		push @logs, "New EV Number     : $ev_number";
		
		# generate new encryption key
		$key = &ReRix::Utils::gen_random_key($SSN);
		
		push @logs, "Generated new encryption key";
		
		$active_flag = 'Y';
		if($device_type eq '5')
		{
			# ESuds devices can initialize, but they will not be set active and will not get a response until
			# customer service sets the active flag = Y, then we will respond to the device 
			
			# Changed 03/02/2004 - eSuds devices are now immediatly
			#$active_flag = 'N';
			$active_flag = 'Y';
		}
		
		$DATABASE->insert(
					table			=> 'device.device',
					insert_columns	=> 'device_name, device_type_id, device_serial_cd, device_active_yn_flag, encryption_key',
					insert_values	=> [$ev_number, $device_type, $SSN, $active_flag, USAT::Database::SensitiveData->new($key, '*' x length($key))]);
					
		# do this query again, it should work now
		$lookup_device_ref = $DATABASE->select(
								table			=> 'device.device',
								select_columns	=> 'device_id',
								where_columns	=> [ 'device_serial_cd = ?' ],
								where_values	=> [ $SSN ] );
								
		if(not $lookup_device_ref->[0])
		{
			ReRix::Utils::send_alert("Init: ERROR: Failed to lookup device for serial $SSN after doing insert!  Insert must have failed!", $command_hashref);
			push @logs, "ERROR: Failed to lookup device for serial $SSN after doing insert!  Insert must have failed!";
			return (\@logs);
		}

		$device_id = $lookup_device_ref->[0][0];
		push @logs, "New Device ID     : $device_id";
	}
	else
	{
		$device_id = $lookup_device_ref->[0][0];
		push @logs, "DB Device ID      : $device_id";
		
		$ev_number = $lookup_device_ref->[0][1];
		push @logs, "EV Number         : $ev_number";

		my $db_device_type = $lookup_device_ref->[0][2];
		if($db_device_type ne $device_type)
		{
			ReRix::Utils::send_alert("Init: WARNING: Stored device type $db_device_type not equal to received type $device_type", $command_hashref);
			push @logs, "WARNING: Stored device type $db_device_type not equal to received type $device_type";
		}
		
		$key = $lookup_device_ref->[0][3];
		
		if($device_type =~ m/^(0|1|5|6)$/)
		{
			# generate new encryption key - see bug #346
			$key = &ReRix::Utils::gen_random_key($SSN);
		
			push @logs, "Generated new encryption key";
		
			$DATABASE->update(
					table			=> 'device.device',
					update_columns	=> 'encryption_key',
					update_values	=> [USAT::Database::SensitiveData->new($key, '*' x length($key))],
					where_columns	=> ['device_id = ?'],
					where_values	=> [$device_id] );
		}

		$active_flag = $lookup_device_ref->[0][4];
		push @logs, "Active Flag       : $active_flag";
	}
	
	# we should now have an initialized device that exists in the device table, and have the following info;
	# device id, ev number, encryption key, silicon serial number, device type
	
	# now make sure the POS exists to it can record transactions and be linked to a customer and location
	
	my $lookup_pos_ref = $DATABASE->select(
						table			=> 'pos',
						select_columns	=> 'pos_id, location_id, customer_id',
						where_columns	=> ['device_id = ?'],
						where_values	=> [$device_id] );
						
	my $pos_id = $lookup_pos_ref->[0][0];
	if(not defined $pos_id)
	{
		push @logs, "POS               : inserting $device_id";

		$DATABASE->insert(
					table			=> 'pos',
					insert_columns	=> 'device_id',
					insert_values	=> [$device_id]);
	}
	else
	{
		push @logs, "POS               : exists; $pos_id";
	}
	
	# get the POS ID
	if(not defined $pos_id)
	{
		$lookup_pos_ref = $DATABASE->select(
							table			=> 'pos',
							select_columns	=> 'pos_id',
							where_columns	=> ['device_id = ?'],
							where_values	=> [$device_id] );
		
		$pos_id = $lookup_pos_ref->[0][0];
		push @logs, "POS               : new $pos_id";
	}
	
	if(not defined $pos_id)
	{
		ReRix::Utils::send_alert("Init: ERROR: Failed to lookup pos for device_id $device_id after doing insert!  Insert must have failed!", $command_hashref);
		push @logs, "ERROR: Failed to lookup pos for device_id $device_id after doing insert!  Insert must have failed!";
		return (\@logs);
	}
	
	my $err;
	
	# Create hosts if they don't exist
	my $hosts_created_count = USAT::POS::API::PTA::Util::create_default_hosts_if_needed($DATABASE, $device_id, \$err);
	if(defined $err)
	{
		ReRix::Utils::send_alert("Init: ERROR: Failed to create default hosts: " . $err, $command_hashref);
		push @logs, "ERROR: Failed to create default hosts: " . $err;
		return (\@logs);
	}
	
	push @logs, "Default Hosts     : Created $hosts_created_count Hosts";

	if(defined $device_type_pos_pta_tmpl)
	{
		# Create pos_pta's if they don't exist
		my $import_pos_pta_success = USAT::POS::API::PTA::Util::import_template($DATABASE, $device_id, $device_type_pos_pta_tmpl, 'S', \$err);
		if(defined $err || $import_pos_pta_success == 0)
		{
			ReRix::Utils::send_alert("Init: ERROR: Failed to import pos_pta template $device_type_pos_pta_tmpl: " . $err, $command_hashref);
			push @logs, "ERROR: Failed to import pos_pta template $device_type_pos_pta_tmpl: " . $err;
			return (\@logs);
		}
		
		push @logs, "POS PTA Import    : Template $device_type_pos_pta_tmpl imported successfully in Safe Mode";
	}
	else
	{
		push @logs, "POS PTA Import    : Import Skipped: Default template ID not found!";
	}

	if($device_type =~ /^(0|1|6|12)$/)
	{
		# now check if a config file exists for this device in the file_transfer table, and if not create one
		my $lookup_config_ref = $DATABASE->select(
							table			=> 'file_transfer',
							select_columns	=> 'count(1)',
							where_columns	=> ['file_transfer_name = ?'],
							where_values	=> ["$ev_number-CFG"] );

		# make sure the config exists for G4/G5/MEI/T2

		if($lookup_config_ref->[0][0] eq '1')
		{
			push @logs, "Config Init       : $ev_number-CFG exists";
		}
		else
		{
			my $default_config;
			
			if($device_type eq '0')
			{
				$default_config = 'G4-DEFAULT-CFG';
			}
			elsif($device_type eq '1')
			{
				$default_config = 'G5-DEFAULT-CFG';
			}
			elsif($device_type eq '6')
			{
				$default_config = 'MEI-DEFAULT-CFG';
			}
			elsif($device_type eq '12')
			{
				$default_config = 'T2-DEFAULT-CFG';
			}
			
			push @logs, "Config Init       : Does not exist; Using default: $default_config";

			my $default_config_data = &blob_select_file($DATABASE->{handle}, $default_config);
			if(not defined $default_config_data)
			{
				ReRix::Utils::send_alert("Init: ERROR: Failed to load $default_config", $command_hashref);
				push @logs, "Init: ERROR: Failed to load $default_config";
				return (\@logs);
			}
			
			$default_config_data = pack("H*", $default_config_data);
		
			$DATABASE->insert(	table			=> 'file_transfer',
								insert_columns	=> 'file_transfer_name, file_transfer_type_cd',
								insert_values	=> ["$ev_number-CFG", "1"]);
			
			&blob_insert($raw_handle, "update file_transfer set file_transfer_content = :blob where file_transfer_name = :name", $default_config_data, "$ev_number-CFG");
		}
		
		if($device_type =~ /^(0|1)$/)
		{
			# insert pokes for full config rather than waiting for config request - see bug #698
			push @logs, @{ReRix::InfoRequest::_config_poke($DATABASE, $ev_number, $device_type, $command_hashref)};
		}
	}
		
	if ($device_type =~ /^(11|12)$/)
	{	
		# schedule config file transfer for Kiosk/T2
		my $cfg_file_id_ref = $DATABASE->select(
			table			=> 'file_transfer',
			select_columns	=> 'file_transfer_id',
			where_columns	=> ['file_transfer_name = ?'],
			where_values	=> ["$ev_number-CFG"] );			

		my $cfg_file_id = $cfg_file_id_ref->[0][0];

		if(defined $cfg_file_id)
		{
			my $cfg_validated = 1;
		
			if ($device_type =~ /^(11)$/)
			{
				# check if required fields are populated in config file
				my $check_cfg_ref = $DATABASE->query(
					query => q{
						SELECT COUNT(1)
						FROM device.device_setting
						WHERE device_id = ?
							AND device_setting_parameter_cd IN (?, ?, ?)
							AND CASE
									WHEN device_setting_value IS NULL THEN 0
									ELSE LENGTH(device_setting_value)
								END > ?
					},
					values	=> [
						$device_id,
						'SSN',
						'VMC',
						'EncKey',
						6
					]
				);
				
				$cfg_validated = 0 if $check_cfg_ref->[0][0] != 3;
			}
				
			if ($cfg_validated)
			{
				my ($cfg_file_name, $packet_size);

				if ($device_type =~ /^(11)$/)
				{
					$cfg_file_name = "EportNW.ini";
					$packet_size = 1024;
				}
				else
				{
					$cfg_file_name = "$ev_number-CFG";
					$packet_size = 100;
				}			

				#removing any requests for the config file
				$DATABASE->update(	table	=> 'machine_command_pending',
					update_columns	=> 'execute_cd',
					update_values	=> ['A'],
					where_columns	=> ['machine_id = ?', 'data_type = ?', 'upper(command) = upper(?)'],
					where_values	=> [$ev_number, '9B', unpack("H*", $cfg_file_name)] );

				# check if a pending file transfer start command already exists for this device and file
				my $check_cmd_ref = $DATABASE->query(
						query	=> q{
							select count(1) 
							from device_file_transfer dft, machine_command_pending mcp 
							where dft.device_id = ?
								and dft.file_transfer_id = ?
								and dft.device_file_transfer_status_cd = 0 
								and dft.device_file_transfer_direct = 'O' 
								and mcp.machine_id = ?
								and mcp.data_type = 'A4'
								and dft.device_file_transfer_id = mcp.command 
								and mcp.execute_cd = 'P'
						},
						values	=> [
							$device_id,
							$cfg_file_id,
							$ev_number
						]					
					);	

				my $check_cmd_count = $check_cmd_ref->[0][0];

				if($check_cmd_count == 0)
				{
					push @logs, "Config Init       : Creating File Transfer Start v1.1 command to upload $ev_number-CFG to $ev_number";

					# get the next device_file_transfer_id from the sequence				
					my $transfer_id_ref = $DATABASE->query(
						query	=> q{
							select seq_device_file_transfer_id.nextval from dual
						}
					);

					my $transfer_id = $transfer_id_ref->[0][0];

					# insert the device_file_transfer								
					$DATABASE->do(
						query	=> q{
							insert into device_file_transfer(
								device_file_transfer_id, 
								device_id, 
								file_transfer_id, 
								device_file_transfer_direct, 
								device_file_transfer_status_cd, 
								device_file_transfer_pkt_size 
							) 
							values(
								?,
								?,
								?,
								'O',
								0,
								?
							)
						},
						values	=> [
							$transfer_id,
							$device_id,
							$cfg_file_id,
							$packet_size
						]
					);				


					# insert a pending message for this new transfer				
					$DATABASE->do(
						query	=> q{
							insert into machine_command_pending(
								machine_id, 
								data_type, 
								command, 
								execute_cd, 
								execute_order
							) 
							values(
								?,
								'A4',
								?,
								'P',
								1
							)
						},
						values	=> [
							$ev_number,
							$transfer_id
						]
					);
				}
			}
		}
		elsif ($device_type =~ /^(12)$/)
		{
			ReRix::Utils::send_alert("Init: ERROR: Failed to select file_transfer_id for $ev_number-CFG, INSERT must have failed", $command_hashref);
			push @logs, "Init: ERROR: Failed to select file_transfer_id for $ev_number-CFG, INSERT must have failed";
			return (\@logs);
		}			
	}
	
	# if this is an MEI device, Peek for it's full config
	if($device_type eq '6')
	{
		my $lookup_pending_ref = $DATABASE->select(
				table			=> 'machine_command_pending',
				select_columns	=> 'count(1)',
				where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?', 'execute_cd in (?,?)'],
				where_values	=> [$ev_number, '87', '4200000000000000FF', 'P', 'S'] );
				
		if ($lookup_pending_ref->[0][0] eq '0')
		{
			$DATABASE->insert(
				table			=> 'machine_command_pending',
				insert_columns	=> 'machine_id, data_type, command, execute_cd, execute_order',
				insert_values	=> [$ev_number, '87', '4200000000000000FF', 'P', 1]);
				
			push @logs, "Full Config Peek  : Queuing for $ev_number";
		}
	}
	
	if($active_flag eq 'N' && $ReRix_Functions{DEVICE_REACTIVATION_ON_REINIT})
	{
		push @logs, "Active Flag       : Device is NOT active; setting active flag = Y";
		
		$DATABASE->update(
					table			=> 'device',
					update_columns	=> 'device_active_yn_flag',
					update_values	=> ['Y'],
					where_columns  => ['device_id = ?'],
					where_values   => [$device_id]);
	}
	
	### device firmware version query
	if ($device_type =~ /^(0|1)$/) 	#G4/G5
	{	
		push @logs, @{(ReRix::ClientVersion::request_g4($DATABASE, $ev_number))[0]};
		push @logs, @{(ReRix::ClientVersion::request_gx_gprs_settings($DATABASE, $ev_number))[0]};
	}
	elsif ($device_type eq '5') #eSuds
	{	
		push @logs, @{(ReRix::ClientVersion::request($DATABASE, $ev_number))[0]};
	}

	# eSuds wants a device reactivate message to startup correctly
	if($device_type eq '5')
	{
		push @logs, "Queued for ESuds  : Device Reactive Message";
		$DATABASE->insert(	table=> 'machine_command_pending',
						insert_columns=> 'machine_id, data_type, execute_cd, execute_order',
						insert_values=> [$ev_number, '76', 'P', 1]);
	}
	
	my $response = pack("CH2a*a*", $msg_no, '8F', $key, $ev_number);
	my $response_hex = unpack("H*", $response);
	push @logs, "Response          : " . ReRix::Utils::mask_response($response_hex, 4);
	# pass back the message using the original EV number
	ReRix::Utils::enqueue_outbound_command($command_hashref, $response_hex);
				
	USAT::Common::CallInRecord::device_initialized($DATABASE, $command_hashref, $command_hashref->{session_id});
	
	if ($device_type =~ /^(4|11)$/) # send init email
	{
		send_init_email($DATABASE, $device_id, $command_version, $device_info, $terminal_info);
	}
	
	return (\@logs);
}

####################################################################
# NOTE:  A byte block is defined as a series of bytes with the last
#        byte's 8th bit set to 1.  No other byte in the block has 
#        the 8th bit set to 1
####################################################################
sub parse_byteblock 
{
	my($message) = @_;
	my($byteblock, $whatisleft);
	my($workingbyte);
   
	$byteblock = '';
	# There will always be at least one byte in a block
	do 
	{
		$workingbyte = ord(substr($message,0,1));
		$message = substr($message,1);
		$byteblock .=  chr($workingbyte & 0x7F); # make sure to remove the high 8th bit
	}
	while ((not ($workingbyte & 0x80)) and length $message);

	if (length $message == 0) 
	{
		# Remove the high 8th bit is it is at the end of the message
		$byteblock .=  chr($workingbyte & 0x7F);
	}

	return ($byteblock, $message);
}

sub blob_select_file
{
	my ($db, $file_name) = @_;
	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB
	my $stmt = $db->prepare(q{select file_transfer_content from file_transfer where file_transfer_name = :file_name });
	$stmt->bind_param(":file_name", $file_name);

	$stmt->execute();
	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}

sub blob_insert
{
	my ($db, $sqlStmt, $buf, $name) = @_;
	my $LONG_RAW_TYPE=24;
	my $stmt = $db->prepare($sqlStmt);
	my %attrib;
	$attrib{'ora_type'} = $LONG_RAW_TYPE;
	$stmt->bind_param(":blob", $buf, \%attrib);
	$stmt->bind_param(":name", $name);
	$stmt->execute();
	if(defined $db->errstr)
	{
		print "blob_insert error: " . $db->errstr . "\n";
	}
	$stmt->finish();
}

sub send_init_email
{
	my ($DATABASE, $device_id, $version, $device_info, $terminal_info) = @_;

	if ($ReRix_Init{init_email_enabled})
	{
		my $subject = "ReRix Init V$version: new device initialization";		
		my $content = "$subject\n\n"
			. "Device ID: $device_id\n";	

		my $aref = $DATABASE->query(query => q{
			SELECT dt.device_type_desc, d.device_serial_cd, d.device_name, c.customer_name, l.location_name
			FROM device.device d
				INNER JOIN device.device_type dt ON dt.device_type_id = d.device_type_id
				INNER JOIN pss.pos p ON p.device_id = d.device_id
				INNER JOIN location.customer c ON c.customer_id = p.customer_id
				INNER JOIN location.location l ON l.location_id = p.location_id
			WHERE d.device_id = ?
			},
			values  => [$device_id]
		);

		if ($aref->[0])
		{
			$content .= "Device Type: " . $aref->[0]->[0] . "\n"
						. "Serial Number: " . $aref->[0]->[1] . "\n"
						. "EV Number: " . $aref->[0]->[2] . "\n"
						. "Customer: " . $aref->[0]->[3] . "\n"
						. "Location: " . $aref->[0]->[4] . "\n\n";
		}
		
		$content .= "Device Info: $device_info\n" if defined $device_info && length($device_info) > 0;
		$content .= "Terminal Info: $terminal_info\n" if defined $terminal_info && length($terminal_info) > 0;
		
		ReRix::Utils::send_email_detailed($ReRix_Init{init_email_addr_from}, $ReRix_Init{init_email_addr_from}, $ReRix_Init{init_email_addr_to}, $ReRix_Init{init_email_addr_to}, $subject, $content);
	}
}

1;
