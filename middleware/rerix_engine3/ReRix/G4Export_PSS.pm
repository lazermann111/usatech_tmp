#--------------------------------------------------------------------------------
#
# 	Description
# 		Provides functionality to export credit, passcard, cash, and dex data from 
#		middleware to Customer Reporting System via intermediate tables
#
#--------------------------------------------------------------------------------

package ReRix::G4Export_PSS;

use strict;
use warnings;
use ReRixConfig qw(%ReRix_G4Export);
use Time::Local;
use ReRix::Utils;
use USAT::Security::StringMask qw(mask_credit_card);
use USAT::Database::SensitiveData;

sub createG4ExportRec
{
	my @logs;
	my ($DATABASE, $device_id, $device_tran_cd, $machine_trans_no, $timestamp, $magstripe, $card_type, $amt, $vend_qty, $vend_col_bytes, $vended_items) = @_;
	
	if(not defined $timestamp)
	{
		# this is a live auth, need to lookup card information
		
		my $lookup_tran = $DATABASE->select(
				select_columns	=> "t.tran_received_raw_acct_data, ps.client_payment_type_cd, to_char(t.tran_start_ts, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
				table			=> 'pos p, pos_pta pp, payment_subtype ps, tran t',
				where_columns	=> [ 'p.pos_id = pp.pos_id', 'ps.payment_subtype_id = pp.payment_subtype_id', 'pp.pos_pta_id = t.pos_pta_id', 'p.device_id = ?', 't.tran_device_tran_cd = ?'],
				where_values	=> [$device_id, $device_tran_cd] );
				
		# we expect tran_start_ts is already adjusted to the timezone of the device
		
		if (defined $lookup_tran->[0][0])
		{
			$magstripe = $lookup_tran->[0][0];	
			$card_type = $lookup_tran->[0][1];
			
			my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
			my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $lookup_tran->[0][2]);
			$timestamp = Time::Local::timegm($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);
		}
		else
		{
			push @logs, "Could not find transaction $device_tran_cd for $device_id in tran table! Aborting export...";
			return (\@logs);
		}
	}

	# reporting system expects the amount in pennies
	$amt *= 100;
	
	my $ftype = 'MISC';
	
	# determine the kind of G4 record to build (CREDIT, MISC)
	if ($card_type =~ m/^(C|R)$/)
	{
		$ftype = 'CREDIT'
	}
	elsif ($card_type eq 'M')
	{
		$magstripe = 'Currency Vend';
	}
	
	my $lookup_device_info = $DATABASE->select(
			select_columns	=> "d.device_serial_cd, c.customer_name, l.location_name",
			table			=> 'device d, pos p, location.location l, customer c',
			where_columns	=> [ 'd.device_id = p.device_id', 'p.location_id = l.location_id', 'p.customer_id = c.customer_id', 'd.device_id = ?'],
			where_values	=> [$device_id] );

	if(not defined $lookup_device_info->[0][0])
	{
		push @logs, "Could not find device information for device_id  $device_id! Aborting export...";
		return (\@logs);
	}
	
	my $device_serial_cd 	= $lookup_device_info->[0][0];
	my $customer_name 		= $lookup_device_info->[0][1];
	my $location_name 		= $lookup_device_info->[0][2];

	$location_name =~ s/,//g;

	push @logs, "Device ID        : $device_id";
	push @logs, "Serial Number    : $device_serial_cd";
	push @logs, "Customer Name    : $customer_name";
	push @logs, "Location Name    : $location_name";
	push @logs, "Device Tran CD   : $device_tran_cd";
	push @logs, "Machine Trans No : $machine_trans_no";
	push @logs, "Tran Timestamp   : $timestamp";
	push @logs, "Tran Date/Time   : " . gmtime($timestamp);
	push @logs, "Amount           : $amt";
	push @logs, "Card Type        : $card_type";
	push @logs, "Card             : " . mask_credit_card($magstripe);
	push @logs, "G4 Category      : $ftype";
	
	# start building the G4 rec
	my $g4rec = "$device_serial_cd,$location_name,";
	my $g4rec_secured = $g4rec;

	# format the trandate,trantime info
	my ($sec, $min, $hr, $dy, $mon, undef, undef, undef, undef) = gmtime($timestamp);
	$mon = ($mon+1);
	
	$g4rec .= sprintf("%02d/%02d,%02d:%02d,", $mon, $dy, $hr, $min);
	$g4rec_secured .= $g4rec;
	
	# the G4 Category affects how we build the middle of the record
	if ($ftype eq 'CREDIT')
	{
		# add the apcode, sale, data1, merch, card, data2
		$g4rec .= sprintf("APGReRix,%05d,0000,E.179001415993577953,$magstripe,~,","$amt");
		$g4rec_secured .= sprintf("APGReRix,%05d,0000,E.179001415993577953,".mask_credit_card($magstripe).",~,","$amt");
		
		# add the startdate,starttime,data3,data4
		$g4rec .= sprintf("%02d/%02d,%02d:%02d,0000,~,", $mon, $dy, $min, $sec);
		$g4rec_secured .= $g4rec;
	}
	else	# passcard, cash
	{
		# add the card, sale, data3, data4
		$g4rec .= sprintf("$magstripe,%05d,0000,~,","$amt");
		$g4rec_secured .= sprintf(mask_credit_card($magstripe).",%05d,0000,~,","$amt");
		
		# add the stopdate,stoptime,data5,data6
		$g4rec .= sprintf("%02d/%02d,%02d:%02d,0000,~,", $mon, $dy, $min, $sec);
		$g4rec_secured .= $g4rec;
	}
			
	# -- build the vend column string in G4 format --
	# first, create an array containing the 'denormalized' vend list of columns
	my @vends;
	my $vend_counter = 0;
	foreach my $key (%$vended_items)
	{		
		if (defined($vended_items->{$key}{QTY}))
		{
			for (my $i = 0; $i < $vended_items->{$key}{QTY}; $i++)
			{
				$vend_counter++;
				# for each item vended create a placeholder in the array; $key is the 
				# column number we want to place in the final string
				push @vends, $key;
				push @logs, "Vend $vend_counter           : $key";
			}
		}
	}
	
	# finish building the record
	$g4rec .=  buildSrvCntString($vend_qty, @vends);
	$g4rec_secured .= $g4rec;
	
	# append machine trans no
	$g4rec .= ",$machine_trans_no";
	$g4rec_secured .= $g4rec;
	
	### write this data to the database (raw string parsed by insert trigger) ###
	my $insert_data_table;
	if ($ftype eq 'CREDIT')
	{
		$insert_data_table = 'export_credit';
	}
	elsif ($ftype eq 'MISC')	# passcard, cash
	{
		$insert_data_table = 'export_passaccess';
	}
	
	my $insert_data = $DATABASE->do(
		query	=> qq{INSERT INTO $insert_data_table (raw_data) VALUES (?)},
		values	=> [ USAT::Database::SensitiveData->new($g4rec, $g4rec_secured) ] );
	
	return (\@logs);
}

sub buildSrvCntString
{
	my ($vend_qty, @vends) = @_;
	# the first 4 chars are = 2 chars for total vend count + 2 chars for the first column vended
	my $vendstr = "~," . sprintf("%02d", $vend_qty) . sprintf("%02d",$vends[0]) . ",~,";
	my ($i, $j);
	for ($i=1; $i<10; )
	{
		for ($j=0; $j<2; $j++)
		{
			if (defined $vends[$i])
			{
				$vendstr .= sprintf("%02d",$vends[$i]);
			}
			else
			{
				$vendstr .= "00";
			}
			$i++;
			
		}
		if ($i < 10)
		{
			$vendstr .= ",~,";
		}
	}
	return $vendstr;	
	
}

1;
