package ReRix::InfoRequest;

use strict;
use Evend::ReRix::Shared;
use ReRixConfig;
use ReRix::Utils;
use POSIX qw(ceil);

my $log_format_width = $ReRix_global{log_format_key_width};

sub parse
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;

	my $message = MakeString($command_hashref->{inbound_command});

	my $reqnbr = unpack("C", substr($message,1,1));
	
	push @logs, "Request Number    : $reqnbr";

	my $response;

	if($reqnbr == 0) # Unix Time (84h)
	{
		push @logs, "Request           : Unix Time";
		push(@logs, @{&unix_time($DATABASE,$command_hashref)});
	}
	elsif($reqnbr == 1) # BCD Time (85h)
	{
		push @logs, "Request           : BCD Time";
		push(@logs, @{&bcd_time($DATABASE,$command_hashref)});
	}
	elsif($reqnbr == 2) # Configuration in Poke (88h)
	{
		push @logs, "Request           : Configuration Poke";
		push(@logs, @{&config_poke($DATABASE,$command_hashref)});
	}
	
	return(\@logs);
}

sub unix_time
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;

	my $message = MakeString($command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id};
	my $response_no = $command_hashref->{response_no}; 

	my $time_zone;
	
	if(defined $device_id)
	{
		# Use the device_id as a rerefence to lookup the time zone
	
		my $lookup_tz_ref =	$DATABASE->select(	
								table          => 'location.location, pos',
								select_columns => 'location.location_time_zone_cd',
								where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
								where_values   => [$device_id] );
								
		$time_zone = $lookup_tz_ref->[0][0];
	}
	
	if(not defined $time_zone)
	{
		$time_zone = 'EST';
		push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
	}
	else
	{
		push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
	}

	my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
	
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
	$Month = $Month+1;
    $Year  = $Year+1900;
	
	push @logs, "Time              : $Month/$Day/$Year $Hour:$Minute:$Second ";

	my $response = pack('CCN', $response_no, 0x84, $time);
	push @logs, "Response          : " . unpack("H*", $response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);
}

sub bcd_time
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;

	my $message = MakeString($command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id};
	my $response_no = $command_hashref->{response_no}; 
	
	my $time_zone;
	
	if(defined $device_id)
	{
		# Use the device_id as a rerefence to lookup the time zone
	
		my $lookup_tz_ref =	$DATABASE->select(	
								table          => 'location.location, pos',
								select_columns => 'location.location_time_zone_cd',
								where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
								where_values   => [$device_id] );
								
		$time_zone = $lookup_tz_ref->[0][0];
	}
	
	if(not defined $time_zone)
	{
		$time_zone = 'EST';
		push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
	}
	else
	{
		push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
	}


	my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
	
	my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
	my ($Year1, $Year2);

	$Month = $Month+1;
    $Year  = $Year+1900;
    
	$Year1 = substr($Year,0,2);
	$Year2 = substr($Year,2,2);
	
	# make sure these are all 2 characters long, if not pad with a zero
	#if(length($Second) < 2)	{	$Second = '0' . $Second;	}
	#if(length($Minute) < 2)	{	$Minute = '0' . $Minute;	}
	#if(length($Hour) < 2)	{	$Hour = '0' . $Hour;		}
	#if(length($Day) < 2)	{	$Day = '0' . $Day;			}
	#if(length($Month) < 2)	{	$Month = '0' . $Month;		}
	
	push @logs, "Time              : $Month/$Day/$Year $Hour:$Minute:$Second ";

	#my $response = pack('C9', $response_no, 0x85, $Hour, $Minute, $Second, $Month, $Day, $Year1, $Year2);
	my $response = pack('C9', $response_no, 0x85, $Hour, $Minute, $Second, $Month, $Day, $Year1, $Year2);
	#my $response = pack('CCH2H2H2H2H2H2H2', $response_no, 0x85, $Hour, $Minute, $Second, $Month, $Day, $Year1, $Year2);
	push @logs, "Response          : " . unpack("H*", $response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);
}

# This command should insert Poke commands into machine_command_pending table that will overwrite the 
# entire configuration in nv-ram.  
sub config_poke
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;

	my $response_no = $command_hashref->{response_no};
	my $msg_no = $command_hashref->{msg_no};
	my $machine_id = $command_hashref->{machine_id};
	my $device_type = $command_hashref->{device_type};
	
	push @logs, @{_config_poke($DATABASE, $machine_id, $device_type, $command_hashref)};

	my $response = pack("CH2C", $response_no, '2F', $msg_no);
	push @logs, "Response          : " . unpack("H*",$response);
	
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);	
}

sub _config_poke
{
	my (@logs);
	my ($DATABASE, $machine_id, $device_type, $command_hashref) = @_;
	
	my $raw_handle = $DATABASE->{handle};

	# get the config file from the database
	
	# use blobSelect because the column is a LONG and it won't come back otherwise
	my $poke_data = &blob_select_config($raw_handle, $machine_id);
	
	if(not defined $poke_data)
	{
		ReRix::Utils::send_alert("InfoRequest: Config            : Config not found for $machine_id!", $command_hashref);
		push @logs, "Config            : Config not found for $machine_id!" ;
		return(\@logs);
	}

	# Data in file_transfer_content coulumn is stored HEX encoded
	$poke_data = pack("H*", $poke_data);
	
	if($device_type =~ m/^(0|1)$/)
	{
		push @logs, "Poke              : This is an G4/G4 device!  Sending portions of complete config...";
		
		# remove any existing pokes other than the 3 below
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['C'],
							where_columns	=> ['machine_id = ?', 'data_type = ?', 'execute_order > ?', '(execute_cd = ? or (execute_cd = ? and execute_date < (sysdate-(90/86400))))'],
							where_values	=> [$machine_id, '88', '2', 'P', 'S'] );
		
		# we are sending the complete config except for communication settings
		#	0 	- 135	/ 2 = 0		length = 136
		#	142 - 283	/ 2 = 71	length = 142
		#	356 - 511	/ 2 = 178	length = 156

		push @logs, &create_poke(0, 136, $machine_id, 0, $DATABASE);
		push @logs, &create_poke(71, 142, $machine_id, 1, $DATABASE);
		push @logs, &create_poke(178, 156, $machine_id, 2, $DATABASE);
		
		# overwrite counters
		# bug #589 - don't send counters
		#push @logs, &create_poke(160, 36, $machine_id, 3, $DATABASE);
	}
	else
	{
		# remove any existing pokes
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['C'],
							where_columns	=> ['machine_id = ?', 'data_type = ?', '(execute_cd = ? or (execute_cd = ? and execute_date < (sysdate-(90/86400))))'],
							where_values	=> [$machine_id, '88', 'P', 'S'] );

		push @logs, "Poke              : Sending complete config...";
		
		my $poke_size = 200;

		# figure out how many Pokes it will take to transmit the entire file
		my $total_bytes = length($poke_data);
		my $num_parts = ceil($total_bytes/$poke_size);

		push @logs, "Poke Size         : $poke_size";
		push @logs, "Total Bytes       : $total_bytes";
		push @logs, "Num Parts         : $num_parts";

		# create each Poke and put it in the machine_command_pending table

		for(my $counter=0; $counter<$num_parts; $counter++)
		{
			my $pos = ($counter*$poke_size);
			my $chunk;
			if(($pos+$poke_size) > $total_bytes)
			{
				# this should be the last Poke
				$chunk = substr($poke_data, $pos);
			}
			else
			{
				$chunk = substr($poke_data, $pos, $poke_size);
			}

			my $memory_location = $pos;
			
			push @logs, "File Location     : $pos";
			push @logs, "Memory Location   : $memory_location";
			
			push @logs, &create_poke($memory_location, length($chunk), $machine_id, $counter++, $DATABASE);
		}
	}
	
	return(\@logs);
}

sub file_transfer_request
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};
	
	my $file_name = substr($message, 1);
	if(!defined $file_name || length($file_name) <= 0)
	{
		push @logs, "Invalid file transfer request file name: $file_name";
		return(\@logs);
	}
	
	push @logs, "File Name         : $file_name (" . length($file_name) . " characters)";
	
	my $cmd_file_transfer_start;
	my $packet_size;
	my $file_id;
	my $pending_id;
	my $transfer_id;
	my $execute_order = 1;

	if ($device_type =~ m/^(4|11|12)$/) # Sony, Kiosk, T2
	{
		$cmd_file_transfer_start = 'A4';
		$packet_size = 1024;
	}
	else
	{
		$cmd_file_transfer_start = '7C';
		$packet_size = 256;
	}
	
	# lookup the file_id in the database
	# allowing only requests for config files now to prevent possibly requesting a sensitive file
	my $lookup_file_ref = $DATABASE->select(
		table			=> 'file_transfer',
		select_columns	=> 'file_transfer_id',
		where_columns	=> [ 'file_transfer_name = ?', 'file_transfer_type_cd = ?' ],
		where_values	=> [ $file_name, '1' ]
	);
	
	if (!defined $lookup_file_ref->[0])
	{
		push @logs, "File Not Found    : $file_name";
		return(\@logs);
	}
	
	$file_id = $lookup_file_ref->[0][0];

	# check if there's already a transfer pending for this file
	my $check_exists_ref = $DATABASE->select(
		table			=> 'device_file_transfer dft, machine_command_pending mcp',
		select_columns	=> 'mcp.machine_command_pending_id, dft.device_file_transfer_id',
		where_columns	=> [ 'dft.device_id = ?', 'dft.file_transfer_id = ?', 'dft.device_file_transfer_status_cd = ?', 'dft.device_file_transfer_direct = ?', 'mcp.machine_id = ?', 'upper(mcp.data_type) = ?', 'dft.device_file_transfer_id = mcp.command and mcp.execute_cd in (?,?)' ],
		where_values	=> [ $device_id, $file_id, '0', 'O', $machine_id, $cmd_file_transfer_start, 'P', 'S' ]
	);

	if(defined $check_exists_ref->[0] && $check_exists_ref->[0][0] > 0)
	{
		$pending_id = $check_exists_ref->[0][0];
		$transfer_id = $check_exists_ref->[0][1];
		push @logs, "File Request      : A pending outgoing file transfer already exists for $file_name: $pending_id, $transfer_id";
	}
	else
	{
		# create a new device_file_transfer and pending command
		# get device_file_transfer_id from the sequence
		my $transfer_seq_ref = $DATABASE->select(	table			=> 'dual',
													select_columns	=> 'device.SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL' );
		
		if(!defined $transfer_seq_ref->[0])
		{
			push @logs, "Failed to query device_file_transfer sequence for a new ID!";
			return(\@logs);
		}

		$transfer_id = $transfer_seq_ref->[0][0];
		push @logs, "New Transfer ID   : $transfer_id";
		
		# get pending_id from the sequence
		my $pending_seq_ref = $DATABASE->select(	table			=> 'dual',
													select_columns	=> 'SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL' );
		
		if(!defined $pending_seq_ref->[0])
		{
			push @logs, "Failed to query machine_command_pending sequence for a new ID!";
			return(\@logs);
		}
		
		$pending_id = $pending_seq_ref->[0][0];
		push @logs, "New Pending ID    : $pending_id";

		# insert the device_file_transfer
		$DATABASE->insert(	table=> 'device_file_transfer',
							insert_columns=> 'device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size',
							insert_values=> [$transfer_id, $device_id, $file_id, 'O', '0', $packet_size]);

		# now insert a pending message for this new transfer
		$DATABASE->insert(	table=> 'machine_command_pending',
							insert_columns=> 'machine_command_pending_id, machine_id, data_type, command, execute_cd, execute_order',
							insert_values=> [$pending_id, $machine_id, $cmd_file_transfer_start, $transfer_id, 'P', $execute_order]);
	}
	
	# update the pending command to sent
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'message_number, execute_cd',
						update_values	=> [$response_no, 'S'],
						where_columns	=> ['machine_command_pending_id = ?'],
						where_values	=> [$pending_id] );

	# now start the file transfer
	push @logs, "Starting Requested File Transfer...";
	
	if($cmd_file_transfer_start eq '7C')
	{
		push @logs, "File Transfer     : $transfer_id";
		foreach(@{ReRix::FileTransferOutgoing::send_file($DATABASE, $command_hashref, $response_no, $pending_id, $transfer_id)})
		{
			push(@logs, $_);
		}
	}
	elsif($cmd_file_transfer_start eq 'A4')
	{
		push @logs, "File Transfer V1.1: $transfer_id";
		foreach(@{ReRix::FileTransferOutgoing::send_file_v1_1($DATABASE, $command_hashref, $response_no, $pending_id, $transfer_id)})
		{
			push(@logs, $_);
		}
	}
	
	return(\@logs);
}

sub blob_select_config
{
	my ($db, $machine_id) = @_;
	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB
	my $stmt = $db->prepare(q{select file_transfer_content from file_transfer where file_transfer_name = :file_name });
	$stmt->bind_param(":file_name", "$machine_id-CFG");

	$stmt->execute();

	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return $buffer;
}

sub create_poke
{
	# don't put actual poke data in the pending table, just the poke info.  We'll construct
	# the poke on the fly when the command is sent - this is done in case there are changes made to the
	# config file between the time we create the pokes when they are sent

	my ($addr, $length, $machine_id, $order, $DATABASE) = @_;
	my $poke = unpack("H*", pack('cNN', ord("B"), $addr, $length));
	
	my $data_type = '88';
	
	my $lookup_pending_ref = $DATABASE->select(
		table			=> 'machine_command_pending'
		,select_columns	=> 'count(1)'
		,where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?', 'execute_cd in (?,?)']
		,where_values	=> [$machine_id, $data_type, $poke, 'P', 'S']
	);
	if (not defined $lookup_pending_ref->[0]) 
	{
		return sprintf("%-".$log_format_width."s", "create_poke").": Failed to check for pending request for $machine_id in create_poke";
	}
	if ($lookup_pending_ref->[0][0] eq '0') 
	{
		$DATABASE->insert(
			table			=> 'machine_command_pending'
			,insert_columns	=> 'machine_id, data_type, command, execute_cd, execute_order'
			,insert_values	=> [$machine_id, $data_type, $poke, 'P', $order]
		);
		return sprintf("%-".$log_format_width."s", "POKE").": $machine_id, $data_type, $poke, P, $order";
	}
	else 
	{
		return sprintf("%-".$log_format_width."s", "POKE EXISTS").": $machine_id, $data_type, $poke, P, $order";
	}
}

1;
