#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		07/30/2003	pcowan		Creation of commands
# 1.10		09/20/2004	pcowan		Added support for MEI
# 1.11		11/01/2004	pcowan		Addded MEI device type to incoming Peek response update
# 1.12		11/15/2004	pcowan		Added Wavecom modem info peek parsing
# 1.13		12/07/2004	pcowan		Changed machine_command_pending update to only remove sent peeks 
# 1.14		07/01/2005	dkouznetsov	Added support for Kiosk and Sony devices
# 1.15	 	08/08/2005	dkouznetsov	Added gzip compression functionality
# 1.16		08/30/2005	pcowan		Changed gprs device modem update logic slightly

package ReRix::FileTransferIncoming;

use strict;
use ReRixConfig qw(%ReRix_FileTransferIncoming);
use Evend::ReRix::Shared;
use ReRix::Utils;
use USAT::Common::CallInRecord;
use ReRix::G4Export;
use USAT::App::ReRix::Const qw(BOOLEAN EVENT_STATE EVENT_TYPE_HOST_NUM FILE_TRANSFER_TYPE);

my $incoming_root = "$ReRix_FileTransferIncoming{data_root_path}/";
my $CMD_V1   = '1.0';
my $CMD_V1_1 = '1.1';

sub start
{
	my ($DATABASE, $command_hashref) = @_;
	return _start($DATABASE, $command_hashref, $CMD_V1);
}

sub start_v1_1
{
	my ($DATABASE, $command_hashref) = @_;
	return _start($DATABASE, $command_hashref, $CMD_V1_1);
}

sub _start
{
	my (@logs);
	my ($DATABASE, $command_hashref, $command_version) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 
	my $device_id = $command_hashref->{device_id}; 
	my $device_type = $command_hashref->{device_type}; 
	
	my (undef, $num_packets, $num_bytes, $group_num, $file_type, $file_name);
	
	if ($command_version eq $CMD_V1)
	{
		(undef, $num_packets, $num_bytes, $group_num, $file_type, $file_name) = unpack("CCNCCA*", $message);
	}
	elsif ($command_version eq $CMD_V1_1)
	{
		(undef, $num_packets, $num_bytes, $group_num, $file_type, $file_name) = unpack("CnNCCA*", $message);
	}
	
    push @logs, "Num Packets       : $num_packets";
    push @logs, "Num Bytes         : $num_bytes";
    push @logs, "Group Number      : $group_num";
    push @logs, "File Type         : $file_type";
    push @logs, "File Name         : $file_name";

	my $time_now = time();
	
	my $prefix = $incoming_root . "incoming_" . $machine_id . "_" .$group_num;

	&startDataFile($prefix);
	&startControlFile($prefix, time(), $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, "-1");
	push @logs, "Started Data and Control File; expecting $num_packets packets";

	if($device_type =~ /^(0|1|6)$/ && $file_type eq '2')
	{
		# this is the start of a Peek, which was requested from a pending message...
		# mark the pending message ack'd
		
		push @logs, "Peek Response     : Removing any Sent Peek requests";
		# we don't know the pending message ID, so just update any pending pokes
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['A'],
							where_columns	=> ['machine_id = ?', 'data_type = ?', 'execute_cd = ?'],
							where_values	=> [$machine_id, '87', 'S'] );
	}

	if($device_type =~ /^(0|1|6)$/ && $file_type eq '1')
	{
		# this could be the result of a Peek for the entire config
		# mark the pending message ack'd
		
		push @logs, "Config Transfer   : Removing any pending Peeks for entire config";
		# we don't know the pending message ID, so just update any pending pokes
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['A'],
							where_columns	=> ['machine_id = ?', 'data_type = ?', 'command = ?'],
							where_values	=> [$machine_id, '87', '4200000000000000FF'] );
	}

	if($device_type eq '5' && $file_type eq '4')
	{
		# if this is an eSuds device and the incoming file is an eSuds generic file, it may have been 
		# sent as a response to an eSuds file request message (9B)
		# so remove any 9B messages for this file name
		
		my $command = unpack("H*", $file_name);
		
		push @logs, "eSuds File        : Removing any requests for file: $file_name";
		# we don't know the pending message ID, so just update any pending pokes
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['A'],
							where_columns	=> ['machine_id = ?', 'data_type = ?', 'upper(command) = ?'],
							where_values	=> [$machine_id, '9B', uc($command)] );
	}

	# pack a response and save it
	my $response;
	if ($command_version eq $CMD_V1)
	{
		$response = pack("CCC", $response_no, 0x7D, $group_num);
	}
	elsif ($command_version eq $CMD_V1_1)
	{
		$response = pack("CCC", $response_no, 0xA5, $group_num);
	}
	push @logs, "Response          : " . unpack("H*", $response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
	
	return(\@logs);
}

sub transfer
{
	my ($DATABASE, $command_hashref) = @_;
	return _transfer($DATABASE, $command_hashref, $CMD_V1);
}

sub transfer_v1_1
{
	my ($DATABASE, $command_hashref) = @_;
	return _transfer($DATABASE, $command_hashref, $CMD_V1_1);
}

sub _transfer
{
	my (@logs);
	my ($DATABASE, $command_hashref, $command_version) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 
	my $device_type = $command_hashref->{device_type}; 
	my $ssn = $command_hashref->{SSN}; 

	my (undef, $group_num, $incoming_packet_num, $payload);
	
	if ($command_version eq $CMD_V1)
	{
		(undef, $group_num, $incoming_packet_num, $payload) = unpack("CCCa*", $message);
	}
	elsif ($command_version eq $CMD_V1_1)
	{
		(undef, $group_num, $incoming_packet_num, $payload) = unpack("CCna*", $message);
	}
	
	push @logs, "Group Number      : $group_num";
	push @logs, "Packet Number     : $incoming_packet_num";
	#push @logs, "Payload           : " . unpack("H*", $payload);
	
	my $prefix = $incoming_root . "incoming_" . $machine_id . "_" .$group_num;
	my ($start_time, $device_id, $stored_machine_id, $num_packets, $num_bytes, $stored_group_num, $file_type, $file_name, $stored_packet_num) = &readControlFile($prefix);
	if(not defined $start_time)
	{
		# problem reading control file
		ReRix::Utils::send_alert("FileTransferIncoming: ERROR: Problem reading control file for $prefix", $command_hashref);
		push @logs, "ERROR: Problem reading control file for $prefix";
		return(\@logs);
	}
	
	my ($decompress_flag, $client_file_name, $compressed_file_name, $update_device_settings, $overwrite_file) = (0, $file_name, $file_name, 0, 0);
	if ($device_type =~ /^(4|11)$/ && lc(substr($file_name, -3)) eq '.gz')
	{
		$client_file_name = substr($file_name, 0, -3);
		if ($file_type =~ /^(1|12)$/ || $client_file_name =~ /^(EportGPRSInfo)$/)
		{
			$decompress_flag = 1;
		}
	}
	
	push @logs, "File Name         : $file_name";
	
	my $ctrl_file = "$prefix.control";

	my $response;
	my $success = 0;
	
	if($stored_packet_num >= $incoming_packet_num)
	{
		# duplicate packet - we've already see this packet, so ack it but don't write the 
		# data and don't update control file
		push @logs, "Dupe packet       : $stored_packet_num >= $incoming_packet_num";
		if ($command_version eq $CMD_V1)
		{
			$response = pack("CCCC", $response_no, 0x7F, $group_num, $incoming_packet_num);
		}
		elsif ($command_version eq $CMD_V1_1)
		{
			$response = pack("CCCn", $response_no, 0xA7, $group_num, $incoming_packet_num);
		}
	}
	elsif($stored_packet_num == ($incoming_packet_num-1))
	{
		# next packet in sequence - append the data, update the control file to this packet 
		# num, ack this packet num
		push @logs, "Packet received   : " . ($incoming_packet_num+1) . " of " . $num_packets;
		if ($command_version eq $CMD_V1)
		{
			$response = pack("CCCC", $response_no, 0x7F, $group_num, $incoming_packet_num);
		}
		elsif ($command_version eq $CMD_V1_1)
		{
			$response = pack("CCCn", $response_no, 0xA7, $group_num, $incoming_packet_num);
		}
		&updateControlFile($prefix, $incoming_packet_num);
		&updateDataFile($prefix, $payload);
		$success = 1;
	}
	elsif($stored_packet_num < 0)
	{
		# we missed a packet, and have never received a packet - resend the file transfer start ACK
		push @logs, "Missed a transfer packet: $stored_packet_num < $incoming_packet_num";
		push @logs, "Resending File Transfer Start ACK";
		if ($command_version eq $CMD_V1)
		{
			$response = pack("CCC", $response_no, 0x7D, $group_num);
		}
		elsif ($command_version eq $CMD_V1_1)
		{
			$response = pack("CCC", $response_no, 0xA5, $group_num);
		}
	}
	else
	{
		# we missed a packet - don't write the data, don't update control file, ack the stored
		# packet num to get the client back in sync
		push @logs, "Missed a transfer packet: $stored_packet_num < $incoming_packet_num";
		if ($command_version eq $CMD_V1)
		{
			$response = pack("CCCC", $response_no, 0x7F, $group_num, $stored_packet_num);
		}
		elsif ($command_version eq $CMD_V1_1)
		{
			$response = pack("CCCn", $response_no, 0xA7, $group_num, $stored_packet_num);
		}
	}
	
	if((($incoming_packet_num+1) == $num_packets) && ($success == 1))
	{
		# This was the last packet.  
		# Packet numbers must start at zero.  
		# Write file to database.  
		# Cleanup temp files and directory
		
		push @logs, "Last packet       : Transfer Is Complete!";
		
		# Check if all the data was received
		
		my $data_file = "$prefix.data";
		my ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev, $size, $atime, $mtime, $ctime, $blksize, $blocks) = stat($data_file);
		
		if($num_bytes == $size)
		{
			push @logs, "Correct num bytes : $size";
		}
		else
		{
			push @logs, "WARNING: Received $size bytes, but expecting $num_bytes bytes";
		}
		
		if ($decompress_flag == 1)
		{
			push @logs, "Trying to decompress file $file_name";
		}
		
		# get the accumulated data
		my ($data, $readDataFileRC) = &readDataFile($prefix, $decompress_flag);
		
		if ($decompress_flag == 1)
		{
			if ($readDataFileRC eq 'DECOMPRESSION_ERROR')
			{
				push @logs, "Error decompressing file $file_name, saving compressed file";
			}
			else
			{
				push @logs, "Successfully decompressed file $file_name";	
			}
		}
		
		if ($device_type =~ /^(4|11)$/)
		{
			if ($file_type eq '1')
			{
				$file_name = "$machine_id-CFG";
				if ($readDataFileRC eq 'SUCCESS')
				{				
					$update_device_settings = 1;
				}
			}		
			elsif ($client_file_name =~ /^(EportGPRSInfo)$/)
			{
				$file_name = "$machine_id-GPRS";
				$overwrite_file = 1;
			}
		
			if ($decompress_flag == 1 && $readDataFileRC eq 'DECOMPRESSION_ERROR')
			{
				$file_name .= '.gz';
			}
		}
		
		if($file_type eq '12')
		{
			# email this file out; to addresses are listed on the first line, comma delimited
			# from address is on the second line
			# subject is on the 3rd line
			# the rest is message data
			
			my @data_array = split(/\n/, $data);
			my @to_emails_array = split(/,/, $data_array[0]);
			my $from_email = substr($data_array[1], 0, 60);
			my $subject = substr($data_array[2], 0, 100);
			my $message = substr(join("\n", @data_array[3..$#data_array]), 0, 4000);

			foreach my $to_addr (@to_emails_array)
			{
				$to_addr = substr($to_addr, 0, 60);
				$DATABASE->insert(	table=> 'ob_email_queue',
						insert_columns=> 'OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG',
						insert_values=> [$from_email, $from_email, $to_addr, $to_addr, $subject, $message]);
				
				push @logs, "Auto-Email File   : Inserted email to $to_addr: $subject";
			}
			
			rename($ctrl_file, ($ctrl_file .'.'. time()));
			rename($data_file, ($data_file .'.'. time()));

			push @logs, "Response          : " . unpack("H*", $response);
			ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
			return(\@logs);	
		}

		if($device_type =~ /^(0|1|6)$/ && $file_type eq '1')
		{
			# special case for G4
			# check if there are pending Pokes for this unit and don't save if there are - if we 
			# store the incoming config but we have pokes pending, we will poke back the config 
			# data it just transfered
			
			# for changes to take effect on the unit, the operator must call it in first then make changes
			
			my $check_for_pokes_ref = $DATABASE->select(
									table			=> 'machine_command_pending',
									select_columns	=> 'count(1)',
									where_columns	=> ['machine_id = ?', 'data_type = ?', '(execute_cd = ? or (execute_cd = ? and execute_date < (sysdate-(90/86400))))'],
									where_values	=> [$machine_id, '88', 'P', 'S'] );
			
			my $poke_count = $check_for_pokes_ref->[0][0];
			if($poke_count > 0)
			{
				push @logs, "Gx Config         : Exists... NOT Overwriting $file_name; Pokes already exists for current config";
				
				if($device_type =~ /^(0|1)$/)
				{
					# in this case we want to pull out the communication settings from this file and overwrite the value in the database
					# unless there are pokes pending for those memory areas specifically
					
					
				
				}
				
				rename($ctrl_file, ($ctrl_file .'.'. time()));
				rename($data_file, ($data_file .'.'. time()));
				
				push @logs, "Response          : " . unpack("H*", $response);
				ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
				return(\@logs);	
			}
		}
		
		if($device_type =~ /^(0|1)$/ && $file_type eq '2')
		{
			if($data =~ /^USA-/)
			{
				# special case for Peeks from a G4/G5 - if it's sending version info, write it to the 
				# device_setting table so an admin can see it on the website
				
				my $version = $data;
				
				# trim spaces
				$version =~ s/\s+$//;
				$version =~ s/^\s+//;
				
				my $setting_name = 'Firmware Version';
				&ReRix::Utils::store_setting($device_id, $setting_name, $version, $DATABASE);
				push @logs, "Stored Setting    : $setting_name = $version";
				
				if($data =~ /^USA-G(51v5|x)/)
				{
					# peek for bootloader and diagnostic versions
					$DATABASE->insert(
						table			=> 'machine_command_pending',
						insert_columns	=> 'machine_id, data_type, command, execute_cd, execute_order',
						insert_values	=> [$machine_id, '87', '4400807E9000000021', 'P', 1]);
						
					push @logs, "Peek              : Bootloader and Diagnostic Version";
				}
			}
			else
			{
				my $last_peek_query = $DATABASE->query(
					query	=> q|
						select command from (
						    select command
	    					from machine_command_pending_hist
	    					where machine_id = ?
	    					and data_type = ?
	    					and execute_cd = ?
	    					order by execute_date desc
						)
						where rownum = 1|,
					values	=> [$machine_id, '87', 'A']
				);
				
				if(defined $last_peek_query->[0]->[0])
				{
					my $last_peek = $last_peek_query->[0]->[0];
					
					if($last_peek eq '4400807E9000000021')	# Boot Loader/Diagnostic App Rev
					{
						my ($bootloader_val, $diag_val) = &ReRix::Utils::parse_bootloader_diag_rev($data);
						
						my $bootloader_name = 'Bootloader Rev';
						&ReRix::Utils::store_setting($device_id, $bootloader_name, $bootloader_val, $DATABASE);
						push @logs, "Stored Setting    : $bootloader_name = $bootloader_val";
						
						my $diag_name = 'Diagnostic App Rev';
						&ReRix::Utils::store_setting($device_id, $diag_name, $diag_val, $DATABASE);
						push @logs, "Stored Setting    : $diag_name = $diag_val";
					}
				}
				else
				{
					push @logs, "Last peek not found : Unable to parse peek data";
				}
			}
		}
		
		# check if the file exists
		my $file_transfer_id;

		my $check_exists_ref = $DATABASE->select(
					table			=> 'file_transfer',
					select_columns	=> 'file_transfer_id',
					where_columns	=> ['file_transfer_name = ?'],
					where_values	=> [$file_name] );
					
		if($device_type =~ /^(0|1|4|6|11|12)$/ && $file_type eq '1')
		{
			# special case for g4's, Sony, MEI, Kiosk and T2 - if it just transfered it's config, overwrite the current config 
			# file machine_id-CFG with this new config file		
			$overwrite_file = 1;
		}
		
		if($overwrite_file == 1 && (defined $check_exists_ref->[0][0]))
		{			
			$file_transfer_id = $check_exists_ref->[0][0];
			push @logs, "Gx Config         : Exists... Overwriting file_transfer_id $file_transfer_id";
		}
		else
		{
			# Get a new File_Transfer ID
			my $file_transfer_seq_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'device.SEQ_FILE_TRANSFER_ID.NEXTVAL' );
	
			if(not $file_transfer_seq_ref->[0])
			{
				push @logs, "Failed to query file_transfer sequence for a new ID!";
				return(\@logs);
			}
			
			$file_transfer_id = $file_transfer_seq_ref->[0][0];
			push @logs, "New Transfer ID   : $file_transfer_id";
		
			# insert the file, don't insert the content because we need to use blob_insert
			$DATABASE->insert(	table=> 'device.file_transfer',
								insert_columns=> 'file_transfer_id, file_transfer_name, file_transfer_type_cd',
								insert_values=> [$file_transfer_id, $file_name, $file_type]);
		}

		# check for new GPRS modem format information, need to do it here to store the previously created file_id
		if(($device_type =~ /^(0|1)$/ && $file_type eq '2') || ($device_type =~ /^(11)$/ && $file_name eq "$machine_id-GPRS"))
		{
			my ($csq, $cgsn, $cimi, $cgmr, $cgmm, $cgmi, $cnum, $ccid);
			if($data =~ /$ReRix_FileTransferIncoming{MODEM_INFO_REGEX}/ || $device_type =~ /^(11)$/)
			{
				($csq, $cgsn, $cimi, $cgmr, $cgmm, $cgmi, $cnum, $ccid) = &ReRix::Utils::parse_multitech_modem_info_new($data);
			}
			elsif($data =~ /^CSQ:/)
			{
				($csq, $cgsn, $cimi, $cgmr, $cgmm, $cgmi, $cnum, $ccid) = &ReRix::Utils::parse_multitech_modem_info($data);
			}
			
			if($ccid)
			{
				push @logs, "Multitech Info    : CSQ = $csq, CGSN = $cgsn, CIMI = $cimi, CGMR = $cgmr, CGMI = $cgmi, CNUM = $cnum, CCID = $ccid";
				
				if(length($ccid) == 20)
				{
					my $check_gprs_device_ref = $DATABASE->select(
								table			=> 'gprs_device',
								select_columns	=> 'device_id',
								where_columns	=> ['iccid = ?'],
								where_values	=> [$ccid] );
								
					if(!defined $check_gprs_device_ref->[0][0] || $check_gprs_device_ref->[0][0] != $device_id)
					{
						push @logs, "Multitech Info    : Updating gprs_device table with new/changed SIM assignment";
						$DATABASE->do(
							query	=> q{ 
								UPDATE gprs_device
								SET device_id = ?, gprs_device_state_id = ?, assigned_by = ?, assigned_ts = sysdate, device_type_name = ?, device_firmware_name = ?, imei = ?, rssi = ?, last_file_transfer_id = ? 
								WHERE iccid = ? },
							values => [$device_id, 5, 'SYSTEM', $cgmi, $cgmr, $cgsn, $csq, $file_transfer_id, $ccid] );
					}
					else
					{
						push @logs, "Multitech Info    : Updating gprs_device table with recent modem information";
						$DATABASE->do(
							query	=> q{ 
								UPDATE gprs_device
								SET device_type_name = ?, device_firmware_name = ?, imei = ?, rssi = ?, last_file_transfer_id = ?
								WHERE iccid = ? },
							values => [$cgmi, $cgmr, $cgsn, $csq, $file_transfer_id, $ccid] );
					}
				}
				else
				{
					push @logs, "Multitech Info    : Invalid ICCID; Skipping Update";
				}
			}
		}
		
		if($device_type =~ /^(4|11|12)$/)
		{
			# if this is a Sony, Kiosk or T2 device, the file may have been 
			# sent as a response to a file request message (9B)
			# so remove any 9B messages for this file name

			push @logs, "Kiosk File        : Removing any requests for file: $client_file_name";
			# we don't know the pending message ID, so just update any pending file transfer requests
			$DATABASE->update(	table			=> 'machine_command_pending',
								update_columns	=> 'execute_cd',
								update_values	=> ['A'],
								where_columns	=> ['machine_id = ?', 'data_type = ?', 'upper(command) = upper(?)'],
								where_values	=> [$machine_id, '9B', unpack("H*", $client_file_name)] );
								

			# remove any pending File Transfer Start messages for this file name
			push @logs, "Kiosk File        : Removing any File Transfer Start commands for file: $client_file_name";
			my $update_cmd_stmt = $raw_handle->prepare("update machine_command_pending set execute_cd = 'A' where machine_command_pending_id in (select mcp.machine_command_pending_id from file_transfer ft, device_file_transfer dft, machine_command_pending mcp where upper(ft.file_transfer_name) in (upper(:client_file_name), upper(:compressed_file_name)) and dft.file_transfer_id = ft.file_transfer_id and dft.device_id = :device_id and dft.device_file_transfer_status_cd = 0 and dft.device_file_transfer_direct = 'O' and mcp.machine_id = :machine_id and mcp.data_type = 'A4' and dft.device_file_transfer_id = mcp.command and mcp.execute_cd = 'P')");
			$update_cmd_stmt->bind_param(":client_file_name", $client_file_name);
			$update_cmd_stmt->bind_param(":compressed_file_name", $compressed_file_name);
			$update_cmd_stmt->bind_param(":device_id", $device_id);
			$update_cmd_stmt->bind_param(":machine_id", $machine_id);
			$update_cmd_stmt->execute();
			$update_cmd_stmt->finish();
			
			#update device settings
			if ($update_device_settings == 1)
			{
				$DATABASE->do(
					query	=> q{
						DELETE FROM device.device_setting WHERE device_id = ? AND device_setting_parameter_cd <> 'New Merchant ID'
					},
					values	=> [
						$device_id
					]
				);

				my @config_lines = split(/\n/, $data);
				foreach my $config_line (@config_lines)
				{
					chomp($config_line);
					my ($config_param, $config_value) = split(/=/, $config_line, 2);	
					($config_param, $config_value) = (substr($config_param, 0, 60), substr($config_value, 0, 200));
					
					$config_param =~ s/^\s+|\s+$//s;

					if (length($config_param) > 0)
					{
						$config_value =~ s/^\s+|\s+$//s;
					
						if (length($config_value) == 0)
						{
							$config_value = ' ';
						}

						my $config_param_ref = $DATABASE->select(
												table			=> 'device.device_setting_parameter',
												select_columns	=> 'count(1)',
												where_columns	=> ['device_setting_parameter_cd = ?'],
												where_values	=> [$config_param] );

						if($config_param_ref->[0][0] eq '0')
						{
							$DATABASE->insert(
										table			=> 'device.device_setting_parameter',
										insert_columns	=> 'device_setting_parameter_cd',
										insert_values	=> [$config_param]);
						}

						$DATABASE->insert(
									table			=> 'device.device_setting',
									insert_columns	=> 'device_id, device_setting_parameter_cd, device_setting_value',
									insert_values	=> [$device_id, $config_param, $config_value]);
					}
				}
			}
		}
		
		my $event_id;
		
		# 01/09/04 - pwc - Added call_in_record logging
		if($file_type =~ /^(0|3)$/)
		{
			USAT::Common::CallInRecord::dex_received($DATABASE, $command_hashref, $command_hashref->{session_id}, length($data));
			
			if ($file_type eq FILE_TRANSFER_TYPE->{DEX_FILE_FOR_FILL_TO_FILL} && $file_name =~ m/^($machine_id)-DEX FILL-([a-fA-F0-9]{8})$/)
			{
				my $event_device_tran_cd = unpack("N", pack("H*", $2));
				$event_id = ReRix::Utils::create_event($DATABASE, \@logs, $command_hashref, EVENT_TYPE_HOST_NUM->{FILL}, EVENT_STATE->{INCOMPLETE}, $event_device_tran_cd, BOOLEAN->{FALSE}, BOOLEAN->{FALSE});
			}
		}
		elsif($file_type eq '1')
		{
			USAT::Common::CallInRecord::device_sent_config($DATABASE, $command_hashref, $command_hashref->{session_id});
		}
		
		# insert doesn't work right for blobs, so do an update with using blobInsert
		&blob_insert($raw_handle, "update device.file_transfer set file_transfer_content = :blob where file_transfer_id = $file_transfer_id", $data);
		
		my $device_file_transfer_id = $DATABASE->sequence_next(sequence => 'device.seq_device_file_transfer_id');
		
		# insert the device_file_transfer
		$DATABASE->insert(	table=> 'device.device_file_transfer',
							insert_columns=> 'device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd',
							insert_values=> [$device_file_transfer_id, $device_id, $file_transfer_id, 'I', '1']);
		
		if (defined $event_id)
		{
			$DATABASE->insert(	table => 'device.device_file_transfer_event',
								insert_columns => 'event_id, device_file_transfer_id',
								insert_values => [$event_id, $device_file_transfer_id] );
		}
		
		push @logs, "Stored file       : $file_transfer_id $file_name";
				
		# do we really want to delete the files?
		# just rename them for now
		
		rename($ctrl_file, ($ctrl_file .'.'. time()));
		rename($data_file, ($data_file .'.'. time()));
	}
	
	push @logs, "Response          : " . unpack("H*", $response);
	ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
	
	return(\@logs);
}

sub kill_transfer
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 
	my $device_id = $command_hashref->{device_id}; 

	my (undef, $group_num) = unpack("CC", $message);

	push @logs, "Group Number      : $group_num";
	
	my $prefix = $incoming_root . "incoming_" . $machine_id . "_" .$group_num;
	my $ctrl_file = "$prefix.control";
	my $data_file = "$prefix.data";		
	
	rename($ctrl_file, ($ctrl_file .'.'. time()));
	rename($data_file, ($data_file .'.'. time()));

	# pack a response and save it
	my $response = pack("CCC", $response_no, 0x81, $group_num);

	push @logs, "Response          : " . unpack("H*", $response);
	
	ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
	
	return(\@logs);
}

sub readControlFile
{
	my ($prefix) = @_;
	my $ctrl_file = "$prefix.control";
	if(not -f $ctrl_file)
	{
		return (undef);
	}
	
	open(CTRLFILE, "<$ctrl_file");
	
	my $start_time = readline(CTRLFILE);
	my $device_id = readline(CTRLFILE);
	my $machine_id = readline(CTRLFILE);
	my $num_packets = readline(CTRLFILE);
	my $num_bytes = readline(CTRLFILE);
	my $group_num = readline(CTRLFILE);
	my $file_type = readline(CTRLFILE);
	my $file_name = readline(CTRLFILE);
	my $packet_num = readline(CTRLFILE);
	
	chomp($start_time);
	chomp($device_id);
	chomp($machine_id);
	chomp($num_packets);
	chomp($num_bytes);
	chomp($group_num);
	chomp($file_type);
	chomp($file_name);
	chomp($packet_num);
	
	close(CTRLFILE);
	
	return ($start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num);
}

sub startControlFile
{
	my ($prefix, $start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num) = @_;

	my $ctrl_file = "$prefix.control";
	if(-f $ctrl_file)
	{
		rename($ctrl_file, ($ctrl_file .'.'. time()));
	}
	
	# write the control file to hold temp data
	open(CTRLFILE, ">$ctrl_file");
	print CTRLFILE "$start_time\n";		# start time of transfer
	print CTRLFILE "$device_id\n";		# device id
	print CTRLFILE "$machine_id\n";		# machine EV number
	print CTRLFILE "$num_packets\n";	# number of packets to expect
	print CTRLFILE "$num_bytes\n";		# number of bytes to expect
	print CTRLFILE "$group_num\n";		# transfer group number
	print CTRLFILE "$file_type\n";		# file type ID
	print CTRLFILE "$file_name\n";		# real file name - may include fully qualified path
	print CTRLFILE "$packet_num\n";		# last packet num transfered
	close(CTRLFILE);
}

sub updateControlFile
{
	my ($prefix, $new_packet_num) = @_;
	
	my ($start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num) = readControlFile($prefix);
	if(not defined $start_time)
	{
		return undef;
	}
	
	$packet_num = $new_packet_num;
	
	my $ctrl_file = "$prefix.control";
	open(CTRLFILE, ">$ctrl_file");
	print CTRLFILE "$start_time\n";		# start time of transfer
	print CTRLFILE "$device_id\n";		# device ID
	print CTRLFILE "$machine_id\n";		# machine EV number
	print CTRLFILE "$num_packets\n";	# number of packets to expect
	print CTRLFILE "$num_bytes\n";		# number of bytes to expect
	print CTRLFILE "$group_num\n";		# transfer group number
	print CTRLFILE "$file_type\n";		# file type ID
	print CTRLFILE "$file_name\n";		# real file name - may include fully qualified path
	print CTRLFILE "$packet_num\n";		# last packet num transfered
	close(CTRLFILE);
}

sub startDataFile
{
	my ($prefix) = @_;

	my $data_file = "$prefix.data";
	if(-f $data_file)
	{
		rename($data_file, ($data_file .'.'. time()));
	}
	
	open(DATAFILE, ">$data_file");
	close(DATAFILE);
}

sub updateDataFile
{
	my ($prefix, $payload) = @_;

	my $data_file = "$prefix.data";
	
	open(DATAFILE, ">>$data_file");
	#binmode(DATAFILE);
	#syswrite(DATAFILE, $payload, length($payload), -1);
	print DATAFILE $payload;
	close(DATAFILE);
}

sub readDataFile
{
	my ($prefix, $decompress_flag) = @_;
	my $data_file = "$prefix.data";	
	my $buf;
	my $return_code = 'SUCCESS';
	
	if ($decompress_flag == 1)
	{
		#$buf = readpipe("/usr/bin/gzip -dcq -S . $data_file");
		my $compressed_file = $data_file . '.gz';
		rename($data_file, $compressed_file);
		system("/usr/bin/gzip -dfnq $compressed_file");
		
		if (!(-f $data_file))
		{
			rename($compressed_file, $data_file);
			$return_code = 'DECOMPRESSION_ERROR';
		}
	}
	
	open(DATAFILE, "<$data_file");
	#binmode(DATAFILE);
	#sysread(DATAFILE, $buf, 5000000);
	read(DATAFILE, $buf, 5000000);
	close(DATAFILE);
	
	return ($buf, $return_code);
}

sub blob_insert
{
	my ($raw_handle, $sql, $blob) = @_;
	
	my $LONG_RAW_TYPE=24;
	my %attrib;
	$attrib{'ORA_TYPE'} = $LONG_RAW_TYPE;

	my $stmt = $raw_handle->prepare($sql);
	$stmt->bind_param(":blob", unpack("H*", $blob), \%attrib); 
	$stmt->execute();
	
	if(defined $raw_handle->errstr)
	{
		print "blob_insert error: " . $raw_handle->errstr . " ($sql)\n";
	}
}

1;
