#--------------------------------------------------------------------------------
# Change History
#
# Version  	Date		Programmer	Description
# -------	----------	----------	--------------------------------------------
# 1.00		07/30/2003	pcowan		Creation of commands
# 1.01		07/20/2005	dkouznetsov	Added support for Kiosk and Sony devices
# 1.02	 	08/08/2005	dkouznetsov	Added gzip compression functionality

package ReRix::FileTransferOutgoing;

use strict;
use ReRixConfig qw(%ReRix_FileTransferOutgoing);
use Evend::ReRix::Shared;
use ReRix::Utils;
use POSIX;

my $outgoing_root = "$ReRix_FileTransferOutgoing{data_root_path}/";
my $default_packet_size = $ReRix_FileTransferOutgoing{default_packet_size};

my $CMD_V1   = '1.0';
my $CMD_V1_1 = '1.1';

sub send_file
{
	my ($DATABASE, $command_hashref, $response_no, $pending_message_id, $device_file_transfer_id) = @_;
	return _send_file($DATABASE, $command_hashref, $response_no, $pending_message_id, $device_file_transfer_id, $CMD_V1);
}

sub send_file_v1_1
{
	my ($DATABASE, $command_hashref, $response_no, $pending_message_id, $device_file_transfer_id) = @_;
	return _send_file($DATABASE, $command_hashref, $response_no, $pending_message_id, $device_file_transfer_id, $CMD_V1_1);
}

sub _send_file
{
	my (@logs);
	my ($DATABASE, $command_hashref, $response_no, $pending_message_id, $device_file_transfer_id, $command_version) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $device_id = $command_hashref->{device_id};
	my $device_type = $command_hashref->{device_type};
	
	# lookup transfer in device_file_transfer table
	my $lookup_transfer_ref = $DATABASE->select(
								table			=> 'device_file_transfer',
								select_columns	=> 'file_transfer_id, device_file_transfer_pkt_size, device_file_transfer_group_num',
								where_columns	=> [ 'device_file_transfer_id = ?' ],
								where_values	=> [ $device_file_transfer_id ] );
	
	if(not $lookup_transfer_ref->[0])
	{
		push @logs, "No device_file_transfer found for ID $device_file_transfer_id";
		return(\@logs);	
	}
	
	my $transfer_id = $device_file_transfer_id;
	my $file_id = $lookup_transfer_ref->[0][0];
	my $packet_size = $lookup_transfer_ref->[0][1];
	my $group_num = $lookup_transfer_ref->[0][2];
	
	if(not defined $packet_size)
	{
		$packet_size = $default_packet_size;
	}
	
	if(not defined $group_num)
	{
		# get a group number from the sequence
		my $lookup_group_num_ref = $DATABASE->select(
									table			=> 'dual',
									select_columns	=> 'SEQ_DEVI_FILE_TRANS_GROUP_NUM.nextval' );
		
		if(not $lookup_group_num_ref->[0])
		{
			push @logs, "Failed to query sequence SEQ_DEVI_FILE_TRANS_GROUP_NUM for nextval!";
			return(\@logs);	
		}
	
		$group_num = $lookup_group_num_ref->[0][0];
		
		# now set the group num in the database so if it gets transfered again, it gets the same group num
		$DATABASE->update(	table			=> 'device_file_transfer',
							update_columns	=> 'device_file_transfer_group_num',
							update_values	=> [$group_num],
							where_columns	=> [ 'device_file_transfer_id = ?' ],
							where_values	=> [ $device_file_transfer_id ] );

		push @logs, "New Group Num     : $group_num";
	}
	else
	{
		push @logs, "Stored Group Num  : $group_num";
	}
	
	push @logs, "Transfer ID       : $transfer_id";
	push @logs, "File ID           : $file_id";
	push @logs, "Packet Size       : $packet_size";

	my $lookup_file_ref = $DATABASE->select(
								table			=> 'file_transfer',
								select_columns	=> 'file_transfer_name, file_transfer_type_cd',
								where_columns	=> [ 'file_transfer_id = ?' ],
								where_values	=> [ $file_id ] );
	
	if(not $lookup_file_ref->[0])
	{
		push @logs, "No file found for ID $file_id";
		return(\@logs);	
	}
	
	my $file_name = $lookup_file_ref->[0][0];
	my $file_type = $lookup_file_ref->[0][1];

	push @logs, "File Name         : $file_name";
	push @logs, "File Type         : $file_type";

	# get the file content
	my $file_content_hex = &blob_select($raw_handle, $file_id);
	if(not defined $file_content_hex)
	{
		ReRix::Utils::send_alert("FileTransferOutgoing: Failed to retrieve blob file content for $file_id", $command_hashref);
		push @logs, "Failed to retrieve blob file content for $file_id";
		return(\@logs);	
	}
	
	# bug fix - if hex size if off there may be an extra 0x00 at the end that appears to be left in by perl chop it off
	#if(((length($file_content_hex)/2) == 1) && substr($file_content_hex, (length($file_content_hex)-1), 1) == 0)
	if((length($file_content_hex)%2) == 1)
	{
		push @logs, "Chopping last character from HEX to fix Perl bug!";
		$file_content_hex = substr($file_content_hex, 0, (length($file_content_hex)-1));
	}

	push @logs, "Hex Size          : " . length($file_content_hex);

	my $file_content = pack("H*", $file_content_hex);
	push @logs, "Packed Size       : " . length($file_content);

	my $num_bytes = length($file_content);
	push @logs, "Bytes             : $num_bytes";

	# calculate how many packets we will chop the file into
	my $num_packets = ceil($num_bytes/$packet_size);
	push @logs, "Num Packets       : $num_packets";
	
	my $prefix = $outgoing_root . "outgoing_" . $machine_id . "_" . $group_num;
	push @logs, "Prefix            : $prefix";
	
	# write the file to temp directory
	if ($device_type =~ /^(4|11)$/ && lc(substr($file_name, -3)) ne '.gz')
	{
		push @logs, "Trying to compress file $file_name";

		if (&startDataFile($prefix, $file_content, 1) eq 'COMPRESSION_ERROR')
		{
			push @logs, "Unable to compress file $file_name, sending uncompressed file";
		}
		else
		{
			push @logs, "Successfully compressed file $file_name";
			$file_name .= '.gz';
			open(DATAFILE, "<$prefix.data");
			binmode(DATAFILE);

			my $compressed_buffer;
			$num_bytes = sysread(DATAFILE, $compressed_buffer, 5000000);
			push @logs, "Compressed Bytes  : $num_bytes";

			$num_packets = ceil($num_bytes/$packet_size);
			push @logs, "New Num Packets   : $num_packets";
		}
	}
	else
	{
		&startDataFile($prefix, $file_content, 0);
	}
	
	push @logs, "Transfer Info     : Sending $num_bytes bytes in $num_packets packets";
	
	&startControlFile($prefix, time(), $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, "-1", $file_id, $transfer_id, $pending_message_id, $packet_size);
	
	my $response;
	if ($command_version eq $CMD_V1)
	{
		$response = pack("CCCNCCA*", $response_no, 0x7C, $num_packets, $num_bytes, $group_num, $file_type, $file_name);
	}
	elsif ($command_version eq $CMD_V1_1)
	{
		$response = pack("CCnNCCA*", $response_no, 0xA4, $num_packets, $num_bytes, $group_num, $file_type, $file_name);
	}
	push @logs, "Response          : " . unpack("H*", $response);
				
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	return(\@logs);
}

sub start_ack
{
	my ($DATABASE, $command_hashref) = @_;
	return _start_ack($DATABASE, $command_hashref, $CMD_V1);
}

sub start_ack_v1_1
{
	my ($DATABASE, $command_hashref) = @_;
	return _start_ack($DATABASE, $command_hashref, $CMD_V1_1);
}

sub _start_ack
{
	my (@logs);
	my ($DATABASE, $command_hashref, $command_version) = @_;
	my $raw_handle = $DATABASE->{handle};
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 
	my $msg_no = $command_hashref->{msg_no}; 
	
	# extract the file group num
	my $group_num = ord(substr($message, 1,1));
	
	my $prefix = $outgoing_root . "outgoing_" . $machine_id . "_" . $group_num;
	push @logs, "Prefix            : $prefix";
	
	# read the control file	
	my ($start_time, $device_id, $stored_machine_id, $num_packets, $num_bytes, $stored_group_num, $file_type, $file_name, $stored_packet_num, $file_id, $transfer_id, $pending_message_id, $packet_size) = &readControlFile($prefix);

	# get the first chunk of data to send	
	my $packet_num = 0;
	my $payload = &readDataChunk($prefix, $packet_num, $packet_size);
	
	push @logs, "Packet Number     : $packet_num";
	push @logs, "Payload Length    : " . length($payload);
	push @logs, "File Name         : $file_name";
	push @logs, "Pending Message ID: $pending_message_id";
	
	if(not defined $payload)
	{
		ReRix::Utils::send_alert("Failed to read data from: $prefix", $command_hashref);
		push @logs, "Failed to read data from: $prefix";
		return(\@logs);
	}
	
	# touch the execute_date on machine_command_pending, so that if the transfer takes longer than 5 minutes,
	# we don't try to start it again
	my $update_pending_stmt = $raw_handle->prepare('UPDATE machine_command_pending SET execute_date = sysdate WHERE machine_command_pending_id = :pending_id');
	$update_pending_stmt->bind_param(":pending_id", $pending_message_id);
	$update_pending_stmt->execute();
	
	if(defined $raw_handle->errstr)
	{
		push @logs, "Warning! Failed to update machine_command_pending.execute_date: $raw_handle->errstr";
	}
	
	$update_pending_stmt->finish();
	
	my $response;
	if ($command_version eq $CMD_V1)
	{
		$response = pack("CCCCA*", $response_no, 0x7E, $group_num, $packet_num, $payload);
	}
	elsif ($command_version eq $CMD_V1_1)
	{
		$response = pack("CCCnA*", $response_no, 0xA6, $group_num, $packet_num, $payload);
	}
	push @logs, "Response          : " . unpack("H*", $response);
				
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	
	return(\@logs);
}

sub transfer_ack
{
	my ($DATABASE, $command_hashref) = @_;
	return _transfer_ack($DATABASE, $command_hashref, $CMD_V1);
}

sub transfer_ack_v1_1
{
	my ($DATABASE, $command_hashref) = @_;
	return _transfer_ack($DATABASE, $command_hashref, $CMD_V1_1);
}

sub _transfer_ack
{
	my (@logs);
	my ($DATABASE, $command_hashref, $command_version) = @_;
	my $raw_handle = $DATABASE->{handle};

	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 

	my $group_num = ord(substr($message, 1, 1));
	my $incoming_packet_num;
	
	if ($command_version eq $CMD_V1)
	{
		$incoming_packet_num = ord(substr($message, 2, 1));
	}
	elsif ($command_version eq $CMD_V1_1)
	{
		$incoming_packet_num = unpack("n", substr($message, 2, 2));
	}
	
	push @logs, "Group Number      : $group_num";
	push @logs, "Acked Packet      : $incoming_packet_num";

	my $packet_num = ($incoming_packet_num+1);
	my $prefix = $outgoing_root . "outgoing_" . $machine_id . "_" . $group_num;
	my ($start_time, $device_id, $stored_machine_id, $num_packets, $num_bytes, $stored_group_num, $file_type, $file_name, $stored_packet_num, $file_id, $transfer_id, $pending_message_id, $packet_size) = &readControlFile($prefix);
	
	push @logs, "File Name         : $file_name";
	push @logs, "Pending Message ID: $pending_message_id";

	if($packet_num >= $num_packets)
	{
		# Transfer is complete
		push @logs, "Transfer          : Complete ($packet_num/$num_packets)";
		
		$DATABASE->update(	table			=> 'device_file_transfer',
							update_columns	=> 'device_file_transfer_status_cd',
							update_values	=> ['1'],
							where_columns	=> ['device_file_transfer_id = ?'],
							where_values	=> [$transfer_id] );
							
		# this is kinda wierd, but now that the transfer is finished, mark this message as acked
		$DATABASE->update(	table			=> 'machine_command_pending',
							update_columns	=> 'execute_cd',
							update_values	=> ['A'],
							where_columns	=> ['machine_command_pending_id = ?'],
							where_values	=> [$pending_message_id] );

		my $ctrl_file = "$prefix.control";
		my $data_file = "$prefix.data";
		rename($ctrl_file, ($ctrl_file .'.'. time()));
		rename($data_file, ($data_file .'.'. time()));
		
		return(\@logs);
	}
	else
	{
		# touch the execute_date on machine_command_pending, so that if the transfer takes longer than 5 minutes,
		# we don't try to start it again
		my $update_pending_stmt = $raw_handle->prepare('UPDATE machine_command_pending SET execute_date = sysdate WHERE machine_command_pending_id = :pending_id');
		$update_pending_stmt->bind_param(":pending_id", $pending_message_id);
		$update_pending_stmt->execute();
		
		if(defined $raw_handle->errstr)
		{
			push @logs, "Warning! Failed to update machine_command_pending.execute_date: $raw_handle->errstr";
		}
		
		$update_pending_stmt->finish();
	}

	push @logs, "Transfer          : Continuing ($packet_num/$num_packets)";
	
	my $payload = &readDataChunk($prefix, $packet_num, $packet_size);
	
	push @logs, "Prefix            : $prefix";
	push @logs, "Packet Number     : $packet_num";
	push @logs, "Payload Length    : " . length($payload);

	if(not defined $payload)
	{
		ReRix::Utils::send_alert("Failed to read data from: $prefix", $command_hashref);
		push @logs, "Failed to read data from: $prefix";
		return(\@logs);
	}
	
	my $response;
	if ($command_version eq $CMD_V1)
	{
		$response = pack("CCCCA*", $response_no, 0x7E, $group_num, $packet_num, $payload);
	}
	elsif ($command_version eq $CMD_V1_1)
	{
		$response = pack("CCCnA*", $response_no, 0xA6, $group_num, $packet_num, $payload);
	}
	push @logs, "Response          : " . unpack("H*", $response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	
	return(\@logs);
}

sub kill_transfer_ack
{
	my (@logs);
	my ($DATABASE, $command_hashref) = @_;
	
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $machine_id = $command_hashref->{machine_id}; 
	my $response_no = $command_hashref->{response_no}; 

	my $group_num = ord(substr($message, 1, 1));
	
	push @logs, "Group Number      : $group_num";

	my $prefix = $outgoing_root . "outgoing_" . $machine_id . "_" . $group_num;
	my ($start_time, $device_id, $stored_machine_id, $num_packets, $num_bytes, $stored_group_num, $file_type, $file_name, $stored_packet_num, $file_id, $transfer_id, $pending_message_id, $packet_size) = &readControlFile($prefix);
	
	push @logs, "Prefix            : $prefix";
	push @logs, "Transfer          : Killed";
	
	# update device_file_transfer mark transfer as 0 to it will send again 
	$DATABASE->update(	table			=> 'device_file_transfer',
						update_columns	=> 'device_file_transfer_status_cd',
						update_values	=> ['1'],
						where_columns	=> ['device_file_transfer_id = ?'],
						where_values	=> [$transfer_id] );
						
	# update machine_command_pending mark pending command as P it will send again 
	$DATABASE->update(	table			=> 'machine_command_pending',
						update_columns	=> 'execute_cd',
						update_values	=> ['P'],
						where_columns	=> ['machine_command_pending_id = ?'],
						where_values	=> [$pending_message_id] );

	my $ctrl_file = "$prefix.control";
	my $data_file = "$prefix.data";
	rename($ctrl_file, ($ctrl_file .'.'. time()));
	rename($data_file, ($data_file .'.'. time()));
	
	# pack a response and save it
	my $response = pack("CCC", $response_no, 0x81, $group_num);

	push @logs, "Response          : " . unpack("H*", $response);
						
	ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
	
	return(\@logs);
}

sub startDataFile
{
	my ($prefix, $file_content, $compress_flag) = @_;
	my $return_code = 'SUCCESS';

	my $data_file = "$prefix.data";
	if(-f $data_file)
	{
		rename($data_file, ($data_file .'.'. time()));
	}
	
	open(DATAFILE, ">$data_file");
	print DATAFILE $file_content;
	close(DATAFILE);
	
	if ($compress_flag == 1)
	{
		system("/usr/bin/gzip -9fnq $data_file");
		
		my $compressed_file = $data_file . '.gz';
		if (-f $compressed_file)
		{
			rename($compressed_file, $data_file);
		}
		else
		{
			$return_code = 'COMPRESSION_ERROR';
		}
	}
	
	return $return_code;
}

sub readDataChunk
{
	my ($prefix, $packet_num, $packet_size) = @_;
	my $seek_to = ($packet_num * $packet_size);
	my $data_file = "$prefix.data";
	open(DATAFILE, "<$data_file");
	binmode(DATAFILE);
	my $payload;
	sysseek(DATAFILE, $seek_to, 0);
	sysread(DATAFILE, $payload, $packet_size);
	close(DATAFILE); 
	return $payload;
}

sub readControlFile
{
	my ($prefix) = @_;
	my $ctrl_file = "$prefix.control";
	if(not -f $ctrl_file)
	{
		return (undef);
	}
	
	open(CTRLFILE, "<$ctrl_file");
	
	my $start_time = readline(CTRLFILE);
	my $device_id = readline(CTRLFILE);
	my $machine_id = readline(CTRLFILE);
	my $num_packets = readline(CTRLFILE);
	my $num_bytes = readline(CTRLFILE);
	my $group_num = readline(CTRLFILE);
	my $file_type = readline(CTRLFILE);
	my $file_name = readline(CTRLFILE);
	my $packet_num = readline(CTRLFILE);
	my $file_id = readline(CTRLFILE);
	my $transfer_id = readline(CTRLFILE);
	my $pending_message_id = readline(CTRLFILE);
	my $packet_size = readline(CTRLFILE);
	
	chomp($start_time);
	chomp($device_id);
	chomp($machine_id);
	chomp($num_packets);
	chomp($num_bytes);
	chomp($group_num);
	chomp($file_type);
	chomp($file_name);
	chomp($packet_num);
	chomp($file_id);
	chomp($transfer_id);
	chomp($pending_message_id);
	chomp($packet_size);
	
	close(CTRLFILE);
	
	return ($start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num, $file_id, $transfer_id, $pending_message_id, $packet_size);
}

sub startControlFile
{
	my ($prefix, $start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num, $file_id, $transfer_id, $pending_message_id, $packet_size) = @_;

	my $ctrl_file = "$prefix.control";
	if(-f $ctrl_file)
	{
		rename($ctrl_file, ($ctrl_file .'.'. time()));
	}
	
	# write the control file to hold temp data
	open(CTRLFILE, ">$ctrl_file");
	print CTRLFILE "$start_time\n";			# start time of transfer
	print CTRLFILE "$device_id\n";			# device id
	print CTRLFILE "$machine_id\n";			# machine EV number
	print CTRLFILE "$num_packets\n";		# number of packets to expect
	print CTRLFILE "$num_bytes\n";			# number of bytes to expect
	print CTRLFILE "$group_num\n";			# transfer group number
	print CTRLFILE "$file_type\n";			# file type ID
	print CTRLFILE "$file_name\n";			# real file name - may include fully qualified path
	print CTRLFILE "$packet_num\n";			# last packet num transfered
	print CTRLFILE "$file_id\n";			# database ID of file in file_transfer table
	print CTRLFILE "$transfer_id\n";		# database ID of transfer in device_file_transfer table
	print CTRLFILE "$pending_message_id\n";	# database ID of the pending message in machine_command_pending table
	print CTRLFILE "$packet_size\n";		# packet size
	close(CTRLFILE);
}

sub updateControlFile
{
	my ($prefix, $new_packet_num) = @_;
	
	my ($start_time, $device_id, $machine_id, $num_packets, $num_bytes, $group_num, $file_type, $file_name, $packet_num, $file_id, $transfer_id, $pending_message_id, $packet_size) = readControlFile($prefix);
	if(not defined $start_time)
	{
		return undef;
	}
	
	$packet_num = $new_packet_num;
	
	my $ctrl_file = "$prefix.control";
	open(CTRLFILE, ">$ctrl_file");
	print CTRLFILE "$start_time\n";			# start time of transfer
	print CTRLFILE "$device_id\n";			# device ID
	print CTRLFILE "$machine_id\n";			# machine EV number
	print CTRLFILE "$num_packets\n";		# number of packets to expect
	print CTRLFILE "$num_bytes\n";			# number of bytes to expect
	print CTRLFILE "$group_num\n";			# transfer group number
	print CTRLFILE "$file_type\n";			# file type ID
	print CTRLFILE "$file_name\n";			# real file name - may include fully qualified path
	print CTRLFILE "$packet_num\n";			# last packet num transfered
	print CTRLFILE "$file_id\n";			# database ID of file in file_transfer table
	print CTRLFILE "$transfer_id\n";		# database ID of transfer in device_file_transfer table
	print CTRLFILE "$pending_message_id\n"; # database ID of the pending message in machine_command_pending table
	print CTRLFILE "$packet_size\n";		# packet size
	close(CTRLFILE);
}

sub blob_select
{
	my ($db, $file_id) = @_;
	my ($blob, $buffer);
	
	$db->{LongReadLen}=5000000;  # Make sure buffer is big enough for BLOB
	my $stmt = $db->prepare(q{select file_transfer_content from file_transfer where file_transfer_id = :file_id});
	$stmt->bind_param(":file_id", $file_id);
	
	$stmt->execute();
	
	$blob = $stmt->fetchrow;
	$stmt->finish();

	return $blob;	

	#while ($blob = $stmt->fetchrow)
	#{
	#	print "BLOB = \"" . $blob . "\"\n";
	#	$buffer = $buffer . $blob;
	#}
	#$stmt->finish();

	#return $buffer;
}

1;
