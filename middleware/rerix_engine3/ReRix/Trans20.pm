package ReRix::Trans20;

use strict;
use ReRixConfig;
use Evend::ReRix::Shared;
use ReRix::AuthClient;
use Evend::CardUtils;
use ReRix::G4Export;
use USAT::Common::CallInRecord;
use ReRix::ESuds;
use USAT::POS::Config;
use USAT::POS::Const ();
use USAT::POS (USAT::POS::Config::HANDLERS);
use Time::Local;
use ReRix::Utils;
use USAT::Security::StringMask qw(mask_credit_card);
use USAT::POS::API::PTA::Util;
use USAT::Math::Currency;
use USAT::App::ReRix::Const qw(BOOLEAN CARD_READER_TYPE EVENT_DETAIL_TYPE EVENT_STATE EVENT_TYPE_HOST_NUM);

use constant {
	CMD_V2		=> 2.0,
	CMD_V2_BCD	=> '2.0_bcd',
	CMD_V2_1	=> 2.1,
	CMD_V2_2	=> 2.2,
	CMD_V3		=> 3.0,
	CMD_V3_1	=> 3.1,
	CMD_V3_BCD	=> '3.0_bcd',
	CMD_PM_V1	=> '1.0_pm'
};

use constant TRANSACTION_AMOUNT_SUMMARY 	=> 201;
use constant EPORT_GENERIC_VEND_TLI_ID 		=> 200;
use constant SONY_GENERIC_VEND_TLI_ID 		=> 202;

sub parse_auth_v2 { return _parse_auth(@_, CMD_V2); }
sub parse_auth_v3 { return _parse_auth(@_, CMD_V3); }
sub parse_auth_v3_1 { return _parse_auth(@_, CMD_V3_1); }
sub parse_permission_req_v1 { return _parse_auth(@_, CMD_PM_V1); }

sub parse_net_batch_v2 { return _parse_net_batch(@_, CMD_V2); }
sub parse_net_batch_v2_1 { return _parse_net_batch(@_, CMD_V2_1); }

sub parse_local_batch_v2 { return _parse_local_batch(@_, CMD_V2); }
sub parse_local_batch_v2_bcd { return _parse_local_batch(@_, CMD_V2_BCD); }
sub parse_local_batch_v2_2 { return _parse_local_batch(@_, CMD_V2_2); }

sub parse_cash_sale_v2_bcd { return _parse_cash_sale(@_, CMD_V2_BCD); }
sub parse_cash_sale_v3 { return _parse_cash_sale(@_, CMD_V3); }
sub parse_cash_sale_v3_bcd { return _parse_cash_sale(@_, CMD_V3_BCD); }

sub _parse_auth ($$$)
{
	my ($DATABASE, $command_hashref, $command_version) = @_;
	my @logs;
	
	### validate that command version is supported ###
	unless ($command_version eq CMD_V2 || $command_version eq CMD_V3 || $command_version eq CMD_V3_1 || $command_version eq CMD_PM_V1)
	{
		push @logs, "".(caller(0))[3].": unsupported message version $command_version";
		return(\@logs);
	}
	push @logs, "Command Version   : $command_version";
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};

	my ($machine_trans_no, $trans_id, $card_type, $auth_amt, $pin_len, $pin_data, $card, $magstripe, $card_reader_type, $key_id_len, $key_id, $decrypted_len, $encrypted_len, $encrypted);
	if ($command_version eq CMD_V2)
	{
		if($device_type =~ m/^(4)$/)
		{
			# sony send the trans ID in little-endian order
			(undef, $trans_id, $card_type, $auth_amt, $magstripe) = unpack("CVaH6A*", $message);
			push @logs, "LE Generated ID   : $trans_id";
			
			# sony machine_trans_no is different format
			$machine_trans_no = "U:$SSN:$trans_id";
		}
		else
		{
			(undef, $trans_id, $card_type, $auth_amt, $magstripe) = unpack("CNaH6A*", $message);
			push @logs, "BE Generated ID   : $trans_id";
			$machine_trans_no = "X:$machine_id:$trans_id";
		}

		# :-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
		$auth_amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $auth_amt)))));
		$auth_amt = sprintf("%.02f", ($auth_amt * .01));

		# if it's an eSuds device, break off and do my own thing
		if($device_type eq '5')
		{
			my ($res, $approved) = ReRix::ESuds::authorize($DATABASE, $command_hashref, $trans_id, $card_type, $auth_amt, undef, $magstripe);
			push @logs, @{$res};

			# Auth response command is a hex '60' (decimal 96)
			my $response = pack("CCNC", $response_no, 96, $trans_id, $approved == 1 ? 1 : 0);
			push @logs, "Response          : " . unpack("H*", $response);								
			ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

			return(\@logs);
		}
	}
	elsif ($command_version eq CMD_V3)
	{
		# G4/eSuds sends the trans ID in big-endian order, Kiosk and Sony also send trans ID in big-endian order starting in Auth V3
		(undef, $trans_id, $card_type, $auth_amt, $pin_len, $magstripe) = unpack("CNaNCA*", $message);
		($pin_data, $magstripe) = (substr($magstripe, 0, $pin_len), substr($magstripe, $pin_len, length $magstripe));
		push @logs, "BE Generated ID   : $trans_id";
		$machine_trans_no = "X:$machine_id:$trans_id";
		$auth_amt = sprintf("%.02f", ($auth_amt * .01));

		# if it's an eSuds device, break off and do my own thing
		if($device_type eq '5')
		{
			my ($res, $approved, $approved_amt) = ReRix::ESuds::authorize($DATABASE, $command_hashref, $trans_id, $card_type, $auth_amt, $pin_data, $magstripe);
			push @logs, @{$res};

			# Auth response command is a hex 'A1' (decimal 161)
			my $response = (defined $approved_amt && ($approved == 0 || $approved == 1)) || $approved == 2
				? pack("CCNCN", $response_no, 161, $trans_id, $approved, int($approved_amt * 100 + 0.5))	#round to nearest cent
				: pack("CCNC", $response_no, 161, $trans_id, $approved);
			push @logs, "Response          : " . unpack("H*", $response);
			ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

			return(\@logs);
		}
	}
	elsif ($command_version eq CMD_V3_1)
	{
		(undef, $trans_id, $card_type, $auth_amt, $card_reader_type) = unpack("CNaNC", $message);
		push @logs, "BE Generated ID   : $trans_id";
		
		if ($card_reader_type != CARD_READER_TYPE->{MAGTEK_MAGNESAFE})
		{
			$magstripe = "<ERROR:INVALID_CARD_READER>";
			push @logs, "Received invalid card reader type: $card_reader_type";
		}
		else
		{
			push @logs, "Card reader type  : $card_reader_type";
			$machine_trans_no = "X:$machine_id:$trans_id";
			$auth_amt = sprintf("%.02f", ($auth_amt * .01));

			$decrypted_len = unpack("C", substr($message, 11, 1));
			$key_id_len = unpack("C", substr($message, 12, 1));
			$key_id = substr($message, 13, $key_id_len);
			$encrypted_len = unpack("C", substr($message, 13 + $key_id_len, 1));
			$encrypted = substr($message, 14 + $key_id_len, $encrypted_len);

			$magstripe = ReRix::Utils::DUKPT_decrypt(\@logs, $key_id, $encrypted, $decrypted_len);
			if (!defined $magstripe || $magstripe !~ m/^[\x20-\x7E]+$/)
			{
				$magstripe = "<ERROR:DECRYPTION_FAILURE>";
				push @logs, "Card data decryption failed!";
			}			
		}
	}
	elsif ($command_version eq CMD_PM_V1)
	{
		(undef, $trans_id, $card_type, $pin_len, $magstripe) = unpack("CNaCA*", $message);
		
		($pin_data, $magstripe) = (substr($magstripe, 0, $pin_len), substr($magstripe, $pin_len, length $magstripe));
		push @logs, "BE Generated ID   : $trans_id";
		$machine_trans_no = "X:$machine_id:$trans_id";
		
		$auth_amt = 0;
		
		# translate entry method codes to card type codes
		if($card_type  =~ m/^(B|M)$/)
		{
			# we don't handle these now but may need to in the future
			push @logs, "Received unhandled entry method! : $card_type";
		}
		elsif($card_type eq 'C')
		{
			# translate contactless type to Special RFID
			$card_type = 'P';
		}
		elsif($card_type eq 'S')
		{
			# translate magnetic stripe type to Special
			$card_type = 'S';
		}
		else
		{
			push @logs, "Received unrecognized entry method! : $card_type";
		}
	}

	### parse magstripe data ###
	my $card_number;
	if($card_type =~ m/^(S|C|R|P)$/)
	{
		my $cr = Evend::CardUtils->new();
		$card =  $cr->cleanSwipeData($magstripe);

		if( substr($card, 0, 1) eq '%' )	{ $card = substr( $card, 1 ); }
		if( substr($card, -2, 1) eq '?' )	{ $card = substr( $card, 0, -2 ); }
		elsif( substr($card, -1, 1) eq '?' ){ $card = substr( $card, 0, -1 ); }
	
		my $cardreader = Evend::CardUtils->new();
		($card_number, undef, undef, undef) = $cardreader->parse($card);
	}
	elsif($card_type =~ m/^(E|I)$/)
	{
		$card_number = $card;
	}
	else
	{
		push @logs, "Received unrecognized card type! : $card_type";
	}
	
	### miscellaneous logging ###
	push @logs, "Card Type         : $card_type";
	push @logs, "Card              : ".mask_credit_card(defined $card_number ? $card_number : $card);
	push @logs, "Amount            : $auth_amt";

	### handle timestamp adjustments, if required ###
	my $timestamp;
	if(defined $device_id)
	{
		# Use the device_id as a rerefence to lookup the time zone

		my $lookup_tz_ref = $DATABASE->select(	
			table          => 'location.location, pos',
			select_columns => 'location.location_time_zone_cd',
			where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
			where_values   => [$device_id] );

		my $time_zone = $lookup_tz_ref->[0][0];

		push @logs, "Adjusting Timestamp to compensate for Time Zone differences...";

		if(not defined $time_zone)
		{
			$time_zone = 'EST';
			push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
		}
		else
		{
			push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
		}

		push @logs, "Server Time       : " . localtime();
		$timestamp = ReRix::Utils::getAdjustedTimestamp($time_zone);
		push @logs, "Adjusted Time     : " . gmtime($timestamp);
	}

	### try POS handler first; try transd/speciald second ###
	my $posh = USAT::POS->new($DATABASE);
	my ($auth_code, $trans_no);
	my $return_code;
	my $approved_response_cd;
	my $approved_amt;
	if (defined $device_id)
	{
		my $approved_flag;
		my $error_message;
		my $acct_entry_method = $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__cash
		? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__manual
		: $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__credit_card || $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__special_card
			? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__magnetic_stripe
			: $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__rfid_credit_card || $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__rfid_special_card
				? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__contactless
				: USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__unspecified;
		my $authority_tran_cd;
		my $authority_auth_ref_cd;
		my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($timestamp);
		$Month += 1;
		$Year += 1900;
		my $start_ts = "$Month/$Day/$Year $Hour:$Minute:$Second";
		$auth_code = '';
		my $db_tran_id;

		### call POS to authorize the tran ###
		$posh->authorize(
			$magstripe
			,undef		#pin_data--always null
			,$device_id
			,$trans_id
			,$card_type
			,$auth_amt
			,$start_ts	#must be form MM/DD/YYYY HH24::MI::SS
			,'N'
			,$magstripe
			,$acct_entry_method
			,\$approved_flag
			,\$approved_amt
			,\$authority_tran_cd
			,\$authority_auth_ref_cd
			,\$db_tran_id
			,\$return_code
			,\$error_message
		);

		push @logs, "POS Processor     : authorize(".mask_credit_card($magstripe).", undef, $device_id, $trans_id, $card_type, $auth_amt, $start_ts, N, ".mask_credit_card($magstripe).", $acct_entry_method, ...)";
		push @logs, "POS Return Code   : $return_code";
		push @logs, "POS Error Message : $error_message" if defined $error_message;
		push @logs, "POS Approved Flag : $approved_flag";
		
		### determine result action ###
		if (!defined $return_code
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_id_not_found
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_type_id_unknown
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__card_type_unknown
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__cardtype_not_accepted_at_pos)
		{	#transd/speciald failover cases
			push @logs, "POS Processor     : authorize failed! return_code = $return_code ($error_message)! switching back to transd/speciald processing...";
		}
		elsif ($approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__success)	#success
		{
			$approved_response_cd = 1;
			$auth_code = 'A';
			$trans_no = $db_tran_id;
		}
		elsif ($approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__success_conditional
			&& $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__insufficient_balance)	#conditionally approved (due to insufficient funds)
		{
			if ($command_version eq CMD_V2 || $command_version eq CMD_PM_V1)
			{
				$approved_response_cd = 0;
				$auth_code = 'E';
				push @logs, "POS Processor     : authorize conditional success, but handling as declined (message response does not support conditional auth).";
			}
			elsif ($command_version eq CMD_V3 || $command_version eq CMD_V3_1)
			{
				$approved_response_cd = 2;
				$auth_code = 'A';
				push @logs, "POS Processor     : authorize conditional success ($error_message)";
			}
			$trans_no = $db_tran_id;
		}
		elsif ($approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__decline
			|| $approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__decline_permanent)	#decline or perm decline (currently mean the same in supported client responses)
		{
			$approved_response_cd = 0;
			if ($command_version eq CMD_V3 || $command_version eq CMD_V3_1)
			{
				if ($return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__invalid_pin_entry)
				{
					$approved_response_cd = 4;
					push @logs, "POS Processor     : invalid pin detected ($error_message)";
				}
				else
				{
					$approved_amt = $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__insufficient_balance ? 0 : undef;	#decline $0 amt ==> insuf funds; decline undef amt, ==> generic decline
				}
			}
			$auth_code = 'E';
			$trans_no = $db_tran_id;
		}
		elsif ($approved_flag eq USAT::POS::Const::PKG_AUTH_RESULT_GLOBALS__failure)	#failure
		{
			$approved_response_cd = $command_version eq CMD_V3 || $command_version eq CMD_V3_1 ? 3 : 0;
			$auth_code = 'E';
			$trans_no = $db_tran_id;
		}
		else
		{
			push @logs, "POS Processor     : authorize failed! return_code = $return_code ($error_message)!";
			$approved_response_cd = 0;
			$auth_code = 'E';
			$trans_no = $db_tran_id;
		}
	}
	
	### connect to transd/speciald, if necessary ###
	if (!defined $return_code 
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_id_not_found
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_type_id_unknown
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__card_type_unknown
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__cardtype_not_accepted_at_pos)
	{
		push @logs, "POS Processor     : successfully passed transaction handling to transd/speciald";
		push @logs, "Machine Trans ID  : $machine_trans_no";

		if($card_type eq 'R')
		{
			push @logs, "RFID Credit Card  : Substituting C card type for R";
			$card_type = 'C';
		}
		elsif($card_type eq 'P')
		{
			push @logs, "RFID Special Card : Substituting S card type for P";
			$card_type = 'S';
		}

		my $trans = ReRix::AuthClient->connect();
		$trans->send("t6,$machine_id,$card,$auth_amt,$card_type,$machine_trans_no,$timestamp,$magstripe");
		$auth_code = $trans->get_code();
		$trans_no = $trans->authorization_number();
		$trans->disconnect();
	}
	
	### authority response logging ###
	push @logs, "Auth Code         : $auth_code";
	push @logs, "Trans No          : $trans_no";
	
	### prepare message response ###
	my $response;
	if ($command_version eq CMD_V2)
	{
		if($device_type =~ m/^(4)$/)
		{
			# sony send the trans ID in little-endian order
			$response = pack("CCVC", $response_no, 96, $trans_id, ($auth_code eq 'A' ? 1:0));
		}
		else
		{
			$response = pack("CCNC", $response_no, 96, $trans_id, ($auth_code eq 'A' ? 1:0));
		}
	}
	elsif ($command_version eq CMD_V3 || $command_version eq CMD_V3_1)
	{
		unless (defined $approved_response_cd)	#set code (for transd/speciald result) if approved_response_cd not yet set
		{
			$approved_response_cd = defined $trans_no && $trans_no ne '' ? $auth_code eq 'A' ? 1 : 0 : 3;
		}
		# g4 sends the trans ID in big-endian order, Kiosk and Sony also send trans ID in big-endian order starting in Auth V3
		$response = (defined $approved_amt && ($approved_response_cd == 0 || $approved_response_cd == 1)) || $approved_response_cd == 2
			? pack("CCNCN", $response_no, 161, $trans_id, $approved_response_cd, int($approved_amt * 100 + 0.5))	#round to nearest cent--assumes currency is in 2 decimal place format
			: pack("CCNC", $response_no, 161, $trans_id, $approved_response_cd);
	}
	elsif ($command_version eq CMD_PM_V1)
	{
		unless (defined $approved_response_cd)	#set code (for transd/speciald result) if approved_response_cd not yet set
		{
			$approved_response_cd = defined $trans_no && $trans_no ne '' ? $auth_code eq 'A' ? 1 : 0 : 3;
		}
		
		if($approved_response_cd =~ m/^(1|2)$/)
		{
			my $action_cd;
			my $action_param;
			
			my $permission_action_query = $DATABASE->query(
				query	=> q|
					select pa.permission_action_id, a.action_id, dta.device_type_action_cd, a.action_name, a.action_param_type_cd, a.action_param_size
					from tran t, pss.consumer_acct_permission cap, pss.permission_action pa, device.action a, device.device_type_action dta
                    where t.consumer_acct_id = cap.consumer_acct_id
                    and cap.permission_action_id = pa.permission_action_id
                    and pa.action_id = a.action_id
                    and a.action_id = dta.action_id
                    and dta.device_type_id = ?
					and t.tran_id = ?
					order by cap.consumer_acct_permission_order|,
				values	=> [$device_type, $trans_no]
			);
			
			if(defined $permission_action_query->[0]->[0])
			{
				$action_cd = $permission_action_query->[0]->[2];

				my $permission_action_id = $permission_action_query->[0]->[0];
				my $action_name = $permission_action_query->[0]->[3];
				my $action_param_type = $permission_action_query->[0]->[4];
				my $action_param_size = $permission_action_query->[0]->[5];

				push @logs, "Permission ID     : $permission_action_id";
				push @logs, "Action CD         : $action_cd ($action_name)";
				push @logs, "Param Type/Size   : $action_param_type / $action_param_size";
				
				if($action_param_type eq 'B')
				{
					push @logs, "Action Param Type : B - Bitmap";
					
					my $bitmap_size = ($action_param_size*8);
					
					my @bitmap = ();
					
					for(my $i=0; $i<$bitmap_size; $i++)
					{
						push(@bitmap, 0);
					}
					
					my $action_param_query = $DATABASE->query(
						query	=> q|
							select ap.action_param_id, ap.action_param_cd, ap.action_param_name
							from pss.permission_action pa, pss.permission_action_param pap, device.action_param ap
							where pa.permission_action_id = pap.permission_action_id
							and pap.action_param_id = ap.action_param_id
							and pa.permission_action_id = ?
							order by pap.permission_action_param_order|,
						values	=> [$permission_action_id]
					);
					
					for my $action_param_arr_ref (@$action_param_query)
					{
						my $action_param_id = $action_param_arr_ref->[0];
						my $action_param_cd = $action_param_arr_ref->[1];
						my $action_param_name = $action_param_arr_ref->[2];
						
						push @logs, "Action Param      : $action_param_cd ($action_param_name)";
						
						$bitmap[($bitmap_size-$action_param_cd-1)] = 1;
					}
					
					my $bitmap_str = join('', @bitmap);
					$action_param = ReRix::Utils::bin2dec($bitmap_str);
					
					push @logs, "Param Bitmap      : $bitmap_str ($action_param)";
				}
				elsif($action_param_type eq 'N')
				{
					push @logs, "Action Param Type : N - No Params";
				}
				else
				{
					push @logs, "Unknown action param type! ($action_param_type)";
				}
			}
			
			$response = (defined $action_param 
				? pack("CCNCCC*", $response_no, 171, $trans_id, $approved_response_cd, $action_cd, $action_param)
				: pack("CCNCC", $response_no, 171, $trans_id, $approved_response_cd, $action_cd));
		}
		else
		{
			# not permitted
			$response = pack("CCNC", $response_no, 171, $trans_id, $approved_response_cd);
		}
	}
	push @logs, "Response          : " . unpack("H*", $response);
	
	### queue response ###						
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));

	USAT::Common::CallInRecord::add_auth($DATABASE, $command_hashref, $command_hashref->{session_id}, $card_type, $auth_amt, ($auth_code eq 'A' ? USAT::Common::CallInRecord::AUTH_APPROVED : USAT::Common::CallInRecord::AUTH_DENIED));
	
	return (\@logs);
}

sub _parse_net_batch ($$$)
{
	my @logs;
	my ($DATABASE, $command_hashref, $command_version) = @_;
	
	### validate that command version is supported ###
	unless ($command_version eq CMD_V2 || $command_version eq CMD_V2_1)
	{
		push @logs, "".(caller(0))[3].": unsupported message version $command_version";
		return(\@logs);
	}
	push @logs, "Command Version   : $command_version";

	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};

	my ($machine_trans_no, $trans_id, $amt, $tax, $trans_result, $detail_data);
	
	if ($command_version eq CMD_V2)
	{
		if($device_type =~ m/^(4)$/)
		{
			# sony send the trans ID in little-endian order
			(undef, $trans_id, $amt, $tax, $trans_result, $detail_data) = unpack("aVH6vaa*", $message);
			push @logs, "Generated ID      : $trans_id";
			
			# sony machine_trans_no is different format
			$machine_trans_no = "U:$SSN:$trans_id";
		}
		else
		{
			(undef, $trans_id, $amt, $tax, $trans_result, $detail_data) = unpack("aNH6vaa*", $message);
			push @logs, "G4 Generated ID   : $trans_id";
			$machine_trans_no = "X:$machine_id:$trans_id";
		}
		
		# :-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
		$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
	}
	elsif ($command_version eq CMD_V2_1)
	{
		$tax = 0;
		(undef, $trans_id, $amt, $trans_result, $detail_data) = unpack("aNNaa*", $message);
		push @logs, "Client Tran ID    : $trans_id";
		$machine_trans_no = "X:$machine_id:$trans_id";
	}
	
	$amt = sprintf("%.02f", ($amt * .01));
	$tax = sprintf("%.02f", ($tax * .01));

	push @logs, "Machine ID        : $machine_id";
	push @logs, "Amount            : $amt";
	push @logs, "Tax               : $tax";
	push @logs, "Result            : $trans_result";
	push @logs, "Detail (hex)      : " . unpack("H*",$detail_data);
	
	my %vended_items;
	my $vend_qty = 0; 
	my ($vend_col_bytes, $tran_payload, $pss_tran_id);
	
	if ($device_type =~ m/^(10|11|12)$/) # TransAct, Kiosk, T2
	{
		$tran_payload = $detail_data;
		push @logs, "Kiosk Detail      : " . $tran_payload;
	}
	elsif ($device_type =~ m/^(4)$/) # Sony
	{
		# Legacy Sony DLL doesn't send any detail
		$tran_payload = $detail_data;
		push @logs, "Sony Detail       : Not Provided";
	}
	else
	{
		# get the bytes per vend column from the last 2 bits
		$vend_col_bytes = (ord(substr($detail_data, 0, 1)) >> 6) + 1;

		# get the vend quantity from the first 6 bits
		$vend_qty = (ord(substr($detail_data, 0, 1)) & 0x3f);

		# chop off the first byte; should now have a 0-n Vended Items Description Blocks
		$detail_data = substr($detail_data, 1);

		# the following loop will pull the column id's and vend amounts from the vended items block and place them in a hash
		my $vended_items_order_num = 1;
		for (my $i = 0;  $i < $vend_qty; $i++)
		{
			my $item_tmp = substr($detail_data, (3 + $vend_col_bytes) * $i, (3 + $vend_col_bytes));
			
			if(!defined $item_tmp || length($item_tmp) < 3)
			{
				next;
			}

			my $item_nbr = unpack("H*",substr($item_tmp, 0, $vend_col_bytes));
			my $item_amt = substr($item_tmp, $vend_col_bytes, 3);

			$item_amt = (unpack("N", ("\x00".$item_amt))*.01);

			# add/update item in %vended_items hash - this allows us to accumulate quantities
			# by item number so we can insert summed col#/price/quantity rows into the TRANS_ITEM table.
			$vended_items{$item_nbr}{ORDER} = $vended_items_order_num++ unless defined $vended_items{$item_nbr}{ORDER};
			$vended_items{$item_nbr}{AMT} = sprintf("%.2f", ($vended_items{$item_nbr}{AMT} + $item_amt));
			$vended_items{$item_nbr}{QTY} += 1;
		}

		push @logs, "--------- Column Totals ---------";
		push @logs, "Vend Col Bytes  : $vend_col_bytes";
		push @logs, "Vend Qty        : $vend_qty";
		#display the column/qty/amt in the log
		foreach my $key (sort {$vended_items{$a}{ORDER} <=> $vended_items{$b}{ORDER}} keys %vended_items)
		{
			push @logs, "\t\tColumn : $key";
			push @logs, "\t\t\tAMT    : $vended_items{$key}{AMT}";
			push @logs, "\t\t\tQTY    : $vended_items{$key}{QTY}";
		}
	}
	
	### try POS handler first; try transd/speciald second ###
	my $posh = USAT::POS->new($DATABASE);
	my ($auth_code, $trans_no, $card_type);
	my $return_code;
	my $db_tran_id;
	if (defined $device_id)
	{
		my $tran_state_cd_batch;
		my $tran_return_code;
		$auth_code = '';
		my $error_message;
		my $time_zone;

		# Use the device_id as a rerefence to lookup the time zone
		my $lookup_tz_ref =	$DATABASE->select(	
								table          => 'location.location, pos',
								select_columns => 'location.location_time_zone_cd',
								where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
								where_values   => [$device_id] );

		$time_zone = $lookup_tz_ref->[0][0];
		if(!defined $time_zone)
		{
			$time_zone = 'EST';
			push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
		}
		else
		{
			push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
		}

		my $time = ReRix::Utils::getAdjustedTimestamp($time_zone);
		my ($Second, $Minute, $Hour, $Day, $Month, $Year) = gmtime($time);
		$Month = $Month+1;
		$Year  = $Year+1900;

		my $start_ts = "$Month/$Day/$Year $Hour:$Minute:$Second";
		push @logs, "Device Tran Time  : $start_ts";
	
		my $tran_result = $trans_result;
		if ($device_type =~ m/^(0|1|6|9|10)$/ && $trans_result =~ m/S|R|N|Q/ && $vend_qty == 0)
		{
			push @logs, "No Line Items     : Forcing into Cancelled state";
			$tran_result = 'C';		#force cancel state if no line items present - device bug fix
		}
		
		if($trans_result =~ m/^(F|T|U)$/)
		{
			# force cancelled state for failed vend cases so we process it as a cancelled vend, but we 
			# loose the extra failed reason information; we need to handle these properly in the future
			push @logs, "Failed Vend       : Forcing into Cancelled state";
			$tran_result = 'C';		
		}
		
		my $tran_batch_type_cd = 'A';	#always actual batch for non-esuds
		my $tran_local_auth_yn_flag = 'N';
		
		### call POS to batch the tran ###
		$posh->queue_debit_summary(
			$tran_batch_type_cd
			,undef						#card magstripe data--undef for net batch
			,undef						#card pin entry--undef always (unsupported by device)
			,$device_id
			,$trans_id
			,$tran_result
			,undef						#tran card type (always undef for network batch)
			,undef						#trans total amt (always undef for network batch)
			,$tran_local_auth_yn_flag	#local auth flag--'N' for network
			,undef						#parsed card magstripe data--undef for net batch
			,undef						#acct entry method--undef for net batch
			,$start_ts					#start ts
			,undef						#end ts--undef for net batch
			,\$db_tran_id
			,\$tran_state_cd_batch
			,\$tran_return_code
			,\$error_message
		);
		push @logs, "POS Processor     : queue_debit_summary($tran_batch_type_cd, undef, undef, $device_id, $trans_id, $tran_result, undef, undef, $tran_local_auth_yn_flag, undef, undef, $start_ts, $start_ts, ...)";
		push @logs, "POS Return Code   : $tran_return_code";
		push @logs, "POS Error Message : $error_message" if defined $error_message;
		push @logs, "POS DB Trans ID   : $db_tran_id";

		if ($tran_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
		{
			push @logs, "POS Processor     : queue_debit_summary failed! return_code != ".USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
			ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed! switching back to transd/speciald processing...\n$error_message", $command_hashref) if $debug == 1;
		}
		elsif (!defined $db_tran_id)
		{
			push @logs, "POS Processor     : queue_debit_summary failed! db_tran_id is undefined!";
			ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed!\n$error_message", $command_hashref);
			$tran_return_code = USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__unsuccessful_execution;
		}
		else
		{
			$trans_no = $db_tran_id;
		}
		
		### add line items ###
		my $item_return_code;
		if (defined $tran_return_code && $tran_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution) 
		{
			if ($device_type =~ m/^(10|11|12)$/)
			{
				_load_kiosk_tran_details($DATABASE, $device_type, $device_id, $tran_payload, $db_tran_id, $amt, $trans_result, \@logs);
				$item_return_code = USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
			}
			elsif ($device_type =~ m/^(4)$/)
			{
				$item_return_code = _load_sony_tran_details($DATABASE, $posh, $device_id, $tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $amt, \@logs, $command_hashref);
			}
			else 
			{
				# this should really only process G4/G5/MEI and there should be another default else that creates some kind of generic line item
				$item_return_code = _load_eport_tran_details($DATABASE, $posh, 1, $device_id, $device_type, $tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $amt, $tax, \%vended_items, \@logs, $command_hashref);
			}
		}
		
		### complete batch ###
		my $batch_complete_return_code;
		my $batch_complete_error_message;
		if (defined $tran_return_code && $tran_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution
			&& ($tran_result eq 'C' || defined $item_return_code && $item_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution))
		{
			$posh->queue_tran_finalize(
				$tran_batch_type_cd
				,$db_tran_id
				,undef			#tran total amount (undef == no validation)
				,\$tran_state_cd_batch
				,\$batch_complete_return_code
				,\$batch_complete_error_message
			);
			push @logs, "POS Processor     : queue_tran_finalize($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, undef)";
			push @logs, "Return Code       : $batch_complete_return_code";
			push @logs, "Error Message     : $batch_complete_error_message" if defined $batch_complete_error_message;
			if ($batch_complete_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
			{
				$auth_code = 'A';
			}
			else
			{
				$auth_code = 'E';
				push @logs, "POS Processor     : queue_tran_finalize failed!";
				ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_finalize failed!\n$batch_complete_error_message", $command_hashref);
			}
		}
		else {
			$auth_code = 'E';
		}
		
		$return_code = defined $batch_complete_return_code ? $batch_complete_return_code 
			: defined $item_return_code ? $item_return_code 
			: $tran_return_code;
	}
	
	### connect to transd/speciald, if necessary ###
	if (!defined $return_code 
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_id_not_found
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_type_id_unknown
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__card_type_unknown
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__cardtype_not_accepted_at_pos
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_tran_id_nofound)
	{
		push @logs, "POS Processor     : successfully passed transaction handling to transd/speciald";
		push @logs, "Machine Trans ID  : $machine_trans_no";
		
		# check for duplicates
		my $dupe_detected = 0;

		my $dupe_check_ref = $DATABASE->select(
						table			=> 'transaction_record_hist',
						select_columns	=> 'count(1)',
						where_columns	=> [ 'machine_trans_no = ?'],
						where_values	=> [ $machine_trans_no ] );

		if($dupe_check_ref->[0][0] > 0)
		{
			$dupe_detected = 1;
		}

		if($dupe_detected)
		{
			push @logs, "Duplicate Check   : Duplicate Detected: $machine_trans_no: $SSN";

			my $response;
			if($device_type =~ m/^(4)$/ && $command_version eq CMD_V2)
			{
				# sony send the trans ID in little-endian order
				$response = pack('CH2VC', $response_no, '71', $trans_id, '1');
			}
			else
			{
				$response = pack('CH2NC', $response_no, '71', $trans_id, '1');
			}

			push @logs, "Response          : " . unpack("H*", $response);
			ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));

			return(\@logs);
		}
		else
		{
			push @logs, "Duplicate Check   : $machine_trans_no: New! Storing and exporting record.";
		}		

		my $trans = ReRix::AuthClient->connect();
		$trans->send("t10,$machine_id,$machine_trans_no,$amt,$tax,$trans_result,$vend_qty,");
		$auth_code = $trans->get_code();
		$trans_no = $trans->authorization_number();
		$pss_tran_id = $trans->get_pss_tran_id();
		
		if ($device_type =~ m/^(10|11|12)$/)
		{
			_load_kiosk_tran_details($DATABASE, $device_type, $device_id, $tran_payload, $pss_tran_id, $amt, $trans_result, \@logs);
		}
		elsif ($device_type =~ m/^(4)$/)
		{
			_load_sony_tran_details($DATABASE, $posh, $device_id, 'A', $pss_tran_id, 4, $amt, \@logs, $command_hashref);
		}
	}
	
	if(defined $auth_code)
	{
		# Batch response command is an ascii '7' and the least significant byte
		# of the transaction ID
		
		push @logs, "Auth Code         : $auth_code";
		push @logs, "Trans No          : $trans_no";

		my $response;
		if($device_type =~ m/^(4)$/ && $command_version eq CMD_V2)
		{
			# sony send the trans ID in little-endian order
			$response = pack('CH2VC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
		}
		else
		{
			$response = pack('CH2NC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
		}

		push @logs, "Response          : " . unpack("H*", $response);
		ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
	}
	else
	{
		push @logs, "Transaction Server unreachable";
	}
	
	if ($return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_id_not_found
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_type_id_unknown
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__card_type_unknown
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__cardtype_not_accepted_at_pos
		|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_tran_id_nofound)
	{	#if the terminal is a G4/G5 and the auth succeeded export the data to a flat file for legacy systems for transd/speciald batch event
		if ($device_type =~ m/^(0|1|6|9|10)$/ && $trans_result =~ m/S|R|N|Q/)
		{
			push @logs, "--Exporting G4/G5 Record for $machine_id--";
			push @logs, @{&ReRix::G4Export::createG4ExportRec($DATABASE, $machine_id, $device_id, $machine_trans_no, $vend_qty, undef, $amt, undef, undef, \%vended_items)}; 
		}

		# need to lookup the card type here to write the log
	
		$card_type = 'U';
		
		my $data = $DATABASE->query(
			query	=> q{select card_type from tazdba.transaction_record_hist where machine_trans_no = ?},
			values	=> [$machine_trans_no]
		);
		if(defined $data->[0]->[0])
		{
			$card_type = $data->[0]->[0];
		}
		else
		{
			$data = $DATABASE->query(
				query	=> q{select card_type from tazdba.transaction_failures where machine_trans_no = ?},
				values	=> [$machine_trans_no]
			);
			if(defined $data->[0]->[0])
			{
				$card_type = $data->[0]->[0];
			}
		}		

		if(defined $card_type)
		{
			if($card_type eq 'SP')
			{
				$card_type = 'S' 	# S = Special Card
			}
			else
			{
				$card_type = 'C'	# if it was in the DB and was not SP, then it should be a credit card (?)
			}
		}
	}
	else	#do special g4 export for POS batch event
	{
		### look up card type from PSS schema ###
		my $c_card_type = $DATABASE->query(
			query	=> q{
				SELECT ps.client_payment_type_cd
				FROM tran t, pos_pta pp, payment_subtype ps
				WHERE pp.pos_pta_id = t.pos_pta_id
				AND ps.payment_subtype_id = pp.payment_subtype_id
				AND t.tran_id = ?
			},
			values	=> [
				$db_tran_id
			]
		);
		$card_type = $c_card_type->[0]->[0] if defined $c_card_type->[0]->[0];
	}
	
	USAT::Common::CallInRecord::add_trans($DATABASE, $command_hashref, $command_hashref->{session_id}, $card_type, ($amt+$tax), $vend_qty);

	return(\@logs);
}

sub _parse_local_batch ($$$)
{
	my @logs;
	my ($DATABASE, $command_hashref, $command_version) = @_;
	
	### validate that command version is supported ###
	unless ($command_version eq CMD_V2 || $command_version eq CMD_V2_BCD || $command_version eq CMD_V2_2)
	{
		push @logs, "".(caller(0))[3].": unsupported message version $command_version";
		return(\@logs);
	}
	push @logs, "Command Version   : $command_version";

	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};

	my ($machine_trans_no, undef, $trans_id, $timestamp, $bcd_str, $card_type, $amt, $tax, $maglength, $reqvariable);
	
	if ($command_version eq CMD_V2)
	{
		if($device_type =~ m/^(4)$/)
		{
			# sony send the trans ID in little-endian order
			(undef, $trans_id, $timestamp, $card_type, $amt, $tax, $maglength, $reqvariable) = unpack("aVNaH6vCa*", $message);
			push @logs, "Generated ID      : $trans_id";
			
			# sony machine_trans_no is different format
			$machine_trans_no = "U:$SSN:$trans_id";
		}
		else
		{
			(undef, $trans_id, $timestamp, $card_type, $amt, $tax, $maglength, $reqvariable) = unpack("aNNaH6vCa*", $message);
			push @logs, "G4 Generated ID   : $trans_id";
			$machine_trans_no = "X:$machine_id:$trans_id";
		}
		
		# :-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
		$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
	}
	elsif ($command_version eq CMD_V2_BCD)
	{
		if($device_type =~ m/^(4)$/)
		{
			# sony send the trans ID in little-endian order
			(undef, $trans_id, $bcd_str, $card_type, $amt, $tax, $maglength, $reqvariable) = unpack("aVa7aH6vCa*", $message);
			push @logs, "Generated ID      : $trans_id";
			
			# sony machine_trans_no is different format
			$machine_trans_no = "U:$SSN:$trans_id";
		}
		else
		{
			(undef, $trans_id, $bcd_str, $card_type, $amt, $tax, $maglength, $reqvariable) = unpack("aNa7aH6vCa*", $message);
			push @logs, "G4 Generated ID   : $trans_id";
			$machine_trans_no = "X:$machine_id:$trans_id";
		}
	
		# :-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
		$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
	}
	elsif ($command_version eq CMD_V2_2)
	{
		$tax = 0;
		(undef, $trans_id, $timestamp, $card_type, $amt, $maglength, $reqvariable) = unpack("aNNaNCa*", $message);
		push @logs, "Client Tran ID    : $trans_id";	
		$machine_trans_no = "X:$machine_id:$trans_id";
	}

	$amt = sprintf("%.02f", ($amt * .01));
	$tax = sprintf("%.02f", ($tax * .01));;

	my $card;
	($card, $reqvariable) = (substr($reqvariable, 0, $maglength), substr($reqvariable, $maglength));
	my $magstripe = $card;
	my ($trans_result, $detail_data) = unpack("aa*", $reqvariable);
	
	my $card_number;
	if($card_type =~ m/^(S|C|R|P)$/)
	{
		my $cr = Evend::CardUtils->new();
		$card =  $cr->cleanSwipeData($card);
	
		if( substr($card, 0, 1) eq '%' )	{ $card = substr( $card, 1 ); }
		if( substr($card, -2, 1) eq '?' )	{ $card = substr( $card, 0, -2 ); }
		elsif( substr($card, -1, 1) eq '?' ){ $card = substr( $card, 0, -1 ); }
	
		my $cardreader = Evend::CardUtils->new();
		($card_number, undef, undef, undef) = $cardreader->parse($card);
	}
	elsif($card_type =~ m/^(M|E|I)$/)
	{
		$card_number = $card;
	}
	else
	{
		push @logs, "Received unrecognized card type! : $card_type";
	}
	
	if ($command_version eq CMD_V2 || $command_version eq CMD_V2_2)
	{
		push @logs, "Client Timestamp  : $timestamp";
		if (((time() - (60*24*60*60)) > $timestamp) || ((time() + (60*24*60*60)) < $timestamp))
		{
			push @logs, "Received invalid timestamp! : $timestamp, using current time: ";
			$timestamp = time();
		}
	}
	elsif ($command_version eq CMD_V2_BCD)
	{
		if($device_type eq '0' && $ReRixConfig::ReRix_Functions{G4_BCD_SECONDS_BUG_FIX})
		{
			push @logs, "> Bug Fix         : Calling fix_g4_bcd_seconds_bug_discovered_02_03_2004";
			my $log_ref;
			($bcd_str, $log_ref) = _fix_g4_bcd_seconds_bug_discovered_02_03_2004($bcd_str, $machine_id);
			push @logs, @{$log_ref};
		}

		# check the transaction timestamp to make sure it's not screwed up
		my ($bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year) = unpack("H2H2H2H2H2H4", $bcd_str);
		#push @logs, "Tran BCD Timestamp : $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year";

		# create current time bcd string to check against
		my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
		my $current_bcd_str = sprintf("%02d%02d%02d%02d%02d%04d", $hour, $min, $sec, ($mon + 1), $mday, ($year + 1900));

		my ($current_bcd_hour, $current_bcd_min, $current_bcd_sec, $current_bcd_mon, $current_bcd_mday, $current_bcd_year) = unpack("a2a2a2a2a2a4", $current_bcd_str);
		#push @logs, "Current BCD Timestamp : $current_bcd_hour, $current_bcd_min, $current_bcd_sec, $current_bcd_mon, $current_bcd_mday, $current_bcd_year";
		my $time_validation_failed = 0;

		if($bcd_hour > 24)
		{
			push @logs, "ERROR             : Time Validation Failed! Hour = $bcd_hour: $SSN";
			$bcd_hour = $current_bcd_hour;
			$time_validation_failed = 1;
		}

		if($bcd_min > 60)
		{
			push @logs, "ERROR             : Time Validation Failed! Minute = $bcd_min: $SSN";
			$bcd_min = $current_bcd_min;
			$time_validation_failed = 1;
		}

		if($bcd_sec > 60)
		{
			push @logs, "ERROR             : Time Validation Failed! Second = $bcd_sec: $SSN";
			$bcd_sec = $current_bcd_sec;
			$time_validation_failed = 1;
		}

		if($bcd_mon > 12 || $bcd_mon == 0)
		{
			push @logs, "ERROR             : Time Validation Failed! Month = $bcd_mon: $SSN";
			$bcd_mon = $current_bcd_min;
			$time_validation_failed = 1;
		}

		if($bcd_mday > 31 || $bcd_mday == 0)
		{
			push @logs, "ERROR             : Time Validation Failed! Day = $bcd_mday: $SSN";
			$bcd_mday = $current_bcd_mday;
			$time_validation_failed = 1;
		}

		if($bcd_year > ($current_bcd_year+1) || $bcd_year < ($current_bcd_year-1))
		{
			push @logs, "ERROR             : Time Validation Failed! Year = $bcd_year: $SSN";
			$bcd_year = $current_bcd_year;
			$time_validation_failed = 1;
		}

		if($time_validation_failed)
		{
			$bcd_str = pack("H2H2H2H2H2H4", $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year);
			push @logs, "ERROR             : Substituting BCD String: " . unpack("H*", $bcd_str);
		}

		$timestamp = &ReRix::Utils::convert_bcd_time($bcd_str);
		push @logs, "Client Timestamp  : " . gmtime($timestamp);
	}
	
	if(defined $device_id && $device_type eq '4')
	{
		# -2.02- if this is a PictureStation adjust the timestamp to set it to it's local timezone because the PMI
		#	app doesn't do this for us

		# Use the device_id as a rerefence to lookup the time zone

		my $lookup_tz_ref = $DATABASE->select(	
			table          => 'location.location, pos',
			select_columns => 'location.location_time_zone_cd',
			where_columns  => ['location.location_id = pos.location_id and pos.device_id = ?'],
			where_values   => [$device_id] );

		my $time_zone = $lookup_tz_ref->[0][0];

		push @logs, "Adjusting Timestamp to compensate for Time Zone differences...";

		if(not defined $time_zone)
		{
			$time_zone = 'EST';
			push @logs, "Time Zone         : Using DEFAULT Time Zone; $time_zone";
		}
		else
		{
			push @logs, "Time Zone         : Using DATABASE Time Zone; $time_zone";
		}

		push @logs, "Server Time       : " . localtime();
		$timestamp = ReRix::Utils::getAdjustedTimestamp($time_zone, $timestamp);
		push @logs, "Adjusted Time     : " . gmtime($timestamp);
	}

	my ($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);
	my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	my $timestr = sprintf("%02d:%02d:%02d", $hour, $min, $sec);
	my $datetime = "$date $timestr";

	push @logs, "Machine ID        : $machine_id";
	push @logs, "Timestamp         : $timestamp";
	push @logs, "Date/Time         : $datetime";
	push @logs, "Card Type         : $card_type";
	push @logs, "Amount            : $amt";
	push @logs, "Tax               : $tax";
	push @logs, "Result            : $trans_result";
	push @logs, "Card Length       : $maglength";
	push @logs, "Card              : ".mask_credit_card(defined $card_number ? $card_number : $card);
	push @logs, "Detail Data       : " . unpack("H*", $detail_data);
	
	my %vended_items;
	my $vend_qty = 0;
	my ($vend_col_bytes, $tran_payload, $pss_tran_id);
	
	my $posh = USAT::POS->new($DATABASE);
	my ($auth_code, $trans_no);
	my $return_code;
	my $db_tran_id;

	if($card_type =~ m/^(S|C|R|P|M)$/)
	{
		if ($device_type =~ m/^(10|11|12)$/) #Kiosk
		{
			$tran_payload = $detail_data;
			push @logs, "Kiosk Detail      : " . $tran_payload;
		}
		elsif ($device_type =~ m/^(4)$/) # Sony
		{
			# Legacy Sony DLL doesn't send any detail
			$tran_payload = $detail_data;
			push @logs, "Sony Detail       : Not Provided";
		}
		else
		{
			# get the bytes per vend column from the last 2 bits
			$vend_col_bytes = (ord(substr($detail_data, 0, 1)) >> 6) + 1;

			# get the vend quantity from the first 6 bits
			$vend_qty = (ord(substr($detail_data, 0, 1)) & 0x3f);

			# chop off the first byte; should now have a 0-n Vended Items Description Blocks
			$detail_data = substr($detail_data, 1);

			# the following loop will pull the column id's and vend amounts from the vended items block and place them in a hash
			my $vended_items_order_num = 1;
			for (my $i = 0;  $i < $vend_qty; $i++)
			{
				my $item_tmp = substr($detail_data, (3 + $vend_col_bytes) * $i, (3 + $vend_col_bytes));

				if(!defined $item_tmp || length($item_tmp) < 3)
				{
					next;
				}
				
				my $item_nbr = unpack("H*",substr($item_tmp, 0, $vend_col_bytes));
				my $item_amt = substr($item_tmp, $vend_col_bytes, 3);

				$item_amt = (unpack("N", ("\x00".$item_amt))*.01);

				# add/update item in %vended_items hash - this allows us to accumulate quantities
				# by item number so we can insert summed col#/price/quantity rows into the TRANS_ITEM table.
				$vended_items{$item_nbr}{ORDER} = $vended_items_order_num++ unless defined $vended_items{$item_nbr}{ORDER};
				$vended_items{$item_nbr}{AMT} = sprintf("%.2f", ($vended_items{$item_nbr}{AMT} + $item_amt));
				$vended_items{$item_nbr}{QTY} += 1;
			}

			push @logs, "--------- Column Totals ---------";
			push @logs, "Vend Col Bytes  : $vend_col_bytes";
			push @logs, "Vend Qty        : $vend_qty";
			#display the column/qty/amt in the log
			foreach my $key (sort {$vended_items{$a}{ORDER} <=> $vended_items{$b}{ORDER}} keys %vended_items)
			{
				push @logs, "\t\tColumn : $key";
				push @logs, "\t\t\tAMT    : $vended_items{$key}{AMT}";
				push @logs, "\t\t\tQTY    : $vended_items{$key}{QTY}";
			}
		}

		### try POS handler first; try transd/speciald second ###
		if (defined $device_id)
		{
			my $tran_start_ts = $datetime;
			my $tran_end_ts = $datetime;
			
			my $tran_state_cd_batch;
			my $tran_return_code;
			$auth_code = '';
			my $error_message;
			
			my $tran_result = $trans_result;
			if ($device_type =~ m/^(0|1|6|9|10)$/ && $trans_result =~ m/S|R|N|Q/ && $vend_qty == 0)
			{
				push @logs, "No Line Items     : Forcing into Cancelled state";
				$tran_result = 'C';		#force cancel state if no line items present - device bug fix
			}
			
			if($trans_result =~ m/^(F|T|U)$/)
			{
				# force cancelled state for failed vend cases so we process it as a cancelled vend, but we 
				# loose the extra failed reason information; we need to handle these properly in the future
				push @logs, "Failed Vend       : Forcing into Cancelled state";
				$tran_result = 'C';		
			}
			
			my $tran_batch_type_cd = 'A';	#always actual batch for non-esuds
			my $tran_local_auth_yn_flag = 'Y';
			my $acct_entry_method = $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__cash
				? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__manual
				: $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__credit_card || $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__special_card
					? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__magnetic_stripe
					: $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__rfid_credit_card || $card_type eq USAT::POS::Const::PKG_CLIENT_PAYMENT_TYPE_GLOBALS__rfid_special_card
						? USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__contactless
						: USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__unspecified;
			
			### call POS to batch the tran ###
			$posh->queue_debit_summary(
				$tran_batch_type_cd
				,$card						#card magstripe data
				,undef						#card pin entry--undef always (unsupported by device)
				,$device_id
				,$trans_id
				,$tran_result
				,$card_type					#tran card type
				,$amt						#trans total amt
				,$tran_local_auth_yn_flag	#local auth flag--'Y' for local
				,$card						#parsed card magstripe data--unmodified from original track data
				,$acct_entry_method			#acct entry method--undef for net batch
				,$tran_start_ts				#start ts
				,$tran_end_ts				#end ts
				,\$db_tran_id
				,\$tran_state_cd_batch
				,\$tran_return_code
				,\$error_message
			);
			push @logs, "POS Processor     : queue_debit_summary($tran_batch_type_cd, ".mask_credit_card($card).", undef, $device_id, $trans_id, $tran_result, $card_type, $amt, $tran_local_auth_yn_flag, ".mask_credit_card($card).", $acct_entry_method, $tran_start_ts, $tran_end_ts, ...)";
			push @logs, "POS Return Code   : $tran_return_code";
			push @logs, "POS Error Message : $error_message" if defined $error_message;
			push @logs, "POS DB Trans ID   : $db_tran_id";
	
			if ($tran_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
			{
				push @logs, "POS Processor     : queue_debit_summary failed! return_code != ".USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
				ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed! switching back to transd/speciald processing...\n$error_message", $command_hashref) if $debug == 1;
			}
			elsif (!defined $db_tran_id)
			{
				push @logs, "POS Processor     : queue_debit_summary failed! db_tran_id is undefined!";
				ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed!\n$error_message", $command_hashref);
				$tran_return_code = USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__unsuccessful_execution;
			}
			else
			{
				$trans_no = $db_tran_id;
			}
			
			### add line items ###
			my $item_return_code;
			if (defined $tran_return_code && $tran_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution) 
			{
				if ($device_type =~ m/^(10|11|12)$/)
				{
					_load_kiosk_tran_details($DATABASE, $device_type, $device_id, $tran_payload, $db_tran_id, $amt, $trans_result, \@logs);
					$item_return_code = USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
				}
				elsif ($device_type =~ m/^(4)$/)
				{
					$item_return_code = _load_sony_tran_details($DATABASE, $posh, $device_id, $tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $amt, \@logs, $command_hashref);
				}
				else 
				{
					# this should really only process G4/G5/MEI and there should be another default else that creates some kind of generic line item
					$item_return_code = _load_eport_tran_details($DATABASE, $posh, 1, $device_id, $device_type, $tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $amt, $tax, \%vended_items, \@logs, $command_hashref);
				}
			}
			
			### complete batch ###
			my $batch_complete_return_code;
			my $batch_complete_error_message;
			if (defined $tran_return_code && $tran_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution
				&& ($tran_result eq 'C' || defined $item_return_code && $item_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution))
			{
				$posh->queue_tran_finalize(
					$tran_batch_type_cd
					,$db_tran_id
					,undef			#tran total amount (undef == no validation)
					,\$tran_state_cd_batch
					,\$batch_complete_return_code
					,\$batch_complete_error_message
				);
				push @logs, "POS Processor     : queue_tran_finalize($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, undef)";
				push @logs, "Return Code       : $batch_complete_return_code";
				push @logs, "Error Message     : $batch_complete_error_message" if defined $batch_complete_error_message;
				if ($batch_complete_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
				{
					$auth_code = 'A';
				}
				else
				{
					$auth_code = 'E';
					push @logs, "POS Processor     : queue_tran_finalize failed!";
					ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_finalize failed!\n$batch_complete_error_message", $command_hashref);
				}
			}
			else {
				$auth_code = 'E';
			}
			
			$return_code = defined $batch_complete_return_code ? $batch_complete_return_code 
				: defined $item_return_code ? $item_return_code 
				: $tran_return_code;
		}
		
		### connect to transd/speciald, if necessary ###
		if (!defined $return_code 
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_id_not_found
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_type_id_unknown
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__card_type_unknown
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__cardtype_not_accepted_at_pos
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_tran_id_nofound)
		{
			push @logs, "POS Processor     : successfully passed transaction handling to transd/speciald";
			push @logs, "Machine Trans ID  : $machine_trans_no";
	
			if($card_type eq 'R')
			{
				push @logs, "RFID Credit Card  : Substituting C card type for R";
				$card_type = 'C';
			}
			elsif($card_type eq 'P')
			{
				push @logs, "RFID Special Card : Substituting S card type for P";
				$card_type = 'S';
			}

			# kludge for Sony because they use a the same machine trans no in cases where a live
			# auth fails and they fallback to a local auth.  TransD has logic to handle this case.
			# Requested by Rita per T. Shannon - 04/28/2005 - pcowan
			if($device_type eq '4')
			{
				push @logs, "Skipping duplicate check for Sony PictureStation";
			}
			else
			{
				my $dupe_detected = 0;
				if ($command_version eq CMD_V2 || $command_version eq CMD_V2_2)
				{
					# check for duplicates
					# don't need the special G4 logic in this one, because G4s use the BCD local batch below
			
					my $dupe_check_ref = $DATABASE->select(
									table			=> 'transaction_record',
									select_columns	=> 'count(1)',
									where_columns	=> [ 'machine_trans_no = ?'],
									where_values	=> [ $machine_trans_no ] );
				
					if($dupe_check_ref->[0][0] > 0)
					{
						$dupe_detected = 1;
					}
					else
					{
						my $dupe_check_2_ref = $DATABASE->select(
										table			=> 'transaction_record_hist',
										select_columns	=> 'count(1)',
										where_columns	=> [ 'machine_trans_no = ?'],
										where_values	=> [ $machine_trans_no ] );
					
						if($dupe_check_2_ref->[0][0] > 0)
						{
			
							$dupe_detected = 1;
						}
					}
				}
				elsif ($command_version eq CMD_V2_BCD)
				{
					# check for duplicates
					# account for G4 duplicate trans ID bug explained at top
					my $processed_time_str;
		
					my $dupe_check_ref = $DATABASE->select(
									table			=> 'transaction_record',
									select_columns	=> "to_char(batch_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
									where_columns	=> [ 'machine_trans_no = ?'],
									where_values	=> [ $machine_trans_no ] );
		
					if(defined $dupe_check_ref->[0][0])
					{
						$processed_time_str = $dupe_check_ref->[0][0];
						$dupe_detected = 1;
					}
					else
					{
						my $dupe_check_2_ref = $DATABASE->select(
										table			=> 'transaction_record_hist',
										select_columns	=> "to_char(batch_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
										where_columns	=> [ 'machine_trans_no = ?'],
										where_values	=> [ $machine_trans_no ] );
										
						if(defined $dupe_check_2_ref->[0][0])
						{
							$processed_time_str = $dupe_check_2_ref->[0][0];
							$dupe_detected = 1;
						}
					}
					
					if(defined $processed_time_str && $device_type eq '0')
					{
						# this is a G4 and we've found an existing record...
						# because of a G4 bug, we must examine the batch time to decide if this is really a duplicate
		
						my (undef, undef, undef, undef, undef, undef, undef, $isdst) = time();
						my ($sec, $min, $hr, $dm, $mon, $yr, $dw, $dy) = split(":", $processed_time_str);
						my $processed_ts = Time::Local::timelocal($sec, $min, $hr, $dm, ($mon-1), $yr, $dw, $dy, $isdst);
		
						my $current_ts = time();
		
						# find the difference and round to the nearest minute
						my $et = (($current_ts - $processed_ts)/60);
						$et = sprintf("%d", $et);
		
						if($et > 5)
						{
							# we processed the previous one more than 5 minutes ago, so call this one a dupe
							$dupe_detected = 1;
							push @logs, "G4 Duplicate Check: $machine_trans_no exists, $et min old";
						}
						else
						{
							$dupe_detected = 0;
		
							# contruct a different machine_trans_no to store this one as
							my $new_machine_trans_no = $machine_trans_no . ":" . $current_ts;
		
							# we processed the previous one less than 5 minutes ago, so don't call this one a dupe
							push @logs, "G4 Duplicate Check: $machine_trans_no exists, $et min old; Saving as $new_machine_trans_no";
		
							$machine_trans_no = $new_machine_trans_no;
						}
					}
				}
				
				if($dupe_detected)
				{
					push @logs, "Duplicate Check   : Duplicate Detected: $machine_trans_no: $SSN";
			
					my $response;
					if($device_type =~ m/^(4)$/ && $command_version eq CMD_V2)
					{
						# sony send the trans ID in little-endian order
						$response = pack('CH2VC', $response_no, '71', $trans_id, '1');
					}
					else
					{
						$response = pack('CH2NC', $response_no, '71', $trans_id, '1');
					}
			
					push @logs, "Response          : " . unpack("H*", $response);
					ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));

					return(\@logs);
				}
				else
				{
					push @logs, "Duplicate Check   : $machine_trans_no: New! Storing and exporting record.";
				}
			}
	
			my $trans = ReRix::AuthClient->connect();
			$trans->send("t11,$machine_id,$timestr,$date,$card,$card_type,$amt,$tax,$trans_result,$machine_trans_no,$timestamp,$magstripe,$vend_qty,");
			$pss_tran_id = $trans->get_pss_tran_id();
	
			if ($device_type =~ m/^(10|11|12)$/)
			{
				_load_kiosk_tran_details($DATABASE, $device_type, $device_id, $tran_payload, $pss_tran_id, $amt, $trans_result, \@logs);
			}
			elsif ($device_type =~ m/^(4)$/)
			{
				_load_sony_tran_details($DATABASE, $posh, $device_id, 'A', $pss_tran_id, 1, $amt, \@logs, $command_hashref);
			}
			
			$trans_no = $trans->authorization_number();
			$auth_code = $trans->get_code();
		}

		if (defined $auth_code)
		{
			# Batch response command is an ascii '7' and the least significant byte
			# of the transaction ID
			
			push @logs, "Auth Code         : $auth_code";
			push @logs, "Trans No          : $trans_no";
			
			my $response;
			if($device_type =~ m/^(4)$/ && $command_version eq CMD_V2)
			{
				# sony send the trans ID in little-endian order
				$response = pack('CH2VC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
			}
			else
			{
				$response = pack('CH2NC', $response_no, '71', $trans_id, $auth_code eq 'A'?1:0);
			}
	
			push @logs, "Response          : " . unpack("H*", $response);
			ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
		}
		else
		{
			push @logs, "Transaction Server unreachable";
		}
	}
	elsif($card_type =~ m/^(E|I)$/)
	{
		my $response;
		if($device_type =~ m/^(4)$/ && $command_version eq CMD_V2)
		{
			# sony send the trans ID in little-endian order
			$response = pack('CH2VC', $response_no, '71', $trans_id, '1');
		}
		else
		{
			$response = pack('CH2NC', $response_no, '71', $trans_id, '1');
		}

		push @logs, "Response          : " . unpack("H*", $response);
		ReRix::Utils::enqueue_outbound_command($command_hashref, &MakeHex($response));
	}

	#if the terminal is a G4/G5 and the auth succeeded export the data to a flat file for other processes
	if ($device_type =~ m/^(0|1|6|9|10)$/ && $card_type =~ m/^(E|I)$/)
	{
		if ($card =~ m/^Err\.DEX/)
		{
			my $event_id = ReRix::Utils::create_event($DATABASE, \@logs, $command_hashref, EVENT_TYPE_HOST_NUM->{FILL}, EVENT_STATE->{COMPLETE_FINAL}, $trans_id, BOOLEAN->{TRUE}, BOOLEAN->{TRUE});
			ReRix::Utils::create_event_detail($DATABASE, \@logs, $event_id, EVENT_DETAIL_TYPE->{EVENT_TYPE_VERSION}, 1, undef);
			ReRix::Utils::create_event_detail($DATABASE, \@logs, $event_id, EVENT_DETAIL_TYPE->{HOST_EVENT_TIMESTAMP}, undef, $datetime);
		}
		else
		{
			push @logs, "--Exporting G4/G5 Error Record for $machine_id--";
			push @logs, @{&ReRix::G4Export::createG4ErrorRec($DATABASE, $machine_id, $device_id, $machine_trans_no, $SSN, $timestamp, $card)}; 
		}
	}
	elsif ($device_type =~ m/^(0|1|6|9|10)$/ && $trans_result =~ m/S|R|N|Q/)
	{
		if ($return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_id_not_found
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_type_id_unknown
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__card_type_unknown
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__cardtype_not_accepted_at_pos
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_tran_id_nofound)
		{	#if the terminal is a G4/G5 and the auth succeeded export the data to a flat file for legacy systems for transd/speciald batch event
			push @logs, "--Exporting G4/G5 Record for $machine_id--";
			push @logs, @{&ReRix::G4Export::createG4ExportRec($DATABASE, $machine_id, $device_id, $machine_trans_no, $vend_qty, $timestamp, $amt, $card, $card_type, \%vended_items)}; 
		}
#		elsif ($return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__duplicate_transaction)	#do special g4 export for POS batch event
#		{
#			push @logs, "--Exporting G4/G5 Record from PSS for $machine_id--";
#			push @logs, @{&ReRix::G4Export_PSS::createG4ExportRec($DATABASE, $device_id, $trans_id, $machine_trans_no, undef, undef, undef, $amt, $vend_qty, $vend_col_bytes, \%vended_items)};
#		}
	}

	USAT::Common::CallInRecord::add_trans($DATABASE, $command_hashref, $command_hashref->{session_id}, $card_type, ($amt+$tax), $vend_qty);

	return(\@logs);
}

sub parse_cash_sale
{
	my @logs;
	my ($DATABASE, $command_hashref) = @_;
	
	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};
	
	push @logs, "Deprecated        : parse_cash_sale depreciated and removed";
	ReRix::Utils::send_alert("Trans20 depreciated method parse_cash_sale called by device SSN=$SSN, device_id=$device_id", $command_hashref);

	return(\@logs);
}

# Notes: Since the known bugs we're checking for in the other cash commands have been fixed before this was coded, I'm not 
# putting checking for those bugs in this command.  I'm also going to assume the client sending this command was coded to 
# the spec, and therefore going to read the Batch ID as 4-byte Big Endian number for all clients.  (pcowan - 11/29/2004)
# (erybski - 03/07/2006) Given limited data available and scope (e.g. vending) this was inteded for, I'm assuming cash events 
# uploaded via this message type represent a single quantity vend; thus, if a vending machine is configured for multi-vend in 
# a single session with cash, these each item purchased will be reported either as separate v2 event or as individual cash
# blocks in v3.
sub _parse_cash_sale ($$$)
{
	my @logs;
	my ($DATABASE, $command_hashref, $command_version) = @_;
	
	### validate that command version is supported ###
	unless ($command_version eq CMD_V2_BCD || $command_version eq CMD_V3 || $command_version eq CMD_V3_BCD)
	{
		push @logs, "".(caller(0))[3].": unsupported message version $command_version";
		return(\@logs);
	}
	push @logs, "Command Version   : $command_version";

	my $machine_id = $command_hashref->{machine_id}; 
	my $message = pack("H*",$command_hashref->{inbound_command});
	my $device_id = $command_hashref->{device_id}; 
	my $SSN = $command_hashref->{SSN}; 
	my $device_type = $command_hashref->{device_type}; 
	my $response_no = $command_hashref->{response_no};
	
	my $trans_id = unpack("N", substr($message, 1, 4));
	
	# check if this is a duplicate cash message
	# we'll check for the first one only.  If the first exists, assume the other exist also and return ACK immediatly
	my $dupe_check_machine_trans_no = "X:$machine_id:$trans_id:1";
	my $dupe_check_cash_ref = $DATABASE->select(
					table			=> 'cash_transaction_record',
					select_columns	=> "to_char(processed_date, 'ss:mi:hh24:dd:mm:yyyy:d:ddd')",
					where_columns	=> [ 'machine_trans_no = ?'],
					where_values	=> [ $dupe_check_machine_trans_no ] );
	
	if(defined $dupe_check_cash_ref->[0][0])
	{
		push @logs, "Duplicate Check   : Duplicate Detected: $dupe_check_machine_trans_no: $SSN";

		my $response = pack("CH2N", $response_no, '95', $trans_id);
	
		push @logs, "Response          : " . unpack("H*", $response);
		ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
		
		return(\@logs);
	}
	
	my $block_length = $command_version eq CMD_V3 ? 8 : $command_version eq CMD_V2_BCD || $command_version eq CMD_V3_BCD ? 11 : 0;
	my $blocks = substr($message, 5);
	my $num_blocks = length($blocks)/$block_length;
	if (int($num_blocks) != $num_blocks || $num_blocks <= 0 || $num_blocks > 99)
	{
		push @logs, "Data looks corrupt!  num_blocks = $num_blocks (min=0, max=99)";
		ReRix::Utils::send_alert("Trans20: _parse_cash_sale: Data looks corrupt! num_blocks = $num_blocks", $command_hashref);
		return(\@logs);
	}

	push @logs, "Num Cash Blocks   : $num_blocks";
	
	my $block_counter = 1;
	my $is_dup_detected = 0;
	while(length($blocks) > 0 && !$is_dup_detected)
	{
		my $block = substr($blocks, 0, $block_length);
		$blocks = substr($blocks, $block_length);

		push @logs, "$block_counter> Raw Block      : " . unpack("H*", $block);
		
		my ($timestamp, $bcd_str, $amt, $column);
		if ($command_version eq CMD_V2_BCD || $command_version eq CMD_V3_BCD)
		{
			#note: deliberately handling 1 byte for v2 even though spec allows for N bytes, as this
			#      mimics the (buggy) historical behavior of bcd_v2 cash since it was first written
			($bcd_str, $amt, $column) = unpack("a7H6H2", $block);
		}
		elsif ($command_version eq CMD_V3)
		{
			($timestamp, $amt, $column) = unpack("NH6H2", $block);
		}

		my $machine_trans_no = "X:$machine_id:$trans_id:$block_counter";

		# :-<> ! I know it's ugly, but it's necessary to convert a 3-byte, hex-encoded integer to a 4-byte hex-encoded integer
		$amt = unpack("V", pack("V", hex(unpack("h*", pack("xh*", $amt)))));
		$amt = sprintf("%.02f", ($amt * .01));

		if ($command_version eq CMD_V3)
		{
			if (((time() - (60*24*60*60)) > $timestamp) || ((time() + (60*24*60*60)) < $timestamp))
			{
				push @logs, "Received invalid timestamp! : $timestamp, using current time: ";
				$timestamp = time();
			}
		}
		elsif ($command_version eq CMD_V2_BCD || $command_version eq CMD_V3_BCD) {
			# check the transaction timestamp to make sure it's not screwed up
			my ($bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year) = unpack("H2H2H2H2H2H4", $bcd_str);

			# create current time bcd string to check against
			my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
			my $current_bcd_str = sprintf("%02d%02d%02d%02d%02d%04d", $hour, $min, $sec, ($mon + 1), $mday, ($year + 1900));
			my ($current_bcd_hour, $current_bcd_min, $current_bcd_sec, $current_bcd_mon, $current_bcd_mday, $current_bcd_year) = unpack("a2a2a2a2a2a4", $current_bcd_str);
			
			my $time_validation_failed = 0;

			if($bcd_hour > 24)
			{
				push @logs, "ERROR             : Time Validation Failed! Hour = $bcd_hour: $SSN";
				$bcd_hour = $current_bcd_hour;
				$time_validation_failed = 1;
			}

			if($bcd_min > 60)
			{
				push @logs, "ERROR             : Time Validation Failed! Minute = $bcd_min: $SSN";
				$bcd_min = $current_bcd_min;
				$time_validation_failed = 1;
			}

			if($bcd_sec > 60)
			{
				push @logs, "ERROR             : Time Validation Failed! Second = $bcd_sec: $SSN";
				$bcd_sec = $current_bcd_sec;
				$time_validation_failed = 1;
			}

			if($bcd_mon > 12 || $bcd_mon == 0)
			{
				push @logs, "ERROR             : Time Validation Failed! Month = $bcd_mon: $SSN";
				$bcd_mon = $current_bcd_min;
				$time_validation_failed = 1;
			}

			if($bcd_mday > 31 || $bcd_mday == 0)
			{
				push @logs, "ERROR             : Time Validation Failed! Day = $bcd_mday: $SSN";
				$bcd_mday = $current_bcd_mday;
				$time_validation_failed = 1;
			}

			if($bcd_year > ($current_bcd_year+1) || $bcd_year < ($current_bcd_year-1))
			{
				push @logs, "ERROR             : Time Validation Failed! Year = $bcd_year: $SSN";
				$bcd_year = $current_bcd_year;
				$time_validation_failed = 1;
			}

			if($time_validation_failed)
			{
				$bcd_str = pack("H2H2H2H2H2H4", $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year);
				push @logs, "ERROR             : Substituting BCD String: " . unpack("H*", $bcd_str);
			}

			$timestamp = &ReRix::Utils::convert_bcd_time($bcd_str);
		}
		
		my ($sec, $min, $hour, $mday, $mon, $year) = gmtime($timestamp);
		my $date = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
		my $time = sprintf("%02d:%02d:%02d", $hour, $min, $sec);

		push @logs, "$block_counter> Date/Time      : $date $time";
		push @logs, "$block_counter> Amount         : $amt";
		push @logs, "$block_counter> Column         : $column";
	
		my %vended_items = ( $column => { ORDER => 1, QTY => 1, AMT => $amt } );

		### try POS handler first; try transd/speciald second ###
		my $posh = USAT::POS->new($DATABASE);
		my $db_tran_id;
		my $return_code;
		
		my $tran_device_tran_cd = "$trans_id:$block_counter";	#note: modifying tran cd from device to include block counter
		my $card_type = 'M';
		
		if (defined $device_id)
		{
			my $tran_start_ts = "$date $time";	#MM/DD/YYYY HH24:MI:SS
			my $tran_end_ts = "$date $time";	#MM/DD/YYYY HH24:MI:SS
			
			my $tran_state_cd_batch;
			my $tran_return_code;
			my $error_message;
			
			my $tran_result = 'Q';	#explicitly define this as "success, no receipt", as cash doesn't send a vend state
			my $total_items = scalar keys %vended_items;
			$tran_result = 'C' if scalar keys %vended_items == 0;	#force cancel state if no line items present
			
			my $tran_batch_type_cd = 'A';	#always actual batch for cash
			my $tran_local_auth_yn_flag = 'Y';
			my $acct_entry_method = USAT::POS::Const::PKG_ACCT_ENTRY_METHOD_GLOBALS__manual;	#always manual for cash
			
			### call POS to batch the tran ###
			$posh->queue_debit_summary(
				$tran_batch_type_cd
				,undef						#card magstripe data--undef for cash
				,undef						#card pin entry--undef always (unsupported by device)
				,$device_id
				,$tran_device_tran_cd
				,$tran_result
				,$card_type					#tran card type
				,$amt						#trans total amt
				,$tran_local_auth_yn_flag	#local auth flag--'Y' for local (always for cash)
				,undef						#parsed card magstripe data--undef for cash
				,$acct_entry_method			#acct entry method--undef for net batch
				,$tran_start_ts				#start ts
				,$tran_end_ts				#end ts
				,\$db_tran_id
				,\$tran_state_cd_batch
				,\$tran_return_code
				,\$error_message
			);
			push @logs, "POS Processor     : queue_debit_summary($tran_batch_type_cd, undef, undef, $device_id, $tran_device_tran_cd, $tran_result, $card_type, $amt, $tran_local_auth_yn_flag, undef, $acct_entry_method, $tran_start_ts, $tran_end_ts, ...)";
			push @logs, "POS Return Code   : $tran_return_code";
			push @logs, "POS Error Message : $error_message" if defined $error_message;
			push @logs, "POS DB Trans ID   : $db_tran_id";
	
			if ($tran_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
			{
				push @logs, "POS Processor     : queue_debit_summary failed! return_code != ".USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
				ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed! switching back to transd/speciald processing...\n$error_message", $command_hashref) if $debug == 1;
			}
			elsif (!defined $db_tran_id)
			{
				push @logs, "POS Processor     : queue_debit_summary failed! db_tran_id is undefined!";
				ReRix::Utils::send_alert("ESuds: POS Processor     : queue_debit_summary failed!\n$error_message", $command_hashref);
				$tran_return_code = USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__unsuccessful_execution;
			}
			
			# NOTE:	Sony, Kiosk, etc currently do not support Cash, so there's no support here for non-G4 style devices.
			# 		If Cash support is added to these devices we may need to include support for their line item detail here.
			
			### add line item ###
			my $item_return_code;
			if (defined $tran_return_code && $tran_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution) 
			{
				$item_return_code = _load_eport_tran_details($DATABASE, $posh, 0, $device_id, $device_type, $tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $amt, 0, \%vended_items, \@logs, $command_hashref);
			}
			
			### complete batch ###
			my $batch_complete_return_code;
			my $batch_complete_error_message;
			if (defined $tran_return_code && $tran_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution
				&& ($tran_result eq 'C' || defined $item_return_code && $item_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution))
			{
				$posh->queue_tran_finalize(
					$tran_batch_type_cd
					,$db_tran_id
					,undef			#tran total amount (undef == no validation)
					,\$tran_state_cd_batch
					,\$batch_complete_return_code
					,\$batch_complete_error_message
				);
				push @logs, "POS Processor     : queue_tran_finalize($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, undef)";
				push @logs, "Return Code       : $batch_complete_return_code";
				push @logs, "Error Message     : $batch_complete_error_message" if defined $batch_complete_error_message;
				unless ($batch_complete_return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
				{
					push @logs, "POS Processor     : queue_tran_finalize failed!";
					ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_finalize failed!\n$batch_complete_error_message", $command_hashref);
				}
			}
			
			$return_code = defined $batch_complete_return_code ? $batch_complete_return_code 
				: defined $item_return_code ? $item_return_code 
				: $tran_return_code;
			$is_dup_detected = 1 if defined $return_code && $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__duplicate_transaction;
		}

		### connect to transd/speciald, if necessary ###
		if (!defined $return_code 
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_id_not_found
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_type_id_unknown
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__card_type_unknown
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__cardtype_not_accepted_at_pos
			|| $return_code eq USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__device_tran_id_nofound)
		{
			push @logs, "POS Processor     : successfully passed transaction handling to transd/speciald";
			push @logs, "$block_counter> Trans_No        : $machine_trans_no";
			
			my $insert_cash_rows_inserted = $DATABASE->do(
				query	=> q{INSERT INTO cash_transaction_record(transaction_date, machine_id, machine_trans_no, amount, column_id) VALUES (to_date(?, 'MM/DD/YYYY HH24:MI:SS'), ?, ?, ?, ?)},
				values	=> [ "$date $time", $machine_id, $machine_trans_no, $amt, $column ]
			);
			unless($insert_cash_rows_inserted)
			{
				push @logs, "Insert Cash Transaction ($machine_trans_no) Failed";
				ReRix::Utils::send_alert("Trans20: parse_cash_sale_v3_bcd: Insert Cash Transaction ($machine_trans_no) Failed", $command_hashref);
				return(\@logs);
			} 

			push @logs, "-- Exporting G4/G5 Cash Record for $machine_trans_no --";
			push @logs, @{&ReRix::G4Export::createG4ExportCashRec($DATABASE, $machine_id, $device_id, $machine_trans_no, $amt, $column, $timestamp)};
		}
		
		USAT::Common::CallInRecord::add_trans($DATABASE, $command_hashref, $command_hashref->{session_id}, USAT::Common::CallInRecord::CASH_TRANS, $amt, 1);
		
		$block_counter++;
	}

	my $response = pack("CH2N", $response_no, '95', $trans_id);

	push @logs, "Response          : " . unpack("H*", $response);
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));	

	return(\@logs);
}

sub _fix_g4_bcd_seconds_bug_discovered_02_03_2004 ($$)
{
	my @logs;
	my ($bcd_str, $machine_id) = @_;
	my ($bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year) = unpack("H2H2H2H2H2H4", $bcd_str);
	
	# may want to put some additional checks here to look at the machine ID and decide if it's a version
	# that contains this bug
	
	#push @logs, "> Input BCD Time  : " . unpack("H*", $bcd_str);
	#push @logs, "> Input Seconds   : $bcd_sec";
	
	if($bcd_sec eq $bcd_mon)
	{
		my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);

		if(length($sec) == 1)
		{
			$sec = "0$sec";
		}
		
		push @logs, "> Fixing Seconds  : Using $sec instead of $bcd_sec";

		$bcd_sec = $sec;
	}
	else
	{
		push @logs, "> Seconds OK      : Seconds $bcd_sec != Month $bcd_mon";
	}
	
	$bcd_str = pack("H2H2H2H2H2H4", $bcd_hour, $bcd_min, $bcd_sec, $bcd_mon, $bcd_mday, $bcd_year);
	
	#push @logs, "> Output Seconds  : $bcd_sec";
	#push @logs, "> Output BCD Time : " . unpack("H*", $bcd_str);

	return ($bcd_str, \@logs);
}

sub _get_item_details ($$)
{
	my ($DATABASE, $item_type_id) = @_;
	my $lookup_item_details_ref = $DATABASE->select(
						table			=> 'tran_line_item_type',
						select_columns	=> 'tran_line_item_type_desc',
						where_columns	=> [ 'tran_line_item_type_id = ?' ],
						where_values	=> [ $item_type_id ] );

	return $lookup_item_details_ref->[0]->[0];
}

sub _load_kiosk_tran_details ($$$$$$$$)
{
	my ($DATABASE, $device_type, $device_id, $tran_payload, $pss_tran_id, $batch_amount, $tran_result, $logs_ref) = @_;
	my $ret_cd = 0;
	
	if (defined($pss_tran_id) && $pss_tran_id =~ /^[0-9]+$/ && $pss_tran_id ne '0') 
	{
		push @{$logs_ref}, "Storing Kiosk tran line items, tran_id: $pss_tran_id";
		my $res = $DATABASE->procedure(
			'name'	=> 'PSS.load_tran_details',
			'bind'	=> [ qw(:device_id :device_type_id :tran_id :batch_amount :tran_result :tran_details) ],
			'in'	=> [ $device_id, $device_type, $pss_tran_id, $batch_amount, $tran_result, $tran_payload ]
		);

		if (defined $res)
		{
			push @{$logs_ref}, "Successfully stored Kiosk tran line items";
			$ret_cd = 1;
		}
		else
		{
			push @{$logs_ref}, "Database error storing Kiosk tran line items";
		}
	}
	else
	{
		push @{$logs_ref}, "Unable to store Kiosk tran line items, invalid tran_id: $pss_tran_id";
	}
	
	return $ret_cd;
}

sub _load_sony_tran_details($$$$$$$$$)
{
	my ($DATABASE, $posh, $device_id, $tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $amt, $logs_ref, $command_hashref) = @_;
	
	### find the appropriate host to attach this transaction to
	my $err;
	my $host_data_ref = USAT::POS::API::PTA::Util::get_host_for_transaction($DATABASE, $device_id, \$err);
	
	my $item_type_id = SONY_GENERIC_VEND_TLI_ID;	#generic for single-host vends
	my $item_desc = _get_item_details($DATABASE, $item_type_id);
	
	if (!defined $host_data_ref)
	{
		push @{$logs_ref}, "POS Processor     : Failed to find a host for device $device_id: $err";
		ReRix::Utils::send_alert("POS Processor     : Failed to find a host for device $device_id: $err\n", $command_hashref);
		return USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__unsuccessful_execution;
	}
	
	if (!defined $item_desc)
	{
		push @{$logs_ref}, "POS Processor     : Failed to lookup tran_line_item_type for item_type_id $item_type_id!";
		ReRix::Utils::send_alert("POS Processor     : Failed to lookup tran_line_item_type for item_type_id $item_type_id!\n", $command_hashref);
		return USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__unsuccessful_execution
	}
	
	my $host_id = $host_data_ref->[0];
	my $host_type_name = $host_data_ref->[9];
	my $host_label = $host_data_ref->[7];
	
	push @{$logs_ref}, "HostID/Type/Label : $host_id/$host_type_name/$host_label";
	
	my $item_return_code;
	my $error_message;
	$posh->queue_tran_add_line_item(
		$tran_batch_type_cd
		,$db_tran_id
		,$tran_state_cd_batch
		,$host_id							#host id
		,$amt								#item amount
		,0									#item tax
		,$item_type_id						#item type id
		,$item_desc							#description
		,1									#qty
		,undef								#position_cd
		,\$item_return_code
		,\$error_message
	);
	
	push @{$logs_ref}, "POS Processor     : queue_tran_add_line_item($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $host_id, $amt, 0, $item_type_id, $item_desc, 1, undef)";
	push @{$logs_ref}, "POS Return Code   : $item_return_code";
	push @{$logs_ref}, "POS Error Message : $error_message" if defined $error_message;

		if ($item_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
	{
		push @{$logs_ref}, "POS Processor     : queue_tran_add_line_item failed!";
		ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_add_line_item failed!\n$error_message", $command_hashref);
	}
	
	return $item_return_code;
}

sub _load_eport_tran_details($$$$$$$$$$)
{
	my ($DATABASE, $posh, $add_summary_item, $device_id, $device_type, $tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $amt, $tax, $vended_items_ref, $logs_ref, $command_hashref) = @_;

	### find the appropriate host to attach this transaction to
	my $err;
	my $host_data_ref = USAT::POS::API::PTA::Util::get_host_for_transaction($DATABASE, $device_id, \$err);
				
	my $item_type_id = EPORT_GENERIC_VEND_TLI_ID;	#generic for single-host vends
	my $item_desc = _get_item_details($DATABASE, $item_type_id);
	
	if (!defined $host_data_ref)
	{
		push @{$logs_ref}, "POS Processor     : Failed to find a host for device $device_id: $err";
		ReRix::Utils::send_alert("POS Processor     : Failed to find a host for device $device_id: $err\n", $command_hashref);
		return USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__unsuccessful_execution;
	}
	
	if (!defined $item_desc)
	{
		push @{$logs_ref}, "POS Processor     : Failed to lookup tran_line_item_type for item_type_id $item_type_id!";
		ReRix::Utils::send_alert("POS Processor     : Failed to lookup tran_line_item_type for item_type_id $item_type_id!\n", $command_hashref);
		return USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__unsuccessful_execution;
	}
	
	my $host_id = $host_data_ref->[0];
	my $host_type_name = $host_data_ref->[9];
	my $host_label = $host_data_ref->[7];
	
	push @{$logs_ref}, "HostID/Type/Label : $host_id/$host_type_name/$host_label";
	
	my $item_return_code;
	my $error_message;
	my %vended_items = %{$vended_items_ref};
	my $item_num = 0;
	my $total_items = scalar keys %vended_items;
	LINE_ITEM: foreach my $key (sort {$vended_items{$a}{ORDER} <=> $vended_items{$b}{ORDER}} keys %vended_items)
	{
		$item_num++;
		my $item_amt = 0;
		my $item_tax = 0;
		if ($device_type eq '6')	#if mei device
		{
			if ($total_items == 1)
			{
				$item_amt = (USAT::Math::Currency->new($amt) / $vended_items{$key}->{QTY})->as_float();
				$item_tax = $tax;
			}
			elsif ($total_items > 1)
			{
				$item_amt = (USAT::Math::Currency->new($vended_items{$key}->{AMT}) / $vended_items{$key}->{QTY})->as_float();
				$item_tax = $tax if $item_num == 1;	#store tax in first item (as we only have tax total tax info--no breakdown)
			}
		}
		elsif ($device_type ne '6' && $item_num == 1 && $add_summary_item)	#NOT mei and is the first item--insert special, extra record with null column info
		{
			my $summary_item_type_id = TRANSACTION_AMOUNT_SUMMARY;	#generic for all vends
			my $summary_item_desc = _get_item_details($DATABASE, $summary_item_type_id);
			if (!defined $summary_item_desc)
			{
				push @{$logs_ref}, "POS Processor     : Failed to lookup tran_line_item_type for item_type_id $summary_item_type_id!";
				ReRix::Utils::send_alert("POS Processor     : Failed to lookup tran_line_item_type for item_type_id $summary_item_type_id!\n", $command_hashref);
			}
			$posh->queue_tran_add_line_item(
				$tran_batch_type_cd
				,$db_tran_id
				,$tran_state_cd_batch
				,$host_id							#host id
				,$amt								#item amount--total for entire transaction for this line item type
				,$tax								#item tax--total for entire transaction for this line item type
				,$summary_item_type_id				#item type id
				,$summary_item_desc					#description
				,1									#qty--always 1 for this line item type
				,undef								#position_cd--always undef for this line item type
				,\$item_return_code
				,\$error_message
			);
			push @{$logs_ref}, "POS Processor     : queue_tran_add_line_item($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $host_id, $amt, $tax, $summary_item_type_id, $summary_item_desc, 1, $key)";
			push @{$logs_ref}, "POS Return Code   : $item_return_code";
			push @{$logs_ref}, "POS Error Message : $error_message" if defined $error_message;

			if ($item_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
			{
				push @{$logs_ref}, "POS Processor     : queue_tran_add_line_item failed!";
				ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_add_line_item failed!\n$error_message", $command_hashref);
			}

			last LINE_ITEM if $item_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
		}
		elsif (!$add_summary_item)
		{
			$item_amt = $amt;
			$item_tax = $tax;
		}
		
		my $description = "$item_desc \#$key".($vended_items{$key}->{QTY} > 1 ? " (".$vended_items{$key}->{QTY}.")" : '');
		$posh->queue_tran_add_line_item(
			$tran_batch_type_cd
			,$db_tran_id
			,$tran_state_cd_batch
			,$host_id							#host id
			,$item_amt							#item amount
			,$item_tax							#item tax
			,$item_type_id						#item type id
			,$description						#description
			,$vended_items{$key}->{QTY}			#qty
			,$key								#position_cd
			,\$item_return_code
			,\$error_message
		);
		push @{$logs_ref}, "POS Processor     : queue_tran_add_line_item($tran_batch_type_cd, $db_tran_id, $tran_state_cd_batch, $host_id, $item_amt, $item_tax, $item_type_id, $item_desc, $vended_items{$key}->{QTY}, $key)";
		push @{$logs_ref}, "POS Return Code   : $item_return_code";
		push @{$logs_ref}, "POS Error Message : $error_message" if defined $error_message;

		if ($item_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution)
		{
			push @{$logs_ref}, "POS Processor     : queue_tran_add_line_item failed!";
			ReRix::Utils::send_alert("ESuds: POS Processor     : queue_tran_add_line_item failed!\n$error_message", $command_hashref);
		}

		last LINE_ITEM if $item_return_code ne USAT::POS::Const::PKG_EXEC_HIST_GLOBALS__successful_execution;
	}
	
	return $item_return_code;
}

1;
