package ReRix::AuthClient;

use strict;
use ReRixConfig qw(%ReRix_AuthClient);
use IO::Socket;

sub connect 
{
	my $type = shift;

	my $object = {};
	%$object = @_;

	$object->{timeout} ||= $ReRix_AuthClient{timeout};

	$object->{host} = $ReRix_AuthClient{host};
	$object->{port} = $ReRix_AuthClient{port};

	$object->{socket} = IO::Socket::INET->new(Proto => 'tcp', PeerAddr => $object->{host}, PeerPort => $object->{port}, Timeout => $object->{timeout});

	bless $object,$type;
}

sub send
{
	my $object = shift;
	my $string = shift;
	my $socket = $object->{socket};

	print $socket "$string\n" if $socket;

	my $line =  <$socket>;
	chomp $line;

	my ($code,$authnumber,$account_balance, $inv, $pss_tran_id) = split /\,/, $line;
	$account_balance =~ s/\s//g;
	$object->{authorization_number} = $authnumber;
	$object->{code} = $code;
	$object->{account_balance} = "\$$account_balance" if $account_balance ne '';

	$object->{$authnumber}->{code} = $code;
	$object->{$authnumber}->{account_balance} = "\$$account_balance" if $account_balance ne '';
	$object->{inv} = $inv;
	$object->{pss_tran_id} = $pss_tran_id;

	return 1;
}

sub authorization_number
{
	my($object) = shift;
	return $object->{authorization_number};
}

sub get_code
{
	my($object) = shift;
	my $authorization_number = shift;
	$authorization_number ||= $object->authorization_number();
	return $object->{$authorization_number}->{code};
}

sub account_balance
{
	my ($object) = shift;
	my $authorization_number = shift;
	$authorization_number ||= $object->authorization_number();
	return $object->{$authorization_number}->{account_balance};
}

sub disconnect
{
	my($object) = shift;
	my $socket = $object->{socket};

	eval { $socket->close(); };
	undef $object;
}

# returns 1 if inventory needs to be removed for this transaction, 0 if it
# already has
sub get_inv
{
		my($object) = shift;
		if( !defined $object->{inv} )
		{
			return 0;
		}
		return $object->{inv};
}

sub get_pss_tran_id
{
	my($object) = shift;
	if( !defined $object->{pss_tran_id} )
	{
		return 0;
	}
	return $object->{pss_tran_id};
}

1;
