#!/usr/local/USAT/bin/perl -w

use strict;
use Net::SMTP;
use Sys::Hostname;
use FindBin;
use lib "$FindBin::Bin";
use ReRixConfig qw(%run_rerix $debug %ReRix_global);

my $engine = "$ReRix_global{base_path}/ReRixEngine.pl";
my ($runtime, $starttime, $exit_value, $dumped_core, $sleeptime, $loop_count);
$loop_count = 0;
my $no_purge_queue = 0;
my $running = 1;
while($running)
{
	$sleeptime = 5;
	$loop_count++;
	$starttime = time();

	print (localtime() . " [run_rerix] ReRixEngine (".hostname().") Starting ($loop_count) *********************\n");
	system($engine.($no_purge_queue ? ' -n' : ''));
	print (localtime() . " [run_rerix] ReRixEngine (".hostname().") Exiting  ($loop_count) *********************\n");

	$exit_value = $? >> 8;
	$dumped_core = $? & 128;
	$runtime = time() - $starttime;
	
	if ($exit_value == 2)
	{
		$sleeptime = 0;
		$running = 0;
		$no_purge_queue = 0;
	}
	elsif ($runtime > 60*60 || $exit_value == 1)	# if it ran for more than an hour or self-terminated safely, immediately restart it
	{
		$sleeptime = 0;
		$no_purge_queue = 1;
	}
	elsif ($runtime < 60*5)	# if it ran for less than 5 minutes, sleep 15 seconds
	{
		$sleeptime = 15;
		$no_purge_queue = 0;
	}
	
	my $msg = localtime() . "\n\nHostname: ".hostname()."\n\nExit Value: $exit_value\nDumped Core: $dumped_core\nRun Time: $runtime\nLoop Count: $loop_count\n\n";
	print $msg;
	&send_email($run_rerix{alert_email_addr_from}, $run_rerix{alert_email_addr_to}, "ReRix Engine (".hostname().") Restarting (in $sleeptime sec)", $msg) if $run_rerix{alert_email_enabled} && ($exit_value == 0 || $exit_value == 255 || $dumped_core != 0) && $running;
	
	sleep $sleeptime;
}
print (localtime() . " [run_rerix] ReRixEngine Stopped  *********************\n");

sub send_email
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = $run_rerix{alert_email_smtp_server};
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}
