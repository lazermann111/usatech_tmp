#!/usr/local/USAT/bin/perl

$VERSION = 3.004007;	#[major].[nnn minor][nnn maintenance]
use strict;
use warnings;

BEGIN {	#forks module tuning params
	$ENV{THREADS_NICE} = -1;
	$ENV{THREADS_SOCKET_UNIX} = 1;
}
use forks::BerkeleyDB 0.05;
use forks::BerkeleyDB::shared;
die "forks version 0.23 required--this is only version $forks::VERSION"
	unless $forks::VERSION >= 0.23;
use sigtrap qw(die untrapped normal-signals stack-trace error-signals);
use USAT::App::ReRix::Const qw(:globals);
use Time::HiRes qw(gettimeofday sleep time);
use Time::Local;
use Sys::Statgrab qw(get_process_stats get_error drop_privileges);
use Sys::SigAction;
use Getopt::Long qw(VersionMessage);
use Pod::Usage;
use Data::Compare ();
use Data::Dumper;
use Storable ();
use File::Pid;

use USAT::Common::Const;
use USAT::Common::CallInRecord;

use Thread::Semaphore;
use USAT::Thread::Queue::Any 1.02;
use USAT::Database 0.302003;

use FindBin;
use lib "$FindBin::Bin";
use ReRixConfig qw(%ReRixEngine %ReRix_global %ReRix_SSL);
use constant DEBUG => $ReRixConfig::debug;
use constant PIDFILE => "$ReRix_global{base_path}/$ReRix_global{pid_file}";

use DatabaseConfig;
use QueueDBConfig;

use ReRix::Utils 1.001000;

use constant HANDLER_MODULES => (
	'ReRix::ClientVersion',			# Client Version stuff (only eSuds supported currently)
	'ReRix::DataLog',				# Generic Data Log
	'ReRix::ESuds',					# ESuds Commands
	'ReRix::FileTransferIncoming',	# File Transfer stuff
	'ReRix::FileTransferOutgoing',	# File Transfer stuff
	'ReRix::Gx',					# Gx Peek & Poke
	'ReRix::InfoRequest',			# UNIX time, BCD time, GX Config reqest
	'ReRix::Init',					# Init version 1, 2, 3
	'ReRix::NetLayer',
	'ReRix::Ping',
	'ReRix::Sony',
	'ReRix::Trans20',				# Version 2.0 trans commands
	'ReRix::Update',				# Update status request processing
);
BEGIN {
	if ($^C) {
		eval "use $_" foreach HANDLER_MODULES;
	}
}

use constant {
	MCI_QUEUE__DEFAULT_ENGINE_ID				=> 0,

	MCI_QUEUE_EXECUTE_CD__NEW					=> 'N',
	MCI_QUEUE_EXECUTE_CD__MARKED_FOR_PROCESSING	=> 'Q',
	MCI_QUEUE_EXECUTE_CD__BEING_PROCESSED		=> 'F',
	MCI_QUEUE_EXECUTE_CD__COMPLETE				=> 'Y',
	MCI_QUEUE_EXECUTE_CD__COMPLETE_ERROR		=> 'E',
	MCI_QUEUE_EXECUTE_CD__PURGED				=> 'P',
	
	THR_NON_POOL_CNT => 6	#kludgy but functional way to count # running handlers: total core threads that are not associated with pool (main, shared mem, monitor, handler factory, db queue maintenance, ob queue writer hist)

};
use constant {	#these would likely be better suited as configurable options
	MAIN_LOOP__MAX_SLEEP_SEC					=> 0.25,
	MAIN_LOOP__MIN_SLEEP_SEC					=> 0.05,
	MAIN_THR__MAX_APP_SHUTDOWN_SEC				=> 60,	#seconds
	
	INBOUND_QUEUE_CACHE_CLEANUP_INTERVAL		=> 60,	#seconds
	DB_QUEUE_OPTIMIZATION_INTERVAL				=> 90,	#seconds
	THREAD_STATUS_CHECK_INTERVAL 				=> 30,	#seconds (signifigant impact on all internal monitors; recommended no less than 30 seconds)
	THREAD_STATUS_MAX_FAILURES_DEFAULT 			=> 2,	#max num of consecutive alive check failures allowed

	INBOUND_QUEUE_MAX_MESSAGE_LIFETIME			=> 300,	#seconds (i.e. all messages older than this will be purged, no matter what state they are in)
	
	UPDATE_MCI_QUEUE_DONE__INITIAL_DIVISOR		=> 75,	#affects upper range of how many events are handled at once in sql
	UPDATE_MCI_QUEUE_DONE__DIVISOR_DECREMENT	=> 25,	#affects how many sql events may be required per execute code per loop
};

my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, lines may be out of order

######################################################################
# variables
########################################################################

my $pidfile;

my $sig_int = 0;
my $sig_hup = 0;
our $db_config = new DatabaseConfig;
our $queue_db_config = new QueueDBConfig;
$queue_db_config = $db_config
	if Data::Compare::Compare($db_config, DatabaseConfig->new(%{$db_config}, %{$queue_db_config}), { ignore_hash_keys => ['_permitted'] });	#assign to same object if settings are identical
my $is_queue_db_main_db = $queue_db_config == $db_config;
our $dbh;
our $dbh_queue;
eval "use USAT::Database::DBD::$_ qw(preload_driver)"
	foreach map($_->{driver}, $db_config, $queue_db_config);	#preload DBD::* drivers for optimal memory efficiency

my $command_counter = 0;
my $prog_name = $0;	#(split(/\//, $0))[-1];
my $max_cmd_timeout = DEVICE_TYPE_PROPERTIES->{(sort {DEVICE_TYPE_PROPERTIES->{$b}->{timeout} <=> DEVICE_TYPE_PROPERTIES->{$a}->{timeout}} keys %{+DEVICE_TYPE_PROPERTIES})[0]}->{timeout};
my $last_stat_report_ts = time();
my $application_started = 0;
my $process_commands : shared = 1;

my $command_queue = new USAT::Thread::Queue::Any;
my %inbound_queue_cache = ();	#key = session_id; val = My::ReRix::Session object
my %inbound_queue_cache_expires : shared = ();	#write-only for main thr, read-only for handler thread (key = session_id; val = expires ts (epoch))
my %inbound_queue_done;	#key = execute cd; val = [inbound_id, session_id]
foreach my $execute_cd (MCI_QUEUE_EXECUTE_CD__COMPLETE, MCI_QUEUE_EXECUTE_CD__COMPLETE_ERROR, MCI_QUEUE_EXECUTE_CD__PURGED) {
	my @done_arr : shared;
	$inbound_queue_done{$execute_cd} = \@done_arr;
}
my @outbound_queue : shared;
my @outbound_queue_hist : shared;
my $handler_factory_queue = new USAT::Thread::Queue::Any;
my $thread_monitor_queue = new USAT::Thread::Queue::Any;
my $db_queue_maint_queue = new USAT::Thread::Queue::Any;

my $inbound_queue_cache_cleanup_ts = time();
my $inbound_queue_cache_cleanup_interval = INBOUND_QUEUE_CACHE_CLEANUP_INTERVAL;	#seconds

my $db_queue_optimization_ts = time();
my $db_queue_optimization_interval = DB_QUEUE_OPTIMIZATION_INTERVAL;	#seconds

my %pool = (
	active    => 0, #simple pool monitor flag
	min       => $ReRixEngine{pool_min}, #handler threads
	max       => $ReRixEngine{pool_max}, #units defined by max_type
	max_type  => $ReRixEngine{pool_max_type}, #thread|memory (for all handler threads only)
	queue_max => $ReRixEngine{pool_queue_max}, #max num queue allows before preventing enqueue; if 0 (not implemented yet), then unlimited
	sig => {
		_broadcast_timeout => 10, #seconds
		_lock              => &share({}),
		_semaphore => {
			statistics => new Thread::Semaphore,
		},
		_type => {
			single    => 1,
			broadcast => 2, #thread will block on _lock (parent thread responsible for enough broadcast requests for all handlers)
		},
		shut_down  => 1,
		statistics => 2,
		alive      => 3,
#		reset      => 4,
	},
	peak_load => {
		queue_total => 0,      #num of queued items at peak load
		ts          => time(), #time of peak load (epoch seconds)
		queue_max   => 0,      #max that queue has reached during app lifetime
		ts_max      => time(), #time of max peak load (epoch seconds)
	},
	handler_growby_min   => $ReRixEngine{pool_growby_min}, #min num of threads to grow by each time growth required
	handler_growby_max   => $ReRixEngine{pool_growby_max}, #max num of threads to grow by each time growth required
	handler_timeout      => $ReRixEngine{pool_handler_timeout}, #seconds before pool unused handler downscaling occurs (note: event only checked on receipt of new inbound messages)
	handler_growby_delay => $ReRixEngine{pool_growby_delay}, 
);
my %thread_status : shared = (	#value=time() last set by thread; each critical thread will add its own unique value to this hash
	main	=> time()
);
my %thread_status_max_failures : shared = ();	#value=max num failures; each thread will add its own unique value to this hash (if not using default max failures value)
my @ob_queue_writer_thr = ();	#note: only using 1 at this time
my $num_ob_queue_writer_thr : shared = 0;
my $ob_queue_hist_writer_thr;
my $db_queue_maint_thr;
my $handler_factory_thr;

my ($mem_inuse, $thr_cnt, $ps_href);
our $engine_id;	#package scope so we can use in END block

use constant { 
	DB_ORACLE		=> 'Oracle',
	DB_POSTGRESQL	=> 'Pg'
};

my $queue_db_type = $queue_db_config->{driver} =~ m/Pg$/ ? DB_POSTGRESQL : DB_ORACLE;
my $ib_queue_date_condition = $queue_db_type eq DB_POSTGRESQL	#condition of "valid" messages
	? q{EXTRACT(EPOCH FROM (NOW() - inbound_date)) <= }.INBOUND_QUEUE_MAX_MESSAGE_LIFETIME
	: q{inbound_date >= (}.($queue_db_type eq DB_POSTGRESQL ? 'NOW()' : 'SYSDATE').q{ - (}.INBOUND_QUEUE_MAX_MESSAGE_LIFETIME.q{/86400))};

# SSL environment variables
$ENV{HTTPS_CA_FILE}	= $ReRix_SSL{HTTPS_CA_FILE};
$ENV{HTTPS_PKCS12_FILE}	= $ReRix_SSL{HTTPS_PKCS12_FILE};
$ENV{HTTPS_PKCS12_PASSWORD}	= $ReRix_SSL{HTTPS_PKCS12_PASSWORD};

########################################################################
# main program
########################################################################

sub num_non_handler_thr () {
	return THR_NON_POOL_CNT + $num_ob_queue_writer_thr;
}

sub main {
	drop_privileges();	#drop unnecessary Sys::Statgrab privs, for security

	### handle arguments ###
	my ($version, $help, $no_queue_purge, $force_start);
	Getopt::Long::Configure(qw(no_ignore_case));
	GetOptions(
		'help'				=> \$help,
		'version'			=> \$version,
		'f|forcestart'		=> \$force_start,
		'n|noqueuepurge'	=> \$no_queue_purge,
	) || pod2usage(1);

	### handle version or help ###
	if ($version) {
	  VersionMessage(
		-exitval => 'NOEXIT',
		-verbose => 1,
	  );
	  pod2usage(
		-exitval => 0,
		-sections => 'COPYRIGHT',
		-verbose => 99,
	  );
	}
	pod2usage(-exitval => 0, -verbose => 1) if $help;
	
	### PID file management ###
	$pidfile = File::Pid->new({ file => PIDFILE });
	if ( my $pid = $pidfile->running ) {
		if ($force_start) {
			logline("ReRixEngine: Application instance PID $pid already appears to be running.  Attempting to shut down existing instance...");
			kill('SIGTERM', $pid);
			sleep 2;	#wait for process to settle
			if ($pidfile->running) {
				kill('SIGKILL', $pid);
				sleep 2;	#wait for process to settle
			}
			if ($pidfile->running) {
				logline("ReRixEngine: Existing process ($pid) refused to terminate");
				return;
			}
		} else {
			logline("ReRixEngine: Application instance PID $pid already appears to be running.  Remove this file to start rerix");
			return;
		}
		logline("ReRixEngine: Removing stale PID file: ",$pidfile->file);
	}
	$pidfile->remove;
	$pidfile = File::Pid->new({ file => PIDFILE });
	unless ($pidfile->write) {
		logline("ReRixEngine: unable to write pidfile '".PIDFILE."': $!");
		return;
	}

	### start normal runtime ###
	my $thr_name = 'main';
	_set_main_thr_sig_handlers();
	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	$application_started = 1;
	
	logline('ReRixEngine: Queue database type is ' . ($queue_db_type eq DB_POSTGRESQL ? 'PostgreSQL' : 'Oracle'));
	
	### start thread monitor ###
	$thread_status{$thr_name} = time();
	die "ReRixEngine: Unable to start thread monitor"
		unless threads->new(\&_thr_pool_monitor, \$process_commands);
	die "ReRixEngine: Unable to start DB queue maintenance thread"
		unless $db_queue_maint_thr = threads->new(\&_thr_db_queue_maint);

	### create base handler pool ###
	my $handlers_started : shared = 0;
	if ((my $created_num = pool_growby($pool{min}, \$handlers_started)) != $pool{min}) {
		logline("ReRixEngine: Error creating minimum handler pool: $pool{min} requested, $created_num created");
		return;
	}
	
	### connect main thread to database ###
	$dbh_queue = USAT::Database->new(config => $queue_db_config);
	$dbh = $is_queue_db_main_db ? $dbh_queue : USAT::Database->new(config => $db_config);
	$engine_id = activate_distributed_engine($dbh, $dbh_queue);	#TODO: just activate queue db in future?

	### purge current queue for this engine ###
	if ($no_queue_purge)
	{
		logline("ReRixEngine: SKIPPING purge of existing inbound command queue for rerix engine id $engine_id...");
	}
	else
	{
		_main_update_mci_queue_purge($engine_id, $dbh_queue);
	}
	_main_optimize_db_queues($dbh_queue);

	### load & process inbound messages ###
	my $last_cmd_load_ts = 0;
	my ($db_to_sys_time_adj_sec, $db_to_sys_time_adj_cnt) = (0, 0);
	my @sth_dbqueue;
	my @commands;
	my $next_thread_status_update = time();
	my $loop_start_ts;

	logline('ReRixEngine: Startup complete (took '.sprintf('%.3f', time() - $^T).' seconds)');
	logline('ReRixEngine: Enabling main thread to read from inbound command queue...');
	alarm(THREAD_STATUS_CHECK_INTERVAL * THREAD_STATUS_MAX_FAILURES_DEFAULT);	#last resort main thread lockup protection
	while ($process_commands)
	{
		$loop_start_ts = time();
		_set_main_thr_sig_handlers();

		### get commands from db ###
		my $queue_poll_start_ts = time();
		my $array_ref = _main_mci_dequeue_nb($engine_id, $dbh_queue, \@sth_dbqueue);

		### build command data structure for each command ###
		if ($array_ref->[0]->[0])
		{
			my $db_epoch_ts = dbts_to_epoch_sec(get_date_db($dbh_queue));
			my $epoch_ts = time();
			$db_to_sys_time_adj_sec = (($db_to_sys_time_adj_sec * $db_to_sys_time_adj_cnt) + ($db_epoch_ts - $epoch_ts)) / ++$db_to_sys_time_adj_cnt;
			logline('ReRixEngine: Found '.scalar @{$array_ref}.' new inbound message(s) ('
				.sprintf('%.3f', time() - $queue_poll_start_ts).' seconds to load'
				.($last_cmd_load_ts ? '; '.sprintf('%.3f', time() - $last_cmd_load_ts).' seconds since last messages found' : '')
				.')');
			$last_cmd_load_ts = time();
				
			foreach (@{$array_ref})
			{
				# get the message data
				my $tmp_command = My::ReRix::Session::Message->new(
					inbound_id				=> $_->[0],
					machine_id				=> $_->[1],
					inbound_date			=> (split(/\./o, $_->[2]))[0],	#trim milliseconds, if timestamp returns this
					timestamp				=> $_->[3],
					inbound_command			=> $_->[4],
					execute_date			=> $_->[5],
					execute_cd				=> $_->[6],
					msg_no					=> $_->[7],
					num_times_executed		=> $_->[8],
					session_id				=> $_->[9],
					net_layer_id			=> $_->[10],
					logic_engine_id			=> $engine_id,
					
					db_to_sys_time_adj_sec	=> $db_to_sys_time_adj_sec,	#seconds difference of (db - sys)
				);
				my $session_id = $tmp_command->{session_id};

#				logline("ReRixEngine: Loading msg $tmp_command->{inbound_id} from $tmp_command->{machine_id} (session \#$session_id)...");

				### handle session serialization logic: queue or immediately process, depending on current session state ###
				unless (exists $inbound_queue_cache{$session_id})
				{
					$inbound_queue_cache{$session_id} = My::ReRix::Session->new();
					$inbound_queue_cache{$session_id}->{expires_sec} = $max_cmd_timeout;	#note: handlers will adjust this value per device type
				}
				$tmp_command->{session_expires_sec} = $inbound_queue_cache{$session_id}->{expires_sec};
				
				if (substr($tmp_command->{inbound_command}, 0, 2) ne USAT::App::ReRix::Const::NET_LAYER__INTERNAL_REQUEST)
				{
					$tmp_command->{is_external} = 1;
					$inbound_queue_cache{$session_id}->{in_msg_cnt}++;
				}
				else
				{
					$tmp_command->{is_external} = 0;
				}
				$tmp_command->{session_in_msg_cnt} = $inbound_queue_cache{$session_id}->{in_msg_cnt};

				$inbound_queue_cache{$session_id}->enqueue($tmp_command);
				$inbound_queue_cache_expires{$session_id} = $inbound_queue_cache{$session_id}->expires();	#cache global session expiration for handler thread
				
				if (!$inbound_queue_cache{$session_id}->is_expired()
					&& $inbound_queue_cache{$session_id}->{ready} && $inbound_queue_cache{$session_id}->pending())	#serialized queue ready for another message from this session
				{
					push @commands, $inbound_queue_cache{$session_id}->dequeue();
					$inbound_queue_cache{$session_id}->{ready} = 0;
				}

#				logline("ReRixEngine: Loaded msg $tmp_command->{inbound_id} from $tmp_command->{machine_id} (session \#$session_id)");
			}
			logline('ReRixEngine: Prepared '.scalar @{$array_ref}.' new inbound message(s) for processing');
		}
		
		### prioritize commands ###
		@commands = sort { ENGINE_MSG_PARSER_RULES->{$b->{_command_code}}->{$b->{_command_subcode}}->[3] <=> ENGINE_MSG_PARSER_RULES->{$a->{_command_code}}->{$a->{_command_subcode}}->[3] } @commands
			if @commands;

		### process commands - threaded ###
		my @commands_to_queue;
		if (@commands)
		{
			### queue new commands ###
			my $command_queue_current = $command_queue->pending;
			my $max_element = (scalar @commands < ($pool{queue_max} - $command_queue_current) ? scalar @commands : ($pool{queue_max} - $command_queue_current)) - 1;
			@commands_to_queue = (map { [ undef, $_, ++$command_counter ] } (@commands)[0..$max_element]);	#max messages queue will allow

			if (scalar(@commands_to_queue) < scalar(@commands)) {
				### internal queue exceeded - purge message overflow ###	
				logline("ReRixEngine: Max internal queue size reached ($pool{queue_max} elements): purging ".(scalar(@commands) - scalar(@commands_to_queue))." commands!");
				push @{$inbound_queue_done{+MCI_QUEUE_EXECUTE_CD__PURGED}},
					map(Storable::freeze([$_->{inbound_id}, $_->{session_id}]), (@commands)[($max_element + 1)..(scalar(@commands) - 1)]);	#safe atomic action without locks
			}

			$command_queue_current += scalar @commands_to_queue;
			logline("ReRixEngine: Queueing ".scalar @commands_to_queue." message(s)...");
			$command_queue->enqueue_many( @commands_to_queue );
			logline('ReRixEngine: Messages queued.');

			### adjust pool resources as necessary ###
			my $total_handlers = $thr_cnt - num_non_handler_thr();
			print qq{(($command_queue_current > ($thr_cnt - num_non_handler_thr()))
				&& (($pool{max_type} eq 'thread' && ($thr_cnt - num_non_handler_thr()) < $pool{max}) 
				|| (($pool{max_type} eq 'memory' && $mem_inuse <= $pool{max}) && (($thr_cnt - num_non_handler_thr()) >= $pool{min}))))\n} if DEBUG;
			# add to pool if necessary
			if ($command_queue_current > $total_handlers
				&& (
					$pool{max_type} eq 'thread'
					&& $total_handlers <  $pool{max} 
					|| 
					$pool{max_type} eq 'memory'
					&& $total_handlers >= $pool{min}
					&& $mem_inuse      <= $pool{max}
				)
			) {
				my $pool_optimization_start_ts = time;
				logline('ReRixEngine: Optimizing handler pool resources...');
				my $desired_pool_growth
					= $command_queue_current - $total_handlers;
				my $max_growth = $pool{max} - $total_handlers;
				if ($desired_pool_growth > $max_growth) {
					$desired_pool_growth = $max_growth;
				}
				if ($desired_pool_growth > 0) {
					if ($desired_pool_growth    < $pool{handler_growby_min}) {
						$desired_pool_growth    = $pool{handler_growby_min};
					}
					elsif ($desired_pool_growth > $pool{handler_growby_max}) {
						$desired_pool_growth    = $pool{handler_growby_max};
					}
					logline('ReRixEngine: Attempting to grow thread pool by '
					      . $desired_pool_growth . '...'); 
					my $growth = pool_growby($desired_pool_growth);
					logline("ReRixEngine: Thread pool grew by $growth.");
				}
				logline('ReRixEngine: Optimized handler pool resources ('
				      . sprintf('%.3f', time - $pool_optimization_start_ts)
				      . ' seconds).');
			}
			if ($pool{peak_load}{queue_total} <= $command_queue_current) {
				$pool{peak_load}{queue_total}  = $command_queue_current;
				$pool{peak_load}{ts}           = time;
			}
			if ($pool{peak_load}{queue_max} < $pool{peak_load}{queue_total}) {
				$pool{peak_load}{queue_max} = $pool{peak_load}{queue_total};
				$pool{peak_load}{ts_max}    = time;
			}
			print "$command_queue_current => " . time . " > ("
				. $pool{peak_load}{ts} . " + $pool{handler_timeout})\n"
				if DEBUG;
			if (time > ( $pool{peak_load}{ts} + $pool{handler_timeout} ) ) {
				if ($command_queue_current < $total_handlers
					&& $total_handlers     > $pool{min}
				) {
					logline("ReRixEngine: Attempting to downsize thread pool by 1..."); 
					my $downsize = pool_downsize(1);
					logline("ReRixEngine: Thread pool downsized by $downsize.");
				}
				logline("ReRixEngine: Resetting last known queue peak stat to $command_queue_current (was ".$pool{peak_load}{queue_total}.").");
				$pool{peak_load}{queue_total} = $command_queue_current;
				$pool{peak_load}{ts}          = time;
			}

			### clean up for next loop iteration ###
			@commands = ();
			($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
			logline("ReRixEngine: total mem use=$mem_inuse, total # processes=$thr_cnt; parent thr mem use=".$ps_href->{$$}->{mem}.", max_mem_use_parent=$ReRixEngine{max_mem_use_parent}") if DEBUG;
		}
		
		### mark inbound messaged loaded ###
		if ($array_ref->[0]->[0]) {
			my $load_msg_processing_start_ts = time();
			logline('ReRixEngine: Updating inbound queue to indicate messages being processed...');
			_main_update_mci_queue_processing($engine_id, $dbh_queue, \@sth_dbqueue, [map { $_->[0] } @{$array_ref}]);
			logline('ReRixEngine: Updated inbound queue to indicate messages being processed ('
				.sprintf('%.3f', time() - $load_msg_processing_start_ts).' seconds).');
		}
		
		### make sure min handlers running at all times ###
		if (($thr_cnt - num_non_handler_thr()) < $pool{min}) {
			my $pool_optimization_start_ts = time();
			my $desired_pool_growth = $pool{min} - ($thr_cnt - num_non_handler_thr());
			logline("ReRixEngine: Min pool size not met! Attempting to grow thread pool by $desired_pool_growth...");
			my $growth = pool_growby($desired_pool_growth, \$handlers_started);
			if ($growth != $desired_pool_growth) {
				logline("ReRixEngine: Error creating minimum handler pool: $desired_pool_growth requested, $growth created");
				return;
			}
			logline('ReRixEngine: Optimized handler pool size ('.sprintf('%.3f', time() - $pool_optimization_start_ts).' seconds).');
		}
		
		### update completed messages ###
		my %done_session_ids;
		foreach my $execute_cd (MCI_QUEUE_EXECUTE_CD__COMPLETE, MCI_QUEUE_EXECUTE_CD__COMPLETE_ERROR, MCI_QUEUE_EXECUTE_CD__PURGED) {
			if (scalar @{$inbound_queue_done{$execute_cd}} > 0) {
				my @inbound_ids;
				while (defined(my $value = shift(@{$inbound_queue_done{$execute_cd}})))	#shift is safe atomic action without locks
				{
					my ($id, $session_id, $absolute_session_expires_ts) = eval { @{Storable::thaw($value)} };
					push @inbound_ids, $id;

					### prepare next message for this session, if any exist ###
					if (exists $inbound_queue_cache{$session_id})
					{
						if (!exists($done_session_ids{$session_id}))	{ #do only once for this session this main loop
							$inbound_queue_cache{$session_id}->{ready} = 1;
							$done_session_ids{$session_id} = undef;
						}
						
						$inbound_queue_cache{$session_id}->set_last_msg_processed_ts();
						$inbound_queue_cache{$session_id}->expires($absolute_session_expires_ts)
							if defined $absolute_session_expires_ts;
						$inbound_queue_cache_expires{$session_id} = $inbound_queue_cache{$session_id}->expires();
						
						if (!$inbound_queue_cache{$session_id}->is_expired()
							&& $inbound_queue_cache{$session_id}->{ready} && $inbound_queue_cache{$session_id}->pending())
						{
							push @commands, $inbound_queue_cache{$session_id}->dequeue();
							$inbound_queue_cache{$session_id}->{ready} = 0;
						}
					}
				}

				if (@inbound_ids)
				{
					### update completed messages ###
					my $update_msg_processing_start_ts = time();
					logline('ReRixEngine: Updating inbound queue to mark '.scalar(@inbound_ids).' completed messages (state \''.$execute_cd.'\')...');
					_main_update_mci_queue_done($dbh_queue, $execute_cd, \@inbound_ids);
					logline('ReRixEngine: Updated inbound queue to mark completed messages ('
						.sprintf('%.3f', time() - $update_msg_processing_start_ts).' seconds).');
				}
			}
		}
		
		### clean up after expired sessions ###
		if ($inbound_queue_cache_cleanup_interval + $inbound_queue_cache_cleanup_ts < time())
		{
			my $internal_cache_cleanup_ts = time();
			logline('ReRixEngine: Cleaning internal session cache...');
			my @ids = map($inbound_queue_cache{$_}->is_expired() ? $_ : (), keys %inbound_queue_cache);
			my @inbound_ids = map(map($_->{inbound_id}, @{$inbound_queue_cache{$_}->{queue}}), @ids);
			delete @{inbound_queue_cache_expires}{@ids};
			delete @{inbound_queue_cache}{@ids};
			_main_update_mci_queue_done($dbh_queue, MCI_QUEUE_EXECUTE_CD__PURGED, \@inbound_ids);
			logline('ReRixEngine: Completed cleaning internal session cache ('.(scalar @ids).' deleted; '.(scalar keys %inbound_queue_cache).' not expired) ('
				.sprintf('%.3f', time() - $internal_cache_cleanup_ts).' seconds).');
			$inbound_queue_cache_cleanup_ts = time();
		}
		
		### now issue database-specific table cleanup, if applicable ###
		if ($db_queue_optimization_interval + $db_queue_optimization_ts < time())
		{
			_main_optimize_db_queues($dbh_queue);
			_main_update_mci_queue_purge(
				$engine_id,
				$dbh_queue,
				1,
				MCI_QUEUE_EXECUTE_CD__NEW,
				MCI_QUEUE_EXECUTE_CD__MARKED_FOR_PROCESSING,
				MCI_QUEUE_EXECUTE_CD__BEING_PROCESSED,
				MCI_QUEUE_EXECUTE_CD__COMPLETE,
				MCI_QUEUE_EXECUTE_CD__COMPLETE_ERROR,
				MCI_QUEUE_EXECUTE_CD__PURGED
			);
			$db_queue_optimization_ts = time();
		}
		
		### update thread status monitor ###
		if ($next_thread_status_update <= time()) {
			$thread_status{$thr_name} = time();
			alarm(THREAD_STATUS_CHECK_INTERVAL * THREAD_STATUS_MAX_FAILURES_DEFAULT);	#reset alarm
			$next_thread_status_update = time() + int(THREAD_STATUS_CHECK_INTERVAL / 2);
		}

		### update internal stats ###
		if ($ReRixEngine{stat_report_interval} + $last_stat_report_ts < time())
		{
			report_pool();
			$last_stat_report_ts = time();
		}
		if ($ps_href->{$$}->{mem} > $ReRixEngine{max_mem_use_parent}) {
			logline("ReRixEngine: Max memory exceeded ($ps_href->{$$}->{mem} current; $ReRixEngine{max_mem_use_parent} allowed): restarting immediately");
			$process_commands = 0;
		}
		
#		logline('ReRixEngine: Loop took '.sprintf('%.3f', time() - $loop_start_ts).' seconds...');
		my $sleep = ($loop_start_ts + MAIN_LOOP__MAX_SLEEP_SEC) - time();
		$sleep = $array_ref->[0]->[0] || @commands || $sleep < MAIN_LOOP__MIN_SLEEP_SEC ? MAIN_LOOP__MIN_SLEEP_SEC : $sleep;
#		logline('ReRixEngine: Sleeping for '.sprintf('%.3f', $sleep).' seconds...');
		sleep($sleep);
	}
}

########################################################################
# main program - helper subroutines
########################################################################

sub _main_mci_dequeue_nb ($$$) {
	my ($engine_id, $dbh, $sth_dbqueue_aref) = @_;
	
	$dbh->{print_query} = 0;
	my $c_items_pending = $dbh->query(
		query => ($queue_db_type eq DB_POSTGRESQL
			? q{
				SELECT 1
				FROM engine.machine_cmd_inbound
				WHERE logic_engine_id = ?
				AND execute_cd = ?
				LIMIT 1
				}
			: q{
				SELECT 1
				FROM (
					SELECT 1
					FROM engine.machine_cmd_inbound
					WHERE logic_engine_id = ?
					AND execute_cd = ?
				)
				WHERE ROWNUM = 1
			}
		),
		values => [
			$engine_id,
			MCI_QUEUE_EXECUTE_CD__NEW
		],
		sth	=> \$sth_dbqueue_aref->[0]
	);
	
	my $array_ref;
	if ($c_items_pending->[0]->[0]) {
		my $c_num_rows = $dbh->do(
			query => q{
				UPDATE engine.machine_cmd_inbound
				SET execute_cd = ?
				WHERE logic_engine_id = ?
				AND execute_cd = ?},
#				AND }.$ib_queue_date_condition,
			values => [
				MCI_QUEUE_EXECUTE_CD__MARKED_FOR_PROCESSING,
				$engine_id,
				MCI_QUEUE_EXECUTE_CD__NEW
			],
			sth	=> \$sth_dbqueue_aref->[1]
		);
	
		if (defined $c_num_rows && $c_num_rows > 0) {
			$dbh->do(query => "ALTER SESSION SET nls_date_format = '".DB_DEFAULT_DATE_FORMAT."'") if $queue_db_type eq DB_ORACLE;
			$array_ref = $dbh->query(
				query	=> q{
					SELECT inbound_id, machine_id, inbound_date, timestamp, inbound_command, execute_date, execute_cd, inbound_msg_no, num_times_executed, session_id, net_layer_id
					FROM engine.machine_cmd_inbound
					WHERE logic_engine_id = ?
					AND execute_cd = ?
				}
				,values	=> [
					$engine_id,
					MCI_QUEUE_EXECUTE_CD__MARKED_FOR_PROCESSING
				],
				sth	=> \$sth_dbqueue_aref->[2]
			);
		}
	}
	$dbh->{print_query} = 1;
	
	return $array_ref;
}

sub _main_update_mci_queue_processing ($$$$) {
	my ($engine_id, $dbh, $sth_dbqueue_aref, $inbound_ids) = @_;
	
	$dbh->{print_query} = 0;
	my $c_num_rows = $dbh->do(
		query => q{
			UPDATE engine.machine_cmd_inbound
			SET execute_cd = ?
			WHERE logic_engine_id = ?
			AND execute_cd = ?
		},
		values => [
			MCI_QUEUE_EXECUTE_CD__BEING_PROCESSED,
			$engine_id,
			MCI_QUEUE_EXECUTE_CD__MARKED_FOR_PROCESSING
		],
		sth	=> \$sth_dbqueue_aref->[3]
	);
	$dbh->{print_query} = 1;
	
	return 1;
}

sub _main_update_mci_queue_done ($$$) {
	my ($dbh, $execute_cd, $inbound_ids) = @_;
	my @inbound_ids_sorted = sort @{$inbound_ids};
	
	### update in variable group sizes defined by variable divisor lengths ###
	my $div = UPDATE_MCI_QUEUE_DONE__INITIAL_DIVISOR;
	
	$dbh->begin_work;
	if (scalar @inbound_ids_sorted >= $div) {
		while ($div > 0 && @inbound_ids_sorted) {
			for (1..(int @inbound_ids_sorted / $div)) {
				my @to_delete = (splice(@inbound_ids_sorted, 0, $div));
				$dbh->do(
					query => q{
						INSERT INTO engine.machine_cmd_inbound_hist (
							inbound_id, machine_id, inbound_date,
							inbound_command, timestamp, execute_date,
							execute_cd,
							inbound_msg_no, num_times_executed, logic_engine_id,
							session_id, net_layer_id
						) (
							SELECT
								inbound_id, machine_id, inbound_date,
								inbound_command, timestamp, }.($queue_db_type eq DB_POSTGRESQL ? 'NOW()' : 'SYSDATE').q{,
								?,
								inbound_msg_no, 1, logic_engine_id,
								session_id, net_layer_id
							FROM engine.machine_cmd_inbound
							WHERE inbound_id IN(}.join(',', ('?') x $div).q{)
								AND SUBSTR(inbound_command, 1, 2) <> ?
						)
					}
					,values => [
						$execute_cd,
						@to_delete,
						USAT::App::ReRix::Const::NET_LAYER__INTERNAL_REQUEST
					]
				);
				$dbh->do(
					query => q{
						DELETE FROM engine.machine_cmd_inbound
						WHERE inbound_id IN(}.join(',', ('?') x $div).q{)
					}
					,values => [
						@to_delete
					]
				);
			}
			$div -= UPDATE_MCI_QUEUE_DONE__DIVISOR_DECREMENT;
		}
	}

	### update remainder ###
	if (@inbound_ids_sorted) {
		$dbh->do(
			query => q{
				INSERT INTO engine.machine_cmd_inbound_hist (
					inbound_id, machine_id, inbound_date,
					inbound_command, timestamp, execute_date,
					execute_cd,
					inbound_msg_no, num_times_executed, logic_engine_id,
					session_id, net_layer_id
				) (
					SELECT
						inbound_id, machine_id, inbound_date,
						inbound_command, timestamp, }.($queue_db_type eq DB_POSTGRESQL ? 'NOW()' : 'SYSDATE').q{,
						?,
						inbound_msg_no, 1, logic_engine_id,
						session_id, net_layer_id
					FROM engine.machine_cmd_inbound
					WHERE inbound_id IN(}.join(',', ('?') x scalar @inbound_ids_sorted).q{)
						AND SUBSTR(inbound_command, 1, 2) <> ?
				)
			}
			,values => [
				$execute_cd,
				@inbound_ids_sorted,
				USAT::App::ReRix::Const::NET_LAYER__INTERNAL_REQUEST
			]
		);
		$dbh->do(
			query => q{
				DELETE FROM engine.machine_cmd_inbound
				WHERE inbound_id IN(}.join(',', ('?') x scalar @inbound_ids_sorted).q{)
			}
			,values => [
				@inbound_ids_sorted
			]
		);
	}
	$dbh->commit;
	
	return 1;
}

sub _main_update_mci_queue_purge ($$;$@) {
	my ($engine_id, $dbh, $use_ib_date_condition, @execute_cds) = @_;
	
	my $where_clause = '';
	if ($use_ib_date_condition) {
		$where_clause .= q{ AND NOT }.$ib_queue_date_condition;
	}
	$where_clause .= qq{ AND execute_cd IN (}.join(',', ('?') x scalar @execute_cds).q{)}
		if @execute_cds;
	
	### update in variable group sizes defined by variable divisor lengths ###
	my $db_queue_cleanup_ts = time();
	logline('ReRixEngine: Cleaning inbound database queue'.(@execute_cds ? ' (where execute_cd IN ('.join(',', @execute_cds).')' : '').'...');
	$dbh->begin_work;
	my $num_rows = $dbh->do(
		query => q{
			UPDATE engine.machine_cmd_inbound
			SET execute_cd = 'P'
			WHERE logic_engine_id IN (?, ?)
		}.$where_clause,
		values => [
			$engine_id, 0,
			@execute_cds
		]
	);
	if (defined $num_rows && $num_rows > 0) {
		$dbh->do(
			query => q{
				INSERT INTO engine.machine_cmd_inbound_hist (
					inbound_id, machine_id, inbound_date,
					inbound_command, timestamp, execute_date,
					execute_cd,
					inbound_msg_no, num_times_executed, logic_engine_id,
					session_id, net_layer_id
				) (
					SELECT
						inbound_id, machine_id, inbound_date,
						inbound_command, timestamp, }.($queue_db_type eq DB_POSTGRESQL ? 'NOW()' : 'SYSDATE').q{,
						execute_cd,
						inbound_msg_no, 1, logic_engine_id,
						session_id, net_layer_id
					FROM engine.machine_cmd_inbound
					WHERE logic_engine_id IN (?, ?)
					AND execute_cd = 'P'
					AND SUBSTR(inbound_command, 1, 2) <> ?
				)
			},
			values => [
				$engine_id, 0, USAT::App::ReRix::Const::NET_LAYER__INTERNAL_REQUEST
			]
		);
		$dbh->do(
			query => q{
				DELETE FROM engine.machine_cmd_inbound
				WHERE logic_engine_id IN (?, ?)
				AND execute_cd = 'P'
			},
			values => [
				$engine_id, 0
			]
		);
	}
	$dbh->commit;
	logline('ReRixEngine: Completed cleaning inbound database queue ('
		.sprintf('%.3f', time() - $db_queue_cleanup_ts).' seconds).');
	
	return 1;
}

sub _main_optimize_db_queues () {
	my $dbh_queue = shift;
	logline('ReRixEngine: Requesting DB Queue maintenance...');
	$db_queue_maint_queue->enqueue(undef, 1);
	return 1;
}

########################################################################
# special application states
########################################################################

END {
	if (!threads->tid && $application_started) {	#main thread only
		alarm(0);	#disable main thread watchdog
		my $app_shutdown_start_ts = time();
		eval {
			Sys::SigAction::set_sig_handler(
				'ALRM',
				sub { die 'Main thread appears to have hung during shut down' },
				{ mask => ['ALRM'] }
			);
			alarm(MAIN_THR__MAX_APP_SHUTDOWN_SEC);
			logline('ReRixEngine: Shut down: disabling engine inbound reader...');
			deactivate_distributed_engine($engine_id, $dbh, $dbh_queue) if $engine_id;

			logline('ReRixEngine: Shut down: sending shut down signals to all threads...');
			my @thread_list = ();
			my @ob_queue_thr_tids = map($_->tid, (@ob_queue_writer_thr, ($ob_queue_hist_writer_thr ? $ob_queue_hist_writer_thr : ())));
			foreach (threads->list(threads::running)) {	#note: maybe also include threads::joinable?
				my $tid = $_->tid;
				push @thread_list, $_ unless grep(/^$tid$/, @ob_queue_thr_tids);	#all but OB queue writers
			}

			pool_shutdown();
			logline('ReRixEngine: Shut down: waiting for all threads to shutdown safely...');

			my $current_thr;
			Sys::SigAction::set_sig_handler(
				'ALRM',
				sub {
					die $current_thr 
						? 'Thread #'.$current_thr->tid.' did not respond to shut down or exit in time'
						: 'Some threads did not respond to shut down or exit in time';
				},
				{ mask => ['ALRM'] }
			);

			foreach (@thread_list) {	#join all but OB queue writer(s)
				$current_thr = $_;
				$_->join;
			}

			logline('ReRixEngine: Shut down: sending shut down signals to outbound queue thread...');	#TODO: assume only one ob queue writer at this time
			push @outbound_queue, Storable::freeze([{tid => undef, type => $pool{sig}->{_type}->{single}, sig => $pool{sig}->{shut_down}}, undef, undef]);
			logline('ReRixEngine: Shut down: waiting for outbound queue thread to shutdown safely...');
			foreach (@ob_queue_writer_thr) {	#join OB writer thread(s)
				$current_thr = $_;
				$_->join;
			}
			if ($ob_queue_hist_writer_thr) {
				push @outbound_queue_hist, Storable::freeze([{tid => undef, type => $pool{sig}->{_type}->{single}, sig => $pool{sig}->{shut_down}}, undef, undef]);
				logline('ReRixEngine: Shut down: waiting for outbound queue history thread to shutdown safely...');
			}
			$ob_queue_hist_writer_thr->join;
		};
		alarm(0);
		logline("ReRixEngine: Shut down: Force terminating application immediately: $@")
			if $@;

		eval { $dbh->close; };
		eval { $dbh_queue->close; } unless $is_queue_db_main_db;
		eval { $pidfile->remove; };
		logline('ReRixEngine: ****************** Shut down: shut down complete ('.sprintf('%.3f', time() - $app_shutdown_start_ts).' seconds)');
		exit 1 if $sig_int == 0;	#restart
		exit 2;						#shut down
	}
}

########################################################################
# pool subroutines
########################################################################

sub pool_growby ($$;@) {
	my $num_threads = shift;
	my $handlers_ready_ref = shift;
	return undef unless $num_threads > 0;
	my $total_handlers = $thr_cnt - num_non_handler_thr();
	return 0 if $total_handlers >= $pool{max};
	my $desired_num_handlers = $total_handlers + $num_threads < $pool{max}
		? $total_handlers + $num_threads
		: $pool{max};

#	my $dbh_def = UNIVERSAL::isa($dbh, 'USAT::Database');
#	my $dbh_queue_def = UNIVERSAL::isa($dbh_queue, 'USAT::Database');
#	$dbh->close() if $dbh_def;
#	$dbh_queue->close() if $dbh_queue_def;
	$pool{active} = 1;

	logline("ReRixEngine: Requesting creation of new handler threads...");
	$handler_factory_thr = threads->new(
		\&_thr_handler_factory, $handlers_ready_ref
	) if !$handler_factory_thr;

	### start ob queue writer threads ###
	if (!@ob_queue_writer_thr) {	#TODO: only create one writer at this time...
		logline("ReRixEngine: Trying to create new outbound writer thread...");
		if (my $ob_queue_thr = threads->new(\&_thr_ob_queue_writer)) {
			push @ob_queue_writer_thr, $ob_queue_thr;
			unless ($is_queue_db_main_db || $ob_queue_hist_writer_thr) {	#only create one history writer
				$ob_queue_hist_writer_thr = threads->new(
					\&_thr_ob_queue_hist_writer
				);
				die "Unable to create outbound history writer thread"
					unless $ob_queue_hist_writer_thr;
			}
			$num_ob_queue_writer_thr++;
		} else {
			die "Unable to create outbound writer thread";
		}
		logline("ReRixEngine: Outbound writer thread created.");
	}

	### create handler pool ###
	my $num_new_handlers = $desired_num_handlers - $total_handlers;
	{
		lock(${$handlers_ready_ref}) if $handlers_ready_ref;
		$handler_factory_queue->enqueue(undef, $num_new_handlers);
		if ($handlers_ready_ref) {
			logline("ReRixEngine: Waiting for min pool to be created...");
			${$handlers_ready_ref} = 0;
			cond_wait(${$handlers_ready_ref})
				until ${$handlers_ready_ref} != 0;
			logline("ReRixEngine: Min pool creation complete.");
		}
	}
	
#	$dbh_queue = USAT::Database->new(config => $queue_db_config) if $dbh_queue_def;
#	$dbh = $is_queue_db_main_db ? $dbh_queue : USAT::Database->new(config => $db_config) if $dbh_def;
	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);

	return $handlers_ready_ref
		? ${$handlers_ready_ref}
		: ($desired_num_handlers - $total_handlers);
}

sub pool_downsize ($) {
	my $num_threads = shift;
	return undef unless $num_threads > 0;
	my $total_handlers = $thr_cnt - num_non_handler_thr();
	return 0 if $total_handlers <= $pool{min};

	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	my $desired_num_handlers = $total_handlers - $num_threads > $pool{min}
		? $total_handlers - $num_threads
		: $pool{min};
	my @signal = (
		{
			tid  => undef,
			type => $pool{sig}->{_type}->{single},
			sig  => $pool{sig}->{shut_down},
		},
		undef, undef,
	);

	my $num_sig = $total_handlers - $desired_num_handlers;
	if ($num_sig > 0) {
		logline("ReRixEngine: downsizing pool: sending shutdown signal to $num_sig handler(s)...");
		$command_queue->enqueue_many( (\@signal) x $num_sig );
	}
	return $num_sig;
}

sub pool_shutdown ()
{
	$pool{active} = 0;
	### prepare pool for signal ###
#	{
#		lock($pool{sig}->{_lock});
#		$pool{sig}->{_lock}->{$pool{sig}->{shut_down}} = 0;
#	}

	### send signal ###
	my @signal = ({tid => undef, type => $pool{sig}->{_type}->{single}, sig => $pool{sig}->{shut_down}}, undef, undef);
	#note: should be type broadcast when signal logic improved (to prevent any possible deadlock from a premature handler death), so we can monitor handler self-terminations and force-exit if any handlers stuck in deadlock
	my $num_sig = (&get_mem_stats([$prog_name]))[1] - num_non_handler_thr();
	$command_queue->enqueue_many( (\@signal) x $num_sig );
	$handler_factory_queue->enqueue(@signal);
	$db_queue_maint_queue->enqueue(@signal);
	$thread_monitor_queue->enqueue(@signal);
}

sub report_pool ()
{
	### prepare pool for signal ###
#	lock($pool{sig}->{_lock});
#	$pool{sig}->{_lock}->{$pool{sig}->{statistics}} = 0;

	### send signal ###
	my $stats = get_handler_thr_stats('main', $^T, $command_counter, undef);
	logline($stats);
	$stats =~ s/[ ]{27}//go;
	{
		$pool{sig}->{_semaphore}->{statistics}->down;
		my $f = myopen(">$ReRix_global{base_path}/stats.dat");
		print $f "$stats\n";
		close $f;
		$pool{sig}->{_semaphore}->{statistics}->up;
	}
	return 1 unless DEBUG;	#No stats request sent to other threads unless in debug mode

	my @signal = ({tid => undef, type => $pool{sig}->{_type}->{single}, sig => $pool{sig}->{statistics}}, undef, undef);
	my $num_sig = (&get_mem_stats([$prog_name]))[1] - num_non_handler_thr();
	$command_queue->enqueue_many( (\@signal) x $num_sig);	#note: should be type broadcast when signal logic improved
	$handler_factory_queue->enqueue(@signal);
	$db_queue_maint_queue->enqueue(@signal);
	$thread_monitor_queue->enqueue(@signal);
	push @outbound_queue, Storable::freeze(\@signal);	#TODO: assume only one ob queue writer at this time
	push @outbound_queue_hist, Storable::freeze(\@signal) unless $is_queue_db_main_db;	#TODO: assume only one ob queue at this time
#	cond_wait($pool{sig}->{_lock}) until $pool{sig}->{_lock}->{$pool{sig}->{statistics}} >= ((&get_mem_stats([$prog_name]))[1] - num_non_handler_thr());

	return 1;
}

########################################################################
# thread subroutines
########################################################################

sub threads_list_died () {	#extra feature I wish were in forks.pm: list of threads that prematurely died before a join() or detatch() attempted
	my @threads;
	
	### get list of active processes ###
	my @pid_list = sort keys %{(get_mem_stats([$prog_name]))[2]};

	### determine which threads prematurely died ###
	foreach my $thr_obj (threads->list())
	{
		my $regex = quotemeta $thr_obj->{pid};
		push @threads, $thr_obj unless grep(/^$regex$/, @pid_list);
	}
	return @threads;
}

sub _thr_pool_monitor {
	_set_default_sig_handlers();
	my $thr_name = 'Thread Monitor';
	my $thr_created_time = time();
	($mem_inuse, $thr_cnt, $ps_href) = (undef, undef, undef);
	my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, buffering delayed until thread exit
	
	my ($total_processed_commands, $total_processed_sig) = (0, 0);
	my $thr_process_commands = 1;
	my $next_thread_status_update = time();
	
	my $main_thread_process_commands_ref = shift;
	my %thr_consecutive_failures = ();	#key=thread name; value=number of failures
	
	logline("*** $thr_name created.");
	while ($thr_process_commands)
	{
		my @todo = $thread_monitor_queue->dequeue_tw($next_thread_status_update);
		my ($thr_sig_href, $desired_pool_growth, @args) = @todo;
		logline("*** $thr_name: Got an event") if @todo;
		print Dumper($thr_sig_href, $desired_pool_growth, @args) if DEBUG;
		if (!@todo) {	#timedwait expired
			logline("*** $thr_name: Checking status of threads...");		
			my @keys = keys %thread_status;
			foreach (@keys) {
				my $status_ts = $thread_status{$_};
				my $max_failures = exists $thread_status_max_failures{$_} ? $thread_status_max_failures{$_} : THREAD_STATUS_MAX_FAILURES_DEFAULT;
				if ($status_ts < time() - THREAD_STATUS_CHECK_INTERVAL)	{
					$thr_consecutive_failures{$_}++;
					if ($thr_consecutive_failures{$_} >= $max_failures) {
						logline("*** $thr_name: Thread '$_' appears dead (no status update in over ".THREAD_STATUS_CHECK_INTERVAL()
							." seconds): Immediately requesting main thread to restart application");		
						${$main_thread_process_commands_ref} = 0;
					} else {
						logline("*** $thr_name: Thread '$_' has not recently updated status (no status update in over ".THREAD_STATUS_CHECK_INTERVAL()
							." seconds): will try ".($max_failures - $thr_consecutive_failures{$_})." more time(s)");		
					}
				}
				else {
					delete $thr_consecutive_failures{$_};
				}
			}
			logline("*** $thr_name: Status check complete.");		
		}
		elsif (defined $thr_sig_href)	#thread signal sent (keys=tid, type, sig); process, then return to queue handling
		{
			$total_processed_sig++;
			### handle signal (single & broadcast) ###
			if ($thr_sig_href->{sig} eq $pool{sig}->{shut_down})
			{
				logline("*** $thr_name: shut down request received...");
				$thr_process_commands = 0;
			}
			elsif ($thr_sig_href->{sig} eq $pool{sig}->{statistics})
			{
				logline("*** $thr_name: statistics request received...");
				my $stats = get_thread_monitor_stats($thr_name, $thr_created_time);
				logline($stats);
				$stats =~ s/[ ]{27}//go;
				$pool{sig}->{_semaphore}->{statistics}->down;
				my $f = myopen(">>$ReRix_global{base_path}/stats.dat");
				print $f "$stats\n";
				close $f;
				$pool{sig}->{_semaphore}->{statistics}->up;
			}
			elsif ($thr_sig_href->{sig} eq $pool{sig}->{alive})
			{
				logline("*** $thr_name".threads->tid.": alive status request received...");
				$next_thread_status_update = time();
			}
			else
			{
				logline("*** $thr_name: ignoring unknown message $thr_sig_href->{sig}...");
			}
		}
		else	{	#process commands normally
			$total_processed_commands++;
		}
		
		### update thread status monitor ###
		$next_thread_status_update = time() + THREAD_STATUS_CHECK_INTERVAL;
	}
	logline("*** $thr_name: shutting down...");
	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	logline(get_thread_monitor_stats($thr_name, $thr_created_time));
	logline("*** $thr_name: shut down.");
}

sub _thr_ob_queue_writer {
	_set_default_sig_handlers();
	my $thr_name = 'OB Queue Writer ('.threads->tid.')';
	my $thr_created_time = time();
	($mem_inuse, $thr_cnt, $ps_href) = (undef, undef, undef);
	my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, buffering delayed until thread exit
	$thread_status{$thr_name} = time();

	my $qdbhit = USAT::Database->new(config => $is_queue_db_main_db ? $db_config : $queue_db_config);
	my ($total_processed_commands, $total_processed_sig) = (0, 0);
	my %commands_processed;
	my $thr_process_commands = 1;
	my $next_thread_status_update = time();

	logline("$thr_name created.");
	while ($thr_process_commands)
	{
		if (my $event = shift(@outbound_queue))
		{
			logline("*** $thr_name: Got an event");
			my ($thr_sig_href, $command_hashref, $response) = eval { @{Storable::thaw($event)} };
			print Dumper($thr_sig_href, $command_hashref, $response) if DEBUG;
			if (defined $thr_sig_href)	#thread signal sent (keys=tid, type, sig); process, then return to queue handling
			{
				$total_processed_sig++;
				### handle signal (single & broadcast) ###
				if ($thr_sig_href->{sig} eq $pool{sig}->{shut_down})
				{
					logline("*** $thr_name: shut down request received...");
					$thr_process_commands = 0;
				}
				elsif ($thr_sig_href->{sig} eq $pool{sig}->{statistics})
				{
					logline("*** $thr_name: statistics request received...");
					my $stats = get_handler_thr_stats($thr_name, $thr_created_time, $total_processed_commands, \%commands_processed);
					logline($stats);
					$stats =~ s/[ ]{27}//go;
					$pool{sig}->{_semaphore}->{statistics}->down;
					my $f = myopen(">>$ReRix_global{base_path}/stats.dat");
					print $f "$stats\n";
					close $f;
					$pool{sig}->{_semaphore}->{statistics}->up;
				}
				elsif ($thr_sig_href->{sig} eq $pool{sig}->{alive})
				{
					logline("*** $thr_name: alive status request received...");
					$next_thread_status_update = time();
				}
				else
				{
					logline("*** $thr_name: ignoring unknown message $thr_sig_href->{sig}...");
				}
			}
			else	{	#process commands normally
				$total_processed_commands++;

				### create/validate database connection ###
				unless (defined $qdbhit && $qdbhit->ping)
				{
					logline("*** $thr_name: Trying to load database handles...");
					$qdbhit->close if defined $qdbhit;
					$qdbhit = USAT::Database->new(config => $queue_db_config);
				}
				$qdbhit->do(query => "ALTER SESSION SET nls_date_format = '".DB_DEFAULT_DATE_FORMAT."'") if $is_queue_db_main_db;

				my $cmd_execution_start_ts = time();
				logline("*** $thr_name: Pushing row to outbound queue...");
				my $mco_command_id = $qdbhit->sequence_next(sequence => 'engine.seq_machine_cmd_outbound_id');
				my $vals_aref = [
					$mco_command_id,
					$command_hashref->{machine_id},
					$response,
					$command_hashref->{session_id},
					$command_hashref->{net_layer_id}
				];
				my $result = $qdbhit->do(
					query => q{
						INSERT INTO engine.machine_cmd_outbound (
							command_id, modem_id, command, session_id, net_layer_id
						) VALUES (
							?, ?, ?, ?, ?
						)
					},
					values => $vals_aref
				);
				if (defined $result && $result > 0)
				{
					my $cmd_name = substr($response, 2, 2);
					$commands_processed{$cmd_name}->{__}++;
					push @{$vals_aref}, 'Y';
					logline("*** $thr_name: Pushed row id \#$mco_command_id to outbound queue (".sprintf('%.3f', time() - $cmd_execution_start_ts).' seconds)');
				}
				else
				{
					push @{$vals_aref}, 'E';
					logline("*** $thr_name: Outbound queue writer error: ".$qdbhit->errstr);
				}
				push @outbound_queue_hist, Storable::freeze([undef, $vals_aref]) unless $is_queue_db_main_db;

				### clean up before next message ###
#				$qdbhit->clear_statement_handles();	#is this necessary??
			}
		}
		else
		{
			logline("*** $thr_name: Sleeping & looping") if DEBUG;
			sleep 1;
		}
		
		### update thread status monitor ###
		if ($next_thread_status_update <= time()) {
			$thread_status{$thr_name} = time();
			$next_thread_status_update = time() + int(THREAD_STATUS_CHECK_INTERVAL / 2);
		}
	}
	logline("*** $thr_name: shutting down...");
	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	logline(get_handler_thr_stats($thr_name, $thr_created_time, $total_processed_commands, \%commands_processed));
	$qdbhit->close if defined $qdbhit;
	logline("*** $thr_name: shut down.");
}

sub _thr_ob_queue_hist_writer {
	_set_default_sig_handlers();
	my $thr_name = 'OB Queue History Writer';
	my $thr_created_time = time();
	($mem_inuse, $thr_cnt, $ps_href) = (undef, undef, undef);
	my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, buffering delayed until thread exit
	$thread_status{$thr_name} = time();

	my $dbhit = USAT::Database->new(config => $db_config);
	my ($total_processed_commands, $total_processed_sig) = (0, 0);
	my %commands_processed;
	my $thr_process_commands = 1;
	my $next_thread_status_update = time();

	logline("$thr_name created.");
	while ($thr_process_commands)
	{
		if (my $event = shift(@outbound_queue_hist))
		{
			logline("*** $thr_name: Got an event");
			my ($thr_sig_href, $data_aref) = eval { @{Storable::thaw($event)} };
			print Dumper($thr_sig_href, $data_aref) if DEBUG;
			if (defined $thr_sig_href)	#thread signal sent (keys=tid, type, sig); process, then return to queue handling
			{
				$total_processed_sig++;
				### handle signal (single & broadcast) ###
				if ($thr_sig_href->{sig} eq $pool{sig}->{shut_down})
				{
					logline("*** $thr_name: shut down request received...");
					$thr_process_commands = 0;
				}
				elsif ($thr_sig_href->{sig} eq $pool{sig}->{statistics})
				{
					logline("*** $thr_name: statistics request received...");
					my $stats = get_handler_thr_stats($thr_name, $thr_created_time, $total_processed_commands, \%commands_processed);
					logline($stats);
					$stats =~ s/[ ]{27}//go;
					$pool{sig}->{_semaphore}->{statistics}->down;
					my $f = myopen(">>$ReRix_global{base_path}/stats.dat");
					print $f "$stats\n";
					close $f;
					$pool{sig}->{_semaphore}->{statistics}->up;
				}
				elsif ($thr_sig_href->{sig} eq $pool{sig}->{alive})
				{
					logline("*** $thr_name: alive status request received...");
					$next_thread_status_update = time();
				}
				else
				{
					logline("*** $thr_name: ignoring unknown message $thr_sig_href->{sig}...");
				}
			}
			else	{	#process commands normally
				$total_processed_commands++;

				### create/validate database connection ###
				unless (defined $dbhit && $dbhit->ping)
				{
					logline("*** $thr_name: Trying to load database handles...");
					$dbhit->close if defined $dbhit;
					$dbhit = USAT::Database->new(config => $db_config);
				}

				my $cmd_execution_start_ts = time();
				logline("*** $thr_name: Pushing row to outbound queue hist...");
				if (ref $data_aref eq 'ARRAY' && substr($data_aref->[2], 2, 2) ne USAT::App::ReRix::Const::NET_LAYER__INTERNAL_RESPONSE)
				{
					my $result = $dbhit->do(
						query => q{
							INSERT INTO engine.machine_cmd_outbound_hist (
								command_id, modem_id, command, session_id, net_layer_id, execute_cd, command_date, execute_date
							) VALUES (
								?, ?, ?, ?, ?, ?, SYSDATE, (SYSDATE + 1/86400)
							)
						},
						values => $data_aref
					);
					if (defined $result && $result > 0)
					{
						my $cmd_name = substr($data_aref->[2], 2, 2);
						$commands_processed{$cmd_name}->{__}++;
						logline("*** $thr_name: Pushed row id \#$data_aref->[0] to outbound queue hist (".sprintf('%.3f', time() - $cmd_execution_start_ts).' seconds)');
					}
					else
					{
						logline("*** $thr_name: Outbound queue history error: ".$dbhit->errstr);
					}
				}

				### clean up before next message ###
#				$dbhit->clear_statement_handles();	#is this necessary??
			}
		}
		else
		{
			logline("*** $thr_name: Sleeping & looping") if DEBUG;
			sleep 5;
		}
		
		### update thread status monitor ###
		if ($next_thread_status_update <= time()) {
			$thread_status{$thr_name} = time();
			$next_thread_status_update = time() + int(THREAD_STATUS_CHECK_INTERVAL / 2);
		}
	}
	logline("*** $thr_name: shutting down...");
	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	logline(get_handler_thr_stats($thr_name, $thr_created_time, $total_processed_commands, \%commands_processed));
	$dbhit->close if defined $dbhit;
	logline("*** $thr_name: shut down.");
}

sub _thr_db_queue_maint {	#TODO: add ib queue cleanup as an event?
	_set_default_sig_handlers();
	my $thr_name = 'DB Queue Maintenance';
	my $thr_created_time = time();
	($mem_inuse, $thr_cnt, $ps_href) = (undef, undef, undef);
	my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, buffering delayed until thread exit
	$thread_status{$thr_name} = time();

	my $dbhit = USAT::Database->new(config => $is_queue_db_main_db ? $db_config : $queue_db_config);
	$thread_status_max_failures{$thr_name} = THREAD_STATUS_MAX_FAILURES_DEFAULT + int(1 / (THREAD_STATUS_CHECK_INTERVAL / (2 * 60)));	#2 extra minutes allowed

	my ($total_processed_commands, $total_processed_sig) = (0, 0);
	my %commands_processed;
	my $thr_process_commands = 1;
	my $next_thread_status_update = time();

	logline("$thr_name created.");
	while ($thr_process_commands)
	{
		my @todo = $db_queue_maint_queue->dequeue_tw($next_thread_status_update);
		my ($thr_sig_href, $event_type) = @todo;
		logline("*** $thr_name: Got an event") if @todo;
		print Dumper($thr_sig_href, $event_type) if DEBUG;
		
		if (!@todo) {	#timedwait expired
			logline("*** $thr_name: Woke up with nothing to do...");
		}
		elsif (defined $thr_sig_href)	#thread signal sent (keys=tid, type, sig); process, then return to queue handling
		{
			$total_processed_sig++;
			### handle signal (single & broadcast) ###
			if ($thr_sig_href->{sig} eq $pool{sig}->{shut_down})
			{
				logline("*** $thr_name: shut down request received...");
				$thr_process_commands = 0;
			}
			elsif ($thr_sig_href->{sig} eq $pool{sig}->{statistics})
			{
				logline("*** $thr_name: statistics request received...");
				my $stats = get_handler_thr_stats($thr_name, $thr_created_time, $total_processed_commands, \%commands_processed);
				logline($stats);
				$stats =~ s/[ ]{27}//go;
				$pool{sig}->{_semaphore}->{statistics}->down;
				my $f = myopen(">>$ReRix_global{base_path}/stats.dat");
				print $f "$stats\n";
				close $f;
				$pool{sig}->{_semaphore}->{statistics}->up;
			}
			elsif ($thr_sig_href->{sig} eq $pool{sig}->{alive})
			{
				logline("*** $thr_name: alive status request received...");
				$next_thread_status_update = time();
			}
			else
			{
				logline("*** $thr_name: ignoring unknown message $thr_sig_href->{sig}...");
			}
		}
		else	{	#process commands normally
			$total_processed_commands++;

			### create/validate database connection ###
			unless (defined $dbhit && $dbhit->ping)
			{
				logline("*** $thr_name: Trying to load database handles...");
				$dbhit->close if defined $dbhit;
				$dbhit = USAT::Database->new(config => $db_config);
			}
			
#			if ($event_type eq ...) {
			my $cmd_execution_start_ts = time();
			if ($queue_db_type eq DB_POSTGRESQL) {
				my $db_queue_cleanup_ts = time();
				logline("*** $thr_name: Optimizing database queues...");
				$dbhit->do(query => qq{VACUUM ANALYZE $_})
					foreach ('engine.machine_cmd_inbound', 'engine.machine_cmd_outbound', 'engine.net_layer_device_session');
				logline("*** $thr_name: Completed optimizing database queues ("
					.sprintf('%.3f', time() - $db_queue_cleanup_ts).' seconds).');
			}
#			}

			### clean up before next message ###
#			$dbhit->clear_statement_handles();	#is this necessary??
		}
		
		### update thread status monitor ###
		if ($next_thread_status_update <= time()) {
			$thread_status{$thr_name} = time();
			$next_thread_status_update = time() + int(THREAD_STATUS_CHECK_INTERVAL / 2);
		}
	}
	logline("*** $thr_name: shutting down...");
	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	logline(get_handler_thr_stats($thr_name, $thr_created_time, $total_processed_commands, \%commands_processed));
	$dbhit->close if defined $dbhit;
	logline("*** $thr_name: shut down.");
}

sub _thr_handler_factory {
	_set_default_sig_handlers();
	my $thr_name = 'Handler Factory';
	my $thr_created_time = time();
	($mem_inuse, $thr_cnt, $ps_href) = (undef, undef, undef);
	my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, buffering delayed until thread exit
	$thread_status{$thr_name} = time();

	my $thr_created_cnt_ref = shift;
	
	my ($total_processed_commands, $total_processed_sig) = (0, 0);
	my %commands_processed;
	my $thr_process_commands = 1;
	my $next_thread_status_update = time();
	
	logline("*** $thr_name created.");

	logline('*** Loading all message handler modules...');
	foreach (HANDLER_MODULES) {
		eval "use $_";
		if ($@) {
			logline("Terminal error loading message handler module $_: $@");
			return;
		}
	}
	logline("*** All message handler modules loaded.");

	while ($thr_process_commands)
	{
		my @todo = $handler_factory_queue->dequeue_tw($next_thread_status_update);
		my ($thr_sig_href, $desired_pool_growth, @args) = @todo;
		logline("*** $thr_name: Got an event") if @todo;
		print Dumper($thr_sig_href, $desired_pool_growth, @args) if DEBUG;
		if (!@todo) {	#timedwait expired
			logline("*** $thr_name: Woke up with nothing to do...");
		}
		elsif (defined $thr_sig_href)	#thread signal sent (keys=tid, type, sig); process, then return to queue handling
		{
			$total_processed_sig++;
			### handle signal (single & broadcast) ###
			if ($thr_sig_href->{sig} eq $pool{sig}->{shut_down})
			{
				logline("*** $thr_name: shut down request received...");
				$thr_process_commands = 0;
			}
			elsif ($thr_sig_href->{sig} eq $pool{sig}->{statistics})
			{
				logline("*** $thr_name: statistics request received...");
				my $stats = get_handler_factory_stats($thr_name, $thr_created_time);
				logline($stats);
				$stats =~ s/[ ]{27}//go;
				$pool{sig}->{_semaphore}->{statistics}->down;
				my $f = myopen(">>$ReRix_global{base_path}/stats.dat");
				print $f "$stats\n";
				close $f;
				$pool{sig}->{_semaphore}->{statistics}->up;
			}
			elsif ($thr_sig_href->{sig} eq $pool{sig}->{alive})
			{
				logline("*** $thr_name".threads->tid.": alive status request received...");
				$next_thread_status_update = time();
			}
			else
			{
				logline("*** $thr_name: ignoring unknown message $thr_sig_href->{sig}...");
			}
		}
		else	{	#process commands normally
			lock(${$thr_created_cnt_ref});
			${$thr_created_cnt_ref} = 0;
			$total_processed_commands++;

			### adjust growth to insure we don't exceed max handlers ###
			($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
			my $max_growth = $pool{max} - ($thr_cnt - num_non_handler_thr());
			$desired_pool_growth = $max_growth if $desired_pool_growth > $max_growth;

			if ($desired_pool_growth) {
				logline("*** $thr_name: Growing handler pool by $desired_pool_growth...");
				for (1..$desired_pool_growth) {
					my $thr = threads->new(\&_thr_cmd_handler, @args);
					unless ($thr) {
						logline("*** $thr_name: Error creating new thread, attempting again...");
						sleep 2;	#let system settle down
						unless ($thr = threads->new(\&_thr_cmd_handler, @args)) {
							logline("*** $thr_name: Terminal error creating new thread. Exiting...");
							${$thr_created_cnt_ref} = -1;	#error
							$thr_process_commands = 0;
						}
					}
					sleep $pool{handler_growby_delay}; #let system settle
					${$thr_created_cnt_ref}++;
				}
				logline("*** $thr_name: Pool extended by $desired_pool_growth.");
			} else {
				logline("*** $thr_name: No growth allowed at this time (max $pool{max} threads already active)");
			}
			cond_signal(${$thr_created_cnt_ref});
		}
		
		### update thread status monitor ###
		if ($next_thread_status_update <= time()) {
			$thread_status{$thr_name} = time();
			$next_thread_status_update = time() + int(THREAD_STATUS_CHECK_INTERVAL / 2);
		}
	}
	logline("*** $thr_name: shutting down...");
	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	logline(get_handler_factory_stats($thr_name, $thr_created_time));
	logline("*** $thr_name: shut down.");
}

sub _thr_cmd_handler
{
	_set_default_sig_handlers();
#	$dbh->{InactiveDestroy} = 1;
	my $thr_name = 'Handler thread ('.threads->tid.')';
	my $thr_created_time = time();
	($mem_inuse, $thr_cnt, $ps_href) = (undef, undef, undef);
	my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, buffering delayed until thread exit
	my $dbhit = USAT::Database->new(config => $db_config);
	my ($total_processed_commands, $total_processed_sig) = (0, 0);
	my %commands_processed;
	my $thr_process_commands = 1;
	logline("$thr_name created.");
	while ($thr_process_commands)
	{
		my @logs;
		my ($thr_sig_href, $command, $command_counter) = $command_queue->dequeue;
		
		if (defined $thr_sig_href)	#thread signal sent (keys=tid, type, sig); process, then return to queue handling
		{
			$total_processed_sig++;
			if (!defined $thr_sig_href->{tid} || (defined $thr_sig_href->{tid} && $thr_sig_href->{tid} == threads->tid)) #signal for me (generic or specific to this thread)
			{
				### handle broadcast pre-signal processing ###
				if ($thr_sig_href->{type} eq $pool{sig}->{_type}->{broadcast})
				{
					logline("*** $thr_name: received broadcast signal; sleeping while waiting for other threads...");
					lock($pool{sig}->{_lock});
					$pool{sig}->{_lock}->{$thr_sig_href->{sig}}++;	#assumes parent has pre-populated _lock var with 0 in key $thr_sig_href->{sig}
					if ($pool{sig}->{_lock}->{$thr_sig_href->{sig}} >= ((&get_mem_stats([$prog_name]))[1] - num_non_handler_thr()))	#last thread to receive broadcast (>= just in case any handlers die before all receive broadcast)
					{
						logline("*** $thr_name: last to receive broadcast signal...sending wakeup broadcast...");
						cond_broadcast($pool{sig}->{_lock});
					}
					else
					{
#						$command_queue->enqueue($thr_sig_href, $command, $command_counter);
						#note: need to write implementation to handle cond_timedwait (i.e. separate thread that monitors these vars on a sleep(1) wait and sends signal to shared mem server if any have reached their timeout)
#						until ($pool{sig}->{_lock}->{$thr_sig_href->{sig}} == 0)	#0 indicates all processes have reached broadcast sig block state
#						{
#							last if !cond_timedwait($pool{sig}->{_lock}, time() + $pool{sig}->{_broadcast_timeout});
#						}
						cond_wait($pool{sig}->{_lock}) until $pool{sig}->{_lock}->{$thr_sig_href->{sig}} >= ((&get_mem_stats([$prog_name]))[1] - num_non_handler_thr());	#wait until all handlers received signal
					}
				}
				
				### handle signal (single & broadcast) ###
				if ($thr_sig_href->{sig} eq $pool{sig}->{shut_down})
				{
					logline("*** $thr_name: shut down request received...");
					$thr_process_commands = 0;
				}
				elsif ($thr_sig_href->{sig} eq $pool{sig}->{statistics})
				{
					logline("*** $thr_name: statistics request received...");
					my $stats = get_handler_thr_stats($thr_name, $thr_created_time, $total_processed_commands, \%commands_processed);
					logline($stats);
					$stats =~ s/[ ]{27}//go;
					$pool{sig}->{_semaphore}->{statistics}->down;
					my $f = myopen(">>$ReRix_global{base_path}/stats.dat");
					print $f "$stats\n";
					close $f;
					$pool{sig}->{_semaphore}->{statistics}->up;
				}
				else
				{
					logline("*** $thr_name: ignoring unknown message $thr_sig_href->{sig}...");
				}
			}
			else	#signal wasn't for me; re-queue; note: *not* sure blocking logic (order) will allow for this design--may have to design smarter signal system
			{
				logline("*** $thr_name: message intended for handler #$thr_sig_href->{tid}. Ignoring and re-queuing message...");
				$command_queue->enqueue($thr_sig_href, $command, $command_counter);
			}
		}
		else	{	#process commands normally
			$total_processed_commands++;
			
			### create/validate database connection ###
			unless (defined $dbhit && $dbhit->ping)
			{
				$dbhit->close if defined $dbhit;
				$dbhit = USAT::Database->new(config => $db_config);
			}
			$dbhit->do(query => "ALTER SESSION SET nls_date_format = '".DB_DEFAULT_DATE_FORMAT."'");
			
			### get the device data ###
			ReRix::Utils::get_device_data($dbhit, $command) if $command->{machine_id} ne USAT::App::ReRix::Const::NET_LAYER__MACHINE_ID;
			
			### calculate response num ###
			$command->{response_no} = defined $command->{msg_no} ? $command->{msg_no} : ReRix::Utils::get_message_number($dbhit);

			### add some properties to inbound message ###
			$command->{dbh} = $dbhit;
			$command->{queue_ob} = \@outbound_queue;

			### create device call-in record ###
			USAT::Common::CallInRecord::start($dbhit, $command, $command->{session_id}, $command->{machine_id}, $command->{SSN}, $command->{device_id}) if $command->{is_external} && $command->{session_in_msg_cnt} == 1;

			### prepare to process inbound message ###
			logline("Executing msg $command->{inbound_id} from $command->{machine_id} (session \#$command->{session_id})");
			my $thr_execution_start_ts = time();
			my $command_code = $command->{_command_code};
			my $command_subcode = $command->{_command_subcode};
			my $execute_cd = $command->{execute_cd};

			my $session_expires_ts_offset = (get_device_timeout($command->{device_type}) - $command->{session_expires_sec});
			my $session_expires_ts = $command->{session_id} > 0 && defined $inbound_queue_cache_expires{$command->{session_id}} 
				? $inbound_queue_cache_expires{$command->{session_id}} + $session_expires_ts_offset	#more accurate session expiration
				: time();
			delete $inbound_queue_cache_expires{$command->{session_id}}
				if $session_expires_ts == $session_expires_ts_offset;	#thread-safe cleanup for (non-atomic) previous action (rare but possible auto-vivification)

			my $cmd_ts_epoch = dbts_to_epoch_sec($command->{inbound_date}); #MM DD YYYY HH24 MI SS
			my $curtime = time() + $command->{db_to_sys_time_adj_sec};

			### mask specific content of sensitive messages for logging ###
			my $secure_inbound_command = $command->{inbound_command};
			
			### process inbound message ###
			my $absolute_session_expires_ts;
			if (defined ENGINE_MSG_PARSER_RULES->{$command_code} && defined ENGINE_MSG_PARSER_RULES->{$command_code}->{$command_subcode})
			{
				$secure_inbound_command = ReRix::Utils::mask_sensitive_inbound_message($command->{inbound_command}, $command_code, $command_subcode) if !DEBUG;
				
				if ($session_expires_ts >= $curtime)
				{
					$commands_processed{$command_code}->{$command_subcode}++;

					my $package = ENGINE_MSG_PARSER_RULES->{$command_code}->{$command_subcode}->[0];
					my $cmd = ENGINE_MSG_PARSER_RULES->{$command_code}->{$command_subcode}->[1];
					my $cmd_description = ENGINE_MSG_PARSER_RULES->{$command_code}->{$command_subcode}->[2];
					my $cmd_priority = ENGINE_MSG_PARSER_RULES->{$command_code}->{$command_subcode}->[3];

					### logging ###
					push(@logs, $command_counter . " - " . $cmd_description . " --------------------------");
					push(@logs, "Net Layer ID      : $command->{net_layer_id}");
					push(@logs, "Session ID        : $command->{session_id}");
					push(@logs, "Machine ID        : $command->{machine_id}");
					if(defined $command->{device_id})
					{
						push(@logs, "Device ID         : $command->{device_id}");
						push(@logs, "SSN               : $command->{SSN}");
						push(@logs, "Device Type       : " . &get_device_desc($command->{device_type}));
						push(@logs, "Base Host ID      : $command->{base_host_id}");
					}
					push(@logs, "Msg Number        : $command->{msg_no}");
					push(@logs, "Msg (".sprintf("% 5d", length($command->{inbound_command}) / 2)." bytes) : " . ($@ ? '(error masking sensitive data)' : $secure_inbound_command));

					### handle command ###
					my $cmd_execution_start_ts = time();
					logline("*** $command->{machine_id} [$command->{inbound_id}]: trying to execute command $package\::$cmd...");
					my $res;
					{
						local $@;
						eval { $res = &{eval "\\\&$package\::$cmd"}($dbhit, $command); };
						my $cmd_code_text = '('.$command_code.'h)'.($command_subcode eq '__' ? '' : '('.$command_subcode.'h)');
						if ($@)
						{
							logline("*** $command->{machine_id} [$command->{inbound_id}]: Command error $cmd_code_text ("
								.sprintf('%.3f', time() - $cmd_execution_start_ts)." seconds): unhandled exception occured");
							logline("*** $command->{machine_id} [$command->{inbound_id}]: Unhandled exception: $@");
							$execute_cd = MCI_QUEUE_EXECUTE_CD__COMPLETE_ERROR;
						}
						else
						{
							logline("*** $command->{machine_id} [$command->{inbound_id}]: Command complete $cmd_code_text ("
								.sprintf('%.3f', time() - $cmd_execution_start_ts)." seconds)");
							$execute_cd = MCI_QUEUE_EXECUTE_CD__COMPLETE;
							$absolute_session_expires_ts = time() if $command->{session_expires_sec} < 0;	#force session to expire immediately
						}
					}
					logit($command->{inbound_id}, $command->{machine_id}, \@logs);
					logit($command->{inbound_id}, $command->{machine_id}, $res) if scalar @{$res};

					### unload module ###
#					delete_package($package);
				}
				else
				{
					logit($command->{inbound_id}, $command->{machine_id}, "****************** Command Too Old: $command_code"
						.($command_subcode ne '__' ? $command_subcode : '').': Device session expired '.sprintf('%.3f', $curtime - $session_expires_ts).' second(s) ago: '
						.sprintf('%.3f', $curtime - $cmd_ts_epoch).' sec elapsed since command receipt');
					#ReRix::Utils::send_alert("****************** Command Too Old: $command_code"
					#	.($command_subcode ne '__' ? $command_subcode : '').': Device session expired '.sprintf('%.3f', $curtime - $session_expires_ts).' second(s) ago: '
					#	.sprintf('%.3f', $curtime - $cmd_ts_epoch).' sec elapsed since command receipt');
					$execute_cd = MCI_QUEUE_EXECUTE_CD__COMPLETE_ERROR;
				}
			}
			else
			{
				logit($command->{inbound_id}, $command->{machine_id}, "****************** Command Not Found: $command_code".($command_subcode ne '__'?$command_subcode:''));
				#ReRix::Utils::send_alert("ReRixEngine: ****************** Command Not Found: $command_code".($command_subcode ne '__'?$command_subcode:''));
				$execute_cd = MCI_QUEUE_EXECUTE_CD__COMPLETE_ERROR;
			}
			
			### update database info for this command ###
			push @{$inbound_queue_done{$execute_cd}}, Storable::freeze([$command->{inbound_id}, $command->{session_id}, $absolute_session_expires_ts]);	#safe atomic action without locks

			### send pending messages depending on what kind of device this is ###
			if (defined $command->{device_type} && $command->{device_type} eq PKG_DEVICE_TYPE__ESUDS && $command->{is_external})
			{
				### load modules ###
				my $res = ReRix::Update::send_pending($dbhit, $command, 999);
				logit($command->{inbound_id}, $command->{machine_id}, $res);
			}
			
			### update mem stats ###
			if ($total_processed_commands % 50 == 0 || $total_processed_commands == 1)
			{
				($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
				if ($ps_href->{$$}->{mem} > $ReRixEngine{max_mem_use_thr}) {
					logline("*** $thr_name: Max memory exceeded ($ps_href->{$$}->{mem} current; $ReRixEngine{max_mem_use_thr} allowed): thread shutting down immediately");
					$thr_process_commands = 0;
				}
			}
			
			### update last activity timestamp ###
			if (defined $command->{device_id} && $command->{is_external}) {
				$dbhit->do(
					query => q{
						UPDATE device.device
						SET last_activity_ts = SYSDATE
						WHERE device_id = ?
					}
					,values => [
						$command->{device_id}
					]
				);
			}
			
			### log command to app database if using separate queue database ###
			unless ($is_queue_db_main_db) {
				#Note: insert assumes that queue and main databases are time-synchronized
				my $mci_command_id = $dbhit->sequence_next(sequence => 'engine.seq_machine_cmd_inbound_id');
				$dbhit->do(
					query => q{
						INSERT INTO engine.machine_cmd_inbound_hist (
							inbound_id, machine_id, inbound_date, inbound_command,
							timestamp, execute_date, execute_cd, inbound_msg_no,
							num_times_executed, logic_engine_id, session_id, net_layer_id
						) VALUES (
							?, ?, TO_DATE(?, ?), ?,
							?, SYSDATE, ?, ?, 
							?, ?, ?, ?
						)
					},
					values => [
						$command->{inbound_id},
						$command->{machine_id},
						$command->{inbound_date},
						'MM/DD/YYYY HH24:MI:SS',
						$secure_inbound_command,
						undef,						#currently unused
						$execute_cd,
						$command->{msg_no},
						1,							#app only executes once
						$command->{logic_engine_id},
						$command->{session_id},
						$command->{net_layer_id}
					]
				) unless substr($secure_inbound_command, 0, 2) eq USAT::App::ReRix::Const::NET_LAYER__INTERNAL_REQUEST;
			}
			
			### clean up before next message ###
#			$dbhit->clear_statement_handles();	#is this necessary??
			logline("Execution complete for msg $command->{inbound_id} from $command->{machine_id} (session \#$command->{session_id}) ("
				.sprintf('%.3f', time() - $thr_execution_start_ts)." seconds)");
		}
	}
	logline("*** $thr_name: shutting down...");
	($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	logline(get_handler_thr_stats($thr_name, $thr_created_time, $total_processed_commands, \%commands_processed));
	$dbhit->close if defined $dbhit;
	logline("*** $thr_name: shut down.");
}

########################################################################
# signal handler subroutines
########################################################################

sub _set_main_thr_sig_handlers () {	#set main thread signal handlers
	$SIG{INT} = \&SIGINT;
	$SIG{HUP} = \&SIGHUP;
	$SIG{USR1} = \&SIGUSR1;
	$SIG{USR2} = \&pausesub;
	Sys::SigAction::set_sig_handler('ALRM', \&SIGALRM, { mask => ['ALRM'] });
}

sub _set_default_sig_handlers () {	#set handlers to default
	$SIG{INT}
		= $SIG{HUP}
		= $SIG{USR1}
		= $SIG{USR2}
		= $SIG{ALRM}
		= 'DEFAULT';
}

sub pausesub {
	my $sleeptime = THREAD_STATUS_CHECK_INTERVAL * THREAD_STATUS_MAX_FAILURES_DEFAULT - 5;
	warn "Execution paused for $sleeptime seconds...\n";
	sleep $sleeptime;
	warn "...execution resumed.\n";
}

sub SIGINT
{ 
	$SIG{INT} = \&SIGINT;
	$sig_int++;
	logline("ReRixEngine: Caught SIGINT #$sig_int");
	if ($sig_int == 1)
	{
		logline("ReRixEngine: attempting to shut down safely...");
		$process_commands = 0;
	}
	else
	{
		logline("ReRixEngine: attempting to shut down immediately...");
		exit 2;
	}
}

sub SIGHUP
{
	$SIG{HUP} = \&SIGHUP;
	$sig_hup++;
	logline("ReRixEngine: Caught SIGHUP #$sig_hup");
	if ($sig_hup == 1)
	{
		logline("ReRixEngine: attempting to restart safely...");
		$process_commands = 0;
	}
	else
	{
		logline("ReRixEngine: attempting to restart immediately...");
		exit 1;
	}
}

sub SIGUSR1
{
	$SIG{USR1} = \&SIGUSR1;
	logline("ReRixEngine: Caught SIGUSR1");
	logline("ReRixEngine: Requesting performance stats from handlers...");
	$last_stat_report_ts = time() - $ReRixEngine{stat_report_interval};	#expire timer
}

sub SIGALRM
{
	$SIG{ALRM} = \&SIGALRM;
	logline('ReRixEngine: SIGALRM occured: main thread appears to not have responded in last '.THREAD_STATUS_CHECK_INTERVAL.' seconds');
	logline('ReRixEngine: attempting to restart immediately...');
	exit 1;
}

########################################################################
# general subroutines
########################################################################

sub _create_distributed_engine ($)
{
	my $dbh = shift;
	my $engine_id = MCI_QUEUE__DEFAULT_ENGINE_ID;
	
	### get new serial num from database ###
	my $logic_engine_id_nextval = $dbh->sequence_next(sequence => 'engine.seq_logic_engine_id');
	return $engine_id unless defined $logic_engine_id_nextval;
	$engine_id = $logic_engine_id_nextval;

	my $c_insert_new_engine_id = $dbh->do(
		query	=> q{
			INSERT INTO engine.logic_engine (
				logic_engine_id,
				logic_engine_desc,
				logic_engine_active_yn_flag
			) VALUES (
				?, ?, ?
			)
		}
		,values => [
			$engine_id
			,$ReRix_global{hostname}
			,'N'
		]
	);
	$engine_id = MCI_QUEUE__DEFAULT_ENGINE_ID unless $c_insert_new_engine_id;
	
	if ($engine_id != MCI_QUEUE__DEFAULT_ENGINE_ID)
	{
		### store serial num locally ###
		my $f;
		if ($f = myopen(">$ReRixEngine{dist_engine_sn_path}") or ($ReRixEngine{dist_engine_allow_reg_failure}
			? MCI_QUEUE__DEFAULT_ENGINE_ID : die "Unable to locally store rerix engine id $engine_id: $!"))
		{
			print $f "#Note: this file is auto-generated. Do not modify.\n";
			print $f "$engine_id\n";
		}
	}
	else
	{
		my $warning = "Unable to register unique rerix engine id. "
			.($ReRixEngine{dist_engine_allow_reg_failure}
				? 'Using default message bin engine id '.MCI_QUEUE__DEFAULT_ENGINE_ID.'.'
				: 'Cannot continue...exiting now!');
		warn $warning;
		exit 0 unless $ReRixEngine{dist_engine_allow_reg_failure};
		$engine_id = MCI_QUEUE__DEFAULT_ENGINE_ID;
	}

	return $engine_id;
}

sub activate_distributed_engine ($;$)
{
	my $dbh = shift;
	my $dbh_queue = shift;
	my $engine_id;
	
	my $success = 0;
	if (-f $ReRixEngine{dist_engine_sn_path})
	{
		### load settings ###
		my @file_data;
		{
			my $f = myopen("<$ReRixEngine{dist_engine_sn_path}");
			@file_data = <$f>;
		}
		foreach (grep(!/^(#|\s)/, @file_data))
		{
			chomp $_;
			if ($_ =~ m/^\s*(\d+)\s*$/)
			{
				$engine_id = $1;
				last;
			}
		}
		
		### verify that serial num is in database ###
		my $c_validate_engine_id = $dbh->query(
			query	=> q{
				SELECT COUNT(*)
				FROM engine.logic_engine
				WHERE logic_engine_id = ?
			}
			,values => [
				$engine_id
			]
		);
		$engine_id = _create_distributed_engine($dbh) unless $c_validate_engine_id->[0]->[0];
	}
	else
	{
		$engine_id = _create_distributed_engine($dbh);
		$success = 1;
	}

	### update active flag ###
	my $c_update_status = $dbh->do(
		query	=> q{
			UPDATE engine.logic_engine
			SET logic_engine_active_yn_flag = ?
			WHERE logic_engine_id = ?
		}
		,values => [
			$is_queue_db_main_db ? 'Y' : 'R',	#kludge: central not updating to 'Y' for now if using separate databases, to co-exist with old ReRixEngine architecture instances
			$engine_id
		]
	);
	$success = $c_update_status ? 1 : 0;
	
	unless ($success && ($is_queue_db_main_db || _synchronize_queue_db_activate_distributed_engine($engine_id, $dbh_queue))) {
		my $warning = "Unable to set rerix engine \#$engine_id active. "
			.($ReRixEngine{dist_engine_allow_reg_failure}
				? 'Using default message bin engine id '.MCI_QUEUE__DEFAULT_ENGINE_ID.'.'
				: 'Cannot continue...exiting now!');
		warn $warning;
		exit 0 unless $ReRixEngine{dist_engine_allow_reg_failure};
	}
	
	return $engine_id;
}

sub _synchronize_queue_db_activate_distributed_engine ($$)
{
	my $engine_id = shift;
	my $dbh_queue = shift;

	### synchronize queue database (special case) ###
	my $c_validate_engine_id = $dbh_queue->query(
		query	=> q{
			SELECT COUNT(*)
			FROM engine.logic_engine
			WHERE logic_engine_id = ?
		}
		,values => [
			$engine_id
		]
	);
	my $c_insert_new_engine_id = $dbh_queue->do(
		query	=> q{
			INSERT INTO engine.logic_engine (
				logic_engine_id,
				logic_engine_desc,
				logic_engine_active_yn_flag
			) VALUES (
				?, ?, ?
			)
		}
		,values => [
			$engine_id
			,$ReRix_global{hostname}
			,'N'
		]
	) unless $c_validate_engine_id->[0]->[0];

	my $c_update_status = $dbh_queue->do(
		query	=> q{
			UPDATE engine.logic_engine
			SET logic_engine_active_yn_flag = ?
			WHERE logic_engine_id = ?
		}
		,values => [
			'Y',
			$engine_id
		]
	);
	return $c_update_status;
}

sub deactivate_distributed_engine ($$;$)
{
	my $engine_id = shift;
	my $dbh = shift;
	my $dbh_queue = shift;
	return undef unless defined $engine_id;
	my $c_update_status = $dbh->do(
		query	=> q{
			UPDATE engine.logic_engine
			SET logic_engine_active_yn_flag = ?
			WHERE logic_engine_id = ?
		}
		,values => [
			'N',
			$engine_id
		]
	);
	$c_update_status = _synchronize_queue_db_deactivate_distributed_engine($engine_id, $dbh_queue)
		unless $is_queue_db_main_db;
	$engine_id = undef;
	return $c_update_status;
}

sub _synchronize_queue_db_deactivate_distributed_engine ($$)
{
	my $engine_id = shift;
	my $dbh = shift;
	return undef unless defined $engine_id;
	my $c_update_status = $dbh->do(
		query	=> q{
			UPDATE engine.logic_engine
			SET logic_engine_active_yn_flag = ?
			WHERE logic_engine_id = ?
		}
		,values => [
			'N',
			$engine_id
		]
	);
	return $c_update_status;
}

sub logit ($$$) {
	my ($num, $machine, $msg) = @_;
	my @msg_list = (ref $msg eq 'ARRAY' && @{$msg} > 0) ? @{$msg} : $msg;
	print get_date_stamp() . ' ['
		. sprintf('%5d:%4d', ($$, threads->tid)) . "] $machine [$num] $_\n"
		foreach @msg_list;
}

sub logline ($) {
	my $msg = shift;
	my @msg_list = (ref $msg eq 'ARRAY' && @{$msg} > 0) ? @{$msg} : $msg;
	print get_date_stamp() . ' ['
		. sprintf('%5d:%4d', ($$, threads->tid)) . "] $_\n" foreach @msg_list;
}

sub get_date_stamp () {
	my @epoch_sec = gettimeofday();
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst)
		= localtime($epoch_sec[0]);
	return sprintf(
		'%04d-%02d-%02d %02d:%02d:%02d.', ($year + 1900), ($mon + 1), $mday,
		$hour, $min, $sec
	) . substr(sprintf('%06d', $epoch_sec[1]), 0, 3);
}

sub get_device_desc ($)
{
	my ($device_type_code) = @_;
	my $device_type_code_regex = quotemeta $device_type_code;
	
	if (!defined $device_type_code)	{ return "Unknown (Legacy)"; }
	elsif (!grep(/^$device_type_code_regex$/, keys %{+DEVICE_TYPE_PROPERTIES}))	{ return "Unknown (Other)"; }
	else { return DEVICE_TYPE_PROPERTIES->{$device_type_code}->{name}; }
}

sub get_device_timeout ($)
{
	my ($device_type_code) = @_;
	my $device_type_code_regex = defined $device_type_code ? quotemeta $device_type_code : '';
	
	if (!defined $device_type_code)	{ return $max_cmd_timeout; }	#Unknown (Legacy)
	elsif (!grep(/^$device_type_code_regex$/, keys %{+DEVICE_TYPE_PROPERTIES}))	{ return $max_cmd_timeout; }	#Unknown (Other)
	else
	{ 
		return DEVICE_TYPE_PROPERTIES->{$device_type_code}->{timeout} if DEVICE_TYPE_PROPERTIES->{$device_type_code}->{timeout};
		return 60*60*24*365;	#if timeout 0, return one year (implies essentially infinite timeout)
	}
}

sub dbts_to_epoch_sec ($)
{
	my $timestamp = shift;
	return undef unless defined $timestamp;
	my @ts = split(/\W/, $timestamp);	#MM DD YYYY HH24 MI SS
	my $ts_epoch = eval { timelocal($ts[5], $ts[4], $ts[3], $ts[1], ($ts[0] - 1), $ts[2]); };
	return $ts_epoch;
}

sub get_date_db ($$)
{
	my ($dbh) = @_;
	my $aref;
	if (eval {$dbh->{driver}} eq DB_POSTGRESQL)
	{
		$aref = $dbh->query(
			query => q{SELECT TO_CHAR(NOW(), '}.DB_DEFAULT_DATE_FORMAT.q{')}
		);
	}
	else
	{
		$aref = $dbh->query(
			query => q{SELECT TO_CHAR(sysdate, '}.DB_DEFAULT_DATE_FORMAT.q{') FROM dual}
		);
	}
	return $aref->[0]->[0];
}

sub get_handler_thr_stats ($$$$)
{
	my ($thread_name, $thr_created_time, $total_processed_commands, $commands_processed_href) = @_;
	my ($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	my $alive_timedate = localtime($thr_created_time);
	my $alive_time = sprintf("%.2f", (time() - $thr_created_time) / 86400);	#days
	if (threads->tid > 0)
	{
		my @cmd_proc;
		foreach my $cmd (keys %{$commands_processed_href})
		{
			push @cmd_proc, "\t"
				.(defined ENGINE_MSG_PARSER_RULES->{$cmd} && defined ENGINE_MSG_PARSER_RULES->{$cmd}->{$_}
					? ENGINE_MSG_PARSER_RULES->{$cmd}->{$_}->[2]
					: "$cmd|$_")
				.'='.$commands_processed_href->{$cmd}->{$_} foreach (sort keys %{$commands_processed_href->{$cmd}});
		}
		return "*** $thread_name #".threads->tid.": alive since $alive_timedate ($alive_time days)"."\n".(" " x 50).join("\n".(" " x 50),
			"mem use=".$ps_href->{$$}->{mem}, "commands processed ($total_processed_commands)", @cmd_proc);
	}
	else
	{
		my $timedate = defined $pool{peak_load}->{ts} ? localtime($pool{peak_load}->{ts}) : '?';
		my $timedate_max = defined $pool{peak_load}->{ts_max} ? localtime($pool{peak_load}->{ts_max}) : '?';
		return "*** Main thread: alive since $alive_timedate ($alive_time days)"."\n".(" " x 50).join("\n".(" " x 50), 
			"mem use=".$ps_href->{$$}->{mem},
			"total app mem=".$mem_inuse,
			"total handlers alive=".($thr_cnt - num_non_handler_thr()),
			"total commands processed=".$total_processed_commands,
			"internal queue cmds pending=".$command_queue->pending,
			"internal queue peak in last ".($pool{handler_timeout} / 60)."min=".$pool{peak_load}->{queue_total}." @ $timedate",
			"internal queue max peak=".$pool{peak_load}->{queue_max}." @ $timedate_max");
	}
}

sub get_handler_factory_stats ($$)
{
	my ($thread_name, $thr_created_time) = @_;
	my ($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	my $alive_timedate = localtime($thr_created_time);
	my $alive_time = sprintf("%.2f", (time() - $thr_created_time) / 86400);	#days
	return "*** $thread_name thread: alive since $alive_timedate ($alive_time days)"."\n".(" " x 50)
		.join("\n".(" " x 50), "mem use=".$ps_href->{$$}->{mem}, "total app mem=".$mem_inuse);
}

sub get_thread_monitor_stats ($$)
{
	my ($thread_name, $thr_created_time) = @_;
	my ($mem_inuse, $thr_cnt, $ps_href) = &get_mem_stats([$prog_name]);
	my $alive_timedate = localtime($thr_created_time);
	my $alive_time = sprintf("%.2f", (time() - $thr_created_time) / 86400);	#days
	return "*** $thread_name thread: alive since $alive_timedate ($alive_time days)"."\n".(" " x 50)
		.join("\n".(" " x 50), "mem use=".$ps_href->{$$}->{mem}, "total app mem=".$mem_inuse);
}

sub myopen (@) {
	open my $fh, "@_" or die "Can't open '@_': $!";
	return $fh;
}

sub get_mem_stats (;$) {	#TODO: use $obj->proc_resident (physical mem) instead?
	my $ps_list_aref = $_[0];	#list of process names to report stats on
	my $ps_list_regex;
	if (defined $ps_list_aref && scalar @{$ps_list_aref} > 0)
	{
		my @ps_list_regex;
		foreach my $ps (@{$ps_list_aref})
		{
			push @ps_list_regex, quotemeta $ps;
		}
		$ps_list_regex = join('|', @ps_list_regex);
	}

	my ($ps_cnt, $inuse) = (0, 0);
	my (%mem_hsh, %ps_hsh);
	my $p = get_process_stats;
	if (!$p) {
		warn "process stat error: ".get_error();
		return (-1, -1, \%ps_hsh); #, $free_swap, $free_phys);
	}
	foreach my $obj ($p->all_procs)
	{
		my ($pid, $ppid, $mem, $name)
			= ($obj->pid, $obj->parent_pid, int($obj->proc_size / 1000), $obj->proc_title);
		if ( (!defined $ps_list_regex) || ($name =~ m/($ps_list_regex)/) )
		{
			unless ($name eq '')	#check for defunct processes
			{
				$mem_hsh{$name} = [0, 0] unless (defined $mem_hsh{$name});
				$mem_hsh{$name}->[0]++;	#num of process instances
				$mem_hsh{$name}->[1] += $mem;
			}
			$ps_hsh{$pid} = {
				'ppid'	=> $ppid
				,'mem'	=> $mem
				,'name'	=> $name
			};
		}
	}
	$ps_cnt += $mem_hsh{$_}->[0] foreach (keys %mem_hsh);
	$inuse += $mem_hsh{$_}->[1] foreach (keys %mem_hsh);

	return ($inuse, $ps_cnt, \%ps_hsh); #, $free_swap, $free_phys);
}

########################################################################
main();

########################################################################
package My::ReRix::Session::Message;
use strict;
use warnings;

use USAT::App::ReRix::Const qw(ENGINE_MSG_PARSER_RULES);

use fields qw(
	inbound_id
	machine_id
	inbound_date
	timestamp
	inbound_command
	execute_date
	execute_cd
	msg_no
	num_times_executed
	session_id
	net_layer_id
	logic_engine_id

	db_to_sys_time_adj_sec
	session_expires_sec

	_command_code
	_command_subcode

	device_id
	SSN
	device_type
	base_host_id
	base_host_type_id
	response_no
	encryption_key
	session_in_msg_cnt
	is_external

	dbh
	queue_dbh
	queue_ob
);

sub new {
	my __PACKAGE__ $class = shift;
	Carp::croak "Input not a well formed hash"
		unless @_ % 2 == 0;
	my $self = fields::new($class);
	
	### load data ###
	my %data = @_;
	$self->{$_} = $data{$_}
		foreach keys %data;
	
	### set some defaults ###
	$self->{_command_code} = uc(substr($self->{inbound_command},0,2));
	$self->{_command_subcode} = (defined ENGINE_MSG_PARSER_RULES->{$self->{_command_code}} && !defined ENGINE_MSG_PARSER_RULES->{$self->{_command_code}}->{__})
		? uc(substr($self->{inbound_command},2,2)) : '__';
	return $self;
}

sub CLONE {}
sub DESTROY {}

########################################################################
package My::ReRix::Session;
use strict;
use warnings;
use Carp ();
use fields qw(
	abs_expires_ts
	last_enqueue_ts
	last_msg_processed_ts
	expires_sec	
	ready
	queue
	queue_history_inbound_id
	in_msg_cnt
);

sub new {
	my __PACKAGE__ $class = shift;
	my $self = fields::new($class);
	$self->{abs_expires_ts} = undef;	#undef or <0 => no absolute expiration; if >= 0, superceeds expires_sec 
	$self->{last_enqueue_ts} = 0;
	$self->{last_msg_processed_ts} = 0;
	$self->{expires_sec} = undef;	#undef or <0 => none; applies to last_enqueue_ts and last_msg_processed_ts
	$self->{ready} = 1;
	$self->{queue} = [];
	$self->{queue_history_inbound_id} = [];
	$self->{in_msg_cnt} = 0;
	return $self;
}

sub CLONE {}
sub DESTROY {}

sub pending {
	my __PACKAGE__ $self = shift;
	return scalar(@{$self->{queue}});
}

sub enqueue {	#can only enqueue new My::ReRix::Session::Message objects
	my __PACKAGE__ $self = shift;
	foreach my $obj (@_) {
		unless (eval {$obj->isa('My::ReRix::Session::Message')}) {
			Carp::cluck "Enqueue element ignored: '$obj' is not a valid My::ReRix::Session::Message object";
			next;
		}
		my $inbound_id = $obj->{inbound_id};
		if (grep(/^$inbound_id$/, @{$self->{queue_history_inbound_id}})) {
			Carp::cluck "Enqueue element ignored: Duplicate inbound_id '$inbound_id' detected";
			next;
		}
		push @{$self->{queue}}, $obj;
		push @{$self->{queue_history_inbound_id}}, $inbound_id;
		$self->{last_enqueue_ts} = time();
	}
}

sub dequeue {
	my __PACKAGE__ $self = shift;
	return @{$self->{queue}} ? shift(@{$self->{queue}}) : wantarray ? () : undef;
}

sub dequeue_all {
	my __PACKAGE__ $self = shift;
	my @vals = @{$self->{queue}};
	@{$self->queue} = ();
	return @vals;
}

sub expires {	#undef => never expires
	my __PACKAGE__ $self = shift;
	$self->{abs_expires_ts} = shift(@_) if @_;
	if (defined $self->{abs_expires_ts} && $self->{abs_expires_ts} >= 0) {
		return $self->{abs_expires_ts};
	} elsif (defined($self->{expires_sec}) && $self->{expires_sec} >= 0) {
		my $activity_ts = $self->{last_msg_processed_ts} > $self->{last_enqueue_ts} ? $self->{last_msg_processed_ts} : $self->{last_enqueue_ts};
		return $activity_ts + $self->{expires_sec};
	} else {
		return undef;
	}
}

sub is_expired {
	my __PACKAGE__ $self = shift;
	my $expires_ts = $self->expires();
	return defined($expires_ts) && $expires_ts < time() ? 1 : 0;
}

sub set_last_msg_processed_ts {
	my __PACKAGE__ $self = shift;
	$self->{last_msg_processed_ts} = shift || time();
}

package My::ReRix::TestCmd;
use strict;
use warnings;

sub simple_test {	#method should expect an arbitrary 4-byte integer as input, and echos it back
	my ($DATABASE, $command_hashref) = @_;

	my $message = pack("H*",$command_hashref->{inbound_command});
	my $response_no = $command_hashref->{response_no};
	
	my @logs;
	my $event_id = unpack("N", substr($message, 1, 5));
	push @logs, "Event ID          : " . $event_id;

	my $response = pack("CCN", $response_no, 0xFF, $event_id);	#FF is response
	push @logs, "Response          : " . unpack("H*", $response);
	ReRix::Utils::enqueue_outbound_command($command_hashref, unpack("H*", $response));
	
	return \@logs;
}

__END__

=pod

=head1 NAME

ReRix - Message processing router for ePort and eSuds protocols

=head1 SYNOPSIS

ReRixEngine.pl  [--help|--version] [-n]

=head1 DESCRIPTION

Message processing router for ePort and eSuds protocols.

=head1 OPTIONS

=over 8

=item B<-f, --forcestart>

Terminate running application instances, if any exist.

=item B<-n, --noqueuepurge>

Don't purge the (persistent) pending message queue on application start.

=item B<--help>

Display this help and exit.

=item B<--version>

Output version information and exit.

=back

=head1 COPYRIGHT

Copyright (c) 2003-2007 USA Technologies, Inc. All rights reserved.

=cut
