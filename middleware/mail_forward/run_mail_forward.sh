RUN_DIR=/opt/mail_forward
LOG_DIR=/opt/mail_forward
ERROR_LOG=$LOG_DIR/error.log
TS=`date`

running_count=`ps -ef | egrep "mail_forward\.pl" | wc -l`
if [ $running_count -gt 0 ]; then
	echo "[$TS] Already Running" >> $ERROR_LOG
else
	cd $RUN_DIR
	/usr/bin/perl $RUN_DIR/mail_forward.pl -debug 2>&1 | /usr/local/bin/multilog s104857600 n9 /opt/mail_forward
fi

