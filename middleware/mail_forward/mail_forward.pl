#!/usr/bin/perl
##################################################################
#
# mail_forward.pl
# Version 1.2
#
##################################################################
#
# (c)2004 USA Technologies, Inc.
#
# Modified 04/15/2004 - pcowan
# Modified 04/29/2005 - pcowan - Improving performance and checks
#
#################################################################

use strict;

use Net::SMTP;
use DBI;
use USAT::Database;
use DatabaseConfig;

my $db_config = DatabaseConfig->new(debug => 1, debug_die => 0, print_query => 0);
my $DATABASE = USAT::Database->new(config => $db_config);
my $dbh = $DATABASE->{handle};
if(not $dbh)
{
        print localtime() . "\tFailed to connect to the database!\n";
        exit();
}

my $debug = $ARGV[0];
my $start_time = time();

# the smtp server name or IP
my $smtp_server = 'mailhost';

# the max amount of time in which to consider 2 identical message as duplicates
my $dupe_filter_time = 5;

# the max number of times to try to send an email before giving up
my $max_send_attempts = 5;

LogIt("mail_forward running ----------------------------------------------------");

# list of email to BCC on each message, 1 email address per line
open(CCLIST,"/opt/mail_forward/cc_list");
my @cc_list = <CCLIST>;
close(CCLIST);

# Do a query to get every email that is has been scheduled to send

my $get_emails_stmt = $dbh->prepare("select ob_email_queue_id, ob_email_from_email_addr, ob_email_from_name, ob_email_to_email_addr, ob_email_to_name, ob_email_subject, ob_email_msg, to_char(ob_email_sent_success_ts, 'MM/DD/YYYY HH24:MI:SS') from ob_email_queue where ob_email_scheduled_send_ts > (sysdate-(:dupe_filter_time/1440)) and ob_email_tot_send_attemp_num < :max_send_attempts and ob_email_sent_success_ts is null order by ob_email_sent_success_ts");
$get_emails_stmt->bind_param(":dupe_filter_time", $dupe_filter_time);
$get_emails_stmt->bind_param(":max_send_attempts", $max_send_attempts);
$get_emails_stmt->execute();
if (defined $dbh->errstr)
{
	die "Failed to execute get_emails_stmt! ($dbh->errstr)\n";
}

# declare some counters
my $check_count 			= 0; 
my $previously_sent_count 	= 0; 
my $send_success_count 		= 0;
my $send_failed_count 		= 0;
my $duplicate_count 		= 0;

# initialize a hash that will hold each distinct email;  we will use this for filtering duplicates
my %email_filter;

# iterate through the query results
while (my ($ob_email_queue_id, $ob_email_from_email_addr, $ob_email_from_name, $ob_email_to_email_addr, $ob_email_to_name, $ob_email_subject, $ob_email_msg, $ob_email_sent_success_ts) = $get_emails_stmt->fetchrow_array()) 
{
	$check_count++;
	
	# the key to the hash is every piece of actual message info... thus if any user provided info 
	# differs at all, we will consider that a different message and won't duplicate filter it.
	my $hash_key = ($ob_email_from_email_addr .','. $ob_email_from_name .','. $ob_email_to_email_addr .','. $ob_email_to_name .','. $ob_email_subject .','. $ob_email_msg);
	
	LogIt("Processing ob_email_queue_id $ob_email_queue_id: $ob_email_to_email_addr, $ob_email_subject");
	
	if($ob_email_sent_success_ts)
	{
		# this email has already been sent, just put it in the hash
		$email_filter{$hash_key} = $ob_email_queue_id;
		LogIt("Email $ob_email_queue_id was sent successfully at $ob_email_sent_success_ts");
		$previously_sent_count++;
	}
	else
	{
		# this email has not been sent yet, check if it's a duplicate
		my $previously_sent_email_id = $email_filter{$hash_key};
		if($previously_sent_email_id)
		{
			$duplicate_count++;
			
			# this is a duplidate!  do not send this message, delete it from the table
			LogIt("Duplicate Detected! Email $ob_email_queue_id is a duplicate of $previously_sent_email_id");
			
			my $delete_dupe_stmt = $dbh->prepare("delete from ob_email_queue where ob_email_queue_id = :duplicate_id");
			$delete_dupe_stmt->bind_param(":duplicate_id", $ob_email_queue_id);
			$delete_dupe_stmt->execute();
			if (defined $dbh->errstr) 
			{
				$delete_dupe_stmt->finish();
				die "Failed to execute delete_dupe_stmt! ($dbh->errstr)\n";
			}
			$delete_dupe_stmt->finish();
		}
		else
		{
			#LogIt("Attempting to send new email $ob_email_queue_id");
			
			# this is NOT a duplicate, send it and mark it sent and put it in the hash

			# put this new email in the hash
			$email_filter{$hash_key} = $ob_email_queue_id;
			
			# contruct a nice from and to line using <name>
			if(length($ob_email_from_name) == 0)
			{
				$ob_email_from_name = $ob_email_from_email_addr;
			}
			
			if(length($ob_email_to_name) == 0)
			{
				$ob_email_to_name = $ob_email_to_email_addr;
			}

			my $exit_status = send_email($ob_email_from_name, $ob_email_from_email_addr, $ob_email_to_name, $ob_email_to_email_addr, $ob_email_subject, $ob_email_msg);

			if ($exit_status) 
			{
				$send_success_count++;
				
				# update the record in the database, mark it sent successfully
				LogIt("Email $ob_email_queue_id sent successfully to $ob_email_to_email_addr");
				
				my $update_success_stmt = $dbh->prepare("update ob_email_queue set ob_email_last_send_attempt_ts = sysdate, ob_email_sent_success_ts = sysdate, ob_email_tot_send_attemp_num = (ob_email_tot_send_attemp_num+1) where ob_email_queue_id = :sent_email_id");
				$update_success_stmt->bind_param(":sent_email_id", $ob_email_queue_id);
				$update_success_stmt->execute();
	
				if (defined $dbh->errstr) 
				{
					# this is a very bad case because it means we sent the message but failed to update the 
					# database... this can cause us to bomb people with message if the process contunues
					# to run with this condition
					$update_success_stmt->finish();
					die "Failed to execute update_success_stmt! ($dbh->errstr)\n";
				}
				
				$update_success_stmt->finish();
			}
			else
			{
				$send_failed_count++;
				
				# update the record in the database, don't mark it sent but increase the attempt counter
				# it should get retried again a few times later
				
				LogIt("Failed to send email $ob_email_queue_id to $ob_email_to_email_addr!");
				
				my $update_failed_stmt = $dbh->prepare("update ob_email_queue set ob_email_last_send_attempt_ts = sysdate, ob_email_tot_send_attemp_num = (ob_email_tot_send_attemp_num+1) where ob_email_queue_id = :failed_email_id");
				$update_failed_stmt->bind_param(":failed_email_id", $ob_email_queue_id);
				$update_failed_stmt->execute();
	
				if (defined $dbh->errstr) 
				{
					$update_failed_stmt->finish();
					die "Failed to execute update_failed_stmt! ($dbh->errstr)\n";
				}
				
				$update_failed_stmt->finish();
			}
		}
	}
}

# cleanup the datbase
$get_emails_stmt->finish();
$dbh->disconnect;

LogIt("mail_forward finished in " . (time()-$start_time) . " seconds --------------------------------------");
LogIt("Checked Emails       : $check_count");
LogIt("Previously Sent      : $previously_sent_count");
LogIt("Send Success         : $send_success_count");
LogIt("Send Failed          : $send_failed_count");
LogIt("Duplicates Deleted   : $duplicate_count");
LogIt("-------------------------------------------------------------------------");

sub LogIt
{
	if($debug)
	{
		my $line = shift;
		print localtime() . " $line\n";
	}
}

sub send_email
{
	my ($from_name, $from_addr, $to_name, $to_addr, $subject, $msg) = @_;
	
	my $smtp = Net::SMTP->new($smtp_server, Timeout => 10);
	#my $smtp = Net::SMTP->new($smtp_server, Timeout => 10, Debug => 1);
	if(not defined $smtp)
	{
		return 0;
	}

	$smtp->mail($from_addr);
	$smtp->to($to_addr);

	foreach my $cc_addr (@cc_list)
	{
		$smtp->to($cc_addr);
	}
	
	if(length($from_name) == 0)
	{
		$from_name = $from_addr;
	}
	
	if(length($to_name) == 0)
	{
		$to_name = $to_addr;
	}
	
	$smtp->data(); 
	$smtp->datasend("To: \"$to_name\" <$to_addr>\n");
	$smtp->datasend("From: \"$from_name\" <$from_addr>\n");
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$msg\n\n"); 
	$smtp->dataend(); 
	$smtp->quit();
	
	return 1;
}

