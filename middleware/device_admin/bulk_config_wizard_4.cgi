#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Common::Const;

use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::Profile::PaymentType;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

if(!$PARAM{change_configuration}) {
   if($PARAM{device_type} eq PKG_DEVICE_TYPE__EDGE) {
      print '<form method="post" action="bulk_config_wizard_6_edge.cgi">';
   } else {
      print '<form method="post" action="bulk_config_wizard_6.cgi">';
   }

   HIDDEN_PASS('include_device_ids', 'customer_id', 'location_id', 'parent_location_id','start_serial_head','device_type');
   HIDDEN('edit_mode', 'A');
   HIDDEN('change_configuration',$PARAM{change_configuration});
   BR(4);
   B('Note that you are not changing device cofiguration data for selected devices ! ','red',2);
   BR(4);
   print q{
<table cellspacing="0" cellpadding="2" border="1" width="100%">
 <tr>
  <td align="center" bgcolor="#C0C0C0">
   <input type="button" value="< Back" onClick="javascript:history.go(-1);">
   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
   <input type="submit" name="action" value="Next >">
  </td>
 </tr>
</table>
</form>
};
   USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
   exit();
}

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print '<br><br><font color="red">Undefined Action!</font><br><br>';
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print '<br><br><font color="red">No devices selected!</font><br><br>';
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}
my @devices_array = split /\x00/, $device_ids_str;
$device_ids_str = join(",", @devices_array);

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};
my $parent_location_id = $PARAM{"parent_location_id"};

my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="2" border="1" width="100%"');

my $table_rn = 0;
$table->put($table_rn,0,'Device Configuration Wizard - Page 4: Template Selection','class=header1');
$table_rn++;

my $cust_loc_str = USAT::DeviceAdmin::Util->customer_location_str(\%PARAM);
$table->put($table_rn,0,$cust_loc_str,'class=header1');
$table_rn++;

my @file_transfer_type_cd;
my $questionmarks;
if($PARAM{device_type} eq PKG_DEVICE_TYPE__G4) {
   @file_transfer_type_cd = ( PKG_FILE_TYPE__G4_CONFIGURATION_DATA_TEMPLATE );
   $questionmarks = '?';
} elsif($PARAM{device_type} eq PKG_DEVICE_TYPE__G5) {
   @file_transfer_type_cd = ( PKG_FILE_TYPE__G4_CONFIGURATION_DATA_TEMPLATE );
   $questionmarks = '?';
} elsif($PARAM{device_type} eq PKG_DEVICE_TYPE__EDGE) {
   @file_transfer_type_cd = ( PKG_FILE_TYPE__EDGE_DEFAULT_CONFIGURATION_TEMPLATE,
                              PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE );
   $questionmarks = '?,?';
} else {
}

my $popup1 = OOCGI::Popup->new(name => 'file_name', 
   sql => qq{ SELECT file_transfer_name, file_transfer_name fname 
                FROM file_transfer
               WHERE file_transfer_type_cd IN( $questionmarks )
            ORDER BY file_transfer_name }, bind => \@file_transfer_type_cd );

my $popup2 = USAT::DeviceAdmin::UI::USAPopups->pop_edit_mode;

$table->put($table_rn,0,'<br>Basis Template: '.$popup1.NBSP(1).$popup2.'<br><br>','align=center');
$table_rn++;
$table->put($table_rn,0,q{
<table cellspacing="0" cellpadding="2" border="1" width="100%">
 <tr>
  <td align="center" bgcolor="#C0C0C0">
   <input type="button" value="< Back" onClick="javascript:history.go(-1);">
   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
   <input type="submit" name="action" value="Next >">
  </td>
 </tr>
</table>});
if($PARAM{device_type} eq PKG_DEVICE_TYPE__G4 ||
   $PARAM{device_type} eq PKG_DEVICE_TYPE__G5) {
   print '<form method="post" action="bulk_config_wizard_5.cgi">';
} elsif($PARAM{device_type} eq PKG_DEVICE_TYPE__EDGE) {
   print '<form method="post" action="bulk_config_wizard_5_edge.cgi">';
} else {
   print '<form method="post" action="bulk_config_wizard_6.cgi">';
}

HIDDEN_PASS('customer_id', 'location_id', 'parent_location_id','start_serial_head','device_type');
 
display $table;

HIDDEN('include_device_ids',$device_ids_str);

print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
