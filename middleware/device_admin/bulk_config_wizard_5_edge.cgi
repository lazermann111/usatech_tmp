#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::Common::Const;
use USAT::DeviceAdmin::UI::DAHeader;
use URI;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::Config::Form;
use USAT::DeviceAdmin::Config::Form::Object qw(EDITOR_BASIC EDITOR_ADVANCED);   #just to import EDITOR_* constants
use USAT::DeviceAdmin::Config::Form::Object::TextField qw(RESTRICTION_NUMERIC RESTRICTION_REQUIRED); #just to import RESTRICTION_* constants

my $query = OOCGI::OOCGI->new;
$query->{'escape'} = 0;

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
$session->destroy;

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print '<br><br><font color="red">Undefined Action!</font><br><br>';
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print '<br><br><font color="red">No devices selected!</font><br><br>';
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};

my $file_name = $PARAM{"file_name"};

if(not defined $file_name)
{
	print "Required parameter not found: file_name!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $file_data = USAT::DeviceAdmin::Util::blob_select_by_name(undef, $file_name);

if(not defined $file_data) {
        print "$file_name not found";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
}

my $file_data_hex = uc($file_data);
my $file_data_len = length($file_data_hex);

$file_data = pack("H*", $file_data_hex);

$file_data =~ tr/\r//d;  # added to make sure it splits unix and windows data
chomp($file_data);       # just in case if we have new line at the end, remove it.

my @config_lines = split(/\n/, $file_data);
my $config_params = '';
my $default_textbox_size = 68;
my %config_hash;
my $max_param_code_len  = 60;
my $max_param_value_len = 200;
my @config_array;
my $device_type = PKG_DEVICE_TYPE__EDGE;
my $edit_mode = $PARAM{edit_mode};
my $title = "$file_name (" . ($edit_mode eq 'B' ? 'basic' : 'advanced') . ')';

print '<form name="myform" method="post" action="bulk_config_wizard_6_edge.cgi">';
HIDDEN("edit_mode",   $edit_mode);
HIDDEN("device_type", $device_type);

my $rowno = 0;
my $table = OOCGI::NTable->new('cellpadding="3" border="1" cellspacing="0" width="100%"');
$table->put($rowno++,0, B($title),'align="center" colspan="2" bgcolor="#C0C0C0"');

{
   local $@;
   eval { @config_array = USAT::DeviceAdmin::Config::Form::load_device_type($device_type); };
   if ($@) {
       print "Error: $@";
       USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
       exit;
   }
}

foreach my $config_line (@config_lines)
{
    chomp($config_line);
    my ($config_param, $config_value) = split(/=/, $config_line, 2);
    ($config_param, $config_value) = (substr($config_param, 0, $max_param_code_len), substr($config_value, 0, $max_param_value_len));
    $config_param =~ s/^\s+//;
    $config_param =~ s/\s+$//;
    $config_value =~ s/^\s+//;
    $config_value =~ s/\s+$//;

    $config_line = "$config_param=$config_value";

    if (length($config_param) > 0)
    {
        $config_hash{uc($config_param)} = $config_value;
    }
}

### generate heading index ###
my @heading_index;
foreach my $config_item (@config_array)
{
    next unless UNIVERSAL::isa($config_item, 'USAT::DeviceAdmin::Config::Form::Object');    #ignore unknown elements
    if (($edit_mode eq 'A' || ($edit_mode eq 'B' && $config_item->editor eq EDITOR_BASIC))
        && $config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Header'))
    {
        push @heading_index, $config_item;
    }
}

if (@heading_index)
{
    my $temp = OOCGI::NTable->new('border="0" cellpadding="0" cellspacing="0" align="left"');
    $temp->put(0,0,B('Section Index:').NBSP(3),'valign="top" bgcolor="#EEEEEE"');
    my $str = '';
    foreach my $link ( sort { lc($a->parameter_code) cmp lc($b->parameter_code) } @heading_index ) {
       $str .= q{<a href="#}.URI->new($link->parameter_code)->as_string.q{">}.$link->parameter_code.q{</a><br>};
    }
    $temp->put(0,1,$str,'bgcolor="#EEEEEE"');
       
    $table->put($rowno++,0, '<a name="___section_index"></a>'.$temp->string,
                'colspan="2" width="100%" align="left" bgcolor="#EEEEEE"');
    $table->put($rowno++,0,B('* Please note: in order for the changes to take effect you must check the checkboxes of the properties you want to change','red',-2),'colspan=2 align="center"');
}

my $checkbox_section_count = 0;

foreach my $config_item (@config_array)
{  
    my $parameter_name = $config_item->parameter_name;
    my $parameter_code = $config_item->parameter_code;
    next unless UNIVERSAL::isa($config_item, 'USAT::DeviceAdmin::Config::Form::Object');    #ignore unknown elements

    if ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Header'))
    {
        if ($edit_mode eq 'A' || ($edit_mode eq 'B' && $config_item->editor eq EDITOR_BASIC))
        {
            $checkbox_section_count++;
             
             my $str = qq{<b><input type="checkbox" id=section_$checkbox_section_count value="Y" onClick="select_all($checkbox_section_count);"> Check all}.NBSP(10).q{ <font color="#0000CC"><a name="}.URI->new($parameter_code)->as_string.'">'.$parameter_code.q{</a></b></font>&nbsp;&nbsp;&nbsp;<a href="#___section_index"><img src="/icons/small/back.gif" border="0">};
             $table->put($rowno++,0,$str,'colspan="2" width="100%" align="center" bgcolor="#C0C0C0"');
        }
    }
    else
    {
       my $file_transfer_obj = USAT::DeviceAdmin::DBObj::File_transfer->new({file_transfer_name => $file_name});
       my $fttc = $file_transfer_obj->file_transfer_type_cd; # fttc = file transfer type code
       if(!defined  $config_hash{$parameter_code} && $fttc eq PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE ) {
          next; # CUSTOM data has fever element, so skip the unused ones.
       }
       $config_params = "$config_params|||".$parameter_code;
       my $str2 = '';
       if ($edit_mode eq 'A' || ($edit_mode eq 'B' && $config_item->editor eq EDITOR_BASIC))
       {
             my $label = '';
             if(defined $parameter_name) {
                $label .= $parameter_name . ' [' . $parameter_code . ']';
             } else {
                $label .= $parameter_code;
             }
             if(defined $config_item->restriction) {
                $label .= B('*','red');
             }
             my $checkbox = CHECKBOX("chkb_$parameter_code".'_'.$checkbox_section_count, $parameter_code, B($label));
             $table->put($rowno,0, $checkbox,'width="50%"');

             if ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::TextField'))
             {
                my $param_value = $config_hash{uc($parameter_code)};
                my $param_hash;
                my $value = $config_item->default;
                if(length($param_value) > 0 ) {
                   $value = $param_value;
                }
                $param_hash->{size} = $default_textbox_size;
                if(defined $config_item->maxlength) {
                   if($config_item->maxlength + 7 <= $default_textbox_size) {
                      $param_hash->{size} = $config_item->maxlength + 7;
                   }
                }
                $param_hash->{maxlength} = $max_param_value_len;
                if(defined $config_item->maxlength) {
                   $param_hash->{maxlength} = $config_item->maxlength;
                }
                if(defined $config_item->restriction) {
                   $param_hash->{class} = $config_item->restriction;
                }
                if(defined $config_item->read_only && $config_item->read_only ) {
                   $param_hash->{readonly} = '';
                   $param_hash->{style} = "border:1px solid #000; background-color:#cc9999;color:#000000;";
                }
                $str2 = TEXT("cfg_".$parameter_code, $value, $param_hash);
             }
             elsif ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Radio'))
             {
                my @choices = @{$config_item->choices};
                foreach my $choice (@choices) {
                     my $checked = '';
                     if(length($config_hash{uc ($parameter_code)}) > 0) {
                        if(uc($config_hash{uc($parameter_code)}) eq uc($choice)) {
                           $checked = 'checked';
                        }
                     } else {
                        if(uc($config_item->default) eq uc($choice)) {
                           $checked = 'checked';
                        }
                     }
                     my $name  = 'cfg_'.$parameter_code;
                     my $value = $choice;
                     if($checked) {
                        $str2 .= qq{<input type="radio" name="$name" value="$value" $checked>$choice</input>\n};
                     } else {
                        $str2 .= qq{<input type="radio" name="$name" value="$value" >$choice</input>\n};
                     }
                     
                 }
             }
             elsif ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Select'))
             {
                 $str2 = qq{<select name="cfg_$parameter_code">};

                 my @choices = @{$config_item->choices};
                 foreach my $choice (@choices)
                 {
                     $str2 .= qq{
                          <option value="$choice"}. (length($config_hash{uc($parameter_code)}) > 0 ? (uc($config_hash{uc($ parameter_code)}) eq uc($choice) ? ' selected' : '') : (uc($config_item->default) eq uc($choice) ? ' selected' : '')) . qq{>$choice</option>};
                 }
                 $str2 .= '</select>';
             }
             else
             {
                 $str2 = qq{
                 <input type="text" name="cfg_}.$parameter_code.qq{" value="$config_hash{uc($parameter_code)}" size="$default_textbox_size" maxlength="$max_param_value_len">};
             }
             $table->put($rowno++,1,$str2,'width="50%"');
             $str2 = I($config_item->description,-1);
             $table->put($rowno++,0,$str2,'colspan="2" bgcolor="#EEEEEE"');
        }
    }
}
$config_params = substr($config_params, 3);

HIDDEN("config_params", $config_params),"\n";
HIDDEN("file_name", $file_name),"\n";
HIDDEN("checkbox_section_count",$checkbox_section_count);

$table->put($rowno++,0, B($title),'align="center" colspan="2" bgcolor="#C0C0C0"');

display $table;

HIDDEN_PASS('customer_id','location_id','parent_location_id','include_device_ids');

print qq{
<table cellspacing="0" cellpadding="2" border="1" width="100%">
 <tr>
  <td align="center" bgcolor="#C0C0C0">
   <input type="button" value="< Back" onClick="javascript:history.go(-1);"> 
   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
   <input type="submit" name="action" value="Next >">
  </td>
 </tr>
</table>
<script language="javascript">
<!--
function select_all(checkbox_section) {
    var theForm = document.myform;
    for (i=0; i<theForm.elements.length; i++) {
        if (theForm.elements[i].value >= 0) {
           var val = theForm.elements[i].value;
           var name = 'chkb_'+val+'_' + checkbox_section;
           if (theForm.elements[i].name==name) {
               if(theForm.elements[i].checked == true ) {
                  theForm.elements[i].checked = false;
               } else {
                  theForm.elements[i].checked = true;
               }
           }
        }
    }
}
//-->
</script>

</form>
};

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
