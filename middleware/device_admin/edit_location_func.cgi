#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

my $location_id = $PARAM{"location_id"};
if(length($location_id) == 0)
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	$session->print_menu;
	print "Required Parameter Not Found: location_id";
	$session->destroy;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if($location_id eq '1')
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	$session->print_menu;
	print "<br><font color=\"red\">You may NOT alter the default location \"Unknown\"!</font><br><br>";
	$session->destroy;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $name;
my $addr1;
my $addr2;
my $city;
my $county;
my $postal_cd;
my $parent_location_id;
my $country;
my $location_type_id;
my $state;
my $time_zone;
my $update_count;

my $action = $PARAM{"action"};
if($action eq "Save")
{
	$name   = $PARAM{"name"};
	$addr1  = $PARAM{"addr1"};
	$addr2  = $PARAM{"addr2"};
	$city   = $PARAM{"city"};
	$county = $PARAM{"county"};
	$postal_cd = $PARAM{"postal_cd"};
	$parent_location_id = $PARAM{"parent_location_id"};
	$country   = $PARAM{"country"};
	$location_type_id = $PARAM{"location_type_id"};
	$state     = $PARAM{"state"};
	$time_zone = $PARAM{"time_zone"};
	
	my $update_location_stmt = $dbh->prepare("update location.location set location_name=:name, location_addr1=:addr1, location_addr2=:addr2, location_city=:city, location_county=:county, location_postal_cd=:postal_cd, parent_location_id=:parent_location_id, location_country_cd=:country, location_type_id=:location_type_id, location_state_cd=:state, location_time_zone_cd=:time_zone where location_id = :location_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$update_location_stmt->bind_param(":name", $name);
	$update_location_stmt->bind_param(":addr1", $addr1);
	$update_location_stmt->bind_param(":addr2", $addr2);
	$update_location_stmt->bind_param(":city", $city);
	$update_location_stmt->bind_param(":county", $county);
	$update_location_stmt->bind_param(":postal_cd", $postal_cd);
	$update_location_stmt->bind_param(":parent_location_id", $parent_location_id);
	$update_location_stmt->bind_param(":country", $country);
	$update_location_stmt->bind_param(":location_type_id", $location_type_id);
	$update_location_stmt->bind_param(":state", $state);
	$update_location_stmt->bind_param(":time_zone", $time_zone);
	$update_location_stmt->bind_param(":location_id", $location_id);
	$update_count = $update_location_stmt->execute();
	$update_location_stmt->finish();
	
	$dbh->disconnect;
	print "Location: edit_location.cgi?location_id=$location_id\n";
	$session->destroy;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	exit();	
}
elsif($action eq "Delete")
{
	my $check_delete_stmt = $dbh->prepare("select count(1) from pos where location_id = :location_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$check_delete_stmt->bind_param(":location_id", $location_id);
	$check_delete_stmt->execute();
	my @count_data = $check_delete_stmt->fetchrow_array();
	$check_delete_stmt->finish();
	if(!@count_data)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
       	$session->print_menu;
		print "check_delete_stmt failed!\n";
	    $session->destroy;
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
	else
	{
		my $location_count = $count_data[0];
		if($location_count > 0)
		{
			USAT::DeviceAdmin::UI::DAHeader->printHeader();
			USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        	$session->print_menu;
			print "Can not delete this location because there are $location_count devices currently assigned to it.  <br>Please reassign all the devices to a different location then retry.\n";
        	$session->destroy;
			$dbh->disconnect;
			USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
			exit;
		}

		my $update_location_stmt = $dbh->prepare("update location.location set location_active_yn_flag = 'N' where location_id =  :location_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$update_location_stmt->bind_param(":location_id", $location_id);
		$update_count = $update_location_stmt->execute();
		$update_location_stmt->finish();
		$dbh->disconnect;
		
		print "Location: location_menu.cgi\n";
	    $session->destroy;
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		exit();	
	}
}
elsif($action eq "Undelete")
{
	my $update_location_stmt = $dbh->prepare("update location.location set location_active_yn_flag = 'Y' where location_id =  :location_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$update_location_stmt->bind_param(":location_id", $location_id);
	$update_count = $update_location_stmt->execute();
	$update_location_stmt->finish();
	$dbh->disconnect;
	
	print "Location: location_menu.cgi\n";
	$session->destroy;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
print "No Action Defined!";
$session->destroy;
$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
