#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print q|<form method="post" name="location_list">|;

my $pageNav  = OOCGI::PageNavigation->new(15);

my $parent_location_id = $PARAM{"parent_location_id"};
my $location_type = $PARAM{"location_type"};
my $location_name = $PARAM{"location_name"};
my $location_city = $PARAM{"location_city"};
if(length($location_type) == 0 &&
   length($location_name) == 0 &&
   length($location_city) == 0 &&
   length($parent_location_id) == 0)
{
   $location_name = '';
}

my $list_locations_stmt;


my $bind_value;
my $sql=q|SELECT location_id,
                 location_name,
                 location_city,
                 country_name,
                 location_type.location_type_desc,
                 time_zone_name
            from location.location,
                 location_type,
                 country,
                 time_zone
           WHERE location.location_type_id = location_type.location_type_id (+)
             AND location.location_country_cd = country.country_cd (+)
             AND location.location_time_zone_cd = time_zone.time_zone_cd (+)|;
if(defined $location_name)
{
   $sql .= q| AND LOWER(location_name) LIKE LOWER(?) |;
   $location_name = $query->filter_trim_whitespace_head($location_name);
   $bind_value = $location_name.'%';
}
elsif(defined $location_city)
{
   $sql .= q| AND LOWER(location_city) LIKE LOWER(?) |;
   $location_city = $query->filter_trim_whitespace($location_city);
   $bind_value = $location_city.'%';
}
elsif(defined $parent_location_id)
{
   $sql .= q| AND location.parent_location_id = ? |;
   $bind_value = $parent_location_id;
}
elsif(defined $location_type)
{
   $sql .= q| AND location_type.location_type_id = ? |;
   $bind_value = $location_type;
}

my @table_cols = (
    ['Name',      1],
    ['City',      2],
    ['Country',   3],
    ['Type',      4],
    ['Time Zone', 5]
);

my @sql_cols = (
    'LOWER(location_name)',
    'LOWER(location_city)',
    'LOWER(country_name)',
    'LOWER(location_type.location_type_desc)',
    'LOWER(time_zone_name)'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');
$table->put($rn_table,0,B('Location List'),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

$sql = $sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql, [ $bind_value ] );

my $row0 = "row0";
my $row1 = "row1";
my $i = 0;
while (my @data = $qo->next) 
{
   my $row = $i%2 ? $row1 : $row0;
   $table->insert($rn_table,0,qq|<a href="edit_location.cgi?location_id=$data[0]">$data[1]</a>|,"class=$row");
   $table->insert($rn_table,1,$data[2],"class=$row");
   $table->insert($rn_table,2,$data[3],"class=$row");
   $table->insert($rn_table,3,$data[4],"class=$row");
   $table->insert($rn_table,4,$data[5],"class=$row");
   $rn_table++;
   $i++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
