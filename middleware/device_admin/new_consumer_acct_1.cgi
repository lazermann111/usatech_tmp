#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print qq|<form method="post" action="new_consumer_acct_1a.cgi">|;

my $fmt_popup = OOCGI::Popup->new(
					name => 'consumer_acct_fmt_id', 
					align => 'yes', 
					delimiter => '.',
					style => "font-family: courier; font-size: 12px;",
					sql => q|select consumer_acct_fmt_id, consumer_acct_fmt_name from pss.consumer_acct_fmt order by consumer_acct_fmt_id|);

my $cust_popup = OOCGI::Popup->new(
					name => 'customer_id',
					align => 'yes', 
					delimiter => '.',
					style => "font-family: courier; font-size: 12px;",
					sql => q|SELECT customer_id, customer_name FROM customer WHERE customer_active_yn_flag = 'Y' ORDER BY customer_name|);
					
my $loc_popup = USAT::DeviceAdmin::UI::USAPopups->pop_location(undef);

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
my $rc = 0;
$table->put($rc++,0, B('USAT Card Creation Wizard'),'align=center bgcolor=#C0C0C0 colspan=2');
$table->put($rc,0, 'Select a Card Type', 'nowrap');
$table->put($rc++,1, $fmt_popup);
$table->put($rc,0, 'Select a Customer', 'nowrap');
$table->put($rc++,1, $cust_popup);
$table->put($rc,0, 'Select a Location', 'nowrap');
$table->put($rc++,1, $loc_popup);

display $table;

print "&nbsp<br>" . SUBMIT('action','Next >') . "</form><br>&nbsp;";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

