#!/usr/local/USAT/bin/perl

use strict;
use USAT::DeviceAdmin::UI::USAPopups;
use OOCGI::OOCGI;

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

$query->printHeader();

my $sql = 'SELECT device_serial_format_prefix, device_serial_format_prefix
             FROM device.device_serial_format
            WHERE device_type_id = ?';

my $popPrefix = OOCGI::Popup->new(name => 'start_serial_head', sql => $sql, bind => [ $PARAM{device_type_id} ] );
$popPrefix->head('',' ');
$popPrefix->default('');

print 'Serial Prefix: '.$popPrefix->string;
