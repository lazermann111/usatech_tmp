#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use OOCGI::NTable;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::MessageV4;
use USAT::DeviceAdmin::UI::Header;

use strict;
my $query = OOCGI::OOCGI->new;

USAT::DeviceAdmin::UI::Header->printHeader();
USAT::DeviceAdmin::UI::Header->printFile("header.html");

USAT::DeviceAdmin::UI::MessageV4::message;

BR(1);
H3('ePort Network Protocol Message Maker v4.0');
BR(1);

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

my $table_rn = 0;

my $popDataType = OOCGI::Popup->new( name => 'data_type', 
   style => "font-family: courier; font-size: 10px;",
   pair => [ '418', '-0418 Component Identification',
             '420', '-0420 Authorization Request',
             '421', '-0421 Authorization Responce',
             '000', '-------------- Batch Session --------------',
             '422', '+0422 Batch Session Start',
             '423', '+0423 Online Authorization Batch - Actual',
             '424', '-0424 Online Authorization Batch - Intended',
             '425', '+0425 Offline Sale Batch - Account Data',
             '426', '+0426 Offline Sale Batch - Account&Expiration',
             '427', '+0427 Cash Sale Detail',
             '428', '-0428 Batch Summary',
             '429', '-0429 Batch ACK',
             '001', '------------- Event and Alerts ------------',
             '440', '-0440 Generic Event',
             '441', '-0441 Generic Alert',
             '002', '--------------- File Transfer --------------',
             '800', '-0800 Sony printer Info       --------------'
           ]);
$popDataType->default('421');
if($query->param('data_type') > 0 ) {
   $popDataType->default($query->param('data_type'));
}
    
$table->put($table_rn, 0, 'Data Type:');
$table->put($table_rn, 1, $popDataType);
$table->put($table_rn, 2, '2 Bytes');
$table->put($table_rn, 3, 'Hex');
$table_rn++;

USAT::DeviceAdmin::UI::MessageV4::table($table, $table_rn);

print q'<form method="post">';

display $table;

BR;
SUBMIT('userOP', 'Submit');
BR(2);

print '</form>';
USAT::DeviceAdmin::UI::Header->printFooter("footer.html");
