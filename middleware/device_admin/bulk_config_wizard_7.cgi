#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

require Text::CSV;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print "<br><br><font color=\"red\">Undefined Action!</font><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print "<br><font color=\"red\">No devices selected!</font><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};
my $zero_counters = $PARAM{"zero_counters"};

my $pos_pta_tmpl_id = $PARAM{"pos_pta_tmpl_id"};
my $mode_cd = $PARAM{"mode_cd"};

my @devices_array = split /,/, $device_ids_str;

my $parameters_str = $PARAM{"parameter_to_change"};
my @parameters_array = split /,/, $parameters_str;

my $debug = $PARAM{"debug"};
if(length($debug) == 0)
{
	$debug = 0;
}

if(length($parameters_str) == 0 && length($customer_id) == 0 && length($location_id) == 0 && length($zero_counters) == 0)
{
	print "<br><font color=\"red\">Nothing to do!</font><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

print qq{
<table cellpadding="3" border="1" cellspacing="0" width="100%">

 <form method="post" action="bulk_config_wizard_8.cgi" onSubmit="javascript:return confirm('Please click OK to continue, and wait while your changes are processed!\\n\\nYou will receive a popup confirmation of the update results when completed.');">};

HIDDEN_PASS('customer_id',        'location_id', 'parent_location_id', 
            'include_device_ids', 'debug',       'mode_cd',
            'zero_counters',      'pos_pta_tmpl_id');

HIDDEN("parameter_to_change","" . join(',', @parameters_array) ."");

print qq{
 <tr>
  <th align="center" colspan="3" bgcolor="#C0C0C0">Device Configuration Wizard - Page 8: Confirm Changes</th>
 </tr>

 <tr>
  <td style="font-size: 14pt;"><br>};  
  
if($debug)
{
	print "<center><font color=\"red\"><b>----- DEBUG IS ON!  CHANGES WILL NOT BE SAVED! -----</b></font></center><br>\n";
}

print "
  Please confirm that you want to make the following changes!<br>
  <br>
  Change Customer and Location:<br>
  <table border=\"1\" cellspacing=\"0\" cellpadding=\"2\">
   <tr><th style=\"font-size: 10pt;\">Customer</th><th style=\"font-size: 10pt;\">Location</th><th style=\"font-size: 10pt;\">Parent Location</th></tr>
   <tr>";
   
if(length($customer_id) > 0)
{
    my $customer = USAT::DeviceAdmin::DBObj::Customer->new($PARAM{customer_id});
	print '<td style="font-size: 10pt;">'.$customer->customer_name.'</td>';
} else {
	print "<td style=\"font-size: 10pt;\">Do not change</td>";
}

if(length($location_id) > 0)
{
    my $location = USAT::DeviceAdmin::DBObj::Location->new($PARAM{location_id});
    print '<td style="font-size: 10pt;">'.$location->name_city_state_country.'</td>';
} else {
	print "<td style=\"font-size: 10pt;\">Do not change</td>";
}

if(length($PARAM{parent_location_id}) > 0)
{
    my $location = USAT::DeviceAdmin::DBObj::Location->new($PARAM{parent_location_id});
    print '<td style="font-size: 10pt;">'.$location->name_city_state_country.'</td>';
} else {
    print "<td style=\"font-size: 10pt;\">Do not change</td>";
}

my $template_name;

if(length($PARAM{pos_pta_tmpl_id}) >= 0)
{
    my $ppt = USAT::DeviceAdmin::DBObj::Pos_pta_tmpl->new($PARAM{pos_pta_tmpl_id});
	$template_name = $ppt->pos_pta_tmpl_name;
}

print "</tr></table>
  <br>
  Import Payment Template:
  <table border=\"1\" cellspacing=\"0\" cellpadding=\"2\">
   <tr><th style=\"font-size: 10pt;\">Template Name</th><th style=\"font-size: 10pt;\">Import Mode</th></tr>
";

if(length($mode_cd) && length($pos_pta_tmpl_id) > 0)
{
	print "<tr><td style=\"font-size: 10pt;\">$template_name</td><td style=\"font-size: 10pt;\">$mode_cd</td></tr>\n";
}
else
{
	print "<tr><td style=\"font-size: 10pt;\" colspan=\"2\">Do Not Import</thd></tr>\n";
}

print "</tr></table>
  <br>
  Update Configuration Parameters:
  <table border=\"1\" cellspacing=\"0\" cellpadding=\"2\">
   <tr><th style=\"font-size: 10pt;\">Parameter</th><th style=\"font-size: 10pt;\">New Value</th></tr>
";

if(length($parameters_str) == 0 && !$zero_counters)
{
	print "<tr><td style=\"font-size: 10pt;\" colspan=\"5\">No Configuration Changes</td></tr>\n";
}

foreach my $change_location (@parameters_array)
{
	my $start_addr = $change_location;
	
	my $data = $PARAM{$start_addr};
	my $name = $PARAM{$start_addr."_name"};
	
	print "<tr><td style=\"font-size: 10pt;\">$name&nbsp;</td><td style=\"font-size: 10pt;\" align=\"center\">$data&nbsp;</td></tr>\n";

	if(length($name) == 0)
	{
		print "\n\n<font color=\"red\"><b>Fatal Error: Memory Location Inconsistency!</b></font><br><pre>\n\n";
		print "<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print or email this screen and give it to an administrator.\");</script>";
		
		print "POST DATA -----------------\n";
		foreach my $key (sort(keys(%PARAM))) 
		{
			print "$key = $PARAM{$key}\n";
		}

		print "\n";
		print "ENV -----------------\n";
		foreach my $key (sort(keys(%ENV))) 
		{
			print "$key = $ENV{$key}\n";
		}
		
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
	
    HIDDEN_PASS($start_addr.'_size',           $start_addr.'_align',
                $start_addr.'_pad_with',       $start_addr.'_data_mode',
                $start_addr.'_eerom_location', $start_addr.'_name',
                $start_addr);
}

print "
	<tr><td style=\"font-size: 10pt;\">Zero Out Counters?</td><td style=\"font-size: 10pt;\" align=\"center\">" . ($zero_counters > 0 ? "Yes" : "No") . "</td></tr>
  </table>
  <br>
  For all of the following devices:<br>
  (Pokes will be created and queued for the devices where needed).<br>
  <table border=\"1\" cellspacing=\"0\" cellpadding=\"2\">
   <tr><th style=\"font-size: 10pt;\">EV Number</th><th>Serial Number</th><th style=\"font-size: 10pt;\">Customer</th><th style=\"font-size: 10pt;\">Location</th><th style=\"font-size: 10pt;\">Firmware Version</th></tr>";
foreach my $device_id (@devices_array)
{
    my $sql =q{SELECT device.device_id,
                      device.device_name,
                      device.device_serial_cd,
                      to_char(device.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'),
                      location.location_name,
                      customer.customer_name,
                      device_setting.device_setting_value
                 FROM device,
                      device_type,
                      pos,
                      location.location,
                      customer,
                      device_setting
                WHERE device.device_type_id = device_type.device_type_id
                  AND device.device_id = pos.device_id
                  AND pos.location_id  = location.location_id
                  AND pos.customer_id  = customer.customer_id
                  AND device.device_id = device_setting.device_id(+)
                  AND device_setting.device_setting_parameter_cd(+) = 'Firmware Version'
                  AND device.device_id = ? };
    my $qo = OOCGI::Query->new($sql, { bind => $device_id });

   if(my @data = $qo->next_array) {
	print "<tr><td style=\"font-size: 10pt;\">$data[1]&nbsp;</td><td style=\"font-size: 10pt;\">$data[2]&nbsp;</td><td style=\"font-size: 10pt;\">$data[5]&nbsp;</td><td style=\"font-size: 10pt;\">$data[4]&nbsp;</td><td style=\"font-size: 10pt;\">$data[6]&nbsp;</td></tr>\n";
   }
}

print "</table><br>";

if($debug)
{
	print "<center><font color=\"red\"><b>----- DEBUG IS ON!  CHANGES WILL NOT BE SAVED! -----</b></font></center><br>\n";
}

print "
  </td
 </tr>
</table>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">
 <tr>
  <td align=\"center\" bgcolor=\"#C0C0C0\">
   <input type=\"button\" value=\"< Back\" onClick=\"javascript:history.go(-1);\">
   <input type=button value=\"Cancel\" onClick=\"javascript:window.location = '/';\">
   <input type=\"submit\" name=\"action\" value=\"Finish >\">
  </td>
 </tr>
</form>
</table>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
