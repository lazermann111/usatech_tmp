#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $name;
my $addr1;
my $addr2;
my $city;
my $county;
my $postal_cd;
my $country;
my $customer_type_id;
my $state;
my $app_user_name;
my $email_addr;
my $subdomain;

my $action = $PARAM{"action"};
if($action eq "Save")
{
	$name       = $PARAM{"name"};
	$addr1      = $PARAM{"addr1"};
	$addr2      = $PARAM{"addr2"};
	$city       = $PARAM{"city"};
	$county     = $PARAM{"county"};
	$postal_cd  = $PARAM{"postal_cd"};
	$country    = $PARAM{"country"};
	$state      = $PARAM{"state"};
	$email_addr = $PARAM{"email_addr"};
	$subdomain  = $PARAM{"subdomain"};
	$customer_type_id = $PARAM{"customer_type_id"};
	$app_user_name    = $PARAM{"app_user_name"};
	
#	my $insert_customer_stmt = $dbh->prepare("insert into customer(customer_name, customer_addr1, customer_addr2, customer_city, customer_county, customer_postal_cd, customer_country_cd, customer_type_id, customer_state_cd) values (:customer_name, :customer_addr1, :customer_addr2, :customer_city, :customer_county, :customer_postal_cd, :customer_country_cd, :customer_type_id, :customer_state_cd)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
#	$insert_customer_stmt->bind_param(":customer_name", $name);
#	$insert_customer_stmt->bind_param(":customer_addr1", $addr1);
#	$insert_customer_stmt->bind_param(":customer_addr2", $addr2);
#	$insert_customer_stmt->bind_param(":customer_city", $city);
#	$insert_customer_stmt->bind_param(":customer_county", $county);
#	$insert_customer_stmt->bind_param(":customer_postal_cd", $postal_cd);
#	$insert_customer_stmt->bind_param(":customer_country_cd", $country);
#	$insert_customer_stmt->bind_param(":customer_type_id", $customer_type_id);
#	$insert_customer_stmt->bind_param(":customer_state_cd", $state);
#	$insert_customer_stmt->execute();
#	$insert_customer_stmt->finish()
#"Return Value" := pkg_customer_maint.add_customer('test1','a','b','c','d','e','PA','US',1,'test11111','test@test.test','test1');
#l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
#l_customer_addr1 CUSTOMER.CUSTOMER_ADDR1%TYPE,
#l_customer_addr2 CUSTOMER.CUSTOMER_ADDR2%TYPE,
#l_customer_city CUSTOMER.CUSTOMER_CITY%TYPE,
#l_customer_county CUSTOMER.CUSTOMER_COUNTY%TYPE,
#l_customer_postal_cd CUSTOMER.CUSTOMER_POSTAL_CD%TYPE,
#l_customer_state_cd CUSTOMER.CUSTOMER_STATE_CD%TYPE,
#l_customer_country_cd CUSTOMER.CUSTOMER_COUNTRY_CD%TYPE,  -- US / CA
#l_customer_type_id CUSTOMER.CUSTOMER_TYPE_ID%TYPE,
#l_app_user_name  APP_USER.APP_USER_NAME%TYPE,
#l_email_addr APP_USER.APP_USER_EMAIL_ADDR%TYPE,
#l_subdomain SUBDOMAIN_NAME.SUBDOMAIN_NAME_CD%TYPE)
	my $customer_id;
#	print qq{
	my $create_customer_stmt = $dbh->prepare(q{
		BEGIN
		:customer_id := pkg_customer_maint.add_customer(:customer_name, :customer_addr1, :customer_addr2, :customer_city, :customer_county, :customer_postal_cd, :customer_state_cd, :customer_country_cd, :customer_type_id, :app_user_name, :email_addr, :subdomain); 
		END; 
	}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$create_customer_stmt->bind_param_inout(":customer_id", \$customer_id, 20);
	$create_customer_stmt->bind_param(":customer_name", $name);
	$create_customer_stmt->bind_param(":customer_addr1", $addr1);
	$create_customer_stmt->bind_param(":customer_addr2", $addr2);
	$create_customer_stmt->bind_param(":customer_city", $city);
	$create_customer_stmt->bind_param(":customer_county", $county);
	$create_customer_stmt->bind_param(":customer_postal_cd", $postal_cd);
	$create_customer_stmt->bind_param(":customer_state_cd", $state);
	$create_customer_stmt->bind_param(":customer_country_cd", $country);
	$create_customer_stmt->bind_param(":customer_type_id", $customer_type_id);
	$create_customer_stmt->bind_param(":app_user_name", $app_user_name);
	$create_customer_stmt->bind_param(":email_addr", $email_addr || $app_user_name);
	$create_customer_stmt->bind_param(":subdomain", $subdomain);
	eval {$create_customer_stmt->execute};
	if (@$) {
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
        USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
		print "<br><br>Statement execution error: " . $@ . "<br><br>";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html"); 
		exit();
	}
	my $db_err_str = $dbh->errstr;
	$create_customer_stmt->finish();
	if (defined $db_err_str || !defined $customer_id) {
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
        USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
		print "<br><br>Statement execution error: " . $db_err_str . "<br><br>";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}
#	};
	$dbh->disconnect;
	print "Location: /\n";
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	exit();	
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;
print "No Action Defined!";
$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
