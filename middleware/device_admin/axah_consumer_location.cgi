#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use OOCGI::NTable;
use OOCGI::Query;
use USAT::DeviceAdmin::UI::USAPopups;

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

$query->printHeader();

my $sql = q{SELECT la.location_id,
            RPAD('.', depth*3, '.') || la.location_name , la.location_city,
                   la.location_state_cd, la.location_country_cd
              FROM location.LOCATION la, location.LOCATION lb, vw_location_hierarchy vlh
             WHERE la.location_id = vlh.descendent_location_id
               AND lb.location_id = vlh.ancestor_location_id
               AND lb.parent_location_id IS NULL
               AND LOWER (la.location_name) like ?
          ORDER BY LOWER (vlh.hierarchy_path)};

my $popLocation = OOCGI::Popup->new(name => 'location_id',
              style => "font-family: courier; font-size: 12px;",
              align => 'yes', delimiter => '.',
              bind  => [ lc($PARAM{location_prefix}).'%' ],
              sql => $sql);
$popLocation->head('0','Undefined');

$popLocation->default($PARAM{location_id});

print $popLocation;
