#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Common::Const;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print <<JSCRIPT;
<script language="JavaScript">
function getAXAHUpdateDivDeactivateDiv(url, elementContainer, elementContainerToHide) {
	var theHttpRequest = getNewHttpObject();
	var myArr = new Array(
		null,
		new Array(
			function (elementContainer, elementContainerToHide) {
				document.getElementById(elementContainer).innerHTML = '<img src="/img/progress_bar.gif">';
				document.getElementById(elementContainerToHide).innerHTML='&nbsp;';
			},
			elementContainer,
			elementContainerToHide
		),
		null, 
		null,
		new Array(
			function (theHttpRequest, elementContainer, elementContainerToHide) {
			   if (theHttpRequest.status == 200) {
				   document.getElementById(elementContainer).innerHTML = theHttpRequest.responseText;
			   } else {
				   document.getElementById(elementContainer).innerHTML='<span class="imageviewtitle">The requested content is currently unavailable.<\/span>';
			   }
				document.getElementById(elementContainerToHide).innerHTML='&nbsp;';
			},
			theHttpRequest,
			elementContainer,
			elementContainerToHide
		)
	);
	getAXAH(url, myArr, false, theHttpRequest);	//asynchronous
}
</script>
JSCRIPT

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print '<br><br><font color="red">Undefined Action!</font><br><br>';
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print '<br><br><font color="red">No devices selected!</font><br><br>';
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $popCustomer = USAT::DeviceAdmin::UI::USAPopups->popCustomer;
$popCustomer->head('','Do not change Customer');
$popCustomer->default('');
my $popLocation = USAT::DeviceAdmin::UI::USAPopups->popLocation;
$popLocation->head('','Do not change Location');
$popLocation->default('');


my @devices_array = split /\x00/, $device_ids_str;
$device_ids_str = join(",", @devices_array);

if($PARAM{device_type} eq PKG_DEVICE_TYPE__G4   ||
   $PARAM{device_type} eq PKG_DEVICE_TYPE__G5   ||
   $PARAM{device_type} eq PKG_DEVICE_TYPE__EDGE ) {
   print '<form method="post" action="bulk_config_wizard_4.cgi">';
} else {
   print '<form method="post" action="bulk_config_wizard_6.cgi">';
}

my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="2" border="1" width="100%"');
$table->put(0,0,'Device Configuration Wizard - Page 3: Customer & Location Selection','class=header1 colspan=3');
$table->put(1,0,'Change Customer<br> on All Devices To: ','colspan=2');
$table->put(1,2, $popCustomer);

my $ajax_img = q{<img src="/img/ajax.gif" border="0" title="Click the link at right for AJAX action">};
my $str_location1 = q{<a href="#" onClick="getAXAHUpdateDivDeactivateDiv('axah_get_location_popup.cgi?name=location_id','location_div1', 'location_div2');"> Change Location<br> on All Devices To: </a>};
$table->put(2,0, $ajax_img);
$table->put(2,1, $str_location1);
$table->put(2,2, '<div id=location_div1>&nbsp;</div>');

$table->put(3,0, $ajax_img);
my $str_location2 = q{<a href="#" onClick="getAXAHUpdateDivDeactivateDiv('axah_get_location_popup.cgi?name=parent_location_id','location_div2', 'location_div1');"> Change Parent Location<br> on All Devices To: </a>};
$table->put(3,1, $str_location2);
$table->put(3,2, '<div id=location_div2><input type="hidden" name="parent_location_id" value="">&nbsp;</div>');
my $chbChangeConfigurationYes = RADIO('change_configuration','1', 'Yes', { checked => 'yes'} );
my $chbChangeConfigurationNo  = RADIO('change_configuration','0', 'No' );
$table->put(4,0, 'Change Configuration:','colspan=2');
$table->put(4,2, $chbChangeConfigurationYes.NBSP(4).$chbChangeConfigurationNo);

display $table;

HIDDEN('include_device_ids',"$device_ids_str");
HIDDEN_PASS('start_serial_head','device_type');

print qq{
<table cellspacing="0" cellpadding="2" border="1" width="100%">
 <tr>
  <td align="center" bgcolor="#C0C0C0">
   <input type="button" value="< Back" onClick="javascript:history.go(-1);">
   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
   <input type="submit" name="action" value="Next >">
  </td>
 </tr>
</table>
</form>
};

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
