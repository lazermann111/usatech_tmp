#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu =	$session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $customer_id = $PARAM{"customer_id"};
if(length($customer_id) == 0)
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	print $user_menu;
	print "Required Parameter Not Found: customer_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if($customer_id eq '1')
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	print $user_menu;
	print "<br><font color=\"red\">You may NOT alter the default customer \"Unknown\"!</font><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $name;
my $addr1;
my $addr2;
my $city;
my $county;
my $postal_cd;
my $country;
my $customer_type_id;
my $state;
my $update_count;

my $action = $PARAM{"action"};
if($action eq "Save")
{
	$name      = $PARAM{"name"};
	$addr1     = $PARAM{"addr1"};
	$addr2     = $PARAM{"addr2"};
	$city      = $PARAM{"city"};
	$county    = $PARAM{"county"};
	$postal_cd = $PARAM{"postal_cd"};
	$country   = $PARAM{"country"};
	$state     = $PARAM{"state"};
	$customer_type_id = $PARAM{"customer_type_id"};
	
	my $update_customer_stmt = $dbh->prepare("update customer set customer_name=:name, customer_addr1=:addr1, customer_addr2=:addr2, customer_city=:city, customer_county=:county, customer_postal_cd=:postal_cd, customer_country_cd=:country, customer_type_id=:customer_type_id, customer_state_cd=:state where customer_id = :customer_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$update_customer_stmt->bind_param(":name", $name);
	$update_customer_stmt->bind_param(":addr1", $addr1);
	$update_customer_stmt->bind_param(":addr2", $addr2);
	$update_customer_stmt->bind_param(":city", $city);
	$update_customer_stmt->bind_param(":county", $county);
	$update_customer_stmt->bind_param(":postal_cd", $postal_cd);
	$update_customer_stmt->bind_param(":country", $country);
	$update_customer_stmt->bind_param(":customer_type_id", $customer_type_id);
	$update_customer_stmt->bind_param(":state", $state);
	$update_customer_stmt->bind_param(":customer_id", $customer_id);
	$update_count = $update_customer_stmt->execute();
	$update_customer_stmt->finish();
	
	$dbh->disconnect;
	print "Location: edit_customer.cgi?customer_id=$customer_id\n";
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	exit();	
}
elsif($action eq "Delete")
{
	my $check_delete_stmt = $dbh->prepare("select count(1) from pos where customer_id = :customer_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$check_delete_stmt->bind_param(":customer_id", $customer_id);
	$check_delete_stmt->execute();
	my @count_data = $check_delete_stmt->fetchrow_array();
	$check_delete_stmt->finish();
	if(!@count_data)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	    print $user_menu;
		print "check_delete_stmt failed!\n";
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
	else
	{
		my $customer_count = $count_data[0];
		if($customer_count > 0)
		{
			USAT::DeviceAdmin::UI::DAHeader->printHeader();
			USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	        print $user_menu;
			print "Can not delete this customer because there are $customer_count devices currently assigned to it.  <br>Please reassign all the devices to a different customer then retry.\n";
			$dbh->disconnect;
			USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
			exit;
		}

		my $update_customer_stmt = $dbh->prepare("update customer set customer_active_yn_flag = 'N' where customer_id =  :customer_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$update_customer_stmt->bind_param(":customer_id", $customer_id);
		$update_count = $update_customer_stmt->execute();
		$update_customer_stmt->finish();
		$dbh->disconnect;
		
		print "Location: customer_menu.cgi\n";
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		exit();	
	}
}
elsif($action eq "Undelete")
{
	my $update_customer_stmt = $dbh->prepare("update customer set customer_active_yn_flag = 'Y' where customer_id =  :customer_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$update_customer_stmt->bind_param(":customer_id", $customer_id);
	$update_count = $update_customer_stmt->execute();
	$update_customer_stmt->finish();
	$dbh->disconnect;
	
	print "Location: customer_menu.cgi\n";
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;
print "No Action Defined!";
$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
