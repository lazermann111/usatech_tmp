#!/usr/local/USAT/bin/perl

use strict;

use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::USAPopups;
use OOCGI::OOCGI;
#use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

use warnings;	#optional

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print q{
<link rel="stylesheet" type="text/css" media="all" href="css/calendar/calendar-blue2.css" title="blue" />
<script language="JavaScript" src="/js/calendar/calendar.js"></script>
<script language="JavaScript" src="/js/calendar/calendar-en.js"></script>
<script language="JavaScript" src="/js/calendar/calendar-setup.js"></script>
<script language="JavaScript" src="/js/calendar/swap_dates.js"></script>
};

#my $colorTable = OOCGI::NTable->new('border=1');
#$colorTable->put(0,0,'Green','bgcolor="lightgreen"');
#my $txtGreen = OOCGI::Text->new(name => 'green_top_limit', value =>  60, sz_ml => [6,8]);
#my $txtGold  = OOCGI::Text->new(name => 'gold_top_limit',  value => 120, sz_ml => [6,8]);
#$colorTable->put(0,0,'Green','bgcolor="lightgreen"');
#$colorTable->put(0,1,$txtGreen);
#$colorTable->put(0,2,'Orange','bgcolor="gold"');
#$colorTable->put(0,3,$txtGold);
#$colorTable->put(0,4,'Red','bgcolor="tomato"');

my $sql;
$sql = USAT::DeviceAdmin::UI::USAPopups->sql_terminal_id;
my $popTerminalID = OOCGI::Popup->new( name  => 'terminal_id', sql => $sql,
                                style => "font-family: courier; font-size: 12px;",
                                align => 'yes', delimiter => '.' );
$popTerminalID->head('');
$popTerminalID->default('');

$sql = USAT::DeviceAdmin::UI::USAPopups->sql_authority_id;
my $popAuthorityID = OOCGI::Popup->new( name => 'authority_id', sql => $sql, 
                                style => "font-family: courier; font-size: 12px;",
                                align => 'yes', delimiter => '.' );
$popAuthorityID->head('');
$popAuthorityID->default('');

$sql = USAT::DeviceAdmin::UI::USAPopups->sql_merchant_id;
my $popMerchantID = OOCGI::Popup->new( name => 'merchant_id', sql => $sql,
                                style => "font-family: courier; font-size: 12px;",
                                align => 'yes', delimiter => '.' );
$popMerchantID->head('');
$popMerchantID->default('');

print qq|
<table cellspacing="0" cellpadding="2" border=1 width="100%">

 <tr>
  <th colspan="4" bgcolor="#C0C0C0">POSM Transaction Admin</th>
 </tr>
 <tr>
  <th colspan="2" bgcolor="#C0C0C0">Batch Search</th>
 </tr>
 <form name="form1" method="get" action="batch_list.cgi">
 <tr>
  <td>Terminal:</td>
  <td>
  $popTerminalID
  </td>
 </tr>
 <tr>
  <td>Batch Number:</td>
  <td><input type="text" name="terminal_batch_num"></td>
 </tr>
 <tr>
  <td>State</td>
  <td nowrap>
   <input type="radio" name="state" value="open">Open
   <input type="radio" name="state" value="closed">Closed
   <input type="radio" name="state" value="" checked>Either
   <table border=1 >
   <tr>
   <td   bgcolor="lightgreen" bgcolor="lightgreen">Green</td>
   <td  ><input type="text" name="green_top_limit" tabindex="3" value="60" size="6" maxlength="8" /></td>
   <td  bgcolor="gold">Orange</td>
   <td  ><input type="text" name="gold_top_limit" tabindex="5" value="120" size="6" maxlength="8" /></td>
   <td  bgcolor="tomato">Red</td>
   </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td>Authority:</td>
  <td>
   $popAuthorityID
   </select>
  </td>
 </tr>
 <tr>
  <td>Batch Open Date:</td>
  <td>
   <table cellspacing="0" cellpadding="0">
    <tr><td>From: </td><td><input type="text" id="11" name="open_after">
    <img src="/img/calendar/calendar.gif" id="open_after_trigger" style="cursor: pointer; border: 1px solid red;" title="Date selector"
      onmouseover="this.style.background='lightgreen';" onmouseout="this.style.background='yellow'" />
    </td></tr>
    <tr><td>To: &nbsp;</td><td><input id="12" type="text" name="open_before">
    <img src="/img/calendar/calendar.gif" id="open_before_trigger" style="cursor: pointer; border: 1px solid red;" title="Date selector"
      onmouseover="this.style.background='lightgreen';" onmouseout="this.style.background='yellow'" />
    </td></tr>
   </table>
  </td>
 </tr>
 <tr>
  <td>Batch Close Date:</td>
  <td>
   <table cellspacing="0" cellpadding="0">
    <tr><td>From: </td><td><input type="text" id="21" name="close_after">
    <img src="/img/calendar/calendar.gif" id="close_after_trigger" style="cursor: pointer; border: 1px solid red;" title="Date selector"
      onmouseover="this.style.background='lightgreen';" onmouseout="this.style.background='yellow'" />
    </td></tr>
    <tr><td>To: &nbsp;</td><td><input type="text" id="22" name="close_before">
    <img src="/img/calendar/calendar.gif" id="close_before_trigger" style="cursor: pointer; border: 1px solid red;" title="Date selector"
      onmouseover="this.style.background='lightgreen';" onmouseout="this.style.background='yellow'" />
    </td></tr>
   </table>
  </td>
 </tr>
 <tr>
  <td colspan="2" align="center">
   <input type="submit" name="action" value="List Batches">
  </td>
 </tr>
 </form>
 <tr>
  <th colspan="2" bgcolor="#C0C0C0">Terminal Search</th>
 </tr>
 <form method="GET" action="terminal_list.cgi">
 <tr>
  <td>Authority:</td>
  <td>
   $popAuthorityID
  </td>
 </tr>
 <tr>
  <td>Merchant:</td>
  <td>
   $popMerchantID
  </td>
 </tr>
 <tr>
  <td colspan="2" align="center">
   <input type="submit" name="action" value="List Terminals">
  </td>
 </tr>
 </form>
 <tr>
  <th colspan="2" bgcolor="#C0C0C0">Tran Search</th>
 </tr>
 <form method="GET" name="tran" action="tran_list.cgi">
 <tr>
  <td>Tran ID:</td>
  <td>
   <input type="text" name="tran_id" value="">
   <input type="button" name="button" value="Go To The Record Directly >>" onclick="javascript:window.location = '/tran.cgi?tran_id='+document.tran.tran_id.value;"></button> 
  </td>
 </tr>
 <tr>
  <td>Date:</td>
  <td>
   <table cellspacing="0" cellpadding="0">
    <tr><td>From: </td><td><input type="text" id="31" name="tran_after_date">
    <img src="/img/calendar/calendar.gif" id="tran_after_trigger" style="cursor: pointer; border: 1px solid red;" title="Date selector"
      onmouseover="this.style.background='lightgreen';" onmouseout="this.style.background='yellow'" /></td></tr>
    <tr><td>To: &nbsp;</td><td><input type="text" id="32" name="tran_before_date">
    <img src="/img/calendar/calendar.gif" id="tran_before_trigger" style="cursor: pointer; border: 1px solid red;" title="Date selector"
      onmouseover="this.style.background='lightgreen';" onmouseout="this.style.background='yellow'" /></td></tr>
   </table>
  </td>
 </tr>
 <tr>
  <td colspan="2" align="center">
   <input type="submit" name="myaction" value="List Trans">
  </td>
 </tr>
 </form> 
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "11",                  // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "open_after_trigger",  // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    swap_dates
    });
    Calendar.setup({
        inputField     :    "12",                   // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "open_before_trigger",  // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    swap_dates
    });
    Calendar.setup({
        inputField     :    "21",                   // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "close_after_trigger",  // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    swap_dates
    });
    Calendar.setup({
        inputField     :    "22",                    // id of the input field
        ifFormat       :    "%m/%d/%Y",              // format of the input field
        button         :    "close_before_trigger",  // trigger for the calendar (button ID)
        align          :    "B2",                    // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    swap_dates
    });
    Calendar.setup({
        inputField     :    "31",                   // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "tran_after_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    swap_dates
    });
    Calendar.setup({
        inputField     :    "32",                    // id of the input field
        ifFormat       :    "%m/%d/%Y",              // format of the input field
        button         :    "tran_before_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                    // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    swap_dates
    });
</script>

</table>
|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
