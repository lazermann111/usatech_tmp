#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::Util;
use USAT::Common::Const;
use OOCGI::Query;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

my %file_type_names = USAT::DeviceAdmin::UI::USAPopups->hash_file_transfer_type;


my %transfer_direction = (	'I'	=>	'Client to Server',
							'O'	=>	'Server to Client',
						);

my %transfer_status = (		'0'	=>	'Incomplete',
							'1'	=>	'Complete',
						);

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $file_id = $PARAM{"file_id"};
if(length($file_id) == 0)
{
	print "Required Parameter Not Found: file_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $file_info_sql = <<FILE_INFO_SQL;
   SELECT file_transfer_name,
          file_transfer_type_cd,
          to_char(created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
          to_char(last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
          file_transfer_comment
     FROM file_transfer
    WHERE file_transfer_id = ?
 ORDER BY file_transfer_type_cd
FILE_INFO_SQL

my $qo1 = OOCGI::Query->new($file_info_sql, { bind => [ $file_id ] } );

my $file_name;
my $file_type;
my $create_date;
my $last_updated;
my $comment;

if(my @file_data  = $qo1->next_array) {
    $file_name    = $file_data[0];
    $file_type    = $file_data[1];
    $create_date  = $file_data[2];
    $last_updated = $file_data[3];
    $comment      = $file_data[4];
} else {
	print "File not found!";
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

print <<TABLE1;
<table border="1" width="100%" cellpadding="2" cellspacing="0">
 <tr>
  <th colspan="4" bgcolor="#C0C0C0">File Details</th>
 </tr>
 <tr>
  <td nowrap>File ID</td>
  <td width="50%">$file_id&nbsp;</td>
  <td nowrap>Comment</td>
  <td width="50%">$comment&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>File Name</td>
  <td width="50%">$file_name&nbsp;</td>
  <td nowrap>Create Date</td>
  <td width="50%">$create_date&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>File Type</td>
  <td width="50%">$file_type_names{$file_type}&nbsp;</td>
  <td nowrap>Last Updated</td>
  <td width="50%">$last_updated&nbsp;</td>
 </tr>
</table>
TABLE1

print qq{
<hr width="100%" noshade size="2" color="#000000">

<table cellspacing="0" cellpadding="5" border="1" width="100%">
 <tr>
  <th bgcolor="#C0C0C0" colspan="3">Functions</th>
 </tr>
 <form method="post" action="file_details_func.cgi">
 <input type="hidden" name="file_id" value="$file_id">};
if($file_type eq PKG_FILE_TYPE__CONFIGURATION_FILE ||
   $file_type eq PKG_FILE_TYPE__PROPERTY_LIST ||
   $file_type eq PKG_FILE_TYPE__EDGE_DEFAULT_CONFIGURATION_TEMPLATE ||
   $file_type eq PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE) {
   print q{
    <tr>
     <td align="center" width="50%"><input type="submit" name="action" value="Download File">
     <td align="center" width="50%"><input type="submit" name="action" value="Upload to a Device">
    </tr>};
} else {
   print q{
    <tr>
     <td align="center" width="33%"><input type="submit" name="action" value="Edit File">
     <td align="center" width="33%"><input type="submit" name="action" value="Download File">
     <td align="center" width="33%"><input type="submit" name="action" value="Upload to a Device">
    </tr>};
}

print q{
 </form>
</table>
};

print <<TABLEH;
<hr width="100%" noshade size="2" color="#000000">

<table cellspacing="0" cellpadding="5" border="1" width="100%">
 <tr>
  <th bgcolor="#C0C0C0" colspan="7">Transfer Events</th>
 </tr>
 <tr>
  <th>Transfer ID</th>
  <th>Device</th>
  <th>Transfer Time</th>
  <th>Direction</th>
  <th>Status</th>
  <th>Group Number</th>
  <th>Packet Size</th>
 </tr>
TABLEH

my $list_transfers_sql = <<LIST_TRANSFERS_SQL;
   SELECT device_file_transfer.device_file_transfer_id,
          to_char(device_file_transfer.device_file_transfer_ts, 'MM/DD/YYYY HH:MI:SS AM'),
          device_file_transfer.device_file_transfer_direct,
          device_file_transfer.device_file_transfer_status_cd,
          device_file_transfer.device_file_transfer_group_num,
          device_file_transfer.device_file_transfer_pkt_size,
          device_file_transfer.device_id,
          device.device_name
     FROM device_file_transfer,
          device
    WHERE device_file_transfer.device_id = device.device_id
      AND device_file_transfer.file_transfer_id = ?
 ORDER BY device_file_transfer.created_ts desc
LIST_TRANSFERS_SQL

my $qo2 = OOCGI::Query->new($list_transfers_sql, { bind => [ $file_id ] });
while (my @data = $qo2->next_array) 
{
print <<ROWS;
   <tr><td>$data[0]&nbsp;</td>
       <td><a href=\"profile.cgi?ev_number=$data[7]\">$data[7]</a>&nbsp;</td>
       <td>$data[1]&nbsp;</td>
       <td>$transfer_direction{$data[2]}&nbsp;</td>
       <td>$transfer_status{$data[3]}&nbsp;</td>
       <td>$data[4]&nbsp;</td>
       <td>$data[5]&nbsp;</td>
   </tr>
ROWS
}

print '</table>';

my $file_content = USAT::DeviceAdmin::Util::blob_select(undef, $file_id);
if($file_type ne PKG_FILE_TYPE__MEMORY_MAP_CSV) { 
   $file_content = substr(pack("H*", $file_content),0, 10240);
}

#  <textarea rows=\"5\" cols=\"90\" wrap=\"off\">" . substr(pack("H*", $file_content), 0, 10240) . "</textarea><br>";
print <<AREA;
<hr width="100%" noshade size="2" color="#000000">

<table cellspacing="0" cellpadding="5" border="1" width="100%">
 <tr>
  <th bgcolor="#C0C0C0" colspan="1">File Content Preview</th>
 </tr>
 <tr>
  <td align="center">
   <textarea rows="5" cols="90" wrap="off">$file_content</textarea><br>
AREA

if($file_type eq PKG_FILE_TYPE__MEMORY_MAP_CSV) { 
   if($file_content =~ /\r\n/)
   {
   	  print "Windows Line Breaks<br>\n";
   } else {
      print "Unix Line Breaks<br>\n";
   }
} else {
   if(pack("H*",$file_content) =~ /\r\n/)
   {
   	  print "Windows Line Breaks<br>\n";
   } else {
      print "Unix Line Breaks<br>\n";
   }
}
   
print '<br></td></tr><br></table>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
