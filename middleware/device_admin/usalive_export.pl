#!/usr/local/USAT/bin/perl

require("slw-lib.pl");
#use DBI;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};
#$ENV{ORACLE_HOME} = "/opt/OraHome1";
#my $dbh = DBI->connect("DBI:Oracle:usadbd03","web_user","web_user",{PrintError => 1, RaiseError => 1, AutoCommit => 1}) or print "<br><br>Couldn't connect to database: " . DBI->errstr . "<br><br>";

sub usalive_export_location_by_device_id
{
	my ($device_id) = @_;

	my $ftype = 'SYNC';
	my $timestamp = time();
	
	my ($pathdat, $pathlog, $fname, $ext) = ("/opt/ReRixEngine3/ReRix/G4Export/data/$ftype/",
						"/opt/ReRixEngine3/ReRix/G4Export/logs/",
						$timestamp, 
						".new");
	
	my $get_device_info_stmt = $get_device_info_stmt = $dbh->prepare("select device.device_serial_cd, location.location.location_name, device.device_type_id from device, pos, location.location where device.device_id = pos.device_id and pos.location_id = location.location.location_id and device.device_active_yn_flag = 'Y' and device.device_id = :device_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_device_info_stmt->bind_param(":device_id", $device_id);
	$get_device_info_stmt->execute();
	
	my @device_data = $get_device_info_stmt->fetchrow_array();
	if(not defined @device_data)
	{
		$get_device_info_stmt->finish();
		&print_Header();
		&printFile("header.html");
		print "usalive_export_location_by_device_id($device_id) Failed!";
		&printFile("footer.html");
		exit;
	}
	
	$get_device_info_stmt->finish();
	
	my $serial_num = $device_data[0];
	my $location_name = $device_data[1];
	my $device_type = $device_data[2];
	
	if($device_type eq '0')
	{
		if (open (G4EXPORT, "> $pathdat$fname$ext"))
		{
			# write the record and immediately close the file
			print G4EXPORT "$serial_num;;$location_name\n";
			close G4EXPORT;
			
			# after closing the file change its extension to 'log' so the batching process 
			# will pick it up **after** the file is completely written and closed
			rename $pathdat . $fname . $ext, $pathdat . $fname . ".log";
			
			# log the result of the write
			my $logname = "g4export.log";
			if (open (G4LOG, ">> $pathlog$logname"))
			{
				printf G4LOG "[%s", localtime() . "] Successfully created $pathdat$fname.log\n";
				close G4LOG;
				#warn "Successfully created $pathdat$fname.log\n";
			}
			else
			{
				#warn "Could not write entry to g4export.log\n";
				&print_Header();
				&printFile("header.html");
				print "Could not write entry to g4export.log\n";
				&printFile("footer.html");
				exit;
			}	
		}
		else
		{
			#warn "An error occurred trying to create G4 Export file $pathdat$fname.$ext\n";
			&print_Header();
			&printFile("header.html");
			print "An error occurred trying to create G4 Export file $pathdat$fname$ext\n";
			&printFile("footer.html");
			exit;
		}
	}
}

sub usalive_export_location_by_serial_number
{
	my ($ssn) = @_;

	my $get_device_id_stmt = $get_device_id_stmt = $dbh->prepare("select device.device_id, device.device_type_id from device where device.device_active_yn_flag = 'Y' and device.device_serial_cd = :ssn") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_device_id_stmt->bind_param(":ssn", $ssn);
	$get_device_id_stmt->execute();
	
	my @device_data = $get_device_id_stmt->fetchrow_array();
	if(not defined @device_data)
	{
		$get_device_info_stmt->finish();
		&print_Header();
		&printFile("header.html");
		print "usalive_export_location_by_serial_number($ssn) Failed!";
		&printFile("footer.html");
		exit;
	}
	
	$get_device_id_stmt->finish();
	
	my $device_id = $device_data[0];
	my $device_type = $device_data[1];
	
	if($device_type eq '0')
	{
		&usalive_export_location_by_device_id($device_id);
	}
}

sub usalive_export_locations_by_serial_numbers
{
	my @serials = @_;

	my $ftype = 'SYNC';
	my $timestamp = time();
	my ($pathdat, $pathlog, $fname, $ext) = ("/opt/ReRixEngine3/ReRix/G4Export/data/$ftype/",
						"/opt/ReRixEngine3/ReRix/G4Export/logs/",
						$timestamp, 
						".new");
						
	my $lines;
	
	foreach $ssn (@serials)
	{
		my $get_device_info_stmt = $get_device_info_stmt = $dbh->prepare("select location.location.location_name, device.device_type_id from device, pos, location.location where device.device_id = pos.device_id and pos.location_id = location.location.location_id and device.device_active_yn_flag = 'Y' and device.device_serial_cd = :ssn") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$get_device_info_stmt->bind_param(":ssn", $ssn);
		$get_device_info_stmt->execute();
		
		my @device_data = $get_device_info_stmt->fetchrow_array();
		if(not defined @device_data)
		{
			$get_device_info_stmt->finish();
			&print_Header();
			&printFile("header.html");
			print "usalive_export_location_by_device_id($device_id) Failed!";
			&printFile("footer.html");
			exit;
		}
		
		$get_device_info_stmt->finish();
		my $location_name = $device_data[0];
		my $device_type = $device_data[1];
		
		if($device_type eq '0')
		{
			$lines = $lines . "$ssn;;$location_name\n"
		}
	}
	
	if (open (G4EXPORT, "> $pathdat$fname$ext"))
	{
		# write the record and immediately close the file
		print G4EXPORT $lines;
		close G4EXPORT;
		
		# after closing the file change its extension to 'log' so the batching process 
		# will pick it up **after** the file is completely written and closed
		rename $pathdat . $fname . $ext, $pathdat . $fname . ".log";
		
		# log the result of the write
		my $logname = "g4export.log";
		if (open (G4LOG, ">> $pathlog$logname"))
		{
			printf G4LOG "[%s", localtime() . "] Successfully created $pathdat$fname.log\n";
			close G4LOG;
			#warn "Successfully created $pathdat$fname.log\n";
		}
		else
		{
			#warn "Could not write entry to g4export.log\n";
			&print_Header();
			&printFile("header.html");
			print "Could not write entry to g4export.log\n";
			&printFile("footer.html");
			exit;
		}	
	}
	else
	{
		#warn "An error occurred trying to create G4 Export file $pathdat$fname.$ext\n";
		&print_Header();
		&printFile("header.html");
		print "An error occurred trying to create G4 Export file $pathdat$fname$ext\n";
		&printFile("footer.html");
		exit;
	}
}

sub usalive_export_locations_by_location_id
{
	my ($location_id) = @_;

	my $ftype = 'SYNC';
	my $timestamp = time();
	my ($pathdat, $pathlog, $fname, $ext) = ("/opt/ReRixEngine3/ReRix/G4Export/data/$ftype/",
						"/opt/ReRixEngine3/ReRix/G4Export/logs/",
						$timestamp, 
						".new");
						
	my $lines;
	
	my $get_device_info_stmt = $get_device_info_stmt = $dbh->prepare("select device.device_serial_cd, location.location.location_name from device, pos, location.location where device.device_id = pos.device_id and pos.location_id = location.location.location_id and device.device_active_yn_flag = 'Y' and location.location.location_id = :location_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_device_info_stmt->bind_param(":location_id", $location_id);
	$get_device_info_stmt->execute();
	
	while(my @device_data = $get_device_info_stmt->fetchrow_array())
	{
		my $ssn = $device_data[0];
		my $location_name = $device_data[1];
		$lines = $lines . "$ssn;;$location_name\n"
	}
	
	$get_device_info_stmt->finish();

	if (open (G4EXPORT, "> $pathdat$fname$ext"))
	{
		# write the record and immediately close the file
		print G4EXPORT $lines;
		close G4EXPORT;
		
		# after closing the file change its extension to 'log' so the batching process 
		# will pick it up **after** the file is completely written and closed
		rename $pathdat . $fname . $ext, $pathdat . $fname . ".log";
		
		# log the result of the write
		my $logname = "g4export.log";
		if (open (G4LOG, ">> $pathlog$logname"))
		{
			printf G4LOG "[%s", localtime() . "] Successfully created $pathdat$fname.log\n";
			close G4LOG;
			#warn "Successfully created $pathdat$fname.log\n";
		}
		else
		{
			#warn "Could not write entry to g4export.log\n";
			&print_Header();
			&printFile("header.html");
			print "Could not write entry to g4export.log\n";
			&printFile("footer.html");
			exit;
		}	
	}
	else
	{
		#warn "An error occurred trying to create G4 Export file $pathdat$fname.$ext\n";
		&print_Header();
		&printFile("header.html");
		print "An error occurred trying to create G4 Export file $pathdat$fname$ext\n";
		&printFile("footer.html");
		exit;
	}
}