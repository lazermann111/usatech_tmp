#!/usr/local/USAT/bin/perl

use strict;
use warnings;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::DeviceAdmin::DBObj;
use USAT::POS::Const qw(:PKG_POS_PTA_GLOBALS);
use Date::Calc;
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;

use constant DEBUG => 0;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu =  $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

if(defined $PARAM{action} && $PARAM{action} eq 'Save Clone') {
   my $pos_pta_tmpl = USAT::DeviceAdmin::DBObj::Pos_pta_tmpl->new();
   $pos_pta_tmpl->pos_pta_tmpl_name($PARAM{clone_name});
   $pos_pta_tmpl->pos_pta_tmpl_desc($PARAM{clone_desc});
   $pos_pta_tmpl = $pos_pta_tmpl->insert;
   my $PPT_ID = $pos_pta_tmpl->ID;

   if($PPT_ID > 0) {
      my $sql = "SELECT * FROM pos_pta_tmpl_entry WHERE pos_pta_tmpl_id = $PARAM{pos_pta_tmpl_id}";
      my @sources= USAT::DeviceAdmin::DBObj::Pos_pta_tmpl_entry->objects($sql);
      foreach my $source ( @sources ) {
         my $clone  = USAT::DeviceAdmin::DBObj::Pos_pta_tmpl_entry->new();
         $clone->pos_pta_tmpl_id($PPT_ID);
         $clone->payment_subtype_id($source->payment_subtype_id);
         $clone->pos_pta_encrypt_key($source->pos_pta_encrypt_key);
         $clone->pos_pta_activation_oset_hr($source->pos_pta_activation_oset_hr);
         $clone->pos_pta_encrypt_key($source->pos_pta_encrypt_key);
         $clone->pos_pta_activation_oset_hr($source->pos_pta_activation_oset_hr);
         $clone->pos_pta_deactivation_oset_hr($source->pos_pta_deactivation_oset_hr);
         $clone->payment_subtype_key_id($source->payment_subtype_key_id);
         $clone->pos_pta_pin_req_yn_flag($source->pos_pta_pin_req_yn_flag);
         $clone->pos_pta_device_serial_cd($source->pos_pta_device_serial_cd);
         $clone->pos_pta_encrypt_key2($source->pos_pta_encrypt_key2);
         $clone->authority_payment_mask_id($source->authority_payment_mask_id);
         $clone->pos_pta_priority($source->pos_pta_priority);
         $clone->terminal_id($source->terminal_id);
         $clone->merchant_bank_acct_id($source->merchant_bank_acct_id);
         $clone->currency_cd($source->currency_cd);
         $clone->pos_pta_passthru_allow_yn_flag($source->pos_pta_passthru_allow_yn_flag);
         $clone->pos_pta_pref_auth_amt($source->pos_pta_pref_auth_amt);
         $clone->pos_pta_pref_auth_amt_max($source->pos_pta_pref_auth_amt_max);
         $clone = $clone->insert;
      }
   }

   print CGI->redirect("/template_menu.cgi");
   exit();
}

our $dbh;
&main();

sub main 
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
	$dbh = $DATABASE->{handle};
	
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

    print $user_menu;

	print <<"	EOHTML";
	  <script language="JavaScript" type="text/javascript">
	  <!--
		function confirmSubmit() {
			var agree=confirm("Are you sure you want to continue with this operation?");
			if (agree)
				return true;
			else
				return false;
		}
        function validateForm() {
           var tlength;
           if (!(document.all.pos_pta_tmpl_entry_id === undefined)) {
               tlength = document.all.pos_pta_tmpl_entry_id.length;
               if (tlength === undefined) {
                   tlength = 1;
               }
           }
           for(var i = 0; i < tlength; i++) {
              if(document.getElementById('radio_'+i).checked) {
                 return true;
              }
           }
           alert("Please select a radio box from Edit column");
           return false;
        }
	  // -->
	  </script>
	EOHTML
	
	print Dumper(\%PARAM) if DEBUG;

    my $action = defined $PARAM{action} && length $PARAM{action} > 0 ? $PARAM{action} : "View";
	if($action eq 'New')
	{
		print "
		<table cellspacing=\"0\" cellpadding=\"1\" border=\"1\" width=\"100%\">
		 <tr>
		  <th colspan=\"8\" bgcolor=\"#C0C0C0\">Payment Type Template Admin</th>
		 </tr>
		 <tr>
		  <th colspan=\"8\" bgcolor=\"#C0C0C0\">New Template</th>
		 </tr>
		  <form method=\"post\" action=\"edit_pos_pta_tmpl_func.cgi\">
		 <tr>
		  <td>Name</td>
		  <td><input type=\"text\" name=\"name\" size=\"30\"></td>
		 </tr>
		 <tr>
		  <td>Description</td>
		  <td><input type=\"text\" name=\"desc\" size=\"60\"></td>
		 </tr>
		 <tr>
		  <td>&nbsp;</td>
		  <td><input type=\"submit\" name=\"action\" value=\"Save\">
		 </tr>
		</table>
		";
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}

    if($action eq 'Clone')
    {
        my $pos_pta_tmpl = USAT::DeviceAdmin::DBObj::Pos_pta_tmpl->new($PARAM{pos_pta_tmpl_id});
        my $name = 'Copy of '.$pos_pta_tmpl->pos_pta_tmpl_name;
        my $desc = $pos_pta_tmpl->pos_pta_tmpl_desc;
        print qq{
        <table cellspacing="0" cellpadding="1" border="1" width="100%">
         <tr>
          <th colspan="8" bgcolor="#C0C0C0">Payment Type Template Admin</th>
         </tr>
         <tr>
          <th colspan="8" bgcolor="#C0C0C0">Template Clone</th>
         </tr>
          <form method="post" action="edit_pos_pta_tmpl.cgi">
         <tr>
          <td>Template Name</td>
          <td><input type="text" name="clone_name" value="$name" size="60" maxsize="60"></td>
         </tr>
         <tr>
          <td>Description</td>
          <td><input type="text" name="clone_desc" value="$desc" size="80"></td>
         </tr>
         <tr>
          <td>&nbsp;</td>
          <td><input type="submit" name="action" value="Save Clone">
         </tr>
        </table>
        };
        HIDDEN('pos_pta_tmpl_id', $PARAM{pos_pta_tmpl_id});
        $dbh->disconnect;
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
    }

	my $pos_pta_tmpl_id = length $PARAM{pos_pta_tmpl_id} > 0 ? $PARAM{pos_pta_tmpl_id} : _print_error("Required Parameter Not Found: pos_pta_tmpl_id");
	
	my $err;
	my @template_data = USAT::DeviceAdmin::Profile::PaymentType::get_template_data($dbh, $pos_pta_tmpl_id, \$err);
	printError("<br><br>$err<br><br>") if defined $err;
	print Dumper(\@template_data)."-->" if DEBUG;
	printError("<br><h3>Template not found: $pos_pta_tmpl_id</h3><br><br>") unless scalar @template_data;
	
	my @payment_entry_methods = ();
	my %payment_entry_method_names = ();
	my $get_payment_entry_methods = $dbh->prepare(q{
		SELECT payment_entry_method_cd, payment_entry_method_desc
		FROM pss.payment_entry_method
		ORDER BY display_order
	}) or _print_error("Couldn't prepare statement: " . $dbh->errstr);
	$get_payment_entry_methods->execute() or _print_error("Couldn't execute statement: " . $dbh->errstr);
	while (my $row_aref = $get_payment_entry_methods->fetchrow_arrayref()) 
	{
		push @payment_entry_methods, $row_aref->[0];
		$payment_entry_method_names{$row_aref->[0]} = $row_aref->[1];
	}
	$get_payment_entry_methods->finish();
	
	print "
	<table cellspacing=\"0\" cellpadding=\"1\" border=\"1\" width=\"100%\">
	 <tr>
	  <th colspan=\"9\" bgcolor=\"#C0C0C0\">Payment Type Template Admin</th>
	 </tr>
	 <tr>
	  <th colspan=\"9\" bgcolor=\"#C0C0C0\">$template_data[1]</th>
	 </tr>
	  <form method=\"post\" action=\"edit_pos_pta_tmpl_func.cgi\">
	  <input type=\"hidden\" name=\"pos_pta_tmpl_id\" value=\"$pos_pta_tmpl_id\">
	 <tr>
	  <th bgcolor=\"#C0C0C0\">Edit</th>
	  <th bgcolor=\"#C0C0C0\">Category<br>Priority</th>
	  <th bgcolor=\"#C0C0C0\" nowrap>Payment Entry<br>Method Category</th>
	  <th bgcolor=\"#C0C0C0\">Payment<br>Type Name</th>
	  <th bgcolor=\"#C0C0C0\">Merchant</th>
	  <th bgcolor=\"#C0C0C0\">Activation<br>Time</th>
	  <th bgcolor=\"#C0C0C0\">Deactivation<br>Time</th>
	  <th bgcolor=\"#C0C0C0\" nowrap>Auth<br>Pass-thru</th>
	  <th bgcolor=\"#C0C0C0\" nowrap>Auth<br>Override</th>
	 </tr>
	";
	
	my $last_payment_entry_method_cd;
    my $loop_id = 0;
	foreach my $current_payment_entry_method_cd (@payment_entry_methods)
	{
		my $payment_entry_method_name = $payment_entry_method_names{$current_payment_entry_method_cd};
		
		print "<tr><td colspan=\"9\" bgcolor=\"#C0C0C0\"><img src=\"pixel.gif\" width=\"1\" height=\"1\" border=\"0\"></td></tr>\n" if $last_payment_entry_method_cd && $last_payment_entry_method_cd ne $current_payment_entry_method_cd;
		
		my $get_pos_pta_stmt = $dbh->prepare(q{
			SELECT pp.pos_pta_tmpl_entry_id, 
				ps.payment_subtype_name, 
				pp.pos_pta_activation_oset_hr,
				pp.pos_pta_deactivation_oset_hr,
				pem.payment_entry_method_cd,
				pem.payment_entry_method_desc,
				NVL(pp.pos_pta_priority, 999),
				ps.payment_subtype_table_name,
				ps.payment_subtype_key_name,
				pp.payment_subtype_key_id,
				pp.currency_cd,
				pp.terminal_id,
				pp.authority_payment_mask_id,
				pp.pos_pta_passthru_allow_yn_flag,
				pp.pos_pta_pref_auth_amt,
				pp.pos_pta_pref_auth_amt_max,
				pt.payment_type_desc
			FROM pss.pos_pta_tmpl_entry pp, pss.payment_subtype ps, pss.client_payment_type cpt, pss.payment_entry_method pem, pss.payment_type pt
			WHERE ps.payment_subtype_id = pp.payment_subtype_id
			AND ps.client_payment_type_cd = cpt.client_payment_type_cd
			AND cpt.payment_entry_method_cd = pem.payment_entry_method_cd
			AND cpt.payment_type_cd = pt.payment_type_cd
			AND pp.pos_pta_tmpl_id = :pos_pta_tmpl_id
			AND pem.payment_entry_method_cd = :payment_entry_method_cd
			ORDER BY pp.pos_pta_priority
		}) or _print_error("Couldn't prepare statement: " . $dbh->errstr);
		$get_pos_pta_stmt->bind_param(":pos_pta_tmpl_id", $pos_pta_tmpl_id);
		$get_pos_pta_stmt->bind_param(":payment_entry_method_cd", $current_payment_entry_method_cd);
		$get_pos_pta_stmt->execute() or _print_error("Couldn't execute statement: " . $dbh->errstr);
		my @pos_pta_data;
		while (my $row_aref = $get_pos_pta_stmt->fetchrow_arrayref()) {
			push @pos_pta_data, [@{$row_aref}];
		}
		$get_pos_pta_stmt->finish();
		
		my $payment_entry_method_count = scalar @pos_pta_data;
	
		if ($payment_entry_method_count == 0)
		{ 
			print "
			<tr>
			 <td>&nbsp;</td>
			 <td><input type=\"text\" size=\"3\"></td>
			 <td align=\"left\" valign=\"middle\" ><input type=\"radio\" name=\"payment_entry_method_cd\" value=\"$current_payment_entry_method_cd\">$payment_entry_method_name</td>
			 <td align=\"center\" colspan=\"6\">-</td>
			</tr>\n";
			next;
		}
	
		foreach my $pos_pta_data_ref (@pos_pta_data)
		{
			my $pos_pta_tmpl_entry_id = $pos_pta_data_ref->[0];
			my $pos_pta_tmpl_entry_name = uc($pos_pta_data_ref->[16]) . ': ' . $pos_pta_data_ref->[1];
			my $pos_pta_tmpl_entry_act = $pos_pta_data_ref->[2];
			my $pos_pta_tmpl_entry_deact = $pos_pta_data_ref->[3];
			my $current_payment_entry_method_cd = $pos_pta_data_ref->[4];
			my $pos_pta_tmpl_entry_pri = $pos_pta_data_ref->[6];
			my $pos_pta_tmpl_entry_passthru = $pos_pta_data_ref->[13] eq PKG_POS_PTA_GLOBALS__passthru_allow ? '<img src="/icons/down.gif">' : '';
			my $pos_pta_pref_auth_amt = $pos_pta_data_ref->[14];
			my $pos_pta_pref_auth_amt_max = $pos_pta_data_ref->[15];
			
			# lookup merchant description
			my $err_ref;
			my $merchant_desc = USAT::DeviceAdmin::Profile::PaymentType::_get_merchant_desc($dbh, $pos_pta_data_ref->[7], $pos_pta_data_ref->[8], $pos_pta_data_ref->[9], $err_ref);
			_print_error("Couldn't lookup merchant description: " . $$err_ref) if($err_ref);
			
			print qq{<tr>
					<td nowrap align="center"><input type="radio" id="radio_$loop_id" name="pos_pta_tmpl_entry_id" value="$pos_pta_tmpl_entry_id"></td>
					<td nowrap>
					 <input type="text" name="new_priority_$pos_pta_tmpl_entry_id" value="$pos_pta_tmpl_entry_pri" size="3">
					 <input type="hidden" name="old_priority_$pos_pta_tmpl_entry_id" value="$pos_pta_tmpl_entry_pri">
					 <input type="hidden" name="payment_entry_method_cd_$pos_pta_tmpl_entry_id" value="$current_payment_entry_method_cd">
					</td>
					};
					
					print "<td align=\"left\" valign=\"middle\" rowspan=\"$payment_entry_method_count\"><input type=\"radio\" name=\"payment_entry_method_cd\" value=\"$current_payment_entry_method_cd\">$payment_entry_method_name</td>\n"  if !$last_payment_entry_method_cd || $last_payment_entry_method_cd ne $current_payment_entry_method_cd;
					
					print "
					<td nowrap>$pos_pta_tmpl_entry_name&nbsp;</td>
					<td nowrap align=\"left\">$merchant_desc&nbsp;</td>
					<td nowrap>" . (!defined $pos_pta_data_ref->[2] ? "<font color=\"orange\">Inactive</font>" : ($pos_pta_data_ref->[2] == 0 ? "Now" : "Now plus $pos_pta_data_ref->[2] hours")) . "&nbsp;</td>
					<td nowrap>" . (!defined $pos_pta_data_ref->[3] ? "Never" : "Now plus $pos_pta_data_ref->[3] hours") . "&nbsp;</td>
					<td nowrap align=\"left\">$pos_pta_tmpl_entry_passthru&nbsp;</td>
					<td nowrap align=\"center\">" . (defined $pos_pta_pref_auth_amt ? "\$$pos_pta_pref_auth_amt/\$$pos_pta_pref_auth_amt_max" : "") . "&nbsp;</td>
				   </tr>";
				   
			$last_payment_entry_method_cd = $current_payment_entry_method_cd;
            $loop_id++;
		}
	}

	print qq{
	 <tr>
	  <td align="center" valign="top"><input type="submit" name="action" value="Edit" onClick="javascript:return validateForm();"></td>
	  <td align="center" valign="top"><input type="submit" name="action" value="Reorder" onClick="javascript:return confirmSubmit();"></td>
	  <td align="center" valign="top"><input type="submit" name="action" value="Add New"></td>
	  <td colspan="6" valign="top">
       <input type="text" name="name" value="$template_data[1]" size="50">
       <input type="text" name="desc" value="$template_data[2]" size="50">
       <input type="submit" name="action" value="Update" onClick="javascript:return confirmSubmit();"></td>
	  </td>
	 </tr>
	 </form>
	</table>\n};

	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub _print_error ($) 
{
	my $err_txt = shift;
	print "$err_txt\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

