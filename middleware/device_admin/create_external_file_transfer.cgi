#!/usr/local/USAT/bin/perl

use USAT::DeviceAdmin::DBObj;
use OOCGI::NTable;
use OOCGI::OOCGI;
use Time::Local;
use Net::SMTP;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Common::Const;

use strict;

#kludge: patch for Oracle 10.1.0.x session default date format bug, causing occasional ORA-01801 errors on some views and PL/SQL
OOCGI::Query->modify(q{ALTER SESSION SET NLS_DATE_FORMAT='dd-mon-yy'}) if OOCGI::Query::Config->new->driver =~ /Oracle$/;

my $file_encapsulations = [ '00','None (Raw File)',
							'01','Compressed using GZIP',
							'02','Compressed using GZIP, then encrypted using AES 128 with CBC',
							'03','Encrypted using AES 128 with CBC',
							'04','Archived using TAR, then compressed using GZIP',
							'05','Encrypted using AES 128 without CBC'];
							
my $crc_types = ['00','CRC16',
				 '01','CRC32'];
				 
my $protocols = ['F','FTP (Active Mode)', 
				 'P','FTP (Passive Mode)',
				 'H','HTTP',
				 'S','HTTPS'];
				 
my $file_formats = ['00','G5 - FMM Binary Image',
					'01','Kiosk - Configuration File',
					'05','Kiosk - Application Upgrade',
					'07','Kiosk - Executable File',
					'08','Kiosk - Executable File with Restart',
					'09','Kiosk - Log File',
					'10','Kiosk - Generic File'];

my %file_encapsulations_hash = @{$file_encapsulations};
my %crc_types_hash = @{$crc_types};
my %protocols_hash = @{$protocols};
my %file_formats_hash = @{$file_formats};

my $query   = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars();

my $device_id   = $PARAM{device_id};
my $device_name = $PARAM{device_name};
my $data_type   = 'A9';

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
$session->destroy;

my $device;
if ($device_id) {
	$device = USAT::DeviceAdmin::DBObj::Device->new($device_id);
	$device_name = $device->device_name;
} elsif ($device_name) {
	$device = USAT::DeviceAdmin::DBObj::Vw_device_last_active->new({device_name => $device_name});
	$device_id = $device->device_id;
} else {
	if(!$device_name)
	{
        BR(4);
		B("Required Parameter Not Found: device_name",'red',2);
        BR(4);
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}

	if(!$device_id)
	{
        BR(4);
		B("Required Parameter Not Found: device_id",'red',2);
        BR(4);
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
}
if (!$device->device_id) {
    BR(4);
	B("No device found for device_id=$device_id, device_name=$device_name",'red',2);
    BR(4);
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if($device->device_type_id eq PKG_DEVICE_TYPE__EDGE) {
    BR(4);
	B("Edge device $device_name does not support this operation",'red',2);
    BR(4);
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $T;

print q|<form method="post">|;
print qq|<input type="hidden" name="device_name" value="$device_name">|;
print qq|<input type="hidden" name="device_id" value="$device_id">|;

if($PARAM{userOP} eq 'Save')
{
	$T = save();
}
else
{
	$T = display();
}

display $T;

print q|</form>|;
	
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub save
{	
	my @errors;

	my $protocol = $PARAM{protocol};
	my $ip_address = $PARAM{ip_address};					 
	my $port = $PARAM{port};
	my $login_name = $PARAM{login_name};					
	my $login_password = $PARAM{login_password};
	my $file_path = $PARAM{file_path};
	my $file_name = $PARAM{file_name};
	my $file_format = $PARAM{file_format};					
	my $file_encapsulation = $PARAM{file_encapsulation};
	my $crc_type = $PARAM{crc_type};
	my $crc = $PARAM{crc};
	my $encryption_key = $PARAM{encryption_key};
	my $transfer_start = $PARAM{transfer_start};
	my $transfer_end = $PARAM{transfer_end};
	my $retries = chr($PARAM{retries});
	my $retry_interval = chr($PARAM{retry_interval});
	my $confirm_email = $PARAM{confirm_email};
	my $client_file_path = $PARAM{client_file_path};
	
	push @errors, "Required field not found: device_name" unless(defined $device_name);
	push @errors, "Required field not found: protocol" unless(defined $protocol);
	push @errors, "Required field not found: ip_address" unless(defined $ip_address);
	push @errors, "Required field not found: port" unless(defined $port);
	push @errors, "Required field not found: login_name" unless(defined $login_name);
	push @errors, "Required field not found: login_password" unless(defined $login_password);
	push @errors, "Required field not found: file_path" unless(defined $file_path);
	push @errors, "Required field not found: file_name" unless(defined $file_name);
	push @errors, "Required field not found: file_format" unless(defined $file_format);
	push @errors, "Required field not found: file_encapsulation" unless(defined $file_encapsulation);
	push @errors, "Required field not found: crc_type" unless(defined $crc_type);
	push @errors, "Required field not found: crc" unless(defined $crc);
	push @errors, "Required field not found: encryption_key" unless(defined $encryption_key || $file_encapsulation =~ /^(00|01|04)$/);
	push @errors, "Required field not found: transfer_start" unless(defined $transfer_start);
	push @errors, "Required field not found: transfer_end" unless(defined $transfer_end);
	push @errors, "Required field not found: retries" unless(defined $retries);
	push @errors, "Required field not found: retry_interval" unless(defined $retry_interval);
	
	if($device->device_type_id =~ /^(0|1)$/)
	{
		push @errors, "Field failed G4/G5 length validation: login_name" unless $login_name =~ /^.{1,16}$/;
		push @errors, "Field failed G4/G5 length validation: login_password" unless $login_password =~ /^.{1,16}$/;
		push @errors, "Field failed G4/G5 length validation: file_path" unless $file_path =~ /^.{1,48}$/;
		push @errors, "Field failed G4/G5 length validation: file_name" unless $file_name =~ /^.{1,48}$/;
	}
	elsif($device->device_type_id =~ /^(11)$/)
	{
		push @errors, "Required field not found: client_file_path" unless(defined $client_file_path);
		push @errors, "Field failed validation: client_file_path" unless $client_file_path =~ /^.{1,256}$/;
	}
	
	my $file_path_name = $file_path . $file_name;
	
	push @errors, "Field failed validation: protocol" unless $protocol =~ /^[A-Za-z]$/;
	push @errors, "Field failed validation: ip_address" unless $ip_address =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
	push @errors, "Field failed validation: port" unless $port =~ /^\d{1,5}$/;
	push @errors, "Field failed validation: login_name" unless $login_name =~ /^.{1,256}$/;
	push @errors, "Field failed validation: login_password" unless $login_password =~ /^.{1,256}$/;
	push @errors, "Field failed validation: file_path" unless $file_path =~ /^.{1,256}$/;
	push @errors, "Field failed validation: file_name" unless $file_name =~ /^.{1,256}$/;
	push @errors, "Field failed validation: file_path_name" unless $file_path_name =~ /^.{1,256}$/;
	push @errors, "Field failed validation: file_format" unless $file_format =~ /^[0-9a-zA-Z]{2}$/;
	push @errors, "Field failed validation: file_encapsulation" unless $file_encapsulation =~ /^[0-9a-zA-Z]{2}$/;
	push @errors, "Field failed validation: crc_type" unless $crc_type =~ /^[0-9a-zA-Z]{2}$/;
	push @errors, "Field failed validation: crc" unless $crc =~ /^[0-9a-fA-F]{4,8}$/;
	push @errors, "Field failed validation: encryption_key" unless $encryption_key =~ /^[0-9a-fA-F]{2,2048}$/ || $file_encapsulation =~ /^(00|01|04)$/;
	push @errors, "Field failed validation: retries" unless ord($retries) =~ /^\d{1,3}$/;
	push @errors, "Field failed validation: retry_interval: " . ord($retry_interval) unless ord($retry_interval) =~ /^\d{1,3}$/;
	
	push @errors, "CRC value does not correspond to type" if($crc_type eq '00' && length($crc) != 4);
	push @errors, "CRC value does not correspond to type" if($crc_type eq '01' && length($crc) != 8);
	
	push @errors, "Invalid crc hex value" unless length($crc) % 2 == 0;
	push @errors, "Invalid encryption_key hex value" unless length($encryption_key) % 2 == 0;
	
	my ($octet1, $octet2, $octet3, $octet4) = split(/\./,$ip_address);
	$octet1 = chr($octet1);
	$octet2 = chr($octet2);
	$octet3 = chr($octet3);
	$octet4 = chr($octet4);
	
	my @transfer_start_values = split(/:| |-/, $transfer_start);
	my @transfer_end_values = split(/:| |-/, $transfer_end);
	
	push @errors, "Field failed validation: transfer_start" unless(scalar @transfer_start_values == 6);
	push @errors, "Field failed validation: transfer_end" unless(scalar @transfer_end_values == 6);
	
	if(scalar @errors > 0)
	{
		my $T = OOCGI::NTable->new('border="0" cellpadding="2" cellspacing="0" ');
		my $rc = 0;
		foreach my $line(@errors)
		{
			$T->insert($rc++,0,B($line,"RED"));
		}
		
		return $T;
	}
	
	my $transfer_start_gmtime = Time::Local::timegm($transfer_start_values[5], $transfer_start_values[4], $transfer_start_values[3], $transfer_start_values[1], ($transfer_start_values[0] - 1), $transfer_start_values[2]);
	my $transfer_end_gmtime = Time::Local::timegm($transfer_end_values[5], $transfer_end_values[4], $transfer_end_values[3], $transfer_end_values[1], ($transfer_end_values[0] - 1), $transfer_end_values[2]);

	my $login_name_length = chr(length($login_name));
	my $login_password_length = chr(length($login_password));
	my $file_path_name_length = chr(length($file_path_name));
	my $encryption_key_length = chr(length($encryption_key)/2);
	
	my $message = pack("aaaaa",$protocol, $octet1, $octet2, $octet3, $octet4);
	$message .= pack("n", $port);
	$message .= pack("aa*", $login_name_length, $login_name);
	$message .= pack("aa*", $login_password_length, $login_password);
	$message .= pack("aa*", $file_path_name_length, $file_path_name);
	$message .= pack("H*", $file_format);
	$message .= pack("H*", $file_encapsulation);
	$message .= pack("H*", $crc_type);
	$message .= pack("H*", $crc);
	$message .= pack("a", $encryption_key_length);
	$message .= pack("H*", $encryption_key);
	$message .= pack("NN", $transfer_start_gmtime, $transfer_end_gmtime);
	$message .= pack("aa*", $retries, $retry_interval);
	
	if($device->device_type_id =~ /^(11)$/)
	{
		my $client_file_path_length = chr(length($client_file_path));
		$message .= pack("aa*", $client_file_path_length, $client_file_path);
	}
	
	if(length($message) > 250 && $device->device_type_id =~ /^(0|1)$/)
	{
		push @errors, "Total message is too long!";
		
		my $T = OOCGI::NTable->new('border="0" cellpadding="2" cellspacing="0" ');
		my $rc = 0;
		foreach my $line(@errors)
		{
			$T->insert($rc++,0,B($line,"RED"));
		}
		
		return $T;
	}
	
	my $update_sql = q|UPDATE machine_cmd_pending set execute_cd = 'C' where machine_id = ? and data_type = ?|;
	my $update_result = OOCGI::Query->modify($update_sql, [$device_name, $data_type]);
	
	my $mcp = USAT::DeviceAdmin::DBObj::Machine_cmd_pending->new;
	$mcp->machine_id($device_name);
	$mcp->data_type($data_type);
	$mcp->command(unpack("H*",$message));
	$mcp->execute_cd('P');
	$mcp->execute_order('1');
	$mcp = $mcp->insert;

	my $messageText = OOCGI::Text->new(name=>'message', value=>unpack("H*", $message), size=>60);
	
	my $rn = 0;
	
	my $T = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
	$T->insert($rn,0,B("Creating new pending External File Transfer Request..."), 'colspan=2 align=center');
	$rn++;
	
	$T->insert($rn,0,"Machine ID");
	$T->insert($rn,1,$device_name);
	$rn++;
	
	my ($create_second, $create_minute, $create_hour, $create_day, $create_month, $create_year) = localtime();
	my $createStr = sprintf("%02d-%02d-%04d %02d:%02d:%02d", ($create_month + 1), $create_day, ($create_year + 1900), $create_hour, $create_minute, $create_second);

	$T->insert($rn,0,"Create Time");
	$T->insert($rn,1,$createStr);
	$rn++;

	$T->insert($rn,0,"Data Type");
	$T->insert($rn,1,$data_type);
	$rn++;
	
	$T->insert($rn,0,"Command");
	$T->insert($rn,1,$messageText);
	$rn++;
	
	$T->insert($rn,0,"Command Length");
	$T->insert($rn,1,length($message) . " bytes");
	$rn++;
	
	$T->insert($rn,0,"Existing Commands");
	$T->insert($rn,1,($update_result > 0 ? "$update_result existing External File Transfers removed!" : "No existing File Transfers removed"));
	$rn++;

	$T->insert($rn,0,B("Input Variables"), 'colspan=2 align=center');
	$rn++;

	$T->insert($rn,0,"Protocol"); 			$T->insert($rn,1,"$protocol : $protocols_hash{$protocol}"); 		$rn++;
	$T->insert($rn,0,"Server IP Address"); 	$T->insert($rn,1,$ip_address); 		$rn++;
	$T->insert($rn,0,"Server Port"); 		$T->insert($rn,1,$port); 			$rn++;
	$T->insert($rn,0,"Login Name"); 		$T->insert($rn,1,$login_name); 	$rn++;
	$T->insert($rn,0,"Login Password"); 	$T->insert($rn,1,$login_password); 	$rn++;
	$T->insert($rn,0,"File Path"); 			$T->insert($rn,1,$file_path); 		$rn++;
	$T->insert($rn,0,"File Name"); 			$T->insert($rn,1,$file_name); 		$rn++;
	$T->insert($rn,0,"File Path/Name");		$T->insert($rn,1,$file_path_name);	$rn++;
	$T->insert($rn,0,"File Format"); 		$T->insert($rn,1,"$file_format : $file_formats_hash{$file_format}"); 	$rn++;
	$T->insert($rn,0,"File Encapsulation"); $T->insert($rn,1,"$file_encapsulation : $file_encapsulations_hash{$file_encapsulation}");	$rn++;
	$T->insert($rn,0,"CRC Type"); 			$T->insert($rn,1,"$crc_type : $crc_types_hash{$crc_type}"); 		$rn++;
	$T->insert($rn,0,"CRC Value"); 			$T->insert($rn,1,$crc); 			$rn++;
	$T->insert($rn,0,"Encryption Key"); 	$T->insert($rn,1,$encryption_key); 	$rn++;
	$T->insert($rn,0,"Transfer Start"); 	$T->insert($rn,1,$transfer_start); 	$rn++;
	$T->insert($rn,0,"Transfer End"); 		$T->insert($rn,1,$transfer_end); 	$rn++;
	$T->insert($rn,0,"Retries"); 			$T->insert($rn,1,ord($retries)); 		$rn++;
	$T->insert($rn,0,"Retry Interval"); 	$T->insert($rn,1,ord($retry_interval)); 	$rn++;
	
	if($device->device_type_id =~ /^(11)$/)
	{
		$T->insert($rn,0,"Client File Path");	$T->insert($rn,1,$client_file_path);	$rn++;	
	}
	
	$T->insert($rn,0,"Result");
	if($mcp->ID)
	{
		$T->insert($rn,1,B("Command created successfully!"));
		$rn++;
	}
	else
	{
		$T->insert($rn,1,B("Insert failed!","RED"));
		$rn++;
	}
	
	my $T2 = $T;
	
	my $email_result = &send_email('device_admin@usatech.com', $confirm_email, "External File Transfer Confirmation: $device_name", $T2); 

	$T->insert($rn,0,"Email Result");
	if(defined $confirm_email && length($confirm_email) > 0)
	{
		if($email_result)
		{
			$T->insert($rn,1,B("Confirmation sent successfully to $confirm_email!"));
		}
		else
		{
			$T->insert($rn,1,B("Confirmation failed!","RED"));
		}
	}
	else
	{
		$T->insert($rn,1,B("No confirmation email requested."));
	}
	$rn++;

	$T->insert($rn,0,qq|<input type="button" value="&lt; Back to $device_name" onClick="javascript:window.location='/profile.cgi?ev_number=$device_name';"|, "colspan=2 class=header  align=center");
	$rn++;
	return $T;
}

sub display
{
	my $T = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
	my $rn = 0;
	
	$T->insert($rn++,0,B("Create External Client to Server File Transfer - $device_name",4),'colspan=4 style="background-color:#C0C0C0; text-align:center;"');
	
	my $protocolPopup = OOCGI::Popup->new(
		name=>'protocol', 
		pair =>$protocols);
	
	my $fileFormatPopup = OOCGI::Popup->new(
		name=>'file_format', 
		pair =>$file_formats);
	
	$T->insert($rn,0,'Transfer Protocol');
	$T->insert($rn,1, $protocolPopup);
	$T->insert($rn,2,'File Format');
	$T->insert($rn,3, $fileFormatPopup);
	$rn++;
	
	my $ipAddressText = OOCGI::Text->new(name=>'ip_address',value=>'216.127.247.19');
	my $portText = OOCGI::Text->new(name=>'port',value=>'21');
	$T->insert($rn,0,'Server IP Address');
	$T->insert($rn,1, $ipAddressText);
	$T->insert($rn,2,'Server Port');
	$T->insert($rn,3, $portText);
	$rn++;
	
	my $loginNameText = OOCGI::Text->new(name=>'login_name');
	my $passwordText = OOCGI::Text->new(name=>'login_password');
	
	$T->insert($rn,0,'Login Name');
	$T->insert($rn,1, $loginNameText);
	$T->insert($rn,2,'Login Password');
	$T->insert($rn,3, $passwordText);
	$rn++;
	
	my $filePathText = OOCGI::Text->new(name=>'file_path', size=>70);
	$T->insert($rn,0,'File Path on Server');
	$T->insert($rn,1, $filePathText, 'colspan=3');
	$rn++;

	my $fileNameText = OOCGI::Text->new(name=>'file_name', size=>70);
	$T->insert($rn,0,'File Name on Server');
	$T->insert($rn,1, $fileNameText, 'colspan=3');
	$rn++;

	my $fileEncapsulationPopup = OOCGI::Popup->new(
		name=>'file_encapsulation', 
		pair=> $file_encapsulations);
	
	$T->insert($rn,0,'File Encapsulation');
	$T->insert($rn,1, $fileEncapsulationPopup, 'colspan=3');
	$rn++;
	
	my $crcTypePopup = OOCGI::Popup->new(
		name=>'crc_type', 
		pair => $crc_types);
	my $crcText = OOCGI::Text->new(name=>'crc', size=>40);
	
	$T->insert($rn,0,'CRC Type');
	$T->insert($rn,1, $crcTypePopup);
	$T->insert($rn,2,'CRC (In HEX)');
	$T->insert($rn,3, $crcText);
	$rn++;
	
	my $encryptionKeyText = OOCGI::Text->new(name=>'encryption_key', size=>70);
	$T->insert($rn,0,'Encryption Key (In HEX)');
	$T->insert($rn,1, $encryptionKeyText, 'colspan=3');
	$rn++;
	
	my $start_time = time();
	my $end_time = $start_time + (7 * 24 * 60 * 60);
	
	my ($start_second, $start_minute, $start_hour, $start_day, $start_month, $start_year) = localtime($start_time);
	my ($end_second, $end_minute, $end_hour, $end_day, $end_month, $end_year) = localtime($end_time);
	
	my $startStr = sprintf("%02d-%02d-%04d %02d:%02d:%02d", ($start_month + 1), $start_day, ($start_year + 1900), $start_hour, $start_minute, $start_second);
	my $endStr = sprintf("%02d-%02d-%04d %02d:%02d:%02d", ($end_month + 1), $end_day, ($end_year + 1900), $end_hour, $end_minute, $end_second);
	
	my $transferStartText = OOCGI::Text->new(name=>'transfer_start', value=>$startStr, size=>30);
	my $transferEndText = OOCGI::Text->new(name=>'transfer_end', value=>$endStr, size=>30);
	
	$T->insert($rn,0,'Transfer Time Window Start');
	$T->insert($rn,1, $transferStartText);
	$T->insert($rn,2,'Transfer Time Window End');
	$T->insert($rn,3, $transferEndText);
	$rn++;
	
	my $retriesText = OOCGI::Text->new(name=>'retries',value=>'5');
	my $retryIntervalText = OOCGI::Text->new(name=>'retry_interval',value=>'15');
	
	$T->insert($rn,0,'Number of Retries');
	$T->insert($rn,1, $retriesText);
	$T->insert($rn,2,'Minimum Retry Interval (Minutes)');
	$T->insert($rn,3, $retryIntervalText);
	$rn++;
	
	if($device->device_type_id =~ /^(11)$/)
	{
		my $clientFilePath = OOCGI::Text->new(name=>'client_file_path', size=>70);
		$T->insert($rn,0,'Client File Path/Name');
		$T->insert($rn,1, $clientFilePath, 'colspan=3');
		$rn++;	
	}
	
	my $confirmEmailText = OOCGI::Text->new(name=>'confirm_email', size=>70);
	$T->insert($rn,0,'Confirmation Email');
	$T->insert($rn,1, $confirmEmailText, 'colspan=3');
	$rn++;

	my $str;
	$str = qq|<input type="submit" name="userOP" value="Save">|;
	$T->insert($rn,0, $str,'align=center colspan=4');
	
	return $T;
}

sub send_email
{
        my ($from_addr, $to_addrs, $subject, $content) = @_;
        my @to_array = split(/ |,/, $to_addrs);

		#my $smtp = Net::SMTP->new('mailhost', Debug => 1);
		my $smtp = Net::SMTP->new('mailhost');
        
        if(not defined $smtp)
        {
                warn "send_email_detailed: Failed to connect to SMTP server mailhost!\n";
                return 0;
        }
        
        $smtp->mail($from_addr);

        foreach my $to_addr (@to_array)
        {
                $smtp->to($to_addr);
        }
        
		$smtp->data(); 
		$smtp->datasend("Subject: $subject\n");
		$smtp->datasend("MIME-Version: 1.0\n");
		$smtp->datasend("Content-type: text/html\n");
		$smtp->datasend("Content-Transfer-Encoding: 7bit\n");  
        $smtp->datasend("\n"); 
        $smtp->datasend("$content\n\n"); 
        $smtp->dataend(); 
        
        $smtp->quit();   
        
        return 1;
}
