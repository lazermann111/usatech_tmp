#!/usr/local/USAT/bin/perl

use strict;
use CGI::Carp qw(fatalsToBrowser);
use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::Log;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;

my $userOP      = $query->param('userOP');
my $device_id   = $query->param('device_id');
my $device      = USAT::DeviceAdmin::DBObj::Device->new($device_id);
my $customer_id = $device->Customer()->customer_id;

if($userOP eq 'Change Customer') {
   $customer_id = $query->param('customer_id');

   $device = $device->change_customer($customer_id);
   $device_id = $device->ID;
   print CGI->redirect("/profile.cgi?device_id=$device_id&tab=1");
   exit();
}

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
USAT::DeviceAdmin::UI::DAHeader->printHeader;
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html"); 
$session->print_menu;
$session->destroy;

print "<form>";

my $sql;

$sql = qq|SELECT customer_id, customer_name
            FROM customer
           WHERE customer_active_yn_flag = 'Y'
        ORDER BY customer_name|;

my $popCustomer = OOCGI::Popup->new(name => 'customer_id',
                  align => 'yes', delimiter => '.',
                  style => "font-family: courier; font-size: 12px;",
                  sql => $sql);

$popCustomer->head('1','Undefined');
$popCustomer->default($customer_id);
BR(4);
print $popCustomer->string;
BR(4);
print '<input type="submit" name=userOP value="Change Customer">';
BR(4);
HIDDEN('device_id',$device_id);
print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
