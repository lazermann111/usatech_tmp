#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Common::Const;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::Util;
use USAT::App::DeviceAdmin::Const qw(:globals );
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $device_name   = $PARAM{"device_name"};
my $action = $PARAM{"myaction"};
my $file_name = $device_name . '-CFG';
my $file_id;
my $edit_mode = $PARAM{"edit_mode"};
my $new_ini_file = '';
my $new_ini_file_short = '';

if(length($device_name) == 0)
{
	$dbh->disconnect;
	print $user_menu;

	print "\nRequired Parameter Not Found: device_name\n";

	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit();
}

my ($device_id, $device_serial_cd, $device_type_id) = @{USAT::DeviceAdmin::Util::get_device_info_by_name($dbh, $device_name)};
if (!$device_id) {
	print "Device $device_name not found";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);

my $old_ini_file_hex = USAT::DeviceAdmin::Util::blob_select_by_name($dbh, $file_name);

if(!$old_ini_file_hex) {
  print "Device configuration file $file_name not found for device $device_name ";
  USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
  exit;
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my @default_config_array;

{
   my $local_file_name = PKG_DEVICE_CONF__EDGE.$device->property_list_version;
   my $file_data_hex = USAT::DeviceAdmin::Util::blob_select_by_name($dbh, $local_file_name);

   if(!$file_data_hex) {
      print "Default cfg file $local_file_name not found for device $device_name ";
      USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
      exit;
   }

   my $file_data = pack("H*", $file_data_hex);
   $file_data =~ tr/\r//d;  # added to make sure it splits unix and windows data
   chomp($file_data);       # just in case if there is a new line at the end, remove it

   my @temp_line = split(/\n/, $file_data);
   foreach my $line ( @temp_line ) {
      my ($index, $value) = split(/\=/, $line);
      if(defined $index && $index ne '') {
         $default_config_array[$index] = $value;
      }
   }
}

my $change_count = 0;
my %config_hash;

{ # curly bracket for localizing variables
    my @old_ini_file_lines   = ();
    my @old_ini_file_records = ();
    my $old_ini_file  = pack("H*", $old_ini_file_hex);
    $old_ini_file =~ tr/\r//d;
    chomp($old_ini_file);  # just in case remove last new line if it exist
    @old_ini_file_lines = split(/\n/, $old_ini_file);
    foreach my $old_ini_file_line ( @old_ini_file_lines ) {
       my @arr =  split(/=/, $old_ini_file_line, 2);    
       $old_ini_file_records[$arr[0]] = $arr[1];
    }
	my @config_params = split(/\|\|\|/, $PARAM{"config_params"});
    for(my $i = 0; $i <= $#default_config_array; $i++)
	{
		if(defined $default_config_array[$i]) {
			my $param_value = $PARAM{"cfg_$i"};
			
            # if a DNS host name or IP address doesn't have two dots, use the default value
            if($i =~ m/^(0|1|2|3|20|21|22|23)$/ && $param_value !~ m/\..+\./) {
            	$param_value = $default_config_array[$i];
            }
			
			if(defined $param_value) {
				$new_ini_file .= "$i=" . $param_value . "\n";
				
				if(!defined $old_ini_file_records[$i] || $old_ini_file_records[$i] ne $param_value) {
					if($default_config_array[$i] eq $param_value) {
						$new_ini_file_short .= "$i!\n";
					} else {
						$new_ini_file_short .= "$i=" . $param_value . "\n";
					}
					$change_count++;
				}
			} else {
				$new_ini_file .= "$i=" . $default_config_array[$i] . "\n";
			}
		}
	}
}
         
my $new_ini_file_hex = unpack("H*", $new_ini_file);

print "
<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
 <tr>
  <td align=\"left\">
   <br>
   <center>
   <form>";
   
	print "
		<input type=button value=\"Return to Device Profile\" onClick=\"javascript:window.location = 'profile.cgi?ev_number=$device_name';\">";

print "
   </form>
   </center>
   <pre>
";

if ($change_count == 0)
{
	print "\nNo User Changes!\n";
}
else
{
	print "\n<b>Processing device configuration...</b>\n";

	FILE_OPERATIONS: while (1) 
	{	
		my $file_stmt;
		$file_stmt = $dbh->prepare("select min(file_transfer_id) from device.file_transfer where file_transfer_name = :file_name and file_transfer_type_cd = :file_transfer_type_cd") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$file_stmt->bind_param(":file_name", $file_name);
		$file_stmt->bind_param(":file_transfer_type_cd", PKG_FILE_TYPE__CONFIGURATION_FILE);
		$file_stmt->execute();

		my @file_data = $file_stmt->fetchrow_array();
		$file_stmt->finish();

		if(@file_data)
		{
			$file_id = $file_data[0];
		}
		else
		{
			my $file_ins_stmt = $dbh->prepare("INSERT INTO device.file_transfer(file_transfer_name, file_transfer_type_cd) VALUES(:file_name, :file_transfer_type_cd)") or
			print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
			$file_ins_stmt->bind_param(":file_name", $file_name);
			$file_ins_stmt->bind_param(":file_transfer_type_cd", PKG_FILE_TYPE__CONFIGURATION_FILE);
			$file_ins_stmt->execute();	
			if(defined $dbh->errstr)
			{
				print "\nsql error: " . $dbh->errstr . "\n";
				last FILE_OPERATIONS;
			}
			$file_ins_stmt->finish();

			$file_stmt->execute();
			@file_data = $file_stmt->fetchrow_array();
			$file_stmt->finish();

			if(!@file_data)
			{
				print "\nFailed to get a new device_file_transfer_id!\n";
				last FILE_OPERATIONS;
			}

			$file_id = $file_data[0];
		}

		print "\n<b>Saving file $file_name...</b>\n\n";

		print "ASCII Data:<br>";
		print "<textarea rows=\"10\" cols=\"100\" wrap=\"virtual\">$new_ini_file</textarea>\n\n";

		print "HEX Data:<br>";
		print "<textarea rows=\"10\" cols=\"100\" wrap=\"virtual\">$new_ini_file_hex</textarea>\n";
		
		$dbh->begin_work();
		USAT::DeviceAdmin::Util::save_file($file_name, $new_ini_file_hex, $dbh);
		
		if(defined $dbh->errstr)
		{
			$dbh->rollback();
			print "\nError saving file $file_name: " . $dbh->errstr . "\n";
			last FILE_OPERATIONS;
		}
		
		my $msg = "\n<b>Successfully saved file $file_name.</b>\n";
		
		my $get_file_id_stmt = $dbh->prepare("select SEQ_FILE_TRANSFER_ID.nextval from dual") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$get_file_id_stmt->execute();
		my @file_id_data = $get_file_id_stmt->fetchrow_array();
		$get_file_id_stmt->finish();

		if(!@file_id_data)
		{
			$dbh->rollback();
			print "\nFailed to get a new file_transfer_id!\n";
			last FILE_OPERATIONS;
		}

		my $tmp_file_id = $file_id_data[0];
		my $tmp_file_name = $file_name . '-' . $tmp_file_id;

		USAT::DeviceAdmin::Util::create_file_with_id($tmp_file_id, $tmp_file_name, PKG_FILE_TYPE__PROPERTY_LIST, unpack("H*",$new_ini_file_short), $DATABASE);

		if(defined $dbh->errstr)
		{
			$dbh->rollback();
			print "\nError creating file $tmp_file_name: " . $dbh->errstr . "\n";
			last FILE_OPERATIONS;
		}

		$msg .= "\n<b>Updating device settings...</b>\n";
		my $result = $DATABASE->callproc(
			name  => 'pkg_device_configuration.sp_update_device_settings',
			bind  => [ qw(:pn_device_id :pn_file_transfer_id :pn_resuld_cd :pv_error_message :pn_setting_count) ],
			in    => [ $device_id, $file_id ],
			inout => [ 20, 2000, 20]
		);
		if($result->[0] == 1) {
			$msg .= "\nChanges you made are:\n$new_ini_file_short\n";
			$msg .= "Successfully updated $result->[2] device settings for device_id: $device_id, file_transfer_id: $file_id.<br>";
		} else {
			$dbh->rollback();
			print "\n<b>Error updating device settings for device_id: $device_id, file_transfer_id: $file_id</b>\n";
			last FILE_OPERATIONS;
		}
		
		$msg .= "\n<b>Creating command to send config file...</b>\n";
	
		my $cmd_file_transfer_start = EPORT_CMD__FILE_TRANSFER_START_V4_1;
		
		# now get the next device_file_transfer_id from the sequence
		my $get_transfer_id_stmt = $dbh->prepare("select SEQ_DEVICE_FILE_TRANSFER_ID.nextval from dual") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$get_transfer_id_stmt->execute();
		my @transfer_id_data = $get_transfer_id_stmt->fetchrow_array();
		$get_transfer_id_stmt->finish();

		if(!@transfer_id_data)
		{
			$dbh->rollback();
			print "\nFailed to get a new device_file_transfer_id!\n";
			last FILE_OPERATIONS;
		}

		my $transfer_id = $transfer_id_data[0];		
		
		# now insert the device_file_transfer
		my $insert_transfer_stmt = $dbh->prepare("insert into device.device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size) values (:transfer_id, :device_id, :file_id, :direction, :status, :packet_size)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$insert_transfer_stmt->bind_param(":transfer_id", $transfer_id); 
		$insert_transfer_stmt->bind_param(":device_id", $device_id);
		$insert_transfer_stmt->bind_param(":file_id", $tmp_file_id);
		$insert_transfer_stmt->bind_param(":direction", 'O');
		$insert_transfer_stmt->bind_param(":status", '0');
		$insert_transfer_stmt->bind_param(":packet_size", 1024);
		$insert_transfer_stmt->execute();
		
		if(defined $dbh->errstr)
		{
			$dbh->rollback();
			print "\nFailed to insert into device_file_transfer: " . $dbh->errstr . "\n";
			last FILE_OPERATIONS;
		}
		$insert_transfer_stmt->finish();
		
		# now insert a pending message for this new transfer
		my $insert_pending_msg_stmt = $dbh->prepare("insert into engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order) values (:ev_number, :data_type, :transfer_id, :execute_cd, :execute_order)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$insert_pending_msg_stmt->bind_param(":ev_number", $device_name); 
		$insert_pending_msg_stmt->bind_param(":data_type", $cmd_file_transfer_start); 
		$insert_pending_msg_stmt->bind_param(":transfer_id", $transfer_id); 
		$insert_pending_msg_stmt->bind_param(":execute_cd", DA_CMD__P_PENDING); 
		$insert_pending_msg_stmt->bind_param(":execute_order", '1'); 
		$insert_pending_msg_stmt->execute();
		
		if(defined $dbh->errstr)
		{
			$dbh->rollback();
			print "\nFailed to insert into machine_cmd_pending: " . $dbh->errstr . "\n";
			last FILE_OPERATIONS;
		}
		$insert_pending_msg_stmt->finish();
		
		$msg .= "\n<b>Successfully created File Transfer Start command for file transfer id $tmp_file_id.</b>\n";
		
		$dbh->commit();
		print $msg;
		last FILE_OPERATIONS;
	};
}

$dbh->disconnect;

print "
   </pre>
  </td>
 </tr>
 <tr>
  <td align=\"left\">
   <center>
   <form>
   ";

print "<input type=button value=\"Return to Device Profile\" onClick=\"javascript:window.location = 'profile.cgi?ev_number=$device_name';\">";

print "
   </form>
   </center>
  </td>
 </tr>  
</table>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
