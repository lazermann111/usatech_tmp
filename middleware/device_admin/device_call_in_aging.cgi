#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::PageSort::Simple;
use OOCGI::PageNavigation;
use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;

my %enabled_hash = (	'Y'	=>	'Enabled',
						'N'	=>	'Disabled' );
my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my $pageNav  = OOCGI::PageNavigation->new(25);

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $device_type = $PARAM{"device_type"};
my $enabled     = $PARAM{"enabled"};
my $last_event  = $PARAM{"last_event"};
my $active_days = $PARAM{"active_days"};
my $toler_days  = $PARAM{"toler_days"};

if(length($device_type) == 0 || length($last_event) == 0 || length($active_days) == 0 || length($toler_days) == 0)
{
	print "Required parameter not found: device_type or last_event or min_days or max_days\n";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if(length($enabled) == 0)
{
	$enabled = "'Y'";
}

my @enable_disable = split(/,/,$enabled);
my $questionmarks  = join(',', ('?') x scalar(@enable_disable));

my @table_cols;
my @sql_cols;
my $list_devices_sql;
if ($last_event == 0)
{
	$list_devices_sql = qq{
       SELECT dev.device_id,
              dev.device_name,
              dev.device_serial_cd,
              dev.created_ts,
              devt.device_type_desc,
              dev.device_active_yn_flag,
              dev.encryption_key,
              dev.device_type_id,
              pos.location_id,
              loc.location_name,
              pos.customer_id,
              cus.customer_name,
              TO_CHAR(dev.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'),
              ROUND((SYSDATE - dev.last_activity_ts), 1) last_activity_days,
              TO_CHAR(lci.last_call_ts, 'MM/DD/YYYY HH:MI:SS AM'),
              ROUND((SYSDATE - lci.last_call_ts), 1) last_call_days
         FROM device.device dev,
              device.device_type devt,
              pos,
              location.customer cus,
              location.location loc,
              ( SELECT dv.device_id,
                       MAX(dcir.call_in_start_ts) last_call_ts
                  FROM device_call_in_record dcir,
                       device.device dv
                 WHERE dcir.serial_number = dv.device_serial_cd
                   AND dv.device_type_id = ?
                   AND dv.device_active_yn_flag IN ($questionmarks)
              GROUP BY dv.device_id ) lci
        WHERE dev.device_type_id = devt.device_type_id
          AND dev.device_type_id = ?
          AND dev.device_active_yn_flag IN ($questionmarks)
          AND dev.device_id = pos.device_id
          AND pos.customer_id = cus.customer_id
          AND pos.location_id = loc.location_id
          AND dev.device_id = lci.device_id (+)
          AND dev.last_activity_ts >= SYSDATE - ?
          AND ( lci.last_call_ts IS NULL OR lci.last_call_ts <= SYSDATE - ? )
     };
    @table_cols = ( 
        ['Device&nbsp;Type',              7],
        ['EV&nbsp;Number',                3],
        ['Serial&nbsp;Number',            4],
        ['Customer',                      5],
        ['Location',                      6],
        ['Last&nbsp;Activity',           -1],
        ['Last&nbsp;Call&nbsp;In',       -2],
        ['Status',                       -8]
    );

    @sql_cols = ( 
        'devt.device_type_desc',
        'dev.device_name',
        'dev.device_serial_cd',
        'cus.customer_name',
        'loc.location_name',
        "NVL(dev.last_activity_ts,   TO_DATE('1970', 'YYYY'))",
        "NVL(lci.last_call_ts,       TO_DATE('1970', 'YYYY'))",
        'dev.device_active_yn_flag' );

} elsif ($last_event == 1) {
	$list_devices_sql = qq{
       SELECT dev.device_id,
              dev.device_name,
              dev.device_serial_cd,
              dev.created_ts,
              devt.device_type_desc,
              dev.device_active_yn_flag,
              dev.encryption_key,
              dev.device_type_id,
              pos.location_id,
              loc.location_name,
              pos.customer_id,
              cus.customer_name,
              TO_CHAR(dev.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'),
              ROUND((SYSDATE - dev.last_activity_ts), 1) last_activity_days,
              TO_CHAR(laci.last_ack_ts, 'MM/DD/YYYY HH:MI:SS AM'),
              ROUND((SYSDATE - laci.last_ack_ts), 1) last_ack_days
         FROM device.device dev,
              device.device_type devt,
              pos,
              location.customer cus,
              location.location loc,
              ( SELECT dv.device_id,
                       MAX(dcir.call_in_start_ts) last_ack_ts
                  FROM device_call_in_record dcir,
                       device.device dv
                 WHERE dcir.serial_number = dv.device_serial_cd
                   AND dv.device_type_id = ?
                   AND dv.device_active_yn_flag IN ($questionmarks)
                   AND dcir.inbound_message_count > 1 
                 GROUP BY dv.device_id ) laci
        WHERE dev.device_type_id = devt.device_type_id
          AND dev.device_type_id = ?
          AND dev.device_active_yn_flag IN ($questionmarks)
          AND dev.device_id = pos.device_id
          AND pos.customer_id = cus.customer_id
          AND pos.location_id = loc.location_id
          AND dev.device_id = laci.device_id (+)
          AND dev.last_activity_ts >= SYSDATE - ?
          AND ( laci.last_ack_ts IS NULL OR laci.last_ack_ts <= SYSDATE - ? )
        };

#print "BBB $list_devices_sql <br>";
    @table_cols = ( 
        ['Device&nbsp;Type',              7],
        ['EV&nbsp;Number',                3],
        ['Serial&nbsp;Number',            4],
        ['Customer',                      5],
        ['Location',                      6],
        ['Last&nbsp;Activity',           -1],
        ['Last&nbsp;Ack',                -2],
        ['Status',                       -8]
    );

    @sql_cols = (  
        'devt.device_type_desc',
        'dev.device_name',
        'dev.device_serial_cd',
        'cus.customer_name',
        'loc.location_name',
        "NVL(dev.last_activity_ts,   TO_DATE('1970', 'YYYY'))",
        "NVL(laci.last_ack_ts,TO_DATE('1970', 'YYYY'))",
        'dev.device_active_yn_flag' );

} else {
	$list_devices_sql = qq{
       SELECT dev.device_id,
              dev.device_name,
              dev.device_serial_cd,
              dev.created_ts,
              devt.device_type_desc,
              dev.device_active_yn_flag,
              dev.encryption_key,
              dev.device_type_id,
              pos.location_id,
              loc.location_name,
              pos.customer_id,
              cus.customer_name,
              TO_CHAR(dev.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'),
              ROUND((SYSDATE - dev.last_activity_ts), 1) last_activity_days,
              TO_CHAR(lsb.last_success_bat_ts, 'MM/DD/YYYY HH:MI:SS AM'),
              ROUND((SYSDATE - lsb.last_success_bat_ts), 1) last_success_bat_days
         FROM device.device dev,
              device.device_type devt,
              pos,
              location.customer cus,
              location.location loc,
              ( SELECT dv.device_id,
                       MAX(dcir.call_in_start_ts) last_success_bat_ts
                  FROM device_call_in_record dcir,
                       device.device dv
                 WHERE dcir.serial_number = dv.device_serial_cd
                   AND dv.device_type_id = ?
                   AND dv.device_active_yn_flag IN ($questionmarks)
                   AND dcir.call_in_type IN ('B', 'C')
                   AND dcir.call_in_status = 'S'
                 GROUP BY dv.device_id ) lsb
        WHERE dev.device_type_id = devt.device_type_id
          AND dev.device_type_id = ?
          AND dev.device_active_yn_flag IN ($questionmarks)
          AND dev.device_id = pos.device_id
          AND pos.customer_id = cus.customer_id
          AND pos.location_id = loc.location_id
          AND dev.device_id = lsb.device_id (+)
          AND dev.last_activity_ts >= SYSDATE - ?
          AND ( lsb.last_success_bat_ts IS NULL OR lsb.last_success_bat_ts <= SYSDATE - ?)
        };
    @table_cols = ( 
        ['Device&nbsp;Type',              7],
        ['EV&nbsp;Number',                3],
        ['Serial&nbsp;Number',            4],
        ['Customer',                      5],
        ['Location',                      6],
        ['Last&nbsp;Activity',           -1],
        ['Last&nbsp;Batch&nbsp;Success', -2],
        ['Status',                       -8]
    );

    @sql_cols = (  
        'devt.device_type_desc',
        'dev.device_name',
        'dev.device_serial_cd',
        'cus.customer_name',
        'loc.location_name',
        "NVL(dev.last_activity_ts,   TO_DATE('1970', 'YYYY'))",
        "NVL(lsb.last_success_bat_ts,TO_DATE('1970', 'YYYY'))",
        'dev.device_active_yn_flag' );
}

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $table_cn = 0;
my $table_rn = 0;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');
while( my $column = $pageSort->next_column()) {
         $table->put($table_rn,$table_cn++,$column,'class=header1');
}
$table_rn++;

my $sql = $list_devices_sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql, 
           [ $device_type, @enable_disable, $device_type, @enable_disable, $active_days, $toler_days ] );

#print "$device_type, @enable_disable, $device_type, @enable_disable, $active_days, $toler_days|$sql <br>";
while (my @data = $qo->next_array()) 
{
    $table->put($table_rn, 0, $data[4]);
    $table->put($table_rn, 1, qq{<a style="font-size: 10pt;" href="profile.cgi?device_id=$data[0]">$data[1]</a>});
    $table->put($table_rn, 2, $data[2]);
    $table->put($table_rn, 3, $data[11]);
    $table->put($table_rn, 4, $data[9]);
    if ($data[13] > 5) {
       $table->put($table_rn, 5,B("$data[12]<br>($data[13] days)",'red'),'nowrap');
    } elsif ($data[13] > 2) {
       $table->put($table_rn, 5,B("$data[12]<br>($data[13] days)","#DDCC00"),'nowrap');
    } else {
       $table->put($table_rn, 5,B($data[12],'green'),'nowrap');
    }

    if ($data[15] > $data[13] + 5) {
       $table->put($table_rn, 6,B("$data[14]<br>($data[15] days)",'red'),'nowrap');
    } elsif ($data[15] > $data[13] + 2) {
       $table->put($table_rn, 6,B("$data[14]<br>($data[15] days)","#DDCC00"),'nowrap');
    } else {
       $table->put($table_rn, 6,B($data[14],'green'),'nowrap');
    }

    if ($data[5] eq 'Y') {
       $table->put($table_rn, 7, B($enabled_hash{$data[5]},'green'));
    } else {
       $table->put($table_rn, 7, B($enabled_hash{$data[5]},'red'));
    }

    $table_rn++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
