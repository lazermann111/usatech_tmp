#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $authority_gateway_id = $PARAM{"authority_gateway_id"};
my $authority_gateway_info_stmt = $dbh->prepare(
q{
	select authority_gateway.authority_gateway_id, authority_gateway.authority_gateway_name, authority_gateway.authority_gateway_addr, 
	authority_gateway.authority_gateway_port, authority_gateway.authority_gateway_priority, 
	to_char(authority_gateway.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(authority_gateway.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM') 
	from authority_gateway
	where authority_gateway.authority_gateway_id = :authority_gateway_id
});
$authority_gateway_info_stmt->bind_param(":authority_gateway_id", $authority_gateway_id);
$authority_gateway_info_stmt->execute();
my @authority_gateway_info = $authority_gateway_info_stmt->fetchrow_array();
$authority_gateway_info_stmt->finish();

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=1 width=\"100%\">

 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Authority Gateway</th>
 </tr>
 <tr>
 <tr>
  <td>ID</td><td>$authority_gateway_info[0]</td>
  <td>Name</td><td>$authority_gateway_info[1]</td>
 </tr>
 <tr>
  <td>Address</td><td>$authority_gateway_info[2]</td>
  <td>Port</td><td>$authority_gateway_info[3]</td>
 </tr>
 <tr>
  <td>Created</td><td>$authority_gateway_info[5]</td>
  <td>Last Updated</td><td>$authority_gateway_info[6]</td>
 </tr>
</table>
";

my $authority_list_stmt = $dbh->prepare(
q{
	select authority.authority_id, authority.authority_name, authority_type.authority_type_name, authority_type.authority_type_desc, 
	authority_type.authority_type_id 
	from authority.authority, authority_type, authority_type_gateway 
	where authority.authority_type_id = authority_type.authority_type_id 
	and authority_type.authority_type_id = authority_type_gateway.authority_type_id 
	and authority_type_gateway.authority_gateway_id = :authority_gateway_id
});
$authority_list_stmt->bind_param(":authority_gateway_id", $authority_gateway_id);
$authority_list_stmt->execute();
my @authority_info;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"6\" bgcolor=\"#C0C0C0\">Authorities</th>
 </tr>
 <tr>
  <th>ID</th>
  <th>Name</th>
  <th>Type</th>
 </tr>
";

while(@authority_info = $authority_list_stmt->fetchrow_array())
{
	print "
	     <tr>
    	 <td>$authority_info[0]</td>
	     <td><a href=\"authority.cgi?authority_id=$authority_info[0]\">$authority_info[1]</a></td>
	     <td>$authority_info[2]</td>
    	</tr>
	";
}

$authority_list_stmt->finish();

print "
</table>
";

my $authority_server_list_stmt = $dbh->prepare(
q{
	select authority_server.authority_server_id, authority_server.authority_server_name, authority_server.authority_server_addr, 
	authority_server.authority_server_port, authority_server.authority_server_priority, authority_server.created_ts, 
	authority_server.last_updated_ts 
	from authority_server 
	where authority_server.authority_gateway_id = :authority_gateway_id 
	order by authority_server.authority_server_priority
});
$authority_server_list_stmt->bind_param(":authority_gateway_id", $authority_gateway_id);
$authority_server_list_stmt->execute();
my @authority_server_info;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"6\" bgcolor=\"#C0C0C0\">Authority Servers</th>
 </tr>
 <tr>
  <th>ID</th>
  <th>Name</th>
  <th>Address</th>
  <th>Priority</th>
 </tr>
";

while(@authority_server_info = $authority_server_list_stmt->fetchrow_array())
{
	print "
	     <tr>
    	 <td>$authority_server_info[0]</td>
	     <td><a href=\"authority_server.cgi?authority_server_id=$authority_server_info[0]\">$authority_server_info[1]</a></td>
	     <td>$authority_server_info[2]:$authority_server_info[3]</td>
	     <td>$authority_server_info[4]</td>
    	</tr>
	";
}

$authority_server_list_stmt->finish();

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
