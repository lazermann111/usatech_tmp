#!/usr/local/USAT/bin/perl

use strict;

use CGI qw(-debug);
use OOCGI::OOCGI;
#use CGI::Carp qw(fatalsToBrowser);

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

$query->printHeader();

#Carp::carp "AAA LLL authority_id = $PARAM{authority_id} \n";

my $sql;
my $popTerminal;
my $string = '';
if(defined $PARAM{authority_id} && $PARAM{authority_id} ne '') {
   $sql = q{
      SELECT terminal.terminal_id,
             terminal.terminal_cd, merchant.merchant_name,
             merchant.merchant_cd ,
              '(count=' || count(*) || ')'
        FROM terminal_batch, terminal, merchant, authority.authority, terminal_state, authority_type
       WHERE merchant.authority_id = ?
         AND terminal_batch.terminal_id = terminal.terminal_id
         AND terminal.merchant_id = merchant.merchant_id
         AND merchant.authority_id = authority.authority_id
         AND terminal.terminal_state_id = terminal_state.terminal_state_id
         AND authority.authority_type_id = authority_type.authority_type_id
       GROUP BY terminal.terminal_id,terminal.terminal_cd,merchant.merchant_name,merchant.merchant_cd 
       ORDER BY merchant.merchant_name
   };
   $popTerminal = OOCGI::Popup->new(
       name  => 'terminal_id',
       sql   => $sql,
       bind  => [ $PARAM{authority_id} ],
       style => "font-family: courier; font-size: 12px;",
       align => 'yes', delimiter => '.' 
   );
   if($popTerminal->item_count > 0 ) {
      $popTerminal->head('');
      $popTerminal->default('');
      $string = $popTerminal->string;
   } else {
      $string = '<span style="text-decoration:blink; font-weight:bold; color:#ff0000">NO DATA</span>';
   }
} else {
   $sql = q{
      SELECT terminal.terminal_id,
             terminal.terminal_cd || ' : ' || merchant.merchant_name,
             merchant.merchant_cd, terminal.terminal_id
        FROM terminal, merchant
       WHERE terminal.merchant_id = merchant.merchant_id
       ORDER BY merchant.merchant_name
   };
   $popTerminal = OOCGI::Popup->new(
       name  => 'terminal_id',
       sql   => $sql,
       style => "font-family: courier; font-size: 12px;",
       align => 'yes', delimiter => '.' 
   );
   $popTerminal->head('');
   $popTerminal->default('');
   $string = $popTerminal->string;
}


print $string;
