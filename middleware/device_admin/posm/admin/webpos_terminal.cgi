#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Webpos_terminal->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Webpos_terminal->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Webpos_terminal->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Webpos_terminal->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('webpos_terminal_merchant_cd_id').value == '') {
		  alert("Please insert Webpos Terminal Merchant CD");
		  return false;
	  }
      if (document.getElementById('webpos_terminal_locale_cd_id').value == '') {
		  alert("Please insert Webpos Terminal Locale CD");
		  return false;
	  }
      if (document.getElementById('webpos_terminal_username_id').value == '') {
		  alert("Please insert Webpos Terminal Username");
		  return false;
	  }
      if (document.getElementById('webpos_terminal_password_id').value == '') {
		  alert("Please insert Webpos Terminal Password");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @objects = USAT::DeviceAdmin::DBObj::Webpos_terminal->objects;

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
my $radio_no = 0;
my $sql = 'SELECT webpos_authority_id, webpos_authority_name FROM pss.webpos_authority';

my $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
$table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
$rownum++; $colnum=0;

foreach my $obj ( @objects ) {
   my $ID = $obj->ID;
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio,'rowspan=7');
   $table->put($rownum,$colnum++,'Webpos Terminal ID',          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->webpos_terminal_id);
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,'Webpos Terminal Description', 'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('webpos_terminal_desc',        { sz_ml => [25,60] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,'Webpos Terminal Merchant Cd', 'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('webpos_terminal_merchant_cd', { sz_ml => [25,60] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,'Webpos Terminal Locale Cd', 'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('webpos_terminal_locale_cd', { sz_ml => [25,60] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,'Webpos Terminal Username',    'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('webpos_terminal_username',    { sz_ml => [40,255] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,'Webpos Terminal Password',    'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('webpos_terminal_password',    { sz_ml => [40,255] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,'Webpos Authority ID',     'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Popup('webpos_authority_id',   { sql   => $sql } ));
   $rownum++; $colnum=0;
#  my $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
#                      NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
#  $table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
#  $rownum++; $colnum=0;
   $radio_no++;
} 
if($rownum == 1) {
   $table->put($rownum,$colnum,B('NO RECORDS','red',4),'align=center colspan=3');
} else {
   $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
   $table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
}
$rownum++; $colnum=0;
$table->put($rownum,$colnum++,B('Insert->'),'rowspan=7');
$table->put($rownum,$colnum++,'Webpos Terminal ID',          'class=leftheader');
$table->put($rownum,$colnum,$object->webpos_terminal_id);
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Webpos Terminal Description', 'class=leftheader');
$table->put($rownum,$colnum,$object->Text('webpos_terminal_desc',       { sz_ml => [25,60] } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Webpos Terminal Merchant CD', 'class=leftheader');
$table->put($rownum,$colnum,$object->Text('webpos_terminal_merchant_cd',
		{ sz_ml => [25,60], id => 'webpos_terminal_merchant_cd_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Webpos Terminal Locale CD', 'class=leftheader');
$table->put($rownum,$colnum,$object->Text('webpos_terminal_locale_cd',
		{ sz_ml => [25,60], id => 'webpos_terminal_locale_cd_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Webpos Terminal Username',    'class=leftheader');
$table->put($rownum,$colnum,$object->Text('webpos_terminal_username',
	 	{ sz_ml => [40,255], id => 'webpos_terminal_username_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Webpos Terminal Password',    'class=leftheader');
$table->put($rownum,$colnum,$object->Text('webpos_terminal_password',
	 	{ sz_ml => [40,255], id => 'webpos_terminal_password_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Webpos Authority ID',     'class=leftheader');
$table->put($rownum,$colnum,$object->Popup('webpos_authority_id',  { sql   => $sql } ));

print '<div class="posm_div"><span class="posm_span">Webpos Terminal</span> Table Admin</div>';
display $table;
BR;
SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );

print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
