#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use Regexp::Parser;

my $pageNav  = OOCGI::PageNavigation->new(15);

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Authority_payment_mask->new();

if($PARAM{userOP} eq 'Insert') {
   my $ID  = $object->insert($query)->ID;
   $object = USAT::DeviceAdmin::DBObj::Authority_payment_mask->new();
   print CGI->redirect("authority_payment_mask.cgi?authority_payment_mask_id=$ID&userSearch=Search");
   exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Authority_payment_mask->new($PARAM{radio_name});
   $obj->delete;
}


if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Authority_payment_mask->new($PARAM{radio_name});
   my $regex = $obj->authority_payment_mask_regex;
   my $parser = Regexp::Parser->new;
   my $err = "";
   if (! $parser->regex($regex)) {
       print B("PANIC! For ID $PARAM{radio_name} REGEX is BAD NO UPDATE HAS BEEN PERFORMED <br> $regex",'red');
   } else {
      my $bref  = $obj->authority_payment_mask_bref;
      my $count = $parser->nparen;
      my $ID    = $obj->ID;
      my @arr   = ();
      for(my $i = 1; $i <= $count; $i++) {
          my $val = $PARAM{$ID.'__'.$i};
          if(defined $val && $val > 0 ) {
             push(@arr, "$i:$val");
          }
      }
      my $str = join('|',@arr);
      if($bref ne $str) {
         $obj->authority_payment_mask_bref($str);
      }
      $obj->update($query);
   }
}


print '<form name="myform" method="post">';

print <<"JSCRIPT";
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script type="text/javascript" src="/js/milonic_local/testregex.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   // arguments of the function
   // id    is the authority_payment_mask_bref_id
   // order is the index of popup from top for given authority_payment_mask_bref_id.
   // each authority_payment_mask_bref_id has count (given below ) number of popups.
   // count is number of dropdowns  this is equal to number of opened paranteses '(' for backref
   // for given regex
   // selected_index is which value of select box was default before the page displayed
   // we use this to set value back, if user select already selected combination
   function checkpop(id,order,count,selected_index) {
      var popid = 'pop_'+id+'__'+order;
      var value = document.getElementById(popid).value;
      for(var i = 1; i <= count; i++) {
          var key = 'pop_'+id+'__'+i;
          if (key != popid && document.getElementById(key).value == value && value != '') {
    		  alert('Selection Already exists!');
              document.getElementById(popid).selectedIndex = selected_index;
		      return false;
          }
      }
   }
   function validateFormInsert() {
      if (document.getElementById('authority_payment_mask_name_id').value == '') {
		  alert("Please insert Authority Payment Mask Name");
		  return false;
	  }
      if (document.getElementById('authority_payment_mask_regex_id').value == '') {
		  alert("Please insert Authority Payment Mask Regex");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @table_cols = (
        ['Authority Payment Mask ID'   , 1],
        ['Authority Payment Mask Name' , 2],
        ['Authority Payment Mask Desc' , 3],
        ['Authority Payment Mask Regex', 4],
        ['Authority Payment Mask Bref' , 5],
        ['Payment Subtype ID'          , 6],
);

my @sql_cols = (
        'authority_payment_mask_id',
        'authority_payment_mask_name',
        'authority_payment_mask_desc',
        'authority_payment_mask_regex',
        'authority_payment_mask_bref',
        'payment_subtype_id',
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ FROM authority_payment_mask WHERE authority_payment_mask_id > 0 };

my @bind = ();
if($PARAM{authority_payment_mask_id} ne '') {
   $sql_data .= " AND authority_payment_mask_id = ? ";
   push(@bind,  $PARAM{authority_payment_mask_id});
}
if($PARAM{authority_payment_mask_name} ne '') {
   $sql_data .= " AND LOWER(authority_payment_mask_name) like ? ";
   push(@bind, lc "$PARAM{authority_payment_mask_name}%");
}
if($PARAM{authority_payment_mask_desc} ne '') {
   $sql_data .= " AND LOWER(authority_payment_mask_desc) like ? ";
   push(@bind, lc "$PARAM{authority_payment_mask_desc}%");
}
if($PARAM{authority_payment_mask_regex} ne '') {
   $sql_data .= " AND authority_payment_mask_regex like ? ";
   push(@bind, "$PARAM{authority_payment_mask_regex}%");
}
if($PARAM{authority_payment_mask_bref} ne '') {
   $sql_data .= " AND authority_payment_mask_bref like ? ";
   push(@bind, "$PARAM{authority_payment_mask_bref}%");
}
if($PARAM{payment_subtype_id} ne '' && $PARAM{payment_subtype_id} > 0) {
   $sql_data .= " AND payment_subtype_id = ? ";
   push(@bind, "$PARAM{payment_subtype_id}");
}

#print "DDD $sql_data $PARAM{authority_payment_mask_id}<br>";

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);


my $qo   = $pageNav->get_result_obj($sql_data, \@bind );

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
my $radio_no = 0;
my $sql_payment_subtype = 'SELECT payment_subtype_id, payment_subtype_name FROM pss.payment_subtype';

my $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
$table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
$rownum++; $colnum=0;

while(my %hash = $qo->next_hash) {
   my $ID = $hash{authority_payment_mask_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Authority_payment_mask->new($ID);
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio,'rowspan=7');
   $table->put($rownum,$colnum++,$column_sort[0],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->authority_payment_mask_id);
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[1],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('authority_payment_mask_name',        { sz_ml => [40,60] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[2],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('authority_payment_mask_desc',        { sz_ml => [40,60] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[3],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('authority_payment_mask_regex',
                  { id => "regex_$ID", sz_ml => [60,4000] } ));
   $rownum++; $colnum=1;
   my $testButton = CENTER(BUTTON('aaa','Test Your Regexp', {onclick=> "javascript:testRegex($ID);"}));
   $table->put($rownum,$colnum++,$testButton, 'class=leftheader');
   my $txtTest =  OOCGI::Text->new(name => 'rrr', id => "testv_$ID", size => 40);
   $table->put($rownum,$colnum,  $txtTest);
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[4],          'class=leftheader');
   print $obj->Hidden('authority_payment_mask_bref', { id => "backr_$ID" } );
   my $T = OOCGI::NTable->new("border=1 width=100%");
   my $regex= $obj->authority_payment_mask_regex;
   my $parser = Regexp::Parser->new;
 
   my $bref = $obj->authority_payment_mask_bref;

   my $count = 0; 
   my $err = B($bref,'green');;
   if (! $parser->regex($regex)) {
      $err = B($bref.' REGEX ERROR','red');
   } else {
      $count = $parser->nparen;
   }

   my @brefarr = split('\|', $bref);

   my $rr = 0;
   $T->put($rr,0,'BREF Index');   
   $T->put($rr,1,"$err");   
   for(my $i = 1; $i <= $count; $i++) {
      my $idkey   = $ID.'__'.$i;
      my $popid   = 'pop_'.$idkey;
      my $popAPMB = OOCGI::Popup->new(
         sql => 'SELECT authority_payment_mask_bref_id,regex_bref_name
                   FROM authority_payment_mask_bref',
         onchange => "checkpop($ID,$i,$count,'');",
         name => $idkey, id => $popid);
      $popAPMB->head('','Please Select');
      $popAPMB->default('');
      $T->put($i,0,$i);   
      $T->put($i,1,$popAPMB);   
   }
     

   foreach my $key1 ( @brefarr ) {
      my ($index, $value ) = split(':', $key1);
      my $idkey   = $ID.'__'.$index;
      my $popid   = 'pop_'.$idkey;
      my $popAPMB = OOCGI::Popup->new(
         sql => 'SELECT authority_payment_mask_bref_id,regex_bref_name
                   FROM authority_payment_mask_bref',
         onchange => "checkpop($ID,$index,$count,$value);",
         name => $idkey, id => $popid);
      $popAPMB->head('','Please Select');
      $T->put($index,0,$index);   
      $popAPMB->default($value);
      $T->put($index,1,$popAPMB,'bgcolor=lightgreen');   
   }
   $table->put($rownum,$colnum, $T);
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[5],  'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Popup('payment_subtype_id', { sql   => $sql_payment_subtype } ));
   $rownum++; $colnum=0;
   $radio_no++;
} 

if($rownum == 1) {
   $table->put($rownum,$colnum,B('NO RECORDS','red',4),'align=center colspan=3');
} else {
   $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
   $table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
}
$rownum++; $colnum=0;

$table->put($rownum,$colnum++,B('Insert->'),'rowspan=6');
$table->put($rownum,$colnum++,'Authority Payment Mask ID',          'class=leftheader');
$table->put($rownum,$colnum,$object->authority_payment_mask_id);
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Authority Payment Mask Name', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('authority_payment_mask_name',
            		{ sz_ml => [40,60], id => 'authority_payment_mask_name_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Authority Payment Mask Desc', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('authority_payment_mask_desc',         { sz_ml => [40,60] } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Authority Payment Mask Regex', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('authority_payment_mask_regex',
                    { sz_ml => [60,4000], id => 'authority_payment_mask_regex_id' } ));
$rownum++; $colnum=1;
#$table->put($rownum,$colnum++,'Authority Payment Mask Bref',    'class=leftheader');
##########$table->put($rownum,$colnum++,$popAPMB,    'class=leftheader');
#$table->put($rownum,$colnum,"<div id='bref_div'>$pop_string</div>".$object->Hidden('authority_payment_mask_bref', { id => 'bref_txt_id' } )); 
##############   id => 'bref_txt_id' } ));
#$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Payment Subtype ID',     'class=leftheader');
$table->put($rownum,$colnum,   $object->Popup('payment_subtype_id', { sql   => $sql_payment_subtype } ));
$rownum++; $colnum=1;

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Authority Payment Mask Name: ',     'class=leftheader');
my $txtAuthorityPaymentMaskName = OOCGI::Text->new(name => 'authority_payment_mask_name',
      	value => $PARAM{authority_payment_mask_name}, sz_ml => [40,60]);
$search_table->put(0,1,  $txtAuthorityPaymentMaskName,'bgcolor=lightgreen');

$search_table->put(1,0, 'Authority Payment Mask Desc: ',       'class=leftheader');
my $txtAuthorityPaymentMaskDesc = OOCGI::Text->new(name => 'authority_payment_mask_desc',
     	value => $PARAM{authority_payment_mask_desc}, sz_ml => [40,60]);
$search_table->put(1,1,  $txtAuthorityPaymentMaskDesc,'bgcolor=lightgreen');

$search_table->put(2,0, 'Authority Payment Mask Regex: ',       'class=leftheader');
my $txtAuthorityPaymentMaskRegex = OOCGI::Text->new(name => 'authority_payment_mask_regex',
     	value => $PARAM{authority_payment_mask_regex}, sz_ml => [60,4000]);
$search_table->put(2,1,  $txtAuthorityPaymentMaskRegex,'bgcolor=lightgreen');

$search_table->put(3,0, 'Authority Payment Mask Bref: ',       'class=leftheader');
my $txtAuthorityPaymentMaskBref = OOCGI::Text->new(name => 'authority_payment_mask_bref',
     	value => $PARAM{authority_payment_mask_bref}, sz_ml => [40,60]);
$search_table->put(3,1,  $txtAuthorityPaymentMaskBref,'bgcolor=lightgreen');

$search_table->put(4,0, 'Payment Subtype ID: ',       'class=leftheader');
my $popPaymentSubtypeId = OOCGI::Popup->new(name => 'payment_subtype_id', sql => $sql_payment_subtype );
$popPaymentSubtypeId->head('-10','Please Select');
$popPaymentSubtypeId->default(-10);
if($PARAM{payment_subtype_id} > -1) {
   $popPaymentSubtypeId->default($PARAM{payment_subtype_id});
}
$search_table->put(4,1,  $popPaymentSubtypeId);

print '<div class="posm_div"><span class="posm_span">Authority Payment Mask</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);

if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );

   my $page_table = $pageNav->table;

   display $page_table;
}

print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
