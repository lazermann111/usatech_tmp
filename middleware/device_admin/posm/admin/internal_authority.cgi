#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Internal_authority->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Internal_authority->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Internal_authority->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Internal_authority->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('internal_authority_name_id').value == '') {
		  alert("Please insert Internal Authority Name");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @objects = USAT::DeviceAdmin::DBObj::Internal_authority->objects;

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
$table->put($rownum,$colnum++,' ',                        'class=header0');
$table->put($rownum,$colnum++,'ID',                       'class=header0');
$table->put($rownum,$colnum++,'Name',                     'class=header0');
$table->put($rownum,$colnum++,'Authority Service Type ID','class=header0');
$rownum++;
$colnum = 0;
my $radio_no = 0;
my $sql = 'SELECT authority_service_type_id, authority_service_type_name FROM authority.authority_service_type';
foreach my $obj ( @objects ) {
   my $ID = $obj->ID;
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio);
   $table->put($rownum,$colnum++,$obj->internal_authority_id);
   $table->put($rownum,$colnum++,$obj->Text('internal_authority_name',    { sz_ml => [25,60] } ));
   $table->put($rownum,$colnum++,$obj->Popup('authority_service_type_id', { sql   => $sql } ));
   $rownum++;
   $colnum=0;
   $radio_no++;
} 

$table->put($rownum,$colnum++,B('Insert->'));
$table->put($rownum,$colnum++,$object->internal_authority_id);
$table->put($rownum,$colnum++,$object->Text('internal_authority_name',
		{ sz_ml => [25,60], id => 'internal_authority_name_id' } ));
$table->put($rownum,$colnum++,$object->Popup('authority_service_type_id', { sql   => $sql } ));

print '<div class="posm_div"><span class="posm_span">Internal Authority</span> Table Admin</div>';
display $table;
BR;
SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } );
NBSP(3);
SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );
NBSP(3);
SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );

print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
