#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;

my $pageNav  = OOCGI::PageNavigation->new(15);
#my $pageSort = OOCGI::PageSort->new();

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my $object = USAT::DeviceAdmin::DBObj::Merchant->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Merchant->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Merchant->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Merchant->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('merchant_name_id').value == '') {
		  alert("Please insert Merchant Name");
		  return false;
	  }
      if (document.getElementById('merchant_desc_id').value == '') {
		  alert("Please insert Merchant Desc");
		  return false;
	  }
      if (document.getElementById('merchant_cd_id').value == '') {
		  alert("Please insert Merchant CD");
		  return false;
	  }
      if (document.getElementById('merchant_bus_name_id').value == '') {
		  alert("Please insert Merchant Bus Name");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @table_cols = (
        ['Merchant ID'       , 1],
        ['Merchant Name'     , 2],
        ['Merchant Desc'     , 3],
        ['Merchant CD'       , 4],
        ['Merchant Bus Name' , 5],
        ['Authority ID'      , 6]
);

my @sql_cols = (
        'merchant_id',
        'merchant_name',
        'merchant_desc',
        'merchant_cd',
        'merchant_bus_name',
        'authority_id'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ FROM merchant WHERE merchant_id > 0 };

my @bind = ();
if($PARAM{merchant_name} ne '') {
   $sql_data .= " AND LOWER(merchant_name) like ? ";
   push(@bind, lc "$PARAM{merchant_name}%");
}
if($PARAM{merchant_desc} ne '') {
   $sql_data .= " AND LOWER(merchant_desc) like ? ";
   push(@bind, lc "$PARAM{merchant_desc}%");
}
if($PARAM{merchant_cd} ne '') {
   $sql_data .= " AND LOWER(merchant_cd) like ? ";
   push(@bind, lc "$PARAM{merchant_cd}%");
}
if($PARAM{merchant_bus_name} ne '') {
   $sql_data .= " AND LOWER(merchant_bus_name) = ? ";
   push(@bind, lc "$PARAM{merchant_bus_name}");
}
if($PARAM{authority_id} ne '' && $PARAM{authority_id} > 0) {
   $sql_data .= " AND authority_id = ? ";
   push(@bind, "$PARAM{authority_id}");
}

#print "DDD $sql_data <br>";

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql_data, \@bind);

#my @objects = USAT::DeviceAdmin::DBObj::Merchant->objects;

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
my $radio_no = 0;
my $sql = 'SELECT authority_id, authority_name FROM authority.authority';
#my $sql_state_id = 'SELECT terminal_state_id,terminal_state_name FROM pss.terminal_state';
my $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
$table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
$rownum++; $colnum=0;

while(my %hash = $qo->next_hash) {
   my $ID = $hash{merchant_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Merchant->new($ID);
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio,'rowspan=6');
   $table->put($rownum,$colnum++,$column_sort[0],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->merchant_id);
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[1],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('merchant_name',     { sz_ml => [50,255] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[2],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('merchant_desc',     { sz_ml => [50,255] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[3],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('merchant_cd',     { sz_ml => [50,255] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[4],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('merchant_bus_name', { sz_ml => [50,255] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[5],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Popup('authority_id',     { sql => $sql } ));
   $rownum++; $colnum=0;
   $radio_no++;
} 
if($rownum == 1) {
   $table->put($rownum,$colnum,B('NO RECORDS','red',4),'align=center colspan=3');
} else {
   $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
   $table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
}
$rownum++; $colnum=0;

$table->put($rownum,$colnum++,B('Insert->'),'rowspan=6');
$table->put($rownum,$colnum++,'Merchant ID',          'class=leftheader');
$table->put($rownum,$colnum,$object->merchant_id);
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Merchant Name', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('merchant_name', { sz_ml => [50,255], id => 'merchant_name_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Merchant Desc', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('merchant_desc', { sz_ml => [50,255], id => 'merchant_desc_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Merchant CD', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('merchant_cd',     { sz_ml => [50,255], id => 'merchant_cd_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Merchant Bus Name', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('merchant_bus_name', { sz_ml => [50,255], id => 'merchant_bus_name_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Authority ID', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Popup('authority_id', { sql => $sql } ));
$rownum++; $colnum=1;

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Merchant Name: ',     'class=leftheader');
my $txtMerchantName = OOCGI::Text->new(name => 'merchant_name', value => $PARAM{merchant_name}, sz_ml => [50,255]);
$search_table->put(0,1,  $txtMerchantName,'bgcolor=lightgreen');
$search_table->put(1,0, 'Merchant Desc: ',     'class=leftheader');
my $txtMerchantDesc = OOCGI::Text->new(name => 'merchant_desc', value => $PARAM{merchant_desc}, sz_ml => [50,255]);
$search_table->put(1,1,  $txtMerchantDesc,'bgcolor=lightgreen');
$search_table->put(2,0, 'Merchant CD: ',       'class=leftheader');
my $txtMerchantCd = OOCGI::Text->new(name =>   'merchant_cd', value => $PARAM{merchant_cd}, sz_ml => [50,255]);
$search_table->put(2,1,  $txtMerchantCd);
$search_table->put(3,0, 'Merchant Bus Name: ', 'class=leftheader');
my $txtMerchantBusName = OOCGI::Text->new(name => 'merchant_bus_name', value => $PARAM{merchant_bus_name}, sz_ml => [50,255]);
$search_table->put(3,1,  $txtMerchantBusName,'bgcolor=lightgreen');
$search_table->put(4,0, 'Authority Id: ', 'class=leftheader');
my $popAuthorityId = OOCGI::Popup->new(name => 'authority_id', sql => $sql );
$popAuthorityId->head('-10','Please Select');
$popAuthorityId->default(-10);
if($PARAM{merchant_state_id} > -1) {
   $popAuthorityId->default($PARAM{authority_id});
}
$search_table->put(4,1,  $popAuthorityId);

print '<div class="posm_div"><span class="posm_span">Merchant</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);
if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" });

   my $page_table = $pageNav->table;

   display $page_table;
}
print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
