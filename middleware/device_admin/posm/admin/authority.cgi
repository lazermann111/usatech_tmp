#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;

my $pageNav  = OOCGI::PageNavigation->new(15);

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Authority->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Authority->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Authority->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Authority->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('authority_name_id').value == '') {
		  alert("Please insert Authority Name");
		  return false;
	  }
      return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @table_cols = (
   ['Authority ID'               , 1],
   ['Authority Name'             , 2],
   ['Authority Type Id'          , 3],
   ['Authority Service Id'       , 4],
   ['Remote Server Addr'         , 5],
   ['Remote Server Addr Alt'     , 6],
   ['Remote Server Port Num'     , 7]
);

my @sql_cols = (
   'authority_id',
   'authority_name',
   'authority_type_id',
   'authority_service_id',
   'remote_server_addr',
   'remote_server_addr_alt',
   'remote_server_port_num'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ FROM authority.authority WHERE authority_id > 0 };

my @bind = ();
if($PARAM{authority_name} ne '') {
   $sql_data .= " AND LOWER(authority_name) like ? ";
   push(@bind, lc "$PARAM{authority_name}%");
}
if($PARAM{authority_type_id} ne '' && $PARAM{authority_type_id} > 0) {
   $sql_data .= " AND authority_type_id = ? ";
   push(@bind, "$PARAM{authority_type_id}");
}
if($PARAM{authority_service_id} ne '' && $PARAM{authority_service_id} > 0) {
   $sql_data .= " AND authority_service_id = ? ";
   push(@bind, "$PARAM{authority_service_id}");
}

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

#print "DDD $sql_data <br>";

my $qo   = $pageNav->get_result_obj($sql_data, \@bind );

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
$table->put($rownum,$colnum++,' ',    'class=header0');
$table->put($rownum,$colnum++,$column_sort[0], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[1], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[2], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[3], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[4], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[5], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[6], 'class=header0');
$rownum++;
$colnum = 0;
my $radio_no = 0;
my $sql_type_id    = 'SELECT authority_type_id,    authority_type_name    FROM authority.authority_type';
my $sql_service_id = 'SELECT authority_service_id, authority_service_name FROM authority.authority_service';
while(my %hash = $qo->next_hash) {
   my $ID = $hash{authority_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Authority->new($ID);
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio);
   $table->put($rownum,$colnum++,$obj->authority_id);
   $table->put($rownum,$colnum++,$obj->Text('authority_name',        { sz_ml => [25,60] } ));
   $table->put($rownum,$colnum++,$obj->Popup('authority_type_id',    { sql => $sql_type_id } ));
   $table->put($rownum,$colnum++,$obj->Popup('authority_service_id', { sql => $sql_service_id } ));
   $table->put($rownum,$colnum++,$obj->Text('remote_server_addr',    { sz_ml => [20,255] } ));
   $table->put($rownum,$colnum++,$obj->Text('remote_server_addr_alt',{ sz_ml => [20,255] } ));
   $table->put($rownum,$colnum++,$obj->Text('remote_server_port_num',{ sz_ml => [6,5] } ));
   $rownum++;
   $colnum=0;
   $radio_no++;
} 

$table->put($rownum,$colnum++,B('Insert->'),'colspan=2');
$table->put($rownum,$colnum++,$object->authority_id);
$table->put($rownum,$colnum++,$object->Text('authority_name',        { sz_ml => [25,60], id => 'authority_name_id' } ));
$table->put($rownum,$colnum++,$object->Popup('authority_type_id',    { sql => $sql_type_id } ));
$table->put($rownum,$colnum++,$object->Popup('authority_service_id', { sql => $sql_service_id } ));
$table->put($rownum,$colnum++,$object->Text('remote_server_addr',    { sz_ml => [20,255] } ));
$table->put($rownum,$colnum++,$object->Text('remote_server_addr_alt',{ sz_ml => [20,255] } ));
$table->put($rownum,$colnum++,$object->Text('remote_server_port_num',{ sz_ml => [6,5] } ));

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Authority Name: ',     'class=leftheader');
my $txtAuthorityName = OOCGI::Text->new(name => 'authority_name', value => $PARAM{authority_name}, sz_ml => [25,60]);
$search_table->put(0,1,  $txtAuthorityName, 'bgcolor=lightgreen');

$search_table->put(1,0, 'Authority Type ID: ',       'class=leftheader');
my $popAuthorityTypeId = OOCGI::Popup->new(name => 'authority_type_id', sql => $sql_type_id );
$popAuthorityTypeId->head('-10','Please Select');
$popAuthorityTypeId->default(-10);
if($PARAM{authority_type_id} > -1) {
   $popAuthorityTypeId->default($PARAM{authority_type_id});
}
$search_table->put(1,1,  $popAuthorityTypeId);

$search_table->put(2,0, 'Authority Service ID: ',       'class=leftheader');
my $popAuthorityServiceId = OOCGI::Popup->new(name => 'authority_service_id', sql => $sql_service_id );
$popAuthorityServiceId->head('-10','Please Select');
$popAuthorityServiceId->default(-10);
if($PARAM{authority_service_id} > -1) {
   $popAuthorityServiceId->default($PARAM{authority_service_id});
}
$search_table->put(2,1,  $popAuthorityServiceId);

print '<div class="posm_div"><span class="posm_span">Authority</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);
if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } );
   NBSP(3);
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" });
   NBSP(3);
   SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
}
print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
