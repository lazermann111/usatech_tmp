#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;

my $pageNav  = OOCGI::PageNavigation->new(15);

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Authority_type->new();

if($PARAM{userOP} eq 'Delete') {
   OOCGI::Query->delete('authority.authority_type_gateway', { authority_type_id => $PARAM{radio_name} } );
   my $obj = USAT::DeviceAdmin::DBObj::Authority_type->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object = $object->insert($query);
   my $ID  = $object->ID;
   $object = USAT::DeviceAdmin::DBObj::Authority_type->new();
   if(defined $PARAM{insert_authority_gateway_id} && $PARAM{insert_authority_gateway_id} > 0) {
      OOCGI::Query->insert('authority.authority_type_gateway', 
                           { authority_type_id    => $ID,
                             authority_gateway_id => $PARAM{insert_authority_gateway_id} });
   }
}

if($PARAM{userOP} eq 'Update') {
   if(!defined $PARAM{"present_"}) {
      OOCGI::Query->delete('authority.authority_type_gateway', 
                           { authority_type_id    => $PARAM{radio_name},
                             authority_gateway_id => $PARAM{"present_$PARAM{radio_name}"} });
   }
   if(!defined $PARAM{"absent_"} && defined $PARAM{"absent_$PARAM{radio_name}"} && $PARAM{"absent_$PARAM{radio_name}"} ne '') {
      OOCGI::Query->insert('authority.authority_type_gateway', 
                           { authority_type_id    => $PARAM{radio_name},
                             authority_gateway_id => $PARAM{"absent_$PARAM{radio_name}"} });
   }
   my $obj = USAT::DeviceAdmin::DBObj::Authority_type->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('authority_type_name_id').value == '') {
		  alert("Please insert Authority Type Name");
		  return false;
	  }
      if (document.getElementById('authority_type_desc_id').value == '') {
		  alert("Please insert Authority Type Desc");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @table_cols = (
   ['Authority Type ID'   , 1],
   ['Authority Type Name' , 2],
   ['Authority Type Desc' , 3],
   ['Handler Id'          , 4],
);

my @sql_cols = (
   'authority_type_id',
   'authority_type_name',
   'authority_type_desc',
   'handler_id'
);

#my @objects = USAT::DeviceAdmin::DBObj::Authority_type->objects;

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ FROM authority.authority_type WHERE authority_type_id > 0 };

my @bind = ();
if($PARAM{authority_type_name} ne '') {
   $sql_data .= " AND LOWER(authority_type_name) like ? ";
   push(@bind, lc "$PARAM{authority_type_name}%");
}
if($PARAM{authority_type_desc} ne '') {
   $sql_data .= " AND LOWER(authority_type_desc) like ? ";
   push(@bind, lc "$PARAM{authority_type_desc}%");
}
if($PARAM{authority_type_id} ne '' && $PARAM{authority_type_id} > 0) {
   $sql_data .= " AND authority_type_id = ? ";
   push(@bind, "$PARAM{authority_type_id}");
}
if($PARAM{handler_id} ne '' && $PARAM{handler_id} > 0) {
   $sql_data .= " AND handler_id = ? ";
   push(@bind, "$PARAM{handler_id}");
}

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql_data, \@bind );

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
$table->put($rownum,$colnum++,' ',      'class=header0');
$table->put($rownum,$colnum++,$column_sort[0], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[1], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[2], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[3], 'class=header0');
$table->put($rownum,$colnum++,"Assigned Gateways",'class=header0');
$table->put($rownum,$colnum++,"Available Gateways<br>for Assignment",'class=header0');
$rownum++;
$colnum = 0;
my $radio_no = 0;
my $sql = 'SELECT handler_id, handler_name FROM handler';

my $sql_present = q{SELECT DISTINCT ag.authority_gateway_id, '#' || authority_gateway_priority || ':' authority_gateway_priority, authority_gateway_name
                     FROM authority.authority_gateway ag, authority_type_gateway atg
                    WHERE ag.authority_gateway_id  = atg.authority_gateway_id
                      AND atg.authority_type_id = ?
                    ORDER BY authority_gateway_priority};

my $sql_absent  = q{SELECT DISTINCT authority_gateway_id, '#' || authority_gateway_priority || ':' authority_gateway_priority, authority_gateway_name
                     FROM authority.authority_gateway ag, authority.handler h, authority.authority_type at
                    WHERE at.handler_id = h.handler_id
                      AND ag.handler_id = h.handler_id
                      AND at.handler_id = ?
                      AND authority_gateway_id NOT IN (SELECT authority_gateway_id FROM authority_type_gateway WHERE authority_type_id = ?)
                    ORDER BY authority_gateway_priority};
while(my %hash = $qo->next_hash) {
   my $ID = $hash{authority_type_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Authority_type->new($ID);	
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio);
   $table->put($rownum,$colnum++,$obj->authority_type_id,             { sz_ml => [5, 6 ] } );
   $table->put($rownum,$colnum++,$obj->Text('authority_type_name',    { sz_ml => [20,60] } ));
   $table->put($rownum,$colnum++,$obj->Text('authority_type_desc',    { sz_ml => [31,255] } ));
   $table->put($rownum,$colnum++,$obj->Popup('handler_id',            { sql   => $sql  } ));
   my $popGatewayPresent = OOCGI::Popup->new(name => "present_$ID",
                                             sql  => $sql_present,
                                             bind => [$ID]);
   $popGatewayPresent->head('','Select to Remove');
   $popGatewayPresent->default('');
   $table->put($rownum,$colnum++,$popGatewayPresent);
   my $popGatewayAbsent = OOCGI::Popup->new(name => "absent_$ID",
                                             sql  => $sql_absent,
                                             bind => [$obj->handler_id, $ID]);
   $popGatewayAbsent->head('','Select to Add');
   $popGatewayAbsent->default('');
   $table->put($rownum,$colnum++,$popGatewayAbsent);
   $rownum++;
   $colnum=0;
   $radio_no++;
} 

$table->put($rownum,$colnum++,B('Insert->'),'colspan=2');
$table->put($rownum,$colnum++,$object->authority_type_id);
$table->put($rownum,$colnum++,$object->Text('authority_type_name',    { sz_ml => [20,60],  id => 'authority_type_name_id' } ));
$table->put($rownum,$colnum++,$object->Text('authority_type_desc',    { sz_ml => [31,255], id => 'authority_type_desc_id' } ));
$table->put($rownum,$colnum++,$object->Popup('handler_id',            { sql   => $sql  } ));
$colnum++;

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Authority Type Name: ',     'class=leftheader');
my $txtAuthorityTypeName = OOCGI::Text->new(name => 'authority_type_name',
                           value => $PARAM{authority_type_name}, sz_ml => [25,60]);
$search_table->put(0,1,  $txtAuthorityTypeName,'bgcolor=lightgreen');

$search_table->put(1,0, 'Authority Type Desc: ',     'class=leftheader');
my $txtAuthorityTypeDesc = OOCGI::Text->new(name => 'authority_type_desc',
                        value => $PARAM{authority_type_desc}, sz_ml => [25,60]);
$search_table->put(1,1,  $txtAuthorityTypeDesc,'bgcolor=lightgreen');

$search_table->put(2,0, 'Handler ID: ',       'class=leftheader');
my $popHandlerId = OOCGI::Popup->new(name => 'handler_id', sql => $sql );
$popHandlerId->head('-10','Please Select');
$popHandlerId->default(-10);
if($PARAM{handler_id} > -1) {
   $popHandlerId->default($PARAM{handler_id});
}
$search_table->put(2,1,  $popHandlerId);

print '<div class="posm_div"><span class="posm_span">Authority Type</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);

if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } );
   NBSP(3);
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );
   NBSP(3);
   SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
}
print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
