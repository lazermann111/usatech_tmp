#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;

my $pageNav  = OOCGI::PageNavigation->new(15);

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Authority_gateway->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Authority_gateway->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Authority_gateway->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Authority_gateway->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('authority_gateway_name_id').value == '') {
		  alert("Please insert Authority Gateway Name");
		  return false;
	  }
      if (document.getElementById('authority_gateway_addr_id').value == '') {
		  alert("Please insert Authority Gateway Addr");
		  return false;
	  }
      if (document.getElementById('authority_gateway_port_id').value == '') {
		  alert("Please insert Authority Gateway Port");
		  return false;
	  }
      if (document.getElementById('authority_gateway_priority_id').value == '') {
		  alert("Please insert Authority Gateway Priority");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT
my @table_cols = (
   ['Authority Gateway ID'   ,    1],
   ['Authority Gateway Name' ,    2],
   ['Authority Gateway Addr' ,    3],
   ['Authority Gateway Port' ,    4],
   ['Authority Gateway Priority', 5],
   ['Handler Id'                , 6],
);

my @sql_cols = (
   'authority_gateway_id',
   'authority_gateway_name',
   'authority_gateway_addr',
   'authority_gateway_port',
   'authority_gateway_priority',
   'handler_id'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ 
   FROM authority.authority_gateway WHERE authority_gateway_id > 0 };

my @bind = ();
if($PARAM{authority_gateway_name} ne '') {
   $sql_data .= " AND LOWER(authority_gateway_name) like ? ";
   push(@bind, lc "$PARAM{authority_gateway_name}%");
}
if($PARAM{authority_gateway_addr} ne '') {
   $sql_data .= " AND authority_gateway_addr like ? ";
   push(@bind, "$PARAM{authority_gateway_addr}%");
}
if($PARAM{authority_gateway_priority} ne '' && $PARAM{authority_gateway_priority} > 0) {
   $sql_data .= " AND authority_gateway_priority = ? ";
   push(@bind, "$PARAM{authority_gateway_priority}");
}
if($PARAM{handler_id} ne '' && $PARAM{handler_id} > 0) {
   $sql_data .= " AND handler_id = ? ";
   push(@bind, "$PARAM{handler_id}");
}

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql_data, \@bind );

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
$table->put($rownum,$colnum++,' ',             'class=header0');
$table->put($rownum,$colnum++,$column_sort[0], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[1], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[2], 'class=header0');
$table->put($rownum,$colnum++,'Port',          'class=header0');
$table->put($rownum,$colnum++,$column_sort[4], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[5], 'class=header0');
$rownum++;
$colnum = 0;
my $radio_no = 0;
my $sql = 'SELECT handler_id, handler_name FROM handler';
while(my %hash = $qo->next_hash) {
   my $ID = $hash{authority_gateway_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Authority_gateway->new($ID);	
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio);
   $table->put($rownum,$colnum++,$obj->authority_gateway_id,             { sz_ml => [5, 6 ] } );
   $table->put($rownum,$colnum++,$obj->Text('authority_gateway_name',    { sz_ml => [25,60] } ));
   $table->put($rownum,$colnum++,$obj->Text('authority_gateway_addr',    { sz_ml => [25,255] } ));
   $table->put($rownum,$colnum++,$obj->Text('authority_gateway_port',    { sz_ml => [6,5] } ));
   $table->put($rownum,$colnum++,$obj->Text('authority_gateway_priority',{ sz_ml => [8,38] } ));
   $table->put($rownum,$colnum++,$obj->Popup('handler_id',               { sql   => $sql  } ));
   $rownum++;
   $colnum=0;
   $radio_no++;
} 

$table->put($rownum,$colnum++,B('Insert->'),'colspan=2');
$table->put($rownum,$colnum++,$object->authority_gateway_id);
$table->put($rownum,$colnum++,$object->Text('authority_gateway_name',    { sz_ml => [25,60], id => 'authority_gateway_name_id' } ));
$table->put($rownum,$colnum++,$object->Text('authority_gateway_addr',    { sz_ml => [25,255],id => 'authority_gateway_addr_id' } ));
$table->put($rownum,$colnum++,$object->Text('authority_gateway_port',    { sz_ml => [6,5], id => 'authority_gateway_port_id'  } ));
$table->put($rownum,$colnum++,$object->Text('authority_gateway_priority',{ sz_ml => [8,38], id => 'authority_gateway_priority_id' } ));
$table->put($rownum,$colnum++,$object->Popup('handler_id',               { sql   => $sql  } ));

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Authority Gateway Name: ',     'class=leftheader');
my $txtAuthorityGatewayName = OOCGI::Text->new(name => 'authority_gateway_name',
                       	 value => $PARAM{authority_gateway_name}, sz_ml => [40,60]);
$search_table->put(0,1,  $txtAuthorityGatewayName,'bgcolor=lightgreen');

$search_table->put(1,0, 'Authority Gateway Addr: ',     'class=leftheader');
my $txtAuthorityGatewayAddr = OOCGI::Text->new(name => 'authority_gateway_addr', 
	                     value => $PARAM{authority_gateway_addr}, sz_ml => [40,255]);
$search_table->put(1,1,  $txtAuthorityGatewayAddr);

$search_table->put(2,0, 'Authority Gateway Priority: ',     'class=leftheader');
my $txtAuthorityGatewayPriority = OOCGI::Text->new(name => 'authority_gateway_priority', 
	                     value => $PARAM{authority_gateway_priority}, sz_ml => [40,60]);
$search_table->put(2,1,  $txtAuthorityGatewayPriority);

$search_table->put(3,0, 'Handler ID: ',       'class=leftheader');
my $popHandlerId = OOCGI::Popup->new(name => 'handler_id', sql => $sql );
$popHandlerId->head('-10','Please Select');
$popHandlerId->default(-10);
if($PARAM{handler_id} > -1) {
   $popHandlerId->default($PARAM{handler_id});
}
$search_table->put(3,1,  $popHandlerId);

print '<div class="posm_div"><span class="posm_span">Authority Gateway</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);
if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } );
   NBSP(3);
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );
   NBSP(3);
   SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
}
print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
