#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;

my $pageNav  = OOCGI::PageNavigation->new(15);

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Handler->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Handler->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Handler->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Handler->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('handler_name_id').value == '') {
		  alert("Please insert Handler Name");
		  return false;
	  }
      if (document.getElementById('handler_class_id').value == '') {
		  alert("Please insert Handler Class");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @table_cols = (
   ['Handler ID'               , 1],
   ['Handler Name'             , 2],
   ['Handler Class'            , 3],
);

my @sql_cols = (
   'handler_id',
   'handler_name',
   'handler_class'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ FROM authority.handler WHERE handler_id > 0 };

my @bind = ();
if($PARAM{handler_name} ne '') {
   $sql_data .= " AND LOWER(handler_name) like ? ";
   push(@bind, lc "$PARAM{handler_name}%");
}
if($PARAM{handler_class} ne '') {
   $sql_data .= " AND LOWER(handler_class) like ? ";
   push(@bind, lc "$PARAM{handler_class}%");
}

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

#print "DDD $sql_data <br>";

my $qo   = $pageNav->get_result_obj($sql_data, \@bind );

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
$table->put($rownum,$colnum++,' ','class=header0');
$table->put($rownum,$colnum++,'Handler ID',   'class=header0');
$table->put($rownum,$colnum++,'Handler Name', 'class=header0');
$table->put($rownum,$colnum++,'Handler Class','class=header0');
$rownum++;
$colnum = 0;
my $radio_no = 0;
while(my %hash = $qo->next_hash) {
   my $ID = $hash{handler_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Handler->new($ID);
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio);
   $table->put($rownum,$colnum++,$obj->handler_id);
   $table->put($rownum,$colnum++,$obj->Text('handler_name', { sz_ml => [40,60]  } ));
   $table->put($rownum,$colnum++,$obj->Text('handler_class',{ sz_ml => [40,255] } ));
   $rownum++;
   $colnum=0;
   $radio_no++;
} 

$table->put($rownum,$colnum++,B('Insert->'),'colspan=2');
$table->put($rownum,$colnum++,$object->handler_id);
$table->put($rownum,$colnum++,$object->Text('handler_name', { sz_ml => [40,60],  id => 'handler_name_id'  } ));
$table->put($rownum,$colnum++,$object->Text('handler_class',{ sz_ml => [40,255], id => 'handler_class_id' } ));

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Handler Name: ',     'class=leftheader');
my $txtHandlerName = OOCGI::Text->new(name => 'handler_name', value => $PARAM{handler_name}, sz_ml => [25,60]);
$search_table->put(0,1,  $txtHandlerName, 'bgcolor=lightgreen');

$search_table->put(1,0, 'Handler Class: ',     'class=leftheader');
my $txtHandlerClass = OOCGI::Text->new(name => 'handler_class', value => $PARAM{handler_class}, sz_ml => [25,60]);
$search_table->put(1,1,  $txtHandlerClass, 'bgcolor=lightgreen');

print '<div class="posm_div"><span class="posm_span">Handler</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);
if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } );
   NBSP(3);
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );
   NBSP(3);
   SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
}
print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
