#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Tran_line_item_type->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Tran_line_item_type->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Tran_line_item_type->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Tran_line_item_type->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('tran_line_item_type_desc_id').value == '') {
		  alert("Please insert Tran Line Item Type Desc");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my $sql2 = 'SELECT * FROM tran_line_item_type ORDER BY tran_line_item_type_id';
my @objects = USAT::DeviceAdmin::DBObj::Tran_line_item_type->objects($sql2);

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
$table->put($rownum,$colnum++,' ',                            'class=header0');
$table->put($rownum,$colnum++,'ID',                           'class=header0');
$table->put($rownum,$colnum++,'Tran Line Item Type Desc',     'class=header0');
$table->put($rownum,$colnum++,'Tran Line Item Type Sign PN',  'class=header0');
$table->put($rownum,$colnum++,'Tran Line Item Type Group CD', 'class=header0');
$rownum++;
$colnum = 0;
my $radio_no = 0;

my $sql = 'SELECT tran_line_item_type_group_cd, tran_line_item_type_group_name FROM pss.tran_line_item_type_group';
foreach my $obj ( @objects ) {
   my $ID = $obj->ID;
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio);
   $table->put($rownum,$colnum++,$obj->tran_line_item_type_id);
   $table->put($rownum,$colnum++,$obj->Text('tran_line_item_type_desc',      { sz_ml => [45,60] } ));
   $table->put($rownum,$colnum++,$obj->Popup('tran_line_item_type_sign_pn',
                                       { pair  => ['P','P','N','N'] } ));
   $table->put($rownum,$colnum++,$obj->Popup('tran_line_item_type_group_cd', { sql => $sql } ));
   $rownum++;
   $colnum=0;
   $radio_no++;
} 

$table->put($rownum,$colnum++,B('Insert->'));
$table->put($rownum,$colnum++,$object->Text('tran_line_item_type_id'));
$table->put($rownum,$colnum++,$object->Text('tran_line_item_type_desc',
   		{ sz_ml => [45,60], id => 'tran_line_item_type_desc_id' } ));
$table->put($rownum,$colnum++,$object->Popup('tran_line_item_type_sign_pn',
                                       { pair  => ['P','P','N','N'] } ));
$table->put($rownum,$colnum++,$object->Popup('tran_line_item_type_group_cd',
                                       { sql   => $sql } ));

print '<div class="posm_div"><span class="posm_span">Tran Line Item Type</span> Table Admin</div>';
display $table;
BR;
SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } );
NBSP(3);
SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );
#NBSP(3);
#SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );

print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
