#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;

my $pageNav  = OOCGI::PageNavigation->new(15);
#my $pageSort = OOCGI::PageSort->new();

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Payment_subtype->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Payment_subtype->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Payment_subtype->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Payment_subtype->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('payment_subtype_name_id').value == '') {
		  alert("Please insert Payment Subtype Name");
		  return false;
	  }
      if (document.getElementById('payment_subtype_class_id').value == -10) {
		  alert("Please choose a Handler");
		  return false;
	  }
      if (document.getElementById('payment_subtype_key_name_id').value == '') {
		  alert("Please insert Payment Subtype Key Name");
		  return false;
	  }
      if (document.getElementById('payment_subtype_table_name_id').value == '') {
		  alert("Please insert Payment Subtype Table Name");
		  return false;
	  }
      if (document.getElementById('payment_subtype_key_desc_name_id').value == '') {
		  alert("Please insert Payment Subtype Key Desc Name");
		  return false;
	  }
	  return confirmSubmit();
   }
JSCRIPT

my $sql_ps_class_detail = q{
	SELECT * from 
	(SELECT handler_class, handler_name,
		   'TERMINAL_ID' payment_subtype_key_name,
		   'TERMINAL' payment_subtype_table_name,
		   'TERMINAL_DESC' payment_subtype_key_desc_name
	  FROM authority.handler
	UNION SELECT '-10', ' Please Select','','',''
	  FROM DUAL
	UNION                       /* legacy handlers with unique authority tables */
	SELECT 'Aramark', 'Aramark','ARAMARK_PAYMENT_TYPE_ID','ARAMARK_PAYMENT_TYPE','ARAMARK_PAYMENT_TYPE_DESC'
	  FROM DUAL
	UNION SELECT 'Banorte', 'Banorte Financial','BANORTE_TERMINAL_ID','BANORTE_TERMINAL','BANORTE_TERMINAL_DESC'
	  FROM DUAL
	UNION SELECT 'BlackBoard', 'BlackBoard','BLACKBRD_TENDER_ID','BLACKBRD_TENDER','BLACKBRD_TENDER_NAME'
	  FROM DUAL
	UNION SELECT 'Cash', 'Cash','Not Applicable','Not Applicable','Not Applicable'
	  FROM DUAL
	UNION SELECT 'Internal', 'USAT Special Card', 'INTERNAL_PAYMENT_TYPE_ID','INTERNAL_PAYMENT_TYPE','INTERNAL_PAYMENT_TYPE_DESC'
	  FROM DUAL
	)
};
my $qo_handlers = OOCGI::Query->new($sql_ps_class_detail);
my @handler_js_arr;
$DB::single=1;
while (my %d = $qo_handlers->next_hash) {
	push @handler_js_arr, '['.join(',', map { qq{"$_"} } @d{qw/handler_class handler_name payment_subtype_key_name payment_subtype_table_name payment_subtype_key_desc_name/}).']';
}
my $handler_js_arr = "var handler_module_arr = [" . join(',', @handler_js_arr) . "];";

print <<JSCRIPT;

   $handler_js_arr

   function handler_module_config(handler, id) {
      for (var i = 0; i < handler_module_arr.length; i++) {
         if (handler == handler_module_arr[i][0]) {
            document.getElementById('payment_subtype_key_name_' + id).value = handler_module_arr[i][2];
            document.getElementById('payment_subtype_table_name_' + id).value = handler_module_arr[i][3];
            document.getElementById('payment_subtype_key_desc_name_' + id).value = handler_module_arr[i][4];
            break;
         }
      }
      return true;
   }
-->
</script>
JSCRIPT

my @table_cols = (
        ['Payment Subtype ID'           , 1],
        ['Payment Subtype Name'         , 2],
        ['Payment Subtype Desc'         , 3],
        ['Handler'                      , 4],
        ['Payment Subtype Key Name'     , 5],
        ['Payment Subtype Table Name'   , 6],
        ['Payment Subtype Key Desc Name', 7],
        ['Client Payment Type'          , 8],
        ['Default Authority Payment Mask', 9],
);

my @sql_cols = (
        'payment_subtype_id',
        'payment_subtype_name',
        'payment_subtype_desc',
        'payment_subtype_class',
        'payment_subtype_key_name',
        'payment_subtype_table_name',
        'payment_subtype_key_desc_name',
        'client_payment_type_cd',
        'authority_payment_mask_id',
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ FROM payment_subtype WHERE payment_subtype_id > 0 };

my @bind = ();
if($PARAM{payment_subtype_name} ne '') {
   $sql_data .= " AND LOWER(payment_subtype_name) like ? ";
   push(@bind, lc "$PARAM{payment_subtype_name}%");
}
if($PARAM{payment_subtype_desc} ne '') {
   $sql_data .= " AND LOWER(payment_subtype_desc) like ? ";
   push(@bind, lc "$PARAM{payment_subtype_desc}%");
}
if($PARAM{payment_subtype_class} ne '' && $PARAM{payment_subtype_class} ne '-10') {
   $sql_data .= " AND LOWER(payment_subtype_class) like ? ";
   push(@bind, lc "$PARAM{payment_subtype_class}%");
}
if($PARAM{payment_subtype_key_name} ne '') {
   $sql_data .= " AND LOWER(payment_subtype_key_name) like ? ";
   push(@bind, lc "$PARAM{payment_subtype_key_name}%");
}
if($PARAM{payment_subtype_table_name} ne '') {
   $sql_data .= " AND LOWER(payment_subtype_table_name) like ? ";
   push(@bind, lc "$PARAM{payment_subtype_table_name}%");
}
if($PARAM{payment_subtype_key_desc_name} ne '') {
   $sql_data .= " AND LOWER(payment_subtype_key_desc_name) like ? ";
   push(@bind, lc "$PARAM{payment_subtype_key_desc_name}%");
}
if($PARAM{client_payment_type_cd} ne '' && $PARAM{client_payment_type_cd} ne '-10') {
   $sql_data .= " AND LOWER(client_payment_type_cd) like ? ";
   push(@bind, lc "$PARAM{client_payment_type_cd}%");
}
if($PARAM{authority_payment_mask_id} ne '' && $PARAM{authority_payment_mask_id} > 0) {
   $sql_data .= " AND authority_payment_mask_id = ? ";
   push(@bind, lc "$PARAM{authority_payment_mask_id}");
}

#print "DDD $sql_data <br>";

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

#print "DDD $sql_data <br>";

my $qo   = $pageNav->get_result_obj($sql_data, \@bind );

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
my $radio_no = 0;
my $sql_mask_id = q{SELECT apm.authority_payment_mask_id, payment_subtype_name || ' - ' || authority_payment_mask_name
                         FROM pss.authority_payment_mask apm, pss.payment_subtype ps
                        WHERE apm.payment_subtype_id = ps.payment_subtype_id};
my $sql_client_payment_type = q{SELECT client_payment_type_cd, client_payment_type_desc FROM pss.client_payment_type};
my $sql_ps_class = qq{SELECT handler_class, handler_name FROM ($sql_ps_class_detail)};
my $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
$table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
$rownum++; $colnum=0;

my $rowid=0;
while(my %hash = $qo->next_hash) {
   $rowid++;
   my $ID = $hash{payment_subtype_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Payment_subtype->new($ID);
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio,'rowspan=9');
   $table->put($rownum,$colnum++,$column_sort[0],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->payment_subtype_id);
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[1],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('payment_subtype_name',        { sz_ml => [40,60] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[2],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('payment_subtype_desc',        { sz_ml => [40,60] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[3],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Popup('payment_subtype_class',
      { sql => $sql_ps_class, onChange => "javascript:handler_module_config(this.options[this.selectedIndex].value, $rowid)" } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[4],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('payment_subtype_key_name',   { sz_ml => [30,30], id => "payment_subtype_key_name_$rowid" } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[5],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('payment_subtype_table_name',   { sz_ml => [30,30], id => "payment_subtype_table_name_$rowid" } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[6],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('payment_subtype_key_desc_name',   { sz_ml => [30,30], id => "payment_subtype_key_desc_name_$rowid" } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[7],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Popup('client_payment_type_cd',   { sql => $sql_client_payment_type } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[8],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Popup('authority_payment_mask_id', { sql   => $sql_mask_id } ));
   $rownum++; $colnum=0;
#  my $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
#                      NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
#  $table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
#  $rownum++; $colnum=0;
   $radio_no++;
} 

if($rownum == 1) {
   $table->put($rownum,$colnum,B('NO RECORDS','red',4),'align=center colspan=3');
} else {
   $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
   $table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
}
$rownum++; $colnum=0;

$table->put($rownum,$colnum++,B('Insert->'),'rowspan=9');
$table->put($rownum,$colnum++,'Payment Subtype ID',          'class=leftheader');
$table->put($rownum,$colnum,$object->payment_subtype_id);
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Payment Subtype Name', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('payment_subtype_name',
        		{ sz_ml => [40,60], id => 'payment_subtype_name_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Payment Subtype Desc', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('payment_subtype_desc',         { sz_ml => [40,60] } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Handler', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Popup('payment_subtype_class',
   { sql => $sql_ps_class, onChange => "javascript:handler_module_config(this.options[this.selectedIndex].value, 'id')",
    id => 'payment_subtype_class_id'} ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Payment Subtype Key Name',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('payment_subtype_key_name',
	     	{ sz_ml => [30,30], id => 'payment_subtype_key_name_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Payment Subtype Table Name',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('payment_subtype_table_name',
	 	{ sz_ml => [30,30], id => 'payment_subtype_table_name_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Payment Subtype Key Desc Name',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('payment_subtype_key_desc_name',
	 	{ sz_ml => [30,30], id => 'payment_subtype_key_desc_name_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Client Payment Type',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Popup('client_payment_type_cd',   { sql => $sql_client_payment_type } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Default Authority Payment Mask',     'class=leftheader');
$table->put($rownum,$colnum,   $object->Popup('authority_payment_mask_id', { sql   => $sql_mask_id } ));
$rownum++; $colnum=1;

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Payment Subtype Name: ',     'class=leftheader');
my $txtAuthorityPaymentMaskName = OOCGI::Text->new(name => 'payment_subtype_name',
      	value => $PARAM{payment_subtype_name}, sz_ml => [40,60]);
$search_table->put(0,1,  $txtAuthorityPaymentMaskName,'bgcolor=lightgreen');

$search_table->put(1,0, 'Payment Subtype Desc: ',       'class=leftheader');
my $txtAuthorityPaymentMaskDesc = OOCGI::Text->new(name => 'payment_subtype_desc',
     	value => $PARAM{payment_subtype_desc}, sz_ml => [40,60]);
$search_table->put(1,1,  $txtAuthorityPaymentMaskDesc,'bgcolor=lightgreen');

$search_table->put(2,0, 'Handler: ',       'class=leftheader');
my $txtAuthorityPaymentMaskClass = OOCGI::Popup->new(name => 'payment_subtype_class', sql => $sql_ps_class);
if($PARAM{payment_subtype_class} ne '-10') {
   $txtAuthorityPaymentMaskClass->default($PARAM{payment_subtype_class});
}
$search_table->put(2,1,  $txtAuthorityPaymentMaskClass,'bgcolor=lightgreen');

$search_table->put(3,0, 'Payment Subtype Key Name: ',       'class=leftheader');
my $txtAuthorityPaymentMaskKeyName = OOCGI::Text->new(name => 'payment_subtype_key_name',
     	value => $PARAM{payment_subtype_key_name}, sz_ml => [30,30]);
$search_table->put(3,1,  $txtAuthorityPaymentMaskKeyName,'bgcolor=lightgreen');

$search_table->put(4,0, 'Payment Subtype Table Name: ',       'class=leftheader');
my $txtAuthorityPaymentMaskTableName = OOCGI::Text->new(name => 'payment_subtype_table_name',
     	value => $PARAM{payment_subtype_table_name}, sz_ml => [30,30]);
$search_table->put(4,1,  $txtAuthorityPaymentMaskTableName,'bgcolor=lightgreen');

$search_table->put(5,0, 'Payment Subtype Key Desc Name: ',       'class=leftheader');
my $txtAuthorityPaymentMaskKeyDescName = OOCGI::Text->new(name => 'payment_subtype_key_desc_name',
     	value => $PARAM{payment_subtype_key_desc_name}, sz_ml => [30,30]);
$search_table->put(5,1,  $txtAuthorityPaymentMaskKeyDescName,'bgcolor=lightgreen');

$search_table->put(6,0, 'Client Payment Type: ',       'class=leftheader');
my $txtClientPaymentTypeCd = OOCGI::Popup->new(name => 'client_payment_type_cd', sql => $sql_client_payment_type);
$txtClientPaymentTypeCd->head('-10','Please Select');
$txtClientPaymentTypeCd->default(-10);
if($PARAM{client_payment_type_cd} ne '-10') {
   $txtClientPaymentTypeCd->default($PARAM{client_payment_type_cd});
}
$search_table->put(6,1,  $txtClientPaymentTypeCd,'bgcolor=lightgreen');

$search_table->put(7,0, 'Default Authority Payment Mask: ',       'class=leftheader');
my $popMaskId = OOCGI::Popup->new(name => 'authority_payment_mask_id', sql => $sql_mask_id );
$popMaskId->head('-10','Please Select');
$popMaskId->default(-10);
if($PARAM{payment_subtype_id} > -1) {
   $popMaskId->default($PARAM{authority_payment_mask_id});
}
$search_table->put(7,1,  $popMaskId);

print '<div class="posm_div"><span class="posm_span">Payment Subtype</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);

if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" });

   my $page_table = $pageNav->table;

   display $page_table;
}

print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
