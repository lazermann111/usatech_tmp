#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;

my $pageNav  = OOCGI::PageNavigation->new(15);

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Authority_assn->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Authority_assn->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Authority_assn->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Authority_assn->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('authority_assn_id_id').value == '') {
		  alert("Please insert Authority Assn ID");
		  return false;
	  }
      if (document.getElementById('authority_assn_name_id').value == '') {
		  alert("Please insert Authority Assn Name");
		  return false;
	  }
      if (document.getElementById('authority_assn_desc_id').value == '') {
		  alert("Please insert Authority Assn Desc");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @table_cols = (
   ['Authority Assn ID'   , 1],
   ['Authority Assn Name' , 2],
   ['Authority Assn Desc' , 3],
);

my @sql_cols = (
   'authority_assn_id',
   'authority_assn_name',
   'authority_assn_desc',
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ FROM authority.authority_assn WHERE authority_assn_id > 0 };

my @bind = ();
if($PARAM{authority_assn_name} ne '') {
   $sql_data .= " AND LOWER(authority_assn_name) like ? ";
   push(@bind, lc "$PARAM{authority_assn_name}%");
}
if($PARAM{authority_type_id} ne '' && $PARAM{authority_assn_desc} > 0) {
   $sql_data .= " AND authority_assn_desc = ? ";
   push(@bind, "$PARAM{authority_assn_desc}");
}

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

#print "DDD $sql_data <br>";

my $qo   = $pageNav->get_result_obj($sql_data, \@bind );

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
$table->put($rownum,$colnum++,' ',    'class=header0');
$table->put($rownum,$colnum++,$column_sort[0], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[1], 'class=header0');
$table->put($rownum,$colnum++,$column_sort[2], 'class=header0');
$rownum++;
$colnum = 0;
my $radio_no = 0;
while(my %hash = $qo->next_hash) {
   my $ID = $hash{authority_assn_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Authority_assn->new($ID);
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio);
   $table->put($rownum,$colnum++,$obj->authority_assn_id);
   $table->put($rownum,$colnum++,$obj->Text('authority_assn_name', { sz_ml => [40,60] } ));
   $table->put($rownum,$colnum++,$obj->Text('authority_assn_desc', { sz_ml => [40,255] } ));
   $rownum++;
   $colnum=0;
   $radio_no++;
} 

$table->put($rownum,$colnum++,B('Insert->'),'colspan=1');
$table->put($rownum,$colnum++,$object->Text('authority_assn_id',   { id => 'authority_assn_id_id' } ));
$table->put($rownum,$colnum++,$object->Text('authority_assn_name', { sz_ml => [40,60], id => 'authority_assn_name_id' } ));
$table->put($rownum,$colnum++,$object->Text('authority_assn_desc', { sz_ml => [40,255], id => 'authority_assn_desc_id' } ));

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Authority Assn Name: ',     'class=leftheader');
my $txtAuthorityName = OOCGI::Text->new(name => 'authority_assn_name', value => $PARAM{authority_assn_name}, sz_ml => [25,60]);
$search_table->put(0,1,  $txtAuthorityName, 'bgcolor=lightgreen');

$search_table->put(1,0, 'Authority Assn Desc: ',     'class=leftheader');
my $txtAuthorityDesc = OOCGI::Text->new(name => 'authority_assn_desc', value => $PARAM{authority_assn_desc}, sz_ml => [25,60]);
$search_table->put(1,1,  $txtAuthorityDesc, 'bgcolor=lightgreen');

print '<div class="posm_div"><span class="posm_span">Authority Assn</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);
if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } );
   NBSP(3);
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );
   NBSP(3);
   SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
}
print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
