#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $table = OOCGI::NTable->new('width=40% border="0"');
my %color = ();
$color{0} = 'bgcolor=lightgreen';
$color{1} = 'bgcolor=cream';
$color{2} = 'bgcolor=lightblue';

my $rownum=0;
$table->put(   $rownum,0, 'Authority', $color{2});
$table->linkto($rownum,0, 'authority.cgi');
$rownum++;
$table->put($   rownum,0, 'Authority Gateway', $color{2});
$table->linkto($rownum,0, 'authority_gateway.cgi');
$rownum++;
$table->put(   $rownum,0, 'Authority Type', $color{2});
$table->linkto($rownum,0, 'authority_type.cgi');
$rownum++;
$table->put(   $rownum,0, 'Authority Assn', $color{2});
$table->linkto($rownum,0, 'authority_assn.cgi');
$rownum++;
$table->put(   $rownum,0, 'Handler', $color{2});
$table->linkto($rownum,0, 'handler.cgi');
$rownum++;
$table->put(   $rownum,0, 'Merchant', $color{2});
$table->linkto($rownum,0, 'merchant.cgi');
$rownum++;
$table->put(   $rownum,0, 'Terminal', $color{2});
$table->linkto($rownum,0, 'terminal.cgi');
$rownum++;
$table->put(   $rownum,0, 'Authority Payment Mask', $color{2});
$table->linkto($rownum,0, 'authority_payment_mask.cgi');
$rownum++;
$table->put(   $rownum,0, 'Payment Subtype', $color{2});
$table->linkto($rownum,0, 'payment_subtype.cgi');
$rownum++;
$table->put(   $rownum,0, 'Aramark Authority', $color{$rownum%2});
$table->linkto($rownum,0, 'aramark_authority.cgi');
$rownum++;
$table->put(   $rownum,0, 'Aramark Payment Type', $color{$rownum%2});
$table->linkto($rownum,0, 'aramark_payment_type.cgi');
$rownum++;
$table->put(   $rownum,0, 'Authority Server', $color{$rownum%2});
$table->linkto($rownum,0, 'authority_server.cgi');
$rownum++;
$table->put(   $rownum,0, 'Authority Service', $color{$rownum%2});
$table->linkto($rownum,0, 'authority_service.cgi');
$rownum++;
$table->put(   $rownum,0, 'Authority Service Type', $color{$rownum%2});
$table->linkto($rownum,0, 'authority_service_type.cgi');
$rownum++;
$table->put(   $rownum,0, 'Banorte Authority', $color{$rownum%2});
$table->linkto($rownum,0, 'banorte_authority.cgi');
$rownum++;
$table->put(   $rownum,0, 'Banorte Terminal', $color{$rownum%2});
$table->linkto($rownum,0, 'banorte_terminal.cgi');
$rownum++;
$table->put(   $rownum,0, 'Blackbrd Authority', $color{$rownum%2});
$table->linkto($rownum,0, 'blackbrd_authority.cgi');
$rownum++;
$table->put(   $rownum,0, 'Blackbrd Tender', $color{$rownum%2});
$table->linkto($rownum,0, 'blackbrd_tender.cgi');
$rownum++;
$table->put(   $rownum,0, 'Internal Authority', $color{$rownum%2});
$table->linkto($rownum,0, 'internal_authority.cgi');
$rownum++;
$table->put(   $rownum,0, 'Internal Payment Type', $color{$rownum%2});
$table->linkto($rownum,0, 'internal_payment_type.cgi');
$rownum++;
$table->put(   $rownum,0, 'Webpos Authority', $color{$rownum%2});
$table->linkto($rownum,0, 'webpos_authority.cgi');
$rownum++;
$table->put(   $rownum,0, 'Webpos Terminal', $color{$rownum%2});
$table->linkto($rownum,0, 'webpos_terminal.cgi');
$rownum++;
$table->put(   $rownum,0, 'Tran Line Item Type', $color{$rownum%2});
$table->linkto($rownum,0, 'tran_line_item_type.cgi');
$rownum++;

BR(2);
print '<div class="posm_div">POSM Admin</div>';
BR(2);
display $table;
BR(2);

print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
