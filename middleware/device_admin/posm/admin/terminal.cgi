#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;

my $pageNav  = OOCGI::PageNavigation->new(15);
#my $pageSort = OOCGI::PageSort->new();

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $object = USAT::DeviceAdmin::DBObj::Terminal->new();

if($PARAM{userOP} eq 'Delete') {
   my $obj = USAT::DeviceAdmin::DBObj::Terminal->new($PARAM{radio_name});
   $obj->delete;
}

if($PARAM{userOP} eq 'Insert') {
   $object->insert($query);
   $object = USAT::DeviceAdmin::DBObj::Terminal->new();
}

if($PARAM{userOP} eq 'Update') {
   my $obj = USAT::DeviceAdmin::DBObj::Terminal->new($PARAM{radio_name});
   $obj->update($query);
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print '<form method="post">';
print <<JSCRIPT;
<script language="JavaScript" type="text/javascript" src="/js/posm_validate.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
   function validateFormInsert() {
      if (document.getElementById('terminal_desc_id').value == '') {
		  alert("Please insert Terminal Desc");
		  return false;
	  }
      if (document.getElementById('terminal_cd_id').value == '') {
		  alert("Please insert Terminal CD");
		  return false;
	  }
      if (document.getElementById('terminal_next_batch_num_id').value == '') {
		  alert("Please insert Terminal Next Batch Num");
		  return false;
	  }
      if (document.getElementById('terminal_max_batch_num_id').value == '') {
		  alert("Please insert Terminal Max Batch Num");
		  return false;
	  }
      if (document.getElementById('terminal_batch_max_tran_id').value == '') {
		  alert("Please insert Terminal Batch Max Tran");
		  return false;
	  }
      if (document.getElementById('terminal_batch_cycle_num_id').value == '') {
		  alert("Please insert Terminal Batch Cycle Num");
		  return false;
	  }
      if (document.getElementById('terminal_min_batch_num_id').value == '') {
		  alert("Please insert Terminal Min Batch Num");
		  return false;
	  }
      if (document.getElementById('terminal_min_batch_close_hr_id').value == '') {
		  alert("Please insert Terminal Min Batch Close HR");
		  return false;
	  }
      if (document.getElementById('terminal_max_batch_close_hr_id').value == '') {
		  alert("Please insert Terminal Max Batch Close HR");
		  return false;
	  }
	  return confirmSubmit();
   }
-->
</script>
JSCRIPT

my @table_cols = (
        ['Terminal ID'                , 1],
        ['Terminal Desc'              , 2],
        ['Terminal CD'                , 3],
        ['Terminal Encrypt Key'       , 4],
        ['Terminal Encrypt Key2'      , 5],
        ['Terminal Next Batch Num'    , 6],
        ['Merchant ID'                , 7],
        ['Terminal State ID'          , 8],
        ['Terminal Max Batch Num'     , 9],
        ['Terminal Batch Max Tran'    ,10],
        ['Terminal Batch Cycle Num'   ,11],
        ['Terminal Min Batch Num'     ,12],
        ['Terminal Min Batch Close Hr',13],
        ['Terminal Max Batch Close Hr',14]
);

my @sql_cols = (
        'terminal_id',
        'terminal_desc',
        'terminal_cd',
        'terminal_encrypt_key',
        'terminal_encrypt_key2',
        'terminal_next_batch_num',
        'merchant_id',
        'terminal_state_id',
        'terminal_max_batch_num',
        'terminal_batch_max_tran',
        'terminal_batch_cycle_num',
        'terminal_min_batch_num',
        'terminal_min_batch_close_hr',
        'terminal_max_batch_close_hr'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ FROM terminal WHERE terminal_id > 0 };

my @bind = ();
if($PARAM{terminal_desc} ne '') {
   $sql_data .= " AND LOWER(terminal_desc) like ? ";
   push(@bind, lc "$PARAM{terminal_desc}%");
}
if($PARAM{terminal_cd} ne '') {
   $sql_data .= " AND LOWER(terminal_cd) like ? ";
   push(@bind, lc "$PARAM{terminal_cd}%");
}
if($PARAM{merchant_id} ne '' && $PARAM{merchant_id} > 0) {
   $sql_data .= " AND merchant_id = ? ";
   push(@bind, "$PARAM{merchant_id}");
}
if($PARAM{terminal_state_id} ne '' && $PARAM{terminal_state_id} > 0) {
   $sql_data .= " AND terminal_state_id = ? ";
   push(@bind, "$PARAM{terminal_state_id}");
}

#print "DDD $sql_data <br>";

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
       push(@column_sort, $column);
}

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

#print "DDD $sql_data <br>";

my $qo   = $pageNav->get_result_obj($sql_data, \@bind );

my $table = OOCGI::NTable->new('width=100% border="1"');

my $rownum = 0;
my $colnum = 0;
my $radio_no = 0;
my $sql_merchant = 'SELECT merchant_id, merchant_name FROM pss.merchant';
my $sql_state_id = 'SELECT terminal_state_id,terminal_state_name FROM pss.terminal_state';

my $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
$table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
$rownum++; $colnum=0;

while(my %hash = $qo->next_hash) {
   my $ID = $hash{terminal_id};
   my $obj   = USAT::DeviceAdmin::DBObj::Terminal->new($ID);
   my $radio = qq{<input type = "radio" name = "radio_name" id="radio_$radio_no" value = "$ID" >};
   $table->put($rownum,$colnum++,$radio,'rowspan=14');
   $table->put($rownum,$colnum++,$column_sort[0],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->terminal_id);
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[1],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_desc',           { sz_ml => [40,255] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[2],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_cd',             { sz_ml => [40,255] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[3],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_encrypt_key',    { sz_ml => [40,2048] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[4],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_encrypt_key2',   { sz_ml => [40,2048] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[5],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_next_batch_num', { sz_ml => [20,20] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[6],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Popup('merchant_id',            { sql   => $sql_merchant } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[7],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Popup('terminal_state_id',      { sql   => $sql_state_id } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[8],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_max_batch_num',  { sz_ml => [20,38] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[9],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_batch_max_tran', { sz_ml => [20,38] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[10],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_batch_cycle_num'));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[11],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_min_batch_num',  { sz_ml => [20,38] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[12],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_min_batch_close_hr', { sz_ml => [20,38] } ));
   $rownum++; $colnum=1;
   $table->put($rownum,$colnum++,$column_sort[13],          'class=leftheader');
   $table->put($rownum,$colnum,   $obj->Text('terminal_max_batch_close_hr', { sz_ml => [20,38] } ));
   $rownum++; $colnum=0;
#  my $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
#                      NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
#  $table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
#  $rownum++; $colnum=0;
   $radio_no++;
} 
if($rownum == 1) {
   $table->put($rownum,$colnum,B('NO RECORDS','red',4),'align=center colspan=3');
} else {
   $update_delete = SUBMIT('userOP','Update', { onClick => "javascript:return validateForm();" } ).
                    NBSP(3).SUBMIT('userOP','Delete', { onClick => "javascript:return validateForm();" } );
   $table->put($rownum,$colnum,$update_delete,'align=center colspan=3');
}
$rownum++; $colnum=0;

$table->put($rownum,$colnum++,B('Insert->'),'rowspan=14');
$table->put($rownum,$colnum++,'Terminal ID',          'class=leftheader');
$table->put($rownum,$colnum,$object->terminal_id);
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Desc', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_desc',
		{ sz_ml => [40,255], id => 'terminal_desc_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal CD', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_cd',
		{ sz_ml => [40,255], id => 'terminal_cd_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Encrypt Key', 'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_encrypt_key',    { sz_ml => [40,2048] } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Encrypt Key2',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_encrypt_key2',   { sz_ml => [40,2048] } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Next Batch Num',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_next_batch_num',
	   	{ sz_ml => [20,20], id => 'terminal_next_batch_num_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Merchant ID',     'class=leftheader');
$table->put($rownum,$colnum,   $object->Popup('merchant_id',            { sql   => $sql_merchant } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal State ID',     'class=leftheader');
$table->put($rownum,$colnum,   $object->Popup('terminal_state_id',      { sql   => $sql_state_id } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Max Batch Num',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_max_batch_num',
	  	{ sz_ml => [20,38], id => 'terminal_max_batch_num_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Batch Max Tran',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_batch_max_tran',
	   	{ sz_ml => [20,38], id => 'terminal_batch_max_tran_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Batch Cycle Num',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_batch_cycle_num',
		{ sz_ml => [20,38], id => 'terminal_batch_cycle_num_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Min Batch Num',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_min_batch_num',
	  	{ sz_ml => [20,38], id => 'terminal_min_batch_num_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Min Batch Close Hr',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_min_batch_close_hr',
	   	{ sz_ml => [20,38], id => 'terminal_min_batch_close_hr_id' } ));
$rownum++; $colnum=1;
$table->put($rownum,$colnum++,'Terminal Max Batch Close Hr',    'class=leftheader');
$table->put($rownum,$colnum,   $object->Text('terminal_max_batch_close_hr',
	   	{ sz_ml => [20,38], id => 'terminal_max_batch_close_hr_id' } ));
$rownum++; $colnum=1;

my $search_table = OOCGI::NTable->new('width=80% border="1"');

$search_table->put(0,0, 'Terminal Desc: ',     'class=leftheader');
my $txtTerminalDesc = OOCGI::Text->new(name => 'terminal_desc', value => $PARAM{terminal_desc}, sz_ml => [40,255]);
$search_table->put(0,1,  $txtTerminalDesc,'bgcolor=lightgreen');
$search_table->put(1,0, 'Terminal CD: ',       'class=leftheader');
my $txtTerminalCd = OOCGI::Text->new(name => 'terminal_cd', value => $PARAM{terminal_cd}, sz_ml => [40,255]);
$search_table->put(1,1,  $txtTerminalCd);
$search_table->put(2,0, 'Merchant ID: ',       'class=leftheader');
my $popMerchantId = OOCGI::Popup->new(name => 'merchant_id', sql => $sql_merchant );
$popMerchantId->head('-10','Please Select');
$popMerchantId->default(-10);
if($PARAM{merchant_id} > -1) {
   $popMerchantId->default($PARAM{merchant_id});
}
$search_table->put(2,1,  $popMerchantId);
$search_table->put(3,0, 'Terminal State ID: ', 'class=leftheader');
my $popTerminalStateId = OOCGI::Popup->new(name => 'terminal_state_id', sql => $sql_state_id );
$popTerminalStateId->head('-10','Please Select');
$popTerminalStateId->default(-10);
if($PARAM{terminal_state_id} > -1) {
   $popTerminalStateId->default($PARAM{terminal_state_id});
}
$search_table->put(3,1,  $popTerminalStateId);

print '<div class="posm_div"><span class="posm_span">Terminal</span> Table Admin</div>';
display $search_table;
BR(2);
SUBMIT('userSearch', 'Search');
HIDDEN('hiddenSearch', 'Search');
BR(2);

if($PARAM{'userSearch'} eq 'Search' || $PARAM{'hiddenSearch'} eq 'Search' ) {
   display $table;
   BR;
   SUBMIT('userOP','Insert', { onClick => "javascript:return validateFormInsert();" } );

   my $page_table = $pageNav->table;

   display $page_table;
}

print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
