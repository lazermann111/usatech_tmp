#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use OOCGI::NTable;
use OOCGI::OOCGI 0.11;
use OOCGI::Query 0.10;
use USAT::DeviceAdmin::UI::ParentLocation;
use USAT::DeviceAdmin::UI::DAHeader;

use strict;

use constant TOOLTIP__1 => q{Enter first couple of Characters of the Location name,  then press Look up for Parent Location button.};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print '<script type="text/javascript" src="/js/ajax.js"></script>';

if(length($PARAM{location_id}) == 0 && !$PARAM{location_name})
{
   print "Required Parameter Not Found: location_id";
   USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
   exit;
}

my $count_sql = q{
    SELECT COUNT(1)
    FROM device.device d, pss.pos p
    WHERE p.location_id = ?
		AND p.device_id = d.device_id
		AND d.device_active_yn_flag = 'Y'
};

my $count = OOCGI::Query->new($count_sql, { bind => [ $PARAM{location_id} ] } )->node(0,0);

my $location;
if($PARAM{myaction} eq 'Save New') {
   $location = USAT::DeviceAdmin::DBObj::Location->new();
   $location = $location->insert($query);
}

my $sql;
my $qo;
if($PARAM{location_id}) {
   $sql = 'SELECT location_id FROM location.location where location_id = ?';
   $qo  = OOCGI::Query->new($sql, { bind => $PARAM{location_id} });
} else {
   $sql = 'SELECT location_id FROM location.location where location_name = ?';
   $qo  = OOCGI::Query->new($sql, { bind => $PARAM{location_name} });
}

my $bgcolor = "#C0C0C0";
my $action_type;
if(my %hs = $qo->next_hash) {
   $location = USAT::DeviceAdmin::DBObj::Location->new($hs{location_id});
#  $bgcolor = "#FFFF00";
   $action_type = 'Edit';
} else {
   $location = USAT::DeviceAdmin::DBObj::Location->new();
   $location->location_name($PARAM{location_name});
   $location->location_country_cd('US');
   $location->location_state_cd('AL');
   $location->location_type_id('10');
#  $bgcolor = "#33FF33";
   $action_type = 'New';
}

if($PARAM{myaction} eq 'Delete' && $location->location_id != 1) {
   $location->location_active_yn_flag('N');
   $location = $location->update;
}

if($PARAM{myaction} eq 'Undelete' && $location->location_id != 1) {
   $location->location_active_yn_flag('Y');
   $location = $location->update;
}

if($PARAM{myaction} eq 'Save' && $location->location_id != 1) {
   my $locaname = $PARAM{$location->oocgi_nameof('location_name')};
   if($locaname eq $location->location_name) {
      $location = $location->update($query);
   } else {
      my $sql = 'SELECT * FROM location.location WHERE location_name = ?';
      my $qo  = OOCGI::Query->new($sql, { bind => $locaname });
      if(my %hash = $qo->next_hash) {
         print B("LOCATION => $locaname <= ALREADY EXISTS",'RED',4);
      } else {
         $location = $location->update($query);
      }
   }
}

print q|<form method="post">|;
my $name = $location->location_name;
my $parent_location_id      = $location->parent_location_id;
my $location_state_cd       = $location->location_state_cd;
my $location_country_cd     = $location->location_country_cd;
my $location_active_yn_flag = $location->location_active_yn_flag;

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

my $table_rn = 0;
$table->put($table_rn,0,"$action_type Location - $name ($count Devices)",qq|colspan=4
            class="header0"|);
$table_rn++;

$table->put($table_rn,0,'Name','class=leftheader width=14%');
if($action_type eq 'New') {
   $table->put($table_rn,1, $location->Text('location_name', { readonly => 1, size => 40}));
} else {
   $table->put($table_rn,1, $location->Text('location_name', { size => 40}));
}
$table->put($table_rn,2,'Type','class=leftheader width=12%');
$sql = "SELECT location_type_id, location_type_desc
          FROM location_type
      ORDER BY location_type_desc";
my $popType = $location->Popup('location_type_id', { sql => $sql, style => "font-size: 11px;"});
$popType->head('','Undefined');
$table->put($table_rn,3, $popType);
$table_rn++;
$table->put($table_rn,0,'Address Line 1', 'class=leftheader');
$table->put($table_rn,1, $location->Text('location_addr1', { size => 40}));
$table->put($table_rn,2,'City', 'class=leftheader');
$table->put($table_rn,3, $location->Text('location_city', { size => 30}));
$table_rn++;

$table->put($table_rn,0,'Address Line 2', 'class=leftheader');
$table->put($table_rn,1, $location->Text('location_addr2', { size => 40}));
$table->put($table_rn,2,'State', 'class=leftheader');
$sql = "SELECT state_cd, state_name
          FROM state
         WHERE country_cd = '$location_country_cd'
      ORDER BY state_name";
my $popState = $location->Popup('location_state_cd', { sql => $sql,
                                  id => "state"});
$popState->head('','Undefined');

my $buttonNewState = BUTTON(undef, 'New State', { onclick => "document.location='new_state.cgi';" });
$table->put($table_rn,3,'<div id=statecd>'.$popState.'</div>'.$buttonNewState);
$table_rn++;
$table->put($table_rn,0,'County', 'class=leftheader');
$table->put($table_rn,1, $location->Text('location_county', { size => 40}));
$table->put($table_rn,2,'Postal Code', 'class=leftheader');
$table->put($table_rn,3, $location->Text('location_postal_cd', { size => 30}));
$table_rn++;

$table->put($table_rn,0,'Country', 'class=leftheader');

my  $popLocation = OOCGI::Popup->new(
                   object => $location, field => 'location_country_cd',
                   style => "font-size: 11px;",
                   onchange => "setTheValue('')",
                   id => "country");

$sql = "SELECT country_cd, country_name 
          FROM country
      ORDER BY country_name";

my $qo2 = OOCGI::Query->new($sql);
while(my @arr = $qo2->next_array) {
   if($arr[0] eq 'AU' or $arr[0] eq 'US') {
      $popLocation->head($arr[0],$arr[1]);
   } else {
      $popLocation->tail($arr[0],$arr[1]);
   }
}

$popLocation->head('','Undefined');

$popLocation->default($location->location_country_cd);

$table->put($table_rn,1, $popLocation);
$table->put($table_rn,2,'Time Zone', 'class=leftheader');
$sql = "SELECT time_zone_cd, time_zone_name
          FROM time_zone
      ORDER BY time_zone_cd";
my $popTimeZone = $location->Popup('location_time_zone_cd', { sql => $sql, style => "font-size: 11px;" });
$popTimeZone->head('','Undefined');
if(defined $location->location_time_zone_cd)
{
        $popTimeZone->default($location->location_time_zone_cd);
}
else
{
        $popTimeZone->default('EST');
}
$table->put($table_rn,3, $popTimeZone);
$table_rn++;

$table->put($table_rn,0,'Parent Location', 'class=leftheader');

##print qq{AAA },$location->ID,qq{$parent_location_id $tooltip };

my $tooltip = TOOLTIP__1;
my $ltable = USAT::DeviceAdmin::UI::ParentLocation::get_parent_location_table(
   $location,
   $parent_location_id,
   $tooltip);
$table->put($table_rn,1,$ltable,' colspan=3');

$table_rn++;
if($action_type ne 'New') {
   $table->put($table_rn,0,'Deleted', 'class=leftheader');
   $table->put($table_rn,1, ($location_active_yn_flag eq "Y" ? "No" : "Yes"), 'colspan=3');
   $table_rn++;
}

my $str;
if ($location_active_yn_flag eq "Y")
{
   my $LID = $location->ID;
   if($LID != 1) {
      $str = SUBMIT('myaction','Save').
      SUBMIT('myaction','Delete',         { onclick => "return confirmSubmit('Delete')"});
   }
   $str .= ''; 
   if($count > 0 ) {
   $str.=BUTTON('NA','List Devices',
         { onclick => "javascript:window.location = '/device_list.cgi?location_id=$LID';"});
   } else {
   $str.=BUTTON('NA','List Devices', 
         { onclick => "javascript:window.location = '/device_list.cgi?location_id=$LID';", disabled => 'yes'});
   }
   $str.=BUTTON('NA','List Child Locations',
         { onclick => "javascript:window.location = '/location_list.cgi?parent_location_id=$LID';"}).
   BUTTON('NA','View Hierarchy',       { onclick => "javascript:window.location = '/location_tree.cgi?source_id=$LID';"});
} else {
   if($action_type eq 'New') {
      $str = SUBMIT("myaction","Save New");
   } else {
      my $LID = $location->ID;
      if($LID != 1) {
         $str = SUBMIT("myaction","Undelete", { onclick => "return confirmSubmit('Undelete')"});
      }
   }
}

$table->put($table_rn,0, $str,'align=center colspan=4');

my $LID = $location->ID;
HIDDEN('location_id',$LID);
HIDDEN('location_name',$PARAM{location_name});

print qq|
<script LANGUAGE="JavaScript">
<!--
var defval;
var flag = 1;
var strval = '<select name="|.$location->oocgi_nameof('location_state_cd').qq|" tabindex="1" id="state">'; 
function setTheValue(val){ 
    obj     = document.getElementById('state'); 
    country = document.getElementById('country');
    if(flag == 1) {
       flag = 0;
       defval = document.getElementById('statecd').innerHTML;
    }
    if(country.value == '$location_country_cd') {
       val = '$location_state_cd';
       document.getElementById('statecd').innerHTML = defval;
|;

my $state_sql = 'SELECT state_cd, state_name, country_cd FROM state ORDER BY country_cd';

my $state_qo = OOCGI::Query->new($state_sql);

my @states;
my $strhead = qq|document.getElementById('statecd').innerHTML = strval +
       '<option value="">Undefined</option>'|;
$states[0] = $strhead;
while(my @arr = $state_qo->next_array) {
    push(@states, qq|'<option value="$arr[0]">$arr[1]</option>'|);
    if(my @nextarr = $state_qo->peek) {
       if($arr[2] ne $nextarr[2]) {
          push(@states, qq|'</select>'|);
          my $str = join('+',@states);
          print qq|} else if(country.value == '$arr[2]') {
             $str|;
          @states = ();
          $states[0] = $strhead;
       }
    } else {
       push(@states, qq|'</select>'|);
       my $str = join('+',@states);
       print qq|
       } else if(country.value == '$arr[2]') {
          $str|;
    }
}
print qq|
    } else {
       document.getElementById('statecd').innerHTML = strval + '<option value="">Undefined</option></select>';
    }
    obj.value = val;
}
   function confirmSubmit(myaction)
   {
      var agree;
      switch(myaction)
      {
         case 'Delete':
         {
      agree=confirm("To delete a location record you must first reassign all it's device to a different location.  If you have not done this already, click Cancel and do so now.\\n\\n Continue to delete $name?");
      break;
   }
   case 'Undelete':
   {
      agree=confirm("OK to undelete $name?");
      break;
   }  
    }
    if (agree)
   return true ;
    else
   return false ;
  }
  // -->
  </script>
|;

display $table;
print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
