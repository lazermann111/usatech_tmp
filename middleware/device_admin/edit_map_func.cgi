#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
require Text::CSV;
use USAT::DeviceAdmin::Util;
use POSIX qw(ceil);
use USAT::DeviceAdmin::UI::DAHeader;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $map_name = $PARAM{"map_name"};
my $number_of_locations = $PARAM{"number_of_location"};
my $action = $PARAM{"action"};

my $new_map = "";
my $mem_counter = 0;

print "
<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
 <tr>
  <td align=\"left\">
  	<input type=button value=\"Return to Template Menu\" onClick=\"javascript:window.location = '/template_menu.cgi';\">
  	<br>
   <pre>
";

my $location_counter_sum = 0;
my $new_order_sum = 0;

print "Validating Order... ";
for (my $location_counter = 1; $location_counter <= $number_of_locations; $location_counter++)
{
	my $order = $PARAM{$location_counter."_order"};
	$location_counter_sum = $location_counter_sum + $location_counter;
	$new_order_sum = $new_order_sum + $order;
}

if($location_counter_sum ne $new_order_sum)
{
	print "<font color=red><b>Order Validation Failed!\n";
	print "Please check your order numbers and make sure there are no duplicates.\n\n</b></font>";
	print "<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print this screen and give it to an administrator.\");</script>\n";
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}
else
{
	print "Looks good!\n\n";
}

my %data_hash;

for (my $location_counter = 1; $location_counter <= $number_of_locations; $location_counter++)
{
#	($name,$start_addr,$size,$align,$pad_with,$data_mode,$display,$description,$choices,$default_choice,$allow_change) = $csv->fields;
	
	my $order = $PARAM{$location_counter."_order"};
	my $name = $PARAM{$location_counter."_name"};
	my $address = $PARAM{$location_counter."_address"};
	my $size = $PARAM{$location_counter."_size"};
	my $align = $PARAM{$location_counter."_align"};
	my $pad_with = $PARAM{$location_counter."_pad_with"};
	my $data_mode = $PARAM{$location_counter."_data_mode"};
	my $display = $PARAM{$location_counter."_display"};
	my $description = $PARAM{$location_counter."_description"};
	my $choices = $PARAM{$location_counter."_choices"};
	my $default_choice = $PARAM{$location_counter."_default_choice"};
	my $allow_change = $PARAM{$location_counter."_allow_change"};
	
	print "Location Counter     : $location_counter -----------------------\n";
	print "New Order            : $order\n";
	print "Memory Counter       : $mem_counter\n";
	print "Name                 : $name\n";
	print "Address              : $address\n";
	print "Size                 : $size\n";
	print "Align                : $align\n";
	print "Pad With             : $pad_with\n";
	print "Data Mode            : $data_mode\n";
	print "Display              : $display\n";
	print "Description          : $description\n";
	print "Choices              : $choices\n";
	print "Default Choice       : $default_choice\n";
	print "Allow Change         : $allow_change\n\n";
	
	chomp($order);
	chomp($location_counter);
	chomp($mem_counter);
	chomp($name);
	chomp($address);
	chomp($size);
	chomp($align);
	chomp($data_mode);
	chomp($display);
	chomp($description);
	chomp($choices);
	chomp($default_choice);
	chomp($allow_change);
	
	
	if($data_mode eq 'C' && length($choices) == 0)
	{
		print "<font color=red><b>Possible choices are required for a Choice field!\n";
		print "Please input choices separated by | for: $name\n</b></font>";
		print "<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print this screen and give it to an administrator.\");</script>\n";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
		
	if($data_mode eq 'C' && length($default_choice) == 0)
	{
		print "<font color=red><b>A default choice is required for a Choice field!\n";
		print "Please input a default choice for: $name\n</b></font>";
		print "<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print this screen and give it to an administrator.\");</script>\n";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}

    if($data_mode eq 'P' && length($choices) == 0)
    {
        print "<font color=red><b>Popup String are required for a Popup field!\n";
        print "Please input popup string as key,velue separated by | for: $name\n</b></font>";
        print "<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK,
 then print this screen and give it to an administrator.\");</script>\n";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
    }

    if($data_mode eq 'P' && length($default_choice) == 0)
    {
        print "<font color=red><b>A default value is required for a Popup field!\n";
        print "Please input a default popup value for: $name\n</b></font>";
        print "<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK,
 then print this screen and give it to an administrator.\");</script>\n";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
    }
	
	$data_hash{$order} = [$name,$address,$size,$align,$pad_with,$data_mode,$display,$description,$choices,$default_choice,$allow_change];
	$mem_counter = $mem_counter + $size;
}

print "Checking memory size: $mem_counter ... ";
if($map_name =~ /(G4|g4)/ && $mem_counter ne '512')
{
	print "<font color=red><b>Size Validation Failed!\n";
	print "Please check your memory addresses and sizes and make sure there are no overlaps or gaps.\n\n</b></font>";
	print "<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print this screen and give it to an administrator.\");</script>\n";
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}
else
{
	print "Looks good!\n\n";
}

print "Writing CSV...\n\n";

for (my $location_counter = 1; $location_counter <= $number_of_locations; $location_counter++)
{
	my $hash_array_ref = $data_hash{$location_counter};

	my @fields;
	push (@fields, $hash_array_ref->[0]);
	push (@fields, $hash_array_ref->[1]);
	push (@fields, $hash_array_ref->[2]);
	push (@fields, $hash_array_ref->[3]);
	push (@fields, $hash_array_ref->[4]);
	push (@fields, $hash_array_ref->[5]);
	push (@fields, $hash_array_ref->[6]);
	push (@fields, $hash_array_ref->[7]);
	push (@fields, $hash_array_ref->[8]);
	push (@fields, $hash_array_ref->[9]);
	push (@fields, $hash_array_ref->[10]);
	
	my $csv = Text::CSV->new;
	
	if ($csv->combine(@fields)) 
	{
		my $new_line = $csv->string();
		print "Line $location_counter    \t: $new_line\n";
		$new_map = $new_map . $new_line . "\n";
	}
	else
	{
		print "<font color=red><b>Failed to combine: "  . $csv->error_input() . "</b></font>\n\n";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
	
	$mem_counter += $hash_array_ref->[2];
}

if(USAT::DeviceAdmin::Util::save_file($map_name, $new_map, $dbh))
{
	print "\n\nSave Success: $map_name\n";
}
else
{
	print "Save Failed!: $map_name\n";
}

$dbh->disconnect;

print "
   </pre>
  </td>
 </tr>
 <tr>
  <td align=\"left\">
  	<input type=button value=\"Return to Template Menu\" onClick=\"javascript:window.location = '/template_menu.cgi';\">
  </td>
 </tr>  
</table>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

