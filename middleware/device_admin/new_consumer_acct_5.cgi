#!/usr/local/USAT/bin/perl

use strict;

require Text::CSV;
use OOCGI::OOCGI;
use OOCGI::NTable;
use Data::Dumper;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

use constant DBTS_FORMAT => 'YYYY-MM-DD HH24:MI:SS';

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

my $action = $params{'action'};

if(defined $action && $action eq 'Download CSV Database')
{
	my $csv = $params{'csv'};
	print "Content-Type: application/force-download\n";
	print "Content-Disposition: attachment; filename=data.csv\n\n";
	print $csv;
	exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print qq|<div align=left><pre>CARD GENERATION LOG:\n\n|;

my $consumer_acct_fmt_id = $params{'consumer_acct_fmt_id'};
my $consumer_acct_fmt_name = $params{'consumer_acct_fmt_name'};
my $authority_id = $params{'authority_id'};
my $merchant_id = $params{'merchant_id'};
my $merchant_name = $params{'merchant_name'};
my $merchant_desc = $params{'merchant_desc'};
my $merchant_cd = $params{'merchant_cd'};
my $terminal_id = $params{'terminal_id'};
my $terminal_desc = $params{'terminal_desc'};
my $customer_id = $params{'customer_id'};
my $customer_name = $params{'customer_name'};
my $location_id = $params{'location_id'};
my $location_name = $params{'location_name'};
my $num_cards = $params{'num_cards'};
my $exp = $params{'exp'};
my $exp_display = $params{'exp_display'};
my $deactivation_date = $params{'deactivation_date'};
my $create_date = $params{'create_date'};
my $balance = $params{'balance'};
my $action_id = $params{'action_id'};
my $device_type_id = $params{'device_type_id'};
my $action_params_str = $params{'action_params'};
my @action_params = split(/,/, $action_params_str);

print "Creating cards for format $consumer_acct_fmt_id\n";

print "Using deactivation date: $deactivation_date\n";

if(not $merchant_id)
{
	# lookup next merchant_cd for selected authority
	
	print "Creating new merchant account\n";
	
	my $next_merchant_cd_sql = qq|
		select nvl(max(to_number(merchant.merchant_cd)), 0)+1
		from merchant
		where merchant.authority_id = ?|;
	
	my $next_merchant_cd_query = OOCGI::Query->new($next_merchant_cd_sql, undef, [$params{'authority_id'}]);
	my @next_merchant_cd_arr = $next_merchant_cd_query->next;
	if(defined $next_merchant_cd_arr[0])
	{
		$merchant_cd = sprintf("%03d", $next_merchant_cd_arr[0]);
	}
	
	printError("Failed to lookup next merchant_cd for authority_id " . $params{'authority_id'}) if(not $merchant_cd);
	
	print "New merchant code: $merchant_cd\n";
	 
	# create merchant

	my $merchant = USAT::DeviceAdmin::DBObj::Merchant->new();
	$merchant->merchant_cd($merchant_cd);
	$merchant->merchant_name(substr($merchant_name, 0, 255));
	$merchant->merchant_desc(substr($merchant_desc, 0, 255));
	$merchant->merchant_bus_name('USA Technologies, Inc.');
	$merchant->authority_id($authority_id);
	$merchant = $merchant->insert;
	
	if(defined $merchant)
	{
		$merchant_id = $merchant->ID;
	}
	else
	{
		printError("Failed to create merchant!");
	}
	
	print "Created new merchant: $merchant_id\n";

	# create customer_merchant

	my $cust_merch_rows = OOCGI::Query->insert('customer_merchant', { 
		customer_id => $customer_id,
		merchant_id => $merchant_id});
		
	printError("Failed to create customer_merchant ($customer_id, $merchant_id)!") unless($cust_merch_rows > 0);
	
	print "Created new customer_merchant: $customer_id, $merchant_id\n";
}
else
{
	print "Using existing merchant: id=$merchant_id, cd=$merchant_cd\n";
}

if(not $terminal_id)
{
	# create terminal
	
	($terminal_id) = OOCGI::Query->insert('pss.terminal', { 
		terminal_cd => '000',
		terminal_next_batch_num => 1,
		merchant_id => $merchant_id,
		terminal_desc => substr($terminal_desc, 0, 255),
		terminal_state_id => 1,
		terminal_max_batch_num => 999,
		terminal_batch_max_tran => 999,
		terminal_batch_cycle_num => 1,
		terminal_min_batch_num => 850,
		terminal_min_batch_close_hr => 24,
		terminal_max_batch_close_hr => 25}, 'terminal_id');
	
	printError("Failed to create terminal!") unless(defined $terminal_id);
	
	print "Created new terminal: $terminal_id\n";
}
else
{
	print "Using existing terminal $terminal_id\n";
}

# lookup consumer using consumer_accounts linked to merchant via merchant_consumer_acct

my $consumer_id;
my $consumer_email_addr = 'merchant_' . $merchant_id . '@usatech.com';

my $consumer_sql = qq|
	select consumer.consumer_id
	from pss.consumer
	where consumer_email_addr1 = ?|;

my $consumer_query = OOCGI::Query->new($consumer_sql, undef, [$consumer_email_addr]);
if ($consumer_query->row_count > 0)
{
	my @consumer_arr = $consumer_query->next;
	$consumer_id = $consumer_arr[0];
	
	print "Using existing consumer $consumer_id\n";
}
else
{
	if(not defined $merchant_name || length($merchant_name) == 0)
	{
		my $merchant_name_sql = q|select merchant_name from merchant where merchant_id = ?|;
		my $merchant_name_query = OOCGI::Query->new($merchant_name_sql, undef, [$merchant_id]);
		
		printError("Failed to lookup mechant name for $merchant_id!") unless ($merchant_name_query->row_count == 1);
		
		my %merchant_name_hash = $merchant_name_query->next_hash;
		$merchant_name = $merchant_name_hash{merchant_name};
		
		print "Using merchant name $merchant_name\n";
	}
	
	# create new if none exists
	($consumer_id) =  OOCGI::Query->insert('pss.consumer', {
		consumer_email_addr1 => $consumer_email_addr,
		consumer_fname => 'Virtual Consumer',
		consumer_lname => substr($merchant_name, 0, 50),
		consumer_type_id => 4}, 'consumer_id');

	printError("Failed to create consumer $consumer_email_addr!") unless(defined $consumer_id);

	print "Created new consumer: $consumer_id - $consumer_email_addr\n";
}


my $order_by;
if($consumer_acct_fmt_id eq '1') {
   $order_by = 'order by to_number(consumer_acct_cd) desc';
} elsif($consumer_acct_fmt_id eq '0') {
   $order_by = 'order by consumer_acct_id desc';
} else {
   print "Wrong Consumer Acct Fmt ID $consumer_acct_fmt_id \n";
   USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
   exit();
}
 
my $consumer_acct_sql = qq|
   	SELECT consumer_acct_cd
      FROM (
       	    SELECT consumer_acct.consumer_acct_cd 
 	          FROM consumer_acct
 	         WHERE consumer_id = ?
 	         $order_by )
     WHERE rownum = 1|;

# lookup last created consumer_acct_cd
my $last_consumer_acct_cd = 0;

my $consumer_acct_query = OOCGI::Query->new($consumer_acct_sql, { bind => [ $consumer_id ] } );
if (my @consumer_acct_arr = $consumer_acct_query->next_array)
{
   $last_consumer_acct_cd = $consumer_acct_arr[0];
   print "Consumer accounts exists: last consumer account code: $last_consumer_acct_cd\n";
} else {
   print "No consumer accounts exists\n";
}

my @cards_arr = ();

if($consumer_acct_fmt_id == 0)
{
	my $lostCardNum = 0;
	
	my $cmd = "java -cp cardnumbergenerator.jar:common.jar:colt.jar com.usatech.cardnumbergenerator.CardFormat0Simple $exp $last_consumer_acct_cd $merchant_cd $lostCardNum $num_cards 2>&1";
	print "Calling card generator: $cmd\n";
	my $out = `$cmd`;
	
	printError("$out") if($out =~ m/(failed|Exception)/);

	my @out_lines = split(/\n/, $out);
	foreach my $line (@out_lines)
	{
		print "Output: $line\n";
		
		my @fields = split(/,/, $line);
		my $magstripe = $fields[0];
		my $consumer_acct_cd = $fields[1];
		my $account_id = $fields[2];
		my $vv = $fields[3];
		my $lostCardCD = $fields[4];
		
		my $consumer_acct_insert = OOCGI::Query->new(undef, { typemap => { DBI::SQL_TYPE_DATE => DBTS_FORMAT, DBI::SQL_TYPE_TIMESTAMP => DBTS_FORMAT } } );
		 	
		my ($consumer_acct_id) =  $consumer_acct_insert->insert('pss.consumer_acct', {
			consumer_acct_cd => $consumer_acct_cd,
			consumer_acct_active_yn_flag => 'Y',
			consumer_acct_balance => $balance,
			consumer_id => $consumer_id,
			location_id => $location_id,
			consumer_acct_issue_num => 1,
			payment_subtype_id => 1, # payment_subtype_id is deprecated but required
			consumer_acct_activation_ts => sub { $consumer_acct_insert->SYSDATE() },
			consumer_acct_deactivation_ts => $deactivation_date,
			currency_cd => 'USD',
			consumer_acct_fmt_id => $consumer_acct_fmt_id,
			consumer_acct_issue_cd => $lostCardCD,
			consumer_acct_validation_cd => $vv}, 'consumer_acct_id');
		
		printError("Failed to insert new consumer_acct: $consumer_acct_cd!") unless(defined $consumer_acct_id);
		
		print "Inserted consumer_acct $consumer_acct_id: $consumer_acct_cd\n";
		
		my $merchant_consumer_acct_insert = OOCGI::Query->insert('pss.merchant_consumer_acct', { 
			merchant_id => $merchant_id,
			consumer_acct_id => $consumer_acct_id});
		
		printError("Failed to insert new merchant_consumer_acct!") if($merchant_consumer_acct_insert != 1);

		print "Inserted merchant_consumer_acct ($merchant_id, $consumer_acct_id): $consumer_acct_cd\n";
		
		push(@cards_arr, [$magstripe, $consumer_acct_cd, $exp, $exp_display, $create_date, $consumer_acct_fmt_name, $customer_name, $location_name]);
	}
}
elsif($consumer_acct_fmt_id == 1)
{
	# lookup device_type_action.device_type_action_cd to encode on card
	
	my $device_type_action_sql = q|
		select device_type_action.device_type_action_cd
		from device.device_type_action
    	where device_type_action.action_id = ?
    	and device_type_action.device_type_id = ?|;
    	
    my $device_type_action_query = OOCGI::Query->new($device_type_action_sql, undef, [$action_id, $device_type_id]);
    
	printError("Failed to lookup device_type_action_cd for $action_id, $device_type_id!") if ($device_type_action_query->row_count == 0);
    
	my @device_type_action_cd_arr = $device_type_action_query->next;
	my $device_type_action_cd = $device_type_action_cd_arr[0];
	
	print "Encoding device_type_action_cd $device_type_action_cd on cards for device type $device_type_id\n";
	
	# lookup existing permission_actions for selected action
	my $permission_action_sql = q|
		select permission_action.permission_action_id, permission_action_param.action_param_id
		from pss.permission_action, pss.permission_action_param
		where permission_action.permission_action_id = permission_action_param.permission_action_id(+)
		and permission_action.action_id = ?
		order by permission_action.permission_action_id asc, permission_action_param.action_param_id asc|;
		
	# the following section sorts permission_actions by their exiting permissions_action_params, so we can 
	# lookup existing permission_actions by params and action_id

	my %action_param_hash = ();
	my $last_permission_action_id;
	my $current_key;
	
    my $permission_action_query = OOCGI::Query->new($permission_action_sql, undef, [$action_id]);
	while(my %permission_action_hash = $permission_action_query->next_hash)
	{
		my $this_permission_action_id = $permission_action_hash{permission_action_id};
		my $this_action_param_id = defined $permission_action_hash{action_param_id} ? $permission_action_hash{action_param_id} : "-";
		
		if(not defined $last_permission_action_id)
		{
			$current_key = $this_action_param_id;
			$last_permission_action_id = $this_permission_action_id;
		}
		elsif($last_permission_action_id eq $this_permission_action_id)
		{
			$current_key .= ",$this_action_param_id";
		}
		else
		{
			$action_param_hash{$current_key} = $last_permission_action_id;
			
			$current_key = $this_action_param_id;
			$last_permission_action_id = $this_permission_action_id;
		}
	}
	
	$action_param_hash{$current_key} = $last_permission_action_id;
	
	# now see if there is an existing matching permission_action
	@action_params = sort(@action_params);
	my $action_param_key = scalar(@action_params) > 0 ? join(',',@action_params) : "-";
	
	my $permission_action_id = $action_param_hash{$action_param_key};
	if(defined $permission_action_id)
	{
		# we found a matching permission_action
		print "Using existing permission action $permission_action_id with action_params $action_param_key\n";
	}
	else
	{
		($permission_action_id) = OOCGI::Query->insert('pss.permission_action', { 
			action_id => $action_id}, 'permission_action_id');
			
			
		printError("Failed to create permission_action for action_id $action_id!") unless(defined $permission_action_id);
		
		my $counter = 1;
		foreach my $action_param_id (@action_params)
		{
			my $permission_action_param_rows = OOCGI::Query->insert('pss.permission_action_param', { 
				permission_action_id => $permission_action_id,
				action_param_id => $action_param_id,
				permission_action_param_order => $counter++});
			
			printError("Failed to create permission_action_param ($permission_action_id, $action_param_id)!") if($permission_action_param_rows != 1);
		}
		
		print "Created new permission_action $permission_action_id with action_id $action_id and action_params $action_param_key\n";
	}
	
	my $cmd = "java -cp cardnumbergenerator.jar:common.jar:colt.jar com.usatech.cardnumbergenerator.CardFormat1Simple $exp $last_consumer_acct_cd $merchant_cd $device_type_action_cd $num_cards 2>&1";
	print "Calling card generator: $cmd\n";
	my $out = `$cmd`;
	
	printError("$out") if($out =~ m/(failed|Exception)/);
	
	my @out_lines = split(/\n/, $out);
	foreach my $line (@out_lines)
	{
		print "Output: $line\n";
		
		my @fields = split(/,/, $line);
		my $magstripe = $fields[0];
		my $consumer_acct_cd = $fields[1];
		my $account_id = $fields[2];
		my $vv = $fields[3];
		
		my $consumer_acct_insert = OOCGI::Query->new(undef, { typemap => { DBI::SQL_TYPE_DATE => DBTS_FORMAT, DBI::SQL_TYPE_TIMESTAMP => DBTS_FORMAT } } );
		 	
		my ($consumer_acct_id) =  $consumer_acct_insert->insert('pss.consumer_acct', {
			consumer_acct_cd => $consumer_acct_cd,
			consumer_acct_active_yn_flag => 'Y',
			consumer_acct_balance => 0,
			consumer_id => $consumer_id,
			location_id => $location_id,
			consumer_acct_issue_num => 1,
			payment_subtype_id => $authority_id	,
			consumer_acct_activation_ts => sub { $consumer_acct_insert->SYSDATE() },
			consumer_acct_deactivation_ts => $deactivation_date,
			currency_cd => 'USD',
			consumer_acct_fmt_id => $consumer_acct_fmt_id,
			consumer_acct_validation_cd => $vv}, 'consumer_acct_id');
		
		printError("Failed to insert new consumer_acct: $consumer_acct_cd!") unless(defined $consumer_acct_id);
		
		print "Inserted consumer_acct $consumer_acct_id: $consumer_acct_cd\n";
		
		my $merchant_consumer_acct_insert = OOCGI::Query->insert('pss.merchant_consumer_acct', { 
			merchant_id => $merchant_id,
			consumer_acct_id => $consumer_acct_id});
		
		printError("Failed to insert new merchant_consumer_acct!") if($merchant_consumer_acct_insert != 1);
		
		print "Inserted merchant_consumer_acct ($merchant_id, $consumer_acct_id): $consumer_acct_cd\n";
		
		my $consumer_acct_permission_query = OOCGI::Query->insert('pss.consumer_acct_permission', { 
			consumer_acct_id => $consumer_acct_id,
			permission_action_id => $permission_action_id,
			consumer_acct_permission_order => 1});
			
		printError("Failed to insert new consumer_acct_permission ($consumer_acct_id, $permission_action_id)!") if($consumer_acct_permission_query != 1);
		print "Inserted consumer_acct_permission ($consumer_acct_id, $permission_action_id): $consumer_acct_cd\n";

		push(@cards_arr, [$magstripe, $consumer_acct_cd, $exp, $exp_display, $create_date, $consumer_acct_fmt_name, $customer_name, $location_name]);
	}
}
else
{
	printError("Unhandled consumer account format: $consumer_acct_fmt_id");
}

my $card_count = scalar(@cards_arr);
my $csv_doc;
my $csv = Text::CSV->new;
my @headers = ('Magstripe', 'Account Number', 'Expiration Date', 'Display Expiration Date', 'Activation Date', 'Card Type', 'Customer', 'Location');
$csv->combine(@headers);
$csv_doc .= $csv->string . "\n";

print q|</pre></div align=left>|;

print qq|<br><form method="post" action="new_consumer_acct_5.cgi">\n|;
print B("$card_count cards generated successfully!<br>\n", "GREEN");
print q|<textarea rows="20" cols="50" readonly>|;
foreach my $card_arr_ref (@cards_arr)
{
	print "$card_arr_ref->[0]\n";
	
	if ($csv->combine(@$card_arr_ref)) 
	{
    	my $csv_line = $csv->string;
		$csv_doc .= $csv_line . "\n";
	}
	else
	{
		printError("Failed to generate CSV database for card $card_arr_ref->[0]! ($csv->error_input)");
    }
}

print qq|</textarea><br>\n<br>\n|;
HIDDEN('csv', $csv_doc);
print qq|<input type=submit name="action" value="Download CSV Database"> |;
print qq|<input type=button value="Start Over" onClick="javascript:window.location = '/new_consumer_acct_1.cgi';"><br>\n<br>\n<br>\n|;
print qq|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub printError
{
	my $err_txt = shift;
	print B("$err_txt<br>\n", "red");
	print B("Card generation failed! For help, click OK then send this screen output to an administrator.<br>\n", "red");
	print "<div align=left><pre>\n\n". Dumper($query) . "\n\n</pre><div>";
	print q|<script language="javascript">window.alert("Card generation failed! For help, click OK then send this screen output to an administrator.");</script>|;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
    exit;
}
