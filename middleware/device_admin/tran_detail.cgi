#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::Security::StringMask;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();

USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

my %status_codes_hash = (	'0'	=>	'No Status Available',
							'1'	=>	'Idle, Available',
							'2'	=>	'In 1st Cycle',
							'3'	=>	'Out Of Service',
							'4'	=>	'Nothing on Port',
							'5'	=>	'Idle, Not Available',
							'6'	=>	'Manual Service Mode',
							'7'	=>	'In 2nd Cycle',
							'8'	=>	'Transaction In Progress',
						);

my $tran_id = $PARAM{"tran_id"};
if(length($tran_id) == 0)
{
	print "Required Parameter Not Found: tran_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $get_tran_stmt = $dbh->prepare(q{
	SELECT TO_CHAR(t.tran_start_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
		DECODE(ps.client_payment_type_cd, 'M', TO_CHAR(a2.auth_ts, 'MM/DD/YYYY HH:MI:SS AM'), TO_CHAR(a.auth_ts, 'MM/DD/YYYY HH:MI:SS AM')), 
		TO_CHAR(t.tran_upload_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
		DECODE(ps.client_payment_type_cd, 'M', a2.auth_amt, a.auth_amt), 
		t.tran_state_cd, 
		ps.payment_subtype_name, 
		NVL(ca.consumer_acct_cd, t.tran_parsed_acct_num), 
		t.tran_device_tran_cd, 
		DECODE(ps.client_payment_type_cd, 'M', a2.auth_result_cd, a.auth_result_cd), 
		DECODE(ps.client_payment_type_cd, 'M', a2.auth_type_cd, a.auth_type_cd),
		ts.tran_state_desc,
		a2.auth_id AS sale_id,
		t.tran_global_trans_cd,
		tdrt.tran_device_result_type_desc,
		DECODE(ps.client_payment_type_cd, 'M', ar2.auth_result_desc, ar.auth_result_desc),
		DECODE(ps.client_payment_type_cd, 'M', at2.auth_type_desc, at.auth_type_desc),
		DECODE(ps.client_payment_type_cd, 'M', a2.auth_resp_cd, a.auth_resp_cd),
		DECODE(ps.client_payment_type_cd, 'M', a2.auth_resp_desc, a.auth_resp_desc),
		d.device_id,
		d.device_serial_cd,
		dt.device_type_desc
	FROM tran t, pos_pta pp, payment_subtype ps, consumer_acct ca, tran_state ts, tran_device_result_type tdrt,
		(SELECT * FROM (SELECT auth_id, auth_amt, auth_type_cd, auth_result_cd, auth_ts, auth_resp_cd, auth_resp_desc FROM auth WHERE tran_id = :tran_id AND auth_type_cd IN ('N', 'L') UNION SELECT 0 AS auth_id, NULL, NULL, NULL, NULL, NULL, NULL FROM DUAL ORDER BY auth_id DESC) WHERE ROWNUM = 1) a, 
        (SELECT * FROM (SELECT auth_id, auth_amt, auth_type_cd, auth_result_cd, auth_ts, auth_resp_cd, auth_resp_desc FROM auth WHERE tran_id = :tran_id AND auth_type_cd IN ('U', 'S') UNION SELECT 0 AS auth_id, NULL, NULL, NULL, NULL, NULL, NULL FROM DUAL ORDER BY auth_id DESC) WHERE ROWNUM = 1) a2,
		auth_result ar, auth_type at, auth_result ar2, auth_type at2, pos, device d, device_type dt
	WHERE t.tran_id = :tran_id
	AND t.pos_pta_id = pp.pos_pta_id
	AND t.tran_state_cd = ts.tran_state_cd
	AND ps.payment_subtype_id = pp.payment_subtype_id
	AND t.consumer_acct_id = ca.consumer_acct_id(+) 
	AND t.tran_device_result_type_cd = tdrt.tran_device_result_type_cd(+) 
 	AND a.auth_result_cd = ar.auth_result_cd(+)
 	AND a.auth_type_cd = at.auth_type_cd(+)
 	AND a2.auth_result_cd = ar2.auth_result_cd(+)
 	AND a2.auth_type_cd = at2.auth_type_cd(+)
 	AND pp.pos_id = pos.pos_id
 	AND pos.device_id = d.device_id 
 	and d.device_type_id = dt.device_type_id
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_tran_stmt->bind_param(":tran_id", $tran_id);
$get_tran_stmt->execute();
my @data = $get_tran_stmt->fetchrow_array();
$get_tran_stmt->finish();

my $start_ts = $data[0];
my $auth_ts = $data[1];
my $upload_ts = $data[2];
my $auth_amount = $data[3];
my $state_id = $data[4];
my $payment_type = $data[5];
my $account_num = $data[6];
my $device_tran_id = $data[7];
my $auth_result_cd = $data[8];
my $auth_type_cd = $data[9];
my $state = $data[10];
my $sale_id = $data[11];
my $machine_trans_no = $data[12];
my $device_tran_result = $data[13];
my $auth_result_desc = $data[14];
my $auth_type_desc = $data[15];
my $auth_resp_cd = $data[16];
my $auth_resp_desc = $data[17];
my $device_id = $data[18];
my $device_serial_cd = $data[19];
my $device_type_desc = $data[20];
#my $state = "Unknown($state_id)";
#if($state_id eq '6')
#{
#	$state = "Auth Success";
#}
#elsif($state_id eq '7')
#{
#	$state = "Auth Decline";
#}
#elsif($state_id eq '8')
#{
#	$state = "Batched";
#}
#elsif($state_id eq 'C')
#{
#	$state = "Cancelled";
#}
#elsif($state_id eq 'F')
#{
#	$state = "Vend Failed";
#}
#elsif($state_id eq 'T')
#{
#	$state = "Vend Timeout";
#}

#my $auth_type = "Unknown($auth_type_cd)";
#if($auth_type_cd eq 'L')
#{
#	$auth_type = 'Local';
#}
#elsif($auth_type_cd eq 'N')
#{
#	$auth_type = 'Network';
#}

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Transaction Details</th>
 </tr>
 <tr>
  <td nowrap>Tran ID</td>
  <td width=\"50%\">$tran_id&nbsp;</td>
  <td nowrap>Start Time</td>
  <td width=\"50%\">$start_ts&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Device Tran ID</td>
  <td width=\"50%\">$device_tran_id&nbsp;</td>
  <td nowrap>Auth Time</td>
  <td width=\"50%\">$auth_ts&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>State</td>
  <td width=\"50%\">$state&nbsp;</td>
  <td nowrap>Upload Time</td>
  <td width=\"50%\">$upload_ts&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Payment Type</td>
  <td width=\"50%\">$payment_type&nbsp;</td>
  <td nowrap>Auth Type</td>
  <td width=\"50%\">$auth_type_desc&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Account/Card Number</td>
  <td width=\"50%\">" . USAT::Security::StringMask::mask_credit_card($account_num) . "&nbsp;</td>
  <td nowrap>Auth Result</td>
  <td width=\"50%\">$auth_result_desc&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Machine Trans No</td>
  <td width=\"50%\">$machine_trans_no&nbsp;</td>
  <td nowrap>Auth Response Code</td>
  <td width=\"50%\">$auth_resp_cd&nbsp;</td>
 </tr>
 <tr>
   <td nowrap>Auth Amount</td>
   <td width=\"50%\">$auth_amount&nbsp;</td>
   <td nowrap>Auth Response Desc</td>
   <td width=\"50%\">$auth_resp_desc&nbsp;</td>
 </tr>
 <tr>
   <td nowrap>Device Serial</td>
   <td width=\"50%\"><a href=\"profile.cgi?device_id=$device_id\">$device_serial_cd</a></td>
   <td nowrap>Device Type</td>
   <td width=\"50%\">$device_type_desc</td>
 </tr>
</table>

<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"9\" bgcolor=\"#C0C0C0\">Line Items</th>
 </tr>
 <tr>
  <th>Line Item ID</th>
  <th>Host ID</th>
  <th>Batch Type</th>
  <th>Line Item Type</th>
  <th>Amount</th>
  <th>Quantity</th>
  <th>Line Total</th>
  <th>Description</th>
 </tr>
";

my $line_items_stmt = $dbh->prepare(q{
	SELECT tli.tran_line_item_id, 
		tli.tran_line_item_amount, 
		tli.tran_line_item_tax, 
		tli.tran_line_item_desc, 
		tli.tran_line_item_type_id, 
		tli.tran_line_item_quantity, 
		tli.host_id, 
		tlibt.tran_line_item_batch_type_cd,
		tlit.tran_line_item_type_desc,
		tlibt.tran_line_item_batch_type_desc,
		(NVL(tli.tran_line_item_amount, 0) + NVL(tli.tran_line_item_tax, 0)) * NVL(tli.tran_line_item_quantity, 0)
	FROM pss.tran_line_item tli, pss.tran_line_item_batch_type tlibt, pss.tran_line_item_type tlit
	WHERE tli.tran_line_item_batch_type_cd = tlibt.tran_line_item_batch_type_cd
	AND tran_id = :tran_id
	AND tli.tran_line_item_type_id = tlit.tran_line_item_type_id
	ORDER BY tli.tran_line_item_id, tlibt.tran_line_item_batch_type_cd
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$line_items_stmt->bind_param(":tran_id", $tran_id);
$line_items_stmt->execute();
while (@data = $line_items_stmt->fetchrow_array()) 
{
	my $line_item_id = $data[0];
	my $amount = $data[1];
	my $tax = $data[2];
	my $desc = $data[3];
	my $type_id = $data[4];
	my $quantity = $data[5];
	my $host_id = $data[6];
	my $batch_type_cd = $data[7];
	my $type_desc = $data[8];
	my $batch_type_desc = $data[9];
	my $line_total = $data[10];

#	my $batch_type_desc = "Unknown($batch_type_cd)";
#	if($batch_type_cd eq 'A')
#	{
#		$batch_type_desc = "Actual";
#	}
#	elsif($batch_type_cd eq 'I')
#	{
#		$batch_type_desc = "Intended";
#	}

print "	

 <tr>
  <td align=\"left\">$line_item_id&nbsp;</td>
  <td align=\"left\"><a href=\"/host_profile.cgi?host_id=$host_id\">$host_id</a>&nbsp;</td>
  <td align=\"left\">$batch_type_desc&nbsp;</td>
  <td align=\"left\">$type_desc&nbsp;</td>
  <td align=\"left\">$amount&nbsp;</td>
  <td align=\"left\">$quantity&nbsp;</td>
  <td align=\"left\">$line_total&nbsp;</td>
  <td align=\"left\">$desc&nbsp;</td>
 </tr>";
}

$line_items_stmt->finish();

print <<"EOHTML";
</table>
<hr width="100%" noshade size="2" color="#000000">
<table border="1" width="100%" cellpadding="2" cellspacing="0">
 <tr>
  <th colspan="7" bgcolor="#C0C0C0">Settlement & Refund Details</th>
 </tr>
 <tr>
  <th>Settlement ID</th>
  <th>Settlement Amount</th>
  <th>Settlement Time</th>
  <th>Device Tran Result</th>
  <th>Settlement State</th>
  <th>Terminal Batch ID</th>
  <th>Terminal Batch Num</th>
 </tr>
EOHTML

my $settlement_details_stmt = $dbh->prepare(q{
	SELECT sb.settlement_batch_id, 
		TO_CHAR(sb.settlement_batch_end_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
		sbs.settlement_batch_state_name, 
		tsb.tran_settlement_b_amt,
		x.refund_id,
		x.refund_state_name,
		x.refund_type_desc,
		x.refund_amt, 
		x.refund_desc, 
		TO_CHAR(x.refund_issue_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
		x.refund_issue_by, 
		x.terminal_batch_id, 
		TO_CHAR(x.settlement_batch_end_ts, 'MM/DD/YYYY HH:MI:SS AM'),
		sb.terminal_batch_id,
		tb.terminal_batch_num,
		x.terminal_batch_num
	FROM pss.tran_settlement_batch tsb, 
		pss.settlement_batch sb, 
		pss.settlement_batch_state sbs,
		(	SELECT rsb.settlement_batch_id, 
				r.refund_id, 
				rs.refund_state_name, 
				rt.refund_type_desc,
				r.refund_amt, 
				r.refund_desc, 
				r.refund_issue_ts, 
				r.refund_issue_by, 
				r.terminal_batch_id, 
				sb.settlement_batch_end_ts,
				tb.terminal_batch_num
				FROM tran t,
				pss.refund r, 
				pss.refund_state rs, 
				pss.refund_type rt,
				pss.refund_settlement_batch rsb,
				pss.settlement_batch sb,
				pss.terminal_batch tb
			WHERE t.tran_id = r.tran_id
			AND rs.refund_state_id = r.refund_state_id
			AND rt.refund_type_cd = r.refund_type_cd
			AND t.tran_id = :tran_id
			AND r.refund_id = rsb.refund_id (+)
 			AND rsb.settlement_batch_id = sb.settlement_batch_id (+)
 			AND r.terminal_batch_id = tb.terminal_batch_id (+)
		) x,
		pss.terminal_batch tb
	WHERE tsb.auth_id = :sale_id
	AND tsb.settlement_batch_id = sb.settlement_batch_id (+)
	AND sb.settlement_batch_state_id = sbs.settlement_batch_state_id (+)
	AND tsb.settlement_batch_id = x.settlement_batch_id (+)
	AND sb.terminal_batch_id = tb.terminal_batch_id (+)
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$settlement_details_stmt->bind_param(":tran_id", $tran_id);
$settlement_details_stmt->bind_param(":sale_id", $sale_id);
$settlement_details_stmt->execute();
my %settlement_hash;
while (@data = $settlement_details_stmt->fetchrow_array()) 
{
	my ($settlement_batch_id, 
		$settlement_batch_ts, 
		$settlement_batch_state_name, 
		$tran_settlement_b_amt,
		$tran_refund_id,
		$tran_refund_state_name,
		$tran_refund_type_desc,
		$tran_refund_amount, 
		$tran_refund_desc, 
		$tran_refund_ts, 
		$tran_refund_issed_by, 
		$tran_refund_terminal_batch_id, 
		$tran_refund_settlement_ts,
		$terminal_batch_id,
		$terminal_batch_num,
		$tran_refund_terminal_batch_num) = @data;
	push @{$settlement_hash{$settlement_batch_id}}, {
		settlement_batch_id	=> $settlement_batch_id, 
		settlement_batch_ts	=> $settlement_batch_ts, 
		settlement_batch_state_name	=> $settlement_batch_state_name, 
		tran_settlement_b_amt => $tran_settlement_b_amt,
		tran_refund_id	=> $tran_refund_id,
		tran_refund_state_name	=> $tran_refund_state_name,
		tran_refund_type_desc	=> $tran_refund_type_desc,
		tran_refund_amount	=> $tran_refund_amount, 
		tran_refund_desc	=> $tran_refund_desc, 
		tran_refund_ts	=> $tran_refund_ts, 
		tran_refund_issed_by	=> $tran_refund_issed_by, 
		tran_refund_terminal_batch_id	=> $tran_refund_terminal_batch_id, 
		tran_refund_settlement_ts	=> $tran_refund_settlement_ts,
		terminal_batch_id	=> $terminal_batch_id,
		terminal_batch_num	=> $terminal_batch_num,
		tran_refund_terminal_batch_num	=> $tran_refund_terminal_batch_num
	};
}
#use Data::Dumper;
#print Dumper(\%settlement_hash);
#$VAR1 = {
#          '18364' => [
#                       {
#                         'tran_refund_issed_by' => 'USA Technologies, Inc.',
#                         'tran_refund_ts' => '10/12/2004 05:39:30 PM',
#                         'tran_refund_terminal_batch_id' => undef,
#                         'tran_refund_id' => '13',
#                         'settlement_batch_ts' => '10/12/2004 05:21:40 PM',
#                         'settlement_batch_id' => '18364',
#                         'tran_refund_state_name' => 'SERVER_REFUND_SUCCESS',
#                         'tran_refund_amount' => '7',
#                         'tran_refund_desc' => 'Actual purchase adjustment',
#                         'tran_refund_type_desc' => 'Generic Refund',
#                         'settlement_batch_state_name' => 'SERVER_SETTLEMENT_SUCCESS',
#                         'tran_refund_settlement_ts' => '12-OCT-04'
#                       }
#                     ]
#        };
foreach my $batch_id (sort {$settlement_hash{$a} <=> $settlement_hash{$b}} keys %settlement_hash)
{
	print <<"	EOHTML";
 <tr>
  <td align="left">$settlement_hash{$batch_id}->[0]->{settlement_batch_id}&nbsp;</td>
  <td align="left">$settlement_hash{$batch_id}->[0]->{tran_settlement_b_amt}&nbsp;</td>
  <td align="left">$settlement_hash{$batch_id}->[0]->{settlement_batch_ts}&nbsp;</td>
  <td align="left">$device_tran_result&nbsp;</td>
  <td align="left">$settlement_hash{$batch_id}->[0]->{settlement_batch_state_name}&nbsp;</td>
  <td align="left"><a href=\"terminal_batch.cgi?terminal_batch_id=$settlement_hash{$batch_id}->[0]->{terminal_batch_id}\">$settlement_hash{$batch_id}->[0]->{terminal_batch_id}</a>&nbsp;</td>
  <td align="left">$settlement_hash{$batch_id}->[0]->{terminal_batch_num}&nbsp;</td>
 </tr>
	EOHTML
	if (defined $settlement_hash{$batch_id}->[0]->{tran_refund_id})
	{
		print <<"		EOHTML";
 <tr>
  <td align="left">&nbsp;</td>
  <td align="left" colspan="2">
		EOHTML
		
		for (my $i = 0; $i < scalar @{$settlement_hash{$batch_id}}; $i++)
		{
			if (defined $settlement_hash{$batch_id}->[$i]->{tran_refund_id})
			{
				print <<"				EOHTML";
   <table border="1" cellpadding="2" cellspacing="0">
    <tr>
     <td nowrap><b>Refund ID</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_id}&nbsp;</td>
     <td nowrap><b>Issued By</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_issed_by}&nbsp;</td>
    </tr>
    <tr>
     <td nowrap><b>Amount</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_amount}&nbsp;</td>
     <td nowrap><b>Refund Time</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_ts}&nbsp;</td>
    </tr>
    <tr>
     <td nowrap><b>Type</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_type_desc}&nbsp;</td>
     <td nowrap><b>Settlement Time</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_settlement_ts}&nbsp;</td>
    </tr>
    <tr>
     <td nowrap><b>Description</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_desc}&nbsp;</td>
     <td nowrap><b>State</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_state_name}&nbsp;</td>
    </tr>
    <tr>
     <td nowrap><b>Terminal Batch ID</b></td>
     <td width=\"50%\"><a href=\"terminal_batch.cgi?terminal_batch_id=$settlement_hash{$batch_id}->[0]->{tran_refund_terminal_batch_id}\">$settlement_hash{$batch_id}->[$i]->{tran_refund_terminal_batch_id}</a>&nbsp;</td>
     <td nowrap><b>Terminal Batch Num</b></td>
     <td width=\"50%\">$settlement_hash{$batch_id}->[$i]->{tran_refund_terminal_batch_num}&nbsp;</td>
    </tr>
   </table>
				EOHTML
			}
		}
		
		print <<"		EOHTML";
     </td>
    </tr>
  </td>
 </tr>
		EOHTML
	}
}

$settlement_details_stmt->finish();

print "</table>
";

$dbh->disconnect;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
