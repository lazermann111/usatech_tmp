#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::Query;
use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::Common::Const;
use USAT::DeviceAdmin::UI::DAHeader;

require Text::CSV;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print "<br><br><font color=\"red\">Undefined Action!</font><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_type         = $PARAM{"device_type"};
my $location_id         = $PARAM{"location_id"};
my $customer_id         = $PARAM{"customer_id"};
my $firmware_version    = $PARAM{"firmware_version"};
my $start_serial_number = $PARAM{"start_serial_number"};
my $num_devices_seq     = $PARAM{"num_devices_seq"};
my $num_devices         = $PARAM{"num_devices"};
my $start_serial_head   = $PARAM{"start_serial_head"};
if(defined $start_serial_head && $start_serial_head eq '') {
   $start_serial_head ='%';
}

my $end_serial_number;

if(length($start_serial_number) > 0)
{
	$start_serial_number = &padSer($start_serial_head, $start_serial_number, $device_type);
}

if(length($start_serial_number) > 0 && length($num_devices_seq) > 0)
{
    if($device_type == PKG_DEVICE_TYPE__EDGE ) {
	   $end_serial_number = substr($start_serial_number, 3) + $num_devices_seq - 1;
	   $end_serial_number = &padSer($start_serial_head, $end_serial_number, $device_type);
    } else {
	   $end_serial_number = substr($start_serial_number, 2) + $num_devices_seq - 1;
	   $end_serial_number = &padSer($start_serial_head, $end_serial_number, $device_type);
    }
}

if(length($num_devices) == 0)
{
	$num_devices = 99;
}

if($num_devices > 100000)
{
	print "<br><br><font color=\"red\">Sorry, I cannot update more than 100000 devices at a time.<br><br>Please refine your search criteria to be more specific.</font><br><br>";
	print "<input type=\"button\" value=\"< Back\" onClick=\"javascript:history.go(-1);\"><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $search_string = "";
my @search_criteria;
push(@search_criteria, 'Y');

if(length($device_type) > 0)
{
	$search_string .=  " AND device.device_type_id = ?";
	push (@search_criteria, "$device_type");
}
		
if(length($location_id) > 0)
{
	$search_string .=  " AND location.location_id = ?";
	push(@search_criteria, $location_id);
}

if(length($customer_id) > 0)
{
	$search_string .=  " AND customer.customer_id = ?";
	push(@search_criteria, $customer_id);
}

if(length($start_serial_number) > 0)
{
	$search_string .=  " AND device.device_serial_cd >= ?";
	push(@search_criteria, $start_serial_number);
} else {
	$search_string .=  " AND device.device_serial_cd like ?";
	push(@search_criteria, $start_serial_head.'%');
}

if(length($end_serial_number) > 0)
{
	$search_string .=  " AND device.device_serial_cd <= ?";
	push(@search_criteria, $end_serial_number);
}

if(length($PARAM{"serial_number_list"}) > 0) {
    my $serial_number_str   = $PARAM{"serial_number_list"};
    my @serial_number_array = split(/\s/, $serial_number_str);
    my $questionmarks;
    foreach my $value ( @serial_number_array ) {
       chomp($value);
       if(length($value) > 0) {
	      push( @search_criteria, $value );
          $questionmarks .= "?,";
       }
    }
    chop($questionmarks);
	$search_string .=  " AND device.device_serial_cd IN( $questionmarks )";
}

if(length($firmware_version) > 0)
{
	$search_string .=  " AND device_setting.device_setting_value = ?";
	push(@search_criteria, $firmware_version);
}

my $sql = "
   SELECT * FROM (
        SELECT device.device_id,
               device.device_name,
               device.device_serial_cd,
               to_char(device.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'),
               location.location_name,
               customer.customer_name,
               NVL(device_setting.device_setting_value, ds2.device_setting_value),
			   device_type.device_type_desc
          FROM device,
               device_type,
               pos,
               location.location,
               customer,
               device_setting,
			   device_setting ds2
         WHERE device.device_type_id = device_type.device_type_id
           AND device.device_id = pos.device_id
           AND pos.location_id = location.location_id
           AND pos.customer_id = customer.customer_id
           AND device.device_id = device_setting.device_id(+)
		   AND device_setting.device_setting_parameter_cd(+) = DECODE(device.device_type_id, 11, 'Public_PC_Version', 'Firmware Version')
		   AND device.device_id = ds2.device_id(+)
           AND ds2.device_setting_parameter_cd(+) = DECODE(device.device_type_id, 11, 'SoftwareVersion', 'Firmware Version')
           AND device.device_active_yn_flag = ? ". $search_string . "
      ORDER BY device.device_serial_cd )
         WHERE rownum <= ?";

push(@search_criteria, $num_devices);
my $search_values = join(',', @search_criteria );
my $qo = OOCGI::Query->new($sql, { bind => \@search_criteria});
my $table = OOCGI::NTable->new("cellspacing=0 cellpadding=2 border=1 width=100%");
my $table_rn = 0;

$table->insert($table_rn, 0, B('Device Configuration Wizard - Page 2: Device Selection Confirmation'),
               'bgcolor=#C0C0C0 colspan=8 align=center');
$table_rn++;
my $format = 'align=center bgcolor=#C0C0C0';
$table->insert($table_rn, 0, B('Include?'),         $format);
$table->insert($table_rn, 1, B('Serial Number'),    $format);
$table->insert($table_rn, 2, B('Device Name'),      $format);
$table->insert($table_rn, 3, B('Device Type'),      $format);
$table->insert($table_rn, 4, B('Location'),         $format);
$table->insert($table_rn, 5, B('Customer'),         $format);
$table->insert($table_rn, 6, B('Firmware Version'), $format);
$table->insert($table_rn, 7, B('Last Activity'),    $format);

print <<EOHTML;
<form method="post" action="bulk_config_wizard_3.cgi">
<script>
    function confirmSubmit()
    {
       var obj = document.getElementById("check_count");
       if (obj.value > 0) {
         return true ;
       } else {
         alert("Please make a checkbox selection!");
         return false ;
       }
    }
    function count_check(field) {
       var count = 0;
       var obj = document.getElementById("check_count");
       for (i = 0; i < field.length; i++) {
          if(field[i].checked == true) {
             count++;
          }
       }
       obj.value = count;
    }

// following is checking all checkboxes
    var checkflag = "true";
    function check(field) {
       var obj = document.getElementById("check_count");
       var count = 0;
       if (checkflag == "false") {
          for (i = 0; i < field.length; i++) {
             field[i].checked = true;
             count++;
          }
          checkflag = "true";
          obj.value = count;
          return "Uncheck All";
       } else {
          for (i = 0; i < field.length; i++) {
             field[i].checked = false;
          }
          checkflag = "false";
          obj.value = 0;
          return "Check All";
       }
    }
</script>
EOHTML

my $counter = 0;

### kludge to work around javascript passing different types in single vs. multiple checkbox cases ###
my $js_func_value;
if ($qo->rowNumber() > 1) {
	$js_func_value = 'this.form.include_device_ids';
} else {
	$js_func_value = 'new Array(this.form.include_device_ids)';
}
while (my @data = $qo->next) 
{
    $table_rn++;
	$counter++;
    $table->insert($table_rn, 0, qq{<input type="checkbox" name="include_device_ids"
    onClick="count_check($js_func_value)" value=$data[0] checked>},'align=center');
    $table->insert($table_rn, 1, $data[2]);
    $table->insert($table_rn, 2, $data[1]);
	$table->insert($table_rn, 3, $data[7]);
    $table->insert($table_rn, 4, $data[4]);
    $table->insert($table_rn, 5, $data[5]);
    $table->insert($table_rn, 6, $data[6]);
    $table->insert($table_rn, 7, $data[3]);
}
$table_rn++;

my $controlTable = OOCGI::NTable->new('cellspacing=0 cellpadding=2 width=100%');
$controlTable->insert(0,0,qq{
       <input type=button value="Uncheck All" onClick="this.value=check($js_func_value)">
       <input type=text id=check_count value=$counter >},'bgcolor="#C0C0C0" width=40%');
$controlTable->insert(0,1,q{
       <input type="button" value="&lt; Back" onClick="javascript:history.go(-1);">
       <input type=button value="Cancel" onClick="javascript:window.location = '/';">
       <input type="submit" name="action" value="Next >" onClick="return confirmSubmit()"> }, 'bgcolor="#C0C0C0"');

if($counter == 0)
{
    $table->insert($table_rn,0, B('<br>Sorry, no device were found that match your search criteria!<br><br>'),
                   'colspan=8 style="font-size: 14pt;" align=center');
    $table_rn++;
}

$table->insert($table_rn,0, $controlTable, 'colspan=8 align=center');

display $table;
HIDDEN_PASS('start_serial_head');
HIDDEN('device_type', $device_type);

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub padSer
{
	my ($head, $num, $dev_type) = @_;
    if($dev_type == PKG_DEVICE_TYPE__EDGE) {
	   if(length($num) <= 8)
	   {
	   	   $num = substr('000000000', 0, (8-length($num))) . $num;
		   $num = $head . $num;
	   }
	   return $num;
   } else {
	   if(length($num) <= 6)
	   {
	   	   $num = substr('0000000', 0, (6-length($num))) . $num;
		   $num = $head . $num;
	   }
	   return $num;
   } 
}
