#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print q|<form method="post" action="new_consumer_acct_4.cgi">|;

my $err_str;

if($err_str)
{
	print $err_str;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my @actions_params_arr = $query->param("action_params");

my $rc = 0;	
my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
$table->put($rc++,0, B('USAT Card Creation Wizard'),'align=center bgcolor=#C0C0C0 colspan=2');

my $num_cards_text = OOCGI::Text->new(id => 'num_cards', name => 'num_cards', size => 4);
$table->put($rc,0,"Number of Cards to Generate", 'nowrap');
$table->put($rc++,1,"$num_cards_text");

my $expiration_text = OOCGI::Text->new(id => 'exp', name => 'exp', size => 4, );
$table->put($rc,0,"Expiration Date (Leave blank for no expiration)", 'nowrap');
$table->put($rc++,1,"$expiration_text YYMM");


if($params{'consumer_acct_fmt_id'} eq '0')
{
	my $balance_text = OOCGI::Text->new(id => 'balance', name => 'balance', size => 4);
	$table->put($rc,0,"Card Starting Balance (Whole Dollars)", 'nowrap');
	$table->put($rc++,1,"$balance_text");
}

display $table;

HIDDEN_PASS('consumer_acct_fmt_id', 'customer_id', 'location_id', 'authority_id', 'merchant_id', 'merchant_name', 'merchant_desc', 'device_type_id', 'action_id');
HIDDEN('action_params',join(',', @actions_params_arr)) if(scalar(@actions_params_arr) > 0);

print "&nbsp<br>" . SUBMIT('action','Next >') . "</form><br>&nbsp;";
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
