#!/usr/local/USAT/bin/perl


use strict;

use OOCGI::OOCGI;
use warnings;
use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use Date::Calc;
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;

use constant DEBUG => 0;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

our $dbh;
&main();

sub main 
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
	$dbh = $DATABASE->{handle};

	if(DEBUG)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		print "<pre>\n";
		print Dumper(\%PARAM);
	}

	### determine action ###
	my $action = length $PARAM{action} > 0 ? $PARAM{action} : printError("Required Parameter Not Found: action");

	if($action eq 'Save')
	{
		my $pos_pta_tmpl_name = length $PARAM{name} > 0 ? $PARAM{name} : printError("Required Parameter Not Found: name");
		my $pos_pta_tmpl_desc = length $PARAM{desc} > 0 ? $PARAM{desc} : printError("Required Parameter Not Found: desc");
		
		my $insert_pos_pta_tmpl = $DATABASE->do(
			query => q{
				INSERT INTO pos_pta_tmpl(pos_pta_tmpl_name, pos_pta_tmpl_desc)
				VALUES(?, ?)
			},
			values => [
				$pos_pta_tmpl_name,
				$pos_pta_tmpl_desc
			]
		);
		printError("Database error inserting pos_pta_tmpl($pos_pta_tmpl_name, $pos_pta_tmpl_desc): ".$DATABASE->errstr) if defined $DATABASE->errstr;

		$dbh->disconnect;
		print CGI->redirect("/template_menu.cgi");
		exit 0;
	}
	
	### check input variables ###
	my $pos_pta_tmpl_id = length $PARAM{pos_pta_tmpl_id} > 0 ? $PARAM{pos_pta_tmpl_id} : printError("Required Parameter Not Found: pos_pta_tmpl_id");
	
	my $err;
	my @template_data = USAT::DeviceAdmin::Profile::PaymentType::get_template_data($dbh, $pos_pta_tmpl_id, \$err);
	printError("<br><br>$err<br><br>") if defined $err;
	print Dumper(\@template_data)."-->" if DEBUG;
	printError("<br><h3>Template not found: $pos_pta_tmpl_id</h3><br><br>") unless scalar @template_data;
	
	if ($action eq 'Reorder')
	{
		my %priorities_hash = ();
		
		foreach my $param (keys %PARAM)
		{
			if($param =~ /^new_priority_/)
			{
				my $pos_pta_tmpl_entry_id = substr($param, 13);
				my $new_priority = $PARAM{"new_priority_$pos_pta_tmpl_entry_id"};
				my $entry_payment_entry_method_cd = $PARAM{"payment_entry_method_cd_$pos_pta_tmpl_entry_id"};
				
				printError("Missing Category Priority Value! ($pos_pta_tmpl_entry_id, $new_priority, $entry_payment_entry_method_cd)") if !$new_priority;
				
				my $hash_key = "$entry_payment_entry_method_cd.$new_priority";
				
				if(defined $priorities_hash{$hash_key})
				{
					printError("Duplicate Category Priority Values! ($pos_pta_tmpl_entry_id, $new_priority, $entry_payment_entry_method_cd)");
				}
				else
				{
					$priorities_hash{$hash_key} = $new_priority;
				}
			}
		}

		foreach my $param (keys %PARAM)
		{
			if($param =~ /^new_priority_/)
			{
				my $pos_pta_tmpl_entry_id = substr($param, 13);
				my $new_priority = $PARAM{"new_priority_$pos_pta_tmpl_entry_id"};
				my $old_priority = $PARAM{"old_priority_$pos_pta_tmpl_entry_id"};
				
				if($new_priority != $old_priority)
				{
					my $update_pos_pta_tmpl_entry_priority = $DATABASE->do(
						query => q{
							UPDATE pos_pta_tmpl_entry
							SET pos_pta_priority = ?
							WHERE pos_pta_tmpl_entry_id = ?
						},
						values => [
							$new_priority,
							$pos_pta_tmpl_entry_id
						]
					);
					printError("Database error updating pos_pta_tmpl_entry $pos_pta_tmpl_entry_id: ".$DATABASE->errstr) if defined $DATABASE->errstr;
				}
			}
		}
		
		$dbh->disconnect;
		print CGI->redirect("/edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id");
		exit 0;
	}
	elsif ($action eq 'Add New')
	{
		### print header ###
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;

		### check input variables ###
		my ($pos_pta_tmpl_id, $payment_entry_method_cd, $payment_entry_method_desc);
		foreach my $req_param ("pos_pta_tmpl_id", "payment_entry_method_cd")
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print Dumper(eval "\$$req_param") if DEBUG;
		}
		
		my $get_payment_entry_method = $dbh->prepare(q{
			SELECT payment_entry_method_desc
			FROM pss.payment_entry_method
			WHERE payment_entry_method_cd = :payment_entry_method_cd
		}) or _print_error("Couldn't prepare statement: " . $dbh->errstr);
		$get_payment_entry_method->bind_param(":payment_entry_method_cd", $payment_entry_method_cd);
		$get_payment_entry_method->execute() or _print_error("Couldn't execute statement: " . $dbh->errstr);
		if (my $row_aref = $get_payment_entry_method->fetchrow_arrayref()) {
			$payment_entry_method_desc = $row_aref->[0];
		}
		$get_payment_entry_method->finish();		
		
		### load list of available new types for this template ###
		my $err;
		my @payment_subtypes = USAT::DeviceAdmin::Profile::PaymentType::get_available_payment_subtypes_by_payment_entry_method_cd($dbh, $payment_entry_method_cd, \$err);
		printError("<br><br>$err<br><br>") if defined $err;
		print Dumper(\@payment_subtypes)."-->" if DEBUG;
		printError("<br><h3>No payment types available to be added at this time.</h3><br><br>") unless scalar @payment_subtypes;
				
		### print form ###
		print <<"		EOHTML";
<table width="100%" border="1" cellpadding="2" cellspacing="0">
<form method="post" action="edit_pos_pta_tmpl_entry.cgi">
<input type="hidden" name="pos_pta_tmpl_id" value="$pos_pta_tmpl_id">
 <tr>
  <th colspan="4" bgcolor="#C0C0C0">New Payment Type Template Entry<br></th>
 </tr>
 <tr>
  <th colspan="4" bgcolor="#C0C0C0">$template_data[1]<br></th>
 </tr>
 <tr>
  <td width="25%" nowrap>Template ID</td>
  <td width="25%">$pos_pta_tmpl_id</td>
  <td width="25%" nowrap>Payment Entry Method</td>
  <td width="25%">$payment_entry_method_desc</td>
 </tr>
 <tr>
  <td nowrap>Payment Type</td>
  <td colspan="3">
		EOHTML
		if (scalar @payment_subtypes > 0)
		{
			print "  <select name=\"payment_subtype_id\">\n";
			print "   <option value=\"\">-- Choose a type --</option>\n";
			print "   <option value=\"$_->[0]\">$_->[1]</option>\n" foreach @payment_subtypes;
			print "  </select>\n";
		}
		else
		{
			print "   <input type=\"hidden\" name=\"payment_subtype_id\" value=\"N/A\">N/A\n";
		}
		print <<"		EOHTML";
  </td>
 </tr>
 <tr>
  <td colspan="4" align="center">
   <input type="button" value="&lt; Back" onClick="javascript:window.location='/edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id';">
   <input type="submit" name="action" value="Next &gt;">&nbsp;
  </td>
 </tr>
</form>
</table>
		EOHTML
	}
	elsif ($action eq 'Edit')
	{
		my $pos_pta_tmpl_entry_id = length $PARAM{pos_pta_tmpl_entry_id} > 0 ? $PARAM{pos_pta_tmpl_entry_id} : printError("Required Parameter Not Found: pos_pta_tmpl_entry_id");
		$dbh->disconnect;
		print CGI->redirect("/edit_pos_pta_tmpl_entry.cgi?pos_pta_tmpl_entry_id=$pos_pta_tmpl_entry_id&pos_pta_tmpl_id=$pos_pta_tmpl_id&action=Edit");
		exit 0;
	}
	elsif ($action eq 'Update')
	{
		my $name = length $PARAM{name} > 0 ? $PARAM{name} : printError("Required Parameter Not Found: name");
		my $desc = length $PARAM{desc} > 0 ? $PARAM{desc} : printError("Required Parameter Not Found: desc");
		
		my $update_pos_pta_tmpl_entry_priority = $DATABASE->do(
			query => q{
				UPDATE pos_pta_tmpl
				SET pos_pta_tmpl_name = ?,
					pos_pta_tmpl_desc = ?
				WHERE pos_pta_tmpl_id = ?
			},
			values => [
				$name,
				$desc,
				$pos_pta_tmpl_id
			]
		);
		printError("Database error updating pos_pta_tmpl $pos_pta_tmpl_id: ".$DATABASE->errstr) if defined $DATABASE->errstr;
		
		$dbh->disconnect;
		print CGI->redirect("/edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id");
		exit 0;
	}
	else
	{
		printError("Unknown action Parameter: $action");
	}

	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub printError ($) {
	my $err_txt = shift;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	print "<pre>\n";
	print "$err_txt\n\n";
	print Dumper(\%PARAM);
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}
