#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
require Text::CSV;
use POSIX;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::POS::API::PTA::Util;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $source_device_id = $PARAM{"source_device_id"};
my $target_device_id = $PARAM{"target_device_id"};
my $source_ev_number = $PARAM{"source_ev_number"};
my $target_ev_number = $PARAM{"target_ev_number"};
my $source_pos_id = $PARAM{"source_pos_id"};
my $target_pos_id = $PARAM{"target_pos_id"};
my $source_location_id = $PARAM{"source_location_id"};
my $target_location_id = $PARAM{"target_location_id"};
my $source_customer_id = $PARAM{"source_customer_id"};
my $target_customer_id = $PARAM{"target_customer_id"};

my $source_file_name = "$source_ev_number-CFG";
my $raw_map_data = USAT::DeviceAdmin::Util::load_file($source_file_name, $dbh);
if(not defined $raw_map_data)
{
	print "$source_file_name not found";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

$raw_map_data = pack("H*", $raw_map_data);

my $poke1_result = -1;
my ($poke1_msg, $poke1_err);
my $poke2_result = -1;
my ($poke2_msg, $poke2_err);
my $poke3_result = -1;
my ($poke3_msg, $poke3_err);
my $counters_result = -1;
my ($counters_msg, $counters_err);
my $custloc_result = -1;

# we are sending very specific sections of the config, not the whole thing for G4/G5
#	0 	- 135	/ 2 = 0		length = 136
#	142 - 283	/ 2 = 71	length = 142
#	356 - 511	/ 2 = 178	length = 156

my $config_part_1 = unpack("H*",substr($raw_map_data, 0, 136));
my $config_part_2 = unpack("H*",substr($raw_map_data, 142, 142));
my $config_part_3 = unpack("H*",substr($raw_map_data, 356, 156));
my $zero_all_counters = '000000000000000000000000000000000000000000000000000000000000000000000000';

($poke1_result, $poke1_msg, $poke1_err) = &update_config_part($dbh,$target_device_id, 0, $config_part_1);
($poke2_result, $poke2_msg, $poke2_err) = &update_config_part($dbh,$target_device_id, 142, $config_part_2);
($poke3_result, $poke3_msg, $poke3_err) = &update_config_part($dbh,$target_device_id, 356, $config_part_3);
($counters_result, $counters_msg, $counters_err) = &update_config_part($dbh,$target_device_id, 320, $zero_all_counters);

print ("<table width=\"100%\" cellspacing=\"0\" cellpadding=\"2\" border=\"1\"><tr><td><pre>\n");

my $error_count = 0;

if($poke1_result < 0)
{
	print "Error creating Poke 1! $poke1_msg, $poke1_err\n";
	$error_count++;
}
elsif($poke1_result == 1)
{
	print "Poke 1 created successfully!\n";
}
else
{
	print "Poke 1 skipped, no change.\n";
}

if($poke2_result < 0)
{
	print "Error creating Poke 2!  $poke2_msg, $poke2_err\n";
	$error_count++;
}
elsif($poke2_result == 1)
{
	print "Poke 2 created successfully!\n";
}
else
{
	print "Poke 2 skipped: no change.\n";
}

if($poke3_result < 0)
{
	print "Error creating Poke 3!  $poke3_msg, $poke3_err\n";
	$error_count++;
}
elsif($poke3_result == 1)
{
	print "Poke 3 created successfully!\n";
}
else
{
	print "Poke 3 skipped: no change.\n";
}

if($counters_result < 0)
{
	print "Error creating Counters Poke!  $counters_msg, $counters_err\n";
	$error_count++;
}
elsif($counters_result == 1)
{
	print "Counters Poke created successfully!\n";
}
else
{
	print "Counters Poke skipped: no change.\n";
}

if($source_location_id != $target_location_id || $source_customer_id != $target_customer_id)
{
	my $result = $DATABASE->callproc(
		name  => 'pkg_device_maint.sp_update_loc_id_cust_id',
		bind  => [ qw(:deviceID :newLocationID :newCustomerId :newDeviceID :newPOSID :statusCd :errMsg) ],
		in    => [ $target_device_id, $source_location_id, $source_customer_id ],
		inout => [ 20, 20, 20, 2000 ]
	);
	if(defined $result && defined $result->[2] && $result->[2] == 0)
	{
		print "Customer and Location Updated successfully!\n";
		$target_device_id = $result->[0];
		$target_pos_id = $result->[1];
	}
	else
	{
		my $db_err_str = defined $result && defined $result->[3] ? $result->[3] : $dbh->errstr;
		print "Error updating Customer and Location! $db_err_str\n";
		$error_count++;
	}
}
else
{
	print "Customer and Location update skipped: no change.\n";
}

my $err_ref;
my $res = USAT::POS::API::PTA::Util::clone_device_pos_ptas($DATABASE, undef, undef, \$err_ref, $source_pos_id, $target_pos_id);
if(!$res)
{
	print "Error cloning Payment Types! $err_ref\n";
	$error_count++;
}
else
{
	print "Payment Types Cloned Successfully!\n";
}

if($error_count > 0)
{
	print "WARNING: $error_count of 6 updated failed!  Please print or save this page and sent it to an administrator for help.\n";
}
else
{
	print "\n\nDevice Cloned Successfully!\n\n";
}
print "<a href=\"profile.cgi?device_id=$target_device_id\">Continue</a>\n";
print "</pre></td></tr></table>";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub update_config_part
{
	my ($dbh, $device_id, $offset, $new_data) = @_;
	my ($return_code, $return_msg,$db_err_str);
	my $update_ref = $dbh->prepare("BEGIN sp_update_config (:device_id, :offset, :new_data, :data_type, :device_type, :debug, :return_code, :return_msg); END;");
	$update_ref->bind_param(":device_id", $device_id);
	$update_ref->bind_param(":offset", $offset);
	$update_ref->bind_param(":new_data", $new_data);
	$update_ref->bind_param(":data_type", 'H');
	$update_ref->bind_param(":device_type", '1');		# only allowing G4/G5 for now... so hardcoding this
	$update_ref->bind_param(":debug", '0');
	$update_ref->bind_param_inout(":return_code", \$return_code, 40);
	$update_ref->bind_param_inout(":return_msg", \$return_msg, 2048);
	$update_ref->execute;
	$db_err_str = $dbh->errstr;
	$update_ref->finish();
	return ($return_code, $return_msg, $db_err_str);
}

