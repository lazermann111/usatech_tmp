#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;

my %params = OOCGI::OOCGI->new()->Vars();

my $prefix             = lc $params{location_prefix};
my $location_id        = $params{location_id};
my $parent_location_id = $params{parent_location_id};

print "Content-Type: text/html; charset=ISO-8859-1\n\n";

my $sql;
my $bind;
if($parent_location_id) {
$sql = q|SELECT location_id, location_name, location_city, location_state_cd, location_country_cd
           FROM location.location
          WHERE location_active_yn_flag = 'Y'
            AND location_id = ? /* parent location id */
          UNION
         SELECT location_id, location_name, location_city, location_state_cd, location_country_cd
           FROM location.location
          WHERE location_active_yn_flag = 'Y'
            AND location_id != 1
            AND LOWER(location_name) like ?
            AND location_id != ? /* parent location id */
            AND location_id NOT IN (
                SELECT DISTINCT descendent_location_id
                  FROM vw_location_hierarchy
                 WHERE ancestor_location_id = ? /* location id */
            )
       ORDER BY location_name|;
   $bind = [$parent_location_id,$prefix.'%', $parent_location_id,$location_id];
} else {
$sql = q|SELECT location_id, location_name, location_city, location_state_cd, location_country_cd
           FROM location.location
          WHERE location_active_yn_flag = 'Y'
            AND location_id != 1
            AND LOWER(location_name) like ?
            AND location_id NOT IN (
                SELECT DISTINCT descendent_location_id
                  FROM vw_location_hierarchy
                 WHERE ancestor_location_id = ? /* location id */
            )
       ORDER BY location_name|;
   $bind = [$prefix.'%',$location_id];
}

my $popLocation = OOCGI::Popup->new(name => 'USAT::DeviceAdmin::DBObj::Location__parent_location_id__'.$location_id,
                  delimiter => '.',
                  id    => 'parent_location_id',
                  align => 'yes',
                  bind  => $bind,
                  style => "font-family: courier; font-size: 12px;",
                  sql   => $sql);

$popLocation->head('','Undefined');
if($parent_location_id) {
   $popLocation->default($parent_location_id);
} else {
   $popLocation->default('');
}

print $popLocation->string;
