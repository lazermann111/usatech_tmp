#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Common::Const;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::Util;
use Crypt::Rijndael_PP ':all';
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $device_name = $PARAM{"device_name"};

if(length($device_name) == 0)
{
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	print $user_menu;

	print "\nRequired Parameter Not Found: device_name\n";

	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit();
}

my ($device_id, $device_serial_cd, $device_type_id) = @{USAT::DeviceAdmin::Util::get_device_info_by_name($dbh, $device_name)};
if (!$device_id) {
	print "Device $device_name not found";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $edit_mode = $PARAM{"edit_mode"};
my $new_ini_file = '';

if ($edit_mode eq 'raw')
{
	$new_ini_file = $PARAM{"ini_file"};
}
else
{
	my $PUBLIC_PC = 1;
	my $device_subtype = 0;
	
	if ($PARAM{"config_params"} =~ m/Public_PC_Version/)
	{
		$device_subtype = $PUBLIC_PC;
	}

	my @config_params = split(/\|\|\|/, $PARAM{"config_params"});
	foreach my $config_param (@config_params)
	{	
		my $param_value = $PARAM{"cfg_$config_param"};
	
		if ($device_subtype == $PUBLIC_PC && $config_param =~ m/^(OpenAPIPassword|MFPAdminPassword|SNMPCommunity)$/)
		{			
			my $remainder = length($param_value) % 16;
			#pad value with 0x00
			$param_value .= pack('H', '00') x (16 - $remainder) if $remainder > 0;		
			#encrypt value using shared key
			$param_value = uc(unpack('H*', rijndael_encrypt('703449366F54514E34735039326C5947', MODE_ECB, $param_value, 128, 128)));	
		}

		$new_ini_file = "$new_ini_file\r\n$config_param=" . $param_value;
	}
	
	$new_ini_file = substr($new_ini_file, 2);
}

my $old_ini_file_hex = $PARAM{"old_ini_file_hex"};
my $action = $PARAM{"myaction"};
my $file_id;

my $new_ini_file_hex = uc(unpack("H*", $new_ini_file));

my $file_name;

if ($action eq 'Backup')
{
	$file_name = $device_name . "-BAK";
}
else
{
	$file_name = $device_name . "-CFG";
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print "
<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
 <tr>
  <td align=\"left\">
   <br>
   <center>
   <form>";
   
	print "
		<input type=button value=\"Return to Device Profile\" onClick=\"javascript:window.location = 'profile.cgi?ev_number=$device_name';\">";

print "
   </form>
   </center>
   <pre>
";

if(defined $device_name)
{
	print "\nProcessing device configuration...\n";
}
else
{
	print "\nProcessing configuration template...\n";
}

FILE_OPERATIONS: while (1) 
{	
	my $update_file = 0;
	my $file_stmt;
	$file_stmt = $dbh->prepare("select min(file_transfer_id) from device.file_transfer where file_transfer_name = :file_name and file_transfer_type_cd = :file_transfer_type_cd") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$file_stmt->bind_param(":file_name", $file_name);
	$file_stmt->bind_param(":file_transfer_type_cd", PKG_FILE_TYPE__CONFIGURATION_FILE);
	$file_stmt->execute();

	my @file_data = $file_stmt->fetchrow_array();
	$file_stmt->finish();

	if(@file_data)
	{
		$file_id = $file_data[0];
	}
	else
	{
		$update_file = 1;
		my $file_ins_stmt = $dbh->prepare("insert into device.file_transfer(file_transfer_name, file_transfer_type_cd) values(:file_name, :file_transfer_type_cd)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$file_ins_stmt->bind_param(":file_name", $file_name);
		$file_ins_stmt->bind_param(":file_transfer_type_cd", PKG_FILE_TYPE__CONFIGURATION_FILE);
		$file_ins_stmt->execute();	
		if(defined $dbh->errstr)
		{
			print "\nsql error: " . $dbh->errstr . "\n";
			last FILE_OPERATIONS;
		}
		$file_ins_stmt->finish();

		$file_stmt->execute();
		@file_data = $file_stmt->fetchrow_array();
		$file_stmt->finish();

		if(!@file_data)
		{
			print "\nFailed to get a new device_file_transfer_id!\n";
			last FILE_OPERATIONS;
		}

		$file_id = $file_data[0];
	}

	if(($new_ini_file_hex ne $old_ini_file_hex) || ($update_file == 1))
	{
		print "\n<b>Saving file $file_name...</b>\n\n";

		print "ASCII Data:<br>";
		print "<textarea rows=\"10\" cols=\"100\" wrap=\"virtual\">$new_ini_file</textarea>\n\n";

		print "HEX Data:<br>";
		print "<textarea rows=\"10\" cols=\"100\" wrap=\"virtual\">$new_ini_file_hex</textarea>\n";

		my $stmt = $dbh->prepare(q{update file_transfer set file_transfer_content = :file_content where file_transfer_name = :file_name});
		$stmt->bind_param(":file_name", $file_name);
		$stmt->bind_param(":file_content", unpack("H*", $new_ini_file));
		$stmt->execute();
		if(defined $dbh->errstr)
	   	{
		   	print "\nblob update error: " . $dbh->errstr . "\n";
			last FILE_OPERATIONS;
		}

		$stmt->finish();

		print "\n<b>Successfully saved file $file_name.</b>\n";
		
		if ($action ne 'Backup')
		{
			print "\n<b>Updating device settings...</b>\n";

			#update device settings

			$DATABASE->do(
				query	=> q{
					DELETE FROM device.device_setting WHERE device_id = ? AND device_setting_parameter_cd <> 'New Merchant ID'
				},
				values	=> [
					$device_id
				]
			);

			my @config_lines = split(/\r\n/, $new_ini_file);
			foreach my $config_line (@config_lines)
			{
				chomp($config_line);
				my ($config_param, $config_value) = split(/=/, $config_line, 2);	
				($config_param, $config_value) = (substr($config_param, 0, 60), substr($config_value, 0, 200));

				$config_param =~ s/^\s+//;
				$config_param =~ s/\s+$//;
				$config_value =~ s/^\s+//;
				$config_value =~ s/\s+$//;

				if (length($config_param) > 0)
				{
					if (length($config_value) == 0)
					{
						$config_value = ' ';
					}

					my $config_param_ref = $DATABASE->select(
											table			=> 'device.device_setting_parameter',
											select_columns	=> 'count(1)',
											where_columns	=> ['device_setting_parameter_cd = ?'],
											where_values	=> [$config_param] );

					if($config_param_ref->[0][0] eq '0')
					{
						$DATABASE->insert(
									table			=> 'device.device_setting_parameter',
									insert_columns	=> 'device_setting_parameter_cd',
									insert_values	=> [$config_param]);
					}

					$DATABASE->insert(
								table			=> 'device.device_setting',
								insert_columns	=> 'device_id, device_setting_parameter_cd, device_setting_value',
								insert_values	=> [$device_id, $config_param, $config_value]);
				}
			}

			print "\n<b>Successfully updated device settings.</b>\n";
		}
	}
	else
	{
		print "\nNo User Changes!\n";
	}

	if($action eq 'Save and Send')
	{
		print "\n<b>Creating command to send config file...</b>\n";
	
		my $cmd_file_transfer_start;
			
		if ($device_type_id =~ m/^(4|11|12)$/) # Sony, Kiosk, T2
		{
			$cmd_file_transfer_start = 'A4';
			
			#removing any requests for the config file
			$DATABASE->update(	table	=> 'engine.machine_cmd_pending',
				update_columns	=> 'execute_cd',
				update_values	=> ['A'],
				where_columns	=> ['machine_id = ?', 'data_type = ?', 'upper(command) = upper(?)'],
				where_values	=> [$device_name, '9B', unpack("H*", "EportNW.ini")] );
		}
		else
		{
			$cmd_file_transfer_start = '7C';
		}

		# check if a pending file transfer start command already exists for this device and file
		my $check_cmd_stmt = $dbh->prepare("select count(1) from device.device_file_transfer dft, engine.machine_cmd_pending mcp where dft.device_id = :device_id and dft.file_transfer_id = :file_id and dft.device_file_transfer_status_cd = 0 and dft.device_file_transfer_direct = 'O' and mcp.machine_id = :ev_number and mcp.data_type = :data_type and dft.device_file_transfer_id = mcp.command and mcp.execute_cd = 'P'") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$check_cmd_stmt->bind_param(":device_id", $device_id);
		$check_cmd_stmt->bind_param(":file_id", $file_id);
		$check_cmd_stmt->bind_param(":ev_number", $device_name);
		$check_cmd_stmt->bind_param(":data_type", $cmd_file_transfer_start);
		$check_cmd_stmt->execute();

		my @check_cmd_data = $check_cmd_stmt->fetchrow_array();
		my $check_cmd_count = $check_cmd_data[0];
		$check_cmd_stmt->finish();

		if($check_cmd_count > 0)
		{
			print "\nA pending File Transfer Start command already exists for device $device_name and file $file_name\n";
			last FILE_OPERATIONS;
		}
		
		# now get the next device_file_transfer_id from the sequence
		my $get_transfer_id_stmt = $dbh->prepare("select SEQ_DEVICE_FILE_TRANSFER_ID.nextval from dual") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$get_transfer_id_stmt->execute();
		my @transfer_id_data = $get_transfer_id_stmt->fetchrow_array();

		if(!@transfer_id_data)
		{
			print "\nFailed to get a new device_file_transfer_id!\n";
			last FILE_OPERATIONS;
		}

		$get_transfer_id_stmt->finish();
		my $transfer_id = $transfer_id_data[0];		
		
		# now insert the device_file_transfer
		my $insert_transfer_stmt = $dbh->prepare("insert into device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size) values (:transfer_id, :device_id, :file_id, :direction, :status, :packet_size)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$insert_transfer_stmt->bind_param(":transfer_id", $transfer_id); 
		$insert_transfer_stmt->bind_param(":device_id", $device_id);
		$insert_transfer_stmt->bind_param(":file_id", $file_id);
		$insert_transfer_stmt->bind_param(":direction", 'O');
		$insert_transfer_stmt->bind_param(":status", '0');
		$insert_transfer_stmt->bind_param(":packet_size", 1024);
		$insert_transfer_stmt->execute();
		
		if(defined $dbh->errstr)
		{
			print "\nsql error: " . $dbh->errstr . "\n";
			last FILE_OPERATIONS;
		}
		$insert_transfer_stmt->finish();
		
		# now insert a pending message for this new transfer
		my $insert_pending_msg_stmt = $dbh->prepare("insert into engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order) values (:ev_number, :data_type, :transfer_id, :execute_cd, :execute_order)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$insert_pending_msg_stmt->bind_param(":ev_number", $device_name); 
		$insert_pending_msg_stmt->bind_param(":data_type", $cmd_file_transfer_start); 
		$insert_pending_msg_stmt->bind_param(":transfer_id", $transfer_id); 
		$insert_pending_msg_stmt->bind_param(":execute_cd", 'P'); 
		$insert_pending_msg_stmt->bind_param(":execute_order", '1'); 
		$insert_pending_msg_stmt->execute();
		
		if(defined $dbh->errstr)
		{
			print "\nsql error: " . $dbh->errstr . "\n";
			last FILE_OPERATIONS;
		}
		$insert_pending_msg_stmt->finish();
		
		print "\n<b>Successfully created File Transfer Start command.</b>\n";
	}

	last FILE_OPERATIONS;
};

$dbh->disconnect;

print "
   </pre>
  </td>
 </tr>
 <tr>
  <td align=\"left\">
   <center>
   <form>
   ";

if(defined $device_name)
{
	print "<input type=button value=\"Return to Device Profile\" onClick=\"javascript:window.location = 'profile.cgi?ev_number=$device_name';\">";
}
else 
{
	print "<input type=button value=\"Return to Template Menu\" onClick=\"javascript:window.location = 'template_menu.cgi';\">";
}

print "
   </form>
   </center>
  </td>
 </tr>  
</table>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
