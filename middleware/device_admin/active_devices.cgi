#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::App::DeviceAdmin::Const qw(:globals :globals_hash);
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $time_value   = $PARAM{"time_value"};
my $time_units   = $PARAM{"time_units"};
my $device_limit = $PARAM{"device_limit"};

if(length($time_value) == 0)
{
	print "Required parameter not found: time_value\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if(length($time_units) == 0)
{
	print "Required parameter not found: time_units\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if(length($device_limit) == 0)
{
	print "Required parameter not found: device_limit\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $list_devices_stmt = $dbh->prepare(q{
	select *
	from
	(
    	select machine_id, count(1)
	    from engine.machine_cmd_inbound_hist
    	where inbound_date > (sysdate-(:time_value/:time_units))
	    group by machine_id
    	having count(1) > 1
	    order by count(1) desc
	)
	where rownum <= :device_limit
});


$list_devices_stmt->bind_param(":time_value", $time_value);
$list_devices_stmt->bind_param(":time_units", $time_units);
$list_devices_stmt->bind_param(":device_limit", $device_limit);
$list_devices_stmt->execute();
my $device_list_ref = $list_devices_stmt->fetchall_arrayref();
my $device_count = scalar(@$device_list_ref);
$list_devices_stmt->finish();

print "<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">";

if($device_count == 0)
{
	print "
	 <tr>
	  <th style=\"font-size: 10pt;\">No devices found that match your search criteria!</th>
	 </td>
	";
}
else
{
	print "
	 <tr>
	  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Message Count</th>
	  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Device Name</th>
	  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Serial Number</th>
	  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Device Type</th>
	  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Location</th>
	  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Customer</th>
	 </tr>
	";
	
	foreach my $device_ref (@{$device_list_ref})
	{
		if($device_ref->[0] eq 'EV017289')
		{
			# EV017289 is IPSentry
			next;
		}
		
		my $get_device_info_stmt = $dbh->prepare("select device_id, device_name, device_serial_cd, device_type_desc, to_char(last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), device_type_id, device_active_yn_flag, rownum as row_num, customer_name, location_name, device_setting_value, ROUND((SYSDATE - last_activity_ts), 1) from ( select device.device_id, device.device_name, device.device_serial_cd, device_type.device_type_desc, last_activity_ts, device.device_type_id, device.device_active_yn_flag, customer.customer_name, location.location_name, device_setting.device_setting_value from device.device, device.device_type, pss.pos, location.customer, location.location, device.device_setting where device.device_type_id = device_type.device_type_id and device.device_id = pos.device_id and pos.location_id = location.location_id and pos.customer_id = customer.customer_id and device.device_id = device_setting.device_id(+) and device_setting.device_setting_parameter_cd(+) = 'Firmware Version' and device.device_name = :device_name and device.device_active_yn_flag = :enabled order by device.device_name, device.created_ts desc )") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$get_device_info_stmt->bind_param(":device_name", $device_ref->[0]);
		$get_device_info_stmt->bind_param(":enabled", 'Y');
		$get_device_info_stmt->execute();
		my @device_data = $get_device_info_stmt->fetchrow_array();
		$get_device_info_stmt->finish();
		
		my $get_message_info_stmt = $dbh->prepare(q{
			select upper(decode(substr(inbound_command,0,2), '9A', substr(inbound_command,0,4), substr(inbound_command,0,2))), count(1)
			from engine.machine_cmd_inbound_hist
			where inbound_date > (sysdate-(:time_value/:time_units))
			and machine_id = :machine_id
			group by upper(decode(substr(inbound_command,0,2), '9A', substr(inbound_command,0,4), substr(inbound_command,0,2)))
			order by count(1) desc
		});
	
		$get_message_info_stmt->bind_param(":machine_id", $device_ref->[0]);
		$get_message_info_stmt->bind_param(":time_value", $time_value);
		$get_message_info_stmt->bind_param(":time_units", $time_units);
		$get_message_info_stmt->execute();
		my $message_list_ref = $get_message_info_stmt->fetchall_arrayref();
		$get_message_info_stmt->finish();
		
		print "
		 <tr>
		  <td style=\"font-size: 10pt;\">$device_ref->[1]&nbsp;</td>
		  <td style=\"font-size: 10pt;\">$device_ref->[0]&nbsp;</td>
		  <td style=\"font-size: 10pt;\"><a href=\"profile.cgi?serial_number=$device_data[2]\">$device_data[2]</a>&nbsp;</td>
		  <td style=\"font-size: 10pt;\">$device_data[3]&nbsp;</td>
		  <td style=\"font-size: 10pt;\">$device_data[9]&nbsp;</td>
		  <td style=\"font-size: 10pt;\">$device_data[8]&nbsp;</td>
		 </tr>
		";
	
		print "
		 <tr>
		  <td style=\"font-size: 10pt;\" colspan=\"6\">
		   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
		";
	
		foreach my $message_ref (@{$message_list_ref})
		{
			print "
			<tr>
			 <td>&nbsp;&nbsp;&nbsp;</td>
			 <td style=\"font-size: 10pt;\">$message_ref->[1]</td>
			 <td style=\"font-size: 10pt;\" nowrap width=\"100%\">&nbsp;" . pkg_enps_command_name->{$message_ref->[0]} . "</td>
			</tr>
			";
		}
		
		print " </table></td></tr>\n";
	}
}

print "</table>\n";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

