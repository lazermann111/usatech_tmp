#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use OOCGI::NTable;
use OOCGI::OOCGI 0.11;
use OOCGI::Query 0.10;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

use strict;
my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $app_user = USAT::DeviceAdmin::DBObj::App_user->new();

if($PARAM{userOP} eq 'Save') {
   $app_user = $app_user->insert($query);
   my $app_user_id = $app_user->ID;
   print CGI->redirect("/esuds_privileges.cgi?app_user_id=$app_user_id");
   exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print <<JSCRIPT;
<script LANGUAGE="JavaScript">
   function confirmPasswordChange()
   {
      var agree=confirm("Are you sure you want to reset password?");
      if (agree)
          return true ;
      else
          return false ;
    }
</script>
JSCRIPT

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

my $table_rn = 0;

print q|<form method="post">|;
$table->put($table_rn,0,'App User Edit Page','colspan=2 align=center class=header0');
$table_rn++;
$table->put($table_rn,0,'User Name', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_name', { sz_ml => [30,50]}));
$table_rn++;
$table->put($table_rn,0,'User First Name', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_fname', { sz_ml => [30,50]}));
$table_rn++;
$table->put($table_rn,0,'User Last Name', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_lname', { sz_ml => [30,50]}));
$table_rn++;
$table->put($table_rn,0,'User Email Addr', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_email_addr', { sz_ml => [30,50]}));
$table_rn++;
my $passwd_submit = q{<input type="submit" name="userOP" value="Change Password" onClick="return confirmPasswordChange()">};
$table->put($table_rn,0,'User Password', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_password', { sz_ml => [30,50]}));
$table_rn++;
$table->put($table_rn,0,'User Active', 'class=leftheader');
$table->put($table_rn,1,$app_user->Popup('app_user_active_yn_flag', { pair => ['Y', 'Yes', 'N', 'No'] }));
$table_rn++;
$table->put($table_rn,0,'Force Password Change', 'class=leftheader');
$table->put($table_rn,1,$app_user->Popup('force_pw_change_yn_flag', { pair => ['Y', 'Yes', 'N', 'No'] }));
$table_rn++;

my $submit = SUBMIT('userOP','Save');
my $back   = BUTTON('userOP', '< Back', { onclick => "javascript:history.go(-1);"});

$table->put($table_rn,0,$submit.' '.$back,'colspan=2 align=center');
display $table;
print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
