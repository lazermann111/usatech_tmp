#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $authority_server_id = $PARAM{"authority_server_id"};
my $authority_server_info_stmt = $dbh->prepare(
q{
	select authority_server.authority_server_id, authority_server.authority_server_name, authority_server.authority_server_addr, 
	authority_server.authority_server_port, authority_server.authority_server_priority, to_char(authority_server.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	to_char(authority_server.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'), authority_server.authority_gateway_id, authority_gateway.authority_gateway_name
	from authority_server, authority_gateway
	where authority_server.authority_gateway_id = authority_gateway.authority_gateway_id
	and authority_server.authority_server_id = :authority_server_id
});
$authority_server_info_stmt->bind_param(":authority_server_id", $authority_server_id);
$authority_server_info_stmt->execute();
my @authority_server_info = $authority_server_info_stmt->fetchrow_array();
$authority_server_info_stmt->finish();

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=1 width=\"100%\">

 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Authority Server</th>
 </tr>
 <tr>
 <tr>
  <td>ID</td><td>$authority_server_info[0]&nbsp;</td>
  <td>Name</td><td>$authority_server_info[1]&nbsp;</td>
 </tr>
 <tr>
  <td>Address</td><td>$authority_server_info[2]&nbsp;</td>
  <td>Port</td><td>$authority_server_info[3]&nbsp;</td>
 </tr>
 <tr>
  <td>Created</td><td>$authority_server_info[5]&nbsp;</td>
  <td>Last Updated</td><td>$authority_server_info[6]&nbsp;</td>
 </tr>
 <tr>
  <td>Authority Gateway</td><td><a href=\"authority_gateway.cgi?authority_gateway_id=$authority_server_info[7]\">$authority_server_info[8]</a>&nbsp;</td>
  <td colspan=\"2\">&nbsp;</td>
 </tr>
";

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
