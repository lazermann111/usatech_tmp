#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

USAT::DeviceAdmin::UI::DAHeader->printHeader();

USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=1 width=\"100%\">
 
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Location Admin</th>
 </tr>
 <tr>
  <form method=\"get\" action=\"location_list.cgi\">
  <td>Location Name: </td>
  <td colspan=\"2\"><input type=\"text\" name=\"location_name\"></td>
  <td><input type=\"submit\" value=\"Search\"></td>
  </form>
 </tr>
 <tr>
  <form method=\"get\" action=\"location_list.cgi\">
  <td>Location City: </td>
  <td colspan=\"2\"><input type=\"text\" name=\"location_city\"></td>
  <td><input type=\"submit\" value=\"Search\"></td>
  </form>
 </tr>
 <tr>
  <form method=\"get\" action=\"location_list.cgi\">
  <td colspan=\"1\">Location Type: </td>
  <td colspan=\"2\">
   <select name=\"location_type\">
";

my $location_types_stmt = $dbh->prepare("select location_type_id, location_type_desc from location_type order by location_type_desc") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$location_types_stmt->execute();
while (my @data = $location_types_stmt->fetchrow_array()) 
{
	print "   <option value=\"$data[0]\">$data[1]</option>\n";
}
$location_types_stmt->finish();

print "
   </select>
  </td>
  <td><input type=\"submit\" name=\"action\" value=\"Search\"></td>
  </form>
 </tr>
 <tr>
  <form method=\"get\" action=\"new_location.cgi\">
  <td>New Location: </td>
  <td colspan=\"2\">Name: <input type=\"text\" name=\"location_name\" maxlength=\"60\"></td>
  <td><input type=\"submit\" value=\"Next >\"></td>
  </form>
 </tr>

</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
