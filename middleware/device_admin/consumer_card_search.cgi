#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);

use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use USAT::DeviceAdmin::UI::DAHeader;

use strict;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my %PARAM = $query->Vars();

my $action = $PARAM{'action'};

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
$session->destroy;

my %B = ( C => '%', B => '',  E => '%' );
my %E = ( C => '%', B => '%', E => ''  );

my $popup = OOCGI::Popup->new();
$popup->tail('C','Contains');
$popup->tail('B','Begins');
$popup->tail('E','Ends');

print q|
<script type="text/javascript" src="/js/ajax.js"></script>
<style>
/* regular button, hover */
.btn {
   background-color:#FFFFFF;text-align:left; border:0;color:blue;width:100%;
}
/* regular button, hover */
.btn:hover{
     color:white;
     background-color:red;
}
</style>
|;

my @binds = ();
my $txt = "%%";
if($PARAM{consumer_acct_cd}) {
   $txt = $PARAM{consumer_acct_cd};
   my $be  = $PARAM{popup_acct_cd};
   $txt =  "$B{$be}$txt$E{$be}";
   push(@binds,$txt);
}


my $SQLB = q|
    SELECT ca.consumer_acct_id,
           ca.consumer_acct_cd,
           ca.consumer_acct_balance,
           l.location_id,
           l.location_name,
           caf.consumer_acct_fmt_name,
           ca.consumer_acct_active_yn_flag
    FROM consumer_acct ca,
         location.location l,
         pss.consumer_acct_fmt caf,
         consumer c
    WHERE c.consumer_id > 0
    AND c.consumer_id = ca.consumer_id
    AND ca.consumer_acct_fmt_id = caf.consumer_acct_fmt_id(+)
    AND ca.location_id = l.location_id
|;

if($PARAM{consumer_acct_cd}) 
{
	$SQLB .= q| AND ca.consumer_acct_cd like ? |;
}

my $style1 = 'class=leftheader';
my $style3 = 'style="{text-align:right;}"';
#----------------------------------------
if($PARAM{action} eq 'Search') {
   if($PARAM{merchant_id}) {
      $SQLB .= " and exists (
                select 1
                from consumer_acct, pss.merchant_consumer_acct
                where consumer_acct.consumer_acct_id = merchant_consumer_acct.consumer_acct_id
                and merchant_consumer_acct.merchant_id = ?
                and consumer_acct.consumer_id = c.consumer_id
                )";
      push(@binds,$PARAM{merchant_id} );
   }
   
}
#----------------------------------------
#print "BBBB @binds <br>";
#print "SQLB $SQLB \n";
my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
my $table_rn = 0;

my $style2 = 'style="{background-color:#FFFFFF}"';

print q|<form name=myForm method="post">|;

$table->put($table_rn,0,'Card Search Screen','class=header0 colspan=2');
$table_rn++;
$table->put($table_rn,0,'Card Number','class=leftheader nowrap');
$popup->name('popup_acct_cd');
if($PARAM{popup_acct_cd}) {
   $popup->default($PARAM{popup_acct_cd});
}
my $text = OOCGI::Text->new(id => 'id_consumer_acct_cd', name => 'consumer_acct_cd', size => 40);
$text->default($PARAM{consumer_acct_cd});
$table->put($table_rn,1, $popup.$text);
$table_rn++;

my $merchant_popup = OOCGI::Popup->new(
					name => 'merchant_id', 
					align => 'yes', 
					delimiter => '.',
					style => "font-family: courier; font-size: 12px;",
					sql => q|
						SELECT distinct m.merchant_id, m.merchant_name, m.merchant_cd
						  FROM merchant m, pss.merchant_consumer_acct mca, authority.authority a
						 WHERE m.merchant_id = mca.merchant_id
						   AND m.authority_id = a.authority_id
					     ORDER BY m.merchant_name|);
						
$merchant_popup->head('0','Undefined');
$merchant_popup->default($PARAM{merchant_id});

$table->put($table_rn,0,'Consumer Merchant','class=leftheader');
$table->put($table_rn,1, $merchant_popup);
$table_rn++;

display $table;

#if($PARAM{consumer_id}) {
#   print  qq|<br><input type="button" value="< Back" onClick="javascript:history.go(-1);">|;
#   print  qq| <input type="submit" name="action" value="Search" onClick="return confirmSubmit()"><br><br>|;
#} else {
   print  qq|<br><input type="submit" name="action" value="Search" onClick="return confirmSubmit()"><br><br>|;
#}

if($PARAM{action}) {
   my $pageNav  = OOCGI::PageNavigation->new(15);
   my @table_cols = (
        ['Card Code', -1],
        ['Balance',    2],
        ['Location',   3],
        ['Card Type',  4],
        ['Active',     5]
   );

   my @sql_cols = (
        'consumer_acct_cd',
        'consumer_acct_balance',
        'location_name',
        'consumer_acct_fmt_name',
        'consumer_acct_active_yn_flag'
   );

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $search_rn = 0;
my $search_cn = 0;
my $colspan  = $#table_cols + 1;
my $search   = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

while( my $column = $pageSort->next_column()) {
         $search->put($search_rn,$search_cn++,$column,'class=header1');
}
$search_rn++;

my $sql = $SQLB.$pageSort->sql_order_by_with(@sql_cols);
my $qo   = $pageNav->get_result_obj($sql, \@binds );

   while(my @data = $qo->next) {
      my $search_cn = 0;
      my $CAID = $data[0]; # consumer_accot_id
      $search->put($search_rn,$search_cn, $data[1],'style="{text-align:right;}"');
      $search->linkto($search_rn,$search_cn++,'edit_consumer_acct.cgi?consumer_acct_id='.$CAID);
      $search->put($search_rn,$search_cn++, $data[2]);
      $search->put($search_rn,$search_cn++, $data[4]);
#     $search->linkto($search_rn,$search_cn++,'edit_location.cgi?location_id='.$data[3]);
      $search->put($search_rn,$search_cn++, $data[5],'style="{text-align:right;}"');
      $search->put($search_rn,$search_cn++, $data[6],'style="{text-align:right;}"');

      $search_rn++;
   }
   display $search;

   my $page_table = $pageNav->table;
   display $page_table;
   $pageNav->enable();
   $pageSort->enable();
}

print q|<input type="hidden" name="consumer_id" value="">|;
print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
$query->DESTROY(1);
