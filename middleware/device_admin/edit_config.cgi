#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
require Text::CSV;
use USAT::App::DeviceAdmin::Const qw(:globals :globals_hash);
use USAT::Common::Const;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::Util;

my $qo  = OOCGI::Query->new;
my $dbh = $qo->dbh;

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;
my $file_name = $PARAM{"file_name"};
my $edit_mode = $PARAM{"edit_mode"};

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $map_name  = $PARAM{"map"};

if(!$map_name)
{
	print "Required parameter not found: map_name!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if(!$file_name)
{
	print "Required parameter not found: file_name!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $raw_map_data = USAT::DeviceAdmin::Util::load_file($map_name, $dbh);
if(!$raw_map_data)
{
	print "Failed to load $map_name!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $file_data = USAT::DeviceAdmin::Util::load_file($file_name, $dbh);
if(!$file_data)
{
        print "$file_name not found";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
}

my @map_lines = ();
if($raw_map_data =~ /\r\n/)
{
   @map_lines = split /\r\n/, $raw_map_data;
} else {
   @map_lines = split /\n/, $raw_map_data;
}

$file_data = pack("H*", $file_data);

my $ssn       = $PARAM{"ssn"};
my $ev_number = $PARAM{"ev_number"};

# lookup the device type so we know how to address memory natively
my $device_type;

if(length($ev_number) > 0)
{
	(undef, undef, $device_type) = @{USAT::DeviceAdmin::Util::get_device_info_by_name($dbh, $ev_number)};
}
elsif(length($ssn) > 0)
{
	(undef, undef, $device_type) = @{USAT::DeviceAdmin::Util::get_device_info_by_serial($dbh, $ssn)};
}
elsif($map_name =~ m/^g4_/)
{
	$device_type = PKG_DEVICE_TYPE__G5;
}
elsif($map_name =~ m/^mei_/)
{
	$device_type = PKG_DEVICE_TYPE__MEI_TELEMETER;
}

if(not defined $device_type)
{
	print "Unable to determine device type!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

print q{
<table cellpadding="3" border="1" cellspacing="0" width="100%">
 <tr>
  <th align="center" colspan="2" bgcolor="#C0C0C0">Edit Configuration - };
  
if(defined $ssn)
{
	print $ssn;
}
else
{
	print $file_name;
}

print q{
  </th>
 </tr>

<form method=post action="edit_config_func.cgi">};

HIDDEN("number_of_locations", scalar(@map_lines));
HIDDEN("file_name",           $file_name);
HIDDEN("ssn",                 $ssn);
HIDDEN("ev_number",           $ev_number);
HIDDEN("device_type",         $device_type);

my $location_counter = 1;
my $csv = Text::CSV->new;
my $eerom_location;
my $field_length;

foreach my $line (@map_lines)
{
	if ($csv->parse($line)) 
	{
		my ($name,$start_addr,$size,$align,$pad_with,$data_mode,$display,$description,$choices,$default_choice,$allow_change) = $csv->fields;
	
		my $location_data = substr($file_data,$start_addr,$size);
		
		if($device_type =~ m/^(0|1)/ || $file_name =~ m/^(G4|G5)/)
		{
			# G4/G5 uses a 2 byte per address scheme, ugh...
			$eerom_location = unpack("H*",chr($start_addr/2));
		
			if ($start_addr eq GX_MAP__AUTH_MODE)
			{
				# disable local auth
				$choices = 'N';
				$description .= ' <font color=red>Local Authorization has been disabled.</font>';
			}
			elsif ($start_addr eq GX_MAP__MDB_INVENTORY_FORMAT)
			{
				# disable BCD Inventory Format
				$choices = 'Y';
				$description .= ' <font color=red>BCD Inventory Format has been disabled.</font>';
			}
		}
		else
		{
			# assume others use a 1 to 1 byte addressing
			$eerom_location = unpack("H*",chr($start_addr));
		}
		
		$field_length = $size;
		
		if($data_mode eq 'H')
		{
			$location_data = unpack("H*", $location_data);
			$field_length = $size*2;
		}
		
		if($align eq 'C')
		{
			$location_data =~ s/$pad_with+$//;		# trim spaces on right
			$location_data =~ s/^$pad_with+//;		# trim spaces on left
		}
		elsif($align eq 'R')
		{
			$location_data =~ s/^$pad_with+//;		# trim spaces on left
		}
		elsif($align eq 'L')
		{
			$location_data =~ s/$pad_with+$//;		# trim spaces on right
		}
		
		my $end_byte = $start_addr+$size-1;
		
		print "<!-- BEGIN $location_counter - Address $start_addr: $name -->\n";
		
		HIDDEN($start_addr."_counter",        $location_counter);
		HIDDEN($start_addr."_size",           $size);
		HIDDEN($start_addr."_align",          $align);
		HIDDEN($start_addr."_pad_with",       $pad_with);
		HIDDEN($start_addr."_data_mode",      $data_mode);
		HIDDEN($start_addr."_eerom_location", $eerom_location);
		HIDDEN($start_addr."_name",           $name);

		if($display eq 'N' && $edit_mode ne 'A')
		{
			HIDDEN($start_addr, $location_data);
		    HIDDEN($start_addr."_original",       $location_data);
		}
		else
		{
			print " 
			 <tr>
			  <td align=\"left\"><b>$name</b></td>
			";
			
			if(($data_mode eq "A") || ($data_mode eq "H"))
			{
		        HIDDEN($start_addr."_original",       $location_data);
                my $text = OOCGI::Text->new(name => $start_addr, value => $location_data,
                                            sz_ml => [$field_length, $field_length]);
 			print q{<td align="left" nowrap>},$text->string,qq{</td>\n};
			}
			elsif ($data_mode eq "P")
			{
				my @choices = split(/\|/, $choices);
                my $pair_str = '';
                for(my $i = 0; $i <= $#choices; $i++) {
                   my @array = split(/,/,$choices[$i]);
                   $pair_str .= $array[0].' ';
                } 
                my @pair = split(/,/,join(",", @choices));
                my $popup = OOCGI::Popup->new(name => $start_addr, pair => \@pair);
                my $number = unpack('H2',$location_data);
                if($number eq '00' || $number == 0 || $number eq '') {
				   print qq{  <td align="left" nowrap>\n};
                   $popup->default('00');
                   print $popup->string;
				   print "  </td>\n";
                } elsif($pair_str =~ /$number/) {
				   print qq{  <td align="left" nowrap>\n};
                   $popup->default($number);
                   print $popup->string;
				   print "  </td>\n";
                } else {
				   print qq{  <td align="left" nowrap bgcolor="red">\n};
                   $popup->tail($number, $number);
                   $popup->default($number);
                   print $popup->string;
				   print "  </td>\n";
                }
		        HIDDEN($start_addr."_original",       $number);
			}
			elsif ($data_mode eq "C")
			{
		        HIDDEN($start_addr."_original",       $location_data);
				print qq{  <td align="left" nowrap>\n};
				my @choices = split(/\|/, $choices);
				my $choices_size = scalar(@choices);
				foreach my $choice (@choices)
				{
                    my $radio;
                    if($location_data eq $choice || $choices_size == 1) {
                       $radio = OOCGI::Radio->new(name => $start_addr, value => $choice, label => $choice, checked => 'yes');
                    } else {
                       $radio = OOCGI::Radio->new(name => $start_addr, value => $choice, label => $choice );
                    }
                    print $radio->string;
				}
				print "  </td>\n";
			} else {
		        HIDDEN($start_addr."_original",       $location_data);
            }
		
			print qq{
			 </tr>
			  <tr>
			  <td align="left" valign="top" colspan="2" width="100%" bgcolor="#EEEEEE">
			   <font size="-1">$description <br>
			   ($location_counter/$data_mode/$size/$eerom_location/$start_addr - $end_byte/$align/$pad_with)</font>
			  </td>
			 </tr>\n};
		}
		
		print "<!-- END $location_counter - Address $start_addr: $name -->\n\n";
		$location_counter++;
	}
	else
	{
		print "Failed to parse: " . $csv->error_input();
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
}

print q{
 <tr>
  <td align="center" colspan="5">
   <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td align="center">
     };

if($device_type eq '6')
{
	print q{
	<table cellspacing="0" cellpadding="5" border="1">
	 <tr>
	  <td valign="top" align="center">
	   <input type="submit" value="Save and Send" name="action">
	   <input type="hidden" name="send" value="complete">
	  </td>
	  <td align="center" valign="top" rowspan="2">
	   <input type="submit" value="Save Only" name="action">
	  </td>
	  <td valign="top" align="center" rowspan="2">
	   <input type="button" value="Cancel" onClick="javascript:history.go(-1);">
	  </td>
	 </tr>
	</table>};
}
elsif(defined $ssn || defined $ev_number) # this is a device  config
{
	print q{
	<table cellspacing="0" cellpadding="5" border="1">
	 <tr>
	  <td valign="top" align="center">
	   <input type="submit" value="Save and Send" name="action">
	  </td>
	  <td align="center" valign="top" rowspan="2">
	   <input type="submit" value="Save Only" name="action">
	  </td>
	  <td valign="top" align="center" rowspan="2">
	   <input type="button" value="Cancel" onClick="javascript:history.go(-1);">
	  </td>
	 </tr>
	 <tr>
	  <td>
	   <input type="radio" name="send" value="changes" checked>Changes
	   <input type="radio" name="send" value="complete">Complete<br>
	   <input type="checkbox" name="comms" value="Y">Communication Settings<br>
	   <input type="checkbox" name="counters" value="Y">Zero Out Counters<br>
	  </td>
	 </tr>
	</table>};
}
else	# this is a template
{
    SUBMIT("action", "Save");
    SUBMIT("action", "Save and Copy To");
    print q{<input type="text" name="new_name">};
    SUBMIT("action", "Delete Template");
    print q{<input type=button value="Cancel" onClick="javascript:window.location = '/template_menu.cgi';">};
}

print q{
     </td>
    </tr>
   </table>
       
 <tr>
  <th align="center" colspan="2" bgcolor="#C0C0C0">Edit Configuration - };
  
if(defined $ssn)
{
	print $ssn;
}
else
{
	print $file_name;
}

print "
  </th>
 </tr>

  </td>
 </form>
 </tr>
</table>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

