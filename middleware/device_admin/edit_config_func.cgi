#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
require Text::CSV;
use USAT::DeviceAdmin::Util;
use POSIX qw(ceil);
use USAT::DeviceAdmin::UI::DAHeader;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

my $ev_number           = $PARAM{"ev_number"};
my $file_name           = $PARAM{"file_name"};
my $number_of_locations = $PARAM{"number_of_locations"};
my $new_name            = $PARAM{"new_name"};
my $device_type         = $PARAM{"device_type"};

my $send_comms          = $PARAM{"comms"};
my $send_counters       = $PARAM{"counters"};

my $action              = $PARAM{"action"};
my $send_action         = $PARAM{"send"};

if(length($ev_number) == 0)
{
	$ev_number = undef;
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
$session->destroy;

if((length($new_name) == 0) && ($action eq 'Save and Copy To'))
{
	print "Required Parameter Not Found: new_name";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit();
}

my $new_location_file = "";
my $execute_order_counter = 0;
my $change_count = 0;
my $mem_counter = 0;

print "
<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
 <tr>
  <td align=\"left\">
   <br>
   <center>
   <form>";
   
if(defined $ev_number)
{
	print "<input type=button value=\"Return to Device Profile\" onClick=\"javascript:window.location = '/profile.cgi?ev_number=$ev_number';\">";
}
else
{
	print "<input type=button value=\"Return to Template Menu\" onClick=\"javascript:window.location = '/template_menu.cgi';\">";
}

print "
   </form>
   </center>
   <pre>
";

if(defined $ev_number)
{
	print "Processing a device configuration...\n\n";
	
	my $get_max_stmt = $dbh->prepare("select max(execute_order) from engine.machine_cmd_pending where machine_id = :ev_number") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_max_stmt->bind_param(":ev_number", "$ev_number");
	$get_max_stmt->execute();
	my @max_data = $get_max_stmt->fetchrow_array();
	$execute_order_counter = $max_data[0];
	$get_max_stmt->finish();
	
	if(not defined $execute_order_counter)
	{
		print "Using 0 for execute_order_counter\n\n";
		$execute_order_counter = 0;
	}
	else
	{
		$execute_order_counter++;
		print "Using $execute_order_counter for execute_order_counter\n\n";
	}
}
else
{
	print "Processing a configuration template...\n\n";
}

for(my $location_counter = 1; $location_counter <= $number_of_locations; $location_counter++)
{
	my $start_addr = $mem_counter;
	
	my $data           = $PARAM{$start_addr};
	my $orig_counter   = $PARAM{$start_addr."_counter"};
	my $size           = $PARAM{$start_addr."_size"};
	my $orig           = $PARAM{$start_addr."_original"};
	my $align          = $PARAM{$start_addr."_align"};
	my $pad_with       = $PARAM{$start_addr."_pad_with"};
	my $data_mode      = $PARAM{$start_addr."_data_mode"};
	my $eerom_location = $PARAM{$start_addr."_eerom_location"};
	my $name           = $PARAM{$start_addr."_name"};

	print "$location_counter) $orig_counter $start_addr $size $align $pad_with $data_mode ---------------------------- [ $name ]\n";
	print "IN: \"$data\" len=".length($data)."\n";
	
	if(length($orig_counter) == 0)
	{
		print "<font color=\"red\"><b>Fatal Error: Memory Location Inconsistency!</b></font>
		<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print this screen and give it to an administrator.\");</script>\n";
	    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
	
	my $zerod = 0;
	if(($device_type =~ m/^(0|1)/) && ($send_counters eq 'Y') && ($start_addr >= 320 && $start_addr < 356))
	{
		$data = '00000000';
		print "Zero'ing out counter $name\n";
		$zerod = 1;
	}
	
	if($data ne $orig)
	{
		print "<font color=\"red\"><b>CHANGED!</b></font> \"$data\"  <>  \"$orig\"\n";

		$change_count++;

		if($zerod)
		{
			print "Skipping Poke for individial zero'd counter\n";
		}
		else
		{
			if($action eq 'Save and Send' && $send_action eq 'changes')
			{
				# insert a Poke for this memory location
	
				my ($poke_loc, $poke_size);
				if($device_type =~ m/^(0|1)/)
				{
					# G4/G5 uses a 2 byte per address scheme, ugh...
					# $eerom_location should already be adjusted
					
					$poke_loc = ord(pack("H*", $eerom_location));
					$poke_size = $size;
					if($poke_size == 1)
					{
						# you must send at least 2 bytes
						$poke_size = 2;
					}
				}
				else
				{
					# this can happen for MEI
					$poke_loc = ord(pack("H*", $eerom_location));
					$poke_size = $size;
				}
				
				print (&create_poke($poke_loc, $poke_size));
			}
		}
	}

	if($data_mode eq 'P') {
	   $data_mode = 'H';
    }

	if(($data_mode eq 'H' && length($data) < ($size*2)) || ($data_mode ne 'H' && length($data) < $size))
	{
		$data = &pad($data,$pad_with,$size,$align,$data_mode);
		print "Padded: \"$data\" len=".length($data)."\n";
	}
	else
	{
		print "NO PADDING NEEDED\n";
	}
	
	if ($data_mode eq 'H')
	{
		$data = pack("H*", $data);
	}

	print "OUT: \"" . unpack("H*",$data) . "\" len=".length($data)."\n\n";
	
	$new_location_file .= $data;
	$mem_counter = $mem_counter + $size;
}

if($change_count > 0)
{
	print "<font color=\"red\"><b>\n----------------------------------- $change_count User Changes! -----------------------------------</b></font>\n";

	print "\n\n<b>Saving file $file_name</b>\n\n";
	
	print "Raw Data:<br>\n";
	print "<textarea rows=\"10\" cols=\"100\" wrap=\"on\">$new_location_file</textarea>\n\n";

	print "HEX Data:<br>\n";
	print "<textarea rows=\"10\" cols=\"100\" wrap=\"on\">" . uc((unpack("H*", $new_location_file)) . "</textarea>\n\n");
	
	if(USAT::DeviceAdmin::Util::save_file($file_name, uc(unpack("H*", $new_location_file)), $dbh))
	{
		print "Save Success: $file_name\n";
	}
	else
	{
		print "Save Failed!: $file_name\n";
	}
}
else
{
	print "\n\nNo User Changes!\n\n";
}

if($action eq 'Save and Send' && $send_action eq 'complete')
{
	print "<b>Creating Pokes to Send Complete...</b>\n\n";
	my $poke_data = $new_location_file;

	if($device_type =~ m/^(0|1)/)
	{
		print "\nThis is an G4/G4 device!  Sending portions of complete config...\n";
		
		# we are sending very specific sections of the config, not the whole thing for G4/G5
		#	0 	- 135	/ 2 = 0		length = 136
		#	142 - 283	/ 2 = 71	length = 142
		#	356 - 511	/ 2 = 178	length = 156

		print (&create_poke(0, 136));
		print (&create_poke(71, 142));
		print (&create_poke(178, 156));
	}
	else
	{
		my $poke_size = 200;

		# figure out how many Pokes it will take to transmit the entire file
		my $total_bytes = length($poke_data);
		my $num_parts = ceil($total_bytes/$poke_size);

		print "Poke Size         : $poke_size\n";
		print "Total Bytes       : $total_bytes\n";
		print "Num Parts         : $num_parts\n\n";

		# create each Poke and put it in the machine_cmd_pending table

		for(my $counter=0; $counter<$num_parts; $counter++)
		{
			my $pos = ($counter*$poke_size);
			my $chunk;
			if(($pos+$poke_size) > $total_bytes)
			{
				# this should be the last Poke
				$chunk = substr($poke_data, $pos);
			}
			else
			{
				$chunk = substr($poke_data, $pos, $poke_size);
			}

			#my $memory_location = ($pos/2);
			my $memory_location = $pos;
			print "Poke Number       : $counter\n";
			print "File Location     : $pos\n";
			print "Memory Location   : $memory_location\n";
			print "Size              : " . length($chunk) . "\n";
			
			print (&create_poke($memory_location, length($chunk)));
		}
	}
}
elsif($action eq 'Save and Copy To')
{
	USAT::DeviceAdmin::Util::create_file($new_name, 6, uc(unpack("H*", $new_location_file)), $dbh) or print "File Create Error!\n\n";
	print "Saved $new_name\n\n";
}

if($send_comms eq 'Y')
{
	# queue up 2 pokes for comm settings
	# comms are:
	#	136	- 141	/ 2 = 68	length = 6
	#	284 - 289	/ 2 = 142	length = 6

	print "\nSending Communication Parameters Pokes...\n";
	print (&create_poke(68, 6));
	print (&create_poke(142, 6));
}

if($send_counters eq 'Y')
{
	# queue up 1 poke to overwrite counters
	# counters are:
	#	320	- 355	/ 2 = 160	length = 36

	print "\nSending Counters Poke...\n";
	print (&create_poke(160, 36));
}

$dbh->disconnect;

print "
   </pre>
  </td>
 </tr>
 <tr>
  <td align=\"left\">
   <center>
   <form>
   ";

if(defined $ev_number)
{
	print "<input type=button value=\"Return to Device Profile\" onClick=\"javascript:window.location = '/profile.cgi?ev_number=$ev_number';\">";
}
else
{
	print "<input type=button value=\"Return to Template Menu\" onClick=\"javascript:window.location = '/template_menu.cgi';\">";
}

print "
   </form>
   </center>
  </td>
 </tr>  
</table>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub pad
{
	my ( $string_out, $left_or_right, $pad_buffer);
	my ($string_in,$pad_with,$pad_size,$align,$data_mode) = @_;
	
	if($data_mode eq 'H')
	{
		$pad_size = ($pad_size * 2);
	}
	
	if($align eq 'L')
	{
		$left_or_right = 'R';
	}
	elsif($align eq 'R')
	{
		$left_or_right = 'L';
	}
	elsif($align eq 'C')
	{
		$left_or_right = 'C';
	}		
	
	print "Padding on $left_or_right with \"$pad_with\" to $pad_size\n";
	
	$pad_buffer = "";
	
	if (length($string_in) < $pad_size)
	{
		for (my $pad_counter = 0;$pad_counter < $pad_size-length($string_in);$pad_counter += length($pad_with))
		{
			$pad_buffer .= $pad_with;
		}
	}
	
	if ($left_or_right eq "L")
	{
		$string_out = $pad_buffer.$string_in;
	}
	elsif ($left_or_right eq "R")
	{
		$string_out = $string_in.$pad_buffer;
	}
	elsif ($left_or_right eq "C")
	{
		$string_out = &pad_center($string_in, $pad_size, $pad_with);
	}
	
	return $string_out;
}

sub pad_center
{
	my ($str, $len, $pad) = @_;

	my $counter = 1;
	while(length($str) < $len)
	{
		if(($counter % 2) == 0)
		{
			$str = $str . $pad;
		}
		else
		{
			$str = $pad . $str;
		}
		
		$counter++;
	}
	
	return $str;
}

sub create_poke
{
	my ($addr, $length) = @_;
	
	my $poke = pack('cNN', ord("B"), $addr, $length);
	
	# check if this poke already exists
	my $check_poke_exists_stmt = $dbh->prepare("select count(1) from engine.machine_cmd_pending where machine_id = :machine_id and data_type = :data_type and command = :command and execute_cd in ('P', 'S')");
	$check_poke_exists_stmt->bind_param(":machine_id", $ev_number);
	$check_poke_exists_stmt->bind_param(":data_type", "88");
	$check_poke_exists_stmt->bind_param(":command", unpack("H*", $poke));
	$check_poke_exists_stmt->execute();
	my @poke_exists_data = $check_poke_exists_stmt->fetchrow_array();
	my $poke_exists_count = $poke_exists_data[0];
	$check_poke_exists_stmt->finish();
	
	if($poke_exists_count)
	{
		return "Poke Already Exists: $ev_number 88 " . unpack("H*", $poke) . " P $execute_order_counter\n";
	}
	else
	{
		# don't put actual poke data in the pending table, just the poke info.  We'll construct
		# the poke on the fly when the command is sent - this is done in case there are changes made to the
		# config file between the time we create the pokes when they are sent

		my $insert_poke_stmt = $dbh->prepare("insert into engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order) values (:machine_id, :data_type, :command, :execute_cd, :execute_order)");
		$insert_poke_stmt->bind_param(":machine_id", $ev_number);
		$insert_poke_stmt->bind_param(":data_type", "88");
		$insert_poke_stmt->bind_param(":command", unpack("H*", $poke));
		$insert_poke_stmt->bind_param(":execute_cd", "P");
		$insert_poke_stmt->bind_param(":execute_order", $execute_order_counter++);
		$insert_poke_stmt->execute();
		$insert_poke_stmt->finish();
		
		return "Poke: $ev_number 88 " . unpack("H*", $poke) . " P $execute_order_counter\n";
	}
}
