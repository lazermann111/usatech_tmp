#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use OOCGI::NTable;
use OOCGI::OOCGI 0.11;
use OOCGI::Query 0.10;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

use strict;
my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

use constant SEND_PASSWORD   => 'Notify';
use constant CHANGE_PASSWORD => 'Change And Notify';
use constant EMAIL_SENDER    => 'device-admin@usatech.com';
use constant SMTP_SERVER     => 'mailhost';


my $app_user_id = $PARAM{app_user_id};
my $app_user;
if(length($app_user_id) > 0 && $app_user_id >0 )
{
   $app_user = USAT::DeviceAdmin::DBObj::App_user->new($app_user_id);

   if($PARAM{userOP} eq 'Save') {
      $app_user = $app_user->update($query);
      print CGI->redirect("/esuds_privileges.cgi?app_user_id=$app_user_id");
      exit();
   }
}

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

if($PARAM{userOP} eq SEND_PASSWORD) {
   my $email_to          = $app_user->app_user_email_addr;
   my $app_user_password = $app_user->app_user_password;
   my $app_user_fname    = ucfirst $app_user->app_user_fname;
   my $subject  = "Your Requested Password";
   my $content  = << "   MAIL";
   Dear $app_user_fname,

      Your requested password is $app_user_password

   Regards,

   customerservice\@usatech.com
   MAIL
   USAT::DeviceAdmin::Util->send_email(EMAIL_SENDER,$email_to,$subject,$content);
}

if($PARAM{userOP} eq CHANGE_PASSWORD) {
   my $email_to          = $app_user->app_user_email_addr;
   my $app_user_password = $app_user->app_user_password;
   my $app_user_fname    = ucfirst $app_user->app_user_fname;
   my $subject  = "Your New Password";
   my $new_password = USAT::DeviceAdmin::Util->random_password(8);
   $app_user->app_user_password($new_password);
   $app_user = $app_user->update;

   my $content  = << "   MAIL";
   Dear $app_user_fname,

      Your requested new password is $new_password

   Regards,

   customerservice\@usatech.com
   MAIL
   if($new_password eq $app_user->app_user_password) {
      USAT::DeviceAdmin::Util->send_email(EMAIL_SENDER,$email_to,$subject,$content);
   } else {
      print B("Password Creation Problem, No email send to user",'red',3);
   }
}

print <<JSCRIPT;
<script LANGUAGE="JavaScript">
   function confirmPasswordChange()
   {
      var agree=confirm("Are you sure you want to reset password?");
      if (agree)
          return true ;
      else
          return false ;
    }
</script>
JSCRIPT

if(length($app_user_id) == 0 && !$PARAM{app_user_name})
{
    print "Required Parameter Not Found: app_user_id";
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
    exit;
}


my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

my $table_rn = 0;

print q|<form method="post">|;
$table->put($table_rn,0,'App User Edit Page','colspan=2 align=center class=header0');
$table_rn++;
$table->put($table_rn,0,'User Name', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_name', { sz_ml => [30,50]}));
$table_rn++;
$table->put($table_rn,0,'User First Name', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_fname', { sz_ml => [30,50]}));
$table_rn++;
$table->put($table_rn,0,'User Last Name', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_lname', { sz_ml => [30,50]}));
$table_rn++;
$table->put($table_rn,0,'User Email Addr', 'class=leftheader');
$table->put($table_rn,1,$app_user->Text('app_user_email_addr', { sz_ml => [30,50]}));
$table_rn++;
my $passwd_change = q{<input type="submit" name="userOP" value='}.CHANGE_PASSWORD.q{' onClick="return confirmPasswordChange()">};
my $passwd_send   = q{<input type="submit" name="userOP" value='}.SEND_PASSWORD.q{'">};
$table->put($table_rn,0,'User Password', 'class=leftheader');
$table->put($table_rn,1,'****************'.$passwd_change.NBSP(3).$passwd_send);
$table_rn++;
$table->put($table_rn,0,'User Active', 'class=leftheader');
$table->put($table_rn,1,$app_user->Popup('app_user_active_yn_flag', { pair => ['Y', 'Yes', 'N', 'No'] }));
$table_rn++;
$table->put($table_rn,0,'Force Password Change', 'class=leftheader');
$table->put($table_rn,1,$app_user->Popup('force_pw_change_yn_flag', { pair => ['Y', 'Yes', 'N', 'No'] }));
$table_rn++;
my $submit = SUBMIT('userOP','Save');
my $back   = BUTTON('userOP', '< Back', { onclick => "javascript:history.go(-1);"});
$table->put($table_rn,0,$submit.' '.$back,'colspan=2 align=center');
display $table;
HIDDEN('app_user_id',$app_user_id);
print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
