#!/usr/local/USAT/bin/perl

use strict;

use CGI qw(-debug);
use OOCGI::OOCGI;
#use CGI::Carp qw(fatalsToBrowser);

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

$query->printHeader();

#Carp::carp "AAA LLL authority_id = $PARAM{authority_id} \n";

my $sql;
my $popMerchant;
my $string = '';
if(defined $PARAM{authority_id} && $PARAM{authority_id} ne '') {
   $sql = q{
      SELECT merchant.merchant_id, merchant.merchant_name, merchant.merchant_cd
      FROM terminal, merchant, authority.authority, terminal_state, authority_type
     WHERE authority.authority_id      = ?
       AND terminal.merchant_id        = merchant.merchant_id
       AND merchant.authority_id       = authority.authority_id
       AND terminal.terminal_state_id  = terminal_state.terminal_state_id
       AND authority.authority_type_id = authority_type.authority_type_id
       ORDER BY merchant_name
   };
   $popMerchant = OOCGI::Popup->new(
       name  => 'merchant_id',
       sql   => $sql,
       bind  => [ $PARAM{authority_id} ],
       style => "font-family: courier; font-size: 12px;",
       align => 'yes', delimiter => '.' 
   );
   $popMerchant = OOCGI::Popup->new(
       name  => 'merchant_id',
       sql   => $sql,
       bind  => [ $PARAM{authority_id} ],
       style => "font-family: courier; font-size: 12px;",
       align => 'yes', delimiter => '.'
   );
   if($popMerchant->item_count > 0 ) {
      $popMerchant->head('');
      $popMerchant->default('');
      $string = $popMerchant->string;
   } else {
      $string = '<span style="text-decoration:blink; font-weight:bold; color:#ff0000">NO DATA</span>';
   }
} else {
   $sql = q{
      SELECT merchant_id, merchant_name, merchant_cd
        FROM merchant
       ORDER BY merchant_name
   };
   $popMerchant = OOCGI::Popup->new(
       name  => 'merchant_id',
       sql   => $sql,
       style => "font-family: courier; font-size: 12px;",
       align => 'yes', delimiter => '.' 
   );
   $popMerchant->head('');
   $popMerchant->default('');
   $string = $popMerchant->string;
}


print $string;
