#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::App::DeviceAdmin::Const qw(:globals );
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader;
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html"); 

print $user_menu;

print qq|
<script>
function addLoadEvent(func) { 
  var oldonload = window.onload; 
  if (typeof window.onload != 'function') { 
    window.onload = func; 
  } else { 
    window.onload = function() { 
      oldonload(); 
      func(); 
    } 
  } 
} 
 
addLoadEvent(run_refresh); 

function run_refresh() {
   var url1 = "axah_refresh_js_cache.cgi?dummy=yes";
   getAXAHUpdateDiv(url1,'message_div');
}
</script>
<br>
<br><br>
<div id='message_div'>
Starting the process for javascript refresh ...
</div>
<br><br><br>
|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
