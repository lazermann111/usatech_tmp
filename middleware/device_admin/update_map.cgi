#!/usr/local/USAT/bin/perl
#################################################################
#
# edit_map.cgi
#
#################################################################
#
# Written By: Paul Cowan
# Created:  10/09/2003
# (c)2003 USA Technologies, Inc.
#
#################################################################
# Required Libraries
#################################################################

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;
use Fcntl;
require Text::CSV;

#################################################################

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();

print "<html><head><title>Memory Map Editor - Processing</title></head>\n";
print "<body><pre>\n";

my $map_name = $PARAM{"map_name"};
my $action   = $PARAM{"action"};
my $number_of_locations = $PARAM{"number_of_location"};

my $new_map = "";
my $mem_counter = 0;

for (my $location_counter = 1; $location_counter <= $number_of_locations; $location_counter++)
{
	my $name        = $PARAM{$location_counter."_name"};
	my $address     = $PARAM{$location_counter."_address"};
	my $size        = $PARAM{$location_counter."_size"};
	my $align       = $PARAM{$location_counter."_align"};
	my $pad_with    = $PARAM{$location_counter."_pad_with"};
	my $data_mode   = $PARAM{$location_counter."_data_mode"};
	my $description = $PARAM{$location_counter."_description"};
	
	print "Location Counter     : $location_counter\n";
	print "Memory Counter       : $mem_counter\n";
	print "Name                 : $name\n";
	print "Address              : $address\n";
	print "Size                 : $size\n";
	print "Align                : $align\n";
	print "Pad With             : $pad_with\n";
	print "Type                 : $data_mode\n";
	print "Description          : $description\n";
	
	chomp($location_counter);
	chomp($mem_counter);
	chomp($name);
	chomp($address);
	chomp($size);
	chomp($align);
	chomp($data_mode);
	chomp($description);
	
	if($mem_counter != $address)
	{
		print "<font color=red><b>Memory address counter does not match input memory address!\n";
		print "Please check your Mem Addr and Size parameters for the field: $name\n</b></font>";
		exit;
	}
	
	if($data_mode eq 'Y' && $size ne '1')
	{
		print "<font color=red><b>A Y/N field size can not be large that 1!\n";
		print "Please check your Size and Type parameters for the field: $name\n</b></font>";
		exit;
	}

	my @fields;
	push (@fields, $name);
	push (@fields, $address);
	push (@fields, $size);
	push (@fields, $align);
	push (@fields, $pad_with);
	push (@fields, $data_mode);
	push (@fields, $description);
	
	my $csv = Text::CSV->new;
	
	if ($csv->combine(@fields)) 
	{
		my $new_line = $csv->string();
		print "<b>Output               : $new_line</b>\n\n";
		$new_map = $new_map . $new_line . "\n";
	}
	else
	{
		print "Failed to combine: "  . $csv->error_input() . "\n\n";
		exit;
	}
	
	$mem_counter += $size;
}

open (FILE,">$map_name") or print "<font color=red><b>Failed to open $map_name for writing!</b></font>\n";
flock(FILE, 2);
print FILE $new_map;
flock(FILE, 8);
close(FILE);

print "<b>Wrote $map_name!\n</b>";

print "</pre></body></html>";
