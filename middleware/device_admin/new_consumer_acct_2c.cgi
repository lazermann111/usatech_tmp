#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use Data::Dumper;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $err_str;

$err_str .= B('Required parameter not found: action_id<br>', 'red') unless(length($params{'action_id'}) > 0);

if($err_str)
{
	print $err_str;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $action_id = $params{'action_id'};

my $action_sql = q|
	select action.action_id, action.action_name, action.action_desc
	from device.action
	where action.action_id = ?|;

my $action_param_sql = q|
	select action_param.action_param_id, action_param.action_param_cd, action_param.action_param_name, action_param.action_param_desc
	from device.action_param
	where action_param.action_id = ?
	order by action_param.action_param_cd|;

print q|<form method="post" action="new_consumer_acct_3.cgi">|;

my $rc = 0;	
my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
$table->put($rc++,0, B('USAT Card Creation Wizard'),'align=center bgcolor=#C0C0C0 colspan=2');

my $action_query = OOCGI::Query->new($action_sql, undef, [$action_id]);
my %action_hash = $action_query->next_hash;
		
$table->put($rc++,0,B("Select optional parameters for action: $action_hash{action_name}"), 'colspan=2 align=center');
		
my $action_param_query = OOCGI::Query->new($action_param_sql, undef, [$action_id]);
if ($action_param_query->row_count > 0)
{
	while(my %hash = $action_param_query->next_hash) 
	{
		$table->put($rc,0, qq|<input type="checkbox" name="action_params" value="$hash{action_param_id}">|);
		$table->put($rc++,1,"$hash{action_param_name}");
	}
}
else
{
	$table->put($rc++,0,"No optional parameters for this action", 'colspan=2');
}

display $table;

HIDDEN_PASS('consumer_acct_fmt_id', 'customer_id', 'location_id', 'authority_id', 'merchant_id', 'merchant_name', 'merchant_desc', 'device_type_id', 'action_id');

print "&nbsp<br>" . SUBMIT('action','Next >') . "</form><br>&nbsp;";
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
