#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

my $location_id = $PARAM{"location_id"};
if(length($location_id) == 0)
{
	print "Required Parameter Not Found: location_id";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $name;
my $addr1;
my $addr2;
my $city;
my $county;
my $postal_cd;
my $parent_location_id;
my $country;
my $location_type_id;
my $state;
my $time_zone;

my $get_location_stmt = $dbh->prepare("select location_id, location_name, location_addr1, location_addr2, location_city, location_county, location_postal_cd, parent_location_id, location_country_cd, location_type_id, location_state_cd, location_time_zone_cd from location.location where location_id = :location_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_location_stmt->bind_param(":location_id", $location_id);
$get_location_stmt->execute();

my @location_data = $get_location_stmt->fetchrow_array();
if(!@location_data)
{
	print "Location $location_id not found!\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

$name = $location_data[1];
$addr1 = $location_data[2];
$addr2 = $location_data[3];
$city = $location_data[4];
$county = $location_data[5];
$postal_cd = $location_data[6];
$parent_location_id = $location_data[7];
$country = $location_data[8];
$location_type_id = $location_data[9];
$state = $location_data[10];
$time_zone = $location_data[11];

$get_location_stmt->finish();

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Edit Location - $name</th>
 </tr>
 <form method=\"post\" action=\"edit_location_func.cgi\">
 <input type=\"hidden\" name=\"location_id\" value=\"$location_id\">
 <tr>
  <td nowrap>Name</td>
  <td width=\"50%\"><input type=\"text\" name=\"name\" value=\"$name\" size=\"40\"></td>
  <td nowrap>Type</td>
  <td width=\"50%\">
   <select name=\"location_type_id\">
";
    
my $location_types_stmt = $dbh->prepare("select location_type_id, location_type_desc from location_type order by location_type_desc") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$location_types_stmt->execute();
while (my @data = $location_types_stmt->fetchrow_array()) 
{
	print "   <option value=\"$data[0]\" " . ($data[0] eq $location_type_id ? 'selected' : '&nbsp;') . ">$data[1]</option>\n";
}
$location_types_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>Address Line 1</td>
  <td width=\"50%\"><input type=\"text\" name=\"addr1\" value=\"$addr1\" size=\"40\" maxlength=\"60\"></td>
  <td nowrap>City</td>
  <td width=\"50%\"><input type=\"text\" name=\"city\" value=\"$city\" size=\"40\" maxlength=\"28\"></td>
 </tr>
 <tr>
  <td nowrap>Address Line 2</td>
  <td width=\"50%\"><input type=\"text\" name=\"addr2\" value=\"$addr2\" size=\"40\" maxlength=\"60\"></td>
  <td nowrap>State</td>
  <td width=\"50%\">
   <select name=\"state\">
    <option value=\"\">Undefined</option>
";
    
my $states_stmt = $dbh->prepare("select state_cd, state_name from state order by state_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$states_stmt->execute();
while (my @data = $states_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" " . ($data[0] eq $state ? 'selected' : '&nbsp;') . ">$data[1]</option>\n";
}
$states_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>County</td>
  <td width=\"50%\"><input type=\"text\" name=\"county\" value=\"$county\" size=\"40\" maxlength=\"28\"></td>
  <td nowrap>Postal Code</td>
  <td width=\"50%\"><input type=\"text\" name=\"postal_cd\" value=\"$postal_cd\" size=\"40\" maxlength=\"10\"></td>
 </tr>
 <tr>
  <td nowrap>Country</td>
  <td width=\"50%\">
   <select name=\"country\">
";
    
my $country_list_stmt = $dbh->prepare("select country_cd, country_name from country order by country_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$country_list_stmt->execute();
while (my @data = $country_list_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" " . ($data[0] eq $country ? 'selected' : '&nbsp;') . ">$data[1]</option>\n";
}
$country_list_stmt->finish();

print "
   </select>
  </td>
  <td nowrap>Time Zone</td>
  <td width=\"50%\"><select name=\"time_zone\">
";
    
my $time_zone_stmt = $dbh->prepare("select time_zone_cd, time_zone_name from time_zone order by time_zone_cd") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$time_zone_stmt->execute();
while (my @data = $time_zone_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" " . ($data[0] eq $time_zone ? 'selected' : '&nbsp;') . ">$data[0] - $data[1]</option>\n";
}
$time_zone_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>Parent Location</td>
  <td width=\"50%\" colspan=\"3\">
   <select name=\"parent_location_id\">
   <option value=\"\">None</option>";
    
my $parents_stmt = $dbh->prepare("select location_id, location_name, location_city, location_state_cd, location_country_cd from location.location where location_active_yn_flag = 'Y' order by location_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$parents_stmt->execute();
while (my @data = $parents_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" " . ($data[0] eq $parent_location_id ? 'selected' : '&nbsp;') . ">$data[1] - $data[2], $data[3] $data[4] </option>\n";
}
$parents_stmt->finish();

print "
   </select>
  </td>
 </tr>

  <script LANGUAGE=\"JavaScript\">
  <!--
  	function confirmSubmit()
	{
		var agree=confirm(\"To delete a location record you must first reassign all it's device to a different location.  If you have not done this already, click Cancel and do so now.\\n\\nContinue to delete $name?\");
		if (agree)
			return true ;
		else
			return false ;
  	}
  // -->
  </script>

  <td colspan=\"4\" align=\"center\">
   <input type=\"submit\" name=\"action\" value=\"Save\">
   <input type=\"submit\" name=\"action\" value=\"Delete\" onClick=\"return confirmSubmit()\">
   <input type=button value=\"List Devices\" onClick=\"javascript:window.location = '/device_list.cgi?location_id=$location_id';\">
  </td>
 </tr>
 </form>
</table>
";


$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFile("footer.html");
