#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
use OOCGI::NTable;

my $query = OOCGI::OOCGI->new;

my $pageNav  = OOCGI::PageNavigation->new(25);
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $terminal_batch_id = $PARAM{"terminal_batch_id"};
if(length($terminal_batch_id) == 0)
{
	print "Required Parameter Not Found: terminal_batch_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $get_tb_sql = q{
	SELECT tb.terminal_batch_num,
           t.terminal_desc,
           tb.terminal_batch_cycle_num,
           TO_CHAR(tb.terminal_batch_open_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           TO_CHAR(tb.terminal_batch_close_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           t.terminal_id,
           to_char(tb.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
           to_char(tb.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           t.terminal_cd,
           ts.terminal_state_name
      FROM terminal_batch tb, terminal t, terminal_state ts
     WHERE tb.terminal_batch_id = ?
       AND t.terminal_state_id = ts.terminal_state_id
       AND tb.terminal_id = t.terminal_id
};

my $qo2  = OOCGI::Query->new($get_tb_sql, { bind => $terminal_batch_id });
if(my @data = $qo2->next_array)
{
   my $terminal_batch_num = $data[0];
   my $terminal_desc = $data[1];
   my $terminal_batch_cycle_num = $data[2];
   my $terminal_batch_open_ts = $data[3];
   my $terminal_batch_close_ts = $data[4];
   my $terminal_id = $data[5];
   my $terminal_cd = $data[8];
   my $terminal_state = $data[9];

   my $get_sale_sql = q{
   	   SELECT NVL(SUM(auth_amt), 0), COUNT(1)
   	     FROM auth
	    WHERE terminal_batch_id = ?
	      AND auth_type_cd IN ('U', 'S')
   };

   my $qo4 = OOCGI::Query->new($get_sale_sql, { bind => $terminal_batch_id } );
   my @sale_data = $qo4->next_array;

   my $sale_sum = $sale_data[0];
   my $sale_num = $sale_data[1];

   my $get_refund_sql = q{
   	   SELECT ABS(NVL(SUM(refund_amt), 0)), COUNT(1)
	     FROM refund
	    WHERE terminal_batch_id = ?
   };
   my $qo5 = OOCGI::Query->new($get_refund_sql, { bind => $terminal_batch_id } );
   my @refund_data = $qo5->next_array;

   my $refund_sum = $refund_data[0];
   my $refund_num = $refund_data[1];

   print "
   <table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
    <tr>
     <th colspan=\"4\" bgcolor=\"#C0C0C0\">Terminal Batch</th>
    </tr>
    <tr>
     <td width=\"25%\">Terminal Batch ID</td>
     <td width=\"25%\">$terminal_batch_id&nbsp;</td>
     <td width=\"25%\">Terminal Batch Number</td>
     <td width=\"25%\">$terminal_batch_num&nbsp;</td>
    </tr>
    <tr>
      <td>Created</td>
      <td>$data[6]&nbsp;</td>
      <td>Last Updated</td>
      <td>$data[7]&nbsp;</td>
    </tr>
    <tr>
   <td>Batch Open Time</td>
   <td>$terminal_batch_open_ts&nbsp;</td>
   <td>Terminal Batch Cycle Count</td>
   <td>$terminal_batch_cycle_num&nbsp;</td>
 </tr>
 <tr>
    <td>Batch Close Time</td>
    <td>$terminal_batch_close_ts&nbsp;</td>
     <td>Sale Totals</td>
     <td>$sale_num / \$$sale_sum</td>
 </tr>
 <tr>
     <td>Terminal Name</td>
     <td>$terminal_desc</td>
     <td>Refund Totals</td>
     <td>$refund_num / \$$refund_sum</td>
 </tr>
 <tr>
	 <td>Terminal Code</td>
	 <td><a href=\"terminal.cgi?terminal_id=$terminal_id\">$terminal_cd</a></td>
     <td>Terminal State</td>
     <td>$terminal_state&nbsp;</td>
 </tr>
</table>
";
}

my $settlement_batch_list_sql = q{
	SELECT settlement_batch.settlement_batch_id,
           to_char(settlement_batch.settlement_batch_start_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	       to_char(settlement_batch.settlement_batch_end_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           settlement_batch_state.settlement_batch_state_name, 
	       to_char(settlement_batch.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           to_char(settlement_batch.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	       settlement_batch.settlement_batch_resp_cd,
           settlement_batch.settlement_batch_resp_desc,
           settlement_batch.settlement_batch_ref_cd, 
	       settlement_batch.terminal_batch_id 
	  FROM settlement_batch, settlement_batch_state 
	 WHERE settlement_batch.settlement_batch_state_id = settlement_batch_state.settlement_batch_state_id 
	   AND settlement_batch.terminal_batch_id = ?
	 ORDER BY settlement_batch.settlement_batch_start_ts desc
};

my $qo3 = OOCGI::Query->new($settlement_batch_list_sql, { bind => $terminal_batch_id } );
#$settlement_batch_list_stmt->bind_param(":terminal_batch_id", $terminal_batch_id);
#$settlement_batch_list_stmt->execute();
#my @settlement_batch_info;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
   <th colspan=\"6\" bgcolor=\"#C0C0C0\">Settlements</th>
  </tr>
  <tr>
   <th>ID</th>
   <th>Settlement Date</th>
   <th>Result</th>
  </tr>
";

while(my @settlement_batch_info = $qo3->next_array)
{
	print "
	     <tr>
    	 <td><a href=\"settlement_batch.cgi?settlement_batch_id=$settlement_batch_info[0]\">$settlement_batch_info[0]</a></td>
	     <td>$settlement_batch_info[1]&nbsp;</td>
	     <td>$settlement_batch_info[3]&nbsp;</td>
    	</tr>
	";
}

print "
</table>
";

my $transaction_list_sql = q{
	SELECT auth.auth_id,
           auth.tran_id,
           auth_type.auth_type_desc,
           auth_state.auth_state_name,
           auth.auth_parsed_acct_data, 
	       acct_entry_method.acct_entry_method_desc,
           auth.auth_amt,
           to_char(auth.auth_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           auth.auth_result_cd, 
	       auth.auth_resp_cd,
           auth.auth_resp_desc,
           auth.auth_authority_tran_cd,
           auth.auth_authority_ref_cd, 
	       to_char(auth.auth_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           to_char(auth.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	       to_char(auth.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           auth.terminal_batch_id,
           auth.auth_amt_approved, 
	       auth.auth_authority_misc_data,
           tran_state.tran_state_desc
	  FROM auth, auth_type,
           auth_state,
           acct_entry_method,
           tran,
           tran_state
	 WHERE auth.auth_type_cd = auth_type.auth_type_cd 
	   AND auth.auth_state_id = auth_state.auth_state_id 
	   AND auth.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd 
	   AND auth.tran_id = tran.tran_id
	   AND tran.tran_state_cd = tran_state.tran_state_cd
	   AND auth.terminal_batch_id = ?
};

my @table_cols = (
     ['Detail ID',          ,  2],
     ['Tran ID',            , -1],
     ['Type',               ,  3],
     ['Detail State',       ,  4],
     ['Transaction State',  ,  5],
     ['Amount',             ,  6],
     ['Timestamp',          , -7]
);

my @sql_cols = (
    'auth.auth_id',
    'auth.tran_id',
    'auth_type.auth_type_desc',
    'auth_state.auth_state_name',
    'tran_state.tran_state_desc',
    'auth.auth_amt',
    "NVL(auth.auth_ts, TO_DATE('1970', 'YYYY'))"
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');
$table->put($rn_table,0,B('Sales'),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

my $sql = $transaction_list_sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql,  [ $terminal_batch_id ]  );

while(my @transaction_info = $qo->next_array)
{
    $table->put($rn_table,0,qq{<a href=\"auth.cgi?auth_id=$transaction_info[0]\">$transaction_info[0]</a>});
    $table->put($rn_table,1,qq{<a href=\"tran.cgi?tran_id=$transaction_info[1]\">$transaction_info[1]</a>});
    $table->put($rn_table,2,qq{$transaction_info[2]});
    $table->put($rn_table,3,qq{$transaction_info[3]});
    $table->put($rn_table,4,qq{$transaction_info[19]});
    $table->put($rn_table,5,qq{\$$transaction_info[6]});
    $table->put($rn_table,6,qq{$transaction_info[7]});

    $rn_table++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

my $refund_list_sql = q{
	SELECT refund.refund_id,
           refund.tran_id,
           abs(refund.refund_amt),
           refund.refund_desc,
           to_char(refund.refund_issue_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	       refund.refund_issue_by,
           refund_type.refund_type_desc,
           to_char(refund.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	       to_char(refund.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           refund_state.refund_state_name, refund.terminal_batch_id,
           refund.refund_resp_cd, 
	       refund.refund_resp_desc,
           refund.refund_authority_tran_cd,
           refund.refund_authority_ref_cd, 
	       to_char(refund.refund_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           tran.parent_tran_id,
           refund.acct_entry_method_cd,
           refund.refund_parsed_acct_data,
           tran_state.tran_state_desc
	  FROM tran,
           refund,
           refund_type,
           refund_state,
           acct_entry_method,
           tran_state
	 WHERE tran.tran_id = refund.tran_id
	   AND refund.refund_type_cd = refund_type.refund_type_cd 
	   AND refund.refund_state_id = refund_state.refund_state_id 
	   AND refund.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd 
	   AND tran.tran_state_cd = tran_state.tran_state_cd
	   AND refund.terminal_batch_id = ?
	 ORDER BY refund.refund_issue_ts desc
};

my $qo1 = OOCGI::Query->new($refund_list_sql, { bind => $terminal_batch_id } );

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"7\" bgcolor=\"#C0C0C0\">Refunds</th>
 </tr>
 <tr>
  <th>Refund ID</th>
  <th>Tran ID</th>
  <th>Type</th>
  <th>Refund State</th>
  <th>Transaction State</th>
  <th>Amount</th>
  <th>Timestamp</th>
 </tr>
";

while(my @refund_info = $qo1->next_array)
{
	print "
        <tr>
    	 <td><a href=\"refund.cgi?refund_id=$refund_info[0]\">$refund_info[0]</a></td>
    	 <td><a href=\"tran.cgi?tran_id=$refund_info[1]\">$refund_info[1]</a></td>
	     <td>$refund_info[6]&nbsp;</td>
	     <td>$refund_info[9]&nbsp;</td>
	     <td>$refund_info[19]&nbsp;</td>
	     <td>\$$refund_info[2]&nbsp;</td>
	     <td>$refund_info[4]&nbsp;</td>
    	</tr>
	";
}

print "
</table>
";


$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
