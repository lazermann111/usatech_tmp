#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::DBObj;
use USAT::POS::Const qw(:PKG_POS_PTA_GLOBALS);
use USAT::App::DeviceAdmin::Const qw(:globals :globals_hash);
use USAT::Common::Const;

my %params = OOCGI::OOCGI->new()->Vars();

print "Content-Type: text/html; charset=ISO-8859-1\n\n";

# Customer Location
my $device_id = $params{device_id};
my $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);
my $DTI = $device->device_type_id;

my $T = OOCGI::NTable->new('border=1 width=100% noshade size=2 cellspacing=0');

my $SQL = q|
   SELECT HOST.host_id,
          HOST.host_type_id,
          HOST.host_equipment_id,
          TO_CHAR (HOST.host_last_start_ts, 'MM/DD/YYYY HH:MI:SS AM'),
          HOST.host_status_cd,
          HOST.host_serial_cd,
          HOST.host_est_complete_minut,
          HOST.host_port_num,
          HOST.host_position_num,
          HOST.host_label_cd,
          host_type.host_type_desc,
          host_equipment.host_equipment_mfgr,
          host_equipment.host_equipment_model,
          host_type.host_default_complete_minut,
          device_type_host_type.device_type_host_type_cd
     FROM device, HOST, host_type, host_equipment, device_type_host_type
    WHERE device.device_id = host.device_id
      and HOST.host_type_id = host_type.host_type_id
      and host.host_equipment_id = host_equipment.host_equipment_id
      and host_type.host_type_id = device_type_host_type.host_type_id
      and device.device_type_id = device_type_host_type.device_type_id
      AND device.device_id = ?
    ORDER BY HOST.host_port_num, HOST.host_position_num
    |;
if($DTI eq PKG_DEVICE_TYPE__ESUDS ) {
   $T->put(0,0,'Current Room Configuration',"colspan=7 class=header0");
   $T->put(['Host ID','Port','Type','Position','Label','Status','Last Start Time'],'class=header0');
} else {
   $T->put(0,0,'Component Information',"colspan=5 class=header0");
   $T->put(['Host ID','Type','Manufacturer','Model','Serial Number'],'class=header0');
}
my $QOBJ = OOCGI::Query->new($SQL, { bind => $device_id });
my $rn = 2;
while (my @data = $QOBJ->next)
{
   my $host_id               = $data[0];    # <<<
   my $host_type_id          = $data[1];
   my $host_equipment_id     = $data[2];
   my $host_last_start_ts    = $data[3];
   my $host_status           = $data[4];
   my $host_serial_cd        = $data[5];    # <<<
   my $host_comp_min         = $data[6];
   my $host_port             = $data[7];
   my $host_position         = $data[8];
   my $host_label            = $data[9];
   my $host_type_name        = $data[10];   # <<<
   my $host_manufacturer     = $data[11];   # <<<
   my $host_model            = $data[12];   # <<<
   my $host_default_comp_min = $data[13];
   my $host_type_cd          = $data[14];   # <<<

   if($DTI eq PKG_DEVICE_TYPE__ESUDS ) {
      $T->put($rn,0,qq{<a href="/host_profile.cgi?host_id=$host_id">$host_id</a>},'class=right');
      $T->put($rn,1,$host_port,'class=right');
      $T->put($rn,2,"$host_manufacturer $host_type_name");
      $T->put($rn,3,$host_position eq '0' ? 'Bottom' : 'Top');
      $T->put($rn,4,$host_label,'class=right');
      my $str = pkg_esuds_cycle_codes->{$host_status} . (($host_status eq '2' || $host_status eq '7') ? " ($host_comp_min Min)" : "") . "";
      $T->put($rn,5,$str);
      $T->put($rn,6,$host_last_start_ts,'class=right');
   } else {
      $T->put($rn,0,qq{<a href="/host_profile.cgi?host_id=$host_id">$host_id</a>});
      $T->put($rn,1,$host_type_name);
      $T->put($rn,2,$host_manufacturer);
      $T->put($rn,3,$host_model);
      $T->put($rn,4,$host_serial_cd);
   }
   $rn++;
}
display $T;
