#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use HTML::Entities;
use USAT::Database;
use USAT::Security::StringMask qw(mask_credit_card);
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $refund_id = $PARAM{"refund_id"};
if(length($refund_id) == 0)
{
	print "Required Parameter Not Found: refund_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $detail_info_stmt = $dbh->prepare(q{
	select refund.refund_id, 
	refund.tran_id, 
	refund.refund_desc, 
	refund_state.refund_state_name, 
	refund.refund_parsed_acct_data, 
	acct_entry_method.acct_entry_method_desc, 
	abs(refund.refund_amt), 
	to_char(refund.refund_issue_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	'placeholder',
	refund.refund_resp_cd, 
	refund.refund_resp_desc, 
	refund.refund_authority_tran_cd, 
	refund.refund_authority_ref_cd, 
	to_char(refund.refund_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(refund.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(refund.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	refund.terminal_batch_id,
	refund.refund_authority_misc_data,
	refund.refund_issue_by
	from refund, refund_type, refund_state, acct_entry_method
	where refund.refund_type_cd = refund_type.refund_type_cd
	and refund.refund_state_id = refund_state.refund_state_id
	and refund.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd(+)
	and refund.refund_id = :refund_id
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$detail_info_stmt->bind_param(":refund_id", $refund_id);
$detail_info_stmt->execute();
my @detail_data = $detail_info_stmt->fetchrow_array();
$detail_info_stmt->finish();

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Refund Details</th>
 </tr>
 <tr>
  <td nowrap>Detail ID</td>
  <td>$detail_data[0]&nbsp;</td>
  <td nowrap>Description</td>
  <td>$detail_data[2]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>State</td>
  <td>$detail_data[3]&nbsp;</td>
  <td nowrap>Account Data</td>
  <td>" . mask_credit_card($detail_data[4]) . "&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Amount</td>
  <td>\$$detail_data[6]&nbsp;</td>
  <td nowrap>Entry Method</td>
  <td>$detail_data[5]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Issued By</td>
  <td>$detail_data[18]&nbsp;</td>
  <td nowrap>Result</td>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Response Code</td>
  <td>$detail_data[9]&nbsp;</td>
  <td nowrap>Response Msg</td>
  <td>$detail_data[10]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Authorization Code</td>
  <td>$detail_data[11]&nbsp;</td>
  <td nowrap>Reference Num</td>
  <td>$detail_data[12]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Detail Timestamp</td>
  <td>$detail_data[7]&nbsp;</td>
  <td nowrap>Authority Timestamp</td>
  <td>$detail_data[13]&nbsp;</td>
 </tr>
 <tr>
   <td nowrap>Created</td>
   <td>$detail_data[14]&nbsp;</td>
   <td nowrap>Last Updated</td>
   <td>$detail_data[15]&nbsp; $detail_data[16]</td>
 </tr>
 <tr>
   <td nowrap>Terminal Batch ID</td>
   <td><a href=\"terminal_batch.cgi?terminal_batch_id=$detail_data[16]\">$detail_data[16]</a>&nbsp;</td>
  <td nowrap>Transaction ID</td>
  <td><a href=\"tran.cgi?tran_id=$detail_data[1]\">$detail_data[1]</a>&nbsp;</td>

 </tr>
 <tr>
  <td nowrap>Misc Data</td>
  <td colspan=\"3\">" . HTML::Entities::encode($detail_data[19]) . "&nbsp;</td>
 </tr>
</table>
";

my $settlement_batch_list_stmt = $dbh->prepare(
q{
	select settlement_batch.settlement_batch_id, to_char(settlement_batch.settlement_batch_start_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	to_char(settlement_batch.settlement_batch_end_ts, 'MM/DD/YYYY HH:MI:SS AM'), settlement_batch_state.settlement_batch_state_name, 
	to_char(settlement_batch.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(settlement_batch.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	settlement_batch.settlement_batch_resp_cd, settlement_batch.settlement_batch_resp_desc, settlement_batch.settlement_batch_ref_cd, 
	settlement_batch.terminal_batch_id 
	from settlement_batch, settlement_batch_state, refund_settlement_batch
	where settlement_batch.settlement_batch_state_id = settlement_batch_state.settlement_batch_state_id 
	and settlement_batch.settlement_batch_id = refund_settlement_batch.settlement_batch_id
	and refund_settlement_batch.refund_id = :refund_id
	order by settlement_batch.settlement_batch_start_ts desc
});
$settlement_batch_list_stmt->bind_param(":refund_id", $refund_id);
$settlement_batch_list_stmt->execute();
my @settlement_batch_info;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
   <th colspan=\"6\" bgcolor=\"#C0C0C0\">Settlements</th>
  </tr>
  <tr>
   <th>ID</th>
   <th>Settlement Date</th>
   <th>Result</th>
  </tr>
";

while(@settlement_batch_info = $settlement_batch_list_stmt->fetchrow_array())
{
	print "
	     <tr>
    	 <td><a href=\"settlement_batch.cgi?settlement_batch_id=$settlement_batch_info[0]\">$settlement_batch_info[0]</a></td>
	     <td>$settlement_batch_info[1]&nbsp;</td>
	     <td>$settlement_batch_info[3]&nbsp;</td>
    	</tr>
	";
}

$settlement_batch_list_stmt->finish();

print "
</table>
";

$dbh->disconnect;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
