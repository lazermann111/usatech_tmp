#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars();
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $event_id = $PARAM{event_id};
if(!$event_id)
{
	print "Required Parameter Not Found: event_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $data_stmt = $dbh->prepare(q{
	SELECT e.event_id,
		et.event_type_name,
		es.event_state_name,
		h.host_id,
		ht.host_type_desc,
		d.device_id,
		d.device_serial_cd,
		TO_CHAR(e.event_start_ts, 'MM/DD/YYYY HH:MI:SS AM'),
		TO_CHAR(e.event_end_ts, 'MM/DD/YYYY HH:MI:SS AM'),
		TO_CHAR(e.event_upload_complete_ts, 'MM/DD/YYYY HH:MI:SS AM'),
		TO_CHAR(e.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
		TO_CHAR(e.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
		e.event_device_tran_cd,
		e.event_global_trans_cd,
		dt.device_type_desc,
		se.device_batch_id,
		se.device_new_batch_id,
		to_char(cast(se.device_event_utc_ts as date) + se.device_event_utc_offset_min/1440, 'MM/DD/YYYY HH:MI:SS AM'),
		t.tran_id,
		TO_CHAR(t.tran_start_ts, 'MM/DD/YYYY HH:MI:SS AM')
	FROM device.event e
	JOIN device.event_state es
		ON e.event_state_id = es.event_state_id
	JOIN device.event_type et
		ON e.event_type_id = et.event_type_id
	JOIN device.host h
		ON e.host_id = h.host_id
	JOIN device.host_type ht
		ON h.host_type_id = ht.host_type_id		
	JOIN device.device d
		ON h.device_id = d.device_id
	JOIN device.device_type dt
		ON d.device_type_id = dt.device_type_id
	LEFT OUTER JOIN device.settlement_event se
		ON e.event_id = se.event_id
	LEFT OUTER JOIN pss.tran t
		ON e.event_global_trans_cd = t.tran_global_trans_cd
	WHERE e.event_id = :event_id
	ORDER BY t.created_ts
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$data_stmt->bind_param(":event_id", $event_id);
$data_stmt->execute();
my @data = $data_stmt->fetchrow_array();
$data_stmt->finish();

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Event Information</th>
 </tr>
 <tr>
  <td nowrap>Event ID</td>
  <td width=\"50%\">$data[0]&nbsp;</td>
  <td nowrap>Event Type</td>
  <td width=\"50%\">$data[1]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Event State</td>
  <td>$data[2]&nbsp;</td>
  <td nowrap>Start Time</td>
  <td>$data[7]&nbsp;</td>
 </tr>
 <tr>
  <td>Created</td>
  <td>$data[10]&nbsp;</td>
  <td nowrap>End Time</td>
  <td>$data[8]&nbsp;</td>
 </tr> 
 <tr>
  <td nowrap>Last Updated</td>
  <td>$data[11]&nbsp;</td>
  <td nowrap>Upload Time</td>
  <td>$data[9]&nbsp;</td>
 </tr> 
 <tr>
  <td nowrap>Device Event ID</td>
  <td>$data[12]&nbsp;</td>
  <td nowrap>Global Event Code</td>
  <td>$data[13]&nbsp;</td>
 </tr> 
 <tr>
  <td nowrap>Device Serial #</td>
  <td><a href=\"profile.cgi?device_id=$data[5]\">$data[6]</a>&nbsp;</td>
  <td nowrap>Device Type</td>
  <td>$data[14]&nbsp;</td>  
 </tr> 
 <tr>
  <td nowrap>Host ID</td>
  <td><a href=\"host_profile.cgi?host_id=$data[3]\">$data[3]</a>&nbsp;</td>
  <td nowrap>Host Type</td>
  <td>$data[4]&nbsp;</td>
 </tr>
";

if ($data[18]) {
	print "
	 <tr>
	  <td nowrap>Reference Tran ID</td>
	  <td><a href=\"tran.cgi?tran_id=$data[18]\">$data[18]</a>&nbsp;</td>
	  <td nowrap>Tran Start Time</td>
	  <td>$data[19]&nbsp;</td>
	 </tr>
	";
}
 
print "
</table>
";

if (defined $data[15]) {
	print"
	<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
	<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
	 <tr>
	   <th colspan=\"6\" bgcolor=\"#C0C0C0\">Settlement Event</th>
	  </tr>
	  <tr>
	   <td nowrap>Device Batch ID</td>
	   <td width=\"50%\">$data[15]&nbsp;</td>
	   <td nowrap>Device New Batch ID</td>
	   <td width=\"50%\">$data[16]&nbsp;</td>
	  </tr> 
	  <tr>
	   <td nowrap>Timestamp</td>
	   <td colspan=\"3\">$data[17]&nbsp;</td>
	  </tr>
	</table>
	";
}

my $details_stmt = $dbh->prepare(
q{
	SELECT ed.event_detail_id,
		edt.event_detail_type_name,
		ed.event_detail_value,
		TO_CHAR(NVL(ed.event_detail_value_ts, ed.created_ts), 'MM/DD/YYYY HH:MI:SS AM')
	FROM device.event_detail ed
	JOIN device.event_detail_type edt
		ON ed.event_detail_type_id = edt.event_detail_type_id
	WHERE ed.event_id = :event_id
	ORDER BY ed.event_detail_id
});
$details_stmt->bind_param(":event_id", $event_id);
$details_stmt->execute();
my @details;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
   <th colspan=\"6\" bgcolor=\"#C0C0C0\">Event Details</th>
  </tr>
  <tr>
   <th nowrap>Detail ID</th>
   <th nowrap>Detail Type</th>
   <th>Value</th>
   <th>Timestamp</th>
  </tr>
";

while(@details = $details_stmt->fetchrow_array())
{
	print "
	     <tr>    	 
	     <td>$details[0]&nbsp;</td>
		 <td>$details[1]&nbsp;</td>
		 <td>$details[2]&nbsp;</td>
	     <td>$details[3]&nbsp;</td>
    	</tr>
	";
}

$details_stmt->finish();

print "
</table>
";

my $counters_stmt = $dbh->prepare(
q{
	SELECT hc.host_counter_id,
		h.host_id,
		ht.host_type_desc,
		hc.host_counter_parameter, 
		CASE WHEN hc.host_counter_parameter LIKE '%_MONEY' AND LENGTH(hc.host_counter_value) <= 14
			AND NVL(DBADMIN.TO_NUMBER_OR_NULL(hc.host_counter_value), -1) >= 0 THEN
			TRIM(TO_CHAR(DBADMIN.TO_NUMBER_OR_NULL(hc.host_counter_value) / 100, '$999,999,999,990.00'))
		ELSE hc.host_counter_value END,
		to_char(hc.created_ts, 'MM/DD/YYYY HH:MI:SS AM')
	FROM device.event e
	JOIN device.host_counter_event hce
		ON e.event_id = hce.event_id		
	JOIN device.host_counter hc
		ON hce.host_counter_id = hc.host_counter_id
	JOIN device.host h
		ON hc.host_id = h.host_id
	JOIN device.host_type ht
		ON h.host_type_id = ht.host_type_id
	WHERE e.event_id = :event_id
	ORDER BY hc.host_counter_id
});
$counters_stmt->bind_param(":event_id", $event_id);
$counters_stmt->execute();
my @counters;

my $i = 0;
while(@counters = $counters_stmt->fetchrow_array())
{
	if ($i == 0) {
		print "
			<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
			<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
			 <tr>
			   <th colspan=\"6\" bgcolor=\"#C0C0C0\">Event Counters</th>
			  </tr>
			  <tr>
			   <th nowrap>Counter ID</th>
			   <th nowrap>Host ID</th>
			   <th nowrap>Host Type</th>
			   <th>Counter</th>
			   <th>Value</th>
			   <th>Timestamp</th>
			  </tr>
			";	
	}

	print "
	     <tr>    	 
	     <td>$counters[0]&nbsp;</td>
		 <td><a href=\"host_profile.cgi?host_id=$counters[1]\">$counters[1]</a>&nbsp;</td>
		 <td>$counters[2]&nbsp;</td>
	     <td>$counters[3]&nbsp;</td>
		 <td>$counters[4]&nbsp;</td>
		 <td>$counters[5]&nbsp;</td>
    	</tr>
	";
	
	$i++;
}

$counters_stmt->finish();

if ($i > 0) {
	print "
		</table>
	";
}

my $files_stmt = $dbh->prepare(
q{
	SELECT dft.device_file_transfer_id,
		ft.file_transfer_id,
		ft.file_transfer_name,
		DECODE(dft.device_file_transfer_direct, 'O', 'Server to Client', 'Client to Server'),
		ftt.file_transfer_type_name,
		TO_CHAR(dft.device_file_transfer_ts, 'MM/DD/YYYY HH:MI:SS AM'),
		DECODE(dft.device_file_transfer_status_cd, '0', 'Cancelled', '1', 'Complete', 'S', 'Sending', 'Incomplete')
	FROM device.device_file_transfer_event dfte
	JOIN device.device_file_transfer dft
		ON dfte.device_file_transfer_id = dft.device_file_transfer_id
	JOIN device.file_transfer ft
		ON dft.file_transfer_id = ft.file_transfer_id
	JOIN device.file_transfer_type ftt
		ON ft.file_transfer_type_cd = ftt.file_transfer_type_cd
	WHERE dfte.event_id = :event_id
	ORDER BY dft.created_ts DESC
});
$files_stmt->bind_param(":event_id", $event_id);
$files_stmt->execute();
my @files;

$i = 0;
while(@files = $files_stmt->fetchrow_array())
{
	if ($i == 0) {
		print "
			<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
			<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
			 <tr>
			   <th colspan=\"6\" bgcolor=\"#C0C0C0\">Event Files</th>
			  </tr>
			  <tr>
			   <th nowrap>Transfer ID</th>
			   <th nowrap>File Name</th>
			   <th>Direction</th>
			   <th>File Type</th>
			   <th>Transfer Time</th>
			   <th>Status</th>
			  </tr>
			";	
	}

	print "
	     <tr>    	 
	     <td>$files[0]&nbsp;</td>
		 <td><a href=\"file_details.cgi?file_id=$files[1]\">$files[2]</a>&nbsp;</td>
		 <td>$files[3]&nbsp;</td>
	     <td>$files[4]&nbsp;</td>
		 <td>$files[5]&nbsp;</td>
		 <td>$files[6]&nbsp;</td>
    	</tr>
	";
	
	$i++;
}

$files_stmt->finish();

if ($i > 0) {
	print "
		</table>
	";
}

$dbh->disconnect;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
