#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
require LWP::UserAgent;
use HTTP::Request::Common qw(POST);
use USAT::DeviceAdmin::UI::DAHeader;

my $production = 1;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};
my $DATABASE2 = USAT::Database->new(
	primary => 'tulsap1', backup => 'tulsap1', 
	username =>"payman", password =>"payman", 
	PrintError => 1, RaiseError => 1, AutoCommit => 1
);
my $quick_pss_dbh = $DATABASE2->{handle};

my %PRC_HASH = (
	'1' => 	"Operation Success",
	'2' => 	"Operation Pending",
	'3' => 	"Undefined Object",
	'4' => 	"Parameter Too Short",
	'5' => 	"Parameter Too Long",
	'6' => 	"Parameter Format Error",
	'7' => 	"Parameter Value Error",
	'8' => 	"Duplicate Object",
	'9' => 	"Parameter Mismatch",
	'10' => 	"Input Error",
	'11' => 	"Object not valid in present state",
	'12' => 	"Communication Error",
	'13' => 	"Internal ETILL Error",
	'14' => 	"Database Communication Error",
	'15' => 	"A cassette specific error occured",
	'17' => 	"Unsupported API Version",
	'18' => 	"Obsolete API Version",
	'19' => 	"Auto-approve Failed",
	'20' => 	"Auto-deposit Failed",
	'21' => 	"Cassette Not Running!",
	'22' => 	"Cassette Not Valid!",
	'23' => 	"Unsupported Sysplex Environment",
	'24' => 	"Missing Parameter Value",
	'30' => 	"XML Document Error",
	'31' => 	"Corequisite Parameter Not Found",
	'32' => 	"Invalid Parameter Combination",
	'33' => 	"Batch Error",
	'34' => 	"Financial Failure",
	'50' => 	"Servlet Init Error",
	'51' => 	"Authentication Error",
	'52' => 	"Authorization Error",
	'53' => 	"Unhandled Exception",
	'54' => 	"Duplicate Parameter Value Not Allowed",
	'55' => 	"Command Not Supported",
	'56' => 	"Cryptography Error",
	'57' => 	"Not Active",
	'58' => 	"Parameter Not Allowed",
	'59' => 	"Delete Error",
	'60' => 	"Websphere Error",
	'61' => 	"Sysplex Admin Only",
	'62' => 	"Realm Error"
);

my %SRC_HASH = (

	'0' => 	'No additional information available.',
	'1' => 	'An initialization message is included in the return data buffer. This buffer must be freed by the caller of this routine.',
	'2' => 	'Input stream exceeds maximum length.',
	'3' => 	'Unknown command.',
	'4' => 	'An unexpected error has occurred.',
	'5' => 	'The Payment Manager received an exception when reading data from the merchant server.',
	'6' => 	'API initialization failed.',
	'110' => 	'Response refers to the merchant number parameter.',
	'111' => 	'Response refers to the order number parameter.',
	'112' => 	'Response refers to the PAYMENTNUMBER parameter.',
	'113' => 	'Response refers to the CREDITNUMBER parameter.',
	'114' => 	'Response refers to the BATCHNUMBER parameter. (Note: In previous versions this return code referenced the BATCHID parameter.)',
	'115' => 	'Response refers to the ACCOUNTNUMBER parameter.',
	'116' => 	'Response refers to the PAYMENTTYPE parameter.',
	'117' => 	'Response refers to the AMOUNT parameter.',
	'118' => 	'Response refers to the AMOUNTEXP10 parameter.',
	'119' => 	'Response refers to the CURRENCY parameter.',
	'120' => 	'Response refers to the order description parameter.',
	'121' => 	'Response refers to the character set parameter.',
	'122' => 	'Response refers to the success URL parameter.',
	'123' => 	'Response refers to the failure URL parameter.',
	'124' => 	'Response refers to the cancel URL parameter.',
	'125' => 	'Response refers to the approve flag parameter.',
	'126' => 	'Response refers to the payment amount parameter.',
	'127' => 	'Response refers to the splits allowed parameter.',
	'128' => 	'Response refers to the deposit flag parameter.',
	'129' => 	'Response refers to the protocol data parameter.',
	'130' => 	'Response refers to the order URL parameter.',
	'131' => 	'Response refers to the service URL parameter.',
	'132' => 	'Response refers to the cassette command parameter.',
	'133' => 	'Response refers to the user parameter.',
	'134' => 	'Response refers to the event type parameter.',
	'135' => 	'Response refers to the withCredits parameter.',
	'136' => 	'Response refers to the creation begin time parameter.',
	'137' => 	'Response refers to the creation end time parameter.',
	'138' => 	'Response refers to the minimum amount parameter.',
	'139' => 	'Response refers to the maximum amount parameter.',
	'140' => 	'Response refers to the ?return at most? parameter.',
	'141' => 	'Response refers to the keys only parameter.',
	'143' => 	'Response refers to the dtd path parameter.',
	'144' => 	'Response refers to the reference number parameter.',
	'145' => 	'Response refers to the withOrders parameter.',
	'146' => 	'Response refers to the messages key.',
	'147' => 	'Response refers to the batch open beginning time parameter.',
	'148' => 	'Response refers to the batch open ending time parameter.',
	'149' => 	'Response refers to the batch close beginning time parameter.',
	'150' => 	'Response refers to the batch close ending time parameter.',
	'151' => 	'Response refers to the status parameter.',
	'153' => 	'Response refers to the close allowed parameter.',
	'154' => 	'Response refers to the withPayments parameter.',
	'155' => 	'Response refers to the time registered parameter.',
	'156' => 	'Response refers to the minimum approve amount parameter.',
	'157' => 	'Response refers to the maximum approve amount parameter.',
	'158' => 	'Response refers to the minimum deposit amount parameter.',
	'159' => 	'Response refers to the maximum deposit amount parameter.',
	'160' => 	'Response refers to the order URL parameter.',
	'161' => 	'Response refers to the modification beginning time parameter.',
	'162' => 	'Response refers to the modification ending time parameter.',
	'165' => 	'Response refers to the delete order parameter.',
	'166' => 	'Response refers to the minimum un-approved amount parameter.',
	'167' => 	'Response refers to the maximum un-approved amount parameter.',
	'168' => 	'Response refers to the approve allowed parameter.',
	'169' => 	'Response refers to the PURGEALLOWED parameter.',
	'170' => 	'Response refers to the $MAXBATCHSIZE parameter.',
	'171' => 	'Inspect cassette-specific data for further information.',
	'172' => 	'Response refers to the FORCE parameter. May be returned in response to the BATCHCLOSE command. Indicates that the error described by the primary return code refers to the boolean parameter FORCE.',
	'173' => 	'Response refers to the acceptPayment approve flag parameter.',
	'174' => 	'Response refers to the acceptPayment deposit flag parameter.',
	'175' => 	'Response refers to the receivePayment approve flag parameter.',
	'176' => 	'Response refers to the receivePayment deposit flag parameter.',
	'177' => 	'Response refers to the ApprovalExpiration parameter.',
	'202' => 	'Response refers to merchant payment system (such as SET).',
	'203' => 	'Response refers to an account.',
	'204' => 	'Response refers to an order entity.',
	'205' => 	'Response refers to a payment entity.',
	'206' => 	'Response refers to a credit entity.',
	'207' => 	'Response refers to a batch entity.',
	'208' => 	'Response refers to a brand.',
	'209' => 	'Response refers to the state.',
	'211' => 	'Response refers to batch objects.',
	'212' => 	'An error occurred during automatic batch open',
	'213' => 	'The batch is empty. An attempt was made to close a batch that does not contain any payments or credits. It is up to the cassette to decide whether or not this is an error condition.',
	'214' => 	'Response refers to the sysplex flag.',
	'215' => 	'Response refers to the communication type.',
	'216' => 	'Response refers to the payment group name.',
	'217' => 	'Response refers to the admin host name.',
	'218' => 	'Response refers to the Net.Dispatcher host name.',
	'219' => 	'Response refers to the sysplex name.',
	'301' => 	'The specified Payment Manager host is not valid.',
	'303' => 	'The Payment Manager hostname parameter is in error.',
	'306' => 	'Could not locate host IP address.',
	'307' => 	'Could not initialize socket library.',
	'308' => 	'A PaymentServerHandle is required for this API.',
	'309' => 	'A communication error occurred.',
	'310' => 	'Bits that are reserved for future use are non-zero. They must be zero.',
	'311' => 	'The value specified on the TimePeriod is invalid.',
	'312' => 	'The keyword in the protocol data is not valid.',
	'313' => 	'The amount range is not valid.',
	'320' => 	'Could not open a socket to communicate with the Payment Manager. TCP/IP socket resources may be depleted.',
	'321' => 	'Could not open a network connection to the Payment Manager using port and address specified earlier on an etInitializeAPI() call.',
	'322' => 	'Could not send data on network connection with Payment Manager. Payment Manager may have closed the connection.',
	'323' => 	'Could not receive data on network connection with Payment Manager. Payment Manager may have closed the connection.',
	'324' => 	'Could not check for data ready to read on network connection with Payment Manager. Payment Manager may have closed the connection.',
	'325' => 	'Failed to close the socket.',
	'400' => 	'An encoding error occurred.',
	'401' => 	'The XML document type is not supported.',
	'402' => 	'The document is empty.',
	'403' => 	'The order collection is missing.',
	'404' => 	'The XML document generated by an XDM query was too large. Refine the search criteria and re-attempt the query.',
	'500' => 	'An error occurred during the servlet initialization.',
	'501' => 	'The property file can not be located.',
	'502' => 	'An error occurred while loading the property file.',
	'503' => 	'Response refers to the JDBC driver name.',
	'504' => 	'Response refers to the JDBC URL.',
	'505' => 	'Response refers to the database owner.',
	'506' => 	'Response refers to the database user id.',
	'507' => 	'Response refers to the database password.',
	'508' => 	'Response refers to the log path.',
	'509' => 	'Response refers to the host name.',
	'510' => 	'Response refers to the Payment Manager engine port number.',
	'511' => 	'An error occurred while loading JDBC driver.',
	'512' => 	'An error occurred while either connecting to the database or executing the SQL statement.',
	'513' => 	'An error occurred while initializing the error log.',
	'514' => 	'An error occurred while loading the cassette.',
	'515' => 	'The root password is not valid.',
	'516' => 	'Response refers to the maximum number of database connections.',
	'517' => 	'Response refers to the minimum role allowed to view sensitive financial data.',
	'518' => 	'Parameter refers to the new password.',
	'519' => 	'Parameter refers to the data source name.',
	'530' => 	'Response refers to the Operation parameter.',
	'531' => 	'Response refers to the etApiVersion parameter.',
	'553' => 	'No authenticated user was given for the Payment Manager command.',
	'554' => 	'The specified user is not authorized to perform the requested operation.',
	'555' => 	'There is no name specified for the ProtectedRealm setting in the PaymentServlet.properties file.',
	'556' => 	'The realm specified in the PaymentServlet.properties file is unknown.',
	'557' => 	'Response refers to the eTill.RealmClass property.',
	'600' => 	'Response refers to the Payment Manager administration entity.',
	'601' => 	'Response refers to a cassette administration entity.',
	'602' => 	'Response refers to a merchant administration entity.',
	'603' => 	'Response refers to a payment system administration entity.',
	'604' => 	'Response refers to an account administration entity.',
	'611' => 	'Response refers to the ETILLHOSTNAME parameter.',
	'612' => 	'Response refers to the TRACESETTING parameter.',
	'613' => 	'Response refers to the TRACEFILESIZE parameter.',
	'614' => 	'Response refers to the LOGPATH parameter.',
	'615' => 	'Response refers to the CASSETTENAME parameter.',
	'616' => 	'Response refers to the MERCHANTTITLE parameter.',
	'617' => 	'Response refers to the ACCOUNTTITLE parameter.',
	'618' => 	'Response refers to the FINANCIALINSTITUTION parameter.',
	'619' => 	'Response refers to the OBJECTNAME parameter.',
	'620' => 	'Response refers to the ENABLED parameter.',
	'621' => 	'Response refers to the EVENTLISTENER object.',
	'622' => 	'Response refers to the LISTENERURL parameter.',
	'623' => 	'Response refers to the SOCKSPORT parameter.',
	'624' => 	'Response refers to the user role parameter.',
	'625' => 	'Response refers to the user object.',
	'626' => 	'Response refers to the user (the user is not enabled).',
	'627' => 	'Response refers to the User object (the user has rights to the Payment Manager. The user is misconfigured).',
	'628' => 	'Encryption key has been altered.',
	'629' => 	'Encryption key did not exist for the specified component.',
	'630' => 	'Response refers to the SOCKSHOST parameter.',
	'631' => 	'Failed to encrypt encryption key.',
	'632' => 	'Failed to decrypt encryption key.',
	'633' => 	'The encryption key type is not supported.',
	'634' => 	'Failed to validate encryption key.',
	'635' => 	'Failed to generate encryption key.',
	'636' => 	'The user is not the ACL owner.',
	'637' => 	'A realm error has been occurred.',
	'638' => 	'The ACL is not defined.',
	'639' => 	'The user is the last owner of the ACL.',
	'640' => 	'The user is not defined in the WebSphere realm.',
	'641' => 	'Try to remove a user�s access rights who does not have one in the Payment Manager.',
	'642' => 	'Response refers to the FILTER parameter.',
	'643' => 	'Response refers to the TRANSACTIONID parameter.',
	'644' => 	'Response refers to the ORDERDATA1 parameter.',
	'645' => 	'Response refers to the ORDERDATA2 parameter.',
	'646' => 	'Response refers to the ORDERDATA3 parameter.',
	'647' => 	'Response refers to the ORDERDATA4 parameter.',
	'648' => 	'Response refers to the ORDERDATA5 parameter.',
	'649' => 	'Response refers to the service thread pool size eTill.spoolsize.',
	'650' => 	'It is only valid to change the PM password immediately after the Payment Manager Application Server is started.',
	'651' => 	'Response refers to the wpm.AsynApproveDelayTimeInSecs parameter.',
	'652' => 	'Response refers to the wpm.ApprovalExpirationDelayTimeInMins parameter.',
	'653' => 	'Response refers to the protocol thread pool size wpm.ppoolsize.',
	'900' => 	'Response refers to purchase card data�s shipping amount parameter.',
	'901' => 	'Response refers to purchase card data�s duty amount parameter.',
	'902' => 	'Response refers to purchase card data�s duty reference parameter.',
	'903' => 	'Response refers to purchase card data�s national tax amount parameter.',
	'904' => 	'Response refers to purchase card data�s national tax rate parameter.',
	'905' => 	'Response refers to purchase card data�s local tax amount parameter.',
	'906' => 	'Response refers to purchase card data�s other tax amount parameter.',
	'907' => 	'Response refers to purchase card data�s total tax amount parameter.',
	'908' => 	'Response refers to purchase card data�s merchant tax id parameter.',
	'909' => 	'Response refers to purchase card data�s alternate tax id parameter.',
	'910' => 	'Response refers to purchase card data�s tax exempt indicator parameter.',
	'911' => 	'Response refers to purchase card data�s merchant duty tariff reference parameter.',
	'912' => 	'Response refers to purchase card data�s customer duty tariff reference parameter.',
	'913' => 	'Response refers to purchase card data�s summary commodity code parameter.',
	'914' => 	'Response refers to purchase card data�s merchant type parameter.',
	'915' => 	'Response refers to purchase card data�s merchant country code parameter.',
	'916' => 	'Response refers to purchase card data�s merchant city code parameter.',
	'917' => 	'Response refers to purchase card data�s merchant state province parameter.',
	'918' => 	'Response refers to purchase card data�s merchant postal code parameter.',
	'919' => 	'Response refers to purchase card data�s merchant location id parameter.',
	'920' => 	'Response refers to purchase card data�s merchant name parameter.',
	'921' => 	'Response refers to purchase card data�s ship from country code parameter.',
	'922' => 	'Response refers to purchase card data�s ship from city code parameter.',
	'923' => 	'Response refers to purchase card data�s ship from state province parameter.',
	'924' => 	'Response refers to purchase card data�s ship from postal code parameter.',
	'925' => 	'Response refers to purchase card data�s ship from location id parameter.',
	'926' => 	'Response refers to purchase card data�s ship to country code parameter.',
	'927' => 	'Response refers to purchase card data�s ship to city code parameter.',
	'928' => 	'Response refers to purchase card data�s ship to state province parameter.',
	'929' => 	'Response refers to purchase card data�s ship to postal code parameter.',
	'930' => 	'Response refers to purchase card data�s ship to location id parameter.',
	'931' => 	'Response refers to purchase card data�s merchant order number parameter.',
	'932' => 	'Response refers to purchase card data�s customer reference number parameter.',
	'933' => 	'Response refers to purchase card data�s order summary parameter.',
	'934' => 	'Response refers to purchase card data�s customer service phone parameter.',
	'935' => 	'Response refers to purchase card data�s discount amount parameter.',
	'936' => 	'Response refers to purchase card data�s shipping national tax rate parameter.',
	'937' => 	'Response refers to purchase card data�s shipping national tax amount parameter.',
	'938' => 	'Response refers to purchase card data�s national tax invoice reference parameter.',
	'939' => 	'Response refers to purchase card data�s print customer service phone number parameter.',
	'940' => 	'Response refers to line item data�s commodity code parameter.',
	'941' => 	'Response refers to line item data�s product code parameter.',
	'942' => 	'Response refers to line item data�s descriptor parameter.',
	'943' => 	'Response refers to line item data�s quantity parameter.',
	'944' => 	'Response refers to line item data�s SKU parameter.',
	'945' => 	'Response refers to line item data�s unit cost parameter.',
	'946' => 	'Response refers to line item data�s unit of measure parameter.',
	'947' => 	'Response refers to line item data�s net cost parameter.',
	'948' => 	'Response refers to line item data�s discount amount parameter.',
	'949' => 	'Response refers to line item data�s discount indicator parameter.',
	'950' => 	'Response refers to line item data�s national tax amount parameter.',
	'951' => 	'Response refers to line item data�s national tax rate parameter.',
	'952' => 	'Response refers to line item data�s national tax type parameter.',
	'953' => 	'Response refers to line item data�s local tax amount parameter.',
	'954' => 	'Response refers to line item data�s local tax rate parameter.',
	'955' => 	'Response refers to line item data�s other tax amount parameter.',
	'956' => 	'Response refers to line item data�s total cost parameter.',
	'1000' => 	'The cassette does not support this command.',
	'1001' => 	'The cassette does not support this command.',
	'1002' => 	'Batch ID was either (1) specified when prohibited or (2) not specified when required.',
	'1003' => 	'The cassette allows only complete refund reversals (that is, the amount must be zero).',
	'1004' => 	'The operation experienced financial failure.',
	'1008' => 	'An encryption error occurred while the cassette was composing or processing a protocol message.',
	'1009' => 	'A decryption error occurred while the cassette was composing or processing a protocol message.',
	'1010' => 	'A BATCHOPEN or BATCHCLOSE command but the financial processor associated with the account controls batch processing.',
	'1011' => 	'The currency for all transactions in a batch must be the same.',
	'1012' => 	'The amount exponent for all transactions in a batch must be the same.',
	'1014' => 	'Response refers to the brand parameter (specified in protocol data).',
	'1015' => 	'Response refers to the PAN parameter (specified in protocol data).',
	'1016' => 	'Response refers to the expiry parameter (specified in protocol data).',
	'1017' => 	'This account only allows complete deposit reversals (that is, the amount must be zero).',
	'1018' => 	'A communication error occurred between the cassette and an entity with which it communicates.',
	'1019' => 	'The cassette received a unexpected NULL response from an entity with which it communicates.',
	'1020' => 	'The cassette received a unexpected response from an entity with which it communicates.',
	'1021' => 	'A batch-related error occurred.',
	'1022' => 	'The totals for this batch calculated by the Payment Manager and the financial institution did not match.',
	'1040' => 	'While processing an APPROVE with automatic deposit, the cassette successfully completed the approval, but could not successfully complete the deposit.',
	'1041' => 	'The financial institution declined the request for an unknown reason.',
	'1042' => 	'The financial institution declined the request due to the expiry value.',
	'1043' => 	'The financial institution declined the request due to a problem with the purchase instrument (the credit card, check or whatever instrument is used by this cassette�s payment protocol).',
	'1051' => 	'Response refers to the group of AVS parameters (specified in protocol data).',
	'1052' => 	'Response refers to the AVS country code parameter (specified in protocol data).',
	'1053' => 	'Response refers to the AVS street address parameter (specified in protocol data).',
	'1054' => 	'Response refers to the AVS city parameter (specified in protocol data).',
	'1055' => 	'Response refers to the AVS state/province parameter (specified in protocol data).',
	'1056' => 	'Response refers to the AVS postal code parameter (specified in protocol data).',
	'1057' => 	'Response refers to the AVS location id parameter (specified in protocol data).',
	'1058' => 	'Response refers to the cardholder name parameter (specified in protocol data).',
	'1059' => 	'Response refers to the maximum batch size parameter (specified in protocol data).',
	'1060' => 	'Response refers to the currency parameter (specified in protocol data).',
	'1061' => 	'The operation failed completely or partially. Human intervention is required to resolve the failure.',
	'1062' => 	'The approval for the payment has expired. You must obtain a new approval for the payment amount before you can successfully deposit. If the cassette supports ApproveReversal, then use it to obtain the new approval for the existing payment. Otherwise, use Approve to create a new approved payment which you can subsequently deposit.',
	'1063' => 	'Approval of the specified amount would cause the cumulative amount of all payments exceed the original order amount.',
	'1064' => 	'Cassette version specified in the database table exceeds the maximum length.',
	'1065' => 	'Response refers to the specified card verification code.',
	'1066' => 	'Response refers to the specified authorization code.',
	'1067' => 	'Response refers to the specified decline code.',
	'1068' => 	'The realm defined in PaymentServlet.properties could not be initialized.',
	'1069' => 	'An error occurred while using the realm defined in PaymentServlet.properties.',
	'1071' => 	'Response refers to the group of shipping address parameters (specified in protocol data).',
	'1072' => 	'Response refers to the shipping country code parameter (specified in protocol data).',
	'1073' => 	'Response refers to the shipping street address parameter (specified in protocol data).',
	'1074' => 	'Response refers to the shipping city parameter (specified in protocol data).',
	'1075' => 	'Response refers to the shipping state/province parameter (specified in protocol data).',
	'1076' => 	'Response refers to the shipping postal code parameter (specified in protocol data).',
	'1081' => 	'Response refers to the group of billing address parameters (specified in protocol data).',
	'1082' => 	'Response refers to the billing country code parameter (specified in protocol data).',
	'1083' => 	'Response refers to the billing street address parameter (specified in protocol data).',
	'1084' => 	'Response refers to the billing city parameter (specified in protocol data).',
	'1085' => 	'Response refers to the billing state/province parameter (specified in protocol data).',
	'1086' => 	'Response refers to the billing postal code parameter (specified in protocol data).',
	'1087' => 	'Response refers to the approve flag on the merchant account on AcceptPayment.',
	'1088' => 	'Response refers to the deposit flag on the merchant account on AcceptPayment.',
	'1089' => 	'Response refers to the approve flag on the merchant account on ReceivePayment.',
	'1090' => 	'Response refers to the deposit flag on the merchant account on ReceivePayment.',
	'1092' => 	'Response refers to the country code parameter (specified in protocol data).',
	'1093' => 	'Response refers to the street address parameter (specified in protocol data).',
	'1094' => 	'Response refers to the city parameter (specified in protocol data).',
	'1095' => 	'Response refers to the state or province parameter (specified in protocol data).',
	'1096' => 	'Response refers to the postal (zip) code parameter (specified in protocol data).',
	'1097' => 	'Response refers to the AVS code parameter (specified in protocol data).',
	'1098' => 	'Conflicting protocol data was specified with this API command.',
	'1099' => 	'Response refers to the batch close time parameter (specified in protocol data).',
	'1100' => 	'Response refers to the payment method parameter (specified in protocol data).',
	'1101' => 	'Response refers to the financial institution batch identification parameter (specified in protocol data).',
	'1102' => 	'Response refers to the first auxiliary text parameter (specified in protocol data).',
	'1103' => 	'Response refers to the second auxiliary text parameter (specified in protocol data).',
	'1104' => 	'Response refers to the specified authorization reason.',
	'1105' => 	'Response refers to the Buyer Name.',
	'1106' => 	'Response refers to the Street Address, Line 2.',
	'1107' => 	'Response refers to the phone number.',
	'1108' => 	'Response refers to the email address.',
	'1109' => 	'Response refers to the check routing number.',
	'1110' => 	'Response refers to the checking account number.'
);	


my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

# uncomment for production!
# require SSL
if($production)
{
	if($ENV{SERVER_PORT} eq '80')
	{
	        $dbh->disconnect;
	        $quick_pss_dbh->disconnect;
	
			print CGI->redirect("https://device-admin.usatech.com/charge_card.cgi");
	        exit();
	}
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

my $log;

my $MerchantNum = '20030028';
my $ClientId    = 'WEBAPP';
my $Currency    = '840';
my $PaymentData = $PARAM{"PaymentData"};
my $CardHolderName = $PARAM{"CardHolderName"};
my $AmountLowestCurr = $PARAM{"AmountLowestCurr"};
my $AVS1 = $PARAM{"AVS1"};
my $AVS2 = $PARAM{"AVS2"};
my $ExpMonth = $PARAM{"ExpMonth"};
my $ExpYear  = $PARAM{"ExpYear"};
my $OrderId  = $PARAM{"OrderId"};

my $action = $PARAM{"action"};
if($action eq 'Next >')
{
	my $error;
	print "&nbsp<br>";
	
	if(length($PaymentData) == 0)
	{
		print "<font color=\"red\"><li>Missing Card Number</li></font><br>\n";
		$error = 1;
	}

	if(length($CardHolderName) == 0)
	{
		print "<font color=\"red\"><li>Missing Name on Card</li></font><br>\n";
		$error = 1;
	}
	
	if(length($AmountLowestCurr) == 0)
	{
		print "<font color=\"red\"><li>Missing Amount</li></font><br>\n";
		$error = 1;
	}
	
	if(length($AVS1) == 0)
	{
		print "<font color=\"red\"><li>Missing Street Address</li></font><br>\n";
		$error = 1;
	}
	
	if(length($AVS2) == 0)
	{
		print "<font color=\"red\"><li>Missing Zip Code</li></font><br>\n";
		$error = 1;
	}
	
	if(length($ExpMonth) == 0)
	{
		print "<font color=\"red\"><li>Missing Expiration Month</li></font><br>\n";
		$error = 1;
	}

	if(length($ExpYear) == 0)
	{
		print "<font color=\"red\"><li>Missing Expiration Year</li></font><br>\n";
		$error = 1;
	}
	
	if($error)
	{
		print "<br><font color=\"red\">
		Please supply the missing information and try again, or contact an administrator 
		if you continue to have problems.<br>
		<br>
		<input type=\"button\" value=\"< Back\" onClick=\"javascript:history.go(-1);\"></font><br>
		&nbsp;
		";		
		
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}
	
	my $get_trans_no_stmt = $dbh->prepare("select trans_no_seq.NEXTVAL from dual") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_trans_no_stmt->execute();
	
	my @trans_no_row = $get_trans_no_stmt->fetchrow_array();
	if(!@trans_no_row)
	{
		print "get_trans_no_stmt query failed to retrieve query sequence trans_no_seq for NEXTVAL\n";
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}
	
	$OrderId = $trans_no_row[0];
	$get_trans_no_stmt->finish();

	
	print "
	<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\" width=\"50%\">

	 <form method=\"post\">
	 <input type=\"hidden\" name=\"OrderId\" value=\"$OrderId\">
	 <input type=\"hidden\" name=\"PaymentData\" value=\"$PaymentData\">
	 <input type=\"hidden\" name=\"CardHolderName\" value=\"$CardHolderName\">
	 <input type=\"hidden\" name=\"AmountLowestCurr\" value=\"" . ($AmountLowestCurr*100) . "\">
	 <input type=\"hidden\" name=\"AVS1\" value=\"$AVS1\">
	 <input type=\"hidden\" name=\"AVS2\" value=\"$AVS2\">
	 <input type=\"hidden\" name=\"ExpMonth\" value=\"$ExpMonth\">
	 <input type=\"hidden\" name=\"ExpYear\" value=\"$ExpYear\">

	 <tr><th bgcolor=\"#C0C0C0\" colspan=\"2\">Confirm Transaction</th></tr>
	 <tr><td>USA Technologies Order Id</td><td>$OrderId&nbsp;</td></tr>
	 <tr><td>Client ID</td><td>$ClientId&nbsp;</td></tr>
	 <tr><td>Currency Code</td><td>$Currency&nbsp;</td></tr>
  	 <tr><td>Card Number</td><td>$PaymentData&nbsp;</td></tr>
  	 <tr><td>Name on Card</td><td>$CardHolderName&nbsp;</td></tr>
	 <tr><td>Amount</td><td>\$$AmountLowestCurr (" . ($AmountLowestCurr*100) . " pennies)&nbsp;</td></tr>
	 <tr><td>Street Address</td><td>$AVS1&nbsp;</td></tr>
	 <tr><td>Zip Code</td><td>$AVS2&nbsp;</td></tr>
	 <tr><td>Expiration</td><td>$ExpMonth/$ExpYear&nbsp;</td></tr>
	 <tr><th bgcolor=\"#C0C0C0\" colspan=\"2\"><input type=\"button\" value=\"< Back\" onClick=\"javascript:history.go(-1);\"> <input type=\"submit\" name=\"action\" value=\"Submit Transaction\"></th></tr>
	 </form>
	</table>
	<br>
	";
}
elsif($action eq 'Submit Transaction')
{
	my $send_auth_resp;
	my $auth_success = 99;
	my $auth_resp;

	my $send_deposit_resp;
	my $deposit_success = 99;
	my $deposit_resp;
	
	$log = $log . "Authorizing transaction...\n\n";
	($send_auth_resp) = &pss_Send_Request('OrderCreateAndApproveNoTrack2',
												$ClientId,
												$OrderId,
												$AmountLowestCurr,
												$Currency,
												$PaymentData,
												$CardHolderName,
												$AVS1,
												$AVS2,
												$ExpMonth,
												$ExpYear);
	
	if(defined $send_auth_resp)
	{
		$log = $log . "OrderCreateAndApproveNoTrack2 sent successfully\n";
		$log = $log . "Result = $send_auth_resp\n";
		
		# Parse ResultXML
		($auth_success, $auth_resp) = &pss_Parse_Result($send_auth_resp);
		
		$log = $log . "ResultCode = $auth_success\n";
		$log = $log . "ResultText = $auth_resp\n";
	
		# If We Were Successfully Authorized Try to Settle
		if ($auth_success eq '0') 
		{
			$log = $log . "\nAuthorization Approved!  Now Depositing Transaction...\n";
			
			# Settle Transaction
			my ($send_deposit_resp) = &pss_Send_Request( 'OrderDeposit',
															$ClientId,
															$OrderId,
															$AmountLowestCurr);
			if(defined $send_deposit_resp)
			{
				$log = $log . "OrderDeposit sent successfully\n";
				$log = $log . "Result = $send_deposit_resp\n";
	
				# Parse ResultXML
				($deposit_success,$deposit_resp) = &pss_Parse_Result($send_deposit_resp);
	
				$log = $log . "ResultCode = $deposit_success\n";
				$log = $log . "ResultText = $deposit_resp\n";
	
				if ($deposit_success == 0) 
				{
					$log = $log . "\nDeposit Successfull!  Transaction was completed successfully!...\n";
				}
				else
				{
					$log = $log . "\nDeposit Failed!\n";
				}
			}
			else 
			{
				$log = $log . "\nSend Deposit Failed!\n";
			}
		}
		else
		{
			$log = $log . "\nAuthorization Failed!\n";
		}
	}
	else
	{
		$log = $log . "\nSend Auth Failed!\n";
	}
	
	if($deposit_success == 0)
	{
		my $get_wcp_info_stmt = $quick_pss_dbh->prepare("select pss.wcp_tran_id, etpayment.batchnumber, etpayment.depositamount, etpayment.referencenumber, etpayment.paymenttype, visapayment.approvalcode, visapayment.authresponsecode, visapayment.authresponsetext, visapayment.authsourcecode, visapayment.localtrxdate, visapayment.localtrxtime, visapayment.retrievalrefnum, visapayment.trxsequencenum, visapayment.paysequencenum, visapayment.transactionid, visapayment.validationcode from payman.etpayment etpayment, payman.etorder etorder, payman.visanetpayment visapayment, payman.visanetorder visaorder, quick_pss.pss pss where etpayment.merchantname = etorder.merchantname and etpayment.ordernumber = etorder.ordernumber and etorder.merchantname = visaorder.merchantnumber and etorder.ordernumber = visaorder.ordernumber and visaorder.merchantnumber = visapayment.merchantnumber and visaorder.ordernumber = visapayment.ordernumber and visapayment.ordernumber = pss.wcp_tran_id and visapayment.merchantnumber = :merchant_no and pss.client_tran_id = :trans_no and pss.client_id = :client_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		$get_wcp_info_stmt->bind_param(":merchant_no", $MerchantNum);
		$get_wcp_info_stmt->bind_param(":trans_no", $OrderId);
		$get_wcp_info_stmt->bind_param(":client_name", $ClientId);
		$get_wcp_info_stmt->execute();
		my @wcp_array = $get_wcp_info_stmt->fetchrow_array();
		$get_wcp_info_stmt->finish();
		if(!@wcp_array)
		{
			$log = $log . "\n\nFailed to find WCP Transaction Data for trans_no $OrderId!<br><br>";
			print $log;
			USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
			exit;
		}
		
		my $wcp_id = $wcp_array[0];
		my $batch_num = $wcp_array[1];
		my $deposit_amount = $wcp_array[2];
		my $reference_num = $wcp_array[3];
		my $payment_type = $wcp_array[4];
		my $approval_code = $wcp_array[5];
		my $auth_resp_code = $wcp_array[6];
		my $auth_resp_text = $wcp_array[7];
		my $auth_source_text = $wcp_array[8];
		my $txn_date = $wcp_array[9];
		my $txn_time = $wcp_array[10];
		my $retrievalrefnum = $wcp_array[11];
		my $trxsequencenum = $wcp_array[12];
		my $paysequencenum = $wcp_array[13];
		my $transactionid = $wcp_array[14];
		my $validationcode = $wcp_array[15];
		
		print"
		&nbsp;
		<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\" width=\"50%\">
		  <tr><th bgcolor=\"#C0C0C0\" colspan=\"2\">Order Details</th></tr>
		 
		 <tr><td>USA Technologies Order Id</td><td>$OrderId&nbsp;</td></tr>
		 <tr><td>Client ID</td><td>$ClientId&nbsp;</td></tr>
		 <tr><td>Currency Code</td><td>$Currency&nbsp;</td></tr>
	  	 <tr><td>Card Number</td><td>$PaymentData&nbsp;</td></tr>
	  	 <tr><td>Name on Card</td><td>$CardHolderName&nbsp;</td></tr>
		 <tr><td>Amount</td><td>\$" . ($AmountLowestCurr/100) . " (" . ($AmountLowestCurr) . " pennies)&nbsp;</td></tr>
		 <tr><td>Street Address</td><td>$AVS1&nbsp;</td></tr>
		 <tr><td>Zip Code</td><td>$AVS2&nbsp;</td></tr>
		 <tr><td>Expiration</td><td>$ExpMonth/$ExpYear&nbsp;</td></tr>
		 
		</table>
		&nbsp;
		<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\" width=\"50%\">
		 <tr><th bgcolor=\"#C0C0C0\" colspan=\"2\">Authorization Response</th></tr>

		 <tr><td>Auth Result Code</td><td>$auth_success&nbsp;</td></tr>
		 <tr><td>Auth Result Text</td><td>$auth_resp&nbsp;</td></tr>
		 <tr><td>Deposit Result Code</td><td>$deposit_success&nbsp;</td></tr>
		 <tr><td>Despoit Result Text</td><td>$deposit_resp&nbsp;</td></tr>
		 
		</table>
		&nbsp;
		<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\" width=\"50%\">
		 <tr><th bgcolor=\"#C0C0C0\" colspan=\"2\">Processing Information</th></tr>

		 <tr><td>WCP Order ID</td><td>$wcp_id&nbsp;</td></tr>
		 <tr><td>Marchant Number</td><td>$MerchantNum&nbsp;</td></tr>
		 <tr><td>Batch Number</td><td>$batch_num&nbsp;</td></tr>
		 <tr><td>Deposit Amount</td><td>$deposit_amount&nbsp;</td></tr>
		 <tr><td>Reference Number</td><td>$reference_num&nbsp;</td></tr>
		 <tr><td>Payment Type</td><td>$payment_type&nbsp;</td></tr>
		 <tr><td>Approval Code</td><td>$approval_code&nbsp;</td></tr>
		 <tr><td>Auth Response Code</td><td>$auth_resp_code&nbsp;</td></tr>
		 <tr><td>Auth Response Text</td><td>$auth_resp_text&nbsp;</td></tr>
		 <tr><td>Txn Date</td><td>$txn_date&nbsp;</td></tr>
		 <tr><td>Txn Time</td><td>$txn_time&nbsp;</td></tr>
		 <tr><td>Retrieval Reference Number</td><td>$retrievalrefnum&nbsp;</td></tr>
		 <tr><td>Transaction Sequence Number</td><td>$trxsequencenum&nbsp;</td></tr>
		 <tr><td>Payment Sequence Number</td><td>$paysequencenum&nbsp;</td></tr>
		 <tr><td>Transaction ID</td><td>$transactionid&nbsp;</td></tr>
		 <tr><td>Validation Code</td><td>$validationcode&nbsp;</td></tr>

		</table>
		&nbsp;<br>";
	}
	elsif($auth_success eq '2')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nOperation failed due to communication failure with processor</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '2')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nOperation failed due to communication failure with database</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '3')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nOperation failed due to invalid Authority configuration</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '4')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nOperation failed due to WCP object being in incorrect state for requested operation</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '5')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nOperation failed due to an invalid parameter value (usually Amount)</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '6')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nPayment System unable to parse processor response message due to formatting problem</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '10')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nApprove operation failed due to decline from processor</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '11')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nOperation failed due to an expired instrument</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '12')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nOrderCreate operation failed due to invalid credit card track data</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '20')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nThe requested order was not found</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '30')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nDuplicate Transaction Detected!</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '40')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nAuthroization was denied by the payment processor!</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '50')
	{
		my @return_str = split(/ /, $auth_resp);
		my $PRC = substr($return_str[2], 4);
		my $SRC = substr($return_str[3], 4);
		
		my $PRC_msg = $PRC_HASH{$PRC};
		my $SRC_msg = $SRC_HASH{$SRC};
		
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nThe supplied card data failed validation!<br><br>$PRC_msg<br>$SRC_msg</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '80')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nUser has insufficient privileges to perform requested operation</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '81')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nInvalid user/password specified</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '90')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nPayment System does not support the specified operation</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '98')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nRequest parameters are in an incorrect state</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	elsif($auth_success eq '99')
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nUnable to communicate with the specified Payment System</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
	else
	{
		print "<table><tr><td align=\"left\"><pre><b>Transaction Failed!\n\nUnexpected failure!</b>\n\n\n\n\nFailure Log Follows: \n\n$log\n</pre></td></tr></table>";
	}
}
else
{
	print "
	<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"2\">
	 <tr><th colspan=\"2\" bgcolor=\"#C0C0C0\">Manual Transaction Processing</th></tr>
	 <form method=\"post\">
	 <tr><td>Card Number</td><td><input type=\"text\" name=\"PaymentData\" size=\"17\" maxlength=\"16\"></td></tr>
	 <tr><td>Name on Card</td><td><input type=\"text\" name=\"CardHolderName\" size=\"25\"></td></tr>
	 <tr><td>Amount</td><td><input type=\"text\" name=\"AmountLowestCurr\" size=\"8\"></td></tr>
	 <tr><td>Street Address</td><td><input type=\"text\" name=\"AVS1\" size=\"40\"></td></tr>
	 <tr><td>Zip Code</td><td><input type=\"text\" name=\"AVS2\" size=\"6\" maxlength=\"5\"></td></tr>
	 <tr><td>Expiration</td><td><select name=\"ExpMonth\"><option value=\"01\">01</option><option value=\"02\">02</option><option value=\"03\">03</option><option value=\"04\">04</option><option value=\"05\">05</option><option value=\"06\">06</option><option value=\"07\">07</option><option value=\"08\">08</option><option value=\"09\">09</option><option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option></select> <select name=\"ExpYear\"><option value=\"2004\">2004</option><option value=\"2005\">2005</option><option value=\"2006\">2006</option><option value=\"2007\">2007</option><option value=\"2008\">2008</option><option value=\"2009\">2009</option><option value=\"2010\">2010</option></select></td></tr>
	 <tr><td colspan=\"2\" align=\"center\"><input type=\"reset\" value=\"Clear\"> <input type=\"submit\" name=\"action\" value=\"Next >\"></td></tr>
	</table>
	";
}

$dbh->disconnect;
$quick_pss_dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub pss_Send_Request 
{
	my ($PSSOperation_local,
		$ClientId_local,
		$OrderId_local,
		$AmountLowestCurr_local,
		$Currency_local,
		$PaymentData_local,
		$CardHolderName_local,
		$AVS1_local,
		$AVS2_local,
		$ExpMonth_local,
		$ExpYear_local	) = @_;

	my $post_data = "PSSOperation=$PSSOperation_local&ClientId=$ClientId_local&OrderId=$OrderId_local&AmountLowestCurr=$AmountLowestCurr_local&PaymentData=$PaymentData_local&Currency=$Currency_local&AVS1=$AVS1_local&AVS2=$AVS2_local&CardHolderName=$CardHolderName_local&ExpMonth=$ExpMonth_local&ExpYear=$ExpYear_local";
	#my $url = "http://129.41.132.175/professor/PssRequestServlet";
	my $url = "http://10.24.77.139/professor/PssRequestServlet";
	my $agent_name = "PSSReRIXAgent/1.0";   
	
	$post_data =~ s/ /%20/gi;
	
	$log = $log . "\npost_data = $post_data<br>\n";
	$log = $log . "url = $url<br>\n";
	$log = $log . "agent_name = $agent_name<br>\n";

	my $user_agent = new LWP::UserAgent;
	$user_agent->agent($agent_name);
	$user_agent->timeout(10);

	my $request = new HTTP::Request('GET', $url."?".$post_data);

	$log = $log . "request = $agent_name<br>\n";

	my $response = $user_agent->request($request); 
	$log = $log . "response = $response<br>\n";
   
	if ($response->is_success) 
	{
		$log = $log . "HTTP response SUCCESS<br>\n";
		return $response->as_string();
	} 
	else 
	{
		$log = $log . "HTTP response FAILURE<br>\n";
		return undef;
	}
}

sub pss_Parse_Result 
{
	my(   $ResultXML,
          $ResultCode,
          $ResultText,
          $start_of_result_code,
          $end_of_result_code,
          $start_of_result_text,
          $end_of_result_text);

	$ResultXML = $_[0];
   
	if ($ResultXML =~ "read timeout.*") 
	{
		$ResultCode = "255";
		$ResultText = "Communication Timeout";
	}
	else
	{
		$ResultCode = &find_XML_Field_Data($ResultXML,"ResultCode");
		$ResultText = &find_XML_Field_Data($ResultXML,"ResultText");
	}
   
	return ($ResultCode,$ResultText);
}                         

sub find_XML_Field_Data 
{
	my(   	$XML_Data,
            $XML_Tag,
            $nth_occurence,
            $start_of_XML_tag,
            $start_of_XML_field,
            $end_of_XML_field,
            $XML_field_data,
            $n_count,
            $found_error);

	$XML_Data = $_[0];
	$XML_Tag = $_[1];
	$nth_occurence = $_[2];

	if ($nth_occurence eq "")
	{
		$nth_occurence = 1;
	}

	$n_count = 0;
	$start_of_XML_tag = -1;
	$found_error = 0;

	while (($n_count < $nth_occurence) && ($found_error == 0))
	{
		$start_of_XML_tag += 1;
		$start_of_XML_tag = index($XML_Data,'<'.$XML_Tag.'>',$start_of_XML_tag);
      
		if ($start_of_XML_tag < 0) 
		{
			$found_error = 1;
		}
		else
		{
			$n_count++;
		}
	}

	# Did we find the nth occurence of the tag?
	if ($found_error) 
	{
		$XML_field_data = "";
	}
	else
	{
		$start_of_XML_field = $start_of_XML_tag+length($XML_Tag)+2;
		$end_of_XML_field = index($XML_Data,'</'.$XML_Tag.'>',$start_of_XML_field);

		# Did we find the end tag?
		if ($end_of_XML_field < 0)
		{
			$XML_field_data = "";
		}
		else
		{
			$XML_field_data = substr($XML_Data,$start_of_XML_field,($end_of_XML_field-$start_of_XML_field));
		}
	}

	return $XML_field_data;
}

