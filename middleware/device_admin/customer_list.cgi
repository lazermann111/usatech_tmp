#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
$session->destroy;

print q|<form method="post" name="customer_list">|;

my $pageNav  = OOCGI::PageNavigation->new(15);
my $pageSort = OOCGI::PageSort->new();

my $customer_type = $PARAM{customer_type};
my $customer_name = $PARAM{customer_name};
if(length($customer_type) == 0 && length($customer_name) == 0)
{
   $customer_name = '';
}

my $bind_value;
my $sql = q|SELECT customer_id,
                   customer_name,
                   customer_city,
                   country.country_name,
                   customer_type.customer_type_desc
              from location.customer,
                   customer_type,
                   country
             WHERE customer.customer_type_id = customer_type.customer_type_id (+)
               AND customer.customer_country_cd = country.country_cd (+)|;
if(defined $customer_name)
{
   $sql .= q| AND lower(customer_name) like lower(?) |;
   $customer_name = $query->filter_trim_whitespace($customer_name);
   $bind_value = $customer_name.'%';
}
else
{
   $sql .= q| AND customer.customer_type_id = ? |;
   $bind_value = $customer_type;
}

my @table_cols = (
        ['Name'      , 1],
        ['City'      , 2],
        ['Country'   , 3],
        ['Type'      , 4]
);

my @sql_cols = (
   'LOWER(customer_name)',
   'LOWER(customer_city)',
   'LOWER(country_name)',
   'LOWER(customer_type_desc)'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');
$table->put($rn_table,0,B('Customer List'),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

$sql = $sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql, [ $bind_value ] );

my $row0 = "row0";
my $row1 = "row1";
my $i = 0;
while (my @data = $qo->next)
{
   my $row = $i%2 ? $row1 : $row0;
   $table->insert($rn_table,0,qq|<a href="edit_customer.cgi?customer_id=$data[0]">$data[1]</a>|,"class=$row");
   $table->insert($rn_table,1,$data[2],"class=$row");
   $table->insert($rn_table,2,$data[3],"class=$row");
   $table->insert($rn_table,3,$data[4],"class=$row");
   $rn_table++;
   $i++;
}

display $table;
my $page_table = $pageNav->table;

display $page_table;

print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
