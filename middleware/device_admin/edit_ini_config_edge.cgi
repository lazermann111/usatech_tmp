#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::DBObj;
use USAT::Common::Const;
use URI;
use USAT::DeviceAdmin::Config::Form;
use USAT::DeviceAdmin::Config::Form::Object qw(EDITOR_BASIC EDITOR_ADVANCED);	#just to import EDITOR_* constants
use USAT::DeviceAdmin::Config::Form::Object::TextField qw(RESTRICTION_NUMERIC RESTRICTION_REQUIRED);	#just to import RESTRICTION_* constants
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::Util;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

sub main () {
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	print $user_menu;
	my $file_name = $PARAM{"file_name"};
	my $device_name = $PARAM{"device_name"};

    my $checkbox_section_count = 0;
	if(length($file_name) == 0 || length($device_name) == 0 )
	{
		print "Required parameters not found: file_name, device_name";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
	
	my ($device_id, $device_serial_cd, $device_type_id) = @{USAT::DeviceAdmin::Util::get_device_info_by_name(undef, $device_name)};
	if (!$device_id) {
		print "Device $device_name not found";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
       	exit;
	}

    my $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);

    my @default_config_lines;
    my %default_config_hash;
    my $local_file_name = PKG_DEVICE_CONF__EDGE.$device->property_list_version;
    my $file_data_hex = USAT::DeviceAdmin::Util::blob_select_by_name(undef, $local_file_name);	

    if(!$file_data_hex) {
            print "Default cfg file $local_file_name not found for device $device_name ";
            USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
            exit;
    }

    my $file_data = pack("H*", $file_data_hex);

    $file_data =~ tr/\r//d;  # added to make sure it splits unix and windows data
    chomp($file_data);       # added to make sure to delete last new line character
    @default_config_lines = split(/\n/, $file_data);
    foreach my $default_config_lines ( @default_config_lines ) {
       my ($key, $value) = split(/=/, $default_config_lines, 2);
       $default_config_hash{$key} = $key;
    }
    my $javascript_array_str = qq{<script type="text/javascript">
<!--
    var defCfg = new Array();
    var defCfgNoUse = new Array();
    \n};

    foreach my $item ( sort @default_config_lines ) {
         my ($key, $value) = split(/=/, $item, 2);
         $javascript_array_str .= qq{defCfg[$key] = "$value";\n};
         if(defined $default_config_hash{$key} && $default_config_hash{$key} eq $key) {
            $javascript_array_str .= qq{defCfgNoUse[$key] = "$key";\n};
         }
    }
    $javascript_array_str .= qq{//-->
</script>\n};
    print $javascript_array_str;

	my $action    = $PARAM{"myaction"};
	my $edit_mode = $PARAM{"edit_mode"};
	my $max_param_code_len = 60;
	my $max_param_value_len = 200;

	my @config_array;
	{
		local $@;
		eval { @config_array = USAT::DeviceAdmin::Config::Form::load_device_type($device_type_id); };
		if ($@) {
			print "Error: $@";
			USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
			exit;
		}
	}

	my $title = "$action Configuration - $device_serial_cd - $file_name";

	if (length($edit_mode) > 0)
	{
		$title = "$title ($edit_mode)";
	    $title = "$title, <font color=\"red\">*</font><font size=\"-1\"> denotes required parameter</font>";
	}
	
    my $qo = OOCGI::Query->new(
               'SELECT file_transfer_type_cd
                  FROM device.file_transfer where file_transfer_name = ?',
            { bind => [ $file_name ] });

    my $fttc;
    if(my @arr = $qo->next_array) {
       $fttc = $arr[0];
    } else {
       print "Default cfg file $file_name not found for device $device_name";
       USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
       exit;
    }
	
	$file_data_hex = USAT::DeviceAdmin::Util::blob_select_by_name(undef, $file_name);

	if(!$file_data_hex)
	{
		print "$file_name not found";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}

	$file_data = pack("H*", $file_data_hex);
	print "
	<script language=\"javascript\">
	<!--
	function ValidateForm(f)
	{	
		for (i=0; i<f.elements.length; i++) 
		{
            var key = f.elements[i].name.substring(4);
            if(defCfgNoUse[key] == key) {
			if (f.elements[i].name.substring(0, 4) == 'cfg_')
			{
				f.elements[i].value = f.elements[i].value.replace(/^\\s*|\\s*\$/g,'');

				if (f.elements[i].className == '".RESTRICTION_REQUIRED."' && f.elements[i].value == '')
				{
					alert('Please enter a value for the required parameter ' + key);
                    alert(f.elements[i].name);
					return false;
				}

				if (f.elements[i].className == '".RESTRICTION_NUMERIC."' && (f.elements[i].value == '' || isNaN(f.elements[i].value)))
				{				
					alert('Please enter a numeric value for the required parameter ' + key);
					return false;
				}
			}
			}
		}
		return true;
	}

	function doSubmit(myform, actionValue)
	{
		if (ValidateForm(myform) == false) {
           alert('No Submit!')
           return;
        }
		myform.myaction.value = actionValue;
		myform.submit();
	}
	// -->
	</script>

	<table cellpadding=\"3\" border=\"1\" cellspacing=\"0\" width=\"100%\">
	 <tr>
	  <th align=\"center\" colspan=\"2\" bgcolor=\"#C0C0C0\">$title</th>
	 </tr>

	 <form name=\"myform\" method=post action=\"edit_ini_config_edge_func.cgi\">";

	HIDDEN('edit_mode',   $edit_mode);
	HIDDEN('device_name', $device_name);
	HIDDEN('myaction',    '');
	HIDDEN('action_type', $PARAM{"myaction"});

    $file_data =~ tr/\r//d;  # added to make sure it splits unix and windows data
    chomp($file_data);       # added to make sure to delete last new line character
   	my @config_lines = split(/\n/, $file_data);
	my $config_params = '';
	my $default_textbox_size = 68;
	my %config_hash;

	foreach my $config_line (@config_lines)
	{
		chomp($config_line);
		my ($config_param, $config_value) = split(/=/, $config_line, 2);	

		if (length($config_param) > 0)
		{
			$config_hash{uc($config_param)} = $config_value;
		}
	}

	### generate heading index ###
	my @heading_index;
	foreach my $config_item (@config_array)
	{	
		next unless UNIVERSAL::isa($config_item, 'USAT::DeviceAdmin::Config::Form::Object');	#ignore unknown elements
		if (($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
			&& $config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Header'))
		{
			push @heading_index, $config_item;
		}
	}
	if (@heading_index)
	{
		print "
			<tr>
				<td colspan=\"2\" width=\"100%\" align=\"left\" bgcolor=\"#EEEEEE\">
					<a name=\"___section_index\"></a>
					<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr>
					<td valign=\"top\" bgcolor=\"#EEEEEE\">
						<b>Section Index:</b>&nbsp;&nbsp;&nbsp;
					</td>
						<td bgcolor=\"#EEEEEE\">";
		print "
					<a href=\"#".URI->new($_->parameter_code)->as_string."\">".$_->parameter_code."</a><br>" foreach sort { lc($a->parameter_code) cmp lc($b->parameter_code) } @heading_index;
		print "
				</td>
				</tr>
				</table>
			</td>
		</tr>";
	}

	### generated form elements ###
    print html_form_submit_buttons($device_name,1); # 1 radio button at the top
    print qq{<tr><td colspan="2" align="center"><input type="checkbox" onClick="select_all(9999);">Set All To Defaults<br><font color="red">* Checkboxes are used to set default values</font></tr></td>};
	foreach my $config_item (@config_array)
	{	
        my $parameter_name = $config_item->parameter_name;
        my $parameter_code = $config_item->parameter_code;
		next unless UNIVERSAL::isa($config_item, 'USAT::DeviceAdmin::Config::Form::Object');	#ignore unknown elements

		if ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Header'))
		{
			if ($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
			{
                $checkbox_section_count++;
				print qq{
					<tr>
						<td colspan="2" width="100%" align="center" bgcolor="#C0C0C0"><b><input type="checkbox" id=section_$checkbox_section_count onClick="select_all($checkbox_section_count);">}.q{ <font color="#0000CC"><a name="}.URI->new($parameter_code)->as_string.'">'.$parameter_code.q{</a></b></font>&nbsp;&nbsp;&nbsp;<a href="#___section_index"><img src="/icons/small/back.gif" border="0"></td>
					</tr>};
		    }
		}
		else
		{
            if(!defined  $config_hash{$parameter_code} && $fttc eq PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE ) {
               next; # CUSTOM data has fewer elements, so skip the unused ones.
            }
        	$config_params = "$config_params|||".$parameter_code;

			if ($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
			{
				print qq{
					<tr>
					  <td width="50%"><input type="checkbox" onclick="toggle($parameter_code,$checkbox_section_count)"; name="chkb_$parameter_code}.'_'.qq{$checkbox_section_count" value="$parameter_code"> <b>}.(defined $parameter_name ? $parameter_name . ' [' . $parameter_code . ']' : $parameter_code) . (defined $config_item->restriction ? qq{<font color="red">*</font>} : "") . "</b>&nbsp;</td>
					  <td width=\"50%\">";
				 if ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::TextField'))
				 {
				 	my $param_value = $config_hash{$parameter_code};
					if(defined $config_item->read_only && $config_item->read_only ne '') {
					print "
<input type=\"text\" name=\"cfg_".$parameter_code."\" value=\"" . (length($param_value) > 0 ? $param_value : $config_item->default) .  "\" style=\"border:1px solid #000; background-color:#cc9999;color:#000000;\" size=\"" . (defined $config_item->maxlength ? ($config_item->maxlength + 7 > $default_textbox_size ? $default_textbox_size : $config_item->maxlength + 7) : $default_textbox_size) . "\" maxlength=\"" . (defined $config_item->maxlength ? $config_item->maxlength : $max_param_value_len) . "\"" . (defined $config_item->restriction ? " class=\"".$config_item->restriction."\"" : "") .(defined $config_item->read_only ? $config_item->read_only ? 'readonly' : '': ''). ">";
                    } else {
					print "
						<input type=\"text\" name=\"cfg_".$parameter_code."\" value=\"" . (length($param_value) > 0 ? $param_value : $config_item->default) .  "\" size=\"" . (defined $config_item->maxlength ? ($config_item->maxlength + 7 > $default_textbox_size ? $default_textbox_size : $config_item->maxlength + 7) : $default_textbox_size) . "\" maxlength=\"" . (defined $config_item->maxlength ? $config_item->maxlength : $max_param_value_len) . "\"" . (defined $config_item->restriction ? " class=\"".$config_item->restriction."\"" : "") . ">";
                    }
				 }
				 elsif ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Radio'))
				 {
					my @choices = @{$config_item->choices};
					foreach my $choice (@choices)
					{
						print "
							<input type=\"radio\" name=\"cfg_".$parameter_code."\" value=\"$choice\"" . (length($config_hash{uc($parameter_code)}) > 0 ? (uc($config_hash{uc($parameter_code)}) eq uc($choice) ? ' checked' : '') : (uc($config_item->default) eq uc($choice) ? ' checked' : '')) . ">$choice</input>";
					}
				 }
				 elsif ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Select'))
				 {
					print "
						<select name=\"cfg_".$parameter_code."\">";

					my @choices = @{$config_item->choices};
					foreach my $choice (@choices)
					{
						print "   
							<option value=\"$choice\"" . (length($config_hash{uc($parameter_code)}) > 0 ? (uc($config_hash{uc($parameter_code)}) eq uc($choice) ? ' selected' : '') : (uc($config_item->default) eq uc($choice) ? ' selected' : '')) . ">$choice</option>";
					}

					print "
						</select>";
				 }				 
				 else
				 {
					print "
						<input type=\"text\" name=\"cfg_".$parameter_code."\" value=\"$config_hash{uc($parameter_code)}\" size=\"$default_textbox_size\" maxlength=\"$max_param_value_len\">";
				 }
                 if(!defined $default_config_hash{$parameter_code}) {
                     NBSP(5);
                     B('Unsupported Property','red'); 
                 }

				 print "
					  </td>
					</tr>
					<tr>
					  <td colspan=\"2\" width=\"100%\" bgcolor=\"#EEEEEE\"><font size=\"-1\">".$config_item->description."</font>&nbsp;</td>
					</tr>";
			}
			else
			{
				print "
					<input type=\"hidden\" name=\"cfg_".$parameter_code."\" value=\"" . (length($config_hash{uc($parameter_code)}) > 0 ? $config_hash{uc($parameter_code)} : $config_item->default) . "\">";
			}

			delete($config_hash{uc($parameter_code)});
		}
	}

	foreach my $config_line (@config_lines)
	{
		my ($config_param, $config_value) = split(/=/, $config_line, 2);
		if (defined $config_hash{uc($config_param)})
		{
			$config_params = "$config_params|||$config_param";	
			HIDDEN("cfg_".$config_param, $config_value);
		}
	}

	$config_params = substr($config_params, 3);
	HIDDEN("config_params", $config_params); #??? No need

	print html_form_submit_buttons($device_name,0); # 0 no radio button at the bottom

	print qq{
	 <tr>
	  <th align=\"center\" colspan=\"2\" bgcolor=\"#C0C0C0\">$title</th>
	 </tr>

	 </form>
	</table>
    <script language=\"javascript\">
    <!--
    var theForm = document.myform;
    var checked_box_array = new Array();
    for (i=0; i<theForm.elements.length; i++) {
         checked_box_array[i] = false;
    }

    function toggle(val,section) {
       if(checked_box_array[val] == true) {
          checked_box_array[val] = false;
       } else {
          checked_box_array[val] = true;
       }
       var text_name = 'cfg_'+val;
       var temp_value = document.getElementsByName(text_name)[0].value;
       document.getElementsByName(text_name)[0].value = defCfg[val];
       defCfg[val] = temp_value;
    }
       
    function select_all(checkbox_section) {
        if(checkbox_section == 9999) {
           for (i=0; i<theForm.elements.length; i++) {
               if (theForm.elements[i].value >= 0) {
                  for(j = 0; j <= $checkbox_section_count; j++) {
                      var val = theForm.elements[i].value;
                      var name = 'chkb_'+val+'_'+j
                      if (theForm.elements[i].name==name) {
                        if(checked_box_array[i] == true) {
                           theForm.elements[i].checked = false;
                        } else {
                           theForm.elements[i].checked = true;
                        }
                        if(checked_box_array[i] == true) {
                           checked_box_array[i] = false;
                        } else {
                           checked_box_array[i] = true;
                        }
                        var text_name = 'cfg_'+val;
                        var temp_value = document.getElementsByName(text_name)[0].value;
                        if(defCfg[val] === undefined ) {
                           document.getElementsByName(text_name)[0].value = '';
                        } else {
                           document.getElementsByName(text_name)[0].value = defCfg[val];
                        }
                        defCfg[val] = temp_value;
                     }
                  }
               }
           }
        } else {
           for (i=0; i<theForm.elements.length; i++) {
               if (theForm.elements[i].value >= 0) {
                  var val = theForm.elements[i].value;
                  var name = 'chkb_'+val+'_'+checkbox_section;
                  if (theForm.elements[i].name==name) {
                     if(theForm.elements[i].checked == true) {
                        theForm.elements[i].checked = false;
                     } else {
                        theForm.elements[i].checked = true;
                     }
                     if(checked_box_array[i] == true) {
                        checked_box_array[i] = false;
                     } else {
                        checked_box_array[i] = true;
                     }
                     var text_name = 'cfg_'+val;
                     var temp_value = document.getElementsByName(text_name)[0].value;
                     if(defCfg[val] === undefined ) {
                        document.getElementsByName(text_name)[0].value = '';
                     } else {
                        document.getElementsByName(text_name)[0].value = defCfg[val];
                     }
                     defCfg[val] = temp_value;
                  }
               }
           }
        }
    }
    //-->
    </script>
	};

	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub html_form_submit_buttons ($) {
	my $device_name = shift;
    my $top_flag  = shift;
	my $str = qq{
	 <tr>
	  <td align="center" colspan="2">

			<table cellspacing="0" cellpadding="5" border="0">
			 <tr>
			  <td>
			   <input type="button" value="Save and Send" onClick="doSubmit(this.form, 'Save and Send');">
			  </td>
			  <td>
			   <input type="button" value="Cancel" onClick="document.location = '/profile.cgi?ev_number=$device_name';">
			  </td>
			 </tr>
			</table>
	  </td>
	 </tr>
      };

     return $str;
}

main();
