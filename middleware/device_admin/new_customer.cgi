#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $customer_name = $PARAM{"customer_name"};

if(length($customer_name) == 0)
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	print "Required parameter not found: customer_name";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit();
}

my $get_customer_stmt = $dbh->prepare("select customer_id from location.customer where customer_name = :customer_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_customer_stmt->bind_param(":customer_name", $customer_name);
$get_customer_stmt->execute();
my @customer_data = $get_customer_stmt->fetchrow_array();
$get_customer_stmt->finish();
if(@customer_data)
{
	$dbh->disconnect;
	print CGI->redirect("/edit_customer.cgi?customer_id=$customer_data[0]");
	exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">New customer</th>
 </tr>
 <form method=\"post\" action=\"new_customer_func.cgi\">
 <input type=\"hidden\" name=\"customer_id\">
 <tr>
  <td nowrap>Name</td>
  <td width=\"50%\">$customer_name<input type=\"hidden\" name=\"name\" value=\"$customer_name\" size=\"40\"></td>
  <td nowrap>Type</td>
  <td width=\"50%\">
   <select name=\"customer_type_id\">
";
my $customer_type_id;
my $customer_types_stmt = $dbh->prepare("select customer_type_id, customer_type_desc from customer_type order by customer_type_desc") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$customer_types_stmt->execute();
while (my @data = $customer_types_stmt->fetchrow_array()) 
{
	print "   <option value=\"$data[0]\" " . ($data[0] eq $customer_type_id ? 'selected' : '&nbsp;') . ">$data[1]</option>\n";
}
$customer_types_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>Address Line 1</td>
  <td width=\"50%\"><input type=\"text\" name=\"addr1\" size=\"40\" maxlength=\"60\"></td>
  <td nowrap>City</td>
  <td width=\"50%\"><input type=\"text\" name=\"city\" size=\"40\" maxlength=\"28\"></td>
 </tr>
 <tr>
  <td nowrap>Address Line 2</td>
  <td width=\"50%\"><input type=\"text\" name=\"addr2\" size=\"40\" maxlength=\"60\"></td>
  <td nowrap>State</td>
  <td width=\"50%\">
   <select name=\"state\">
";
    
my $states_stmt = $dbh->prepare("select state_cd, state_name from state order by state_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$states_stmt->execute();
my $state;
while (my @data = $states_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" " . ($data[0] eq $state ? 'selected' : '&nbsp;') . ">$data[1]</option>\n";
}
$states_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>County</td>
  <td width=\"50%\"><input type=\"text\" name=\"county\" size=\"40\" maxlength=\"28\"></td>
  <td nowrap>Postal Code</td>
  <td width=\"50%\"><input type=\"text\" name=\"postal_cd\" size=\"40\" maxlength=\"10\"></td>
 </tr>
 <tr>
  <td nowrap>Country</td>
  <td width=\"50%\" colspan=\"3\">
   <select name=\"country\">
";
    
my $country_list_stmt = $dbh->prepare("select country_cd, country_name from country order by country_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$country_list_stmt->execute();
my $country;
while (my @data = $country_list_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" " . ($data[0] eq $country ? 'selected' : '&nbsp;') . ">$data[1]</option>\n";
}
$country_list_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">eSuds Report Website Options (all fields optional)</th>
 </tr>
 <tr>
  <td nowrap>User Name (i.e. e-mail)</td>
  <td width=\"50%\"><input type=\"text\" name=\"app_user_name\" size=\"40\" maxlength=\"60\"></td>
  <td nowrap>E-mail Address</td>
  <td width=\"50%\"><input type=\"text\" name=\"email_addr\" size=\"40\" maxlength=\"60\"></td>
 </tr>
 <tr>
  <td nowrap>New Sub-domain for user</td>
  <td width=\"50%\" colspan=\"3\"><input type=\"text\" name=\"subdomain\" size=\"40\" maxlength=\"60\"></td>
</tr>
 <tr>
  <td colspan=\"4\" align=\"center\">
   <input type=\"submit\" name=\"action\" value=\"Save\">
  </td>
 </tr>
 </form>
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
