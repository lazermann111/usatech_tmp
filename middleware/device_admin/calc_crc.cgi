#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use Evend::Misc::crc;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();

USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

my $hexstr = $PARAM{"hexstr"};

if($hexstr)
{
	my $bytes = pack("H*", $hexstr);
	my $crc = &getcrc16s($bytes);
	print "&nbsp;<br>$hexstr CRC = \"" . unpack("H*", $crc) . "\"<br><br>";
}

print "<hr width=\"100%\" noshade size=\"2\" color=\"#000000\"><br>\n";

print "<form method=get>\n";
print "Bytes in Hex: <input type=\"text\" name=\"hexstr\"> <input type=\"submit\" value=\"Calculate CRC\"><br>&nbsp;\n";

USAT::DeviceAdmin::UI::DAHeader->printFile("footer.html");
sub getcrc16s
{
    my ($message)=@_;
    return pack "C*", Evend::Misc::crc::CalcCrc($message, length $message);
}
