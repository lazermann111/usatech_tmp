#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use OOCGI::Query;
use OOCGI::OOCGI;
use OOCGI::NTable;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print q|<form method="post" name="device_list" action="device_list.cgi">|;

my $pageNav  = OOCGI::PageNavigation->new(25);

my $tran_id     = $PARAM{tran_id};
my $device_type = $PARAM{tran_after_date};
my $location    = $PARAM{tran_before_date};

my $sql;
my @params = ();

$sql = q{ SELECT tran_id, 
                 to_char( tran_start_ts, 'MM/DD/YYYY HH24:MI:SS')
            FROM tran
           WHERE tran_id > 0 };


if($PARAM{tran_id}) {
   $sql .= ' AND tran_id like ? ';
   push(@params, "\%$PARAM{tran_id}");
} 
if($PARAM{tran_after_date}) {
   $sql .= q{ AND tran_start_ts > to_date(?, 'MM/DD/YYYY') };
   push(@params, "$PARAM{tran_after_date}");
} 
if($PARAM{tran_before_date}) {
   $sql .= q{ AND tran_start_ts < to_date(?, 'MM/DD/YYYY') };
   push(@params, "$PARAM{tran_before_date}");
} 

my @table_cols = (
    ['Tran ID',  , -2],
    ['Start TS'  , -1]
);

my @sql_cols = (
    'tran_id',
    'tran_start_ts'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;

my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

$table->put($rn_table,0,B('Tran List'),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

$sql = $sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo = $pageNav->get_result_obj($sql, \@params );

my $row0 = "row0";
my $row1 = "row1";
my $i = 0;
while (@data = $qo->next) 
{
    my $row = $i%2 ? $row1 : $row0;
    my $str1 = qq|<a href="tran.cgi?tran_id=$data[0]">$data[0]</a>|;
    $table->put($rn_table,0,$str1,"class=$row");
    $table->put($rn_table,1,$data[1],"class=$row");
    
    $rn_table++;
    $i++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
