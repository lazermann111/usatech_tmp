#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use OOCGI::NTable;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my $pageNav  = OOCGI::PageNavigation->new(25);

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $authority_id = $PARAM{"authority_id"};

my $authority_info_sql = q{
	SELECT authority.authority_id,
           authority.authority_name,
           authority_type.authority_type_name,
           authority_type.authority_type_desc, 
	       authority_type.authority_type_id,
           to_char(authority.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	       to_char(authority.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM') 
	  FROM authority.authority, authority_type 
	 WHERE authority.authority_type_id = authority_type.authority_type_id 
	   AND authority.authority_id = ?
};

my @authority_info = OOCGI::Query->new($authority_info_sql, { bind => $authority_id } )->next_array;

print qq{
<table cellspacing="0" cellpadding="2" border=1 width="100%">

 <tr>
  <th colspan="4" bgcolor="#C0C0C0">Authority</th>
 </tr>
 <tr>
 <tr>
  <td>ID</td><td>$authority_info[0]</td>
  <td>Name</td><td>$authority_info[1]</td>
 </tr>
 </tr>
 <tr>
  <td>Type</td><td>$authority_info[2]</td>
  <td>Description</td><td>$authority_info[3]</td>
 </tr>
 <tr>
  <td>Created</td><td>$authority_info[5]</td>
  <td>Last Updated</td><td>$authority_info[6]</td>
 </tr>
</table>
};

my $authority_gateway_list_sql = q{
	SELECT authority_gateway.authority_gateway_id,
           authority_gateway.authority_gateway_name,
           authority_gateway.authority_gateway_addr, 
	       authority_gateway.authority_gateway_port,
           authority_gateway.authority_gateway_priority 
	  FROM authority_gateway, authority_type_gateway 
	 WHERE authority_gateway.authority_gateway_id = authority_type_gateway.authority_gateway_id 
	   AND authority_type_gateway.authority_type_id = ?
	 ORDER BY authority_gateway.authority_gateway_priority
};

my $qo = OOCGI::Query->new($authority_gateway_list_sql, { bind => $authority_info[4] }); 

print q{
<hr width="100%" noshade size="2" color="#000000">
<table border="1" width="100%" cellpadding="2" cellspacing="0">
 <tr>
  <th colspan="6" bgcolor="#C0C0C0">Authority Gateways</th>
 </tr>
 <tr>
  <th>ID</th>
  <th>Name</th>
  <th>Address</th>
  <th>Priority</th>
 </tr>
};

while(my @authority_gateway_info = $qo->next_array)
{
	print qq{
	     <tr>
    	 <td>$authority_gateway_info[0]</td>
	     <td><a href="authority_gateway.cgi?authority_gateway_id=$authority_gateway_info[0]">$authority_gateway_info[1]</a></td>
	     <td>$authority_gateway_info[2]:$authority_gateway_info[3]</td>
	     <td>$authority_gateway_info[4]</td>
    	</tr>
	};
}

print "
</table>
";

#          merchant.merchant_desc,
#          merchant.merchant_bus_name, 
#       merchant.authority_id,
#          merchant.created_ts,
#          merchant.last_updated_ts 

my $merchant_list_sql = q{
	SELECT merchant.merchant_id,
           merchant.merchant_cd,
           merchant.merchant_name
	  FROM merchant 
	 WHERE authority_id = ?
};

my @table_cols = (
    ['ID'      ,  2],
    ['Code'    , -1],
    ['Name'    ,  3]
);

my @sql_cols = ( 
    'merchant.merchant_id',
    'merchant.merchant_cd',
    'merchant.merchant_name'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');
$table->put($rn_table,0,B('Merchants'),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

my $sql = $merchant_list_sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql, [  $authority_id ] );

while(my @merchant_info = $qo->next_array)
{
    $table->put($rn_table,0,qq{$merchant_info[0]});
    $table->put($rn_table,1,qq{<a href="merchant.cgi?merchant_id=$merchant_info[0]">$merchant_info[1]</a>});
    $table->put($rn_table,2,qq{$merchant_info[2]});

    $rn_table++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
