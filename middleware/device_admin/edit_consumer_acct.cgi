#!/usr/local/USAT/bin/perl

use strict;
use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::USAPopups;
use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::Query;
use USAT::DeviceAdmin::UI::DAHeader;

sub main {
my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my %PARAM = $query->Vars();

my $consumer_acct_id = $query->param('consumer_acct_id');

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

print q|
<script>
  <!--
        function confirmSubmit()
        {
                var agree=confirm("Please Confirm Your Action!");
                if (agree)
                        return true ;
                else
                        return false ;
        }
  // -->
  </script>
|;

if($query->param('action') eq 'Clear Holds') {
   my @auth_id = $query->param('chkbox');
   foreach my $auth_id ( @auth_id ) {
      OOCGI::Query->delete('consumer_acct_auth_hold', { consumer_acct_id => $consumer_acct_id, auth_id => $auth_id });
   } 
}

if(length($consumer_acct_id) == 0)
{
	print "Required Parameter Not Found: consumer_acct_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}


my $account = USAT::DeviceAdmin::DBObj::Consumer_acct->new($consumer_acct_id);

if($PARAM{userOP} eq 'Toggle') {
   my $yn_flag = $account->consumer_acct_active_yn_flag;

   if($yn_flag eq 'Y') {
      $account->consumer_acct_active_yn_flag('N');
   } else {
      $account->consumer_acct_active_yn_flag('Y');
   }
   $account = $account->update;
}
if($query->param('action') eq 'Save') {
   $account->location_id($PARAM{location_id});
   my $obj = $account->update($query);
  
   if(defined $obj) {
      $account = $obj;
   } else {
      print B('ERROR No Update :','red',3),$account->errstr,'<br>';
   }
}
if($query->param('action') eq 'Deactivate') {
   $account->consumer_acct_deactivation_ts(OOCGI::Query->SYSDATE);
   my $obj = $account->update;
   if(defined $obj) {
      $account = $obj;
   } else {
      print B('ERROR No Update :','red',3),$account->errstr,'<br>';
   }
}

if(not defined $account->ID)
{
        print "Consumer_acc $consumer_acct_id not found!\n";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
}

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
my $table_rn = 0;

print q|<form method="post">|;

my $style1 = qq|style="background-color:#C0C0C0; text-align:center;"'|;
my $rstyle = qq|style="text-align:right;"'|;
$table->put($table_rn,0,B("Edit Consumer Account - " . $account->consumer_acct_id,2),"colspan=4 $style1");
$table_rn++;


$table->put($table_rn,0,'Account ID');
$table->put($table_rn,1, $account->consumer_acct_id);
$table->put($table_rn,2,'Card Code');
$table->put($table_rn,3, $account->consumer_acct_cd);

$table_rn++;

$table->put($table_rn,0,'Balance');
$table->put($table_rn,1, $account->Text('consumer_acct_balance', { size => 20}));
$table->put($table_rn,2,'Active');
#$table->put($table_rn,3, ($account->consumer_acct_active_yn_flag eq 'Y' ? B('Yes','green') : B('No','red')));
$table->put($table_rn,3, $account->colored_yn_flag.' '.SUBMIT('userOP','Toggle'));
$table_rn++;

$table->put($table_rn,0,'Activation Date');
$table->put($table_rn,1, $account->consumer_acct_activation_ts);
$table->put($table_rn,2,'Deactivation Date');
$table->put($table_rn,3, $account->consumer_acct_deactivation_ts);
$table_rn++;

$DB::single=1;
$table->put($table_rn,0,'Confirmation Code');
$table->put($table_rn,1, $account->Text('consumer_acct_confirmation_cd'));
$table->put($table_rn,2,'Balance Refresh Amount');
$table->put($table_rn,3, $account->Text('consumer_acct_balref_amt'));
$table_rn++;

$table->put($table_rn,0,'Balance Refresh Period');
$table->put($table_rn,1, $account->Text('consumer_acct_balref_period_hr'));
$table->put($table_rn,2,'Last Balance Refresh Date');
$table->put($table_rn,3, $account->consumer_acct_last_balref_ts);
$table_rn++;

my ($fmt_name, $fmt_desc);

if(defined $account->consumer_acct_fmt_id)
{
	my $consumer_acct_fmt_sql = q|
		select caf.consumer_acct_fmt_id, caf.consumer_acct_fmt_regex, caf.consumer_acct_fmt_regex_bref, caf.consumer_acct_fmt_name, caf.consumer_acct_fmt_desc
    	from pss.consumer_acct_fmt caf
    	where caf.consumer_acct_fmt_id = ?|;
    	
	my $consumer_acct_fmt_query = OOCGI::Query->new($consumer_acct_fmt_sql, undef, [$account->consumer_acct_fmt_id]);
	my %consumer_acct_fmt_hash = $consumer_acct_fmt_query->next_hash;
	$fmt_name = $consumer_acct_fmt_hash{consumer_acct_fmt_name};
	$fmt_desc = $consumer_acct_fmt_hash{consumer_acct_fmt_desc};
}

$table->put($table_rn,0,'Card Type');
$table->put($table_rn,1, $fmt_name);
$table->put($table_rn,2,'Card Type Description');
$table->put($table_rn,3, $fmt_desc);
$table_rn++;

my $loc_popup = USAT::DeviceAdmin::UI::USAPopups->pop_location();
$loc_popup->default($account->location_id);
$table->put($table_rn,0,'Location');
$table->put($table_rn,1, $loc_popup, 'colspan=3');
$table_rn++;

display $table;

my $str;
$str = q|&nbsp;<br><input type="submit" name="action" value="Save" onClick="return confirmSubmit()">|;
if(!defined($account->consumer_acct_deactivation_ts) || $account->consumer_acct_deactivation_ts eq '') {
   $str.= q| <input type="submit" name="action" value="Deactivate" onClick="return confirmSubmit()"><br>&nbsp;|;
}

print $str;

HIDDEN("consumer_acct_id",$consumer_acct_id);

if(defined $account->consumer_acct_fmt_id && $account->consumer_acct_fmt_id == 1)
{
	my $at_rn = 0;
	my $action_table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
	$action_table->put($at_rn++,0,B("Maintenance Card Actions",2)," colspan=2 $style1");

	my $permission_action_sql = q|
		select pa.permission_action_id, a.action_id, a.action_name, a.action_desc
		from pss.consumer_acct_permission cap, pss.permission_action pa, device.action a
		where cap.permission_action_id = pa.permission_action_id
		and pa.action_id = a.action_id
		and cap.consumer_acct_id = ?
		order by cap.consumer_acct_permission_order|;
	
	my $action_param_sql = q|
		select ap.action_param_id, ap.action_param_cd, ap.action_param_name, ap.action_param_desc
		from pss.permission_action_param pap, device.action_param ap
		where pap.action_param_id = ap.action_param_id
		and pap.permission_action_id = ?
		order by pap.permission_action_param_order|;
		
	my $permission_action_id;
		
	my $permission_action_query = OOCGI::Query->new($permission_action_sql, undef, [$consumer_acct_id]);
	if ($permission_action_query->row_count > 0)
	{
		my %hash = $permission_action_query->next_hash;
		# we're making the assumption that there's only 1 permission_action linked to this consumer_acct
		# the database is designed to support more than 1, but the protocol does not
		# until we need more, we'll just use the first listed
		
		$permission_action_id = $hash{permission_action_id};
		
		$action_table->put($at_rn,0,$hash{action_name});
		$action_table->put($at_rn,1,$hash{action_desc});
		$at_rn++;
			
		my $action_param_query = OOCGI::Query->new($action_param_sql, undef, [$hash{permission_action_id}]);
		if($action_param_query->row_count  > 0)
		{
			while(my %hash2 = $action_param_query->next_hash)
			{
				$action_table->put($at_rn++,0,"- " . $hash2{action_param_name}, "colspan=2");
			}
		}
		else
		{
			$action_table->put($at_rn++,0,"- No optional parameters", "colspan=2");
		}
	}
	else
	{
		$action_table->put($at_rn++,0,B("No actions defined for this card!"));
	}
	
	display $action_table;
	
	print qq|&nbsp;<br><input type="button" value="Change Card Actions" onClick="javascript:window.location='edit_consumer_acct_permission.cgi?consumer_acct_id=$consumer_acct_id&permission_action_id=$permission_action_id'"><br>&nbsp;|;
}

my $sql = q|
   SELECT auth.auth_id,
          auth.auth_amt,
          to_char(consumer_acct_auth_hold.created_ts, 'MM/DD/YYYY HH24:MI:SS') created_ts,
          device.device_serial_cd,
          location.location_name,
          device.device_id,
          location.location_id
     FROM consumer_acct_auth_hold,
          auth,
          tran,
          pos_pta,
          pos,
          device,
          location.location
    WHERE tran.consumer_acct_id =  ?
      AND auth.auth_id = consumer_acct_auth_hold.auth_id
      AND auth.tran_id = tran.tran_id
      AND tran.pos_pta_id = pos_pta.pos_pta_id
      AND pos_pta.pos_id = pos.pos_id
      AND pos.device_id = device.device_id
      AND pos.location_id = location.location_id
    ORDER BY consumer_acct_auth_hold.created_ts desc
|;

my $qo = OOCGI::Query->new($sql, {data_as_hash => 1, bind => $consumer_acct_id});

my $table2 = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

my $table2_rn = 0;
$table2->put($table2_rn,0,B('Temporary Holds',2),"colspan=6 $style1");
$table2_rn++;
$table2->put($table2_rn,0,'Tran Detail ID',$style1);
$table2->put($table2_rn,1,'Hold Amount',$style1);
$table2->put($table2_rn,2,'Created Date',$style1);
$table2->put($table2_rn,3,'Device',$style1);
$table2->put($table2_rn,4,'Location',$style1);
$str = q|<input type="submit" name="action" value="Clear Holds" onClick="return confirmSubmit()">|;
$table2->put($table2_rn,5, $str,$style1);
$table2_rn++;
while(my %hs = $qo->next) {
   $table2->put($table2_rn,0,$hs{auth_id},$rstyle); $table2->linkto($table2_rn,0,"auth.cgi?auth_id=$hs{auth_id}");
   $table2->put($table2_rn,1,$hs{auth_amt},$rstyle);
   $table2->put($table2_rn,2,$hs{created_ts},$rstyle);
   $table2->put($table2_rn,3,$hs{device_serial_cd},$rstyle); $table2->linkto($table2_rn,3,"profile.cgi?device_id=$hs{device_id}");
   $table2->put($table2_rn,4,$hs{location_name}); $table2->linkto($table2_rn,4,"edit_location.cgi?location_id=$hs{location_id}");
   my $checkbox = OOCGI::Checkbox->new(name => 'chkbox',
                                 label => $hs{auth_id},
                                 value => $hs{auth_id});
   $table2->put($table2_rn, 5, $checkbox);
   $table2_rn++;
}

display $table2;

print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
$query->DESTROY(1);
}
main();

