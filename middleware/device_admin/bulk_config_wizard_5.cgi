#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::App::DeviceAdmin::Const qw(:globals :globals_hash);
use USAT::DeviceAdmin::UI::DAHeader;
require Text::CSV;
use USAT::DeviceAdmin::Util;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
$session->destroy;

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print "<br><br><font color=\"red\">Undefined Action!</font><br><br>";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print "<br><br><font color=\"red\">No devices selected!</font><br><br>";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};

my $map_name = "g4_generic_map.csv";
my $file_name = $PARAM{"file_name"};

if(not defined $file_name)
{
	print "Required parameter not found: file_name!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $raw_map_data = USAT::DeviceAdmin::Util::load_file($map_name, $dbh);
if(not defined $raw_map_data)
{
	print "Failed to load $map_name!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $file_data = USAT::DeviceAdmin::Util::load_file($file_name, $dbh);
if(not defined $file_data)
{
        print "$file_name not found";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
}

my @map_lines = ();
if($raw_map_data =~ /\r\n/)
{
   @map_lines = split /\r\n/, $raw_map_data;
} else {
   @map_lines = split /\n/, $raw_map_data;
}
$file_data = pack("H*", $file_data);

my $edit_mode = $PARAM{"edit_mode"};

my $cust_loc_str = USAT::DeviceAdmin::Util->customer_location_str(\%PARAM);
print "
<form method=\"post\" action=\"bulk_config_wizard_6.cgi\">
<table cellpadding=\"3\" border=\"1\" cellspacing=\"0\" width=\"100%\">


 <tr>
  <th align=\"center\" colspan=\"3\" bgcolor=\"#C0C0C0\">Device Configuration Wizard - Page 5: Parameter Selection</th>
 </tr>
 <tr>
  <th align=\"center\" colspan=\"3\" bgcolor=\"#C0C0C0\">$cust_loc_str</th>
 </tr>

 <tr>
  <th style=\"font-size: 10pt;\" align=\"center\" bgcolor=\"#EEEEEE\" nowrap>Check to<br>Change</th>
  <td style=\"font-size: 10pt;\" align=\"center\" colspan=\"2\" bgcolor=\"#EEEEEE\">
   <b>Parameter to Change on All Devices</b>
  </td>
 </tr>

";

my $location_counter = 1;
my $csv = Text::CSV->new;
foreach my $line (@map_lines)
{
	if ($csv->parse($line)) 
	{
	   my	($name,$start_addr,$size,$align,$pad_with,$data_mode,$display,$description,$choices,$default_choice,$allow_change) = $csv->fields;
	
		my $location_data = substr($file_data,$start_addr,$size);
		
		# G4/G5 uses a 2 byte per address scheme, ugh...
		my $eerom_location = unpack("H*",chr($start_addr/2));
		
		if ($start_addr eq GX_MAP__AUTH_MODE)
		{
			# disable local auth
			$choices = 'N';
			$description .= ' <font color=red>Local Authorization has been disabled.</font>';
		}
		elsif ($start_addr eq GX_MAP__MDB_INVENTORY_FORMAT)
		{
			# disable BCD Inventory Format
			$choices = 'Y';
			$description .= ' <font color=red>BCD Inventory Format has been disabled.</font>';
		}
		
		my $field_length = $size + 2;  # added 2 for visibility
		
		if($data_mode eq 'H')
		{
			$location_data = unpack("H*", $location_data);
			$field_length = $size*2 + 2; # added 2 for visibility
		}
		
		if($align eq 'C')
		{
			$location_data =~ s/$pad_with+$//;		# trim spaces on right
			$location_data =~ s/^$pad_with+//;		# trim spaces on left
		}
		elsif($align eq 'R')
		{
			$location_data =~ s/^$pad_with+//;		# trim spaces on left
		}
		elsif($align eq 'L')
		{
			$location_data =~ s/$pad_with+$//;		# trim spaces on right
		}
		
		my $end_byte = $start_addr+$size-1;
		
		print "<!-- BEGIN $location_counter - Address $start_addr: $name -->\n";
		
		HIDDEN($start_addr.'_counter',        $location_counter);
		HIDDEN($start_addr.'_size',           $size);
		HIDDEN($start_addr.'_original',       $location_data);
		HIDDEN($start_addr.'_align',          $align);
		HIDDEN($start_addr.'_pad_with',       $pad_with);
		HIDDEN($start_addr.'_data_mode',      $data_mode);
		HIDDEN($start_addr.'_eerom_location', $eerom_location);
		HIDDEN($start_addr.'_name',           $name);
	
		if($display eq 'N' && $edit_mode ne 'A')
		{
			print " <input type=\"hidden\" name=\"$start_addr\" value=\"$location_data\">\n";
		}
		else
		{
			print " 
			 <tr>
			  <td style=\"font-size: 10pt;\" align=\"center\" rowspan=\"2\" bgcolor=\"#EEEEEE\"><input type=\"checkbox\" name=\"parameter_to_change\" value=\"$start_addr\"></td>
			  <td style=\"font-size: 10pt;\" align=\"left\"><b>$name</b></td>
			";
			
			if(($data_mode eq "A") || ($data_mode eq "H"))
			{
				print "<td style=\"font-size: 10pt;\" align=\"left\" nowrap><input type=\"text\" name=\"$start_addr\" value=\"$location_data\" maxlength=\"$field_length\" size=\"$field_length\"></td>\n";
			}
            elsif ($data_mode eq "P")
            {
                my @choices = split(/\|/, $choices);
                my $pair_str = '';
                for(my $i = 0; $i <= $#choices; $i++) {
                   my @array = split(/,/,$choices[$i]);
                   $pair_str .= $array[0].' ';
                }
                my @pair = split(/,/,join(",", @choices));
                my $popup = OOCGI::Popup->new(name => $start_addr, pair => \@pair);
                my $number = unpack("H*", $location_data);
                if($number eq '00' || $number == 0 || $number eq '') {
                   print qq{  <td align="left" nowrap>\n};
                   $popup->default('00');
                   print $popup->string;
                   print "  </td>\n";
                } elsif($pair_str =~ /$number/) {
                   print qq{  <td align="left" nowrap>\n};
                   $popup->default($number);
                   print $popup->string;
                   print "  </td>\n";
                } else {
                   print qq{  <td align="left" nowrap bgcolor="red">\n};
                   $popup->tail($number, $number);
                   $popup->default($number);
                   print $popup->string;
                   print "  </td>\n";
                }
                HIDDEN($start_addr."_original",       $number);
            }
			elsif ($data_mode eq "C")
			{
				print "  <td align=\"left\" nowrap>\n";
				my @choices = split(/\|/, $choices);
				my $choices_size = scalar(@choices);
				foreach my $choice (@choices)
				{
					print "   <input type=\"radio\" name=\"$start_addr\" value=\"$choice\" " . ($location_data eq $choice || $choices_size == 1 ? 'checked' : '') . ">$choice\n";
				}
				print "  </td>\n";
			}
		
			print "
			 </tr>
			  <tr>
			  <td style=\"font-size: 10pt;\" align=\"left\" valign=\"top\" colspan=\"2\" width=\"100%\" bgcolor=\"#EEEEEE\">
			   <font size=\"-1\">$description <br>
			   ($location_counter/$data_mode/$size/$eerom_location/$start_addr - $end_byte/$align/$pad_with)</font>
			  </td>
			 </tr>\n";
		}
		
		print "<!-- END $location_counter - Address $start_addr: $name -->\n\n";
		$location_counter++;
	}
	else
	{
		print "Failed to parse: " . $csv->error_input();
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
}

print '</table>';

HIDDEN_PASS('customer_id','location_id','parent_location_id','include_device_ids');

print qq{
<table cellspacing="0" cellpadding="2" border="1" width="100%">
 <tr>
  <td align="center" bgcolor="#C0C0C0">
   <input type="button" value="< Back" onClick="javascript:history.go(-1);"> 
   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
   <input type="submit" name="action" value="Next >">
  </td>
 </tr>
</table>
</form>
};

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

