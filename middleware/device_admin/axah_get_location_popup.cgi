#!/usr/local/USAT/bin/perl

use strict;
use USAT::DeviceAdmin::UI::USAPopups;
use OOCGI::OOCGI;

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

$query->printHeader();

my $popLocation;
if($PARAM{name} eq 'location_id') {
   $popLocation = USAT::DeviceAdmin::UI::USAPopups->pop_location();
   $popLocation->head('','Do not change Location');
}
if($PARAM{name} eq 'parent_location_id') {
   $popLocation = USAT::DeviceAdmin::UI::USAPopups->pop_parent_location();
   $popLocation->head('','Do not change parent Location');
}
$popLocation->default('');

print $popLocation->string;
