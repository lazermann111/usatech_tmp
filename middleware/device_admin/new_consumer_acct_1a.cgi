#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print q|<form method="post" action="new_consumer_acct_2.cgi">|;

my $authority_sql = q|
	select authority.authority_id, authority.authority_name 
	from authority.authority, pss.consumer_acct_fmt_authority
	where authority.authority_id = consumer_acct_fmt_authority.authority_id
	and consumer_acct_fmt_authority.consumer_acct_fmt_id = ?
	order by authority.authority_name|;

my $authority_popup = OOCGI::Popup->new(
					name => 'authority_id',
					align => 'yes', 
					delimiter => '.',
					style => "font-family: courier; font-size: 12px;",
					sql => $authority_sql,
					bind => [$params{'consumer_acct_fmt_id'}]);

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
my $rc = 0;
$table->put($rc++,0, B('USAT Card Creation Wizard'),'align=center bgcolor=#C0C0C0 colspan=2');
$table->put($rc,0, 'Select a Card Processing Module', 'nowrap');
$table->put($rc++,1, $authority_popup);

display $table;

HIDDEN_PASS('consumer_acct_fmt_id', 'customer_id', 'location_id');

print "&nbsp<br>" . SUBMIT('action','Next >') . "</form><br>&nbsp;";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
