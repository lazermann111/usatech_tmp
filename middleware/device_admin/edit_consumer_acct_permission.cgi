#!/usr/local/USAT/bin/perl

use strict;

use Data::Dumper;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::UI::USAPopups;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

print qq|<form method="post" action="edit_consumer_acct_permission.cgi">|;

my $err_str;

my $consumer_acct_id = $params{'consumer_acct_id'};
my $permission_action_id =  $params{'permission_action_id'};

my $device_type_id = $params{'device_type_id'};
my $prev_device_type_id = $params{'prev_device_type_id'};
my $action_id =  $params{'action_id'};
my $prev_action_id =  $params{'prev_action_id'};
my @action_params = $query->param("action_params");

my $action = $query->param("action");
my $finished = $query->param("finished");

$err_str .= B('Required parameter not found: consumer_acct_id<br>', 'red') unless(length($consumer_acct_id) > 0);

if($err_str)
{
	print $err_str;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if(defined($device_type_id) && defined($prev_device_type_id) && $prev_device_type_id ne $device_type_id)
{
	$action_id = undef;
	@action_params = ();
	$finished = 0;
}

if(defined($action_id) && defined($prev_action_id) && $prev_action_id ne $action_id)
{
	@action_params = ();
	$finished = 0;
}

if($action eq 'Finish' && defined($device_type_id) && defined($action_id) && $finished)
{
	print qq|<div align=left><pre>CHANGE LOG:\n\n|;
	
	# lookup existing permission_actions for selected action
	my $permission_action_sql = q|
		select permission_action.permission_action_id, permission_action_param.action_param_id
		from pss.permission_action, pss.permission_action_param
		where permission_action.permission_action_id = permission_action_param.permission_action_id(+)
		and permission_action.action_id = ?
		order by permission_action.permission_action_id asc, permission_action_param.action_param_id asc|;
		
	# the following section sorts permission_actions by their exiting permissions_action_params, so we can 
	# lookup existing permission_actions by params and action_id

	my %action_param_hash = ();
	my $last_permission_action_id;
	my $current_key;
	
    my $permission_action_query = OOCGI::Query->new($permission_action_sql, undef, [$action_id]);
	while(my %permission_action_hash = $permission_action_query->next_hash)
	{
		my $this_permission_action_id = $permission_action_hash{permission_action_id};
		my $this_action_param_id = defined $permission_action_hash{action_param_id} ? $permission_action_hash{action_param_id} : "-";
		
		if(not defined $last_permission_action_id)
		{
			$current_key = $this_action_param_id;
			$last_permission_action_id = $this_permission_action_id;
		}
		elsif($last_permission_action_id eq $this_permission_action_id)
		{
			$current_key .= ",$this_action_param_id";
		}
		else
		{
			$action_param_hash{$current_key} = $last_permission_action_id;
			
			$current_key = $this_action_param_id;
			$last_permission_action_id = $this_permission_action_id;
		}
	}
	
	$action_param_hash{$current_key} = $last_permission_action_id;
	
	# now see if there is an existing matching permission_action
	@action_params = sort(@action_params);
	my $action_param_key = scalar(@action_params) > 0 ? join(',',@action_params) : "-";
	
	print "key = $action_param_key<br>";
	
	my $new_permission_action_id = $action_param_hash{$action_param_key};
	if(defined $new_permission_action_id)
	{
		# we found a matching permission_action
		print "Using existing permission action $new_permission_action_id with action_params $action_param_key\n";
	}
	else
	{
		($new_permission_action_id) = OOCGI::Query->insert('pss.permission_action', { 
			action_id => $action_id}, 'permission_action_id');
			
			
		printError("Failed to create permission_action for action_id $action_id!") unless(defined $new_permission_action_id);
		
		my $counter = 1;
		foreach my $action_param_id (@action_params)
		{
			my $permission_action_param_rows = OOCGI::Query->insert('pss.permission_action_param', { 
				permission_action_id => $new_permission_action_id,
				action_param_id => $action_param_id,
				permission_action_param_order => $counter++});
			
			printError("Failed to create permission_action_param ($new_permission_action_id, $action_param_id)!") if($permission_action_param_rows != 1);
		}
		
		print "Created new permission_action $new_permission_action_id with action_id $action_id and action_params $action_param_key\n";
	}
	
	my $result;
	
	if($permission_action_id eq $new_permission_action_id) 
	{
		print "Old permission_action_id $permission_action_id == new permission_action_id $new_permission_action_id (NO CHANGE)";
		$result = "No change!";
	}
	else
	{
		my $consumer_acct_permission_delete = OOCGI::Query->delete('pss.consumer_acct_permission', {
			consumer_acct_id => $consumer_acct_id,
			permission_action_id => $permission_action_id});
			
		printError("Failed to delete old consumer_acct_permission ($consumer_acct_id, $permission_action_id)!") if($consumer_acct_permission_delete != 1);
		print "Deleted old consumer_acct_permission ($consumer_acct_id, $permission_action_id)\n";
		
		my $consumer_acct_permission_insert = OOCGI::Query->insert('pss.consumer_acct_permission', { 
			consumer_acct_id => $consumer_acct_id,
			permission_action_id => $new_permission_action_id,
			consumer_acct_permission_order => 1});
			
		printError("Failed to insert new consumer_acct_permission ($consumer_acct_id, $new_permission_action_id)!") if($consumer_acct_permission_insert != 1);
		print "Inserted new consumer_acct_permission ($consumer_acct_id, $new_permission_action_id)\n";
		
		$result = "Card actions changed successfully!";
	}
	
	print q|</pre></div align=left>|;

	print qq|<br>\n|;
	print B("$result<br>&nbsp;<br>\n", "GREEN");
	print qq|<input type=button value="< Back to Consumer Account" onClick="javascript:window.location = '/edit_consumer_acct.cgi?consumer_acct_id=$consumer_acct_id';"><br>\n<br>\n<br>\n|;

	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $device_type_sql = q|
	select distinct device_type.device_type_id, device_type.device_type_desc
	from device_type, device.device_type_action
	where device_type.device_type_id = device_type_action.device_type_id
	order by device_type.device_type_id|;
	
my $rc = 0;	
my $device_type_table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
$device_type_table->put($rc++,0, B('Select the target device type for this card'),'align=center bgcolor=#C0C0C0 colspan=2');

my $device_type_popup = OOCGI::Popup->new(
					name => 'device_type_id',
					align => 'yes', 
					delimiter => '.',
					style => "font-family: courier; font-size: 12px;",
					sql => $device_type_sql);

$device_type_popup->default($device_type_id) if(length($device_type_id > 0));

$device_type_table->put($rc,0,"Target Device Type", 'nowrap');
$device_type_table->put($rc++,1,$device_type_popup);

display $device_type_table;

if(length($device_type_id) > 0)
{
	my $action_sql = q|
		select action.action_id, action.action_name, action.action_desc
		from device.action, device.device_type_action
		where action.action_id = device_type_action.action_id
		and device_type_action.device_type_id = ?|;
		
	$rc = 0;
	my $actions_table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
	$actions_table->put($rc++,0, B('Select a maintenance card action'),'align=center bgcolor=#C0C0C0 colspan=2');
		
	my $action_query = OOCGI::Query->new($action_sql, undef, [$params{'device_type_id'}]);
		
	while(my %hash = $action_query->next_hash) 
	{
		my $action_desc = $hash{action_name};
		$action_desc .= qq|<br><font color="gray">&nbsp;$hash{action_desc}</font>| if(defined $hash{action_desc});
		
		$actions_table->put($rc,0, qq|<input type="radio" name="action_id"| . ($hash{action_id} eq $action_id ? "checked" : "") . qq| value="$hash{action_id}">|);
		$actions_table->put($rc++,1,$action_desc);
	}
	
	display $actions_table;
}


if(length($action_id) > 0)
{
	my $action_param_sql = q|
		select action_param.action_param_id, action_param.action_param_cd, action_param.action_param_name, action_param.action_param_desc
		from device.action_param
		where action_param.action_id = ?
		order by action_param.action_param_cd|;
	
	$rc = 0;	
	my $param_table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
	$param_table->put($rc++,0, B('Select optional parameters for action'),'align=center bgcolor=#C0C0C0 colspan=2');
	
	my $action_param_query = OOCGI::Query->new($action_param_sql, undef, [$action_id]);
	if ($action_param_query->row_count > 0)
	{
		while(my %hash = $action_param_query->next_hash) 
		{
			$param_table->put($rc,0, qq|<input type="checkbox" name="action_params" value="$hash{action_param_id}">|);
			$param_table->put($rc++,1,"$hash{action_param_name}");
		}
	}
	else
	{
		$param_table->put($rc++,0,"No optional parameters for this action", 'colspan=2');
	}
	
	display $param_table;
	
	$finished = 1;
}

HIDDEN('finished', $finished);
HIDDEN('prev_device_type_id', $params{'device_type_id'});
HIDDEN('prev_action_id', $params{'action_id'});
HIDDEN_PASS('consumer_acct_id', 'permission_action_id');

my $button = ($finished ? SUBMIT('action','Finish') : SUBMIT('action','Next >'));
print "&nbsp<br>$button</form><br>&nbsp;";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub printError
{
	my $err_txt = shift;
	print B("$err_txt<br>\n", "red");
	print B("Card generation failed! For help, click OK then send this screen output to an administrator.<br>\n", "red");
	print "<div align=left><pre>\n\n". Dumper($query) . "\n\n</pre><div>";
	print q|<script language="javascript">window.alert("Card generation failed! For help, click OK then send this screen output to an administrator.");</script>|;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
    exit;
}
