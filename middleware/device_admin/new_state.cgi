#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use OOCGI::NTable;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;

use strict;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print q|
<script>
function formSubmit(form) {
    form.submit();
}
</script>
|;

my $sql;
my $qo;

my $bgcolor = "#C0C0C0";
my $action_type;

if($PARAM{myaction} eq 'Save') {
   my %hash = ();
   $hash{state_cd}   = $PARAM{state_cd};
   $hash{state_name} = $PARAM{state_name};
   $hash{country_cd} = $PARAM{country_cd};
   OOCGI::Query->insert('state', \%hash);
}

if($PARAM{myaction} eq 'Delete') {
   my $sql = "SELECT count(*) 
                FROM location.location
               WHERE location_country_cd = '$PARAM{country_cd}'
                 AND location_state_cd   = '$PARAM{delete_state}'";
   my $qo = OOCGI::Query->new($sql);
   my $flag = 0;
   if(my @arr = $qo->next ) {
      if($arr[0] == 0) {
         $flag = 1;   # count return 0
      }
   } else {
      $flag = 1; # count returns null
   }
   if($flag) {
      OOCGI::Query->delete('state', {country_cd => $PARAM{country_cd}, state_cd => $PARAM{delete_state}});
   }
}

print q|<form method="post">|;

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

my $table_rn = 0;
$table->put($table_rn,0,"New State ",qq|colspan=4
            style="background-color:$bgcolor; text-align:center;"|);
$table_rn++;

$table->put($table_rn,0,B('Country:'));

my  $popCountry = OOCGI::Popup->new(name => 'country_cd',
                   style => "font-size: 11px;",
                   onchange => "formSubmit(this.form)",
                   id => "country");

$sql = "SELECT country_cd, country_name 
          FROM country
      ORDER BY country_name";

my $qo2 = OOCGI::Query->new($sql);
while(my @arr = $qo2->next) {
   if($arr[0] eq 'AU' or $arr[0] eq 'US') {
      $popCountry->head($arr[0],$arr[1]);
   } else {
      $popCountry->tail($arr[0],$arr[1]);
   }
}

$popCountry->head('','Undefined');
$popCountry->default($PARAM{country_cd});

$table->put($table_rn,1, $popCountry,'colspan=2');
$table_rn++;

$table->put($table_rn,0,B('State Code:'));
$table->put($table_rn,1,B('State Name:'));
my $str = qq|<input type="submit" name="myaction" value="Delete">|;
$table->put($table_rn,2, B("Check to Delete and press ").$str,'align=center colspan=4');
$table_rn++;
$sql = "SELECT state_cd, state_name 
          FROM state
         WHERE country_cd = '$PARAM{country_cd}'
      ORDER BY state_cd";

$qo = OOCGI::Query->new($sql);
while ( my @arr = $qo->next ) {
   $table->put($table_rn,0, $arr[0]);
   $table->put($table_rn,1, $arr[1]);
   my $label = 'Delete '.$arr[1];
   my $ckbDeleteState = OOCGI::Radio->new(name  => 'delete_state', 
                                       value => $arr[0],
                                       label => $label);
   $table->put($table_rn,2, $ckbDeleteState );
   $table_rn++;
}
my $txtStateCd =  OOCGI::Text->new(name => 'state_cd');
$table->put($table_rn,0, B("Code: ").$txtStateCd);
my $txtStateName =  OOCGI::Text->new(name => 'state_name');
$table->put($table_rn,1, B("Name: ").$txtStateName);
$str = qq|<input type="submit" name="myaction" value="Save">|;
$table->put($table_rn,2, B("<== Enter New State and press ").$str,'align=center colspan=4');

display $table;
print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
