#!/usr/local/USAT/bin/perl


use strict;

use OOCGI::OOCGI;
use warnings;
use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use Date::Calc;
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;

use constant DEBUG => 0;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu =  $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

our $dbh;
&main();

sub main 
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
	$dbh = $DATABASE->{handle};

	if(DEBUG)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		print "<pre>\n";
		print "IN = " . Dumper(\%PARAM);
	}

	my $pos_pta_tmpl_id = length $PARAM{pos_pta_tmpl_id} > 0 ? $PARAM{pos_pta_tmpl_id} : printError("Required Parameter Not Found: pos_pta_tmpl_id");
	my $pos_pta_tmpl_entry_id = length $PARAM{pos_pta_tmpl_entry_id} > 0 ? $PARAM{pos_pta_tmpl_entry_id} : printError("Required Parameter Not Found: pos_pta_tmpl_entry_id");
	my $userAction = length $PARAM{userAction} > 0 ? $PARAM{userAction} : printError("Required Parameter Not Found: userAction");
	if ($userAction eq 'Delete')
	{
		my $result;
		my $delete_stmt = $dbh->prepare(q{
			DELETE
			FROM pss.pos_pta_tmpl_entry
			WHERE pos_pta_tmpl_entry_id = :pos_pta_tmpl_entry_id
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$delete_stmt->bind_param(":pos_pta_tmpl_entry_id", $pos_pta_tmpl_entry_id);
		$result = $delete_stmt->execute();
		$delete_stmt->finish();
		printError("delete failed!") unless defined $result;		

		if(DEBUG)
		{
			USAT::DeviceAdmin::UI::DAHeader->printHeader();
			print "Location: <a href=\"edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id\">edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id</a>\n";
		}
		else
		{
			print CGI->redirect("/edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id");
		}
		$dbh->disconnect;
		exit 0;
	}
	elsif($userAction eq 'Save')
	{
		### Update Template Entry ###
		my (
			$pos_pta_device_serial_cd,
			$pos_pta_encrypt_key,
			$pos_pta_encrypt_key2,
			$pos_pta_encrypt_key_hex,
			$pos_pta_encrypt_key2_hex,
			$pos_pta_pin_req_yn_flag,
			$pos_pta_passthru_allow_yn_flag,

            $pos_pta_pref_auth_amt,
            $pos_pta_pref_auth_amt_max,
			
			$payment_subtype_id,
			$payment_subtype_key_id,
			
			$authority_payment_mask_id,
			
			$pos_pta_activation_oset_hr,
			$pos_pta_deactivation_oset_hr,

			$original_payment_subtype_id,
			$original_payment_subtype_key_id,
			$original_pos_pta_device_serial_cd,
			$original_pos_pta_encrypt_key,
			$original_pos_pta_encrypt_key2,
			$original_pos_pta_pin_req_yn_flag,
			$original_pos_pta_passthru_allow_yn_flag,

            $original_pos_pta_pref_auth_amt,
            $original_pos_pta_pref_auth_amt_max,
			
			$pos_pta_priority,
			$currency_cd,
			$payment_entry_method_cd
		);
		foreach my $req_param (
			"pos_pta_tmpl_id",
			"pos_pta_tmpl_entry_id",
			"payment_entry_method_cd",
			"pos_pta_pin_req_yn_flag",
			"payment_subtype_id",
			"payment_subtype_key_id",
			"pos_pta_priority",
			"currency_cd",
			"pos_pta_passthru_allow_yn_flag"
		)
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print "Required Param: $req_param = $PARAM{$req_param}\n";
		}
		foreach my $nonreq_param (
			"pos_pta_device_serial_cd",
			"pos_pta_encrypt_key",
			"pos_pta_encrypt_key2",
			"pos_pta_encrypt_key_hex",
			"pos_pta_encrypt_key2_hex",
			"pos_pta_activation_oset_hr", 
			"pos_pta_deactivation_oset_hr",
			"authority_payment_mask_id",
			"pos_pta_pref_auth_amt",
			"pos_pta_pref_auth_amt_max",
			"original_payment_subtype_id",
			"original_payment_subtype_key_id",
			"original_authority_payment_mask_id",
			"original_pos_pta_device_serial_cd",
			"original_pos_pta_encrypt_key",
			"original_pos_pta_encrypt_key2",
			"original_pos_pta_pin_req_yn_flag",
			"original_pos_pta_passthru_allow_yn_flag",
			"original_pos_pta_pref_auth_amt",
			"original_pos_pta_pref_auth_amt_max"
		)
		{
			if (defined $PARAM{$nonreq_param} && length $PARAM{$nonreq_param} > 0) 
			{
				eval "\$$nonreq_param = \$PARAM{$nonreq_param}";
				print "Non-Required Param: $nonreq_param = \"$PARAM{$nonreq_param}\"\n";
			}
			else
			{
				print "Non-Required Param: $nonreq_param = \"\"\n";
			}
		}
		
		### validate/parse input ###
		my $error_html = "";
		$error_html .= "<li>Encryption key '$pos_pta_encrypt_key' not a valid Hex string" if (defined $pos_pta_encrypt_key_hex && $pos_pta_encrypt_key_hex == 1 && $pos_pta_encrypt_key !~ m/^[a-fA-F0-9]*$/);
		$error_html .= "<li>Encryption key 2 '$pos_pta_encrypt_key2' not a valid Hex string" if (defined $pos_pta_encrypt_key2_hex && $pos_pta_encrypt_key2_hex == 1 && $pos_pta_encrypt_key2 !~ m/^[a-fA-F0-9]*$/);
		$error_html .= "<li>Invalid PIN required flag 'pos_pta_pin_req_yn_flag': must be Y or N" unless $pos_pta_pin_req_yn_flag =~ m/^[NY]$/;
		$error_html .= "<li>Invalid Invalid authority pass-through required flag 'pos_pta_passthru_allow_yn_flag': must be Y or N" unless $pos_pta_passthru_allow_yn_flag =~ m/^[NY]$/;
		$error_html .= "<li>Invalid Activation Offset: must be numeric ($pos_pta_activation_oset_hr)" if (defined $pos_pta_activation_oset_hr && $pos_pta_activation_oset_hr !~ m/^[-]?\d+$/);
		$error_html .= "<li>Invalid Deactivation Offset: must be numeric ($pos_pta_deactivation_oset_hr)" if (defined $pos_pta_deactivation_oset_hr && $pos_pta_deactivation_oset_hr !~ m/^[-]?\d+$/);
		$error_html .= "<li>Invalid Priority: must be numeric" unless ($pos_pta_priority =~ m/[1-9]/);
		$error_html .= "<li>Override Amount '$pos_pta_pref_auth_amt' not a valid number" if ($pos_pta_pref_auth_amt && $pos_pta_pref_auth_amt !~ m/^[0-9]+(\.[0-9]{0,2})?$/);
		$error_html .= "<li>Override Limit '$pos_pta_pref_auth_amt_max' not a valid number" if ($pos_pta_pref_auth_amt_max && $pos_pta_pref_auth_amt_max !~ m/^[0-9]+(\.[0-9]{0,2})?$/);
		$error_html .= "<li>Override amount can not be greater than limit! ($pos_pta_pref_auth_amt > $pos_pta_pref_auth_amt_max)" if ($pos_pta_pref_auth_amt && $pos_pta_pref_auth_amt_max && $pos_pta_pref_auth_amt > $pos_pta_pref_auth_amt_max);
		$error_html .= "<li>Override limit is required with override amount!" if ($pos_pta_pref_auth_amt && !$pos_pta_pref_auth_amt_max);
		$error_html .= "<li>Override amount is required with override limit!" if (!$pos_pta_pref_auth_amt && $pos_pta_pref_auth_amt_max);
		
		my $check_priority_stmt = $dbh->prepare(q{
			SELECT 1
			FROM pss.pos_pta_tmpl_entry ppte
			JOIN pss.payment_subtype ps
				ON ppte.payment_subtype_id = ps.payment_subtype_id
			JOIN pss.client_payment_type cpt
				ON ps.client_payment_type_cd = cpt.client_payment_type_cd
			WHERE ppte.pos_pta_tmpl_id = :pos_pta_tmpl_id
				AND ppte.pos_pta_tmpl_entry_id <> :pos_pta_tmpl_entry_id
				AND ppte.pos_pta_priority = :pos_pta_priority
				AND cpt.payment_entry_method_cd = :payment_entry_method_cd
		}) or _print_error("Couldn't prepare statement: " . $dbh->errstr);
		$check_priority_stmt->bind_param(":pos_pta_tmpl_id", $pos_pta_tmpl_id);
		$check_priority_stmt->bind_param(":pos_pta_tmpl_entry_id", $pos_pta_tmpl_entry_id);
		$check_priority_stmt->bind_param(":pos_pta_priority", $pos_pta_priority);
		$check_priority_stmt->bind_param(":payment_entry_method_cd", $payment_entry_method_cd);
		$check_priority_stmt->execute() or _print_error("Couldn't execute statement: " . $dbh->errstr);
		if (my $row_aref = $check_priority_stmt->fetchrow_arrayref()) {
			$error_html .= "<li>Duplicate Category Priority Value: $pos_pta_priority!"
		}
		$check_priority_stmt->finish();
		
		printError("<ul>$error_html</ul>") if $error_html ne "";
		
		if($pos_pta_tmpl_entry_id > 0)
		{
			### update data ###
			my $result;
			my $pos_pta_update_stmt = $dbh->prepare(q{
				UPDATE pss.pos_pta_tmpl_entry
				SET pos_pta_device_serial_cd = :pos_pta_device_serial_cd,
					pos_pta_encrypt_key = :pos_pta_encrypt_key,
					pos_pta_encrypt_key2 = :pos_pta_encrypt_key2,
					pos_pta_activation_oset_hr = :pos_pta_activation_oset_hr,
					pos_pta_deactivation_oset_hr = :pos_pta_deactivation_oset_hr,
					payment_subtype_id = :payment_subtype_id,
					payment_subtype_key_id = :payment_subtype_key_id,
					pos_pta_pin_req_yn_flag = :pos_pta_pin_req_yn_flag, 
					authority_payment_mask_id = :authority_payment_mask_id,
					pos_pta_priority = :pos_pta_priority,
					currency_cd = :currency_cd,
					pos_pta_passthru_allow_yn_flag = :pos_pta_passthru_allow_yn_flag,
					pos_pta_pref_auth_amt = :pos_pta_pref_auth_amt,
					pos_pta_pref_auth_amt_max = :pos_pta_pref_auth_amt_max
				WHERE pos_pta_tmpl_entry_id = :pos_pta_tmpl_entry_id
			}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
			$pos_pta_update_stmt->bind_param(":pos_pta_device_serial_cd", $pos_pta_device_serial_cd);
			$pos_pta_update_stmt->bind_param(":pos_pta_encrypt_key", defined $pos_pta_encrypt_key ? defined $pos_pta_encrypt_key_hex && $pos_pta_encrypt_key_hex == 1 ? $pos_pta_encrypt_key : unpack("H*", pack("A*", $pos_pta_encrypt_key)) : $pos_pta_encrypt_key);
			$pos_pta_update_stmt->bind_param(":pos_pta_encrypt_key2", defined $pos_pta_encrypt_key2 ? defined $pos_pta_encrypt_key2_hex && $pos_pta_encrypt_key2_hex == 1 ? $pos_pta_encrypt_key2 : unpack("H*", pack("A*", $pos_pta_encrypt_key2)) : $pos_pta_encrypt_key2);
			$pos_pta_update_stmt->bind_param(":pos_pta_activation_oset_hr", $pos_pta_activation_oset_hr);
			$pos_pta_update_stmt->bind_param(":pos_pta_deactivation_oset_hr", $pos_pta_deactivation_oset_hr);
			$pos_pta_update_stmt->bind_param(":payment_subtype_id", $payment_subtype_id);
			$pos_pta_update_stmt->bind_param(":payment_subtype_key_id", $payment_subtype_key_id eq 'N/A' ? undef : $payment_subtype_key_id);
			$pos_pta_update_stmt->bind_param(":pos_pta_pin_req_yn_flag", $pos_pta_pin_req_yn_flag);
			$pos_pta_update_stmt->bind_param(":authority_payment_mask_id", $authority_payment_mask_id);
			$pos_pta_update_stmt->bind_param(":pos_pta_tmpl_entry_id", $pos_pta_tmpl_entry_id);
			$pos_pta_update_stmt->bind_param(":pos_pta_priority", $pos_pta_priority);
			$pos_pta_update_stmt->bind_param(":currency_cd", $currency_cd);
			$pos_pta_update_stmt->bind_param(":pos_pta_passthru_allow_yn_flag", $pos_pta_passthru_allow_yn_flag);
			$pos_pta_update_stmt->bind_param(":pos_pta_pref_auth_amt", $pos_pta_pref_auth_amt);
			$pos_pta_update_stmt->bind_param(":pos_pta_pref_auth_amt_max", $pos_pta_pref_auth_amt_max);
			$result = $pos_pta_update_stmt->execute();
			$pos_pta_update_stmt->finish();
			printError("update failed!") unless defined $result;
		}
		else
		{
			### insert data ###
			my $result;
			my $pos_pta_insert_stmt = $dbh->prepare(q{
				INSERT INTO pos_pta_tmpl_entry
				(
					pos_pta_tmpl_id,
					payment_subtype_id,
					pos_pta_encrypt_key,
					pos_pta_activation_oset_hr,
					pos_pta_deactivation_oset_hr,
					payment_subtype_key_id,
					pos_pta_pin_req_yn_flag,
					pos_pta_device_serial_cd,
					pos_pta_encrypt_key2,
					authority_payment_mask_id,
					pos_pta_priority,
					currency_cd,
					pos_pta_passthru_allow_yn_flag,
					pos_pta_pref_auth_amt,
					pos_pta_pref_auth_amt_max
				)
				VALUES
				(
					:pos_pta_tmpl_id,
					:payment_subtype_id,
					:pos_pta_encrypt_key,
					:pos_pta_activation_oset_hr,
					:pos_pta_deactivation_oset_hr,
					:payment_subtype_key_id,
					:pos_pta_pin_req_yn_flag,
					:pos_pta_device_serial_cd,
					:pos_pta_encrypt_key2,
					:authority_payment_mask_id,
					:pos_pta_priority,
					:currency_cd,
					:pos_pta_passthru_allow_yn_flag,
					:pos_pta_pref_auth_amt,
					:pos_pta_pref_auth_amt_max
				)
			}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
			$pos_pta_insert_stmt->bind_param(":pos_pta_tmpl_id", $pos_pta_tmpl_id);
			$pos_pta_insert_stmt->bind_param(":payment_subtype_id", $payment_subtype_id);
			$pos_pta_insert_stmt->bind_param(":pos_pta_encrypt_key", defined $pos_pta_encrypt_key ? defined $pos_pta_encrypt_key_hex && $pos_pta_encrypt_key_hex == 1 ? $pos_pta_encrypt_key : unpack("H*", pack("A*", $pos_pta_encrypt_key)) : $pos_pta_encrypt_key);
			$pos_pta_insert_stmt->bind_param(":pos_pta_activation_oset_hr", $pos_pta_activation_oset_hr);
			$pos_pta_insert_stmt->bind_param(":pos_pta_deactivation_oset_hr", $pos_pta_deactivation_oset_hr);
			$pos_pta_insert_stmt->bind_param(":payment_subtype_key_id", $payment_subtype_key_id eq 'N/A' ? undef : $payment_subtype_key_id);
			$pos_pta_insert_stmt->bind_param(":pos_pta_pin_req_yn_flag", $pos_pta_pin_req_yn_flag);
			$pos_pta_insert_stmt->bind_param(":pos_pta_device_serial_cd", $pos_pta_device_serial_cd);
			$pos_pta_insert_stmt->bind_param(":pos_pta_encrypt_key2", defined $pos_pta_encrypt_key2 ? defined $pos_pta_encrypt_key2_hex && $pos_pta_encrypt_key2_hex == 1 ? $pos_pta_encrypt_key2 : unpack("H*", pack("A*", $pos_pta_encrypt_key2)) : $pos_pta_encrypt_key2);
			$pos_pta_insert_stmt->bind_param(":authority_payment_mask_id", $authority_payment_mask_id);
			$pos_pta_insert_stmt->bind_param(":pos_pta_priority", $pos_pta_priority);
			$pos_pta_insert_stmt->bind_param(":currency_cd", $currency_cd);
			$pos_pta_insert_stmt->bind_param(":pos_pta_passthru_allow_yn_flag", $pos_pta_passthru_allow_yn_flag);
			$pos_pta_insert_stmt->bind_param(":pos_pta_pref_auth_amt", $pos_pta_pref_auth_amt);
			$pos_pta_insert_stmt->bind_param(":pos_pta_pref_auth_amt_max", $pos_pta_pref_auth_amt_max);
			$result = $pos_pta_insert_stmt->execute();
			$pos_pta_insert_stmt->finish();
			printError("insert failed!") unless defined $result;
		}

		if(DEBUG)
		{
			USAT::DeviceAdmin::UI::DAHeader->printHeader();
			print "Location: <a href=\"edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id\">edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id</a>\n";
		}
		else
		{
			print CGI->redirect("/edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id");
		}
		$dbh->disconnect;
		exit 0;
	}
	else
	{
		printError("Unknown userAction Parameter: $userAction");
	}

	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub printError ($) {
	my $err_txt = shift;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
    USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	print "$err_txt\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}
