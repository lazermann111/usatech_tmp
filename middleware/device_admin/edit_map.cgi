#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;

require Text::CSV;
use USAT::DeviceAdmin::Util;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $map_name = $PARAM{"map"};

if(not defined $map_name)
{
	print "Required parameter not found: map_name!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $raw_map_data = USAT::DeviceAdmin::Util::load_file($map_name, $dbh);
if(not defined $raw_map_data)
{
	print "Failed to load $map_name!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my @map_lines = ();
if($raw_map_data =~ /\r\n/)
{
   @map_lines = split /\r\n/, $raw_map_data;
} else {
   @map_lines = split /\n/, $raw_map_data;
}

print "
<table cellpadding=\"2\" border=\"1\" bordercolor=\"#000000\" cellspacing=\"0\" width=\"100%\">
 <tr>
  <th align=\"center\" colspan=\"7\" bgcolor=\"#C0C0C0\">Memory Map Editor - $map_name</th>
 </tr>
 <form method=\"post\" action=\"edit_map_func.cgi\">
 <input type=\"hidden\" name=\"number_of_location\" value=\"".scalar(@map_lines)."\">
 <input type=\"hidden\" name=\"map_name\" value=\"$map_name\">
";

my $location_counter = 1;
my $csv = Text::CSV->new;
foreach my $line (@map_lines)
{
	if ($csv->parse($line)) 
	{
		#"Type Of Card Data On Receipt","209","1","L","0","C","Y","This field determines what card data (if any) will be shown on the printed receipt. Choices are show card data YES Y. Field is Y/N/S.","Y|N|S","S","Y"
		
        my	($name,$start_addr,$size,$align,$pad_with,$data_mode,$display,$description,$choices,$default_choice,$allow_change) = $csv->fields;
	
		print "<tr>\n";
		print " <td bgcolor=\"#C0C0C0\">Order<br><input type=\"text\" maxlength=\"3\" size=\"3\" name=\"$location_counter" . "_order\" value=\"$location_counter\"></td>\n";
		print " <td bgcolor=\"#C0C0C0\">Name<br><input type=\"text\" maxlength=\"100\" size=\"50\" name=\"$location_counter" . "_name\" value=\"$name\"></td>\n";
		print " <td bgcolor=\"#C0C0C0\">Mem Addr<br><input type=\"text\" maxlength=\"3\" size=\"3\" name=\"$location_counter" . "_address\" value=\"$start_addr\"></td>\n";
		print " <td bgcolor=\"#C0C0C0\">Size<br><input type=\"text\" maxlength=\"3\" size=\"3\" name=\"$location_counter" . "_size\" value=\"$size\"></td>\n";
		print " <td bgcolor=\"#C0C0C0\">Align<br><select name=\"$location_counter" . "_align\"><option value=\"R\" " . ($align eq 'R' ? "selected" : "") . ">Right</option><option value=\"L\" " . ($align eq 'L' ? "selected" : "") . ">Left</option><option value=\"C\" " . ($align eq 'C' ? "selected" : "") . ">Center</option></select></td>\n";
		print " <td bgcolor=\"#C0C0C0\">Padding<br><select name=\"$location_counter" . "_pad_with\"><option value=\" \" " . ($pad_with eq ' ' ? "selected" : "") . ">Spaces</option><option value=\"0\" " . ($pad_with eq '0' ? "selected" : "") . ">Zeros</option><option value=\"f\" " . ($pad_with eq 'f' ? "selected" : "") . ">ff<option value=\"a\" " . ($pad_with eq 'a' ? "selected" : "") . ">aa</option></select></td>\n";
		print " <td bgcolor=\"#C0C0C0\">Type<br><select name=\"$location_counter" . "_data_mode\"><option value=\"H\" " . ($data_mode eq 'H' ? "selected" : "") . ">Hex</option><option value=\"A\" " . ($data_mode eq 'A' ? "selected" : "") . ">Ascii</option><option value=\"C\" " . ($data_mode eq 'C' ? "selected" : "") . ">Choice</option><option value=\"P\" " . ($data_mode eq 'P' ? "selected" : "") . ">Popup</option></select></td>\n";
		print "</tr>\n";
		print "<tr>\n";
		print " <td bgcolor=\"#C0C0C0\" colspan=\"6\" rowspan=\"2\">Description<br><textarea cols=\"70\" rows=\"4\" name=\"$location_counter" . "_description\">$description</textarea>";
		
		if($data_mode eq 'C')
		{
			print "<br>Possible Choices <input type=\"text\" size=\"25\" name=\"$location_counter" . "_choices\" value=\"$choices\"> &nbsp;&nbsp;&nbsp;";
			print "Default Choice <input type=\"text\" size=\"5\" name=\"$location_counter" . "_default_choice\" value=\"$default_choice\"><br>";
		} elsif ($data_mode eq 'P') {
			print "<br>Popup String <input type=\"text\" size=\"25\" name=\"$location_counter" . "_choices\" value=\"$choices\"> &nbsp;&nbsp;&nbsp;";
			print "Default Popup <input type=\"text\" size=\"5\" name=\"$location_counter" . "_default_choice\" value=\"$default_choice\"><br>";
        }
		
		print "</td>\n";
		print " <td bgcolor=\"#C0C0C0\">Display?<br><select name=\"$location_counter" . "_display\"><option value=\"Y\" " . ($display eq 'Y' ? "selected" : "") . ">Yes</option><option value=\"N\" " . ($display eq 'N' ? "selected" : "") . ">No</option></select></td>\n";
		print "</tr><tr>\n";
		print " <td bgcolor=\"#C0C0C0\">Send Changes?<br><select name=\"$location_counter" . "_allow_change\"><option value=\"Y\" " . ($allow_change eq 'Y' ? "selected" : "") . ">Yes</option><option value=\"N\" " . ($allow_change eq 'N' ? "selected" : "") . ">No</option></select></td>\n";
		print "</tr><tr>\n";
		print " <td bgcolor=\"#000000\" colspan=\"7\" height=\"1\"><table></table></td>\n";
		print "</tr>\n";
		
		$location_counter++;
	}
	else
	{
		print "Failed to parse: " . $csv->error_input() . "\n\n";
	    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
}

print "
 <tr>
  <td bgcolor=\"#C0C0C0\" colspan=\"7\" align=\"center\">
   <br>
   <input type=\"submit\" name=\"action\" value=\"Save\">
   <br>&nbsp;
  </td>
 </tr>
 </form>
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

