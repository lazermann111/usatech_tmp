#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $settlement_batch_id = $PARAM{"settlement_batch_id"};
my $settlement_batch_info_stmt = $dbh->prepare(
q{
	select settlement_batch.settlement_batch_id, TO_CHAR(settlement_batch.settlement_batch_start_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	TO_CHAR(settlement_batch.settlement_batch_end_ts, 'MM/DD/YYYY HH:MI:SS AM'), settlement_batch_state.settlement_batch_state_name, 
	settlement_batch.settlement_batch_resp_cd, settlement_batch.settlement_batch_resp_desc, settlement_batch.settlement_batch_ref_cd, 
	TO_CHAR(settlement_batch.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), TO_CHAR(settlement_batch.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	terminal_batch.terminal_batch_id, terminal_batch.terminal_batch_num
	from settlement_batch, settlement_batch_state, terminal_batch
	where settlement_batch.settlement_batch_state_id = settlement_batch_state.settlement_batch_state_id
	and settlement_batch.terminal_batch_id = terminal_batch.terminal_batch_id(+)
	and settlement_batch.settlement_batch_id = :settlement_batch_id
});
$settlement_batch_info_stmt->bind_param(":settlement_batch_id", $settlement_batch_id);
$settlement_batch_info_stmt->execute();
my @settlement_batch_info = $settlement_batch_info_stmt->fetchrow_array();
$settlement_batch_info_stmt->finish();

my $tran_totals_stmt = $dbh->prepare(
q{
	select count(1), nvl(sum(tran_settlement_batch.tran_settlement_b_amt), 0)
	from tran_settlement_batch
	where tran_settlement_batch.settlement_batch_id = :settlement_batch_id
});

$tran_totals_stmt->bind_param(":settlement_batch_id", $settlement_batch_id);
$tran_totals_stmt->execute();
my @tran_totals_info = $tran_totals_stmt->fetchrow_array();
$tran_totals_stmt->finish();

my $refund_totals_stmt = $dbh->prepare(
q{
	select count(1), abs(nvl(sum(refund_settlement_batch.refund_settlement_b_amt), 0))
	from refund_settlement_batch
	where refund_settlement_batch.settlement_batch_id = :settlement_batch_id
});

$refund_totals_stmt->bind_param(":settlement_batch_id", $settlement_batch_id);
$refund_totals_stmt->execute();
my @refund_totals_info = $refund_totals_stmt->fetchrow_array();
$refund_totals_stmt->finish();

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=1 width=\"100%\">

 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Settlement</th>
 </tr>
 <tr>
  <td>ID</td><td>$settlement_batch_info[0]&nbsp;</td>
  <td>State</td><td>$settlement_batch_info[3]&nbsp;</td>
 </tr>
 <tr>
  <td>Start Time</td><td>$settlement_batch_info[1]&nbsp;</td>
  <td>End Time</td><td>$settlement_batch_info[2]&nbsp;</td>
 </tr>
 <tr>
  <td>Created</td><td>$settlement_batch_info[7]&nbsp;</td>
  <td>Last Updated</td><td>$settlement_batch_info[8]&nbsp;</td>
 </tr>
 <tr>
  <td>Response Code</td><td>$settlement_batch_info[4]&nbsp;</td>
  <td>Response Message</td><td>$settlement_batch_info[5]&nbsp;</td>
 </tr>
 <tr>
  <td>Reference Code</td><td>$settlement_batch_info[6]&nbsp;</td>
  <td>Terminal Batch</td><td><a href=\"terminal_batch.cgi?terminal_batch_id=$settlement_batch_info[9]\">$settlement_batch_info[10]</a>&nbsp;</td>
 </tr>
 <tr>
  <td>Sale Totals</td><td>$tran_totals_info[0] / \$$tran_totals_info[1]&nbsp;</td>
  <td>Refund Totals</td><td>$refund_totals_info[0] / \$$refund_totals_info[1]&nbsp;</td>
 </tr>
</table>
";

my $transaction_list_stmt = $dbh->prepare(
q{
	select auth.auth_id, auth.tran_id, auth_type.auth_type_desc, auth_state.auth_state_name, auth.auth_parsed_acct_data, 
	acct_entry_method.acct_entry_method_desc, auth.auth_amt, to_char(auth.auth_ts, 'MM/DD/YYYY HH:MI:SS AM'), auth.auth_result_cd, 
	auth.auth_resp_cd, auth.auth_resp_desc, auth.auth_authority_tran_cd, auth.auth_authority_ref_cd, 
	to_char(auth.auth_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(auth.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	to_char(auth.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'), auth.terminal_batch_id, auth.auth_amt_approved, auth.auth_authority_misc_data,
	tran_settlement_batch.settlement_batch_id, tran_settlement_batch.tran_settlement_b_resp_cd, tran_settlement_batch.tran_settlement_b_resp_desc, 
	tran_settlement_batch.tran_settlement_b_amt, to_char(tran_settlement_batch.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	to_char(tran_settlement_batch.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM')
	from auth, auth_type, auth_state, acct_entry_method, tran_settlement_batch
	where auth.auth_id = tran_settlement_batch.auth_id
	and auth.auth_type_cd = auth_type.auth_type_cd
	and auth.auth_state_id = auth_state.auth_state_id
	and auth.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd(+)
	and tran_settlement_batch.settlement_batch_id = :settlement_batch_id
	order by auth.auth_ts desc
	
});
$transaction_list_stmt->bind_param(":settlement_batch_id", $settlement_batch_id);
$transaction_list_stmt->execute();
my @transaction_info;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"6\" bgcolor=\"#C0C0C0\">Sales</th>
 </tr>
 <tr>
  <th>Detail ID</th>
  <th>Tran ID</th>
  <th>Response Code</th>
  <th>Response Desc</th>
  <th>Settlement Amount</th>
  <th>Timestamp</th>
 </tr>
";

while(@transaction_info = $transaction_list_stmt->fetchrow_array())
{
	print "
        <tr>
	     <td><a href=\"auth.cgi?auth_id=$transaction_info[0]\">$transaction_info[0]</a>&nbsp;</td>
    	 <td><a href=\"tran.cgi?tran_id=$transaction_info[1]\">$transaction_info[1]</a></td>
	     <td>$transaction_info[20]&nbsp;</td>
	     <td>$transaction_info[21]&nbsp;</td>
	     <td>\$$transaction_info[22]&nbsp;</td>
	     <td>$transaction_info[23]&nbsp;</td>
    	</tr>
	";
}

$transaction_list_stmt->finish();

print "
</table>
";





my $refund_list_stmt = $dbh->prepare(
q{
	select refund.refund_id, refund.tran_id, abs(refund.refund_amt), refund.refund_desc, to_char(refund.refund_issue_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	refund.refund_issue_by, refund_type.refund_type_desc, to_char(refund.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	to_char(refund.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'), refund_state.refund_state_name, refund.terminal_batch_id, refund.refund_resp_cd, 
	refund.refund_resp_desc, refund.refund_authority_tran_cd, refund.refund_authority_ref_cd, 
	to_char(refund.refund_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'), tran.parent_tran_id, refund.acct_entry_method_cd, refund.refund_parsed_acct_data,
	refund_settlement_batch.settlement_batch_id, refund_settlement_batch.refund_settlement_b_resp_cd, refund_settlement_batch.refund_settlement_b_resp_desc, 
	abs(refund_settlement_batch.refund_settlement_b_amt), to_char(refund_settlement_batch.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	to_char(refund_settlement_batch.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM')
	from tran, refund, refund_type, refund_state, acct_entry_method, refund_settlement_batch
	where tran.tran_id = refund.tran_id
	and refund.refund_id = refund_settlement_batch.refund_id
    and refund.refund_type_cd = refund_type.refund_type_cd 
	and refund.refund_state_id = refund_state.refund_state_id 
	and refund.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd 
	and refund_settlement_batch.settlement_batch_id = :settlement_batch_id
	order by refund.refund_issue_ts desc
	
});
$refund_list_stmt->bind_param(":settlement_batch_id", $settlement_batch_id);
$refund_list_stmt->execute();
my @refund_info;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"6\" bgcolor=\"#C0C0C0\">Refunds</th>
 </tr>
 <tr>
  <th>Detail ID</th>
  <th>Tran ID</th>
  <th>Response Code</th>
  <th>Response Desc</th>
  <th>Settlement Amount</th>
  <th>Timestamp</th>
 </tr>
";

while(@refund_info = $refund_list_stmt->fetchrow_array())
{
	print "
        <tr>
	     <td><a href=\"refund.cgi?refund_id=$refund_info[0]\">$refund_info[0]</a>&nbsp;</td>
    	 <td><a href=\"tran.cgi?tran_id=$refund_info[1]\">$refund_info[1]</a></td>
	     <td>$refund_info[20]&nbsp;</td>
	     <td>$refund_info[21]&nbsp;</td>
	     <td>\$$refund_info[22]&nbsp;</td>
	     <td>$refund_info[23]&nbsp;</td>
    	</tr>
	";
}

$refund_list_stmt->finish();

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
