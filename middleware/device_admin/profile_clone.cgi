#!/usr/local/USAT/bin/perl

use strict;

use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::Profile::PaymentType;
use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::Query;
use USAT::Database;
use USAT::Common::Const;
use USAT::POS::API::PTA::Util;
use USAT::DeviceAdmin::UI::DAHeader;

sub main {
    my $query = OOCGI::OOCGI->new;
    my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
    my $user_menu = $session->print_menu;
    $session->destroy;
    
    ### share same connection between libraries ###
    my $usat_dbh = USAT::Database->new();
    OOCGI::Query->open_db($usat_dbh->{handle});

    my %PARAM = $query->Vars();

    my $from_number = $query->filter_trim_whitespace($PARAM{from_number});
    my $to_number   = $query->filter_trim_whitespace($PARAM{to_number});
    if( $PARAM{myaction} eq 'Compare') {
        my $frqo;
        my $toqo;
        my $frID;
        my $toID;
        my $device_id;
        my $ssn;
        if($PARAM{ev_or_serial_number} eq 'S') {
            my $sql = 'SELECT * FROM device.vw_device_last_active WHERE device_serial_cd = ?';
            $frqo  = OOCGI::Query->new($sql,{ bind => $from_number } );
            if(my %hash = $frqo->next_hash) {
               $ssn = $hash{device_serial_cd};
               $frID= $hash{device_type_id};
            }
            $toqo  = OOCGI::Query->new($sql,{ bind => $to_number } );
            if(my %hash = $toqo->next_hash) {
               $device_id = $hash{device_id};
               $toID      = $hash{device_type_id};
            }
        } else {
            my $sql = 'SELECT * FROM device.vw_device_last_active WHERE device_name = ?';
            $frqo  = OOCGI::Query->new($sql,{ bind => $from_number } );
            if(my %hash = $frqo->next_hash) {
               $ssn = $hash{device_serial_cd};
               $frID= $hash{device_type_id};
            }
            $toqo  = OOCGI::Query->new($sql,{ bind => $to_number } );
            if(my %hash = $toqo->next_hash) {
               $device_id = $hash{device_id};
               $toID      = $hash{device_type_id};
            }
        }
        if(defined $device_id && defined $ssn) {
           if(($frID == PKG_DEVICE_TYPE__G4 && $toID == PKG_DEVICE_TYPE__G4) ||
              ($frID == PKG_DEVICE_TYPE__G5 && $toID == PKG_DEVICE_TYPE__G5)){
			  print CGI->redirect("/gx_clone.cgi?device_id=$device_id&ssn=$ssn");
              exit(); 
           }
        }
    }

    USAT::DeviceAdmin::UI::DAHeader->printHeader();
    USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;

    print q|
    <script LANGUAGE="JavaScript">
    <!--
    function confirmCheckbox()
    {
        var agree=confirm("Please verify any custom options you may have made before proceeding.");
        if (agree)
            return true ;
        else
            return false ;
    }
    function formSubmit(form) {
        form.submit();
    }
    function confirmSubmit()
    {
        if (document.clone_form.from_number.value == '') {
          alert('Source number is empty !');
          return false;
        }
        if (document.clone_form.to_number.value == '') {
          alert('target number is empty !');
          return false;
        }
        if (document.clone_form.from_number.value == document.clone_form.to_number.value)
        {
           alert('Source and Target number should be different !!!');
           return false ;
        } else {
           return true ;
        }
    }
    // -->
    </script>
    |;


    my $sql;
    my $qo;

    my $bgcolor = "#C0C0C0";
    my $action_type;

    print q|<form name="clone_form" method="post">|;

    my $THead = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

    my $head_rn = 0;
    my $radButton1 = OOCGI::Radio->new(name => 'ev_or_serial_number',
                                       value => 'E', label => B('Ev Number'), $PARAM{myaction} ? (disabled => 1) : ());
    my $radButton2 = OOCGI::Radio->new(name => 'ev_or_serial_number',
                                        value => 'S', label => B('Serial Number'), $PARAM{myaction} ? (disabled => 1) : ());
    if($PARAM{ev_or_serial_number} eq 'E') {
        $radButton1->checked;
        HIDDEN('ev_or_serial_number', 'E') if $PARAM{myaction};
    }
    else {
        $radButton2->checked;
        HIDDEN('ev_or_serial_number', 'S') if $PARAM{myaction};
    }
    if ($PARAM{myaction}) {
        $THead->put($head_rn,0,B($PARAM{ev_or_serial_number} eq 'E' ? 'Ev Number' : 'Serial Number'),qq|colspan=4 align=center|);
    } else {
        $THead->put($head_rn,0,$radButton2.'  '.$radButton1 ,qq|colspan=4 align=center|);
    }
    $head_rn++;
    my $txtFromNumber = OOCGI::Text->new(name => 'from_number', value => $from_number, $PARAM{myaction} ? (disabled => 1) : ());
    HIDDEN('from_number', $from_number) if $PARAM{myaction};
    $THead->put($head_rn,0,B('Source Number : '));
    $THead->put($head_rn,1,$txtFromNumber);

    #my $arrow = '<img src=/img/arrow_small_right.gif>';
    #my $txtBlank =  $arrow.$arrow.'======================='.$arrow.$arrow;
    #$THead->put($head_rn,1, $txtBlank, 'align=center');
    $head_rn++;
    my $txtToNumber =  OOCGI::Text->new(name => 'to_number', value => $to_number, $PARAM{myaction} ? (disabled => 1) : ());
    HIDDEN('to_number', $to_number) if $PARAM{myaction};
    $THead->put($head_rn,0,B('Target Number : '));
    $THead->put($head_rn,1,$txtToNumber);
    $head_rn++;

    my $str = qq|<input type="submit" name="myaction" value="Compare" onClick="return confirmSubmit()">|;

    if(!$PARAM{myaction}) {
        my $note = q|This application clones device server settings from one|
            .q| device to another.  Cloned settings include:<br>|
            .q|<ul><li>Customer</li><li>Location</li><li>Payment Configuration</li></ul>|
            .q|You may optionally assign the clone source device to the "Unknown" location after|
            .q| the process is complete, if cloning is being used to replace a device at a location.<p>|
            .q|To do this, enter either the |.B('EV Number','green').' or '.B('Serial Number','green')
            .q| for the clone source device |.B('(Source Number)').q| and the the clone target device |.B('(Target Number).');
        $THead->put($head_rn,0, $note,'width=60% align=justify');
        $THead->put($head_rn,1, $str,'align=center');
        # Clone will overwrite Compare, do not increase row number
        #$head_rn++;
    }

    my $frDevice;
    my $toDevice;
    my $frID;
    my $toID;

    if($PARAM{myaction} eq 'Clone' || $PARAM{myaction} eq 'Compare') {
        if($PARAM{ev_or_serial_number} eq 'S') {
            my $sql = 'SELECT device_id FROM device.vw_device_last_active WHERE device_serial_cd = ?';
            $frID  = OOCGI::Query->new($sql,{ bind => $from_number } )->node(0,0);
            $toID  = OOCGI::Query->new($sql,{ bind => $to_number } )->node(0,0);
        } else {
            my $sql = 'SELECT device_id FROM device.vw_device_last_active WHERE device_name = ?';
            $frID  = OOCGI::Query->new($sql,{ bind => $from_number } )->node(0,0);
            $toID  = OOCGI::Query->new($sql,{ bind => $to_number } )->node(0,0);
        }
    }

    if($PARAM{myaction} eq 'Clone') {
        $frDevice = USAT::DeviceAdmin::DBObj::Device->new($frID);
        $toDevice = USAT::DeviceAdmin::DBObj::Device->new($toID);
        my $txt = q|SELECT * FROM (SELECT pos_id FROM pss.pos WHERE device_id = ? ORDER BY pos_activation_ts DESC) WHERE ROWNUM = 1|;
        my $frPOS = OOCGI::Query->new($txt, { bind => $frDevice->ID } )->node(0,0);
        my $toPOS = OOCGI::Query->new($txt, { bind => $toDevice->ID } )->node(0,0);

        # CLONE STARTS HERE
        print B("Cloning the Device Started ... <br>","#006600");
        my $flag;
        ($frDevice, $toDevice, $flag) = myclone($usat_dbh, $frPOS, $frDevice, $toDevice, \%PARAM);
        if($flag eq '') {
            print B("Cloning the Device Succesful <br>","#006600");
            my $button = "<a href='profile.cgi?tab=1&device_id=".$toDevice->device_id
                ."'>Go To Cloned Device</a>";
            print '<br><br><br>'.$button.'<br><br><br>';
        } else {
            print B("Device cloning failed! <br>$flag","RED");
        }
    } else {
        BR;
        print B('USAT Device Setting Clone Tool',3);
        BR(2); 
        display $THead;
    }

    my $TMain = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
    my $main_rn = 0;
    $TMain->put($main_rn,0,' ','bgcolor=#CDCDCD');
    $main_rn = 0;

    if($PARAM{myaction} eq 'Compare') {
        if($frID eq '') {
            # probably you wanted to select ev number
            $str = qq|<input type="button" name="" value="<-- Go Back" onClick="javascript:history.back(1)">|;
            print $str;
            print B("<br>Are you sure your Selection Type (EV/Serial) matches your entry? ","RED",4);
            USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
            exit();
        }
        if($toID eq '') {
            # probably something wrong in here
            print B("<br>Destination Device ID Can't be found? ","RED",4);
            USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
            exit();
        }
        $frDevice = USAT::DeviceAdmin::DBObj::Device->new($frID);
        $toDevice = USAT::DeviceAdmin::DBObj::Device->new($toID);

        if($frDevice->device_type_id == $toDevice->device_type_id) {
            my $str = qq|<input type="submit" name="myaction" value="Clone" onClick="return confirmCheckbox()">|;
            if($PARAM{myaction} ne 'Clone') {
                $TMain->put($main_rn,0, B('Properties To Be Cloned'),'align=center colspan=3');
                print '<br>'.$str.'<br><br>';
            }
        } else {
            print B(q|Unable to clone: Device types do not match<br>|,'RED',5);
        }

        my $txt = q|SELECT pos_id FROM pos WHERE device_id = ?|;
        my $frPOS = OOCGI::Query->new($txt, { bind => $frDevice->ID } )->node(0,0);
        my $toPOS = OOCGI::Query->new($txt, { bind => $toDevice->ID } )->node(0,0);

        $main_rn++;
        $TMain->put($main_rn,0, OOCGI::NTable->spaces(1),'align=center');
        $TMain->put($main_rn,1, B('SOURCE<br>('.($PARAM{ev_or_serial_number} eq 'S' ? $frDevice->device_serial_cd : $frDevice->device_name).')'),
            'align=center bgcolor=#CDCDCD');
        $TMain->put($main_rn,2, B('TARGET<br>('.($PARAM{ev_or_serial_number} eq 'S' ? $toDevice->device_serial_cd : $toDevice->device_name).')'),
            'align=center bgcolor=#CDCDCD');
        $main_rn++;
        $TMain->put($main_rn,0,B('Device ID'));
        $TMain->put($main_rn,1,$frDevice->ID);
        my $bgcolor = ''; 
        if($frDevice->ID != $toDevice->ID) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toDevice->ID, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('EV Number'));
        $TMain->put($main_rn,1,$frDevice->device_name);
        $bgcolor = ''; 
        if($frDevice->device_name ne $toDevice->device_name) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toDevice->device_name, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Device Serial Code'));
        $TMain->put($main_rn,1,$frDevice->device_serial_cd);
        $bgcolor = ''; 
        if($frDevice->device_serial_cd ne $toDevice->device_serial_cd) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toDevice->device_serial_cd, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Device Type Id'));
        $TMain->put($main_rn,1,$frDevice->device_type_id);
        $bgcolor = ''; 
        if($frDevice->device_type_id != $toDevice->device_type_id) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toDevice->device_type_id, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('POS'),'colspan=3 align=center bgcolor=#CDCDCD');
        $main_rn++;
        $TMain->put($main_rn,0,B('POS ID'));
        $TMain->put($main_rn,1,$frPOS);
        $bgcolor = ''; 
        if($frPOS != $toPOS) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toPOS, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location'),'colspan=3 align=center bgcolor=#CDCDCD');

        my $frLocation = $frDevice->Location;
        my $toLocation = $toDevice->Location;
        $main_rn++;
        $TMain->put($main_rn,0,B('Location ID'));
        $TMain->put($main_rn,1,$frLocation->ID);
        $bgcolor = ''; 
        if($frLocation->ID != $toLocation->ID) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->ID, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location Name'));
        my $text = B("Set to 'Unknown' after Clone<br>(e.g. use for device replacements)",'red');
        my $ckbLocation = OOCGI::Checkbox->new(name => "location_checkbox", 
           label   => ' ',
           value   => '1',
           checked => $toDevice->device_type_id eq PKG_DEVICE_TYPE__ESUDS ? 1 : 0);
        my $TFrLocationName = OOCGI::NTable->new('border="0" width="100%" cellpadding="0" cellspacing="0" ');
        $TFrLocationName->put(0,0,' '.$ckbLocation->string);
        $TFrLocationName->put(0,1," $text");
        $TMain->put($main_rn,1,$frLocation->location_name.($frDevice->location_id > 1 ? '<br>'.$TFrLocationName->string : ''));
        $bgcolor = ''; 
        if($frLocation->location_name ne $toLocation->location_name) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_name, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location Address 1'));
        $TMain->put($main_rn,1,$frLocation->location_addr1);
        $bgcolor = ''; 
        if($frLocation->location_addr1 ne $toLocation->location_addr1) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_addr1, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location Address 2'));
        $TMain->put($main_rn,1,$frLocation->location_addr2);
        $bgcolor = ''; 
        if($frLocation->location_addr2 ne $toLocation->location_addr2) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_addr2, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location City'));
        $TMain->put($main_rn,1,$frLocation->location_city);
        $bgcolor = ''; 
        if($frLocation->location_city ne $toLocation->location_city) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_city, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location County'));
        $bgcolor = ''; 
        if($frLocation->location_county ne $toLocation->location_county) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_county, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location Postal Code'));
        $TMain->put($main_rn,1,$frLocation->location_postal_cd);
        $bgcolor = ''; 
        if($frLocation->location_postal_cd != $toLocation->location_postal_cd) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_postal_cd, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Parent Location ID'));
        $TMain->put($main_rn,1,$frLocation->parent_location_id);
        $bgcolor = ''; 
        if($frLocation->parent_location_id != $toLocation->parent_location_id) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->parent_location_id, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location Country Code'));
        $TMain->put($main_rn,1,$frLocation->location_country_cd);
        $bgcolor = ''; 
        if($frLocation->location_country_cd ne $toLocation->location_country_cd) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_country_cd, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location Type ID'));
        $TMain->put($main_rn,1,$frLocation->location_type_id);
        $bgcolor = ''; 
        if($frLocation->location_type_id != $toLocation->location_type_id) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_type_id, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location State Code'));
        $TMain->put($main_rn,1,$frLocation->location_state_cd);
        $bgcolor = ''; 
        if($frLocation->location_state_cd ne $toLocation->location_state_cd) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_state_cd, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location Time Zone Code'));
        $TMain->put($main_rn,1,$frLocation->location_time_zone_cd);
        $bgcolor = ''; 
        if($frLocation->location_time_zone_cd ne $toLocation->location_time_zone_cd) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_time_zone_cd, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Location Active YN Flag'));
        $TMain->put($main_rn,1,$frLocation->location_active_yn_flag);
        $bgcolor = ''; 
        if($frLocation->location_active_yn_flag ne $toLocation->location_active_yn_flag) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toLocation->location_active_yn_flag, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer'),'colspan=3 align=center bgcolor=#CDCDCD');

        my $frCustomer = $frDevice->Customer;
        my $toCustomer = $toDevice->Customer;
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer ID'));
        $TMain->put($main_rn,1,$frCustomer->ID);
        $bgcolor = ''; 
        if($frCustomer->ID != $toCustomer->ID) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->ID, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer Name'));
        $TMain->put($main_rn,1,$frCustomer->customer_name);
        $bgcolor = ''; 
        if($frCustomer->customer_name ne $toCustomer->customer_name) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_name, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer Address 1'));
        $TMain->put($main_rn,1,$frCustomer->customer_addr1);
        $bgcolor = ''; 
        if($frCustomer->customer_addr1 ne $toCustomer->customer_addr1) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_addr1, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer Address 2'));
        $TMain->put($main_rn,1,$frCustomer->customer_addr2);
        $bgcolor = ''; 
        if($frCustomer->customer_addr2 ne $toCustomer->customer_addr2) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_addr2, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer City'));
        $TMain->put($main_rn,1,$frCustomer->customer_city);
        $bgcolor = ''; 
        if($frCustomer->customer_city ne $toCustomer->customer_city) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_city, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer County'));
        $TMain->put($main_rn,1,$frCustomer->customer_county);
        $bgcolor = ''; 
        if($frCustomer->customer_county ne $toCustomer->customer_county) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_county, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer Postal Code'));
        $TMain->put($main_rn,1,$frCustomer->customer_postal_cd);
        $bgcolor = ''; 
        if($frCustomer->customer_postal_cd != $toCustomer->customer_postal_cd) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_postal_cd, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer Country Code'));
        $TMain->put($main_rn,1,$frCustomer->customer_country_cd);
        $bgcolor = ''; 
        if($frCustomer->customer_country_cd ne $toCustomer->customer_country_cd) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_country_cd, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer Type ID'));
        $TMain->put($main_rn,1,$frCustomer->customer_type_id);
        $bgcolor = ''; 
        if($frCustomer->customer_type_id != $toCustomer->customer_type_id) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_type_id, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer State Code'));
        $TMain->put($main_rn,1,$frCustomer->customer_state_cd);
        $bgcolor = ''; 
        if($frCustomer->customer_state_cd ne $toCustomer->customer_state_cd) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_state_cd, $bgcolor);
        $main_rn++;
        $TMain->put($main_rn,0,B('Customer Active YN Flag'));
        $TMain->put($main_rn,1,$frCustomer->customer_active_yn_flag);
        $bgcolor = ''; 
        if($frCustomer->customer_active_yn_flag ne $toCustomer->customer_active_yn_flag) {
            $bgcolor = 'bgcolor=red';
        }
        $TMain->put($main_rn,2,$toCustomer->customer_active_yn_flag, $bgcolor);
        $main_rn++;

        my $qo = OOCGI::Query->new;
        my $str = '';;
        if (my $pos_pta_table = pos_pta_summary_table($qo->dbh, $frDevice, $PARAM{ev_or_serial_number})) {
            $str .= $pos_pta_table->string;
        }
        if (my $pos_pta_table = pos_pta_summary_table($qo->dbh, $toDevice, $PARAM{ev_or_serial_number})) {
            $str .= $pos_pta_table->string;
        }
        $TMain->put($main_rn,0,$str,'colspan=4');
    }

    display $TMain;
    print q|</form>|;

    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}
main();


sub pos_pta_summary_table ($$$) {
    my $dbh = shift;
    my $device_obj = shift;
    my $display_param_type = shift;
    my $device_id = $device_obj->ID;
    
    my $err_ref;
    my @pos_pta_data = USAT::DeviceAdmin::Profile::PaymentType::_get_summary_data($dbh, $device_id, $err_ref);
    if (scalar @pos_pta_data > 0)
    {
        my $ret;
        my $TPM = OOCGI::NTable->new(qq|border="1" cellpadding="1" cellspacing="0" width="100%"|);
        my $tpmrn = 0;
        $TPM->put($tpmrn++,0, B('Payment Configuration ('
            .($display_param_type eq 'S' ? $device_obj->device_serial_cd : $device_obj->device_name).')')
            ,'align=center bgcolor=#C0C0C0 colspan=4');
        $TPM->put($tpmrn,0, 'Type'    ,'align=center bgcolor=#C0C0C0');
        $TPM->put($tpmrn,1, 'Merchant','align=center bgcolor=#C0C0C0');
        $TPM->put($tpmrn,2, 'Status'  ,'align=center bgcolor=#C0C0C0');
        $tpmrn++;
        my $last_payment_entry_method_cd;
        my $consecutive_cnt = 0;
        for (my $i = 0; $i < scalar @pos_pta_data; $i++)
        {
            my $merchant_desc = USAT::DeviceAdmin::Profile::PaymentType::_get_merchant_desc($dbh, $pos_pta_data[$i]->[8], $pos_pta_data[$i]->[9], $pos_pta_data[$i]->[10], $err_ref);
            if($last_payment_entry_method_cd ne $pos_pta_data[$i]->[6]) {
                $TPM->put($tpmrn++,0,$pos_pta_data[$i]->[12],'colspan=3 align=center bgcolor=#DADADA');
            }
            $last_payment_entry_method_cd = $pos_pta_data[$i]->[6];
            $TPM->put($tpmrn,0,$pos_pta_data[$i]->[1]);
            $TPM->put($tpmrn,1,$merchant_desc);
            my $status = '';
            if(defined $pos_pta_data[$i]->[2] ) {
                $status = $pos_pta_data[$i]->[3]; 
            } elsif( defined $pos_pta_data[$i]->[4]) {
                $status = $pos_pta_data[$i]->[5]; 
            } else {
                $status = '<b><font color="orange">Inactive</font></b>';
            }
			$status .= (defined $pos_pta_data[$i]->[13] ? " (\$$pos_pta_data[$i]->[13] Override)" : "");
            $TPM->put($tpmrn,2,$status);
            $tpmrn++;
        }
        return $TPM;
    }
    return undef;
}

sub myclone ($$$$$) {
    my $msg = "";
    my $usat_dbh   = shift;
    my $src_pos_id = shift;
    my $frDevice   = shift;
    my $toDevice   = shift;
    my $PARAM      = shift;

    # start a transaction
    $usat_dbh->begin_work();

    # RC stands for eSuds Room Controller.
    # excecute stored procedure ( already exist ) to create new POS relation                 
    # copy host records ( what do you mean by host, source or destination?)(Stored procedure doen't do this?)
    # update new RC with old RC customer and location
    # if all these steps are succesfull $flag should be true, than commit else rollback
    my $err;
    COMMITBLOCK: {
        # first run stored procedure 
        # STEP 1
        my $result = $usat_dbh->callproc(
            name  => 'pkg_device_maint.sp_update_loc_id_cust_id',
            bind  => [ qw(:toDeviceId :frDeviceLocationId :frDeviceCustomerId :toDeviceIdNew :toPosIdNew :statusCd :errMsg) ],
            in    => [ $toDevice->ID, $frDevice->location_id, $frDevice->customer_id ],
            inout => [ 20, 20, 20, 2000 ]
        );
        
        print "NEW DEVICE_ID = $result->[0]  NEW POS_ID = $result->[1] <br>";
        # check the status of return code 
        if(!defined $result->[2] || $result->[2] != 0) { # stored procedure returns 0 as success
            $err = "Stored Procedure Returned error: $result->[3]";
            last COMMITBLOCK;
        }
        # each time location or customer (usually customer does not change) changes
        # a new device_id is created. Device table act as a history of a Device.
        # For a given device, serial number and ev number are fixed, but device_id
        # changes. When ever the value of device device_active_yn_flag is 'Y' than
        # that is the active device.
        $toDevice = USAT::DeviceAdmin::DBObj::Device->new($result->[0]);
        my $tar_pos_id = $result->[1];

        #STEP 2
        my $err_str;
        my $success = USAT::POS::API::PTA::Util::clone_device_pos_ptas($usat_dbh, undef, undef, $err_str, $src_pos_id, $tar_pos_id);
        unless ($success) { # if $success not true than something went wrong, $count == 0 OK 
            $err = 'Pos_pta error occured: '.${$err_str};
            last COMMITBLOCK;
        }

        # STEP 3
        if(defined $PARAM->{location_checkbox} && $PARAM->{location_checkbox} == 1) {
            # source pos_id
            $result = $usat_dbh->callproc(
                name  => 'pkg_device_maint.sp_update_loc_id',
                bind  => [ qw(:frDeviceId :UnknownLoc :frDeviceIdNew :frPosIdNew :statusCd :errMsg) ],
                in    => [ $frDevice->ID, 1 ], # new location id will be 1 = Unknown
                inout => [ 20, 20, 20, 2000 ]
            );
            
            # check the status of return code 
            if(!defined $result->[2] || $result->[2] != 0) { # stored procedure returns 0 as success
               $err = "Setting Location Id to 1 has a problem: $result->[3]";
               last COMMITBLOCK;
            }
            $frDevice = USAT::DeviceAdmin::DBObj::Device->new($result->[0]);
            print "NEW DEVICE_ID = $result->[0]  NEW POS_ID = $result->[1] <br>";
        }
        $usat_dbh->commit();
    }

    if(defined $err) {
        $usat_dbh->rollback();
    }

    $msg = $err; 
    return ($frDevice, $toDevice, $msg);
}
