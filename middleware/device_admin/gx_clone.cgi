#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
require Text::CSV;
use POSIX;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::POS::API::PTA::Util;
use USAT::DeviceAdmin::UI::DAHeader;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $err_ref;

my $target_device_id = $PARAM{"device_id"};
if(length($target_device_id) == 0)
{
	print "Required Parameter Not Found: device_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $source_ssn = $PARAM{"ssn_new"} || $PARAM{"ssn"};
if(length($source_ssn) == 0)
{
	print "Required Parameter Not Found: ssn_new or ssn";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

$source_ssn =~ s/^\s+//;
$source_ssn =~ s/\s+$//;
$source_ssn = &padE4($source_ssn);

#- target -----------------------------------------

my $get_target_device_info_stmt = $dbh->prepare("select device.device_id, device.device_name, device.device_serial_cd, to_char(device.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(device.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), device_type.device_type_desc, device.device_active_yn_flag, device.encryption_key, device.device_type_id, pos.location_id, pos.customer_id, pos.pos_id from device, device_type, pos where device.device_type_id = device_type.device_type_id and device.device_id = pos.device_id and device.device_id = :device_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_target_device_info_stmt->bind_param(":device_id", $target_device_id);
$get_target_device_info_stmt->execute();
my @target_device_data = $get_target_device_info_stmt->fetchrow_array();
if(!@target_device_data)
{
	print "Target Device not found! ($target_device_id)";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

$get_target_device_info_stmt->finish();

my $target_device_name = $target_device_data[1];
my $target_device_serial_cd = $target_device_data[2];
my $target_location_id = $target_device_data[9];
my $target_customer_id = $target_device_data[10];
my $target_pos_id = $target_device_data[11];
my $target_ssn = $target_device_serial_cd;
my $target_ev_number = $target_device_name;

my $get_target_location_stmt = $dbh->prepare("select location_id, location_name, location_city, location_state_cd, location_country_cd from location.location where location.location_id = :location_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_target_location_stmt->bind_param(":location_id", $target_location_id);
$get_target_location_stmt->execute();
my @get_target_location_array = $get_target_location_stmt->fetchrow_array();
my $target_location = "$get_target_location_array[1] - $get_target_location_array[2], $get_target_location_array[3] $get_target_location_array[4]";
$get_target_location_stmt->finish();

my $get_target_customer_stmt = $dbh->prepare("select customer_id, customer_name from customer where customer_id = :customer_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_target_customer_stmt->bind_param(":customer_id", $target_customer_id);
$get_target_customer_stmt->execute();
my @get_target_customer_array = $get_target_customer_stmt->fetchrow_array();
my $target_customer = "$get_target_customer_array[1]";
$get_target_customer_stmt->finish();

#my @target_hosts_array = USAT::POS::API::PTA::Util::get_hosts($DATABASE, $target_device_id, \$err_ref);
my @target_pos_ptas_array = USAT::POS::API::PTA::Util::get_active_pos_pta_entries($DATABASE, $target_device_id, \$err_ref);

#- source -----------------------------------------------------

my $get_source_device_info_stmt = $dbh->prepare("select device.device_id, device.device_name, device.device_serial_cd, to_char(device.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(device.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), device_type.device_type_desc, device.device_active_yn_flag, device.encryption_key, device.device_type_id, pos.location_id, pos.customer_id, pos.pos_id from device, device_type, pos where device.device_type_id = device_type.device_type_id and device.device_id = pos.device_id and device.device_type_id in (0,1) and device.device_active_yn_flag = 'Y' and device.device_serial_cd = :ssn") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_source_device_info_stmt->bind_param(":ssn", $source_ssn);
$get_source_device_info_stmt->execute();
my @source_device_data = $get_source_device_info_stmt->fetchrow_array();
if(!@source_device_data)
{
	print "Source Device not found! ($source_ssn)";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

$get_source_device_info_stmt->finish();

my $source_device_id = $source_device_data[0];
my $source_device_name = $source_device_data[1];
my $source_device_serial_cd = $source_device_data[2];
my $source_location_id = $source_device_data[9];
my $source_customer_id = $source_device_data[10];
my $source_pos_id = $source_device_data[11];
my $source_ev_number = $source_device_name;

my $get_source_location_stmt = $dbh->prepare("select location_id, location_name, location_city, location_state_cd, location_country_cd from location.location where location.location_id = :location_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_source_location_stmt->bind_param(":location_id", $source_location_id);
$get_source_location_stmt->execute();
my @get_source_location_array = $get_source_location_stmt->fetchrow_array();
my $source_location = "$get_source_location_array[1] - $get_source_location_array[2], $get_source_location_array[3] $get_source_location_array[4]";
$get_source_location_stmt->finish();

my $get_source_customer_stmt = $dbh->prepare("select customer_id, customer_name from customer where customer_id = :customer_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_source_customer_stmt->bind_param(":customer_id", $source_customer_id);
$get_source_customer_stmt->execute();
my @get_source_customer_stmt = $get_source_customer_stmt->fetchrow_array();
my $source_customer = "$get_source_customer_stmt[1]";
$get_source_customer_stmt->finish();

#my @source_hosts_array = USAT::POS::API::PTA::Util::get_hosts($DATABASE, $source_device_id, \$err_ref);
my @source_pos_ptas_array = USAT::POS::API::PTA::Util::get_active_pos_pta_entries($DATABASE, $source_device_id, \$err_ref);

# ---------------------------------------------------------------------------

if(defined $err_ref)
{
	print "Error! ($err_ref)";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

print ("<table width=\"100%\" cellspacing=\"0\" cellpadding=\"2\" border=\"1\">\n");
print ("<form action=\"gx_clone_func.cgi\" method=\"post\">\n");

print ("<input type=\"hidden\" name=\"source_device_id\" value=\"$source_device_id\">\n");
print ("<input type=\"hidden\" name=\"target_device_id\" value=\"$target_device_id\">\n");
print ("<input type=\"hidden\" name=\"source_ev_number\" value=\"$source_ev_number\">\n");
print ("<input type=\"hidden\" name=\"target_ev_number\" value=\"$target_ev_number\">\n");
print ("<input type=\"hidden\" name=\"source_pos_id\" value=\"$source_pos_id\">\n");
print ("<input type=\"hidden\" name=\"target_pos_id\" value=\"$target_pos_id\">\n");
print ("<input type=\"hidden\" name=\"source_location_id\" value=\"$source_location_id\">\n");
print ("<input type=\"hidden\" name=\"target_location_id\" value=\"$target_location_id\">\n");
print ("<input type=\"hidden\" name=\"source_customer_id\" value=\"$source_customer_id\">\n");
print ("<input type=\"hidden\" name=\"target_customer_id\" value=\"$target_customer_id\">\n");

print ("<tr><th colspan=\"3\" bgcolor=\"#C0C0C0\">Confirm Device Clone Action</th></tr>\n");
print ("<tr><th>&nbsp;</th><th>Clone From</th><th>Clone To</th></tr>\n");
print ("<tr><td>Device ID</td><td>$source_device_id</td><td>$target_device_id</td></tr>\n");
print ("<tr><td>EV Number</td><td>$source_device_name</td><td>$target_device_name</td></tr>\n");
print ("<tr><td>Serial Number</td><td>$source_device_serial_cd</td><td>$target_device_serial_cd</td></tr>\n");
print ("<tr><td>Location</td></td><td colspan=\"2\" align=\"center\">$target_location becomes $source_location</td></tr>\n");
print ("<tr><td>Customer</td><td colspan=\"2\" align=\"center\">$target_customer becomes $source_customer</td></tr>\n");
#print ("<tr><td>Hosts</td><td colspan=\"2\" align=\"center\">" . scalar(@target_hosts_array) . " Hosts becomes " . scalar(@source_hosts_array) . " Hosts</td></tr>\n");
print ("<tr><td>Payment Types</td><td colspan=\"2\" align=\"center\">" . scalar(@target_pos_ptas_array) . " Payment Types becomes " . scalar(@source_pos_ptas_array) . " Payment Types</td></tr>\n");
print ("<tr><td>Configuration</td><td colspan=\"2\" align=\"center\">Copy Full Configuration (excluding counters and comms)</tr>\n");
print ("<tr><td>Counters</td><td colspan=\"2\" align=\"center\">Zero Out Counters</tr>\n");
print ("<tr><td>Pokes</td><td colspan=\"2\" align=\"center\">Create Pokes for Cloned Configuration</tr>\n");
print ("<tr><td align=\"center\" colspan=\"3\"><input type=\"submit\" name=\"action\" value=\"Proceed\"></td></tr>\n");
print ("</table>\n");

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub padE4
{
	my ($num) = @_;
	if(length($num) <= 6)
	{
		$num = substr('0000000', 0, (6-length($num))) . $num;
		$num = 'E4' . $num;
	}
	return $num;
}

