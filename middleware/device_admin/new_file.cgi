#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my $pop_file_type_str = USAT::DeviceAdmin::UI::USAPopups->pop_file_transfer_type->string;

my %PARAM = $query->Vars();

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $ev_number = $PARAM{"ev_number"};

print qq{
<table border="1" width="100%" cellpadding="2" cellspacing="0">
 <tr>
  <th colspan="4" bgcolor="#C0C0C0">Upload File to Server }. ($ev_number ? " for $ev_number" : "") .qq{</th>
 </tr>
 <form method="post" action="new_file_func.cgi" enctype="multipart/form-data">
 <input type="hidden" name="ev_number" value="$ev_number">
 <tr>
  <td nowrap>File Name on Server (As sent to client)</td>
  <td width="50%"><input type="text" name="file_name" size="40" maxlength="255"></td>
 </tr>
 <tr>
  <td nowrap>File Type</td>
  <td width="50%">
    $pop_file_type_str
  </td>
 </tr>
 <tr>
  <td nowrap>Descriptive Comment</td>
  <td width="50%"><input type="text" name="file_comment" size="60" maxlength="1000"></td>
 </tr>
 <tr>
  <td nowrap>File to Upload to Server</td>
  <td width="50%"><input type="file" name="file_data"></td>
 </tr>
 <tr>
  <td colspan="4" align="center">
   <input type="submit" name="action" value="Upload to Server">
  </td>
 </tr>
 </form>
</table>
};
$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
