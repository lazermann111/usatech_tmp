#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::Util;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

my $file_id = $PARAM{"file_id"};
if(length($file_id) == 0)
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	print "Required Parameter Not Found: file_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $file_info_stmt = $dbh->prepare("select file_transfer_name, file_transfer_type_cd, to_char(created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM') from file_transfer where file_transfer_id = :file_id order by file_transfer_type_cd") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$file_info_stmt->bind_param(":file_id", $file_id);
$file_info_stmt->execute();

my @file_data = $file_info_stmt->fetchrow_array();
if(!@file_data)
{
	print "File not found!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $file_name = $file_data[0];
my $file_type = $file_data[1];
my $create_date = $file_data[2];
my $last_updated = $file_data[3];

$file_info_stmt->finish();

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">File Details</th>
 </tr>
 <tr>
  <td nowrap>File ID</td>
  <td width=\"50%\">$file_id&nbsp;</td>
  <td nowrap>Create Date</td>
  <td width=\"50%\">$create_date&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>File Name</td>
  <td width=\"50%\">$file_name&nbsp;</td>
  <td nowrap>Last Updated</td>
  <td width=\"50%\">$last_updated&nbsp;</td>
 </tr>
</table>
";

my $file_content = USAT::DeviceAdmin::Util::blob_select($dbh, $file_id);

print "

<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">

<table cellspacing=\"0\" cellpadding=\"5\" border=\"1\" width=\"100%\">
 <form method=\"post\" action=\"file_edit.cgi\">
 <tr>
  <th bgcolor=\"#C0C0C0\" colspan=\"1\">File Content Preview</th>
 </tr>
 <tr>
  <th><textarea rows=\"20\" cols=\"90\" wrap=\"virtual\">" . pack("H*", $file_content) . "</textarea></th>
 </tr>
</table>
";


$dbh->disconnect;
