#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use Data::Dumper;
use Date::Calc qw(Days_in_Month Today Delta_Days);
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print q|<form method="post" action="new_consumer_acct_5.cgi" onSubmit="javascript:return confirm('Create Cards?');">|;

my $err_str;

$err_str .= B('Invalid parameter: expiration date<br>', 'red') if(length($params{'exp'}) > 0 && !($params{'exp'} =~ /^([0-9]{4})$/));
$err_str .= B('Required parameter not found: num cards<br>', 'red') unless($params{'num_cards'});
$err_str .= B('Invalid parameter: num cards<br>', 'red') unless($params{'num_cards'} && $params{'num_cards'} =~ /^([0-9]{1,6})$/);

if($params{'consumer_acct_fmt_id'} eq '0')
{
	$err_str .= B('Required parameter not found: balance<br>', 'red') unless($params{'balance'});
	$err_str .= B('Invalid parameter: balance<br>', 'red') unless($params{'balance'} =~ /^([0-9]{1,6})$/);
}

my $exp = $params{'exp'};
$exp = '3712' unless(length($exp) > 0);
my $exp_year = '20' . substr($exp, 0, 2);
my $exp_month = substr($exp, 2, 2);
my $exp_display = "$exp_month/$exp_year";

my ($year,$month,$day) = Today();
my $create_date = (length($month) == 1 ? "0$month" : $month) . "/$year";

$err_str .= B('Invalid parameter: expiration month must be between 1-12!<br>', 'red') if($exp_month < 1 || $exp_month > 12);
$err_str .= B('Invalid parameter: expiration year can not be larger than 37!<br>', 'red') if($exp_year > 2037);

if(!$err_str)
{
	my $exp_day = Days_in_Month($exp_year, $exp_month);
	my $exp_date = "$exp_year-$exp_month-$exp_day 23:59:59";
	
	if(Delta_Days($year, $month, $day, $exp_year, $exp_month, $exp_day) < 0)
	{
		$err_str .= B('Invalid parameter: expiration date is in the past!<br>', 'red');
	}
}

if($err_str)
{
	print $err_str;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $exp_day = Days_in_Month($exp_year, $exp_month);
my $exp_date = "$exp_year-$exp_month-$exp_day 23:59:59";

my $rc = 0;	
my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
$table->put($rc++,0, B('USAT Card Creation Wizard'),'align=center bgcolor=#C0C0C0 colspan=2');

$table->put($rc++,0, B('Confirm Choices'), 'colspan=2 align=center');

my $authority_sql = q|
	select authority.authority_name, authority_type.authority_type_name, authority_type.authority_type_desc
	from authority.authority, authority_type
	where authority.authority_type_id = authority_type.authority_type_id
	and authority.authority_id = ?|;
	
my $customer_sql = q|
	select customer_id, customer_name 
	from customer 
	where customer_id = ?|; 
	
my $fmt_sql = q|
	select consumer_acct_fmt_id, consumer_acct_fmt_name 
	from pss.consumer_acct_fmt 
	where consumer_acct_fmt_id = ?|;
	
my $location_sql = q|
	select location_id, location_name 
	from location.location 
	where location_id = ?|; 
	
my $device_type_action_sql = q|
	select device_type.device_type_id, action.action_id, device_type.device_type_desc, action.action_name, action.action_desc, device_type_action.device_type_action_cd
	from device.action, device.device_type_action, device.device_type
	where action.action_id = device_type_action.action_id
	and device_type_action.device_type_id = device_type.device_type_id
    and device_type_action.action_id = ?|;

my $action_param_sql = q|
	select action_param.action_param_id, action_param.action_param_cd, action_param.action_param_name, action_param.action_param_desc
	from device.action_param
	where action_param.action_param_id = ?|;

my $fmt_query = OOCGI::Query->new($fmt_sql, undef, [$params{'consumer_acct_fmt_id'}]);
my %fmt_hash = $fmt_query->next_hash;
$table->put($rc,0,"Card Type");
$table->put($rc++,1,$fmt_hash{consumer_acct_fmt_name});

my $customer_query = OOCGI::Query->new($customer_sql, undef, [$params{'customer_id'}]);
my %customer_hash = $customer_query->next_hash;
$table->put($rc,0,"Customer");
$table->put($rc++,1,$customer_hash{customer_name});

my $location_query = OOCGI::Query->new($location_sql, undef, [$params{'location_id'}]);
my %location_hash = $location_query->next_hash;
$table->put($rc,0,"Location");
$table->put($rc++,1,$location_hash{location_name});

my $authority_query = OOCGI::Query->new($authority_sql, undef, [$params{'authority_id'}]);
my %authority_hash = $authority_query->next_hash;
$table->put($rc,0,"Authority");
$table->put($rc++,1,$authority_hash{authority_name});

$table->put($rc,0,"Authority Type");
$table->put($rc++,1,$authority_hash{authority_type_name});

if(defined $params{'merchant_id'} && length($params{'merchant_id'}) > 0)
{
	my $merchant_sql = q|
		select merchant.merchant_id, merchant.merchant_name, merchant.merchant_desc, merchant.merchant_cd, terminal.terminal_id, terminal.terminal_desc
		from merchant, terminal 
		where merchant.merchant_id = terminal.merchant_id(+)
		and merchant.merchant_id = ?|;

	my $merchant_query = OOCGI::Query->new($merchant_sql, undef, [$params{'merchant_id'}]);
	my %merchant_hash = $merchant_query->next_hash;
	
	$table->put($rc,0,"Merchant ID");
	$table->put($rc++,1,$params{'merchant_id'});

	$table->put($rc,0,"Merchant Name");
	$table->put($rc++,1,$merchant_hash{'merchant_name'});

	$table->put($rc,0,"Merchant Desc");
	$table->put($rc++,1,$merchant_hash{'merchant_desc'});

	$table->put($rc,0,"Merchant Code (Group Code)");
	$table->put($rc++,1,$merchant_hash{'merchant_cd'});
	HIDDEN('merchant_cd', $merchant_hash{'merchant_cd'});

	$table->put($rc,0,"Terminal ID");
	$table->put($rc++,1,$merchant_hash{'terminal_id'});
	
	$table->put($rc,0,"Terminal Desc");
	$table->put($rc++,1,$merchant_hash{'terminal_desc'});
}
else
{
	$table->put($rc,0,"Merchant Name");
	$table->put($rc++,1,$params{'merchant_name'});

	$table->put($rc,0,"Merchant Desc");
	$table->put($rc++,1,$params{'merchant_desc'});

	$table->put($rc,0,"Terminal Desc");
	$table->put($rc++,1,$params{'terminal_desc'});
}

$table->put($rc,0,"Expiration Date");
$table->put($rc++,1,$exp);

$table->put($rc,0,"Expiration Display Date");
$table->put($rc++,1,$exp_display);

$table->put($rc,0,"Deactivation Date");
$table->put($rc++,1,$exp_date);

$table->put($rc,0,"Create Date");
$table->put($rc++,1,$create_date);

$table->put($rc,0,"Number of Cards");
$table->put($rc++,1,$params{'num_cards'});

if(defined $params{'balance'} && length($params{'balance'}) > 0)
{
	$table->put($rc,0,"Card Starting Balance");
	$table->put($rc++,1,$params{'balance'});
}

if(defined $params{'action_id'} && length($params{'action_id'}) > 0)
{
	my $action_id = $params{'action_id'};
	
	my $action_str;
	my $action_devices_types;
	
	my $device_type_action_query = OOCGI::Query->new($device_type_action_sql, undef, [$action_id]);
	while(my %device_type_action_hash = $device_type_action_query->next_hash)
	{
		if(not defined $action_str)
		{
			$action_str = $device_type_action_hash{action_name} . '<br><font color="gray">' . $device_type_action_hash{action_desc} . '</font>';
		}
		
		if(defined $action_devices_types)
		{
			$action_devices_types .= ", " . $device_type_action_hash{device_type_desc}
		}
		else
		{
			$action_devices_types = $device_type_action_hash{device_type_desc}
		}
	} 
	
	$table->put($rc,0,"Maitenance Card Action");
	$table->put($rc++,1,$action_str);
	
	$table->put($rc,0,"Action Device Types");
	$table->put($rc++,1,$action_devices_types);

	my $action_params_str = $params{'action_params'};
	my @action_params = split(/,/, $action_params_str);
	if(scalar(@action_params) > 0)
	{
		$table->put($rc,0," Parameters");
		my $params_desc;
		
		foreach my $action_param (@action_params)
		{
			my $action_param_query = OOCGI::Query->new($action_param_sql, undef, [$action_param]);
			my %action_param_hash = $action_param_query->next_hash;
			
			$params_desc .= $action_param_hash{action_param_name} . '<br>';
		}
		
		$table->put($rc++,1,$params_desc);
	}
}

display $table;

HIDDEN('customer_name', $customer_hash{customer_name});
HIDDEN('location_name', $location_hash{location_name});
HIDDEN('consumer_acct_fmt_name', $fmt_hash{consumer_acct_fmt_name});
HIDDEN('exp', $exp);
HIDDEN('exp_display', $exp_display);
HIDDEN('deactivation_date', $exp_date);
HIDDEN('create_date', $create_date);
HIDDEN_PASS('consumer_acct_fmt_id', 'customer_id', 'location_id', 'authority_id', 'merchant_id', 'merchant_name', 'merchant_desc', 'device_type_id', 'action_id', 'action_params', 'num_cards', 'balance');

print "&nbsp<br>" . SUBMIT('action','Finish') . "</form><br>&nbsp;";


USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
