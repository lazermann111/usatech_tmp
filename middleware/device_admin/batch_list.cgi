#!/usr/local/USAT/bin/perl

use strict;
use warnings;

use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::Date;
use USAT::DeviceAdmin::UI::DAHeader;

require Text::CSV;

use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::DeviceAdmin::Profile::TranHistory;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my $pageNav  = OOCGI::PageNavigation->new(25);

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $terminal_batch_num = $PARAM{"terminal_batch_num"};
my $terminal_id  = $PARAM{"terminal_id"};
my $state        = $PARAM{"state"};
my $authority_id = $PARAM{"authority_id"};
my $open_after   = $PARAM{"open_after"};
my $open_before  = $PARAM{"open_before"};
my $close_after  = $PARAM{"close_after"};
my $close_before = $PARAM{"close_before"};

my @params;

my $list_batches_sql = q{
	SELECT terminal_batch.terminal_batch_id, 
	       terminal_batch.terminal_batch_num, 
	       to_char(terminal_batch.terminal_batch_open_ts, 'MM/DD/YYYY HH24:MI:SS'),
	       to_char(terminal_batch.terminal_batch_close_ts, 'MM/DD/YYYY HH24:MI:SS'),
	       terminal.terminal_id, 
       	   terminal.terminal_cd, 
	       terminal.terminal_desc, 
	       terminal_state.terminal_state_name,
	       merchant.merchant_id, 
	       merchant.merchant_cd,
	       merchant.merchant_name,
	       merchant.merchant_desc,
	       merchant.merchant_bus_name,
	       authority.authority_id, 
	       authority.authority_name,
	       authority_type.authority_type_name,
	       authority_type.authority_type_desc
	  FROM terminal_batch, terminal, merchant, authority.authority, terminal_state, authority_type
	 WHERE terminal_batch.terminal_id = terminal.terminal_id
	   AND terminal.merchant_id = merchant.merchant_id
	   AND merchant.authority_id = authority.authority_id
	   AND terminal.terminal_state_id = terminal_state.terminal_state_id
	   AND authority.authority_type_id = authority_type.authority_type_id 
};

if(defined $terminal_id && length($terminal_id) > 0)
{
	$list_batches_sql .= ' and terminal.terminal_id = ?';
	push(@params, $terminal_id);
}

if(defined $terminal_batch_num && length($terminal_batch_num) > 0)
{
	$list_batches_sql .= ' and terminal_batch.terminal_batch_num = ?';
	push(@params, $terminal_batch_num);
}

if(defined $state && length($state) > 0)
{
	$list_batches_sql .= ' and terminal_batch.terminal_batch_close_ts is null' if($state eq 'open');
	$list_batches_sql .= ' and terminal_batch.terminal_batch_close_ts is not null' if($state eq 'closed');
}

if(defined $authority_id && length($authority_id) > 0)
{
	$list_batches_sql .= ' and authority.authority_id = ?';
	push(@params, $authority_id);
}

if(defined $open_after && length($open_after) > 0)
{
	$list_batches_sql .= ' and terminal_batch.terminal_batch_open_ts > to_date(?, \'MM/DD/YYYY\')';
	push(@params, $open_after);
}

if(defined $open_before && length($open_before) > 0)
{
	$list_batches_sql .= ' and terminal_batch.terminal_batch_open_ts < to_date(?, \'MM/DD/YYYY\')';
	push(@params, $open_before);
}

if(defined $close_after && length($close_after) > 0)
{
	$list_batches_sql .= ' and terminal_batch.terminal_batch_close_ts > to_date(?, \'MM/DD/YYYY\')';
	push(@params, $close_after);
}

if(defined $close_before && length($close_before) > 0)
{
	$list_batches_sql .= ' and terminal_batch.terminal_batch_close_ts < to_date(?, \'MM/DD/YYYY\')';
	push(@params, $close_before);
}

my @table_cols = (
    ['Merchant',    ,  1],
    ['Terminal',    ,  2],
    ['Batch<br>Num' ,  3],
    ['State'        ,  5],
    ['Time Opened', ,  6],
    ['Time Closed'  ,  7],
    ['Authority'    ,  4]
);

my @sql_cols = (
    'LOWER(merchant.merchant_name)',
    'terminal.terminal_cd',
    'terminal_batch.terminal_batch_num',
    undef,
    "NVL(terminal_batch.terminal_batch_open_ts, TO_DATE('1970', 'YYYY'))",
    "NVL(terminal_batch.terminal_batch_close_ts, TO_DATE('1970', 'YYYY'))",
    'authority.authority_name'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');
$table->put($rn_table,0,B('Device List'),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

my $sql = $list_batches_sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql, \@params );


while (my @batch_data = $qo->next_array()) 
{
    $table->put($rn_table,0,qq{<a href="merchant.cgi?merchant_id=$batch_data[8]">$batch_data[10]</a>});
    $table->put($rn_table,1,qq{<a href="terminal.cgi?terminal_id=$batch_data[4]">$batch_data[5]</a>});
    $table->put($rn_table,2,qq{<a href="terminal_batch.cgi?terminal_batch_id=$batch_data[0]">$batch_data[1]</a>});
    $table->put($rn_table,3,((defined $batch_data[3]) ? "Closed" : "Open"));
    my $now  = OOCGI::Date->new->seconds;
    my $date = OOCGI::Date->new($batch_data[2],'MM/DD/YYYY HH24:MI:SS')->seconds;
    my $diff = $now - $date;
    my $color = 'white';
    if(!$batch_data[3]) {
       if($diff < 60 * $PARAM{green_top_limit}) {
          $color = 'lightgreen';
       } 
       elsif($diff < 60 * $PARAM{gold_top_limit}) {
          $color = 'gold';
       } else {
          $color = 'tomato';
       }
    }
    $table->put($rn_table,4,$batch_data[2],"bgcolor=$color nowrap");
    $table->put($rn_table,5,$batch_data[3],'nowrap');
    $table->put($rn_table,6,qq{<a href="authority.cgi?authority_id=$batch_data[13]">$batch_data[14]</a>},'nowrap');

    $rn_table++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

$pageNav->enable();

$pageSort->enable();
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
