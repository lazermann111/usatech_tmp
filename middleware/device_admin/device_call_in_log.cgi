#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

print '<form>';
my $ssn = $PARAM{"ssn"};
if(length($ssn) == 0)
{
	print "Required parameter not found: ssn\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $rowcount = $PARAM{"rowcount"};
if(length($rowcount) == 0)
{
	$rowcount = 25;
}

my %call_type_hash = 
( 
	'U' => 'Session',
	'B' => 'Batch',
	'A' => 'Auth',
	'C' => 'Combo',
);

my %status_hash = 
( 
	'X' => 'Unknown',
	'S' => 'Success',
	'U' => 'Failure',
	'I' => 'In-Progress',
);

my %card_type_hash = 
( 
	'C' => 'Credit',
	'R' => 'RFID',
	'M' => 'Cash',
	'P' => 'Passcard',
	'S' => 'Passcard',
);

my %yesno_hash = 
( 
	'Y' => 'Yes',
	'N' => 'No',
);

my %call_type_color_hash = 
( 
	'U' => '#FFFFFF',
	'B' => '#EEEECC',
	'A' => '#9FBFDF',
	'C' => '#D6E7D3',
);

my %status_color_hash = 
( 
	'X' => '#000000',
	'S' => '#009933',
	'U' => '#CC3300',
	'I' => '#996600',
);

#																			0													1														2							3				4				5					6					7					8					9				10							11						12					13				14					15					16					17					18						19					20				21				22				23			24				25		26
#my $call_in_stmt = $dbh->prepare("select * from (select to_char(call_in_start_ts, 'MM/DD/YYYY HH24:MI:SS'), to_char(call_in_finish_ts, 'MM/DD/YYYY HH24:MI:SS'), ((call_in_finish_ts-call_in_start_ts)*24*60), call_in_type, call_in_status, credit_trans_count, credit_trans_total, cash_trans_count, cash_trans_total, passcard_trans_count, passcard_trans_total, device_sent_config_flag, server_sent_config_flag, auth_card_type, auth_amount, auth_approved_flag, inbound_message_count, inbound_byte_count, outbound_message_count, outbound_byte_count, dex_received_flag, dex_file_size, customer_name, location_name, network_layer, ip_address, modem_id  from device_call_in_record where serial_number = :ssn order by call_in_start_ts desc) where rownum <= $rowcount") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
#my $call_in_stmt = $dbh->prepare("select to_char(call_in_start_ts, 'MM/DD/YYYY HH24:MI:SS'), to_char(call_in_finish_ts, 'MM/DD/YYYY HH24:MI:SS'), ((call_in_finish_ts-call_in_start_ts)*24*60), call_in_type, call_in_status, credit_trans_count, credit_trans_total, cash_trans_count, cash_trans_total, passcard_trans_count, passcard_trans_total, device_sent_config_flag, server_sent_config_flag, auth_card_type, auth_amount, auth_approved_flag, inbound_message_count, inbound_byte_count, outbound_message_count, outbound_byte_count, dex_received_flag, dex_file_size, customer_name, location_name, network_layer, ip_address, modem_id  from device_call_in_record where serial_number = :ssn order by call_in_start_ts desc") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
my $call_in_stmt = $dbh->prepare('select to_char(call_in_start_ts, \'MM/DD/YYYY HH24:MI:SS\'), to_char(call_in_finish_ts, \'MM/DD/YYYY HH24:MI:SS\'), trunc((call_in_finish_ts-call_in_start_ts)*24*60) || \'m \' || round((call_in_finish_ts-call_in_start_ts)*24*60*60) || \'s \', call_in_type, call_in_status, credit_trans_count, credit_trans_total, cash_trans_count, cash_trans_total, passcard_trans_count, passcard_trans_total, device_sent_config_flag, server_sent_config_flag, auth_card_type, auth_amount, auth_approved_flag, inbound_message_count, inbound_byte_count, outbound_message_count, outbound_byte_count, dex_received_flag, dex_file_size, customer_name, location_name, network_layer, ip_address, modem_id, credit_vend_count, cash_vend_count, passcard_vend_count  from device_call_in_record where serial_number = :ssn order by call_in_start_ts desc') or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$call_in_stmt->bind_param(":ssn", $ssn);
$call_in_stmt->execute();

my $textbox = OOCGI::Text->new(name => 'rowcount', value => $PARAM{rowcount}, sz_ml => [4,4]);
my $submit  = SUBMIT('userOP','List');
HIDDEN('ssn',$ssn);
print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\">
 <tr>
  <th bgcolor=\"#A0A0A0\" colspan=\"23\">$textbox $submit <a href=\"profile.cgi?serial_number=$ssn&action=Search\"><font color=\"black\">$ssn</font></a></th>
 </tr>
 <tr>
  <th colspan=\"5\" bgcolor=\"#C0C0C0\">&nbsp;</th>
  <th colspan=\"3\" bgcolor=\"#C0C0C0\">Credit</th>
  <th colspan=\"3\" bgcolor=\"#C0C0C0\">Cash</th>
  <th colspan=\"3\" bgcolor=\"#C0C0C0\">Special</th>  
  <th colspan=\"9\" bgcolor=\"#C0C0C0\">&nbsp;</th>
 </tr>
 <tr>
  <th bgcolor=\"#C0C0C0\">Call<br>Reason</th>
  <th bgcolor=\"#C0C0C0\">Finish<br>Status</th>
  <th bgcolor=\"#C0C0C0\">Start<br>Time</th>
  <th bgcolor=\"#C0C0C0\">End<br>Time</th>
  <th bgcolor=\"#C0C0C0\">Elapsed<br>Time</th>
  <th bgcolor=\"#C0C0C0\">Trans</th>
  <th bgcolor=\"#C0C0C0\">Vend</th>
  <th bgcolor=\"#C0C0C0\">Amt</th>
  <th bgcolor=\"#C0C0C0\">Trans</th>
  <th bgcolor=\"#C0C0C0\">Vend</th>
  <th bgcolor=\"#C0C0C0\">Amt</th>
  <th bgcolor=\"#C0C0C0\">Trans</th>
  <th bgcolor=\"#C0C0C0\">Vend</th>
  <th bgcolor=\"#C0C0C0\">Amt</th>
  <th bgcolor=\"#C0C0C0\">Dex<br>Rec/Size</th>
  <th bgcolor=\"#C0C0C0\">Config Sent<br>Client/Server</th>
  <th bgcolor=\"#C0C0C0\">Auth Request<br>Type/Amt/App</th>
  <th bgcolor=\"#C0C0C0\">Inbound Msgs/Bytes</th>
  <th bgcolor=\"#C0C0C0\">Outbound Msgs/Bytes</th>
  <th bgcolor=\"#C0C0C0\">Customer</th>
  <th bgcolor=\"#C0C0C0\">Location</th>
  <th bgcolor=\"#C0C0C0\">Network<br>Layer</th>
  <th bgcolor=\"#C0C0C0\" nowrap>IP Address/<br>Modem ID</th>
 </tr>
";

my $count = 0;
while (my @data = $call_in_stmt->fetchrow_array()) 
{
	$count++;
	my $bgcolor = $call_type_color_hash{$data[3]};
	
	print " <tr>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">$call_type_hash{$data[3]}&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\"><font color=\"$status_color_hash{$data[4]}\">$status_hash{$data[4]}</font>&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[0]&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[1]&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . $data[2] . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "$data[5]" : "") . "&nbsp;</td>
			 <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "$data[27]" : "") . "&nbsp;</td>
			 <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "\$$data[6]" : "") . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "$data[7]" : "") . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "$data[28]" : "") . "&nbsp;</td>
			 <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "\$$data[8]" : "") . "&nbsp;</td>
			 <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "$data[9]" : "") . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "$data[29]" : "") . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "\$$data[10]" : "") . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? ("$yesno_hash{$data[20]}" . ($data[20] eq 'Y' ? "/$data[21]" : "")) : "") . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /B|C|U/ ? "$yesno_hash{$data[11]}/$yesno_hash{$data[12]}" : "") . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ($data[3] =~ /A|C|U/ ? "$card_type_hash{$data[13]}/\$$data[14]/$yesno_hash{$data[15]}" : "") . "&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[16]/$data[17]&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[18]/$data[19]&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[22]&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[23]&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[24]&nbsp;</td>
	         <td align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ((length($data[26]) > 0) ? $data[26] : $data[25]) . "&nbsp;</td>
	        </tr>
	      ";
	      
	if($count == $rowcount)
	{
		last;
	}
}

$call_in_stmt->finish();

print "
</table>
</form>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
