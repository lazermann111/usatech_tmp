#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::UI::DAHeader;

#require Text::CSV;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::POS::API::PTA::Util;
use USAT::Database;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;
 
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $action = $PARAM{"action"};
if($action ne 'Finish >')
{
	print "<br><font color=\"red\">Undefined Action!</font><br><br>";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print "<br><font color=\"red\">No devices selected!</font><br><br>";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};
my $zero_counters = $PARAM{"zero_counters"};

my $pos_pta_tmpl_id = $PARAM{"pos_pta_tmpl_id"};
my $mode_cd = $PARAM{"mode_cd"};

my @devices_array = split /,/, $device_ids_str;

my $parameters_str = $PARAM{"parameter_to_change"};
my @parameters_array = split /,/, $parameters_str;

my $debug = $PARAM{"debug"};
if(length($debug) == 0)
{
	$debug = 0;
}

print "
<table cellpadding=\"3\" border=\"1\" cellspacing=\"0\" width=\"100%\">

 <tr>
  <th align=\"center\" colspan=\"3\" bgcolor=\"#C0C0C0\">Device Configuration Wizard - Page 9: Processing Log</th>
 </tr>

 <tr>
  <td style=\"font-size: 10pt;\">
  	<input type=button value=\"Start Over\" onClick=\"javascript:window.location = '/bulk_config_wizard_1.cgi';\"> &nbsp;
  	<input type=button value=\"Device Search page\" onClick=\"javascript:window.location = '/';\"> <br>
  <pre>\n";

if($debug)
{
	print "<font color=\"red\"><b>----- DEBUG IS ON!  CHANGES WERE NOT SAVED! -----</b></font>\n\n";
}

print "Selected Devices IDs\t: " . join(",", @devices_array) . "\n";
########print "Selected Parameters\t: " . join(",", @changes_array) . "\n";
print "Selected Customer ID\t: $customer_id\n";
print "Selected Location ID\t: $location_id\n";
print "Selected Parent Location ID\t: $PARAM{parent_location_id}\n";
print "\n";

my $custloc_error_count = 0;
my $custloc_update_count = 0;
my $parentloc_update_count = 0;
my $parentloc_error_count = 0;

if(length($customer_id) > 0 || length($location_id) > 0 || length($PARAM{parent_location_id}) > 0)
{
	print "Updating Customer and/or Location ---------------------\n\n";
	
	foreach my $device_id (@devices_array)
	{
        my $msg = "Update performed on DEVICE ID = $device_id ";

        if(length($customer_id) > 0 && length($location_id) > 0)
        {
            $msg .= "CUSTOMER and LOCATION changed 
               customer_id = $customer_id, location_id = $location_id";
        }
        elsif(length($customer_id) > 0 && length($PARAM{parent_location_id}) > 0 )
        {
            $msg .= "CUSTOMER and PARENT LOCATION changed 
               customer_id = $customer_id, parent_location_id = $PARAM{parent_location_id}";
        }
        elsif(length($customer_id) > 0)
        {
            $msg .= " ONLY CUSTOMER changed customer_id = $customer_id";
        }
        elsif(length($location_id) > 0)
        {
            $msg .= " ONLY LOCATION changed location_id = $location_id";
        }
        elsif(length($PARAM{parent_location_id}) > 0)
        {
            $msg .= " ONLY PARENT LOCATION changed parent_location_id = $PARAM{parent_location_id}";
        }
        my $result;

        if($debug == 0)
        {
	    	if(length($customer_id) > 0 && length($location_id) > 0)
	    	{
	    		### i.e. proc returns [:newDeviceID,:newPOSID,:statusCd,:errMsg] ###
	    		$result = $DATABASE->callproc(
	    			name  => 'pkg_device_maint.sp_update_loc_id_cust_id',
	    			bind  => [ qw(:deviceID :newLocationID :newCustomerId :newDeviceID :newPOSID :statusCd :errMsg) ],
	    			in    => [ $device_id, $location_id, $customer_id ],
	    			inout => [ 20, 20, 20, 2000 ]
	    		);
	    	}
	    	elsif(length($customer_id) > 0)
	    	{
	    		$result = $DATABASE->callproc(
	    			name  => 'pkg_device_maint.sp_update_cust_id',
	    			bind  => [ qw(:deviceID :newCustomerId :newDeviceID :newPOSID :statusCd :errMsg) ],
	    			in    => [ $device_id, $customer_id ],
	    			inout => [ 20, 20, 20, 2000 ]
	    		);
	    	}
	    	elsif(length($location_id) > 0)
	    	{
	    		$result = $DATABASE->callproc(
	    			name  => 'pkg_device_maint.sp_update_loc_id',
	    			bind  => [ qw(:deviceID :newLocationID :newDeviceID :newPOSID :statusCd :errMsg) ],
	    			in    => [ $device_id, $location_id ],
	    			inout => [ 20, 20, 20, 2000 ]
	    		);
	    	}
            if(defined $result->[0] && $result->[0] > 0) {
               $device_id = $result->[0];
            }

            if(length($PARAM{parent_location_id}) > 0 ) {
                # following is parent_location update
                my $sql = q{ UPDATE location.location
                             SET location.parent_location_id = ?
                    WHERE EXISTS
                    (
                          SELECT 1
                            FROM device, pos
                           WHERE device.device_id = pos.device_id
                             AND pos.location_id  = location.location_id
                             AND device.device_id = ?
                    )
					AND location.location_id != 1};
                 if($debug == 0)
                 {
                     my $count = OOCGI::Query->modify($sql, [ $PARAM{parent_location_id}, $device_id ] );
                     if($count > 0) {
                        $parentloc_update_count++;
                     } else {
                        $parentloc_error_count++;
                     }
                 }
            } else {
	    	   if (defined $result->[0] && defined $result->[2] && $result->[2] == 0) {
	    		   $custloc_update_count++;
			       print "Succcess: $msg \n";
	    	   } else {
	    	      	$custloc_error_count++;
	    	      	print "Error: $msg \n";
	    	   }
            }
        }
		else
		{
			print "Succcess: $msg \n";
		}
	}
}
else
{
	print "No Customer or Location Changes! ---------------------\n\n";
}

print "\n";

my $import_count = 0;
my $import_error_count = 0;

if(length($mode_cd) && length($pos_pta_tmpl_id) > 0)
{
	print "Importing Payment Templates ---------------------\n\n";
	
	foreach my $device_id (@devices_array)
	{
		print "Import template $pos_pta_tmpl_id in mode $mode_cd to device $device_id\n";
		if($debug == 0)
		{	
			my $err;
			my $success = USAT::POS::API::PTA::Util::import_template($DATABASE, $device_id, $pos_pta_tmpl_id, $mode_cd, \$err);
		
			if(!$success)
			{
				print "Import failed: $err\n";
				$import_error_count++;
			}
			else
			{
				$import_count++;
			}
		}
		else
		{
			$import_count++;
		}
	}
}

print "\n";

my $change_count = 0;
my $no_change_count = 0;
my $error_count = 0;

if($zero_counters)
{
	print "Zero'ing Out Counters ---------------------\n\n";
	
	foreach my $device_id (@devices_array)
	{
		my ($return_code, $return_msg);
		my $zero_all_data = '000000000000000000000000000000000000000000000000000000000000000000000000';
		
		my $zero_counters_ref = $dbh->prepare("BEGIN sp_update_config (:device_id, :offset, :new_data, :data_type, :device_type, :debug, :return_code, :return_msg); END;");
		$zero_counters_ref->bind_param(":device_id", $device_id);
		$zero_counters_ref->bind_param(":offset", '320');
		$zero_counters_ref->bind_param(":new_data", $zero_all_data);
		$zero_counters_ref->bind_param(":data_type", 'H');
		$zero_counters_ref->bind_param(":device_type", '0');		# only allowing G4/G5 for now... so hardcoding this
		$zero_counters_ref->bind_param(":debug", $debug);
		$zero_counters_ref->bind_param_inout(":return_code", \$return_code, 40);
		$zero_counters_ref->bind_param_inout(":return_msg", \$return_msg, 2048);
		$zero_counters_ref->execute;
		my $db_err_str = $dbh->errstr;
		$zero_counters_ref->finish();
		
		if(defined $db_err_str)
		{
			print "<font color=\"red\"><b>Fatal Error Calling Stored Procedure to Zero Out Counters: $db_err_str!</b></font>
			<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print this screen and give it to an administrator.\");</script>\n";
	        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
			exit;
		}
		
		if($return_code < 0)
		{
			$error_count++;
		}
		elsif($return_code == 0)
		{
			$no_change_count++;
		}
		else
		{
			$change_count++;
		}
		
		print "$return_msg\n";
	}
}
else
{
	print "Not Zero'ing Out Counters ---------------------\n\n";
}

print "\n";

if(length($parameters_str) == 0)
{
	print "No Configuration Changes! ---------------------\n\n";
}
else
{
	print "Updating Configuration Parameters ---------------------\n\n";
}

foreach my $change_location (@parameters_array)
{
	my $start_addr = $change_location;
	
	my $data           = $PARAM{$start_addr};
	my $orig_counter   = $PARAM{$start_addr."_counter"};
	my $size           = $PARAM{$start_addr."_size"};
	my $orig           = $PARAM{$start_addr."_original"};
	my $align          = $PARAM{$start_addr."_align"};
	my $pad_with       = $PARAM{$start_addr."_pad_with"};
	my $data_mode      = $PARAM{$start_addr."_data_mode"};
	my $eerom_location = $PARAM{$start_addr."_eerom_location"};
	my $name           = $PARAM{$start_addr."_name"};

	print "$change_location) $orig_counter $start_addr $size $align $pad_with $data_mode ---------------------------- [ $name ]\n";

	if(length($name) == 0)
	{
		print "\n\n<font color=\"red\"><b>Fatal Error: Memory Location Inconsistency!</b></font><br>\n\n";
		print "<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print or email this screen and give it to an administrator.\");</script>";
		
		print "POST DATA -----------------\n";
		foreach my $key (sort(keys(%PARAM))) 
		{
			print "$key = $PARAM{$key}\n";
		}

		print "\n";
		print "ENV -----------------\n";
		foreach my $key (sort(keys(%ENV))) 
		{
			print "$key = $ENV{$key}\n";
		}
		
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}


	print "IN: \"$data\" len=".length($data)."\n";
	

    if($data_mode eq 'P') {
       # do not remove
       $data_mode = 'H';
    }	
	elsif(($data_mode eq 'H' && length($data) < ($size*2)) || ($data_mode ne 'H' && length($data) < $size))
	{
		$data = &pad($data,$pad_with,$size,$align,$data_mode);
		print "Padded: \"$data\" len=".length($data)."\n";
	}
	else
	{
		print "NO PADDING NEEDED\n";
	}

	print "OUT: \"" . $data . "\" len=".length($data)."\n\n";
	print "Updating devices...\n";

	foreach my $device_id (@devices_array)
	{
		my ($return_code, $return_msg);
		
		my $update_config_ref = $dbh->prepare("BEGIN sp_update_config (:device_id, :offset, :new_data, :data_type, :device_type, :debug, :return_code, :return_msg); END;");
		$update_config_ref->bind_param(":device_id", $device_id);
		$update_config_ref->bind_param(":offset", $start_addr);
		$update_config_ref->bind_param(":new_data", $data);
		$update_config_ref->bind_param(":data_type", $data_mode);
		$update_config_ref->bind_param(":device_type", '0');		# only allowing G4/G5 for now... so hardcoding this
		$update_config_ref->bind_param(":debug", $debug);
		$update_config_ref->bind_param_inout(":return_code", \$return_code, 40);
		$update_config_ref->bind_param_inout(":return_msg", \$return_msg, 2048);
		$update_config_ref->execute;
		my $db_err_str = $dbh->errstr;
		$update_config_ref->finish();
		
		if(defined $db_err_str)
		{
			print "<font color=\"red\"><b>Fatal Error Calling Stored Procedure: $db_err_str!</b></font>
			<script language=\"javascript\">window.alert(\"Fatal Error Occured!  Your changes where NOT saved.  Please click OK, then print this screen and give it to an administrator.\");</script>\n";
	        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
			exit;
		}
		
		if($return_code < 0)
		{
			$error_count++;
		}
		elsif($return_code == 0)
		{
			$no_change_count++;
		}
		else
		{
			$change_count++;
		}
		
		print "$return_msg\n";
	}
	
	print "\n\n";
}

print "\n";
print "Operation Summary ---------------------\n\n";
print "Parameters Changed\t\t\t: $change_count\n";
print "Parameters Skipped\t\t\t: $no_change_count\n";
print "Parameters Errors\t\t\t: $error_count\n";
if(!$PARAM{parent_location_id}) {
   print "Customer/Locations Updated\t\t: $custloc_update_count\n";
   print "Customer/Location Errors\t\t: $custloc_error_count\n";
} else {
   print "Customer/Parent Locations Updated\t: $parentloc_update_count\n";
   print "Customer/Parent Location Errors\t\t: $parentloc_error_count\n";
}
print "Payment Templates Imported\t\t: $import_count\n";
print "Payment Template Errors\t\t\t: $import_error_count\n";

if($error_count > 0 || $custloc_error_count > 0 || $import_error_count > 0)
{
	if($error_count > 0)
	{	
		my $msg = "Warning: $error_count of " . ($change_count+$no_change_count+$error_count) . " Updates Failed!\\n\\nSome changes were not saved correctly. Please click OK, then print or email this screen and give it to an administrator.";
		print "\n\n<font color=\"red\"><b>$msg</b></font><br>\n\n";
		print "<script language=\"javascript\">window.alert(\"$msg\");</script>";
	}
	
	if($custloc_error_count > 0)
	{
#	my $msg = "Warning: $update_errors Customer/Location Updates Failed!  Some changes were not saved correctly. Please click OK, then print or email this screen and give it to an administrator.";
		my $msg = "Warning: Customer/Location Updates Failed!  Some changes were not saved correctly. Please click OK, then print or email this screen and give it to an administrator.";
		print "\n\n<font color=\"red\"><b>$msg</b></font><br>\n\n";
		print "<script language=\"javascript\">window.alert(\"$msg\");</script>";
	}
	
	if($import_error_count > 0)
	{
		my $msg = "Warning: Payment Template Import Failed!  Some changes were not saved correctly. Please click OK, then print or email this screen and give it to an administrator.";
		print "\n\n<font color=\"red\"><b>$msg</b></font><br>\n\n";
		print "<script language=\"javascript\">window.alert(\"$msg\");</script>";
	}
			
	print "POST DATA -----------------\n";
	foreach my $key (sort(keys(%PARAM))) 
	{
		print "$key = $PARAM{$key}\n";
	}

		print "\n";
	print "ENV -----------------\n";
	foreach my $key (sort(keys(%ENV))) 
	{
		print "$key = $ENV{$key}\n";
	}
}
else
{
    if($PARAM{parent_location_id}) {
	print "<script language=\"javascript\">window.alert(\"All changes were processed successfully!\\n\\n$change_count Config Parameters Updated.\\n$no_change_count Config Parameters Already Up-To-Date.\\n$import_count Payment Templates Imported\\n$parentloc_update_count Customers/Parent Locations Updated\");</script>";	
    } else {
	print "<script language=\"javascript\">window.alert(\"All changes were processed successfully!\\n\\n$change_count Config Parameters Updated.\\n$no_change_count Config Parameters Already Up-To-Date.\\n$import_count Payment Templates Imported\\n$custloc_update_count Customers/Locations Updated\");</script>";	
    }
}

if($debug)
{
	print "\n\n";
	print "<font color=\"red\"><b>----- DEBUG IS ON!  CHANGES WERE NOT SAVED! -----</b></font>\n";
}

$dbh->disconnect;

print "
   </pre>
  </td>
 </tr>
</table>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub pad
{
	my (  $string_out,
#		$string_in,
#	$pad_with,
#	$pad_size,
		$left_or_right,
        	$pad_buffer);
	
	my ($string_in,$pad_with,$pad_size,$align,$data_mode) = @_;
	
	if($data_mode eq 'H')
	{
		$pad_size = ($pad_size * 2);
	}
	
	if($align eq 'L')
	{
		$left_or_right = 'R';
	}
	elsif($align eq 'R')
	{
		$left_or_right = 'L';
	}
	elsif($align eq 'C')
	{
		$left_or_right = 'C';
	}		
	
	print "Padding on $left_or_right with \"$pad_with\" to $pad_size\n";
	
	$pad_buffer = "";
	
	if (length($string_in) < $pad_size)
	{
		for (my  $pad_counter = 0;$pad_counter < $pad_size-length($string_in);$pad_counter += length($pad_with))
		{
			$pad_buffer .= $pad_with;
		}
	}
	
	if ($left_or_right eq "L")
	{
		$string_out = $pad_buffer.$string_in;
	}
	elsif ($left_or_right eq "R")
	{
		$string_out = $string_in.$pad_buffer;
	}
	elsif ($left_or_right eq "C")
	{
		$string_out = &pad_center($string_in, $pad_size, $pad_with);
	}
	
	return $string_out;
}

sub pad_center
{
	my ($str, $len, $pad) = @_;

	my $counter = 1;
	while(length($str) < $len)
	{
		if(($counter % 2) == 0)
		{
			$str = $str . $pad;
		}
		else
		{
			$str = $pad . $str;
		}
		
		$counter++;
	}
	
	return $str;
}
