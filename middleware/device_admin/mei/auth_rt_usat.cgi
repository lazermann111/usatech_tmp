#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::MEIHeader;

####### Testing development against production
#my $DATABASE = USAT::Database->new(
#	PrintError => 1, RaiseError => 0, AutoCommit => 1, 
#	primary => 'usadbp', backup => 'usadbp', username => 'web_user', password => 'wxkj21a9'
#);
#######
####### Prodction
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
#######
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %in = $query->Vars;

USAT::DeviceAdmin::UI::MEIHeader->printHeader();
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html");

my $callindate = $in{"auth_date"};
if(length($callindate) == 0)
{
	print "Required parameter not found: auth_date\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
	exit;
}

my $call_in_stmt = $dbh->prepare("SELECT aut.auth_count, ROUND(aut.min_session_time, 0) min_session_time, ROUND(aut.avg_session_time, 0) avg_session_time, ROUND(aut.max_session_time, 0) max_session_time FROM ( SELECT COUNT(1) auth_count, MIN(CASE WHEN dcir.last_message_out_ts IS NULL THEN 0 WHEN ((dcir.last_message_out_ts - dcir.call_in_start_ts) * 86400 > 30) THEN 30 ELSE (dcir.last_message_out_ts - dcir.call_in_start_ts) * 86400 END) min_session_time, AVG(CASE WHEN dcir.last_message_out_ts IS NULL THEN 0 WHEN ((dcir.last_message_out_ts - dcir.call_in_start_ts) * 86400 > 30) THEN 30 ELSE (dcir.last_message_out_ts - dcir.call_in_start_ts) * 86400 END) avg_session_time, MAX(CASE WHEN dcir.last_message_out_ts IS NULL THEN 0 WHEN ((dcir.last_message_out_ts - dcir.call_in_start_ts) * 86400 > 30) THEN 30 ELSE (dcir.last_message_out_ts - dcir.call_in_start_ts) * 86400 END) max_session_time FROM device.device dev, device_call_in_record dcir WHERE dev.device_serial_cd = dcir.serial_number AND dev.device_type_id = 6 AND dcir.call_in_type = 'A' AND dcir.created_ts >= TO_DATE(:callindate, 'MM/DD/YYYY') AND dcir.created_ts < (TO_DATE(:callindate, 'MM/DD/YYYY') + 1) ) aut") || print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";

$call_in_stmt->bind_param(":callindate", $callindate);
$call_in_stmt->execute();

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">
 <tr>
  <th bgcolor=\"#A0A0A0\" colspan=\"4\">Auth Round Trip (USAT - MEI Devices) Report for $callindate</th>
 </tr>
 <tr>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Total Auths</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Min Session Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Avg Session Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Max Session Time</th>
 </tr>
";

my $count = 0;
while (my @data = $call_in_stmt->fetchrow_array()) 
{
	$count++;
	
	print " <tr>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[0]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[1] sec.&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[2] sec.&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[3] sec.&nbsp;</td>
	        </tr>
	      ";
}

$call_in_stmt->finish();

if($count == 0)
{
	print " <tr><td style=\"font-size: 10pt;\" colspan=\"4\" align=\"center\"><font color=\"red\">&nbsp;No Data!</font></td></tr>\n";
}

print "
</table>
";

my $auth_buck_stmt = $dbh->prepare("SELECT (CASE WHEN e.session_time_bucket = 0 THEN '&lt; 3 sec.' WHEN e.session_time_bucket = 7 THEN '&gt; 26 sec.' ELSE TO_CHAR(e.session_time_bucket * 4 - 1) || '-' ||TO_CHAR(e.session_time_bucket * 4 + 2) || ' sec.' END) session_time_bkt, TO_CHAR(ROUND(100 * (RATIO_TO_REPORT(e.auth_count) OVER ()), 2), 'FM990.00') per_total_auth FROM ( SELECT d.session_time_bucket, NVL2(c.auth_count, c.auth_count, 0) auth_count FROM ( SELECT b.session_time_bucket, COUNT(1) auth_count FROM ( SELECT WIDTH_BUCKET(a.session_time, 3, 27, 6) session_time_bucket FROM ( SELECT (CASE WHEN dcir.last_message_out_ts IS NULL THEN 0 WHEN ((dcir.last_message_out_ts - dcir.call_in_start_ts) * 86400 > 30) THEN 30 ELSE (dcir.last_message_out_ts - dcir.call_in_start_ts) * 86400 END) session_time FROM device.device dev, device_call_in_record dcir WHERE dev.device_serial_cd = dcir.serial_number AND dcir.call_in_type = 'A' AND dcir.created_ts >= TO_DATE(:callindate, 'MM/DD/YYYY') AND dcir.created_ts < (TO_DATE(:callindate, 'MM/DD/YYYY') + 1) ) a ) b GROUP BY b.session_time_bucket ) c, ( SELECT (LEVEL - 1) session_time_bucket, 0 auth_count FROM dual CONNECT BY LEVEL <= 8 ) d WHERE d.session_time_bucket = c.session_time_bucket (+) ORDER BY d.session_time_bucket ) e") || print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";

$auth_buck_stmt->bind_param(":callindate", $callindate);
$auth_buck_stmt->execute();

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">
 <tr>
  <th bgcolor=\"#A0A0A0\" colspan=\"2\">Auth Round Trip Groupings (USAT - All Devices) Report for $callindate</th>
 </tr>
 <tr>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Session Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Percentage</th>
 </tr>
";

$count = 0;
while (my @data = $auth_buck_stmt->fetchrow_array()) 
{
	$count++;
	
	print " <tr>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[0]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[1]%&nbsp;</td>
	        </tr>
	      ";
}

$auth_buck_stmt->finish();

if($count == 0)
{
	print " <tr><td style=\"font-size: 10pt;\" colspan=\"2\" align=\"center\"><font color=\"red\">&nbsp;No Data!</font></td></tr>\n";
}

print "
</table>
";


$dbh->disconnect;
USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
