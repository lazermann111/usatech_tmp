#!/usr/local/USAT/bin/perl

use strict;
use CGI::Carp qw(fatalsToBrowser);
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::MEIHeader;

my $query = OOCGI::OOCGI->new;

USAT::DeviceAdmin::UI::MEIHeader->printHeader;
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html"); 

print q|
<style>
div.notes {
text-align: justify;
margin:0 10%;
color:#000fff;
background-color:#00FFFF;
}
h1,h2,p{margin: 0 10px}
h1{font-size: 150%;color: #330000}
h2{font-size: 120%;color: #000}
p{padding-bottom:1em}
h2{padding-top: 0.3em}
</style>
<br>
<div id=nifty class=notes>
<h1>Welcome to USATech Device Admin System</h1>
<h2>Our Device Admin Menu System Has Changed</h2>
<br>
<p>The menu provided at the top of the screen allows the user to navigate in 
the Device Admin system. Please Note that this new menu system is 
drastically different than the previous system. This menu will be visible 
at the top of every page; therefore there will be no need for the back 
button which will reduce the number of buttons clicked, the load on the 
server and wait time for the end user. 
 
 
<p>The user will notice that the order of the items in the new menu follows 
the same order as in the previous system. However, for the Devices, 
Locations, Customers and Files menus, the user will not have to go to any 
other screen; the functionalities of these links are implemented inside 
the new system. As soon as the user moves their mouse pointer on any 
one of the menu items a drop down will show all the available items of 
the old system. For example, when a user brings their mouse pointer on the 
Devices menu, a drop down menu will be shown. By selecting the appropriate 
items from this menu the user can search devices by EV or Serial number. 
The user can also select appropriate links from the list, which are 
provided at the bottom of this list. 
 
<p>Some searches in the old system required that one of the three flags, 
Enable, Disable and Both, had to be selected. In the new system, this 
selection is replaced by the Enable, Disable and Both menus wherever  
necessary. The appropriate flag will be set by menu system internally 
while the user navigates through the menus. To give the effect of which 
flag is in use a color convention will be used. In this system, the 
colors Green, Red and Yellow represent the Enabled, Disabled and Both 
flag respectively. Also, some selections require the user to go many 
levels deep inside menu system. To assist the end user with which menu 
is being used, at each level the selected menu's background color 
will be set to light blue.
</div>
|;

USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
