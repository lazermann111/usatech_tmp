#!/opt/bin/perl
#################################################################
#
# slw-lib.pl
# 
# Version 4.91
#
#################################################################
#
# Written By: Scott Wasserman
# Created: 02/24/1996
# Last Modified: 06/9/2002
#
# This library incorporates Steve Brenner's cgi-lib.pl library
#
#################################################################
# Functions Availible:
#################################################################
#
# filter_List_With_Pattern(pattern,list)
#
# get_Directory_Listing(directory_name)
#
# month_Name_To_Number(month_name)
#
# military_Time(time,am_pm)
#
# kill_Pattern_In_String(string,pattern)
#
# center_String(string)
#
# print_Header()
#
# read_Parse()
#
# print_Html_File(filename)
#
# go_To_Location(URL)
#
# num_To_String(num)
#
# format_Dollar(num,spaces,print_dollar_sign)
#
# email_Message(to,from,subject,bodytext)
#
# clean_URL_Data(URL)
#
# chop_Return(string)
#
# get_UserID()
#
# get_UserIP()
#
# get_Time()
#
# get_Date()
#
# pretty_Date(date)
#
# fix_Date(date)
#
# get_Extended_Date()
#
# all_Lowercase(string)
#
# all_Uppercase(string)
#
# is_Alpha(string)
#
# is_Number(string)
#
# not_True(boolean)
#
# create_Directory(directoryname,mode)
#
# dump_List(list)
#
# dump_Relational_List(relational_list)
#
# on_List(item,list)
#
# not_On_List(item,list)
#
# eat_Last_Char(string)
#
# average_List(list)
#
# pad_String(string)
#
#########################
# Steve Brenner's stuff
#########################
#
# PrintHeader
#
# ReadParse
#
#################################################################
# Globals 
#################################################################

$LINE_FEED = sprintf("%c",10);

#################################################################
# Required Libraries
#################################################################

#################################################################

########################################
#
# give_Random_Item_In_List
#  
########################################
sub give_Random_Item_In_List
{
	return $_[int(rand(scalar(@_)))];
}


########################################
#
# list_Total
#  
########################################
sub list_Total
{
	local(	$total,
			$list_item);
	
	foreach $list_item (@_)
	{
		$total += $list_item;
	}
	
	return $total;
}

########################################
#
# filter_List_With_Pattern
#  
########################################
sub filter_List_With_Pattern
{
	local(	$pattern,
			@search_list,
			@new_list,
			$list_item);
			
	$pattern = &all_Uppercase(shift(@_));
	@search_list = @_;
	
	@new_list = ();
	
	foreach $list_item (@search_list)
	{
		if (&all_Uppercase($list_item) =~ $pattern)
		{
			push(@new_list,$list_item);
		}
	}
	
	return @new_list;
}

########################################
#
# get_Directory_Listing
#  
########################################
sub get_Directory_Listing
{
	local(	@filenames,
			$directory_name);
			
	$directory_name = $_[0];
	
	opendir(DIRECTORY,$directory_name);
	@file_names = readdir(DIRECTORY);
	closedir(DIRECTORY);

	return @file_names;
}


########################################
#
# month_Name_To_Number
#  
########################################
sub month_Name_To_Number
{
	$month_name = &all_Uppercase(&kill_Pattern_In_String($_[0],"."));
	$month_name_length = length($month_name);
	
	@month_name_list = ("JANUARY",
						"FEBRUARY",
						"MARCH",
						"APRIL",
						"MAY",
						"JUNE",
						"JULY",
						"AUGUST",
						"SEPTEMBER",
						"OCTOBER",
						"NOVEMBER",
						"DECEMBER");
						
	$month_num = 1;
	$not_found = 1;
	while ( ($not_found) && ($month_num <= 12) )
	{
		$month_test = substr($month_name_list[($month_num-1)],0,$month_name_length);
		
		if ($month_test eq $month_name)
		{
			$not_found = 0;
		}
		else
		{
			$month_num++;
		}
	}
						
	if ($not_found)
	{
		return "";
	}
	else
	{
		return $month_num;
	}

}


########################################
#
# military_Time
#  
########################################
sub military_Time
{
	local(	$time_in,
			$ampm,
			@time_in_split,
			$hour,
			$minute,
			$time_out);
			
	$time_in = $_[0];
	$ampm = &all_Uppercase(substr($_[1],0,1));

	if ($ampm eq "")
	{
		return $time_in;
	}
	elsif ( ($ampm eq "A") || ($ampm eq "P") )
	{
			
		@time_in_split = split(/:/,$time_in);
		$hour = $time_in_split[0];
		$minute = $time_in_split[1];
		
		if ($ampm eq "A")
		{
			if ($hour eq "12")
			{
				$hour = "0";
				
				$time_out = $hour.":".$minute;
			}
			else
			{
				$time_out = $time_in;
			}
		}
		else
		{
			
			$time_out = ($hour+12).":".$minute;
			return $time_out;
		}
		
		return $time_out;
	}
	else
	{
		return $time_in;
	}
	
}

########################################
#
# kill_Pattern
#  
########################################
sub kill_Pattern_In_String
{
	local(	$string_out,
			$pattern);
			
	$string_out = $_[0];
	$pattern = $_[1];
	$string_out =~ s/$pattern//gi;
	return $string_out;
}

########################################
#
# make_HTML_Line_Breaks
#  
########################################
sub make_HTML_Line_Breaks
{
	local(	$line_in,
			$line_out,
			@chars_in_line,
			$return_counter,
			$char_value,
			$char,
			$char_threshold);
	
	$line_in = $_[0];
	$char_threshold = $_[1];
	
	$line_out = "";
	
	@chars_in_line = split(//,$line_in);
	
	$return_counter = 0;

	$break_at_space = 0;
	
	foreach $char (@chars_in_line)
	{
		$char_value = ord($char);
		
		if ($char_value == 13)
		{
		
		}
		elsif ($char_value == 10)
		{	
			$line_out .= '<BR>';
			$return_counter = 0;
			$break_at_space = 0;
		}
		else
		{
			if (($break_at_space) && ($char_value == 32))
			{
				
				$line_out .= '<BR>';
				$return_counter = 0;
				$break_at_space = 0;
			}
			else
			{
				if ($return_counter > $char_threshold)
				{
					$break_at_space = 1;
					$line_out .= $char;
				}
				else
				{
					$line_out .= $char;
					$return_counter++;
				}
			}
		}
	}
	
	return $line_out;
}




########################################
#
# pad_String
#  
########################################
sub pad_String
{
	local(	$string_in,
			$pad_depth,
			$string_out,
			$add_padding,
			$pad_counter,
			$pad_with,
			$chop_string,
			$pad_on_right);
			
	$string_in = $_[0];
	$pad_depth = $_[1];
	$pad_with = $_[2];
	if ($pad_with eq "")
	{
		$pad_with = " ";
	}
	$chop_string = $_[3];
	if ($chop_string eq "")
	{
		$chop_string = 0;
	}
	$pad_on_right = $_[4];
	if ($pad_on_right eq "")
	{
		$pad_on_right = 1;
	}
	
	$string_out = $string_in;
	if (length($string_out) <= $pad_depth)
	{
		$add_padding = $pad_depth-(length($string_in));
		for ($pad_counter=0;$pad_counter < $add_padding;$pad_counter++)
		{
			if ($pad_on_right)
			{
				$string_out .= $pad_with;
			}
			else
			{
				$string_out = $pad_with.$string_out;
			}
		}
	
	}
	else
	{
		if ($chop_string)
		{
			$string_out = substr($string_in,0,$pad_depth);
		}
	}
	
	return $string_out;
}

########################################
#
# not_True
#  
########################################
sub not_True
{
	if ($_[0])
	{
		return 0;
	}
	else
	{
		return 1;
	}

}


########################################
#
# is_Number
#  
########################################
sub is_Number
{
	local(	$string_in);
			
	$string_in = $_[0];

	if ($string_in =~ /^\d+\Z/)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

########################################
#
# is_Alpha
#  
########################################
sub is_Alpha
{
	local(	$string_in,
			$char_value,
			@string_list,
			$is_alpha,
			$check_char);
			
	$string_in = $_[0];

	if (length($string_in) > 0)
	{
		@string_list = split(//,$string_in);
		
		$is_alpha = 1;
		
		foreach $check_char (@string_list)
		{
			$char_value = ord($check_char);

			if ( (($char_value < 65) or ($char_value > 90)) && (($char_value < 97) or ($char_value > 122)) )
			{
				$is_alpha = 0;
			}
		
		}
	
		return $is_alpha;
	}
	else
	{
		return 0;
	}
}

########################################
#
# execute_Command_Line
#  
########################################
sub execute_Command_Line
{
	local(	$command_line,
			@returned);
			
	$command_line = $_[0];
	
	open(CMD,$command_line." |");
	@returned = <CMD>;
	close CMD;
	
	return @returned;
}



########################################
#
# average_List
#  
########################################
sub average_List
{
	local(	@list_in,
			$list_size,
			$list_total,
			$list_average,
			$list_item);
			
	@list_in = @_;
	$list_size = scalar(@list_in);
	
	if ($list_size > 0)
	{
		$list_total = 0;
		foreach $list_item (@list_in)
		{
			$list_total+=$list_item;
		}
	
		$list_average = $list_total/$list_size;
	}
	else
	{
		$list_average = 0;
	}
	
	return $list_average;
}

########################################
#
# eat_Last_Char
#  
########################################
sub eat_Last_Char
{
	return substr($_[0],0,length($_[0])-1);
}


########################################
#
# on_List
#  
########################################
sub on_List
{
	local(	$item,
			@list,
			$not_found,
			$test_item,
			$item_num);

	$item = shift(@_);
	@list = @_;

	$not_found = 1;
	$item_num = 0;
	
	while ( ($item_num < scalar(@list)) && ($not_found) )
	{
		$test_item = $list[$item_num];
	
		if ($test_item eq $item)
		{
			$not_found = 0;	
		}
		
		$item_num++;
	}

	if ($not_found)
	{
		return 0;
	}
	else
	{
		return 1;
	}

}

########################################
#
# not_On_List
#  
########################################
sub not_On_List
{
	local(	@list_in,
			$search_item,
			$item_counter,
			$not_found);
	
	$search_item = shift(@_);
	@list_in = @_;
			
	if (scalar(@list_in) > 0)
	{
		$item_counter = 0;
		$not_found = 1;
		
		while ( ($item_counter < scalar(@list_in)) && ($not_found) )
		{
			if ($search_item eq $list_in[$item_counter])
			{
				$not_found = 0;
			}
			
			$item_counter++
		}
		
		if ($not_found)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 1;
	}
}

########################################
#
# dump_List
#
# uses global $HTML_HEADER_HAS_BEEN_PRINTED
#
########################################
sub dump_List
{
	local(	$list_item,
			$count);
	

	$count = 0;
	foreach $list_item (@_)
	{
		print $count.") ".$list_item;
		
		if ($HTML_HEADER_HAS_BEEN_PRINTED == 1)
		{
			print "<BR>";
		}
		else
		{
			print "\n";
		}
		
		$count++;
	}


}

########################################
#
# dump_Relational_List
#
# uses global $HTML_HEADER_HAS_BEEN_PRINTED
#  
########################################
sub dump_Relational_List
{
	local(	$key_name,
			$count,
			%relational_list);
	

	$count = 0;
	%relational_list = @_;
		
	print "List contains ".scalar(keys %relational_list)." keys";	
	if ($HTML_HEADER_HAS_BEEN_PRINTED)
	{
		print "<BR>";
	}
	else
	{
		print "\n";
	}
	
	foreach $key_name (keys %relational_list)
	{
		print $key_name.") ".$relational_list{$key_name};
		
		if ($HTML_HEADER_HAS_BEEN_PRINTED)
		{
			print "<BR>";
		}
		else
		{
			print "\n";
		}
		
		$count++;
	}


}

########################################
#
# center_String
#  
########################################
sub center_String
{
	local( 	$string_in,
			$center_block_size,
			$string_out,
			$space_counter,
			$space_buffer);

	$string_in = $_[0];
	$center_block_size = $_[1];
	
	if (length($string_in) > $center_block_size)
	{
		$string_out = substr($string_in,0,$center_block_size);
	}
	else
	{
		$space_buffer = int( ($center_block_size - (length($string_in)))/2 );
		
		$string_out = "";
		if ($space_buffer > 0)
		{
			for ($space_counter=0;$space_counter < $space_buffer;$space_counter++)
			{
				$string_out .= " ";
			}
		}
		
		$string_out .= $string_in;
	}
	
	return $string_out;

}

########################################
#
# print_Header
#
# creates global $HTML_HEADER_HAS_BEEN_PRINTED
#  
########################################
sub print_Header
{
	if (&not_True(($HTML_HEADER_HAS_BEEN_PRINTED)))
	{
		$HTML_HEADER_HAS_BEEN_PRINTED = 1;
		#print &PrintHeader;
		
		print "Content-type: text/html";
		print "\nExpires: Mon, 1 Jan 1990 01:00:00 GMT";
		print "\nCache-Control: no-store, no-cache, must-revalidate";
		print "\nCache-Control: post-check=0, pre-check=0"; 
		print "\nPragma: no-cache";
		print "\n\n";
		
		#print "Expires: Mon, 1 Jan 1990 01:00:00 GMT";
		#print "Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		#print "Cache-Control: no-store, no-cache, must-revalidate";
		#print "Cache-Control: post-check=0, pre-check=0"; 
		#print "Pragma: no-cache";
	}
}

########################################
#
# read_Parse
#  
########################################
sub read_Parse
{
	&ReadParse;
}



########################################
#
# all_Lowercase
#  
########################################
sub all_Lowercase
{
	local( $return_string);
	
	$return_string = $_[0];
	
	$return_string =~ tr/A-Z/a-z/;
	
	return $return_string;

}


########################################
#
# all_Uppercase
#  
########################################
sub all_Uppercase
{
	local( $return_string);
	
	$return_string = $_[0];
	
	$return_string =~ tr/a-z/A-Z/;
	
	return $return_string;

}

########################################
#
# get_UserIP
#  
########################################
sub get_UserIP
{
	local(	$userid);
			
	$userid = $ENV{'REMOTE_ADDR'};

	return $userid
}


########################################
#
# get_UserID
#  
########################################
sub get_UserID
{
	local(	$userid,
			@addr_items,
			$addr,
			$aliases,
			$addrtype,
			$length,
			@addrs,
			$name);
			
	$userid = $ENV{'REMOTE_ADDR'};
	@addr_items = split(/\./,$userid);
	$addr=pack("C4",$addr_items[0],$addr_items[1],$addr_items[2],$addr_items[3]);
	$addrtype="2";
	($name,$aliases,$addrtype,$length,@addrs)=gethostbyaddr($addr,$addrtype);
	if ($name eq "")
	{
		$name=$userid;
	}

	return $name
}


########################################
#
# get_Date
#  
########################################
sub get_Date
{
	
	local(	@currtimeinfo,
			$currdate);
			
	@currtimeinfo = localtime(time);

        if ($currtimeinfo[5] > 100) 
        {
           $currtimeinfo[5] = $currtimeinfo[5] - 100;
        }

        if ($currtimeinfo[5] < 10) 
         {
            $currtimeinfo[5] = "0".$currtimeinfo[5];
         }


	
	$currdate=(($currtimeinfo[4])+1) . "/" . $currtimeinfo[3] . "/" . $currtimeinfo[5];

	return $currdate;
}

########################################
#
# get_Extended_Date
#  
########################################
sub get_Extended_Date
{
	
	local(	@currtimeinfo,
			$currdate,
			$currextendedyear);
			
	@currtimeinfo = localtime(time);
	
	#$currextendedyear = int(((((time)/60)/60)/24)/(365.25))+1970;
	
        $currextendedyear = $currtimeinfo[5] + 1900;

	$currdate=(($currtimeinfo[4])+1) . "/" . $currtimeinfo[3] . "/" . $currextendedyear;

	return $currdate;

}


########################################
#
# get_Time
#  
########################################
sub get_Time
{
	
	local(	@currtimeinfo,
			$currtime);
			
	@currtimeinfo = localtime(time);
	if ($currtimeinfo[1] < 10)
	{
		$currtime=$currtimeinfo[2] . ":0" . $currtimeinfo[1];
	}
	else
	{
		$currtime=$currtimeinfo[2] . ":" . $currtimeinfo[1];
	}
	
	return $currtime;

}

########################################
#
# get_Extended_Time
#  
########################################
sub get_Extended_Time
{
	local(	@currtimeinfo,
			$currtime);
			
	@currtimeinfo = localtime(time);
	if ($currtimeinfo[1] < 10)
	{
		$currtime = $currtimeinfo[2] . ":0" . $currtimeinfo[1];
	}
	else
	{
		$currtime = $currtimeinfo[2] . ":" . $currtimeinfo[1];
	}
	
	if ($currtimeinfo[0] < 10)
	{
		 $currtime .= ".0" . $currtimeinfo[0];
	}
	else
	{
		$currtime .= "." . $currtimeinfo[0];
	}
	
	return $currtime;
}

########################################
#
# fix_Date
#
########################################
sub fix_Date
{
	local(	$date_in,
			@date_split,
			$date_out,
			$month,
			$day,
			$year,
			$error,
			$curr_date,
			@curr_date_split,
			$curr_year);
			
	$date_in = $_[0];
	
	@date_split = split(/\//,$date_in);
	
	if (scalar(@date_split) == 1)
	{
		@date_split = split(/-/,$date_in);
	}
	
	$error = 0;
	
	if (scalar(@date_split) == 3)
	{
		$month = $date_split[0];
		$day = $date_split[1];
		$year = $date_split[2];
		
		if ( (($month < 1) && ($month > 12)) || (($day < 1) && ($day > 31)) )
		{
			$error = 1;
		}
		else
		{
			if (length($month) < 2)
			{
				$month = "0".$month;
			}
			
			if (length($day) < 2)
			{
				$day = "0".$day;
			}
			
			if (length($year) == 2)
			{
				$curr_date = &get_Extended_Date();
				@curr_date_split = split(/\//,$curr_date);
				$curr_year = substr($curr_date_split[2],0,2);
				
				$year = $curr_year.$year;
			}
			
			if (length($year) != 4)
			{
				$error = 1;
			}
			
		}
		
		if ($error)
		{
			$date_out = "";
		}
		else
		{
			$date_out = $month."/".$day."/".$year;
		}	
		
	}
	else
	{	
		$date_out = "";	
	}
	
	
	return $date_out;

}

########################################
#
# pretty_Date
#  
########################################
sub pretty_Date
{	
	local(	$date_in,
			@date_split,
			$abbreviate,
			@apprev_list,
			@month_list,
			$date_out,
			$curr_month);
	
	$date_in = &fix_Date($_[0]);
	$abbreviate = $_[1];
	@date_split = split(/\//,$date_in);
	
	@month_list = ("January","February","March","April","May","June","July","August","September","October","November","December");
	@apprev_list = ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec");
	
	if (($date_split[1] < 10) && (length($date_split[1]) > 1))
	{
		$date_split[1] = substr($date_split[1],1,1);
	}
	
	if ($abbreviate eq "")
	{
		$curr_month = $month_list[($date_split[0])-1];
	}
	else
	{
		$curr_month = $apprev_list[($date_split[0])-1];
	}
	
	$date_out = $curr_month." ".$date_split[1].", ".$date_split[2];
	
	return $date_out;
}

########################################
#
# chop_Return
#  
########################################
sub chop_Return
{

	local(	$stringin,
			$test,
			$choppedstring);
			
	$stringin = $_[0];
	$test = substr($stringin,length($stringin)-1,1);
	
	if ($test eq "\n")
	{
		$choppedstring = substr($stringin,0,length($stringin)-1);
		return $choppedstring;
	}
	else
	{
		return $stringin;
	}
}


########################################
#
# clean_URL_Data(URL)
#  
########################################
sub clean_URL_Data
{

	local($urlout,@url_out_split,$url_out_char);
	
	$urlout = $_[0];
	
	#$urlout =~ s/\t/%09/gi; # Tab (\t)
	$urlout =~ s/ /+/gi;	# Space ( )
	#$urlout =~ s/#/%23/gi;	# Number sign (#)
	$urlout =~ s/%/%25/gi;	# Percent sign (%)
	$urlout =~ s/&/%26/gi;	# Ampersand (&)
	
	$urlout =~ s/\(/%28/gi;	# Right Parenthesis (()
	$urlout =~ s/\)/%29/gi;	# Left Parenthesis ())
	
	#$urlout =~ s/'/%27/gi;	# Apostrophe (')
	$urlout =~ s/\?/%3f/gi;	# Question mark (?)
	$urlout =~ s/\@/%40/gi;	# At symbol (@)
	#$urlout =~ s/^/%5f/gi;	# Caret symbol (^)
	
        # Replace # with %23
        @url_out_split = split(//,$urlout);
        $urlout = "";

        foreach $url_out_char (@url_out_split)
        {
            if (ord($url_out_char) == 35) 
            {
               $urlout .= '%23';
                             
            }
            else
            {
               $urlout .= $url_out_char;
            }
        }

	return $urlout;

}

########################################
#
# email_Message(to,from,subject,bodytext,cc)
#  
########################################
sub email_Message
{

	open (MAIL, "|/usr/lib/sendmail -t");
	print (MAIL "To: ",$_[0],"\n");
        if ($_[4] ne "") 
        {
           print (MAIL "Cc: ",$_[4],"\n");
        }
	print (MAIL "From: ",$_[1],"\n");
	print (MAIL "Subject: ",$_[2],"\n\n");
	print (MAIL $_[3],"\n");
	print (MAIL "\n");
	close (MAIL);

}

########################################
#
# go_To_Location
#  
########################################
sub go_To_Location
{
	print "Location: ".$_[0]."\n\n";
}

########################################
#
# print_Html_File
#  
########################################
sub print_Html_File
{
	local(	@htmldata,
			$htmlline,
			$currfilename);
			
	$currfilename = $_[0];
	
	open(HTMLFILE,"$currfilename");
	@htmldata= <HTMLFILE>;
	close(HTMLFILE);
	
	foreach $htmlline (@htmldata)
	{
	        print "$htmlline";
	}

}

########################################
#
# num_To_String
#  
########################################
sub num_To_String
{
	local(	$numin,
			$numinint,
			$cents,
			$string_out);
			
	$numin = $_[0];
	
	$numinint = int($numin);
	$cents = int(($numin - $numinint+ 0.001)*100);
	
	if ($cents == 100)
	{
		$cents = 0;
		$numinint++;
	}
	
	if ($cents < 1)
	{
		$string_out = "\$".$numinint.".00";
	}
	elsif ($cents <10)
	{
		$string_out = "\$".$numinint.".0".$cents;
	}
	else
	{
		$string_out = "\$".$numinint.".".$cents;
	}

	
	$string_out;

}

########################################
#
# format_Dollar v1.2
#  
########################################
sub format_Dollar
{
	local(	$numin,
			$numinint,
			$cents,
			$tmpcents,
			$string_out,
			$spaces,
			$dont_print_dollar_sign,
			$space_count,
			$comma_counter,
			$comma_num,
			$curr_size,
			@num_split,
			$space_counter,
			$is_negative);
			
	$numin = $_[0];

	if ($numin < 0)
	{
		$is_negative = 1;
		$numin = $numin*-1;
	}
	else
	{
		$is_negative = 0;
	}
	

	$spaces = $_[1];
	$dont_print_dollar_sign = $_[2];
	
	$numinint = int($numin + 0.001);
	
	$tmpcents = $numin - $numinint;
	if ($tmpcents == 0)
	{
		$cents = 0;
	}
	else
	{
		$cents = int(($numin - $numinint + 0.001)*100);
	}
	
	@num_split = split(//,$numinint);
	$comma_num = "";
	$comma_counter = 1;
	for ($space_counter = (scalar(@num_split)-1);$space_counter > 0;$space_counter--)
	{
		if ($comma_counter == 3)
		{
			$comma_num = ",".$num_split[$space_counter].$comma_num;
			$comma_counter = 1;
		}
		else
		{
			$comma_num = $num_split[$space_counter].$comma_num;
			$comma_counter++;
		}
		
		
	}
	
	$comma_num = $num_split[0].$comma_num;
	
	
	$numinint = $comma_num;
	
	if ($cents == 100)
	{
		$cents = 0;
		$numinint++;
	}
	
	if ($cents < 1)
	{
		$string_out = $numinint.".00";
	}
	elsif ($cents <10)
	{
		$string_out = $numinint.".0".$cents;
	}
	else
	{
		$string_out = $numinint.".".$cents;
	}

	$curr_size = length($string_out);

	if ($curr_size < $spaces)
	{
		for ($space_count = 0;$space_count < ($spaces - $curr_size);$space_count++)
		{
			$string_out = '&#32;'.$string_out;
		}
	}
	
	if ($is_negative)
	{
		$string_out = "-".$string_out;
	}
	
	if ( ($dont_print_dollar_sign eq "") || ($dont_print_dollar_sign == 0) )
	{
		$string_out = "\$".$string_out;
	}
	

	return $string_out;

}


########################################
#
# create_Directory
#  
########################################
sub create_Directory
{
	local(	$directory_name,
			$mode);
			
	$directory_name = $_[0];
	$mode = $_[1];
	
	return mkdir($directory_name,$mode);

}


# Perl Routines to Manipulate CGI input
# S.E.Brenner@bioc.cam.ac.uk
# /usr/local/cvsroot/NetworkServices/middleware/device_admin/mei/slw-lib.pl,v 1.1 2005/03/15 21:52:37 erybski Exp
#
# Copyright 1994 Steven E. Brenner  
# Unpublished work.
# Permission granted to use and modify this library so long as the
# copyright above is maintained, modifications are documented, and
# credit is given for any use of the library.
#
# Thanks are due to many people for reporting bugs and suggestions
# especially Meng Weng Wong, Maki Watanabe, Bo Frese Rasmussen,
# Andrew Dalke, Mark-Jason Dominus and Dave Dittrich.

# see http://www.seas.upenn.edu/~mengwong/forms/   or
#     http://www.bio.cam.ac.uk/web/                for more information

# Minimalist http form and script (http://www.bio.cam.ac.uk/web/minimal.cgi):
# if (&MethGet) {
#   print &PrintHeader,
#       '<form method=POST><input type="submit">Data: <input name="myfield">';
# } else {
#   &ReadParse(*input);
#   print &PrintHeader, &PrintVariables(%input);
# }


# MethGet
# Return true if this cgi call was using the GET request, false otherwise
# Now that cgi scripts can be put in the normal file space, it is useful
# to combine both the form and the script in one place with GET used to
# retrieve the form, and POST used to get the result.

sub MethGet {
  return ($ENV{'REQUEST_METHOD'} eq "GET");
}

# ReadParse
# Reads in GET or POST data, converts it to unescaped text, and puts
# one key=value in each member of the list "@in"
# Also creates key/value pairs in %in, using '\0' to separate multiple
# selections

# If a variable-glob parameter (e.g., *cgi_input) is passed to ReadParse,
# information is stored there, rather than in $in, @in, and %in.

sub ReadParse {
    local (*in) = @_ if @_;


  local ($i, $loc, $key, $val);

  # Read in text
  if ($ENV{'REQUEST_METHOD'} eq "GET") {
    $in = $ENV{'QUERY_STRING'};
  } elsif ($ENV{'REQUEST_METHOD'} eq "POST") {
    read(STDIN,$in,$ENV{'CONTENT_LENGTH'});
  }

  @in = split(/&/,$in);

  foreach $i (0 .. $#in) {
    # Convert plus's to spaces
    $in[$i] =~ s/\+/ /g;

    # Split into key and value.  
    ($key, $val) = split(/=/,$in[$i],2); # splits on the first =.

    # Convert %XX from hex numbers to alphanumeric
    $key =~ s/%(..)/pack("c",hex($1))/ge;
    $val =~ s/%(..)/pack("c",hex($1))/ge;

    # Associate key and value
    $in{$key} .= "\0" if (defined($in{$key})); # \0 is the multiple separator
    $in{$key} .= $val;

  }

  return 1; # just for fun
}

# PrintHeader
# Returns the magic line which tells WWW that we're an HTML document

sub PrintHeader {
  return "Content-type: text/html\n\n";
}

# PrintVariables
# Nicely formats variables in an associative array passed as a parameter
# And returns the HTML string.

sub PrintVariables {
  local (%in) = @_;
  local ($old, $out, $output);
  $old = $*;  $* =1;
  $output .=  "<DL COMPACT>";
  foreach $key (sort keys(%in)) {
    foreach (split("\0", $in{$key})) {
      ($out = $_) =~ s/\n/<BR>/g;
      $output .=  "<DT><B>$key</B><DD><I>$out</I><BR>";
    }
  }
  $output .=  "</DL>";
  $* = $old;

  return $output;
}

# PrintVariablesShort
# Nicely formats variables in an associative array passed as a parameter
# Using one line per pair (unless value is multiline)
# And returns the HTML string.


sub PrintVariablesShort {
  local (%in) = @_;
  local ($old, $out, $output);
  $old = $*;  $* =1;
  foreach $key (sort keys(%in)) {
    foreach (split("\0", $in{$key})) {
      ($out = $_) =~ s/\n/<BR>/g;
      $output .= "<B>$key</B> is <I>$out</I><BR>";
    }
  }
  $* = $old;

  return $output;
}

sub printFile
{
	local ($file_name) = @_;
	open(FILE,$file_name);
	while(<FILE>)
	{
		print;
	}
	close(FILE);
}

1; #return true
