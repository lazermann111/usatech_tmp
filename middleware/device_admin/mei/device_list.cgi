#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::MEIHeader;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my %enabled_hash = (	'Y'	=>	'Enabled',
						'N'	=>	'Disabled' );
my $query = OOCGI::OOCGI->new;

my %in = $query->Vars;

USAT::DeviceAdmin::UI::MEIHeader->printHeader();
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html");

my $location = $in{"location_id"};
my $customer = $in{"customer_id"};
my $enabled = $in{"enabled"};


if(length($enabled) == 0)
{
	$enabled = "'Y'";
}

my $start_row = $in{"start_row"};
my $row_count = $in{"row_count"};

if(length($start_row) == 0)
{
	$start_row = 0;
}

if(length($row_count) == 0)
{
	$row_count = 25;
}

my $end_row = ($start_row+$row_count);

my $device_type = 6;

my $list_devices_stmt;
if(length($location) > 0)
{
	$list_devices_stmt = $dbh->prepare("select * from ( select device_id, device_name, device_serial_cd, device_type_desc, to_char(last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), device_type_id, device_active_yn_flag, rownum as row_num, customer_name, location_name, device_setting_value, ROUND((SYSDATE - last_activity_ts), 1) from ( select device.device_id, device.device_name, device.device_serial_cd, device_type.device_type_desc, last_activity_ts, device.device_type_id, device.device_active_yn_flag, customer.customer_name, location.location_name, device_setting.device_setting_value from device, device_type, pos, customer, location.location, device_setting where device.device_type_id = device_type.device_type_id and device.device_id = pos.device_id and pos.location_id = location.location_id and pos.customer_id = customer.customer_id and device.device_id = device_setting.device_id(+) and device_setting.device_setting_parameter_cd(+) = 'Firmware Version' and device_type.device_type_id = :device_type and location.location_id = :location_id and device.device_active_yn_flag in ($enabled) order by device.device_name, device.created_ts desc ) ) where (row_num > :start_row and row_num <= :end_row)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$list_devices_stmt->bind_param(":location_id", $location);
}
elsif(length($customer) > 0)
{
	$list_devices_stmt = $dbh->prepare("select * from ( select device_id, device_name, device_serial_cd, device_type_desc, to_char(last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), device_type_id, device_active_yn_flag, rownum as row_num, customer_name, location_name, device_setting_value, ROUND((SYSDATE - last_activity_ts), 1) from ( select device.device_id, device.device_name, device.device_serial_cd, device_type.device_type_desc, last_activity_ts, device.device_type_id, device.device_active_yn_flag, customer.customer_name, location.location_name, device_setting.device_setting_value from device, device_type, pos, customer, location.location, device_setting where device.device_type_id = device_type.device_type_id and device.device_id = pos.device_id and pos.location_id = location.location_id and pos.customer_id = customer.customer_id and customer.customer_id = :customer_id and device.device_id = device_setting.device_id(+) and device_setting.device_setting_parameter_cd(+) = 'Firmware Version' and device_type.device_type_id = :device_type and device.device_active_yn_flag in ($enabled) order by device.device_name, device.created_ts desc ) ) where (row_num > :start_row and row_num <= :end_row)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$list_devices_stmt->bind_param(":customer_id", $customer);
}
else
{
	$list_devices_stmt = $dbh->prepare("select * from ( select device_id, device_name, device_serial_cd, device_type_desc, to_char(last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), device_type_id, device_active_yn_flag, rownum as row_num, customer_name, location_name, device_setting_value, ROUND((SYSDATE - last_activity_ts), 1) from ( select device.device_id, device.device_name, device.device_serial_cd, device_type.device_type_desc, last_activity_ts, device.device_type_id, device.device_active_yn_flag, customer.customer_name, location.location_name, device_setting.device_setting_value from device, device_type, pos, customer, location.location, device_setting where device.device_type_id = device_type.device_type_id and device.device_id = pos.device_id and pos.location_id = location.location_id and pos.customer_id = customer.customer_id and device.device_id = device_setting.device_id(+) and device_setting.device_setting_parameter_cd(+) = 'Firmware Version' and device_type.device_type_id = :device_type and device.device_active_yn_flag = 'Y' order by device.device_name, device.created_ts desc ) ) where (row_num > :start_row and row_num <= :end_row)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
}

$list_devices_stmt->bind_param(":device_type", $device_type);

$list_devices_stmt->bind_param(":start_row", $start_row);
$list_devices_stmt->bind_param(":end_row", $end_row);
$list_devices_stmt->execute();

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">

 <tr>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Serial Number</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">EV Number</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Customer</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Location</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Last Activity</th>
 </tr>
";

my $result_count = 0;

while (my @data = $list_devices_stmt->fetchrow_array()) 
{
	$result_count++;
	
	print " <tr>
	         <td style=\"font-size: 10pt;\"><a style=\"font-size: 10pt;\" href=\"profile.cgi?serial_number=$data[2]\">$data[2]</a></td>
	         <td style=\"font-size: 10pt;\">$data[1]</td>
	         <td style=\"font-size: 10pt;\">$data[8]</td>
	         <td style=\"font-size: 10pt;\">$data[9]</td>
	         <td style=\"font-size: 10pt;\" nowrap>";
	         
		     if($data[11]  > 5) { print "<font color=\"red\"><b>$data[4]<br>($data[11] days)</b>"; }
			 elsif($data[11]  > 2) { print "<font color=\"#DDCC00\"><b>$data[4]<br>($data[11] days)</b>"; }
		     else { print "<font color=\"green\"><b>$data[4]</b>"; }
		
			print"
	         &nbsp;</font></td>
	        </tr>
	      ";
}

$list_devices_stmt->finish();

print "<tr><th bgcolor=\"#C0C0C0\" colspan=\"8\" align=\"center\">";
print "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"><tr><td width=\"26%\">&nbsp;</td><td width=\"16%\" align=\"left\">\n";

if($start_row > 0)
{
	if(length($location) > 0)
	{
		print "<a href=\"device_list.cgi?location_ide=$location&enabled=$enabled&start_row=" . ($start_row-$row_count) . "&row_count=$row_count\">&lt; Previous</a>\n";	
	}
	elsif(length($customer) > 0)
	{
		print "<a href=\"device_list.cgi?customer_id=$customer&enabled=$enabled&start_row=" . ($start_row-$row_count) . "&row_count=$row_count\">&lt; Previous</a>\n";
	}
	else
	{
		print "<a href=\"device_list.cgi?enabled=$enabled&start_row=" . ($start_row-$row_count) . "&row_count=$row_count\">&lt; Previous</a>\n";
	}
}
else
{
	print "&nbsp;";
}

print "</td><td width=\"16%\" align=\"center\">\n";

if($result_count >= $row_count)
{
	print " | " . ($start_row+1) . " - " . ($end_row) . " | ";
}
else
{
	print " | " . ($start_row+1) . " - " . ($result_count+$start_row) . " | ";
}

print "</td><td width=\"16%\" align=\"right\">\n";

if($result_count >= $row_count)
{
	if(length($location) > 0)
	{
		print "<a href=\"device_list.cgi?location_ide=$location&enabled=$enabled&start_row=$end_row&row_count=$row_count\">Next &gt;</a>";
	}
	elsif(length($customer) > 0)
	{
		print "<a href=\"device_list.cgi?customer_id=$customer&enabled=$enabled&start_row=$end_row&row_count=$row_count\">Next &gt;</a>";
	}
	else
	{
		print "<a href=\"device_list.cgi?enabled=$enabled&start_row=$end_row&row_count=$row_count\">Next &gt;</a>";
	}
	
}
else
{
	print "&nbsp;";
}

print "</td><td width=\"26%\">&nbsp;</td></tr><form method=\"get\" action=\"device_list.cgi\"><tr><td colspan=\"5\" align=\"center\"><hr> &nbsp;Rows Per Page: <select name=\"row_count\"><option value=\"25\">25</option><option value=\"50\"";

if($row_count == 50) {
	print " selected";
}

print ">50</option><option value=\"100\"";

if($row_count == 100) {
	print " selected";
}

print ">100</option><option value=\"250\"";

if($row_count == 250) {
	print " selected";
}

print ">250</option><option value=\"500\"";

if($row_count == 500) {
	print " selected";
}

print ">500</option><option value=\"1000\"";

if($row_count == 1000) {
	print " selected";
}

print ">1000</option></select> <input type=\"submit\" name=\"action\" value=\"Go\">";

if(length($location) > 0)
{
	print "<input type=\"hidden\" name=\"location_ide\" value=\"$location\"><input type=\"hidden\" name=\"enabled\" value=\"$enabled\">";
}
elsif(length($customer) > 0)
{
	print "<input type=\"hidden\" name=\"customer_id\" value=\"$customer\"><input type=\"hidden\" name=\"enabled\" value=\"$enabled\">";
}

print "</td></tr></form></table>\n";

print "
</th></tr></table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");




