#!/usr/local/USAT/bin/perl

use strict;

use USAT::DeviceAdmin::UI::MEIHeader;

use OOCGI::OOCGI;
use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %in = $query->Vars;

USAT::DeviceAdmin::UI::MEIHeader->printHeader();
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html");

my $ssn = $in{"ssn"};
if(length($ssn) == 0)
{
	print "Required parameter not found: ssn\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
	exit;
}

if(!(substr($ssn,0,2) eq 'M1'))
{
	print "Serial number is restricted to M1 devices only!\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
	exit;
}

my $rowcount = $in{"rowcount"};
if(length($rowcount) == 0)
{
	$rowcount = 25;
}

my %call_type_hash = 
( 
	'U' => 'Unknown',
	'B' => 'Batch',
	'A' => 'Auth',
	'C' => 'Combo',
);

my %status_hash = 
( 
	'X' => 'Unknown',
	'S' => 'Success',
	'U' => 'Failure',
	'I' => 'In-Progress',
);

my %card_type_hash = 
( 
	'C' => 'Credit',
	'R' => 'RFID',
	'M' => 'Cash',
	'P' => 'Passcard',
	'S' => 'Passcard',
);

my %yesno_hash = 
( 
	'Y' => 'Yes',
	'N' => 'No',
);

my %call_type_color_hash = 
( 
	'U' => '#FFFFFF',
	'B' => '#EEEECC',
	'A' => '#9FBFDF',
	'C' => '#D6E7D3',
);

my %status_color_hash = 
( 
	'X' => '#000000',
	'S' => '#009933',
	'U' => '#CC3300',
	'I' => '#996600',
);

#																			0													1														2							3				4				5					6					7					8					9				10							11						12					13				14					15					16					17					18						19					20				21				22				23			24				25		26
#my $call_in_stmt = $dbh->prepare("select * from (select to_char(call_in_start_ts, 'MM/DD/YYYY HH24:MI:SS'), to_char(call_in_finish_ts, 'MM/DD/YYYY HH24:MI:SS'), ((call_in_finish_ts-call_in_start_ts)*24*60), call_in_type, call_in_status, credit_trans_count, credit_trans_total, cash_trans_count, cash_trans_total, passcard_trans_count, passcard_trans_total, device_sent_config_flag, server_sent_config_flag, auth_card_type, auth_amount, auth_approved_flag, inbound_message_count, inbound_byte_count, outbound_message_count, outbound_byte_count, dex_received_flag, dex_file_size, customer_name, location_name, network_layer, ip_address, modem_id  from device_call_in_record where serial_number = :ssn order by call_in_start_ts desc) where rownum <= $rowcount") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
my $call_in_stmt = $dbh->prepare("select to_char(call_in_start_ts, 'MM/DD/YYYY HH24:MI:SS'), to_char(call_in_finish_ts, 'MM/DD/YYYY HH24:MI:SS'), ((call_in_finish_ts-call_in_start_ts)*24*60), call_in_type, call_in_status, credit_trans_count, credit_trans_total, cash_trans_count, cash_trans_total, passcard_trans_count, passcard_trans_total, device_sent_config_flag, server_sent_config_flag, auth_card_type, auth_amount, auth_approved_flag, inbound_message_count, inbound_byte_count, outbound_message_count, outbound_byte_count, dex_received_flag, dex_file_size, customer_name, location_name, network_layer, ip_address, modem_id  from device_call_in_record where serial_number = :ssn order by call_in_start_ts desc") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$call_in_stmt->bind_param(":ssn", $ssn);
$call_in_stmt->execute();

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\">
 <tr>
  <th bgcolor=\"#A0A0A0\" colspan=\"17\">Call-In Record for <a href=\"profile.cgi?serial_number=$ssn\"><font color=\"black\">$ssn</font></a></th>
 </tr>
 <tr>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Call<br>Reason</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Finish<br>Status</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Start<br>Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">End<br>Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Elapsed<br>Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Credit<br>Count/Total</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Cash<br>Count/Total</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Passcard<br>Count/Total</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Dex<br>Rec/Size</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Config Sent<br>Client/Server</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Auth Request<br>Type/Amt/App</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Inbound Msgs/Bytes</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Outbound Msgs/Bytes</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Customer</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Location</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Network<br>Layer</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\" nowrap>IP Address/<br>Modem ID</th>
 </tr>
";

my $count = 0;
while (my @data = $call_in_stmt->fetchrow_array()) 
{
	$count++;
	my $bgcolor = $call_type_color_hash{$data[3]};
	
	print " <tr>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">$call_type_hash{$data[3]}&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\"><font color=\"$status_color_hash{$data[4]}\">$status_hash{$data[4]}</font>&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[0]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[1]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">" . (sprintf "%.2f", $data[2]) . "&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">" . "$data[5]/\$$data[6]" . "&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">" . "$data[7]/\$$data[8]" . "&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">" . "$data[9]/\$$data[10]" . "&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ("$yesno_hash{$data[20]}" . ($data[20] eq 'Y' ? "/$data[21]" : "")) . "&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">" . "$yesno_hash{$data[11]}/$yesno_hash{$data[12]}" . "&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">" . "$card_type_hash{$data[13]}/\$$data[14]/$yesno_hash{$data[15]}" . "&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[16]/$data[17]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[18]/$data[19]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[22]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[23]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">$data[24]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap bgcolor=\"$bgcolor\">" . ((length($data[26]) > 0) ? $data[26] : $data[25]) . "&nbsp;</td>
	        </tr>
	      ";
	
	if($count == $rowcount)
	{
		last;
	}
}

$call_in_stmt->finish();

if($count == 0)
{
	print " <tr><td style=\"font-size: 10pt;\" colspan=\"17\" align=\"center\"><font color=\"red\">&nbsp;No Data!</font></td></tr>\n";
}

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
