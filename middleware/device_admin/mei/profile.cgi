#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::MEIHeader;
require Text::CSV;

						
use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %in = $query->Vars;

USAT::DeviceAdmin::UI::MEIHeader->printHeader();
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html");

my $ev_number = $in{"ev_number"};
my $serial_number = $in{"serial_number"};
my $device_id = $in{"device_id"};
if(length($ev_number) == 0 && length($serial_number) == 0 && length($device_id) == 0)
{
	print "Required Parameter Not Found: ev_number or serial_number";
    USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
	exit;
}

$serial_number = &padM1($serial_number);

my $counter_count = $in{"counter_count"};
if(length($counter_count) == 0)
{
	$counter_count = 7;
}

my $get_device_info_stmt;
if(defined $device_id)
{
	$get_device_info_stmt = $dbh->prepare("SELECT dev.device_id, dev.device_name, dev.device_serial_cd, TO_CHAR(dev.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), TO_CHAR(dev.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), devt.device_type_desc, dev.device_active_yn_flag, dev.encryption_key, dev.device_type_id, pos.location_id, pos.customer_id, TO_CHAR(lci.last_call_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - lci.last_call_ts), 1), TO_CHAR(laci.last_ack_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - laci.last_ack_ts), 1), TO_CHAR(lsb.last_success_bat_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - lsb.last_success_bat_ts), 1) FROM device.device dev, device.device_type devt, pos, ( SELECT dv.device_id, MAX(dcir.call_in_start_ts) last_call_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_id = :device_id GROUP BY dv.device_id ) lci, ( SELECT dv.device_id, MAX(dcir.call_in_start_ts) last_ack_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_id = :device_id AND dcir.inbound_message_count > 1 GROUP BY dv.device_id ) laci, ( SELECT dv.device_id, MAX(dcir.call_in_start_ts) last_success_bat_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_id = :device_id AND dcir.call_in_type IN ('B', 'C') AND dcir.call_in_status = 'S' GROUP BY dv.device_id ) lsb WHERE dev.device_type_id = devt.device_type_id
AND dev.device_type_id = 6 AND dev.device_id = pos.device_id AND dev.device_id = lci.device_id (+) AND dev.device_id = laci.device_id (+) AND dev.device_id = lsb.device_id (+) AND dev.device_id = :device_id ") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_device_info_stmt->bind_param(":device_id", $device_id);
}
elsif(defined $ev_number)
{
	$get_device_info_stmt = $dbh->prepare("SELECT dev.device_id, dev.device_name, dev.device_serial_cd, TO_CHAR(dev.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), TO_CHAR(dev.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), devt.device_type_desc, dev.device_active_yn_flag, dev.encryption_key, dev.device_type_id, pos.location_id, pos.customer_id, TO_CHAR(lci.last_call_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - lci.last_call_ts), 1), TO_CHAR(laci.last_ack_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - laci.last_ack_ts), 1), TO_CHAR(lsb.last_success_bat_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - lsb.last_success_bat_ts), 1) FROM device.device dev, device.device_type devt, pos, ( SELECT dv.device_id,  MAX(dcir.call_in_start_ts) last_call_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_name = :ev_number GROUP BY dv.device_id ) lci, ( SELECT dv.device_id, MAX(dcir.call_in_start_ts) last_ack_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_name = :ev_number AND dcir.inbound_message_count > 1 GROUP BY dv.device_id ) laci, ( SELECT dv.device_id, MAX(dcir.call_in_start_ts) last_success_bat_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_name = :ev_number AND dcir.call_in_type IN ('B', 'C') AND dcir.call_in_status = 'S' GROUP BY dv.device_id ) lsb WHERE dev.device_type_id = devt.device_type_id
AND dev.device_type_id = 6 AND dev.device_id = pos.device_id AND dev.device_id = lci.device_id (+) AND dev.device_id = laci.device_id (+) AND dev.device_id = lsb.device_id (+) AND dev.device_name = :ev_number AND dev.device_active_yn_flag = :active") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_device_info_stmt->bind_param(":ev_number", $ev_number);
	$get_device_info_stmt->bind_param(":active", 'Y');
}
else
{
	$get_device_info_stmt = $dbh->prepare("SELECT dev.device_id, dev.device_name, dev.device_serial_cd, TO_CHAR(dev.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), TO_CHAR(dev.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), devt.device_type_desc, dev.device_active_yn_flag, dev.encryption_key, dev.device_type_id, pos.location_id, pos.customer_id, TO_CHAR(lci.last_call_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - lci.last_call_ts), 1), TO_CHAR(laci.last_ack_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - laci.last_ack_ts), 1), TO_CHAR(lsb.last_success_bat_ts, 'MM/DD/YYYY HH:MI:SS AM'), ROUND((SYSDATE - lsb.last_success_bat_ts), 1) FROM device.device dev, device.device_type devt, pos, ( SELECT dv.device_id,  MAX(dcir.call_in_start_ts) last_call_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_serial_cd = :serial_number GROUP BY dv.device_id ) lci, ( SELECT dv.device_id,  MAX(dcir.call_in_start_ts) last_ack_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_serial_cd = :serial_number AND dcir.inbound_message_count > 1 GROUP BY dv.device_id ) laci, ( SELECT dv.device_id, MAX(dcir.call_in_start_ts) last_success_bat_ts FROM device_call_in_record dcir, device.device dv WHERE dcir.serial_number = dv.device_serial_cd AND dv.device_serial_cd = :serial_number AND dcir.call_in_type IN ('B', 'C') AND dcir.call_in_status = 'S' GROUP BY dv.device_id ) lsb WHERE dev.device_type_id = devt.device_type_id
AND dev.device_type_id = 6 AND dev.device_id = pos.device_id AND dev.device_id = lci.device_id (+) AND dev.device_id = laci.device_id (+) AND dev.device_id = lsb.device_id (+) AND dev.device_serial_cd = :serial_number AND dev.device_active_yn_flag = :active") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_device_info_stmt->bind_param(":serial_number", $serial_number);
	$get_device_info_stmt->bind_param(":active", 'Y');
}

$get_device_info_stmt->execute();
my @device_data = $get_device_info_stmt->fetchrow_array();
if(!@device_data)
{
	print "Device not found!";
    USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
	exit;
}

$get_device_info_stmt->finish();

$device_id = $device_data[0];
$ev_number = $device_data[1];
$serial_number = $device_data[2];
my $location_id = $device_data[9];
my $customer_id = $device_data[10];
my $device_type_id = $device_data[8];



## Get GPRS modem info if available
my ($iccid, $iccid_date);
my $get_gprs_stmt = $dbh->prepare("select iccid, to_char(assigned_ts, 'MM/DD/YYYY') from gprs_device where device_id = :device_id order by assigned_ts desc") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_gprs_stmt->bind_param(":device_id", $device_id);
$get_gprs_stmt->execute();
my @gprs_data = $get_gprs_stmt->fetchrow_array();
if(defined $get_gprs_stmt)
{
	$iccid = $gprs_data[0];
	$iccid_date = $gprs_data[1];
}
$get_gprs_stmt->finish();

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">MEI Device Profile</th>
 </tr>
 <tr>
  <td nowrap>Serial Number</td>
  <td width=\"50%\">$device_data[2]&nbsp;</td>
  <td nowrap>Last Activity</td>
  <td width=\"50%\">$device_data[4]&nbsp;</td>
 </tr>
 ";
 
if(defined $iccid)
{
	print "
 <tr>
  <td nowrap>SIM Card ICCID</td>
  <td width=\"50%\"><a href=\"gprsweb?iccid=$iccid\">$iccid</a>&nbsp;</td>
  <td nowrap>SIM Assigned Date</td>
  <td width=\"50%\">$iccid_date&nbsp;</td>
 </tr>
 ";
}

print "
 <tr>
  <td nowrap>Customer</td>
  <td width=\"50%\">";
    
my $customers_stmt = $dbh->prepare("select customer_id, customer_name from customer where customer_id = :customerid order by customer_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$customers_stmt->bind_param(":customerid", $customer_id);
$customers_stmt->execute();
while (my @data = $customers_stmt->fetchrow_array()) 
{
	print " $data[1]";
}
$customers_stmt->finish();

print "
  </td>
  <td nowrap>Location</td>
  <td width=\"50%\">";
    
my $locations_stmt = $dbh->prepare("select location_id, location_name, location_city, location_state_cd, location_country_cd from location.location where location_id = :locationid order by location_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$locations_stmt->bind_param(":locationid", $location_id);
$locations_stmt->execute();
while (my @data = $locations_stmt->fetchrow_array()) 
{
	print " $data[1] - $data[2], $data[3] $data[4]";
}
$locations_stmt->finish();

print "
  </td>
 </tr>
</table>
";

print "
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <td width=\"33%\">Last Call-In Date</td>
  <td width=\"34%\">Last Ack Date (2nd msg sent)</td>
  <td width=\"33%\">Last Batch Success Date</td>
 </tr>
 <tr>
  <td>";
     
if ($device_data[12] > 5) { print "<font color=\"red\"><b>$device_data[11] ($device_data[12] days)</b>"; }
elsif ($device_data[12] > 2) { print "<font color=\"#DDCC00\"><b>$device_data[11] ($device_data[12] days)</b>"; }
else { print "<font color=\"green\"><b>$device_data[11]</b>"; }

print "
  &nbsp;</td>
  <td>";
     
if ($device_data[14] > 5) { print "<font color=\"red\"><b>$device_data[13] ($device_data[14] days)</b>"; }
elsif ($device_data[14] > 2) { print "<font color=\"#DDCC00\"><b>$device_data[13] ($device_data[14] days)</b>"; }
else { print "<font color=\"green\"><b>$device_data[13]</b>"; }

print "
  &nbsp;</td>
  <td>";
     
if ($device_data[16] > 5) { print "<font color=\"red\"><b>$device_data[15] ($device_data[16] days)</b>"; }
elsif ($device_data[16] > 2) { print "<font color=\"#DDCC00\"><b>$device_data[15] ($device_data[16] days)</b>"; }
else { print "<font color=\"green\"><b>$device_data[15]</b>"; }

print "
  &nbsp;</td>
 </tr>
</table>
";

print "
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">

<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
   <form method=\"get\" action=\"activity.cgi\">
   <input type=\"hidden\" name=\"ssn\" value=\"$serial_number\">
  <td align=\"center\" width=\"35%\">
   <input type=\"submit\" value=\"List Calls\">
   <input type=\"text\" name=\"rowcount\" value=\"25\" size=\"3\">
  </td>
  </form>
 </tr>
</table>
";

print "
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">

<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <form method=\"get\" action=\"profile.cgi\">
  <input type=\"hidden\" name=\"serial_number\" value=\"$serial_number\">
  <th colspan=\"2\" colspan=\"4\" bgcolor=\"#C0C0C0\">Counters ( <input type=\"text\" name=\"counter_count\" size=\"2\" value=\"$counter_count\"> Most Recent ) <input type=\"submit\" value=\"List\"></th>
  </form>
 </tr>
 <tr>
  <td>
   <table border=\"0\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
    <tr>
";

my @counter_names = ("CASHLESS_TRANSACTION","CASHLESS_MONEY","CURRENCY_TRANSACTION","CURRENCY_MONEY","PASSCARD_TRANSACTION","PASSCARD_MONEY","TOTAL_SESSIONS","TOTAL_BYTES");

my $countercount = 0;

foreach my $counter_name (@counter_names)
{
	if($countercount =~ m/0|2|4|6|8/)
	{
		print "<td><table border=\"1\" cellpadding=\"2\" cellspacing=\"0\">";
	}
	
	print "
    <tr>
     <td colspan=\"2\">$counter_name</td>
    </tr>
    ";
    
	my $get_counters_stmt = $dbh->prepare("select * from ( select dc.device_counter_parameter, dc.device_counter_value, to_char(dc.created_ts, 'MM/DD/YYYY HH:MI:SS AM') from device.device d, device.device_counter dc where d.device_name = :device_name and d.device_id = dc.device_id and dc.device_counter_parameter = :counter_name order by dc.created_ts desc ) where rownum <= :counter_count") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_counters_stmt->bind_param(":counter_count", $counter_count);
	$get_counters_stmt->bind_param(":device_name", $ev_number);
	$get_counters_stmt->bind_param(":counter_name", $counter_name);
	$get_counters_stmt->execute();
	while(my @counters_data = $get_counters_stmt->fetchrow_array())
	{
		print "
	     <tr>
    	  <td>$counters_data[2]</td>
    	  <td>$counters_data[1]</td>
	     </tr>
    	";
	}
	
	$get_counters_stmt->finish();
	
	if($countercount =~ m/1|3|5|7|9/)
	{
		print "</table></td>";
	}
	
	$countercount++;
}

print "
  </tr>
 </table>
</table>
";


$dbh->disconnect;

USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");

sub padM1
{
	my ($num) = @_;
	if(length($num) <= 6)
	{
		$num = substr('0000000', 0, (6-length($num))) . $num;
		$num = 'M1' . $num;
	}
	return $num;
}


