#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");

&read_Parse();
&print_Header();

print '
<html>
 <head>
  <title>USA Technologies SIM Card Admin</title>
  <style type="text/css">
   TD		{ font-size: 10pt; color: #000000; font-weight:normal;	font-family:\'Courier New\', monospace; }
   .head	{ font-size: 14pt; color: #000000; font-weight:bold;	font-family:\'Courier New\', monospace; }
  </style>
 </head>
 
 <frameset cols="120,*" frameborder="no" border="0">
  <frame src="menu.cgi" name="header" scrolling="no">
';

my $iccid = $in{"iccid"};
if(length $iccid > 0)
{
	print "<frame src=\"iccid_detail.cgi?iccid=$iccid\" name=\"body\">\n";
}
else
{
	print '<frame src="search.cgi" name="body">\n';
}

print '
 </frameset> 

</html>
';
