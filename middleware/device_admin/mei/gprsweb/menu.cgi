#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

&read_Parse();
&print_Header();

print "
<html>
 <head>
  <title>USA Technologies GPRS Device Admin</title>
  <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
 </head>
 <body bgcolor=\"#FFFFFF\">
  <table border=\"0\" cellspacing=\"1\" cellpadding=\"0\" width=\"100%\" class=\"menu\">
   <tr>
    <td align=\"center\"><a href=\"search.cgi\" target=\"body\">Search</a></td>   
   </tr>
   <tr>
    <td align=\"center\"><a href=\"activate_search.cgi\" target=\"body\">Activate</a></td>
   </tr>
   <tr>
    <td align=\"center\"><a href=\"usage.cgi\" target=\"body\">Usage</a></td>
   </tr>
  </table>
  <table border=\"0\" cellspacing=\"1\" cellpadding=\"0\" width=\"100%\" height=\"100%\">
   <tr>
    <td align=\"center\" valign=\"top\" bgcolor=\"#C0C0C0\">
     <br>
     <br>
     <br>
     <span class=\"subhead\">Status Key</span><br>
     <table border=\"0\" cellspacing=\"1\" cellpadding=\"0\" width=\"90%\" bgcolor=\"#FFFFFF\">
      <tr><td align=\"center\" bgcolor=\"#99CCCC\">Allocated</td></tr>
      <tr><td align=\"center\" bgcolor=\"#FFFF99\">Activation Pending</td></tr>
      <tr><td align=\"center\" bgcolor=\"#99FF99\">Activated</td></tr>
      <tr><td align=\"center\" bgcolor=\"#FF9999\">Assigned</td></tr>
     </table>
    </td>
   </tr>
  </table>
 </body>
</html>
";
