#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");
require("header_footer.pl");

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

&read_Parse();
&print_Header();

#&printFile("header.html");
&htmlHeader;

my $iccid_header_list = &list_iccid_headers();
my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
my $start_date = sprintf("%02d/%02d/%04d", $mon + 1, $mday-1, $year + 1900);
my $end_date = sprintf("%02d/%02d/%04d", $mon + 1, $mday-1, $year + 1900);
my $month = sprintf("%02d/%04d", $mon, $year + 1900);

print '
<span class="subhead">Show Specific Sim Card Usage</span><br>
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <form method="get" action="usage_results.cgi">
 <tr>
  <td>ICCID: </td>
  <td>
   <table border="1" cellspacing="0" cellpadding="2">
    <tr>
     <td>First 16 digits</td>
     <td>Last 4 digits</td>
    </tr>
    <tr>
     <td>
      <select name="iccid_first">' . $iccid_header_list . '</select>
     </td>
     <td>
      <input type="text" name="iccid_last" size="7" maxlength="4">
     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type="submit" name="action" value="Show Specific"></td>
 </tr>
 </form>
</table>
&nbsp;<br>
';

print "
<span class=\"subhead\">Show All Sim Card Usage</span><br>
<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\" width=\"50%\">
 <form method=\"get\" action=\"usage_results.cgi\">
  <td>Start Date:</td>
  <td><input type=\"text\" name=\"start_date\" size=\"20\" value=\"$start_date\" maxlength=\"20\">
 </tr>
 <tr>
  <td>End Date:</td>
  <td><input type=\"text\" name=\"end_date\" size=\"20\" value=\"$end_date\" maxlength=\"20\">
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type=\"submit\" name=\"action\" value=\"Show All\"></td>
 </tr>
 </form>
</table>
&nbsp;<br>
";

#&printFile("footer.html");
&htmlFooter;

sub list_iccid_headers
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	my $list;
	my $search_stmt = $dbh->prepare("select distinct substr(iccid, 0, 16) from gprs_device where billable_to_name = 'MEI'") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->execute() or die "Couldn't execute statement: " . $exists_stmt->errstr;
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}
		
		$list = $list . "<option value=\"$data[0]\">" . USAT::DeviceAdmin::GPRS::format_iccid($data[0]) . " </option>";
	}
	
	$search_stmt->finish();
	$dbh->disconnect;
	
	return $list;
}

