#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");
require("header_footer.pl");

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

&read_Parse();
&print_Header();

#&printFile("header.html");
&htmlHeader;

my $iccid_header_list = &list_iccid_headers();
my $billable_name_list = &list_billable_names();
my $allocated_name_list = &list_allocated_names();

print "<span class=\"subhead\">Activate Search</span><br>\n";
print '
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <form method="get" action="activate_search_results.cgi">
 <tr>
  <td>First ICCID: </td>
  <td>
   <table border="1" cellspacing="0" cellpadding="2">
    <tr>
     <td>First 16 digits</td>
     <td>Last 4 digits</td>
    </tr>
    <tr>
     <td>
      <select name="iccid_first">' . $iccid_header_list . '</select>
     </td>
     <td>
      <input type="text" name="iccid_last" size="7" maxlength="4">
     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td>SIM Count: </td>
  <td><input type="text" name="count" size="4"></td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type="submit" name="action" value="List SIMs"></td>
 </tr>
 </form>
</table>
&nbsp;<br>
';

print '
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <form method="get" action="activate_search_results.cgi">
 <tr>
  <td>Allocated To</td>
  <td>
   <select name="allocated_to_name">' . $allocated_name_list . '</select>
  </td>
 </tr>
 <tr>
  <td>SIM Count: </td>
  <td><input type="text" name="count" size="4"></td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type="submit" name="action" value="Search SIMs"></td>
 </tr>
 </form>
</table>
&nbsp;<br>
';

#&printFile("footer.html");
&htmlFooter;

sub list_iccid_headers
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	my $list;
	my $search_stmt = $dbh->prepare("select distinct substr(iccid, 0, 16) from gprs_device where billable_to_name = 'MEI'") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->execute() or die "Couldn't execute statement: " . $exists_stmt->errstr;
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}
		
		$list = $list . "<option value=\"$data[0]\">" . substr($data[0],0,4) . " " . substr($data[0],4,4) . " " . substr($data[0],8,4) . " " . substr($data[0],12,4) . " </option>";
	}
	
	$search_stmt->finish();
	$dbh->disconnect;
	
	return $list;
}

sub list_billable_names
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	my $list = '<option value="">Any</option>';
	my $search_stmt = $dbh->prepare("select distinct billable_to_name from gprs_device where billable_to_name = 'MEI'") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->execute() or die "Couldn't execute statement: " . $exists_stmt->errstr;
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}
		
		$list = $list . "<option value=\"$data[0]\">$data[0]</option>\n";
	}
	
	$search_stmt->finish();
	$dbh->disconnect;
	
	return $list;
}

sub list_allocated_names
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	my $list = '<option value="">Any</option>';
	my $search_stmt = $dbh->prepare("select distinct allocated_to from gprs_device where billable_to_name = 'MEI'") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->execute() or die "Couldn't execute statement: " . $exists_stmt->errstr;
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}
		
		$list = $list . "<option value=\"$data[0]\">$data[0]</option>\n";
	}
	
	$search_stmt->finish();
	$dbh->disconnect;
	
	return $list;
}
