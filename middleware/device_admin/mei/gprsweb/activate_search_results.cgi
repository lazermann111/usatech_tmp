#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");
require("header_footer.pl");

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

&read_Parse();
&print_Header();

#&printFile("header.html");
&htmlHeader;

my %status_codes_hash = (	'1'	=>	['Registered', '#9999FF'],
							'2'	=>	['Allocated', '#99CCCC'],
							'3'	=>	['Activation Pending', '#FFFF99'],
							'4'	=>	['Activated', '#99FF99'],
							'5'	=>	['Assigned', '#FF9999']
						);
						
my $input_validated = 1;

my $action = $in{"action"};
if($action eq 'List SIMs')
{
	my $iccid_first = $in{"iccid_first"};
	my $iccid_last = $in{"iccid_last"};
	my $count = $in{"count"};
	
	if(length($iccid_first) != 16)
	{
		print "<span class=\"error\"><li></li>Invalid or Missing First 16 Digits!</span><br>\n";
		$input_validated = 0;
	}
	
	if(length($iccid_last) < 3)
	{
		print "<span class=\"error\"><li></li>Invalid or Missing Last 4 Digits!</span><br>\n";
		$input_validated = 0;
	}
	
	if(length($count) <= 0)
	{
		print "<span class=\"error\"><li></li>SIM Count is Required!</span><br>\n";
		$input_validated = 0;
	}
	
	$iccid_last = substr($iccid_last,0,3) if(length($iccid_last) == 4);
	
	my $iccid = "$iccid_first" . "$iccid_last" . "0";
	
	if($input_validated)
	{
		my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
		my $dbh = $DATABASE->{handle};
		&execute_list($dbh, $iccid, $count);
		$dbh->disconnect;
	}
}
elsif($action eq 'Search SIMs')
{
	my $billable_to_name = "MEI";
	my $allocated_to_name = $in{"allocated_to_name"};
	my $count = $in{"count"};
	
	$billable_to_name = undef unless length($billable_to_name) > 0;
	$allocated_to_name = undef unless length($allocated_to_name) > 0;
	$count = 999999 unless length($count) > 0;
	
	if($input_validated)
	{
		my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
		my $dbh = $DATABASE->{handle};
		&execute_search($dbh, $billable_to_name, $allocated_to_name, $count);
		$dbh->disconnect;
	}
}
else
{
	print "<span class=\"error\"><li></li>Invalid Action! ($action)</span><br>\n";
}


#&printFile("footer.html");
&htmlFooter;

sub execute_list
{
	my ($dbh, $iccid, $count) = @_;
	
	print "<span class=\"subhead\">Activate Search Results</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<form method=\"post\" action=\"activate_func.cgi\">\n";	
	print "<tr><th>Include?</th><th>ICCID</th><th>Status</th><th>Allocated<br>Date</th><th>Allocated<br>To</th><th>Billable<br>To</th></tr>\n";
	print "<form method=\"post\" action=\"allocate_func.cgi\">\n";	
	#												0			1	2				3				4				5			6				7				8
	my $search_stmt = $dbh->prepare("select gprs_device_id, iccid, imsi, gprs_device_state_id, to_char(allocated_ts, 'MM/DD/YYYY'), allocated_by, allocated_to, billable_to_name, rate_plan_name from (select gprs_device_id, iccid, imsi, gprs_device_state_id, allocated_ts, allocated_by, allocated_to, billable_to_name, rate_plan_name from gprs_device where billable_to_name = 'MEI' and iccid >= :iccid order by ICCID ASC) where rownum <= :count") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->bind_param(":iccid", $iccid);
	$search_stmt->bind_param(":count", $count);
	$search_stmt->execute() or die "Couldn't execute statement: " . $search_stmt->errstr;
	
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			print "NO DATA!\n";
			last;
		}

		print "<tr><td align=\"center\">" . ($data[3] == 2 ? "<input type=\"checkbox\" name=\"iccid\" value=\"$data[1],$data[6],$data[7],$data[8]\" checked>" : "&nbsp;") . "</td>\n";
#		print "<tr><td align=\"center\"><input type=\"checkbox\" name=\"iccid\" value=\"$data[1],$data[6],$data[7]\" checked></td>\n";
		print "<td nowrap><a href=\"iccid_detail.cgi?iccid=$data[1]\">" . USAT::DeviceAdmin::GPRS::format_iccid($data[1]) . "</a></td>\n";
		print "<td bgcolor=\"$status_codes_hash{$data[3]}[1]\">$status_codes_hash{$data[3]}[0]</td>\n";
		print "<td>$data[4]&nbsp;</td>\n";
		print "<td>$data[6]&nbsp;</td>\n";
		print "<td>$data[7]&nbsp;</td></tr>\n";
	}
	
	my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
	my $time_str = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	
	print "</table>\n";
	print "&nbsp;<br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"3\" width=\"790\">\n";
	print "<tr><td>Effective Date: </td><td><input type=\"text\" name=\"effective_ts\" size=\"11\" value=\"$time_str\" maxlength=\"10\"></td></tr>\n";
	print "<tr><td>Activated By: </td><td><input type=\"text\" name=\"activated_by\" size=\"40\"></td></tr>\n";
	print "<tr><td>Activated Notes: </td><td><textarea cols=\"60\" rows=\"5\" name=\"activated_notes\"></textarea></td></tr>\n";
	print "<tr><td>&nbsp;</td><td><input type=\"submit\" value=\"Activate\"></td></tr>\n";
	print "</form>\n";
	print "</table>\n";
	
	$search_stmt->finish();
}

sub execute_search
{
	my ($dbh, $billable_to_name, $allocated_to_name, $maxcount) = @_;
	print "<span class=\"subhead\">Activate Search Results</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<form method=\"post\" action=\"activate_func.cgi\">\n";	
	print "<tr><th>Include?</th><th>ICCID</th><th>Status</th><th>Allocated<br>Date</th><th>Allocated<br>To</th><th>Billable<br>To</th></tr>\n";
	print "<form method=\"post\" action=\"allocate_func.cgi\">\n";	
	#												0			1	2				3				4				5			6				7				8
#	my $search_stmt = $dbh->prepare("select gprs_device_id, iccid, imsi, gprs_device_state_id, allocated_ts, allocated_by, allocated_to, billable_to_name, rate_plan_name from (select gprs_device_id, iccid, imsi, gprs_device_state_id, allocated_ts, allocated_by, allocated_to, billable_to_name, rate_plan_name from gprs_device where iccid >= :iccid order by ICCID ASC) where rownum <= :count") or die "Couldn't prepare statement: " . $dbh->errstr;
	my $search_sql = "select gprs_device_id, iccid, imsi, gprs_device_state_id, to_char(allocated_ts, 'MM/DD/YYYY'), allocated_by, allocated_to, billable_to_name, rate_plan_name from gprs_device ";
	my @params;
	
	if(defined $billable_to_name)
	{
		push(@params, [':billable_to_name', $billable_to_name, 'billable_to_name = :billable_to_name ']);
	}

	if(defined $allocated_to_name)
	{
		push(@params, [':allocated_to_name', $allocated_to_name, 'allocated_to = :allocated_to_name ']);
	}
	
	push(@params, [':status', 2, 'gprs_device_state_id = :status ']);
	
	my $param_count = 0;
	foreach my $param (@params)
	{
		if($param_count > 0)
		{
			$search_sql .= " AND ";
		}
		else
		{
			$search_sql .= " WHERE ";
		}
		
		$search_sql .= $param->[2];
		$param_count++;
	}
	
	$search_sql .= ' order by ICCID ASC';
	
	my $search_stmt = $dbh->prepare($search_sql) or die "Couldn't prepare statement: " . $dbh->errstr;
	foreach my $param (@params)
	{
		$search_stmt->bind_param($param->[0], $param->[1]);
	}
	$search_stmt->execute() or die "Couldn't execute statement: " . $search_stmt->errstr;
	
	my $counter = 0;
	while($counter < $maxcount)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}

		print "<tr><td align=\"center\">" . ($data[3] == 2 ? "<input type=\"checkbox\" name=\"iccid\" value=\"$data[1],$data[6],$data[7],$data[8]\" checked>" : "&nbsp;") . "</td>\n";
		print "<td nowrap><a href=\"iccid_detail.cgi?iccid=$data[1]\">" . USAT::DeviceAdmin::GPRS::format_iccid($data[1]) . "</a></td>\n";
		print "<td bgcolor=\"$status_codes_hash{$data[3]}[1]\">$status_codes_hash{$data[3]}[0]</td>\n";
		print "<td>$data[4]&nbsp;</td>\n";
		print "<td>$data[6]&nbsp;</td>\n";
		print "<td>$data[7]&nbsp;</td></tr>\n";
		
		$counter++;
	}
	
	$search_stmt->finish();

	my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
	my $time_str = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);
	
	print "</table>\n";
	print "&nbsp;<br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"3\" width=\"790\">\n";
	print "<tr><td>Effective Date: </td><td><input type=\"text\" name=\"effective_ts\" size=\"11\" value=\"$time_str\" maxlength=\"10\"></td></tr>\n";
	print "<tr><td>Activated By: </td><td><input type=\"text\" name=\"activated_by\" size=\"40\"></td></tr>\n";
	print "<tr><td>Activated Notes: </td><td><textarea cols=\"60\" rows=\"5\" name=\"activated_notes\"></textarea></td></tr>\n";
	print "<tr><td>&nbsp;</td><td><input type=\"submit\" value=\"Activate\"></td></tr>\n";
	print "</form>\n";
	print "</table>\n";
}



