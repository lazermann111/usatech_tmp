#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");
require("header_footer.pl");

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;
use USAT::DeviceAdmin::Util;

my %status_codes_hash = (	'1'	=>	['Registered', '#9999FF'],
							'2'	=>	['Allocated', '#99CCCC'],
							'3'	=>	['Activation Pending', '#FFFF99'],
							'4'	=>	['Activated', '#99FF99'],
							'5'	=>	['Assigned', '#FF9999']
						);

my $billing_date='12';
my %usage_code_hash = (  '1'     =>    ['Normal', '#99FF99'],
                         '2'     =>    ['Overused', '#FF9999'] 
                        );

&read_Parse();
&print_Header();

#&printFile("header.html");
&htmlHeader;

my $iccid = $in{"iccid"};
my $input_validated = 1;

if(length($iccid) != 20)
{
	print "<span class=\"error\"><li></li>Invalid or Missing ICCID!</span><br>\n";
	$input_validated = 0;
}

if($input_validated)
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	&execute($dbh);
	$dbh->disconnect;
}

#&printFile("footer.html");
&htmlFooter;

sub execute
{
	my ($dbh) = @_;
	my $search_stmt = $dbh->prepare("select gprs_device_id, gprs_device_state_id, ordered_by, to_char(ordered_ts, 'MM/DD/YY'), provider_order_id, ordered_notes, iccid, imsi, allocated_by, to_char(allocated_ts, 'MM/DD/YY'), allocated_to, allocated_notes, billable_to_name, billable_to_notes, activated_by, to_char(activated_ts, 'MM/DD/YY'), activated_notes, provider_activation_id, to_char(provider_activation_ts, 'MM/DD/YY'), msisdn, phone_number, rate_plan_name, assigned_by, to_char(assigned_ts, 'MM/DD/YY'), assigned_notes, device_id, imei, device_type_name, device_firmware_name, rssi, created_by, to_char(created_ts, 'MM/DD/YY'), last_updated_by, to_char(last_updated_ts, 'MM/DD/YY'), pin1, puk1, pin2, puk2, modem_info, to_char(rssi_ts, 'MM/DD/YYYY HH24:MI:SS') from gprs_device where billable_to_name = 'MEI' and iccid = :iccid") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->bind_param(":iccid", $iccid);
	$search_stmt->execute() or die "Couldn't execute statement: " . $search_stmt->errstr;
	my @data = $search_stmt->fetchrow_array();
	$search_stmt->finish();
	if(not defined $data[0])
	{
		print "<span class=\"error\">ICCID $iccid not found!</span>\n";
		return;
	}

	print "<span class=\"subhead\">SIM Card Information: $data[6]</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>GPRS Device ID</td><td>$data[0]</td><td>ICCID</td><td>" . USAT::DeviceAdmin::GPRS::format_iccid($data[6]) . "</td></tr>\n";
	print "<tr><td>Status</td><td bgcolor=\"$status_codes_hash{$data[1]}[1]\">$status_codes_hash{$data[1]}[0]</td><td>IMSI</td><td>$data[7]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
     # puk information
	print "<br><span class=\"subhead\">PIN/PUK Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Pin1</td><td>Puk1</td><td>Pin2</td><td>Puk2</td></tr>\n";
	print "<tr><td>$data[34]&nbsp;</td><td>$data[35]&nbsp;</td><td>$data[36]&nbsp;</td><td>$data[37]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
     # order information 
	print "<br><span class=\"subhead\">Order Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Ordered By</td><td>Date Ordered</td><td>Cingular Order ID</td></tr>\n";
	print "<tr><td>$data[2]&nbsp;</td><td>$data[3]&nbsp;</td><td>$data[4]&nbsp;</td></tr>\n";
	print "<tr><td colspan=\"3\">Order Notes: $data[5]</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
	print "<br><span class=\"subhead\">Allocation Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Allocated By</td><td>Date Allocated</td><td>Allocated To</td></tr>\n";
	print "<tr><td>$data[8]&nbsp;</td><td>$data[9]&nbsp;</td><td>$data[10]&nbsp;</td</tr>\n";
	print "<tr><td colspan=\"3\">Allocation Notes: $data[11]&nbsp;</td></tr>\n";
	print "<tr><td>Billable to</td><td colspan=\"2\">$data[12]&nbsp;</td></tr>\n";
	print "<tr><td colspan=\"3\">Billable Notes: $data[13]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
	print "<br><span class=\"subhead\">Activation Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Activated By</td><td>Date Activated</td></tr>\n";
	print "<tr><td>$data[14]&nbsp;</td><td>$data[15]&nbsp;</td></tr>\n";
	print "<tr><td colspan=\"2\">Activation Notes: $data[16]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
	print "<br><span class=\"subhead\">Assignment Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Assigned By</td><td>Date Assigned</td><td>Assigned To Device</td></tr>\n";
	print "<tr><td>$data[22]&nbsp;</td><td>$data[23]&nbsp;</td><td><a href=\"/mei/device_profile.cgi?device_id=$data[25]\" target=\"_device_admin\">$data[25]</a>&nbsp;</td></tr>\n";
	print "<tr><td colspan=\"3\">Assignment Notes: $data[24]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
	print "<br><span class=\"subhead\">Modem Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>IMEI</td><td>$data[26]&nbsp;</td></tr>\n";
	print "<tr><td>Modem Type</td><td>$data[27]&nbsp;</td></tr>\n";
	print "<tr><td>Modem Firmware</td><td>$data[28]&nbsp;</td></tr>\n";
    print "<tr><td>Last RSSI</td><td>$data[29]&nbsp;</td></tr>\n";
    my $rssi_ts = $data[39];
    if (defined $rssi_ts)
	{
		print "<tr><td>Last RSSI Timestamp</td><td>$rssi_ts&nbsp;</td></tr>\n";	
	}

	my $modem_info = $data[38];
	if(defined $modem_info)
	{
       	print "<tr><td colspan=\"2\">Raw Modem Data:<br><pre>$modem_info</pre></td></tr>\n";
	}

    print "</table>\n";
    print "&nbsp;\n";
	
	print "<br><span class=\"subhead\">Major SIM Change History</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>&nbsp;</td><td nowrap>Status at Time of Change</td><td nowrap>Change Timestamp</td></tr>\n";

	my $history_stmt = $dbh->prepare("select gprs_device_hist_id, gprs_device_state_id, to_char(created_ts, 'MM/DD/YY HH24:MI:SS') from gprs_device_hist where billable_to_name = 'MEI' and iccid = :iccid order by created_ts desc") or die "Couldn't prepare statement: " . $dbh->errstr;
	$history_stmt->bind_param(":iccid", $iccid);
	$history_stmt->execute() or die "Couldn't execute statement: " . $history_stmt->errstr;
	while(my @hist_row = $history_stmt->fetchrow_array())
	{
		print "
		<tr>
		 <td>&nbsp;<a href=\"iccid_hist_detail.cgi?gprs_device_hist_id=$hist_row[0]\">View</a>&nbsp;</td>
		 <td nowrap bgcolor=\"$status_codes_hash{$hist_row[1]}[1]\">$status_codes_hash{$hist_row[1]}[0]</td>
		 <td width=\"100%\">$hist_row[2]</td>
		</tr>\n";
	}
	$history_stmt->finish();
	print "</table>\n";
	print "&nbsp;\n";
	
        ## generate daily report for this month;

        print "<span>&nbsp</span><br>\n";
        print "<span class=\"subhead\">Daily Usage For This Month</span><br>\n";
        #print "<span>&nbsp</span><br>\n";
        print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
        print "<tr><th>Date</th><th>Total Usage (kb)</th></tr>\n";

		my $sql = $dbh->prepare(" select to_char(report_date,'MM/DD/YYYY'), included_usage, total_kb_usage  from device.gprs_daily_usage where iccid = :iccid and report_date >= trunc(sysdate - $billing_date, 'MM') + $billing_date -1 order by report_date ") or die "Couldn't prepare statement: " . $dbh->errstr;

        $sql->bind_param(":iccid", $iccid);
        $sql->execute() or die "Couldn't execute statement: " . $sql->errstr;
        my $remains; 
        while(1)
        {
        my @data = $sql->fetchrow_array();
        if(not defined $data[0])
                {
                        last;
                }
        $remains=$data[1] if ($remains==null); 
        $remains=$remains - $data[2];
        my $color_code=$usage_code_hash{1}[1];
        $color_code=$usage_code_hash{2}[1] if ($remains < 0);

        print "<tr>\n";
        print "<td nowrap>$data[0]&nbsp;</td>\n";
        #print "<td nowrap>$data[1]&nbsp;</td>\n";
        print "<td nowrap>$data[2]&nbsp;</td>\n";
        #print "<td nowrap bgcolor=\"$color_code\">$remains</td>\n";;
        print "</tr>\n";
        }
        print "</table>\n";
        $sql->finish();
      
        print "<span>&nbsp</span><br>\n";
        print "<b><a href=\"usage_detail.cgi?iccid=$iccid\">  See more usage report about this iccid.  </a></b>\n";
        print "<span>&nbsp</span><br>\n";

        ## change rate plan
	my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
	my $time_str = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);

	if($data[1] == 1)
	{
		print "&nbsp;<br>\n";
		print "<span class=\"subhead\">Allocate</span><br>\n";
		print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"3\" width=\"790\">\n";
		print "<form method=\"post\" action=\"allocate_func.cgi\">\n";
		print "<input type=\"hidden\" name=\"iccid\" value=\"$iccid\">\n";
		print "<tr><td>Allocated By: </td><td><input type=\"text\" name=\"allocated_by\" size=\"40\"></td></tr>\n";
		print "<tr><td>Allocated To: </td><td><input type=\"text\" name=\"allocated_to\" size=\"40\"></td></tr>\n";
		print "<tr><td>Allocated Notes: </td><td><textarea cols=\"60\" rows=\"5\" name=\"allocated_notes\"></textarea></td></tr>\n";
		print "<tr><td>Billable Notes: </td><td><textarea cols=\"60\" rows=\"5\" name=\"billable_notes\"></textarea></td></tr>\n";
		print "<tr><td>&nbsp;</td><td><input type=\"submit\" value=\"Allocate\"></td></tr>\n";
		print "</form>\n";
		print "</table>\n";
	}
	elsif($data[1] == 2)
	{
		print "&nbsp;\n";
		print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
		print " <form method=\"post\" action=\"iccid_detail_func.cgi\">\n";
		print " <tr>";
		print " <input type=\"hidden\" name=\"iccid\" value=\"$data[6]\">\n";
		print " <td><input type=\"submit\" name=\"action\" value=\"Dealocate\"></td>\n";
		print "</tr>";
		print "</form>\n";	
		print "</table>\n";
		print "&nbsp;<br>\n";
		print "<span class=\"subhead\">Activate</span><br>\n";
		print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"3\" width=\"790\">\n";
		print "<form method=\"post\" action=\"activate_func.cgi\">\n";
		print "<tr><td>Effective Date: </td><td><input type=\"text\" name=\"effective_ts\" size=\"11\" value=\"$time_str\" maxlength=\"10\"></td></tr>\n";
		print "<tr><td>Activated By: </td><td><input type=\"text\" name=\"activated_by\" size=\"40\"></td></tr>\n";
		print "<tr><td>Activated Notes: </td><td><textarea cols=\"60\" rows=\"5\" name=\"activated_notes\"></textarea></td></tr>\n";
		print "<tr><td>&nbsp;</td><td><input type=\"submit\" value=\"Activate\"></td></tr>\n";
		print "</form>\n";
		print "</table>\n";
	}
	elsif($data[1] == 3)
	{
		
	}
	elsif($data[1] >= 4)
	{
		
	}
	
	print "<br><span class=\"subhead\">$data[6]</span><br>\n";
}
