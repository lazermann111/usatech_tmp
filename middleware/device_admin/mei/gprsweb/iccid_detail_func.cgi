#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");
require("header_footer.pl");

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

&read_Parse();
&print_Header();

#&printFile("header.html");
&htmlHeader;

print "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
print "<tr><td><pre>\n";

my $iccid = $in{"iccid"};
if(length($iccid) < 20)
{
	print "<span class=\"error\"><li></li>Missing ICCID!</span><br>\n";
	#&printFile("footer.html");
&htmlFooter;
	exit(1);
}

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $action = $in{"action"};
if($action eq 'Dealocate')
{
	my $sql = "update gprs_device set gprs_device_state_id = ?, allocated_by = ?, allocated_to = ?, allocated_ts = ?, allocated_notes = ?, billable_to_name = ?, billable_to_notes = ? where billable_to_name = 'MEI' and iccid = ?";
	my $args = [1, undef, undef, undef, undef, undef, undef, $iccid];
	my $update_stmt = $dbh->prepare_cached($sql) or print "Couldn't prepare statement: " . $dbh->errstr;
	my $result = $update_stmt->execute(@$args) or print " Couldn't execute statement: " . $update_stmt->errstr;

	if($result)
	{
		print "Success ($result): <a href=\"iccid_detail.cgi?iccid=$iccid\">" . USAT::DeviceAdmin::GPRS::format_iccid($iccid) . "</a> Dealocated!\n";
	}
	else
	{
		print "Failed ($result): <a href=\"iccid_detail.cgi?iccid=$iccid\">" . USAT::DeviceAdmin::GPRS::format_iccid($iccid) . "</a> NOT Dealocated Successfully\n";
	}
}
else
{
	print "<span class=\"error\"><li></li>Unknown action! ($action)</span><br>\n";
}

print "</pre></td></tr></table>\n";
#&printFile("footer.html");
&htmlFooter;

$dbh->disconnect;


