#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");
require("header_footer.pl");

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

&read_Parse();

my %usage_code_hash = (  '1'     =>    ['Normal', '#99FF99'],
                         '2'     =>    ['Overused', '#FF9999'] 
                        );

my $input_validated = 1;
my $billing_date = '12';
						
my $action = $in{"action"};
if($action eq 'Show Specific')
{
	my $iccid_first = $in{"iccid_first"};
	my $iccid_last = $in{"iccid_last"};
	
	my $input_validated = 1;
	
	my $parse_error;
	if(length($iccid_first) != 16)
	{
		$parse_error = "<span class=\"error\"><li></li>Invalid or Missing First 16 Digits!</span><br>\n";
		$input_validated = 0;
	}
	
	if(length($iccid_last) < 3)
	{
		$parse_error = "<span class=\"error\"><li></li>Invalid or Missing Last 4 Digits!</span><br>\n";
		$input_validated = 0;
	}
	
	#$iccid_last = substr($iccid_last,0,3) if(length($iccid_last) == 4);
	
	my $iccid = "$iccid_first" . "$iccid_last";
# put date invalidation here	
	if($input_validated)
	{
		print CGI->redirect("usage_detail.cgi?iccid=$iccid");
		exit 0;
	}
	
	&print_Header();
	#&printFile("header.html");
&htmlHeader;
	print "$parse_error";

}
elsif($action eq 'Show All')
{
	&print_Header();
	#&printFile("header.html");
&htmlHeader;
	my $start_date = $in{"start_date"};
	my $end_date = $in{"end_date"};
	
	#if($input_validated)
	#{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
        print "<span>&nbsp</span><br>\n";
        print "<span class=\"subhead\">Daily SIM Usage between $start_date and $end_date </span><br>\n";
        #print "<span>&nbsp</span><br>\n";
        print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
        print "<tr><th>Date</th><th>Iccid</th><th>Device Serial <br> Number</th><th>Total Usage <br>(kb)</th></tr>\n";

my $sql = $dbh->prepare(" select to_char(report_date,'MM/DD/YYYY'), a.iccid, c.device_id, device_serial_cd, included_usage, total_kb_usage  from device.gprs_daily_usage a, device.gprs_device b, device.device c  where a.billable_to_name = 'MEI' and a.iccid=b.iccid and b.device_id = c.device_id(+) and report_date between to_date (:start_date, 'MM/DD/YYYY') and to_date (:end_date, 'MM/DD/YYYY') order by report_date desc, total_kb_usage desc, iccid") or die "Couldn't prepare statement: " . $dbh->errstr;

        $sql->bind_param(":start_date", $start_date);
        $sql->bind_param(":end_date", $end_date);
        $sql->execute() or die "Couldn't execute statement: " . $sql->errstr;
      
        while(1)
        {
        my @data = $sql->fetchrow_array();
        if(not defined $data[0])
                {
                        last;
                }
       
        print "<tr>\n";
        print "<td nowrap>$data[0]&nbsp;</td>\n";
        print "<td nowrap><a href=\"usage_detail.cgi?iccid=$data[1]\">" . USAT::DeviceAdmin::GPRS::format_iccid($data[1]) . "</a></td>\n";
        print "<td nowrap><a href=\"/mei/device_profile.cgi?device_id=$data[2]\" target=\"_device_admin\">$data[3]</a>&nbsp;</td>\n";
        #print "<td nowrap>$data[4]&nbsp;</td>\n";
        print "<td nowrap>$data[5]&nbsp;</td>\n";
        
        }
        print "</table>\n";
        $sql->finish();
		$dbh->disconnect;
	#}
}
else
{
	&print_Header();
	#&printFile("header.html");
&htmlHeader;
	print "<span class=\"error\"><li></li>Invalid Action! ($action)</span><br>\n";
}
	

#&printFile("footer.html");
&htmlFooter;
