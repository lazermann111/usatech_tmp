#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");
require("header_footer.pl");

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

&read_Parse();
&print_Header();

#&printFile("header.html");
&htmlHeader;

my $iccid_str = $in{"iccid"};
my $rate_plan = "USAT";
my $effective_ts = $in{"effective_ts"};
my $activated_by = $in{"activated_by"};
my $activated_notes = $in{"activated_notes"};
my $input_validated = 1;

if(length($iccid_str) < 20)
{
	print "<span class=\"error\"><li></li>No ICCIDs!</span><br>\n";
	$input_validated = 0;
}

if(length($rate_plan) != 4)
{
	print "<span class=\"error\"><li></li>Invalid Rate Plan!</span><br>\n";
	$input_validated = 0;
}

if(length($effective_ts) != 10)
{
	print "<span class=\"error\"><li></li>Effective Date must match MM/DD/YYYY</span><br>\n";
	$input_validated = 0;
}

if(length($activated_by) <= 0)
{
	print "<span class=\"error\"><li></li>Ordered Date must match MM/DD/YYYY</span><br>\n";
	$input_validated = 0;
}

if($input_validated)
{
	&execute();
}

#&printFile("footer.html");
&htmlFooter;

sub execute
{
	my @iccid_array = split /\x00/, $iccid_str;
	$iccid_str = join(",", @iccid_array);
	
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 0);
	my $dbh = $DATABASE->{handle};
	
	my $failed_count = 0;
	my $activate_count = 0;
	my $msg;
	
	my $batch_file_content;
	
	print "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td><pre>\n";

	foreach $iccid_info (@iccid_array)
	{
		chomp($iccid_info);

		if(length($iccid_info) < 20)
		{
			print "Error: ICCID too short!\n";
			$failed_count++;
			next;
		}

		my ($iccid,$allocated_to,$billable_to,$old_rate_plan) = split(/,/, $iccid_info);
		
		print "Updating <a href=\"iccid_detail.cgi?iccid=$iccid\">" . USAT::DeviceAdmin::GPRS::format_iccid($iccid) . "</a>... \t";
		
		my $sql = "update gprs_device set gprs_device_state_id = ?, activated_by = ?, activated_ts = sysdate, activated_notes = ?, provider_activation_ts = to_date(?, 'MM/DD/YYYY'), rate_plan_name = ? where billable_to_name = 'MEI' and iccid = ?";
		my $args = [3, $activated_by, $activated_notes, $effective_ts, $rate_plan, $iccid];
		my $update_stmt = $dbh->prepare_cached($sql) or return (0, "Couldn't prepare statement: " . $dbh->errstr);
		my $result = $update_stmt->execute(@$args) or return (0, "Couldn't execute statement: " . $update_stmt->errstr);

		if($result)
		{
			print "Success: $result\n";
			
			print "Activating $iccid,$rate_plan,$effective_ts,$allocated_to,$billable_to...\t";

			my ($activate_result, $activate_msg);
			if($old_rate_plan eq 'KILL')
			{
				($activate_result, $activate_msg) = &Cingular::change_rate($iccid, $rate_plan, $effective_ts);
			}
			else
			{
				($activate_result, $activate_msg) = &Cingular::activate($iccid, $rate_plan, $effective_ts, $allocated_to, $billable_to);
			}
			
			if($activate_result)
			{
				print "Success: $activate_msg\n";
				$activate_count++;
				$dbh->commit();
			}
			else
			{
				print "Failed: $activate_msg... Rolling back update!\n";
				$dbh->rollback();
				$failed_count++;
			}
		}
		else
		{
			$dbh->rollback();
			print "Failed: $msg\n";
			$failed_count++;
		}

		$update_stmt->finish();
		
		print "\n";
	}
	
	print "</pre></td></tr></table>\n";

	$dbh->disconnect;

	if($failed_count > 0)
	{
		$msg = "Processing failed!  For help please copy this screen and send it to an administrator.";
	}
	else
	{
		$msg = "Activations processed successfully!";
	}
	
	$msg = "$msg\\n\\n SIMs Activated: $activate_count \\n Update Failures: $failed_count";
	print "<script language=\"javascript\">window.alert(\"$msg\");</script>\n";
}

sub update 
{
	my ($dbh, $sql, $args) = @_;
	return ($result, "Update Succeeded!");
}
