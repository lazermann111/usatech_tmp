#!/usr/local/USAT/bin/perl

require("../slw-lib.pl");
require("header_footer.pl");

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

&read_Parse();
&print_Header();

#&printFile("header.html");
&htmlHeader;

my %status_codes_hash = (	'1'	=>	['Registered', '#9999FF'],
							'2'	=>	['Allocated', '#99CCCC'],
							'3'	=>	['Activation Pending', '#FFFF99'],
							'4'	=>	['Activated', '#99FF99'],
							'5'	=>	['Assigned', '#FF9999']
						);
my $input_validated = 1;
						
my $action = $in{"action"};
if($action eq 'List SIMs')
{
	my $iccid_first = $in{"iccid_first"};
	my $iccid_last = $in{"iccid_last"};
	my $count = $in{"count"};
	
	my $input_validated = 1;
	
	if(length($iccid_first) != 16)
	{
		print "<span class=\"error\"><li></li>Invalid or Missing First 16 Digits!</span><br>\n";
		$input_validated = 0;
	}
	
	if(length($iccid_last) < 3)
	{
		print "<span class=\"error\"><li></li>Invalid or Missing Last 4 Digits!</span><br>\n";
		$input_validated = 0;
	}
	
	if(length($count) <= 0)
	{
		print "<span class=\"error\"><li></li>SIM Count is Required!</span><br>\n";
		$input_validated = 0;
	}
	
	$iccid_last = substr($iccid_last,0,3) if(length($iccid_last) == 4);
	
	my $iccid = "$iccid_first" . "$iccid_last" . "0";
	
	if($input_validated)
	{
		my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
		my $dbh = $DATABASE->{handle};
		&execute_list($dbh, $iccid, $count);
		$dbh->disconnect;
	}
}
elsif($action eq 'Search SIMs')
{
	my $status = $in{"status"};
	my $billable_to_name;
	my $allocated_to_name = $in{"allocated_to_name"};
	my $rate_plan;
	
	$status = undef unless length($status) > 0;
	$billable_to_name = undef unless length($billable_to_name) > 0;
	$allocated_to_name = undef unless length($allocated_to_name) > 0;
	$rate_plan = undef unless length($rate_plan) > 0;
	
	if($input_validated)
	{
		my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
		my $dbh = $DATABASE->{handle};
		&execute_search($dbh, $status, $billable_to_name, $allocated_to_name, $rate_plan);
		$dbh->disconnect;
	}
}
else
{
	print "<span class=\"error\"><li></li>Invalid Action! ($action)</span><br>\n";
}
	

#&printFile("footer.html");
&htmlFooter;

sub execute_list
{
	my ($dbh, $iccid, $count) = @_;
	print "<span class=\"subhead\">SIM Search Results</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><th>ICCID</th><th>Status</th><th>Allocated<br>To</th><th>Billable<br>To</th><th>Assigned<br>Device</th></tr>\n";
	my $search_stmt = $dbh->prepare("select gprs_device_id, iccid, imsi, gprs_device_state_id, ordered_by, allocated_to, billable_to_name, device_id, rate_plan_name, to_char(ordered_ts, 'MM/DD/YY'), to_char(allocated_ts, 'MM/DD/YY'), to_char(activated_ts, 'MM/DD/YY'), to_char(provider_activation_ts, 'MM/DD/YY'), to_char(assigned_ts, 'MM/DD/YY'), device_serial_cd from (select gprs_device.gprs_device_id, gprs_device.iccid, gprs_device.imsi, gprs_device.gprs_device_state_id, gprs_device.ordered_by, gprs_device.allocated_to, gprs_device.billable_to_name, gprs_device.device_id, gprs_device.rate_plan_name, gprs_device.ordered_ts, gprs_device.allocated_ts, gprs_device.activated_ts, gprs_device.provider_activation_ts, gprs_device.assigned_ts, device.device_serial_cd from gprs_device, device where gprs_device.billable_to_name = 'MEI' and gprs_device.device_id = device.device_id(+) and iccid >= :iccid order by ICCID ASC) where rownum <= :count") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->bind_param(":iccid", $iccid);
	$search_stmt->bind_param(":count", $count);
	$search_stmt->execute() or die "Couldn't execute statement: " . $search_stmt->errstr;
	my $count = 0;
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}

		print "<tr>\n";
		print "<td nowrap><a href=\"iccid_detail.cgi?iccid=$data[1]\">" . GPRS::format_iccid($data[1]) . "</a></td>\n";
		print "<td nowrap bgcolor=\"$status_codes_hash{$data[3]}[1]\">$status_codes_hash{$data[3]}[0]";
		print " $data[9]"  if($data[3] == 1);
		print " $data[10]" if($data[3] == 2);
		print " $data[11]" if($data[3] == 3);
		print " $data[12]" if($data[3] == 4);
		print " $data[13]" if($data[3] == 5);
		
		print "</td>\n";
		print "<td nowrap>$data[5]&nbsp;</td>\n";
		print "<td nowrap>$data[6]&nbsp;</td>\n";
		#print "<td nowrap>$data[8]&nbsp;</td>\n";
		print "<td nowrap><a href=\"/mei/device_profile.cgi?device_id=$data[7]\" target=\"_device_admin\">$data[14]</a>&nbsp;</td>\n";
		print "</tr>\n";

		$count++;
	}
	
	print "</table><br>\n";
	print "<span class=\"subhead\">$count rows</span>\n";
	
	$search_stmt->finish();
}

sub execute_search
{
	my ($dbh, $status, $billable_to_name, $allocated_to_name, $rate_plan) = @_;
	print "<span class=\"subhead\">SIM Search Results</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><th>ICCID</th><th>Status</th><th>Allocated<br>To</th><th>Billable<br>To</th><th>Assigned<br>Device</th></tr>\n";
	my $search_sql = "select gprs_device.gprs_device_id, gprs_device.iccid, gprs_device.imsi, gprs_device.gprs_device_state_id, gprs_device.ordered_by, gprs_device.allocated_to, gprs_device.billable_to_name, gprs_device.device_id, gprs_device.rate_plan_name, to_char(gprs_device.ordered_ts, 'MM/DD/YY'), to_char(gprs_device.allocated_ts, 'MM/DD/YY'), to_char(gprs_device.activated_ts, 'MM/DD/YY'), to_char(gprs_device.provider_activation_ts, 'MM/DD/YY'), to_char(gprs_device.assigned_ts, 'MM/DD/YY'), device.device_serial_cd from gprs_device, device where gprs_device.billable_to_name = 'MEI' and gprs_device.device_id = device.device_id(+) ";
	my @params;
	
	if(defined $status)
	{
		push(@params, [':status', $status, 'gprs_device_state_id = :status ']);
	}
	
	if(defined $billable_to_name)
	{
		push(@params, [':billable_to_name', $billable_to_name, 'billable_to_name = :billable_to_name ']);
	}

	if(defined $allocated_to_name)
	{
		push(@params, [':allocated_to_name', $allocated_to_name, 'allocated_to = :allocated_to_name ']);
	}
	
	if(defined $rate_plan)
	{
		push(@params, [':rate_plan', $rate_plan, 'rate_plan_name = :rate_plan ']);
	}

	my $param_count = 0;
	foreach my $param (@params)
	{
		$search_sql .= " AND " . $param->[2];
		$param_count++;
	}
	
	$search_sql .= ' order by ICCID ASC';
	
	my $search_stmt = $dbh->prepare($search_sql) or die "Couldn't prepare statement: " . $dbh->errstr;
	foreach my $param (@params)
	{
		$search_stmt->bind_param($param->[0], $param->[1]);
	}
	$search_stmt->execute() or die "Couldn't execute statement: " . $search_stmt->errstr;
	my $count = 0;
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}

		print "<tr>\n";
		print "<td nowrap><a href=\"iccid_detail.cgi?iccid=$data[1]\">" . GPRS::format_iccid($data[1]) . "</a></td>\n";
		print "<td bgcolor=\"$status_codes_hash{$data[3]}[1]\">$status_codes_hash{$data[3]}[0]";
		print " $data[9]"  if($data[3] == 1);
		print " $data[10]" if($data[3] == 2);
		print " $data[11]" if($data[3] == 3);
		print " $data[12]" if($data[3] == 4);
		print " $data[13]" if($data[3] == 5);
		
		print "</td>\n";
		print "<td>$data[5]&nbsp;</td>\n";
		print "<td>$data[6]&nbsp;</td>\n";
		#print "<td>$data[8]&nbsp;</td>\n";
		print "<td><a href=\"/mei/device_profile.cgi?device_id=$data[7]\" target=\"_device_admin\">$data[14]</a>&nbsp;</td>\n";
		print "</tr>\n";

		$count++;
	}
	
        print "</table><br>\n";
        print "<span class=\"subhead\">$count rows</span>\n";

	$search_stmt->finish();
}

