
#######################  BEGIN Date Time init

($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = localtime();
$currentYear = 1900 + $yearOffset;

## END Date Time init


#######################  BEGIN htmlHeader

sub htmlHeader {
print <<HTMLOUT;
<!-- BEGIN HEADER -->

<html>
 <head>
  <title>USA Technologies GPRS Device Admin</title>
  <link rel="stylesheet" type="text/css" href="style.css">
 </head>
 <body>
  <table border="0" cellspacing="1" cellpadding="0" width="720" height="100%">
   <tr>
    <td>

     <table border="0" cellspacing="0" cellpadding="0" width="100%" class="title">
      <tr>
       <td align="center">
        USA Technologies SIM Card Admin
       </td>
      </tr>
     </table>

    </td>
   </tr>
   <tr>
    <td>
     &nbsp;
    </td>
   </tr>
   <tr>
    <td height="100%" align="left" valign="top">


<!-- END HEADER -->

<!-- BEGIN CONTENT -->
HTMLOUT
}  ## END htmlHeader


#######################  BEGIN htmlFooter

sub htmlFooter {
print <<HTMLOUT;
<!-- END CONTENT -->

<!-- BEGIN FOOTER -->

    </td>
   </tr>

   <tr>
    <td>
     &nbsp;
    </td>
   </tr>

   <tr>
    <td>

     <table border="0" cellspacing="0" cellpadding="0" width="100%" class="title">
      <tr>
       <td align="center">
        &copy; Copyright 2005-$currentYear - Confidential
       </td>
      </tr>
     </table>

    </td>
   </tr>

  </table>
 </body>
</html>

<!-- END FOOTER -->
HTMLOUT
}  ## END htmlFooter

1; #return true


