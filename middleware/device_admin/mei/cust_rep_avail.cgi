#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::MEIHeader;

####### Testing development against production
#my $DATABASE = USAT::Database->new(
#	PrintError => 1, RaiseError => 0, AutoCommit => 1, 
#	primary => 'usadbp', backup => 'usadbp', username => 'web_user', password => 'wxkj21a9'
#);
#######
####### Prodction
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
#######
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %in = $query->Vars;

USAT::DeviceAdmin::UI::MEIHeader->printHeader();
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html");

my $checkdate = $in{"check_date"};
if(length($checkdate) == 0)
{
	print "Required parameter not found: check_date\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
	exit;
}

my $http_check_stmt = $dbh->prepare("SELECT avc.total_checks, avc.successful_checks, (CASE WHEN avc.total_checks = 0 THEN 0 ELSE ROUND((avc.successful_checks / avc.total_checks) * 100, 1) END) success_percentage, avc.failed_checks, (CASE WHEN avc.total_checks = 0 THEN 0 ELSE ROUND((avc.failed_checks / avc.total_checks) * 100, 1) END) fail_percentage, ROUND(avc.avg_response_time_ms, 0) avg_response_time_ms, ROUND(avc.avg_content_length, 0) avg_content_length FROM ( SELECT  COUNT(1) total_checks, SUM(CASE WHEN htc.http_check_status = 'S' THEN 1 ELSE 0 END) successful_checks, SUM(CASE WHEN htc.http_check_status = 'U' THEN 1 ELSE 0 END) failed_checks, AVG(CASE WHEN htc.http_check_status = 'S' THEN htc.response_time_ms END) avg_response_time_ms, AVG(CASE WHEN htc.http_check_status = 'S' THEN htc.content_length END) avg_content_length FROM tracking.http_check htc WHERE htc.http_check_type_id = 1 AND htc.created_ts >= TO_DATE(:checkdate, 'MM/DD/YYYY') AND htc.created_ts < (TO_DATE(:checkdate, 'MM/DD/YYYY') + 1) ) avc") || print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";

$http_check_stmt->bind_param(":checkdate", $checkdate);
$http_check_stmt->execute();


print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">
 <tr>
  <th bgcolor=\"#A0A0A0\" colspan=\"7\">Login Page Availability Report for $checkdate</th>
 </tr>
 <tr>
  <th bgcolor=\"#C0C0C0\" colspan=\"7\">Aggregated Metrics</th>
 </tr>
 <tr>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Total Checks</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Successful Checks</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Success Percentage</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Failed Checks</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Fail Percentage</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Avg Response Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Avg Content Length</th>
 </tr>
";

my $count = 0;
while (my @data = $http_check_stmt->fetchrow_array()) 
{
	$count++;
	
	print " <tr>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[0]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[1]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[2]%&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[3]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[4]%&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[5] ms&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[6] byts&nbsp;</td>
	        </tr>
	      ";
}

$http_check_stmt->finish();

if($count == 0)
{
	print " <tr><td style=\"font-size: 10pt;\" colspan=\"17\" align=\"center\"><font color=\"red\">&nbsp;No Data!</font></td></tr>\n";
}

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
