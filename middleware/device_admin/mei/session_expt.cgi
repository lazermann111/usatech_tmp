#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::MEIHeader;

####### Testing development against production
#my $DATABASE = USAT::Database->new(
#	PrintError => 1, RaiseError => 0, AutoCommit => 1, 
#	primary => 'usadbp', backup => 'usadbp', username => 'web_user', password => 'wxkj21a9'
#);
#######
####### Prodction
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
#######
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %in = $query->Vars;

USAT::DeviceAdmin::UI::MEIHeader->printHeader();
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html");

my $serverdate = $in{"expt_date"};
if(length($serverdate) == 0)
{
	print "Required parameter not found: expt_date\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
	exit;
}

my $call_in_stmt = $dbh->prepare("SELECT sex.device_serial_cd, sex.machine_id, TO_CHAR(sex.call_in_start_ts, 'MM/DD/YYYY HH24:MI:SS') call_in_start, sex.session_count, sex.calls_after_prior_batch, sex.session_count - sex.calls_after_prior_batch batch_session_count FROM ( SELECT sc.device_call_in_record_id, sc.device_id, sc.machine_id, sc.device_serial_cd, sc.device_type, sc.firmware_version, sc.server_date, dci.call_in_start_ts, dci.call_in_finish_ts, dci.network_layer, dci.ip_address, sc.session_count, tracking.fn_calls_after_prior_batch(sc.device_call_in_record_id, sc.device_serial_cd) calls_after_prior_batch FROM ( SELECT cci.device_call_in_record_id, cci.device_id, cci.machine_id, cci.device_serial_cd, cci.device_type, cci.firmware_version, cci.server_date, (CASE cci.session_count_rolled WHEN 'N' THEN (cci.session_count_curr - cci.session_count_prev) ELSE (cci.session_count_curr - cci.session_count_prev + 1000000) END) session_count FROM tracking.counter_call_in_rec cci WHERE cci.server_date = TO_DATE(:serverdate, 'MM/DD/YYYY') AND cci.device_type = 'MEI Telemeter' ) sc JOIN device_call_in_record dci ON sc.device_call_in_record_id = dci.device_call_in_record_id AND sc.server_date = TRUNC(dci.call_in_start_ts) ) sex WHERE (sex.session_count - sex.calls_after_prior_batch) != 1 AND sex.session_count < 990000 ORDER BY batch_session_count DESC") || print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";

$call_in_stmt->bind_param(":serverdate", $serverdate);
$call_in_stmt->execute();


print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">
 <tr>
  <th bgcolor=\"#A0A0A0\" colspan=\"6\">Session Exception Report for $serverdate</th>
 </tr>
 <tr>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Serial Number</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">EV Number</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Call Start Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Session Count</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Calls After Prior Batch</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Batch Session Count</th>
 </tr>
";

my $count = 0;
while (my @data = $call_in_stmt->fetchrow_array()) 
{
	$count++;
	
	print " <tr>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap><a style=\"font-size: 10pt;\" href=\"activity.cgi?ssn=$data[0]&rowcount=25\">$data[0]</a>&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[1]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[2]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[3]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[4]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[5]&nbsp;</td>
	        </tr>
	      ";
}

$call_in_stmt->finish();

if($count == 0)
{
	print " <tr><td style=\"font-size: 10pt;\" colspan=\"6\" align=\"center\"><font color=\"red\">&nbsp;No Data!</font></td></tr>\n";
}

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
