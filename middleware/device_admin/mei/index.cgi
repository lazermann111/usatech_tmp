#!/usr/local/USAT/bin/perl

use strict;
use CGI::Carp qw(fatalsToBrowser);
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::MEIHeader;

USAT::DeviceAdmin::UI::MEIHeader->printHeader;
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html"); 

print q|
<style>
div.notes {
text-align: justify;
margin:0 10%;
color:#000fff;
background-color:#00FFFF;
}
h1,h2,p{margin: 0 10px}
h1{font-size: 150%;color: #330000}
h2{font-size: 120%;color: #000}
p{padding-bottom:1em}
h2{padding-top: 0.3em}
</style>
<table cellspacing="0" cellpadding="6" border=1 width="100%">
 <tr>
  <th colspan="1" bgcolor="#C0C0C0">USA Technologies Device Admin</th>
 </tr>
</table>
&nbsp;
<table cellspacing="0" cellpadding="2" border="1" width="95%">
 <tr>
  <th colspan="2" bgcolor="#C0C0C0">Quick Links</th>
 </tr>
 <tr>
  <td width="50%"><li><a href="http://www.usatech.com">USA Technologies Public Website - www.usatech.com</a></li></td>
  <td width="50%"><li><a href="http://usalive.usatech.com">USALive Customer Reporting - usalive.usatech.com</a></li></td>
 </tr>
 <tr>
  <td width="50%"><li><a href="whatisnew.cgi">What's New?</a></li></td>
  <td width="50%">&nbsp;</td>
 </tr>
</table>
&nbsp;
<br>
|;

USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
