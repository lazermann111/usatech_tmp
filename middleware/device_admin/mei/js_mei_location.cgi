#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::Const qw(:globals );
use USAT::DeviceAdmin::UI::MEIHeader;

my $query = OOCGI::OOCGI->new;

USAT::DeviceAdmin::UI::MEIHeader->printHeader;
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html"); 

my $msg = JS_MENU__PROGRESS_MSG;
print qq|
<br>
<h3>Starting the process for mei location javascript refresh ...<h3>
<br>$msg<br>
<img src=/mei/img/progress_bar.gif>
<br><br>
|;

my $cmd_path = JS_MENU__CRON_SCRIPT_PATH;
my $js_path  = JS_MENU__JS_MEI_SCRIPT_PATH;

my @cmd = `perl $cmd_path/js_mei_location.pl`;
open(FILE,">$js_path/mei_location.js") or die "Couldn't open file: $!";
foreach my $line ( @cmd ) {
   print FILE $line;
}
close(FILE);
print q|
<h1>Done ...<h1>
<br><br>
|;

USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
