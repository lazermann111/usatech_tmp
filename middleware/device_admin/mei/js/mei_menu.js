_menuCloseDelay=30000;
_menuOpenDelay=150;
_subOffsetTop=2;
_subOffsetLeft=0;

forgetClickValue="true";

with(menuStyle=new mm_style()){
bordercolor="#999999";
borderstyle="solid";
borderwidth=2;
fontfamily="Verdana, Tahoma, Arial";
fontsize="75%";
fontstyle="normal";
headerbgcolor="#ffffff";
headercolor="#000000";
offbgcolor="#eee8aa";
offcolor="#000000";
onbgcolor="#00ffff";
oncolor="#000099";
padding=4;
pagebgcolor="#82B6D7";
pagecolor="black";
separatorcolor="#999999";
separatorsize=2;
subimage="/mei/img/down_arrow.gif";
subimagepadding=2;
openonclick="true";
closeonclick="true";
}

with(HeadersStyle=new mm_style()){
bordercolor="#000080";
borderstyle="solid";
borderwidth=1;
fontfamily="verdana,helvetica";
fontsize="12px";
fontstyle="normal";
fontweight="normal";
headerbgcolor="#3399CC";
headercolor="#FFFF33";
offbgcolor="#eee8aa";
offcolor="#666666";
onbgcolor="#00ffff";
oncolor="#000000";
padding=5;
subimage="/mei/img/arrow.gif";
onsubimage="/mei/img/arrow.gif";
}

with(submenuStyle=new mm_style()){
styleid=1;
bordercolor="#000000";
borderstyle="solid";
borderwidth=1;
fontfamily="Verdana, Tahoma, Arial";
fontsize="10pt";
fontstyle="bold";
headerbgcolor="#ffffff";
headercolor="#000000";
offbgcolor="#ffffff";
offcolor="#000000";
onbgcolor="#00ffff";
oncolor="#000000";
padding=4;
pagebgcolor="#82B6D7";
pagecolor="black";
subimage="/mei/img/arrow.gif";
onsubimage="/mei/img/arrow.gif";
subimagepadding=0;
}

with(submenuStyleG=new mm_style()){
styleid=0;
bordercolor="#000000";
borderstyle="solid";
borderwidth=1;
fontfamily="Verdana, Tahoma, Arial";
fontsize="10pt";
fontstyle="bold";
headerbgcolor="#ffffff";
headercolor="#000000";
offbgcolor="#eee8aa";
offcolor="#000000";
onbgcolor="#00ffff";
oncolor="#000000";
padding=4;
pagebgcolor="#82B6D7";
pagecolor="black";
subimage="/mei/img/arrow.gif";
onsubimage="/mei/img/arrow.gif";
subimagepadding=0;
}

with(milonic=new menuname("Main Menu")){
followscroll='1';
top=30;
screenposition="center"
alwaysvisible=1;
orientation="horizontal";
style=menuStyle;
aI("status=Back To Home Page;text=Home;url=/mei;tooltip=Back To Home Page");
aI("showmenu=Devices;text=Devices;tooltip=Tools to search, view, and modify device information");
aI("showmenu=sess_excp;text=Session Exception;tooltip=View daily device session exception reports");
aI("showmenu=batc_succ;text=Batch Success;tooltip=View daily batch success statistics");
aI("showmenu=usat_trip;text=USAT Round Trip;tooltip=Overall daily round trip network performance statistics");
aI("showmenu=gprs_trip;text=GPRS Round Trip;tooltip=GPRS network daily round trip network performance statistics");
aI("showmenu=utilities;text=Utilities;tooltip=Refresh cached menu elements<br>(<b>NOTE</b>: use only if necessary)");
}

with(milonic=new menuname("utilities")) {
style=HeadersStyle;
aI("text=Refresh Javascripts;type=header;");
aI("text=Customer;url=js_mei_customer.cgi");
aI("text=Location;url=js_mei_location.cgi");
aI("text=Dates;url=js_mei_dates.cgi");
}

with(milonic=new menuname("Devices")) {
style=HeadersStyle;
aI("text=Serial Number:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/mei/profile.cgi name=search><table><tr><td><input name=serial_number size=10> <input type=submit value=Search></td></tr></table></form>;type=form;");
aI("text=Devices Menu;type=header;");
aI("showmenu=Customers;text=by Customers;");
aI("showmenu=Locations;text=by Locations;");
aI("text=Full Device List;url=/mei/device_list.cgi?action=View");
}

with(milonic=new menuname("Customers")) {
style=submenuStyleG;
overflow="scroll";
align="center";
aI("showmenu=cust__;text=#0-9;");
aI("showmenu=cust_a;text=A;");
aI("showmenu=cust_b;text=B;");
aI("showmenu=cust_c;text=C;");
aI("showmenu=cust_d;text=D;");
aI("showmenu=cust_e;text=E;");
aI("showmenu=cust_f;text=F;");
aI("showmenu=cust_g;text=G;");
aI("showmenu=cust_h;text=H;");
aI("showmenu=cust_i;text=I;");
aI("showmenu=cust_j;text=J;");
aI("showmenu=cust_k;text=K;");
aI("showmenu=cust_l;text=L;");
aI("showmenu=cust_m;text=M;");
aI("showmenu=cust_n;text=N;");
aI("showmenu=cust_o;text=O;");
aI("showmenu=cust_q;text=Q;");
aI("showmenu=cust_p;text=P;");
aI("showmenu=cust_r;text=R;");
aI("showmenu=cust_s;text=S;");
aI("showmenu=cust_t;text=T;");
aI("showmenu=cust_u;text=U;");
aI("showmenu=cust_v;text=V;");
aI("showmenu=cust_w;text=W;");
aI("showmenu=cust_x;text=X;");
aI("showmenu=cust_y;text=Y;");
aI("showmenu=cust_z;text=Z;");
}

drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
