#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::MEIHeader;

####### Testing development against production
#my $DATABASE = USAT::Database->new(
#	PrintError => 1, RaiseError => 0, AutoCommit => 1, 
#	primary => 'usadbp', backup => 'usadbp', username => 'web_user', password => 'wxkj21a9'
#);
#######
####### Prodction
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
#######
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;

my %in = $query->Vars;

USAT::DeviceAdmin::UI::MEIHeader->printHeader();
USAT::DeviceAdmin::UI::MEIHeader->printFile("header.html");

my $callindate = $in{"batch_date"};
if(length($callindate) == 0)
{
	print "Required parameter not found: batch_date\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
	exit;
}

my $call_in_stmt = $dbh->prepare("SELECT bat.attempted_batch, bat.successful_batch, (CASE WHEN bat.attempted_batch = 0 THEN 0 ELSE ROUND((bat.successful_batch / bat.attempted_batch) * 100, 1) END) success_percentage, bat.failed_batch, (CASE WHEN bat.attempted_batch = 0 THEN 0 ELSE ROUND((bat.failed_batch / bat.attempted_batch) * 100, 1) END) fail_percentage, ROUND(bat.min_session_time, 0) min_session_success_time, ROUND(bat.avg_session_time, 0) avg_session_success_time, ROUND(bat.max_session_time, 0) max_session_success_time FROM ( SELECT COUNT(1) attempted_batch, SUM(CASE WHEN dcir.call_in_status = 'S' THEN 1 ELSE 0 END) successful_batch, SUM(CASE WHEN dcir.call_in_status != 'S' THEN 1 ELSE 0 END) failed_batch, MIN(CASE WHEN dcir.last_trans_in_ts IS NOT NULL THEN (dcir.last_trans_in_ts - dcir.call_in_start_ts) * 86400 END) min_session_time, AVG(CASE WHEN dcir.last_trans_in_ts IS NOT NULL THEN (dcir.last_trans_in_ts - dcir.call_in_start_ts) * 86400 END) avg_session_time, MAX(CASE WHEN dcir.last_trans_in_ts IS NOT NULL THEN (dcir.last_trans_in_ts - dcir.call_in_start_ts) * 86400 END) max_session_time FROM device.device dev, device_call_in_record dcir WHERE dev.device_serial_cd = dcir.serial_number AND dev.device_type_id = 6 AND dcir.call_in_type = 'B' AND dcir.created_ts >= TO_DATE(:callindate, 'MM/DD/YYYY') AND dcir.created_ts < (TO_DATE(:callindate, 'MM/DD/YYYY') + 1) ) bat") || print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";

$call_in_stmt->bind_param(":callindate", $callindate);
$call_in_stmt->execute();


print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">
 <tr>
  <th bgcolor=\"#A0A0A0\" colspan=\"8\">Batch Success Report for $callindate</th>
 </tr>
 <tr>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Attempted Batch</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Successful Batch</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Success Percentage</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Failed Batch</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Fail Percentage</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Min Success Session Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Avg Success Session Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Max Success Session Time</th>
 </tr>
";

my $count = 0;
while (my @data = $call_in_stmt->fetchrow_array()) 
{
	$count++;
	
	print " <tr>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[0]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[1]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[2]%&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[3]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[4]%&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[5] sec.&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[6] sec.&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[7] sec.&nbsp;</td>
	        </tr>
	      ";
}

$call_in_stmt->finish();

if($count == 0)
{
	print " <tr><td style=\"font-size: 10pt;\" colspan=\"8\" align=\"center\"><font color=\"red\">&nbsp;No Data!</font></td></tr>\n";
}

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::MEIHeader->printFooter("footer.html");
