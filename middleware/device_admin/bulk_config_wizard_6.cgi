#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::UI::DAHeader;

require Text::CSV;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print "<br><br><font color=\"red\">Undefined Action!</font><br><br>";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print "<br><font color=\"red\">No devices selected!</font><br><br>";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my @devices_array = split /,/, $device_ids_str;

my $parameters_str = $PARAM{"parameter_to_change"};
my @parameters_array = split /\x00/, $parameters_str;

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};

my $cust_loc_str = USAT::DeviceAdmin::Util->customer_location_str(\%PARAM);

print "<form method=\"post\" action=\"bulk_config_wizard_6b.cgi\">";

HIDDEN_PASS('customer_id', 'location_id', 'parent_location_id');

print "
 <input type=\"hidden\" name=\"include_device_ids\" value=\"" . join(',',@devices_array) . "\">
 <input type=\"hidden\" name=\"parameter_to_change\" value=\"" . join(',',@parameters_array) . "\">

<table cellpadding=\"3\" border=\"1\" cellspacing=\"0\" width=\"100%\">
 <tr>
  <th align=\"center\" colspan=\"3\" bgcolor=\"#C0C0C0\">Device Configuration Wizard - Page 6: Other Settings</th>
 </tr>
 <tr>
  <th align=\"center\" colspan=\"3\" bgcolor=\"#C0C0C0\">$cust_loc_str</th>
 </tr>

 <tr>
  <td style=\"font-size: 12pt;\">Do you want to Zero Out Device Counters?</td><td style=\"font-size: 12pt;\"><input type=\"radio\" name=\"zero_counters\" value=\"1\">Yes <input type=\"radio\" name=\"zero_counters\" value=\"0\" checked>No</td>
 </tr>
 
 <tr>
  <td style=\"font-size: 12pt;\">Turn Debugging ON?</td><td style=\"font-size: 12pt;\"><input type=\"radio\" name=\"debug\" value=\"1\">Yes <input type=\"radio\" name=\"debug\" value=\"0\" checked>No</td>
 </tr>
  
</table>";

foreach my $change_location (@parameters_array)
{
	my $start_addr = $change_location;
	
    HIDDEN_PASS($start_addr.'_size',           $start_addr.'_align',
                $start_addr.'_pad_with',       $start_addr.'_data_mode',
                $start_addr.'_eerom_location', $start_addr.'_name',
                $start_addr);
}

$dbh->disconnect;

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">
 <tr>
  <td align=\"center\" bgcolor=\"#C0C0C0\">
   <input type=\"button\" value=\"< Back\" onClick=\"javascript:history.go(-1);\">
   <input type=button value=\"Cancel\" onClick=\"javascript:window.location = '/';\">
   <input type=\"submit\" name=\"action\" value=\"Next >\">
  </td>
 </tr>
</table>
</form>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
