#!/usr/local/USAT/bin/perl

use strict;

use USAT::App::DeviceAdmin::Const qw(:globals );
use OOCGI::OOCGI;
use USAT::DeviceAdmin::DBObj;
use USAT::Common::Const;
use URI;
use USAT::DeviceAdmin::Config::Form;
use USAT::DeviceAdmin::Config::Form::Object qw(EDITOR_BASIC EDITOR_ADVANCED);	#just to import EDITOR_* constants
use USAT::DeviceAdmin::Config::Form::Object::TextField qw(RESTRICTION_NUMERIC RESTRICTION_REQUIRED);	#just to import RESTRICTION_* constants
use Data::Dumper;

use USAT::DeviceAdmin::UI::DAHeader;

use USAT::DeviceAdmin::Util;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;
my ($file_id, $file_type);

sub main () {
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	print $user_menu;
	
	my $file_name = $PARAM{"file_name"};
	if(!$file_name)
	{
		print "Required parameter not found: file_name!";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}

	my $sql = 'SELECT file_transfer_id, file_transfer_type_cd
				FROM device.file_transfer
				WHERE file_transfer_name = ?
					AND file_transfer_type_cd IN (?, ?)';
	my $qo = OOCGI::Query->new($sql, { bind => [
		$file_name,
		PKG_FILE_TYPE__EDGE_DEFAULT_CONFIGURATION_TEMPLATE,
		PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE
	] } );

	if( my @arr = $qo->next_array ) {
		$file_id = $arr[0];
		$file_type = $arr[1];
	} else {
		print "Template $file_name not found";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
	
	my $file_data_hex = USAT::DeviceAdmin::Util::blob_select(undef, $file_id);
	if(!$file_data_hex)
	{
		print "Template data not found for file_id: $file_id";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}	

	my $edit_mode = $PARAM{"edit_mode"} eq 'B' ? EDITOR_BASIC : EDITOR_ADVANCED;

	my $max_param_code_len   = 60;
	my $max_param_value_len  = 200;
	my $default_textbox_size = 68;

	my $title = "$file_name ($edit_mode)";
	$title = $title.B('*','red').B(' denotes required parameter',-1);

	my $file_data = pack("H*", $file_data_hex);
	print qq{
	<script language=\"javascript\">
	<!--
	function ValidateForm(f)
	{	
		for (i=0; i<f.elements.length; i++) 
		{
			if (f.elements[i].name.substring(0, 4) == 'cfg_')
			{
				f.elements[i].value = f.elements[i].value.replace(/^\\s*|\\s*\$/g,'');

				if (f.elements[i].className == '".RESTRICTION_REQUIRED."' && f.elements[i].value == '')
				{
					alert('Please enter a value for the required parameter ' + f.elements[i].name.substring(4));
					return false;
				}

				if (f.elements[i].className == '".RESTRICTION_NUMERIC."' && (f.elements[i].value == '' || isNaN(f.elements[i].value)))
				{				
					alert('Please enter a numeric value for the required parameter ' + f.elements[i].name.substring(4));
					return false;
				}
			}
		}
		return true;
	}

	function doSubmit(myform, actionValue)
	{
		if (ValidateForm(myform) == false) return;
		myform.myaction.value = actionValue;
		myform.submit();
	}
	// -->
	</script>

	<table cellpadding="3" border="1" cellspacing="0" width="100%">
	 <tr>
	  <th align="center" colspan="2" bgcolor="#C0C0C0">$title</th>
	 </tr>

	 <form name="myform" method=post action="edit_config_edge_func.cgi">
		<input type="hidden" name="edit_mode" value="$edit_mode">
		<input type="hidden" name="myaction" value="">};

    $file_data =~ tr/\r//d;  # to make sure it splits unix and windows data
    chomp($file_data);       # added to make sure to delete last new line character
    my @config_lines = split(/\n/, $file_data);
	my %config_hash;

    my @config_array;
    { # localize eval
        local $@;
        eval { @config_array = USAT::DeviceAdmin::Config::Form::load_device_type(PKG_DEVICE_TYPE__EDGE); };
        if ($@) {
            print "Error: $@";
            USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
            exit;
        }
    }

	foreach my $config_line (@config_lines)
	{
		chomp($config_line);
		my ($config_param, $config_value) = split(/=/, $config_line, 2);	
		($config_param, $config_value) = (substr($config_param, 0, $max_param_code_len), substr($config_value, 0, $max_param_value_len));

		$config_param =~ s/^\s+//;
		$config_param =~ s/\s+$//;
		$config_value =~ s/^\s+//;
		$config_value =~ s/\s+$//;

		$config_line = "$config_param=$config_value";

		if (length($config_param) > 0)
		{
			$config_hash{$config_param} = $config_value;
		}
	}

	### generate heading index ###
	my @heading_index;
	foreach my $config_item (@config_array)
	{	
		next unless UNIVERSAL::isa($config_item, 'USAT::DeviceAdmin::Config::Form::Object');	#ignore unknown elements
		if (($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
			&& $config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Header'))
		{
			push @heading_index, $config_item;
		}
	}
	if (@heading_index)
	{
		print qq{
			<tr>
				<td colspan="2" width="100%" align="left" bgcolor="#EEEEEE">
					<a name="___section_index"></a>
					<table border="0" cellpadding="0" cellspacing="0">
					<tr>
					<td valign="top" bgcolor="#EEEEEE">
						<b>Section Index:</b>&nbsp;&nbsp;&nbsp;
					</td>
					<td bgcolor="#EEEEEE">};

		foreach ( sort { lc($a->parameter_code) cmp lc($b->parameter_code) }  @heading_index ) {
			print '<a href="#'.URI->new($_->parameter_code)->as_string.'">'.$_->parameter_code.'</a><br>';
        }

		print "
					</td>
					</tr>
					</table>
				</td>
			</tr>";
	}

	### generated form elements ###
	foreach my $config_item (@config_array)
	{	
        my $parameter_name = $config_item->parameter_name;
        my $parameter_code = $config_item->parameter_code;
		next unless UNIVERSAL::isa($config_item, 'USAT::DeviceAdmin::Config::Form::Object');	#ignore unknown elements

		if ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Header'))
		{
			if ($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
			{
				print qq{
					<tr>
						<td colspan="2" width="100%" align="center" bgcolor="#C0C0C0"><b><font color="#0000CC"><a name="}.URI->new($parameter_code)->as_string.'">'.$parameter_code.q{</a></b></font>&nbsp;&nbsp;&nbsp;<a href="#___section_index"><img src="/icons/small/back.gif" border="0"></td>
						</tr>};
		    }
		}
		else
		{
            if(!defined $config_hash{$parameter_code} && $file_type eq PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE ) {
               next; # CUSTOM data has fewer elements, so skip the unused ones.
            }
			if ($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
			{
				print qq{
					<tr>
					  <td width="50%"><b>}.(defined $parameter_name ? $parameter_name . ' [' . $parameter_code . ']' : $parameter_code) . (defined $config_item->restriction ? qq{<font color="red">*</font>} : "") . "</b>&nbsp;</td>
					  <td width=\"50%\">";
				 if ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::TextField'))
				 {
				 	my $param_value = $config_hash{$parameter_code};
					if(defined $config_item->read_only && $config_item->read_only ne '') {
					print "
						<input type=\"text\" name=\"cfg_".$parameter_code."\" value=\"" . (length($param_value) > 0 ? $param_value : $config_item->default) .  "\" style=\"border:1px solid #000; background-color:#cc9999;color:#000000;\"  size=\"" . (defined $config_item->maxlength ? ($config_item->maxlength + 7 > $default_textbox_size ? $default_textbox_size : $config_item->maxlength + 7) : $default_textbox_size) . "\" maxlength=\"" . (defined $config_item->maxlength ? $config_item->maxlength : $max_param_value_len) . "\"" . (defined $config_item->restriction ? " class=\"".$config_item->restriction."\"" : "") .(defined $config_item->read_only ? $config_item->read_only ? 'readonly' : '': ''). ">";
                    } else { 
					print "
						<input type=\"text\" name=\"cfg_".$parameter_code."\" value=\"" . (length($param_value) > 0 ? $param_value : $config_item->default) .  "\" size=\"" . (defined $config_item->maxlength ? ($config_item->maxlength + 7 > $default_textbox_size ? $default_textbox_size : $config_item->maxlength + 7) : $default_textbox_size) . "\" maxlength=\"" . (defined $config_item->maxlength ? $config_item->maxlength : $max_param_value_len) . "\"" . (defined $config_item->restriction ? " class=\"".$config_item->restriction."\"" : "") . ">";
                    }
				 }
				 elsif ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Radio'))
				 {
					my @choices = @{$config_item->choices};
					foreach my $choice (@choices)
					{
						print "
							<input type=\"radio\" name=\"cfg_".$parameter_code."\" value=\"$choice\"" . (length($config_hash{uc($parameter_code)}) > 0 ? (uc($config_hash{uc($parameter_code)}) eq uc($choice) ? ' checked' : '') : (uc($config_item->default) eq uc($choice) ? ' checked' : '')) . ">$choice</input>";
					}
				 }
				 elsif ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Select'))
				 {
					print "
						<select name=\"cfg_".$parameter_code."\">";

					my @choices = @{$config_item->choices};
					foreach my $choice (@choices)
					{
						print "   
							<option value=\"$choice\"" . (length($config_hash{uc($parameter_code)}) > 0 ? (uc($config_hash{uc($parameter_code)}) eq uc($choice) ? ' selected' : '') : (uc($config_item->default) eq uc($choice) ? ' selected' : '')) . ">$choice</option>";
					}

					print "
						</select>";
				 }				 
				 else
				 {
					print "
						<input type=\"text\" name=\"cfg_".$parameter_code."\" value=\"$config_hash{uc($parameter_code)}\" size=\"$default_textbox_size\" maxlength=\"$max_param_value_len\">";
				 }

				 print "
					  </td>
					</tr>
					<tr>
					  <td colspan=\"2\" width=\"100%\" bgcolor=\"#EEEEEE\"><font size=\"-1\">".$config_item->description."</font>&nbsp;</td>
					</tr>";
			}
			else
			{
				print "
					<input type=\"hidden\" name=\"cfg_".$parameter_code."\" value=\"" . (length($config_hash{uc($parameter_code)}) > 0 ? $config_hash{uc($parameter_code)} : $config_item->default) . "\">";
			}

			delete($config_hash{uc($parameter_code)});
		}
	}

	foreach my $config_line (@config_lines)
	{
		my ($config_param, $config_value) = split(/=/, $config_line, 2);
		if (defined $config_hash{uc($config_param)})
		{
			HIDDEN('cfg_'.$config_param, $config_value);
		}
	}

	HIDDEN('file_id', $file_id);

	print html_form_submit_buttons();

	print qq{
	 <tr>
	  <th align="center" colspan="2" bgcolor="#C0C0C0">$title</th>
	 </tr>

	 </form>
	</table>
	};

	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub html_form_submit_buttons () {
	my $str = qq{
	 <tr>
	  <td align="center" colspan="2">};
    if($file_type eq PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE ) {
		$str .= q{<input type="button" value='Save' onClick="doSubmit(this.form, 'Save');">&nbsp;};
    }
	
	$str .= q{<input type="button" value='Copy To' onClick="doSubmit(this.form, 'Copy To');">&nbsp;};

	$str .= OOCGI::Popup->new(name => 'new_file_type', 
		sql => qq{SELECT file_transfer_type_cd, file_transfer_type_name
					FROM device.file_transfer_type
					WHERE file_transfer_type_cd IN(?, ?)
					ORDER BY file_transfer_type_name }, bind => [
						PKG_FILE_TYPE__EDGE_DEFAULT_CONFIGURATION_TEMPLATE,
						PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE] );
			   
	$str .=  '&nbsp;<input type="text" name="new_file_name" size="30" maxlength="50">';
    $str .= q{
		<input type="button" value="Cancel" onClick="document.location = 'template_menu.cgi';">
	  </td>
	 </tr>
      };

     return $str;
}

main();
