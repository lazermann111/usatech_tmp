#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::DBObj;

my %params = OOCGI::OOCGI->new()->Vars();

print "Content-Type: text/html; charset=ISO-8859-1\n\n";

# Customer Location
my $device_id = $params{device_id};
my $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);
my $DTI = $device->device_type_id;

my $T = OOCGI::NTable->new('border=1 width=100% noshade size=2 cellspacing=0');

my $location = $device->Location;
my $LID = $location->ID;
my $loc = $location->location_name.' '.
          $location->location_city.' '.
          $location->location_state_cd.' '.
          $location->location_country_cd;
my $customer = $device->Customer;
my $CID = $customer->ID;
my $cus = $customer->customer_name.' '.
          $customer->customer_city.' '.
          $customer->customer_state_cd;
my $locbut;
if($params{show_disabled_device} eq 'Y') {
   $locbut = qq|<button type="button" disabled>Change Location</button>|;
} else {
   $locbut = qq|<button type="button"
                 onclick="return redirectWithParams('change_location.cgi','device_id=$device_id');">
                 Change Location</button>|;
}
my $location_sql = q{
   SELECT l.location_name
     FROM location.vw_location_hierarchy a, location.location l
    WHERE descendent_location_id = ?
      AND l.location_id = a.ancestor_location_id
 ORDER BY depth
};
my $qo = OOCGI::Query->new($location_sql, { bind => [ $LID ] } );
my $rowNumber = $qo->rowNumber;
my $TL = OOCGI::NTable->new('border=0 width=100% noshade size=2 cellspacing=0');
$TL->insert(0,0,'&nbsp;'); # if location ID is null put something in here.
my $rn = 0;

    while(my @arr = $qo->next_array) {
       my $x = $rowNumber - $rn - 1;
       my $val;
       if($rn) {
          $val = '...' x $x.$arr[0];
       } else {
          my $loc = qq|<a href="javascript:redirectWithParams('edit_location.cgi','location_id=$LID')">$arr[0]</a>|;
          $val = '...' x $x.$loc;
       }
       $TL->insert($rn, 0, $val, 'style="font-family: courier; font-size: 12px;"');
       $rn++;
    }
    my $popLocation = $TL->string;

my $button = '';
if($LID > 1) { # do not show hierarchy for unknow locations ( large list )
   $button = BUTTON(undef,'View Hierarchy',{ onclick => "javascript:window.location = '/location_tree.cgi?source_id=$LID';"});
}
my $popCustomer = qq|<a href="javascript:redirectWithParams('edit_customer.cgi','customer_id=$CID')">$cus</a>|;
$T->put(0,0,'Authorization Location','colspan=4 class=header0');
$T->put(2,0,"Location",'class=leftheader');
$T->put(2,1,$popLocation);
$T->put(2,2,$locbut.'<br><br>'.$button,'align=center');
$T->put(1,0,"Customer",'class=leftheader');
$T->put(1,1,$popCustomer);
my $cusbut;
if($params{show_disabled_device} eq 'Y') {
   $cusbut = qq|<button type="button" disabled>Change Customer</button>|;
} else {
   $cusbut = qq|<button type="button"
                 onclick="return redirectWithParams('change_customer.cgi','device_id=$device_id')">
                 Change Customer</button>|;
}
$T->put(1,2,$cusbut,'align=center');

display $T;

