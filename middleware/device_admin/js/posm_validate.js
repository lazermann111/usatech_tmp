function confirmSubmit() {
  var agree=confirm("Are you sure you wish to proceeed with this change?");
  if (agree) {
      return true;
   } else {
      return false;
   }
}

function validateForm() {
   var i = 0;
   while (!(document.getElementById('radio_'+i) === undefined) && document.getElementById('radio_'+i) != null) {
      if(document.getElementById('radio_'+i).checked) {
         if(confirmSubmit()) {
            return true;
         } else {
            return false;
         }
      }
      i++;
   }
   alert("No item selected for update or delete.\nPlease select a radio button next to the item you wish to change.");
   return false;
}
