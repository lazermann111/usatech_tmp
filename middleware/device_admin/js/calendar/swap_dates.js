// following function requires two text field attached with calendar
// id of from date field should be an odd number such as 3, 23 or 33
// id of to element should be even number and must be next even number
// to assigned to from number. Considering above numbers it should be
// 4, 24 or 34
    function swap_dates(cal) {
       var num = this.inputField.id;
       var odd_even = num%2;
       var date_from;
       var date_to
       if(odd_even == 1) {
           date_from = document.getElementById(num);
           num++;
           date_to   = document.getElementById(num);
       } else {
           date_to   = document.getElementById(num);
           num--;
           date_from = document.getElementById(num);
       }
       if( date_from.value  != '' && date_to.value != '') {
           var obj_date_from   = new Date(date_from.value);
           var obj_date_to  = new Date(date_to.value);
           if( obj_date_to < obj_date_from  ) {
//             alert("Order of dates are wrong. It will be swapped!");
               var temp = date_from.value;
               date_from.value  = date_to.value;
               date_to.value = temp;
               this.inputField = date_from;
           }
        }
    }

// following function requires two text fields
// id of from date field should be an odd number such as 3, 23 or 33
// id of to element should be even number and must be next even number
// to assigned to from number. Considering above numbers it should be
// 4, 24 or 34
    function swap_fields() {
       var field_from = document.getElementById(33);
       var field_to   = document.getElementById(34);
       if( field_from.value  != '' && field_to.value != '') {
           if( field_to.value < field_from.value  ) {
               var temp = field_from.value;
               field_from.value  = field_to.value;
               field_to.value = temp;
           }
        }
    }
