//main menu tooltips
var tooltip_mm_home = 'Back To Device Admin Home Page';
var tooltip_mm_devices = 'Tools to search, view, and modify device information';
var tooltip_mm_locations = 'Search, create, or edit location information';
var tooltip_mm_customers = 'Search, create, or edit customer information';
var tooltip_mm_consumers = 'Search, view, or edit consumer and consumer account information';
var tooltip_mm_cards = 'Search, view, or edit card information for given Consumer merchant';
var tooltip_mm_transactions = 'Search for authority terminals or terminal batch information';
var tooltip_mm_file_type = 'Search for files transferred by (or to be transferred to) devices';
var tooltip_mm_config_templates = 'Edit device configuration or payment templates';
var tooltip_mm_reports = 'View performance metrics or exception reports';
var tooltip_mm_advanced_tools = 'Access GPRSWeb.<br>Configure various application templates.';
var tooltip_mm_posm = 'POSM Admin Scripts.';

//devices menu tooltips
var tooltip_sm_devices_search_checkbox = 'Check this checkbox only if<br>You want to get list of disabled devices.';
var tooltip_sm_devices_search_ev_number = 'Enter a complete EV number, or partial EV number, <br> or search with no text to list all devices<br><b>--- You Can Also Do the following ---</b><br>^TD to find all the devices with EV number starting with TD<br>24$ to find all the devices with EV number ending with 24<br>^TD%24$ to find all the devices with EV number starting with TD and ending with 24<br> ';
var tooltip_sm_devices_search_serial_number = 'Enter a complete serial number, partial text,<br>or search with no text to list all devices<br><b>--- You Can Also Do the following ---</b><br>^EV1 to find all the devices with serial number starting with EV1<br>8$ to find all the devices with serial number ending with 8<br>^EV1%8$ to find all the devices with serial number starting with EV1 and ending with 8<br> ';
var tooltip_sm_devices_load_customer = '<b><font color=red>NOTE:</font></b> Click to return to home page, where ' + "\\'" + 'by Customers' + "\\'" + ' menu item may be used';
var tooltip_sm_devices_load_location = '<b><font color=red>NOTE:</font></b> Click to return to home page, where ' + "\\'" + 'by Locations' + "\\'" + ' menu item may be used';
var tooltip_sm_devices_device_configuration_wizard = 'Configure a variety of settings for multiple ePorts (G4, G5, G6, etc.) at the same time';
var tooltip_sm_devices_serial_number_allocations = 'Reserve or report reserved serial numbers for any supported device';
var tooltip_sm_devices_device_cloning = 'Clone customer, location, and server payment settings from one device to another';

//locations menu tooltips
var tooltip_sm_locations_search_location_name = 'Enter the first few letters of or a complete location name to search';
var tooltip_sm_locations_search_location_city = 'Enter the first few letters or a city a complete city name to search';
var tooltip_sm_locations_new_location = "Enter a complete location name and click \\'Next\\' to begin creating a new location";

//customers menu tooltips
var tooltip_sm_customers_search_customer_name = 'Enter the first few letters of a customer name name to search';
var tooltip_sm_customers_new_customer = "Enter a complete location name and click \\'Next\\' to begin creating a new customer";
var tooltip_sm_customers_esuds_privileges = "Search, create, or edit eSuds web user accounts and privileges";

//files menu tooltips
var tooltip_sm_files_search_file_name = 'Enter a few characters of or a complete file name to search';
var tooltip_sm_files_create_ext_file_transfer = "Enter a complete EV number and click \\'Next\\' to<br>begin creating an external file transfer";


//menu rendering stuff from here on out
_menuCloseDelay=30000;
_menuOpenDelay=150;
_subOffsetTop=2;
_subOffsetLeft=0;
forgetClickValue="true";

with(menuStyle=new mm_style()){
bordercolor="#999999";
borderstyle="solid";
borderwidth=2;
fontfamily="Verdana, Tahoma, Arial";
fontsize="100%";
fontstyle="normal";
headerbgcolor="#ffffff";
headercolor="#000000";
offbgcolor="#eee8aa";
offcolor="#000000";
onbgcolor="#00ffff";
oncolor="#000099";
padding=4;
pagebgcolor="#82B6D7";
pagecolor="black";
separatorcolor="#999999";
separatorsize=2;
subimage="/img/down_arrow.gif";
subimagepadding=2;
openonclick="true";
closeonclick="true";
}

with(HeadersStyle=new mm_style()){
bordercolor="#000080";
borderstyle="solid";
borderwidth=1;
fontfamily="verdana,helvetica";
fontsize="12px";
fontstyle="normal";
fontweight="normal";
headerbgcolor="#3399CC";
headercolor="#FFFF33";
offbgcolor="#eee8aa";
offcolor="#666666";
onbgcolor="#00ffff";
oncolor="#000000";
padding=5;
subimage="/img/arrow.gif";
onsubimage="/img/arrow.gif";
}

with(submenuStyleG=new mm_style()){
styleid=0;
bordercolor="#000000";
borderstyle="solid";
borderwidth=1;
fontfamily="Verdana, Tahoma, Arial";
fontsize="10pt";
fontstyle="bold";
headerbgcolor="#ffffff";
headercolor="#000000";
offbgcolor="#eee8aa";
offcolor="#000000";
onbgcolor="#00ffff";
oncolor="#000000";
padding=4;
pagebgcolor="#82B6D7";
pagecolor="black";
subimage="/img/arrow.gif";
onsubimage="/img/arrow.gif";
subimagepadding=0;
}
with(submenuStyleGR=new mm_style()){
styleid=0;
bordercolor="#000000";
borderstyle="solid";
borderwidth=1;
fontfamily="Verdana, Tahoma, Arial";
fontsize="10pt";
fontstyle="bold";
headerbgcolor="#ffffff";
headercolor="#000000";
offbgcolor="#eee8aa";
offcolor="#000000";
onbgcolor="#00ffff";
oncolor="#000000";
padding=4;
pagebgcolor="#82B6D7";
pagecolor="black";
subimage="/img/arrow_left.gif";
onsubimage="/img/arrow_left.gif";
subimagepadding=0;
}

with(milonic=new menuname("Main Menu")){
style=menuStyle;
followscroll='1';
top=30;
screenposition="center"
alwaysvisible=1;
orientation="horizontal";
aI("showmenu=user;text=<div id='user_name_id'></div>;tooltip=User;");
aI("text=Home;url=/index.cgi?index1=2;tooltip=" + tooltip_mm_home);
aI("showmenu=Devices;text=Devices;tooltip=" + tooltip_mm_devices);
aI("showmenu=loca_type;text=Locations;tooltip=" + tooltip_mm_locations);
aI("showmenu=cust_type;text=Customers;tooltip=" + tooltip_mm_customers);
aI("showmenu=consumers;text=Consumers;tooltip=" + tooltip_mm_consumers);
aI("text=Transactions;url=/tran_menu.cgi;tooltip=" + tooltip_mm_transactions);
aI("showmenu=file_type;text=Files;tooltip=" + tooltip_mm_file_type);
aI("text=Reports;url=/utility_menu.cgi;tooltip=" + tooltip_mm_reports);
aI("showmenu=mainadvancedtools;text=Advanced Tools;tooltip=" + tooltip_mm_advanced_tools);
}

with(milonic=new menuname("cust_type")) {
style=HeadersStyle;
overflow="scroll";
aI("align=center;text=Customer Name:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/customer_list.cgi name=search><table><tr><td><input name=customer_name size=16 onmouseover='showtip(tooltip_sm_customers_search_customer_name)' onmouseout='hidetip()'> <input type=submit value=Search></td></tr></table></form>;type=form;");
aI("align=center;text=New Customer Name:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/edit_customer.cgi name=search><table><tr><td><input name=customer_name size=16 onmouseover='showtip(tooltip_sm_customers_new_customer)' onmouseout='hidetip()'> <input type=submit value=Next></td></tr></table></form>;type=form;");
aI("align=center;text=Customer Type;type=header;");
aI("text=Distributor;url=/customer_list.cgi?customer_type=2&action=Search;");
aI("text=End User;url=/customer_list.cgi?customer_type=3&action=Search;");
aI("text=Sony PictureStation Operator;url=/customer_list.cgi?customer_type=4&action=Search;");
aI("text=eSuds Operator;url=/customer_list.cgi?customer_type=1&action=Search;");
aI("text=Advanced Tools;type=header;");
aI("text=eSuds Web Privilege Manager;url=/esuds_privileges.cgi;tooltip=" + tooltip_sm_customers_esuds_privileges);
}

with(milonic=new menuname("mainadvancedtools")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
aI("text=GPRSWeb;url=/gprsweb/");
aI("text=Config Templates;url=/template_menu.cgi;tooltip=" + tooltip_mm_config_templates + ";separatorsize=1");
aI("showmenu=posm_level_0_0;url=/posm/admin/index.cgi;separatorsize=1;text=POSM;");
aI("text=Refresh Menu Content;url=/js_refresh.cgi");
aI("text=Refresh Gprsweb Users;url=gprsweb_user.cgi");
}

with(milonic=new menuname("Customers")) {
style=submenuStyleG;
overflow="scroll";
align="center";
aI("showmenu=cust__;text=#0-9;");
aI("showmenu=cust_a;text=A;");
aI("showmenu=cust_b;text=B;");
aI("showmenu=cust_c;text=C;");
aI("showmenu=cust_d;text=D;");
aI("showmenu=cust_e;text=E;");
aI("showmenu=cust_f;text=F;");
aI("showmenu=cust_g;text=G;");
aI("showmenu=cust_h;text=H;");
aI("showmenu=cust_i;text=I;");
aI("showmenu=cust_j;text=J;");
aI("showmenu=cust_k;text=K;");
aI("showmenu=cust_l;text=L;");
aI("showmenu=cust_m;text=M;");
aI("showmenu=cust_n;text=N;");
aI("showmenu=cust_o;text=O;");
aI("showmenu=cust_q;text=Q;");
aI("showmenu=cust_p;text=P;");
aI("showmenu=cust_r;text=R;");
aI("showmenu=cust_s;text=S;");
aI("showmenu=cust_t;text=T;");
aI("showmenu=cust_u;text=U;");
aI("showmenu=cust_v;text=V;");
aI("showmenu=cust_w;text=W;");
aI("showmenu=cust_x;text=X;");
aI("showmenu=cust_y;text=Y;");
aI("showmenu=cust_z;text=Z;");
}

with(milonic=new menuname("user")) {
style=submenuStyleG;
overflow="scroll";
aI("url=/logout.cgi;text=Log Out;tooltip=Logout");
}

with(milonic=new menuname("consumers")) {
style=submenuStyleG;
overflow="scroll";
aI("url=/consumer_search.cgi;text=Consumer/Card Search;tooltip=" + tooltip_mm_consumers);
aI("url=/consumer_card_search.cgi;text=Card Search;tooltip=" + tooltip_mm_cards);
aI("url=/new_consumer_acct_1.cgi;text=Card Creation Wizard;");
}

with(milonic=new menuname("posm_level_0_0")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
overflow="scroll";
aI("showmenu=posm_level_1_0;separatorsize=1;text=Authority MID/TID;");
aI("showmenu=posm_level_1_1;separatorsize=1;text=Payment Mask Setup;");
aI("showmenu=posm_level_1_2;separatorsize=1;text=Authority Routing;");
aI("showmenu=posm_level_1_3;separatorsize=1;text=Advanced Setup;");
aI("showmenu=posm_level_1_4;separatorsize=1;text=API Config;");
}

with(milonic=new menuname("posm_level_1_0")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
overflow="scroll";
aI("url=/posm/admin/merchant.cgi;separatorsize=1;text=Merchant;");
aI("url=/posm/admin/terminal.cgi;separatorsize=1;text=Terminal;");
aI("showmenu=posm_level_2_0;separatorsize=1;text=Special Authority;");
}

with(milonic=new menuname("posm_level_1_1")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
overflow="scroll";
aI("url=/posm/admin/authority_payment_mask.cgi;separatorsize=1;text=Authority Payment Mask;");
aI("url=/posm/admin/payment_subtype.cgi;separatorsize=1;text=Payment Subtype;");
}

with(milonic=new menuname("posm_level_1_2")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
overflow="scroll";
aI("url=/posm/admin/authority.cgi;separatorsize=1;text=Authority;");
aI("url=/posm/admin/authority_type.cgi;separatorsize=1;text=Authority Type & Routing;");
aI("url=/posm/admin/authority_gateway.cgi;separatorsize=1;text=Authority Gateway;");
aI("url=/posm/admin/authority_server.cgi;separatorsize=1;text=Authority Server;");
aI("showmenu=posm_level_2_1;separatorsize=1;text=Special Authority;");
}

with(milonic=new menuname("posm_level_1_3")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
overflow="scroll";
aI("url=/posm/admin/authority_assn.cgi;separatorsize=1;text=Authority Associations;");
aI("url=/posm/admin/tran_line_item_type.cgi;separatorsize=1;text=Tran Line Item Types;");
}

with(milonic=new menuname("posm_level_1_4")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
overflow="scroll";
aI("url=/posm/admin/authority_service_type.cgi;separatorsize=1;text=Authority Service Type;");
aI("url=/posm/admin/authority_service.cgi;separatorsize=1;text=Authority Service;");
aI("url=/posm/admin/handler.cgi;separatorsize=1;text=Handler Class;");
}

with(milonic=new menuname("posm_level_2_0")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
overflow="scroll";
aI("url=/posm/admin/aramark_payment_type.cgi;separatorsize=1;text=Aramark Payment Type;");
//aI("url=/posm/admin/banorte_terminal.cgi;separatorsize=1;text=Banorte Terminal;");
aI("url=/posm/admin/blackbrd_tender.cgi;separatorsize=1;text=Blackboard Tender;");
aI("url=/posm/admin/internal_payment_type.cgi;separatorsize=1;text=USAT Internal Payment Type;");
//aI("url=/posm/admin/webpos_terminal.cgi;separatorsize=1;text=Webpos Terminal;");
}

with(milonic=new menuname("posm_level_2_1")) {
openstyle="rtl";
align="left";
style=submenuStyleGR;
overflow="scroll";
aI("url=/posm/admin/aramark_authority.cgi;separatorsize=1;text=Aramark Authority;");
//aI("url=/posm/admin/banorte_authority.cgi;separatorsize=1;text=Banorte Authority;");
aI("url=/posm/admin/blackbrd_authority.cgi;separatorsize=1;text=Blackboard Authority;");
aI("url=/posm/admin/internal_authority.cgi;separatorsize=1;text=USAT Internal Authority;");
//aI("url=/posm/admin/webpos_authority.cgi;separatorsize=1;text=Webpos Authority;");
}

drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
