with(milonic=new menuname("Devices")) {
style=HeadersStyle;
aI("text=EV Number:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/profile.cgi name=search><table><tr><td><input type=checkbox name=all_devices_for_this_ev value=Y onmouseover='showtip(tooltip_sm_devices_search_checkbox)' onmouseout='hidetip()'><input name=ev_number size=16 onmouseover='showtip(tooltip_sm_devices_search_ev_number)' onmouseout='hidetip()'><input type=hidden name=tab value=1> <input type=submit value=Search></td></tr></table></form>;type=form;");
aI("text=Serial Number:;type=header;");
aI("text=<FORM METHOD=GET ACTION=/profile.cgi name=search><table><tr><td align=left><input name=serial_number size=16 onmouseover='showtip(tooltip_sm_devices_search_serial_number)' onmouseout='hidetip()'><input type=hidden name=tab value=1> <input type=submit value=Search></td></tr></table></form>;type=form;");
aI("text=View Devices;type=header;");
aI("showmenu=Customers;text=by Customer;");
aI("showmenu=Locations;text=by Location;");
aI("showmenu=device_type;text=by Type;");
aI("text=Advanced Tools;type=header;");
aI("text=Device Configuration Wizard;url=/bulk_config_wizard_1.cgi;tooltip=" + tooltip_sm_devices_device_configuration_wizard);
aI("text=Serial Number Allocations;url=/serial_number.cgi;tooltip=" + tooltip_sm_devices_serial_number_allocations);
aI("text=Device Cloning;url=/profile_clone.cgi;tooltip=" + tooltip_sm_devices_device_cloning);
}

with(milonic=new menuname("Locations")) {
style=submenuStyleG;
overflow="scroll";
align="center";
aI("showmenu=loca__;text=#0-9;");
aI("showmenu=loca_a;text=A;");
aI("showmenu=loca_b;text=B;");
aI("showmenu=loca_c;text=C;");
aI("showmenu=loca_d;text=D;");
aI("showmenu=loca_e;text=E;");
aI("showmenu=loca_f;text=F;");
aI("showmenu=loca_g;text=G;");
aI("showmenu=loca_h;text=H;");
aI("showmenu=loca_i;text=I;");
aI("showmenu=loca_j;text=J;");
aI("showmenu=loca_k;text=K;");
aI("showmenu=loca_l;text=L;");
aI("showmenu=loca_m;text=M;");
aI("showmenu=loca_n;text=N;");
aI("showmenu=loca_o;text=O;");
aI("showmenu=loca_q;text=Q;");
aI("showmenu=loca_p;text=P;");
aI("showmenu=loca_r;text=R;");
aI("showmenu=loca_s;text=S;");
aI("showmenu=loca_t;text=T;");
aI("showmenu=loca_u;text=U;");
aI("showmenu=loca_v;text=V;");
aI("showmenu=loca_w;text=W;");
aI("showmenu=loca_x;text=X;");
aI("showmenu=loca_y;text=Y;");
aI("showmenu=loca_z;text=Z;");
}

drawMenus()
