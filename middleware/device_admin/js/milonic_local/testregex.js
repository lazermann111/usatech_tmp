function testRegex(ID) {
    var regexid = 'regex_';
    var backrid = 'backr_';
    var testvid = 'testv_';
	if(ID) {
       regexid = 'regex_'+ID;
       backrid = 'backr_'+ID;
       testvid = 'testv_'+ID;
    }
    var regex = document.getElementById(regexid).value;
    var backr = document.getElementById(backrid).value;
    var testv = document.getElementById(testvid).value;

	var regexBrefCodes = new Array('1','1.1','1.2','1.3','1.4','1.5','1.6','1.7','1.8','1.9','10','11','12','13','14','15','16','17','2','3','4','5','6','7','8','9');
	var regexBrefNames = new Array('Primary\ Account\ Number','Primary\ Account\ Number\,\ Part\ 1','Primary\ Account\ Number\,\ Part\ 2','Primary\ Account\ Number\,\ Part\ 3','Primary\ Account\ Number\,\ Part\ 4','Primary\ Account\ Number\,\ Part\ 5','Primary\ Account\ Number\,\ Part\ 6','Primary\ Account\ Number\,\ Part\ 7','Primary\ Account\ Number\,\ Part\ 8','Primary\ Account\ Number\,\ Part\ 9','Issue\ Code\ \(Issue\ Num\,\ Lost\ Card\ Code\,\ etc\.\)','Country\ Code\ \(numeric\)','Check\ Data\:\ Longitude\ Redundancy','Check\ Data\:\ mod\ 10\ \(Luhn\)','Check\ Data\:\ mod\ 97','Group\ Code','Verification\ Value','Service\ Card\ Process\ Code','Name\ \(Card\ Holder\)','Expiration\ Date\ \(YYMM\ or\ YYYYMM\)','Additional\ Data\ \(service\ code\,\ etc\.\)','Discretionary\ Data\ \(PVKI\,\ PVV\,\ Offset\,\ CVV\,\ CVC\,\ etc\.\)','Use\ and\ Security\ Data\ \(Track\ 3\)','Custom\ Data\ 1','Custom\ Data\ 2','Custom\ Data\ 3');
	var regexBrefDescs = new Array('','','','','','','','','','','','','','','','','','','','','','','','','','');
	var regexBrefTypes = new Array();
	for (var i = 0; i < regexBrefCodes.length; i++) {
		regexBrefTypes[regexBrefCodes[i]] = regexBrefNames[i];
	}

	var myRegex = new RegExp(regex, 'i');
	var myResArr = myRegex.exec(testv);
	var myRegexBref = backr;	
	if (myResArr == null) {
		alert("Invalid Match.\n\nPlease check that you have selected the correct method\nand have entered a correct sample string.");
	}
	else {
		var brefResStr = "";
		if (myRegexBref != '') {
			brefResStr = "\n\nMatched substring results are as follows:\n";
			var myBrefCodeArr = myRegexBref.split('|');
			for (var i = 0; i < myBrefCodeArr.length; i++) {
				var myBrefPartsArr = myBrefCodeArr[i].split(':');	//0=num, 1=db id
				//alert("myBrefPartsArr="+myBrefPartsArr+"\nmyResArr="+myResArr+"\nregexBrefTypes="+regexBrefTypes);
				brefResStr = brefResStr + " - " + regexBrefTypes[myBrefPartsArr[1]] + ": " + myResArr[myBrefPartsArr[0]] + "\n";
			}
		}
		alert('Successful Match!' + brefResStr);
	}
}
