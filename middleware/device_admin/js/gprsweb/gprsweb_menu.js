_menuCloseDelay=30000;
_menuOpenDelay=150;
_subOffsetTop=2;
_subOffsetLeft=0;

forgetClickValue="true";

with(menuStyle=new mm_style()){
bordercolor="#999999";
borderstyle="solid";
borderwidth=1;
fontfamily="Verdana, Tahoma, Arial";
fontsize="100%";
fontstyle="bold";
headerbgcolor="#ffffff";
headercolor="#000000";
offbgcolor="#eee8aa";
offcolor="#000000";
onbgcolor="#00ffff";
oncolor="#000099";
padding=4;
pagebgcolor="#82B6D7";
pagecolor="black";
separatorcolor="#999999";
separatorsize=1;
subimage="/img/down_arrow.gif";
subimagepadding=2;
openonclick="true";
closeonclick="true";
}

with(HeadersStyle=new mm_style()){
bordercolor="#000080";
borderstyle="solid";
borderwidth=1;
fontfamily="verdana,helvetica";
fontsize="12px";
fontstyle="normal";
fontweight="normal";
headerbgcolor="#3399CC";
headercolor="#FFFF33";
offbgcolor="#eee8aa";
offcolor="#666666";
onbgcolor="#00ffff";
oncolor="#000000";
padding=5;
subimage="/img/arrow.gif";
onsubimage="/img/arrow.gif";
}

with(milonic=new menuname("Main Menu")){
followscroll='1';
divides=2;
top=30;
screenposition="center"
alwaysvisible=1;
orientation="horizontal";
style=menuStyle;
aI("status=Back To Device Admin;text=Home;url=/;tooltip=Back To Device Admin");
aI("text=Search;url=/gprsweb/search.cgi;tooltip=Search for SIM card information using a variety of methods");
aI("text=Register;url=/gprsweb/register.cgi;tooltip=Register a new Cingular SIM card order");
aI("text=Allocate;url=/gprsweb/allocate_search.cgi;tooltip=Allocate a block of new SIM cards");
aI("text=Activate;url=/gprsweb/activate_search.cgi;tooltip=Search for SIM cards to activate");
aI("text=Usage;url=/gprsweb/usage.cgi;tooltip=View reports on SIM card usage, plan information, or exceeded monthly plan limits");
aI("text=Refresh;url=/gprsweb/synchronize.cgi;tooltip=Refresh Cingular SIM card information<br>(<b>NOTE</b>: use only if necessary)");
aI("text=Status;url=/gprsweb/status.cgi;tooltip=Legend of color codes used through this web site");
aI("text=All;url=/gprsweb/search_results.cgi?status=&action=Search+SIMs;");
aI("text=Registered;url=/gprsweb/search_results.cgi?status=1&action=Search+SIMs;offbgcolor=#9999FF;onbgcolor=#9999FF");
aI("text=Allocated;url=/gprsweb/search_results.cgi?status=2&action=Search+SIMs;offbgcolor=#99CCCC;onbgcolor=#99CCCC");
aI("text=Activated;url=/gprsweb/search_results.cgi?status=4&action=Search+SIMs;offbgcolor=#99FF99;onbgcolor=#99FF99");
aI("text=Pending;url=/gprsweb/search_results.cgi?status=3&action=Search+SIMs;offbgcolor=#FFFF99;onbgcolor=#FFFF99");
aI("text=Assigned;url=/gprsweb/search_results.cgi?status=5&action=Search+SIMs;offbgcolor=#FF9999;onbgcolor=#FF9999");
}

drawMenus()

// Add this bit if you haven't finished building menus yet.
clearTimeout(_mst)
_mst=null
_startM=1
