#!/usr/local/USAT/bin/perl

use strict;
use CGI::Carp qw(fatalsToBrowser);
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj::Da_session;

my $query = OOCGI::OOCGI->new;


if(!$query->param('index1')) {
   print CGI->redirect("/index.cgi?index1=2");
   exit(0);
}

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader;
USAT::DeviceAdmin::UI::DAHeader->printFile('header.html');

print $user_menu;

print q|
<style>
div.notes {
text-align: justify;
margin:0 10%;
color:#000fff;
background-color:#00FFFF;
}
h1,h2,p{margin: 0 10px}
h1{font-size: 150%;color: #330000}
h2{font-size: 120%;color: #000}
p{padding-bottom:1em}
h2{padding-top: 0.3em}
</style>
<table cellspacing="0" cellpadding="6" border=1 width="100%">
 <tr>
  <th colspan="1" bgcolor="#C0C0C0">USA Technologies Device Admin</th>
 </tr>
</table>
&nbsp;
<table cellspacing="0" cellpadding="2" border="1" width="95%">
 <tr>
  <th colspan="2" bgcolor="#C0C0C0">Quick Links</th>
 </tr>
 <tr>
  <td width="50%"><li><a href="http://www.usatech.com">USA Technologies Public Website - www.usatech.com</a></li></td>
  <td width="50%"><li><a href="http://usalive.usatech.com">USALive Customer Reporting - usalive.usatech.com</a></li></td>
 </tr>
 <tr>
  <td width="50%"><li><a href="http://knowledgebase.usatech.com">Internal Intranet - USA Technologies Knowledge Base</a></li></td>
  <td width="50%"><li><a href="http://www.usatech.com/manuals">Public Manuals</a></li></td>
 </tr>
 <tr>
  <td width="50%"><li><a href="http://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=Products">Products</a></li></td>
  <td width="50%"><li><a href="http://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=Forms">Forms</a></li></td>
 </tr>
 <tr>
  <td width="50%"><li><a href="http://knowledgebase.usatech.com/JSPWiki/Wiki.jsp?page=PhoneExtensions">Employee Directory</a></li></td>
  <td width="50%"><li><a href="https://red-mao.cingular.com/USA/">Cingular - Enterprise On Demand Website</a></li></td>
 </tr>
 <tr>
  <td width="50%"><li><a href="http://dashboard.usatech.com/">Dashboard</a></li></td>
  <td width="50%"><li><a href="http://usanetrax1.usatech.com/mrtg/">MRTG Reports</a></li></td>
 </tr>
 <tr>
  <td width="50%"><li><a href="/terminal_list.cgi?authority_id=2">First Horizon Merchant List</a></li></td>
  <td width="50%"><li><a href="/batch_list.cgi?terminal_id=&terminal_batch_num=&state=open&authority_id=&open_after=&open_beore=&close_after=&close_beore=&action=List+Batches">Currently Open First Horizon Batches</a></li></td>
 </tr>
 <tr>
  <td width="50%"><li><a href="http://finance.yahoo.com/q?s=USAT">USA Technologies Inc. (USAT) - Yahoo! Finance</a></li></td>
  <td width="50%"><li><a href="http://www.sec.gov/cgi-bin/browse-edgar?company=usa+technologies&CIK=&State=&SIC=&action=getcompany">SEC Filings</a></li></td>
 </tr>
</table>
&nbsp;
<table cellspacing="0" cellpadding="2" border=1 width="95%">
 <tr>
  <th colspan="2" bgcolor="#C0C0C0">Help</th>
 </tr>
 <tr>
  <td width="50%"><li><a href="/whatsnew.cgi">What's New?</a></li></td>
  <td width="50%"><li><a href="/help.cgi">Help</a></li></td>
 </tr>
</table>

<br>
|;

USAT::DeviceAdmin::UI::DAHeader->printFooter('footer.html');
