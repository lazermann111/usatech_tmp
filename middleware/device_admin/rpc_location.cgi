#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;

my $log = OOCGI::Log->new('log_rpc');

my %params = OOCGI::OOCGI->new()->Vars();
#my $query_string = "";
#foreach my $key ( keys %ENV ) {
## $log->print("KEY $key $ENV{$key}");
#  if($key eq 'QUERY_STRING') {
#     $query_string = $ENV{$key};
#  }
#}
#
#my %params = ();
#my @pairs = split(/&/, $query_string);
#
print "Content-Type: text/html; charset=ISO-8859-1\n\n";
#foreach my $pair ( @pairs ) {
#  $pair =~ tr/Q/=/;
#  my ($name, $value) = split(/=/,$pair);
## print "NN|$name|VV|$value|<br>";
#  $params{$name} = $value;
#}
#$log->print("QS = $query_string");

my @arr = split(/,/, $params{notinthis});
my $bind_ref;
my $questionmarks;
#############################my $hhh; 
if($#arr > 100 ) {
   $questionmarks = $params{notinthis};
   $bind_ref = [$params{param} ];
#############################   $hhh = '111';
} else {
   $questionmarks = '?,' x ($#arr+1);
   chop $questionmarks;
   $bind_ref = [$params{param}, @arr ];
#############################   $hhh = '222';
}

#############################warn "AAA  $hhh @{$bind_ref} <br>";
my $sql = qq{
	SELECT   l.location_id, RPAD ('.', DEPTH * 3, '.') || l.location_name,
			 l.location_city, l.location_state_cd, l.location_country_cd
		FROM vw_location_hierarchy vlh,
			 (SELECT DISTINCT parent_location_id location_id
						 /* get distinct schools */
			  FROM            LOCATION.LOCATION lx, cust_loc clx
						WHERE lx.location_id = clx.location_id
						  /* join on campus */
						  AND clx.customer_id = ? ) x,
			 LOCATION.LOCATION l,
			 cust_loc cl
	   WHERE x.location_id = vlh.ancestor_location_id
		 AND l.location_id = vlh.descendent_location_id
		 AND l.location_id = cl.location_id(+)
         AND l.location_id NOT IN( $questionmarks ) 
		 AND (   DEPTH = 0
			  OR (DEPTH = 1 AND cl.customer_id = ? AND cl.location_id IS NOT NULL
				 )
			 )
	ORDER BY LOWER (vlh.hierarchy_path)
		 };
   if($params{location} eq 'S') { # School
#############################warn "CCC  $hhh|@{$bind_ref}|$questionmarks <br>";
       $sql = qq{
			SELECT DISTINCT l.location_id, l.location_name, l.location_city, l.location_state_cd,
				   l.location_country_cd
			  FROM LOCATION.LOCATION l_descendent,
				   LOCATION.LOCATION l,
				   cust_loc cl
			 WHERE l.location_id = l_descendent.parent_location_id
			   AND cl.location_id = l_descendent.location_id
			   AND cl.customer_id = ?
               AND l.location_id NOT IN ( $questionmarks )
          ORDER BY l.location_name, l.location_city, l.location_state_cd
   		};
   } elsif($params{location} eq 'C') { # Campus
#############################warn "DDD  $hhh @{$bind_ref} <br>";
       $sql = qq{
			SELECT DISTINCT l.location_id, l.location_name, l.location_city, l.location_state_cd,
				   l.location_country_cd
			  FROM LOCATION.LOCATION l, cust_loc cl
			 WHERE cl.location_id = l.location_id AND cl.customer_id = ?
               AND l.location_id NOT IN( $questionmarks )   
          ORDER BY l.location_name, l.location_city, l.location_state_cd
 		};
   } else {
    push(@{$bind_ref}, $params{param}); 
#############################warn "EEE  $hhh @{$bind_ref} <br>";
   		#default: SC case
   }
  

   my $popLocation = OOCGI::Popup->new(name => $params{location_div_prefix}.'__location_id',
                   align => 'yes', delimiter => '.',
                   id    => $params{location_div_prefix}.'__location_id_div',
                   bind  => $bind_ref,
                   onchange => "confirmLocation('$params{location_div_prefix}')",
                   style => "font-family: courier; font-size: 12px;",
                   sql   => $sql);

    $popLocation->head('1','Undefined');
    $popLocation->default('1');

print '<b>Location : </b>'.$popLocation->string;
