#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print qq|<form method="post" action="new_consumer_acct_2b.cgi">|;

my $err_str;



if((not defined $params{'merchant_id'}) || length($params{'merchant_id'}) == 0)
{
	$err_str .= B('Required parameter not found: merchant_name<br>', 'red') unless(length($params{'merchant_name'}) > 0);
	$err_str .= B('Required parameter not found: merchant_desc<br>', 'red') unless(length($params{'merchant_desc'}) > 0);
	$err_str .= B('Required parameter not found: terminal_desc<br>', 'red') unless(length($params{'terminal_desc'}) > 0);
}

if($err_str)
{
	print $err_str;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $device_type_sql = q|
	select distinct device_type.device_type_id, device_type.device_type_desc
	from device_type, device.device_type_action
	where device_type.device_type_id = device_type_action.device_type_id
	order by device_type.device_type_id|;

my $rc = 0;	
my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
$table->put($rc++,0, B('USAT Card Creation Wizard'),'align=center bgcolor=#C0C0C0 colspan=2');

my $device_type_popup = OOCGI::Popup->new(
					name => 'device_type_id',
					align => 'yes', 
					delimiter => '.',
					style => "font-family: courier; font-size: 12px;",
					sql => $device_type_sql);

$table->put($rc,0,"Target Device Type", 'nowrap');
$table->put($rc++,1,$device_type_popup);

display $table;

HIDDEN_PASS('consumer_acct_fmt_id', 'customer_id', 'location_id', 'authority_id', 'merchant_id', 'merchant_name', 'merchant_desc');

print "&nbsp<br>" . SUBMIT('action','Next >') . "</form><br>&nbsp;";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");


