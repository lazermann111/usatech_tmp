#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

my %cycle_codes_hash = (	'0'	=>	'No Status Available',
				'1'	=>	'Idle, Available',
				'2'	=>	'In 1st Cycle',
				'3'	=>	'Out Of Service',
				'4'	=>	'Nothing on Port',
				'5'	=>	'Idle, Not Available',
				'6'	=>	'Manual Service Mode',
				'7'	=>	'In 2nd Cycle',
				'8'	=>	'Transaction In Progress',
			);

my %enabled_hash = (	'Y'	=>	'Enabled',
						'N'	=>	'Disabled' );

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print q|
<style type="text/css">
	   .warn			{ font-size:10pt; color:#FF0000; font-weight:normal; font-family:'Courier New', monospace; }
	   .location_type	{ font-size:8pt;  color:#999999; font-weight:normal; font-family:'Courier New', monospace; }
	   .location_name	{ font-size:10pt; color:#000000; font-weight:bold; font-family:'Courier New', monospace; }
	   .device			{ font-size:10pt; color:#0000FF; font-weight:normal; font-family:'Courier New', monospace; }
	   .ssn				{ font-size:10pt; color:#0000FF; font-weight:normal; font-family:'Courier New', monospace; }
	   .machine			{ font-size:8pt;  color:#339999; font-weight:normal; font-family:'Courier New', monospace; }
	   .cards			{ font-size:8pt; background-color: #FFFF00; font-weight:normal; font-family:'Courier New', monospace; }
</style>
|;

my $source_id = $PARAM{"source_id"};
if(length($source_id) == 0)
{
	print "Required parameter not found: source_id\n";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $root_id = $source_id;

while(1)
{
    my $sql = q{
       SELECT location.parent_location_id
         FROM location.location
        WHERE location.location_id = ?
    };

    my $qo = OOCGI::Query->new($sql, { bind => "$root_id" } );

    my $flag = 0;
    while(my @arr = $qo->next_array) {
       if(!$arr[0]) {
         $flag = 1;
         last;
       } 
       $root_id = $arr[0];
    }
    if($flag) {
       last;
    }
}

print_location($root_id);

sub print_location
{
	my ($location_id) = @_;
	
	my $sql = q|
		SELECT location.location_id,
               location.location_name,
               location_type.location_type_desc,
               count(consumer_acct.consumer_acct_id)
		  FROM location.location,
               location_type,
               consumer_acct
		 WHERE location.location_type_id = location_type.location_type_id 
		   AND location.location_id = ?
		   AND location.location_id = consumer_acct.location_id(+)
	  GROUP BY location.location_id, location.location_name, location_type.location_type_desc|;
    my @arr;
    my $qo = OOCGI::Query->new($sql, { bind => "$location_id" } );
	my $location_info_ref;
    while(my @arr = $qo->next_array) {
       $location_info_ref = \@arr;
    }

	$sql = q|
       SELECT device.device_id,
              device.device_name,
              device.device_serial_cd,
              device.device_type_id,
              device_type.device_type_desc,
              to_char(device.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'),
              ((sysdate-device.last_activity_ts)*1440),
              pos.pos_id,
              customer.customer_id,
              customer.customer_name
         FROM device,
              device_type,
              pos,
              customer
        WHERE device.device_type_id = device_type.device_type_id
          AND device.device_id = pos.device_id
          AND pos.location_id = ?
          AND pos.customer_id = customer.customer_id
          AND device.device_active_yn_flag = 'Y'|;

    $qo = OOCGI::Query->new($sql, { bind => "$location_id" } );
	
    my $device_count = $qo->rowNumber;
	my $esuds_device_count = 0;
	
	while( my @arr = $qo->next_array)
	{
		$esuds_device_count++ if($arr[3] eq '5');
	}
    $qo->rewind;
	
	$sql = q{
       SELECT location.location_id
         FROM location.location
        WHERE location.parent_location_id = ?
          AND location.location_active_yn_flag = 'Y'
     ORDER BY location.location_name };

    my $qoc = OOCGI::Query->new($sql, { bind => "$location_id" } );
	my $child_count = $qoc->rowNumber;

	my $location_type = $location_info_ref->[2];
	$location_type =~ s/Education - Secondary//;
	
	my $bgcolor = "#FFFFFF";
	if($source_id != $root_id && $location_id == $source_id)
	{
		$bgcolor = "#FFFFCC";
	}
	
	print qq|
	<table cellspacing="0" cellpadding="0" border="0">
	 <tr>
	  <td align="left" valign="center" bgcolor="$bgcolor" style="border-top: 1px solid black">
	   <img src="pixel.gif" width="220" height="1" border="0"><br>
	   <span class="location_type">$location_type |;
	
	print "($child_count children)" if($child_count > 1);
	
	print qq| </span><br>
	   <a href="edit_location.cgi?location_id=$location_info_ref->[0]" class="location_name">$location_info_ref->[1]</a>
	|;
	
	if($device_count > 0)
	{
		if($esuds_device_count > 1)
		{
			print qq|<span class="warn">($esuds_device_count Room Controllers!)</span>|;
		}
		elsif($device_count > 1)
		{
			print qq|<span class="location_type">($device_count devices)</span>|;
		}
		
		if($location_info_ref->[3] > 0)
		{
			print qq|<br><a href="consumer_search.cgi?location_id=$location_id&action=Search" class="cards">$location_info_ref->[3] cards at this location!</a>|;
		}

		while(my @device_arr = $qo->next_array) 
		{
			my $device_id = $device_arr[0];
            my $room_table = OOCGI::NTable->new(qq{cellspacing="0" align="left" cellpadding="0" border="0" nowrap});
            $room_table->put(0,0,'&#149;&nbsp;','class="device"');
            $room_table->put(0,1,qq{$device_arr[4]: <a href="profile.cgi?device_id=$device_arr[0]" class="ssn">$device_arr[2]</a>},
                                 'class="device" align="left" valign="top" nowrap');
				
            $room_table->put(1,0,'&nbsp;');

            my $room_string = '';			
			if($device_arr[3] == 5) # room controller
			{
				my $sql = q{
                   SELECT host_type.host_type_desc,
                          decode(host_type_desc, 'Stacked Dryer', count(1)/2, decode(host_type_desc, 'Stacked Washer/Dryer', count(1)/2, count(1))) 
                     FROM host,
                          host_type
                    WHERE host.host_type_id = host_type.host_type_id
                      AND host.device_id =  ?
                 GROUP BY host_type.host_type_desc};

                my $qoh = OOCGI::Query->new($sql, { bind => "$device_id" } );

            my $host_count = $qoh->rowNumber;

				while(my @host_totals_ref = $qoh->next_array) 
				{
					$room_string .= "$host_totals_ref[1] $host_totals_ref[0]s, \n";
				}
				$room_string .= "<br>\n";
			}
			else
			{
				$room_string .= qq|Customer: <a href="edit_customer.cgi?customer_id=$device_arr[8]" class="machine">$device_arr[9]</a><br>\n|;
			}
			
			$room_string .= "Last Activity: " . ($device_arr[6] > 75 ? qq|<font color="red">$device_arr[5]</font>| : $device_arr[5]) . "<br>";
				
			if($device_arr[3] == 5) # room controller
			{
				my $sql = q{
                   SELECT pos_pta.pos_pta_id,
                          payment_subtype.payment_subtype_name
                     FROM payment_subtype,
                          pos_pta,
                          pos
                    WHERE pos.pos_id = pos_pta.pos_id
                      AND pos_pta.payment_subtype_id = payment_subtype.payment_subtype_id
                      AND pos.device_id = ?
                      AND pos_pta.pos_pta_activation_ts < sysdate
                      AND (pos_pta.pos_pta_deactivation_ts is null or pos_pta.pos_pta_deactivation_ts > sysdate)};

                my $qod = OOCGI::Query->new($sql, { bind => "$device_id" } );
				my $payment_types_count = $qod->rowNumber;
				
				$room_string .= "Payment Types: ";
	
				while(my @payment_type_arr = $qod->next_array)
				{
					$room_string .= qq|<a href="edit_pos_pta_settings.cgi?pos_pta_id=$payment_type_arr[0]&device_id=$device_id&action=Edit%20Settings&callback_cgi=profile.cgi" class="machine">$payment_type_arr[1]</a> |;
				}
			}
		
            $room_table->put(1,1, $room_string,	q{class="machine" align="left" valign="top" nowrap});
            display $room_table
		}
	}
	elsif($child_count <= 0)
	{
		if($location_info_ref->[3] > 0)
		{
			print qq|<br><a href="consumer_search.cgi?location_id=$location_id&action=Search" class="cards">$location_info_ref->[3] cards at this location!</a>|;
		}
		
		print qq|<br>&nbsp;<span class="warn">Empty Location!</span>|;
	}
	else
	{
		if($location_info_ref->[3] > 0)
		{
			print qq|<br><a href="consumer_search.cgi?location_id=$location_id&action=Search" class="cards">$location_info_ref->[3] cards at this location!</a>|;
		}
	}

	print "  </td>\n";
	print "  <td>\n";
	
	if($child_count > 0)
	{
		while( my @child_arr = $qoc->next_array)
		{
			print_location($child_arr[0]);
		}
	}
	
	print "  </td>\n";
	print " </tr>\n";
	print "</table>\n";
}

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
