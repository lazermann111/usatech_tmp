#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Common::Const;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $sql;
$sql = "SELECT '', '-- Select Template --' file_transfer_name FROM dual
		UNION
		SELECT DECODE(file_transfer_type_cd, ?, 'MAP_', 'INI_') || file_transfer_name, file_transfer_name
        FROM device.file_transfer
        WHERE file_transfer_type_cd IN(?,?,?)
        ORDER BY file_transfer_name";

my $popFileName = OOCGI::Popup->new(name => 'file_name',
            sql => $sql, bind => [PKG_FILE_TYPE__G4_CONFIGURATION_DATA_TEMPLATE,
								  PKG_FILE_TYPE__G4_CONFIGURATION_DATA_TEMPLATE,
                                  PKG_FILE_TYPE__EDGE_DEFAULT_CONFIGURATION_TEMPLATE,
                                  PKG_FILE_TYPE__EDGE_CUSTOM_CONFIGURATION_TEMPLATE],
			onchange => 'selectTemplate(this);'
);

my $popMap = USAT::DeviceAdmin::UI::USAPopups->pop_map;

my $popEditMode = USAT::DeviceAdmin::UI::USAPopups->pop_edit_mode;
$sql = USAT::DeviceAdmin::UI::USAPopups->sql_file_transfer_name(17);
my $popMapEditor = OOCGI::Popup->new( name => 'map', sql => $sql );

$sql = USAT::DeviceAdmin::UI::USAPopups->sql_pos_pta_tmpl_id;
my $popPosPtaTmplID = OOCGI::Popup->new( name => 'pos_pta_tmpl_id', sql => $sql );

$sql = USAT::DeviceAdmin::UI::USAPopups->sql_file_transfer_name(17);
my $popMemoryMap = OOCGI::Popup->new(name => 'map', sql => $sql );

print qq|
<script>
function selectTemplate(template) {
	var mapVisibility;
	if (template.value.indexOf('INI_') == 0) {
		mapVisibility = 'hidden';
	} else {
		mapVisibility = 'visible';
	}
	document.getElementById('memory_map').style.visibility = mapVisibility;
}

function editTemplate() {
	var f = document.template_editor;
	if (f.file_name.value.indexOf('INI_') == 0) {
		window.location = 'edit_config_edge.cgi?file_name=' + escape(f.file_name.value.substring(4, f.file_name.value.length)) + '&edit_mode=' + f.edit_mode.value;
	} else {
		window.location = 'edit_config.cgi?file_name=' + escape(f.file_name.value.substring(4, f.file_name.value.length)) + '&edit_mode=' + f.edit_mode.value + '&map=' + escape(f.map.value);
	}
}
</script>
<table cellspacing="0" cellpadding="2" border=1 width="100%">
 <tr>
  <th colspan="4" bgcolor="#C0C0C0">Configuration Template Admin</th>
 </tr>
 <tr>
  <form name="template_editor">
  <td>Template File: </td>
  <td>
   $popFileName
   $popEditMode
   </td>
   <td>
   <div id="memory_map">
   Memory Map: 
   $popMap
   </div>
  </td>
  <td><input type="button" onclick="editTemplate();" value="Edit Template"></td>
  </form>
 </tr>
 <tr>
  <form method="get" action="edit_map.cgi">
  <td>Memory Map Editor: </td>
  <td colspan="2">
  $popMemoryMap
  </td>
  <td><input type="submit" value="Edit Map"></td>
  </form>
 </tr>
 
 <tr>
  <form method="get" action="edit_pos_pta_tmpl.cgi">
  <td>Payment Type Template: </td>
  <td colspan="2">
  $popPosPtaTmplID
  </td>
  <td>
   <input type="submit" name="action" value="Edit Template">
   <input type="submit" name="action" value="New">
   <input type="submit" name="action" value="Clone">
  </td>
  </form>
 </tr>
</table>
|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
