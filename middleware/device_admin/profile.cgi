#!/usr/local/USAT/bin/perl

use strict;

#use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::Util;
use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::Log;
use USAT::DeviceAdmin::DBObj::Da_session;
use OOCGI::IndependentAction;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::ProfileTables;
use USAT::DeviceAdmin::DBObj;
use USAT::Database;
use USAT::App::DeviceAdmin::Const qw(:globals );
use USAT::Common::Const;
use USAT::DeviceAdmin::UI::DAHeader;

use constant KIOSK_PUBLIC_PC => 1;  #used only in Kiosk context--indicates if device is PublicPC instance or not
use constant MAX_SESSION_TRACKED_DEVICE => 10;

my $SQL;
my $QOBJ;

my $query = OOCGI::OOCGI->new;
my %PARAM = $query->Vars();

my $ssn = $PARAM{ssn};

my $ev_number;
if($PARAM{ev_number}) {
   $ev_number = uc $PARAM{ev_number};
   $ev_number =~ s/ //g;
}
my $serial_number;
if($PARAM{serial_number}) {
   $serial_number = $PARAM{serial_number};
}
my $device_id;
if($PARAM{device_id}) {
  $device_id = $PARAM{device_id};
}

my $action = '';
if($PARAM{myaction}) {
   $action = $PARAM{myaction};
} elsif($PARAM{action}) {
   $action = $PARAM{myaction};
}


if(length($ev_number) == 0 && length($serial_number) == 0 && length($device_id) == 0)
{
	print CGI->redirect("/device_list.cgi?enabled='Y'");
    exit();
}

if($PARAM{all_devices_for_this_ev} eq 'Y')
{
	print CGI->redirect("/device_list.cgi?ev_number=$ev_number\&show_disabled_device=Y\&enabled='Y','N'");
    exit();
}

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
#kludge: patch for Oracle 10.1.0.x session default date format bug, causing occasional ORA-01801 errors on some views and PL/SQL
$DATABASE->do(query=>q{ALTER SESSION SET NLS_DATE_FORMAT='dd-mon-yy'}) if $DATABASE->{driver} =~ /Oracle$/;
my $dbh = $DATABASE->{handle};
OOCGI::Query->open_db($dbh);

my $device;
if($PARAM{show_disabled_device} eq 'Y') {
    $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);
} else {
   if($device_id)
   {
       $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);
       #note: sql must be on one line to mitigate potential: Oracle OCI8 + view issues
       my $sql = q|SELECT device_id, device_name FROM vw_device_last_active WHERE device_name = ?|;
       my $qo = OOCGI::Query->new($sql, { bind => $device->device_name } );
       if(my @arr = $qo->next_array) {
          $device_id = $arr[0];
          $ev_number = $arr[1];
       } else {
          USAT::DeviceAdmin::UI::DAHeader->printHeader();
          USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
          print B("No Active Device found for This Device ID '$device_id' ","RED",4); 
          USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
          exit();
       }
       $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);
   } elsif($ev_number) {
       #note: sql must be on one line to mitigate potential: Oracle OCI8 + view issues
       my $sql = q|SELECT device_id, device_name FROM vw_device_last_active WHERE device_name like ?|;
       my $bind;
       if($ev_number =~ /\^/ && $ev_number =~ /\$/ && $ev_number =~ /\%/) {
          $bind = $ev_number;
       } elsif($ev_number =~ /\^/) {
          $bind = $ev_number.'%';
       } elsif($ev_number =~ /\$/) {
          $bind = '%'.$ev_number;
       } else {
          $bind = '%'.$ev_number.'%';
       }
       $bind  =~ tr/\^//d;
       $bind  =~ tr/\$//d;
       my $qo = OOCGI::Query->new($sql, { bind => $bind });
       if ($qo->row_count > 1) {
		   print CGI->redirect("/device_list.cgi?enabled='Y'&ev_number=".CGI::escape(uc($ev_number)));
           exit();
       }
       $ev_number =~ tr/\^//d;
       $ev_number =~ tr/\$//d;
       $ev_number =~ tr/\%//d;
       if(my @arr = $qo->next) {
          $device_id = $arr[0];
          $ev_number = $arr[1];
       } else {
          USAT::DeviceAdmin::UI::DAHeader->printHeader();
          USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
          BR(2);
          B("No Device found for EV NUMBER '$ev_number' ","RED",2); 
          BR(3);
          USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
          exit();
       }
       $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);
   } else {
       #note: sql must be on one line to mitigate potential: Oracle OCI8 + view issues
       $serial_number =~ s/ //g;
       my $upper_serial_number = uc $serial_number;
       my $bind;
       if($upper_serial_number =~ /\^/ && $upper_serial_number =~ /\$/ && $upper_serial_number =~ /\%/) {
          $bind = $upper_serial_number;
       } elsif($upper_serial_number =~ /\^/) {
          $bind = $upper_serial_number.'%';
       } elsif($upper_serial_number =~ /\$/) {
          $bind = '%'.$upper_serial_number;
       } else {
          $bind = '%'.$upper_serial_number.'%';
       }
       $bind =~  tr/\^//d;
       $bind =~  tr/\$//d;
       my $sql = q|SELECT device_id, device_serial_cd FROM device.vw_device_last_active WHERE device_serial_cd like ?|;            
       my $qo = OOCGI::Query->new($sql, { bind => $bind } );
       if ($qo->row_count > 1) {
    	   print CGI->redirect("/device_list.cgi?enabled='Y'&serial_number=".CGI::escape(uc($serial_number)));
           exit();
       }
       if(my @arr = $qo->next) {
          $device_id     = $arr[0];
          $serial_number = $arr[1];
       } else {
          USAT::DeviceAdmin::UI::DAHeader->printHeader();
          USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
          BR(2);
          B("No Device found for SERIAL NUMBER '$serial_number' ","RED",2);
          BR(3);
          USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
          exit();
       }
       $device = USAT::DeviceAdmin::DBObj::Device->new($device_id);
   }
}
my ($session, $global_session) = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

### special case functions that may include HTTP redirects ###
if('Import Template' eq $action ) {
	$session->destroy;
    my $template_name = $PARAM{template_name};
    my $template_data = USAT::DeviceAdmin::Util::load_file($template_name, $dbh);
    my $ev_number = $device->device_name();
    if(not defined $template_data)
    {
        USAT::DeviceAdmin::UI::DAHeader->printHeader();
        USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print "$template_name not found";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
    }

    if(not USAT::DeviceAdmin::Util::save_file("$ev_number-CFG", $template_data, $dbh))
    {
        USAT::DeviceAdmin::UI::DAHeader->printHeader();
        USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print "$ev_number-CFG save failed!";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
    }

	print CGI->redirect("/edit_config.cgi?map=g4_generic_map.csv&file_name=$ev_number-CFG&ev_number=$ev_number&ssn=$ssn&edit_mode=B");
    exit();
}

### now we will globally print standard page HTTP headers ###
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;

### special case toggle action (which could change device_id being viewed?) ###
if('Toggle' eq $action) {
   my $last_active_flag = $PARAM{last_active_flag};
   if(length($last_active_flag) == 0)
   {
      print qq|
      <script>
      alert("Required Parameter Not Found: last_active_flag");
      </script>|;
   } else {
      if($last_active_flag =~ m/^(Y|N)$/o) {
      	 OOCGI::Query->dbh->begin_work();
         OOCGI::Query->update(
         	'device.device d',
         	{ device_active_yn_flag => 'N' },
         	undef,
         	q{WHERE EXISTS (
         		SELECT 1
         		FROM device.device dx
         		WHERE dx.device_id = d.device_id
         		AND device_name = ?
         		AND device_active_yn_flag = ?
         	)},
         	{ bind => [$device->device_name, 'Y'] }
         );
		if($last_active_flag eq 'Y') {
			$device = $device->new($device->ID);
		} elsif( $last_active_flag eq 'N' ) {
			$device->device_active_yn_flag('Y');
			$device = $device->update;
		}
		OOCGI::Query->dbh->commit();
      } else {
         print qq|
         <script>
         alert("Invalid Active Flag Value: $last_active_flag");
         </script>|;
      }
   }
}

### set some global page session properties ###
my $tab = $PARAM{tab}
	|| ($global_session->profile && exists($global_session->profile->{$device->device_name})
		? $global_session->profile->{$device->device_name}->{tab} : '')
	|| PROFILE_TAB__DEVICE_PROFILE;

if($device->ID) {  # set variables in session
   $session->device_id($device->ID);
   $session->ev_number($device->device_name);
   $session->tab($tab);
} else {
   print B("!",'red', 4);
   $session->destroy;
   USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
   exit;
}

### save specific session properties ###
if ($session->ev_number) {	#if ev_number & global session exists, update global session with new state information
	$global_session->profile({}) unless ref($global_session->profile) eq 'HASH';
	if (scalar keys %{$global_session->profile} > MAX_SESSION_TRACKED_DEVICE) {	#if we have exceeded our buffer, delete oldest element
		my $oldest_key = (sort {$global_session->profile->{$a}->{last_view_ts}
			<=> $global_session->profile->{$b}->{last_view_ts}} keys %{$global_session->profile})[0];
		delete $global_session->profile->{$oldest_key};
	}
	#now, store current session info
	$global_session->profile->{$session->ev_number} = {
		last_view_ts => time(),
		tab          => $session->tab
	};
}
$global_session->update();	#unlock as soon as possible
$global_session->unlock();	#unlock as soon as possible

$device_id      = $device->ID;
$ev_number      = $device->device_name;
$serial_number  = $device->device_serial_cd;
my $DTI = $device->device_type_id;

### print global javascript functions ###
if($tab eq PROFILE_TAB__PAYMENT_CONFIGURATION || $tab eq PROFILE_TAB__DEVICE_SETTINGS) {
print q{
<link rel="stylesheet" type="text/css" media="all" href="css/calendar/calendar-blue2.css" title="blue" />
<script language="JavaScript" src="/js/calendar/calendar.js"></script>
<script language="JavaScript" src="/js/calendar/calendar-en.js"></script>
<script language="JavaScript" src="/js/calendar/calendar-setup.js"></script>
<script language="JavaScript" src="/js/calendar/swap_dates.js"></script>
};
}
print <<EOHTML;
<script>
function confirmSubmit(str)
{
   var agree=confirm(str);
   if (agree)
     return true ;
   else
     return false ;
}
function getCheckedValue(radioObj) {
	if (!radioObj)
		return "";
	var radioLength = radioObj.length;
	if (radioLength == undefined)
		if (radioObj.checked)
			return radioObj.value;
		else
			return "";
	for (var i = 0; i < radioLength; i++) {
		if (radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}
function getSelectedValues(selObj) {
	if (!selObj)
		return new Array();
	var selLength;
	if ("options" in selObj)
		selLength = selObj.options.length;
	if (selLength == undefined)
		return new Array();
	var selectedArray = new Array();
	var count = 0;
	for (var i = 0; i < selLength; i++) {
		if (("selected" in selObj.options[i] && selObj.options[i].selected)
			|| ("checked" in selObj.options[i] && selObj.options[i].checked))
			selectedArray[count++] = selObj.options[i].value;
	}
	return selectedArray;
}
function redirectWithParams(targ, str, params) {	//TODO: test m-select and m-checkbox fields
// targ   - i.e. document.location.pathname
// str    - (optional) hand-coded query string (i.e. 'myaction=test1234&somevar=test')
// params - (optional) Array of objects (text, select, checkbox, etc.) to append to query string
	var func_method = 'get';	//'get' (for debugging) or 'post'

	// use current page pathname if no target specified
	if (targ === null || targ == '')
		targ = document.location.pathname;
	var newloc = '';
	if (func_method == "get")
		if (str != "" || (params != undefined && params.length > 0))
			newloc = targ+"?";
		else
			newloc = targ;
	if (str != "")
		newloc += str;
	if (params != undefined) {
		for (var i = 0; i < params.length; i++) {
	/*
			alert("obj="+params[i]
				+ "\\nname="+params[i].name
				+ "\\nvalue="+params[i].value
				+ "\\nprop length? " + ("length" in params[i])
				+ "\\nprop checked? " + ("checked" in params[i])
				+ "\\nprop selectedIndex? " + ("selectedIndex" in params[i])
				+ "\\nprop options? " + ("options" in params[i]));
			if ("length" in params[i]) {
				alert("obj[0]="+params[i][0]
					+ "\\nname="+params[i][0].name
					+ "\\nvalue="+params[i].value
					+ "\\nprop length? " + ("length" in params[i][0])
					+ "\\nprop checked? " + ("checked" in params[i][0])
					+ "\\nprop selectedIndex? " + ("selectedIndex" in params[i][0])
					+ "\\nprop options? " + ("options" in params[i][0]));
			}
	*/
			var name = '';
			var val = '';
			if ("length" in params[i]) {
				if ("options" in params[i]) {
					name = params[i].name;
					var selVals = getSelectedValues(params[i]);
					for (var i = 0; i < selVals.length; i++) {
						if (val == "")
							val = escape(selVals[i]);
						else
							val = val + "," + escape(selVals[i]);
					}
				}
				else {
					name = params[i][0].name;
					val = escape(getCheckedValue(params[i]));
				}
			} else if ("checked" in params[i] && params[i].checked == true) {
				name = params[i].name;
				val = escape(getCheckedValue(params[i]));
			} else {
				name = params[i].name;
				val = escape(params[i].value);
			}
			newloc += "&"+name+"="+val;
		}
	}

	if (func_method == 'get')
		document.location = newloc;
	else {
		var myform=document.createElement("form");
		myform.setAttribute("method", "post");
		myform.setAttribute("action", targ);

		var newvars = newloc.split("&");
		for (var i = 0; i < newvars.length; i++) {
			var pair = newvars[i].split("=");
			var myhidden=document.createElement("input");
			myhidden.setAttribute("type","hidden");
			myhidden.setAttribute("name",unescape(pair[0]));
			myhidden.setAttribute("value",unescape(pair[1]));
			myform.appendChild(myhidden);
		}
		var myinsertloc = document.getElementById('___my_redirectWithParams___');
		myinsertloc.appendChild(myform);
		myform.submit();
	}

	return false;
}
function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
			if (pair[0] == variable) {
			return pair[1];
		}
	}
	alert('Query Variable ' + variable + ' not found');
}

function ignoreEnterKey(e) {
	var pK = e ? e.which : window.event.keyCode;
	return pK != 13;
}

</script>
<div id="___my_redirectWithParams___"></div>
<style>

td.titlerow {
   text-align:center;
   background-color:#CCCCCC;
   font-size: 0.90em;
}

table { empty-cells:show; }

td.right {
   text-align:right;
}
  
td.center {
   text-align:center;
}
td.number {
   text-align:right;
}

form {margin:0; padding:0; }
</style>
EOHTML

### handle standard page actions (before any table rendering) ###
my $pending_cmd_id = $PARAM{cancel_pending_command_id};
if(defined $pending_cmd_id && $pending_cmd_id > 0) {
    my $mcp = USAT::DeviceAdmin::DBObj::Machine_cmd_pending->new($pending_cmd_id);
    # following lines added to make sure that we have correc_id to create an object
    # usually there is a problem when user delete a record and do refresh, this
    # linew will prevent program crashing
    if( defined $mcp && $mcp->ID ne '' ) {
       $mcp->execute_cd('C');
       $mcp->update;
   }
}

if( $action eq 'Download File' )
{
   my $download_file_name = $PARAM{download_file_name};

   if($DTI ne PKG_DEVICE_TYPE__EDGE &&
	  (length($download_file_name) == 0 ||
      $download_file_name eq 'full path on client' ||
      $download_file_name eq 'name or path on client')) {
       print qq|<script>
         alert("Required Parameter Not Found: download_file_name");
       </script>|;
   } else {
        # remove any pending File Transfer Start messages for this file name
        my $download_file_name_gz = $download_file_name.'.gz';
        my $sql = "
           UPDATE engine.machine_cmd_pending SET execute_cd = 'A'
            WHERE machine_command_pending_id
               IN (SELECT mcp.machine_command_pending_id
                     FROM device.file_transfer ft,
                          device.device_file_transfer dft,
                          engine.machine_cmd_pending mcp
                    WHERE UPPER(ft.file_transfer_name) 
                       IN (UPPER(?), UPPER(?))
                      AND dft.file_transfer_id = ft.file_transfer_id
                      AND dft.device_id = ?
                      AND dft.device_file_transfer_status_cd = 0
                      AND dft.device_file_transfer_direct = 'O'
				  AND mcp.machine_id = ?
				  AND mcp.data_type IN (" . FILE_TRANSFER_CMD_LIST . ")
                      AND dft.device_file_transfer_id = mcp.command
                      AND mcp.execute_cd = 'P')";
         OOCGI::Query->modify($sql, [$download_file_name, $download_file_name_gz, $device_id, $ev_number]);
	  
	  if($DTI eq PKG_DEVICE_TYPE__EDGE) {
         my $request_type = unpack('H2', pack('C', 1));
         my $file_type = unpack('H4', pack('n', $PARAM{download_file_type}));
		 my $file_name_length = unpack('H2', pack('C', length($download_file_name)));
		 my $file_name = unpack("H*", $download_file_name);
         my $command = uc($request_type . $file_type . $file_name_length . $file_name);
         USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,EPORT_CMD__GENERIC_REQUEST_V4_1,$command);
      } else {	  
		  my $dfn = uc(unpack("H*", $download_file_name));
		  USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,'9B',"$dfn");
      }
   }
}

if( $action eq DA_BTN__DOWNLOAD_CONFIG ) {
   my $dfn;
   my $file_transfer_name = 'EportNW.ini';
   if($DTI eq PKG_DEVICE_TYPE__SONY_PICTURESTATION ||
      $DTI eq PKG_DEVICE_TYPE__KIOSK ||
      $DTI eq PKG_DEVICE_TYPE__T2 ) {
      if($DTI eq PKG_DEVICE_TYPE__T2 )
      {
         $file_transfer_name = "$ev_number-CFG";
      }
      # remove any pending File Transfer Start messages for this file name
      my $sql = "UPDATE engine.machine_cmd_pending
                    SET execute_cd = 'A'
                  WHERE machine_command_pending_id 
                    IN (SELECT mcp.machine_command_pending_id
                          FROM device.file_transfer ft,
                               device.device_file_transfer dft,
                               engine.machine_cmd_pending mcp
                         WHERE upper(ft.file_transfer_name) = upper(?)
                           AND dft.file_transfer_id = ft.file_transfer_id
                           AND dft.device_id = $device_id
                           AND dft.device_file_transfer_status_cd = 0
                           AND dft.device_file_transfer_direct = 'O'
                           AND mcp.machine_id = ?
                           AND mcp.data_type IN (" . FILE_TRANSFER_CMD_LIST . ")
                           AND dft.device_file_transfer_id = mcp.command
                           AND mcp.execute_cd = 'P')";
      OOCGI::Query->modify($sql, [$file_transfer_name, $ev_number]);

      $dfn = uc(unpack("H*", "$file_transfer_name"));
      USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,'9B',"$dfn");
   } elsif($DTI eq PKG_DEVICE_TYPE__EDGE) {
      my $sql = q{UPDATE engine.machine_cmd_pending
                     SET execute_cd = 'A'
                   WHERE machine_id = ?
                     AND data_type  = ?
                     AND execute_cd = ?}; 
      OOCGI::Query->modify($sql, [ $ev_number, EPORT_CMD__GENERIC_REQUEST_V4_1, DA_CMD__P_PENDING ]);

      my $sql = 'SELECT * FROM (
					SELECT configurable_property_list_ids
					FROM device.device_type_property_list
					WHERE device_type_id = ?
						AND property_list_version <= ?
					ORDER BY property_list_version DESC
				) WHERE ROWNUM = 1';
      my $qo = OOCGI::Query->new($sql,
               { bind => [ PKG_DEVICE_TYPE__EDGE, $device->property_list_version ], LongReadLen => 50000 } );
      if(my @arr = $qo->next_array) {
         my $payload_length = unpack('H4', pack('n', length($arr[0])));
		 my $payload = unpack("H*", $arr[0]);
         my $command = '000008' . uc($payload_length . $payload);
         USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,EPORT_CMD__GENERIC_RESPONSE_V4_1,$command);
      }
   } elsif($DTI eq PKG_DEVICE_TYPE__ESUDS) {
      $dfn = unpack("H*", "/usr/local/usatech/etc/esuds.properties"); # download_file_name
      USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,'9B',"$dfn");
   }
}

if( $DTI eq PKG_DEVICE_TYPE__KIOSK && $action eq 'Download GPRS Info' ) {
   my $file_transfer_name = 'EportGPRSInfo';
   my $dfn = uc(unpack("H*", "$file_transfer_name"));
   USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,'9B',"$dfn");
}

my $msg = USAT::DeviceAdmin::DBObj::Machine_cmd_pending::formaction($query,$ev_number);
if($msg) {
   print B($msg,"RED",4);
}

if( 'Device Reactive' eq $action ) {
    USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,'76','');
}

if( 'Shutdown' eq $action ) {
    my $shutdown_option = $PARAM{shutdown_option};
    my $shutdown_state  = $PARAM{shutdown_state};
    my $command = "$shutdown_option$shutdown_state";
    USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,'73',$command);
}

# following is Additional part for ESUDS Device Profile (tab 1)
if( $action eq 'toggle_monitor_flag' ) {
   my $monitor_flag = $PARAM{monitor_flag};

   my $sql = q|SELECT count(*) FROM device_setting
             WHERE device_id = ?
               AND device_setting_parameter_cd = 'Monitor Room'|; 

  if(OOCGI::Query->new($sql, { bind => $device_id})->node(0,0)) {
      my $ds = USAT::DeviceAdmin::DBObj::Device_setting->new({ device_id => $device_id,
               device_setting_parameter_cd => 'Monitor Room'});
      if( $monitor_flag eq 'Y') {
          $ds->device_setting_value('N');
      } elsif( $monitor_flag eq 'N') {
          $ds->device_setting_value('Y');
      } else {
          $ds->device_setting_value('Y');
      }
      $ds->update;
   }
} 

if( $PARAM{userOP} eq DA_BTN__QUEUE_COMMAND) {
    # CB command consist of
    # 1 byte result code for this case it is 0
    # 1-256 bytes response message, we will use 1 byte and set it to zero
    # 1 byte action code  which comes from $PARAM{generic_response_s2c}
	# payload dependent on action code

	my $generic_response_s2c = $PARAM{generic_response_s2c};
	
    # result code
    my $result_code = unpack('H2',pack('C',0));

    # response message
    my $response_message = unpack('H2',pack('C',0));

    # action_code
	my $action_code = substr($generic_response_s2c, 1);
    my $action_code_hex = unpack('H2',pack('C', $action_code));
	
	my $command = $result_code.$response_message.$action_code_hex;
	
	my $payload;
	my $err;
	if ($generic_response_s2c eq '-1') {
		$err = 'Required Parameter Not Found: Action';
	} elsif (substr($generic_response_s2c, 0, 1) eq 'Y') {
		$payload = $PARAM{payload};
		if ($payload) {
			if ($action_code =~ /^5|10$/) {
				$command .= unpack('H4', pack('n', $payload));			
			} else {
				if ($action_code eq '8') {
					$command .= unpack('H4', pack('n', length($payload)));
				}
				$command .= unpack('H*', $payload);
			}		
		} else {
			$err = 'Required Parameter Not Found: Payload';
		}
	}

	if ($err) {
		print "<script>alert('$err');</script>";
	} else {
		USAT::DeviceAdmin::DBObj::Machine_cmd_pending::create_request($query,$ev_number,EPORT_CMD__GENERIC_RESPONSE_V4_1,uc($command));
	}
}

if('Firmware Version' eq $action) {
    my $mcp = USAT::DeviceAdmin::DBObj::Machine_cmd_pending->new;
    $mcp->machine_id($ev_number);
    $mcp->data_type(EPORT_CMD__PEEK_V1);
    $mcp->command('4400007F200000000F');
    $mcp->execute_cd(DA_CMD__P_PENDING);
    $mcp->execute_order('15');
    $mcp->insert;
}
elsif( 'Upload Config' eq $action ) {
    my $mcp = USAT::DeviceAdmin::DBObj::Machine_cmd_pending->new;
    $mcp->machine_id($ev_number);
    $mcp->data_type(EPORT_CMD__PEEK_V1);
    $mcp->command('4200000000000000FF');
    $mcp->execute_cd(DA_CMD__P_PENDING);
    $mcp->execute_order('1');
    $mcp->insert;
}
elsif( 'Wavecom Modem Info' eq $action ) {
    my $mcp = USAT::DeviceAdmin::DBObj::Machine_cmd_pending->new;
    $mcp->machine_id($ev_number);
    $mcp->data_type(EPORT_CMD__PEEK_V1);
    $mcp->command('4400802C0000000190');
    $mcp->execute_cd(DA_CMD__P_PENDING);
    $mcp->execute_order('16');
    $mcp->insert;
}
elsif( 'Boot Load/App Rev' eq $action ) {
    my $mcp = USAT::DeviceAdmin::DBObj::Machine_cmd_pending->new;
    $mcp->machine_id($ev_number);
    $mcp->data_type(EPORT_CMD__PEEK_V1);
    $mcp->command('4400807E9000000021');
    $mcp->execute_cd(DA_CMD__P_PENDING);
    $mcp->execute_order('17');
    $mcp->insert;
}
elsif( 'Bezel Info' eq $action ) {
    my $mcp = USAT::DeviceAdmin::DBObj::Machine_cmd_pending->new;
    $mcp->machine_id($ev_number);
    $mcp->data_type(EPORT_CMD__PEEK_V1);
    $mcp->command('4400807EB20000004C');
    $mcp->execute_cd(DA_CMD__P_PENDING);
    $mcp->execute_order('18');
    $mcp->insert;
}
elsif( 'Cancel All' eq $action ) {
    my $sql = q|UPDATE engine.machine_cmd_pending
				SET execute_cd = ?
				WHERE machine_command_pending_id IN (
					SELECT mcp.machine_command_pending_id
					FROM engine.machine_cmd_pending mcp
					LEFT OUTER JOIN device.device_file_transfer dft
						ON dft.device_file_transfer_id = CASE
							WHEN UPPER(mcp.data_type) IN (| . FILE_TRANSFER_CMD_LIST . q|) THEN TO_NUMBER(mcp.command)
							ELSE 0
						END
					LEFT OUTER JOIN device.file_transfer ft
						ON dft.file_transfer_id = ft.file_transfer_id   
					WHERE mcp.machine_id  = ?
						AND NVL(ft.file_transfer_type_cd, -1) <> ?
				)|;
    OOCGI::Query->modify($sql, [ 
		DA_CMD__C_CANCEL, 
		$ev_number, 
		PKG_FILE_TYPE__PROPERTY_LIST 
	] );
}
elsif('Reorder' eq $action) {
	my %priorities_hash = ();
	
	foreach my $param (keys %PARAM)
	{
		if($param =~ /^new_priority_/)
		{
			my $pos_pta_id = substr($param, 13);
			my $new_priority = $PARAM{$param};
			my $pos_pta_payment_entry_method_cd = $PARAM{"pos_pta_payment_entry_method_cd_$pos_pta_id"};
			
			printError("Missing Category Priority Value! (Category: $pos_pta_payment_entry_method_cd, POS PTA: $pos_pta_id)") if !$new_priority;
			
			my $hash_key = "$pos_pta_payment_entry_method_cd.$new_priority";
			
			if(defined $priorities_hash{$hash_key})
			{
				printError("Duplicate Category Priority Values! (Category: $pos_pta_payment_entry_method_cd, Priority: $new_priority)");
			}
			else
			{
				$priorities_hash{$hash_key} = $new_priority;
			}
		}
	}

	foreach my $param (keys %PARAM)
	{
		if($param =~ /^new_priority_/)
		{
			my $pos_pta_id = substr($param, 13);
			my $new_priority = $PARAM{$param};
			my $old_priority = $PARAM{"old_priority_$pos_pta_id"};
			
			if($new_priority != $old_priority)
			{
				my $update_pos_pta_priority = $DATABASE->do(
					query => q{
						UPDATE pss.pos_pta
						SET pos_pta_priority = ?
						WHERE pos_pta_id = ?
					},
					values => [
						$new_priority,
						$pos_pta_id
					]
				);
				printError("Database error updating pos_pta $pos_pta_id: ".$DATABASE->errstr) if defined $DATABASE->errstr;
			}
		}
	}
}

my $argsv = {};
$argsv->{session}           = $session;
$argsv->{query}             = $query;
$argsv->{object}            = $device;
$argsv->{ev_number_cfg}     = "$ev_number-CFG"; # do not put '-' into variable names
$argsv->{ev_number_cfg_config_file_data} = undef;
$argsv->{dbh}               = $dbh;

# calculate kiosk_show_full_config
if($DTI eq PKG_DEVICE_TYPE__SONY_PICTURESTATION
	|| $DTI eq PKG_DEVICE_TYPE__KIOSK
	|| $DTI eq PKG_DEVICE_TYPE__EDGE
	|| $DTI eq PKG_DEVICE_TYPE__T2) {  # put variable into this scope
      my $SQL = q|SELECT COUNT(1)
                    FROM device.file_transfer 
                   WHERE file_transfer_name = ?
					AND file_transfer_type_cd = ?|;
      my $qo = OOCGI::Query->new($SQL, { bind => [$argsv->{ev_number_cfg}, PKG_FILE_TYPE__CONFIGURATION_FILE] } );
      if($qo->node(0,0) > 0) {
          $session->kiosk_show_full_config(1);
      } else {
         # if there is no config file in the database, most likely it's a PictureStation with the old DLL
          $session->kiosk_show_full_config(0);
      }
}

my $tabTable = OOCGI::NTable->new('width=100%');
my $tabTable_cn = 0;
my @style;
for(my $i = 1; $i <= 5; $i++) {
    $style[$i] = 'style="background-color:#EEE8AA;text-align:center;"';
    if($i == $tab) {
       $style[$i] = 'style="background-color:#00ffff;text-align:center;"';
    }
}

# NOTE: user-defined constants to use with both object and in HTML page
use constant MY_IA_FUNC       => 'my_ia_js_function';
use constant MY_IA_PARAM      => 'tab';
use constant MY_IA_PARAM_LAST => MY_IA_PARAM.'last__';

# NOTE: all params must be globally unique within HTML page
my $ia = OOCGI::IndependentAction->new(
  MY_IA_FUNC,                  #your unique javascript function name to call to perform callback (requires one string param)
  MY_IA_PARAM.'form_id__',     #a unique form ID
  MY_IA_PARAM.'form_name__',   #a unique form name
  MY_IA_PARAM,                 #name of parameter to add to param list when performing callback
  MY_IA_PARAM_LAST             #name of parameter to add to param list when performing callback
);

my $style = qq|width:100%;background-color:transparent;|;
my $onclick = qq|onClick="|.MY_IA_FUNC.'(';
my $onclick_arg2 = qq|new Array('show_disabled_device', 'device_id', 'ev_number','serial_number')|;
my $button1 = qq|<button style="$style" $onclick|.PROFILE_TAB__DEVICE_PROFILE.','.$onclick_arg2.q|)"><b>Device Profile</b></button>|;
my $button2 = qq|<button style="$style" $onclick|.PROFILE_TAB__PAYMENT_CONFIGURATION.','.$onclick_arg2.q|)"><b>Payment Configuration</b></button>|;
my $button3 = qq|<button style="$style" $onclick|.PROFILE_TAB__DEVICE_CONFIGURATION.','.$onclick_arg2.q|)"><b>Device Configuration</b></button>|;
my $button4 = qq|<button style="$style" $onclick|.PROFILE_TAB__DEVICE_SETTINGS.','.$onclick_arg2.q|)"><b>Device Settings</b></button>|;

$tabTable->insert(0,$tabTable_cn++,$button1,$style[1]);
$tabTable->insert(0,$tabTable_cn++,$button2,$style[2]);
$tabTable->insert(0,$tabTable_cn++,$button3,$style[3]);
if ($DTI eq PKG_DEVICE_TYPE__G4
    || $DTI eq PKG_DEVICE_TYPE__G5
    || $DTI eq PKG_DEVICE_TYPE__MEI_TELEMETER
	|| $DTI eq PKG_DEVICE_TYPE__EDGE) {   #ePort-type device only
    $tabTable->insert(0,$tabTable_cn++,$button4,$style[4]);
    if ($DTI ne PKG_DEVICE_TYPE__EDGE) {
		my $button5 = qq|<button style="$style" $onclick|.PROFILE_TAB__FILE_TRANSFER.','.$onclick_arg2.q|)"><b>File Transfer</b></button>|;
		$tabTable->insert(0,$tabTable_cn++,$button5,$style[5]);
	}
}
if ($session->kiosk_show_full_config
    && ($DTI eq PKG_DEVICE_TYPE__SONY_PICTURESTATION
        || $DTI eq PKG_DEVICE_TYPE__KIOSK
        || $DTI eq PKG_DEVICE_TYPE__EDGE
        || $DTI eq PKG_DEVICE_TYPE__T2)) {
   my $device_subtype = 0;
   my ($config_file_hex, $config_file_data);
   $config_file_hex = USAT::DeviceAdmin::Util::blob_select_by_name($dbh, "$ev_number-CFG");

   if(defined $config_file_hex)
   {
        $session->kiosk_show_full_config(1);
        $config_file_data = pack("H*", $config_file_hex);
		$argsv->{ev_number_cfg_config_file_data}     = $config_file_data;
        if ($config_file_data =~ /Public_PC_Version/) {
            $device_subtype = KIOSK_PUBLIC_PC;
        }
        if ($device_subtype == KIOSK_PUBLIC_PC) {
            $session->default_cfg_file('PUBLICPC-DEFAULT-CFG');
        } elsif ($DTI eq PKG_DEVICE_TYPE__T2) {
            $session->default_cfg_file('T2-DEFAULT-CFG');
        } elsif ($DTI eq PKG_DEVICE_TYPE__EDGE) {
            $session->default_cfg_file(PKG_DEVICE_CONF__EDGE.$device->property_list_version);
        } else {
            $session->default_cfg_file('KIOSK-DEFAULT-CFG');
        }
        my $button5 = qq|<button style="$style" $onclick|.PROFILE_TAB__FILE_TRANSFER.','.$onclick_arg2.q|)"><b>File Transfer</b></button>|;
        $tabTable->insert(0,$tabTable_cn++,$button5,$style[5]);
   } else {
        # if there is no config file in the database, most likely it's a PictureStation with the old DLL
        $session->kiosk_show_full_config(0);
   }
} elsif ($DTI eq PKG_DEVICE_TYPE__ESUDS) {
    $session->default_cfg_file('ESUDS-DEFAULT-CFG');
    my $button5 = qq|<button style="$style" $onclick|.PROFILE_TAB__FILE_TRANSFER.','.$onclick_arg2.q|)"><b>File Transfer</b></button>|;
    $tabTable->insert(0,$tabTable_cn++,$button5,$style[5]);
} else {
    $session->default_cfg_file('');
}

display $tabTable;

my $table_rn = 0;
my $table = OOCGI::NTable->new('width=100%');

my $titleTable = OOCGI::NTable->new('border=1 width=100% noshade size=2 cellspacing=0');
$titleTable->insert(0, 0, $device->titlerow, 'class=titlerow');
$table->insert($table_rn++, 0, $titleTable);  # title

if($tab eq PROFILE_TAB__DEVICE_PROFILE) { # Device Profile
   my $tableDeviceProfile = USAT::DeviceAdmin::UI::ProfileTables->tableDeviceProfile($argsv);
   my $tableDeviceDates = USAT::DeviceAdmin::UI::ProfileTables->tableDeviceDates($argsv);
   my $tableLocationCustomerTracking = USAT::DeviceAdmin::UI::ProfileTables->tableLocationCustomerTracking($argsv);
   my $tableHostInformation  = USAT::DeviceAdmin::UI::ProfileTables->tableHostInformation($argsv);
   my $tableUSALiveLocation = USAT::DeviceAdmin::UI::ProfileTables->tableUSALiveLocation($argsv);
   $table->insert($table_rn++,0,$tableDeviceProfile);  # profile name
   $table->insert($table_rn++,0,$tableDeviceDates);  # dates
   $table->insert($table_rn++,0,$tableUSALiveLocation);  # USALive Location Info
   $table->insert($table_rn++,0,$tableLocationCustomerTracking);  # location, customer popup 
   $table->insert($table_rn++,0,$tableHostInformation);  # host information
} elsif($tab eq PROFILE_TAB__PAYMENT_CONFIGURATION) { # payment configuration
   my $tablePaymentConfiguration = USAT::DeviceAdmin::UI::ProfileTables->tablePaymentConfiguration($argsv);
   my $tableTransactionHistory   = USAT::DeviceAdmin::UI::ProfileTables->tableTransactionHistory($argsv);
   $table->insert($table_rn++,0,$tablePaymentConfiguration);  # configured peyment types
   $table->insert($table_rn++,0,$tableTransactionHistory); # transaction history
} elsif($tab eq PROFILE_TAB__DEVICE_CONFIGURATION) { # Device Configuration
	my $raw_map_data;
	if ($DTI eq PKG_DEVICE_TYPE__G4 ||
		$DTI eq PKG_DEVICE_TYPE__G5 ||
		$DTI eq PKG_DEVICE_TYPE__MEI_TELEMETER) {
   my $map_name = 'g4_generic_map.csv';
   if ($DTI eq PKG_DEVICE_TYPE__MEI_TELEMETER) {
       $map_name = 'mei_generic_map.csv';
   }
		$raw_map_data = USAT::DeviceAdmin::Util::load_file($map_name, $dbh);
		if(!$raw_map_data)
   {
       print "Failed to load $map_name!";
       USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
       $session->destroy;
       exit;
   }
   $argsv->{map_name}          = $map_name;
	}
   my $tableDeviceConfiguration  = USAT::DeviceAdmin::UI::ProfileTables->tableDeviceConfiguration($argsv);
   my $tablePendingCommands = USAT::DeviceAdmin::UI::ProfileTables->tablePendingCommands($argsv,$raw_map_data);
   my $tableCommandHistory  = USAT::DeviceAdmin::UI::ProfileTables->tableCommandHistory($argsv,$raw_map_data);
   if($DTI ne PKG_DEVICE_TYPE__SONY_PICTURESTATION &&
      $DTI ne PKG_DEVICE_TYPE__KIOSK &&
      $DTI ne PKG_DEVICE_TYPE__EDGE  &&
      $DTI ne PKG_DEVICE_TYPE__T2 ) {
	  my $tableServerClientRequests = USAT::DeviceAdmin::UI::ProfileTables->tableServerClientRequests($argsv);
      $table->insert($table_rn++,0,$tableServerClientRequests);  # transaction history
   }
   $table->insert($table_rn++,0,$tableDeviceConfiguration);  # edit configuration, import template, clone serial number
   $table->insert($table_rn++,0,$tablePendingCommands); # pending commands
   $table->insert($table_rn++,0,$tableCommandHistory); # command history
} elsif($tab eq PROFILE_TAB__DEVICE_SETTINGS) { # Device Setting
   if ($DTI eq PKG_DEVICE_TYPE__G4 ||
       $DTI eq PKG_DEVICE_TYPE__G5 ||
       $DTI eq PKG_DEVICE_TYPE__MEI_TELEMETER ||
	   $DTI eq PKG_DEVICE_TYPE__EDGE) {
       my $tableDeviceSettings = USAT::DeviceAdmin::UI::ProfileTables->tableDeviceSettings($argsv);
       my $tableListAll        = USAT::DeviceAdmin::UI::ProfileTables->tableListAll($argsv);
	   my $tableCounters       = USAT::DeviceAdmin::UI::ProfileTables->tableCounters($argsv);
	   my $tableEventHistory   = USAT::DeviceAdmin::UI::ProfileTables->tableEventHistory($argsv);
       $table->insert($table_rn++,0,$tableDeviceSettings); # device settings
       $table->insert($table_rn++,0,$tableListAll); # list all
	   $table->insert($table_rn++,0,$tableCounters); # counters
	   $table->insert($table_rn++,0,$tableEventHistory); # event history
   }
} elsif($tab eq PROFILE_TAB__FILE_TRANSFER) { # File Transfer
	if ($DTI ne PKG_DEVICE_TYPE__G4 &&
		$DTI ne PKG_DEVICE_TYPE__MEI_TELEMETER) {
		my $tableFileTransfer = USAT::DeviceAdmin::UI::ProfileTables->tableFileTransfer($argsv);
		$table->insert($table_rn++,0,$tableFileTransfer);
	}
	my $tableFileTransferHistory = USAT::DeviceAdmin::UI::ProfileTables->tableFileTransferHistory($argsv);
	$table->insert($table_rn++,0,$tableFileTransferHistory);
}

print qq|<form name="profile" action="" method="post">|;

HIDDEN('device_id', $device_id);
HIDDEN('tab',       $tab);
HIDDEN('ssn',       $device->device_serial_cd);
HIDDEN('ev_number', $device->device_name);

display $table;

print '</form>';
if($tab eq PROFILE_TAB__PAYMENT_CONFIGURATION || $tab eq PROFILE_TAB__DEVICE_SETTINGS) {
print q{
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "11",                  // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    swap_dates
    });
    Calendar.setup({
        inputField     :    "12",                   // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    swap_dates
    });
</script>
};
}

$ia->enable();

$session->destroy;
$dbh->disconnect;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub padE4
{
    my ($num) = @_;
    if(length($num) <= 6)
    {
        $num = substr('0000000', 0, (6-length($num))) . $num;
        $num = 'E4' . $num;
    }
    return $num;
}

sub printError ($) {
	my $err_txt = shift;
	BR(2);
	B($err_txt, "RED", 2);
	BR(3);
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}
