#!/usr/local/USAT/bin/perl

use strict;
use CGI::Carp qw(fatalsToBrowser);
use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::Log;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;

my $userOP      = $query->param('userOP');

my $device_id   = $query->param('device_id');
my $device      = USAT::DeviceAdmin::DBObj::Device->new($device_id);
my $location_id = $device->Location()->location_id;

if($userOP eq 'Change Location') {
   $location_id = $query->param('location_id');

   $device = $device->change_location($location_id);
   $device_id = $device->ID;
   print CGI->redirect("/profile.cgi?device_id=$device_id&tab=1");
   exit();
}

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
USAT::DeviceAdmin::UI::DAHeader->printHeader;
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html"); 
$session->print_menu;
$session->destroy;

print "<form>";

my $popLocation = USAT::DeviceAdmin::UI::USAPopups->pop_location($device_id);
$popLocation->head('1','Undefined');
$popLocation->default($location_id);
BR(4);
print $popLocation->string;
BR(4);
print '<input type="submit" name=userOP value="Change Location">';
BR(4);
HIDDEN('device_id',$device_id);
print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
