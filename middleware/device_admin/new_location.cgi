#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

my $location_name = $PARAM{"location_name"};

if(length($location_name) == 0)
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	$session->print_menu;
	print "Required parameter not found: name";
	$session->destroy;
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit();
}

my $get_location_stmt = $dbh->prepare("select location_id from location.location where location_name = :location_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_location_stmt->bind_param(":location_name", $location_name);
$get_location_stmt->execute();
my @location_data = $get_location_stmt->fetchrow_array();
$get_location_stmt->finish();
if(@location_data)
{
	$dbh->disconnect;
	$session->destroy;
	print CGI->redirect("/edit_location.cgi?location_id=$location_data[0]");
	exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">New Location</th>
 </tr>
 <form method=\"post\" action=\"new_location_func.cgi\">
 <input type=\"hidden\" name=\"location_id\">
 <tr>
  <td nowrap>Name</td>
  <td width=\"50%\">$location_name<input type=\"hidden\" name=\"name\" value=\"$location_name\" size=\"40\"></td>
  <td nowrap>Type</td>
  <td width=\"50%\">
   <select name=\"location_type_id\">
";
    
my $location_types_stmt = $dbh->prepare("select location_type_id, location_type_desc from location_type order by location_type_desc") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$location_types_stmt->execute();
while (my @data = $location_types_stmt->fetchrow_array()) 
{
	print "   <option value=\"$data[0]\">$data[1]</option>\n";
}
$location_types_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>Address Line 1</td>
  <td width=\"50%\"><input type=\"text\" name=\"addr1\" size=\"40\" maxlength=\"60\"></td>
  <td nowrap>City</td>
  <td width=\"50%\"><input type=\"text\" name=\"city\" size=\"40\" maxlength=\"28\"></td>
 </tr>
 <tr>
  <td nowrap>Address Line 2</td>
  <td width=\"50%\"><input type=\"text\" name=\"addr2\" size=\"40\" maxlength=\"60\"></td>
  <td nowrap>State</td>
  <td width=\"50%\">
   <select name=\"state\">
";
    
my $states_stmt = $dbh->prepare("select state_cd, state_name from state order by state_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$states_stmt->execute();
while (my @data = $states_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" >$data[1]</option>\n";
}
$states_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>County</td>
  <td width=\"50%\"><input type=\"text\" name=\"county\" size=\"40\" maxlength=\"28\"></td>
  <td nowrap>Postal Code</td>
  <td width=\"50%\"><input type=\"text\" name=\"postal_cd\" size=\"40\" maxlength=\"10\"></td>
 </tr>
 <tr>
  <td nowrap>Country</td>
  <td width=\"50%\">
   <select name=\"country\">
";
    
my $country_list_stmt = $dbh->prepare("select country_cd, country_name from country order by country_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$country_list_stmt->execute();
while (my @data = $country_list_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" >$data[1]</option>\n";
}
$country_list_stmt->finish();

print "
   </select>
  </td>
  <td nowrap>Time Zone</td>
  <td width=\"50%\"><select name=\"time_zone\">
";
    
my $time_zone_stmt = $dbh->prepare("select time_zone_cd, time_zone_name from time_zone order by time_zone_cd") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$time_zone_stmt->execute();
while (my @data = $time_zone_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\">$data[0] - $data[1]</option>\n";
}
$time_zone_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>Parent Location</td>
  <td width=\"50%\" colspan=\"3\">
   <select name=\"parent_location_id\">
   <option value=\"\">None</option>";
    
my $parents_stmt = $dbh->prepare("select location_id, location_name, location_city, location_state_cd, location_country_cd from location.location where location_active_yn_flag = 'Y' order by location_name") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$parents_stmt->execute();
while (my @data = $parents_stmt->fetchrow_array()) 
{
	print "<option value=\"$data[0]\" >$data[1] - $data[2], $data[3] $data[4] </option>\n";
}
$parents_stmt->finish();

print "
   </select>
  </td>
 </tr>
 <tr>
  <td colspan=\"4\" align=\"center\">
   <input type=\"submit\" name=\"action\" value=\"Save\">
  </td>
 </tr>
 </form>
</table>
";

$session->destroy;

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
