#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print qq|<form method="post" action="new_consumer_acct_2c.cgi">|;

my $err_str;

if(not $params{'device_type_id'} >= 0)
{
	$err_str .= B('Required parameter not found: device_type_id<br>', 'red') unless($params{'device_type_id'});
}

if($err_str)
{
	print $err_str;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $action_sql = q|
	select action.action_id, action.action_name, action.action_desc
	from device.action, device.device_type_action
	where action.action_id = device_type_action.action_id
	and device_type_action.device_type_id = ?|;

my $rc = 0;	
my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
$table->put($rc++,0, B('USAT Card Creation Wizard'),'align=center bgcolor=#C0C0C0 colspan=2');
$table->put($rc++,0, B('Select a Maintenance Card Type'),'align=center colspan=2');

my $action_query = OOCGI::Query->new($action_sql, undef, [$params{'device_type_id'}]);

while(my %hash = $action_query->next_hash) 
{
	my $action_desc = $hash{action_name};
	$action_desc .= qq|<br><font color="gray">&nbsp;$hash{action_desc}</font>| if(defined $hash{action_desc});
	
	$table->put($rc,0, qq|<input type="radio" checked name="action_id" value="$hash{action_id}">|);
	$table->put($rc++,1,$action_desc);
}

display $table;

HIDDEN_PASS('consumer_acct_fmt_id', 'customer_id', 'location_id', 'authority_id', 'merchant_id', 'merchant_name', 'merchant_desc', 'device_type_id');

print "&nbsp<br>" . SUBMIT('action','Next >') . "</form><br>&nbsp;";
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
