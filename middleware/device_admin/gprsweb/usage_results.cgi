#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use OOCGI::PageNavigation;
use OOCGI::PageSort;
use USAT::DeviceAdmin::DBObj;

#use USAT::Database;
#use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my $pageNav  = OOCGI::PageNavigation->new(25,$query);
my $pageSort;

my %PARAM = $query->Vars;

my %usage_code_hash = (  '1'     =>    ['Normal', '#99FF99'],
                         '2'     =>    ['Overused', '#FF9999'] 
                        );

my $input_validated = 1;
my $billing_date = '12';
						
my $action = $PARAM{"action"};
if($action eq 'Show Specific')
{
	my $iccid_first = $PARAM{"iccid_first"};
	my $iccid_last  = $PARAM{"iccid_last"};
	my $iccid_full  = $PARAM{"iccid_full"};
	
	my $input_validated = 1;
	
	my $parse_error;
  
    my $iccid; 
    if(defined $iccid_full && $iccid_full ne '') {
       if(length($iccid_full) != 20) {
		  $parse_error = "<span class=\"error\"><li></li>Invalid or Missing 20 Digits!</span><br>\n";
       } else {
	      $iccid = $iccid_full;
       }
    } else { 
	   if(length($iccid_first) != 16)
	   {
	      	$parse_error = "<span class=\"error\"><li></li>Invalid or Missing First 16 Digits!</span><br>\n";
		   $input_validated = 0;
	   }
	
	   if(length($iccid_last) < 3)
	   {
	   	   $parse_error = "<span class=\"error\"><li></li>Invalid or Missing Last 4 Digits!</span><br>\n";
		   $input_validated = 0;
	   }
	
	   $iccid = "$iccid_first" . "$iccid_last";
    }
# put date invalidation here	
	if($input_validated)
	{
		print CGI->redirect("usage_detail.cgi?iccid=$iccid");
		exit 0;
	}
	
	$query->printHeader();
	$query->printFile("header.html");
	print "$parse_error";

}
elsif($action eq 'Show All')
{
	$query->printHeader();
	$query->printFile("header.html");
	my $start_date = $PARAM{"start_date"};
	my $end_date = $PARAM{"end_date"};

    my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="1" border="1" width="100%" class="list"');
    my $rownum = 0;

    $table->put($rownum++,0,qq{<br><span class="subhead">Daily SIM Usage between $start_date and $end_date </span><br><br>},
        'class=header0 align=center colspan=5');	

    my %table_cols = (
        1 => ['Date',                       'report_date',     -1],
        2 => ['ICCID',                      'iccid' ,           1],
        3 => ['Device Serial<br>Number',    'device_serial_cd', 1],
        4 => ['Monthly Plan<br>Limit (Kb)', 'included_usage',   1],
        5 => ['Total Usage<br>(Kb)',        'total_kb_usage'  , 1]
    );

    my $sql = q{
          SELECT to_char(report_date,'MM/DD/YYYY'),
                 a.iccid,
                 c.device_id,
                 device_serial_cd,
                 included_usage,
                 total_kb_usage
            FROM device.gprs_daily_usage a,
                 device.gprs_device b,
                 device.device c
           WHERE a.iccid=b.iccid
             AND b.device_id = c.device_id(+)
             AND report_date between to_date (?, 'MM/DD/YYYY')
             AND to_date (?, 'MM/DD/YYYY') };

    $pageSort = OOCGI::PageSort->new(
        default_sort_field      => 1 * $table_cols{1}->[2],
        default_sort_field_last => 2 * $table_cols{2}->[2]
    );

    my $colnum = 0;
    foreach my $key (map($_ * $table_cols{$_}->[2], sort {$a <=> $b} keys %table_cols)) {
        (my $text = $table_cols{abs($key)}->[0]) =~ s/\s/&nbsp;/go;
        $table->put($rownum,$colnum++,$pageSort->sort_column($key, $text),'class=header1');
    }
    $rownum++;

    my ($sort_id, $sort_asc, $last_sort_id, $last_sort_asc) = $pageSort->get_sorted_columns();
    $sql .= '    ORDER BY '.$table_cols{$sort_id}->[1].($sort_asc ? '' : ' DESC');
    $sql .= ','.$table_cols{$last_sort_id}->[1].($last_sort_asc ? '' : ' DESC') if $last_sort_id;


    my $qo   = $pageNav->get_result_obj($sql, [ $start_date, $end_date ]);      
    while(my @data = $qo->next_array)
    {
        $table->put($rownum,0,$data[0]);
        $table->put($rownum,1,qq{<a href="usage_detail.cgi?iccid=$data[1]">}.USAT::DeviceAdmin::GPRS::format_iccid($data[1]).'</a>');
        $table->put($rownum,2,qq{<a href="/profile.cgi?device_id=$data[2]" target="_device_admin">$data[3] </a>}.'&nbsp;');
        $table->put($rownum,3,$data[4], 'align=right');
        $table->put($rownum,4,$data[5], 'align=right');
        $rownum++;
    }
    display $table;
    my $page_table = $pageNav->table;
    display $page_table;
     
     $pageNav->enable();
     $pageSort->enable();
}
elsif($action eq 'Show Overage')
{
    $query->printHeader();
    $query->printFile("header.html");
	my $month = $PARAM{"month"};
    my ($mon, $year)=split(/\//, $month);
    my $start_date=sprintf("%02d/%02d/%04d", $mon, $billing_date, $year);
    my $end_date=sprintf("%02d/%02d/%04d", $mon+1, $billing_date-1, $year);

    my %table_cols = (
        1 => ['ICCID',                       'iccid',           -1],
        2 => ['Device Serial Number',        'device_serial_cd', 1],
        3 => ['Monthly Plan<br>Limit (Kb)',  'included_usage',   1],
        4 => ['Total Usage<br>(Kb)',         'total_usage'  ,    1],
        5 => ['Remains<br>(Kb)',             'diff'            , 1],
        6 => ['Average Daily<br>Usage (Kb)', 'avg_daily_usage' , 1]
    );
    my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="1" border="1" width="100%" class="list"');
    my $rownum = 0;

    $table->put($rownum++,0,qq{<br><span class="subhead">SIM cards which exceed the usage limit</span><br>
                <span>&nbsp</span><br>\n
                <span>From $start_date to $end_date</span><br>\n
                <span>&nbsp</span><br>\n
                }, 'class=header0 align=center colspan=6');	

     my $sql = "SELECT a.iccid,
                       b.device_id,
                       device_serial_cd,
                       included_usage,
                       total_usage,
                       diff, (case when diff >= 0 then 1 else 2 end),
                       avg_daily_usage
                  FROM (SELECT trunc(report_date - $billing_date, 'MM') a,
                               iccid,
                               included_usage,
                               sum(total_kb_usage) total_usage,
                               included_usage - sum(total_kb_usage) diff,
                               sum(total_kb_usage)/count(*) avg_daily_usage
                          FROM device.gprs_daily_usage
                         WHERE report_date between to_date(?, 'MM/YYYY') + $billing_date
                           AND last_day(to_date(?, 'MM/YYYY')) + $billing_date
                      GROUP BY trunc(report_date - $billing_date, 'MM'), iccid, included_usage) a,
                         device.gprs_device b,
                         device.device c
                 WHERE a.iccid=b.iccid
                   AND b.device_id=c.device_id(+)
                   AND diff < 0 ";

     $pageSort = OOCGI::PageSort->new(
          default_sort_field      => 1 * $table_cols{1}->[2],
          default_sort_field_last => 2 * $table_cols{2}->[2]
     );

     my $colnum = 0;
     foreach my $key (map($_ * $table_cols{$_}->[2], sort {$a <=> $b} keys %table_cols)) {
         (my $text = $table_cols{abs($key)}->[0]) =~ s/\s/&nbsp;/go;
         $table->put($rownum,$colnum++,$pageSort->sort_column($key, $text),'class=header1');
     }
     $rownum++;

     my ($sort_id, $sort_asc, $last_sort_id, $last_sort_asc) = $pageSort->get_sorted_columns();
     $sql .= '    ORDER BY '.$table_cols{$sort_id}->[1].($sort_asc ? '' : ' DESC');
     $sql .= ','.$table_cols{$last_sort_id}->[1].($last_sort_asc ? '' : ' DESC') if $last_sort_id;


     my $qo   = $pageNav->get_result_obj($sql, [ $month, $month ]);
 
     while(my @data = $qo->next_array)
     {
           $table->put($rownum,0,qq{<a href="usage_detail.cgi?iccid=$data[0]">}.USAT::DeviceAdmin::GPRS::format_iccid($data[0]).'</a>');
           $table->put($rownum,1,qq{<a href="/profile.cgi?device_id=$data[1]" target="_device_admin">$data[2] </a>}.'&nbsp;');
           $table->put($rownum,2, $data[3],'align=right'); 
           $table->put($rownum,3, $data[4],'align=right'); 
           $table->put($rownum,4, $data[5],"bgcolor=$usage_code_hash{$data[6]}[1] align=right"); 
           my $avg=sprintf("%.2f",$data[7]);
           $table->put($rownum,5, $avg,'align=right'); 
           $rownum++;
     }

     display $table;
     my $page_table = $pageNav->table;
     display $page_table;
     
     $pageNav->enable();
     $pageSort->enable();
}
else
{
	$query->printHeader();
	$query->printFile("header.html");
	print "<span class=\"error\"><li></li>Invalid Action! ($action)</span><br>\n";
}

$query->printFile("footer.html");
