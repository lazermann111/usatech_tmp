#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::GprsTables;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

print "<span class=\"subhead\">Allocate SIMs</span><br>\n";
print '<form method="get" action="allocate_search_results.cgi" onSubmit="javascript:return swap_fields();">';

print USAT::DeviceAdmin::UI::GprsTables->SIMSearch;

print '</form>';

$query->printFile("footer.html");
