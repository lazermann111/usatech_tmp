#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;

use USAT::Database;
use USAT::DeviceAdmin::GPRS;

my $query = OOCGI::OOCGI->new;

$query->printHeader();

my ($return_code, $return_message) = &USAT::DeviceAdmin::GPRS::synch_all();

print "<pre>\n";
print "return_code = $return_code\n";
print "return_message = $return_message\n";
print "</pre>\n";


