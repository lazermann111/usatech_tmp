#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;

use USAT::Database;
#use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

print '
<span class="subhead">On-Demand Refresh from Cingular</span><br>
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <form method="GET" action="synchronize_func.cgi">
 <tr>
  <td>
   This page will contact Cingular, get an updated list of SIM cards, and make sure that our database and their 
   database are synchronized.<br>
   <br>
   This update is run automatically once every night, so you should only do this as needed after making many updates and 
   can not wait untill tomorrow for the refresh.<br>
   <br>
  </td>
 </tr>
 <tr>
  <td>Step 1: Click <input type="submit" name="action" value="Refresh Active SIMs List"></td>
 </tr>
 <tr>
  <td>Step 2: Wait 15 minutes.  Cingular only updates the active SIMs list every 15 minutes.</td>
 </tr>
 <tr>
  <td>Step 3: Click <input type="submit" name="action" value="Synchronize With Cingular"><br>
  <br>
  The refresh process can take a couple minutes. You should let it run and not click stop in the middle.</td>
 </tr>
 </form>
</table>
&nbsp;<br>
';

$query->printFile("footer.html");
