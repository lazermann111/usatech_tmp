#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::DBObj;

use USAT::Database;
use USAT::DeviceAdmin::GPRS;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my $iccid_str = $in{"iccid"};
my $allocated_by = $in{"allocated_by"};
my $allocated_to = $in{"allocated_to"};
my $allocated_notes = $in{"allocated_notes"};
my $billable_to = $in{"billable_to"};
my $billable_notes = $in{"billable_notes"};

my $input_validated = 1;

if(length($iccid_str) < 20)
{
	print "<span class=\"error\"><li></li>No ICCIDs Checked!</span><br>\n";
	$input_validated = 0;
}

if(length($allocated_by) <= 0)
{
	print "<span class=\"error\"><li></li>Allocated By is Required!</span><br>\n";
	$input_validated = 0;
}

if(length($allocated_to) <= 0)
{
	print "<span class=\"error\"><li></li>Allocated To is Required!</span><br>\n";
	$input_validated = 0;
}

if(length($billable_to) <= 0)
{
	print "<span class=\"error\"><li></li>Billable To is Required!</span><br>\n";
	$input_validated = 0;
}

if($input_validated)
{
	&execute();
}

$query->printFile("footer.html");

sub execute
{
	my @iccid_array = split /\x00/, $iccid_str;
	$iccid_str = join(",", @iccid_array);

	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	
	my $update_count = 0;
	my $failed_count = 0;
	my $msg;
	
	print "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td><pre>\n";

	foreach my $iccid (@iccid_array)
	{
		chomp($iccid);
		
		if(length($iccid) != 20)
		{
			print "Error: ICCID too short!\n";
			$failed_count++;
			next;
		}
		
		print "Updating <a href=\"iccid_detail.cgi?iccid=$iccid\">" . USAT::DeviceAdmin::GPRS::format_iccid($iccid) . "</a>... \t";
			
		my $sql = "update gprs_device set gprs_device_state_id = ?, allocated_by = ?, allocated_ts = sysdate, allocated_to = ?, allocated_notes = ?, billable_to_name = ?, billable_to_notes = ? where iccid = ?";
		my $args = [2, $allocated_by, $allocated_to, $allocated_notes, $billable_to, $billable_notes, $iccid];
		my ($result, $msg) = USAT::DeviceAdmin::GPRS::update($dbh, $sql, $args);
		if($result)
		{
			print "Success: $msg\n";
			$update_count++;
		}
		else
		{
			print "Failed: $msg\n";
			$failed_count++;
		}
		
		print "\n";
	}

	print "</pre></td></tr></table>\n";

	$dbh->disconnect;
	
	if($failed_count > 0)
	{
		$msg = "Processing failed!  For help please copy this screen and send it to an administrator.";
	}
	else
	{
		$msg = "Allocation processed successfully!";
	}
	
	$msg = "$msg\\n\\n SIMs Allocated: $update_count \\n Failures: $failed_count";
	print "<script language=\"javascript\">window.alert(\"$msg\");</script>\n";
}

