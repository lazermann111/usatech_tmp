#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::GprsPopups;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my $popICCID = USAT::DeviceAdmin::UI::GprsPopups->popICCID;

my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
my $start_date = sprintf("%02d/%02d/%04d", $mon + 1, $mday-1, $year + 1900);
my $end_date = sprintf("%02d/%02d/%04d", $mon + 1, $mday-1, $year + 1900);
my $month = sprintf("%02d/%04d", $mon, $year + 1900);

print '
<span class="subhead">Show Specific Sim Card Usage</span><br>
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <form method="get" action="usage_results.cgi">
 <tr>
  <td>ICCID: </td>
  <td>
   <table border="1" cellspacing="0" cellpadding="2">
    <tr>
     <td>First 16 digits</td>
     <td>Last 4 digits</td>
     <td>Full ICCID</td>
    </tr>
    <tr>
     <td>
      ' . $popICCID . '
     </td>
     <td>
      <input type="text" name="iccid_last" size="7" maxlength="4">
     </td>
     <td>
      <input type="text" name="iccid_full" size="25" maxlength="20">
     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type="submit" name="action" value="Show Specific"></td>
 </tr>
 </form>
</table>
&nbsp;<br>
';

print "
<span class=\"subhead\">Show All Sim Card Usage</span><br>
<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\" width=\"50%\">
 <form method=\"get\" action=\"usage_results.cgi\">
  <td>Start Date:</td>
  <td><input type=\"text\" name=\"start_date\" size=\"20\" value=\"$start_date\" maxlength=\"20\">
 </tr>
 <tr>
  <td>End Date:</td>
  <td><input type=\"text\" name=\"end_date\" size=\"20\" value=\"$end_date\" maxlength=\"20\">
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type=\"submit\" name=\"action\" value=\"Show All\"></td>
 </tr>
 </form>
</table>
&nbsp;<br>
";

print "
<span class=\"subhead\">Show Sim Cards Exceeding the Monthly Usage Limit</span><br>
<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\" width=\"50%\">
 <form method=\"get\" action=\"usage_results.cgi\">
  <td>Month:</td>
  <td><input type=\"text\" name=\"month\" size=\"20\" value=\"$month\" maxlength=\"20\">
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type=\"submit\" name=\"action\" value=\"Show Overage\"></td>
 </tr>
 </form>
</table>
&nbsp;<br>
";

$query->printFile("footer.html");
