#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;

use USAT::Database;
use USAT::DeviceAdmin::GPRS;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::Util;

my %status_codes_hash = (	'1'	=>	['Registered', '#9999FF'],
				'2'	=>	['Allocated', '#99CCCC'],
				'3'	=>	['Activation Pending', '#FFFF99'],
				'4'	=>	['Activated', '#99FF99'],
				'5'	=>	['Assigned', '#FF9999']
			);

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my $gprs_device_hist_id = $in{"gprs_device_hist_id"};
my $input_validated = 1;

if(length($gprs_device_hist_id) == 0)
{
	print "<span class=\"error\"><li></li>Invalid or Missing gprs_device_hist_id!</span><br>\n";
	$input_validated = 0;
}

if($input_validated)
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	&execute($dbh);
	$dbh->disconnect;
}

$query->printFile("footer.html");

sub execute
{
	my ($dbh) = @_;
	my $search_stmt = $dbh->prepare("select gprs_device_id, gprs_device_state_id, ordered_by, to_char(ordered_ts, 'MM/DD/YYYY'), provider_order_id, ordered_notes, iccid, imsi, allocated_by, to_char(allocated_ts, 'MM/DD/YYYY'), allocated_to, allocated_notes, billable_to_name, billable_to_notes, activated_by, to_char(activated_ts, 'MM/DD/YYYY'), activated_notes, provider_activation_id, to_char(provider_activation_ts, 'MM/DD/YYYY'), msisdn, phone_number, rate_plan_name, assigned_by, to_char(assigned_ts, 'MM/DD/YYYY'), assigned_notes, device_id, imei, device_type_name, device_firmware_name, rssi, created_by, to_char(created_ts, 'MM/DD/YYYY'), last_updated_by, to_char(last_updated_ts, 'MM/DD/YYYY') from gprs_device_hist where gprs_device_hist_id = :gprs_device_hist_id") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->bind_param(":gprs_device_hist_id", $gprs_device_hist_id);
	$search_stmt->execute() or die "Couldn't execute statement: " . $search_stmt->errstr;
	my @data = $search_stmt->fetchrow_array();
	$search_stmt->finish();
	if(not defined $data[0])
	{
		print "<span class=\"error\">gprs_device_hist_id $gprs_device_hist_id not found!</span>\n";
		return;
	}

	print "<span class=\"subhead\">SIM Card Information: $data[6]</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>GPRS Device ID</td><td>$data[0]</td><td>ICCID</td><td><a href=\"https://eod.wireless.att.com/USA/history.asp?open=11&cat=Service&page=Sim%20History%20/%20Status&iccid=$data[6]\" target=\"_cingular\">" . USAT::DeviceAdmin::GPRS::format_iccid($data[6]) . "</a></td></tr>\n";
	print "<tr><td>Status</td><td bgcolor=\"$status_codes_hash{$data[1]}[1]\">$status_codes_hash{$data[1]}[0]</td><td>IMSI</td><td>$data[7]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
     # puk information
	print "<br><span class=\"subhead\">PIN/PUK Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Pin1</td><td>Puk1</td><td>Pin2</td><td>Puk2</td></tr>\n";
	print "<tr><td>$data[34]&nbsp;</td><td>$data[35]&nbsp;</td><td>$data[36]&nbsp;</td><td>$data[37]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
     # order information 
	print "<br><span class=\"subhead\">Order Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Ordered By</td><td>Date Ordered</td><td>Cingular Order ID</td></tr>\n";
	print "<tr><td>$data[2]&nbsp;</td><td>$data[3]&nbsp;</td><td><a href=\"https://eod.wireless.att.com/USA/view_sim_order.asp?order_id=$data[4]\" target=\"_cingular\">$data[4]</a>&nbsp;</td></tr>\n";
	print "<tr><td colspan=\"3\">Order Notes: $data[5]</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
	print "<br><span class=\"subhead\">Allocation Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Allocated By</td><td>Date Allocated</td><td>Allocated To</td></tr>\n";
	print "<tr><td>$data[8]&nbsp;</td><td>$data[9]&nbsp;</td><td>$data[10]&nbsp;</td</tr>\n";
	print "<tr><td colspan=\"3\">Allocation Notes: $data[11]&nbsp;</td></tr>\n";
	print "<tr><td>Billable to</td><td colspan=\"2\">$data[12]&nbsp;</td></tr>\n";
	print "<tr><td colspan=\"3\">Billable Notes: $data[13]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
	print "<br><span class=\"subhead\">Activation Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Activated By</td><td>Date Activated</td></tr>\n";
	print "<tr><td>$data[14]&nbsp;</td><td>$data[15]&nbsp;</td></tr>\n";
	print "<tr><td colspan=\"2\">Activation Notes: $data[16]&nbsp;</td></tr>\n";
	print "<tr><td>Cingular Activation ID</td><td><a href=\"https://eod.wireless.att.com/USA/batch_results.asp?batch=$data[17]\" target=\"_cingular\">$data[17]</a>&nbsp;</td></tr>\n";
	print "<tr><td>Cingular Activation Date</td><td>$data[18]&nbsp;</td></tr>\n";
	print "<tr><td>MSISDN</td><td>$data[19]&nbsp;</td></tr>\n";
	print "<tr><td>Phone Number</td><td>$data[20]&nbsp;</td></tr>\n";
	print "<tr><td>Rate Plan</td><td><a href=\"https://eod.wireless.att.com/USA/view_rate_plans.asp?open=11&cat=Service&page=View%20Rate%20Plans\" target=\"_cingular\">$data[21]</a>&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
	print "<br><span class=\"subhead\">Assignment Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>Assigned By</td><td>Date Assigned</td><td>Assigned To Device</td></tr>\n";
	print "<tr><td>$data[22]&nbsp;</td><td>$data[23]&nbsp;</td><td><a href=\"/profile.cgi?device_id=$data[25]\" target=\"_device_admin\">$data[25]</a>&nbsp;</td></tr>\n";
	print "<tr><td colspan=\"3\">Assignment Notes: $data[24]&nbsp;</td></tr>\n";
	print "</table>\n";
	print "&nbsp;\n";
	print "<br><span class=\"subhead\">Modem Information</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>IMEI</td><td>$data[26]&nbsp;</td></tr>\n";
	print "<tr><td>Modem Type</td><td>$data[27]&nbsp;</td></tr>\n";
	print "<tr><td>Modem Firmware</td><td>$data[28]&nbsp;</td></tr>\n";
        print "<tr><td>Last RSSI</td><td>$data[29]&nbsp;</td></tr>\n";

        my $file_data = USAT::DeviceAdmin::Util::blob_select($dbh, $data[38]);
	if(defined $file_data && length($file_data) > 0)
	{
        	$file_data = pack("H*", $file_data);
       		print "<tr><td colspan=\"2\">Raw Modem Data:<br><pre>$file_data</pre></td></tr>\n";
	}
	print "</table>\n";
	print "&nbsp;\n";	
}

