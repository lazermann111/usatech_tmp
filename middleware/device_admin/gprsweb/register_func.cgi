#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my $provider_order_id = $in{"provider_order_id"};
my $ordered_by = $in{"ordered_by"};
my $ordered_ts = $in{"ordered_ts"};
my $ordered_notes = $in{"ordered_notes"};
my $input_validated = 1;

if(length($provider_order_id) < 4)
{
	print "<span class=\"error\"><li></li>Invalid or Missing OrderID!</span><br>\n";
	$input_validated = 0;
}

if(length($ordered_by) <= 0)
{
	print "<span class=\"error\"><li></li>Ordered By is Required!</span><br>\n";
	$input_validated = 0;
}

if(length($ordered_ts) != 10)
{
	print "<span class=\"error\"><li></li>Ordered Date must match MM/DD/YYYY</span><br>\n";
	$input_validated = 0;
}

if($input_validated)
{
	&execute();
}

$query->printFile("footer.html");

sub execute
{
	my $page_data = &USAT::DeviceAdmin::GPRS::Cingular::get_page_content("/USA/view_sim_order_list.asp?id=$provider_order_id");
	if(not defined $page_data)
	{
		print "<tspan class=\"error\">Failed to retrieve page data for OrderID: $provider_order_id</span>\n";
		return;
	}
	
	my $start = index($page_data, 'ICCID,IMSI,PIN1,PUK1,PIN2,PUK2<br>');
	my $end = index($page_data, '</font>', ($start+34));
	my $list_data = substr($page_data, ($start+34), ($end-($start+34))-2);
	
	if($start <= 0)
	{
		print "<span class=\"error\">Invalid page content:<br><hr>$page_data</span>\n";
		return;
	}

	my @lines = split(/<br>/, $list_data);
	
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	
	my $insert_count = 0;
	my $exists_count = 0;
	my $failed_count = 0;
	my $msg;
	
	print "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td><pre>\n";

	foreach my $line (@lines)
	{
		chomp($line);
		
		print "Processing : $line...\t";
		
		if(length($line) < 36)
		{
			print "Error: Line too short!\n";
			$failed_count++;
			next;
		}
		
		my @fields= split(/,/, $line);
		my $iccid = $fields[0];
		my $imsi = $fields[1];
		my $pin1 = $fields[2];
		my $puk1 = $fields[3];
		my $pin2 = $fields[4];
		my $puk2 = $fields[5];
		
		if(length($iccid) != 20)
		{
			print "ICCID too short!\n";
			$failed_count++;
			next;
		}
		if(length($imsi) != 15)
		{
			print "IMSI too short!\n";
			$failed_count++;
			next;
		}
	
        ## need validate the pin puk number?
		print "Passed input validation tests!\n";
		
		my $exists_stmt = $dbh->prepare("select gprs_device_id, gprs_device_state_id, ordered_by, to_char(ordered_ts, 'MM/DD/YYYY'), provider_order_id, ordered_notes, iccid, imsi from gprs_device where iccid = :iccid") or die "Couldn't prepare statement: " . $dbh->errstr;
		$exists_stmt->bind_param(":iccid", "$iccid");
		$exists_stmt->execute() or die "Couldn't execute statement: " . $exists_stmt->errstr;
		my @data = $exists_stmt->fetchrow_array();
		
		if(defined $data[0])
		{
			print "$iccid exists: $data[0], $data[1], $data[2], $data[3], $data[4], $data[5]\n";
			$exists_count++;
		}
		else
		{
			print "ICCID $iccid does not exist: inserting... ";
			
			my $sql = "insert into gprs_device(gprs_device_state_id, ordered_by, ordered_ts, provider_order_id, ordered_notes, iccid, imsi, pin1, puk1, pin2, puk2) values (?, ?, to_date(?, 'MM/DD/YYYY'), ?, ?, ?, ?, ?, ?, ?, ?)";
			my $args = [1, $ordered_by, $ordered_ts, $provider_order_id, $ordered_notes, $iccid, $imsi, $pin1, $puk1, $pin2, $puk2];
			my ($result, $msg) = &insert($dbh, $sql, $args);
			if($result)
			{
				print "Success: $msg\n";
				$insert_count++;
			}
			else
			{
				print "Failed: $msg\n";
				$failed_count++;
			}
		}
		
		$exists_stmt->finish();
		
		print "\n";
	}

	print "</pre></td></tr></table>\n";

	$dbh->disconnect;
	
	if($failed_count > 0)
	{
		$msg = "Processing failed!  For help please copy this screen and send it to an administrator.";
	}
	else
	{
		$msg = "Order processed successfully!";
	}
	
	$msg = "$msg\\n\\n SIMs Inserted: $insert_count \\n SIMs Previously Registered: $exists_count \\n Failures: $failed_count";
	print "<script language=\"javascript\">window.alert(\"$msg\");</script>\n";
}

sub insert 
{
	my ($dbh, $sql, $args) = @_;
	my $insert_stmt = $dbh->prepare_cached($sql) or return (0, "Couldn't prepare statement: " . $dbh->errstr);
	my $result = $insert_stmt->execute(@$args) or return (0, "Couldn't execute statement: " . $insert_stmt->errstr);
	$insert_stmt->finish();
	return ($result, "Insert Succeeded!");
}
