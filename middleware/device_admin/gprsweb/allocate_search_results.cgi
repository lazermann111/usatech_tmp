#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my %status_codes_hash = (	'1'	=>	['Registered', '#9999FF'],
				'2'	=>	['Allocated', '#99CCCC'],
				'3'	=>	['Activation Pending', '#FFFF99'],
				'4'	=>	['Activated', '#99FF99'],
				'5'	=>	['Assigned', '#FF9999']
			);

my $iccid_first = $in{"iccid_first"};
my $iccid_last = $in{"iccid_last"};
my $count = $in{"count"};

my $frICCID = $in{frICCID};
my $toICCID = $in{toICCID};

if( $frICCID > 0 && $toICCID ) {
    $iccid_first = substr($frICCID, 0, 16);
    my $fr_num   = substr($frICCID, 0, 19);
    my $to_num   = substr($toICCID, 0, 19);
    $count = $to_num -  $fr_num + 1;
    $iccid_last  = substr($frICCID,16, 4);
}
   
print qq|$iccid_first a $iccid_last a $count |;
 
my $input_validated = 1;

if(length($iccid_first) != 16)
{
	print "<span class=\"error\"><li></li>Invalid or Missing First 16 Digits!</span><br>\n";
	$input_validated = 0;
}

if(length($iccid_last) < 3)
{
	print "<span class=\"error\"><li></li>Invalid or Missing Last 4 Digits!</span><br>\n";
	$input_validated = 0;
}

if(length($count) <= 0)
{
	print "<span class=\"error\"><li></li>SIM Count is Required!</span><br>\n";
	$input_validated = 0;
}

$iccid_last = substr($iccid_last,0,3) if(length($iccid_last) == 4);

my $iccid = "$iccid_first" . "$iccid_last" . "0";

if($input_validated)
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	&execute($dbh);
	$dbh->disconnect;
}

$query->printFile("footer.html");

sub execute
{
	print "<span class=\"subhead\">Allocate Search Results</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<form method=\"post\" action=\"allocate_func.cgi\">\n";	
	print "<tr><th>Include?</th><th>ICCID</th><th>Status</th><th>Ordered<br>Date</th><th>Ordered<br>By</th><th>Order<br>ID</th></tr>\n";
	print "<form method=\"post\" action=\"allocate_func.cgi\">\n";	
	my ($dbh) = @_;
	my $search_stmt = $dbh->prepare("select gprs_device_id, iccid, imsi, gprs_device_state_id, ordered_by, to_char(ordered_ts, 'MM/DD/YYYY'), provider_order_id from (select gprs_device_id, iccid, imsi, gprs_device_state_id, ordered_by, ordered_ts, provider_order_id from gprs_device where iccid >= :iccid order by ICCID ASC) where rownum <= :count") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->bind_param(":iccid", $iccid);
	$search_stmt->bind_param(":count", $count);
	$search_stmt->execute() or die "Couldn't execute statement: " . $search_stmt->errstr;
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}

		print "<tr><td align=\"center\">" . ($data[3] == 1 ? "<input type=\"checkbox\" name=\"iccid\" value=\"$data[1]\" checked>" : "&nbsp;") . "</td>\n";
		print "<td nowrap><a href=\"iccid_detail.cgi?iccid=$data[1]\">" . USAT::DeviceAdmin::GPRS::format_iccid($data[1]) . "</a></td>\n";
		print "<td bgcolor=\"$status_codes_hash{$data[3]}[1]\">$status_codes_hash{$data[3]}[0]</td>\n";
		print "<td>$data[5]&nbsp;</td>\n";
		print "<td>$data[4]&nbsp;</td>\n";
		print "<td><a href=\"https://eod.wireless.att.com/USA/view_sim_order.asp?order_id=$data[6]\">$data[6]</a>&nbsp;</td></tr>\n";
	}
	
	print "</table>\n";
	print "&nbsp;<br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"3\" width=\"790\">\n";
	print "<tr><td>Allocated By: </td><td><input type=\"text\" name=\"allocated_by\" size=\"40\"></td></tr>\n";
	print "<tr><td>Allocated To: </td><td><input type=\"text\" name=\"allocated_to\" size=\"40\"></td></tr>\n";
	print "<tr><td>Allocated Notes: </td><td><textarea cols=\"60\" rows=\"5\" name=\"allocated_notes\"></textarea></td></tr>\n";
	print "<tr><td>Billable To: </td><td><select name=\"billable_to\"><option value=\"USAT\">USAT</option><option value=\"MEI\">MEI</option></select></td></tr>\n";
	print "<tr><td>Billable Notes: </td><td><textarea cols=\"60\" rows=\"5\" name=\"billable_notes\"></textarea></td></tr>\n";
	print "<tr><td>&nbsp;</td><td><input type=\"submit\" value=\"Allocate\"></td></tr>\n";
	print "</form>\n";
	print "</table>\n";
	
	$search_stmt->finish();
}



