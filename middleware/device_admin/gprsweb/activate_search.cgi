#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::GprsTables;
use USAT::DeviceAdmin::DBObj::Da_session;

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

print q{ <script type="text/javascript" src="/js/calendar/swap_dates.js"> </script> };

my $popBillableToName = USAT::DeviceAdmin::UI::GprsPopups->popBillableToName;
$popBillableToName->head('','Any');
$popBillableToName->default('');

my $popAllocatedToName = USAT::DeviceAdmin::UI::GprsPopups->popAllocatedToName;
$popAllocatedToName->head('','Any');
$popAllocatedToName->default('');

print "<span class=\"subhead\">Activate Search</span><br>\n";
print '<form method="get" action="activate_search_results.cgi" onSubmit="javascript:return swap_fields();">';

print USAT::DeviceAdmin::UI::GprsTables->SIMSearch;

print '
</form> 
<form method="get" action="activate_search_results.cgi">
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <tr>
  <td>Billable To</td>
  <td>
   ' . $popBillableToName . '
  </td>
 </tr>
 <tr>
  <td>Allocated To</td>
  <td>
   ' . $popAllocatedToName . '
  </td>
 </tr>
 <tr>
  <td>SIM Count: </td>
  <td><input type="text" name="count" size="4"></td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type="submit" name="action" value="Search SIMs"></td>
 </tr>
</table>
</form>
&nbsp;<br>
';

print '
<form method="post" action="activate_search_results.cgi">
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <tr>
  <td>
   ICCID List (1 per line)
  </td>
  <td>
   <textarea name="iccid_list" rows="10" cols="25"></textarea>
  </td>
 </tr>
  <td>&nbsp;</td>
  <td><input type="submit" name="action" value="List All SIMs"></td>
 </tr>
</table>
</form>
&nbsp;<br>
';

$query->printFile("footer.html");
