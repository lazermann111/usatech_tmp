#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();

$query->printFile("header.html");

print "<span class=\"subhead\">Register New SIM Order</span><br>\n";
print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
print "<form method=\"post\" action=\"register_func.cgi\">";
print "<tr><td>Cingular Order ID: </td><td><input type=\"text\" name=\"provider_order_id\" size=\"8\"></td></tr>\n";
print "<tr><td>Ordered Date: </td><td><input type=\"text\" name=\"ordered_ts\" size=\"11\" value=\"MM/DD/YYYY\" maxlength=\"10\"></td></tr>\n";
print "<tr><td>Ordered By: </td><td><input type=\"text\" name=\"ordered_by\" size=\"40\"></td></tr>\n";
print "<tr><td>Order Notes: </td><td><textarea cols=\"60\" rows=\"5\" name=\"ordered_notes\"></textarea></td></tr>\n";
print "<tr><td>&nbsp;</td><td><input type=\"submit\" value=\"Register New SIMs\"></td></tr>\n";
print "</form>\n";
print "</table>\n";

$query->printFile("footer.html");
