#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;
use Net::SMTP;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

print "<pre>\n";
	
my $action = $in{"action"};
if($action eq 'Refresh Active SIMs List')
{
	my ($code,$msg) = USAT::DeviceAdmin::GPRS::Cingular::refresh_active_sims_file();
	if($code)
	{
		print "Success: $msg\n\nPlease return to this site in 15 minutes or more and click Synchronize With Cingular from the refresh page.";
	}
	else
	{
		print "Refresh request failed!  Please copy this page and send it to an website administrator.\n\n$msg";
	}
}
elsif($action eq 'Synchronize With Cingular')
{

	my $email = $in{"email"};
	
	&USAT::DeviceAdmin::GPRS::synch_all();
	
	if(length($email) > 0)
	{
		my $email_sent = &send_email('rerixengine@usatech.com', $email, 'GPRS Refresh Complete (' . localtime() . ')', 'The Cingular GPRS website refresh you requested is complete!');
		if($email_sent)
		{
			print "Email sent successfully to $email\n";
		}
		else
		{
			print "Email send failed!\n";
		}
	}
}
else
{
	print "<span class=\"error\"><li></li>Unknown action! ($action)</span><br>\n";
}

print "</pre>\n";
$query->printFile("footer.html");

sub send_email
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = 'mailhost';
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}
