#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::GprsTables;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %PARAM = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my $table = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');

print '<form method="post">';

my $rownum = 0;

$table->put($rownum, 0, 'Status', 'class=header1');
$table->put($rownum, 1, 'Blue',   'class=header1');
$table->put($rownum, 2, 'Orange', 'class=header1');
$table->put($rownum, 3, 'Total', 'class=header1');

$rownum++;

my $blue   = '89310380';
my $orange = '89014103';
my $sql;
my $sql3;
my $count1;
my $count2;
my $count3;
my $sum1 = 0;
my $sum2 = 0;
my $sum3 = 0;
my $fmt_blue   = 'align=right bgcolor=lightblue';
my $fmt_orange = 'align=right bgcolor=orange';
$table->put($rownum,0, 'Registred', 'bgcolor="#9999FF"');
$sql = 'SELECT count(*) FROM gprs_device WHERE gprs_device_state_id = ? AND substr(iccid,0,8) = ?';
#$sql3= 'SELECT count(*) FROM gprs_device WHERE gprs_device_state_id = ?';

$count1 = OOCGI::Query->new($sql , { bind => [ 1, $blue   ] })->node(0,0);
$sum1 += $count1;
$table->put($rownum,1,  $count1, $fmt_blue);
$count2 = OOCGI::Query->new($sql , { bind => [ 1, $orange ] })->node(0,0);
$sum2 += $count2;
$table->put($rownum,2, $count2, $fmt_orange);
#########$count3 = OOCGI::Query->new($sql3, { bind => 1 })->node(0,0);
$table->put($rownum,3,  $count1+$count2,'align=right');
$rownum++;

$count1 = OOCGI::Query->new($sql , { bind => [ 2, $blue   ] })->node(0,0);
$sum1 += $count1;
$table->put($rownum,0, 'Allocated','bgcolor="#99CCCC"');
$table->put($rownum,1, $count1, $fmt_blue);
$count2 = OOCGI::Query->new($sql , { bind => [ 2, $orange ] })->node(0,0);
$sum2 += $count2;
$table->put($rownum,2, $count2, $fmt_orange);
#########$count3 = OOCGI::Query->new($sql3, { bind => 2 })->node(0,0);
$table->put($rownum,3,  $count1+$count2,'align=right');
$rownum++;

$table->put($rownum,0, 'Activated','bgcolor="#99FF99"');
$count1 = OOCGI::Query->new($sql , { bind => [ 4, $blue   ] })->node(0,0);
$sum1 += $count1;
$table->put($rownum,1, $count1, $fmt_blue);
$count2 = OOCGI::Query->new($sql , { bind => [ 4, $orange ] })->node(0,0);
$sum2 += $count2;
$table->put($rownum,2, $count2, $fmt_orange);
#########$count3 = OOCGI::Query->new($sql3, { bind => 4 })->node(0,0);
$table->put($rownum,3,  $count1+$count2,'align=right');
$rownum++;

$table->put($rownum,0, 'Assigned','bgcolor="#FF9999"');
$count1 = OOCGI::Query->new($sql , { bind => [ 5, $blue   ] })->node(0,0);
$sum1 += $count1;
$table->put($rownum,1, $count1, $fmt_blue);
$count2 = OOCGI::Query->new($sql , { bind => [ 5, $orange ] })->node(0,0);
$sum2 += $count2;
$table->put($rownum,2, $count2, $fmt_orange);
#########$count3 = OOCGI::Query->new($sql3, { bind => 5 })->node(0,0);
$table->put($rownum,3,  $count1+$count2,'align=right');
$rownum++;

$sum3 = $sum1 + $sum2;

$table->put($rownum,0, 'Total');
$table->put($rownum,1, $sum1, $fmt_blue);
$table->put($rownum,2, $sum2, $fmt_orange);
$table->put($rownum,3, $sum3,'align=right');


display $table;

print '</form>';

$query->printFile("footer.html");
