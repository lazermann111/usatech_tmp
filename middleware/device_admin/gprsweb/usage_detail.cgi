#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;
use USAT::DeviceAdmin::DBObj;

my $billing_date='12';
my %usage_code_hash = (  '1'     =>    ['Normal', '#99FF99'],
                         '2'     =>    ['Overused', '#FF9999'] 
                        );

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my $iccid = $in{"iccid"};
my $input_validated = 1;

if(length($iccid) != 20)
{
	print "<span class=\"error\"><li></li>Invalid or Missing ICCID!</span><br>\n";
	$input_validated = 0;
}

if($input_validated)
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	&execute($dbh);
	$dbh->disconnect;
}

$query->printFile("footer.html");

sub execute
{
# put device serial id link

        print "<span><a href=\"iccid_detail.cgi?iccid=$iccid\">". "Iccid:". USAT::DeviceAdmin::GPRS::format_iccid($iccid) . "</a></span><br>\n";
         my ($dbh) = @_;
         my $sql = $dbh->prepare("select a.device_id, device_serial_cd from device.gprs_device a, device.device b where a.device_id = b.device_id(+) and iccid = :iccid") or die "Couldn't prepare statement: " . $dbh->errstr;
         $sql->bind_param(":iccid", $iccid);
         $sql->execute() or die "Couldn't execute statement: " . $sql->errstr;
         my $serial_num="Device Not Assigned";
         my $device_id="0";
         while(1)
         {
         my @data = $sql->fetchrow_array();
         if(not defined $data[0])
                {
                        last;
                }
          $device_id=$data[0];
          $serial_num=$data[1];
          }
         
          $sql->finish(); 
          print "<span><a href=\"/profile.cgi?device_id=$device_id\" target=\"_device_admin\">Device Serial Number: $serial_num</a>&nbsp;<span><br>\n";


# generate monthly summary report;
        print "<span>&nbsp</span><br>\n";
        print "<span class=\"subhead\">Recent Monthly Usage Summary</span><br>\n";
        #print "<span>&nbsp</span><br>\n";
        print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
        print "<tr><th>Date</th><th>Monthly Plan <br> Limit (Kb)</th><th>Total Usage <br> (Kb)</th><th>Remains <br> (Kb)</th><th>Average Daily <br> Usage (Kb)</th></tr>\n";
        $sql = $dbh->prepare("select 'From '||to_char(trunc(a, 'MM') + $billing_date -1 , 'MM/DD/YYYY')||' to '||to_char(trunc(a+31, 'MM') + $billing_date - 2, 'MM/DD/YYYY') report_date, included_usage, total_usage, diff, (case when diff >= 0 then 1 else 2 end), avg_daily_usage from (select trunc(report_date - $billing_date + 1, 'MM') a, included_usage, sum(total_kb_usage) total_usage, included_usage - sum(total_kb_usage) diff, sum(total_kb_usage)/count(*) avg_daily_usage from device.gprs_daily_usage where iccid = :iccid and report_date >= trunc(trunc(sysdate, 'MM') - 180, 'MM') + $billing_date group by trunc(report_date - $billing_date + 1, 'MM'), included_usage) order by report_date ") or die "Couldn't prepare statement: " . $dbh->errstr;
        
        $sql->bind_param(":iccid", $iccid);
        $sql->execute() or die "Couldn't execute statement: " . $sql->errstr;
        while(1)
        {
        my @data = $sql->fetchrow_array();
        if(not defined $data[0])
                {
                        last;
                }
        print "</td>\n";
        print "<td nowrap>$data[0]&nbsp;</td>\n";
        print "<td nowrap>$data[1]&nbsp;</td>\n";
        print "<td nowrap>$data[2]&nbsp;</td>\n";
        print "<td nowrap bgcolor=\"$usage_code_hash{$data[4]}[1]\">$data[3]</td>\n";;
        my $avg=sprintf("%.2f",$data[5]);
        print "<td nowrap>$avg&nbsp;</td>\n";
        print "</tr>\n";
        }
        print "</table>\n";
        $sql->finish(); 

## generate daily report for this month;

        print "<span>&nbsp</span><br>\n";
        print "<span class=\"subhead\">Daily Usage For This Month</span><br>\n";
        #print "<span>&nbsp</span><br>\n";
        print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
        print "<tr><th>Date</th><th>Monthly Plan Limit (Kb)</th><th>Total Usage (Kb)</th><th>Remains (Kb)</th></tr>\n";

$sql = $dbh->prepare(" select to_char(report_date,'MM/DD/YYYY'), included_usage, total_kb_usage  from device.gprs_daily_usage where iccid = :iccid and report_date >= trunc(sysdate - $billing_date, 'MM') + $billing_date -1 order by report_date ") or die "Couldn't prepare statement: " . $dbh->errstr;

        $sql->bind_param(":iccid", $iccid);
        $sql->execute() or die "Couldn't execute statement: " . $sql->errstr;
        my $remains; 
        while(1)
        {
        my @data = $sql->fetchrow_array();
        if(not defined $data[0])
                {
                        last;
                }
        $remains=$data[1] if (!defined $remains); 
        $remains=$remains - $data[2];

        my $color_code=$usage_code_hash{1}[1];
        $color_code=$usage_code_hash{2}[1] if ($remains < 0);

        print "<tr>\n";
        print "<td nowrap>$data[0]&nbsp;</td>\n";
        print "<td nowrap>$data[1]&nbsp;</td>\n";
        print "<td nowrap>$data[2]&nbsp;</td>\n";;
        print "<td nowrap bgcolor=\"$color_code\">$remains</td>\n";;
        #print "<td nowrap bgcolor=\"if ($remains >=0) $usage_code_hash{1}[1]; else $usage_code_hash{1}[1];\">$remains</td>\n";;
        print "</tr>\n";
        }
        print "</table>\n";
        $sql->finish();

## generate daily report for last month;

        print "<span>&nbsp</span><br>\n";
        print "<span class=\"subhead\">Daily Usage For Last Month</span><br>\n";
        #print "<span>&nbsp</span><br>\n";
        print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
        print "<tr><th>Date</th><th>Monthly Plan Limit (Kb)</th><th>Total Usage (Kb)</th><th>Remains (Kb)</th></tr>\n";

$sql = $dbh->prepare(" select to_char(report_date,'MM/DD/YYYY'), included_usage, total_kb_usage  from device.gprs_daily_usage where iccid = :iccid and report_date > trunc(trunc(sysdate - $billing_date, 'MM') - 1, 'MM') + $billing_date -1 and report_date < trunc(sysdate - $billing_date, 'MM') + $billing_date
-1  order by report_date ") or die "Couldn't prepare statement: " . $dbh->errstr;

        $sql->bind_param(":iccid", $iccid);
        $sql->execute() or die "Couldn't execute statement: " . $sql->errstr;
        while(1)
        {
        my @data = $sql->fetchrow_array();
        if(not defined $data[0])
                {
                        last;
                }
        $remains=$data[1] if (!defined $remains);
        $remains=$remains - $data[2];
        my $color_code=$usage_code_hash{1}[1];
        $color_code=$usage_code_hash{2}[1] if ($remains < 0);
         
        print "<tr>\n";
        print "<td nowrap>$data[0]&nbsp;</td>\n";
        print "<td nowrap>$data[1]&nbsp;</td>\n";
        print "<td nowrap>$data[2]&nbsp;</td>\n";;
        print "<td nowrap bgcolor=\"$color_code\">$remains</td>\n";;
        print "</tr>\n";
        }
        print "</table>\n";
        $sql->finish();

}
