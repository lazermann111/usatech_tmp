#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

print "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
print "<tr><td><pre>\n";

my $iccid = $in{"iccid"};
if(length($iccid) < 20)
{
	print "<span class=\"error\"><li></li>Missing ICCID!</span><br>\n";
	$query->printFile("footer.html");
	exit(1);
}

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $action = $in{"action"};
if($action eq 'Dealocate')
{
	my $sql = "update gprs_device set gprs_device_state_id = ?, allocated_by = ?, allocated_to = ?, allocated_ts = ?, allocated_notes = ?, billable_to_name = ?, billable_to_notes = ? where iccid = ?";
	my $args = [1, undef, undef, undef, undef, undef, undef, $iccid];
	my $update_stmt = $dbh->prepare_cached($sql) or print "Couldn't prepare statement: " . $dbh->errstr;
	my $result = $update_stmt->execute(@$args) or print " Couldn't execute statement: " . $update_stmt->errstr;

	if($result)
	{
		print "Success ($result): <a href=\"iccid_detail.cgi?iccid=$iccid\">" . USAT::DeviceAdmin::GPRS::format_iccid($iccid) . "</a> Dealocated!\n";
	}
	else
	{
		print "Failed ($result): <a href=\"iccid_detail.cgi?iccid=$iccid\">" . USAT::DeviceAdmin::GPRS::format_iccid($iccid) . "</a> NOT Dealocated Successfully\n";
	}
}
elsif($action eq 'Refresh from Cingular')
{
	print "Please use the Refresh button on the left menu.<br>\n";
}
elsif($action eq 'Change Rate')
{
	my $rate_plan = $in{"rate_plan"};
	my $effective_ts = $in{"effective_ts"};
	my $activated_by = $in{"activated_by"};
	my $activated_notes = $in{"activated_notes"};
	if(length($rate_plan) != 4)
	{
		print "<span class=\"error\"><li></li>Invalid Rate Plan!</span><br>\n";
	}
	elsif(length($effective_ts) != 10)
	{
		print "<span class=\"error\"><li></li>Effective Date must match MM/DD/YYYY</span><br>\n";
	}
	elsif(length($activated_by) <= 0)
	{
		print "<span class=\"error\"><li></li>Ordered Date must match MM/DD/YYYY</span><br>\n";
	}
	else
	{
		&change_rate($iccid, $rate_plan, $effective_ts, $activated_by, $activated_notes);
	}
}
else
{
	print "<span class=\"error\"><li></li>Unknown action! ($action)</span><br>\n";
}

print "</pre></td></tr></table>\n";


$query->printFile("footer.html");

$dbh->disconnect;

sub change_rate
{
	my ($iccid, $rate_plan, $effective_ts, $activated_by, $activated_notes) = @_;
	
	my $failed_count = 0;
	my $activate_count = 0;
	my $msg;
	
	print "Updating <a href=\"iccid_detail.cgi?iccid=$iccid\">" . USAT::DeviceAdmin::GPRS::format_iccid($iccid) . "</a>... \t";
	
	my $sql = "update gprs_device set gprs_device_state_id = ?, activated_by = ?, activated_ts = sysdate, activated_notes = ?, provider_activation_ts = to_date(?, 'MM/DD/YYYY'), rate_plan_name = ? where iccid = ?";
	my $args = [3, $activated_by, $activated_notes, $effective_ts, $rate_plan, $iccid];
	my $update_stmt = $dbh->prepare_cached($sql) or return (0, "Couldn't prepare statement: " . $dbh->errstr);
	my $result = $update_stmt->execute(@$args) or return (0, "Couldn't execute statement: " . $update_stmt->errstr);

	if($result)
	{
		print "Success: $result\n";
		print "Changing Rate: $iccid,$rate_plan,$effective_ts ...\t";

		my ($activate_result, $activate_msg) = &USAT::DeviceAdmin::GPRS::Cingular::change_rate($iccid, $rate_plan, $effective_ts);
		if($activate_result)
		{
			print "Success: $activate_msg\n";
			$activate_count++;
			$dbh->commit();
		}
		else
		{
			print "Failed: $activate_msg... Rolling back update!\n";
			$dbh->rollback();
			$failed_count++;
		}
	}
	else
	{
		$dbh->rollback();
		print "Failed: $msg\n";
		$failed_count++;
	}

	$update_stmt->finish();
		
	if($failed_count > 0)
	{
		$msg = "Processing failed!  For help please copy this screen and send it to an administrator.";
	}
	else
	{
		$msg = "Activations processed successfully!";
	}
	
	$msg = "$msg\\n\\n SIMs Updated: $activate_count \\n Update Failures: $failed_count";
	print "<script language=\"javascript\">window.alert(\"$msg\");</script>\n";
}
