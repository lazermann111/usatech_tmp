#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use OOCGI::Query;
use OOCGI::PageNavigation;
use OOCGI::PageSort;
use USAT::DeviceAdmin::DBObj;

use USAT::Database;
use USAT::DeviceAdmin::GPRS;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my $pageNav  = OOCGI::PageNavigation->new(25,$query);
my $pageSort;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my %status_codes_hash = (	'1'	=>	['Registered', '#9999FF'],
							'2'	=>	['Allocated', '#99CCCC'],
							'3'	=>	['Activation Pending', '#FFFF99'],
							'4'	=>	['Activated', '#99FF99'],
							'5'	=>	['Assigned', '#FF9999']
						);
my $input_validated = 1;
						
my $action = $in{"action"};
if($action eq 'List SIMs')
{
	my $iccid_first = $in{"iccid_first"};
	my $iccid_last = $in{"iccid_last"};
	my $count = $in{"count"};

    my $frICCID = $in{frICCID};
    my $toICCID = $in{toICCID};

    if( $frICCID > 0 && $toICCID ) {
        $iccid_first = substr($frICCID, 0, 16);
        my $fr_num   = substr($frICCID, 0, 19);
        my $to_num   = substr($toICCID, 0, 19);
        $count = $to_num -  $fr_num + 1;
        $iccid_last  = substr($frICCID,16, 4);
    }
	
	my $input_validated = 1;
	
	if(length($iccid_first) != 16)
	{
		print "<span class=\"error\"><li></li>Invalid or Missing First 16 Digits!</span><br>\n";
		$input_validated = 0;
	}
	
	if(length($iccid_last) < 3)
	{
		print "<span class=\"error\"><li></li>Invalid or Missing Last 4 Digits!</span><br>\n";
		$input_validated = 0;
	}
	
	if(length($count) <= 0)
	{
		print "<span class=\"error\"><li></li>SIM Count is Required!</span><br>\n";
		$input_validated = 0;
	}
	
	$iccid_last = substr($iccid_last,0,3) if(length($iccid_last) == 4);
	
	my $iccid = "$iccid_first" . "$iccid_last" . "0";
	
	if($input_validated)
	{
		my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
		my $dbh = $DATABASE->{handle};
		&execute_list($dbh, $iccid, $count);
		$dbh->disconnect;
	}
}
elsif($action eq 'Search SIMs')
{
	my $status = $in{"status"};
	my $billable_to_name = $in{"billable_to_name"};
	my $allocated_to_name = $in{"allocated_to_name"};
	my $rate_plan = $in{"rate_plan"};
	
	$status = undef unless length($status) > 0;
	$billable_to_name = undef unless length($billable_to_name) > 0;
	$allocated_to_name = undef unless length($allocated_to_name) > 0;
	$rate_plan = undef unless length($rate_plan) > 0;
	
	if($input_validated)
	{
		my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
		my $dbh = $DATABASE->{handle};
		&execute_search($dbh, $status, $billable_to_name, $allocated_to_name, $rate_plan);
		$dbh->disconnect;
	}
}
else
{
	print "<span class=\"error\"><li></li>Invalid Action! ($action)</span><br>\n";
}
	
$pageNav->enable();
#$pageSort->enable();

$query->printFile("footer.html");

sub execute_list
{
	my ($dbh, $iccid, $count) = @_;
	print "<span class=\"subhead\">--SIM Search Results</span><br>\n";
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><th>ICCID</th><th>Status</th><th>Allocated<br>To</th><th>Billable<br>To</th><th>Rate<br>Plan</th><th>Assigned<br>Device</th></tr>\n";
	my $search_stmt = $dbh->prepare("
       SELECT gprs_device_id,
              iccid,
              imsi,
              gprs_device_state_id,
              ordered_by,
              allocated_to,
              billable_to_name,
              device_id,
              rate_plan_name,
              to_char(ordered_ts, 'MM/DD/YY'),
              to_char(allocated_ts, 'MM/DD/YY'),
              to_char(activated_ts, 'MM/DD/YY'),
              to_char(provider_activation_ts, 'MM/DD/YY'),
              to_char(assigned_ts, 'MM/DD/YY'),
              device_serial_cd
         FROM (SELECT gprs_device.gprs_device_id,
                      gprs_device.iccid,
                      gprs_device.imsi,
                      gprs_device.gprs_device_state_id,
                      gprs_device.ordered_by,
                      gprs_device.allocated_to,
                      gprs_device.billable_to_name,
                      gprs_device.device_id,
                      gprs_device.rate_plan_name,
                      gprs_device.ordered_ts,
                      gprs_device.allocated_ts,
                      gprs_device.activated_ts,
                      gprs_device.provider_activation_ts,
                      gprs_device.assigned_ts,
                      device.device_serial_cd
                 FROM gprs_device,
                      device
                WHERE gprs_device.device_id = device.device_id(+)
                  AND iccid >= :iccid
             ORDER BY ICCID ASC)
         WHERE rownum <= :count") or die "Couldn't prepare statement: " . $dbh->errstr;
	$search_stmt->bind_param(":iccid", $iccid);
	$search_stmt->bind_param(":count", $count);
	$search_stmt->execute() or die "Couldn't execute statement: " . $search_stmt->errstr;
	my $count = 0;
	while(1)
	{
		my @data = $search_stmt->fetchrow_array();
		if(not defined $data[0])
		{
			last;
		}

		print "<tr>\n";
		print "<td nowrap><a href=\"iccid_detail.cgi?iccid=$data[1]\">" . USAT::DeviceAdmin::GPRS::format_iccid($data[1]) . "</a></td>\n";
		print "<td nowrap bgcolor=\"$status_codes_hash{$data[3]}[1]\">$status_codes_hash{$data[3]}[0]";
		print " $data[9]"  if($data[3] == 1);
		print " $data[10]" if($data[3] == 2);
		print " $data[11]" if($data[3] == 3);
		print " $data[12]" if($data[3] == 4);
		print " $data[13]" if($data[3] == 5);
		
		print "</td>\n";
		print "<td nowrap>$data[5]&nbsp;</td>\n";
		print "<td nowrap>$data[6]&nbsp;</td>\n";
		print "<td nowrap>$data[8]&nbsp;</td>\n";
		print "<td nowrap><a href=\"/profile.cgi?device_id=$data[7]\" target=\"_device_admin\">$data[14]</a>&nbsp;</td>\n";
		print "</tr>\n";

		$count++;
	}
	
	print "</table><br>\n";
	print "<span class=\"subhead\">$count rows</span>\n";
	
	$search_stmt->finish();
}

sub execute_search
{
	my ($dbh, $status, $billable_to_name, $allocated_to_name, $rate_plan) = @_;

    my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="1" border="1" width="100%" class="list"');
    my $table_rn = 0;
    $table->put($table_rn,0,'SIM Search Results','colspan=6 class=header0');
    $table_rn++;

	my $sql = "
       SELECT gprs_device.gprs_device_id,
              gprs_device.iccid,
              gprs_device.imsi,
              gprs_device.gprs_device_state_id,
              gprs_device.ordered_by,
              gprs_device.allocated_to,
              gprs_device.billable_to_name,
              gprs_device.device_id,
              gprs_device.rate_plan_name,
              to_char(gprs_device.ordered_ts, 'MM/DD/YY'),
              to_char(gprs_device.allocated_ts, 'MM/DD/YY'),
              to_char(gprs_device.activated_ts, 'MM/DD/YY'),
              to_char(gprs_device.provider_activation_ts, 'MM/DD/YY'),
              to_char(gprs_device.assigned_ts, 'MM/DD/YY'),
              device.device_serial_cd
         FROM gprs_device,
              device
        WHERE gprs_device.device_id = device.device_id(+) ";
	my @params;
	
	if(defined $status)
	{
        $sql .= 'AND gprs_device_state_id = ? ';
		push(@params, $status);
	}
	
	if(defined $billable_to_name)
	{
        $sql .= 'AND billable_to_name = ? ';
		push(@params, $billable_to_name);
	}

	if(defined $allocated_to_name)
	{
        $sql .= 'AND allocated_to = ? ';
		push(@params, $allocated_to_name);
	}
	
	if(defined $rate_plan)
	{
        $sql .= 'AND rate_plan_name = ? ';
		push(@params, $rate_plan);
	}

#$sql .= ' order by ICCID ASC';

    my %table_cols = (
        1 => ['ICCID',              'iccid',           -1],
        2 => ['Status',             'gprs_device_state_id' , 1],
        3 => ['Allocated<br>To',    'allocated_to'    , 1],
        4 => ['Billable<br>To',     'billable_to_name', 1],
        5 => ['Rate<br>Plan',       'rate_plan_name'  , 1],
        6 => ['Assigned<br>Device', 'device_serial_cd', 1]
    );

    $pageSort = OOCGI::PageSort->new(
        default_sort_field      => 1 * $table_cols{1}->[2],
        default_sort_field_last => 2 * $table_cols{2}->[2]
    );

    my ($sort_id, $sort_asc, $last_sort_id, $last_sort_asc) = $pageSort->get_sorted_columns();
    $sql .= '    ORDER BY '.$table_cols{$sort_id}->[1].($sort_asc ? '' : ' DESC');
    $sql .= ','.$table_cols{$last_sort_id}->[1].($last_sort_asc ? '' : ' DESC') if $last_sort_id;

    my $table_cn = 0;
    foreach my $key (map($_ * $table_cols{$_}->[2], sort {$a <=> $b} keys %table_cols)) {
        (my $text = $table_cols{abs($key)}->[0]) =~ s/\s/&nbsp;/go;
        $table->put($table_rn,$table_cn++,$pageSort->sort_column($key, $text),'class=header1');
    }
    $table_rn++;

#   my $table_cn = 0;
#   foreach my $key (sort {$a <=> $b} keys %table_cols) {
#      (my $text = $table_cols{abs($key)}->[0]) =~ s/\s/&nbsp;/go;
#      if (defined $table_cols{abs($key)}->[1]) {
#          $table->put($table_rn,$table_cn++,$pageSort->sort_column($key, $text),'class=header1');
#       #  $table->put(0,$table_cn++,$pageSort->sort_column($key * $table_cols{$key}->[2], $text),'class=headstyle1');
#      } else {
#          $table->put($table_rn,$table_cn++,$text,'class=header1');
#      }
#   }
#   $table_rn++;

#print "PPP $sql <br>";
    my $qo   = $pageNav->get_result_obj($sql, \@params);	


    while(my @data = $qo->next_array) {
        $table->put($table_rn, 0,qq{<a href="iccid_detail.cgi?iccid=$data[1]">}.USAT::DeviceAdmin::GPRS::format_iccid($data[1])."</a>\n",'nowrap');
        my $str = '';
		if($data[3] == 1) { $str .= " $data[9]";  }
		if($data[3] == 2) { $str .= " $data[10]"; }
		if($data[3] == 3) { $str .= " $data[11]"; }
		if($data[3] == 4) { $str .= " $data[12]"; }
		if($data[3] == 5) { $str .= " $data[13]"; }
        $table->put($table_rn, 1, $status_codes_hash{$data[3]}[0].$str,qq{bgcolor="$status_codes_hash{$data[3]}[1]"});
        $table->put($table_rn, 2, $data[5]);
        $table->put($table_rn, 3, $data[6]);
        $table->put($table_rn, 4, $data[8]);
        $table->put($table_rn, 5, qq{<a href="/profile.cgi?device_id=$data[7]" target="_device_admin">$data[14]</a>&nbsp;});
        $table_rn++;
	}

    display $table;
    my $page_table = $pageNav->table;
    display $page_table;
}

