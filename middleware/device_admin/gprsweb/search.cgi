#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::GprsTables;
use USAT::DeviceAdmin::UI::GprsPopups;
use USAT::DeviceAdmin::DBObj;

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;

my %in = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

my $popStatus   = USAT::DeviceAdmin::UI::GprsPopups->popStatus;
$popStatus->head('','Any');
$popStatus->default('');

my $popRatePlan = USAT::DeviceAdmin::UI::GprsPopups->popRatePlan();
$popRatePlan->head('','Any');
$popRatePlan->default('');

my $popBillableToName = USAT::DeviceAdmin::UI::GprsPopups->popBillableToName;
$popBillableToName->head('','Any');
$popBillableToName->default('');

my $popAllocatedToName = USAT::DeviceAdmin::UI::GprsPopups->popAllocatedToName;
$popAllocatedToName->head('','Any');
$popAllocatedToName->default('');

print '
<span class="subhead">SIM Search</span><br>
<form method="get" action="search_results.cgi" onSubmit="javascript:return swap_fields();">';

print USAT::DeviceAdmin::UI::GprsTables->SIMSearch;

print '</form>';

print '
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <form method="get" action="search_results.cgi">
 <tr>
  <td>Status</td>
  <td>'.$popStatus.'
  </td>
 </tr>
 <tr>
  <td>Billable To</td>
  <td>
   ' . $popBillableToName . '
  </td>
 </tr>
 <tr>
  <td>Allocated To</td>
  <td>
   ' . $popAllocatedToName . '
  </td>
 </tr>
 <tr>
  <td>Rate Plan</td>
  <td>
';

print $popRatePlan;

print '</td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td><input type="submit" name="action" value="Search SIMs"></td>
 </tr>
 </form>
</table>
&nbsp;<br>
';

print '
<span class="subhead">Go To SIM</span><br>
<table border="1" cellspacing="0" cellpadding="2" width="100%">
 <form method="get" action="iccid_detail.cgi">
 <tr>
  <td>Full ICCID: </td>
  <td>
   <input type="text" name="iccid" size="25" maxlength="20">
   <input type="submit" name="action" value="Go To">
  </td>
 </tr>
 </form>
</table>
';

$query->printFile("footer.html");
