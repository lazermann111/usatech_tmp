#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::GprsPopups;
use USAT::DeviceAdmin::DBObj::Da_session;
use USAT::DeviceAdmin::Util;

use USAT::Database;
use USAT::DeviceAdmin::GPRS::Cingular;
use USAT::DeviceAdmin::GPRS;
use USAT::DeviceAdmin::GPRS::DBObj;

my %status_codes_hash = (	
                '1'	=>	['Registered', '#9999FF'],
				'2'	=>	['Allocated', '#99CCCC'],
				'3'	=>	['Activation Pending', '#FFFF99'],
				'4'	=>	['Activated', '#99FF99'],
				'5'	=>	['Assigned', '#FF9999']
			);

my $billing_date='12';
my %usage_code_hash = (  '1'     =>    ['Normal', '#99FF99'],
                         '2'     =>    ['Overused', '#FF9999'] 
                        );

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
$session->destroy;


my %PARAM = $query->Vars;

$query->printHeader();
$query->printFile("header.html");

print <<"SCRIPT";
<script>
function setInput(val) {
   if(val == 'allocated_by_id') {
      document.getElementById(val).innerHTML = '<input type="text" name="allocated_by" size="40">';
   }
   if(val == 'allocated_to_id') {
      document.getElementById(val).innerHTML = '<input type="text" name="allocated_to" size="40">';
   }
   if(val == 'activated_by_id') {
      document.getElementById(val).innerHTML = '<input type="text" name="activated_by" size="40">';
   }
}
</script>
SCRIPT

my $popRatePlan = USAT::DeviceAdmin::UI::GprsPopups->popRatePlan();

my $iccid = $PARAM{"iccid"};
my $input_validated = 1;

if(length($iccid) != 20)
{
	print "<span class=\"error\"><li></li>Invalid or Missing ICCID!</span><br>\n";
	$input_validated = 0;
}

if($input_validated)
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 0, AutoCommit => 1);
	my $dbh = $DATABASE->{handle};
	&execute($dbh);
	$dbh->disconnect;
}

$query->printFile("footer.html");

sub execute
{
	my ($dbh) = @_;
    my $device = USAT::DeviceAdmin::GPRS::DBObj::Gprs_device->new({iccid => $iccid});
	if(not defined $device)
	{
		print qq{<span class="error">ICCID $iccid not found!</span>\n};
		return;
	}

	print qq{<span class="subhead">SIM Card Information:},$device->iccid, qq{</span><br>\n}; # 6
    my $table1 = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
    $table1->put(0, 0, 'GPRS Device ID');
    $table1->put(0, 1, $device->ID); # 0
    $table1->put(0, 2, 'ICCID');
    $table1->put(0, 3, q{<a href="https://eod.wireless.att.com/USA/history.asp?open=11&cat=Service&page=Sim%20History%20/%20Status&iccid=}.$device->iccid.q{" target="_cingular">} . USAT::DeviceAdmin::GPRS::format_iccid($device->iccid) . '</a>' ); # 6

    $table1->put(1, 0, 'Status');
    $table1->put(1, 1, $status_codes_hash{$device->gprs_device_state_id}[0],'bgcolor='.$status_codes_hash{$device->gprs_device_state_id}[1]);
    $table1->put(1, 2, 'IMSI');
    $table1->put(1, 3, $device->imsi);
    display $table1;	
	print "&nbsp;\n";
     # puk information
	print qq{<br><span class="subhead">PIN/PUK Information</span><br>\n};
    my $table2 = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
    $table2->put(0,0,'Pin1');
    $table2->put(0,1,'Puk1');
    $table2->put(0,2,'Pin2');
    $table2->put(0,3,'Puk2');
    $table2->put(1,0,$device->pin1);
    $table2->put(1,1,$device->puk1);
    $table2->put(1,2,$device->pin2);
    $table2->put(1,3,$device->puk2);
    display $table2;	
	print "&nbsp;\n";
     # order information 
	print qq{<br><span class="subhead">Order Information</span><br>\n};
    my $table3 = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
    $table3->put(0,0,'Ordered By');
    $table3->put(0,1,'Date Ordered');
    $table3->put(0,2,'Cingular Order ID');
    $table3->put(1,0,$device->ordered_by);
    $table3->put(1,1,$device->ordered_ts);
    $table3->put(1,2,q{<a href="https://eod.wireless.att.com/USA/view_sim_order.asp?order_id=}.$device->provider_order_id.q{" target="_cingular">}.$device->provider_order_id.'</a>');
    $table3->put(2,0,'Order Notes: '.$device->ordered_notes,'colspan=3');
	display $table3;
	print "&nbsp;\n";
	print qq{<br><span class="subhead">Allocation Information</span><br>\n};
    my $table4 = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
    $table4->put(0,0,'Allocated By');
    $table4->put(0,1,'Date Allocated');
    $table4->put(0,2,'Allocated To');
    $table4->put(1,0,$device->allocated_by);
    $table4->put(1,1,$device->allocated_ts);
    $table4->put(1,2,$device->allocated_to);
    $table4->put(2,0,'Allocation Notes: '.$device->allocated_notes,'colspan=3');
    $table4->put(3,0,'Billable To');
    $table4->put(3,1,$device->billable_to_name,'colspan=2');
    $table4->put(4,0,'Billable Notes: '.$device->billable_to_notes,'colspan=3');
	display $table4;
	print "&nbsp;\n";
	print qq{<br><span class="subhead">Activation Information</span><br>\n};
    my $table5 = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
    $table5->put(0,0,'Activated By');
    $table5->put(0,1,'Date Activated');
    $table5->put(1,0,$device->activated_by);
    $table5->put(1,1,$device->activated_ts);
    $table5->put(2,0,'Activation Notes: '.$device->activated_notes,'colspan=2');
    $table5->put(3,0,'Cingular Activation ID');
    $table5->put(3,1,'<a href="https://eod.wireless.att.com/USA/batch_results.asp?batch='.$device->provider_activation_id.'" target="_cingular">'.$device->provider_activation_id.'</a>');
    $table5->put(4,0,'Cingular Activation Date');
    $table5->put(4,1,$device->provider_activation_ts);
    $table5->put(5,0,'MSISDN');
    $table5->put(5,1,$device->msisdn);
    $table5->put(6,0,'Phone Number');
    $table5->put(6,1,$device->phone_number);
    $table5->put(7,0,'Rate Plan');
    $table5->put(7,1,'<a href="https://eod.wireless.att.com/USA/view_rate_plans.asp?open=11&cat=Service&page=View%20Rate%20Plans" target="_cingular">'.$device->rate_plan_name.'</a>');
    display $table5;	
	print "&nbsp;\n";
	print qq{<br><span class="subhead">Assignment Information</span><br>\n};
    my $table6 = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
    $table6->put(0,0,'Assigned By');
    $table6->put(0,1,'Date Assigned');
    $table6->put(0,2,'Assigned To Device');
    $table6->put(1,0,$device->assigned_by);
    $table6->put(1,1,$device->assigned_ts);
    $table6->put(1,2,'<a href="/profile.cgi?device_id='.$device->device_id.'&tab=1" target="_device_admin">'.$device->device_id.'</a>');
    $table6->put(2,0,'Assignment Notes: '.$device->assigned_notes,'colspan=3');
	display $table6;
	print "&nbsp;\n";
	print qq{<br><span class="subhead">Modem Information</span><br>\n};
    my $table7 = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
    $table7->put(0,0,'IMEI');
    $table7->put(0,1,$device->imei);
    $table7->put(1,0,'Modem Type');
    $table7->put(1,1,$device->device_type_name);
    $table7->put(2,0,'Modem Firmware');
    $table7->put(2,1,$device->device_firmware_name);
    my $rssi = $device->rssi;
    my $rssi_ts = $device->rssi_ts;
    $table7->put(3,0,'Last RSSI');
    $table7->put(3,1,"$rssi&nbsp; " . (defined $rssi_ts ? "($rssi_ts)" : ""));
    my $modem_info = $device->modem_info;
	if(defined $modem_info)
	{
            $table7->put(4,0,"Raw Modem Data:<br><pre>$modem_info</pre>",'colspan=2');
	}

    display $table7
    print "&nbsp;\n";
	
	print qq{<br><span class="subhead">Major SIM Change History</span><br>\n};
	print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
	print "<tr><td>&nbsp;</td><td nowrap>Status at Time of Change</td><td nowrap>Change Timestamp</td></tr>\n";

	my $history_stmt = $dbh->prepare("select gprs_device_hist_id, gprs_device_state_id, to_char(created_ts, 'MM/DD/YY HH24:MI:SS') from gprs_device_hist where iccid = :iccid order by created_ts desc") or die "Couldn't prepare statement: " . $dbh->errstr;
	$history_stmt->bind_param(":iccid", $iccid);
	$history_stmt->execute() or die "Couldn't execute statement: " . $history_stmt->errstr;
	while(my @hist_row = $history_stmt->fetchrow_array())
	{
		print "
		<tr>
		 <td>&nbsp;<a href=\"iccid_hist_detail.cgi?gprs_device_hist_id=$hist_row[0]\" target=\"_hist\">View</a>&nbsp;</td>
		 <td nowrap bgcolor=\"$status_codes_hash{$hist_row[1]}[1]\">$status_codes_hash{$hist_row[1]}[0]</td>
		 <td width=\"100%\">$hist_row[2]</td>
		</tr>\n";
	}
	$history_stmt->finish();
	print "</table>\n";
	print "&nbsp;\n";
	
        ## generate daily report for this month;

        print "<span>&nbsp</span><br>\n";
        print "<span class=\"subhead\">Daily Usage For This Month</span><br>\n";
        #print "<span>&nbsp</span><br>\n";
        print "<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\" width=\"100%\" class=\"list\">\n";
        print "<tr><th>Date</th><th>Monthly Plan Limit (Kb)</th><th>Total Usage (Kb)</th><th>Remains (Kb)</th></tr>\n";

		my $sql = "
           SELECT to_char(report_date,'MM/DD/YYYY'),
                  included_usage,
                  total_kb_usage
             FROM device.gprs_daily_usage
            WHERE iccid = ?
              AND report_date >= trunc(sysdate - $billing_date, 'MM') + $billing_date -1
         ORDER BY report_date ";

        my $qo = OOCGI::Query->new($sql, { bind => $iccid } );
        
        my $remains; 
        while(my @data = $qo->next_array)
        {
            $remains=$data[1] if (!defined $remains); 
            $remains=$remains - $data[2];
            my $color_code=$usage_code_hash{1}[1];
            $color_code=$usage_code_hash{2}[1] if ($remains < 0);

            print "<tr>\n";
            print "<td nowrap>$data[0]&nbsp;</td>\n";
            print "<td nowrap>$data[1]&nbsp;</td>\n";
            print "<td nowrap>$data[2]&nbsp;</td>\n";
            print "<td nowrap bgcolor=\"$color_code\">$remains</td>\n";;
            print "</tr>\n";
        }
        print "</table>\n";
      
        print "<span>&nbsp</span><br>\n";
        print "<b><a href=\"usage_detail.cgi?iccid=$iccid\">  See more usage report about this iccid.  </a></b>\n";
        print "<span>&nbsp</span><br>\n";

        ## change rate plan
	my ($sec, $min, $hour, $mday, $mon, $year) = localtime();
	my $time_str = sprintf("%02d/%02d/%04d", $mon + 1, $mday, $year + 1900);

	if($device->gprs_device_state_id == 1)
	{
        my $popAllocatedByName = USAT::DeviceAdmin::UI::GprsPopups->popAllocatedByName;
        my $popAllocatedToName = USAT::DeviceAdmin::UI::GprsPopups->popAllocatedToName({name => 'allocated_to'});
        my $popBillableToName  = USAT::DeviceAdmin::UI::GprsPopups->popBillableToName;
		print "&nbsp;<br>\n";
		print "<span class=\"subhead\">Allocate</span><br>\n";
		print "<form method=\"post\" action=\"allocate_func.cgi\">\n";
        my $table = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="3" width="790"');
        $table->put(0,0,'Allocated By: ');
        $table->put(0,1,'<div id="allocated_by_id">'.$popAllocatedByName.'</div>');
        $table->put(0,2,q{<input type="button" value="New" onclick="setInput('allocated_by_id');">});
        $table->put(1,0,'Allocated To: ');
        $table->put(1,1,'<div id="allocated_to_id">'.$popAllocatedToName.'</div>');
        $table->put(1,2,q{<input type="button" value="New" onclick="setInput('allocated_to_id');">});
        $table->put(2,0,'Allocated Notes: ');
        $table->put(2,1,'<textarea cols="60" rows="5" name="allocated_notes"></textarea>','colspan=2');
        $table->put(3,0,'Billable To: ');
        $table->put(3,1,$popBillableToName);
        $table->put(4,0,'Billable Notes: ');
        $table->put(4,1,'<textarea cols="60" rows="5" name="billable_notes"></textarea>','colspan=2');
        $table->put(5,0,'<input type="submit" value="Allocate">','colspan=3 align=center');
        display $table;
		print "<input type=\"hidden\" name=\"iccid\" value=\"$iccid\">\n";
		print "</form>\n";
	}
	elsif($device->gprs_device_state_id == 2)
	{
		print "&nbsp;\n";
		print " <form method=\"post\" action=\"iccid_detail_func.cgi\">\n";
		print q{ <input type="hidden" name="iccid" value="},$device->iccid,qq{">\n};
		my $table = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
		$table->put(0,0,'<input type="submit" name="action" value="Dealocate">','align=center');
		display $table;

		print "</form>\n";	
		print "&nbsp;<br>\n";
		print "<span class=\"subhead\">Activate</span><br>\n";
		print "<form method=\"post\" action=\"activate_func.cgi\">\n";	
		print q{ <input type="hidden" name="iccid" value="}.$device->iccid.','.$device->allocated_to.','.$device->billable_to_name.','.$device->rate_plan_name.qq{">\n};
        my $popActivatedByName = USAT::DeviceAdmin::UI::GprsPopups->popActivatedByName;
        $table = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="3" width="790"');
		$table->put(0,0,'Rate Plan: ');
		$table->put(0,1,$popRatePlan);
		$table->put(1,0,'Effective Date: ');
		$table->put(1,1,qq{<input type="text" name="effective_ts" size="11" value="$time_str" maxlength="10">});
		$table->put(2,0,'Activated By: ');
		$table->put(2,1,qq{<div id="activated_by_id">}.$popActivatedByName.q{</div>});
		$table->put(2,2,q{<input type="button" value="New" onclick="setInput('activated_by_id');">});
		$table->put(3,0,'Activated Notes: ');
		$table->put(3,1,'<textarea cols="60" rows="5" name="activated_notes"></textarea>','colspan=2');
		$table->put(4,0,'<input type="submit" value="Activate">','colspan=3 align=center');
		display $table;
		print "</form>\n";
	}
	elsif($device->gprs_device_state_id == 3)
	{
		
	}
	elsif($device->gprs_device_state_id >= 4)
	{
		print "<br><span class=\"subhead\">Change Rate Plan</span><br>\n";
		print "<form method=\"post\" action=\"iccid_detail_func.cgi\">\n";
		print q{ <input type="hidden" name="iccid" value="}.$device->iccid.qq{">\n};
        $popRatePlan->tail('KILL','KILL: Select to Deactivate SIM');
        my $popActivatedByName = USAT::DeviceAdmin::UI::GprsPopups->popActivatedByName;
		my $table = OOCGI::NTable->new('border="1" cellspacing="0" cellpadding="1" width="100%" class="list"');
		$table->put(0,0,'New Rate Plan: ');
		$table->put(0,1, $popRatePlan);
		$table->put(1,0,'Effective Date: ');
		$table->put(1,1,qq{<input type="text" name="effective_ts" size="11" value="$time_str" maxlength="10">});
		$table->put(2,0,' Activated By: ');
		$table->put(2,1,qq{<div id="activated_by_id">}.$popActivatedByName.q{</div>});
		$table->put(2,2,q{<input type="button" value="New" onclick="setInput('activated_by_id');">});
		$table->put(3,0,'Rate Change Notes: ');
		$table->put(3,1,'<textarea cols="60" rows="5" name="activated_notes"></textarea>');
		$table->put(4,0,'<input type="submit" name="action" value="Change Rate">','align=center colspan=3');
		display $table;
		print "</form>\n";	
	}
	
	print q{<br><span class="subhead">}.$device->iccid."</span><br>\n";
}
