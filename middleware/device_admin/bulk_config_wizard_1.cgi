#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::USAPopups;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Common::Const;

require Text::CSV;

my $query = OOCGI::OOCGI->new;

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print q{
<script>
function clearSelect() {
   var obj1 = document.getElementById('start_serial_number_id');
   var obj2 = document.getElementById('num_devices_seq_id');
   obj1.value = '';
   obj2.value = '';
}
function clearList() {
   var obj1 = document.getElementById('serial_number_list_id');
   obj1.value = '';
}
</script>
};

my $popCustomer = USAT::DeviceAdmin::UI::USAPopups->popCustomer;
$popCustomer->head('','Any Customer');
$popCustomer->default('');
my $popLocation = USAT::DeviceAdmin::UI::USAPopups->pop_location;
$popLocation->head('','Any Location');
$popLocation->default('');

my $device_type_sql = 'SELECT device_type_id, device_type_desc
              FROM device_type
          ORDER BY upper(device_type_desc)';

my $popDeviceType = OOCGI::Popup->new(
                    name     => 'device_type', 
                    onchange => "get_serial_format_prefix()",
                    id       => 'device_type_id',
                    sql      => $device_type_sql,
					style 	 => "font-family: courier; font-size: 12px;");
$popDeviceType->head('','Any Device Type');
$popDeviceType->default('');

my $popFirmware   = USAT::DeviceAdmin::UI::USAPopups->popFirmware({param => 'Firmware Version'} );
$popFirmware->head('','Any Firmware Version');
$popFirmware->default('');

my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="2" border="1" width="100%"');
$table->put(0,0,'Device Configuration Wizard - Page 1: Search Criteria','class=header1 colspan=6');
$table->put(1,0,'Customer: ');
$table->put(1,1, $popCustomer);
$table->put(2,0,'Location: ');
$table->put(2,1, $popLocation);
$table->put(3,0,'Firmware: ');
$table->put(3,1, $popFirmware);
$table->put(4,0,'Device Type: ');
$table->put(4,1, $popDeviceType);
$table->put(5,0,'First serial number in sequence: ');
my $sql = 'SELECT device_serial_format_prefix, device_serial_format_prefix
             FROM device.device_serial_format
            WHERE device_type_id = ?';

my $txtPrefix = TEXT('start_serial_head','',{sz_ml => [4,10]});

$table->put(5,1,'Serial Prefix: '.$txtPrefix.' Serial Number: <input type="text" onChange="clearList();" id="start_serial_number_id"  name="start_serial_number" size="11"> Number of devices in sequence: <input type="text" name="num_devices_seq" id="num_devices_seq_id" size="7">');
$table->put(6,0,'Max number of devices: ');
$table->put(6,1,'<input type="text" name="num_devices" size="7" value="20">');
$table->put(7,0,'Serial Number List<br>(1 per line)');
my $serialNumberList = OOCGI::TextArea->new(name => 'serial_number_list',
                       id => 'serial_number_list_id',
                       onChange => 'clearSelect();',
                       cols => 20, rows => 10);
$table->put(7,1,$serialNumberList);
$table->put(8,0,q{
 <table cellspacing="0" cellpadding="2" border="1" width="100%">
  <tr>
   <td align="center" bgcolor="#C0C0C0">
    <input type=button value="Cancel" onClick="javascript:window.location = '/';">
    <input type="reset" value="Reset">
    <input type="submit" name="action" value="Next >">
   </td>
  </tr>
 </table>},'colspan=2');

print ' <form method="post" onload="get_serial_format_prefix()" action="bulk_config_wizard_2.cgi"> ';

display $table;
    
print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
