#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my $pageNav  = OOCGI::PageNavigation->new(25);

my %PARAM = $query->Vars;
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $terminal_id = $PARAM{"terminal_id"};

my $terminal_info_sql = q{
	SELECT terminal.terminal_id,
           terminal.terminal_cd,
           terminal.terminal_encrypt_key,
           terminal.terminal_encrypt_key2, 
	       terminal.terminal_next_batch_num,
           terminal.merchant_id,
           terminal.terminal_desc,
           terminal_state.terminal_state_name, 
	       terminal.terminal_max_batch_num,
           terminal.terminal_batch_max_tran,
           terminal.terminal_batch_cycle_num, 
	       to_char(terminal.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           to_char(terminal.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	       merchant.merchant_name,
           terminal.terminal_min_batch_num,
           terminal.terminal_min_batch_close_hr,
           terminal.terminal_max_batch_close_hr
	  FROM terminal,
           terminal_state,
           merchant
	 WHERE terminal.terminal_state_id = terminal_state.terminal_state_id 
	   AND terminal.merchant_id = merchant.merchant_id
	   AND terminal.terminal_id = ?
};
my $qo = OOCGI::Query->new($terminal_info_sql, { bind => $terminal_id } );

if(my @terminal_info = $qo->next_array) {
   my $intable = OOCGI::NTable->new('cellspacing="0" cellpadding="2" border="1" width="100%"');
   my $rownum  = 0;
   $intable->put(0,0,'Terminal','colspan=4 align=center bgcolor=#C0C0C0');
   $rownum++;
   $intable->put($rownum,0,'ID');
   $intable->put($rownum,1,$terminal_info[0]);
   $intable->put($rownum,2,'State');
   $intable->put($rownum,3,$terminal_info[7]);
   $rownum++;
   $intable->put($rownum,0,'Code');
   $intable->put($rownum,1,$terminal_info[1]);
   $intable->put($rownum,2,'Key 1');
   $intable->put($rownum,3,$terminal_info[2]);
   $rownum++;
   $intable->put($rownum,0,'Name');
   $intable->put($rownum,1,$terminal_info[6]);
   $intable->put($rownum,2,'Key 2');
   $intable->put($rownum,3,$terminal_info[3]);
   $rownum++;
   $intable->put($rownum,0,'Merchant');
   $intable->put($rownum,1,qq{<a href="merchant.cgi?merchant_id=$terminal_info[5]">$terminal_info[13]</a>});
   $intable->put($rownum,2,'Next Batch Number');
   $intable->put($rownum,3,$terminal_info[4]);
   $rownum++;
   $intable->put($rownum,0,'Created');
   $intable->put($rownum,1,$terminal_info[11]);
   $intable->put($rownum,2,'Batch Cycle Number');
   $intable->put($rownum,3,$terminal_info[10]);
   $rownum++;
   $intable->put($rownum,0,'Last Updated');
   $intable->put($rownum,1,$terminal_info[12]);
   $intable->put($rownum,2,'Max Batch Number');
   $intable->put($rownum,3,$terminal_info[8]);
   $rownum++;
   $intable->put($rownum,0,'Batch Close Settings');
   $intable->put($rownum,1,I(qq{Close batches if they contain at least $terminal_info[14] transactions and have been open for at least $terminal_info[15] hours, or $terminal_info[9] transactions, or $terminal_info[16] hours.}),'colspan=3');

   display $intable;
}

my $terminal_batch_list_sql = q{
	SELECT terminal_batch.terminal_batch_id,
           terminal_batch.terminal_id,
           terminal_batch.terminal_batch_num, 
	       to_char(terminal_batch.terminal_batch_open_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
           to_char(terminal_batch.terminal_batch_close_ts, 'MM/DD/YYYY HH:MI:SS AM'),
           to_char(terminal_batch.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	       to_char(terminal_batch.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM') 
	  FROM terminal_batch 
	 WHERE terminal_batch.terminal_id = ?
};

my @table_cols = (
    ['ID'          ,  1],
    ['Batch<br>Num',  2],
    ['Time Opened' , -3],
    ['Time Closed' , -4],
);

my @sql_cols = (
    'terminal_batch.terminal_batch_id',
    'terminal_batch.terminal_batch_num',
    "NVL(terminal_batch.terminal_batch_open_ts, TO_DATE('1970', 'YYYY'))",
    "NVL(terminal_batch.terminal_batch_close_ts, TO_DATE('1970', 'YYYY'))"
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');
$table->put($rn_table,0,B('Terminal Batches'),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

my $sql = $terminal_batch_list_sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo1  = $pageNav->get_result_obj($sql, [  $terminal_id ] );

while (my @terminal_batch_info = $qo1->next_array())
{
    $table->put($rn_table,0,$terminal_batch_info[0]);
    $table->put($rn_table,1,qq{<a href="terminal_batch.cgi?terminal_batch_id=$terminal_batch_info[0]">$terminal_batch_info[2]</a>});
    $table->put($rn_table,2,$terminal_batch_info[3]);
    $table->put($rn_table,3,$terminal_batch_info[4]);

    $rn_table++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
