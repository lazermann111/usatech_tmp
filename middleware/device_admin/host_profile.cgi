#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my %status_codes_hash = (
	'0'	=>	'No Status Available',
	'1'	=>	'Idle, Available',
	'2'	=>	'In 1st Cycle',
	'3'	=>	'Out Of Service',
	'4'	=>	'Nothing on Port',
	'5'	=>	'Idle, Not Available',
	'6'	=>	'Manual Service Mode',
	'7'	=>	'In 2nd Cycle',
	'8'	=>	'Transaction In Progress',
);

use constant {
	EQUIP_POSITION_UNKNOWN 		=> 'U',
	EQUIP_POSITION_TOP 			=> 'T',
	EQUIP_POSITION_BOTTOM 		=> 'B',
};

my $host_id = $PARAM{"host_id"};
if(length($host_id) == 0)
{
	print "Required Parameter Not Found: host_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $get_host_stmt = $dbh->prepare(q{
	  SELECT HOST.host_id,
	         HOST.host_type_id, 
	         HOST.host_equipment_id,
	         TO_CHAR (HOST.host_last_start_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	         HOST.host_status_cd, 
	         HOST.host_serial_cd,
	         HOST.host_est_complete_minut, 
	         HOST.host_port_num,
	         HOST.host_position_num, 
	         HOST.host_label_cd, 
	         host_type.host_type_desc,
	         host_equipment.host_equipment_mfgr,
	         host_equipment.host_equipment_model,
	         host_type.host_default_complete_minut,
	         device_type_host_type.device_type_host_type_cd,
	         host.device_id,
	         TO_CHAR (HOST.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	         TO_CHAR (HOST.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM')
	    FROM device, HOST, host_type, host_equipment, device_type_host_type
	   WHERE device.device_id = host.device_id
	     and HOST.host_type_id = host_type.host_type_id
	     and host.host_equipment_id = host_equipment.host_equipment_id
	     and host_type.host_type_id = device_type_host_type.host_type_id
	     and device.device_type_id = device_type_host_type.device_type_id
	     AND host.host_id = :host_id
	ORDER BY HOST.host_port_num, HOST.host_position_num	
});
$get_host_stmt->bind_param(":host_id", $host_id);
$get_host_stmt->execute();
my @data = $get_host_stmt->fetchrow_array();
$get_host_stmt->finish();

$host_id = $data[0];
my $host_type_id = $data[1];
my $host_equipment_id = $data[2];
my $host_last_start_ts = $data[3];
my $host_status = $data[4];
my $host_serial_cd = $data[5];
my $host_comp_min = $data[6];
my $host_port = $data[7];
my $host_position = $data[8];
my $host_label = $data[9];
my $host_type_name = $data[10];
my $host_manufacturer = $data[11];
my $host_model = $data[12];
my $host_default_comp_min = $data[13];
my $host_type_cd = $data[14];
my $device_id = $data[15];
my $host_created_ts = $data[16];
my $host_last_updated_ts = $data[17];

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Host Profile</th>
 </tr>
 <tr>
  <td nowrap>Host ID</td>
  <td width=\"50%\">$host_id&nbsp;</td>
  <td nowrap>Created</td>
  <td width=\"50%\">$host_created_ts&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Last Start Time</td>
  <td width=\"50%\">$host_last_start_ts&nbsp;</td>
  <td nowrap>Last Updated</td>
  <td width=\"50%\">$host_last_updated_ts&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Label</td>
  <td width=\"50%\">$host_label&nbsp;</td>
  <td nowrap>Manufacturer</td>
  <td width=\"50%\">$host_manufacturer&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Host Name</td>
  <td width=\"50%\">$host_type_name&nbsp;</td>
  <td nowrap>Model Code</td>
  <td width=\"50%\">$host_model&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Port/Position</td>
  <td width=\"50%\">$host_port/" . ($host_position eq '0' ? 'Bottom' : 'Top') . "&nbsp;</td>
  <td nowrap>Serial Number</td>
  <td width=\"50%\">$host_serial_cd&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Status</td>
  <td width=\"50%\">" . $status_codes_hash{$host_status} . "&nbsp;</td>
  <td nowrap>Completion Minutes</td>
  <td width=\"50%\">$host_comp_min&nbsp;</td>
 </tr>
</table>
";

my $get_device_info_stmt = $dbh->prepare("SELECT dev.device_id, dev.device_name, dev.device_serial_cd, TO_CHAR(dev.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), TO_CHAR(dev.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), devt.device_type_desc, dev.device_active_yn_flag, dev.encryption_key, dev.device_type_id, pos.location_id, pos.customer_id, location.location_name, customer.customer_name FROM device.device dev, device.device_type devt, pos, customer, location.location WHERE dev.device_type_id = devt.device_type_id AND pos.customer_id = customer.customer_id and pos.location_id = location.location_id AND dev.device_id = pos.device_id AND dev.device_id = :device_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_device_info_stmt->bind_param(":device_id", $device_id);
$get_device_info_stmt->execute();
my @device_data = $get_device_info_stmt->fetchrow_array();
$get_device_info_stmt->finish();
if(!@device_data)
{
	print "Device not found!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $ev_number = $device_data[1];
my $serial_number = $device_data[2];
my $device_type = $device_data[5];
my $customer = $device_data[11];
my $location = $device_data[12];

print "
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"6\" bgcolor=\"#C0C0C0\">Device Information</th>
 </tr>
 <tr>
  <th>Device ID</th>
  <th>EV Number</th>
  <th>Serial Number</th>
  <th>Device Type</th>
  <th>Customer</th>
  <th>Location</th>
 </tr>
 <tr>
  <td><a href=\"profile.cgi?device_id=$device_id\">$device_id</a></td>
  <td>$ev_number</td>
  <td>$serial_number</td>
  <td>$device_type</td>
  <td>$customer</td>
  <td>$location</td>
 </tr>
</table>
";

print "
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Settings</th>
 </tr>
 <tr>
  <th>Code</th>
  <th>Value</th>
  <th>Last Updated</th>
 </tr>
";

my $host_settings_stmt = $dbh->prepare("select host_setting_parameter, host_setting_value, to_char(last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM') from host_setting where host_id = :host_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$host_settings_stmt->bind_param(":host_id", $host_id);
$host_settings_stmt->execute();
while (my @setting_data = $host_settings_stmt->fetchrow_array()) 
{
	print "
	 <tr>
	  <td nowrap>$setting_data[0]&nbsp;</td>
	  <td nowrap>$setting_data[1]&nbsp;</td>
	  <td nowrap>$setting_data[2]&nbsp;</td>
	 </tr>";
}
$host_settings_stmt->finish();

print "
</table>
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"5\" bgcolor=\"#C0C0C0\">Cycle Time Estimate</th>
 </tr>
 <tr>
  <th>Cycle Type</th>
  <th>Average Time</th>
  <th>Cycles in Average</th>
  <th>Last Cycle Time</th>
  <th>Last Updated</th>
 </tr>
";

my $host_est_time_stmt = $dbh->prepare("select  tran_line_item_type.tran_line_item_type_desc, est_tran_complete_time.avg_tran_complete_minut, est_tran_complete_time.num_cycle_in_avg, est_tran_complete_time.last_tran_complete_minut, to_char(est_tran_complete_time.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM') from est_tran_complete_time, tran_line_item_type where est_tran_complete_time.tran_line_item_type_id = tran_line_item_type.tran_line_item_type_id and est_tran_complete_time.host_id = :host_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$host_est_time_stmt->bind_param(":host_id", $host_id);
$host_est_time_stmt->execute();
while (my @est_data = $host_est_time_stmt->fetchrow_array()) 
{
	print "
	 <tr>
	  <td nowrap>$est_data[0]&nbsp;</td>
	  <td nowrap>$est_data[1]&nbsp;</td>
	  <td nowrap>$est_data[2]&nbsp;</td>
	  <td nowrap>$est_data[3]&nbsp;</td>
	  <td nowrap>$est_data[4]&nbsp;</td>
	 </tr>";
}
$host_est_time_stmt->finish();

print "
</table>
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"5\" bgcolor=\"#C0C0C0\">Diagnostics</th>
 </tr>
 <tr>
  <th>Code</th>
  <th>Value</th>
  <th>Start Time</th>
  <th>Last Reported Time</th>
  <th>Cleared Time</th>
 </tr>
";

my $host_diag_stmt = $dbh->prepare("select host_diag_cd, host_diag_value, to_char(host_diag_start_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(host_diag_last_reported_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(host_diag_clear_ts, 'MM/DD/YYYY HH:MI:SS AM') from host_diag_status where host_id = :host_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$host_diag_stmt->bind_param(":host_id", $host_id);
$host_diag_stmt->execute();
while (my @diag_data = $host_diag_stmt->fetchrow_array()) 
{
	print "
	 <tr>
	  <td nowrap>$diag_data[0]&nbsp;</td>
	  <td nowrap>$diag_data[1]&nbsp;</td>
	  <td nowrap>$diag_data[2]&nbsp;</td>
	  <td nowrap>$diag_data[3]&nbsp;</td>
	  <td nowrap>$diag_data[4]&nbsp;</td>
	 </tr>";
}
$host_diag_stmt->finish();

print "</table>";

$dbh->disconnect;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
