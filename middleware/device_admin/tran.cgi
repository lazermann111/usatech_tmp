#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::Database;
use USAT::Security::StringMask qw(mask_credit_card);
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my %PARAM = $query->Vars();

my $tran_id = $PARAM{"tran_id"};
if(length($tran_id) == 0)
{
	print "Required Parameter Not Found: tran_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $tran_info_stmt = $dbh->prepare(q{
	select t.tran_id,
    to_char(t.tran_start_ts, 'MM/DD/YYYY HH:MI:SS AM'),
    to_char(t.tran_upload_ts, 'MM/DD/YYYY HH:MI:SS AM'),
    to_char(t.tran_end_ts, 'MM/DD/YYYY HH:MI:SS AM'),
    ts.tran_state_desc,
    t.tran_desc,
    t.consumer_acct_id,
    t.tran_device_tran_cd,
    to_char(t.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
    to_char(t.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
    t.tran_account_pin,
    t.tran_received_raw_acct_data,
    t.tran_parsed_acct_name,
    t.tran_parsed_acct_exp_date,
    t.tran_parsed_acct_num,
    t.pos_pta_id,
    t.tran_device_result_type_cd,
    tdrt.tran_device_result_type_desc,
    t.tran_global_trans_cd,
    t.tran_legacy_trans_no,
    t.tran_parsed_acct_num_hash,
    t.tran_parsed_acct_num_encr,
    pp.pos_pta_id,
    pp.terminal_id,
    pp.merchant_bank_acct_id,
    pp.currency_cd,
    ps.payment_subtype_id,
    ps.payment_subtype_name,
    ps.payment_subtype_desc,
    ps.client_payment_type_cd,
    cpt.client_payment_type_desc,
    d.device_id,
    d.device_name,
    d.device_serial_cd,
    dt.device_type_desc,
    t.parent_tran_id,
    ps.payment_subtype_table_name,
    pp.payment_subtype_key_id,
	pt.payment_type_desc,
	pem.payment_entry_method_desc,
	st.sale_type_desc,
	s.device_batch_id,
	to_char(cast(s.sale_start_utc_ts as date) + s.sale_utc_offset_min/1440, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(cast(s.sale_end_utc_ts as date) + s.sale_utc_offset_min/1440, 'MM/DD/YYYY HH:MI:SS AM'),
	sr.sale_result_desc,
	s.sale_amount,
	rr.receipt_result_desc,
	s.duplicate_count,
	t.consumer_acct_id
	from pss.tran t, pss.tran_state ts, pss.tran_device_result_type tdrt, pss.pos_pta pp, pss.payment_subtype ps, pss.client_payment_type cpt, pss.pos, device.device d, device.device_type dt,
	pss.payment_type pt, pss.payment_entry_method pem, pss.sale s, pss.sale_type st, pss.sale_result sr, pss.receipt_result rr
	where t.tran_state_cd = ts.tran_state_cd
	and t.tran_device_result_type_cd = tdrt.tran_device_result_type_cd(+)
	and t.pos_pta_id = pp.pos_pta_id
	and pp.payment_subtype_id = ps.payment_subtype_id
	and ps.client_payment_type_cd = cpt.client_payment_type_cd
	and cpt.payment_type_cd = pt.payment_type_cd
	and cpt.payment_entry_method_cd = pem.payment_entry_method_cd
	and pp.pos_id = pos.pos_id
	and pos.device_id = d.device_id
	and d.device_type_id = dt.device_type_id
	and t.tran_id = :tran_id
	and t.tran_id = s.tran_id (+)
	and s.sale_type_cd = st.sale_type_cd (+)
	and s.sale_result_id = sr.sale_result_id (+)
	and s.receipt_result_cd = rr.receipt_result_cd (+)
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$tran_info_stmt->bind_param(":tran_id", $tran_id);
$tran_info_stmt->execute();
my @tran_data = $tran_info_stmt->fetchrow_array();
$tran_info_stmt->finish();

my $pos_pta_id = $tran_data[15];

my $acct_data = mask_credit_card($tran_data[11]);

my %tran_totals_hash = ();
my $tran_totals_stmt = $dbh->prepare(q{
	SELECT tran_line_item_batch_type_cd, COUNT(1), NVL(SUM((NVL(tran_line_item_amount, 0) + NVL(tran_line_item_tax, 0)) * NVL(tran_line_item_quantity, 0)), 0)
	FROM pss.tran_line_item
	WHERE tran_id = :tran_id
	GROUP BY tran_line_item_batch_type_cd
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$tran_totals_stmt->bind_param(":tran_id", $tran_id);
$tran_totals_stmt->execute();
while (my @tran_totals = $tran_totals_stmt->fetchrow_array()) 
{
	$tran_totals_hash{$tran_totals[0]} = [$tran_totals[1], $tran_totals[2]];
}
	
$tran_totals_stmt->finish();

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Transaction</th>
 </tr>
 <tr>
  <td nowrap>Tran ID</td>
  <td width=\"50%\">$tran_data[0]&nbsp;</td>
  <td nowrap>Start Time</td>
  <td width=\"50%\">$tran_data[1]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Reference Tran ID</td>
  <td width=\"50%\"><a href=\"tran.cgi?tran_id=$tran_data[35]\">$tran_data[35]</a>&nbsp;</td>
  <td nowrap>End Time</td>
  <td width=\"50%\">$tran_data[3]&nbsp;</td>
 </tr>
 <tr>
  <td>Created</td>
  <td width=\"50%\">$tran_data[8]&nbsp;</td> 
  <td nowrap>Upload Time</td>
  <td width=\"50%\">$tran_data[2]&nbsp;</td>  
 </tr>
 <tr>
  <td nowrap>Last Updated</td>
  <td width=\"50%\">$tran_data[9]&nbsp;</td>
  <td nowrap>Consumer Account</td>
  <td width=\"50%\">" . (defined $tran_data[48] ? "<a href=\"edit_consumer_acct.cgi?consumer_acct_id=$tran_data[48]\">$tran_data[48]</a>" : "") . "&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>State</td>
  <td width=\"50%\">$tran_data[4]&nbsp;</td>
  <td nowrap>Result</td>
  <td width=\"50%\">$tran_data[16]:$tran_data[17]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Device Tran ID</td>
  <td width=\"50%\">$tran_data[7]&nbsp;</td>
  <td nowrap>Global Trans No</td>
  <td width=\"50%\">$tran_data[18]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Legacy Trans No</td>
  <td width=\"50%\">$tran_data[19]&nbsp;</td>
  <td nowrap>Client Payment Type</td>
  <td width=\"50%\">$tran_data[29]:$tran_data[30]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Payment Type</td>
  <td width=\"50%\">$tran_data[38]&nbsp;</td>
  <td nowrap>Payment Entry Method</td>
  <td width=\"50%\">$tran_data[39]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Account Data</td>
  <td width=\"50%\">$acct_data&nbsp;</td>
  <td nowrap>Consumer Account ID</td>
  <td width=\"50%\">$tran_data[6]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Expiration</td>
  <td width=\"50%\">$tran_data[13]&nbsp;</td>
  <td nowrap>Device Serial #</td>
  <td width=\"50%\"><a href=\"profile.cgi?device_id=$tran_data[31]\">$tran_data[33]</a>&nbsp;</td>
 </tr>
 <tr>
   <td nowrap>Name</td>
   <td width=\"50%\">$tran_data[12]&nbsp;</td>
   <td nowrap>Device Type</td>
   <td width=\"50%\">$tran_data[34]&nbsp;</td>
 </tr>
 <tr>
   <td nowrap>Intended Purchase</td>
   <td width=\"50%\">" . ($tran_totals_hash{'I'}[0] ? $tran_totals_hash{'I'}[0] : 0) . "/\$" . ($tran_totals_hash{'I'}[1] ? $tran_totals_hash{'I'}[1] : 0) . "</td>
   <td nowrap>Actual Purchase</td>
   <td width=\"50%\">" . ($tran_totals_hash{'A'}[0] ? $tran_totals_hash{'A'}[0] : 0) . "/\$" . ($tran_totals_hash{'A'}[1] ? $tran_totals_hash{'A'}[1] : 0) . "</td>
 </tr>
 <tr>
   <td nowrap>Payment Type</td>
   <td width=\"50%\">$tran_data[27]&nbsp;</td>
";

if($tran_data[36] eq 'TERMINAL') 
{
	my $terminal_info = $dbh->prepare(q{
		select terminal.terminal_id, terminal.terminal_cd, merchant.merchant_id, merchant.merchant_cd, merchant.merchant_name, merchant.merchant_desc, authority.authority_id, authority.authority_name
		from pss.terminal, pss.merchant, authority.authority
		where terminal.merchant_id = merchant.merchant_id
		and merchant.authority_id = authority.authority_id
		and terminal.terminal_id = :terminal_id
	}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$terminal_info->bind_param(":terminal_id", $tran_data[37]);
	$terminal_info->execute();
	my @terminal_data = $terminal_info->fetchrow_array();
	$terminal_info->finish();
	
	print "
   <td nowrap>Terminal</td>
   <td width=\"50%\"><a href=\"terminal.cgi?terminal_id=$terminal_data[0]\">$terminal_data[1]</a></td>
  </tr>
  <tr>
   <td nowrap>Authority</td>
   <td width=\"50%\"><a href=\"authority.cgi?authority_id=$terminal_data[6]\">$terminal_data[7]</a></td>
   <td nowrap>Merchant</td>
   <td width=\"50%\"><a href=\"merchant.cgi?merchant_id=$terminal_data[2]\">$terminal_data[4]</a></td>
  </tr>
   ";
}
else
{
	print "
   <td nowrap>Payment Category</td>
   <td width=\"50%\">$tran_data[36]</td>
  </tr>
   ";
}

if ($tran_data[40])
{
	print "
 <tr>
   <td nowrap>Sale Type</td>
   <td>$tran_data[40]&nbsp;</td>
   <td nowrap>Sale Result</td>
   <td>$tran_data[44]&nbsp;</td>   
 </tr>
 <tr>
   <td nowrap>Sale Start Time</td>
   <td>$tran_data[42]&nbsp;</td>
   <td nowrap>Sale End Time</td>
   <td>$tran_data[43]&nbsp;</td>   
 </tr>
 <tr> 
  <td nowrap>Sale Amount</td>
   <td>\$$tran_data[45]&nbsp;</td>     
  <td nowrap>Device Batch ID</td>
   <td>$tran_data[41]&nbsp;</td>   
 </tr>
 <tr> 
  <td nowrap>Receipt Result</td>
   <td>$tran_data[46]&nbsp;</td>     
  <td nowrap>Duplicate Count</td>
   <td>$tran_data[47]&nbsp;</td>   
 </tr>
	";
}

print "
</table>

<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">

<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"9\" bgcolor=\"#C0C0C0\">Line Items</th>
 </tr>
 <tr>
  <th>Line Item ID</th>
  <th>Host ID</th>
  <th>Batch Type</th>
  <th>Item Type</th>
  <th>Purchase</th>
  <th>Description</th>
  <th>Timestamp</th>
  <th>Sale Result</th>
 </tr>
";

my $line_items_stmt = $dbh->prepare(q{
	SELECT tli.tran_line_item_id, 
		abs(tli.tran_line_item_amount), 
		tli.tran_line_item_tax, 
		tli.tran_line_item_desc, 
		tli.tran_line_item_type_id, 
		tli.tran_line_item_quantity, 
		tli.host_id, 
		tlibt.tran_line_item_batch_type_cd,
		tlit.tran_line_item_type_desc,
		tlibt.tran_line_item_batch_type_desc,
		(abs(NVL(tli.tran_line_item_amount, 0)) + NVL(tli.tran_line_item_tax, 0)) * NVL(tli.tran_line_item_quantity, 0),
		tli.tran_line_item_batch_type_cd,
		tli.tran_line_item_position_cd,
		to_char(tli.tran_line_item_ts, 'MM/DD/YYYY HH:MI:SS AM'),
		sr.sale_result_desc
	FROM pss.tran_line_item tli, pss.tran_line_item_batch_type tlibt, pss.tran_line_item_type tlit, pss.sale_result sr
	WHERE tli.tran_line_item_batch_type_cd = tlibt.tran_line_item_batch_type_cd
	AND tran_id = :tran_id
	AND tli.tran_line_item_type_id = tlit.tran_line_item_type_id
	AND tli.sale_result_id = sr.sale_result_id (+)
	ORDER BY tlibt.tran_line_item_batch_type_cd desc, tli.tran_line_item_id
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$line_items_stmt->bind_param(":tran_id", $tran_id);
$line_items_stmt->execute();
my %intended_hash = ();
while (my @data = $line_items_stmt->fetchrow_array()) 
{
	my $line_item_id = $data[0];
	my $amount = $data[1];
	my $tax = $data[2];
	my $desc = $data[3];
	my $type_id = $data[4];
	my $quantity = $data[5];
	my $host_id = $data[6];
	my $batch_type_cd = $data[7];
	my $type_desc = $data[8];
	my $batch_type_desc = $data[9];
	my $line_total = $data[10];
	my $tran_line_item_batch_type_cd = $data[11];
	my $tran_line_item_position_cd = $data[12];
	my $tran_line_item_ts = $data[13];
	my $tli_sale_result = $data[14];
	
	my $key = "$host_id $type_id $tran_line_item_position_cd";
	
	if($tran_line_item_batch_type_cd eq 'I')
	{
		$intended_hash{$key} = [$data[0], $data[1], $data[2], $data[3], $data[4], $data[5], $data[6], $data[7], $data[8], $data[9], $data[10], $data[11], $data[12], $data[13], $data[14]];
	}
	else
	{
		my $intended_data = $intended_hash{$key};
		
		print "	
		 <tr>
		  <td>$line_item_id&nbsp;</td>
		  <td><a href=\"/host_profile.cgi?host_id=$host_id\">$host_id</a>&nbsp;</td>
		  <td>$batch_type_desc&nbsp;</td>
		  <td>$type_desc&nbsp;</td>
		  <td nowrap>";
		  
		  if(defined $intended_data)
		  {
		  	print "<table cellspacing=0 cellpadding=0 border=0 width=100%>
		  	 <tr><td nowrap>Intended: </td><td nowrap>$quantity/\$$amount&nbsp;</td></tr>
		  	 <tr><td nowrap>Actual: </td><td nowrap>$quantity/\$$amount&nbsp;</td></tr>
		  	</table>";
		  	delete($intended_hash{$key});
		  }
		  else
		  {
		  	print "$quantity/\$$amount&nbsp;";
		  }
		  
		  print "
		  </td>
		  <td>$desc&nbsp;</td>
		  <td>$tran_line_item_ts&nbsp;</td>
		  <td>$tli_sale_result&nbsp;</td>
		 </tr>";
	}
}

$line_items_stmt->finish();

while(my ($key, $data) = each(%intended_hash)) 
{
	my $line_item_id = $$data[0];
	my $amount = $$data[1];
	my $tax = $$data[2];
	my $desc = $$data[3];
	my $type_id = $$data[4];
	my $quantity = $$data[5];
	my $host_id = $$data[6];
	my $batch_type_cd = $$data[7];
	my $type_desc = $$data[8];
	my $batch_type_desc = $$data[9];
	my $line_total = $$data[10];
	my $tran_line_item_batch_type_cd = $$data[11];
	my $tran_line_item_position_cd = $$data[12];
	my $tran_line_item_ts = $$data[13];
	my $tli_sale_result = $$data[14];

	print "	
	 <tr>
	  <td>$line_item_id&nbsp;</td>
	  <td><a href=\"/host_profile.cgi?host_id=$host_id\">$host_id</a>&nbsp;</td>
	  <td>$batch_type_desc&nbsp;</td>
	  <td>$type_desc&nbsp;</td>
	  <td nowrap>$quantity/\$$amount&nbsp;</td>
	  <td>$desc&nbsp;</td>
	  <td>$tran_line_item_ts&nbsp;</td>
	  <td>$tli_sale_result&nbsp;</td>
	 </tr>";
}

print "
</table>
";

my $details_stmt = $dbh->prepare(q{
	select auth.auth_id, 
	auth.tran_id, 
	auth_type.auth_type_desc, 
	auth_state.auth_state_name, 
	auth.auth_parsed_acct_data, 
	acct_entry_method.acct_entry_method_desc, 
	abs(auth.auth_amt), 
	to_char(auth.auth_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	auth_result.auth_result_desc, 
	auth.auth_resp_cd, 
	auth.auth_resp_desc, 
	auth.auth_authority_tran_cd, 
	auth.auth_authority_ref_cd, 
	to_char(auth.auth_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(auth.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(auth.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	auth.terminal_batch_id,
	auth.auth_amt_approved, 
	auth.auth_authority_misc_data,
	auth_authority_amt_rqst,
	auth_authority_amt_rcvd
	from pss.auth, pss.auth_type, pss.auth_state, pss.auth_result, pss.acct_entry_method
	where auth.auth_type_cd = auth_type.auth_type_cd
	and auth.auth_state_id = auth_state.auth_state_id
	and auth.auth_result_cd = auth_result.auth_result_cd(+)
	and auth.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd(+)
	and auth.tran_id = :tran_id
	order by auth.auth_ts asc
});
$details_stmt->bind_param(":tran_id", $tran_id);
$details_stmt->execute();

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"6\" bgcolor=\"#C0C0C0\">Processing Details</th>
 </tr>
 <tr>
  <th>Detail ID</th>
  <th>Type</th>
  <th>State</th>
  <th>Amount</th>
  <th>Timestamp</th>
  <th>Result</th>
 </tr>
";

my @detail_data;
while (@detail_data = $details_stmt->fetchrow_array()) 
{
	print "
	     <tr>
    	 <td><a href=\"auth.cgi?auth_id=$detail_data[0]\">$detail_data[0]</a></td>
    	 <td>$detail_data[2]&nbsp;</td>
    	 <td>$detail_data[3]&nbsp;</td>
    	 <td>\$$detail_data[6]&nbsp;" . (defined $detail_data[19] ? "(\$$detail_data[19] Override)" : "") . " </td>
    	 <td>$detail_data[7]&nbsp;</td>
    	 <td>$detail_data[8]&nbsp;</td>
    	</tr>
	";
}

$details_stmt->finish();

my $refund_stmt = $dbh->prepare(q{
	select refund.refund_id, 
	refund.tran_id, 
	refund.refund_desc, 
	refund_state.refund_state_name, 
	refund.refund_parsed_acct_data, 
	acct_entry_method.acct_entry_method_desc, 
	abs(refund.refund_amt), 
	to_char(refund.refund_issue_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	refund.refund_resp_cd, 
	refund.refund_resp_desc, 
	refund.refund_authority_tran_cd, 
	refund.refund_authority_ref_cd, 
	to_char(refund.refund_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(refund.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(refund.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	refund.terminal_batch_id,
	refund.refund_authority_misc_data,
	refund.refund_issue_by
	from refund, refund_type, refund_state, acct_entry_method
	where refund.refund_type_cd = refund_type.refund_type_cd
	and refund.refund_state_id = refund_state.refund_state_id
	and refund.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd(+)
	and refund.tran_id = :tran_id
	order by refund.refund_issue_ts asc
});
$refund_stmt->bind_param(":tran_id", $tran_id);
$refund_stmt->execute();

while (my @refund = $refund_stmt->fetchrow_array()) 
{
	print "
	     <tr>
    	 <td><a href=\"refund.cgi?refund_id=$refund[0]\">$refund[0]</a></td>
    	 <td>$refund[2]&nbsp;</td>
    	 <td>$refund[3]&nbsp;</td>
    	 <td>\$$refund[6]&nbsp;</td>
    	 <td>$refund[7]&nbsp;</td>
    	 <td>" . (defined $refund[8] ? ($refund[8] eq '0' ? "Successful" : "Failure") : "") . "&nbsp;</td>
    	</tr>
	";
}

$refund_stmt->finish();

my $tran_refund_stmt = $dbh->prepare(q{
        select refund.refund_id,
        refund.tran_id,
        refund.refund_desc,
        refund_state.refund_state_name,
        refund.refund_parsed_acct_data,
        acct_entry_method.acct_entry_method_desc,
        abs(refund.refund_amt),
        to_char(refund.refund_issue_ts, 'MM/DD/YYYY HH:MI:SS AM'),
        refund.refund_resp_cd,
        refund.refund_resp_desc,
        refund.refund_authority_tran_cd,
        refund.refund_authority_ref_cd,
        to_char(refund.refund_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'),
        to_char(refund.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
        to_char(refund.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
        refund.terminal_batch_id,
        refund.refund_authority_misc_data,
        refund.refund_issue_by
        from pss.tran, pss.refund, pss.refund_type, pss.refund_state, pss.acct_entry_method
        where refund.refund_type_cd = refund_type.refund_type_cd
        and refund.refund_state_id = refund_state.refund_state_id
        and refund.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd(+)
        and refund.tran_id = tran.tran_id
	and tran.parent_tran_id = :tran_id
	and tran.pos_pta_id = :pos_pta_id
        order by refund.refund_issue_ts asc
});
$tran_refund_stmt->bind_param(":tran_id", $tran_id);
$tran_refund_stmt->bind_param(":pos_pta_id", $pos_pta_id);
$tran_refund_stmt->execute();

while (my @refund = $tran_refund_stmt->fetchrow_array())
{
        print "
             <tr>
         <td><a href=\"refund.cgi?refund_id=$refund[0]\">$refund[0]</a></td>
         <td>$refund[2]&nbsp;</td>
         <td>$refund[3]&nbsp;</td>
         <td>\$$refund[6]&nbsp;</td>
         <td>$refund[7]&nbsp;</td>
         <td>" . (defined $refund[8] ? ($refund[8] eq '0' ? "Successful" : "Failure") : "") . "&nbsp;</td>
        </tr>
        ";
}

$tran_refund_stmt->finish();

print "
</table>
";

$dbh->disconnect;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
