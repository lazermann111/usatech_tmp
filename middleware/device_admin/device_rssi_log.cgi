#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use OOCGI::PageSort::Simple;
use OOCGI::PageNavigation;
use USAT::DeviceAdmin::UI::DAHeader;

my %enabled_hash = (	'Y'	=>	'Enabled',
						'N'	=>	'Disabled' );

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my $pageNav  = OOCGI::PageNavigation->new(25);

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $enabled    = $PARAM{"enabled"};
my $toler_rssi = $PARAM{"toler_rssi"};
my $toler_ber  = $PARAM{"toler_ber"};

if (length($toler_rssi) == 0 || length($toler_ber) == 0)
{
	print "Required parameter not found: toler_rssi or toler_ber\n";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

if(length($enabled) == 0)
{
	$enabled = 'Y';
}
my @enable_disable = split(/,/,$enabled);
my $questionmarks  = join(',', ('?') x scalar(@enable_disable));

my $list_devices_sql = "
 SELECT dev.device_id,
        dt.device_type_id,
        dt.device_type_desc,
        dev.device_name,
        dev.device_serial_cd,
        rss.iccid,
        rss.rssi,
        rss.ber,
        TO_CHAR(rss.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM') gprs_last_updated,
        ROUND((SYSDATE - rss.last_updated_ts), 1) gprs_last_updated_days,
        TO_CHAR(dev.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM') last_activity,
        ROUND((SYSDATE - dev.last_activity_ts), 1) last_activity_days,
        dev.device_active_yn_flag
   FROM ( SELECT gprs.device_id,
                 gprs.iccid,
                 TO_NUMBER(SUBSTR(gprs.rssi, 1, INSTR(gprs.rssi, ',') - 1)) rssi,
                 TO_NUMBER(SUBSTR(gprs.rssi, INSTR(gprs.rssi, ',') + 1)) ber,
                 gprs.last_updated_ts
            FROM device.gprs_device gprs
           WHERE gprs.rssi IS NOT NULL ) rss
    JOIN device.device dev ON dev.device_id = rss.device_id
     AND dev.device_active_yn_flag IN ($questionmarks)
    JOIN device.device_type dt ON dev.device_type_id = dt.device_type_id
   WHERE (rss.rssi <= ? OR rss.rssi = 99)
     AND rss.ber >= ? ";

my @table_cols = (
    ['Device Type',       5],
    ['EV Number',         2],
    ['Serial Number',     3],
    ['ICCID',             4],
    ['RSSI',              1],
    ['BER',               6],
    ['GPRS Last Updated', 7],
    ['Last Activity',     8],
    ['Status',            9]
);

my @sql_cols = (
    'dt.device_type_desc',
    'dev.device_name', 
    'dev.device_serial_cd',
    'rss.iccid',
    'rss.rssi',
    'rss.ber',
    "NVL(rss.last_updated_ts,   TO_DATE('1970', 'YYYY'))",
    "NVL(dev.last_activity_ts,  TO_DATE('1970', 'YYYY'))",
    'dev.device_active_yn_flag'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

my $sql = $list_devices_sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql, [ @enable_disable, $toler_rssi,  $toler_ber] );

while (my @data = $qo->next_array()) 
{
    $table->put($rn_table,0, $data[2]);
    $table->put($rn_table,1, qq{<a href="profile.cgi?device_id=$data[0]">$data[3]</a>});
    $table->put($rn_table,2, $data[4]);
    $table->put($rn_table,3, qq{<a href="gprsweb?iccid=$data[5]">$data[5]</a>},'nowrap');
    if ($data[6] < 7) {
       $table->put($rn_table,4, B($data[6],'red'));
    } elsif ($data[6] < 10 || $data[6] == 99) {
       $table->put($rn_table,4, B($data[6],'#DDCC00'));
    } else {
       $table->put($rn_table,4, B($data[6],'green'));
    }
	if ($data[7] > 3) {
        $table->put($rn_table,5, B($data[7],'red'));
    } elsif ($data[7] > 1) {
        $table->put($rn_table,5, B($data[7],'#DDCC00'));
    } else {
        $table->put($rn_table,5, B($data[7],'green'));
    }
	if ($data[9] > 90) {
        $table->put($rn_table,6, B("$data[8]<br>($data[9] days)",'red'),'nowrap');
    } elsif ($data[9] > 45) {
        $table->put($rn_table,6, B("$data[8]<br>($data[9] days)",'#DDCC00'),'nowrap');
    } else {
        $table->put($rn_table,6, B($data[8],'green'),'nowrap');
    }
	if ($data[11] > 5) {
        $table->put($rn_table,7, B("$data[10]<br>($data[11] days)",'red'),'nowrap');
    } elsif ($data[11] > 2) {
        $table->put($rn_table,7, B("$data[10]<br>($data[11] days)",'#DDCC00'),'nowrap');
    } else {
        $table->put($rn_table,7, B($data[10],'green'),'nowrap');
    }
	if ($data[12] eq 'Y') {
        $table->put($rn_table,8, B($enabled_hash{$data[12]},'green'));
    } else {
        $table->put($rn_table,8, B($enabled_hash{$data[12]},'red'));
    }
		
    $rn_table++;
}


#$enabled        = $PARAM{"enabled"};
#my $last_event  = $PARAM{"last_event"};
#my $active_days = $PARAM{"active_days"};
#my $toler_days  = $PARAM{"toler_days"};

display $table;

my $page_table = $pageNav->table;
display $page_table;

$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
