#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use OOCGI::NTable;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;

use strict;
my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $consumer_id = $query->param('consumer_id');
if(length($consumer_id) == 0)
{
	print "Required Parameter Not Found: consumer_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $consumer = USAT::DeviceAdmin::DBObj::Consumer->new($consumer_id);

if($query->param('action') eq 'Save') {
   $consumer = $consumer->update($query);
}
if(not defined $consumer->ID)
{
        print "Location $consumer_id not found!\n";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
}

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
my $sql;
my $table_rn = 0;

print q|<form method="post">|;
my $name = $consumer->consumer_fname.' '.
           $consumer->consumer_mname.' '.
           $consumer->consumer_lname;
my $consumer_state_cd = $consumer->consumer_state_cd;

$table->insert($table_rn,0,B("Edit Consumer - $name",4),'colspan=4
            style="background-color:#C0C0C0; text-align:center;"');
$table_rn++;

$table->insert($table_rn,0,'First Name','width=14%');
$table->insert($table_rn,1, $consumer->Text('consumer_fname', { size => 40}));
$table->insert($table_rn,2,'Work Phone','width=14%');
$table->insert($table_rn,3, $consumer->Text('consumer_work_phone_num', { size => 30}));
$table_rn++;

$table->insert($table_rn,0,'Middle Name','width=14%');
$table->insert($table_rn,1, $consumer->Text('consumer_mname', { size => 40}));
$table->insert($table_rn,2,'Cell Phone','width=14%');
$table->insert($table_rn,3, $consumer->Text('consumer_cell_phone_num', { size => 30}));
$table_rn++;

$table->insert($table_rn,0,'Last Name','width=14%');
$table->insert($table_rn,1, $consumer->Text('consumer_lname', { size => 40}));
$table->insert($table_rn,2,'Fax Number','width=14%');
$table->insert($table_rn,3, $consumer->Text('consumer_fax_phone_num', { size => 30}));
$table_rn++;

$table->insert($table_rn,0,'Solutation','width=14%');
$table->insert($table_rn,1, $consumer->Text('consumer_salutation', { size => 40}));
$table->insert($table_rn,2,'Title','width=14%');
$table->insert($table_rn,3, $consumer->Text('consumer_title', { size => 30}));
$table_rn++;

$table->insert($table_rn,0,'Email 1','width=14%');
$table->insert($table_rn,1, $consumer->Text('consumer_email_addr1', { size => 40}));
$table->insert($table_rn,2,'Email 2','width=14%');
$table->insert($table_rn,3, $consumer->Text('consumer_email_addr2', { size => 30}));
$table_rn++;

$table->insert($table_rn,0,'Pager Code','width=14%');
$table->insert($table_rn,1, $consumer->Text('consumer_pager_cd', { size => 40}));
$table->insert($table_rn,2,'Type','width=14%');
$sql = 'SELECT consumer_type_id, consumer_type_desc
          FROM consumer_type
      ORDER BY consumer_type_desc';
my $popType = $consumer->Popup('consumer_type_id', { sql => $sql, style => "font-size: 11px;"});
$popType->head('','Undefined');
$table->insert($table_rn,3, $popType);
$table_rn++;

$table->insert($table_rn,0,'Address Line 1');
$table->insert($table_rn,1, $consumer->Text('consumer_addr1', { size => 40}));
$table->insert($table_rn,2,'City');
$table->insert($table_rn,3, $consumer->Text('consumer_city', { size => 30}));
$table_rn++;

$table->insert($table_rn,0,'Address Line 2');
$table->insert($table_rn,1, $consumer->Text('consumer_addr2', { size => 40}));
$table->insert($table_rn,2,'State');
$sql = "SELECT state_cd, state_name
          FROM state
      ORDER BY state_name";
my $popState = $consumer->Popup('consumer_state_cd', { sql => $sql,
                                  id => "state"});
$popState->head(' ','Undefined');
$table->insert($table_rn,3,$popState);
$table_rn++;

$table->insert($table_rn,0,'County');
$table->insert($table_rn,1, $consumer->Text('consumer_county', { size => 40}));
$table->insert($table_rn,2,'Postal Code');
$table->insert($table_rn,3, $consumer->Text('consumer_postal_cd', { size => 30}));
$table_rn++;

$table->insert($table_rn,0,'Country');
$sql = "SELECT country_cd, country_name
          FROM country
      ORDER BY country_name";

my  $popLocation = $consumer->Popup('consumer_country_cd', { sql => $sql,
                   style => "font-size: 11px;",
                   onchange => "setTheValue('state',' ')",
                   id => "country"});
$popLocation->head('','Undefined');
$table->insert($table_rn,1, $popLocation);
$table->insert($table_rn,2,'Deleted');

$table->insert($table_rn,3, $consumer->consumer_cell_phone_num, 'colspan=3');
$table_rn++;

my $str;
$str = qq|<input type="submit" name="action" value="Save">
          <input type="submit" name="action" value="Delete" onClick="return confirmSubmit('Delete')">|;
$table->insert($table_rn,0, $str,'align=center colspan=4');

display $table;

print qq|
<input type="hidden" name="consumer_id" value="$consumer_id">
  <script LANGUAGE="JavaScript">
  <!--
function setTheValue(id,val){
    obj = document.getElementById(id);
    country = document.getElementById('country');
    if(country.value == 'US') {
       val = '$consumer_state_cd';
    }
    obj.value = val;
}
  	function confirmSubmit(action)
	{
		var agree;
		switch(action)
		{
			case 'Delete':
			{
				agree=confirm("To delete a consumer record you must first reassign all it's device to a different consumer.  If you have not done this already, click Cancel and do so now.\\n\\nContinue to delete $name?");
				break;
			}
		}
		if (agree)
			return true ;
		else
			return false ;
  	}
  // -->
  </script>|;

print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
$query->DESTROY(1);
