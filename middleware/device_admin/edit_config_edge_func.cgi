#!/usr/local/USAT/bin/perl

use strict;

use USAT::App::DeviceAdmin::Const qw(:globals );
use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::Util;
use Crypt::Rijndael_PP ':all';
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $file_id = $PARAM{file_id};

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $action = $PARAM{"myaction"};
my $new_file_name = $query->filter_trim_whitespace($PARAM{new_file_name});
my $new_file_type = $PARAM{new_file_type};

if(!$file_id) {
  print "Required Parameter Not Found: file_id\n";
  USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
  exit;
}

if($action eq 'Copy To' && !$new_file_name) {
  print "Required Parameter Not Found: new_file_name\n";
  USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
  exit;
}

my $old_config_file_hex = USAT::DeviceAdmin::Util::blob_select(undef, $file_id);
if(!$old_config_file_hex)
{
	print "Template data not found for file_id: $file_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $new_config_file = '';

my @default_config_array;

my $file_data = pack("H*", $old_config_file_hex);
$file_data =~ tr/\r//d;  # added to make sure it splits unix and windows data
chomp($file_data); # play safe remove any newline at the end of string

my @temp_line = split(/\n/, $file_data);
my $record_count = 0;
my $change_count = 0;
my $change_string = '';
foreach my $line ( @temp_line ) {
   my ($index, $value) = split(/=/, $line);
   my $key = 'cfg_'.$index;

   my $param_value = $PARAM{"cfg_$index"};

   if(defined $PARAM{$key} && $PARAM{$key} ne '') { # these are records from web page
      $new_config_file .= "$index=" . $param_value."\n";
      if($param_value ne $value) { # count changed records
         $change_count++;
         $change_string .= "$index=" . $param_value."\n";
      }
   } else { # add original data which is not in web page
      $new_config_file .= "$index=" . $value."\n";
   }
   $record_count++;
}

print "
<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">
 <tr>
  <td align=\"left\">
   <br>
   <center>
   <form>";
   
	print "
		<input type=button value=\"Return to Config Templates\" onClick=\"javascript:window.location = 'template_menu.cgi';\">";

print "
   </form>
   </center>
   <pre>
";

print "\n
Processing Configuration Template...\n
Processed $record_count records.\n
There were $change_count changes.\n";

if ($change_count > 0) {
	print "
Here are the changes you made:\n
$change_string";
}

if ($change_count > 0 || $action eq 'Copy To') {
	my $file_content = uc(unpack("H*", $new_config_file));
	my @result = USAT::DeviceAdmin::DBObj::File_transfer::edge_config_file_save($DATABASE, $action, $file_id, $new_file_name, $new_file_type, $file_content);
	print "
$result[1]\n
Operation status is $result[0].
";
}

print "
   </pre>
  </td>
 </tr>
 <tr>
  <td align=\"left\">
   <center>
   <form>
		<input type=button value=\"Return to Config Templates\" onClick=\"javascript:window.location = 'template_menu.cgi';\">
   </form>
   </center>
  </td>
 </tr>  
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
