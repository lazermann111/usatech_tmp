#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use warnings;
use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::POS::API::PTA::Util;
use Date::Calc;
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;

use constant DEBUG => 0;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

our $dbh;
&main();

sub main {
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
	$dbh = $DATABASE->{handle};
	
	if(DEBUG)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		print "<pre>\n";
		print Dumper(\%PARAM);
		print "</pre>\n";
	}

	### determine action ###
        my $action;
        if(length $PARAM{myaction} > 0 ) {
           $action = $PARAM{myaction};
        } elsif(length $PARAM{action} > 0) {
           $action = $PARAM{action};
        } else {
	       $action = printError("Required Parameter Not Found: myaction or action");
        }
	if ($action eq 'shift_pos_pta_priority')
	{
		### check input variables ###
		my ($callback_cgi, $device_id, $pos_pta_id, $priority_up);
		foreach my $req_param ("callback_cgi", "device_id", "pos_pta_id", "priority_up")
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print Dumper(eval "\$$req_param") if DEBUG;
		}
		
		### handle pos_pta_priority shift ###
		my $err;
		my $success = USAT::DeviceAdmin::Profile::PaymentType::shift_pos_pta_priority($dbh, $device_id, $pos_pta_id, $priority_up, \$err);
		my $direction = $priority_up ? 'up' : 'down';
		printError(qq{<br><h3>Error shifting Payment Type priority $direction</h3>If you are seeing this error, then you may have clicked back in your browser.<br><br><input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='/$callback_cgi?device_id=$device_id';"><p>}) unless $success;
		print CGI->redirect("/$callback_cgi?device_id=$device_id&tab=2");
		$dbh->disconnect;
		exit 0;
	}
	elsif ($action eq 'Add / Reactivate')
	{
		### print header ###
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;

		### check input variables ###
		my ($callback_cgi, $device_id, $payment_subtype_id, $pos_pta_duplicate_settings);
		foreach my $req_param ("callback_cgi", "device_id", "payment_subtype_id", "pos_pta_duplicate_settings")
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print Dumper(eval "\$$req_param") if DEBUG;
		}
		
		### load list of available new types for this POS -- make sure we don't insert unnecessarily ###
		my $err;
		my @payment_subtypes = USAT::DeviceAdmin::Profile::PaymentType::get_available_payment_subtypes($dbh, $device_id, \$err);
		printError("<br><br>$err<br><br>") if defined $err;
		printError(qq{<br><h3>No unique payment types available to be added at this time.</h3>If you are seeing this error, then you may have clicked back in your browser or accidently refreshed the Add New / Reactivate Payment Type completion page.<br><br><input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='/$callback_cgi?device_id=$device_id';"><p>}) unless scalar @payment_subtypes;

		### prepare for insert ###
		my ($pos_id);
		my $pos_stmt = $dbh->prepare(qq{
			SELECT pos_id
			FROM pss.pos
			WHERE device_id = :device_id
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$pos_stmt->bind_param(":device_id", $device_id);
		$pos_stmt->execute();
		my @pos_data = $pos_stmt->fetchrow_array();
		$pos_stmt->finish();
		printError("pos_id $pos_id not found!") unless @pos_data;		
		($pos_id) = @pos_data;
		print Dumper(\@pos_data) if DEBUG;
		
		### load defaults from previously disabled payment type (if exists) ###
		my ($pos_pta_encrypt_key, 
			$pos_pta_encrypt_key2,
			$payment_subtype_key_id,
			$pos_pta_device_serial_cd,
			$pos_pta_pin_req_yn_flag, 
			$pos_pta_regex, 
			$pos_pta_regex_bref,
			$authority_payment_mask_id,
			$pos_pta_passthru_allow_yn_flag,
			$pos_pta_pref_auth_amt,
			$pos_pta_pref_auth_amt_max,
            $currency_cd
		);
		my $pos_old_stmt = $dbh->prepare(qq{
			SELECT pos_pta_encrypt_key, 
				pos_pta_encrypt_key2,
				payment_subtype_key_id,
				pos_pta_device_serial_cd,
				pos_pta_pin_req_yn_flag, 
				pos_pta_regex, 
				pos_pta_regex_bref,
				authority_payment_mask_id,
				pos_pta_passthru_allow_yn_flag,
				pos_pta_pref_auth_amt,
				pos_pta_pref_auth_amt_max,
                currency_cd 
			FROM pss.pos_pta
			WHERE pos_pta_id = (
				SELECT MAX(pos_pta_id)
				FROM pss.pos_pta
				WHERE pos_id = :pos_id
				AND payment_subtype_id = :payment_subtype_id
			)
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$pos_old_stmt->bind_param(":pos_id", $pos_id);
		$pos_old_stmt->bind_param(":payment_subtype_id", $payment_subtype_id);
		$pos_old_stmt->execute();
		my @pos_old_data = $pos_old_stmt->fetchrow_array();
		$pos_old_stmt->finish();
		if ($pos_pta_duplicate_settings eq 'Y')
		{
			($pos_pta_encrypt_key, 
				$pos_pta_encrypt_key2,
				$payment_subtype_key_id,
				$pos_pta_device_serial_cd,
				$pos_pta_pin_req_yn_flag, 
				$pos_pta_regex, 
				$pos_pta_regex_bref,
				$authority_payment_mask_id,
				$pos_pta_passthru_allow_yn_flag,
				$pos_pta_pref_auth_amt,
				$pos_pta_pref_auth_amt_max,
                $currency_cd
			) = @pos_old_data;
			print Dumper(\@pos_old_data) if DEBUG;
		}
		
		my $c_pos_pta_id_nextval = $DATABASE->query(
			query	=> 'SELECT pss.seq_pos_pta_id.NEXTVAL FROM DUAL'
			,values => []
		);
		my $pos_pta_id = $c_pos_pta_id_nextval->[0]->[0];		

		### insert data ###
		my $insert_pos_pta_stmt = $dbh->prepare(q{
			INSERT INTO pss.pos_pta (
				pos_pta_id,
				pos_id,
				payment_subtype_id,
				pos_pta_encrypt_key,
				pos_pta_encrypt_key2,
				payment_subtype_key_id,
				pos_pta_device_serial_cd,
				pos_pta_pin_req_yn_flag,
				pos_pta_regex,
				pos_pta_regex_bref,
				authority_payment_mask_id,
				pos_pta_priority,
				pos_pta_passthru_allow_yn_flag,
				pos_pta_pref_auth_amt,
				pos_pta_pref_auth_amt_max,
                currency_cd
			) (
				SELECT :pos_pta_id,
					:pos_id,
					:payment_subtype_id,
					:pos_pta_encrypt_key,
					:pos_pta_encrypt_key2,
					:payment_subtype_key_id,
					:pos_pta_device_serial_cd,
					:pos_pta_pin_req_yn_flag,
					:pos_pta_regex,
					:pos_pta_regex_bref,
					:authority_payment_mask_id,
					NVL(MAX(pos_pta_priority), 0) + 1,
					:pos_pta_passthru_allow_yn_flag,
					:pos_pta_pref_auth_amt,
					:pos_pta_pref_auth_amt_max,
					:currency_cd
				FROM pss.pos_pta pp, pss.payment_subtype ps, pss.client_payment_type cpt
				WHERE ps.payment_subtype_id = pp.payment_subtype_id
				AND ps.client_payment_type_cd = cpt.client_payment_type_cd
				AND pos_id = :pos_id
				AND cpt.payment_entry_method_cd = (
					SELECT cptx.payment_entry_method_cd
					FROM pss.payment_subtype psx, pss.client_payment_type cptx
					WHERE psx.payment_subtype_id = :payment_subtype_id
						AND psx.client_payment_type_cd = cptx.client_payment_type_cd
				)
				AND (pp.pos_pta_deactivation_ts IS NULL OR pp.pos_pta_deactivation_ts > SYSDATE)
			)
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$insert_pos_pta_stmt->bind_param(":pos_pta_id", $pos_pta_id);
		$insert_pos_pta_stmt->bind_param(":pos_id", $pos_id);
		$insert_pos_pta_stmt->bind_param(":payment_subtype_id", $payment_subtype_id);
		$insert_pos_pta_stmt->bind_param(":pos_pta_encrypt_key", $pos_pta_encrypt_key);
		$insert_pos_pta_stmt->bind_param(":pos_pta_encrypt_key2", $pos_pta_encrypt_key2);
		$insert_pos_pta_stmt->bind_param(":payment_subtype_key_id", $payment_subtype_key_id);
		$insert_pos_pta_stmt->bind_param(":pos_pta_device_serial_cd", $pos_pta_device_serial_cd);
		$insert_pos_pta_stmt->bind_param(":pos_pta_pin_req_yn_flag", defined $pos_pta_pin_req_yn_flag && defined $pos_pta_pin_req_yn_flag ne '' ? $pos_pta_pin_req_yn_flag : 'N');
		$insert_pos_pta_stmt->bind_param(":pos_pta_regex", $pos_pta_regex);
		$insert_pos_pta_stmt->bind_param(":pos_pta_regex_bref", $pos_pta_regex_bref);
		$insert_pos_pta_stmt->bind_param(":authority_payment_mask_id", $authority_payment_mask_id);
		$insert_pos_pta_stmt->bind_param(":pos_pta_passthru_allow_yn_flag", defined $pos_pta_passthru_allow_yn_flag && defined $pos_pta_passthru_allow_yn_flag ne '' ? $pos_pta_passthru_allow_yn_flag : 'N');
		$insert_pos_pta_stmt->bind_param(":pos_pta_pref_auth_amt", $pos_pta_pref_auth_amt);
		$insert_pos_pta_stmt->bind_param(":pos_pta_pref_auth_amt_max", $pos_pta_pref_auth_amt_max);
		$insert_pos_pta_stmt->bind_param(":currency_cd", $currency_cd);
		$insert_pos_pta_stmt->execute() || printError("Database error inserting new pos_pta: $insert_pos_pta_stmt->errstr");
		$insert_pos_pta_stmt->finish();

		### check for host_id record for device ###
		my $hosts_created_count = USAT::POS::API::PTA::Util::create_default_hosts_if_needed($DATABASE, $device_id, \$err);
		printError("<br><br>$err<br><br>") if defined $err;

		### print success ###
		print <<"		EOHTML";
<table width="100%" border="1" cellpadding="2" cellspacing="0">
<form method="post" action="edit_pos_pta_settings.cgi">
<input type="hidden" name="callback_cgi" value="$callback_cgi">
<input type="hidden" name="device_id" value="$device_id">
<input type="hidden" name="pos_pta_id" value="$pos_pta_id">
 <tr>
  <th bgcolor="#C0C0C0">Point-of-Sale Payment Type - $action</th>
 </tr>
 <tr>
  <td align="center">
   Device payment type successfully 
		EOHTML
		if (scalar @pos_old_data > 0)
		{
			print "reactivated";
			print " (using old settings)" if $pos_pta_duplicate_settings eq 'Y';
			print "!";
		}
		else
		{
			print "added or reactivated";
		}
		print <<"		EOHTML";
	<p>
   <input type="submit" name="myaction" value="Edit Settings" onClick="return confirmSubmit();">&nbsp;
   <input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='/$callback_cgi?device_id=$device_id';">
  </td>
 </tr>
</form>
</table>
		EOHTML
	}
	elsif ($action eq 'Save')
	{
		### check input variables ###
		my ($callback_cgi, 
			$device_id, 
			$pos_pta_id,
			$pos_id,
			
			$pos_pta_device_serial_cd,
			$pos_pta_encrypt_key,
			$pos_pta_encrypt_key2,
			$pos_pta_encrypt_key_hex,
			$pos_pta_encrypt_key2_hex,
			$pos_pta_pin_req_yn_flag,
			$pos_pta_passthru_allow_yn_flag,

			$pos_pta_pref_auth_amt,
			$pos_pta_pref_auth_amt_max,
			
			$payment_subtype_id,
			$payment_subtype_key_id,
			
			$authority_payment_mask_id,
			$pos_pta_regex,
			$pos_pta_regex_bref,
            $currency_cd,
			
			$toggle_cd,
			$toggle_date,
			$toggle_month,
			$toggle_day,
			$toggle_year,
			$toggle_time,
			$toggle_ampm,
			$toggle_tzone,

			$original_payment_subtype_id,
			$original_payment_subtype_key_id,
			$original_authority_payment_mask_id,
			$original_pos_pta_regex,
			$original_pos_pta_regex_bref,
			$original_pos_pta_device_serial_cd,
			$original_pos_pta_encrypt_key,
			$original_pos_pta_encrypt_key2,
			$original_pos_pta_pin_req_yn_flag,
			$original_pos_pta_passthru_allow_yn_flag,
			$original_pos_pta_pref_auth_amt,
			$original_pos_pta_pref_auth_amt_max
		);
		foreach my $req_param (
			"callback_cgi", 
			"device_id", 
			"pos_pta_id",
			"pos_id",
			"pos_pta_pin_req_yn_flag",
			"payment_subtype_id",
			"payment_subtype_key_id",
			"pos_pta_regex",
			"toggle_cd",
			"pos_pta_passthru_allow_yn_flag",
            "currency_cd"
		)
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print Dumper(eval "\$$req_param") if DEBUG;
		}
		foreach my $nonreq_param (
			"pos_pta_device_serial_cd",
			"pos_pta_encrypt_key",
			"pos_pta_encrypt_key2",
			"pos_pta_encrypt_key_hex",
			"pos_pta_encrypt_key2_hex",
			"pos_pta_regex_bref", 
			"authority_payment_mask_id",
			"toggle_date", 
			"toggle_month",
			"toggle_day",
			"toggle_year",
			"toggle_time",
			"toggle_ampm",
			"toggle_tzone",
			"original_payment_subtype_id",
			"original_payment_subtype_key_id",
			"original_authority_payment_mask_id",
			"original_pos_pta_regex",
			"original_pos_pta_regex_bref",
			"original_pos_pta_device_serial_cd",
			"original_pos_pta_encrypt_key",
			"original_pos_pta_encrypt_key2",
			"original_pos_pta_pin_req_yn_flag",
			"original_pos_pta_passthru_allow_yn_flag",
			"pos_pta_pref_auth_amt",
			"original_pos_pta_pref_auth_amt",
			"pos_pta_pref_auth_amt_max",
			"original_pos_pta_pref_auth_amt_max",
            "currency_cd"
		)
		{
			eval "\$$nonreq_param = \$PARAM{$nonreq_param}";
			print Dumper(eval "\$$nonreq_param") if DEBUG;
		}
		
		### validate/parse input ###
		my $error_html = "";
		$error_html .= "<li>Encryption key '$pos_pta_encrypt_key' not a valid Hex string" if (defined $pos_pta_encrypt_key_hex && $pos_pta_encrypt_key_hex == 1 && $pos_pta_encrypt_key !~ m/^[a-zA-Z0-9]*$/);
		$error_html .= "<li>Encryption key 2 '$pos_pta_encrypt_key2' not a valid Hex string" if (defined $pos_pta_encrypt_key2_hex && $pos_pta_encrypt_key2_hex == 1 && $pos_pta_encrypt_key2 !~ m/^[a-zA-Z0-9]*$/);
		$error_html .= "<li>Invalid PIN required flag 'pos_pta_pin_req_yn_flag': must be Y or N" unless $pos_pta_pin_req_yn_flag =~ m/^[NY]$/;
		$error_html .= "<li>Invalid authority pass-through required flag 'pos_pta_passthru_allow_yn_flag': must be Y or N" unless $pos_pta_passthru_allow_yn_flag =~ m/^[NY]$/;
		$error_html .= "<li>Override Amount '$pos_pta_pref_auth_amt' not a valid number" if (length($pos_pta_pref_auth_amt) > 0 && $pos_pta_pref_auth_amt !~ m/^[0-9]+(\.[0-9]{0,2})?$/);
		$error_html .= "<li>Override Limit '$pos_pta_pref_auth_amt_max' not a valid number" if (length($pos_pta_pref_auth_amt_max) > 0 && $pos_pta_pref_auth_amt_max !~ m/^[0-9]+(\.[0-9]{0,2})?$/);
		$error_html .= "<li>Override amount can not be greater than limit! ($pos_pta_pref_auth_amt > $pos_pta_pref_auth_amt_max)" if (length($pos_pta_pref_auth_amt) > 0 && length($pos_pta_pref_auth_amt_max) > 0 && $pos_pta_pref_auth_amt > $pos_pta_pref_auth_amt_max);
		$error_html .= "<li>Override limit is required with override amount!" if (length($pos_pta_pref_auth_amt) > 0 && length($pos_pta_pref_auth_amt_max) <= 0);
		$error_html .= "<li>Override amount is required with override limit!" if (length($pos_pta_pref_auth_amt) <= 0 && length($pos_pta_pref_auth_amt_max) > 0);


		#validate regex?
		#validate regex bref?
		my $pos_pta_ts;
		if (lc $toggle_date eq "now")
		{
			$pos_pta_ts = "SYSDATE";
		}
		elsif (lc $toggle_date eq 'custom' && $toggle_cd != -1) #either timestamp editable
		{	
			my ($year,$month,$day, $hour,$min,$sec);
			eval { 
				($year,$month,$day, $hour,$min,$sec) = Date::Calc::Add_Delta_DHMS(
					$toggle_year,$toggle_month,$toggle_day, (
						$toggle_ampm eq 'PM' 
							? $toggle_time == 12 
								? $toggle_time 
								: $toggle_time + 12 
							: $toggle_time == 12 
								? 0 
								: $toggle_time
						),0,0, 
					0,$toggle_tzone,0,0
				) 
			};
			printError("$@") if $@;
			$pos_pta_ts = "TO_DATE('$month/$day/$year $hour\:$min', 'MM/DD/YYYY HH24:MI')";
		}
		printError("<ul>$error_html</ul>") if $error_html ne "";

		### determine authority_payment_mask settings, if applicable ###
		my ($pos_pta_regex_final, $pos_pta_regex_bref_final);
		if ($authority_payment_mask_id)
		{
			$pos_pta_regex_final = $pos_pta_regex_bref_final = '';

			my $c_get_payment_subtype_apm_id = $DATABASE->query(
				query => q{
					SELECT authority_payment_mask_id
					FROM pss.payment_subtype
					WHERE payment_subtype_id = ?
				},
				values => [
					$payment_subtype_id
				]
			);
			$authority_payment_mask_id = '' if (defined $c_get_payment_subtype_apm_id->[0]->[0] && $c_get_payment_subtype_apm_id->[0]->[0] == $authority_payment_mask_id);	#undef pp.authority_payment_mask_id
		}
		else
		{
			$pos_pta_regex_final = $pos_pta_regex;
			$pos_pta_regex_bref_final = $pos_pta_regex_bref;
		}
		
		### determine if we need to create new pos_pta (e.g if pos_pta is active, immutable property has been changed, and transactions exist) ###
		if ($toggle_cd > 0 || $toggle_cd == -1) {
			my $immutable_setting_change_occured = 0;
			if ($original_payment_subtype_id			ne $payment_subtype_id
				|| $original_payment_subtype_key_id		ne $payment_subtype_key_id
				|| $original_authority_payment_mask_id	ne $authority_payment_mask_id
				|| $original_pos_pta_regex				ne $pos_pta_regex_final
				|| $original_pos_pta_regex_bref			ne $pos_pta_regex_bref_final
				|| $original_pos_pta_device_serial_cd	ne $pos_pta_device_serial_cd
				|| $original_pos_pta_encrypt_key		ne $pos_pta_encrypt_key
				|| $original_pos_pta_encrypt_key2		ne $pos_pta_encrypt_key2
				|| $original_pos_pta_pin_req_yn_flag	ne $pos_pta_pin_req_yn_flag
				|| $original_pos_pta_pref_auth_amt		ne $pos_pta_pref_auth_amt
				|| $original_pos_pta_pref_auth_amt_max	ne $pos_pta_pref_auth_amt_max
				|| $original_pos_pta_passthru_allow_yn_flag ne $pos_pta_passthru_allow_yn_flag) {
				my $c_pos_pta_tran_exists = $DATABASE->query(
					query => q{
						SELECT COUNT(1) cnt
						FROM pss.tran
						WHERE pos_pta_id = ?
					},
					values => [
						$pos_pta_id
					]
				);
				$immutable_setting_change_occured = 1 if (defined $c_pos_pta_tran_exists->[0]->[0] && $c_pos_pta_tran_exists->[0]->[0] > 0);
			}

			if ($immutable_setting_change_occured) {
				my $c_pos_pta_deactivation_ts = $DATABASE->query(
					query	=> "SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS') FROM DUAL"
					,values => []
				);

				### deactivate old pos_pta ###
				my $c_deactivate_pos_pta = $DATABASE->do(
					query => q{
						UPDATE pss.pos_pta
						SET pos_pta_deactivation_ts = TO_DATE(?, 'MM-DD-YYYY HH24:MI:SS')
						WHERE pos_pta_id = ?
					},
					values => [
						$c_pos_pta_deactivation_ts->[0]->[0],
						$pos_pta_id
					]
				);
				printError("Database error updating pos_pta $pos_pta_id: ".$DATABASE->errstr) unless defined $c_deactivate_pos_pta && $c_deactivate_pos_pta > 0;
				
				### get settings from old pos_pta ###
				my $c_old_pos_pta_settings = $DATABASE->query(
					query => q{
						SELECT pos_id,
							payment_subtype_id,
							pos_pta_encrypt_key,
							pos_pta_encrypt_key2,
							payment_subtype_key_id,
							pos_pta_device_serial_cd,
							pos_pta_pin_req_yn_flag,
							pos_pta_regex,
							pos_pta_regex_bref,
							authority_payment_mask_id,
							pos_pta_passthru_allow_yn_flag,
							pos_pta_pref_auth_amt,
							pos_pta_pref_auth_amt_max,
                            currency_cd
						FROM pss.pos_pta
						WHERE pos_pta_id = ?
					},
					values => [
						$pos_pta_id
					]
				);
				printError("Database error loading data for pos_pta $pos_pta_id: ".$DATABASE->errstr) unless defined $c_old_pos_pta_settings && $c_old_pos_pta_settings > 0;
				
				### create and immediately activate new pos_pta ###
				my $c_pos_pta_id_nextval = $DATABASE->query(
					query	=> 'SELECT pss.seq_pos_pta_id.NEXTVAL FROM DUAL'
					,values => []
				);
				my $new_pos_pta_id = $c_pos_pta_id_nextval->[0]->[0];
				
				my $c_create_new_pos_pta = $DATABASE->do(
					query => q{
						INSERT INTO pos_pta (
							pos_pta_id,
							pos_id,
							payment_subtype_id,
							pos_pta_encrypt_key,
							pos_pta_encrypt_key2,
							payment_subtype_key_id,
							pos_pta_device_serial_cd,
							pos_pta_pin_req_yn_flag,
							pos_pta_regex,
							pos_pta_regex_bref,
							authority_payment_mask_id,
							pos_pta_passthru_allow_yn_flag,
							pos_pta_pref_auth_amt,
							pos_pta_pref_auth_amt_max,
							currency_cd,
							pos_pta_priority,
							pos_pta_activation_ts
						) (
							SELECT ?, 
								?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
								NVL(MAX(pos_pta_priority), 0) + 1,
								TO_DATE(?, 'MM-DD-YYYY HH24:MI:SS')
							FROM pss.pos_pta pp, pss.payment_subtype ps, pss.client_payment_type cpt
							WHERE ps.payment_subtype_id = pp.payment_subtype_id
							AND ps.client_payment_type_cd = cpt.client_payment_type_cd
							AND pos_id = ?
							AND cpt.payment_entry_method_cd = (
								SELECT cptx.payment_entry_method_cd
								FROM pss.payment_subtype psx, pss.client_payment_type cptx
								WHERE psx.payment_subtype_id = ?
									AND psx.client_payment_type_cd = cptx.client_payment_type_cd
							)
							AND (pp.pos_pta_deactivation_ts IS NULL OR pp.pos_pta_deactivation_ts > SYSDATE)
						)
					},
					values => [
						$new_pos_pta_id,
						@{$c_old_pos_pta_settings->[0]},
						$c_pos_pta_deactivation_ts->[0]->[0],
						$pos_id,
						$payment_subtype_id
					]
				);
				printError("Database error inserting pos_pta $new_pos_pta_id: ".$DATABASE->errstr) unless defined $c_create_new_pos_pta && $c_create_new_pos_pta > 0;

				### update priorities to correct values ###
				my $c_update_priorities = $DATABASE->do(
					query => q{
						UPDATE pss.pos_pta
						SET pos_pta_priority = DECODE(
							pos_pta_id,
							? /* old */, (
								SELECT pos_pta_priority
								FROM pss.pos_pta
								WHERE pos_pta_id = ? /* new */
							),
							? /* new */, (
								SELECT pos_pta_priority
								FROM pss.pos_pta
								WHERE pos_pta_id = ? /* old */
							)
						)
						WHERE pos_pta_id IN (?, ?)
					},
					values => [
						$pos_pta_id,
						$new_pos_pta_id,
						$new_pos_pta_id,
						$pos_pta_id,
						$pos_pta_id,
						$new_pos_pta_id
					]
				);
				printError("Database error updating pos_pta $pos_pta_id, $new_pos_pta_id: ".$DATABASE->errstr) unless defined $c_update_priorities && $c_update_priorities > 0;

				$pos_pta_id = $new_pos_pta_id;	#pos_pta we want to update has changed
			}
		}
		
		### update data ###
		my $result;
		my $pos_pta_update_stmt = $dbh->prepare(q{
			UPDATE pos_pta
			SET pos_pta_device_serial_cd = :pos_pta_device_serial_cd,
				pos_pta_encrypt_key = :pos_pta_encrypt_key,
				pos_pta_encrypt_key2 = :pos_pta_encrypt_key2,
		}.(defined $toggle_date && $toggle_date ne ''
			? (($toggle_cd < -1 || $toggle_cd == 0) 
				? qq{ pos_pta_activation_ts = $pos_pta_ts, }	#activation editable
				: ($toggle_cd > 0 || $toggle_cd == -1)
					? qq{ pos_pta_deactivation_ts = $pos_pta_ts, }	#deactivation editable
					: ''	#none editable
			)
			: ''
		).q{	payment_subtype_key_id = :payment_subtype_key_id,
				pos_pta_pin_req_yn_flag = :pos_pta_pin_req_yn_flag, 
				pos_pta_regex = :pos_pta_regex, 
				pos_pta_regex_bref = :pos_pta_regex_bref,
				authority_payment_mask_id = :authority_payment_mask_id,
				pos_pta_passthru_allow_yn_flag = :pos_pta_passthru_allow_yn_flag,
				pos_pta_pref_auth_amt = :pos_pta_pref_auth_amt,
				pos_pta_pref_auth_amt_max = :pos_pta_pref_auth_amt_max,
                currency_cd = :currency_cd
			WHERE pos_pta_id = :pos_pta_id
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$pos_pta_update_stmt->bind_param(":pos_pta_device_serial_cd", $pos_pta_device_serial_cd);
		$pos_pta_update_stmt->bind_param(":pos_pta_encrypt_key", $pos_pta_encrypt_key ne '' ? defined $pos_pta_encrypt_key_hex && $pos_pta_encrypt_key_hex == 1 ? $pos_pta_encrypt_key : unpack("H*", pack("A*", $pos_pta_encrypt_key)) : $pos_pta_encrypt_key);
		$pos_pta_update_stmt->bind_param(":pos_pta_encrypt_key2", $pos_pta_encrypt_key2 ne '' ? defined $pos_pta_encrypt_key2_hex && $pos_pta_encrypt_key2_hex == 1 ? $pos_pta_encrypt_key2 : unpack("H*", pack("A*", $pos_pta_encrypt_key2)) : $pos_pta_encrypt_key2);
		$pos_pta_update_stmt->bind_param(":payment_subtype_key_id", $payment_subtype_key_id eq 'N/A' ? undef : $payment_subtype_key_id);
		$pos_pta_update_stmt->bind_param(":pos_pta_pin_req_yn_flag", $pos_pta_pin_req_yn_flag);
		$pos_pta_update_stmt->bind_param(":pos_pta_regex", $pos_pta_regex_final);
		$pos_pta_update_stmt->bind_param(":pos_pta_regex_bref", $pos_pta_regex_bref_final);
		$pos_pta_update_stmt->bind_param(":authority_payment_mask_id", $authority_payment_mask_id);
		$pos_pta_update_stmt->bind_param(":pos_pta_passthru_allow_yn_flag", $pos_pta_passthru_allow_yn_flag);
		$pos_pta_update_stmt->bind_param(":pos_pta_pref_auth_amt", $pos_pta_pref_auth_amt);
		$pos_pta_update_stmt->bind_param(":pos_pta_pref_auth_amt_max", $pos_pta_pref_auth_amt_max);
		$pos_pta_update_stmt->bind_param(":currency_cd", $currency_cd);
		$pos_pta_update_stmt->bind_param(":pos_pta_id", $pos_pta_id);
		$result = $pos_pta_update_stmt->execute();
		$pos_pta_update_stmt->finish();
		printError("update unsuccessful!") unless defined $result;		

		print CGI->redirect("/$callback_cgi?device_id=$device_id&tab=2");

		$dbh->disconnect;
		exit 0;
	}
	elsif ($action eq 'Import')
	{
		### check input variables ###
		
		my ($callback_cgi, $device_id, $pos_pta_tmpl_id, $mode_cd);
		foreach my $req_param ("callback_cgi", "device_id", "pos_pta_tmpl_id", "mode_cd")
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print Dumper(eval "\$$req_param") if DEBUG;
		}
		
		### handle pos_pta_priority shift ###
		my $err;
		my $success = USAT::POS::API::PTA::Util::import_template($DATABASE, $device_id, $pos_pta_tmpl_id, $mode_cd, \$err);
		printError(qq{<br>Error importing pos_pta_tmpl $pos_pta_tmpl_id, mode $mode_cd: $err}) unless $success;
		
		$dbh->disconnect;
		print CGI->redirect("/$callback_cgi?device_id=$device_id&tab=2");
		exit 0;
	}
	else
	{
		printError("Unknown myaction or action Parameter: $action");
	}

	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub printError ($) {
	my $err_txt = shift;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	print "$err_txt\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}
