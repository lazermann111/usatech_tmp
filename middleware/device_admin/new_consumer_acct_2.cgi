#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %params = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

if($params{'consumer_acct_fmt_id'} == 1) # maintenance card - need additional info for actions
{
		print q|<form method="post" action="new_consumer_acct_2a.cgi">|;	
}
else
{
	print q|<form method="post" action="new_consumer_acct_3.cgi">|;
}

my $merchant_sql = q|
	select merchant.merchant_id, merchant.merchant_name, merchant.merchant_desc
	from merchant, customer_merchant 
	where merchant.merchant_id = customer_merchant.merchant_id
	and customer_merchant.customer_id = ? 
	and merchant.authority_id = ?|;
	
my $authority_sql = q|
	select authority.authority_name, authority_type.authority_type_name, authority_type.authority_type_desc
	from authority.authority, authority_type
	where authority.authority_type_id = authority_type.authority_type_id
	and authority.authority_id = ?|;

my $rc = 0;	
my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0"');
$table->put($rc++,0, B('USAT Card Creation Wizard'),'align=center bgcolor=#C0C0C0 colspan=2');

my $merchant_query = OOCGI::Query->new($merchant_sql, undef, [$params{'customer_id'}, $params{'authority_id'}]);
if ($merchant_query->row_count > 0)
{
	$table->put($rc++,0, q|The following merchants already exist for this customer.  Please select an existing merchant.|, 'align=center colspan=2');
	
	if(my %hash = $merchant_query->next_hash) 
	{
		$table->put($rc,0, qq|<input type="radio" checked name="merchant_id" value="$hash{merchant_id}">|);
		$table->put($rc++,1,"$hash{merchant_name}<br>");
	}
}
else
{
	$table->put($rc++,0, q|Please create a new merchant and terminal below.|, 'align=center colspan=2');
	
	my $customer = USAT::DeviceAdmin::DBObj::Customer->new($params{'customer_id'});
	
	my $authority_query = OOCGI::Query->new($authority_sql, undef, $params{'authority_id'});
	my %authority_hash = $authority_query->next_hash;
	my $authorty_name = $authority_hash{authority_type_name};
	
	my $merchant_name = $customer->customer_name . ' - ' . $authorty_name . ' Merchant';
	my $terminal_desc = $customer->customer_name . ' - ' . $authorty_name . ' Terminal';
	
	my $merch_name_text = OOCGI::Text->new(id => 'merchant_name', name => 'merchant_name', size => 60);
	my $merch_desc_text = OOCGI::Text->new(id => 'merchant_desc', name => 'merchant_desc', size => 60);
	my $terminal_desc_text = OOCGI::Text->new(id => 'terminal_desc', name => 'terminal_desc', size => 60);

	$merch_name_text->default($merchant_name);
	$merch_desc_text->default($merchant_name);
	$terminal_desc_text->default($terminal_desc);
	
	$table->put($rc,0,"Merchant Name");
	$table->put($rc++,1,$merch_name_text);
	
	$table->put($rc,0,"Merchant Description");
	$table->put($rc++,1,$merch_desc_text);

	$table->put($rc,0,"Terminal Description");
	$table->put($rc++,1,$terminal_desc_text);

}

display $table;

HIDDEN_PASS('consumer_acct_fmt_id', 'customer_id', 'location_id', 'authority_id');

print "&nbsp<br>" . SUBMIT('action','Next >') . "</form><br>&nbsp;";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");


