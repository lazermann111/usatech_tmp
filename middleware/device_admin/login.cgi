#!/usr/local/USAT/bin/perl

use strict;
use warnings;

use USAT::App::DeviceAdmin::Config qw(:globals );
use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::Query;
use USAT::DeviceAdmin::DBObj::Da_session;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Database;

use Net::LDAP;
use Net::LDAPS;
use Net::LDAP::Util qw(
    ldap_explode_dn
    escape_filter_value
    escape_dn_value
);
use Data::Dumper;

my $user_attr      = 'sAMAccountName';
my $member_of_attr = 'memberOf';
my $member_attr    = 'member';
#my $base           = 'DC=' . DA_SESSION__LDAP__HOST . ',DC=com';
my $base           = DA_SESSION__LDAP__BASE;
my $group_base     = 'OU='.STR_DA__DEVICE_ADMIN.',OU='.STR_DA__SECURITY_GROUPS.",$base";

my $query = OOCGI::OOCGI->new;
my %PARAM = $query->Vars();
my $referer = $PARAM{referer};
my $error = q{};

if($PARAM{userOP} and $PARAM{userOP} eq 'Login') {
    # connect to directory and bind with the entered credentials
    my $ldap = Net::LDAPS->new(
        DA_SESSION__LDAP__SYSTEM_NAME,
        port    => DA_SESSION__PORT, 
        version => DA_SESSION__LDAP_VERSION,
    ) or croak $@;
    my $mesg = $ldap->bind(
        DA_SESSION__DOMAIN . "\\" . escape_dn_value( $PARAM{user_name} ),
        password => $PARAM{password},
    );

   if($mesg->code) {
        # Not a valid user - print error and display login again
      $error .= B(DA_SESSION__AUTHENTICATION_ERROR, 'red');
    }
    else {
     # get the groups of which the user is a direct member
     my @user_group_DNs = grep /$group_base$/, $ldap->search(
         base      => $base,
         filter    => "($user_attr="
                    . escape_filter_value( $PARAM{user_name} )
                    . ')',
         deref     => 'search',
         scope     => 'sub',
         attrs     => [$member_of_attr],
         sizelimit => 1,
     )->shift_entry->get_value($member_of_attr);

     # get all the groups related to Device Admin and their members
     my %groups_struct = %{$ldap->search(
         base   => $group_base,
         filter => '(CN=*)',
         deref  => 'search',
         scope  => 'sub',
         attrs  => [$member_attr],
     )->as_struct};
     my %app_groups = map { $_ => $groups_struct{$_}->{$member_attr} }
                      keys %groups_struct;

     # check if the user's groups are members of other groups
     @user_group_DNs = get_members(\%app_groups, @user_group_DNs);                 

     # get the common names for all groups collected
     my  @user_group_CNs = map  { values %$_          }
                         grep { exists $_->{CN}     }
                         map @{ ldap_explode_dn($_) }, @user_group_DNs;

     my %cn_membership = map {$_ => 1} @user_group_CNs;

        if ( !exists $cn_membership{+STR_DA__USERS} ) {
            # error if the user isn't in the Device Admin group at all
            $error .= B(DA_SESSION__AUTHORIZATION_ERROR, 'red');
        }
        else {
          # connect to database and begin session
          my $DATABASE = USAT::Database->new(
             PrintError => 1,
             RaiseError => 1,
             AutoCommit => 1,
          );

          #    kludge: patch for Oracle 10.1.0.x session default date format bug,
          # causing occasional ORA-01801 errors on some views and PL/SQL
             $DATABASE->do(
                 query=>q{ALTER SESSION SET NLS_DATE_FORMAT='dd-mon-yy'}
             ) if $DATABASE->{driver} =~ /Oracle$/;

          my $dbh = $DATABASE->{handle};
          OOCGI::Query->open_db($dbh);

          my $session;
          my $session1 = USAT::DeviceAdmin::DBObj::Da_session->new(undef, 0);
          my $session2 = USAT::DeviceAdmin::DBObj::Da_session->load($session1->ID);
          $session = defined $session2 ? $session2 : $session1;
	  
          # set up callbacks to set session values based on group membership
          # use , instead of => because keys are constants
          my %privileges_for = (
             STR_DA__POSM_ADMINS,
                 sub {$session->posm(DA__POSM_ADMINS)},
             STR_DA__GPRS_WEB_ADMINS,
                 sub {$session->gprs(DA__GPRS_WEB_ADMINS)},
             STR_DA__MENU_CONTENT_ADMINS,
                 sub {$session->js_refresh(DA__MENU_CONTENT_ADMINS)},
             STR_DA__CONFIG_TEMPLATE_ADMINS,
                 sub {$session->config_template(DA__CONFIG_TEMPLATE_ADMINS)},
             STR_DA__DEVICE_CLONING_ADMINS,
                 sub {$session->device_cloning(DA__DEVICE_CLONING_ADMINS)},
             STR_DA__FILE_ADMINS,
                 sub {$session->file_admins(DA__FILE_ADMINS)},
             STR_DA__DEVICE_PROFILE_ADMINS,
                 sub {$session->profile_admins(DA__DEVICE_PROFILE_ADMINS)},
             STR_DA__CONSUMER_ACCOUNT_ADMINS,
                 sub {$session->consumer_account(DA__CONSUMER_ACCOUNT_ADMINS)},
             STR_DA__CUSTOMER_ADMINS,
                 sub {$session->customer_admins(DA__CUSTOMER_ADMINS)},
             STR_DA__LOCATION_ADMINS,
                 sub {$session->location_admins(DA__LOCATION_ADMINS)},
             STR_DA__CONSUMER_ACCOUNT_ADMINS,
                 sub {$session->consumer_account(DA__CONSUMER_ACCOUNT_ADMINS)},
             STR_DA__BULK_DEVICE_CONFIGURATION_ADMINS,
                 sub {$session->bulk_device_configuration(DA__BULK_DEVICE_CONFIGURATION_ADMINS)},
             STR_DA__BULK_CARD_CONFIGURATION_ADMINS,
                 sub {$session->bulk_card_configuration(DA__BULK_CARD_CONFIGURATION_ADMINS)},
             STR_DA__SERIAL_NUMBER_ALLOCATION_ADMINS,
                 sub {$session->serial_number_allocation(DA__SERIAL_NUMBER_ALLOCATION_ADMINS)},
             STR_DA__ESUDS_WEB_PRIVILEGE_ADMINS,
                 sub {$session->esuds_web_privilege(DA__ESUDS_WEB_PRIVILEGE_ADMINS)},
         );
         # run callbacks for each group
         # &{ $privileges_for{$_} } for keys %cn_membership;
         foreach my $key ( keys %cn_membership) {
            if($privileges_for{$key}) { 
               # run anon sub.
               &{ $privileges_for{$key} };
            }
         }


         # write user name to session
         # redirect to index.cgi
         $session->user_name($PARAM{user_name});
         $session->update();	#unlock as soon as possible
         $session->unlock();	#unlock as soon as possible

         if (defined $referer and 
			$referer ne '' and
            $referer !~ '/login.cgi'
         ) {
            print CGI->redirect($referer);
                exit;
         }

#        Carp::carp "RRR 3 ".$ENV{'HTTP_REFERER'}.'|'.$referer."|\n";
         print CGI->redirect("/index.cgi?index1=2");
         exit;
	  }
   }
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile('header.html');

if($PARAM{expire_cookie}) {
    print <<'END_HTML';
<script>
// this fixes an issue with the old method, ambiguous values 
// with this test document.cookie.indexOf( name + "=" );
function Get_Cookie( check_name ) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f
	
	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );
		
		
		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
	
		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found )
	{
		return null;
	}
}				
// this deletes the cookie when called
function Delete_Cookie( name, path, domain ) {
if ( Get_Cookie( name ) ) document.cookie = name + "=" +
( ( path ) ? ";path=" + path : "") +
( ( domain ) ? ";domain=" + domain : "" ) +
";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}
Delete_Cookie('usat_da_session', '/', '');
</script>
END_HTML
}
print <<'END_HTML';
<script>
    document.getElementById('user_name_id').innerHTML = '<font color="green" size="3"><b>Login</font></b>';
</script>
END_HTML
my $table_rn = 0;

my $table = OOCGI::NTable->new('width=30%');

$table->put($table_rn, 0, B('USATech Device Admin'),'colspan=2 align=center');
$table_rn++;
if($error) {
   $table->put($table_rn, 0, $error,'colspan=2 align=center');
   $table_rn++;
}
$table->put($table_rn, 0, B('User Name '));
my $txtUserName = OOCGI::Text->new(name => 'user_name', sz_ml => [20,30]);
$table->put($table_rn, 1, $txtUserName);
$table_rn++;
$table->put($table_rn, 0, B('Password '));
my $txtPassword = OOCGI::Password->new(name => 'password', sz_ml => [20,30]);
$table->put($table_rn, 1, $txtPassword);
$table_rn++;
$table->put($table_rn, 0, SUBMIT('userOP', 'Login'), 'colspan=2 align=center');

if (!defined $referer || $referer eq '') {
    $referer = $ENV{QUERY_STRING};
}

print qq|<form method="post">|;

HIDDEN('referer', $referer);

display $table;

print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter('footer.html');

sub get_members {
    # given a reference to a hash of lists and another list, return a list
    # of all members that are either in or descendants of the second list
    my ($hol_ref, @current) = @_;
    my %all_members = map {$_ => 1} @current; # use hash keys to avoid dupes
    for my $member (@current) {
        for my $parent (grep defined $hol_ref->{$_}, keys %$hol_ref) {
            $all_members{$parent} = 1
                if grep $_ eq $member, @{ $hol_ref->{$parent} };
        }
    }
    while (keys %all_members > @current) {
        # found more list members than we had before, so recurse
        @current = get_members($hol_ref, keys %all_members);
    }
    return @current;
}
