#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::DAHeader;

require Text::CSV;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print '<br><br><font color="red">Undefined Action!</font><br><br>';
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print '<br><font color="red">No devices selected!</font><br><br>';
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};

my $pos_pta_tmpl_id = $PARAM{"pos_pta_tmpl_id"};
my $mode_cd = $PARAM{"mode_cd"};

my @devices_array = split /,/, $device_ids_str;

my $debug = $PARAM{"debug"};
if(length($debug) == 0)
{
	$debug = 0;
}

print qq{
<table cellpadding="3" border="1" cellspacing="0" width="100%">

 <form method="post" action="bulk_config_wizard_8_edge.cgi" onSubmit="javascript:return confirm('Please click OK to continue, and wait while your changes are processed!\\n\\nYou will receive a popup confirmation of the update results when completed.');">};

HIDDEN_PASS('customer_id',        'location_id', 'parent_location_id', 
            'include_device_ids', 'debug',       'mode_cd',
            'pos_pta_tmpl_id',    'config_string', 'file_name');

print qq{
 <tr>
  <th align="center" colspan="3" bgcolor="#C0C0C0">Device Configuration Wizard - Page 8: Confirm Changes</th>
 </tr>

 <tr>
  <td style="font-size: 14pt;"><br>};  
  
if($debug)
{
	print "<center><font color=\"red\"><b>----- DEBUG IS ON!  CHANGES WILL NOT BE SAVED! -----</b></font></center><br>\n";
}

print qq{
  Please confirm that you want to make the following changes!<br>
  <br>
  Change Customer and Location:<br>
  <table border="1" cellspacing="0" cellpadding="2">
   <tr><th style="font-size: 10pt;">Customer</th><th style="font-size: 10pt;">Location</th><th style="font-size: 10pt;">Parent Location</th></tr>
   <tr>
};
   
if(length($customer_id) > 0)
{
    my $customer = USAT::DeviceAdmin::DBObj::Customer->new($PARAM{customer_id});
	print '<td style="font-size: 10pt;">'.$customer->customer_name.'</td>';
} else {
	print '<td style="font-size: 10pt;">Do not change</td>';
}

if(length($location_id) > 0)
{
    my $location = USAT::DeviceAdmin::DBObj::Location->new($PARAM{location_id});
    print '<td style="font-size: 10pt;">'.$location->name_city_state_country.'</td>';
} else {
	print '<td style="font-size: 10pt;">Do not change</td>';
}

if(length($PARAM{parent_location_id}) > 0)
{
    my $location = USAT::DeviceAdmin::DBObj::Location->new($PARAM{parent_location_id});
    print '<td style="font-size: 10pt;">'.$location->name_city_state_country.'</td>';
} else {
    print "<td style=\"font-size: 10pt;\">Do not change</td>";
}

my $template_name;

if(length($PARAM{pos_pta_tmpl_id}) >= 0)
{
    my $ppt = USAT::DeviceAdmin::DBObj::Pos_pta_tmpl->new($PARAM{pos_pta_tmpl_id});
	$template_name = $ppt->pos_pta_tmpl_name;
}

print qq{</tr></table>
  <br>
  Import Payment Template:
  <table border="1" cellspacing="0" cellpadding="2">
   <tr><th style="font-size: 10pt;">Template Name</th><th style="font-size: 10pt;">Import Mode</th></tr>
};

if(length($mode_cd) && length($pos_pta_tmpl_id) > 0)
{
	print qq{<tr><td style="font-size: 10pt;">$template_name</td><td style="font-size: 10pt;">$mode_cd</td></tr>\n};
}
else
{
	print qq{<tr><td style="font-size: 10pt;" colspan="2">Do Not Import</thd></tr>\n};
}

my $table_config_params = OOCGI::NTable->new('align=""left" border="1" cellspacing="2" cellpadding="2"');
my $rowno3 = 0;

$table_config_params->put($rowno3,0,B('parameter=value',-1));
$rowno3++;

print qq{</tr></table>
  <br>
  Update Configuration Parameters:
};

if(length($PARAM{config_string}) == 0)
{
    $table_config_params->put($rowno3,0,'No Configuration Changes');
} else {
    my $config_string = $PARAM{config_string};
    $config_string =~ s/\n/<br>/g;
    $table_config_params->put($rowno3,0,$config_string);
}

display $table_config_params;

BR;

my $table_devices = OOCGI::NTable->new('align=""left" border="1" cellspacing="2" cellpadding="2"');
my $rowno4 = 0;
$table_devices->put($rowno4,0,B('EV Number',-1));
$table_devices->put($rowno4,1,B('Serial Number',-1));
$table_devices->put($rowno4,2,B('Customer',-1));
$table_devices->put($rowno4,3,B('Location',-1));
$table_devices->put($rowno4,4,B('Firmware Version',-1));
$rowno4++;

print " Changes will be done for all of the following devices:<br>";

foreach my $device_id (@devices_array)
{
    my $sql =q{SELECT device.device_id,
                      device.device_name,
                      device.device_serial_cd,
                      to_char(device.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'),
                      location.location_name,
                      customer.customer_name,
                      device_setting.device_setting_value
                 FROM device,
                      device_type,
                      pos,
                      location.location,
                      customer,
                      device_setting
                WHERE device.device_type_id = device_type.device_type_id
                  AND device.device_id = pos.device_id
                  AND pos.location_id  = location.location_id
                  AND pos.customer_id  = customer.customer_id
                  AND device.device_id = device_setting.device_id(+)
                  AND device_setting.device_setting_parameter_cd(+) = 'Firmware Version'
                  AND device.device_id = ? };
    my $qo = OOCGI::Query->new($sql, { bind => $device_id });

   if(my @data = $qo->next_array) {
      $table_devices->put($rowno4,0,$data[1]);
      $table_devices->put($rowno4,1,$data[2]);
      $table_devices->put($rowno4,2,$data[5]);
      $table_devices->put($rowno4,3,$data[4]);
      $table_devices->put($rowno4,4,$data[6]);
      $rowno4++;
   }
}
display $table_devices;

BR;

if($debug)
{
	print qq{<center><font color="red"><b>----- DEBUG IS ON!  CHANGES WILL NOT BE SAVED! -----</b></font></center><br>\n};
}

print qq{
  </td
 </tr>
</table>
<table cellspacing="0" cellpadding="2" border="1" width="100%">
 <tr>
  <td align="center" bgcolor="#C0C0C0">
   <input type="button" value="< Back" onClick="javascript:history.go(-1);">
   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
   <input type="submit" name="action" value="Finish >">
  </td>
 </tr>
</form>
</table>
};

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
