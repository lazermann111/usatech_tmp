#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $merchant_id = $PARAM{"merchant_id"};
my $merchant_info_stmt = $dbh->prepare(
q{
	select merchant.merchant_id, merchant.merchant_cd, merchant.merchant_name, merchant.merchant_desc, merchant.merchant_bus_name, 
	merchant.authority_id, to_char(merchant.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(merchant.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	authority.authority_id, authority.authority_name
	from merchant, authority.authority
	where merchant.authority_id = authority.authority_id
	and merchant.merchant_id = :merchant_id
});
$merchant_info_stmt->bind_param(":merchant_id", $merchant_id);
$merchant_info_stmt->execute();
my @merchant_info = $merchant_info_stmt->fetchrow_array();
$merchant_info_stmt->finish();

print "
<table cellspacing=\"0\" cellpadding=\"2\" border=1 width=\"100%\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Merchant</th>
 </tr>
 <tr>
  <td>ID</td><td>$merchant_info[0]</td>
  <td>Code</td><td>$merchant_info[1]</td>
 </tr>
 <tr>
  <td>Name</td><td>$merchant_info[2]</td>
  <td>Description</td><td>$merchant_info[3]</td>
 </tr>
 <tr>
  <td>Business Name</td><td>$merchant_info[4]</td>
  <td colspan=\"2\">&nbsp</td>
 </tr>
 <tr>
  <td>Created</td><td>$merchant_info[6]</td>
  <td>Last Updated</td><td>$merchant_info[7]</td>
 </tr>
 <tr>
  <td>Authority</td><td><a href=\"authority.cgi?authority_id=$merchant_info[8]\">$merchant_info[9]</a></td>
  <td colspan=\"2\">&nbsp</td>
 </tr>
</table>
";

my $terminal_list_stmt = $dbh->prepare(
q{
	select terminal.terminal_id, terminal.terminal_cd, terminal.terminal_encrypt_key, terminal.terminal_encrypt_key2, 
	terminal.terminal_next_batch_num, terminal.merchant_id, terminal.terminal_desc, terminal_state.terminal_state_name, 
	terminal.terminal_max_batch_num, terminal.terminal_batch_max_tran, terminal.terminal_batch_cycle_num, terminal.created_ts, 
	terminal.last_updated_ts 
	from terminal, terminal_state 
	where terminal.terminal_state_id = terminal_state.terminal_state_id 
	and terminal.merchant_id = :merchant_id 
	order by terminal.terminal_cd
});
$terminal_list_stmt->bind_param(":merchant_id", $merchant_id);
$terminal_list_stmt->execute();
my @terminal_info;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"6\" bgcolor=\"#C0C0C0\">Terminals</th>
 </tr>
 <tr>
  <th>ID</th>
  <th>Code</th>
  <th>Name</th>
  <th>Next Batch Num</th>
  <th>Current State</th>
 </tr>
";

while(@terminal_info = $terminal_list_stmt->fetchrow_array())
{
	print "
	     <tr>
    	 <td>$terminal_info[0]</td>
	     <td><a href=\"terminal.cgi?terminal_id=$terminal_info[0]\">$terminal_info[1]</a></td>
	     <td>$terminal_info[6]</td>
	     <td>$terminal_info[4]</td>
	     <td>$terminal_info[7]</td>
    	</tr>
	";
}

$terminal_list_stmt->finish();

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
