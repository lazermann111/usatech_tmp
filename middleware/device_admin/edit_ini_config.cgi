#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::DBObj;
use URI;
use USAT::DeviceAdmin::Config::Form;
use USAT::DeviceAdmin::Config::Form::Object qw(EDITOR_BASIC EDITOR_ADVANCED);	#just to import EDITOR_* constants
use USAT::DeviceAdmin::Config::Form::Object::TextField qw(RESTRICTION_NUMERIC RESTRICTION_REQUIRED);	#just to import RESTRICTION_* constants
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::Util;
use Crypt::Rijndael_PP ':all';

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

sub main () {
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
	print $user_menu;

	my $file_name = $PARAM{"file_name"};
	my $device_name = $PARAM{"device_name"};

	if(length($file_name) == 0 || length($device_name) == 0 )
	{
		print "Required parameters not found: file_name, device_name";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}

	my ($device_id, $device_serial_cd, $device_type_id) = @{USAT::DeviceAdmin::Util::get_device_info_by_name(undef, $device_name)};
	if (!$device_id) {
		print "Device $device_name not found";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
       	exit;
	}

	my $PUBLIC_PC = 1;
	my $device_subtype = 0;

	my $action    = $PARAM{"myaction"};
	my $edit_mode = $PARAM{"edit_mode"};
	my $max_param_code_len = 60;
	my $max_param_value_len = 200;

	my @config_array;
	if ($edit_mode ne 'raw') {
		local $@;
		eval { @config_array = USAT::DeviceAdmin::Config::Form::load_device_type($device_type_id); };
		if ($@) {
			print "Error: $@";
			USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
			exit;
		}
	}

	my $title = "$action Configuration - $device_serial_cd - $file_name";

	if (length($edit_mode) > 0)
	{
		$title = "$title ($edit_mode)";

		if ($edit_mode ne 'raw')
		{
			$title = "$title, <font color=\"red\">*</font><font size=\"-1\"> denotes required parameter</font>";
		}
	}

	my $file_data = USAT::DeviceAdmin::Util::blob_select_by_name(undef, $file_name);

	if(not defined $file_data)
	{
		print "$file_name not found";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}

	my $file_data_hex = uc($file_data);
	my $file_data_len = length($file_data_hex);

	# remove last CR LF
	if (substr($file_data_hex, $file_data_len - 4, 4) eq '0D0A')
	{
		$file_data_hex = substr($file_data_hex, 0, $file_data_len - 4);
	}

	$file_data = pack("H*", $file_data_hex);
	print "
	<script language=\"javascript\">
	<!--
	function ValidateForm(f)
	{	
		for (i=0; i<f.elements.length; i++) 
		{
			if (f.elements[i].name.substring(0, 4) == 'cfg_')
			{
				f.elements[i].value = f.elements[i].value.replace(/^\\s*|\\s*\$/g,'');

				if (f.elements[i].className == '".RESTRICTION_REQUIRED."' && f.elements[i].value == '')
				{
					alert('Please enter a value for the required parameter ' + f.elements[i].name.substring(4));
					return false;
				}

				if (f.elements[i].className == '".RESTRICTION_NUMERIC."' && (f.elements[i].value == '' || isNaN(f.elements[i].value)))
				{				
					alert('Please enter a numeric value for the required parameter ' + f.elements[i].name.substring(4));
					return false;
				}
			}
		}
		return true;
	}

	function doSubmit(myform, actionValue)
	{
		if (ValidateForm(myform) == false) return;
		myform.myaction.value = actionValue;
		myform.submit();
	}
	// -->
	</script>

	<table cellpadding=\"3\" border=\"1\" cellspacing=\"0\" width=\"100%\">
	 <tr>
	  <th align=\"center\" colspan=\"2\" bgcolor=\"#C0C0C0\">$title</th>
	 </tr>

	 <form method=post action=\"edit_ini_config_func.cgi\">
		<input type=\"hidden\" name=\"edit_mode\" value=\"$edit_mode\">
		<input type=\"hidden\" name=\"device_name\" value=\"$device_name\">
		<input type=\"hidden\" name=\"myaction\" value=\"\">";

	print "
		 <input type=\"hidden\" name=\"old_ini_file_hex\" value=\"";

	if ($action ne 'Import')
	{
		print $file_data_hex;
	}

	print "\">";

	if ($edit_mode eq 'raw')
	{
		print html_form_submit_buttons($device_name);
		print "
		 <tr>
			<td align=\"center\" colspan=\"2\"><textarea name=\"ini_file\" cols=\"100\" rows=\"20\">$file_data</textarea></td>
		 </tr>";
	}
	else
	{
		if ($file_data =~ m/Public_PC_Version/)
		{
			$device_subtype = $PUBLIC_PC;
		}

        $file_data =~ tr/\r//d;  # added to make sure it splits unix and windows data
    	my @config_lines = split(/\n/, $file_data);
		my $config_params = '';
		my $default_textbox_size = 68;
		my %config_hash;

		foreach my $config_line (@config_lines)
		{
			chomp($config_line);
			my ($config_param, $config_value) = split(/=/, $config_line, 2);	
			($config_param, $config_value) = (substr($config_param, 0, $max_param_code_len), substr($config_value, 0, $max_param_value_len));

			$config_param =~ s/^\s+//;
			$config_param =~ s/\s+$//;
			$config_value =~ s/^\s+//;
			$config_value =~ s/\s+$//;

			$config_line = "$config_param=$config_value";

			if (length($config_param) > 0)
			{
				$config_hash{uc($config_param)} = $config_value;
			}
		}

		### generate heading index ###
		my @heading_index;
		foreach my $config_item (@config_array)
		{	
			next unless UNIVERSAL::isa($config_item, 'USAT::DeviceAdmin::Config::Form::Object');	#ignore unknown elements
			last if ($device_subtype != $PUBLIC_PC && $config_item->parameter_code eq 'PublicPC Pricing');
			if (($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
				&& $config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Header'))
			{
				push @heading_index, $config_item;
			}
		}
		if (@heading_index)
		{
			print "
				<tr>
					<td colspan=\"2\" width=\"100%\" align=\"left\" bgcolor=\"#EEEEEE\">
						<a name=\"___section_index\"></a>
						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
						<td valign=\"top\" bgcolor=\"#EEEEEE\">
							<b>Section Index:</b>&nbsp;&nbsp;&nbsp;
						</td>
						<td bgcolor=\"#EEEEEE\">";
			print "
							<a href=\"#".URI->new($_->parameter_code)->as_string."\">".$_->parameter_code."</a><br>" foreach sort { lc($a->parameter_code) cmp lc($b->parameter_code) } @heading_index;
			print "
						</td>
						</tr>
						</table>
					</td>
				</tr>";
		}

		### generated form elements ###
		print html_form_submit_buttons($device_name);
		foreach my $config_item (@config_array)
		{	
			next unless UNIVERSAL::isa($config_item, 'USAT::DeviceAdmin::Config::Form::Object');	#ignore unknown elements

			if ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Header'))
			{
				last if ($device_subtype != $PUBLIC_PC && $config_item->parameter_code eq 'PublicPC Pricing');

				if ($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
				{
					print "
						<tr>
							<td colspan=\"2\" width=\"100%\" align=\"center\" bgcolor=\"#C0C0C0\"><font color=\"#0000CC\"><b><a name=\"".URI->new($config_item->parameter_code)->as_string."\">".$config_item->parameter_code."</a></b></font>&nbsp;&nbsp;&nbsp;<a href=\"#___section_index\"><img src=\"/icons/small/back.gif\" border=\"0\"></td>
						</tr>";
				    }
			    }
			else
			{
				$config_params = "$config_params|||".$config_item->parameter_code;

				if ($edit_mode eq EDITOR_ADVANCED || ($edit_mode eq EDITOR_BASIC && $config_item->editor eq EDITOR_BASIC))
				{
					print "
						<tr>
						  <td width=\"50%\"><b>".(defined $config_item->parameter_name ? $config_item->parameter_name . ' [' . $config_item->parameter_code . ']' : $config_item->parameter_code) . (defined $config_item->restriction ? "<font color=\"red\">*</font>" : "") . "</b>&nbsp;</td>
						  <td width=\"50%\">";

					 if ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::TextField'))
					 {
					 	my $param_value = $config_hash{uc($config_item->parameter_code)};
					 
					 	if ($device_subtype == $PUBLIC_PC && $config_item->parameter_code =~ m/^(OpenAPIPassword|MFPAdminPassword|SNMPCommunity)$/)
					 	{
					 		#decrypt value using shared key
					 		$param_value = rijndael_decrypt('703449366F54514E34735039326C5947', MODE_ECB, pack('H*', $param_value), 128, 128);
					 		#remove 0x00 padding
					 		$param_value =~ s/\x00+$//;
					 	}					 

						print "
							<input type=\"text\" name=\"cfg_".$config_item->parameter_code."\" value=\"" . (length($param_value) > 0 ? $param_value : $config_item->default) .  "\" size=\"" . (defined $config_item->maxlength ? ($config_item->maxlength + 7 > $default_textbox_size ? $default_textbox_size : $config_item->maxlength + 7) : $default_textbox_size) . "\" maxlength=\"" . (defined $config_item->maxlength ? $config_item->maxlength : $max_param_value_len) . "\"" . (defined $config_item->restriction ? " class=\"".$config_item->restriction."\"" : "") . ">";
					 }
					 elsif ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Radio'))
					 {
						my @choices = @{$config_item->choices};
						foreach my $choice (@choices)
						{
							print "
								<input type=\"radio\" name=\"cfg_".$config_item->parameter_code."\" value=\"$choice\"" . (length($config_hash{uc($config_item->parameter_code)}) > 0 ? (uc($config_hash{uc($config_item->parameter_code)}) eq uc($choice) ? ' checked' : '') : (uc($config_item->default) eq uc($choice) ? ' checked' : '')) . ">$choice</input>";
						}
					 }
					 elsif ($config_item->isa('USAT::DeviceAdmin::Config::Form::Object::Select'))
					 {
						print "
							<select name=\"cfg_".$config_item->parameter_code."\">";

						my @choices = @{$config_item->choices};
						foreach my $choice (@choices)
						{
							print "   
								<option value=\"$choice\"" . (length($config_hash{uc($config_item->parameter_code)}) > 0 ? (uc($config_hash{uc($config_item->parameter_code)}) eq uc($choice) ? ' selected' : '') : (uc($config_item->default) eq uc($choice) ? ' selected' : '')) . ">$choice</option>";
						}

						print "
							</select>";
					 }				 
					 else
					 {
						print "
							<input type=\"text\" name=\"cfg_".$config_item->parameter_code."\" value=\"$config_hash{uc($config_item->parameter_code)}\" size=\"$default_textbox_size\" maxlength=\"$max_param_value_len\">";
                     }

					 print "
						  </td>
						</tr>
						<tr>
						  <td colspan=\"2\" width=\"100%\" bgcolor=\"#EEEEEE\"><font size=\"-1\">".$config_item->description."</font>&nbsp;</td>
						</tr>";
				}
				else
				{
					print "
						<input type=\"hidden\" name=\"cfg_".$config_item->parameter_code."\" value=\"" . (length($config_hash{uc($config_item->parameter_code)}) > 0 ? $config_hash{uc($config_item->parameter_code)} : $config_item->default) . "\">";
				}

				delete($config_hash{uc($config_item->parameter_code)});
			}
		}

		foreach my $config_line (@config_lines)
		{
			my ($config_param, $config_value) = split(/=/, $config_line, 2);
			if (defined $config_hash{uc($config_param)})
			{
				$config_params = "$config_params|||$config_param";	
				print "
					<input type=\"hidden\" name=\"cfg_$config_param\" value=\"$config_value\">";
			}
		}

		$config_params = substr($config_params, 3);
		print "
			<input type=\"hidden\" name=\"config_params\" value=\"$config_params\">";
	}

	print html_form_submit_buttons($device_name);

	print "
	 <tr>
	  <th align=\"center\" colspan=\"2\" bgcolor=\"#C0C0C0\">$title</th>
	 </tr>

	 </form>
	</table>
	";

	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub html_form_submit_buttons ($) {
	my $device_name = shift;
	return "
	 <tr>
	  <td align=\"center\" colspan=\"2\">

			<table cellspacing=\"0\" cellpadding=\"5\" border=\"0\">
			 <tr>
			  <td>
			   <input type=\"button\" value=\"Save and Send\" onClick=\"doSubmit(this.form, 'Save and Send');\">
			  </td>
			  <td>
			   <input type=\"button\" value=\"Save Only\" onClick=\"doSubmit(this.form, 'Save Only');\">
			  </td>
			  <td>
			   <input type=\"button\" value=\"Cancel\" onClick=\"document.location = '/profile.cgi?ev_number=$device_name';\">
			  </td>
			 </tr>
			</table>

	  </td>
	 </tr>";
}

main();
