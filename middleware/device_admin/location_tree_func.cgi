#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;
require Text::CSV;
use USAT::Database;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

my $device_id = $PARAM{"device_id"};
if(length($device_id) == 0)
{
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	print "Required Parameter Not Found: device_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $location_id = $PARAM{"location_id"};
if(length($location_id) == 0)
{
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	print "Required Parameter Not Found: location_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $get_device_info_stmt = $dbh->prepare("select device.device_id, device.device_name, device.device_serial_cd, to_char(device.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(device.last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), device_type.device_type_desc, device.device_active_yn_flag, device.encryption_key, device.device_type_id, pos.location_id, pos.customer_id, pos.pos_id from device, device_type, pos where device.device_type_id = device_type.device_type_id and device.device_id = pos.device_id and device.device_id = :device_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_device_info_stmt->bind_param(":device_id", $device_id);
#$get_device_info_stmt->bind_param(":active", 'Y');
$get_device_info_stmt->execute();
my @device_data = $get_device_info_stmt->fetchrow_array();
if(not @device_data)
{
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	print "Device not found!";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

$get_device_info_stmt->finish();

my $device_name = $device_data[1];
my $device_serial_cd = $device_data[2];
$location_id = $device_data[9];
my $customer_id = $device_data[10];
my $pos_id = $device_data[11];

my $ssn = $device_serial_cd;
my $ev_number = $device_name;

my $action = $PARAM{"action"};
if($action eq 'toggle_monitor_flag')
{
	my $monitor_flag = $PARAM{"monitor_flag"};
	
	if($monitor_flag eq 'Y')
	{
		my $toggle_monitor_flag_sql = "update device_setting set device_setting_value = :device_setting_value where device_id = :device_id and device_setting_parameter_cd = :device_setting_parameter_cd";
		my $toggle_monitor_flag_stmt = $dbh->prepare($toggle_monitor_flag_sql);
		$toggle_monitor_flag_stmt->bind_param(":device_setting_value", 'N');
		$toggle_monitor_flag_stmt->bind_param(":device_id", $device_id);
		$toggle_monitor_flag_stmt->bind_param(":device_setting_parameter_cd", "Monitor Room");
		$toggle_monitor_flag_stmt->execute();
		$toggle_monitor_flag_stmt->finish();
	}
	elsif($monitor_flag eq 'N')
	{
		my $toggle_monitor_flag_sql = "update device_setting set device_setting_value = :device_setting_value where device_id = :device_id and device_setting_parameter_cd = :device_setting_parameter_cd";
		my $toggle_monitor_flag_stmt = $dbh->prepare($toggle_monitor_flag_sql);
		$toggle_monitor_flag_stmt->bind_param(":device_setting_value", 'Y');
		$toggle_monitor_flag_stmt->bind_param(":device_id", $device_id);
		$toggle_monitor_flag_stmt->bind_param(":device_setting_parameter_cd", "Monitor Room");
		$toggle_monitor_flag_stmt->execute();
		$toggle_monitor_flag_stmt->finish();
	}
	else
	{
		my $toggle_monitor_flag_sql = "insert into device_setting(device_setting_value, device_id, device_setting_parameter_cd) values(:device_setting_value, :device_id, :device_setting_parameter_cd)";
		my $toggle_monitor_flag_stmt = $dbh->prepare($toggle_monitor_flag_sql);
		$toggle_monitor_flag_stmt->bind_param(":device_setting_value", "Y");
		$toggle_monitor_flag_stmt->bind_param(":device_id", $device_id);
		$toggle_monitor_flag_stmt->bind_param(":device_setting_parameter_cd", "Monitor Room");
		$toggle_monitor_flag_stmt->execute();
		$toggle_monitor_flag_stmt->finish();
	}
	
	print CGI->redirect("/location_tree.cgi?source_id=$location_id");
	$dbh->disconnect;
	exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;
print "No Action Defined!";
$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
