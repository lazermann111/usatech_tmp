#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::UI::DAHeader;

use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::POS::API::PTA::Util;
use USAT::Database;
use USAT::Common::Const;
use USAT::App::DeviceAdmin::Const  qw(:globals );

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;
 
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $action = $PARAM{"action"};
if($action ne 'Finish >')
{
	print '<br><font color="red">Undefined Action!</font><br><br>';
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print '<br><font color="red">No devices selected!</font><br><br>';
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};
my $zero_counters = $PARAM{"zero_counters"};

my $pos_pta_tmpl_id = $PARAM{"pos_pta_tmpl_id"};
my $mode_cd = $PARAM{"mode_cd"};

my @devices_array = split /,/, $device_ids_str;

my $parameters_str = $PARAM{"parameter_to_change"};
my @parameters_array = split /,/, $parameters_str;

my $debug = $PARAM{"debug"};
if(length($debug) == 0)
{
	$debug = 0;
}

print qq{
<table cellpadding="3" border="1" cellspacing="0" width="100%">

 <tr>
  <th align="center" colspan="3" bgcolor="#C0C0C0">Device Configuration Wizard - Page 9: Processing Log</th>
 </tr>

 <tr>
  <td style="font-size: 10pt;">
    <center>
  	<input type=button value="Start Over" onClick="javascript:window.location = '/bulk_config_wizard_1.cgi';"> &nbsp;
  	<input type=button value="Device Search page" onClick="javascript:window.location = '/';"> <br>
    </center>
  <pre>\n
};

if($debug)
{
	print qq{<font color="red"><b>----- DEBUG IS ON!  CHANGES WERE NOT SAVED! -----</b></font>\n};
}

print "Selected Devices IDs\t: " . join(",", @devices_array) . "\n";
########print "Selected Parameters\t: " . join(",", @changes_array) . "\n";
print "Selected Customer ID\t: $customer_id\n";
print "Selected Location ID\t: $location_id\n";
print "Selected Parent Location ID\t: $PARAM{parent_location_id}\n";
print "\n";

my $custloc_error_count    = 0;
my $custloc_update_count   = 0;
my $parentloc_update_count = 0;
my $parentloc_error_count  = 0;

my $change_count           = 0;
my $no_change_count        = 0;
my $error_count            = 0;

my $import_count           = 0;
my $import_error_count     = 0;

my $file_transfer;
my $file_transfer_id;
my $file_transfer_name;

# Now we have to order device_id array with respect to PLV ( Property List Value )
# so that we do not need to read def configuration file many times.
my $questionmarks  = join(',', ('?') x scalar(@devices_array));
my $device_id_sql = qq{
   SELECT d.device_id, ds.device_setting_value
     FROM device d, device_setting ds
    WHERE d.device_id = ds.device_id
      AND d.device_type_id = ? 
      AND ds.device_setting_parameter_cd = 'Property List Version'
      AND d.device_id IN ( $questionmarks )
    ORDER BY ds.device_setting_value, d.device_id
};

my $qo = OOCGI::Query->new($device_id_sql, { bind => [ PKG_DEVICE_TYPE__EDGE, @devices_array ] } );
#foreach my $device_id (@devices_array)
my $device_setting_value = -1;
my @default_config_array;   # Bug 1090 J
while(my %query_hash = $qo->next_hash)
{
    my $device = USAT::DeviceAdmin::DBObj::Device->new($query_hash{device_id});
    my $ev_number = $device->device_name;
    my $device_id = $device->ID;

    my $operation_commit_ok = 1; # please set this to zero whenever there is a problem so operation should be rolled back.
    my $operation_status = "FAILURE for EV number $ev_number <br>";
    $dbh->begin_work();

    if(length($customer_id) > 0 || length($location_id) > 0 || length($PARAM{parent_location_id}) > 0)
    {
	    print "Updating Customer and/or Location for $ev_number ---------------------\n";
	
        my $msg = "Update performed on DEVICE ID = $device_id ";

        if(length($customer_id) > 0 && length($location_id) > 0)
        {
            $msg .= "CUSTOMER and LOCATION changed 
               customer_id = $customer_id, location_id = $location_id";
        }
        elsif(length($customer_id) > 0 && length($PARAM{parent_location_id}) > 0 )
        {
            $msg .= "CUSTOMER and PARENT LOCATION changed 
               customer_id = $customer_id, parent_location_id = $PARAM{parent_location_id}";
        }
        elsif(length($customer_id) > 0)
        {
            $msg .= " ONLY CUSTOMER changed customer_id = $customer_id";
        }
        elsif(length($location_id) > 0)
        {
            $msg .= " ONLY LOCATION changed location_id = $location_id";
        }
        elsif(length($PARAM{parent_location_id}) > 0)
        {
            $msg .= " ONLY PARENT LOCATION changed parent_location_id = $PARAM{parent_location_id}";
        }
        my $result;

        if($debug == 0)
        {
	    	if(length($customer_id) > 0 && length($location_id) > 0)
	    	{
	    		### i.e. proc returns [:newDeviceID,:newPOSID,:statusCd,:errMsg] ###
	    		$result = $DATABASE->callproc(
	    			name  => 'pkg_device_maint.sp_update_loc_id_cust_id',
	    			bind  => [ qw(:deviceID :newLocationID :newCustomerId :newDeviceID :newPOSID :statusCd :errMsg) ],
	    			in    => [ $device_id, $location_id, $customer_id ],
	    			inout => [ 20, 20, 20, 2000 ]
	    		);
	    	}
	    	elsif(length($customer_id) > 0)
	    	{
	    		$result = $DATABASE->callproc(
	    			name  => 'pkg_device_maint.sp_update_cust_id',
	    			bind  => [ qw(:deviceID :newCustomerId :newDeviceID :newPOSID :statusCd :errMsg) ],
	    			in    => [ $device_id, $customer_id ],
	    			inout => [ 20, 20, 20, 2000 ]
	    		);
	    	}
	    	elsif(length($location_id) > 0)
	    	{
	    		$result = $DATABASE->callproc(
	    			name  => 'pkg_device_maint.sp_update_loc_id',
	    			bind  => [ qw(:deviceID :newLocationID :newDeviceID :newPOSID :statusCd :errMsg) ],
	    			in    => [ $device_id, $location_id ],
	    			inout => [ 20, 20, 20, 2000 ]
	    		);
	    	}
            if(defined $result->[0] && $result->[0] > 0) {
               $device_id = $result->[0];
            } else {
               $operation_commit_ok = 0; # rollback.
               Carp::carp "Bulk config ROLLBACK 1\n";
               $operation_status .= "Can't change customer $customer_id or location $location_id <br>";
            }

            if(length($PARAM{parent_location_id}) > 0 ) {
                # following is parent_location update
                my $sql = q{ UPDATE location.location
                             SET location.parent_location_id = ?
                    WHERE EXISTS
                    (
                          SELECT 1
                            FROM device, pos
                           WHERE device.device_id = pos.device_id
                             AND pos.location_id  = location.location_id
                             AND device.device_id = ?
                    )
					AND location.location_id != 1};
                 if($debug == 0)
                 {
                     my $count = OOCGI::Query->modify($sql, [ $PARAM{parent_location_id}, $device_id ] );
                     if($count > 0) {
                        $parentloc_update_count++;
                     } else {
                        $parentloc_error_count++;
                        $operation_commit_ok = 0; # rollback.
                        Carp::carp "Bulk config ROLLBACK 2\n";
                     }
                 }
            } else {
	    	   if (defined $result->[0] && defined $result->[2] && $result->[2] == 0) {
	    		   $custloc_update_count++;
			       print "Succcess: $msg \n";
	    	   } else {
	    	      	$custloc_error_count++;
	    	      	print "Error: $msg \n";
                    $operation_commit_ok = 0; # rollback.
                    Carp::carp "Bulk config ROLLBACK 3\n";
	    	   }
            }
        }
		else
		{
			print "Succcess: $msg \n";
		}
    } else {
	   print "No Customer or Location Changes! for $ev_number ---------------------\n";
    }

    print "\n";


    if(length($mode_cd) && length($pos_pta_tmpl_id) > 0)
    {
    	print "Importing Payment Templates for $ev_number ---------------------\n";
	
		print "Import template $pos_pta_tmpl_id in mode $mode_cd to device $device_id\n";
		if($debug == 0)
		{	
			my $err;
			my $success = USAT::POS::API::PTA::Util::import_template($DATABASE, $device_id, $pos_pta_tmpl_id, $mode_cd, \$err);
		
			if(!$success)
			{
				print "Import failed: $err\n";
				$import_error_count++;
                $operation_commit_ok = 0; # rollback.
                Carp::carp "Bulk config ROLLBACK 4\n";
			}
			else
			{
				$import_count++;
			}
		}
		else
		{
			$import_count++;
		}
    }

    print "\n";


    if(length($PARAM{config_string}) == 0)
    {
       	print "No Configuration Changes for $ev_number ! ---------------------\n";
    }

    if(length($PARAM{config_string}) > 0)
    {
       my @device_config_array;    # Bug 1090 D
       if($device_setting_value != $query_hash{device_setting_value}) {
          # following line is used to detect the changes of PLV ( Property List Value )
          $device_setting_value  = $query_hash{device_setting_value};
          # when ever $device_setting_value changes read default configuration file
          my $file_name = PKG_DEVICE_CONF__EDGE.$query_hash{device_setting_value};
          my ($status_ok, $file_array) = USAT::DeviceAdmin::DBObj::File_transfer::get_config_file($file_name);
          if(defined $status_ok && $status_ok > 0) {
             @default_config_array = @{$file_array};
          } else {
             $operation_commit_ok = 0; # rollback.
             Carp::carp "Bulk config ROLLBACK 5\n";
          }
       } 

       my $file_transfer_obj; # this is original file transfer obj for a given device
       my $device_config_file_id = 0;
       {  # just localize the variables so that we can use same variables
          # following line is used to detect the changes of PLV ( Property List Value )
          my $file_name = $ev_number."-CFG";
          $file_transfer_obj = USAT::DeviceAdmin::DBObj::File_transfer->new({file_transfer_name => $file_name});
          my ($status_ok, $file_array) = USAT::DeviceAdmin::DBObj::File_transfer::get_config_file($file_name);
          if(defined $status_ok && $status_ok > 0 && defined $file_array) {
             @device_config_array = @{$file_array};
             $device_config_file_id = $file_transfer_obj->ID;
          } else {
             $operation_commit_ok = 0; # rollback.
             Carp::carp "Bulk config ROLLBACK 6 for EV Number $ev_number and File name $file_name\n";
          }
       } 

       # here is user entered values. This is bug 1090 U option
       my $config_string = $PARAM{config_string};
       $config_string =~ tr/\r//d;
       my @user_config_lines = split(/\n/, $config_string);
       my $new_ini_file_short;
       my $changes_for_device = '';
       foreach my $user_line ( @user_config_lines ) {
          my ($user_index, $user_value) = split(/=/, $user_line);
#         Carp::carp "AAA $user_index|$user_value|$default_config_array[$user_index]|$device_config_array[$user_index]|BBB \n";
          if(defined $user_index && $user_index ne '' && $device_config_array[$user_index] ne $user_value) {
             if(defined $default_config_array[$user_index]) {
                # if a DNS host name or IP address doesn't have two dots, use the default value
                if($user_index =~ m/^(0|1|2|3|20|21|22|23)$/ && $user_value !~ m/\..+\./) {
                	$user_value = $default_config_array[$user_index];
                }  
                if($default_config_array[$user_index] eq $user_value) {
                   $new_ini_file_short .= "\n$user_index!";
                   $changes_for_device .= "! $user_index $device_config_array[$user_index] ==> $user_value ( default ) \n";
                } else {
                   $new_ini_file_short .= "\n$user_index="."$user_value";
                   $changes_for_device .= "= $user_index|$default_config_array[$user_index]| $device_config_array[$user_index] ==> $user_value \n";
                }
             } else {
                   $changes_for_device .= "- $user_index $device_config_array[$user_index] ==> $user_value ( NOT SUPPORTED ) \n";
             }
             # change device value, this will be used to create new device configuration file;
             $device_config_array[$user_index] = $user_value;
          }
       }

       my $new_device_config_file;
       for(my $i = 0; $i <= $#device_config_array; $i++ ) {
          if(defined($device_config_array[$i]) && defined($default_config_array[$i])) {
          	 # if a DNS host name or IP address doesn't have two dots, use the default value
          	 if($i =~ m/^(0|1|2|3|20|21|22|23)$/ && $device_config_array[$i] !~ m/\..+\./) {
          	 	$new_device_config_file .= $i.'='.$default_config_array[$i]."\n";
          	 } else {
             	$new_device_config_file .= $i.'='.$device_config_array[$i]."\n";
          	 }
          }
       }

#      chop($new_device_config_file);
#      Carp::carp "CONFIG FILE for EV Number $ev_number \n  $new_device_config_file \n";
	   print "Updating Configuration Parameters for $ev_number ---------------------\n";
       print "Changes for $ev_number is \n",$changes_for_device,"\n";

       if(defined $new_ini_file_short && $new_ini_file_short ne '') {
          USAT::DeviceAdmin::Util::save_file($ev_number."-CFG", uc(unpack("H*", $new_device_config_file)), $dbh);     
          
          if(defined $dbh->errstr)
		  {
			  $operation_commit_ok = 0; # rollback.
              Carp::carp "Error saving file ".$ev_number."-CFG: " . $dbh->errstr . "\n";
		  }     
          
		  my $get_file_id_stmt = $dbh->prepare("select DEVICE.SEQ_FILE_TRANSFER_ID.nextval from dual") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		  $get_file_id_stmt->execute();
		  my @file_id_data = $get_file_id_stmt->fetchrow_array();
		  $get_file_id_stmt->finish();

		  if(!@file_id_data)
		  {
			  $operation_commit_ok = 0; # rollback.
              Carp::carp "Failed to get a new file_transfer_id!\n";
		  }

		  $file_transfer_id = $file_id_data[0];
		  $file_transfer_name = $PARAM{file_name} . '-' . $file_transfer_id;

		  USAT::DeviceAdmin::Util::create_file_with_id($file_transfer_id, $file_transfer_name, PKG_FILE_TYPE__PROPERTY_LIST, unpack("H*",$new_ini_file_short), $DATABASE);

		  if(defined $dbh->errstr)
		  {
			  $operation_commit_ok = 0; # rollback.
			  Carp::carp "Error creating file $file_transfer_name: " . $dbh->errstr . "\n";
		  }
          
          if($device_config_file_id) {
             my $result = $DATABASE->callproc(
                     name  => 'pkg_device_configuration.sp_update_device_settings',
                     bind  => [ qw(:pn_device_id :pn_file_transfer_id :pn_resuld_cd :pv_error_message :pn_setting_count) ],
                     in    => [ $device_id, $device_config_file_id ],
                     inout => [ 20, 2000, 20]
             );
             if($result->[0] <= 0) {
                $operation_commit_ok = 0; # rollback.
                Carp::carp "Bulk config ROLLBACK 7\n";
             } else {
                print "Processed $result->[2] records for device_id: $device_id, file_transfer_id: $device_config_file_id. \n";
             }
          } else {
             $operation_commit_ok = 0; # rollback.
             Carp::carp "Bulk config ROLLBACK 8\n";
          }
          print "File transfer id for this operation is : $file_transfer_id \n";
		
		  # now get the next device_file_transfer_id from the sequence
		  my $get_transfer_id_stmt = $dbh->prepare("select DEVICE.SEQ_DEVICE_FILE_TRANSFER_ID.nextval from dual") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		  $get_transfer_id_stmt->execute();
		  my @transfer_id_data = $get_transfer_id_stmt->fetchrow_array();
		  $get_transfer_id_stmt->finish();

		  if(!@transfer_id_data)
		  {
			 $operation_commit_ok = 0; # rollback.
             Carp::carp "Failed to get a new device_file_transfer_id!\n";
		  }

		  my $transfer_id = $transfer_id_data[0];
		
		  # now insert the device_file_transfer
		  my $insert_transfer_stmt = $dbh->prepare("insert into device.device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size) values (:transfer_id, :device_id, :file_id, :direction, :status, :packet_size)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		  $insert_transfer_stmt->bind_param(":transfer_id", $transfer_id); 
		  $insert_transfer_stmt->bind_param(":device_id", $device_id);
		  $insert_transfer_stmt->bind_param(":file_id", $file_transfer_id);
		  $insert_transfer_stmt->bind_param(":direction", 'O');
		  $insert_transfer_stmt->bind_param(":status", '0');
		  $insert_transfer_stmt->bind_param(":packet_size", 1024);
		  $insert_transfer_stmt->execute();
		
		  if(defined $dbh->errstr)
		  {
			 $operation_commit_ok = 0; # rollback.
             Carp::carp "Failed to insert into device_file_transfer: " . $dbh->errstr . "\n";
		  }
		  $insert_transfer_stmt->finish();          
          
		  # now insert a pending message for this new transfer
		  my $insert_pending_msg_stmt = $dbh->prepare("insert into engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order) values (:ev_number, :data_type, :transfer_id, :execute_cd, :execute_order)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
		  $insert_pending_msg_stmt->bind_param(":ev_number", $ev_number); 
		  $insert_pending_msg_stmt->bind_param(":data_type", EPORT_CMD__FILE_TRANSFER_START_V4_1); 
		  $insert_pending_msg_stmt->bind_param(":transfer_id", $transfer_id); 
		  $insert_pending_msg_stmt->bind_param(":execute_cd", DA_CMD__P_PENDING); 
		  $insert_pending_msg_stmt->bind_param(":execute_order", '1'); 
		  $insert_pending_msg_stmt->execute();
		
		  if(defined $dbh->errstr)
		  {
			 $operation_commit_ok = 0; # rollback.
             Carp::carp "Failed to insert into machine_cmd_pending: " . $dbh->errstr . "\n";
		  } else {
             $change_count++;
          }
		  $insert_pending_msg_stmt->finish();
                    
          print "\n\n";
       }
    }
    if($operation_commit_ok) {
       print "Operation committed for $ev_number\n\n";
       $dbh->commit();
    } else {
       print "Operation rolled back for $ev_number\n";
       Carp::carp "Operation rolled back for $ev_number\n";
       $dbh->rollback();
    }
}

print "\n";
print "Operation Summary ---------------------\n\n";
print "Parameters Changed\t\t\t: $change_count\n";
print "Parameters Skipped\t\t\t: $no_change_count\n";
print "Parameters Errors\t\t\t: $error_count\n";
if(!$PARAM{parent_location_id}) {
   print "Customer/Locations Updated\t\t: $custloc_update_count\n";
   print "Customer/Location Errors\t\t: $custloc_error_count\n";
} else {
   print "Customer/Parent Locations Updated\t: $parentloc_update_count\n";
   print "Customer/Parent Location Errors\t\t: $parentloc_error_count\n";
}
print "Payment Templates Imported\t\t: $import_count\n";
print "Payment Template Errors\t\t: $import_error_count\n";

if($error_count > 0 || $custloc_error_count > 0 || $import_error_count > 0)
{
	if($error_count > 0)
	{	
		my $msg = "Warning: $error_count of " . ($change_count+$no_change_count+$error_count) . " Updates Failed!\\n\\nSome changes were not saved correctly. Please click OK, then print or email this screen and give it to an administrator.";
		print "\n\n<font color=\"red\"><b>$msg</b></font><br>\n\n";
		print "<script language=\"javascript\">window.alert(\"$msg\");</script>";
	}
	
	if($custloc_error_count > 0)
	{
#	my $msg = "Warning: $update_errors Customer/Location Updates Failed!  Some changes were not saved correctly. Please click OK, then print or email this screen and give it to an administrator.";
		my $msg = "Warning: Customer/Location Updates Failed!  Some changes were not saved correctly. Please click OK, then print or email this screen and give it to an administrator.";
		print "\n\n<font color=\"red\"><b>$msg</b></font><br>\n\n";
		print "<script language=\"javascript\">window.alert(\"$msg\");</script>";
	}
	
	if($import_error_count > 0)
	{
		my $msg = "Warning: Payment Template Import Failed!  Some changes were not saved correctly. Please click OK, then print or email this screen and give it to an administrator.";
		print "\n\n<font color=\"red\"><b>$msg</b></font><br>\n\n";
		print "<script language=\"javascript\">window.alert(\"$msg\");</script>";
	}
			
	print "POST DATA -----------------\n";
	foreach my $key (sort(keys(%PARAM))) 
	{
		print "$key = $PARAM{$key}\n";
	}

		print "\n";
	print "ENV -----------------\n";
	foreach my $key (sort(keys(%ENV))) 
	{
		print "$key = $ENV{$key}\n";
	}
}
else
{
    if($PARAM{parent_location_id}) {
	print qq{<script language="javascript">window.alert("All changes were processed successfully!\\n\\n$change_count Config Parameters Updated.\\n$no_change_count Config Parameters Already Up-To-Date.\\n$import_count Payment Templates Imported\\n$parentloc_update_count Customers/Parent Locations Updated");</script>};	
    } else {
	print qq{<script language="javascript">window.alert("All changes were processed successfully!\\n\\n$change_count Config Parameters Updated.\\n$no_change_count Config Parameters Already Up-To-Date.\\n$import_count Payment Templates Imported\\n$custloc_update_count Customers/Locations Updated");</script>};	
    }
}

if($debug)
{
	print "\n\n";
	print "<font color=\"red\"><b>----- DEBUG IS ON!  CHANGES WERE NOT SAVED! -----</b></font>\n";
}

$dbh->disconnect;

print "
   </pre>
  </td>
 </tr>
</table>
";

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
