#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use HTML::Entities;
use USAT::Database;
use USAT::Security::StringMask qw(mask_credit_card);
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars();
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $auth_id = $PARAM{auth_id};
if(length($auth_id) == 0)
{
	print "Required Parameter Not Found: auth_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $detail_info_stmt = $dbh->prepare(q{
	select auth.auth_id, 
	auth.tran_id, 
	auth_type.auth_type_desc, 
	auth_state.auth_state_name, 
	auth.auth_parsed_acct_data, 
	acct_entry_method.acct_entry_method_desc, 
	TRIM(TO_CHAR(auth.auth_amt, '$9,999,999,990.00')), 
	to_char(auth.auth_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	auth_result.auth_result_desc, 
	auth.auth_resp_cd, 
	auth.auth_resp_desc, 
	auth.auth_authority_tran_cd, 
	auth.auth_authority_ref_cd, 
	to_char(auth.auth_authority_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(auth.created_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	to_char(auth.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'),
	auth.terminal_batch_id,
	TRIM(TO_CHAR(auth.auth_amt_approved, '$9,999,999,990.00')), 
	auth.auth_authority_misc_data,
	TRIM(TO_CHAR(auth.auth_authority_amt_rqst, '$9,999,999,990.00')),
	TRIM(TO_CHAR(auth.auth_authority_amt_rcvd, '$9,999,999,990.00')),
	auth.trace_number,
	TRIM(TO_CHAR(auth.auth_balance_amt, '$9,999,999,990.00')),
	action.action_id,
	action.action_name,
	auth.auth_action_bitmap
	from pss.auth, pss.auth_type, pss.auth_state, pss.auth_result, pss.acct_entry_method, device.action
	where auth.auth_type_cd = auth_type.auth_type_cd
	and auth.auth_state_id = auth_state.auth_state_id
	and auth.auth_result_cd = auth_result.auth_result_cd(+)
	and auth.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd(+)
	and auth.auth_action_id = action.action_id(+)
	and auth.auth_id = :auth_id
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$detail_info_stmt->bind_param(":auth_id", $auth_id);
$detail_info_stmt->execute();
my @detail_data = $detail_info_stmt->fetchrow_array();
$detail_info_stmt->finish();

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"4\" bgcolor=\"#C0C0C0\">Transaction Details</th>
 </tr>
 <tr>
  <td nowrap>Detail ID</td>
  <td>$detail_data[0]&nbsp;</td>
  <td nowrap>Type</td>
  <td>$detail_data[2]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>State</td>
  <td>$detail_data[3]&nbsp;</td>
  <td nowrap>Account Data</td>
  <td>" . mask_credit_card($detail_data[4]) . "&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Amount Requested</td>
  <td>$detail_data[6]&nbsp;" . (defined $detail_data[19] ? "($detail_data[19] Override)" : "") . "</td>
  <td nowrap>Entry Method</td>
  <td>$detail_data[5]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Amount Approved</td>
  <td>$detail_data[17]&nbsp;" . (defined $detail_data[20] ? "($detail_data[20] Override)" : "") . "</td>
  <td nowrap>Result</td>
  <td>$detail_data[8]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Detail Timestamp</td>
  <td>$detail_data[7]&nbsp;</td>
  <td nowrap>Response Code</td>
  <td>$detail_data[9]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Authority Timestamp</td>
  <td>$detail_data[13]&nbsp;</td>
  <td nowrap>Response Msg</td>
  <td>$detail_data[10]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Created</td>
  <td>$detail_data[14]&nbsp;</td>
  <td nowrap>Authorization Code</td>
  <td>$detail_data[11]&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Last Updated</td>
  <td>$detail_data[15]&nbsp; $detail_data[16]</td>
  <td nowrap>Reference Num</td>
  <td>$detail_data[12]&nbsp;</td>
 </tr>
 <tr>
   <td nowrap>Terminal Batch ID</td>
   <td><a href=\"terminal_batch.cgi?terminal_batch_id=$detail_data[16]\">$detail_data[16]</a>&nbsp;</td>
  <td nowrap>Transaction ID</td>
  <td><a href=\"tran.cgi?tran_id=$detail_data[1]\">$detail_data[1]</a>&nbsp;</td>

 </tr>
 <tr>
  <td nowrap>Misc Data</td>
  <td colspan=\"3\">" . HTML::Entities::encode($detail_data[18]) . "&nbsp;</td>
 </tr>
 <tr>
  <td nowrap>Trace Number</td>
  <td>$detail_data[21]&nbsp;</td>
  <td nowrap>Balance Amount</td>
  <td>$detail_data[22]&nbsp;</td>
 </tr>
";

if ($detail_data[23]) {
	print "
	 <tr>
	  <td nowrap>Action</td>
	  <td colspan=\"3\">$detail_data[24]&nbsp;</td>
	 </tr>
	";
}

print "
</table>
";

if ($detail_data[23] && $detail_data[25]) {
	my $action_param_list_stmt = $dbh->prepare(
	q{
		select action_param_name, protocol_bit_index
		from device.action_param
		where action_id = :action_id 
			and BITAND(:auth_action_bitmap, POWER(2, protocol_bit_index)) > 0
		order by protocol_bit_index, action_param_name
	});
	$action_param_list_stmt->bind_param(":action_id", $detail_data[23]);
	$action_param_list_stmt->bind_param(":auth_action_bitmap", $detail_data[25]);
	$action_param_list_stmt->execute();
	my @action_param_info;

	print"
	<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
	<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
	 <tr>
	   <th colspan=\"2\" bgcolor=\"#C0C0C0\">Action Parameters</th>
	  </tr>
	  <tr>
	   <th>Action Parameter</th>
	   <th>Bit Index</th>
	  </tr>
	";

	while(@action_param_info = $action_param_list_stmt->fetchrow_array())
	{
		print "
			 <tr>
			 <td>$action_param_info[0]&nbsp;</td>
			 <td>$action_param_info[1]&nbsp;</td>
			</tr>
		";
	}

	$action_param_list_stmt->finish();

	print "
	</table>
	";	
}

my $settlement_batch_list_stmt = $dbh->prepare(
q{
	select settlement_batch.settlement_batch_id, to_char(settlement_batch.settlement_batch_start_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	to_char(settlement_batch.settlement_batch_end_ts, 'MM/DD/YYYY HH:MI:SS AM'), settlement_batch_state.settlement_batch_state_name, 
	to_char(settlement_batch.created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(settlement_batch.last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'), 
	settlement_batch.settlement_batch_resp_cd, settlement_batch.settlement_batch_resp_desc, settlement_batch.settlement_batch_ref_cd, 
	settlement_batch.terminal_batch_id 
	from pss.settlement_batch, pss.settlement_batch_state, pss.tran_settlement_batch
	where settlement_batch.settlement_batch_state_id = settlement_batch_state.settlement_batch_state_id 
	and settlement_batch.settlement_batch_id = tran_settlement_batch.settlement_batch_id
	and tran_settlement_batch.auth_id = :auth_id
	order by settlement_batch.settlement_batch_start_ts desc
});
$settlement_batch_list_stmt->bind_param(":auth_id", $auth_id);
$settlement_batch_list_stmt->execute();
my @settlement_batch_info;

print"
<hr width=\"100%\" noshade size=\"2\" color=\"#000000\">
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
   <th colspan=\"6\" bgcolor=\"#C0C0C0\">Settlements</th>
  </tr>
  <tr>
   <th>ID</th>
   <th>Settlement Date</th>
   <th>Result</th>
  </tr>
";

while(@settlement_batch_info = $settlement_batch_list_stmt->fetchrow_array())
{
	print "
	     <tr>
    	 <td><a href=\"settlement_batch.cgi?settlement_batch_id=$settlement_batch_info[0]\">$settlement_batch_info[0]</a></td>
	     <td>$settlement_batch_info[1]&nbsp;</td>
	     <td>$settlement_batch_info[3]&nbsp;</td>
    	</tr>
	";
}

$settlement_batch_list_stmt->finish();

print "
</table>
";

$dbh->disconnect;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
