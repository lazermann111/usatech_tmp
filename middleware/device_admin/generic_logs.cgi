#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::PageNavigation;
use OOCGI::PageSort;
use OOCGI::Query;
use OOCGI::NTable;
use USAT::DeviceAdmin::UI::DAHeader;
require Text::CSV;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print '<form>';

my $device_type = lc($PARAM{"device_type"});
my $ev_number = $PARAM{"ev_number"};
print "<input type=hidden name=device_type value=$device_type>";
print "<input type=hidden name=ev_number   value=$ev_number>";

my $pageNav  = OOCGI::PageNavigation->new(15);

if(length($device_type) == 0)
{
	print "Required parameter not found: device_type\n";
}
elsif(length($ev_number) == 0)
{
	print "Required parameter not found: ev_number\n";
} else {

my $sql = "SELECT to_char(created_ts, 'MM/DD/YY HH24:MI:SS') as timestamp,
                  object_name,
                  additional_information
             FROM exception_data
                    WHERE exception_code_id = 10000
                      AND server_name = ? ";

my $bind_value = $ev_number;

my %table_cols = (
	1 => ['Timestamp',       'created_ts',                   -1],
	2 => ['Log Data',        'LOWER(object_name)',            1],
	3 => ['Additional Info', 'LOWER(additional_information)', 1]
);

my $pageSort = OOCGI::PageSort->new( default_sort_field => 1 * $table_cols{1}->[2] );
my ($sort_id, $sort_asc, $last_sort_id, $last_sort_asc) = $pageSort->get_sorted_columns();
$sql .= '    ORDER BY '.$table_cols{$sort_id}->[1].($sort_asc ? '' : ' DESC');
$sql .= ','.$table_cols{$last_sort_id}->[1].($last_sort_asc ? '' : ' DESC') if $last_sort_id;

my $qo = $pageNav->get_result_obj($sql, $bind_value);

my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

my $rn_table = 0;
$table->put($rn_table,0,B("Generic Logs - $ev_number"),'align=center bgcolor=#C0C0C0 colspan=3 ');

$rn_table++;
my $cn_table = 0;
foreach my $key (map($_ * $table_cols{$_}->[2], sort {$a <=> $b} keys %table_cols)) {
    (my $text = $table_cols{abs($key)}->[0]) =~ s/\s/&nbsp;/go;
    $table->put($rn_table,$cn_table++,$pageSort->sort_column($key, $text),'class=headstyle');
}
$rn_table++;

my $result_count = 0;

my $row0 = "row0";
my $row1 = "row1";
my $i = 0;

while (my @data = $qo->next) 
{
    my $row = $i%2 ? $row1 : $row0;
    $table->put($rn_table,0, $data[0], "class=$row");
    $table->put($rn_table,1, $data[1], "class=$row");
    $table->put($rn_table,2, $data[2], "class=$row");
    $rn_table++;
    $i++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

#<input type=hidden name=file_type value=$file_type>
#<input type=hidden name=file_name value=$file_name>
print '</form>';
$pageNav->enable();
$pageSort->enable();
}

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
