#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

####### Testing development against production
#my $DATABASE = USAT::Database->new(
#	PrintError => 1, RaiseError => 0, AutoCommit => 1, 
#	primary => 'usadbp', backup => 'usadbp', username => 'web_user', password => 'wxkj21a9'
#);
#######
####### Prodction
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
#######
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

$session->print_menu;
$session->destroy;

my $callindate = $PARAM{"auth_date"};
if(length($callindate) == 0)
{
	print "Required parameter not found: auth_date\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $call_in_stmt = $dbh->prepare("SELECT aut.session_attempt_count, aut.auth_attempt_count, aut.auth_success_count, (CASE WHEN aut.auth_attempt_count = 0 THEN 0 ELSE ROUND((aut.auth_success_count / aut.auth_attempt_count) * 100, 1) END) auth_success_per, ROUND(aut.min_auth_success_time, 0) min_auth_success_time, ROUND(aut.avg_auth_success_time, 0) avg_auth_success_time, ROUND(aut.max_auth_success_time, 0) max_auth_success_time, aut.auth_error_count, (CASE WHEN aut.auth_attempt_count = 0 THEN 0 ELSE ROUND((aut.auth_error_count / aut.auth_attempt_count) * 100, 1) END) auth_error_per, aut.session_error_count, (CASE WHEN aut.session_attempt_count = 0 THEN 0 ELSE ROUND((aut.session_error_count / aut.session_attempt_count) * 100, 1) END) session_error_per FROM ( SELECT COUNT(1) session_attempt_count, SUM(CASE WHEN auth.response_msg LIKE 'AuthV3%' THEN 1 ELSE 0 END) auth_attempt_count, SUM(CASE WHEN auth.auth_status = 'S' THEN 1 ELSE 0 END) auth_success_count, MIN(CASE WHEN auth.auth_time_ms <> -1 THEN auth.auth_time_ms END) min_auth_success_time, AVG(CASE WHEN auth.auth_time_ms <> -1 THEN auth.auth_time_ms END) avg_auth_success_time, MAX(CASE WHEN auth.auth_time_ms <> -1 THEN auth.auth_time_ms END) max_auth_success_time, SUM(CASE WHEN auth.auth_status = 'U' AND auth.response_msg LIKE 'AuthV3%' THEN 1 ELSE 0 END) auth_error_count, SUM(CASE WHEN auth.auth_status = 'U' AND auth.response_msg NOT LIKE 'AuthV3%' THEN 1 ELSE 0 END) session_error_count FROM tracking.auth_monitor auth WHERE auth.created_ts >= TO_DATE(:callindate, 'MM/DD/YYYY') AND auth.created_ts < (TO_DATE(:callindate, 'MM/DD/YYYY') + 1) ) aut") || print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";

$call_in_stmt->bind_param(":callindate", $callindate);
$call_in_stmt->execute();


print "
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\" width=\"100%\">
 <tr>
  <th bgcolor=\"#A0A0A0\" colspan=\"11\">Auth Round Trip (GPRS Remote Device) Report for $callindate</th>
 </tr>
 <tr>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Session Attempts</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Auth Attemps</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Auth Success</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Auth Success %</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Min Auth Success Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Avg Auth Success Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Max Auth Success Time</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Auth Error</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Auth Error %</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Session Error</th>
  <th style=\"font-size: 10pt;\" bgcolor=\"#C0C0C0\">Session Error %</th>
 </tr>
";

my $count = 0;
while (my @data = $call_in_stmt->fetchrow_array()) 
{
	$count++;
	
	print " <tr>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[0]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[1]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[2]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[3]%&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[4] ms&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[5] ms&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[6] ms&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[7]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[8]%&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[9]&nbsp;</td>
	         <td style=\"font-size: 10pt;\" align=\"center\" nowrap>$data[10]%&nbsp;</td>
	        </tr>
	      ";
}

$call_in_stmt->finish();

if($count == 0)
{
	print " <tr><td style=\"font-size: 10pt;\" colspan=\"11\" align=\"center\"><font color=\"red\">&nbsp;No Data!</font></td></tr>\n";
}

print "
</table>
";

$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
