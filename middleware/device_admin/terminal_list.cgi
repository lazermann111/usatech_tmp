#!/usr/local/USAT/bin/perl

use strict;
use warnings;	#optional

use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use OOCGI::OOCGI;
use OOCGI::NTable;
require Text::CSV;

use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::DeviceAdmin::Profile::TranHistory;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $pageNav  = OOCGI::PageNavigation->new(25);

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $authority_id = $PARAM{"authority_id"};
my $merchant_id  = $PARAM{"merchant_id"};

my @params;

my $list_terminals_sql = q{
	SELECT terminal.terminal_id, 
	       terminal.terminal_cd, 
	       terminal.terminal_desc, 
	       terminal_state.terminal_state_name,
	       merchant.merchant_id, 
	       merchant.merchant_cd,
	       merchant.merchant_name,
	       merchant.merchant_desc,
	       merchant.merchant_bus_name,
	       authority.authority_id, 
	       authority.authority_name,
	       authority_type.authority_type_name,
	       authority_type.authority_type_desc,
	       terminal.terminal_next_batch_num
	  FROM terminal, merchant, authority.authority, terminal_state, authority_type
	 WHERE terminal.merchant_id = merchant.merchant_id
	   AND merchant.authority_id = authority.authority_id
	   AND terminal.terminal_state_id = terminal_state.terminal_state_id
	   AND authority.authority_type_id = authority_type.authority_type_id 
};

if(defined $authority_id && length($authority_id) > 0)
{
	$list_terminals_sql .= ' and authority.authority_id = ?';
	push(@params, $authority_id);
}

if(defined $merchant_id && length($merchant_id) > 0)
{
	$list_terminals_sql .= ' and merchant.merchant_id = ?';
	push(@params, $merchant_id);
}

my @table_cols = (
    ['Authority',     ,  2],
    ['Merchant'       ,  1],
    ['Merchant<br>Num',  3],
    ['Terminal'       ,  4],
    ['Terminal State' , -5],
    ['Next Batch Num' , -6]
);

my @sql_cols = (
    'authority.authority_name',
    'merchant.merchant_name',
    'merchant.merchant_id',
    'terminal.terminal_cd',
    'terminal_state.terminal_state_name',
    'terminal.terminal_next_batch_num'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');
$table->put($rn_table,0,B('Terminal List'),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

my $sql = $list_terminals_sql.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql, \@params );

while (my @batch_data = $qo->next_array())
{
    $table->put($rn_table,0,qq{<a href="authority.cgi?authority_id=$batch_data[9]">$batch_data[10]});
    $table->put($rn_table,1,qq{<a href="merchant.cgi?merchant_id=$batch_data[4]">$batch_data[6]</a>});
    $table->put($rn_table,2,qq{$batch_data[5]});
    $table->put($rn_table,3,qq{<a href="terminal.cgi?terminal_id=$batch_data[0]">$batch_data[1]</a>});
    $table->put($rn_table,4,qq{$batch_data[3]},'nowrap');
    $table->put($rn_table,5,qq{$batch_data[13]},'nowrap');

    $rn_table++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

$pageNav->enable();

$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
