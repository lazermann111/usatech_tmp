#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use OOCGI::NTable;
use OOCGI::OOCGI 0.11;
use OOCGI::Query 0.10;
use USAT::DeviceAdmin::UI::DAHeader;

use strict;
my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

if($PARAM{myaction} eq 'Delete' && ($PARAM{customer_id} != 1 || $PARAM{customer_name} ne 'Unknown')) {
    my $customer_id = $PARAM{customer_id};
    my $customer = USAT::DeviceAdmin::DBObj::Customer->new($customer_id);
    my $sql = 'SELECT count(1) FROM pos WHERE customer_id = ?';

    my $qo = OOCGI::Query->new($sql, { bind => $customer_id });
    if(my @count_data =  $qo->next_array)
    {
        my $customer_count = $count_data[0];
        if($customer_count > 0)
        {
            USAT::DeviceAdmin::UI::DAHeader->printHeader();
            USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
			print $user_menu;
            print "Can not delete this customer because there are $customer_count devices currently assigned to it.  <br>Please reas
sign all the devices to a different customer then retry.\n";
            USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
            exit;
        }

        $customer->customer_active_yn_flag('N');
        $customer = $customer->update;

        print CGI->redirect("/edit_customer.cgi?customer_id=$customer_id");
        exit();
    } else {
        USAT::DeviceAdmin::UI::DAHeader->printHeader();
        USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
		print $user_menu;
        print "check_delete_stmt failed!\n";
        USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        exit;
    }
}

if($PARAM{myaction} eq 'Undelete') {
    my $customer_id = $PARAM{customer_id};
    my $customer = USAT::DeviceAdmin::DBObj::Customer->new($customer_id);
    $customer->customer_active_yn_flag('Y');
    $customer = $customer->update;

    print CGI->redirect("/edit_customer.cgi?customer_id=$customer_id");
    exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $customer_id = $PARAM{customer_id};
if(length($customer_id) == 0 && !$PARAM{customer_name})
{
    print "Required Parameter Not Found: customer_id";
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
    exit;
}

my $customer;
if($PARAM{myaction} eq 'Save New') {
   my $hash = $query->obj_param('USAT::DeviceAdmin::DBObj::Customer');

   my $qo = OOCGI::Query->new;

   my $dbh = $qo->dbh;
   my $sth = $dbh->prepare(q{begin ? := pkg_customer_maint.add_customer(?,?,?,?,?,?,?,?,?,?,?,?); end;})
   or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
   my $c = 1;
   $customer_id = '';
   $sth->bind_param_inout($c++,\$customer_id,20);
   $sth->bind_param($c++,$hash->{customer_name});
   $sth->bind_param($c++,$hash->{customer_addr1});
   $sth->bind_param($c++,$hash->{customer_addr2});
   $sth->bind_param($c++,$hash->{customer_city});
   $sth->bind_param($c++,$hash->{customer_county});
   $sth->bind_param($c++,$hash->{customer_postal_cd});
   $sth->bind_param($c++,$hash->{customer_state_cd});
   $sth->bind_param($c++,$hash->{customer_country_cd});
   $sth->bind_param($c++,$hash->{customer_type_id});
   $sth->bind_param($c++,$hash->{app_user_name});
   $sth->bind_param($c++,$hash->{email_addr});
   $sth->bind_param($c++,$hash->{subdomain});

   eval {
      $sth->execute(); 
   };
   if ($@) {
       print "<br><br>Statement execution error: " . $@ . "<br><br>";
   };
   $customer = USAT::DeviceAdmin::DBObj::Customer->new($customer_id);
#  foreach my $key ( keys %{$hash} ) {
#      print "$key|$hash->{$key}<br>";
#  }
}

my $sql;
my $qo;
if($customer_id) {
   $sql = 'SELECT customer_id FROM customer where customer_id = ?';
#  $qo  = OOCGI::Query->new($sql, { bind => $PARAM{customer_id} });
   $qo  = OOCGI::Query->new($sql, { bind => $customer_id });
} else {
   $sql = 'SELECT customer_id FROM customer where customer_name = ?';
   $qo  = OOCGI::Query->new($sql, { bind => $PARAM{customer_name} });
}

my $bgcolor = "#C0C0C0";
my $action_type;
if(my %hs = $qo->next_hash) {
   $customer = USAT::DeviceAdmin::DBObj::Customer->new($hs{customer_id});
#  $bgcolor = "#FFFF00";
   $action_type = 'Edit';
} else {
   $customer = USAT::DeviceAdmin::DBObj::Customer->new();
   $customer->customer_name($PARAM{customer_name});
   $customer->customer_country_cd('US');
   $customer->customer_state_cd('AL');
   $customer->customer_type_id('10');
#  $bgcolor = "#33FF33";
   $action_type = 'New';
}


if($PARAM{myaction} eq 'Save' && ($PARAM{customer_id} != 1 || $PARAM{customer_name} ne 'Unknown')) {
   my $custname = $PARAM{$customer->oocgi_nameof('customer_name')};
   if($custname eq $customer->customer_name) {
      $customer = $customer->update($query);
   } else {
      my $sql = 'SELECT * FROM customer WHERE customer_name = ?';
      my $qo  = OOCGI::Query->new($sql, { bind => $custname });
      if(my %hash = $qo->next_hash) {
         print B("<br>CUSTOMER => $custname <= ALREADY EXISTS<br><br>",'RED',4);
      } else {
         $customer = $customer->update($query);
      }
   }
}

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

my $table_rn = 0;

print q|<form method="post">|;
my $name = $customer->customer_name;
my $customer_country_cd     = $customer->customer_country_cd;
my $customer_state_cd = $customer->customer_state_cd;

my $count_sql = q{
   SELECT count(1)
     FROM device,
          device_type,
          pos,
          device_setting
    WHERE device.device_type_id = device_type.device_type_id
      AND device.device_id = pos.device_id
      AND pos.customer_id = ?
      AND device.device_active_yn_flag = 'Y'
      AND device.device_id = device_setting.device_id (+)
      AND device_setting.device_setting_parameter_cd(+) =  
   DECODE(device.device_type_id, 11, 'Public_PC_Version', 'Firmware Version') };

my $count = OOCGI::Query->new($count_sql, { bind => [ $customer->ID ] } )->node(0,0);

$table->put($table_rn,0,"$action_type Customer - $name ( $count Devices )",qq|colspan=4 class="header0"|);
$table_rn++;

$table->put($table_rn,0,'Name','class=leftheader width=24%');
if($action_type eq 'New') {
   $table->put($table_rn,1,$customer->Text('customer_name', { size => 30, readonly => 1}));
} else {
   $table->put($table_rn,1,$customer->Text('customer_name', { size => 30}));
}
$table->put($table_rn,2,'Type','class=leftheader width=14%');
$sql = 'SELECT customer_type_id, customer_type_desc
          FROM customer_type
      ORDER BY customer_type_desc';
my $popType = $customer->Popup('customer_type_id', { id => 'customer_type_id',
                               sql => $sql, style => "font-size: 11px;"});
$popType->head('','Undefined');
$table->put($table_rn,3, $popType);
$table_rn++;

$table->put($table_rn,0,'Address Line 1', 'class=leftheader');
$table->put($table_rn,1, $customer->Text('customer_addr1', { size => 30}));
$table->put($table_rn,2,'City', 'class=leftheader');
$table->put($table_rn,3, $customer->Text('customer_city', { size => 30}));
$table_rn++;

$table->put($table_rn,0,'Address Line 2', 'class=leftheader');
$table->put($table_rn,1, $customer->Text('customer_addr2', { size => 30}));
$table->put($table_rn,2,'State', 'class=leftheader');
$sql = "SELECT state_cd, state_name
          FROM state
         WHERE country_cd = '$customer_country_cd'
      ORDER BY state_name";
my $popState = $customer->Popup('customer_state_cd', { sql => $sql, id => "state"});
$popState->head(' ','Undefined');
my $buttonNewState = qq|<input type="button" value="New State" onClick="document.location='new_state.cgi';">|;
$table->put($table_rn,3,'<div id=statecd>'.$popState.'</div>'.$buttonNewState);
$table_rn++;

$table->put($table_rn,0,'County', 'class=leftheader');
$table->put($table_rn,1, $customer->Text('customer_county', { size => 30}));
$table->put($table_rn,2,'Postal Code', 'class=leftheader');
$table->put($table_rn,3, $customer->Text('customer_postal_cd', { size => 30}));
$table_rn++;

$table->put($table_rn,0,'Country', 'class=leftheader');

my  $popCustomer = OOCGI::Popup->new(
                   object => $customer, field => 'customer_country_cd',
                   style => "font-size: 11px;",
                   onchange => "setTheValue('')",
                   id => "country");

$sql = "SELECT country_cd, country_name
          FROM country
      ORDER BY country_name";

my $qo2 = OOCGI::Query->new($sql);
while(my @arr = $qo2->next_array) {
   if($arr[0] eq 'AU' or $arr[0] eq 'US') {
      $popCustomer->head($arr[0],$arr[1]);
   } else {
      $popCustomer->tail($arr[0],$arr[1]);
   }
}

$popCustomer->head('','Undefined');
$popCustomer->default($customer->customer_country_cd);

if($action_type eq 'New') {
   $table->put($table_rn,1, $popCustomer,'colspan=3');
} else {
   $table->put($table_rn,1, $popCustomer);
   $table->put($table_rn,2,'Deleted');
   $table->put($table_rn,3, ($customer->customer_active_yn_flag eq "Y" ? "No" : "Yes"), 'colspan=3');
}
$table_rn++;
if($action_type eq 'New') {
   $table->put($table_rn,0,B('eSuds Report Website Options (all fields optional)'),
                    'colspan=4 class=header0');
   $table_rn++;
   $table->put($table_rn,0,B('User Name (i.e. e-mail)'), 'class=leftheader');
   $table->put($table_rn,1, $customer->Text('app_user_name',{ sz_ml => [30,40]}));
   $table->put($table_rn,2,B('E-mail Address'), 'class=leftheader');
   $table->put($table_rn,3, $customer->Text('email_addr', {  sz_ml => [30,40] }));
   $table_rn++;
   $table->put($table_rn,0,B('New Sub-domain for user'), 'class=leftheader');
   $table->put($table_rn,1, $customer->Text('subdomain',  {  sz_ml => [30,40]}));
   $table_rn++;
}
my $str;
if ($customer->customer_active_yn_flag eq "Y")
{
   my $CID = $customer->ID;
   if($CID == 1) {
   $str = qq|<input type=button value="List Devices" onClick="javascript:window.location = '/device_list.cgi?customer_id=$CID';">|;
   } else {
      if($count > 0) {
   $str = qq|<input type="submit" name="myaction" value="Save">
             <input type="submit" name="myaction" value="Delete" onClick="return confirmSubmit('Delete')">
             <input type=button value="List Devices" onClick="javascript:window.location = '/device_list.cgi?customer_id=$CID';">|;
      } else {
   $str = qq|<input type="submit" name="myaction" value="Save">
             <input type="submit" name="myaction" value="Delete" onClick="return confirmSubmit('Delete')">
             <input type=button value="List Devices" disabled>|;
      }
  }
} else {
   if($action_type eq 'New') {
   $str = qq|<input type="submit" name="myaction" value="Save New" onClick="return validateSubmit()">|;
   } else {
      my $CID = $customer->ID;
      if($CID != 1) {
         $str = qq|<input type="submit" name="myaction" value="Undelete" onClick="return confirmSubmit('Undelete')">|;
      }
   }
}
$table->put($table_rn,0, $str,'align=center colspan=4');
$table_rn++;


my $CID = $customer->ID;
print qq|
<input type="hidden" name="customer_id" value="$CID">
<input type="hidden" name="customer_name" value="$name">
<script LANGUAGE="JavaScript">
<!--
var defval;
var flag = 1;
var strval = '<select name="|.$customer->oocgi_nameof('customer_state_cd').qq|" tabindex="1" id="state">';
function setTheValue(val){
    obj = document.getElementById('state');
    country = document.getElementById('country');
    if(flag == 1) {
       flag = 0;
       defval = document.getElementById('statecd').innerHTML;
    }
    if(country.value == '$customer_country_cd') {
       val = '$customer_state_cd';
       document.getElementById('statecd').innerHTML = defval;
|;
my $state_sql = 'SELECT state_cd, state_name, country_cd FROM state ORDER BY country_cd';

my $state_qo = OOCGI::Query->new($state_sql);

my @states;
my $strhead = qq|document.getElementById('statecd').innerHTML = strval +
       '<option value="">Undefined</option>'|;
$states[0] = $strhead;
while(my @arr = $state_qo->next_array) {
    push(@states, qq|'<option value="$arr[0]">$arr[1]</option>'|);
    if(my @nextarr = $state_qo->peek) {
       if($arr[2] ne $nextarr[2]) {
          push(@states, qq|'</select>'|);
          my $str = join('+',@states);
          print qq|} else if(country.value == '$arr[2]') {
             $str|;
          @states = ();
          $states[0] = $strhead;
       }
    } else {
       push(@states, qq|'</select>'|);
       my $str = join('+',@states);
       print qq|
       } else if(country.value == '$arr[2]') {
          $str|;
    }
}
print qq|
    } else {
       document.getElementById('statecd').innerHTML = strval + '<option value="">Undefined</option></select>';
    }
    obj.value = val;
}
    function confirmSubmit(myaction)
    {
        var agree;
        switch(myaction)
        {
            case 'Delete':
            {
                agree=confirm("To delete a customer record you must first reassign all it's device to a different customer.  If you have not done this already, click Cancel and do so now.\\n\\nContinue to delete $name?");
                break;
            }
            case 'Undelete':
            {
                agree=confirm("OK to undelete $name?");
                break;
            }
        }
        if (agree)
            return true ;
        else
            return false ;
    }

    function validateSubmit()
    {
        var flag = document.getElementById('customer_type_id').value;
        if (flag > 0) {
            return true;
        } else {
            alert('You must select a Customer TYPE');
            return false;
        }
    }
  // -->
  </script>|;

display $table;
print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
$query->DESTROY(1);
