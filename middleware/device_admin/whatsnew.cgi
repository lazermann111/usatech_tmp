#!/usr/local/USAT/bin/perl

use strict;
use CGI::Carp qw(fatalsToBrowser);
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader;
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html"); 

print $user_menu;

print q|
<style>
div.notes {
text-align: justify;
margin:0 10%;
color:#000fff;
background-color:#00FFFF;
}
h1,h2,p{margin: 0 10px}
h1{font-size: 150%;color: #330000}
h2{font-size: 120%;color: #000}
p{padding-bottom:1em}
h2{padding-top: 0.3em}
</style>
<link rel="stylesheet" type="text/css" href="/nifty/niftyCorners.css">
<link rel="stylesheet" type="text/css" href="/nifty/niftyPrint.css" media="print">
<script type="text/javascript" src="/nifty/nifty.js"></script>
<script type="text/javascript">
window.onload=function(){
if(!NiftyCheck())
    return;
Rounded("div#nifty","all","#FFF","#00FFFF","smooth");
}
</script>
<div id=nifty class=notes>
<div style="text-align: center;"><span
 style="font-weight: bold;">Welcome to the USATech Device
Administration System</span><br>
</div>
<pre><br><span style="font-weight: bold;">Our Device Administration System Menu Has Changed:</span><br><br>The purpose of this change can be summarized as follows:<br><br></pre>
<ol>
  <li> To try to make the most of the device admin functions
available in the dynamic menu system; minimizing back button usage.</li>
  <li> To organize profile pages in logical units; better
organization of device information.</li>
  <li> The use of cache for frequently used database access
operations by increasing performance and reducing database access.</li>
  <li>To unify different device profiles under one common
architecture by using object oriented concepts, in Perl, and to reduce
development time and increase maintainability.</li>
  <li> To add Google style navigation and column sorting to large
reports and to find required information as quickly as possible.</li>
  <li> To add new features to device administration for</li>
  <ul>
    <li> Reducing manual work</li>
    <li> Reducing erroneous data entry</li>
    <li> Detecting problems with visual coloring system</li>
  </ul>
</ol>
<pre><br>Lets refine our itemized list above with detailed explanations of each items.<br></pre>
<hr style="width: 100%; height: 2px;"><span
 style="font-weight: bold;">ITEM 1:</span><br>
<div style="text-align: justify;">
When a user accesses the device administration screen they will notice
the new yellow background dynamic menu at the top of the screen. We
call this menu
dynamic because the content changes according to what the users mouse
pointing at, and also it
moves up and down when the user scrolls on large pages. The new menu
system has a built in tool tips which explains each action
that the user is going to perform. We provided most of the tool tips
and would
appreciate suggestions to add more comments making the tool much
more user friendly. This menu by itself contains much of the old
device administration screens. By clicking the, <span
 style="font-weight: bold;">Devices</span>, <span
 style="font-weight: bold;">Locations</span>,
<span style="font-weight: bold;">Customer</span> and
<span style="font-weight: bold;">Files</span>, menus
the user can immediately access the Search function and
links
related to those menus. Notice that when the user selects any one of
these menus the drop
down menu stays open as long as 30 seconds or until another
menu item is selected, this gives the user enough time to enter
information
into the search fields.
</div>
<hr style="width: 100%; height: 2px;"><span
 style="font-weight: bold;">ITEM 2:</span><br>
<div style="text-align: justify;">Once the user selects <span
 style="font-weight: bold;">by Type</span> from the <span
 style="font-weight: bold;">Devices</span> menu and
accesses the profile
pages of that particular device, they will be shown a tabbed layout of
the new device administration screen. Notice that the first tab is
"<span style="background-color: rgb(0, 204, 204);">Device
Profile</span>" and it has
a different background color than the usual yellow menu color. This
color change is a hint, allowing the user to know which section of the
profile they are on. Notice
that under this tabbed menu there is an information line describing the
properties of the device that are available to the user under each
tabbed screen. The tabbed menu
system has a built in memory (session ID) and remembers where the user
was last on the device menu. If you wander off from profile
&nbsp;page and later return to it during same session, it will take
you to the tap you left off.
</div>
<hr style="width: 100%; height: 2px;"><span
 style="font-weight: bold;">ITEM 3:</span><br>
<div style="text-align: justify;">The new device
admistration program keeps less frequently changed database information
in its cache. The Devices (G4) and (G5), Location and
Customer lists, which change very infrequently, however are accessed
frequenlty through the device profile is a great example of information
in cache. The user is
able to refresh cached information under Menu Refresh and all but the
location list is available under menu system. The location list is the
largest list, making the
dynamic menu very slow when navigating from one screen to another. To
overcome this,
the Location list is hidden under menu system and will not be shown
unless the user
asks for it. For access, "<span style="font-weight: bold;">by
Location</span>" under the <span style="font-weight: bold;">Devices</span>
menu must be
selected; this will allow the user temporary access to Locations.
</div>
<hr style="width: 100%; height: 2px;"><span
 style="font-weight: bold;">ITEM 4:</span><br>
<div style="text-align: justify;">This section is for
development purposes and has no effect on the actual user.
It is important for the user to know that all of the different devices
now have
the same profile pages generated by a common program.
</div>
<hr style="width: 100%; height: 2px;"><span
 style="font-weight: bold;">ITEM 5:</span><br>
<div style="text-align: justify;">The new report
pagination system gives the user more flexibility to navigate
through large reports. Each report has Google style pagination at the
the bottom of the page allowing the user to jump to the next, previous,
first, last or
numbered pages. Currently, each page has a predetermined number of
records that
is displayed, 15, and also displays the total number of records found.
Column headers
for these pages allow the user to sort in ascending or descending
order. One hidden feature of this sorting allows the user to use two
columns
implicitly. For example, if column A is sorted first and then the user
does another sort in column B, the system will sort column B
accordingly
however will remember the sort routine in column A and order them in
such a way that column A and B are then sorted together. (Example:
First search
in column A is by age in descending order therefore the oldest person
will be at the very top. The second search in column B is by last name
and there are four Smith's on column B, the oldest Smith on the list
will be at the very top and the
youngest at the bottom.) In effect, the sorting routine remembers what
column you previously
sorted while doing the current column sort.
<br>
<br>
</div>
Here are sample screens that use these pagination and sorting systems: <br>
<br>
<a href="device_list.cgi?enabled=%27Y%27&amp;action=Search">Device List</a><br>
<a href="location_list.cgi?location_name=">Location List</a><br>
<a href="customer_list.cgi?customer_name=">Customer List</a><br>
<a href="file_list.cgi?file_name=">File List</a>
<hr style="width: 100%; height: 2px;"><span
 style="font-weight: bold;">ITEM 6:</span><br>
<div style="text-align: justify;">In this version of
device administration new features such as serial number
generation and physically swapped device cloning has been added to the
system. These utilities eliminated manual work provided to do these
jobs. You can access these new utilities under:
<br>
</div>
<br>
Devices &gt;&gt; Advanced Tools &gt;&gt; <a href="serial_number.cgi">Serial Number Allocation</a><br>
Devices &gt;&gt; Advanced Tools &gt;&gt; <a href="profile_clone.cgi">Device Cloning</a><br>
<br>
<div style="text-align: justify;">Some user actions may
require more attention then the others. For
example a delete operation which removes a record from database needs
more attention than a report which only displays information from the
database. Some confirmation routines have been added to some of the
buttons
which causes to alter the status of a device. For example the <span
 style="background-color: rgb(2, 5, 2); color: rgb(51, 204, 0);">Toggle</span><span
 style="color: rgb(51, 204, 0);">
</span>button in the Device Profile screen is one of those
buttons. If you see a
confirmation alert please think twice and make sure that the action you
are confirming
is what you want to do.
<br>
</div>
<br>
<div style="text-align: justify;">Some reports display
information about the status of a device and what
the user notices in that report may require some action on their part
to make
sure device is working properly. If the report does not show
the status changes more explicitly, you may not notice the
change. To help you to visually detect these changes a <span
 style="background-color: rgb(255, 0, 0);">red</span>
background
has been added where these changes occur. A good example of these are
the
ePort Device Setting counter color coding scheme to identify
potentially problematic changes in device behavior.
</div>
</div>
|;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
