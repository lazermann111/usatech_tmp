#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use USAT::App::DeviceAdmin::Const qw(:globals);
use OOCGI::NTable;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::USAPopups;
use OOCGI::PageNavigation;
use OOCGI::PageSort;
use USAT::DeviceAdmin::Util;
use Net::SMTP;
use USAT::DeviceAdmin::UI::EsudsPrivileges;
use USAT::DeviceAdmin::UI::DAHeader;

use strict;
my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $SQLB = qq|SELECT DISTINCT au.app_user_id,
                     au.app_user_fname,
                     au.app_user_lname,
                     au.app_user_email_addr
                FROM app_user.app_user au,
                     app_user.app_user_object_permission aoup
               WHERE au.app_user_id > 0
                 AND au.app_user_id = aoup.app_user_id
|;

my $appuser = USAT::DeviceAdmin::DBObj::App_user->new;
$appuser->upload($query);

my $appuser_fname          = $appuser->app_user_fname;
my $appuser_lname          = $appuser->app_user_lname;
my $appuser_email_addr     = $appuser->app_user_email_addr;

my %PARAM = $query->Vars();
my %B = ( C => '%', B => '',  E => '%' );
my %E = ( C => '%', B => '%', E => ''  );

use constant select_sql => '
      SELECT *
        FROM app_user.app_user au,
             app_user.app a,
             app_user.app_user_object_permission aoup
       WHERE au.app_user_id = ?
         AND au.app_user_id = aoup.app_user_id
         AND a.app_id = aoup.app_id
         AND a.app_id = ?';

use constant custtomer_base_sql => q{
SELECT   c.customer_name, 
         lp.location_name ancestor_location_name,
         lc.location_name descendent_location_name
    FROM LOCATION.LOCATION lp, LOCATION.LOCATION lc, cust_loc cl, customer c
   WHERE lp.location_id = lc.parent_location_id
     AND cl.location_id = lc.location_id
     AND c.customer_id = cl.customer_id
     AND c.customer_id IN (select object_cd 
                             from app_user_object_permission 
                            where app_user_id = ?
                              and app_id = ? 
                              and app_object_type_id = ?) /* appropriate customer_id(s) */
ORDER BY LOWER (customer_name),
         LOWER (lp.location_name),
         LOWER (lc.location_name)};

if($PARAM{userOP} eq 'Submit') {
   &USAT::DeviceAdmin::UI::EsudsPrivileges::update_function();
}

my $popup = OOCGI::Popup->new();
$popup->tail('C','Contains');
$popup->tail('B','Begins');
$popup->tail('E','Ends');

print <<SCRIPTS;
<style>
/* regular button, hover */
.btn {
   background-color:#FFFFFF;text-align:left; border:0;color:blue;width:100%;
}
/* regular button, hover */
.btn:hover{
     color:white;
     background-color:red;
}
.normal { background-color: #ffffff; color: #000000; }
.focus  { background-color: #cccc00; color: #000000; }
</style>
<script>
function toggleColor(objElement)
{
   if (objElement.className=='normal') {
       objElement.className='focus';
//alert('AAA ' + objElement.className);
    } else {
       objElement.className='normal';
   }
}
// if user select a customer and a location and checkes a checkbox
// for insertion he can press submit buttons, however if he changes
// his mind and select undefined customer, system will reset it self
// so that user can't insert a value
function confirmCustomer(str)
{
   var cus_obj = document.getElementById(str+'customer_id_div');
   if(cus_obj.value == 1) {
      document.myForm.reset();
   }
}

// if user select a customer and a location and checkes a checkbox
// for insertion he can press submit buttons, however if he changes
// his mind and select undefined location, system will reset it self
// so that user can't insert a value
function confirmLocation(loc_prefix)
{
   var loc_obj = document.getElementById(loc_prefix+'__location_id_div');
   if(loc_obj.value == 1) {
      document.myForm.reset();
   }
}

// in order user to search something he/she should at list enter
// a first name, last name or email, if these fields are
// empty, then there will be no search
function confirmSubmit()
{
   obj1 = document.getElementById('checkbox1_id');
   obj2 = document.getElementById('checkbox2_id');
   obj3 = document.getElementById('checkbox3_id');
   obj5 = document.getElementById('checkbox5_id');
   obj6 = document.getElementById('checkbox6_id');

   if(!obj1.checked && !obj2.checked && !obj3.checked && !obj5.checked && !obj6.checked ) { 
     alert('You must check at least one checkbox!');
     return false ;
   }
   
   flag = 0;
   obj = document.getElementById('id_app_user_fname');
   if(obj.value != '') { flag = 1; }
   obj = document.getElementById('id_app_user_lname');
   if(obj.value != '') { flag = 1; }
   obj = document.getElementById('id_app_user_email_addr');
   if(obj.value != '') { flag = 1; }
   if (flag) {
       return true ;
   } else {
     alert('You must enter a value in least one of the form fields.');
     return false ;
   }
}

// when a user wants to check a insertsion box for insert purposes
// this function will make sure that customer and location are
// checked, othervise no box will be checked.
function confirmSubmit1(operation_type)
{
   var cus_obj = document.getElementById(operation_type + '__customer_id_div');
   var customer_value = cus_obj.value;

   if(cus_obj.value != 1) {
      if(operation_type != 'operator_reports') {
         var loc_obj = document.getElementById(operation_type + '__location_id_div');
         var location_value = loc_obj.value;
     
         if(loc_obj.value != 1) { 
             return true ;
         } else {
           alert('Select a location to check this box!.');
           document.myForm.reset();
           loc_obj.value = location_value;
           cus_obj.value = customer_value;
           return false ;
         }
      }
   } else {
     alert('First select a customer to check this box!.');
     document.myForm.reset();
     cus_obj.value = customer_value;
     return false ;
   }
}

function confirmSubmit2()
{
   var str = 'Are you sure you wish to continue?';
   var agree=confirm(str);
   if (agree)
     return true ;
   else
     document.myForm.reset();
     return false ;
}

function runThis(key)
{
    document.myForm.app_user_id.value = key;
    document.myForm.submit();
    return false;
}

function display_message1() {
   location_div1.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src=/img/progress_bar.gif></b>';
}
function display_message2() {
   location_div2.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src=/img/progress_bar.gif></b>';
}
function display_message3() {
   location_div3.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src=/img/progress_bar.gif></b>';
}
function display_message3s() {
   location_div3s.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src=/img/progress_bar.gif></b>';
}
function display_message4() {
   location_div4.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src=/img/progress_bar.gif></b>';
}
function get_location1(notinthis) {
   var idValue = document.getElementById("user_admin_reports__customer_id_div").value;
   var url1 = "rpc_location.cgi?notinthis="+notinthis+"&location_div_prefix=user_admin_reports&location=S&param=";
   getAXAHUpdateDiv(url1 + escape(idValue),'location_div1');
}

function get_location2(notinthis) {
   var idValue = document.getElementById("user_maintenance__customer_id_div").value;
   var url2 = "rpc_location.cgi?notinthis="+notinthis+"&location_div_prefix=user_maintenance&location=SC&param=";
   getAXAHUpdateDiv(url2 + escape(idValue),'location_div2');
}

function get_location3(notinthis) {
   var idValue = document.getElementById("operator_access_campus__customer_id_div").value;
   var url3 = "rpc_location.cgi?notinthis="+notinthis+"&location_div_prefix=operator_access_campus&location=C&param=";
   getAXAHUpdateDiv(url3 + escape(idValue),'location_div3');
}
function get_location3s(notinthis) {
   var idValue = document.getElementById("operator_access_school__customer_id_div").value;
   var url3s = "rpc_location.cgi?notinthis="+notinthis+"&location_div_prefix=operator_access_school&location=S&param=";
   getAXAHUpdateDiv(url3s + escape(idValue),'location_div3s');
}

function get_location4(notinthis) {
   var idValue = document.getElementById("operator_reports__customer_id_div").value;
   var url4 = "rpc_location.cgi?notinthis="+notinthis+"&location_div_prefix=operator_reports&location=S&param=";
   getAXAHUpdateDiv(url4 + escape(idValue),'location_div4');
}
</script>
SCRIPTS


my @binds = ();
my $txt = "%%";


my $style1 = 'style="{background-color:#999999; font-weight:bold;}"';
my $style3 = 'style="{text-align:right;}"';
#----------------------------------------
if($PARAM{myaction} eq 'Search') {
   if($appuser_fname) {
      my $txt = lc($appuser_fname);
      my $be  = $PARAM{popup_fname};
      $SQLB .= " AND LOWER(au.app_user_fname) like ? ";
      push(@binds,"$B{$be}$txt$E{$be}");
   }
   if($appuser_lname) {
      my $txt = lc($appuser_lname);
      my $be  = $PARAM{popup_lname};
      $SQLB .= " AND LOWER(au.app_user_lname) like ? ";
      push(@binds,"$B{$be}$txt$E{$be}");
   }
   if($appuser_email_addr) {
      my $txt = lc($appuser_email_addr);
      my $be  = $PARAM{popup_email};
      $SQLB .= " AND LOWER(au.app_user_email_addr) like ? ";
      push(@binds,"$B{$be}$txt$E{$be}");
   }
 
   my @arr = (); 
   if($PARAM{chkbox1} == 1) {
      push(@binds,$PARAM{chkbox1});
      push(@arr,'?');
   }
   if($PARAM{chkbox2} == 2) {
      push(@binds,$PARAM{chkbox2});
      push(@arr,'?');
   }
   if($PARAM{chkbox3} == 3) {
      push(@binds,$PARAM{chkbox3});
      push(@arr,'?');
   }
   if($PARAM{chkbox5} == 5) {
      push(@binds,$PARAM{chkbox5});
      push(@arr,'?');
   }
   if($PARAM{chkbox6} == 6) {
      push(@binds,$PARAM{chkbox6});
      push(@arr,'?');
   }
   my $str = join(',',@arr);
#  $str =~ s/[1,2,3,5,6]/\?/g;
   $SQLB .= " AND aoup.app_id IN ($str)";
}
#----------------------------------------
#print "SQL $SQLB <br>";
my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
my $table_rn = 0;

my $style2 = 'style="{background-color:#FFFFFF}"';

print q{ <form name=myForm method="post"> };

$table->put($table_rn,0,'eSuds Priviliges Screen','class=header0 width=24% colspan=2');
$table_rn++;
$table->put($table_rn,0,'App User First Name','class=leftheader width=24%');
$popup->name('popup_fname');
if($PARAM{popup_fname}) {
   $popup->default($PARAM{popup_fname});
}
$table->put($table_rn,1, $popup.$appuser->Text('app_user_fname', { id => 'id_app_user_fname', size => 40}));
$table_rn++;

$table->put($table_rn,0,'App User Last Name','class=leftheader width=24%');
$popup->name('popup_lname');
if($PARAM{popup_lname}) {
   $popup->default($PARAM{popup_lname});
}
$table->put($table_rn,1, $popup.$appuser->Text('app_user_lname', { id => 'id_app_user_lname', size => 40}));
$table_rn++;

$table->put($table_rn,0,'App User Primary Email','class=leftheader width=24%');
$popup->name('popup_email');
if($PARAM{popup_email}) {
   $popup->default($PARAM{popup_email});
}
$table->put($table_rn,1, $popup.$appuser->Text('app_user_email_addr', { id => 'id_app_user_email_addr', size => 40}));
   
my $checkbox1 = OOCGI::Checkbox->new( name => 'chkbox1', value => '1',
                label => 'Student Access', id => 'checkbox1_id');
my $checkbox2 = OOCGI::Checkbox->new( name => 'chkbox2', value => '2',
                label => 'Operator Access', id => 'checkbox2_id');
my $checkbox3 = OOCGI::Checkbox->new( name => 'chkbox3', value => '3',
                label => 'User Maintanence', id => 'checkbox3_id');
my $checkbox5 = OOCGI::Checkbox->new( name => 'chkbox5', value => '5',
                label => 'Operator Reports', id => 'checkbox5_id');
my $checkbox6 = OOCGI::Checkbox->new( name => 'chkbox6', value => '6',
                label => 'Admin Reports', id => 'checkbox6_id');
if(!$PARAM{myaction}) {
   $checkbox2->checked('yes');
   $checkbox3->checked('yes');
   $checkbox5->checked('yes');
   $checkbox6->checked('yes');
} else {
   if($PARAM{chkbox1} == 1) {
      $checkbox1->checked('yes');
   }
   if($PARAM{chkbox2} == 2) {
      $checkbox2->checked('yes');
   }
   if($PARAM{chkbox3} == 3) {
      $checkbox3->checked('yes');
   }
   if($PARAM{chkbox5} == 5) {
      $checkbox5->checked('yes');
   }
   if($PARAM{chkbox6} == 6) {
      $checkbox6->checked('yes');
   }
}
my $boxtable = OOCGI::NTable->new("width=60%");
$boxtable->put(0,0,$checkbox6);
$boxtable->put(0,1,$checkbox1,'align="right"');
$boxtable->put(1,0,$checkbox3);
$boxtable->put(2,0,$checkbox2);
$boxtable->put(3,0,$checkbox5);
$table->put(4,0,'Restrict Search by<br>Privilige Type','class="leftheader"');
$table->put(4,1,$boxtable,'align="left"');

display $table;

my $app_user_id = '';
if(($PARAM{app_user_id} || $PARAM{userOP}) && $PARAM{myaction} ne 'Search') {
   $app_user_id = $PARAM{app_user_id};
   my $appuser = USAT::DeviceAdmin::DBObj::App_user->new($PARAM{app_user_id});

   print  qq|<br><input type="button" value="< Back" onClick="javascript:history.go(-1);">|;
   print  qq|<input type="submit" name="myaction" value="Search" onClick="return confirmSubmit()"><br><br>|;

   my $yesBox = OOCGI::Radio->new(name => 'send_email', value => 'Y', label => 'Yes');
   my $nooBox = OOCGI::Radio->new(name => 'send_email', value => 'N', label => 'No');
   $nooBox->checked;

   my $account_holder = B($appuser->app_user_fname,'green',4).' '.B($appuser->app_user_lname,'green',4);

#   print B("Notify $account_holder regarding privilege changes via e-mail? ",3),$yesBox->string,$nooBox->string;
#   BR(2);

   my $table_rn = 0;
   my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');

   my $submit =  SUBMIT('userOP','Submit', { inclick => 'return confirmSubmit2()'});
   $table->put($table_rn++,0,$submit,'align=center colspan=6');

   #  USER ADMIN REPORTS
   $table_rn = &USAT::DeviceAdmin::UI::EsudsPrivileges::user_admin_reports_table($table,$table_rn );

   #  CASE FOR USER MAINTENANCE,
   $table_rn = &USAT::DeviceAdmin::UI::EsudsPrivileges::user_maintenance_table($table,$table_rn );

   #  OPERATOR ACCESS SCHOOL
   $table_rn = &USAT::DeviceAdmin::UI::EsudsPrivileges::operator_access_school_table($table,$table_rn );
   #  OPERATOR ACCESS CAMPUS
   $table_rn = &USAT::DeviceAdmin::UI::EsudsPrivileges::operator_access_campus_table($table,$table_rn );

   #  OPERATOR REPORTS
   $table_rn = &USAT::DeviceAdmin::UI::EsudsPrivileges::operator_reports_table($table,$table_rn);
   
   $table->put($table_rn++,0,'&nbsp;','align=center colspan=6');
   $table->put($table_rn,0,$submit,'align=center colspan=6');

   my $user = OOCGI::NTable->new('border="1" width="60%" cellpadding="2" cellspacing="0" ');
   $user->put(0,0,'App User First Name','class=leftheader');
   $user->put(0,1,$appuser->app_user_fname);
   $user->put(1,0,'App User Last Name','class=leftheader');
   $user->put(1,1,$appuser->app_user_lname);
   $user->put(2,0,'App User Primary Email','class=leftheader');
   $user->put(2,1,$appuser->app_user_email_addr);
   $user->put(3,0,'Is App User Active','class=leftheader');
   my $active_yn = $appuser->app_user_active_yn_flag eq 'Y' ? 'Yes' : 'No';
   $user->put(3,1,$active_yn);
   display $user;
   BR(2);
   display $table;
} else {
   BR;
   SUBMIT('myaction','Search',{ onClick => 'return confirmSubmit()' });
   BUTTON('','New App User', { onclick => "javascript:window.location='/new_app_user.cgi'" });
   BR(2);
}


if($PARAM{myaction}) {
   my $pageNav  = OOCGI::PageNavigation->new(15);

   my %table_cols = ( #col_num => [col name, sql sort, default sort (1=asc, -1=desc)]
        1 => ['App User ID',          'app_user_id',                 -1],
        2 => ['First Name',           'LOWER(app_user_fname)',        1],
        3 => ['Last Name',            'LOWER(app_user_lname)',        1],
        4 => ['Email',                'LOWER(app_user_email_addr)',   1],
        5 => ['Edit Privileges',       undef,                        -1]
   );

my $pageSort = OOCGI::PageSort->new( default_sort_field => 1 * $table_cols{1}->[2] );
my ($sort_id, $sort_asc, $last_sort_id, $last_sort_asc) = $pageSort->get_sorted_columns();
$SQLB .= '    ORDER BY '.$table_cols{$sort_id}->[1].($sort_asc ? '' : ' DESC');
$SQLB .= ','.$table_cols{$last_sort_id}->[1].($last_sort_asc ? '' : ' DESC') if $last_sort_id;

my $qo   = $pageNav->get_result_obj($SQLB, \@binds);
   my $style1 = 'style="{background-color:#999999; font-weight:bold;}"';
   my $style3 = 'style="{text-align:right;}"';
   my $search = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
   my $search_cn = 0;
   foreach my $key (sort {$a <=> $b} keys %table_cols) {
       (my $text = $table_cols{$key}->[0]) =~ s/\s/&nbsp;/go;
       if (defined $table_cols{$key}->[1]) {
           $search->put(0,$search_cn++,$pageSort->sort_column($key * $table_cols{$key}->[2], $text),'class=headstyle');
       } else {
           $search->put(0,$search_cn++,$text,'class=headstyle');
       }
   }
   my $search_rn = 1;
   while(my @data = $qo->next) {
      my $search_cn = 0;
      my $CID = $data[0]; # app_user_id
      my $button = qq|<a href="javascript:window.location='/edit_app_user.cgi?app_user_id=$CID'">$CID</a>|;
      $search->put($search_rn,$search_cn++, $button,'style="{text-align:right;}"');
      $search->put($search_rn,$search_cn++, $data[1]);
      $search->put($search_rn,$search_cn++, $data[2]);
      $search->put($search_rn,$search_cn++, $data[3]);
      my $button = OOCGI::Button->new(value => 'Edit',
                   onclick => "javascript:runThis('$CID');");
      $search->put($search_rn,$search_cn++, $button);
      $search_rn++;
   }
   display $search;

   my $page_table = $pageNav->table;
   display $page_table;
   $pageNav->enable();
   $pageSort->enable();
}

HIDDEN('app_user_id',$app_user_id);
print q|</form>|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
