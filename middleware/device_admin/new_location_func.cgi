#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

my $name;
my $addr1;
my $addr2;
my $city;
my $county;
my $postal_cd;
my $parent_location_id;
my $country;
my $location_type_id;
my $state;
my $time_zone;

my $action = $PARAM{"action"};
if($action eq "Save")
{
	$name      = $PARAM{"name"};
	$addr1     = $PARAM{"addr1"};
	$addr2     = $PARAM{"addr2"};
	$city      = $PARAM{"city"};
	$county    = $PARAM{"county"};
	$postal_cd = $PARAM{"postal_cd"};
	$country   = $PARAM{"country"};
	$state     = $PARAM{"state"};
	$time_zone = $PARAM{"time_zone"};
	$location_type_id   = $PARAM{"location_type_id"};
	$parent_location_id = $PARAM{"parent_location_id"};
	
	my $insert_location_stmt = $dbh->prepare("insert into location.location(location_name, location_addr1, location_addr2, location_city, location_county, location_postal_cd, parent_location_id, location_country_cd, location_type_id, location_state_cd, location_time_zone_cd) values (:location_name, :location_addr1, :location_addr2, :location_city, :location_county, :location_postal_cd, :parent_location_id, :location_country_cd, :location_type_id, :location_state_cd, :location_time_zone_cd)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$insert_location_stmt->bind_param(":location_name", $name);
	$insert_location_stmt->bind_param(":location_addr1", $addr1);
	$insert_location_stmt->bind_param(":location_addr2", $addr2);
	$insert_location_stmt->bind_param(":location_city", $city);
	$insert_location_stmt->bind_param(":location_county", $county);
	$insert_location_stmt->bind_param(":location_postal_cd", $postal_cd);
	$insert_location_stmt->bind_param(":parent_location_id", $parent_location_id);
	$insert_location_stmt->bind_param(":location_country_cd", $country);
	$insert_location_stmt->bind_param(":location_type_id", $location_type_id);
	$insert_location_stmt->bind_param(":location_state_cd", $state);
	$insert_location_stmt->bind_param(":location_time_zone_cd", $time_zone);
	$insert_location_stmt->execute();
	$insert_location_stmt->finish();
	
	$dbh->disconnect;
    $session->destroy;
	print "Location: /\n";
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	exit();	
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
print "No Action Defined!";
$session->destroy;
$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
