#!/usr/local/USAT/bin/perl


use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::POS::API::PTA::Util;
use Date::Calc;
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;

use constant DEBUG => 0;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

our $dbh;
&main();

sub main {
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
	$dbh = $DATABASE->{handle};

	### print header and some javascript ###
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

    print $user_menu;

	print <<"	EOHTML";
      <script type="text/javascript" src="/js/milonic_local/testregex.js"></script>
	  <script language="JavaScript" type="text/javascript">
	  <!--
		function confirmSubmit() {
            var terminal = document.getElementById('terminal').value;
            if(terminal == '') {
               alert('Please Select A Terminal!');
               return false;
            }
			var agree=confirm("Are you sure you want to continue with this operation?");
			if (agree) {
                if(document.getElementById('override_amount_id').value == '' && document.getElementById('override_limit_id').value == '') {
                    return true;
                }
                if(document.getElementById('override_amount_id').value > 0 && document.getElementById('override_limit_id').value == '') {
                  alert('Please enter Override Limit');
                  return false;
                }
	            if(document.getElementById('override_amount_id').value.match('^[0-9]+(\\.[0-9]{0,2})?\$') == null) {
            		alert("Invalid Number in Override amount.\\n\\nValid Numbers are like 1, 1.2 or 1.25\\n Please Enter Valid Number.");
                    return false;
                }
	            if(document.getElementById('override_limit_id').value.match('^[0-9]+(\\.[0-9]{0,2})?\$') == null) {
            		alert("Invalid Number in Override limit.\\n\\nValid Numbers are like 1, 1.2 or 1.25\\n Please Enter Valid Number.");
                    return false;
                }
				return true;
			} else {
				return false;
            }
		}
		function adjust_days(myform) {
			// sets day field based on selected month and year
			var y = myform.toggle_year.options[myform.toggle_year.selectedIndex].value;
			var m = myform.toggle_month.selectedIndex;
			var oldDayIdx = myform.toggle_day.selectedIndex;
			var days;

			// find number of days in current month
			if ((m == 3) || (m == 5) || (m == 8) || (m == 10)) { days = 30; }
			else if (m == 1) {
				// check for leapyear - Any year divisible by 4, except those divisible by 100 (but NOT 400)
				if ((Math.floor(y/4) == (y/4)) && ((Math.floor(y/100) != (y/100)) || (Math.floor(y/400) == (y/400)))) { days = 29; }
				else { days = 28; }
			}
			else { days = 31; }

			// if (days in new month > current days) then we must add the extra days
			var curEndDateDayLength = myform.toggle_day.length;
			if (days > curEndDateDayLength) {
				myform.toggle_day.length = days;
				for (var i = curEndDateDayLength; i < days; i++) {
					myform.toggle_day.options[i].text = i + 1;
					myform.toggle_day.options[i].value = i + 1;
				}
			}

			// if (days in new month < current days) then we must delete the extra days
			if (days < myform.toggle_day.length) {
				if (oldDayIdx >= days) { myform.toggle_day.selectedIndex = days - 1; }
				myform.toggle_day.length = days;
			}
			return true;
		}
		function setCurrentDate(myform) {
			// changes the date selector menus to the current date
			var currentDate = new Date();
			myform.toggle_year.selectedIndex = 0;
			myform.toggle_month.selectedIndex = currentDate.getMonth();
			adjust_days(myform);
			return true;
		}
	  // -->
	  </script>
	EOHTML

	### determine action ###
        my $action = "";
        if(length $PARAM{myaction} > 0 ) {
           $action = $PARAM{myaction};
        } elsif( length $PARAM{action} > 0 ) {
           $action = $PARAM{action};
        } else {
	       $action = printError("Required Parameter Not Found: action or myaction");
        }
	if ($action eq 'Add New / Reactivate Disabled Type' || $action eq 'Add a New Type')
	{
		### check input variables ###
		print "<!-- ".Dumper(\%PARAM) if DEBUG;
		my ($callback_cgi, $device_id);
		foreach my $req_param ("device_id")
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print Dumper(eval "\$$req_param") if DEBUG;
		}

		### load list of available new types for this POS ###
		my $err;
		my @payment_subtypes = USAT::DeviceAdmin::Profile::PaymentType::get_available_payment_subtypes($dbh, $device_id, \$err);
		printError("<br><br>$err<br><br>") if defined $err;
		print Dumper(\@payment_subtypes)."-->" if DEBUG;
		printError("<br><h3>No unique payment types available to be added at this time.</h3><br><br>") unless scalar @payment_subtypes;
				
		### print form ###
		print <<"		EOHTML";
<table width="100%" border="1" cellpadding="2" cellspacing="0">
<form method="post" action="edit_pos_pta_settings_func.cgi" onSubmit="javascript:return confirmSubmit();">
<!--<input type="hidden" name="callback_cgi" value="$callback_cgi">-->
<input type="hidden" name="callback_cgi" value="profile.cgi">
<input type="hidden" name="device_id" value="$device_id">
 <tr>
  <th colspan="4" bgcolor="#C0C0C0">Point-of-Sale Payment Type - $action<br></th>
 </tr>
 <tr>
  <td nowrap>Payment Type</td>
  <td colspan="3">
		EOHTML
		if (scalar @payment_subtypes > 0)
		{
			print "  <select name=\"payment_subtype_id\">\n";
			print "   <option value=\"\">-- Choose a type --</option>\n";
			print "   <option value=\"$_->[0]\">$_->[1]</option>\n" foreach @payment_subtypes;
			print "  </select>\n";
		}
		else
		{
			print "   <input type=\"hidden\" name=\"payment_subtype_id\" value=\"N/A\">N/A\n";
		}
		print <<"		EOHTML";
  </td>
 </tr> 
 <tr>
  <td width="25%" nowrap>Device ID</td>
  <td width="25%">$device_id</td>
  <td width="25%">Duplicate settings from most recently disabled payment type of the same type and code (if&nbsp;any&nbsp;exist)</td>
  <td width="25%">
   <select name="pos_pta_duplicate_settings">
		EOHTML
		print "   <option value=\"$_->[0]\"" . ($_->[0] eq 'Y' ? ' selected' : '') . ">$_->[1]</option>\n" foreach (['Y', 'Yes'], ['N', 'No']);
		print <<"		EOHTML";
   </select>
  </td>
 </tr>
 <tr>
  <td colspan="4" align="center">
   <input type="submit" name="myaction" value="Add / Reactivate">&nbsp;
   <!--<input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='/$callback_cgi?device_id=$device_id';">-->
   <input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='profile.cgi?device_id=$device_id&tab=2';">
  </td>
 </tr>
</form>
</table>
		EOHTML
	}
	elsif ($action eq 'Edit Settings')
	{
		### check input variables ###
		print "<!-- ".Dumper(\%PARAM) if DEBUG;
		my ($callback_cgi, $device_id, $pos_pta_id);
		foreach my $req_param ("device_id", "pos_pta_id")
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print Dumper(eval "\$$req_param") if DEBUG;
		}

		### load settings for pos_pta and device ###
		my ($pos_pta_device_serial_cd,
			$device_serial_cd,
			$device_type_id,
			$pos_pta_encrypt_key,
			$pos_pta_encrypt_key2,
			$payment_subtype_key_id,
			$pos_pta_pin_req_yn_flag,
			$ps_authority_payment_mask_id,
			$pp_authority_payment_mask_id,
			$pos_pta_regex,
			$pos_pta_regex_bref,
			$payment_subtype_id,
			$payment_subtype_table_name,
			$payment_subtype_key_name,
			$payment_subtype_key_desc_name,
			$payment_subtype_name,
			$sysdate,
			$pos_pta_activation_ts,
			$pos_pta_deactivation_ts,
			$status_disabled,
			$status_enabled,
			$pos_id,
			$pos_pta_passthru_allow_yn_flag,
			$pos_pta_pref_auth_amt,
			$pos_pta_pref_auth_amt_max,
            $currency_cd
		);
		my $get_pos_pta_settings_stmt = $dbh->prepare(q{
			SELECT pp.pos_pta_device_serial_cd, 
				d.device_serial_cd,
				d.device_type_id,
				pp.pos_pta_encrypt_key, 
				pp.pos_pta_encrypt_key2, 
				pp.payment_subtype_key_id, 
				pp.pos_pta_pin_req_yn_flag,
				ps.authority_payment_mask_id,
				pp.authority_payment_mask_id,
				pp.pos_pta_regex,
				pp.pos_pta_regex_bref,
				pp.payment_subtype_id,
				ps.payment_subtype_table_name,
				ps.payment_subtype_key_name,
				ps.payment_subtype_key_desc_name,
				ps.payment_subtype_name,
				TO_CHAR(SYSDATE, 'MM/DD/YYYY HH AM'),
				TO_CHAR(pp.pos_pta_activation_ts, 'MM/DD/YYYY HH AM'),
				TO_CHAR(pp.pos_pta_deactivation_ts, 'MM/DD/YYYY HH AM'),
				DECODE(		--(date1 - date2) - ABS(date1 - date2) ==> 0=d1>=d2, !0=d1<d2
					(pp.pos_pta_deactivation_ts - SYSDATE) - ABS(pp.pos_pta_deactivation_ts - SYSDATE),
					0,
					2,	--enabled until ... (deactive ts adjustable)
					-1	--disabled (none adjustable)
				) AS status_disabled,
				DECODE(
					(pp.pos_pta_activation_ts - SYSDATE) - ABS(pp.pos_pta_activation_ts - SYSDATE),
					0,
					-2,	--disabled until ... (active ts adjustable)
					1	--enabled (deactive ts adjustable)
				) AS status_enabled,
				pp.pos_id,
				pp.pos_pta_passthru_allow_yn_flag,
				pp.pos_pta_pref_auth_amt,
				pp.pos_pta_pref_auth_amt_max,
                pp.currency_cd
			FROM device.device d, 
				pss.pos p, 
				pss.pos_pta pp, 
				pss.payment_subtype ps
			WHERE d.device_id = p.device_id
			AND p.pos_id = pp.pos_id
			AND ps.payment_subtype_id = pp.payment_subtype_id
			AND pp.pos_pta_id = :pos_pta_id
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$get_pos_pta_settings_stmt->bind_param(":pos_pta_id", $pos_pta_id);
		$get_pos_pta_settings_stmt->execute();

		my @pos_pta_data = $get_pos_pta_settings_stmt->fetchrow_array();
		$get_pos_pta_settings_stmt->finish();
		printError("pos_pta_id $pos_pta_id not found!") unless @pos_pta_data;		
		($pos_pta_device_serial_cd,
			$device_serial_cd,
			$device_type_id,
			$pos_pta_encrypt_key,
			$pos_pta_encrypt_key2,
			$payment_subtype_key_id,
			$pos_pta_pin_req_yn_flag,
			$ps_authority_payment_mask_id,
			$pp_authority_payment_mask_id,
			$pos_pta_regex,
			$pos_pta_regex_bref,
			$payment_subtype_id,
			$payment_subtype_table_name,
			$payment_subtype_key_name,
			$payment_subtype_key_desc_name,
			$payment_subtype_name,
			$sysdate,
			$pos_pta_activation_ts,
			$pos_pta_deactivation_ts,
			$status_disabled,
			$status_enabled,
			$pos_id,
			$pos_pta_passthru_allow_yn_flag,
			$pos_pta_pref_auth_amt,
			$pos_pta_pref_auth_amt_max,
            $currency_cd
		) = @pos_pta_data;
		print Dumper(\@pos_pta_data) if DEBUG;
		
		### determine which regex is used (based on presidence) ###
		my ($authority_payment_mask_id, $regex_final, $regex_final_bref, $regex_final_name, $regex_final_desc);
		$authority_payment_mask_id = $pp_authority_payment_mask_id ne '' ? $pp_authority_payment_mask_id : $ps_authority_payment_mask_id;
		my $get_authority_payment_mask_id_stmt = $dbh->prepare(q{
			SELECT authority_payment_mask_regex, 
				authority_payment_mask_bref,
				authority_payment_mask_name,
				authority_payment_mask_desc
			FROM pss.authority_payment_mask
			WHERE authority_payment_mask_id = :authority_payment_mask_id
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$get_authority_payment_mask_id_stmt->bind_param(":authority_payment_mask_id", $authority_payment_mask_id);
		$get_authority_payment_mask_id_stmt->execute();

		my @authority_payment_mask_id_data = $get_authority_payment_mask_id_stmt->fetchrow_array();
		$get_authority_payment_mask_id_stmt->finish();
		printError("authority_payment_mask_id $authority_payment_mask_id not found!") unless @authority_payment_mask_id_data;

		if (($pos_pta_regex ne '' && $pos_pta_regex ne $authority_payment_mask_id_data[0])
			&& $pos_pta_regex_bref ne $authority_payment_mask_id_data[1])
		{
			$regex_final = $pos_pta_regex;
			$regex_final_bref = $pos_pta_regex_bref;
			$regex_final_name = '';
			$regex_final_desc = '';
			$authority_payment_mask_id = undef;
		}
		else
		{
			$regex_final = $authority_payment_mask_id_data[0];
			$regex_final_bref = $authority_payment_mask_id_data[1];
			$regex_final_name = $authority_payment_mask_id_data[2];
			$regex_final_desc = $authority_payment_mask_id_data[3];
		}
		print Dumper($authority_payment_mask_id, $regex_final, $regex_final_bref, $regex_final_name, $regex_final_desc) if DEBUG;
		
		### some text formatting ###
		my $payment_subtype_key_name_formatted = lc $payment_subtype_key_name;
		$payment_subtype_key_name_formatted =~ s/[^a-z]|id$/ /gio;
		$payment_subtype_key_name_formatted =~ s/\b(\w)/\U$1/go;
		my $pos_pta_encrypt_key_hex = $pos_pta_encrypt_key ne '' ? 'CHECKED' : '';
		my $pos_pta_encrypt_key2_hex = $pos_pta_encrypt_key2 ne '' ? 'CHECKED' : '';
		
		### load applicable regex settings ###
		my @regex_pp_default_types;
		my $regex_pp_default_types_stmt = $dbh->prepare(qq{
			SELECT authority_payment_mask_name,
				authority_payment_mask_regex, 
				authority_payment_mask_bref,
				apm.authority_payment_mask_id
			FROM pss.authority_payment_mask apm, pss.payment_subtype ps
			WHERE apm.authority_payment_mask_id = ps.authority_payment_mask_id
			AND ps.payment_subtype_id = :payment_subtype_id
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$regex_pp_default_types_stmt->bind_param(":payment_subtype_id", $payment_subtype_id);
		$regex_pp_default_types_stmt->execute();
		push @regex_pp_default_types, [@_] while @_ = $regex_pp_default_types_stmt->fetchrow_array();
		$regex_pp_default_types_stmt->finish();

		my @regex_types;
		my $regex_types_stmt = $dbh->prepare(qq{
			SELECT authority_payment_mask_name,
				authority_payment_mask_regex, 
				authority_payment_mask_bref,
				apm.authority_payment_mask_id
			FROM pss.authority_payment_mask apm, pss.payment_subtype ps
			WHERE ps.payment_subtype_id = apm.payment_subtype_id
			AND apm.authority_payment_mask_id != ps.authority_payment_mask_id
			AND apm.payment_subtype_id = :payment_subtype_id
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$regex_types_stmt->bind_param(":payment_subtype_id", $payment_subtype_id);
		$regex_types_stmt->execute();
		push @regex_types, [@_] while @_ = $regex_types_stmt->fetchrow_array();
		$regex_types_stmt->finish();
		print Dumper(\@regex_pp_default_types, \@regex_types) if DEBUG;
		
        ### load backref settings ###
        my %regex_bref_types;
		my @regex_bref_codes;
		my @regex_bref_names;
		my @regex_bref_descs;
		my @regex_bref_types;
		my $regex_bref_types_stmt = $dbh->prepare(qq{
			SELECT authority_payment_mask_bref_id, regex_bref_name, regex_bref_desc
			FROM pss.authority_payment_mask_bref
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$regex_bref_types_stmt->execute();
		$regex_bref_types{$_[0]} = { name => $_[1], desc => $_[2] } while @_ = $regex_bref_types_stmt->fetchrow_array();
		$regex_bref_types_stmt->finish();
	
		### load other settings ###
		my @payment_subtype_table_key_ids;
		unless ($payment_subtype_table_name eq 'Not Applicable')
		{
			if($payment_subtype_table_name eq 'TERMINAL')
			{
				my $payment_subtype_table_key_ids_stmt = $dbh->prepare(qq{
					select t.terminal_id, m.merchant_name || ': ' || m.merchant_cd || ': ' || t.terminal_desc || ': ' || t.terminal_cd
					from pss.terminal t, pss.merchant m
					where t.merchant_id = m.merchant_id
					order by m.merchant_name || ': ' || m.merchant_cd || ': ' || t.terminal_desc || ': ' || t.terminal_cd
				}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
				$payment_subtype_table_key_ids_stmt->execute();
				push @payment_subtype_table_key_ids, [@_] while @_ = $payment_subtype_table_key_ids_stmt->fetchrow_array();
				$payment_subtype_table_key_ids_stmt->finish();
			}
			else
			{
				my $payment_subtype_table_key_ids_stmt = $dbh->prepare(qq{
					SELECT $payment_subtype_key_name payment_subtype_key_id, $payment_subtype_key_desc_name payment_subtype_key_desc
					FROM $payment_subtype_table_name
					ORDER BY $payment_subtype_key_desc_name
				}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
				$payment_subtype_table_key_ids_stmt->execute();
				push @payment_subtype_table_key_ids, [@_] while @_ = $payment_subtype_table_key_ids_stmt->fetchrow_array();
				$payment_subtype_table_key_ids_stmt->finish();
			}
		}
		print Dumper(\@payment_subtype_table_key_ids) if DEBUG;

		### query the database to get current date ###
		my ($curdate, $curtime, $curampm) = split(/ /,$sysdate);
		my ($curmm, $curdd, $curyyyy) = split(/\//, $curdate);

		### generate form select elements ###
		my @toggle_type_loop = (map { {'value' => $_->[1], 'default' => '', 'text' => $_->[0]} } (['None', undef], ['Immediately', 'now'], ['Custom:', 'custom']));
		my @toggle_month_loop = (map { {'value' => $_->[1], 'default' => '', 'text' => $_->[0]} } (['January', 1], ['February', 2], ['March', 3], ['April', 4], ['May', 5], ['June', 6], ['July', 7], ['August', 8], ['September', 9], ['October', 10], ['November', 11], ['December', 12]));
		my @toggle_day_loop = (map { {'value' => $_, 'default' => '', 'text' => $_} } (1..Date::Calc::Days_in_Month($curyyyy, $curmm)));
		my @toggle_year_loop = (map { {'value' => $_, 'default' => '', 'text' => $_} } ($curyyyy..($curyyyy + 1)));
		my @toggle_hour_loop = (map { {'value' => $_, 'default' => '', 'text' => $_} } (1..12));
		my @toggle_ampm_loop = (map { {'value' => $_, 'default' => '', 'text' => $_} } ('AM', 'PM'));
		my @toggle_tzone_loop = (map { {'value' => $_->[1], 'default' => '', 'text' => $_->[0]} } (['EST', 0], ['CST', -1], ['MST', -2], ['PST', -3]));

		my $toggle_cd = defined $pos_pta_deactivation_ts ? $status_disabled : defined $pos_pta_activation_ts ? $status_enabled : 0;	#0 = new but not yet enabled
		my $toggle_ts = $toggle_cd > 0 ? $pos_pta_deactivation_ts : $toggle_cd < -1 ? $pos_pta_activation_ts : undef;
		if (defined $toggle_ts && $toggle_ts ne '')
		{
			my ($toggle_date, $toggle_time, $toggle_ampm) = split(/ /, $toggle_ts);
			my ($toggle_mm, $toggle_dd, $toggle_yyyy) = split(/\//, $toggle_date);
			$toggle_type_loop[2]->{'default'} = "checked";
			@toggle_day_loop = (map { {'value' => $_, 'default' => '', 'text' => $_} } (1..Date::Calc::Days_in_Month($toggle_yyyy, $toggle_mm)));
			$toggle_month_loop[($toggle_mm - 1)]->{'default'} = "selected";
			$toggle_day_loop[($toggle_dd - 1)]->{'default'} = "selected";
			$toggle_year_loop[($toggle_yyyy - $curyyyy)]->{'default'} = "selected";
			$toggle_hour_loop[($toggle_time - 1)]->{'default'} = "selected";
			$toggle_ampm_loop[($toggle_ampm eq 'AM'?0:1)]->{'default'} = "selected";
		}
		else
		{
			$toggle_type_loop[0]->{'default'} = "checked";
			$toggle_month_loop[($curmm - 1)]->{'default'} = "selected";
			$toggle_day_loop[($curdd - 1)]->{'default'} = "selected";
			$toggle_year_loop[0]->{'default'} = "selected";
			$toggle_hour_loop[($curtime - 1)]->{'default'} = "selected";
			$toggle_ampm_loop[($curampm eq 'AM'?0:1)]->{'default'} = "selected";
		}
		my $default_found;
		my @toggle_regex_methods = (
			map { {
				'value'		=> $_->[1], 
				'default'	=> !$default_found && $_->[1] eq $regex_final ? ($default_found = 'selected') : '', 
				'text'		=> $_->[0], 
				'bref'		=> $_->[2],
				'id'		=> $_->[3]
			} } (
				@regex_pp_default_types,
				sort { lc $a->[0] cmp lc $b->[0] } @regex_types
			)
		);
		unshift @toggle_regex_methods, { 'text' => 'Custom method', 'value' => $regex_final, 'bref' => $regex_final_bref, 'default' => 'selected', 'id' => '' } unless $default_found;
		my (@regex_ids, @regex_methods, @regex_bref_methods);
		push @regex_ids, $_->{'id'} for (@toggle_regex_methods);
		push @regex_methods, $_->{'value'} for (@toggle_regex_methods);
		push @regex_bref_methods, $_->{'bref'} for (@toggle_regex_methods);

		### compile settings into javascript-readable arrays ###
 		foreach my $bref_code (sort keys %regex_bref_types)
 		{
 			push @regex_bref_codes, $bref_code;
 			push @regex_bref_names, quotemeta $regex_bref_types{$bref_code}->{name};
 			push @regex_bref_descs, quotemeta $regex_bref_types{$bref_code}->{desc};
 		}
 		print Dumper(\@regex_bref_codes, \@regex_bref_names, \@regex_bref_descs)." -->" if DEBUG;
 		my $regex_bref_codes_str = "'".join("','", @regex_bref_codes)."'";
 		my $regex_bref_names_str = "'".join("','", @regex_bref_names)."'";
 		my $regex_bref_descs_str = "'".join("','", @regex_bref_descs)."'";
		my $regex_ids_str = "'".join("','", @regex_ids)."'";
		my $regex_methods_str = "'".join("','", @regex_methods)."'";
		my $regex_bref_methods_str = "'".join("','", @regex_bref_methods)."'";

        my $tooltip = qq{ <img src=img/question.gif  onmouseout='hidetip()' onmouseover='showtip("$regex_final",1)'>};
		### print form ###
		print <<"		EOHTML";
<table width="100%" border="1" cellpadding="2" cellspacing="0">
<form method="post" action="edit_pos_pta_settings_func.cgi" onSubmit="javascript:return confirmSubmit();">
<!--<input type="hidden" name="callback_cgi" value="$callback_cgi">-->
<input type="hidden" name="callback_cgi" value="profile.cgi">
<input type="hidden" name="device_id" value="$device_id">
<input type="hidden" name="pos_pta_id" value="$pos_pta_id">
<input type="hidden" name="pos_id" value="$pos_id">
<input type="hidden" name="toggle_cd" value="$toggle_cd">
<input type="hidden" name="original_payment_subtype_id" value="$payment_subtype_id">
<input type="hidden" name="original_payment_subtype_key_id" value="$payment_subtype_key_id">
<input type="hidden" name="original_authority_payment_mask_id" value="$authority_payment_mask_id">
<input type="hidden" name="original_pos_pta_regex" value="$regex_final">
<input type="hidden" name="original_pos_pta_regex_bref" value="$regex_final_bref">
<input type="hidden" name="original_pos_pta_device_serial_cd" value="$pos_pta_device_serial_cd">
<input type="hidden" name="original_pos_pta_encrypt_key" value="$pos_pta_encrypt_key">
<input type="hidden" name="original_pos_pta_encrypt_key2" value="$pos_pta_encrypt_key2">
<input type="hidden" name="original_pos_pta_pin_req_yn_flag" value="$pos_pta_pin_req_yn_flag">
<input type="hidden" name="original_pos_pta_passthru_allow_yn_flag" value="$pos_pta_passthru_allow_yn_flag">
 <tr>
  <th colspan="4" bgcolor="#C0C0C0">Point-of-Sale Payment Type - $action<br>$payment_subtype_name</th>
 </tr>
 <tr>
  <td width="25%" nowrap>Device ID</td>
  <td width="25%">$device_id</td>
  <td width="25%" nowrap>Serial Number</td>
  <td width="25%">$device_serial_cd</td>
 </tr>
 <tr>
  <td nowrap>$payment_subtype_key_name_formatted</td>
  <td colspan="3">
		EOHTML
		if (scalar @payment_subtype_table_key_ids > 0)
		{
			print "  <select id= \"terminal\" name=\"payment_subtype_key_id\">\n";
			print "   <option value=\"\">Undefined</option>\n";
			print "   <option value=\"$_->[0]\"" . ($_->[0] eq $payment_subtype_key_id ? ' selected' : '') . ">$_->[1]</option>\n" foreach @payment_subtype_table_key_ids;
			print "  </select>\n";
		}
		else
		{
			print "   <input type=\"hidden\" name=\"payment_subtype_key_id\" value=\"N/A\">N/A\n";
		}
		print <<"		EOHTML";
  </td>
 </tr>	 
 <tr>
  <td nowrap>Activation Date</td>
		EOHTML
		my $form_toggle_rowspan = 8;
		if ($toggle_cd > 0 || $toggle_cd == -1)	#deactivation editable
		{
			$form_toggle_rowspan = 7;
			print <<"			EOHTML";
  <td>$pos_pta_activation_ts&nbsp;</td>
  <td>POS PTA ID</td>
  <td>$pos_pta_id</td>  
 </tr>
 <tr>
  <td nowrap>Deactivation Date</td>
			EOHTML
		}
		if ($toggle_cd == -1)	#none editable
		{
			print <<"			EOHTML";
  <td>$pos_pta_deactivation_ts&nbsp;</td>
			EOHTML
		}
		else
		{
			print <<"			EOHTML";
  <td>
   <table border="0" cellpadding="0" cellspacing="0">
			EOHTML
			print "    <tr><td><input type=\"radio\" name=\"toggle_date\" value=\"$_->{value}\" $_->{default}></td><td>$_->{text}<td></tr>\n" for (@toggle_type_loop);
			print <<"			EOHTML";
    <tr>
     <td>&nbsp;</td>
     <td nowrap>
      <select class="dropdown" name="toggle_month" onClick="javascript:this.form.toggle_date[2].checked=true;" onChange="javascript:adjust_days(this.form);">
			EOHTML
			print "       <option value=\"$_->{value}\" $_->{default}>$_->{text}</option>\n" for (@toggle_month_loop);
			print <<"			EOHTML";
      </select>&nbsp;<select class="dropdown" name="toggle_day" onClick="javascript:this.form.toggle_date[2].checked=true;">
			EOHTML
			print "       <option value=\"$_->{value}\" $_->{default}>$_->{text}</option>\n" for (@toggle_day_loop);
			print <<"			EOHTML";      
      </select>,&nbsp;<select class="dropdown" name="toggle_year" onClick="javascript:this.form.toggle_date[2].checked=true;" onChange="javascript:adjust_days(this.form);">
			EOHTML
			print "       <option value=\"$_->{value}\" $_->{default}>$_->{text}</option>\n" for (@toggle_year_loop);
			print <<"			EOHTML";
      </select><br>
      <select class="dropdown" name="toggle_time" onClick="javascript:this.form.toggle_date[2].checked=true;">
			EOHTML
			print "       <option value=\"$_->{value}\" $_->{default}>$_->{text}</option>\n" for (@toggle_hour_loop);
			print <<"			EOHTML";
      </select><select class="dropdown" name="toggle_ampm" onClick="javascript:this.form.toggle_date[2].checked=true;">
			EOHTML
			print "       <option value=\"$_->{value}\" $_->{default}>$_->{text}</option>\n" for (@toggle_ampm_loop);
			print <<"			EOHTML";
      </select>&nbsp;<select class="dropdown" name="toggle_tzone" onClick="javascript:this.form.toggle_date[2].checked=true;">
			EOHTML
			print "       <option value=\"$_->{value}\" $_->{default}>$_->{text}</option>\n" for (@toggle_tzone_loop);
			print <<"			EOHTML";
      </select>
     </td>
    </tr>
   </table>
  </td>
			EOHTML
		}
  my $ID = $authority_payment_mask_id;
		print <<"		EOHTML";
  <td valign="top" colspan="2" rowspan="$form_toggle_rowspan">
   <script language="JavaScript" type="text/javascript">
<!--

function regexSelectedMethod(myForm) {
	var regexIdArr = new Array($regex_ids_str);
	var regexArr = new Array($regex_methods_str);
	var regexBrefArr = new Array($regex_bref_methods_str);
	myForm.selected_regex.value = regexArr[myForm.pos_pta_regex.selectedIndex];
	myForm.selected_regex_bref.value = regexBrefArr[myForm.pos_pta_regex.selectedIndex];
	myForm.authority_payment_mask_id.value = regexIdArr[myForm.pos_pta_regex.selectedIndex];
	myForm.pos_pta_regex_bref.value = myForm.selected_regex_bref.value;
}
// -->
   </script>
   <input type="hidden" name="payment_subtype_id" value="$payment_subtype_id">
   <table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
     <td nowrap colspan="2">Card Magstripe Validation & Decoding Method</td>
    </tr>
    <tr>
     <td nowrap colspan="2"><hr style="width:100%;height:1px;color:#CCCCCC"/></td>
    </tr>
    <tr>
     <td nowrap>Method:&nbsp;</td>
     <td>
      <select name="pos_pta_regex" onChange="javascript:regexSelectedMethod(this.form);">
		EOHTML
		print " <option value=\"$_->{value}\" $_->{default}>$_->{text}</option>\n" for (@toggle_regex_methods);
		print <<"		EOHTML";
      </select>
      <input type="hidden" name="authority_payment_mask_id" value="$authority_payment_mask_id">
      <input type="hidden" name="pos_pta_regex_bref" value="$regex_final_bref">
     </td>
    </tr>
    <tr>
     <td valign="bottom" align="center"><input type="button" name="test_regex" value="Test" onClick="javascript:testRegex($ID);"></td>
     <td>
      <table border="0" cellspacing="0" cellpadding="0">
       <tr>
        <td bgcolor="lightgreen" nowrap>Selected Algo.:$tooltip</td>
        <td><input type="text" name="selected_regex" title="This is Read Only Field" value="$regex_final" id="regex_$ID" size="30" readonly></td>
       </tr>
       <tr>
        <td nowrap>Algo. Bref. Code:&nbsp;</td>
        <td><input type="text" name="selected_regex_bref" value="$regex_final_bref" id="backr_$ID" size="30" disabled></td>
       </tr>
       <tr>
        <td nowrap>Sample Magstripe:&nbsp;</td><td><input type="text" name="test_regex_card_num" value="" id="testv_$ID" size="30"></td>
       </tr>
      </table>
     </td>
    </tr>
    <tr>
     <td nowrap colspan="2">&nbsp;<br>Server Authorization Amount Override</td>
    </tr>
    <tr>
     <td nowrap colspan="2"><hr style="width:100%;height:1px;color:#CCCCCC"/></td>
    </tr>
    <tr>
     <td nowrap>Override Amount&nbsp;</td>
     <td><input type="text" name="pos_pta_pref_auth_amt" id="override_amount_id" value="$pos_pta_pref_auth_amt" size="15"></td>
    </tr>  
    <tr>
     <td nowrap>Override Limit</td>
     <td><input type="text" name="pos_pta_pref_auth_amt_max" id="override_limit_id" value="$pos_pta_pref_auth_amt_max" size="15"></td>
    </tr>
   </table>
  </td>
 </tr>
		EOHTML

print '
<style>
#currency_cd_select { 
width:230px; 
} 
#currency_cd_select option { 
width:320px; 
}
</style>
';

my $popCurrency = OOCGI::Popup->new( name => 'currency_cd',
                  id =>  'currency_cd_select',
                  sql => "SELECT currency_cd, currency_cd || ' - ' || currency_name FROM pss.currency ORDER BY LOWER(currency_name)");
if(defined $currency_cd && $currency_cd ne '') {
   $popCurrency->default($currency_cd);
} else {
   $popCurrency->default('USD');
}
my $popCurrencyString = $popCurrency->string;

		if ($toggle_cd < -1 || $toggle_cd == 0)	#activation editable
		{
			print <<"			EOHTML";
 <tr>
  <td nowrap>Deactivation Date</td>
  <td>$pos_pta_deactivation_ts&nbsp;</td>
 </tr>
			EOHTML
		}
		print <<"		EOHTML";
 <tr>
  <td nowrap>Device Authority Serial Num</td>
  <td><input type="text" name="pos_pta_device_serial_cd" value="$pos_pta_device_serial_cd" size="35"></td>
 </tr>	 
 <tr>
  <td nowrap>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
     <td align="left">Authority Encryption Key</td>
     <td align="right"><input type="checkbox" name="pos_pta_encrypt_key_hex" value="1" $pos_pta_encrypt_key_hex>&nbsp;hex</td>
    </tr>
   </table>
  </td>
  <td><input type="text" name="pos_pta_encrypt_key" value="$pos_pta_encrypt_key" size="35"></td>
 </tr>	 
 <tr>
  <td nowrap>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
     <td nowrap align="left">Authority Encryption Key 2</td>
     <td nowrap align="right"><input type="checkbox" name="pos_pta_encrypt_key2_hex" value="1" $pos_pta_encrypt_key2_hex>&nbsp;hex</td>
    </tr>
   </table>
  </td>
  <td><input type="text" name="pos_pta_encrypt_key2" value="$pos_pta_encrypt_key2" size="35"></td>
 </tr>	 
 <tr>
   <td>Currency</td>
   <td>$popCurrencyString</td>
 </tr>
 <tr>
  <td nowrap>PIN Required?</td>
  <td>
   <select name="pos_pta_pin_req_yn_flag">
		EOHTML
		print "   <option value=\"$_->[0]\"" . ($_->[0] eq uc $pos_pta_pin_req_yn_flag ? ' selected' : '') . ">$_->[1]</option>\n" foreach (['N', 'No'], ['Y', 'Yes']);
		print <<"		EOHTML";
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>Allow Authority Pass-through<br>(on Auth Decline/Failure)?</td>
  <td>
   <select name="pos_pta_passthru_allow_yn_flag">
		EOHTML
		print "   <option value=\"$_->[0]\"" . ($_->[0] eq uc $pos_pta_passthru_allow_yn_flag ? ' selected' : '') . ">$_->[1]</option>\n" foreach (['N', 'No'], ['Y', 'Yes']);
		print <<"		EOHTML";
   </select>
  </td>
 </tr>
 <tr>
  <td colspan="4" align="center">
   <input type="submit" name="myaction" value="Save">&nbsp;
   <input type="reset" value="Reset Form">&nbsp;
   <!--<input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='/$callback_cgi?device_id=$device_id';">-->
   <input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='profile.cgi?device_id=$device_id&tab=2';">
  </td>
 </tr>
</form>
</table>
		EOHTML
	}
	elsif ($action eq 'Import Template')
	{
		my ($callback_cgi, $device_id);
		foreach my $req_param ("device_id")
		{
			if (length $PARAM{$req_param} == 0) { printError("Required Parameter Not Found: $req_param");}
			else { eval "\$$req_param = \$PARAM{$req_param}"; }
			print Dumper(eval "\$$req_param") if DEBUG;
		}

		### load list of available templates ###
		my $err_ref;
		my @templates = USAT::POS::API::PTA::Util::list_templates($DATABASE, \$err_ref);
		printError("<br><br>$$err_ref<br><br>") if defined $err_ref;
		print Dumper(\@templates)."-->" if DEBUG;
		printError("<br><h3>No templates exist!</h3><br><br>") unless scalar @templates;
				
		### print form ###
		print <<"		EOHTML";
<table width="100%" border="1" cellpadding="2" cellspacing="0">
<form method="post" action="edit_pos_pta_settings_func.cgi" onSubmit="javascript:return confirmSubmit();">
<!--<input type="hidden" name="callback_cgi" value="$callback_cgi">-->
<input type="hidden" name="callback_cgi" value="profile.cgi">
<input type="hidden" name="device_id" value="$device_id">
 <tr>
  <th colspan="4" bgcolor="#C0C0C0">Point-of-Sale Payment Type - $action<br></th>
 </tr>
 <tr>
  <td>Device ID</td>
  <td>$device_id</td>
 </tr>
 <tr>
  <td>Payment Type Template</td>
  <td>
		EOHTML
		if (scalar @templates > 0)
		{
			print qq|  <select id="pos_pta_tmpl_id" name="pos_pta_tmpl_id">\n|;
			print qq|   <option value="">-- Choose a template --</option>\n|;
			print "   <option value=\"$_->[0]\">$_->[1]</option>\n" foreach @templates;
			print "  </select>\n";
		}
		print <<"		EOHTML";
  </td>
 </tr>
 <tr>
  <td>Import Mode</td>
  <td>
   <input type="radio" name="mode_cd" value="S" checked> 1. Safe<br>
   <input type="radio" name="mode_cd" value="MS"> 2. Merge Safe<br>
   <input type="radio" name="mode_cd" value="MO"> 3. Merge Overwrite<br>
   <input type="radio" name="mode_cd" value="O"> 4. Overwrite<br>
  </td>
 </tr>
 <tr>
  <td colspan="2" align="center">
   <input type="submit" name="myaction" value="Import">&nbsp;
   <input type="button" value="View Template" onClick="javascript:window.location='/edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id='+this.form.pos_pta_tmpl_id.options[this.form.pos_pta_tmpl_id.selectedIndex].value;">
   <input type="reset" value="Reset Form">&nbsp;
   <!--<input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='/$callback_cgi?device_id=$device_id';">-->
   <input type="button" value="Go Back to Device Profile" onClick="javascript:window.location='profile.cgi?device_id=$device_id&tab=2';">
  </td>
 </tr>
 <tr>
  <td colspan="2">
   <ul>
    <li><b>Safe Mode</b>
        <ul><li>
		Will not deactivate or alter any existing payment types.
		</li><li>
		May create new payment types if none exist for each <b>category</b> based on the selected template.
		</li><li>
		<i>Create new pos_pta records based on the selected pos_pta_tmpl where no active or inactive pos_pta records already exist for 
		this device per payment_entry_method_cd.</i>
		</li></ul>
	</li>
	<li><b>Merge Safe Mode</b>
        <ul><li>
		Will not deactivate or alter any existing payment types.
		</li><li>
		May create new payment types if none exist for each <b>type</b> based on the selected template.
		</li><li>
		<i>Create new pos_pta records based on the selected pos_pta_tmpl where no active or inactive pos_pta records already exist for 
		this device per payment_subtype_id.</i>
		</li></ul>
	</li>
	<li><b>Merge Overwrite Mode</b><br>
        <ul><li>
		May deactivate an existing payment type if one exists for each <b>type</b> being imported.
		</li><li>
		Will create new payment types based on the selected template.
		</li><li>
		<i>Deactivate any existing pos_pta records for this device where a new pos_pta_tmpl_entry is defined in the 
		pos_pta_tmpl per payment_subtype_id.  Create new pos_pta records from all pos_pta_tmpl_entry records for the selected template.</i>
		</li></ul>
	</li>
	<li><b>Overwrite Mode</b><br>
        <ul><li>
		Will deactivate any existing payment types for each <b>category</b> being imported.
		</li><li>
		Will create new payment types based on the selected template.
		</li><li>
		<i>Deactivate any existing pos_pta records for this device where a new pos_pta_tmpl_entry is defined in the 
		pos_pta_tmpl per payment_entry_method_cd.  Create new pos_pta records from all pos_pta_tmpl_entry records for the selected template.</i>
		</li></ul>
	</li>
   <ul>
  </td>
 </tr>
</form>
</table>
		EOHTML
	}
	else
	{
		printError("Unknown myaction or action Parameter: $action");
	}

	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub printError ($$) {
	my $err_txt = shift;
	print "$err_txt\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}
