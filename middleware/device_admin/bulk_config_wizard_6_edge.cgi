#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::Util;
use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use USAT::POS::API::PTA::Util;
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;

use constant DEBUG => 0;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);my $dbh = $DATABASE->{handle};

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $config_string = '';
my @config_params = split(/\|\|\|/, $PARAM{"config_params"});
foreach my $config_param (@config_params)
{  
    my $param_value = $PARAM{"cfg_$config_param"};
    my $checkbox_section_count = $PARAM{checkbox_section_count};
    for(my $section = 0; $section <= $checkbox_section_count; $section++) {
        my $checkbox_index = 'chkb_'.$config_param.'_'.$section;
        if(defined $PARAM{$checkbox_index} && $PARAM{$checkbox_index} >= 0) {
           $config_string .= "$config_param=$param_value\n";
        }
    } 
}

my $action = $PARAM{"action"};
if($action ne 'Next >')
{
	print "<br><br><font color=\"red\">Undefined Action!</font><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $device_ids_str = $PARAM{"include_device_ids"};
if(length($device_ids_str) == 0)
{
	print "<br><font color=\"red\">No devices selected!</font><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}

my $customer_id = $PARAM{"customer_id"};
my $location_id = $PARAM{"location_id"};
my $zero_counters = $PARAM{"zero_counters"};

my @devices_array = split /,/, $device_ids_str;

my $parameters_str = $PARAM{"parameter_to_change"};
my @parameters_array = split /,/, $parameters_str;

my $debug = $PARAM{"debug"};
if(length($debug) == 0)
{
	$debug = 0;
}

=pod
if(length($parameters_str) == 0 && length($customer_id) == 0 && length($location_id) == 0 && length($zero_counters) == 0)
{
	print "<br><font color=\"red\">Nothing to do!</font><br><br>";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;	
}
=cut

my $cust_loc_str = USAT::DeviceAdmin::Util->customer_location_str(\%PARAM);

print q{<form method="post" action="bulk_config_wizard_7_edge.cgi">};

HIDDEN_PASS('customer_id', 'location_id', 'parent_location_id','file_name');

HIDDEN('config_string',$config_string);
#HIDDEN('zero_counters',       $zero_counters);
HIDDEN('include_device_ids',  $device_ids_str);
HIDDEN('parameter_to_change', join(',', @parameters_array));
HIDDEN('debug',               $debug);

print "
<table cellpadding=\"3\" border=\"1\" cellspacing=\"0\" width=\"100%\">
 <tr>
  <th align=\"center\" colspan=\"3\" bgcolor=\"#C0C0C0\">Device Configuration Wizard - Page 7: Configure Payment Types</th>
 </tr>
 <tr>
  <th align=\"center\" colspan=\"3\" bgcolor=\"#C0C0C0\">$cust_loc_str</th>
 </tr>
";


		### load list of available templates ###
		my $err_ref;
		my @templates = USAT::POS::API::PTA::Util::list_templates($DATABASE, \$err_ref);
		printError("<br><br>$$err_ref<br><br>") if defined $err_ref;
		print Dumper(\@templates)."-->" if DEBUG;
		printError("<br><h3>No templates exist!</h3><br><br>") unless scalar @templates;
				
		### print form ###
		print <<"		EOHTML";
 <tr>
  <td>Payment Type Template</td>
  <td>
		EOHTML
		if (scalar @templates > 0)
		{
			print "  <select name=\"pos_pta_tmpl_id\">\n";
			print "   <option value=\"\">-- Choose a template --</option>\n";
			print "   <option value=\"$_->[0]\">$_->[1]</option>\n" foreach @templates;
			print "  </select>\n";
		}
		print <<"		EOHTML";
  </td>
 </tr>
 <tr>
  <td>Import Mode</td>
  <td>
   <input type="radio" name="mode_cd" value="S"> 1. Safe<br>
   <input type="radio" name="mode_cd" value="MS"> 2. Merge Safe<br>
   <input type="radio" name="mode_cd" value="MO"> 3. Merge Overwrite<br>
   <input type="radio" name="mode_cd" value="O"> 4. Overwrite<br>
   <input type="radio" name="mode_cd" value="" checked> X. Do Not Import A Payment Template<br>
  </td>
 </tr>
 <tr>
  <td colspan="2">
   <ul>
    <li><b>Safe Mode</b>
        <ul><li>
		Will not deactivate or alter any existing payment types.
		</li><li>
		May create new payment types if none exist for each <b>category</b> based on the selected template.
		</li><li>
		<i>Create new pos_pta records based on the selected pos_pta_tmpl where no active or inactive pos_pta records already exist for 
		this device per payment_entry_method_cd.</i>
		</li></ul>
	</li>
	<li><b>Merge Safe Mode</b>
        <ul><li>
		Will not deactivate or alter any existing payment types.
		</li><li>
		May create new payment types if none exist for each <b>type</b> based on the selected template.
		</li><li>
		<i>Create new pos_pta records based on the selected pos_pta_tmpl where no active or inactive pos_pta records already exist for 
		this device per payment_subtype_id.</i>
		</li></ul>
	</li>
	<li><b>Merge Overwrite Mode</b><br>
        <ul><li>
		May deactivate an existing payment type if one exists for each <b>type</b> being imported.
		</li><li>
		Will create new payment types based on the selected template.
		</li><li>
		<i>Deactivate any existing pos_pta records for this device where a new pos_pta_tmpl_entry is defined in the 
		pos_pta_tmpl per payment_subtype_id.  Create new pos_pta records from all pos_pta_tmpl_entry records for the selected template.</i>
		</li></ul>
	</li>
	<li><b>Overwrite Mode</b><br>
        <ul><li>
		Will deactivate any existing payment types for each <b>category</b> being imported.
		</li><li>
		Will create new payment types based on the selected template.
		</li><li>
		<i>Deactivate any existing pos_pta records for this device where a new pos_pta_tmpl_entry is defined in the 
		pos_pta_tmpl per payment_entry_method_cd.  Create new pos_pta records from all pos_pta_tmpl_entry records for the selected template.</i>
		</li></ul>
	</li>
   <ul>
  </td>
 </tr>

		EOHTML

print q{
  </td>
 </tr>
</table>
<table cellspacing="0" cellpadding="2" border="1" width="100%">
 <tr>
  <td align="center" bgcolor="#C0C0C0">
   <input type="button" value="< Back" onClick="javascript:history.go(-1);">
   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
   <input type="submit" name="action" value="Next >">
  </td>
 </tr>
</table>
</form>
};

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
