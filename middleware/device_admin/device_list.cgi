#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use OOCGI::Query;
use OOCGI::OOCGI;
use OOCGI::NTable;
use OOCGI::PageNavigation;
use OOCGI::PageSort;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::DBObj;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

my %PARAM = $query->Vars;

my %enabled_hash = (
   'Y'  =>  'Enabled',
   'N'  =>  'Disabled' );

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
$session->destroy;

print q|<form method="post" name="device_list" action="device_list.cgi">|;

my $pageNav  = OOCGI::PageNavigation->new(25);

my $ev_number        = uc($PARAM{ev_number});
my $device_type      = $PARAM{device_type};
my $location         = $PARAM{location_id};
my $customer         = $PARAM{customer_id};
my $serial_number    = $PARAM{serial_number};
my $modem_id         = $PARAM{modem_id};
my $firmware_version = $PARAM{firmware_version};
my $enabled          = $PARAM{enabled};
if(length($device_type)      == 0 &&
   length($location)         == 0 &&
   length($customer)         == 0 &&
   length($serial_number)    == 0 &&
   length($firmware_version) == 0 &&
   length($enabled)          == 0 &&
   length($modem_id)         == 0) {
    print "Required parameter not found: ev_number, serial_number, device_type, location_id,
               customer_id, firmware_version, modem_id, enabled\n";

    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
    exit(0);
}

if(length($enabled) == 0)
{
    $enabled = "'Y'";
}

my $sql;

$sql = "SELECT device_id,
                        device_name,
                        device_serial_cd,
                        device_type_desc,
                        NVL(to_char(last_activity_ts, 'MM/DD/YYYY HH:MI:SS AM'), '(n/a)'),
                        device_type_id,
                        device_active_yn_flag,
                        rownum as row_num,
                        customer_name,
                        location_name,
                        device_setting_value,
                        ROUND((SYSDATE - last_activity_ts), 1)
                   from ( SELECT device.device_id,
                                 device.device_name,
                                 device.device_serial_cd,
                                 device_type.device_type_desc,
                                 last_activity_ts,
                                 device.device_type_id,
                                 device.device_active_yn_flag,
                                 customer.customer_name,
                                 location.location_name,
                                 NVL(ds.device_setting_value, ds2.device_setting_value) device_setting_value
                            FROM device,
                                 device_type,
                                 pos,
                                 customer,
                                 location.location,
                                 device.device_setting ds,
								 device.device_setting ds2
                           WHERE device.device_type_id = device_type.device_type_id
                             AND device.device_id = pos.device_id
                             AND pos.location_id = location.location_id
                             AND pos.customer_id = customer.customer_id
                             AND device.device_id = ds.device_id(+)
                             AND ds.device_setting_parameter_cd(+) = DECODE(device.device_type_id, 11, 'Public_PC_Version', 'Firmware Version')
							 AND device.device_id = ds2.device_id(+)
                             AND ds2.device_setting_parameter_cd(+) = DECODE(device.device_type_id, 11, 'SoftwareVersion', 'Firmware Version')";

if(length($ev_number) > 0) {
    my $like;
    if($ev_number =~ /\^/ && $ev_number =~ /\$/ && $ev_number =~ /\%/) {
       my ($head, $tail) = split('\%',$ev_number);
       $like = "$head%'"." AND device.device_name like '%".$tail;
    } elsif($ev_number =~ /\^/) {
       $like = $ev_number.'%';
    } elsif($ev_number =~ /\$/) {
       $like = '%'.$ev_number;
    } else {
       $like = '%'.$ev_number.'%';
    }
    $like =~  tr/\^//d;
    $like =~  tr/\$//d;
    $ev_number =~ tr/\^//d;
    $ev_number =~ tr/\$//d;
    $ev_number =~ tr/\%//d;
    $sql .= "AND device.device_name like '$like' ";
}
if(length($device_type) > 0) {
   $sql .= "AND device_type.device_type_id = $device_type ";
}
if(length($location) > 0) {
    $sql .= "AND location.location_id = $location ";
}
if(length($customer) > 0) {
    $sql .= "AND customer.customer_id = $customer ";
}
if(length($serial_number) > 0) {
    my $like;
    if($serial_number =~ /\^/ && $serial_number =~ /\$/ && $serial_number =~ /\%/) {
       my ($head, $tail) = split('\%',$serial_number);
       $like = "$head%'"." AND device.device_serial_cd like '%".$tail;
    } elsif($serial_number =~ /\^/) {
       $like = $serial_number.'%';
    } elsif($serial_number =~ /\$/) {
       $like = '%'.$serial_number;
    } else {
       $like = '%'.$serial_number.'%';
    }
    $like =~  tr/\^//d;
    $like =~  tr/\$//d;
    $serial_number =~ tr/\^//d;
    $serial_number =~ tr/\$//d;
    $serial_number =~ tr/\%//d;
    $sql .= "AND device.device_serial_cd like '$like' ";
}
if(length($firmware_version) > 0) {
   $sql .= "AND NVL(ds.device_setting_value, ds2.device_setting_value) = '$firmware_version' ";
}
if(length($modem_id) > 0) {
   $sql .= "AND device.device_serial_cd in (
                SELECT distinct serial_number
                  FROM device_call_in_record
                 WHERE network_layer = 'USANet3MotientGateway'
                   AND modem_id = upper($modem_id) ) ";
}

$sql .= "   AND device.device_active_yn_flag in ($enabled) ";

my %table_cols = ( #col_num => [col name, sql sort, default sort (1=asc, -1=desc)]
    1 => ['Device Type',   'device_type.device_type_id',                   ,  1],
    2 => ['EV Number',     'LOWER(device.device_name)'                     ,  1],
    3 => ['Serial Number', 'device.device_serial_cd'                       ,  1],
    4 => ['Customer',      'LOWER(customer.customer_name)'                 ,  1],
    5 => ['Location',      'LOWER(location.location_name)'                 ,  1],
    6 => ['Firmware',      'NVL(ds.device_setting_value, ds2.device_setting_value)'           ,  1],
    7 => ['Last Activity', "NVL(last_activity_ts, TO_DATE('1970', 'YYYY'))", -1],
    8 => ['Status',        'device.device_active_yn_flag'                  , -1]
);

my $pageSort = OOCGI::PageSort->new(
    default_sort_field      => 8 * $table_cols{8}->[2],
    default_sort_field_last => 7 * $table_cols{7}->[2],
);
my ($sort_id, $sort_asc, $last_sort_id, $last_sort_asc) = $pageSort->get_sorted_columns();
$sql .= '    ORDER BY '.$table_cols{$sort_id}->[1].($sort_asc ? '' : ' DESC');
$sql .= ','.$table_cols{$last_sort_id}->[1].($last_sort_asc ? '' : ' DESC') if $last_sort_id;
$sql .= ')';
######$sql =~ s/\s+/ /g; warn $sql;

my $qo   = $pageNav->get_result_obj($sql);

my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

my $rn_table = 0;
$table->put($rn_table,0,B('Device List'),'colspan=8 class=header0 align=center');
$rn_table++;

my $cn_table = 0;
foreach my $key (map($_ * $table_cols{$_}->[2], sort {$a <=> $b} keys %table_cols)) {
    (my $text = $table_cols{abs($key)}->[0]) =~ s/\s/&nbsp;/go;
    $table->put($rn_table,$cn_table++,$pageSort->sort_column($key, $text),'class=header1');
}
$rn_table++;

my $row0 = "row0";
my $row1 = "row1";
my $i = 0;
while (@data = $qo->next) 
{
    my $row = $i%2 ? $row1 : $row0;
    $table->put($rn_table,0,$data[3],"class=$row");
    my $img1 = qq|<a href="device_list.cgi?ev_number=$data[1]&show_disabled_device=Y&enabled='Y','N'"><img src=img/disabled.gif border='0' title='Display all devices for this EV number.'></a>|;
    if($PARAM{show_disabled_device} eq 'Y') {
       my $str1 = qq|<a href="profile.cgi?show_disabled_device=Y&device_id=$data[0]&tab=1">$data[1]</a>|;
       $table->put($rn_table,1,$str1.' '.$img1,"class=$row nowrap");
    } else {
       my $str1 = qq|<a href="profile.cgi?device_id=$data[0]&tab=1">$data[1]</a>|;
       $table->put($rn_table,1,$str1.' '.$img1,"class=$row nowrap");
    }
    $table->put($rn_table,2,$data[2],"class=$row");
    $table->put($rn_table,3,$data[8],"class=$row");
    $table->put($rn_table,4,$data[9],"class=$row");
    $table->put($rn_table,5,$data[10],"class=$row");
    
    if($data[11]  > 5) {
       $table->put($rn_table,6,B("$data[4]<br>($data[11] days)","red"),"class=$row nowrap");
    } elsif($data[11]  > 2) {
       $table->put($rn_table,6,B("$data[4]<br>($data[11] days)","#DDCC00"),"class=$row nowrap");
    } else {
       $table->put($rn_table,6,B("$data[4]","green"),"class=$row nowrap");
    }
             
    if($data[6] eq 'Y') {
       $table->put($rn_table,7,B($enabled_hash{$data[6]},"green"),"class=$row");
    } else {
       $table->put($rn_table,7,B($enabled_hash{$data[6]},"red"),"class=$row");
    }
    $rn_table++;
    $i++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
