#!/usr/local/USAT/bin/perl

use strict;
use warnings;

use English qw(-no_match_vars);
use Net::LDAPS;
use Net::LDAP::Util qw(escape_dn_value);

use USAT::App::DeviceAdmin::Config qw(:globals);
use OOCGI::NTable;
use OOCGI::OOCGI;
use OOCGI::Query;
use USAT::DeviceAdmin::DBObj::Da_session;
use USAT::DeviceAdmin::UI::DAHeader;

use constant MEMBER_ATTR => 'member';
#use constant BASE        => 'DC=' . DA_SESSION__LDAP__HOST . ',DC=com';
my $base           = DA_SESSION__LDAP__BASE;

# set up authenticated session
my $query   = OOCGI::OOCGI->new;
my %PARAM   = $query->Vars();
my $SESSION = USAT::DeviceAdmin::DBObj::Da_session->authenticate;

# print HTTP, HTML headers & menus
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile('header.html');
$SESSION->print_menu;

my $table = ($PARAM{userOP} and $PARAM{userOP} eq 'Refresh')
    ? refresh_users()
    : prompt_for_password();

print '<form method="post">';
$table->display();
print '</form>';

$SESSION->destroy;
USAT::DeviceAdmin::UI::DAHeader->printFooter('footer.html');


sub refresh_users {
    # connect to directory and bind with the entered credentials
    my $ldap = Net::LDAPS->new(
        DA_SESSION__LDAP__SYSTEM_NAME,
        port    => DA_SESSION__PORT,
        version => DA_SESSION__LDAP_VERSION,
    ) or croak $EVAL_ERROR;
    my $mesg = $ldap->bind(
        DA_SESSION__DOMAIN . q{\\} . escape_dn_value($PARAM{user_name}),
        password => $PARAM{password},
    );
    return prompt_for_password(DA_SESSION__AUTHENTICATION_ERROR)
        if $mesg->code;

    my @gprs_users = # get first initial and surname of GPRS Web Admins
        map {substr($_->get_value('givenname'), 0, 1) . '. '
                  . $_->get_value('sn')}
        sort {$a->get_value('sn') cmp $b->get_value('sn')}
        map {$ldap->search(
            base   => $base,
            filter => "distinguishedname=$_",
            attrs  => ['givenname', 'sn'],
        )->entries}
        map {$_->get_value(MEMBER_ATTR)}
        $ldap->search(
            base   => $base,
            filter => 'cn=DA GPRS Web Admins',
            attrs  => [MEMBER_ATTR],
        )->entries;

    # start making the new HTML table
    my $table = OOCGI::NTable->new('width="30%" border="1"');
    my $row   = 0;
    $table->put( $row++, 0, B('GPRSWEB Users') );

    # wipe the old database table and add new rows to both it and HTML
    OOCGI::Query->modify('delete uitool.gprsweb_user');
    foreach my $user (@gprs_users) {
        my $obj = USAT::DeviceAdmin::DBObj::Gprsweb_user->new;
        $obj->gprsweb_user_name($user);
        $obj->insert;
        $table->put($row++, 0, $user);
    }

    return $table;
}

sub prompt_for_password {
    my $error = shift;

    my $row   = 0;
    my $table = OOCGI::NTable->new('width="30%"');
    use constant SZ_ML => (20, 30);

    $table->put( $row++, 0,
        B('USATech GPRSWEB User Refresh'),
        'colspan="2" align="center"',
    );
    if ($error) {
        $table->put($row++, 0, B($error, 'red'), 'colspan="2" align="center"');
    }

    $table->put( $row,   0, B('User Name ') );
    $table->put( $row++, 1, OOCGI::Text->new(
        name  => 'user_name',
        value => $SESSION->user_name,
        sz_ml => [SZ_ML],
    ) );

    $table->put( $row,   0, B('Password ') );
    $table->put( $row++, 1, OOCGI::Password->new(
        name  => 'password',
        sz_ml => [SZ_ML],
    ) );

    $table->put( $row, 0,
        SUBMIT('userOP', 'Refresh'),
        'colspan="2" align="center"',
    );

    return $table;
}
