#!/usr/local/USAT/bin/perl

require Text::CSV;
use CGI::Carp qw(fatalsToBrowser);
use USAT::DeviceAdmin::DBObj;
use Data::Dumper;
use OOCGI::NTable;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::USAPopups;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use USAT::DeviceAdmin::Util;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::DeviceAdmin::UI::AxahLocation;
use Algorithm::LUHN qw/check_digit is_valid/;

use strict;

my $ISO_PREFIX = '639621';

my $query = OOCGI::OOCGI->new;
my %PARAM = $query->Vars();

my $action = $PARAM{'action'};
if(defined $action && $action eq 'Download CSV Database')
{
	my @cards_arr = ();
	
	my @consumer_acct_ids_arr = $query->param("download_card");
	
	my $consumer_acct_sql = qq|
		SELECT ca.consumer_acct_cd, 													
	           to_char(ca.consumer_acct_deactivation_ts, 'YYMM'), 						
	           to_char(ca.consumer_acct_deactivation_ts, 'MM/YYYY'), 					
	           to_char(ca.created_ts, 'MM/YYYY'), 										
	           caf.consumer_acct_fmt_name,												
	           c.customer_name,														
	           l.location_name,														
	           ca.consumer_acct_fmt_id,												
	           ca.consumer_acct_validation_cd,											
	           ca.consumer_acct_issue_cd												
		  FROM consumer_acct ca,
               pss.consumer_acct_fmt caf,
               pss.merchant_consumer_acct mca,
               merchant m,
               customer_merchant cm,
               customer c,
               location.location l
		 WHERE ca.consumer_acct_fmt_id = caf.consumer_acct_fmt_id
		   AND ca.consumer_acct_id = mca.consumer_acct_id
		   AND mca.merchant_id = m.merchant_id
		   AND m.merchant_id = cm.merchant_id
		   AND cm.customer_id = c.customer_id
		   AND ca.location_id = l.location_id
		   AND ca.consumer_acct_id = ?|;
		
	foreach my $consumer_acct_id (@consumer_acct_ids_arr)
	{
		my $consumer_acct_query = OOCGI::Query->new($consumer_acct_sql, undef, [$consumer_acct_id]);
		my @consumer_acct_data_arr = $consumer_acct_query->next;
		
		my $magstripe = "";
	  	my $consumer_acct_cd = $consumer_acct_data_arr[0];
		my $fmt_id = $consumer_acct_data_arr[7];
		my $exp = $consumer_acct_data_arr[1];
		my $lost_card_cd = $consumer_acct_data_arr[9];
		my $card_vv = $consumer_acct_data_arr[8];
		
		$lost_card_cd = '00' unless defined $lost_card_cd;
	  
		if($fmt_id eq '0')
		{
			$magstripe = $ISO_PREFIX . '0' . $consumer_acct_cd;
#		    my $luhn_cd = com::usatech::cardnumbergenerator::cd::Luhn->generate($magstripe);
			my $luhn_cd = check_digit($magstripe);
			$magstripe .= $luhn_cd . "=" . $exp . $lost_card_cd . $card_vv;
			
			my $temp_magstripe = $magstripe;
			$temp_magstripe =~ s/=/0/;
			
			my $mod97 = sprintf("%02d", USAT::DeviceAdmin::Util->mod97($temp_magstripe));
			$magstripe .= $mod97;
		}
		elsif($fmt_id eq '1')
		{
			# retrieve first action listed regardless of device type... may need to improve this later
			my $device_type_action_sql = q|
				select dta.device_type_action_cd
				from device.device_type_action dta, device.action a, pss.permission_action pa, pss.consumer_acct_permission cap
				where dta.action_id = a.action_id
				and a.action_id = pa.action_id
				and pa.permission_action_id = cap.permission_action_id
				and cap.consumer_acct_id = ?|;
		    
		    my $device_type_action_query = OOCGI::Query->new($device_type_action_sql, undef, [$consumer_acct_id]);
		    if ($device_type_action_query->row_count == 0)
		    {
				USAT::DeviceAdmin::UI::DAHeader->printHeader();
				USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
				printError("Failed to lookup device_type_action_cd for $consumer_acct_id!");
		    }
		    
			my @device_type_action_cd_arr = $device_type_action_query->next;
			my $device_type_action_cd = $device_type_action_cd_arr[0];
			
			$magstripe = $ISO_PREFIX . '1' . $device_type_action_cd . $consumer_acct_cd;
#		my $luhn_cd = com::usatech::cardnumbergenerator::cd::Luhn->generate($magstripe);
    		my $luhn_cd = check_digit($magstripe);
			$magstripe .= $luhn_cd . "=" . $exp . $card_vv;
		}
		else
		{
			USAT::DeviceAdmin::UI::DAHeader->printHeader();
			USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
			printError("Unhandled consumer account format: $fmt_id for consumer_acct $consumer_acct_id");
		}
	    
		push(@cards_arr, [$magstripe, $consumer_acct_cd, $exp, $consumer_acct_data_arr[2], $consumer_acct_data_arr[3], $consumer_acct_data_arr[4], $consumer_acct_data_arr[5], $consumer_acct_data_arr[6]]);
	}

	my $csv_doc;
	my $card_count = scalar(@cards_arr);
	my $csv = Text::CSV->new;
	my @headers = ('Magstripe', 'Account Number', 'Expiration Date', 'Display Expiration Date', 'Activation Date', 'Card Type', 'Customer', 'Location');
	$csv->combine(@headers);
	$csv_doc .= $csv->string . "\n";
	
	foreach my $card_arr_ref (@cards_arr)
	{
		if ($csv->combine(@{$card_arr_ref})) 
		{
	    	my $csv_line = $csv->string;
			$csv_doc .= $csv_line . "\n";
		}
		else
		{
			printError("Failed to generate CSV database for card $card_arr_ref->[0]! ($csv->error_input)");
	    }
	}
	
	print "Content-Type: application/force-download\n";
	print "Content-Disposition: attachment; filename=data.csv\n\n";
	print $csv_doc;
	$query->DESTROY(1);
	exit();
}

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;
$session->destroy;

my %B = ( C => '%', B => '',  E => '%' );
my %E = ( C => '%', B => '%', E => ''  );

my $popup = OOCGI::Popup->new();
$popup->tail('C','Contains');
$popup->tail('B','Begins');
$popup->tail('E','Ends');

print q|
<script type="text/javascript" src="/js/ajax.js"></script>
<style>
/* regular button, hover */
.btn {
   background-color:#FFFFFF;text-align:left; border:0;color:blue;width:100%;
}
/* regular button, hover */
.btn:hover{
     color:white;
     background-color:red;
}
</style>

<script>
function confirmSubmit()
{
   flag = 0;
   
   obj = document.getElementById('id_consumer_acct_cd');
   if(obj.value != '') { flag = 1; }
   obj = document.getElementById('id_consumer_fname');
   if(obj.value != '') { flag = 1; }
   obj = document.getElementById('id_consumer_lname');
   if(obj.value != '') { flag = 1; }
   obj = document.getElementById('id_consumer_email_addr1');
   if(obj.value != '') { flag = 1; }
   
   if(document.myForm.location_id.selectedIndex > 0) { flag = 1; }
   if(document.myForm.merchant_id.selectedIndex > 0) { flag = 1; }
   
   if (flag) {
       return true ;
   } else {
     alert('You must enter a value at least one of the form fields.');
     return false ;
   }
}

function runThis(key)
{
    document.myForm.consumer_id.value = key;
    document.myForm.submit();
    return false;
}

    function count_check(field) {
       var count = 0;
       //var obj = document.getElementById("check_count");
       var obj = document.myForm.check_count;
       for (i = 0; i < field.length; i++) {
          if(field[i].checked == true) {
             count++;
          }
       }
       obj.value = count;
    }

// following is checking all checkboxes
    var checkflag = "false";
    function check(field) {
       //var obj = document.getElementById("check_count");
       var obj = document.myForm.check_count;
       var count = 0;
       if (checkflag == "false") {
          for (i = 0; i < field.length; i++) {
             field[i].checked = true;
             count++;
          }
          checkflag = "true";
          obj.value = count;
          return "Uncheck All";
       } else {
          for (i = 0; i < field.length; i++) {
             field[i].checked = false;
          }
          checkflag = "false";
          obj.value = 0;
          return "Check All";
       }
    }
</script>
|;

my @binds = ();
my $txt = "%%";
if($PARAM{consumer_acct_cd}) {
   $txt = $PARAM{consumer_acct_cd};
   my $be  = $PARAM{popup_acct_cd};
   $txt =  "$B{$be}$txt$E{$be}";
   push(@binds,$txt);
}

my $SQLB = q|SELECT consumer_id,
                     consumer_fname,
                     consumer_lname,
                     (SELECT count(*)
                        FROM consumer_acct
                       WHERE consumer_id = c.consumer_id) account_count,
                     (SELECT count(*)
                        FROM tran t, consumer_acct ca
                       WHERE t.consumer_acct_id = ca.consumer_acct_id
                         AND ca.consumer_id = c.consumer_id) transaction_count,
                     consumer_email_addr1,
                     consumer_email_addr2 
              FROM consumer c
              WHERE consumer_id > 0
|;

if($PARAM{consumer_acct_cd}) 
{
	$SQLB .= q| AND consumer_id IN (SELECT DISTINCT consumer_id FROM consumer_acct WHERE consumer_acct_cd like ?) |;
}

my $consumer = USAT::DeviceAdmin::DBObj::Consumer->new;
$consumer->upload($query);

my $consumer_fname          = $consumer->consumer_fname;
my $consumer_lname          = $consumer->consumer_lname;
my $consumer_email_addr1    = $consumer->consumer_email_addr1;
my $consumer_email_addr2    = $consumer->consumer_email_addr2;

#my $style1 = 'style="{background-color:#999999; font-weight:bold;}"';
my $style1 = 'class=leftheader';
my $style3 = 'style="{text-align:right;}"';
#----------------------------------------
if($PARAM{action} eq 'Search') {
   if($consumer_fname) {
      my $txt = lc($consumer_fname);
      my $be  = $PARAM{popup_fname};
      $SQLB .= " AND LOWER(consumer_fname) like ? ";
      push(@binds,"$B{$be}$txt$E{$be}");
   }
   if($consumer_lname) {
      my $txt = lc($consumer_lname);
      my $be  = $PARAM{popup_lname};
      $SQLB .= " AND LOWER(consumer_lname) like ? ";
      push(@binds,"$B{$be}$txt$E{$be}");
   }
   if($consumer_email_addr1) {
      my $txt = lc($consumer_email_addr1);
      my $be  = $PARAM{popup_email1};
      $SQLB .= " AND LOWER(consumer_email_addr1) like ? ";
      push(@binds,"$B{$be}$txt$E{$be}");
   }
   if($PARAM{location_id}) {
      $SQLB .= " AND consumer_id IN ( SELECT distinct consumer_id
                                       FROM consumer_acct
                                      WHERE location_id = ? ) ";
      push(@binds,$PARAM{location_id} );
   }
   if($PARAM{merchant_id}) {
      $SQLB .= " and exists (
                select 1
                from consumer_acct, pss.merchant_consumer_acct
                where consumer_acct.consumer_acct_id = merchant_consumer_acct.consumer_acct_id
                and merchant_consumer_acct.merchant_id = ?
                and consumer_acct.consumer_id = c.consumer_id
                ) ";
      push(@binds,$PARAM{merchant_id} );
   }
   
}
#----------------------------------------

my $table = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
my $table_rn = 0;

my $style2 = 'style="{background-color:#FFFFFF}"';

print q|<form name=myForm method="post">|;

$table->put($table_rn,0,'Consumer/Card Search Screen','class=header0 colspan=2');
$table_rn++;
$table->put($table_rn,0,'Card Number','class=leftheader nowrap');
$popup->name('popup_acct_cd');
if($PARAM{popup_acct_cd}) {
   $popup->default($PARAM{popup_acct_cd});
}
my $text = OOCGI::Text->new(id => 'id_consumer_acct_cd', name => 'consumer_acct_cd', size => 40);
$text->default($PARAM{consumer_acct_cd});
$table->put($table_rn,1, $popup.$text);
$table_rn++;

$table->put($table_rn,0,'First Name', 'class=leftheader nowrap');
$popup->name('popup_fname');
if($PARAM{popup_fname}) {
   $popup->default($PARAM{popup_fname});
}
$table->put($table_rn,1, $popup.$consumer->Text('consumer_fname', { id => 'id_consumer_fname', size => 40}));
$table_rn++;

$table->put($table_rn,0,'Last Name','class=leftheader nowrap');
$popup->name('popup_lname');
if($PARAM{popup_lname}) {
   $popup->default($PARAM{popup_lname});
}
$table->put($table_rn,1, $popup.$consumer->Text('consumer_lname', { id => 'id_consumer_lname', size => 40}));
$table_rn++;

$table->put($table_rn,0,'Primary Email','class=leftheader nowrap');
$popup->name('popup_email1');
if($PARAM{popup_email1}) {
   $popup->default($PARAM{popup_email1});
}
$table->put($table_rn,1, $popup.$consumer->Text('consumer_email_addr1', { id => 'id_consumer_email_addr1', size => 40}));
$table_rn++;

$table->put($table_rn,0,'Notification Email','class=leftheader nowrap');
$popup->name('popup_email2');
if($PARAM{popup_email2}) {
   $popup->default($PARAM{popup_email2});
}
$table->put($table_rn,1, $popup.$consumer->Text('consumer_email_addr2', { size => 40}), $style2);
$table_rn++;

$table->put($table_rn,0,'Assigned Location','class=leftheader');

my $tooltip = q{Enter first couple of Characters of the Location name,  then press Look up for Location button.};
my $ltable = USAT::DeviceAdmin::UI::AxahLocation::get_location_table($tooltip);

$table->put($table_rn,1, $ltable);
$table_rn++;

my $merchant_popup = OOCGI::Popup->new(
					name => 'merchant_id', 
					align => 'yes', 
					delimiter => '.',
					style => "font-family: courier; font-size: 12px;",
					sql => q|
						SELECT distinct m.merchant_id, m.merchant_name, m.merchant_cd
						  FROM merchant m, pss.merchant_consumer_acct mca, authority.authority a
					 	 WHERE m.merchant_id = mca.merchant_id
						   AND m.authority_id = a.authority_id
						 ORDER BY m.merchant_name|);
						
$merchant_popup->head('0','Undefined');
$merchant_popup->default($PARAM{merchant_id});

$table->put($table_rn,0,'Consumer Merchant','class=leftheader');
$table->put($table_rn,1, $merchant_popup);
$table_rn++;

display $table;

if($PARAM{consumer_id}) {
   print  qq|<br><input type="button" value="< Back" onClick="javascript:history.go(-1);">|;
   print  qq| <input type="submit" name="action" value="Search" onClick="return confirmSubmit()"><br><br>|;
} else {
   print  qq|<br><input type="submit" name="action" value="Search" onClick="return confirmSubmit()"><br><br>|;
}

if($PARAM{action}) {
   my $pageNav  = OOCGI::PageNavigation->new(15);

   my @table_cols = (
        ['Consumer ID',  -1],
        ['First Name',    2],
        ['Last Name',     3],
        ['Num Cards',     4],
        ['Num Trans',     5],
        ['Email',         6]
   );

   my @sql_cols = (
        'consumer_id',
        'LOWER(consumer_fname)',
        'LOWER(consumer_lname)',
        undef,
        undef,
        'LOWER(consumer_email_addr1)'
   );


my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $search_rn = 0;
my $search_cn = 0;
my $colspan  = $#table_cols + 1;
my $search   = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

while( my $column = $pageSort->next_column()) {
         $search->put($search_rn,$search_cn++,$column,'class=header1');
}
$search_rn++;

my $sql = $SQLB.$pageSort->sql_order_by_with(@sql_cols);
my $qo   = $pageNav->get_result_obj($sql, \@binds );

   while(my @data = $qo->next) {
      my $search_cn = 0;
      my $CID = $data[0]; # consumer_id
      my $button = qq|<a href="javascript:runThis('$CID');">$CID</a>|;
      $search->put($search_rn,$search_cn++, $button,'style="{text-align:right;}"');
      $search->put($search_rn,$search_cn++, $data[1]);
      $search->put($search_rn,$search_cn++, $data[2]);
      $search->put($search_rn,$search_cn++, $data[3],'style="{text-align:right;}"');
      $search->put($search_rn,$search_cn++, $data[4],'style="{text-align:right;}"');

      $search->put($search_rn,$search_cn++, $data[5]);
      $search_rn++;
   }
   display $search;

   my $page_table = $pageNav->table;
   display $page_table;
   $pageNav->enable();
   $pageSort->enable();
}

if($PARAM{consumer_id}) {
   my $consumer = USAT::DeviceAdmin::DBObj::Consumer->new($PARAM{consumer_id});

   my $str = $consumer->consumer_fname.' '.$consumer->consumer_lname;
   $str = qq|$str (<a href="edit_consumer.cgi?consumer_id=$PARAM{consumer_id}">Edit</a>)|;
   print B($str),'<br><br>';
   my $Consumer = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
   $Consumer->put(0,0,'Consumer ID',$style1);
   $Consumer->put(0,1,$consumer->consumer_id);
   $Consumer->put(0,2,'Created',$style1);
   $Consumer->put(0,3,$consumer->created_ts);
   
   $Consumer->put(1,0,'Consumer Type',$style1);
   $Consumer->put(1,1,$consumer->Consumer_type->consumer_type_desc);
   $Consumer->put(1,2,'Last Updated',$style1);
   $Consumer->put(1,3,$consumer->last_updated_ts);

   $Consumer->put(2,0,'First Name',$style1);
   $Consumer->put(2,1,$consumer->consumer_fname);
   $Consumer->put(2,2,'Last Name',$style1);
   $Consumer->put(2,3,$consumer->consumer_lname);

   $Consumer->put(3,0,'Primary Email',$style1);
   $Consumer->put(3,1,$consumer->consumer_email_addr1,'colspan=3');
   
   $Consumer->put(4,0,'Notification Email',$style1);
   $Consumer->put(4,1,$consumer->consumer_email_addr2,'colspan=3');

   display $Consumer;
   
   BR;
   print B('Consumer Accounts (Cards)');
   BR(2);

   my $sql = qq|
   	SELECT ca.consumer_acct_id,
           ca.consumer_acct_cd,
           ca.consumer_acct_balance,
           l.location_id,
           l.location_name,
           ca.consumer_acct_active_yn_flag,
           caf.consumer_acct_fmt_id,
           caf.consumer_acct_fmt_name
	FROM consumer_acct ca,
         location.location l,
         pss.consumer_acct_fmt caf
	WHERE consumer_id = $PARAM{consumer_id}
	AND ca.consumer_acct_fmt_id = caf.consumer_acct_fmt_id(+)
	AND ca.location_id = l.location_id
	ORDER BY ca.created_ts|;
   
   my @objs= USAT::DeviceAdmin::DBObj::Consumer_acct->objects($sql);

	my $counter = 0;

	### kludge to work around javascript passing different types in single vs. multiple checkbox cases ###
	my $js_func_value;
	if (scalar(@objs) > 1) {
		$js_func_value = 'this.form.download_card';
	} else {
		$js_func_value = 'new Array(this.form.download_card)';
	}
   
   my $Accounts = OOCGI::NTable->new('border="1" width="100%" cellpadding="2" cellspacing="0" ');
   my $Accounts_rn = 0;
   my $Accounts_cn = 0;
   $Accounts->put($Accounts_rn,$Accounts_cn++, 'Card Code',$style1);
   $Accounts->put($Accounts_rn,$Accounts_cn++, 'Balance',$style1);
   $Accounts->put($Accounts_rn,$Accounts_cn++, 'Location',$style1);
   $Accounts->put($Accounts_rn,$Accounts_cn++, 'Card Type',$style1);
   $Accounts->put($Accounts_rn,$Accounts_cn++, 'Active',$style1);
   $Accounts->put($Accounts_rn,$Accounts_cn++, 'Download',$style1);
   $Accounts_rn++;
   foreach my $obj ( @objs ) {
      $Accounts_cn = 0;
      $Accounts->put($Accounts_rn,$Accounts_cn,  $obj->consumer_acct_cd,$style3);
      $Accounts->linkto($Accounts_rn,$Accounts_cn++,'edit_consumer_acct.cgi?consumer_acct_id='.$obj->consumer_acct_id);
      $Accounts->put($Accounts_rn,$Accounts_cn++,$obj->consumer_acct_balance,$style3);
      $Accounts->put($Accounts_rn,$Accounts_cn  ,$obj->location_name);
      $Accounts->linkto($Accounts_rn,$Accounts_cn++,'edit_location.cgi?location_id='.$obj->location_id);
      $Accounts->put($Accounts_rn,$Accounts_cn++,$obj->consumer_acct_fmt_name,$style3);
      $Accounts->put($Accounts_rn,$Accounts_cn++,($obj->consumer_acct_active_yn_flag eq 'Y' ? B('Yes','green') : B('No','red')),$style3);
      $Accounts->put($Accounts_rn,$Accounts_cn++  ,qq|<input type="checkbox" name="download_card" value="| . $obj->consumer_acct_id . qq|" onClick="javascript:count_check($js_func_value)">|, 'align=center');
      $Accounts_rn++;
      $counter++;
   }
   display $Accounts;
   print qq|<div align="right"><input type=button value="Check All" onClick="this.value=check($js_func_value)"> <input type="text" id="check_count" name="check_count" value="0"></div>|;
   print qq|<input type=submit name="action" value="Download CSV Database"> <br>&nbsp;|;
   BR;
   print B('Consumer Transactions');
   BR(2);
   $sql = "SELECT t.tran_id,
                  to_char(t.tran_start_ts, 'MM/DD/YYYY HH24:MI:SS'),
                  tran_state_desc state,
                  ca.consumer_acct_cd,
                  ca.consumer_acct_id,
                  d.device_serial_cd,
                  d.device_id
             FROM consumer_acct ca, 
                  tran t,
                  pos p,
                  device_type dt,
                  device d,
                  pos_pta pp,
                  tran_state ts
            WHERE ca.consumer_id = ?
              AND ca.consumer_acct_id = t.consumer_acct_id
              AND t.pos_pta_id = pp.pos_pta_id
              AND pp.pos_id = p.pos_id
              AND p.device_id = d.device_id
              AND d.device_type_id = dt.device_type_id
              AND t.tran_state_cd = ts.tran_state_cd
   ";

   my $pageNav  = OOCGI::PageNavigation->new(15);

   my @table_cols = (
        ['Tran ID',            2],
        ['Auth Time',         -1],
        ['State',              3],
        ['Consumer Account',   4],
        ['Serial Number',      5]
   );

   my @sql_cols = (
        'tran_id',
        "NVL(tran_start_ts, TO_DATE('1970', 'YYYY'))",
        'state',
        'consumer_acct_id',
        'device_serial_cd'
   );

   my $pageSort = OOCGI::PageSort::Simple->new(
      data => \@table_cols
   );

   my $Transactions_rn = 0;
   my $Transactions_cn = 0;
   my $colspan  = $#table_cols + 1;
   my $Transactions = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

   while( my $column = $pageSort->next_column()) {
         $Transactions->put($Transactions_rn,$Transactions_cn++,$column,'class=header1');
   }
   $Transactions_rn++;

   $sql = $sql.$pageSort->sql_order_by_with(@sql_cols);

   my $qo   = $pageNav->get_result_obj($sql, [ $PARAM{consumer_id} ] );

   while(my @data = $qo->next) {
      $Transactions_cn = 0;
      $Transactions->put($Transactions_rn,$Transactions_cn  ,$data[0],$style3); $Transactions->linkto($Transactions_rn,$Transactions_cn++,'tran.cgi?tran_id='.$data[0]);
      $Transactions->put($Transactions_rn,$Transactions_cn++,$data[1],$style3);
      $Transactions->put($Transactions_rn,$Transactions_cn++,$data[2]);
      $Transactions->put($Transactions_rn,$Transactions_cn,$data[3],$style3); $Transactions->linkto($Transactions_rn,$Transactions_cn++,'edit_consumer_acct.cgi?consumer_acct_id='.$data[4]);
      $Transactions->put($Transactions_rn,$Transactions_cn,$data[5],$style3); $Transactions->linkto($Transactions_rn,$Transactions_cn++,'profile.cgi?device_id='.$data[6]);
      
      $Transactions_rn++;
   }
   display $Transactions;

   my $page_table = $pageNav->table;
   display $page_table;

   $pageNav->enable();
   $pageSort->enable();
}

print q|<input type="hidden" name="consumer_id" value="">|;
print q|</form>|;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

$query->DESTROY(1);

sub printError
{
	my $err_txt = shift;
	print B("$err_txt<br>\n", "red");
	print B("Card generation failed! For help, click OK then send this screen output to an administrator.<br>\n", "red");
	print "<div align=left><pre>\n\n". Dumper($query) . "\n\n</pre><div>";
	print q|<script language="javascript">window.alert("Card generation failed! For help, click OK then send this screen output to an administrator.");</script>|;
    USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
    $query->DESTROY(1);
    exit;
}

