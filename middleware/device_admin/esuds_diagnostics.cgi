#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

my $device_id = $PARAM{"device_id"};
if(length($device_id) == 0)
{
	print "Required Parameter Not Found: device_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

print "
<table border=\"1\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
 <tr>
  <th colspan=\"13\" bgcolor=\"#C0C0C0\">Active Room Diagnostic Codes</th>
 </tr>
 <tr>
  <th bgcolor=\"#C0C0C0\">Host ID</th>
  <th bgcolor=\"#C0C0C0\">Port</th>
  <th bgcolor=\"#C0C0C0\">Type</th>
  <th bgcolor=\"#C0C0C0\">Position</th>
  <th bgcolor=\"#C0C0C0\">Label</th>
  <th bgcolor=\"#C0C0C0\">Code</th>
  <th bgcolor=\"#C0C0C0\">Value</th>
  <th bgcolor=\"#C0C0C0\">Started </th>
  <th bgcolor=\"#C0C0C0\">Last Reported</th>
  ";

my $get_diag_stmt = $dbh->prepare(q{
	SELECT   HOST.host_id,
			 TO_CHAR (HOST.host_last_start_ts, 'MM/DD/YYYY HH:MI:SS AM'),
			 HOST.host_status_cd, HOST.host_type_id, HOST.host_port_num,
			 HOST.host_position_num, HOST.host_label_cd, host_type.host_type_desc,
			 device_type_host_type.device_type_host_type_cd,
			 host_diag_status.host_diag_cd, host_diag_status.host_diag_value,
			 TO_CHAR (host_diag_status.host_diag_start_ts,
					  'MM/DD/YYYY HH:MI:SS AM'
					 ),
			 TO_CHAR (host_diag_status.host_diag_last_reported_ts,
					  'MM/DD/YYYY HH:MI:SS AM'
					 )
		FROM device, HOST, host_diag_status, host_type, device_type_host_type
	   WHERE device.device_id = HOST.device_id
		 AND HOST.host_id = host_diag_status.host_id
		 AND HOST.host_type_id = host_type.host_type_id
		 AND device_type_host_type.host_type_id = host_type.host_type_id
		 AND device_type_host_type.device_type_id = device.device_type_id
		 AND device.device_id = :device_id
		 AND host_diag_status.host_diag_clear_ts IS NULL
	ORDER BY HOST.host_port_num, HOST.host_position_num
}) or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$get_diag_stmt->bind_param(":device_id", $device_id);
$get_diag_stmt->execute();
while (my @data = $get_diag_stmt->fetchrow_array()) 
{
	my $host_id = $data[0]; 
	my $host_port = $data[4]; 
	my $host_type_name = $data[7]; 
	my $host_position = $data[5]; 
	my $host_label = $data[6]; 
	my $host_diag_code = $data[9]; 
	my $host_diag_value = $data[10]; 
	my $host_diag_start_ts = $data[11]; 
	my $host_diag_reported_ts = $data[12]; 
	
	print "
	 <tr>
	  <td><a href=\"host_profile.cgi?host_id=$host_id\">$host_id</a>&nbsp;</td>
	  <td>$host_port&nbsp;</td>
	  <td>$host_type_name&nbsp;</td>
	  <td>" . ($host_position eq '0' ? 'Bottom' : 'Top') . "&nbsp;</td>
	  <td>$host_label&nbsp;</td>
	  <td>$host_diag_code&nbsp;</td>
	  <td>$host_diag_value&nbsp;</td>
	  <td>$host_diag_start_ts&nbsp;</td>
	  <td>$host_diag_reported_ts&nbsp;</td>
	 </tr>";
}

$get_diag_stmt->finish();
	  
print "
</table>
";

$dbh->disconnect;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
