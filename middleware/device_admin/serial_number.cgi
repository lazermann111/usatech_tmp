#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use OOCGI::NTable;
use USAT::DeviceAdmin::DBObj;
use USAT::App::DeviceAdmin::Config qw( :globals );
use Net::SMTP;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;

my %PARAM = $query->Vars;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;


USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
$session->print_menu;

my $regex_sql = q{
   SELECT device_type_serial_cd_regex
     FROM device.device_type
    WHERE device_type_id IN (
                 SELECT device_type_id
                   FROM device.device_serial_format)
};

my $qo_regex  = OOCGI::Query->new($regex_sql );
my @regex_arr = ();
while(my @arr = $qo_regex->next_array) {
   push(@regex_arr, $arr[0]);
}
my $regex_str   = join('\|',@regex_arr);
my $regex_email = '/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/';

print <<"SCRIPT";
<form name='serial_number' action='serial_number.cgi' onsubmit='return Validate()' method='post'>
<style>
td.header {
   text-align:center;
   color:#FFFFFF;
   background-color:#00C100;
}
td.row0 {
   background-color:#9FFF9F;
   text-align:right;
}
td.row1 {
   background-color:#E7FFE7;
   text-align:right;
}
</style>

<script>
var which_button = 0;
function Validate()
{
    if(!(typeof document.serial_number.userOP  == 'undefined')) {
       if(document.serial_number.device_serial_info_email.value == '') {
          alert('You must enter valid email address!');
          return false;
       }
       var reg = $regex_email;
       var email_address = document.serial_number.device_serial_info_email.value;
       if(reg.test(email_address) == false ) {
          alert('You must enter valid email address!');
          return false;
       }
       if(document.serial_number.device_serial_info_desc.value == '') {
          alert('You must enter description of your action!');
          return false;
       }
       if(document.serial_number.num_to_allocate.value == '') {
          alert('You must enter allocated amount!');
          return false;
       }
       return true;
    }

    if(!(typeof document.serial_number.userOP1  == 'undefined')) {
    if(which_button == 1) {
       if((document.serial_number.device_serial_format_id.value != '-1') &&
          (document.serial_number.device_serial_format_id.value != '')) {
          return true;
       } else {
          alert('You must select a device type and a serial number format! ');
       }
       return false;
    }
    }

    if(!(typeof document.serial_number.userOP2  == 'undefined')) {
    if(which_button == 2) {
//     var reg = new RegExp('[EG][0-9][0-9][0-9][0-9][0-9][0-9][0-9]');
       var reg = new RegExp('$regex_str');
       var str = document.serial_number.txt_serial_number.value;
       if( reg.test(str.toUpperCase()) ) {
          return true;
       } else {
          alert('You must enter correct format for serial number!');
          return false;
       }
    }
    }
    return false;
}

//function set_start_at_to_zero() {
//    document.getElementById('num_to_start').value = '';
//}

function populateStart(val) {
    var obj = document.getElementById('num_to_start');
    obj.value = val;
}
function submitForm() {
    document.serial_number.submit();
}
function selectFormat() {
    var obj = document.getElementById('device_type');
    var obj2 = document.getElementById('format_id');
    var device_type = obj.value;
SCRIPT

    my $device_type_sql = '
       SELECT device_type_id
         FROM device.device_type
        WHERE device_type_id IN (
              SELECT device_type_id
                FROM device.device_serial_format)';

    my $qo_dt     = OOCGI::Query->new($device_type_sql);
    while(my @arr = $qo_dt->next_array) {
print qq^
   if(device_type == $arr[0]) {
      obj2.innerHTML="<select name='device_serial_format_id'><option selected='selected' value=''>Please Select</option>^;

      my $sql = 'SELECT device_serial_format_id, device_serial_format_name 
                   FROM device_serial_format where device_type_id = ?';
      my $qo  = OOCGI::Query->new($sql, { bind => "$arr[0]" });
      while(my %hash = $qo->next_hash) {
         print "<option value='$hash{device_serial_format_id}'>$hash{device_serial_format_name}</option>";
      }
print ' </select>";
   }';
}
print '
}
</script>';
my $rn = 0;
my $format = {
   table_format => "align=center border=1 cellpadding=0 cellspacing=0 width=100%"
};
my $TForm = OOCGI::NTable->new($format);
my $example = OOCGI::NTable->new($format);
$example->insert(0,0,B('Prefix'));
$example->insert(0,1,B('Value'));
$example->insert(0,2,B('Suffix'));
$example->insert(0,3,'G4[0-9]{6}','rowspan=2');

if($PARAM{device_type_id} >= 0 && $PARAM{device_serial_format_id} ) {
   my $format_obj = USAT::DeviceAdmin::DBObj::Device_serial_format->new($PARAM{device_serial_format_id});
   my $txtPrefix = OOCGI::Text->new(name => 'prefix',
                   readonly => 'yes', 
                   value => $format_obj->device_serial_format_prefix);
   my $device_suffix_regex = USAT::DeviceAdmin::DBObj::Device_type->new($PARAM{device_type_id})->device_type_serial_cd_regex;
   $example->insert(0,3,$device_suffix_regex,'rowspan=2');
   $example->insert(1,0,$txtPrefix);
#  $example->insert(1,0,B($format_obj->device_serial_format_prefix,'red'));
   $example->insert(1,1,B('123456','green'));
   $example->insert(1,2,B($format_obj->device_serial_format_suffix,'red'));
} else {
   $example->insert(0,3,'G5[0-9]{6}','rowspan=2');
   $example->insert(1,0,B('G5','red'));
   $example->insert(1,1,B('123456','green'));
   $example->insert(1,2,B('NA','red'));
}
$TForm->insert($rn++,0,$example,'colspan=2');

$TForm->insert($rn,  0,B('Enter a valid Email Address:'));
my $txtEmailAddress = OOCGI::Text->new(name => 'device_serial_info_email',size => 40);
$TForm->insert($rn++,1,$txtEmailAddress);
$TForm->insert($rn,  0,B('Please Enter Description of the Job:'));
my $areaJobDescription = OOCGI::TextArea->new(name => 'device_serial_info_desc',cols => 50, rows => 5);
$TForm->insert($rn++,1,$areaJobDescription);
$TForm->insert($rn,  0,B('Number of Serial Num to Allocate:'));
my $txtAllocateNum = OOCGI::Text->new(name => 'num_to_allocate');
$txtAllocateNum->default($PARAM{num_to_allocate});
$TForm->insert($rn++,1,$txtAllocateNum);
$TForm->insert($rn,  0,B('Serial Number to Start At:'));
my $txtNumberStart = OOCGI::Text->new(name => 'num_to_start', id => 'num_to_start');
$txtNumberStart->default($PARAM{num_to_start});
$TForm->insert($rn++,1,$txtNumberStart);

#my $radio1 = OOCGI::Radio->new(name => 'insert_type', value => 'N', onclick => 'set_start_at_to_zero()',
my $radio1 = OOCGI::Radio->new(name => 'insert_type', value => 'N', label => B('Next Available'));

if($PARAM{insert_type} eq 'N') { $radio1->checked('yes'); }

$TForm->insert($rn,0, $radio1);
my $radio2 = OOCGI::Radio->new(name => 'insert_type', value => 'S',
                    label => B('Sequantial Guaranteed'));

if($PARAM{insert_type} eq 'S') { $radio2->checked('yes'); }
if(!$PARAM{insert_type}) { $radio2->checked('yes'); }
$TForm->insert($rn,1, $radio2);
if($PARAM{userOP1} eq 'Assign Numbers') {
   BR;
   print B('USAT Device Serial Number Assignment Utility',3);
   BR(2);
   display $TForm;
   print qq|<input type="hidden" name="device_serial_format_id" value=$PARAM{device_serial_format_id}>|;
   print qq|<input type="hidden" name="device_type_id" value=$PARAM{device_type_id}>|;
   print qq|<br><input type="submit" name="userOP" value="Submit"><br><br>|;
}

if($PARAM{userOP2} eq 'List Assigned Logs') {
   my $format = {
      table_format => "align=center border=1 cellpadding=0 cellspacing=0 width=60%"
   };
   my $TResult = OOCGI::NTable->new($format);
   my $sql = 'SELECT *
                FROM device_serial ds
               WHERE ds.device_serial_info_id
                  IN ( SELECT device_serial_info_id
                         FROM device_serial
                        WHERE device_serial_cd = ?)
               ORDER BY ds.device_serial_cd';

   my $qo = OOCGI::Query->new($sql,'hash',uc($PARAM{txt_serial_number}));
   my $device_serial_info_id;
   # note that row number starts at 3. This is because we are going to add
   # headers later which comes from the sql
   my $rn = 7;
   my $string = '';
   while(my %hash = $qo->next) {
       $string .= "$hash{device_serial_cd}\n";
       $device_serial_info_id = $hash{device_serial_info_id};
       $TResult->insert($rn,0,$hash{device_serial_id},"class=row".$rn%2);
       $TResult->insert($rn,1,$hash{device_serial_cd},"class=row".$rn%2);
       $rn++;
   }

   my $objDeviceSerialInfo = USAT::DeviceAdmin::DBObj::Device_serial_info->new($device_serial_info_id);
   $TResult->insert(0,0, 'Email:');
   $TResult->insert(0,1, $objDeviceSerialInfo->device_serial_info_email);
   $TResult->insert(1,0, 'Description:');
   $TResult->insert(1,1, $objDeviceSerialInfo->device_serial_info_desc);
   my $areaDeviceSerialCD = OOCGI::TextArea->new(value => $string, rows => 10, cols => 15);
   $TResult->insert(2,0, 'You can select All<br> Device Serial CD from right');
   $TResult->insert(2,1, $areaDeviceSerialCD);
   my $submit = qq|<input type="submit" name="userOP" value="Start Over">|;
   $TResult->insert(3,0,$submit,'colspan=2 align=center');

   my $TList;
   if($PARAM{userOP2} ne 'List Assigned Logs') {
      $TList = list_table(\%PARAM);
   } else {
      $TList = '';
   }

   $TResult->insert(4,0,$TList,'colspan=2 align=center');
   $TResult->insert(5,0, B('Assigned Device List'),'align=center colspan=2 bgcolor=#CDCDCD');
   $TResult->insert(6,0, 'Device Serial ID','class=header');
   $TResult->insert(6,1, 'Device Serial CD','class=header');
   $rn++;

   display $TResult;
}
my @assigned;
my $visible_limit = 0;
my $send_email    = 1;
if($PARAM{device_serial_format_id} != '' && $PARAM{device_serial_format_id} != -1) {
   my $deviceSerialFormat = USAT::DeviceAdmin::DBObj::Device_serial_format->new($PARAM{device_serial_format_id});

   my $device_serial_format_length = $deviceSerialFormat->device_serial_format_length;
   my $device_serial_format_prefix = $deviceSerialFormat->device_serial_format_prefix;
   my $device_serial_format_suffix = $deviceSerialFormat->device_serial_format_suffix;
   my $digit_length = $device_serial_format_length - length ( $device_serial_format_prefix )
                                                   - length ( $device_serial_format_suffix );
   my $digit_format = '%0'.$digit_length.'d';

   my @sequence  =  &sequence(\%PARAM);
   # user entered a number to allocate device_type_cd
   my $areaAssigned;
   if($PARAM{num_to_allocate}) {
      my $count = $PARAM{num_to_allocate};
      if($PARAM{num_to_start}) { 
         # user might enter a start position
         if($PARAM{insert_type} eq 'S') {
            # user might ask sequantial numbers
            for(my $i = 1; $i <= $#sequence; $i++) {
                if($sequence[$i-1] == $PARAM{num_to_start}) {
                   # sequence has some data for this start position so don'd do anything
                   last;
                }
                if($sequence[$i-1] > $PARAM{num_to_start} && $sequence[$i] < $PARAM{num_to_start} ) { next; }
#print "AAA $sequence[$i-1]|$sequence[$i]|$PARAM{num_to_start}|$count <br>";
                my $diff = $sequence[$i] - $sequence[$i-1];
                if($diff <  $PARAM{num_to_allocate}) { next; }
                if(!$count) { last;}
                if($diff == 1) { next; }
                $diff--;
                for(my $j = $PARAM{num_to_start}; $j < $PARAM{num_to_start} + $diff; $j++) {
                    if(!$count) {
                       # we assigned all the numbers now is the time to ascape the loop
                       last;
                    } else {
                       $count--;
                    }
#                    print "AAA >>>>>>>>>>>>> E4 ", $j, ' count = ', $count, '<br>';
                    my $serialNumber = $PARAM{prefix}.sprintf($digit_format,$j);
                    push(@assigned, $serialNumber);
                    $areaAssigned .= ' '.$serialNumber."\n";
                }
            }
         } else {
            # default numbering is  any available number from starting position
            my $begin = $PARAM{num_to_start};
               for(my $i = 1; $i <= $#sequence; $i++) {
                  my $diff = $sequence[$i] - $sequence[$i-1];
                  if($diff > 1 && $begin > $PARAM{num_to_start} ) {
                     $begin = $sequence[$i-1] + 1;
                  }
#print "BBB $sequence[$i-1]|$sequence[$i]|$begin|$count <br>";
                  while($sequence[$i-1] < $begin && $sequence[$i] > $begin || $sequence[$i-1] == $sequence[$i] ) { 
                     if(!$count) { last;}
#                     print "BBB >>>>>>>>>>>>> E4 ", $begin, ' count = ', $count, '<br>';
                     my $serialNumber = $PARAM{prefix}.sprintf($digit_format,$begin);
                     push(@assigned, $serialNumber);
                     $areaAssigned .= ' '.$serialNumber."\n";
                     $begin++;
                     $count--;
                  }
             }
         }
      } else {
         # user does not care about a start position
         if($PARAM{insert_type} eq 'S') {
            # user might ask sequantial numbers
            for(my $i = 1; $i <= $#sequence; $i++) {
                my $diff = $sequence[$i] - $sequence[$i-1];
                if($diff <= $PARAM{num_to_allocate}) { next; }
                if(!$count) { last;}
                if($diff == 1) { next; }
                $diff--;
                for(my $j = $sequence[$i-1]+1; $j < $sequence[$i-1]+ $diff; $j++) {
                    if(!$count) {
                       # we assigned all the numbers now is the time to ascape the loop
                       last;
                    } else {
                       $count--;
                    }
#                    print "CCC >>>>>>>>>>>>> E4 ", $j, ' count = ', $count, '<br>';
                    my $serialNumber = $PARAM{prefix}.sprintf($digit_format,$j);
                    push(@assigned, $serialNumber);
                    $areaAssigned .= ' '.$serialNumber."\n";
                }
            }
         } else {
            # default numbering is  any available number from starting position
            for(my $i = 1; $i <= $#sequence; $i++) {
                if(!$count) { last;}
                my $diff = $sequence[$i] - $sequence[$i-1];
                if($diff == 1) { next; }
                $diff--;
                for(my $j = $sequence[$i-1]+1; $j <= $sequence[$i-1]+ $diff; $j++) {
                    if(!$count) {
                       # we assigned all the numbers now is the time to ascape the loop
                       last;
                    } else {
                       $count--;
                    }
#                    print "DDD >>>>>>>>>>>>> E4 ", $j, ' count = ', $count, '<br>';
                    my $serialNumber = $PARAM{prefix}.sprintf($digit_format,$j);
                    push(@assigned, $serialNumber);
                    $areaAssigned .= ' '.$serialNumber."\n";
                }
            }
         }
      }

      my $assigned_count = $#assigned +1;
      if($assigned_count > 0 ) {
         my $areaAssignedSerial = OOCGI::TextArea->new(name => 'assigned_serial',
                                           value => $areaAssigned,
                                           rows => 10, cols => 15);
         BR;
         print B("$assigned_count Generated Serial Numbers:",'red',3);
         BR(2);
         print B('A copy of this list has been emailed to you for your records.','red',3);
         BR(2);
         print $areaAssignedSerial->string;
         BR;
         print B('Select all and Copy Paste.','red',3);
      } else {
         $send_email = 0;
         print B('<br><br>Unable to assign serial numbers in the range you requested!
         <br>There are not enough available serial numbers to complete your request!<br><br>','red');
      }
#     @sequence  =  &sequence(\%P);
   }
   if(!$PARAM{userOP}) {   
      my $TList = list_table(\%PARAM);
      display $TList;
   }
}

if($PARAM{userOP} eq 'Submit' && $send_email) {
   my $deviceSerialInfo = USAT::DeviceAdmin::DBObj::Device_serial_info->new;
   $deviceSerialInfo->device_serial_info_email($PARAM{device_serial_info_email});
   $deviceSerialInfo->device_serial_info_desc($PARAM{device_serial_info_desc});
   my $obj = $deviceSerialInfo->insert;
   if(defined $obj) {
      $deviceSerialInfo = $obj;
   } else {
      print $deviceSerialInfo->errstr;
   }
   my $DSII = $deviceSerialInfo->ID;
   my $string = ''; 
   foreach my $serial_code ( @assigned ) {
      $string .= "$serial_code\n";
      my $deviceSerial = USAT::DeviceAdmin::DBObj::Device_serial->new;
      $deviceSerial->device_serial_format_id($PARAM{device_serial_format_id});
      $deviceSerial->device_serial_info_id($DSII);
      $deviceSerial->device_serial_cd($serial_code);
      $obj = $deviceSerial->insert;
      if(defined $obj) {
         $deviceSerial = $obj;
      } else {
         print $deviceSerial->errstr;
      }
   }
   my $requested_insert_type = "";
   if($PARAM{insert_type} eq 'S') {
      $requested_insert_type = "SEQUENTIAL";
   } else {
      $requested_insert_type = "NON sequential ( NEXT AVAILABLE )";
   }
   my $count = $#assigned + 1;
   my $message = "This is an automatic mail from Serial Number Assignment Tool.\n\n
Your requested $count $requested_insert_type serial numbers listed below.\n\n$string\n\n";
       
   send_email(DA_CONSTANTS__SERIAL_NUMBER_EMAIL, $PARAM{device_serial_info_email},'Serial Numbers',$message);
}

$format = {
   table_format => "align=center border=1 cellpadding=0 cellspacing=0 width=100%"
};

my $TFirst = OOCGI::NTable->new($format);

my $sql = "SELECT device_type_id, device_type_desc
          FROM device.device_type
         WHERE device_type_id IN( 
                                  SELECT device_type_id
								    FROM device.device_serial_format )
      ORDER BY device_type_desc"; 

my $popDeviceType = OOCGI::Popup->new(sql => $sql, name => "device_type_id", id => "device_type",
                    onchange => "selectFormat();");
#                   onchange => "selectFormat(this.form);");

$popDeviceType->head('-1','Please Select');

if($PARAM{device_type_id} >= 0) {
   $popDeviceType->default($PARAM{device_type_id});
} else {
   $popDeviceType->default(-1);
}

$rn = 0;
$TFirst->insert($rn,  0,B('Device Type:'));
$TFirst->insert($rn++,1,$popDeviceType);

my $popDeviceSerialFormat;
if($PARAM{device_serial_format_id}) {
   $sql = "SELECT device_serial_format_id, device_serial_format_name
             FROM device_serial_format
            WHERE device_type_id = $PARAM{device_type_id}
         ORDER BY device_serial_format_name"; 

    $popDeviceSerialFormat = OOCGI::Popup->new( sql => $sql,
                             id   => "device_serial_format_id",
                             name => "device_serial_format_id");
    $popDeviceSerialFormat->head('-1','Please Select');
    $popDeviceSerialFormat->default($PARAM{device_serial_format_id});
} else {
    $popDeviceSerialFormat = OOCGI::Popup->new( name => 'device_serial_format_id');
    $popDeviceSerialFormat->head('-1','Please Select');
    $popDeviceSerialFormat->default(-1);
}

$TFirst->insert($rn,  0,B('Serial Number Format:'));
$TFirst->insert($rn++,1,'<span id="format_id">'.$popDeviceSerialFormat.'</span>');
my $submit_assign = q|<input type="submit" name="userOP1"
                      onclick="javascript:which_button=1;" value="Assign Numbers">|;
$TFirst->insert($rn,0,'If you want to assign serial numbers, please select a 
    Device Type and A Serial number format and press '.B('Assign Numbers','red').' button. 
    This will take you next screen where you will enter necessary info.', 'align=justify width=60%');
$TFirst->insert($rn,1,$submit_assign,'align=center');
$rn++;
$TFirst->insert($rn,  0,B('Enter A Serial Number:'));
my $txtSerial_number = OOCGI::Text->new( name => 'txt_serial_number');
$txtSerial_number->default($PARAM{txt_serial_number});
$TFirst->insert($rn++,1,$txtSerial_number);
##########$TFirst->insert($rn,0,'To display serial numbers entered previously, Please select
##########   a Device Type, Serial Number Format, Serial Number and press '.
$TFirst->insert($rn,0,'To display serial numbers entered previously, Please enter
   a valid Serial Number and press '.
   B('List Assigned Logs','red').' button.','align=justify');
my $submit_log    = q|<input type="submit" name="userOP2"
                      onclick="javascript:which_button=2;" value="List Assigned Logs">|;
$TFirst->insert($rn,1,$submit_log,'align=center');
$rn++;

if($PARAM{userOP} eq 'Submit') {
   print qq|<br><input type="submit" name="userOP" value="Start Over">|;
} elsif($PARAM{userOP1} eq 'Assign Numbers') {
   ;
} elsif($PARAM{userOP2} eq 'List Assigned Logs') {
   ;
} else {
   BR;
   print B('USAT Device Serial Number Assignment Utility',3);
   BR(2);
   display $TFirst;
#  print qq|<br>$submit_assign $submit_log |;
}

BR(2);
print '</form>';

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");

sub sequence {
   my $temp = shift;
   my %P = %{$temp};
   my $format_obj = USAT::DeviceAdmin::DBObj::Device_serial_format->new($P{device_serial_format_id});
   my $prefix = $format_obj->device_serial_format_prefix;
   my $suffix = $format_obj->device_serial_format_suffix;
   my $format_len = $format_obj->device_serial_format_length;
   my $max_len = $format_len - length($prefix) - length($suffix);
   my $max_value;
   for(my $i=0; $i<$max_len; $i++) {
    $max_value .= '9'; 
   }

   $max_value++;

   my $sql = "SELECT device_serial_cd
                FROM device_serial
               WHERE device_serial_format_id = $P{device_serial_format_id}
            ORDER BY device_serial_cd";

   my $qo = OOCGI::Query->new($sql);

   my $firsttime = 1;
   my @sequence  = (0);
   my $previus_number;
   if($qo->rowNumber > 0 ) {
   while(my @arr = $qo->next_array) {
      $_ = $arr[0];
      s/$prefix//g;
      my $number = $_+0;
      if($firsttime) {
         $firsttime = 0;
         $previus_number = $number;
         push(@sequence,$number);
      } else {
         if($number == $previus_number - 1) {
            $previus_number++;
         } else {
            push(@sequence,$number);
            $previus_number = $number;
         }
      }
#    print "RRR $number|$previus_number|$arr[0]<br>";
   }
   } else {
       @sequence  = ($P{num_to_start});
            push(@sequence,$P{num_to_start});
   }

   push(@sequence, $max_value);

   # now make sure that if sequential record is asked 
   # there is no existing record in the sequence
   if($P{insert_type} eq 'S') {
      my $range_flag = 1;
      my $count = $P{num_to_allocate};
      my $begin = $P{num_to_start};
      my $end   = $begin + $count;
      for(my $i = 1; $i <= $#sequence; $i++ ) {
          if(($begin > $sequence[$i-1]) && ( $begin < $sequence[$i])) {
             if($end   > $sequence[$i-1] && $end   <= $sequence[$i]) {
                ; # everything is fine
                $range_flag = 0;
             } else {
                @sequence = ();
                BR(2);
                print B('There are existing records in the range!','red',4);
                BR(2);
                last;
             }
          }
      }
      if($range_flag) {
         @sequence = ();
         BR(2);
         print B('There are existing records in the range!','red',4);
         BR(2);
      }
   }
   return @sequence;
}

sub send_email{
    my ($from_addr, $to_addrs, $subject, $content) = @_;
    my @to_array = split(/ |,/, $to_addrs);

    my $smtp = Net::SMTP->new('mailhost');

    if(not defined $smtp) {
	warn "send_email: Failed to connect to SMTP server mailhost!\n";
	return 0;
    }
    $smtp->mail($from_addr);
    foreach my $to_addr (@to_array) {
	$smtp->to($to_addr);
#   print "HELLO $from_addr $to_addr $subject $content";
    }
    $smtp->data();
    $smtp->datasend("Subject: $subject\n");
    $smtp->datasend("\n");
    $smtp->datasend("$content\n\n");
    $smtp->dataend();
    $smtp->quit(); 
    return 1;
}

sub list_table {
   my $temp = shift;
   my %P = %{$temp};
   my $TList = OOCGI::NTable->new;
   my @sequence  =  &sequence(\%P);
   my $rn_list = 0;
   $TList->insert($rn_list,0,'Range Start', 'class=header');
   $TList->insert($rn_list,1,'Range End',  'class=header');
   $TList->insert($rn_list,2,'Range Count','class=header');
   $rn_list++;
   my $sum = 0;
   for(my $i = 1; $i <= $#sequence; $i++) {
       my $diff = $sequence[$i] - $sequence[$i-1];
       $diff--;
       if($diff < 1) { next; }
       $sum += $diff;
       my $start = $sequence[$i-1]+1;
       my $link = "<a id='start_value' href='javascript:populateStart($start);'>$start</a>";
       $TList->insert($rn_list, 0, $link,          "class=row".$rn_list%2);
       $TList->insert($rn_list, 1, $sequence[$i]-1,  "class=row".$rn_list%2);
       $TList->insert($rn_list, 2, $diff,          "class=row".$rn_list%2);
       $rn_list++;
   }
   $TList->insert($rn_list,0,'Total Available Numbers ==>  ',"colspan=2 class=row".$rn_list%2);
   $TList->insert($rn_list,2,$sum,"class=row".$rn_list%2);
   return $TList;
}
