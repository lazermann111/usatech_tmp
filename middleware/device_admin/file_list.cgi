#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::DBObj;
use USAT::DeviceAdmin::UI::USAPopups;
use OOCGI::NTable;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars();
my %E = ( B => '%', E => '' );

my $popup = OOCGI::Popup->new();
$popup->tail('B','Begins');
$popup->tail('E','Equals');
$popup->name('pop_file_name');

USAT::DeviceAdmin::UI::DAHeader->printHeader();

my %file_type_names = USAT::DeviceAdmin::UI::USAPopups->hash_file_transfer_type;

USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

my $pageNav  = OOCGI::PageNavigation->new(15);

my $ev_number = $PARAM{ev_number};
my $file_type = $PARAM{file_type};
my $file_name = $PARAM{file_name};
my $file_id   = $PARAM{file_id};
my $be = 'B';
my $txt_file_name;
if($PARAM{txt_file_name}) {
   $txt_file_name = $PARAM{txt_file_name};
   $be = uc $PARAM{pop_file_name};
   $popup->default($be);
   $file_name = $txt_file_name;
}

if($txt_file_name && $file_name) {
  $file_name = $txt_file_name;
} 
if(!$txt_file_name && $file_name) {
  $txt_file_name = $file_name;
} 

my $list_files_stmt;

my $hint = '';
my $sql = "file_transfer_id,
            file_transfer_name,
            file_transfer_type_cd,
            file_transfer_comment,
            TO_CHAR(created_ts,      'MM/DD/YYYY HH:MI:SS AM'),
            TO_CHAR(last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM')
       FROM device.file_transfer ft
    ";

my @binds = ();
my $where = '';
if(defined($file_type) && $file_type ge '0') {
   $hint = '/*+INDEX(ft IDX_FILE_TRANSFER_TYPE_CD)*/';
   $where = $where ? 'AND ' : 'WHERE ';
   $sql .= $where."
        file_transfer_type_cd = ? ";
   push(@binds, $file_type);
}
if(defined $file_name && $file_name ne '')
{
   $hint = '/*+INDEX(ft IDX_FILE_TRANSFER_NAME)*/';
   $where = $where ? 'AND ' : 'WHERE ';
   $sql .= $where."
      file_transfer_name LIKE ? ";
   push(@binds, "$txt_file_name$E{$be}");
}
if(defined($file_id) && $file_id > 0) {
   $hint = '/*+INDEX(ft PK_FILE_TRANSFER)*/';
   $where = $where ? 'AND ' : 'WHERE ';
   $sql .= $where."
        file_transfer_id = ? ";
   push(@binds, $file_id);
}

$sql = "SELECT $hint $sql";

my @table_cols = (
	['ID',           -2 ],
	['Name',          1 ],
	['Type',          3 ],
	['Comment',       4 ],
	['Create Date',  -5 ],
	['Last Updated', -6 ]
);

my @sql_cols = (
    'file_transfer_id',
    'file_transfer_name',
    'file_transfer_type_cd',
    'file_transfer_comment',
    'created_ts',
    'last_updated_ts'
);

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $cn_table = 0;
my $rn_table = 0;
my $colspan  = $#table_cols + 1;
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

my $txtField = OOCGI::Text->new(name => 'txt_file_name', value => $txt_file_name, size => 30);
my $filetype_text = $file_type_names{$file_type};
my $search = '<br><table><tr><td><b>File Type:</b></td><td colspan=3>'.$filetype_text.'</td></tr>
              <tr><td><b>File Name:</b></td><td>'.$popup.'</td><td>'.$txtField.'</td><td>'.
             ' <input type=submit name=userOP value=Submit></td></tr></table><br>';

print '<form>';
print $search;

$sql = $sql.$pageSort->sql_order_by_with(@sql_cols);
#print "SQL $sql <br>";
my $qo   = $pageNav->get_result_obj($sql, \@binds);
my $table = OOCGI::NTable->new('cellspacing="0" cellpadding="5" border="1" width="100%"');

my $header = $ev_number ? "Choose File to Upload to $ev_number" : "File Search Results";
$table->put($rn_table,0,B($header),"colspan=$colspan class=header0 align=center");
$rn_table++;

while( my $column = $pageSort->next_column()) {
         $table->put($rn_table,$cn_table++,$column,'class=header1');
}
$rn_table++;

my $row0 = "row0";
my $row1 = "row1";
my $i = 0;

while(my @data = $qo->next) {
   my $file_transfer_id      = $data[0];
   my $file_transfer_name    = $data[1];
   my $file_transfer_type_cd = $data[2];
   my $file_transfer_comment = $data[3];
   my $created_ts            = $data[4];
   my $last_updated_ts       = $data[5];
 
   my $row = $i%2 ? $row1 : $row0;
   if(defined $ev_number)
   {
        $table->put($rn_table,0,
        qq|<a href="file_details_func.cgi?file_id=$file_transfer_id&ev_number=$ev_number&action=Upload to a Device">$file_transfer_id</a>|,"class=$row");
        $table->put($rn_table,1, $file_transfer_name,"class=$row");
        $table->put($rn_table,2, $file_type_names{$file_transfer_type_cd},"class=$row");
        $table->put($rn_table,3, $file_transfer_comment,"class=$row");
        $table->put($rn_table,4, $created_ts,"class=$row");
        $table->put($rn_table,5, $last_updated_ts,"class=$row");
   } else {
        $table->put($rn_table,0,
        qq|<a href="file_details.cgi?file_id=$file_transfer_id">$file_transfer_id</a>|,"class=$row");
        $table->put($rn_table,1, $file_transfer_name,"class=$row");
        $table->put($rn_table,2, $file_type_names{$file_transfer_type_cd},"class=$row");
        $table->put($rn_table,3, $file_transfer_comment,"class=$row");
        $table->put($rn_table,4, $created_ts,"class=$row");
        $table->put($rn_table,5, $last_updated_ts,"class=$row");
   }
   $rn_table++;
   $i++;
}

display $table;

my $page_table = $pageNav->table;
display $page_table;

HIDDEN('file_type', "$PARAM{file_type}");
print "
</form>";
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
