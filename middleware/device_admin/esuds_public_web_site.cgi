#!/usr/local/USAT/bin/perl

use CGI::Carp qw(fatalsToBrowser);
use OOCGI::Query;
use OOCGI::OOCGI;
use OOCGI::NTable;
use OOCGI::PageNavigation;
use OOCGI::PageSort::Simple;
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");

print $user_menu;

print q|<form method="post" name="esud_school_list" action="esuds_public_web_site.cgi">|;

my $pageNav  = OOCGI::PageNavigation->new(25);

my $sql = qq{
    SELECT location_name,
           'http://' || subdomain_name_cd || '.' || domain_name_url,
           location_id
      FROM location.location INNER JOIN APP_USER.SUBDOMAIN_NAME ausn
        ON location.location_id = ausn.OBJECT_CD
     WHERE location_active_yn_flag = 'Y'
       AND location_type_id = 6   
   AND 'http://' || subdomain_name_cd || '.' || domain_name_url like '%esuds.net%'
    ORDER BY location_name
};

my @table_cols = (
        ['School'          , 1],
        ['Public Web Site' , 2],
        ['Location'        , 3]
);

my @sql_cols = (
        'location_name',
        "'http://' || subdomain_name_cd || '.' || domain_name_url",
        'location_id'
);

my $rownum = 0;
my $colnum = 0;
my $table = OOCGI::NTable->new('width=100% border="1"');

my $pageSort = OOCGI::PageSort::Simple->new(
   data => \@table_cols
);

my $sql_data = q{ SELECT }.join(',',@sql_cols).q{ 
      FROM location.location INNER JOIN APP_USER.SUBDOMAIN_NAME ausn
        ON location.location_id = ausn.OBJECT_CD
     WHERE location_active_yn_flag = 'Y'
       AND location_type_id = 6
   AND 'http://' || subdomain_name_cd || '.' || domain_name_url like ?
};

my @column_sort = ();
while( my $column = $pageSort->next_column()) {
    $table->put($rownum,$colnum++,$column,'class=header1');
}
$colnum = 0;

$sql_data = $sql_data.$pageSort->sql_order_by_with(@sql_cols);

my $qo   = $pageNav->get_result_obj($sql_data, ['%esuds.net%'] );

$rownum++;
while(my @arr = $qo->next_array) {
#  $table->put($rownum,$colnum++, $arr[0]);
   $table->put($rownum,$colnum++,
               "<a href='/edit_location.cgi?location_id=$arr[2]'>$arr[0]</a>");
   $table->put($rownum,$colnum++, "<a href='$arr[1]'>$arr[1]</a>");
#  $table->put($rownum,$colnum++,
#              "<a href='/edit_location.cgi?location_id=$arr[2]'>$arr[2]</a>",'align=right');
   $colnum = 0;
   $rownum++;
}
$table->print_columns([0,1]);
display $table;

my $page_table = $pageNav->table;
display $page_table;

print '</form>';
$pageNav->enable();
$pageSort->enable();

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
