#!/usr/local/USAT/bin/perl


use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::Profile::PaymentType;
use Date::Calc;
use Data::Dumper;
use USAT::DeviceAdmin::UI::DAHeader;

use constant DEBUG => 0;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;	

my %PARAM = $query->Vars;

our $dbh;
&main();

sub main 
{
	my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
	$dbh = $DATABASE->{handle};

	### print header and some javascript ###
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	if(DEBUG)
	{
		print "<pre>\n";
		print Dumper(\%PARAM);
		print "</pre>\n";
	}

	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;

	print <<"	EOHTML";
	  <script language="JavaScript" type="text/javascript">
	  <!--
		function confirmSubmit() {
			var agree=confirm("Are you sure you want to continue with this operation?");
			if (agree) {
                if(document.getElementById('override_amount_id').value == '' && document.getElementById('override_limit_id').value == '') {
                    return true;
                }
                if(document.getElementById('override_amount_id').value > 0 && document.getElementById('override_limit_id').value == '') {
                  alert('Please enter Override Limit');
                  return false; 
                }
                if(document.getElementById('override_amount_id').value.match('^[0-9]+(\\.[0-9]{0,2})?\$') == null) {
                    alert("Invalid Number in Override amount.\\n\\nValid Numbers are like 1, 1.2 or 1.25\\n Please Enter Valid Number.");
                    return false;
                }
                if(document.getElementById('override_limit_id').value.match('^[0-9]+(\\.[0-9]{0,2})?\$') == null) {
                    alert("Invalid Numberin Override limit.\\n\\nValid Numbers are like 1, 1.2 or 1.25\\n Please Enter Valid Number.");
                    return false;
                }
			    return true;
		     } else {
			    return false;
             }
		}
	  // -->
	  </script>
	EOHTML
	
	my $pos_pta_tmpl_id = length $PARAM{pos_pta_tmpl_id} > 0 ? $PARAM{pos_pta_tmpl_id} : printError("Required Parameter Not Found: pos_pta_tmpl_id");
	my $pos_pta_tmpl_entry_id = length $PARAM{pos_pta_tmpl_entry_id} > 0 ? $PARAM{pos_pta_tmpl_entry_id} : 0;
	my $pp_payment_subtype_id = length $PARAM{payment_subtype_id} > 0 ? $PARAM{payment_subtype_id} : 0;
	
	printError("Required Parameter Not Found: payment_subtype_id") if($pp_payment_subtype_id == 0 && $pos_pta_tmpl_entry_id == 0);

	my $err;
	
	my @template_data = USAT::DeviceAdmin::Profile::PaymentType::get_template_data($dbh, $pos_pta_tmpl_id, \$err);
	printError("<br><br>$err<br><br>") if defined $err;
	print Dumper(\@template_data)."-->" if DEBUG;
	printError("<br><h3>Template not found: $pos_pta_tmpl_id</h3><br><br>") unless scalar @template_data;

	my (	$pp_pos_pta_device_serial_cd, 
			$pp_pos_pta_encrypt_key, 
			$pp_pos_pta_encrypt_key2, 
			$pp_payment_subtype_key_id, 
			$pp_pos_pta_pin_req_yn_flag,
			$ps_authority_payment_mask_id,
			$pp_authority_payment_mask_id,
			$ps_payment_subtype_table_name,
			$ps_payment_subtype_key_name,
			$ps_payment_subtype_key_desc_name,
			$ps_payment_subtype_name,
			$sysdate,
			$pp_pos_pta_activation_oset_hr,
			$pp_pos_pta_deactivation_oset_hr,
			$pp_pos_pta_priority,
			$pp_terminal_id,
			$pp_merchant_bank_acct_id,
			$pp_currency_cd,
			$payment_entry_method_cd,
			$payment_entry_method_desc,
			$pp_pos_pta_passthru_allow_yn_flag,
			$pos_pta_pref_auth_amt,
			$pos_pta_pref_auth_amt_max
	);
	
	if($pos_pta_tmpl_entry_id > 0)
	{
		### load settings for pos_pta_tmpl_entry ###
		my $get_pos_pta_settings_stmt = $dbh->prepare(q{
			SELECT pp.pos_pta_device_serial_cd, 
				pp.pos_pta_encrypt_key, 
				pp.pos_pta_encrypt_key2, 
				pp.payment_subtype_key_id, 
				pp.pos_pta_pin_req_yn_flag,
				ps.authority_payment_mask_id,
				pp.authority_payment_mask_id,
				pp.payment_subtype_id,
				ps.payment_subtype_table_name,
				ps.payment_subtype_key_name,
				ps.payment_subtype_key_desc_name,
				ps.payment_subtype_name,
				TO_CHAR(SYSDATE, 'MM/DD/YYYY HH AM'),
				pp.pos_pta_activation_oset_hr,
				pp.pos_pta_deactivation_oset_hr,
				pp.pos_pta_priority,
				pp.terminal_id,
				pp.merchant_bank_acct_id,
				pp.currency_cd,
				cpt.payment_entry_method_cd,
				pem.payment_entry_method_desc,
				pp.pos_pta_passthru_allow_yn_flag,
				pp.pos_pta_pref_auth_amt,
				pp.pos_pta_pref_auth_amt_max
			FROM 
				pss.pos_pta_tmpl_entry pp,
				pss.payment_subtype ps,
				pss.client_payment_type cpt,
				pss.payment_entry_method pem
			WHERE ps.payment_subtype_id = pp.payment_subtype_id
			AND ps.client_payment_type_cd = cpt.client_payment_type_cd
			AND cpt.payment_entry_method_cd = pem.payment_entry_method_cd
			AND pp.pos_pta_tmpl_entry_id = :pos_pta_tmpl_entry_id
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$get_pos_pta_settings_stmt->bind_param(":pos_pta_tmpl_entry_id", $pos_pta_tmpl_entry_id);
		$get_pos_pta_settings_stmt->execute();

		my @pos_pta_data = $get_pos_pta_settings_stmt->fetchrow_array();
		$get_pos_pta_settings_stmt->finish();
		printError("pos_pta_tmpl_entry_id $pos_pta_tmpl_entry_id not found!") unless @pos_pta_data;		
		
		(	$pp_pos_pta_device_serial_cd, 
			$pp_pos_pta_encrypt_key, 
			$pp_pos_pta_encrypt_key2, 
			$pp_payment_subtype_key_id, 
			$pp_pos_pta_pin_req_yn_flag,
			$ps_authority_payment_mask_id,
			$pp_authority_payment_mask_id,
			undef,
			$ps_payment_subtype_table_name,
			$ps_payment_subtype_key_name,
			$ps_payment_subtype_key_desc_name,
			$ps_payment_subtype_name,
			$sysdate,
			$pp_pos_pta_activation_oset_hr,
			$pp_pos_pta_deactivation_oset_hr,
			$pp_pos_pta_priority,
			$pp_terminal_id,
			$pp_merchant_bank_acct_id,
			$pp_currency_cd,
			$payment_entry_method_cd,
			$payment_entry_method_desc,
			$pp_pos_pta_passthru_allow_yn_flag,
			$pos_pta_pref_auth_amt,
			$pos_pta_pref_auth_amt_max
		) = @pos_pta_data;
		
		if ($pp_payment_subtype_id == 0) {
			$pp_payment_subtype_id = $pos_pta_data[7];
		} else {
			my $get_payment_subtype_stmt = $dbh->prepare(q{
				SELECT
					ps.authority_payment_mask_id,
					ps.payment_subtype_table_name,
					ps.payment_subtype_key_name,
					ps.payment_subtype_key_desc_name,
					ps.payment_subtype_name
				FROM 
					pss.payment_subtype ps
				WHERE ps.payment_subtype_id = :payment_subtype_id
			}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
			$get_payment_subtype_stmt->bind_param(":payment_subtype_id", $pp_payment_subtype_id);
			$get_payment_subtype_stmt->execute();

			my @ps_data = $get_payment_subtype_stmt->fetchrow_array();
			$get_payment_subtype_stmt->finish();
			
			(	$ps_authority_payment_mask_id,
				$ps_payment_subtype_table_name,
				$ps_payment_subtype_key_name,
				$ps_payment_subtype_key_desc_name,
				$ps_payment_subtype_name
			) = @ps_data;			
		}
		
		print Dumper(\@pos_pta_data) if DEBUG;
	}
	else
	{
		### load settings for payment_subtype ###
		
		my $get_payment_subtype_settings_stmt = $dbh->prepare(q{
			SELECT 
				ps.authority_payment_mask_id,
				ps.payment_subtype_table_name,
				ps.payment_subtype_key_name,
				ps.payment_subtype_key_desc_name,
				ps.payment_subtype_name,
				TO_CHAR(SYSDATE, 'MM/DD/YYYY HH AM'),
				cpt.payment_entry_method_cd,
				pem.payment_entry_method_desc
			FROM 
				pss.payment_subtype ps,
				pss.client_payment_type cpt,
				pss.payment_entry_method pem
			WHERE ps.payment_subtype_id = :payment_subtype_id
				AND ps.client_payment_type_cd = cpt.client_payment_type_cd
				AND cpt.payment_entry_method_cd = pem.payment_entry_method_cd
		}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
		$get_payment_subtype_settings_stmt->bind_param(":payment_subtype_id", $pp_payment_subtype_id);
		$get_payment_subtype_settings_stmt->execute();

		my @payment_subtype_data = $get_payment_subtype_settings_stmt->fetchrow_array();
		$get_payment_subtype_settings_stmt->finish();
		printError("payment_subtype_id $pp_payment_subtype_id not found!") unless @payment_subtype_data;		
		
		(	$ps_authority_payment_mask_id,
			$ps_payment_subtype_table_name,
			$ps_payment_subtype_key_name,
			$ps_payment_subtype_key_desc_name,
			$ps_payment_subtype_name,
			$sysdate,
			$payment_entry_method_cd,
			$payment_entry_method_desc
		) = @payment_subtype_data;
		print Dumper(\@payment_subtype_data) if DEBUG;
	}
	
	my @currency_list = USAT::DeviceAdmin::Profile::PaymentType::get_currency_list($dbh, \$err);
	printError("<br><br>$err<br><br>") if defined $err;
	print Dumper(\@currency_list)."-->" if DEBUG;
		
	my @payment_subtypes = USAT::DeviceAdmin::Profile::PaymentType::get_available_payment_subtypes_by_payment_entry_method_cd($dbh, $payment_entry_method_cd, \$err);
	printError("<br><br>$err<br><br>") if defined $err;
	print Dumper(\@payment_subtypes)."-->" if DEBUG;
	printError("<br><h3>No payment subtypes found for payment_entry_method_cd = $payment_entry_method_cd.</h3><br><br>") unless scalar @payment_subtypes;
		
	### determine which regex is used (based on presidence) ###
	my ($authority_payment_mask_id, $regex_final, $regex_final_bref, $regex_final_name, $regex_final_desc);
	$authority_payment_mask_id = $pp_authority_payment_mask_id ne '' ? $pp_authority_payment_mask_id : $ps_authority_payment_mask_id;

	my $get_authority_payment_mask_id_stmt = $dbh->prepare(q{
		SELECT authority_payment_mask_regex, 
			authority_payment_mask_bref,
			authority_payment_mask_name,
			authority_payment_mask_desc,
			authority_payment_mask_id
		FROM pss.authority_payment_mask
		WHERE authority_payment_mask_id = :authority_payment_mask_id
	}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
	$get_authority_payment_mask_id_stmt->bind_param(":authority_payment_mask_id", $authority_payment_mask_id);
	$get_authority_payment_mask_id_stmt->execute();

	my @authority_payment_mask_id_data = $get_authority_payment_mask_id_stmt->fetchrow_array();
	$get_authority_payment_mask_id_stmt->finish();
	printError("authority_payment_mask_id $authority_payment_mask_id not found!") unless @authority_payment_mask_id_data;

	$regex_final = $authority_payment_mask_id_data[0];
	$regex_final_bref = $authority_payment_mask_id_data[1];
	$regex_final_name = $authority_payment_mask_id_data[2];
	$regex_final_desc = $authority_payment_mask_id_data[3];
		
	print Dumper($authority_payment_mask_id, $regex_final, $regex_final_bref, $regex_final_name, $regex_final_desc) if DEBUG;
		
	### some text formatting ###
	my $payment_subtype_key_name_formatted = lc $ps_payment_subtype_key_name;
	$payment_subtype_key_name_formatted =~ s/[^a-z]|id$/ /gio;
	$payment_subtype_key_name_formatted =~ s/\b(\w)/\U$1/go;
	my $pos_pta_encrypt_key_hex = $pp_pos_pta_encrypt_key ne '' ? 'CHECKED' : '';
	my $pos_pta_encrypt_key2_hex = $pp_pos_pta_encrypt_key2 ne '' ? 'CHECKED' : '';
	
	my @regex_types;
	my $regex_types_stmt = $dbh->prepare(qq{
		SELECT distinct UPPER(pt.payment_type_desc) || ': ' || ps.payment_subtype_name || ': ' || NVL(authority_payment_mask_desc, authority_payment_mask_name) my_desc,
			authority_payment_mask_regex, 
			authority_payment_mask_bref,
			apm.authority_payment_mask_id			
		FROM pss.authority_payment_mask apm, pss.payment_subtype ps, pss.client_payment_type cpt, pss.payment_type pt
		WHERE (ps.authority_payment_mask_id = apm.authority_payment_mask_id OR ps.payment_subtype_id = apm.payment_subtype_id)
		AND ps.client_payment_type_cd = cpt.client_payment_type_cd
		AND cpt.payment_type_cd = pt.payment_type_cd
		AND cpt.payment_entry_method_cd = :payment_entry_method_cd
		ORDER BY my_desc
	}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
	#$regex_types_stmt->bind_param(":payment_subtype_id", $pp_payment_subtype_id);
	$regex_types_stmt->bind_param(":payment_entry_method_cd", $payment_entry_method_cd);
	$regex_types_stmt->execute();
	push @regex_types, [@_] while @_ = $regex_types_stmt->fetchrow_array();
	$regex_types_stmt->finish();
	print Dumper(\@regex_types) if DEBUG;
		
    ### load backref settings ###
    my %regex_bref_types;
	my @regex_bref_codes;
	my @regex_bref_names;
	my @regex_bref_descs;
	my @regex_bref_types;
	my $regex_bref_types_stmt = $dbh->prepare(qq{
		SELECT authority_payment_mask_bref_id, regex_bref_name, regex_bref_desc
		FROM authority_payment_mask_bref
	}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
	$regex_bref_types_stmt->execute();
	$regex_bref_types{$_[0]} = { name => $_[1], desc => $_[2] } while @_ = $regex_bref_types_stmt->fetchrow_array();
	$regex_bref_types_stmt->finish();
	
	### load other settings ###
	my @payment_subtype_table_key_ids;
	unless ($ps_payment_subtype_table_name eq 'Not Applicable')
	{
		if($ps_payment_subtype_table_name eq 'TERMINAL')
		{
			my $payment_subtype_table_key_ids_stmt = $dbh->prepare(qq{
				select t.terminal_id, m.merchant_name || ': ' || m.merchant_cd || ': ' || t.terminal_desc || ': ' || t.terminal_cd
				from pss.terminal t, pss.merchant m
				where t.merchant_id = m.merchant_id
				order by m.merchant_name || ': ' || m.merchant_cd || ': ' || t.terminal_desc || ': ' || t.terminal_cd
			}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
			$payment_subtype_table_key_ids_stmt->execute();
			push @payment_subtype_table_key_ids, [@_] while @_ = $payment_subtype_table_key_ids_stmt->fetchrow_array();
			$payment_subtype_table_key_ids_stmt->finish();
		}
		else
		{
			my $payment_subtype_table_key_ids_stmt = $dbh->prepare(qq{
				SELECT $ps_payment_subtype_key_name payment_subtype_key_id, $ps_payment_subtype_key_desc_name payment_subtype_key_desc
				FROM $ps_payment_subtype_table_name
				ORDER BY $ps_payment_subtype_key_desc_name
			}) or printError("<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>");
			$payment_subtype_table_key_ids_stmt->execute();
			push @payment_subtype_table_key_ids, [@_] while @_ = $payment_subtype_table_key_ids_stmt->fetchrow_array();
			$payment_subtype_table_key_ids_stmt->finish();
		}
	}
	print Dumper(\@payment_subtype_table_key_ids) if DEBUG;

	### query the database to get current date ###
	my ($curdate, $curtime, $curampm) = split(/ /,$sysdate);
	my ($curmm, $curdd, $curyyyy) = split(/\//, $curdate);

	### generate form select elements ###
	my $default_found;
	my @toggle_regex_methods = (
		map { {
			'value'		=> $_->[1], 
			'default'	=> !$default_found && $_->[3] eq $authority_payment_mask_id ? ($default_found = 'selected') : '', 
			'text'		=> $_->[0], 
			'bref'		=> $_->[2],
			'id'		=> $_->[3]
		} } (
			sort { lc $a->[0] cmp lc $b->[0] } @regex_types
		)
	);
	unshift @toggle_regex_methods, { 'text' => 'Custom method', 'value' => $regex_final, 'bref' => $regex_final_bref, 'default' => 'selected', 'id' => '' } unless $default_found;
	my (@regex_ids, @regex_methods, @regex_bref_methods);
	push @regex_ids, $_->{'id'} for (@toggle_regex_methods);
	push @regex_methods, $_->{'value'} for (@toggle_regex_methods);
	push @regex_bref_methods, $_->{'bref'} for (@toggle_regex_methods);

	### compile settings into javascript-readable arrays ###
	foreach my $bref_code (sort keys %regex_bref_types)
	{
		push @regex_bref_codes, $bref_code;
		push @regex_bref_names, quotemeta $regex_bref_types{$bref_code}->{name};
		push @regex_bref_descs, quotemeta $regex_bref_types{$bref_code}->{desc};
	}
	print Dumper(\@regex_bref_codes, \@regex_bref_names, \@regex_bref_descs)." -->" if DEBUG;
	my $regex_bref_codes_str = "'".join("','", @regex_bref_codes)."',''";
	my $regex_bref_names_str = "'".join("','", @regex_bref_names)."',''";
	my $regex_bref_descs_str = "'".join("','", @regex_bref_descs)."',''";
	my $regex_ids_str = "'".join("','", @regex_ids)."',''";
	my $regex_methods_str = "'".join("','", @regex_methods)."',''";
	my $regex_bref_methods_str = "'".join("','", @regex_bref_methods)."',''";

	### print form ###
	print <<"	EOHTML";
<table width="100%" border="1" cellpadding="2" cellspacing="0">
<form method="post" action="edit_pos_pta_tmpl_entry_func.cgi" onSubmit="javascript:return confirmSubmit();">
<input type="hidden" name="pos_pta_tmpl_id" value="$pos_pta_tmpl_id">
<input type="hidden" name="pos_pta_tmpl_entry_id" value="$pos_pta_tmpl_entry_id">
<input type="hidden" name="payment_entry_method_cd" value="$payment_entry_method_cd">
<input type="hidden" name="original_payment_subtype_id" value="$pp_payment_subtype_id">
<input type="hidden" name="original_payment_subtype_key_id" value="$pp_payment_subtype_key_id">
<input type="hidden" name="original_authority_payment_mask_id" value="$pp_authority_payment_mask_id">
<input type="hidden" name="original_pos_pta_regex" value="$regex_final">
<input type="hidden" name="original_pos_pta_regex_bref" value="$regex_final_bref">
<input type="hidden" name="original_pos_pta_device_serial_cd" value="$pp_pos_pta_device_serial_cd">
<input type="hidden" name="original_pos_pta_encrypt_key" value="$pp_pos_pta_encrypt_key">
<input type="hidden" name="original_pos_pta_encrypt_key2" value="$pp_pos_pta_encrypt_key2">
<input type="hidden" name="original_pos_pta_pin_req_yn_flag" value="$pp_pos_pta_pin_req_yn_flag">
<input type="hidden" name="original_pos_pta_passthru_allow_yn_flag" value="$pp_pos_pta_passthru_allow_yn_flag">
<input type="hidden" name="original_pos_pta_pref_auth_amt" value="$pos_pta_pref_auth_amt">
<input type="hidden" name="original_pos_pta_pref_auth_amt_max" value="$pos_pta_pref_auth_amt_max">
 <tr>
  <th colspan="2" bgcolor="#C0C0C0">Point-of-Sale Payment Type Template Entry</th>
 </tr>
 <tr>
  <td nowrap>Template Entry ID</td>
  <td>
	EOHTML
	print ($pos_pta_tmpl_entry_id == 0 ? "New Template Entry" : $pos_pta_tmpl_entry_id);
	print <<"	EOHTML";
	</td>
 </tr>
 <tr>
  <td nowrap>Template ID</td>
  <td>$pos_pta_tmpl_id</td>
 </tr>
 <tr>
  <td nowrap>Template Name</td>
  <td>$template_data[1]</td>
 </tr>
 <tr>
  <td nowrap>Payment Type</td>
  <td>
	EOHTML
	if (scalar @payment_subtypes > 0)
	{
		print "  <select name=\"payment_subtype_id\" onchange=\"document.forms[0].action = document.location.href; document.forms[0].submit();\">\n";
		print "   <option value=\"$_->[0]\" " . ($_->[0] eq $pp_payment_subtype_id ? ' selected' : '') . ">$_->[1]</option>\n" foreach @payment_subtypes;
		print "  </select>\n";
	}
	else
	{
		print "   <input type=\"hidden\" name=\"payment_subtype_id\" value=\"N/A\">N/A\n";
	}
	print <<"	EOHTML";
  </td>
 </tr>
 <tr>
  <td nowrap>$payment_subtype_key_name_formatted</td>
  <td>
	EOHTML

	if (scalar @payment_subtype_table_key_ids > 0)
	{
		print "  <select name=\"payment_subtype_key_id\">\n";
		print "   <option value=\"$_->[0]\"" . ($_->[0] eq $pp_payment_subtype_key_id ? ' selected' : '') . ">$_->[1]</option>\n" foreach @payment_subtype_table_key_ids;
		print "  </select>\n";
	}
	else
	{
		print "   <input type=\"hidden\" name=\"payment_subtype_key_id\" value=\"N/A\">N/A\n";
	}
	print <<"	EOHTML";
  </td>
 </tr>
 <tr>
  <td nowrap>Payment Entry Method</td>
  <td>$payment_entry_method_desc</td>
 </tr> 
 <tr>
  <td nowrap>Device Serial Number</td>
  <td><input type="text" name="pos_pta_device_serial_cd" value="$pp_pos_pta_device_serial_cd" size="60"></td>
 </tr>	 
 </tr>	 
  <td nowrap>Activation Offset Hours</td>
	EOHTML
	$pp_pos_pta_activation_oset_hr = 0 if($pos_pta_tmpl_entry_id eq 0);
	print <<"	EOHTML";
  <td><input type="text" name="pos_pta_activation_oset_hr" value="$pp_pos_pta_activation_oset_hr" size="60"> (0=Now, Empty=Inactive)</td>
 </tr>
 <tr>
  <td nowrap>Deactivation Offset Hours</td>
  <td><input type="text" name="pos_pta_deactivation_oset_hr" value="$pp_pos_pta_deactivation_oset_hr" size="60"> (0=Now, Empty=Never)</td>
 </tr>
 <tr>
  <td nowrap>
   Authority Encryption Key</td>
  </td>
  <td><input type="text" name="pos_pta_encrypt_key" value="$pp_pos_pta_encrypt_key" size="60"> <input type="checkbox" name="pos_pta_encrypt_key_hex" value="1" $pos_pta_encrypt_key_hex> Hex Encoded</td>
 </tr>	 
 <tr>
  <td nowrap>
   Authority Encryption Key 2</td>
  </td>
  <td><input type="text" name="pos_pta_encrypt_key2" value="$pp_pos_pta_encrypt_key2" size="60"> <input type="checkbox" name="pos_pta_encrypt_key2_hex" value="1" $pos_pta_encrypt_key2_hex> Hex Encoded</td>
 </tr>
 <tr>
  <td nowrap>PIN Required?</td>
  <td>
   <select name="pos_pta_pin_req_yn_flag">
	EOHTML
	print "   <option value=\"$_->[0]\"" . ($_->[0] eq uc $pp_pos_pta_pin_req_yn_flag ? ' selected' : '') . ">$_->[1]</option>\n" foreach (['N', 'No'], ['Y', 'Yes']);
	print <<"	EOHTML";
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>Allow Auth Pass-through<br>(on Decline/Failure)?</td>
  <td>
   <select name="pos_pta_passthru_allow_yn_flag">
	EOHTML
	print "   <option value=\"$_->[0]\"" . ($_->[0] eq uc $pp_pos_pta_passthru_allow_yn_flag ? ' selected' : '') . ">$_->[1]</option>\n" foreach (['N', 'No'], ['Y', 'Yes']);
	print <<"	EOHTML";
   </select>
  </td>
 </tr>
 <tr>
  <td nowrap>Server Auth Override Amount</td>
  <td>
   <input type="text" name="pos_pta_pref_auth_amt" id="override_amount_id" value="$pos_pta_pref_auth_amt" size="10"> 
  </td>
 </tr>
 <tr>
  <td nowrap>Server Auth Override Limit</td>
  <td>
   <input type="text" name="pos_pta_pref_auth_amt_max" id="override_limit_id" value="$pos_pta_pref_auth_amt_max" size="10">
  </td>
 </tr>
 <tr>
  <td nowrap>Currency</td>
  <td>
	EOHTML
	$pp_currency_cd = 'USD' unless defined $pp_currency_cd;
	print "  <select name=\"currency_cd\">\n";
	print "   <option value=\"$_->[0]\"" . ($_->[0] eq $pp_currency_cd ? ' selected' : '') . ">$_->[0] - $_->[1]</option>\n" foreach @currency_list;
	print "  </select>\n";
	print <<"	EOHTML";
  </td>
 </tr>
 <tr>
  <td nowrap>Priority in Category</td>
	EOHTML
	$pp_pos_pta_priority = 1 unless defined $pp_pos_pta_priority;
	print <<"	EOHTML";
  <td><input type="text" name="pos_pta_priority" value="$pp_pos_pta_priority" size="3"></td>
 </tr>	 
 <tr>
  <td valign="top">
   <script language="JavaScript" type="text/javascript">
<!--
function testRegex(myform) {
	var regexBrefArr = new Array($regex_bref_methods_str);
	var regexBrefCodes = new Array($regex_bref_codes_str);
	var regexBrefNames = new Array($regex_bref_names_str);
	var regexBrefDescs = new Array($regex_bref_descs_str);
	var regexBrefTypes = new Array();
	for (var i = 0; i < regexBrefCodes.length; i++) {
		regexBrefTypes[regexBrefCodes[i]] = regexBrefNames[i];
	}
	
	var myRegex = new RegExp(myform.pos_pta_regex[myform.pos_pta_regex.selectedIndex].value, 'i');
	var myResArr = myRegex.exec(myform.test_regex_card_num.value);
	var myRegexBref = regexBrefArr[myform.pos_pta_regex.selectedIndex];
	if (myResArr == null) {
		alert("Invalid Match.\\n\\nPlease check that you have selected the correct method\\nand have entered a correct sample string.");
	}
	else {
		var brefResStr = "";
		if (myRegexBref != '') {
			brefResStr = "\\n\\nMatched substring results are as follows:\\n";
			var myBrefCodeArr = myRegexBref.split('|');
			for (var i = 0; i < myBrefCodeArr.length; i++) {
				var myBrefPartsArr = myBrefCodeArr[i].split(':');	//0=num, 1=db id
				//alert("myBrefPartsArr="+myBrefPartsArr+"\\nmyResArr="+myResArr+"\\nregexBrefTypes="+regexBrefTypes);
				brefResStr = brefResStr + " - " + regexBrefTypes[myBrefPartsArr[1]] + ": " + myResArr[myBrefPartsArr[0]] + "\\n";
			}
		}
		alert('Successful Match!' + brefResStr);
	}
}

function regexSelectedMethod(myForm) {
	var regexIdArr = new Array($regex_ids_str);
	var regexArr = new Array($regex_methods_str);
	var regexBrefArr = new Array($regex_bref_methods_str);
	myForm.selected_regex.value = regexArr[myForm.pos_pta_regex.selectedIndex];
	myForm.selected_regex_bref.value = regexBrefArr[myForm.pos_pta_regex.selectedIndex];
	myForm.authority_payment_mask_id.value = regexIdArr[myForm.pos_pta_regex.selectedIndex];
	myForm.pos_pta_regex_bref.value = myForm.selected_regex_bref.value;
}
// -->
   </script>
   Card Magstripe Validation<br>and Decoding Method
  </td>
  <td>

   <table border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td nowrap>Method:&nbsp;</td>
     <td>
      <select name="pos_pta_regex" onChange="javascript:regexSelectedMethod(this.form);">
	EOHTML
	print " <option value=\"$_->{value}\" $_->{default}>$_->{text}</option>\n" for (@toggle_regex_methods);
	print <<"	EOHTML";
		<option value="">Use default for selected payment type</option>
      </select>
      <input type="hidden" name="authority_payment_mask_id" value="$authority_payment_mask_id">
      <input type="hidden" name="pos_pta_regex_bref" value="$regex_final_bref">
     </td>
    </tr>
    <tr>
     <td nowrap>Selected Algo.:&nbsp;</td>
     <td><input type="text" name="selected_regex" value="$regex_final" size="80" disabled></td>
    </tr>
    <tr>
     <td nowrap>Algo. Bref. Code:&nbsp;</td>
     <td><input type="text" name="selected_regex_bref" value="$regex_final_bref" size="80" disabled></td>
    </tr>
    <tr>
     <td nowrap>Sample Magstripe:&nbsp;</td>
     <td>
      <input type="text" name="test_regex_card_num" value="" size="80">
      <input type="button" name="test_regex" value="Test" onClick="javascript:testRegex(this.form);">
     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td colspan="4" align="center">
   <input type="button" value="Cancel" onClick="javascript:window.location='/edit_pos_pta_tmpl.cgi?pos_pta_tmpl_id=$pos_pta_tmpl_id';">
   <input type="submit" name="userAction" value="Save" > 
	EOHTML
	print "   <input type=\"submit\" name=\"userAction\" value=\"Delete\">" if($pos_pta_tmpl_entry_id > 0);
	print <<"	EOHTML";
  </td>
 </tr>
</form>
</table>
	EOHTML

	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
}

sub printError ($$) {
	my $err_txt = shift;
	print "$err_txt\n";
	$dbh->disconnect;
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}
