#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::Database;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Common::Const;

my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);
my $dbh = $DATABASE->{handle};

my $query = new OOCGI::OOCGI;

my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my $action = $query->param("action"); 

if($action eq "Upload to Server")
{
	my $ev_number = $query->param("ev_number"); 
	my $server_file_name = $query->param("file_name"); 
	my $file_type = $query->param("file_type"); 
	my $comment = $query->param("file_comment");
	my $upload_filehandle = $query->upload("file_data");
	
	if(length($server_file_name) == 0)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "Missing required parameter: file_name\n";
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}

	if(length($file_type) == 0)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "Missing required parameter: file_type\n";
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}

    if($ev_number =~ /^TD/) {
       my $sql = 'SELECT count(1) FROM file_transfer WHERE file_transfer_name = ?';
       Carp::carp "SERVER FILE NAME $server_file_name \n";
       if(OOCGI::Query->new($sql, { bind => [ $server_file_name ] } )->node(0,0) >= 1) {
		  USAT::DeviceAdmin::UI::DAHeader->printHeader();
		  USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
          print $user_menu;
          BR(2);
          B("File Create Error for $server_file_name File exists!",'red',2);
          BR(2);
		  $dbh->disconnect;
		  USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		  exit();
	   }
    }
	
	if(length($upload_filehandle) == 0)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "Missing required parameter: upload_filehandle\n";
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}
	
	my $file_data;
	while ( <$upload_filehandle> ) 
	{
		$file_data = $file_data . $_;
	} 
	
	if(length($file_data) == 0)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "File upload failed!  No data.\n";
		$dbh->disconnect;
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}
	
	my $get_file_id_stmt = $dbh->prepare("select device.SEQ_FILE_TRANSFER_ID.nextval from dual") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_file_id_stmt->execute();
	my @file_id_data = $get_file_id_stmt->fetchrow_array();
	
	if(!@file_id_data)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "Failed to get a new file_transfer_id!";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		$dbh->disconnect;
		exit;
	}
	
	$get_file_id_stmt->finish();
	my $file_id = $file_id_data[0];
	
	my $LONG_RAW_TYPE=24;
	my %attrib;
	$attrib{'ORA_TYPE'} = $LONG_RAW_TYPE;
	
	my $insert_file_stmt = $dbh->prepare("insert into file_transfer(file_transfer_id, file_transfer_name, file_transfer_type_cd, file_transfer_content, file_transfer_comment) values (:file_id, trim(:file_name), :file_type, :file_content, :file_comment)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$insert_file_stmt->bind_param(":file_id", $file_id);
	$insert_file_stmt->bind_param(":file_name", $server_file_name);
	$insert_file_stmt->bind_param(":file_type", $file_type);
	$insert_file_stmt->bind_param(":file_content", unpack("H*", $file_data), \%attrib); 
	$insert_file_stmt->bind_param(":file_comment", $comment);
	$insert_file_stmt->execute();
	$insert_file_stmt->finish();

	if(defined $dbh->errstr)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "sql insert error: " . $dbh->errstr . "\n";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		$dbh->disconnect;
		exit;		
	}
	
	$dbh->disconnect;
	
	if(length($ev_number) > 0)
	{
		print CGI->redirect("/file_details_func.cgi?file_id=$file_id&ev_number=$ev_number&action=Upload%20to%20a%20Device");
	}
	else
	{
		print CGI->redirect("/file_details.cgi?file_id=$file_id");
	}
	
	exit();	
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;
print "No Action Defined!";
$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
