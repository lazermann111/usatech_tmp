#!/usr/local/USAT/bin/perl

use strict;

use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::DAHeader;
use USAT::Common::Const;
use USAT::App::DeviceAdmin::Const qw(:globals );
use USAT::DeviceAdmin::Util;

use USAT::Database;
my $DATABASE = USAT::Database->new(PrintError => 1, RaiseError => 1, AutoCommit => 1);

#kludge: patch for Oracle 10.1.0.x session default date format bug, causing occasional ORA-01801 errors on some views and PL/SQL
$DATABASE->do(query=>q{ALTER SESSION SET NLS_DATE_FORMAT='dd-mon-yy'}) if $DATABASE->{driver} =~ /Oracle$/;

my $dbh = $DATABASE->{handle};

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my %PARAM = $query->Vars;

my $ev_number = $PARAM{ev_number};

if(!$ev_number) {
   if($PARAM{serial_number}) {
      my $serial_number = $PARAM{serial_number};
      #note: sql must be on one line to mitigate potential: Oracle OCI8 + view issues
      my $sql = q|SELECT device_name FROM device.vw_device_last_active WHERE device_serial_cd = ?|;
      $serial_number =~ s/ //g;
      my $upper_serial_number = uc $serial_number;
      my $qo = OOCGI::Query->new($sql, { bind => $upper_serial_number } );
      if ($qo->row_count > 0) {
          $ev_number = $qo->node(0,0);
      } else {
          USAT::DeviceAdmin::UI::DAHeader->printHeader();
          USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
		  print $user_menu;
          print "Can not get EV Number from Serial Number: ";
          USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
          exit;
      }
   }
}

my $file_id   = $PARAM{file_id};
if(length($file_id) == 0 || $file_id eq 'file id' || $file_id =~ /[^0-9]/)
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	print "Required Parameter Not Found: file_id";
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $file_info_stmt = $dbh->prepare("select file_transfer_name, file_transfer_type_cd, to_char(created_ts, 'MM/DD/YYYY HH:MI:SS AM'), to_char(last_updated_ts, 'MM/DD/YYYY HH:MI:SS AM'), file_transfer_comment from file_transfer where file_transfer_id = :file_id order by file_transfer_type_cd") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
$file_info_stmt->bind_param(":file_id", $file_id);
#$file_info_stmt->bind_param(":file_transfer_type_cd", PKG_FILE_TYPE__EDGE_DEFAULT_CONFIGURATION_TEMPLATE );
$file_info_stmt->execute();

my @file_data = $file_info_stmt->fetchrow_array();
if(!@file_data)
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	BR(2);
    B("File not exist!",'red',2);
    BR(3);
    print '<input type="button" value="< Back" onClick="javascript:history.go(-1);">';
    BR(3);
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	exit;
}

my $file_name = $file_data[0];
my $file_type = $file_data[1];
my $create_date = $file_data[2];
my $last_updated = $file_data[3];
my $comment = $file_data[4];

$file_info_stmt->finish();

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# profile.cgi uses myaction instead of action
# following lines added to handle request coming
# from profile.cgi
my $action = $PARAM{myaction};
if(!defined $action) {
   $action = $PARAM{action};
}
if($action eq 'Download File')
{
	my $file_content = USAT::DeviceAdmin::Util::blob_select($dbh, $file_id);
	if(not defined $file_content)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "File not found: file_id = $file_id";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	}
	else
	{
		if($file_name =~ /\//)
		{
			$file_name = substr($file_name, (rindex($file_name, '/')+1));
		}
		
		print "Content-Type: application/force-download\n";
		print "Content-Disposition: attachment; filename=$file_name\n\n";
		if ($file_type eq PKG_FILE_TYPE__MEMORY_MAP_CSV) {
			print $file_content;
		} else {
	   		print pack("H*", $file_content);
		}
	}
	
	$dbh->disconnect;
	exit();
}
elsif($action eq 'Edit File')
{
    if( $file_type ne PKG_FILE_TYPE__EDGE_DEFAULT_CONFIGURATION_TEMPLATE ) {
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	
	my $file_content = USAT::DeviceAdmin::Util::blob_select($dbh, $file_id);
	my $line_break_format = "UNIX";
	$line_break_format = "WINDOWS" if(pack("H*",$file_content) =~ /\r\n/);

    if($file_type ne PKG_FILE_TYPE__MEMORY_MAP_CSV) { 
       $file_content = pack("H*", $file_content);
    }	

print <<EDITFILE;
	<table cellspacing="0" cellpadding="5" border="1" width="100%">
	 <form method="post" action="file_details_func.cgi" enctype="multipart/form-data">
	 <tr>
	  <th bgcolor="#C0C0C0" colspan="1">Edit File - $file_name</th>
	 </tr>
	 <tr>
	  <td align="center">
	   Comment: <input type="text" name="file_comment" value="$comment" size="60">
	   Line Breaks: 
	   <select name="line_break_format">
	    <option value="UNIX" " . ($line_break_format eq "UNIX" ? 'selected' : '') . ">Unix Line Breaks</option>  
	    <option value="WINDOWS" " . ($line_break_format eq "WINDOWS" ? 'selected' : '') . ">Windows Line Breaks</option>
	   </select>
	  </td>
	 </tr>
	 <tr>
	  <td align="center">
	  	Edit File Content:<br><textarea name="file_content" rows="20" cols="90" wrap="off">$file_content</textarea><br>
	  	or Upload File to Server: <input type="file" name="file_data" size="80">
	  </td>
	 </tr>
	 <tr>
	  <td align="center">
	   <input type="hidden" name="file_id" value="$file_id">
	   <input type="submit" name="action" value="Save Changes">
	   <input type="button" value="Cancel" onClick="javascript:history.go(-1);">
	  </td>
	 </tr>
	 </form>
	</table>
EDITFILE
	
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	$dbh->disconnect;
	exit();
    } else {
       USAT::DeviceAdmin::UI::DAHeader->printHeader();
       USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
       print $user_menu;
       BR(2);
       B("File $file_name can not be edited!",'red',2);
       BR(3);
       print '<input type="button" value="< Back" onClick="javascript:history.go(-1);">';
       BR(3);
       USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
       exit;
    }
}
elsif($action eq 'Save Changes')
{
    if( $file_type ne PKG_FILE_TYPE__EDGE_DEFAULT_CONFIGURATION_TEMPLATE ) {
	my $file_content;
	my $file_comment = $PARAM{file_comment};
	my $line_break_format = $PARAM{line_break_format};
	my $upload_filehandle = $query->upload("file_data");
	
	if (defined $upload_filehandle)
	{
		while ( <$upload_filehandle> ) 
		{
			$file_content = $file_content . $_;
		} 
	}
	else
	{
		 $file_content = $PARAM{file_content};

		if($line_break_format eq 'UNIX')
		{
			$file_content =~ s/(\r\n|\n|\r)/\n/g;
		}
		elsif($line_break_format eq 'WINDOWS')
		{
			$file_content =~ s/(\r\n|\n|\r)/\r\n/g;
		}
	}
	
	my $update_file_stmt = $dbh->prepare("update file_transfer set file_transfer_content = :file_content, file_transfer_comment = :file_comment where file_transfer_id = :file_id") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
    if($file_type ne PKG_FILE_TYPE__MEMORY_MAP_CSV) { 
       $file_content = unpack("H*", $file_content);
    }
	$update_file_stmt->bind_param(":file_content", $file_content); 
	$update_file_stmt->bind_param(":file_comment", $file_comment);
	$update_file_stmt->bind_param(":file_id", $file_id);
	$update_file_stmt->execute();
	$update_file_stmt->finish();
	
	if(defined $dbh->errstr)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "sql error: " . $dbh->errstr . "\n";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	}
	else
	{
		print CGI->redirect("/file_details.cgi?file_id=$file_id");
	}
	
	$dbh->disconnect;
	exit();
    } else {
       USAT::DeviceAdmin::UI::DAHeader->printHeader();
       USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
       print $user_menu;
       BR(2);
       B("File $file_name can not be edited!",'red',2);
       BR(3);
       print '<input type="button" value="< Back" onClick="javascript:history.go(-1);">';
       BR(3);
       USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
       exit;
    }
}
elsif($action eq 'Upload to a Device')
{
	USAT::DeviceAdmin::UI::DAHeader->printHeader();
	USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
    print $user_menu;
	
	print qq{

	<table cellspacing="0" cellpadding="5" border="1" width="100%">
	 <form method="post" action="file_details_func.cgi">
	 <tr>
	  <th bgcolor="#C0C0C0" colspan="2">Upload File to Device</th>
	 </tr>
	 <tr>
	  <td>File ID</td><td>$file_id</td>
	 </tr>
	 <tr>
	  <td>File Name</td><td>$file_name</td>
	 </tr>
	 <tr>
	  <td>Comment</td><td>$comment &nbsp;</td>
	 </tr>
	 <tr>};
	 
if(length($ev_number) > 0)
{
	print qq{	  <td>Device EV Number</td><td><input type="hidden" name="ev_number" value="$ev_number">$ev_number</td>};
}
else
{
	print qq{	  <td>Device EV Number</td><td><input type="text" name="ev_number" value="$ev_number"></td>};
}

print qq{
	 </tr>
	 <tr>
	  <td>Execute Order</td><td><input type="text" name="execute_order" value="1"></td>
	 </tr>
	 <tr>
	  <td>Packet Size</td><td><input type="text" name="packet_size" value="1024"></td>
	 </tr>
	 <tr>
	  <td align="center" colspan="2">
	   <input type="hidden" name="file_id" value="$file_id">
	   <input type="submit" name="action" value="Schedule Device Upload">
	   <input type="button" value="Cancel" onClick="javascript:history.go(-1);">
	  </td>
	 </tr>
	 </form>
	</table>
	};
	
	USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	$dbh->disconnect;
	exit();
}
elsif($action eq 'Schedule Device Upload')
{
#my $ev_number     = $PARAM{ev_number};
	my $packet_size   = $PARAM{packet_size};
	my $file_id       = $PARAM{file_id};
	my $execute_order = $PARAM{execute_order};
	
	# first validate the inputs
    if(length($ev_number) != 8 || !(substr($ev_number, 0, 2) ne 'EV' || substr($ev_number, 0, 2) ne 'TD'))
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "Invalid EV Number\n";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}
	
	if(length($packet_size) == 0)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "Invalid Packet Size\n";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}
	
	if(length($execute_order) == 0)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "Invalid Execute Order\n";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit();
	}
		
	# now check the ev number and get the active device id and device type
	my $get_device_id_stmt = $dbh->prepare("select device_id, device_type_id from device where device_active_yn_flag = 'Y' and device_name = :ev_number") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_device_id_stmt->bind_param(":ev_number", $ev_number);
	$get_device_id_stmt->execute();
	
	my @device_data = $get_device_id_stmt->fetchrow_array();
	if(!@device_data)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "No device found for $ev_number";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}

	$get_device_id_stmt->finish();
	my $device_id = $device_data[0];
	my $device_type = $device_data[1];
	my $cmd_file_transfer_start;

 	
	if ($device_type eq PKG_DEVICE_TYPE__EDGE) # Edge Long File
    {
		$cmd_file_transfer_start = EPORT_CMD__FILE_TRANSFER_START_V4_1;
	}
    elsif ($device_type =~ m/^(1|4|11|12)$/) # Gx, Sony, Kiosk, T2
	{
		$cmd_file_transfer_start = 'A4';
		
		#removing any requests for this file name
		$DATABASE->update(	table			=> 'engine.machine_cmd_pending',
			update_columns	=> 'execute_cd',
			update_values	=> ['A'],
			where_columns	=> ['machine_id = ?', 'data_type = ?', 'upper(command) = upper(?)'],
			where_values	=> [$ev_number, '9B', unpack("H*", $file_name)] );
	}
	else
	{
		$cmd_file_transfer_start = '7C';
	}
	
	# check if a pending file transfer start command already exists for this device and file
	my $check_cmd_stmt = $dbh->prepare("select count(1) from device.device_file_transfer dft, engine.machine_cmd_pending mcp where dft.device_id = :device_id and dft.file_transfer_id = :file_id and dft.device_file_transfer_status_cd = 0 and dft.device_file_transfer_direct = 'O' and mcp.machine_id = :ev_number and mcp.data_type = :data_type and dft.device_file_transfer_id = mcp.command and mcp.execute_cd = 'P'") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$check_cmd_stmt->bind_param(":device_id", $device_id);
	$check_cmd_stmt->bind_param(":file_id", $file_id);
	$check_cmd_stmt->bind_param(":ev_number", $ev_number);
	$check_cmd_stmt->bind_param(":data_type", $cmd_file_transfer_start);
	$check_cmd_stmt->execute();

	my @check_cmd_data = $check_cmd_stmt->fetchrow_array();
	my $check_cmd_count = $check_cmd_data[0];
	$check_cmd_stmt->finish();
	
	if($check_cmd_count > 0)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
        BR(2);
		B("A pending File Transfer Start command already exists for device $ev_number and file_id $file_id",'red',2);
        BR(2);
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		exit;
	}
	
	# now get the next device_file_transfer_id from the sequence
	my $get_transfer_id_stmt = $dbh->prepare("select SEQ_DEVICE_FILE_TRANSFER_ID.nextval from dual") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$get_transfer_id_stmt->execute();
	my @transfer_id_data = $get_transfer_id_stmt->fetchrow_array();
	
	if(!@transfer_id_data)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "Failed to get a new device_file_transfer_id!";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
		$dbh->disconnect;
		exit;
	}
	
	$get_transfer_id_stmt->finish();
	my $transfer_id = $transfer_id_data[0];
	
	
	# now insert the device_file_transfer
	my $insert_transfer_stmt = $dbh->prepare("insert into device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size) values (:transfer_id, :device_id, :file_id, :direction, :status, :packet_size)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$insert_transfer_stmt->bind_param(":transfer_id", $transfer_id); 
	$insert_transfer_stmt->bind_param(":device_id", $device_id);
	$insert_transfer_stmt->bind_param(":file_id", $file_id);
	$insert_transfer_stmt->bind_param(":direction", 'O');
	$insert_transfer_stmt->bind_param(":status", '0');
	$insert_transfer_stmt->bind_param(":packet_size", $packet_size);
	$insert_transfer_stmt->execute();
	
	if(defined $dbh->errstr)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "sql error: " . $dbh->errstr . "\n";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
        $dbh->disconnect;
		exit();
	}
	$insert_transfer_stmt->finish();

    # now write information into device_setting table
	if ($device_type eq PKG_DEVICE_TYPE__EDGE && $file_type eq PKG_FILE_TYPE__CONFIGURATION_FILE) # Edge Long File
    {
       my $operation_status = "FAILURE for device_id = $device_id and file_id = $file_id<br>";
       my $message_str      =  "\n<b>Updating device settings...</b>\n";
       my @config_lines;
       my $new_config_file;
       if($file_id) {
          my $result   = USAT::DeviceAdmin::DBObj::File_transfer::edge_template_merge(
                         $DATABASE, $device_id, $ev_number.'-CFG',$file_id);
          if($result->[0] eq 'FAILURE') {
		     USAT::DeviceAdmin::UI::DAHeader->printHeader();
		     USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
             print $user_menu;
             $operation_status.= " STATUS 2 \n";
             print $operation_status;
             Carp::carp $operation_status;
		     USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	         $dbh->disconnect;
		     exit();
          }
       } else {
		  USAT::DeviceAdmin::UI::DAHeader->printHeader();
		  USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
          print $user_menu;
          $operation_status.= " STATUS 3 \n";
          print $operation_status;
          Carp::carp $operation_status;
		  USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	      $dbh->disconnect;
		  exit();
       }
    }
	
	# now insert a pending message for this new transfer
	my $insert_pending_msg_stmt = $dbh->prepare("insert into engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order) values (:ev_number, :data_type, :transfer_id, :execute_cd, :execute_order)") or print "<br><br>Couldn't prepare statement: " . $dbh->errstr . "<br><br>";
	$insert_pending_msg_stmt->bind_param(":ev_number", $ev_number); 
	$insert_pending_msg_stmt->bind_param(":data_type", $cmd_file_transfer_start); 
	$insert_pending_msg_stmt->bind_param(":transfer_id", $transfer_id); 
	$insert_pending_msg_stmt->bind_param(":execute_cd", 'P'); 
	$insert_pending_msg_stmt->bind_param(":execute_order", '1'); 
	$insert_pending_msg_stmt->execute();
	
	if(defined $dbh->errstr)
	{
		USAT::DeviceAdmin::UI::DAHeader->printHeader();
		USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
        print $user_menu;
		print "sql error: " . $dbh->errstr . "\n";
		USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
	    $dbh->disconnect;
		exit();
	}
	$insert_pending_msg_stmt->finish();
	
	
	$dbh->disconnect;
	print CGI->redirect("/profile.cgi?device_id=$device_id");
	exit();
}

USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;
print "No Action Defined!";
$dbh->disconnect;
USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
