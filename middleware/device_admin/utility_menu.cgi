#!/usr/local/USAT/bin/perl

use strict;
use OOCGI::OOCGI;
use USAT::DeviceAdmin::UI::USAPopups;
use Date::Calc qw(Today Add_Delta_Days);
use USAT::DeviceAdmin::UI::DAHeader;

my $query = OOCGI::OOCGI->new;
my $session = USAT::DeviceAdmin::DBObj::Da_session->authenticate;
my $user_menu = $session->print_menu;
$session->destroy;

my @date_pair;
for (my $i = 1; $i <= 30; $i++) {
	my @currdate = Add_Delta_Days(Today(), -$i);
        my $key = qq{$currdate[1]/$currdate[2]/$currdate[0]};
        push(@date_pair,$key);
        push(@date_pair,$key);
} 

my $popExptDate  = OOCGI::Popup->new( name => 'expt_date',  pair => \@date_pair );
my $popAuthDate  = OOCGI::Popup->new( name => 'auth_date',  pair => \@date_pair );
my $popBatchDate = OOCGI::Popup->new( name => 'batch_date', pair => \@date_pair );

my $sql;
$sql = USAT::DeviceAdmin::UI::USAPopups->sql_device_type;
my $popDeviceTypeID = OOCGI::Popup->new( name => 'device_type', sql => $sql);

$sql = USAT::DeviceAdmin::UI::USAPopups->sql_device_type_desc;
my $popDeviceTypeDesc = OOCGI::Popup->new( name => 'device_type', sql => $sql);

my $popEnabled = USAT::DeviceAdmin::UI::USAPopups->pop_enabled;

my $popFile = OOCGI::Popup->new( name => 'file', pair => USAT::DeviceAdmin::UI::USAPopups->arr_file);

my $popTimeUnits = OOCGI::Popup->new( name => 'time_units',
    pair => USAT::DeviceAdmin::UI::USAPopups->arr_time_units);
$popTimeUnits->default('1440');

my $popLastEvent = OOCGI::Popup->new( name => 'last_event',
    pair => USAT::DeviceAdmin::UI::USAPopups->arr_last_event);

$popLastEvent->default('2');
 
USAT::DeviceAdmin::UI::DAHeader->printHeader();
USAT::DeviceAdmin::UI::DAHeader->printFile("header.html");
print $user_menu;

print qq|
<table cellspacing="0" cellpadding="2" border=1 width="100%">

 <tr>
  <th colspan="4" bgcolor="#C0C0C0">Reports</th>
 </tr>
 <tr>
  <form method="get" action="device_call_in_aging.cgi">
  <td>Call In Aging Report: </td>
  <td colspan="2">
   </select>
   $popDeviceTypeID 
   $popEnabled
   $popLastEvent<br>
   Activity Days: <input type="text" name="active_days" value="10" size="3" maxlength="3"> &nbsp;
   Tolerance Days: <input type="text" name="toler_days" value="2" size="3" maxlength="3">

  </td>
  <td><input type="submit" value="View"></td>
  </form>
 </tr>
 <tr>
  <form method="get" action="active_devices.cgi">
  <td>Active Devices: </td>
  <td colspan="2">
  Show the top <input type="text" name="device_limit" value="10" size="2"> most active devices in the last <input type="text" name="time_value" value="15" size="2"> 
  $popTimeUnits
  </td>
  <td><input type="submit" value="View"></td>
  </form>
 </tr>
 <tr>
  <form method="get" action="device_rssi_log.cgi">
  <td>GPRS Device RSSI Log: </td>
  <td colspan="2">
   Tolerance RSSI: <input type="text" name="toler_rssi" value="10" size="2" maxlength="2"> &nbsp;
   Tolerance BER: <input type="text" name="toler_ber" value="0" size="2" maxlength="2"> &nbsp;
   $popEnabled
  </td>
  <td><input type="submit" value="View"></td>
  </form>
 </tr>
 <tr>
  <form method="get" action="session_expt.cgi">
  <td>Session Exception Report:</td>
  <td colspan="2">
  $popDeviceTypeDesc<br>Date: 
  $popExptDate &nbsp;&nbsp;(<i>report may take 20+ seconds to load</i>)
  </td>
  <td><input type="submit" name="action" value="View"></td>
  </form>
 </tr>
 <tr>
  <form method="get" action="batch_success.cgi">
  <td>Batch Success Report:</td>
  <td colspan="2">
  $popDeviceTypeID<br>Date: 
  $popBatchDate &nbsp;&nbsp;(<i>report may take 20+ seconds to load</i>)
  </td>
  <td><input type="submit" name="action" value="View"></td>
  </form>
 </tr>
 <tr>
  <form method="get" action="auth_rt_usat.cgi">
  <td>Auth Round Trip Report:</td>
  <td colspan="2">
  $popDeviceTypeID<br>Date: 
  $popAuthDate &nbsp;&nbsp;(<i>USAT Round Trip Time</i>)
  </td>
  <td><input type="submit" name="action" value="View"></td>
  </form>
 </tr>
 <tr>
  <form method="get" action="auth_rt_remote.cgi">
  <td>Auth Round Trip Report:</td>
  <td colspan="2">Date:
  $popAuthDate &nbsp;&nbsp;(<i>GPRS Remote Device Round Trip Time</i>)
  </td>
  <td><input type="submit" name="action" value="View"></td>
  </form>
 </tr>
</table>
|;

USAT::DeviceAdmin::UI::DAHeader->printFooter("footer.html");
