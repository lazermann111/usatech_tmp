package com.usatech.iso8583.gateway;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ThreadPoolExecutor;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;

import com.usatech.iso8583.*;
import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.transaction.*;
import com.usatech.iso8583.util.ISO8583Util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.logging.*;
import org.picocontainer.PicoContainer;
import org.picocontainer.defaults.PicoInvocationTargetInitializationException;

public class ISO8583Gateway extends HttpServlet implements Runnable
{
	private static final String version = "1.3.19.10";
	private static final String releaseDate = "12/16/2009";
	
	private static Log log = LogFactory.getLog(ISO8583Gateway.class);
	private static Log transactionLog = LogFactory.getLog("TRANSACTION_LOG");

	private static PicoContainer container;

	private static Map<String, ISO8583Interchange> interchanges = new HashMap<String, ISO8583Interchange>();
	private static ISO8583TransactionManager txnManager;
	private static ThreadPoolExecutor threadPool;

	private List<ISO8583Transaction> transactionsToRecover = new ArrayList<ISO8583Transaction>();
	private boolean stopFlag = false;

	public boolean getStopFlag()
	{
		return stopFlag;
	}

	public PicoContainer getContainer()
	{
		return container;
	}

	public Map<String, ISO8583Interchange> getInterchanges()
	{
		return interchanges;
	}

	public ISO8583TransactionManager getTransactionManager()
	{
		return txnManager;
	}

	public ThreadPoolExecutor getThreadPool()
	{
		return threadPool;
	}

	public List<ISO8583Transaction> getTransactionToRecover()
	{
		return transactionsToRecover;
	}

	public String getVersion()
	{
		return version;
	}

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

		log.info("ISO8583 Gateway v" + version + " (" + releaseDate + ") Copyright (c) 2005-2009 USA Technologies.  All rights reserved.");

		String componentsFile = config.getServletContext().getRealPath("WEB-INF") + config.getInitParameter("ISO8583GatewayComponents");
		if (componentsFile == null)
		{
			log.fatal("ISO8583Gateway failed to start! Missing \"ISO8583GatewayComponents\" servlet initialization parameter!");
			return;
		}

		log.info("Loading and starting components from " + componentsFile);

		try
		{
			try
			{
				container = ISO8583Util.buildContainerFromFile(null, "ISO8583Gateway", componentsFile);
			}
			catch (InvocationTargetException ite)
			{
				throw ite.getCause();
			}
			catch (PicoInvocationTargetInitializationException pitie)
			{
				throw pitie.getCause();
			}
		}
		catch (NoSuchElementException e)
		{
			log.fatal("ISO8583Gateway failed to start! A required configuration property is missing: " + e.getMessage(), e);
			return;
		}
		catch (ConfigurationException e)
		{
			log.fatal("ISO8583Gateway failed to start! A configuration parameter is incorrect: " + e.getMessage(), e);
			return;
		}
		catch (Throwable e)
		{
			log.fatal("ISO8583Gateway failed to start! Caught unexpected error, stacktrace follows: " + e.getMessage(), e);
			return;
		}

		//add gateway to ServletContext
		config.getServletContext().setAttribute("ISO8583Gateway", this);

		List<ISO8583Interchange> interchangeList = container.getComponentInstancesOfType(ISO8583Interchange.class);
		for (ISO8583Interchange interchange : interchangeList)
		{
			log.info("Registering interchange name " + interchange.getName() + " for " + interchange.getClass().getName());
			interchanges.put(interchange.getName(), interchange);
		}

		if (interchangeList.isEmpty())
			log.fatal("No ISO8583 Interchanges are defined in " + componentsFile);

		txnManager = (ISO8583TransactionManager) container.getComponentInstanceOfType(ISO8583TransactionManager.class);
		threadPool = (ThreadPoolExecutor) container.getComponentInstanceOfType(ThreadPoolExecutor.class);

		// load any transactions that were left in an inconsistent state on startup since the last shutdown
		for (String interchangeName : interchanges.keySet())
		{
			ISO8583Interchange interchange = interchanges.get(interchangeName);
			List<ISO8583Transaction> pendingList = txnManager.getPending(interchangeName);
			log.info("There are " + pendingList.size() + " pending transactions for interchange " + interchange.getName());
			for (ISO8583Transaction pending : pendingList)
			{
				pending.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				transactionsToRecover.add(pending);
			}
		}

		threadPool.execute(this);
	}

	public void destroy()
	{
		log.info("ISO8583Gateway is shutting down...");

		stopFlag = true;

		// put these transactions into a recovery state before shutdown
		for (String interchangeName : interchanges.keySet())
		{
			ISO8583Interchange interchange = interchanges.get(interchangeName);
			List<ISO8583Transaction> pendingList = txnManager.getPending(interchangeName);
			log.info("There are " + pendingList.size() + " pending transactions for interchange " + interchange.getName());
			for (ISO8583Transaction pending : pendingList)
			{
				pending.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			}
		}
		
		container.stop();
		log.info("ISO8583Gateway shut down.");
	}

	public ISO8583Response process(ISO8583Request request, String interchangeName)
	{
		try
		{
			if (request == null)
			{
				log.warn("Request denied: Null request object!");
				return new ISO8583Response(ISO8583Response.ERROR_EMPTY_REQUEST_OBJECT, "Empty Request!");
			}

			if (request.getTransactionType() == null)
			{
				log.error("Request denied: Missing Transaction Type");
				return new ISO8583Response(ISO8583Response.ERROR_EMPTY_TRANSACTION_TYPE, "Missing Transaction Type");
			}

			if (interchangeName == null)
			{
				log.warn("Request denied: Null interchange name!");
				return new ISO8583Response(ISO8583Response.ERROR_EMPTY_INTERCHANGE_NAME, "Empty Interchange Name!");
			}

			ISO8583Interchange interchange = interchanges.get(interchangeName);
			if (interchange == null)
			{
				log.warn("Request denied: Unkown interchange: " + interchangeName);
				return new ISO8583Response(ISO8583Response.ERROR_UNKNOWN_INTERCHANGE_NAME, "Unkown Interchange: " + interchangeName);
			}

			request.setGatewayTimestamp(Calendar.getInstance().getTime());
			ISO8583Transaction transaction = txnManager.start(request, interchangeName);
			if (transaction == null)
			{
				log.error("Failed to start a new transaction, aborting processing and returing error response!");
				return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Failed to start a new transaction!");
			}

			log.info("REQUEST: " + transaction + ": " + request);
			transactionLog.info(interchangeName + "," + transaction.getTransactionID() + ",REQ," + request);

			ISO8583Response response = interchange.process(transaction);
			if (transaction.getState() != ISO8583Transaction.STATE_NEEDS_RECOVERY)
				txnManager.end(transaction);
			else
				log.warn(transaction + " will be queued for recovery");

			log.info("RESPONSE: " + transaction + ": " + response);
			transactionLog.info(interchangeName + "," + transaction.getTransactionID() + ",RES," + response);

			return response;
		}
		catch (Throwable e)
		{
			log.error("Processing Failed: Caught Unexpected Exception: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Processing failed: " + e.getMessage());
		}
	}

	public void run()
	{
		while (!stopFlag)
		{
			//log.debug("Scanning for transactions that need recovery...");

			for (String interchangeName : interchanges.keySet())
			{
				ISO8583Interchange interchange = interchanges.get(interchangeName);
				if (!interchange.isConnected())
					log.debug("Skipping " + interchange.getName() + " transaction recovery: Not Connected");

				List<ISO8583Transaction> recoverableList = txnManager.getNeedsRecovery(interchangeName);
				for (ISO8583Transaction recoverable : recoverableList)
				{
					if (!transactionsToRecover.contains(recoverable))
						transactionsToRecover.add(recoverable);
				}
			}

			List<ISO8583Transaction> recovered = new ArrayList<ISO8583Transaction>();

			for (ISO8583Transaction transaction : transactionsToRecover)
			{
				log.info("RECOVER: " + transaction);
				ISO8583Interchange interchange = interchanges.get(transaction.getInterchangeName());
				if (!interchange.isConnected())
				{
					log.info("Skipping recovery: Interchange " + interchange.getName() + " is not connected");
				}
				else
				{
					if (interchange.recover(transaction))
					{
						txnManager.end(transaction);
						// this should prevent a ConcurrentModificationException
						recovered.add(transaction);
						log.info("RECOVER SUCCESS: " + transaction);
					}
					else
					{
						log.error("RECOVER FAILURE: " + transaction);
					}
				}
			}

			// this should prevent a ConcurrentModificationException
			transactionsToRecover.removeAll(recovered);

			try
			{
				Thread.sleep(30000);
			}
			catch (InterruptedException e)
			{
			}
		}
	}
}
