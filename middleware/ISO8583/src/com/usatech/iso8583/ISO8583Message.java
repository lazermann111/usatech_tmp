package com.usatech.iso8583;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class ISO8583Message implements Serializable
{
	public static final String TRANSACTION_TYPE_ADJUSTMENT = "ADJUSTMENT";
	public static final String TRANSACTION_TYPE_AUTHORIZATION = "AUTHORIZATION";
	public static final String TRANSACTION_TYPE_REFUND = "REFUND";
	public static final String TRANSACTION_TYPE_REVERSAL = "REVERSAL";
	public static final String TRANSACTION_TYPE_SALE = "SALE";
	public static final String TRANSACTION_TYPE_SETTLEMENT = "SETTLEMENT";
	public static final String TRANSACTION_TYPE_SETTLEMENT_RETRY = "SETTLEMENT_RETRY";
	public static final String TRANSACTION_TYPE_TEST = "TEST";
	public static final String TRANSACTION_TYPE_UPLOAD = "UPLOAD";
	public static final String TRANSACTION_TYPE_VOID = "VOID";

	public static final String ENTRY_MODE_UNSPECIFIED = "U";
	public static final String ENTRY_MODE_MANUALLY_KEYED = "M";
	public static final String ENTRY_MODE_SWIPED = "S";
	public static final String ENTRY_MODE_CONTACTLESS_SWIPED = "R";

	public static final String PIN_ENTRY_UNSPECIFIED = "U";
	public static final String PIN_ENTRY_CAPABILITY = "Y";
	public static final String PIN_ENTRY_NO_CAPABILITY = "N";

	public static final String POS_CAPABILITY_MANUAL_ONLY = "M";
	public static final String POS_CAPABILITY_MAG_SWIPE = "S";
	public static final String POS_CAPABILITY_PROXIMITY = "P";
	public static final String POS_CAPABILITY_EMV_CONTACT = "C";
	public static final String POS_CAPABILITY_EMV_DUAL = "E";
	public static final String POS_CAPABILITY_MICR = "I";

	public static final String POS_ENVIRONMENT_UNSPECIFIED = "?";
	public static final String POS_ENVIRONMENT_UNATTENDED = "U";
	public static final String POS_ENVIRONMENT_ATTENDED = "A";
	public static final String POS_ENVIRONMENT_MAIL_TELEPHONE = "M";
	public static final String POS_ENVIRONMENT_ECOMMERCE = "E";
	public static final String POS_ENVIRONMENT_MOBILE = "C";

	public static final int FIELD_TRACE_NUMBER = 0;
	public static final int FIELD_ACQUIRER_ID = 1;
	public static final int FIELD_TERMINAL_ID = 2;
	public static final int FIELD_PAN_DATA = 3;
	public static final int FIELD_TRACK_DATA = 4;
	public static final int FIELD_ENTRY_MODE = 5;
	public static final int FIELD_PIN_ENTRY_CAPABILITY = 6;
	public static final int FIELD_POS_ENVIRONMENT = 7;
	public static final int FIELD_ONLINE = 8;
	public static final int FIELD_CVV2 = 9;
	public static final int FIELD_AVS_ZIP = 10;
	public static final int FIELD_AVS_ADDRESS = 11;
	public static final int FIELD_AMOUNT = 12;
	public static final int FIELD_RETRIEVAL_REFERENCE_NUMBER = 13;
	public static final int FIELD_EFFECTIVE_DATE = 14;
	public static final int FIELD_APPROVAL_CODE = 15;
	public static final int FIELD_ORIGINAL_AMOUNT = 16;
	public static final int FIELD_ORIGINAL_TRACE_NUMBER = 17;
	public static final int FIELD_BATCH_NUMBER = 18;
	public static final int FIELD_BATCH_TOTALS = 19;
	public static final int FIELD_CVV2_RESULT = 20;
	public static final int FIELD_AVS_ADDRESS_MATCH = 21;
	public static final int FIELD_AVS_ZIP_MATCH = 22;
	public static final int FIELD_AVS_RESULT = 23;
	public static final int FIELD_RESPONSE_CODE = 24;
	public static final int FIELD_RESPONSE_MESSAGE = 25;
	public static final int FIELD_TRANSACTION_TYPE = 26;
	public static final int FIELD_TRANSACTION_SUB_TYPE = 27;
	public static final int FIELD_TRANSACTION_LIST = 28;
	public static final int FIELD_PS2000 = 29;
	public static final int FIELD_HOST_RESPONSE_TIMEOUT = 30;
	public static final int FIELD_GATEWAY_TIMESTAMP = 31;
	public static final int FIELD_MISC_DATA = 32;
	public static final int FIELD_TERMINAL_ENCRYPT_KEY = 33;
	public static final int FIELD_PARTIAL_AUTH_ALLOWED = 34;
	public static final int FIELD_POS_ENTRY_CAPABILITY = 35;
	public static final int FIELD_POS_IDENTIFIER = 36;
	public static final int FIELD_INVOICE_NUMBER = 37;
	public static final int FIELD_STREET = 38;
	public static final int FIELD_CITY = 39;
	public static final int FIELD_STATE = 40;
	public static final int FIELD_POSTAL = 41;
	public static final int FIELD_COUNTRY_CODE = 42;
	public static final int FIELD_PHONE = 43;
	public static final int FIELD_EMAIL = 44;
	public static final int FIELD_CUSTOMER_ID = 45;
	public static final int FIELD_DOING_BUSINESS_AS = 46;
	
	public static final int BIT_PRIMARY_ACCOUNT_NUMBER = 2;
	public static final int BIT_EXPIRATION_DATE = 14;
	public static final int BIT_PAN_SEQUENCE_NUMBER = 23;
	public static final int BIT_TRACK_2_DATA = 35;
	public static final int BIT_TRACK_1_DATA = 45;

	public static final HashMap<String, String> TYPE_NAMES = new HashMap<String, String>();
	public static final HashMap<Integer, String> FIELD_NAMES = new HashMap<Integer, String>();
	public static final HashMap<String, String> ENTRY_MODE_NAMES = new HashMap<String, String>();
	public static final HashMap<String, String> PIN_ENTRY_NAMES = new HashMap<String, String>();
	public static final HashMap<String, String> POS_ENVIRONMENT_NAMES = new HashMap<String, String>();

	static
	{
		TYPE_NAMES.put(TRANSACTION_TYPE_ADJUSTMENT, "Adjustment");
		TYPE_NAMES.put(TRANSACTION_TYPE_AUTHORIZATION, "Authorization");
		TYPE_NAMES.put(TRANSACTION_TYPE_REFUND, "Refund");
		TYPE_NAMES.put(TRANSACTION_TYPE_REVERSAL, "Reversal");
		TYPE_NAMES.put(TRANSACTION_TYPE_SALE, "Sale");
		TYPE_NAMES.put(TRANSACTION_TYPE_SETTLEMENT, "Settlement");
		TYPE_NAMES.put(TRANSACTION_TYPE_TEST, "Test");
		TYPE_NAMES.put(TRANSACTION_TYPE_UPLOAD, "Upload");
		TYPE_NAMES.put(TRANSACTION_TYPE_VOID, "Void");

		FIELD_NAMES.put(FIELD_TRACE_NUMBER, "Trace Number");
		FIELD_NAMES.put(FIELD_ACQUIRER_ID, "Acquirer ID");
		FIELD_NAMES.put(FIELD_TERMINAL_ID, "Terminal ID");
		FIELD_NAMES.put(FIELD_PAN_DATA, "Pan Data");
		FIELD_NAMES.put(FIELD_TRACK_DATA, "Track Data");
		FIELD_NAMES.put(FIELD_ENTRY_MODE, "Entry Mode");
		FIELD_NAMES.put(FIELD_PIN_ENTRY_CAPABILITY, "PIN Entry Capability");
		FIELD_NAMES.put(FIELD_POS_ENVIRONMENT, "POS Environment");
		FIELD_NAMES.put(FIELD_ONLINE, "Online");
		FIELD_NAMES.put(FIELD_CVV2, "CVV2");
		FIELD_NAMES.put(FIELD_AVS_ZIP, "AVS Zip");
		FIELD_NAMES.put(FIELD_AVS_ADDRESS, "AVS Address");
		FIELD_NAMES.put(FIELD_AMOUNT, "Amount");
		FIELD_NAMES.put(FIELD_RETRIEVAL_REFERENCE_NUMBER, "Retrieval Reference Number");
		FIELD_NAMES.put(FIELD_EFFECTIVE_DATE, "Effective Date");
		FIELD_NAMES.put(FIELD_APPROVAL_CODE, "Approval Code");
		FIELD_NAMES.put(FIELD_ORIGINAL_AMOUNT, "Original Amount");
		FIELD_NAMES.put(FIELD_ORIGINAL_TRACE_NUMBER, "Original Trace Number");
		FIELD_NAMES.put(FIELD_BATCH_NUMBER, "Batch Number");
		FIELD_NAMES.put(FIELD_BATCH_TOTALS, "Batch Totals");
		FIELD_NAMES.put(FIELD_CVV2_RESULT, "CVV2 Result");
		FIELD_NAMES.put(FIELD_AVS_ADDRESS_MATCH, "AVS Address Match");
		FIELD_NAMES.put(FIELD_AVS_ZIP_MATCH, "AVS Zip Match");
		FIELD_NAMES.put(FIELD_AVS_RESULT, "AVS Result");
		FIELD_NAMES.put(FIELD_RESPONSE_CODE, "Response Code");
		FIELD_NAMES.put(FIELD_RESPONSE_MESSAGE, "Response Message");
		FIELD_NAMES.put(FIELD_TRANSACTION_TYPE, "Transaction Type");
		FIELD_NAMES.put(FIELD_TRANSACTION_SUB_TYPE, "Transaction Subtype");
		FIELD_NAMES.put(FIELD_TRANSACTION_LIST, "Transaction List");
		FIELD_NAMES.put(FIELD_PS2000, "Payment Services 2000");
		FIELD_NAMES.put(FIELD_HOST_RESPONSE_TIMEOUT, "Request Timeout");
		FIELD_NAMES.put(FIELD_GATEWAY_TIMESTAMP, "Gateway Timestamp");
		FIELD_NAMES.put(FIELD_MISC_DATA, "Misc Data");
		FIELD_NAMES.put(FIELD_TERMINAL_ENCRYPT_KEY, "Terminal Encrypt Key");
		FIELD_NAMES.put(FIELD_PARTIAL_AUTH_ALLOWED, "Partial Auth Allowed");
		FIELD_NAMES.put(FIELD_POS_ENTRY_CAPABILITY, "POS Entry Capability");
		FIELD_NAMES.put(FIELD_POS_IDENTIFIER, "POS Identifier");
		FIELD_NAMES.put(FIELD_INVOICE_NUMBER, "Invoice Number");
		FIELD_NAMES.put(FIELD_STREET, "Street Address");
		FIELD_NAMES.put(FIELD_CITY, "City");
		FIELD_NAMES.put(FIELD_STATE, "State");
		FIELD_NAMES.put(FIELD_POSTAL, "Postal Code");
		FIELD_NAMES.put(FIELD_COUNTRY_CODE, "Country Code");
		FIELD_NAMES.put(FIELD_PHONE, "Phone Number");
		FIELD_NAMES.put(FIELD_EMAIL, "Email");
		FIELD_NAMES.put(FIELD_CUSTOMER_ID, "Customer Id");
		FIELD_NAMES.put(FIELD_DOING_BUSINESS_AS, "Doing Business As");

		ENTRY_MODE_NAMES.put(ENTRY_MODE_UNSPECIFIED, "Unspecified");
		ENTRY_MODE_NAMES.put(ENTRY_MODE_MANUALLY_KEYED, "Manually Keyed");
		ENTRY_MODE_NAMES.put(ENTRY_MODE_SWIPED, "Swiped");
		ENTRY_MODE_NAMES.put(ENTRY_MODE_CONTACTLESS_SWIPED, "Contactless Swiped");

		PIN_ENTRY_NAMES.put(PIN_ENTRY_UNSPECIFIED, "Unspecified");
		PIN_ENTRY_NAMES.put(PIN_ENTRY_CAPABILITY, "Capability");
		PIN_ENTRY_NAMES.put(PIN_ENTRY_NO_CAPABILITY, "No Capability");

		POS_ENVIRONMENT_NAMES.put(POS_ENVIRONMENT_UNSPECIFIED, "Unspecified");
		POS_ENVIRONMENT_NAMES.put(POS_ENVIRONMENT_UNATTENDED, "Unattended");
		POS_ENVIRONMENT_NAMES.put(POS_ENVIRONMENT_ATTENDED, "Attended");
		POS_ENVIRONMENT_NAMES.put(POS_ENVIRONMENT_MOBILE, "Mobile");
		POS_ENVIRONMENT_NAMES.put(POS_ENVIRONMENT_MAIL_TELEPHONE, "Mail/Telephone");
		POS_ENVIRONMENT_NAMES.put(POS_ENVIRONMENT_ECOMMERCE, "ECommerce");
	}

	protected Map fields = Collections.synchronizedMap(new HashMap());

	protected transient String rendered;

	public ISO8583Message()
	{
		super();
	}

	public boolean hasField(int fieldID)
	{
		return (fields.get(fieldID) != null);
	}

	public Object getField(int fieldID)
	{
		return fields.get(fieldID);
	}

	public void setField(int fieldID, Object value)
	{
		if (value != null)
			fields.put(fieldID, value);
	}

	public String getTransactionType()
	{
		return (String) getField(FIELD_TRANSACTION_TYPE);
	}

	public void setTransactionType(String transactionType)
	{
		setField(FIELD_TRANSACTION_TYPE, transactionType);
	}

	public boolean hasTransactionType()
	{
		return hasField(FIELD_TRANSACTION_TYPE);
	}

	public String getTransactionSubType()
	{
		return (String) getField(FIELD_TRANSACTION_SUB_TYPE);
	}

	public void setTransactionSubType(String transactionSubType)
	{
		setField(FIELD_TRANSACTION_SUB_TYPE, transactionSubType);
	}

	public boolean hasTransactionSubType()
	{
		return hasField(FIELD_TRANSACTION_SUB_TYPE);
	}

	//	public abstract String getClassName();
	
	public PS2000 getPS2000()
	{
		return (PS2000) getField(FIELD_PS2000);
	}
	
	public void setPS2000(PS2000 ps2000)
	{
		setField(FIELD_PS2000, ps2000);
	}
	
	public boolean hasPS2000()
	{
		return hasField(FIELD_PS2000);
	}
	
	public String getMiscData()
	{
		return (String) getField(FIELD_MISC_DATA);
	}

	public void setMiscData(String miscData)
	{
		setField(FIELD_MISC_DATA, miscData);
	}

	public boolean hasMiscData()
	{
		return hasField(FIELD_MISC_DATA);
	}
	
	public String getTerminalEncryptKey()
	{
		return (String) getField(FIELD_TERMINAL_ENCRYPT_KEY);
	}

	public void setTerminalEncryptKey(String terminalEncryptKey)
	{
		setField(FIELD_TERMINAL_ENCRYPT_KEY, terminalEncryptKey);
	}

	public boolean hasTerminalEncryptKey()
	{
		return hasField(FIELD_TERMINAL_ENCRYPT_KEY);
	}

	public String getDoingBusinessAs() {
		return (String) getField(FIELD_DOING_BUSINESS_AS);
	}

	public void setDoingBusinessAs(String doingBusinessAs) {
		setField(FIELD_DOING_BUSINESS_AS, doingBusinessAs);
	}

	public boolean hasDoingBusinessAs() {
		return hasField(FIELD_DOING_BUSINESS_AS);
	}

	public String getStreet() {
		return (String) getField(FIELD_STREET);
	}

	public void setStreet(String street) {
		setField(FIELD_STREET, street);
	}

	public boolean hasStreet() {
		return hasField(FIELD_STREET);
	}

	public String getCity() {
		return (String) getField(FIELD_CITY);
	}

	public void setCity(String city) {
		setField(FIELD_CITY, city);
	}

	public boolean hasCity() {
		return hasField(FIELD_CITY);
	}

	public String getState() {
		return (String) getField(FIELD_STATE);
	}

	public void setState(String state) {
		setField(FIELD_STATE, state);
	}

	public boolean hasState() {
		return hasField(FIELD_STATE);
	}

	public String getPostal() {
		return (String) getField(FIELD_POSTAL);
	}

	public void setPostal(String postal) {
		setField(FIELD_POSTAL, postal);
	}

	public boolean hasPostal() {
		return hasField(FIELD_POSTAL);
	}

	public String getCountryCode() {
		return (String) getField(FIELD_COUNTRY_CODE);
	}

	public void setCountryCode(String countryCode) {
		setField(FIELD_COUNTRY_CODE, countryCode);
	}

	public boolean hasCountryCode() {
		return hasField(FIELD_COUNTRY_CODE);
	}

	public String getPhone() {
		return (String) getField(FIELD_PHONE);
	}

	public void setPhone(String phone) {
		setField(FIELD_PHONE, phone);
	}

	public boolean hasPhone() {
		return hasField(FIELD_PHONE);
	}

	public String getEmail() {
		return (String) getField(FIELD_EMAIL);
	}

	public void setEmail(String email) {
		setField(FIELD_EMAIL, email);
	}

	public boolean hasEmail() {
		return hasField(FIELD_EMAIL);
	}

	public Long getCustomerId() {
		return (Long) getField(FIELD_CUSTOMER_ID);
	}

	public void setCustomerId(Long customerId) {
		setField(FIELD_CUSTOMER_ID, customerId);
	}

	public boolean hasCustomerId() {
		return hasField(FIELD_CUSTOMER_ID);
	}

	public String toString()
	{
		if (rendered != null)
			return rendered;

		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName());
		sb.append("[");

		Iterator iter = fields.entrySet().iterator();
		while (iter.hasNext())
		{
			Map.Entry entry = (Map.Entry) iter.next();
			String name = FIELD_NAMES.get(entry.getKey());
			sb.append(" ");
			sb.append((name == null ? entry.getKey() : name));
			sb.append("=");

			switch (Integer.valueOf(entry.getKey().toString())) {
				case FIELD_AVS_ADDRESS:
				case FIELD_AVS_ZIP:
				case FIELD_CVV2:
				case FIELD_TERMINAL_ENCRYPT_KEY:
					sb.append(entry.getValue().toString().length()).append(" bytes");
					break;
				default:
					if (entry.getValue() instanceof List)
						sb.append("List of " + ((List) entry.getValue()).size());
					else
						sb.append(entry.getValue());
			}
			sb.append(";");
		}

		sb.append(" ]");
		rendered = sb.toString();
		return rendered;
	}
}
