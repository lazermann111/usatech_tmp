package com.usatech.iso8583;

import java.io.Serializable;
import java.util.*;

public class ISO8583Response extends ISO8583Message implements Serializable
{
	public static final String SUCCESS 									= "00";
	public static final String ERROR_UNSUPPORTED_TRANSACTION_TYPE 		= "GE00";
	public static final String ERROR_UNKNOWN_INTERCHANGE_NAME 			= "GE01";
	public static final String ERROR_EMPTY_REQUEST_OBJECT 				= "GE02";
	public static final String ERROR_EMPTY_INTERCHANGE_NAME 			= "GE03";
	public static final String ERROR_INTERNAL_ERROR 					= "GE04";
	public static final String ERROR_HOST_RESPONSE_TIMEOUT 				= "GE05";
	public static final String ERROR_CLIENT_REQUEST_FAILED_VALIDATION 	= "GE06";
	public static final String ERROR_HOST_RESPONSE_FAILED_VALIDATION 	= "GE07";
	public static final String ERROR_EMPTY_TRANSACTION_TYPE 			= "GE08";
	public static final String ERROR_HOST_CONNECTION_FAILURE 			= "GE09";

	public static final String AVS_ADDRESS_MATCH_FAILURE 				= "N";
	public static final String AVS_ADDRESS_MATCH_SUCCESS 				= "Y";
	public static final String AVS_ADDRESS_MATCH_UNAVAILABLE 			= "X";

	public static final String AVS_ZIP_MATCH_FAILURE 					= "N";
	public static final String AVS_ZIP_MATCH_SUCCESS 					= "Y";
	public static final String AVS_ZIP_MATCH_UNAVAILABLE 				= "X";

	public static final String AVS_RESULT_ADDRESS_SUCCESS_ZIP_FAILURE 	= "A";
	public static final String AVS_RESULT_NOT_SUPPORTED 				= "E";
	public static final String AVS_RESULT_ADDRESS_FAILURE_ZIP_FAILURE 	= "N";
	public static final String AVS_RESULT_TEMPORARILY_UNAVAILABLE 		= "R";
	public static final String AVS_RESULT_ADDRESS_UNAVAILABLE 			= "U";
	public static final String AVS_RESULT_ADDRESS_SUCCESS_ZIP_SUCCESS 	= "Y";
	public static final String AVS_RESULT_ADDRESS_FAILURE_ZIP_SUCCESS 	= "Z";

	public static final HashMap<String, String> ERROR_NAMES = new HashMap<String, String>();
	public static final HashMap<String, String> AVS_ADDRESS_MATCH_NAMES = new HashMap<String, String>();
	public static final HashMap<String, String> AVS_ZIP_MATCH_NAMES = new HashMap<String, String>();
	public static final HashMap<String, String> AVS_RESULT_NAMES = new HashMap<String, String>();

	static
	{
		ERROR_NAMES.put(ERROR_UNSUPPORTED_TRANSACTION_TYPE, "Unsupported Transaction Type");
		ERROR_NAMES.put(ERROR_UNKNOWN_INTERCHANGE_NAME, "Unknown Interchange Name");
		ERROR_NAMES.put(ERROR_EMPTY_REQUEST_OBJECT, "Empty Request");
		ERROR_NAMES.put(ERROR_EMPTY_INTERCHANGE_NAME, "Empty Interchange Name");
		ERROR_NAMES.put(ERROR_INTERNAL_ERROR, "Internal Error");
		ERROR_NAMES.put(ERROR_HOST_RESPONSE_TIMEOUT, "Host Response Timeout");
		ERROR_NAMES.put(ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Client Request Failed Validation");
		ERROR_NAMES.put(ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host Response Failed Validation");
		ERROR_NAMES.put(ERROR_HOST_CONNECTION_FAILURE, "Host Connection Failure");

		AVS_ADDRESS_MATCH_NAMES.put(AVS_ADDRESS_MATCH_FAILURE, "Address Match Failure");
		AVS_ADDRESS_MATCH_NAMES.put(AVS_ADDRESS_MATCH_SUCCESS, "Address Match Success");
		AVS_ADDRESS_MATCH_NAMES.put(AVS_ADDRESS_MATCH_UNAVAILABLE, "Address Match Service Unavailable");

		AVS_ZIP_MATCH_NAMES.put(AVS_ZIP_MATCH_FAILURE, "Zip Match Failure");
		AVS_ZIP_MATCH_NAMES.put(AVS_ZIP_MATCH_SUCCESS, "Zip Match Success");
		AVS_ZIP_MATCH_NAMES.put(AVS_ZIP_MATCH_UNAVAILABLE, "Zip Match Service Unavailable");

		AVS_RESULT_NAMES.put(AVS_RESULT_ADDRESS_SUCCESS_ZIP_SUCCESS, "Address Success Zip Success");
		AVS_RESULT_NAMES.put(AVS_RESULT_ADDRESS_SUCCESS_ZIP_FAILURE, "Address Success Zip Failure");
		AVS_RESULT_NAMES.put(AVS_RESULT_ADDRESS_FAILURE_ZIP_SUCCESS, "Address Failure Zip Success");
		AVS_RESULT_NAMES.put(AVS_RESULT_ADDRESS_FAILURE_ZIP_FAILURE, "Address Failure Zip Failure");
		AVS_RESULT_NAMES.put(AVS_RESULT_NOT_SUPPORTED, "AVS Not Supported");
		AVS_RESULT_NAMES.put(AVS_RESULT_ADDRESS_UNAVAILABLE, "AVS Unavailable");
	}

	protected transient String rendered;

	public ISO8583Response()
	{
		super();
	}

	public boolean isInternalError()
	{
		if (hasResponseCode() && getResponseCode().startsWith("GE"))
			return true;
		return false;
	}

	public ISO8583Response(String responseCode, String responseMessage)
	{
		setResponseCode(responseCode);
		setResponseMessage(responseMessage);
	}

	public int getAmount()
	{
		if (!hasAmount())
			return -1;

		return (Integer) getField(FIELD_AMOUNT);
	}

	public void setAmount(int amount)
	{
		setField(FIELD_AMOUNT, amount);
	}

	public boolean hasAmount()
	{
		return hasField(FIELD_AMOUNT);
	}

	public String getApprovalCode()
	{
		return (String) getField(FIELD_APPROVAL_CODE);
	}

	public void setApprovalCode(String approvalCode)
	{
		setField(FIELD_APPROVAL_CODE, approvalCode);
	}

	public boolean hasApprovalCode()
	{
		return hasField(FIELD_APPROVAL_CODE);
	}

	public String getCvv2Result()
	{
		return (String) getField(FIELD_CVV2_RESULT);
	}

	public void setCvv2Result(String cvv2)
	{
		setField(FIELD_CVV2_RESULT, cvv2);
	}

	public boolean hasCvv2Result()
	{
		return hasField(FIELD_CVV2_RESULT);
	}

	public String getAvsAddressMatch()
	{
		return (String) getField(FIELD_AVS_ADDRESS_MATCH);
	}

	public void setAvsAddressMatch(String avsAddressMatch)
	{
		setField(FIELD_AVS_ADDRESS_MATCH, avsAddressMatch);
	}

	public boolean hasAvsAddressMatch()
	{
		return hasField(FIELD_AVS_ADDRESS_MATCH);
	}

	public String getAvsZipMatch()
	{
		return (String) getField(FIELD_AVS_ZIP_MATCH);
	}

	public void setAvsZipMatch(String avsZipMatch)
	{
		setField(FIELD_AVS_ZIP_MATCH, avsZipMatch);
	}

	public boolean hasAvsZipMatch()
	{
		return hasField(FIELD_AVS_ZIP_MATCH);
	}

	public String getAvsResult()
	{
		return (String) getField(FIELD_AVS_RESULT);
	}

	public void setAvsResult(String avsResult)
	{
		setField(FIELD_AVS_RESULT, avsResult);
	}

	public boolean hasAvsResult()
	{
		return hasField(FIELD_AVS_RESULT);
	}

	public Date getEffectiveDate()
	{
		return (Date) getField(FIELD_EFFECTIVE_DATE);
	}

	public void setEffectiveDate(Date effectiveDate)
	{
		setField(FIELD_EFFECTIVE_DATE, effectiveDate);
	}

	public boolean hasEffectiveDate()
	{
		return hasField(FIELD_EFFECTIVE_DATE);
	}

	public String getRetrievalReferenceNumber()
	{
		return (String) getField(FIELD_RETRIEVAL_REFERENCE_NUMBER);
	}

	public void setRetrievalReferenceNumber(String retrievalReferenceNumber)
	{
		setField(FIELD_RETRIEVAL_REFERENCE_NUMBER, retrievalReferenceNumber);
	}

	public boolean hasRetrievalReferenceNumber()
	{
		return hasField(FIELD_RETRIEVAL_REFERENCE_NUMBER);
	}

	public int getTraceNumber()
	{
		if (!hasTraceNumber())
			return -1;

		return (Integer) getField(FIELD_TRACE_NUMBER);
	}

	public void setTraceNumber(int traceNumber)
	{
		setField(FIELD_TRACE_NUMBER, traceNumber);
	}

	public boolean hasTraceNumber()
	{
		return hasField(FIELD_TRACE_NUMBER);
	}

	public String getResponseCode()
	{
		return (String) getField(FIELD_RESPONSE_CODE);
	}

	public void setResponseCode(String responseCode)
	{
		setField(FIELD_RESPONSE_CODE, responseCode);
	}

	public boolean hasResponseCode()
	{
		return hasField(FIELD_RESPONSE_CODE);
	}

	public String getResponseMessage()
	{
		return (String) getField(FIELD_RESPONSE_MESSAGE);
	}

	public void setResponseMessage(String responseMessage)
	{
		setField(FIELD_RESPONSE_MESSAGE, responseMessage);
	}

	public boolean hasResponseMessage()
	{
		return hasField(FIELD_RESPONSE_MESSAGE);
	}
}
