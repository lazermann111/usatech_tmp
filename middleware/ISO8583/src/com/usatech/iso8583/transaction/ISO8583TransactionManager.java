package com.usatech.iso8583.transaction;

import java.util.List;

import com.usatech.iso8583.ISO8583Request;

public interface ISO8583TransactionManager
{
	public List<ISO8583Transaction> getPending(String interchangeName);

	public List<ISO8583Transaction> getNeedsRecovery(String interchangeName);

	public ISO8583Transaction start(ISO8583Request request, String interchangeName);

	public void end(ISO8583Transaction transaction);
}
