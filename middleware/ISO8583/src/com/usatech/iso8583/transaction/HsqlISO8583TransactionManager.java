package com.usatech.iso8583.transaction;

import java.sql.*;
import java.util.*;
import java.util.concurrent.ThreadPoolExecutor;

import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.util.ISO8583Util;

import org.apache.commons.configuration.*;
import org.apache.commons.logging.*;
import org.picocontainer.Startable;

public class HsqlISO8583TransactionManager implements ISO8583TransactionManager, Startable
{
	private static Log log = LogFactory.getLog(HsqlISO8583TransactionManager.class);

	private ThreadPoolExecutor threadPool;
	private Configuration config;

	private Driver driver;
	private Connection connection;
    private Statement statement;
    private PreparedStatement tranInsert;
    private PreparedStatement tranUpdate;
    private PreparedStatement tranDelete;
    private PreparedStatement tranGetPending;
    private PreparedStatement tranGetNeedsRecovery;
	private Object sqlLock = new Object();

	public HsqlISO8583TransactionManager(ThreadPoolExecutor threadPool, Configuration config) throws ConfigurationException
	{
		this.threadPool = threadPool;
		this.config = config;

		String dbDriverClassName = config.getString("hsqltxnmanager.driver");
		String dbUrl = config.getString("hsqltxnmanager.url");
		log.info("hsqltxnmanager.url = " + dbUrl);

		try
		{
			driver = (Driver) Class.forName(dbDriverClassName).newInstance();
			DriverManager.registerDriver(driver);
			connection = DriverManager.getConnection(dbUrl);
		}
		catch (Exception e)
		{
			throw new ConfigurationException("Failed to establish a connection to the HSQL database using the url: " + dbUrl + ": " + e.getMessage(), e);
		}
        
        try
        {
            statement = connection.createStatement();
            tranInsert = connection.prepareStatement("INSERT INTO transaction(transaction_id, transaction_state, objectxml, interchange_name) VALUES (?,?,?,?);");
            tranUpdate = connection.prepareStatement("UPDATE transaction SET transaction_state = ?, objectxml = ? where transaction_id = ?;");
            tranDelete = connection.prepareStatement("DELETE FROM transaction WHERE transaction_id = ?;");
            tranGetPending = connection.prepareStatement("SELECT transaction_id, transaction_state, objectxml FROM transaction WHERE interchange_name = ? and transaction_state != ?;");
            tranGetNeedsRecovery = connection.prepareStatement("SELECT transaction_id, transaction_state, objectxml FROM transaction WHERE interchange_name = ? and transaction_state = ?;");
        }
        catch (SQLException e)
        {
            throw new ConfigurationException("Failed to prepare SQL statements : " + e.getMessage(), e);
        }
	}

	public List<ISO8583Transaction> getPending(String interchangeName)
	{
        ResultSet rs = null;
        ArrayList<ISO8583Transaction> list = new ArrayList<ISO8583Transaction>();
        
		synchronized (sqlLock)
		{
			try
			{
                tranGetPending.setString(1, interchangeName);
                tranGetPending.setInt(2, ISO8583Transaction.STATE_NEEDS_RECOVERY);
				rs = tranGetPending.executeQuery();

				while (rs.next())
				{
					HsqlISO8583Transaction tran = (HsqlISO8583Transaction) ISO8583Util.fromXML(rs.getString(3));
					tran.setTransactionManager(this);
					tran.setStateNoUpdate(rs.getInt(2));
					list.add(tran);
				}

			}
			catch (SQLException e)
			{
				log.error("Failed to lookup pending transactions: " + e.getMessage(), e);
			}
			finally
			{
				if (rs != null)
					try
					{
						rs.close();
					}
					catch (Exception e2)
					{
					}
			}
		}
        
        return list;
	}

	public List<ISO8583Transaction> getNeedsRecovery(String interchangeName)
	{
        ResultSet rs = null;
        ArrayList<ISO8583Transaction> list = new ArrayList<ISO8583Transaction>();
        
		synchronized (sqlLock)
		{
			try
			{
                tranGetNeedsRecovery.setString(1, interchangeName);
                tranGetNeedsRecovery.setInt(2, ISO8583Transaction.STATE_NEEDS_RECOVERY);
				rs = tranGetNeedsRecovery.executeQuery();

				while (rs.next())
				{
					HsqlISO8583Transaction tran = (HsqlISO8583Transaction) ISO8583Util.fromXML(rs.getString(3));
					tran.setTransactionManager(this);
					tran.setStateNoUpdate(rs.getInt(2));
					list.add(tran);
				}

			}
			catch (SQLException e)
			{
				log.error("Failed to lookup needs recovery transactions: " + e.getMessage(), e);
			}
			finally
			{
				if (rs != null)
					try
					{
						rs.close();
					}
					catch (Exception e2)
					{
					}
			}			
		}
        
        return list;
	}

	public ISO8583Transaction start(ISO8583Request request, String interchangeName)
	{
        log.info("Starting ISO8583Transaction, Trace Number=" + request.getTraceNumber());

        HsqlISO8583Transaction tran;
        
		synchronized (sqlLock)
		{
            int transactionID = getNextTransactionID();
            if (transactionID <= 0)
                return null;
            
			tran = new HsqlISO8583Transaction(this, transactionID, request, interchangeName);
			String xml = ISO8583Util.toXML(tran);

			try
			{
                tranInsert.setInt(1, transactionID);
                tranInsert.setInt(2, ISO8583Transaction.STATE_NEW);
                tranInsert.setString(3, xml);
                tranInsert.setString(4, interchangeName);
                tranInsert.execute();
			}
			catch (Exception e)
			{
				log.error("Failed to insert new transaction " + transactionID + ": " + e.getMessage(), e);
				return null;
			}
		}
        
        return tran;
	}

	private int getNextTransactionID()
	{
		int transactionID = -1;
		ResultSet rs = null;

		try
		{
			rs = statement.executeQuery("SELECT NEXT VALUE FOR transaction_id_seq FROM dual;");

			if (rs.next())
				transactionID = rs.getInt(1);

		}
		catch (SQLException e)
		{
			log.error("Failed to get next value from transaction_id_seq sequence: " + e.getMessage(), e);
			return -1;
		}
		finally
		{
			if (rs != null)
				try
				{
					rs.close();
				}
				catch (Exception e2)
				{
				}
		}

		return transactionID;
	}

	public void end(ISO8583Transaction transaction)
	{
        log.info("Ending ISO8583Transaction, Trace Number=" + transaction.getRequest().getTraceNumber());
        
        synchronized (sqlLock)
        {
    		try
    		{
                tranDelete.setInt(1, transaction.getTransactionID());
                tranDelete.execute();
    		}
    		catch (SQLException e)
    		{
    			log.error("Failed to delete " + transaction + ": " + e.getMessage(), e);
    		}
    		
    		checkpoint();
        }
	}

	public synchronized void start()
	{
		log.info("HsqlISO8583TransactionManager Starting up...");
		// anything to do?
	}

	public synchronized void stop()
	{
		log.info("HsqlISO8583TransactionManager Shutting down...");
        
		try
		{
            statement.executeUpdate("SHUTDOWN;");
		}
		catch (SQLException e)
		{
			log.error("Failed to shutdown HSQL database cleanly: " + e.getMessage(), e);
		}
        
		try {statement.close();} catch (Exception e) {}
        try {tranInsert.close();} catch (Exception e) {}
        try {tranUpdate.close();} catch (Exception e) {}
        try {tranDelete.close();} catch (Exception e) {}
        try {tranGetPending.close();} catch (Exception e) {}
        try {tranGetNeedsRecovery.close();} catch (Exception e) {}
        try {connection.close();} catch (Exception e) {}
        try {DriverManager.deregisterDriver(driver);} catch (Exception e) {}
	}
	
	private void checkpoint()
	{
		long st = System.currentTimeMillis();

		try
		{
            statement.executeUpdate("CHECKPOINT;");
		}
		catch (SQLException e)
		{
			log.error("Failed to checkpoint HSQL database: " + e.getMessage(), e);
		}		
		long et = (System.currentTimeMillis() - st);
		log.debug("CHECKPOINT executed in " + et + " ms.");
	}

	void update(ISO8583Transaction transaction)
	{
        HsqlISO8583Transaction hsqlTran = (HsqlISO8583Transaction) transaction;
        
        synchronized (sqlLock)
        {
    		try
    		{
                tranUpdate.setInt(1, hsqlTran.getState());
                tranUpdate.setString(2, ISO8583Util.toXML(hsqlTran));
                tranUpdate.setInt(3, hsqlTran.getTransactionID());
                tranUpdate.execute();
    		}
    		catch (SQLException e)
    		{
    			log.error("Failed to update " + transaction + ": " + e.getMessage(), e);
    		}
        }
	}
}
