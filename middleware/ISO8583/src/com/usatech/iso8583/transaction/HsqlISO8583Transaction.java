package com.usatech.iso8583.transaction;

import com.usatech.iso8583.*;

public class HsqlISO8583Transaction implements ISO8583Transaction, Runnable
{
	private transient HsqlISO8583TransactionManager txnManager;
	private transient int state;

	private int transactionID;
	private ISO8583Request request;
	private ISO8583Response response;
	private String interchangeName;

	public HsqlISO8583Transaction(HsqlISO8583TransactionManager txnManager, int transactionID, ISO8583Request request, String interchangeName)
	{
		this.txnManager = txnManager;
		this.transactionID = transactionID;
		this.request = request;
		this.interchangeName = interchangeName;
		this.state = ISO8583Transaction.STATE_NEW;
	}

	public int getTransactionID()
	{
		return transactionID;
	}

	public ISO8583Request getRequest()
	{
		return request;
	}

	public String getInterchangeName()
	{
		return interchangeName;
	}

	public void setState(int state)
	{
		this.state = state;
		txnManager.update(this);
	}

	void setStateNoUpdate(int state)
	{
		this.state = state;
	}

	public int getState()
	{
		return state;
	}

	public HsqlISO8583TransactionManager getTransactionManager()
	{
		return txnManager;
	}

	public void setTransactionManager(HsqlISO8583TransactionManager txnManager)
	{
		this.txnManager = txnManager;
	}

	public void run()
	{
		// is this needed?
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("HsqlISO8583Transaction[id=");
		sb.append(transactionID);
		sb.append(" state=");
		sb.append(ISO8583Transaction.STATE_DESC[state]);
		sb.append(" interchange=");
		sb.append(interchangeName);
		//sb.append(" request=");
		//sb.append(request);
		//sb.append(" response=");
		//sb.append(response);
		sb.append("]");
		return sb.toString();
	}
}
