package com.usatech.iso8583.transaction;

import com.usatech.iso8583.ISO8583Request;

public interface ISO8583Transaction
{
	public static final int STATE_NEW = 0;
	public static final int STATE_REQUEST_UNVALIDATED = 11;
	public static final int STATE_REQUEST_FAILED_VALIDATION = 12;
	public static final int STATE_REQUEST_VALIDATED = 13;
	public static final int STATE_ERROR_PRE_TRANSMIT = 14;
	public static final int STATE_REQUEST_TRANSMITTED = 15;
	public static final int STATE_ERROR_DURING_TRANSMIT = 16;
	public static final int STATE_RESPONSE_UNVALIDATED = 17;
	public static final int STATE_RESPONSE_FAILED_VALIDATION = 18;
	public static final int STATE_RESPONSE_VALIDATED = 19;
	public static final int STATE_ERROR_POST_TRANSMIT = 20;
	public static final int STATE_NEEDS_RECOVERY = 21;

	public static final String[] STATE_DESC = { "NEW", // 00
			"", // 01
			"", // 02
			"", // 03
			"", // 04
			"", // 05
			"", // 06
			"", // 07
			"", // 08
			"", // 09
			"", // 10
			"REQUEST_UNVALIDATED", // 11
			"REQUEST_FAILED_VALIDATION", // 12
			"REQUEST_VALIDATED", // 13
			"ERROR_PRE_TRANSMIT", // 14
			"REQUEST_TRANSMITTED", // 15
			"ERROR_DURING_TRANSMIT", // 16
			"RESPONSE_UNVALIDATED", // 17
			"RESPONSE_FAILED_VALIDATION", // 18
			"RESPONSE_VALIDATED", // 19
			"ERROR_POST_TRANSMIT", // 20
			"NEEDS_RECOVERY" // 21
	};

	public int getTransactionID();

	public ISO8583Request getRequest();

	public String getInterchangeName();

	public void setState(int state);

	public int getState();
}
