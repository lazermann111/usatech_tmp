package com.usatech.iso8583.test;

import com.usatech.iso8583.interchange.fhms.jpos.MerchantLinkFrameRelayChannel;
import com.usatech.iso8583.interchange.fhms.jpos.packager.FHMSPackager;

import org.jpos.iso.*;
import org.jpos.util.*;

public class Test1
{

	public static void main(String args[]) throws Exception
	{
		new Test1();
	}

	public Test1() throws Exception
	{
		Logger logger = new Logger();
		logger.addListener(new SimpleLogListener(System.out));
		ISOChannel channel = new MerchantLinkFrameRelayChannel("10.0.0.76", 1234, new FHMSPackager());
		((LogSource) channel).setLogger(logger, "test");
		channel.connect();
		ISOMsg out = new ISOMsg();
		out.setMTI("0100");
		out.set(3, "004000");
		out.set(4, "1000");
		out.set(11, "1234");
		out.set(22, "022");
		out.set(24, "000");
		out.set(25, "00");
		out.set(35, "5490990148049999=12121234554323");
		out.set(41, "521");
		out.set(42, "0020030003");
		channel.send(out);
		//ISOMsg in = channel.receive();
		channel.disconnect();
	}
}
