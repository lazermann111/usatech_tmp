package com.usatech.iso8583.test;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class TCPNIOEchoServer
{
	private static final char[] HEX_CHARS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static void main(String args[]) throws Exception
	{
		new TCPNIOEchoServer(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
	}

	private ServerSocketChannel serverChannel;
	private ThreadPoolExecutor threadPool;
	private int delay = 0;

	public TCPNIOEchoServer(int port, int delay) throws Exception
	{
		this.delay = delay;

		threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		serverChannel = ServerSocketChannel.open();
		serverChannel.socket().bind(new InetSocketAddress(port));

		System.out.println("Echo Server ready to accept connections on port " + port);

		try
		{
			while (true)
			{
				ByteBuffer receiveBuffer = ByteBuffer.allocateDirect(50000);
				SocketChannel channel = serverChannel.accept();
				threadPool.execute(new ClientHandler(channel, receiveBuffer));
			}
		}
		catch (Exception e)
		{
			System.out.println("Caught unexpected exception, shutting down...");
			e.printStackTrace(System.out);
		}
		finally
		{
			threadPool.shutdownNow();

			try
			{
				serverChannel.close();
			}
			catch (Exception e)
			{
			}
		}
	}

	private class ClientHandler implements Runnable
	{
		private SocketChannel channel;
		private ByteBuffer buffer;
		private String name;

		private ClientHandler(SocketChannel channel, ByteBuffer buffer)
		{
			this.channel = channel;
			this.buffer = buffer;
			InetSocketAddress remoteAddress = (InetSocketAddress) channel.socket().getRemoteSocketAddress();
			name = remoteAddress.getAddress().getHostAddress() + ":" + remoteAddress.getPort();
			output(name, "New connection from " + name);
		}

		public void run()
		{
			try
			{
				while (true)
				{
					buffer.clear();
					int bytesRead = channel.read(buffer);
					if (bytesRead <= 0)
						break;
					buffer.flip();

					if (delay > 0)
						Thread.sleep(delay);

					output(name, dumpBuffer(buffer, true));
					while(buffer.hasRemaining())
						channel.write(buffer);
				}
			}
			catch (Exception e)
			{
				output(name, "Caught exception, shutting down...");
				e.printStackTrace(System.out);
			}
			finally
			{
				try
				{
					channel.close();
				}
				catch (Exception e)
				{
				}
			}
		}
	}

	public static String dumpBuffer(ByteBuffer buffer, boolean allHex)
	{
		StringBuilder sb = new StringBuilder();
		buffer.mark();
		while (buffer.hasRemaining())
		{
			byte b = buffer.get();
			if (!allHex && Character.isLetterOrDigit((char) b))
				sb.append((char) b);
			else
				sb.append(byteToHex(b));
			//if (buffer.hasRemaining())
			//	sb.append(",");

		}
		buffer.reset();
		return sb.toString();
	}

	public static String bytesToHex(byte[] data, char sep)
	{
		int len = data.length;
		StringBuilder sb = new StringBuilder((len * 3));
		for (int i = 0; i < len; i++)
		{
			sb.append(byteToHex(data[i]));
			if (i < (len - 1))
				sb.append(sep);
		}

		return sb.toString();
	}

	public static String bytesToHex(byte[] data)
	{
		int len = data.length;
		StringBuilder sb = new StringBuilder((len * 3));
		for (int i = 0; i < len; i++)
		{
			sb.append(byteToHex(data[i]));
		}

		return sb.toString();
	}

	public static String byteToHex(byte b)
	{
		return Character.toString(HEX_CHARS[((b & 0xf0) >> 4)]) + Character.toString(HEX_CHARS[(b & 0xf)]);
	}

	private static void output(String name, String msg)
	{
		System.out.println(new java.util.Date() + " [" + name + "] " + msg);
	}
}
