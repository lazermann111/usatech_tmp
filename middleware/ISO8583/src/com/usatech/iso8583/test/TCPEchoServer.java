package com.usatech.iso8583.test;

import java.io.*;
import java.net.*;

public class TCPEchoServer
{
	public static void main(String[] args) throws Exception
	{

		// create server socket
		int port = Integer.parseInt(args[0]);
		ServerSocket serverSocket = new ServerSocket(port);
		System.out.println("Echo Server ready");

		while (true)
		{
			// accept connection
			Socket socket = serverSocket.accept();
			String client = socket.getInetAddress().getHostName() + ":" + socket.getPort();
			System.out.println("Connection by " + client);
			socket.setSoTimeout(100000);

			// start thread
			ServerThread thread = new ServerThread(socket);
			thread.start();
		}
	}
}

class ServerThread extends Thread
{
	private Socket socket;

	public ServerThread(Socket socket)
	{
		this.socket = socket;
	}

	public void run()
	{
		try
		{
			// create socket streams
			BufferedReader sockin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter sockout = new PrintWriter(socket.getOutputStream(), true);

			while (true)
			{
				// receive message
				String message = sockin.readLine();
				if (message == null)
					break;

				// send message back
				sockout.println(message);
			}
			socket.close();
			System.out.println("Connection closed");
		}
		catch (Exception e)
		{
			System.err.println(e);
		}
	}
}
