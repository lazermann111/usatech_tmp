package com.usatech.iso8583.test;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class TCPNIORelayServer
{
	private static final char[] HEX_CHARS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static void main(String args[]) throws Exception
	{
		new TCPNIORelayServer(Integer.parseInt(args[0]), args[1], Integer.parseInt(args[2]));
	}

	private ServerSocketChannel serverChannel;
	private ThreadPoolExecutor threadPool;

	public TCPNIORelayServer(int port, String remoteHost, int remotePort) throws Exception
	{
		threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		threadPool.setCorePoolSize(2);
		threadPool.prestartAllCoreThreads();

		InetSocketAddress remoteAddress = new InetSocketAddress(remoteHost, remotePort);
		String remoteName = remoteHost + ":" + remotePort;

		serverChannel = ServerSocketChannel.open();
		serverChannel.socket().bind(new InetSocketAddress(port));
		System.out.println("Ready to accept connections on port " + port);

		try
		{
			while (true)
			{
				SocketChannel clientChannel = serverChannel.accept();

				InetSocketAddress clientAddress = (InetSocketAddress) clientChannel.socket().getRemoteSocketAddress();
				String clientName = clientAddress.getAddress().getHostAddress() + ":" + clientAddress.getPort();

				output("INIT", "New connection from " + clientName);

				SocketChannel remoteChannel = SocketChannel.open(remoteAddress);
				output("INIT", "Connected to " + remoteName);

				output("INIT", "Starting relays");

				threadPool.execute(new Relay(clientChannel, remoteChannel, clientName + "->" + remoteName));
				threadPool.execute(new Relay(remoteChannel, clientChannel, remoteName + "->" + clientName));
			}
		}
		catch (Exception e)
		{
			System.out.println("Caught unexpected exception, shutting down...");
			e.printStackTrace(System.out);
		}
		finally
		{
			threadPool.shutdownNow();

			try
			{
				serverChannel.close();
			}
			catch (Exception e)
			{
			}
		}
	}

	private class Relay implements Runnable
	{
		private SocketChannel from;
		private SocketChannel to;
		private String name;

		private Relay(SocketChannel from, SocketChannel to, String name)
		{
			this.from = from;
			this.to = to;
			this.name = name;
		}

		public void run()
		{
			output(name, "Running...");

			try
			{
				while (true)
				{
					ByteBuffer buffer = ByteBuffer.allocate(5000);
					buffer.clear();
					int bytesRead = from.read(buffer);
					if (bytesRead <= 0)
					{
						output(name, "Read " + bytesRead + " bytes! open=" + from.isOpen());
						break;
					}
					buffer.flip();
					output(name, dumpBuffer(buffer, true));
					while(buffer.hasRemaining())
						to.write(buffer);
				}
			}
			catch (Exception e)
			{
				output(name, "Caught exception, shutting down...");
				e.printStackTrace(System.out);
			}
			finally
			{
				try
				{
					to.close();
				}
				catch (Exception e)
				{
				}

				try
				{
					from.close();
				}
				catch (Exception e)
				{
				}
			}
		}
	}

	public static String dumpBuffer(ByteBuffer buffer, boolean allHex)
	{
		StringBuilder sb = new StringBuilder();
		buffer.mark();
		while (buffer.hasRemaining())
		{
			byte b = buffer.get();
			if (!allHex && Character.isLetterOrDigit((char) b))
				sb.append((char) b);
			else
				sb.append(byteToHex(b));
			//if (buffer.hasRemaining())
			//	sb.append(",");

		}
		buffer.reset();
		return sb.toString();
	}

	public static String bytesToHex(byte[] data, char sep)
	{
		int len = data.length;
		StringBuilder sb = new StringBuilder((len * 3));
		for (int i = 0; i < len; i++)
		{
			sb.append(byteToHex(data[i]));
			if (i < (len - 1))
				sb.append(sep);
		}

		return sb.toString();
	}

	public static String bytesToHex(byte[] data)
	{
		int len = data.length;
		StringBuilder sb = new StringBuilder((len * 3));
		for (int i = 0; i < len; i++)
		{
			sb.append(byteToHex(data[i]));
		}

		return sb.toString();
	}

	public static String byteToHex(byte b)
	{
		return Character.toString(HEX_CHARS[((b & 0xf0) >> 4)]) + Character.toString(HEX_CHARS[(b & 0xf)]);
	}

	private static void output(String name, String msg)
	{
		System.out.println(new java.util.Date() + " [" + name + "] " + msg);
	}
}
