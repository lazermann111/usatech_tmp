package com.usatech.iso8583.test;

import com.usatech.iso8583.interchange.fhms.jpos.MerchantLinkFrameRelayChannel;
import com.usatech.iso8583.interchange.fhms.jpos.message.*;
import com.usatech.iso8583.interchange.fhms.jpos.packager.FHMSPackager;
import com.usatech.iso8583.interchange.fhms.tlv.*;

import org.jpos.iso.*;
import org.jpos.util.*;

/*
 import java.util.Arrays;

 import org.jpos.*;
 import org.jpos.iso.channel.*;
 import org.jpos.iso.packager.*;
 import org.jpos.util.*;

 import com.usatech.util.crypto.crc.CRC16;
 */

public class FHMSTest
{

	public static void main(String args[]) throws Exception
	{
		new FHMSTest();
	}

	public FHMSTest() throws Exception
	{
		FHMSPackager packager = new FHMSPackager();
		packager.setFieldPackager(60, new IFB_LLLBINARY(999, "RESERVED PRIVATE FIELD 60"));
		packager.setFieldPackager(62, new IFB_LLLBINARY(999, "RESERVED PRIVATE FIELD 62"));
		packager.setFieldPackager(63, new ISOIFB_LLLTLVBINARY(999, "ADDITIONAL DATA"));
		//packager.setFieldPackager(63, new IFB_LLLBINARY(999, "ADDITIONAL DATA"));

		FHMSISOMsg out = new FHMSISOMsg();
		out.setPackager(packager);
		out.setMTI("0320");
		out.setPrimaryAccountNumber("1343455");
		out.setProcessingCode("004000");
		out.setTransactionAmount("1000");
		out.setSystemTraceNumber("1234");
		out.setTransactionLocalTime("115556");
		out.setTransactionLocalDate("0524");
		out.setExpirationDate("0507");
		out.setPointOfServiceEntryMode("022");
		out.setPointOfServiceConditionCode("00");
		out.setTrack2Data("5490990148049999=12121234554323");
		out.setCardAcceptorTerminalID("521");
		out.setRetrievalReferenceNumber("456346");
		out.setAuthorizationIdentificationResponse("677880");
		out.setResponseCode("YY");
		out.setCardAcceptorTerminalID("436456");
		out.setCardAcceptorAcquirerID("0020030003");
		out.setTrack1Data("TEST TRACK 1");
		out.setPINData("3454".getBytes());
		out.setAdditionalAmounts("3464564647");

		Field60OriginalMessageData fld60Out = new Field60OriginalMessageData();
		fld60Out.setOriginalMessageType("0400");
		fld60Out.setOriginalSystemsTraceAuditNumber("123456");
		fld60Out.addToISOMsg(out);

		/*
		 Field60BatchNumber fld60Out = new Field60BatchNumber();
		 fld60Out.setBatchNumber("45567");
		 fld60Out.addToISOMsg(out);
		 
		 Field60OriginalAmount fld60Out = new Field60OriginalAmount();
		 fld60Out.setOriginalAmount("56734004");
		 fld60Out.addToISOMsg(out);
		 */

		Field62PurchaseOrderNumber fld62Out = new Field62PurchaseOrderNumber();
		fld62Out.setPurchaseOrderNumber("545690");
		fld62Out.addToISOMsg(out);

		/*
		 Field63ReconciliationRequestTotals fld63Out = new Field63ReconciliationRequestTotals(63);
		 fld63Out.setCapturedSalesCount("123");
		 fld63Out.setDebitRefundAmount("3445");
		 fld63Out.setRefundCount("546");
		 fld63Out.addToISOMsg(out);
		 */

		ISOTLVField fld63Out = new ISOTLVField(63);

		Field63PS2000TerminalGeneratedData21 data21Out = new Field63PS2000TerminalGeneratedData21();
		data21Out.setAuthorizedAmount("12345678");
		data21Out.addToTLV(fld63Out);

		Field63AddressVerificationData54 data54Out = new Field63AddressVerificationData54();
		data54Out.setZipCode("19000");
		data54Out.setAddress("1 Test Str");
		data54Out.addToTLV(fld63Out);

		Field63AlternateHostResponse22 data22Out = new Field63AlternateHostResponse22();
		data22Out.setResponseText("Test response");
		data22Out.addToTLV(fld63Out);

		Field63AVSResponse55 data55Out = new Field63AVSResponse55();
		data55Out.setAddressMatch("Y");
		data55Out.setZipCodeMatch("N");
		data55Out.setAddressResponseCode("A");
		data55Out.addToTLV(fld63Out);

		Field63BatchNumber37 data37Out = new Field63BatchNumber37();
		data37Out.setBatchNumber("111111");
		data37Out.addToTLV(fld63Out);

		Field63CashBackAmount41 data41Out = new Field63CashBackAmount41();
		data41Out.setCashBackAmount("4500");
		data41Out.addToTLV(fld63Out);

		Field63CVV2Data16 data16Out = new Field63CVV2Data16();
		data16Out.setCode("998899");
		data16Out.addToTLV(fld63Out);

		Field63DUKPTKeySerialNumber33 data33Out = new Field63DUKPTKeySerialNumber33();
		data33Out.setDUKPTKeySerialNumber("456789");
		data33Out.addToTLV(fld63Out);

		Field63EBTResponseData48 data48Out = new Field63EBTResponseData48();
		data48Out.setEBTCashBalance("123");
		data48Out.setEBTFoodBalance("456");
		data48Out.setCaseNumber("24");
		data48Out.addToTLV(fld63Out);

		Field63EBTTerminalData47 data47Out = new Field63EBTTerminalData47();
		data47Out.setClerkID("123");
		data47Out.setSupervisorID("235");
		data47Out.setVoucherNumber("4567");
		data47Out.setGenerationNumber("456789");
		data47Out.addToTLV(fld63Out);

		Field63PaymentServices200020 data20Out = new Field63PaymentServices200020();
		data20Out.setVisaResponseCode("45");
		data20Out.setPS2000Indicator("Y");
		data20Out.setTransactionIdentifier("987667");
		data20Out.addToTLV(fld63Out);

		Field63TaxAmount39 data39Out = new Field63TaxAmount39();
		data39Out.setTaxAmount("24334");
		data39Out.addToTLV(fld63Out);

		Field63TipAmount38 data38Out = new Field63TipAmount38();
		data38Out.setTipAmount("3242");
		data38Out.addToTLV(fld63Out);

		out.set(fld63Out);

		out.dump(System.out, "");

		fld60Out.dump(System.out, "");
		fld62Out.dump(System.out, "");
		//fld63Out.dump(System.out, "");
		System.out.println("Tag 21");
		data21Out.dump(System.out, "");
		System.out.println("Tag 54");
		data54Out.dump(System.out, "");
		System.out.println("Tag 22");
		data22Out.dump(System.out, "");
		System.out.println("Tag 55");
		data55Out.dump(System.out, "");
		System.out.println("Tag 37");
		data37Out.dump(System.out, "");
		System.out.println("Tag 41");
		data41Out.dump(System.out, "");
		System.out.println("Tag 16");
		data16Out.dump(System.out, "");
		System.out.println("Tag 33");
		data33Out.dump(System.out, "");
		System.out.println("Tag 48");
		data48Out.dump(System.out, "");
		System.out.println("Tag 47");
		data47Out.dump(System.out, "");
		System.out.println("Tag 20");
		data20Out.dump(System.out, "");
		System.out.println("Tag 39");
		data39Out.dump(System.out, "");
		System.out.println("Tag 38");
		data38Out.dump(System.out, "");

		byte[] packedMsg = out.pack();
		System.out.println("Packed outgoing message: " + ISOUtil.hexString(packedMsg));

		out.setSequenceNumber("1234");

		Logger logger = new Logger();
		logger.addListener(new SimpleLogListener(System.out));

		ISOChannel channel = new MerchantLinkFrameRelayChannel("127.0.0.1", 12345, packager);
		((LogSource) channel).setLogger(logger, "FHMSTest");

		channel.connect();
		channel.send(out);
		System.out.println("Testing channel receive():");
		ISOMsg inMsg = channel.receive();
		channel.disconnect();

		System.out.println("Sequence number: " + (String) inMsg.getValue(61));
		inMsg.unset(61);

		FHMSISOMsg in = new FHMSISOMsg(inMsg);
		System.out.println("Primary account number: " + (String) in.getPrimaryAccountNumber());

		System.out.println("Packed incoming message: " + ISOUtil.hexString(in.pack()));
		in.dump(System.out, "");

		/*
		 ISOMsg in = new ISOMsg();
		 in.setPackager(packager);
		 in.unpack(packedMsg);
		 in.dump(System.out, "");
		 */

		Field60OriginalMessageData fld60In = new Field60OriginalMessageData();
		//Field60BatchNumber fld60In = new Field60BatchNumber();
		//Field60OriginalAmount fld60In = new Field60OriginalAmount();
		fld60In.getFromISOMsg(in);
		fld60In.dump(System.out, "");

		Field62PurchaseOrderNumber fld62In = new Field62PurchaseOrderNumber();
		fld62In.getFromISOMsg(in);
		fld62In.dump(System.out, "");

		/*
		 Field63ReconciliationRequestTotals fld63In = new Field63ReconciliationRequestTotals(63);
		 fld63In.getFromISOMsg(in);
		 fld63In.dump(System.out, "");
		 */

		ISOTLVField fld63In = (ISOTLVField) in.getComponent(63);

		Field63PS2000TerminalGeneratedData21 data21In = new Field63PS2000TerminalGeneratedData21();
		data21In.getFromTLV(fld63In);
		System.out.println("Tag 21");
		data21In.dump(System.out, "");

		Field63AddressVerificationData54 data54In = new Field63AddressVerificationData54();
		data54In.getFromTLV(fld63In);
		System.out.println("Tag 54");
		data54In.dump(System.out, "");

		Field63AlternateHostResponse22 data22In = new Field63AlternateHostResponse22();
		data22In.getFromTLV(fld63In);
		System.out.println("Tag 22");
		data22In.dump(System.out, "");

		Field63AVSResponse55 data55In = new Field63AVSResponse55();
		data55In.getFromTLV(fld63In);
		System.out.println("Tag 55");
		data55In.dump(System.out, "");

		Field63BatchNumber37 data37In = new Field63BatchNumber37();
		data37In.getFromTLV(fld63In);
		System.out.println("Tag 37");
		data37In.dump(System.out, "");

		Field63CashBackAmount41 data41In = new Field63CashBackAmount41();
		data41In.getFromTLV(fld63In);
		System.out.println("Tag 41");
		data41In.dump(System.out, "");

		Field63CVV2Data16 data16In = new Field63CVV2Data16();
		data16In.getFromTLV(fld63In);
		System.out.println("Tag 16");
		data16In.dump(System.out, "");

		Field63DUKPTKeySerialNumber33 data33In = new Field63DUKPTKeySerialNumber33();
		data33In.getFromTLV(fld63In);
		System.out.println("Tag 33");
		data33In.dump(System.out, "");

		Field63EBTResponseData48 data48In = new Field63EBTResponseData48();
		data48In.getFromTLV(fld63In);
		System.out.println("Tag 48");
		data48In.dump(System.out, "");

		Field63EBTTerminalData47 data47In = new Field63EBTTerminalData47();
		data47In.getFromTLV(fld63In);
		System.out.println("Tag 47");
		data47In.dump(System.out, "");

		Field63PaymentServices200020 data20In = new Field63PaymentServices200020();
		data20In.getFromTLV(fld63In);
		System.out.println("Tag 20");
		data20In.dump(System.out, "");

		Field63TaxAmount39 data39In = new Field63TaxAmount39();
		data39In.getFromTLV(fld63In);
		System.out.println("Tag 39");
		data39In.dump(System.out, "");

		Field63TipAmount38 data38In = new Field63TipAmount38();
		data38In.getFromTLV(fld63In);
		System.out.println("Tag 38");
		data38In.dump(System.out, "");

		/*
		 FHMSPackager packager = new FHMSPackager();
		 packager.setFieldPackager(63, new ISOIFB_LLLTLVBINARY(999, "ADDITIONAL DATA"));
		 
		 byte[] aid1={(byte)0xA0,(byte)0x00,0x00,0x00,(byte)0x96,0x02,0x00};
		 byte[] aid2={(byte)0xA0,(byte)0x00,0x00,0x00,(byte)0x96,0x02,0x01};
		 byte[] aid3={(byte)0xA0,(byte)0x00,0x00,0x00,(byte)0x96,0x02,0x02};
		 byte[] atc={(byte)0x30,(byte)0x30,(byte)0x30,(byte)0x31,(byte)0x30,(byte)0x30,(byte)0x30,(byte)0x31,(byte)0x30,(byte)0x30,(byte)0x30,(byte)0x31,(byte)0x30,(byte)0x30,(byte)0x30,(byte)0x31};
		 ISOMsg m=new ISOMsg("0200");
		 ISOTLVField tb=new ISOTLVField(63);
		 //tb.addTLV(345,t100);
		 tb.addTLV(22,aid1);
		 tb.addTLV(22,aid2);
		 tb.addTLV(22,aid3);
		 tb.addTLV(17,atc);//9F70
		 m.set(tb);
		 m.setPackager(packager);
		 byte[] out=m.pack();
		 System.out.println("Packed ISOMsg: " + ISOUtil.hexString(out));

		 ISOMsg r=new ISOMsg();
		 r.setPackager(packager);
		 r.unpack(out);
		 ISOTLVField bt=(ISOTLVField)r.getComponent(63);
		 byte[] test1=bt.getFirstTLV(17);
		 byte[] test2=bt.getFirstTLV(22);
		 byte[] test3=bt.getNextTLV();
		 byte[] test4=bt.getNextTLV();
		 //byte[] test5=bt.getFirstTLV(345);
		 System.out.println("test1: " + ISOUtil.hexString(test1));
		 System.out.println("test2: " + ISOUtil.hexString(test2));
		 System.out.println("test3: " + ISOUtil.hexString(test3));
		 System.out.println("test4: " + ISOUtil.hexString(test4));
		 //System.out.println(ISOUtil.hexString(test5));
		 */
	}
}
