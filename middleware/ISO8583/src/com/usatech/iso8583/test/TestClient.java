package com.usatech.iso8583.test;

import java.io.*;
import java.net.Socket;

import com.usatech.util.Conversions;

public class TestClient
{
	public static void main(String args[]) throws Exception
	{
		String remoteAddress = args[0];
		int remotePort = Integer.parseInt(args[1]);
		String hexData = args[2];

		Socket socket = new Socket(remoteAddress, remotePort);
		InputStream is = new BufferedInputStream(socket.getInputStream());
		OutputStream os = new BufferedOutputStream(socket.getOutputStream());

		Thread.sleep(5000);

		byte[] sendBytes = Conversions.hexToByteArray(hexData);

		System.out.println("-> " + Conversions.bytesToHex(sendBytes) + " (" + new String(sendBytes) + ")");
		os.write(sendBytes);
		os.flush();

		while (!socket.isClosed())
		{
			int avail = is.available();
			System.out.println("avail=" + avail);
			if (avail > 0)
			{
				byte[] receiveBytes = new byte[avail];
				is.read(receiveBytes);
				System.out.println("<- " + Conversions.bytesToHex(receiveBytes) + " (" + new String(receiveBytes) + ")");
			}
			Thread.sleep(200);
		}

		/*
		 String out = Conversions.bytesToHex(receiveBytes);
		 System.out.println(out);
		 
		 if(len < 0)
		 System.out.println("End Of Stream!");
		 else
		 {
		 System.out.println("<- [" + len + "] " + Conversions.bytesToHex(receiveBytes));
		 }
		 
		 Thread.sleep(1000);
		 socket.close();
		 
		 */
	}
}
