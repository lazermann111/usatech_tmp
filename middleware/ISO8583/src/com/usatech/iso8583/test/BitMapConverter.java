package com.usatech.iso8583.test;

import java.util.HashMap;

import com.usatech.util.Conversions;

public class BitMapConverter
{
	private static final String padding = "0000000000000000000000000000000000000000000000000000000000000000";

	private static HashMap<Integer, String> dataElements = new HashMap<Integer, String>();

	static
	{
		dataElements.put(new Integer(1), "Bitmap Continuation Flag");
		dataElements.put(new Integer(2), "Primary Account Number");
		dataElements.put(new Integer(3), "Processing Code");
		dataElements.put(new Integer(4), "Transaction Amount");
		dataElements.put(new Integer(11), "System Trace Number");
		dataElements.put(new Integer(12), "Time, Local Transaction");
		dataElements.put(new Integer(13), "Date, Local Transaction");
		dataElements.put(new Integer(14), "Date, Expiration");
		dataElements.put(new Integer(22), "POS Entry Mode");
		dataElements.put(new Integer(24), "NII");
		dataElements.put(new Integer(25), "POS Condition Code");
		dataElements.put(new Integer(35), "Track II Data");
		dataElements.put(new Integer(37), "Retrieval Reference Number");
		dataElements.put(new Integer(38), "Approval Code");
		dataElements.put(new Integer(39), "Response Code");
		dataElements.put(new Integer(41), "Terminal ID");
		dataElements.put(new Integer(42), "Merchant ID");
		dataElements.put(new Integer(45), "Track I Data");
		dataElements.put(new Integer(52), "PIN");
		dataElements.put(new Integer(54), "Additional Amounts");
		dataElements.put(new Integer(60), "Private Use 60");
		dataElements.put(new Integer(62), "Private Use 62");
		dataElements.put(new Integer(63), "Private Use 63");
	}

	// 303801000E800000
	// 30       38       01       00       0E       80       00       00
	// 00110000 00111000 00000001 00000000 00001110 10000000 00000000 00000000

	public static void main(String args[])
	{
		StringBuilder bits = new StringBuilder();
		String hexString = args[0];
		byte[] bytes = Conversions.hexToByteArray(hexString);
		for (int i = 0; i < bytes.length; i++)
		{
			StringBuilder sb = new StringBuilder(Integer.toBinaryString(bytes[i]));
			if (sb.length() < 8)
				sb.insert(0, padding.substring(0, (8 - sb.length())));
			else if (sb.length() > 8)
				sb = new StringBuilder(sb.substring(sb.length() - 8, sb.length()));
			System.out.println(Conversions.byteToHex(bytes[i]) + "\t" + sb.toString());

			bits.append(sb);
		}

		System.out.println();
		System.out.println(bits);

		System.out.println();
		for (int i = 0; i < bits.length(); i++)
		{
			if (bits.charAt(i) == '1')
				System.out.println(i + 1 + "\t" + dataElements.get(new Integer(i + 1)));
		}
	}
}
