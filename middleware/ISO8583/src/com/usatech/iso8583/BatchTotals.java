package com.usatech.iso8583;

import java.io.Serializable;
import java.util.*;

public class BatchTotals implements Serializable
{
	protected int creditSaleCount;
	protected int creditSaleAmount;
	protected int creditRefundCount;
	protected int creditRefundAmount;

	protected transient String rendered;

	public BatchTotals()
	{
		this(0, 0, 0, 0);
	}

	public BatchTotals(int creditSaleCount, int creditSaleAmount, int creditRefundCount, int creditRefundAmount)
	{
		this.creditSaleCount = creditSaleCount;
		this.creditSaleAmount = creditSaleAmount;
		this.creditRefundCount = creditRefundCount;
		this.creditRefundAmount = creditRefundAmount;
	}

	public int getCreditSaleCount()
	{
		return creditSaleCount;
	}

	public void setCreditSaleCount(int creditSaleCount)
	{
		this.creditSaleCount = creditSaleCount;
	}

	public int getCreditSaleAmount()
	{
		return creditSaleAmount;
	}

	public void setCreditSaleAmount(int creditSaleAmount)
	{
		this.creditSaleAmount = creditSaleAmount;
	}

	public int getCreditRefundCount()
	{
		return creditRefundCount;
	}

	public void setCreditRefundCount(int creditRefundCount)
	{
		this.creditRefundCount = creditRefundCount;
	}

	public int getCreditRefundAmount()
	{
		return creditRefundAmount;
	}

	public void setCreditRefundAmount(int creditRefundAmount)
	{
		this.creditRefundAmount = creditRefundAmount;
	}

	public String toString()
	{
		if (rendered != null)
			return rendered;

		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName());
		sb.append("[");

		List<String> elements = new ArrayList<String>();
		renderElements(elements);
		for (String element : elements)
		{
			sb.append(element);
			sb.append(" ");
		}

		sb.append("]");
		rendered = sb.toString();
		return rendered;
	}

	public void renderElements(List<String> elements)
	{
		elements.add(creditSaleCount + ",");
		elements.add(creditSaleAmount + ",");
		elements.add(creditRefundCount + ",");
		elements.add(creditRefundAmount + "");
	}
}
