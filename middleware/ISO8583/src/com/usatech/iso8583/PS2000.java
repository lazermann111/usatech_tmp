package com.usatech.iso8583;

public class PS2000
{
	protected String indicator;
	protected String identifier;
	protected String validationCode;
	protected String responseCode;
	protected String entryMode;

	public PS2000()
	{
	}

	public PS2000(String indicator, String identifier, String validationCode, String responseCode, String entryMode)
	{
		this.indicator = indicator;
		this.identifier = identifier;
		this.validationCode = validationCode;
		this.responseCode = responseCode;
		this.entryMode = entryMode;
	}

	public String getIndicator()
	{
		return indicator;
	}

	public void setIndicator(String indicator)
	{
		this.indicator = indicator;
	}

	public String getIdentifier()
	{
		return identifier;
	}

	public void setIdentifier(String identifier)
	{
		this.identifier = identifier;
	}
	
	public String getValidationCode()
	{
		return validationCode;
	}

	public void setValidationCode(String validationCode)
	{
		this.validationCode = validationCode;
	}
	
	public String getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(String responseCode)
	{
		this.responseCode = responseCode;
	}
	
	public String getEntryMode()
	{
		return entryMode;
	}

	public void setEntryMode(String entryMode)
	{
		this.entryMode = entryMode;
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("PS2000[ind=");
		sb.append(indicator);
		sb.append(" ident=");
		sb.append(identifier);
		sb.append(" validCd=");
		sb.append(validationCode);
		sb.append(" respCd=");
		sb.append(responseCode);
		sb.append(" entryMd=");
		sb.append(entryMode);
		sb.append("]");
		return sb.toString();
	}
}
