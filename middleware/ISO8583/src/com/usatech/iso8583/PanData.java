package com.usatech.iso8583;

import com.usatech.iso8583.util.Helper;

public class PanData
{
	public static final int VISA 					= 1;
	public static final int MASTERCARD 				= 2;
	public static final int AMERICAN_EXPRESS 		= 3;
	public static final int DISCOVER 				= 4;
	public static final int UNKNOWN 				= 9;

	protected String pan;
	protected String expiration;

	public PanData()
	{
	}

	public PanData(String pan, String expiration)
	{
		setPan(pan);
		setExpiration(expiration);
	}

	public String getPan()
	{
		return pan;
	}

	public void setPan(String pan)
	{
		this.pan = pan;
		
		// some Amex Track1 cards have a space in between digits 4-5 and 10-11
		if(this.pan != null)
			this.pan = this.pan.replaceAll(" ", "");
	}

	public String getPanMasked()
	{
		return PanData.maskPAN(pan);
	}

	public String getExpiration()
	{
		return expiration;
	}

	public void setExpiration(String expiration)
	{
		this.expiration = expiration;
	}
	
	public String getExpirationMasked()
	{
		return Helper.maskString(expiration);
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("PanData[pan=");
		sb.append(getPanMasked());
		sb.append(" exp=");
		sb.append(getExpirationMasked());
		sb.append("]");
		return sb.toString();
	}
	
	public int getCardType()
	{
		if(pan == null)
			return UNKNOWN;
		
		return getCardType(pan);
	}
	
	public static int getCardType(String pan)
	{
		if (pan.startsWith("4"))
		{
			return VISA;
		}
		else if (pan.startsWith("5"))
		{
			return MASTERCARD;
		}
		else if (pan.startsWith("6011"))
		{
			return DISCOVER;
		}
		else if (pan.startsWith("34") || pan.startsWith("37"))
		{
			return AMERICAN_EXPRESS;
		}
		else
		{
			return UNKNOWN;
		}
	}
	
	public static String maskPAN(String pan)
	{
		if (pan == null)
			return null;

		StringBuilder sb = new StringBuilder();
		sb.append(pan);
		for (int i = 6; i < (sb.length() - 4); i++)
		{
			sb.setCharAt(i, '*');
		}
		return sb.toString();
	}
}
