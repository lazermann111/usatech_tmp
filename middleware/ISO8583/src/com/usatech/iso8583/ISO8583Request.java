package com.usatech.iso8583;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ISO8583Request extends ISO8583Message implements Serializable
{
	public ISO8583Request()
	{
		super();
	}

	public String getAcquirerID()
	{
		return (String) getField(FIELD_ACQUIRER_ID);
	}

	public void setAcquirerID(String acquirerID)
	{
		setField(FIELD_ACQUIRER_ID, acquirerID);
	}

	public boolean hasAcquirerID()
	{
		return hasField(FIELD_ACQUIRER_ID);
	}

	public int getAmount()
	{
		if (!hasAmount())
			return -1;

		return (Integer) getField(FIELD_AMOUNT);
	}

	public void setAmount(int amount)
	{
		setField(FIELD_AMOUNT, amount);
	}

	public boolean hasAmount()
	{
		return hasField(FIELD_AMOUNT);
	}

	public String getApprovalCode()
	{
		return (String) getField(FIELD_APPROVAL_CODE);
	}

	public void setApprovalCode(String approvalCode)
	{
		setField(FIELD_APPROVAL_CODE, approvalCode);
	}

	public boolean hasApprovalCode()
	{
		return hasField(FIELD_APPROVAL_CODE);
	}

	public String getAvsAddress()
	{
		return (String) getField(FIELD_AVS_ADDRESS);
	}

	public void setAvsAddress(String avsAddress)
	{
		setField(FIELD_AVS_ADDRESS, avsAddress);
	}

	public boolean hasAvsAddress()
	{
		return hasField(FIELD_AVS_ADDRESS);
	}

	public String getAvsZip()
	{
		return (String) getField(FIELD_AVS_ZIP);
	}

	public void setAvsZip(String avsZip)
	{
		setField(FIELD_AVS_ZIP, avsZip);
	}

	public boolean hasAvsZip()
	{
		return hasField(FIELD_AVS_ZIP);
	}

	public int getBatchNumber()
	{
		if (!hasBatchNumber())
			return -1;

		return (Integer) getField(FIELD_BATCH_NUMBER);
	}

	public void setBatchNumber(int batchNumber)
	{
		if (batchNumber >= 0)
			setField(FIELD_BATCH_NUMBER, batchNumber);
	}

	public boolean hasBatchNumber()
	{
		return hasField(FIELD_BATCH_NUMBER);
	}

	public BatchTotals getBatchTotals()
	{
		return (BatchTotals) getField(FIELD_BATCH_TOTALS);
	}

	public void setBatchTotals(BatchTotals batchTotals)
	{
		setField(FIELD_BATCH_TOTALS, batchTotals);
	}

	public boolean hasBatchTotals()
	{
		return hasField(FIELD_BATCH_TOTALS);
	}

	public String getCvv2()
	{
		return (String) getField(FIELD_CVV2);
	}

	public void setCvv2(String cvv2)
	{
		setField(FIELD_CVV2, cvv2);
	}

	public boolean hasCvv2()
	{
		return hasField(FIELD_CVV2);
	}

	public Date getEffectiveDate()
	{
		return (Date) getField(FIELD_EFFECTIVE_DATE);
	}

	public void setEffectiveDate(Date effectiveDate)
	{
		setField(FIELD_EFFECTIVE_DATE, effectiveDate);
	}

	public boolean hasEffectiveDate()
	{
		return hasField(FIELD_EFFECTIVE_DATE);
	}

	public String getEntryMode()
	{
		return (String) getField(FIELD_ENTRY_MODE);
	}

	public void setEntryMode(String entryMode)
	{
		setField(FIELD_ENTRY_MODE, entryMode);
	}

	public boolean hasEntryMode()
	{
		return hasField(FIELD_ENTRY_MODE);
	}

	public boolean isOnline()
	{
		if (!hasField(FIELD_ONLINE))
			return false;
		return (Boolean) getField(FIELD_ONLINE);
	}

	public boolean getOnline()
	{
		return isOnline();
	}

	public void setOnline(boolean online)
	{
		setField(FIELD_ONLINE, online);
	}

	public int getOriginalAmount()
	{
		if (!hasOriginalAmount())
			return -1;

		return (Integer) getField(FIELD_ORIGINAL_AMOUNT);
	}

	public void setOriginalAmount(int originalAmount)
	{
		setField(FIELD_ORIGINAL_AMOUNT, originalAmount);
	}

	public boolean hasOriginalAmount()
	{
		return hasField(FIELD_ORIGINAL_AMOUNT);
	}

	public int getOriginalTraceNumber()
	{
		if (!hasOriginalTraceNumber())
			return -1;

		return (Integer) getField(FIELD_ORIGINAL_TRACE_NUMBER);
	}

	public void setOriginalTraceNumber(int originalTraceNumber)
	{
		setField(FIELD_ORIGINAL_TRACE_NUMBER, originalTraceNumber);
	}

	public boolean hasOriginalTraceNumber()
	{
		return hasField(FIELD_ORIGINAL_TRACE_NUMBER);
	}

	public PanData getPanData()
	{
		return (PanData) getField(FIELD_PAN_DATA);
	}

	public void setPanData(PanData panData)
	{
		setField(FIELD_PAN_DATA, panData);
	}

	public boolean hasPanData()
	{
		return hasField(FIELD_PAN_DATA);
	}

	public String getPinEntryCapability()
	{
		return (String) getField(FIELD_PIN_ENTRY_CAPABILITY);
	}

	public void setPinEntryCapability(String pinEntryCapability)
	{
		setField(FIELD_PIN_ENTRY_CAPABILITY, pinEntryCapability);
	}

	public boolean hasPinEntryCapability()
	{
		return hasField(FIELD_PIN_ENTRY_CAPABILITY);
	}

	public String getPosEntryCapability() {
		return (String) getField(FIELD_POS_ENTRY_CAPABILITY);
	}

	public void setPosEntryCapability(String posEntryCapability) {
		setField(FIELD_POS_ENTRY_CAPABILITY, posEntryCapability);
	}

	public boolean hasPosEntryCapability() {
		return hasField(FIELD_POS_ENTRY_CAPABILITY);
	}

	public String getPosEnvironment()
	{
		return (String) getField(FIELD_POS_ENVIRONMENT);
	}

	public void setPosEnvironment(String posEnvironment)
	{
		setField(FIELD_POS_ENVIRONMENT, posEnvironment);
	}

	public boolean hasPosEnvironment()
	{
		return hasField(FIELD_POS_ENVIRONMENT);
	}

	public String getRetrievalReferenceNumber()
	{
		return (String) getField(FIELD_RETRIEVAL_REFERENCE_NUMBER);
	}

	public void setRetrievalReferenceNumber(String retrievalReferenceNumber)
	{
		setField(FIELD_RETRIEVAL_REFERENCE_NUMBER, retrievalReferenceNumber);
	}

	public boolean hasRetrievalReferenceNumber()
	{
		return hasField(FIELD_RETRIEVAL_REFERENCE_NUMBER);
	}

	public String getTerminalID()
	{
		return (String) getField(FIELD_TERMINAL_ID);
	}

	public void setTerminalID(String terminalID)
	{
		setField(FIELD_TERMINAL_ID, terminalID);
	}

	public boolean hasTerminalID()
	{
		return hasField(FIELD_TERMINAL_ID);
	}

	public int getTraceNumber()
	{
		if (!hasTraceNumber())
			return -1;

		return (Integer) getField(FIELD_TRACE_NUMBER);
	}

	public void setTraceNumber(int traceNumber)
	{
		setField(FIELD_TRACE_NUMBER, traceNumber);
	}

	public boolean hasTraceNumber()
	{
		return hasField(FIELD_TRACE_NUMBER);
	}

	public TrackData getTrackData()
	{
		return (TrackData) getField(FIELD_TRACK_DATA);
	}

	public void setTrackData(TrackData trackData)
	{
		setField(FIELD_TRACK_DATA, trackData);
	}

	public boolean hasTrackData()
	{
		return hasField(FIELD_TRACK_DATA);
	}

	public List getTransactionList()
	{
		return (List) getField(FIELD_TRANSACTION_LIST);
	}

	public void setTransactionList(List transactionList)
	{
		setField(FIELD_TRANSACTION_LIST, transactionList);
	}

	public boolean hasTransactionList()
	{
		return hasField(FIELD_TRANSACTION_LIST);
	}
	
	public int getHostResponseTimeout()
	{
		if (!hasHostResponseTimeout())
			return -1;

		return (Integer) getField(FIELD_HOST_RESPONSE_TIMEOUT);
	}

	public void setHostResponseTimeout(int hostResponseTimeout)
	{
		setField(FIELD_HOST_RESPONSE_TIMEOUT, hostResponseTimeout);
	}

	public boolean hasHostResponseTimeout()
	{
		return hasField(FIELD_HOST_RESPONSE_TIMEOUT);
	}
	
	public Date getGatewayTimestamp()
	{
		return (Date) getField(FIELD_GATEWAY_TIMESTAMP);
	}

	public void setGatewayTimestamp(Date gatewayTimestamp)
	{
		setField(FIELD_GATEWAY_TIMESTAMP, gatewayTimestamp);
	}	

	public boolean isPartialAuthAllowed() {
		if(!hasField(FIELD_PARTIAL_AUTH_ALLOWED))
			return false;
		return (Boolean) getField(FIELD_PARTIAL_AUTH_ALLOWED);
	}

	public boolean getPartialAuthAllowed() {
		return isPartialAuthAllowed();
	}

	public void setPartialAuthAllowed(boolean partialAuthAllowed) {
		setField(FIELD_PARTIAL_AUTH_ALLOWED, partialAuthAllowed);
	}

	public String getPosIdentifier() {
		return (String) getField(FIELD_POS_IDENTIFIER);
	}

	public void setPosIdentifier(String posIdentifier) {
		setField(FIELD_POS_IDENTIFIER, posIdentifier);
	}

	public boolean hasPosIdentifier() {
		return hasField(FIELD_POS_IDENTIFIER);
	}

	public String getInvoiceNumber() {
		return (String) getField(FIELD_INVOICE_NUMBER);
	}

	public void setInvoiceNumber(String invoiceNumber) {
		setField(FIELD_INVOICE_NUMBER, invoiceNumber);
	}

	public boolean hasInvoiceNumber() {
		return hasField(FIELD_INVOICE_NUMBER);
	}
}
