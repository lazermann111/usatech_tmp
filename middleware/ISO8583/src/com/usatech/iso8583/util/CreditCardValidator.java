package com.usatech.iso8583.util;

import com.acmetech.cc.CreditCardInfo;
import com.usatech.iso8583.*;

public class CreditCardValidator
{
	public static void validateTrackData(TrackData trackData) throws NumberFormatException
	{
		validatePanData(trackData.extractPANData());
		
		if(trackData.hasTrack1() && trackData.getTrack1().length() > 76)
			throw new NumberFormatException("Invalid Track1 length: " + trackData.getTrack1().length() + " > 76");

		if(trackData.hasTrack2() && trackData.getTrack2().length() > 37)
			throw new NumberFormatException("Invalid Track2 length: " + trackData.getTrack2().length() + " > 37");

		// do other checks here on track format
	}

	public static void validatePanData(PanData panData) throws NumberFormatException
	{
		String pan = panData.getPan();
		String exp = panData.getExpiration();
		
		if (pan.length() < 13 || pan.length() > 19)
			throw new NumberFormatException("Invalid PAN length: " + pan.length());

		char[] panChars = pan.toCharArray();
		for (int i = 0; i < panChars.length; i++)
		{
			if (!Character.isDigit(panChars[i]))
				throw new NumberFormatException("Found non-numeric character in PAN at position " + (i + 1));
		}

		if (!CreditCardInfo.validateChecksum(pan))
			throw new NumberFormatException("PAN failed MOD 10 check");

		if (exp.length() != 4)
			throw new NumberFormatException("Invalid expiration length: " + exp.length() + " != 4");

		char[] expChars = exp.toCharArray();
		for (int i = 0; i < expChars.length; i++)
		{
			if (!Character.isDigit(expChars[i]))
				throw new NumberFormatException("Found non-numeric character in Expiration at position " + (i + 1));
		}

		int year = Integer.parseInt(exp.substring(0, 2));
		int month = Integer.parseInt(exp.substring(2));

		if (year < 0 || year > 99)
			throw new NumberFormatException("Invalid expiration year");

		if (month < 1 || month > 12)
			throw new NumberFormatException("Invalid expiration month");
	}

	/*
	 private boolean validate(String number, int type) 
	 {
	 if (Integer.parseInt(number.substring(0, 2)) < 51 || Integer.parseInt(number.substring(0, 2)) > 55)
	 {
	 // mastercard
	 if(number.length() != 16)
	 return false;
	 }
	 
	 case VISA:
	 if ((number.length() != 13 && number.length() != 16) ||
	 Integer.parseInt(number.substring(0, 1)) != 4)
	 {
	 return false;
	 }
	 break;
	 
	 case AMEX:
	 if (number.length() != 15 ||
	 (Integer.parseInt(number.substring(0, 2)) != 34 &&
	 Integer.parseInt(number.substring(0, 2)) != 37))
	 {
	 return false;
	 }
	 break;
	 
	 case DISCOVER:
	 if (number.length() != 16 ||
	 Integer.parseInt(number.substring(0, 5)) != 6011)
	 {
	 return false;
	 }
	 break;
	 
	 case DINERS:
	 if (number.length() != 14 ||
	 ((Integer.parseInt(number.substring(0, 2)) != 36 &&
	 Integer.parseInt(number.substring(0, 2)) != 38) &&
	 Integer.parseInt(number.substring(0, 3)) < 300 ||
	 Integer.parseInt(number.substring(0, 3)) > 305))
	 {
	 return false;
	 }
	 break;
	 }
	 return luhnValidate(number);
	 }
	 
	 private boolean luhnValidate(String numberString) 
	 {
	 char[] charArray = numberString.toCharArray();
	 int[] number = new int[charArray.length];
	 int total = 0;
	 
	 for (int i=0; i < charArray.length; i++) 
	 {
	 number[i] = Character.getNumericValue(charArray[i]);
	 }
	 
	 for (int i = number.length-2; i > -1; i-=2) 
	 {
	 number[i] *= 2;
	 
	 if (number[i] > 9)
	 number[i] -= 9;
	 }
	 
	 for (int i=0; i < number.length; i++)
	 total += number[i];
	 
	 if (total % 10 != 0)
	 return false;
	 
	 return true;
	 }
	 */
}
