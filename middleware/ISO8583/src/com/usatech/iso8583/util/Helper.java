package com.usatech.iso8583.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {
	protected static Pattern cardNumberPattern = Pattern.compile("([0-9]{13,})");
	
	public static boolean isInteger(String str)   
	{   
	    try {
	    	Integer.parseInt(str);   
	    } catch(Exception e) {   
	    	return false;
	    }   
	    return true;   
	}
	
	public static boolean isDouble(String str)   
	{   
	    try {
	    	Double.parseDouble(str);   
	    } catch(Exception e) {   
	    	return false;
	    }   
	    return true;   
	}
	
    public static boolean isBlank(String str)
    {
        return str == null || str.trim().length() == 0;
    }
    
    public static String getCardNumber(String cardData) {
    	Matcher cardDataMatcher = cardNumberPattern.matcher(cardData);
    	if (cardDataMatcher.find())
    		return cardDataMatcher.group(1);
    	else
    		return cardData;
    }
	
	public static String maskString(String s)
	{
		if (s == null || s.length() == 0)
			return "";
		return s.length() + " bytes";
	}    
}
