package com.usatech.iso8583.util;

import java.util.concurrent.*;

import org.apache.commons.logging.*;
import org.picocontainer.Startable;

public class ISO8583Executor extends ThreadPoolExecutor implements Startable
{
	private static Log log = LogFactory.getLog(ISO8583Executor.class);

	public ISO8583Executor()
	{
		super(5, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());
	}

	public void start()
	{

	}

	public void stop()
	{
		super.shutdownNow();
	}
}
