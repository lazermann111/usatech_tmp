package com.usatech.iso8583.util;

import com.usatech.util.Conversions;

public class HexConverter
{
	public static void main(String args[])
	{
		System.out.println(new String(Conversions.hexToByteArray(args[0])));
	}
}
