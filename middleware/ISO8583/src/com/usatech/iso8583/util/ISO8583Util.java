package com.usatech.iso8583.util;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;

import org.nanocontainer.script.xml.XMLContainerBuilder;
import org.picocontainer.PicoContainer;
import org.picocontainer.defaults.ObjectReference;
import org.picocontainer.defaults.SimpleReference;

import com.thoughtworks.xstream.XStream;
import com.usatech.iso8583.gateway.ISO8583Gateway;

public class ISO8583Util
{
	public static final String LB = System.getProperty("line.separator");
	public static NumberFormat FOUR_DIGIT_PADDER = new DecimalFormat("0000");

	private static transient XStream xStream = new XStream();

	public static String toXML(Object o)
	{
		return xStream.toXML(o);
	}

	public static Object fromXML(String xml)
	{
		return xStream.fromXML(xml);
	}

	public static PicoContainer buildContainerFromResource(PicoContainer parentContainer, Object scope, String resourceName,
			Class clazz) throws Exception
	{
		InputStream stream = clazz.getResourceAsStream(resourceName);
		if (stream == null)
			throw new IOException("Unable to find or open resource " + resourceName);

		Reader reader = new InputStreamReader(stream);
		XMLContainerBuilder builder = new XMLContainerBuilder(reader, clazz.getClassLoader());
		ObjectReference containerRef = new SimpleReference();
		ObjectReference parentContainerRef = new SimpleReference();
		parentContainerRef.set(parentContainer);
		builder.buildContainer(containerRef, parentContainerRef, scope, true);
		return (PicoContainer) containerRef.get();
	}

	public static PicoContainer buildContainerFromFile(PicoContainer parentContainer, Object scope, String fileName) throws Exception
	{
		Reader reader = new FileReader(fileName);
		XMLContainerBuilder builder = new XMLContainerBuilder(reader, ISO8583Gateway.class.getClassLoader());
		ObjectReference containerRef = new SimpleReference();
		ObjectReference parentContainerRef = new SimpleReference();
		parentContainerRef.set(parentContainer);
		builder.buildContainer(containerRef, parentContainerRef, scope, true);
		return (PicoContainer) containerRef.get();
	}

	public static String rightJustify(String s, int length, char padding) {
		if(s == null) {
			char[] chars = new char[length];
			Arrays.fill(chars, 0, length, padding);
			return new String(chars);
		}
		if(s.length() >= length)
			return s.substring(s.length() - length, s.length());
		char[] chars = new char[length];
		s.getChars(0, s.length(), chars, length - s.length());
		Arrays.fill(chars, 0, length - s.length(), padding);
		return new String(chars);
	}

	public static String leftJustify(String s, int length, char padding) {
		if(s == null) {
			char[] chars = new char[length];
			Arrays.fill(chars, 0, length, padding);
			return new String(chars);
		}
		if(s.length() >= length)
			return s.substring(0, length);
		char[] chars = new char[length];
		s.getChars(0, s.length(), chars, 0);
		Arrays.fill(chars, s.length(), length, padding);
		return new String(chars);
	}
}
