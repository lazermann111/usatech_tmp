package com.usatech.iso8583.util;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ISO8583GatewayConfiguration extends PropertiesConfiguration
{
	private static Log log = LogFactory.getLog(ISO8583GatewayConfiguration.class);

	public ISO8583GatewayConfiguration() throws ConfigurationException
	{
		super(getConfigFile("/iso8583gateway.properties"));
		log.info("Loaded configuration from " + getURL());
		setThrowExceptionOnMissing(true);
	}

	protected static java.net.URL getConfigFile(String file) {
		java.net.URL url = ISO8583GatewayConfiguration.class.getResource(file);
		if(url == null) {
			ClassLoader classLoader = ISO8583GatewayConfiguration.class.getClassLoader();
			StringBuilder sb = new StringBuilder("Could not find resource '" + file + "' in classpath: \n");
			sb.append(classLoader.getClass().getName());
	    	if(classLoader instanceof URLClassLoader) {
	    		sb.append(" with urls=");
	    		URL[] urls = ((URLClassLoader)classLoader).getURLs();
	    		sb.append(Arrays.toString(urls));
	    	}
			log.debug(sb.toString());
		}
		return url;
	}
}
