package com.usatech.iso8583.jpos;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequest;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.MUX;
import org.jpos.util.NameRegistrar;

import simple.io.Log;

import com.usatech.iso8583.util.ISO8583Util;

public class USATISOMUX implements Runnable, USATISOSource, MUX
{
	private static Log log = Log.getLog();

	public static final int CONNECT = 0;
	public static final int TX = 1;
	public static final int RX = 2;
	public static final int TX_EXPIRED = 3;
	public static final int RX_EXPIRED = 4;
	public static final int TX_PENDING = 5;
	public static final int RX_PENDING = 6;
	public static final int RX_UNKNOWN = 7;
	public static final int RX_FORWARDED = 8;
	public static final int SIZEOF_CNT = 9;

	private ThreadPoolExecutor threadPool;

	private USATISOChannel channel;
	private Receiver rx;
	private List txQueue;
	private Map<String, ISORequest> rxQueue;
	private int traceNumberField = 11;
	private volatile boolean terminate = false;
	private boolean doConnect;
	private long reconnectDelay = 5000;
	private long lastConnectAttempt = -1;
	private boolean isRunning = false;

	private String name;
	private int[] cnt;

	private ISORequestListener requestListener;

	/**
	 * @param c a connected or unconnected ISOChannel
	 */
	public USATISOMUX(USATISOChannel channel, ThreadPoolExecutor threadPool, boolean doConnect)
	{
		this.channel = channel;
		this.threadPool = threadPool;
		this.doConnect = doConnect;
		txQueue = Collections.synchronizedList(new ArrayList());
		rxQueue = Collections.synchronizedMap(new HashMap<String, ISORequest>());
		cnt = new int[SIZEOF_CNT];
		rx = new Receiver();

		name = "";

		threadPool.execute(this);
	}

	/**
	 * allow changes to default value 11 (used in ANSI X9.2 messages)
	 * @param traceNumberField new traceNumberField
	 */
	public void setTraceNumberField(int traceNumberField)
	{
		if (traceNumberField > 0)
			this.traceNumberField = traceNumberField;
	}

	/**
	 * @return the underlying ISOChannel
	 */
	public USATISOChannel getISOChannel()
	{
		return channel;
	}

	/**
	 * set an ISORequestListener for unmatched messages
	 * @param rl a request listener object
	 * @see ISORequestListener
	 */
	public void setISORequestListener(ISORequestListener rl)
	{
		requestListener = rl;
	}

	/**
	 * remove possible ISORequestListener 
	 * @see ISORequestListener
	 */
	public void removeISORequestListener()
	{
		requestListener = null;
	}

	/**
	 * construct key to match request with responses
	 * @param   m   request/response
	 * @return      key (default terminal(41) + tracenumber(11))
	 */
	protected String getKey(ISOMsg m) throws ISOException
	{
		return (m.hasField(41) ? ISOUtil.zeropad((String) m.getValue(41), 16) : "") + (m.hasField(traceNumberField) ? ISOUtil.zeropad((String) m.getValue(traceNumberField), 6) : Long.toString(System.currentTimeMillis()));
	}

	/**
	 * get rid of expired requests
	 */
	private void purgeRxQueue()
	{
		synchronized(rxQueue) {
			for(Iterator<Map.Entry<String, ISORequest>> iter = rxQueue.entrySet().iterator(); iter.hasNext();) {
				Map.Entry<String, ISORequest> entry = iter.next();
				ISORequest isoRequest = entry.getValue();
				if(isoRequest != null && isoRequest.isExpired()) {
					iter.remove();
					cnt[RX_EXPIRED]++;
				}
			}
		}
	}

	/**
	 * show Counters
	 * @param p - where to print
	 */
	public String getCounters()
	{
		cnt[TX_PENDING] = txQueue.size();
		cnt[RX_PENDING] = rxQueue.size();

		StringBuilder sb = new StringBuilder();

		sb.append("           Connections: " + cnt[CONNECT] + ISO8583Util.LB);
		sb.append("           TX messages: " + cnt[TX] + ISO8583Util.LB);
		sb.append("            TX expired: " + cnt[TX_EXPIRED] + ISO8583Util.LB);
		sb.append("            TX pending: " + cnt[TX_PENDING] + ISO8583Util.LB);
		sb.append("           RX messages: " + cnt[RX] + ISO8583Util.LB);
		sb.append("            RX expired: " + cnt[RX_EXPIRED] + ISO8583Util.LB);
		sb.append("            RX pending: " + cnt[RX_PENDING] + ISO8583Util.LB);
		sb.append("          RX unmatched: " + cnt[RX_UNKNOWN] + ISO8583Util.LB);
		sb.append("          RX forwarded: " + cnt[RX_FORWARDED] + ISO8583Util.LB);

		return sb.toString();
	}

	public void resetCounters()
	{
		cnt = new int[SIZEOF_CNT];
	}

	/**
	 * @return number of re-connections on the underlying channel
	 */
	public int getConnectionCount()
	{
		return cnt[CONNECT];
	}

	/**
	 * @return number of transmitted messages
	 */
	public int getTransmitCount()
	{
		return cnt[TX];
	}

	/**
	 * @return number of expired messages
	 */
	public int getExpiredCount()
	{
		return cnt[TX_EXPIRED];
	}

	/**
	 * @return number of messages waiting to be transmited
	 */
	public int getTransmitPendingCount()
	{
		return txQueue.size();
	}

	/**
	 * @return number of received messages
	 */
	public int getReceiveCount()
	{
		return cnt[RX];
	}

	/**
	 * @return number of unanswered messages
	 */
	public int getReceiveExpiredCount()
	{
		return cnt[RX_EXPIRED];
	}

	/**
	 * @return number of messages waiting for response
	 */
	public int getReceivePendingCount()
	{
		return rxQueue.size();
	}

	/**
	 * @return number of unknown messages received
	 */
	public int getUnknownCount()
	{
		return cnt[RX_UNKNOWN];
	}

	/**
	 * @return number of forwarded messages received
	 */
	public int getForwardedCount()
	{
		return cnt[RX_FORWARDED];
	}
	
	public void setReconnectDelay(long reconnectDelay)
	{
		this.reconnectDelay = reconnectDelay;
	}
	
	public long getReconnectDelay()
	{
		return reconnectDelay;
	}

	private class Receiver implements Runnable
	{
		private boolean isRunning = false;

		protected Receiver()
		{

		}

		public boolean isRunning()
		{
			return isRunning;
		}

		public void run()
		{
			log.debug("MUX receiver thread running");
			isRunning = true;

			while (!terminate || !rxQueue.isEmpty() || !txQueue.isEmpty())
			{
				if (channel.isConnected())
				{
					try
					{
						ISOMsg d = channel.receive();
						cnt[RX]++;
						String k = getKey(d);
						ISORequest r = rxQueue.get(k);
						boolean forward = true;
						if (r != null)
						{
							rxQueue.remove(k);
							synchronized (r)
							{
								if (r.isExpired())
								{
									if ((++cnt[RX_EXPIRED]) % 10 == 0)
										purgeRxQueue();
								}
								else
								{
									r.setResponse(d);
									forward = false;
								}
							}
						}
						if (forward)
						{
							if (requestListener != null)
							{
								requestListener.process(USATISOMUX.this, d);
								cnt[RX_FORWARDED]++;
							}
							else
								cnt[RX_UNKNOWN]++;
						}
					}
					catch (ISOException e)
					{
						if (!terminate)
							log.warn("MUX caught ISOException during receive: {0}", e.getMessage(), e);
						
						synchronized (USATISOMUX.this)
						{
							USATISOMUX.this.notify();
						}
					}
					catch (Throwable e)
					{
						if (!terminate)
						{
							if (e instanceof EOFException)
								log.warn("Detected closed channel: {0}", e.toString());
							else
								log.warn("MUX caught unexpected excepting during receive: {0}", e.getMessage(), e);
							
							try
							{
								channel.disconnect();
							}
							catch(Throwable e2)
							{
								log.error("Disonnect failed: {0}", e2.getMessage(), e2);
							}
							
							synchronized (USATISOMUX.this)
							{
								USATISOMUX.this.notify();
							}
						}
					}
				}
				else
				{
					try
					{
						synchronized (rx)
						{
							rx.wait();
						}
					}
					catch (InterruptedException e)
					{
						log.warn("MUX receiver interrupted!", e);
					}
				}
			}

			log.debug("MUX receiver thread exiting");
			isRunning = false;
		}
	}

	private void doTransmit() throws ISOException, IOException
	{
		while (txQueue.size() > 0 && !terminate)
		{
			Object o = txQueue.get(0);
			ISOMsg m = null;

			if (o instanceof ISORequest)
			{
				ISORequest r = (ISORequest) o;
				if (r.isExpired())
					cnt[TX_EXPIRED]++;
				else
				{
					m = r.getRequest();
					rxQueue.put(getKey(m), r);
					r.setTransmitted();
					synchronized (rx)
					{
						rx.notify(); // required by ChannelPool
					}
				}
			}
			else if (o instanceof ISOMsg)
			{
				m = (ISOMsg) o;
			}

			if (m != null)
			{
				try
				{
					channel.send(m);
					cnt[TX]++;
				}
				catch (ISOException e)
				{
					log.warn("MUX caught excepting during send: {0}", e.getMessage(), e);
					if(o instanceof ISORequest)
						((ISORequest) o).setResponse(null);
				}
			}
			txQueue.remove(o);
		}
	}

	public void run()
	{
		log.debug("MUX transmitter thread running");
		isRunning = true;

		threadPool.execute(rx);

		boolean firstTime = true;
		while (!terminate || !txQueue.isEmpty())
		{
			try
			{
				if (channel.isConnected())
				{
					doTransmit();
				}
				else if (doConnect || !txQueue.isEmpty())
				{
					if (firstTime)
					{
						firstTime = false;
						lastConnectAttempt = System.currentTimeMillis();
						channel.connect();
					}
					else
					{
						// this keeps us from rapidly reconnecting to the host
						long lastConnectET = (System.currentTimeMillis() - lastConnectAttempt);
						if(lastConnectET < reconnectDelay)
						{
							long delay = (reconnectDelay-lastConnectET);
							log.debug("Waiting {0} ms to reconnect", delay);
							try
							{
								Thread.sleep(delay);
							}
							catch(Exception e) {}
						}
						lastConnectAttempt = System.currentTimeMillis();
						channel.reconnect();
					}
					cnt[CONNECT]++;
					synchronized (rx)
					{
						rx.notify();
					}
				}
				else
				{
					// nothing to do ...
					/*
					try
					{
						Thread.sleep(1000);
					}
					catch (InterruptedException ex)
					{
					}
					*/
					
					synchronized (this)
					{
						this.wait();
					}
				}
				synchronized (this)
				{
					if (!terminate && channel.isConnected() && txQueue.isEmpty())
					{
						this.wait();
					}
				}
			}
			catch (Exception e)
			{
				log.warn("MUX transmitter caught exception: {0}", e.getMessage(), e);
				try
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException ex)
				{
				}
			}
		}

		// Wait for the receive queue to empty out before shutting down
		while (!rxQueue.isEmpty())
		{
			try
			{
				Thread.sleep(5000); // Wait for the receive queue to clear.
				purgeRxQueue(); // get rid of expired stuff
			}
			catch (InterruptedException e)
			{
				break;
			}
		}

		// By closing the channel, we force the receive thread to terminate
		try
		{
			channel.disconnect();
		}
		catch (IOException e)
		{
		}

		synchronized (rx)
		{
			rx.notify();
		}

		log.debug("MUX transmitter thread exiting");
		isRunning = false;
	}

	public boolean isTransmitterRunning()
	{
		return isRunning;
	}
	
	public boolean isReceiverRunning()
	{
		return rx != null && rx.isRunning();
	}

	/**
	 * queue an ISORequest
	 */
	synchronized public void queue(ISORequest r)
	{
		txQueue.add(r);
		this.notify();
	}

	/**
	 * send a message over channel, usually a
	 * response from an ISORequestListener
	 */
	synchronized public void send(ISOMsg m)
	{
		txQueue.add(m);
		this.notify();
	}

	public synchronized void terminate()
	{
		log.warn("USATISOMUX Shutting down...");

		terminate = true;
		txQueue.clear();
		rxQueue.clear();
		this.notify();
		
		// tx thread will notify rx thread and disconnect when the loops exits
	}

	public boolean isConnected()
	{
		if (channel == null)
			return false;

		return channel.isConnected();
	}

	public long getConnectTime()
	{
		if (!isConnected())
			return -1;

		return channel.getConnectTime();
	}

	public boolean isTerminating()
	{
		return terminate;
	}

	public boolean isTerminated()
	{
		return !isReceiverRunning() && !isTransmitterRunning();
	}

	/**
	 * associates this ISOMUX with a name using NameRegistrar
	 * @param name name to register
	 * @see NameRegistrar
	 */
	public void setName(String name)
	{
		this.name = name;
		NameRegistrar.register("mux." + name, this);
	}

	/**
	 * @return this ISOMUX's name ("" if no name was set)
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * @return connect flag value
	 */
	public boolean getConnect()
	{
		return doConnect;
	}

	public void connect() {
		if(doConnect)
			return;
		synchronized(this) {
			doConnect = true;
			notify();
		}
	}
	public ISOMsg request(ISOMsg m, long timeout) throws ISOException
	{
		ISORequest req = new ISORequest(m);
		queue(req);
		return req.getResponse((int) timeout);
	}
}
