package com.usatech.iso8583.jpos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.util.NameRegistrar;

public class USATChannelPool implements USATISOChannel
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(USATChannelPool.class);

	protected boolean usable = true;
	protected List<USATISOChannel> pool;
	protected USATISOChannel current;
	protected String name;

	public USATChannelPool()
	{
		pool = Collections.synchronizedList(new ArrayList<USATISOChannel>());
	}

	public void setPackager(ISOPackager p)
	{
		// nothing to do
	}

	public synchronized void connect() throws IOException
	{
		if (pool.size() < 1)
			return;

		if (current != null && current.isConnected())
		{
			log.debug("Skipping connect: pool is already connected on ISOChannel: " + current);
			return;
		}

		for (USATISOChannel channel : pool)
		{
			try
			{
				channel.connect();
				if (channel.isConnected())
				{
					current = channel;
					usable = true;
					break;
				}
			}
			catch (IOException e)
			{
				log.debug("Pool failed to connect on ISOChannel: " + channel + ": " + e.getMessage());
			}
		}

		if (current == null)
			throw new IOException("Pool failed to connect on all ISOChannels!");
	}

	public synchronized void disconnect() throws IOException
	{
		current = null;
		usable = false;

		for (USATISOChannel channel : pool)
		{
			try
			{
				if(channel.isConnected())
					channel.disconnect();
			}
			catch (IOException e)
			{
				log.debug("Pool failed to disconnect ISOChannel: " + channel + ": " + e.getMessage());
			}
		}
	}

	public synchronized void reconnect() throws IOException
	{
		disconnect();
		connect();
	}

	public boolean isConnected()
	{
		if (usable && current != null && current.isConnected())
			return true;

		return false;
	}

	public synchronized long getConnectTime()
	{
		if(current == null)
			return -1;
		return current.getConnectTime();
	}

	public long getLastSend()
	{
		if(current == null)
			return -1;
		return current.getLastSend();
	}

	public long getLastReceive()
	{
		if(current == null)
			return -1;
		return current.getLastReceive();
	}

	public ISOMsg receive() throws IOException, ISOException
	{
		return getCurrent().receive();
	}

	public void send(ISOMsg m) throws IOException, ISOException
	{
		getCurrent().send(m);
	}

	public void setUsable(boolean usable)
	{
		this.usable = usable;
	}
	
	public ISOPackager getPackager()
	{
		return null;
	}

	public void addChannel(USATISOChannel channel)
	{
		pool.add(channel);
	}

	public void removeChannel(USATISOChannel channel)
	{
		pool.remove(channel);
	}

	public void clearChannels()
	{
		pool.clear();
	}

	public int size()
	{
		return pool.size();
	}

	public synchronized ISOChannel getCurrent() throws IOException, ISOException
	{
		//if (current == null)
		//	connect();
		if(current == null)
			throw new ISOException("USATChannelPool Not Connected");
		// This causes rapid reconnects; ISOMUX will handle the reconnection
		//else if (!usable)
		//	reconnect();
		else if (!usable)
			throw new ISOException("USATChannelPool Not Usable");

		return current;
	}

	public void setName(String name)
	{
		this.name = name;
		NameRegistrar.register("channel." + name, this);
	}

	public String getName()
	{
		return this.name;
	}
}
