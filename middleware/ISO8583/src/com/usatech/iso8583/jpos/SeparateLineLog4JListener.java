package com.usatech.iso8583.jpos;

import java.io.*;
import java.util.Scanner;

import org.apache.log4j.Level;
import org.apache.log4j.xml.DOMConfigurator;
import org.jpos.core.*;
import org.jpos.util.*;

public class SeparateLineLog4JListener implements LogListener, ReConfigurable
{
	private Level _level;

	public SeparateLineLog4JListener()
	{
		this(Level.DEBUG_INT);
	}

	public SeparateLineLog4JListener(int level)
	{
		setLevel(level);
	}

	public void setLevel(int level)
	{
		_level = Level.toLevel(level);
	}

	public void setLevel(String level)
	{
		_level = Level.toLevel(level);
	}

	public void close()
	{
	}

	/**
	 * Expects the following properties:
	 * <ul>
	 *  <li>config   - Configuration file path
	 *  <li>priority - Log4J priority (debug, info, warn, error)
	 *  <li>watch    - interval (in ms) to monitor XML config file for changes 
	 * </ul>
	 */
	public void setConfiguration(Configuration cfg) throws ConfigurationException
	{
		DOMConfigurator.configureAndWatch(cfg.get("config"), cfg.getLong("watch"));
		setLevel(cfg.get("priority"));
	}

	public synchronized LogEvent log(LogEvent ev)
	{
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ev.getRealm());
		if (logger.isEnabledFor(_level))
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			ev.dump(ps, "");

			Scanner scanner = new Scanner(baos.toString());
			scanner.useDelimiter("\n");
			while (scanner.hasNext())
			{
				String s = scanner.next().replace('\r', ' ');
				if (s.length() > 0)
					logger.log(_level, s);
			}

			//logger.log(_level, baos.toString().replaceAll("(\r|\n)", ""));
		}
		return ev;
	}
}
