package com.usatech.iso8583.jpos;

import org.jpos.iso.ISOChannel;

public interface USATISOChannel extends ISOChannel
{
	public long getConnectTime();
	public long getLastSend();
	public long getLastReceive();
}
