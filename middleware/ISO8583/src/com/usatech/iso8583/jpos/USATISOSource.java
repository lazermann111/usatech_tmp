package com.usatech.iso8583.jpos;

import org.jpos.iso.ISOSource;

public interface USATISOSource extends ISOSource
{
	public long getConnectTime();
}
