/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field60OriginalMessageDataPackager;

import org.jpos.iso.*;

public class Field60OriginalMessageData extends ISOMsg
{
	private static final long serialVersionUID = 3688509895943139633L;
	private Field60OriginalMessageDataPackager msgPackager;

	public Field60OriginalMessageData() throws ISOException
	{
		super();
		setFieldNumber(60);
		//this.set(2, "");
		//this.set(3, "");
		this.set(4, "");
		msgPackager = new Field60OriginalMessageDataPackager();
		this.setPackager(msgPackager);
	}

	public Field60OriginalMessageData(int fieldNumber) throws ISOException
	{
		this();
		setFieldNumber(fieldNumber);
	}

	public String getOriginalMessageType() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setOriginalMessageType(String value) throws ISOException
	{
		this.set(2, value);
	}

	public String getOriginalSystemsTraceAuditNumber() throws ISOException
	{
		if (!hasField(3))
			return null;
		else
			return (String) this.getValue(3);
	}

	public void setOriginalSystemsTraceAuditNumber(String value) throws ISOException
	{
		this.set(3, value);
	}

	public void addToISOMsg(ISOMsg msg) throws ISOException
	{
		msg.set(fieldNumber, this.pack());
	}

	public void getFromISOMsg(ISOMsg msg) throws ISOException
	{
		this.unpack(ISOUtil.hex2byte(msg.getString(fieldNumber)));
	}
}
