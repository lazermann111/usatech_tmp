package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class FixedLengthCharRightSpacePadded extends ISOStringFieldPackager
{
	public FixedLengthCharRightSpacePadded()
	{
		super(0, null, RightTPadder.SPACE_PADDER, AsciiInterpreter.INSTANCE, NullPrefixer.INSTANCE);
	}

	/**
	 * @param len - field len
	 * @param description symbolic descrption
	 */
	public FixedLengthCharRightSpacePadded(int len, String description)
	{
		super(len, description, RightTPadder.SPACE_PADDER, AsciiInterpreter.INSTANCE, NullPrefixer.INSTANCE);
	}
}
