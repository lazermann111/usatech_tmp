package com.usatech.iso8583.interchange.fhms;

import java.net.SocketTimeoutException;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.PS2000;
import com.usatech.iso8583.interchange.fhms.jpos.message.FHMSISOMsg;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.util.CreditCardValidator;

public abstract class FHMSAction
{
	public abstract ISO8583Response process(ISO8583Transaction transaction);

	public abstract boolean recover(ISO8583Transaction transaction);
	
	protected abstract FHMSISOMsg createSimulatedResponse(FHMSISOMsg isoRequest); 

	private FHMSInterchange interchange;

	protected FHMSAction(FHMSInterchange interchange)
	{
		this.interchange = interchange;
	}

	public FHMSInterchange getInterchange()
	{
		return interchange;
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException
	{
		for (int i = 0; i < requiredFields.length; i++)
		{
			if (!request.hasField(requiredFields[i]))
				throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(requiredFields[i]));
		}

		if (request.hasTrackData())
		{
			if (!request.getTrackData().hasTrack1() && !request.getTrackData().hasTrack2())
				throw new ValidationException("Required Field Not Found: TrackData Tracks");

			try
			{
				CreditCardValidator.validateTrackData(request.getTrackData());
			}
			catch (NumberFormatException e)
			{
				throw new ValidationException("Invalid Track Data: " + e.getMessage());
			}
		}

		if (request.hasPanData())
		{
			if (request.getPanData().getPan() == null)
				throw new ValidationException("Required Field Not Found: PanData PAN");

			if (request.getPanData().getExpiration() == null)
				throw new ValidationException("Required Field Not Found: PanData Expiration");

			try
			{
				CreditCardValidator.validatePanData(request.getPanData());
			}
			catch (NumberFormatException e)
			{
				throw new ValidationException("Invalid Pan Data: " + e.getMessage());
			}
		}

		if (request.hasAvsAddress() && !request.hasAvsZip())
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_AVS_ZIP) + " is required with " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_AVS_ADDRESS));
	}

	protected FHMSISOMsg sendRequest(FHMSISOMsg isoRequest, ISO8583Transaction transaction) throws Exception
	{
		return sendRequest(isoRequest, transaction, -1);
	}
	
	protected FHMSISOMsg sendRequest(FHMSISOMsg isoRequest, ISO8583Transaction transaction, int hostResponseTimeout) throws Exception
	{
		FHMSISOMsg isoResponse = null;

		
		transaction.setState(ISO8583Transaction.STATE_REQUEST_TRANSMITTED);
		
		if(interchange.isSimulationMode())
			isoResponse = createSimulatedResponse(isoRequest);
		else
			// this will queue the request and block untill we get a response or timeout
			isoResponse = interchange.send(isoRequest, hostResponseTimeout);

		if (isoResponse == null)
			throw new SocketTimeoutException();

		return isoResponse;
	}

	protected ISO8583Response readResponse(FHMSISOMsg isoResponse) throws Exception
	{
		return readResponse(isoResponse, true);
	}

	protected ISO8583Response readResponse(FHMSISOMsg isoResponse, boolean requireEffectiveDate) throws Exception
	{
		ISO8583Response response = new ISO8583Response();

		if (isoResponse.getTransactionAmount() != null)
			response.setAmount(Integer.parseInt(isoResponse.getTransactionAmount()));

		if (isoResponse.getAuthorizationIdentificationResponse() != null)
			response.setApprovalCode(isoResponse.getAuthorizationIdentificationResponse());

		if (isoResponse.getCVV2ResultCode() != null)
			response.setCvv2Result(isoResponse.getCVV2ResultCode());

		if (isoResponse.getAVSResponseAddressMatch() != null)
			response.setAvsAddressMatch(isoResponse.getAVSResponseAddressMatch());

		if (isoResponse.getAVSResponseZipMatch() != null)
			response.setAvsZipMatch(isoResponse.getAVSResponseZipMatch());

		if (isoResponse.getAVSResponseCode() != null)
			response.setAvsResult(isoResponse.getAVSResponseCode());

		if (isoResponse.getTransactionLocalDate() != null && isoResponse.getTransactionLocalTime() != null)
			response.setEffectiveDate(FHMSUtil.parseDateTime(isoResponse.getTransactionLocalDate(), isoResponse.getTransactionLocalTime()));
		else if(requireEffectiveDate)
			throw new ValidationException("Response Did Not Contain a Required Field: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_EFFECTIVE_DATE));

		if (isoResponse.getRetrievalReferenceNumber() != null)
			response.setRetrievalReferenceNumber(isoResponse.getRetrievalReferenceNumber());

		if (isoResponse.getSystemTraceNumber() == null)
			throw new ValidationException("Response Did Not Contain a Required Field: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACE_NUMBER));
		response.setTraceNumber(Integer.parseInt(isoResponse.getSystemTraceNumber()));

		if (isoResponse.getResponseCode() == null)
			throw new ValidationException("Response Did Not Contain a Required Field: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_RESPONSE_CODE));
		response.setResponseCode(isoResponse.getResponseCode());

		if (isoResponse.getAlternateHostResponse() != null)
			response.setResponseMessage(isoResponse.getAlternateHostResponse());
		
		if(isoResponse.getPS2000Indicator() != null)
		{
			PS2000 ps2000 = new PS2000();
			ps2000.setIndicator(isoResponse.getPS2000Indicator());
			ps2000.setIdentifier(isoResponse.getPS2000TransactionIdentifier());
			ps2000.setValidationCode(isoResponse.getPS2000ValidationCode());
			ps2000.setResponseCode(isoResponse.getPS2000ResponseCode());
			ps2000.setEntryMode(isoResponse.getPS2000EntryMode());
			response.setPS2000(ps2000);
		}

		return response;
	}
}
