/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63ReconciliationRequestTotalsPackager;

public class Field63ReconciliationRequestTotals extends ISOMsg
{
	private static final long serialVersionUID = 3979268053588259632L;
	private Field63ReconciliationRequestTotalsPackager msgPackager;

	public Field63ReconciliationRequestTotals() throws ISOException
	{
		super();
		//this.set(2, "");
		//this.set(3, "");
		//this.set(4, "");
		//this.set(5, "");
		//this.set(6, "");
		//this.set(7, "");
		//this.set(8, "");
		//this.set(9, "");
		this.set(10, "0");
		msgPackager = new Field63ReconciliationRequestTotalsPackager();
		this.setPackager(msgPackager);
	}

	public Field63ReconciliationRequestTotals(int fieldNumber) throws ISOException
	{
		this();
		setFieldNumber(fieldNumber);
	}

	public String getCapturedSalesCount() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setCapturedSalesCount(String value) throws ISOException
	{
		this.set(2, value);
	}

	public String getSalesAmount() throws ISOException
	{
		if (!hasField(3))
			return null;
		else
			return (String) this.getValue(3);
	}

	public void setSalesAmount(String value) throws ISOException
	{
		this.set(3, value);
	}

	public String getRefundCount() throws ISOException
	{
		if (!hasField(4))
			return null;
		else
			return (String) this.getValue(4);
	}

	public void setRefundCount(String value) throws ISOException
	{
		this.set(4, value);
	}

	public String getRefundAmount() throws ISOException
	{
		if (!hasField(5))
			return null;
		else
			return (String) this.getValue(5);
	}

	public void setRefundAmount(String value) throws ISOException
	{
		this.set(5, value);
	}

	public String getDebitSalesCount() throws ISOException
	{
		if (!hasField(6))
			return null;
		else
			return (String) this.getValue(6);
	}

	public void setDebitSalesCount(String value) throws ISOException
	{
		this.set(6, value);
	}

	public String getDebitSalesAmount() throws ISOException
	{
		if (!hasField(7))
			return null;
		else
			return (String) this.getValue(7);
	}

	public void setDebitSalesAmount(String value) throws ISOException
	{
		this.set(7, value);
	}

	public String getDebitRefundCount() throws ISOException
	{
		if (!hasField(8))
			return null;
		else
			return (String) this.getValue(8);
	}

	public void setDebitRefundCount(String value) throws ISOException
	{
		this.set(8, value);
	}

	public String getDebitRefundAmount() throws ISOException
	{
		if (!hasField(9))
			return null;
		else
			return (String) this.getValue(9);
	}

	public void setDebitRefundAmount(String value) throws ISOException
	{
		this.set(9, value);
	}

	public void addToISOMsg(ISOMsg msg) throws ISOException
	{
		msg.set(fieldNumber, this.pack());
	}

	public void getFromISOMsg(ISOMsg msg) throws ISOException
	{
		this.unpack(ISOUtil.hex2byte(msg.getString(fieldNumber)));
	}
}
