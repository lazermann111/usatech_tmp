/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63AddressVerificationData54Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63AddressVerificationData54 extends ISOMsg
{
	private static final long serialVersionUID = 3257284725558294582L;

	private static final int TLV_TAG = 54;

	private Field63AddressVerificationData54Packager msgPackager;

	public Field63AddressVerificationData54() throws ISOException
	{
		super();
		//this.set(2, "");
		//this.set(3, "");
		msgPackager = new Field63AddressVerificationData54Packager();
		this.setPackager(msgPackager);
	}

	public String getZipCode() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setZipCode(String value) throws ISOException
	{
		this.set(2, value);
	}

	public String getAddress() throws ISOException
	{
		if (!hasField(3))
			return null;
		else
			return (String) this.getValue(3);
	}

	public void setAddress(String value) throws ISOException
	{
		this.set(3, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
