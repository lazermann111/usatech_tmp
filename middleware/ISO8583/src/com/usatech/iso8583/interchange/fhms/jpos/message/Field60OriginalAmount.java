/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field60OriginalAmountPackager;

import org.jpos.iso.*;

public class Field60OriginalAmount extends ISOMsg
{
	private static final long serialVersionUID = 4051041982350898999L;
	private Field60OriginalAmountPackager msgPackager;

	public Field60OriginalAmount() throws ISOException
	{
		super();
		setFieldNumber(60);
		//this.set(2, "");
		msgPackager = new Field60OriginalAmountPackager();
		this.setPackager(msgPackager);
	}

	public Field60OriginalAmount(int fieldNumber) throws ISOException
	{
		this();
		setFieldNumber(fieldNumber);
	}

	public String getOriginalAmount() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setOriginalAmount(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToISOMsg(ISOMsg msg) throws ISOException
	{
		msg.set(fieldNumber, this.pack());
	}

	public void getFromISOMsg(ISOMsg msg) throws ISOException
	{
		this.unpack(ISOUtil.hex2byte(msg.getString(fieldNumber)));
	}
}
