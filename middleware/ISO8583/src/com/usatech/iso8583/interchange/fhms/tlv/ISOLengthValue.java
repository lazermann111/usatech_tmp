/**
 * Copyright *2000, GlobalPlatform, Inc., all rights reserved.
 * The Technology provided or described herein is subject to updates,
 * revisions, and extensions by GlobalPlatform.  Use of this information is
 * governed by the GlobalPlatform License Agreement and any use inconsistent
 * with that Agreement is strictly prohibited.
 **/

package com.usatech.iso8583.interchange.fhms.tlv;

/**
 * Helper class for implementing TLV classes
 * @author Christophe Colas
 * @version 1.5.4 
 */
class ISOLengthValue
{
	int length;
	int value;
}
