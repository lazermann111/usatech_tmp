package com.usatech.iso8583.interchange.fhms;

public class ValidationException extends Exception
{
	public ValidationException()
	{
		super();
	}

	public ValidationException(String msg)
	{
		super(msg);
	}

	public ValidationException(String msg, Exception e)
	{
		super(msg, e);
	}
}
