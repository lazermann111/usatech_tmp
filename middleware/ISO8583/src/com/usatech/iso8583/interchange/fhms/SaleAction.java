package com.usatech.iso8583.interchange.fhms;

import java.net.SocketTimeoutException;
import java.util.Date;

import com.usatech.iso8583.*;
import com.usatech.iso8583.interchange.fhms.jpos.message.FHMSISOMsg;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.util.Util;

import org.apache.commons.logging.*;
import org.jpos.iso.ISOException;

public class SaleAction extends FHMSAction
{
	private static Log log = LogFactory.getLog(SaleAction.class);

	private static int[] requiredFields = { ISO8583Message.FIELD_ACQUIRER_ID, ISO8583Message.FIELD_TERMINAL_ID, ISO8583Message.FIELD_TRACE_NUMBER, ISO8583Message.FIELD_AMOUNT, ISO8583Message.FIELD_POS_ENVIRONMENT, ISO8583Message.FIELD_ENTRY_MODE, ISO8583Message.FIELD_ONLINE };

	public SaleAction(FHMSInterchange interchange)
	{
		super(interchange);
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException
	{
		super.validateRequest(request, requiredFields);

		if (!request.hasPanData() && !request.hasTrackData())
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) + " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));

		if (!request.isOnline())
		{
			int[] onlineRequiredFields = { ISO8583Message.FIELD_EFFECTIVE_DATE };
			super.validateRequest(request, onlineRequiredFields);
		}
		else
		{
			// requires pan data or track data, which is checked above
		}
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		ISO8583Request request = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);

		FHMSISOMsg isoRequest = null;

		try
		{
			validateRequest(request, requiredFields);

			isoRequest = new FHMSISOMsg();

			if (request.isOnline())
				isoRequest.setMTI("0200");
			else
				isoRequest.setMTI("0220");

			isoRequest.setProcessingCode("004000");

			if (request.hasTrackData())
			{
				if (request.getTrackData().hasTrack1())
					isoRequest.setTrack1Data(request.getTrackData().getTrack1());
				if (request.getTrackData().hasTrack2())
					isoRequest.setTrack2Data(request.getTrackData().getTrack2());
				isoRequest.setPointOfServiceEntryMode(FHMSUtil.buildPOSEntryModeString(request.getEntryMode(), request.getPinEntryCapability(), request.getTrackData().extractPANData()));
			}
			else
			{
				isoRequest.setPrimaryAccountNumber(request.getPanData().getPan());
				isoRequest.setExpirationDate(request.getPanData().getExpiration());
				isoRequest.setPointOfServiceEntryMode(FHMSUtil.buildPOSEntryModeString(request.getEntryMode(), request.getPinEntryCapability(), request.getPanData()));
			}

			if (request.hasEffectiveDate())
			{
				isoRequest.setTransactionLocalTime(FHMSUtil.getHHmmss(request.getEffectiveDate()));
				isoRequest.setTransactionLocalDate(FHMSUtil.getMMDD(request.getEffectiveDate()));
			}

			if (request.hasRetrievalReferenceNumber())
				isoRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());

			if (request.hasApprovalCode())
				isoRequest.setAuthorizationIdentificationResponse(request.getApprovalCode());

			if (request.hasCvv2())
				isoRequest.setCVV2RequestValue(request.getCvv2());

			if (request.hasAvsZip())
				isoRequest.setAVSRequestData(request.getAvsZip(), request.getAvsAddress());
			
			if(request.hasPS2000())
			{
				PS2000 ps2000 = request.getPS2000();
				isoRequest.setPS2000(ps2000.getIndicator(), ps2000.getIdentifier(), ps2000.getValidationCode(), ps2000.getResponseCode(), ps2000.getEntryMode());
			}

			isoRequest.setCardAcceptorAcquirerID(request.getAcquirerID());
			isoRequest.setCardAcceptorTerminalID(request.getTerminalID());
			isoRequest.setSystemTraceNumber(Integer.toString(request.getTraceNumber()));
			isoRequest.setTransactionAmount(Integer.toString(request.getAmount()));
			isoRequest.setPointOfServiceConditionCode(FHMSUtil.buildPOSConditionCode(request.getPosEnvironment()));
		}
		catch (ValidationException e)
		{
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		}
		catch (ISOException e)
		{
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Caught unexpected exception validating " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception validating request: " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);

		FHMSISOMsg isoResponse = null;
		
		try
		{
			isoResponse = sendRequest(isoRequest, transaction, (request.getHostResponseTimeout()*1000));
		}
		catch (SocketTimeoutException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.warn("Timeout occured waiting for host response for " + transaction);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, "Timeout occured waiting for host response.");
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception transmitting " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting " + transaction + ": " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);

		ISO8583Response response = null;
		
		// patch 07/18/2007 (1.3.11) - effective date is not required in the response to an offline sale
		
		try
		{
			if(request.isOnline())
				response = readResponse(isoResponse, true);
			else
				response = readResponse(isoResponse, false);
		}
		catch (ValidationException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host response failed input validation: " + e.getMessage());
		}
		catch (ISOException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host response failed input validation: " + e.getMessage());
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception while constructing response: " + transaction.getTransactionID() + ": " + e.getMessage() + ", response: " + isoResponse, e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
		}
		
		// patch 07/18/2007 (1.3.11) - if no effective date was received, set the response effective date to the request date
		if(!response.hasEffectiveDate())
		{
			log.warn("Response to " + transaction + " has no effectiveDate: substituting request effectiveDate: " + FHMSUtil.TRAN_TIME_FORMAT.get().format(request.getEffectiveDate()));
			response.setEffectiveDate(request.getEffectiveDate());
		}

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);

		return response;
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		ISO8583Request request = transaction.getRequest();
		/*
		if (!request.isOnline())
		{
			log.info("Skipping recovery of " + transaction + ": Not an online transaction");
			return true;
		}
		*/

		FHMSISOMsg isoRequest = null;

		try
		{
			isoRequest = new FHMSISOMsg();
			isoRequest.setMTI("0400");
			isoRequest.setProcessingCode("004000");
			isoRequest.setCardAcceptorAcquirerID(request.getAcquirerID());
			isoRequest.setCardAcceptorTerminalID(request.getTerminalID());
			isoRequest.setSystemTraceNumber(Integer.toString(request.getTraceNumber()));
			isoRequest.setTransactionAmount(Integer.toString(request.getAmount()));
			isoRequest.setPointOfServiceConditionCode(FHMSUtil.buildPOSConditionCode(request.getPosEnvironment()));
			
			if (request.hasTrackData())
			{
				if (request.getTrackData().hasTrack1())
					isoRequest.setTrack1Data(request.getTrackData().getTrack1());
				else
					isoRequest.setTrack2Data(request.getTrackData().getTrack2());
				isoRequest.setPointOfServiceEntryMode(FHMSUtil.buildPOSEntryModeString(request.getEntryMode(), request.getPinEntryCapability(), request.getTrackData().extractPANData()));
			}
			else
			{
				isoRequest.setPrimaryAccountNumber(request.getPanData().getPan());
				isoRequest.setExpirationDate(request.getPanData().getExpiration());
				isoRequest.setPointOfServiceEntryMode(FHMSUtil.buildPOSEntryModeString(request.getEntryMode(), request.getPinEntryCapability(), request.getPanData()));
			}
		}
		catch (ISOException e)
		{
			log.error("Attempted recovery of " + transaction + " failed input validation: " + e.getMessage(), e);
			return true;
		}
		catch (Throwable e)
		{
			log.error("Attempted recovery of " + transaction + " caught unexpected constructing request: " + e.getMessage(), e);
			return true;
		}

		FHMSISOMsg isoResponse = null;

		try
		{
			// this will queue the request and block untill we get a response or timeout
			isoResponse = getInterchange().send(isoRequest);
		}
		catch (Throwable e)
		{
			log.error("Attempted recovery of " + transaction + " caught unexpected exception while transmitting: " + transaction.getTransactionID() + ": " + e.getMessage(), e);
			return false;
		}

		if (isoResponse == null)
		{
			log.warn("Attempted recovery timeout occured waiting for host response for " + transaction);
			return false;
		}

		log.info("Attempted recovery of " + transaction + " received reversal response: " + isoResponse);
		return true;
	}

	protected FHMSISOMsg createSimulatedResponse(FHMSISOMsg isoRequest)
	{
		try
		{
			FHMSISOMsg isoResponse = new FHMSISOMsg();
			
			if(isoRequest.getMTI().equals("0200"))
				isoResponse.setMTI("0210");
			else
				isoResponse.setMTI("0230");
			
			isoResponse.setProcessingCode(isoRequest.getProcessingCode());
			isoResponse.setCardAcceptorAcquirerID(isoRequest.getCardAcceptorAcquirerID());
			isoResponse.setCardAcceptorTerminalID(isoRequest.getCardAcceptorTerminalID());
			isoResponse.setSystemTraceNumber(isoRequest.getSystemTraceNumber());
			isoResponse.setTransactionAmount(isoRequest.getTransactionAmount());
			
			if(isoRequest.getTransactionLocalDate() != null)
			{
				isoResponse.setTransactionLocalDate(isoRequest.getTransactionLocalDate());
				isoResponse.setTransactionLocalTime(isoRequest.getTransactionLocalTime());
			}
			else
			{
				Date now = new Date();
				isoResponse.setTransactionLocalDate(FHMSUtil.getMMDD(now));
				isoResponse.setTransactionLocalTime(FHMSUtil.getHHmmss(now));
			}
			
			if(isoRequest.getRetrievalReferenceNumber() != null)
				isoResponse.setRetrievalReferenceNumber(isoRequest.getRetrievalReferenceNumber());
			else
				isoResponse.setRetrievalReferenceNumber("0000000" + Util.getRandomInt(10000, 99999));
			
			if(isoRequest.getAuthorizationIdentificationResponse() != null)
				isoResponse.setAuthorizationIdentificationResponse(isoRequest.getAuthorizationIdentificationResponse());
			else
				isoResponse.setAuthorizationIdentificationResponse("T" + Util.getRandomInt(10000, 99999));
			
			isoResponse.setResponseCode("00");
			//isoResponse.setResponseCode("51");
			//isoResponse.setAlternateHostResponse("AUTH DECLINED");
			
			return isoResponse;
		}
		catch(ISOException e)
		{
			log.error("Failed to create simulated response: " + e.getMessage(), e);
			return null;
		}
	}
}
