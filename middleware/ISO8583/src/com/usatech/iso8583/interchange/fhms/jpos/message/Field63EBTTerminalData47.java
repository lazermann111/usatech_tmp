/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63EBTTerminalData47Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63EBTTerminalData47 extends ISOMsg
{
	private static final long serialVersionUID = 3258131358034508087L;
	private static final int TLV_TAG = 47;
	private Field63EBTTerminalData47Packager msgPackager;

	public Field63EBTTerminalData47() throws ISOException
	{
		super();
		//this.set(2, "");
		//this.set(3, "");
		//this.set(4, "");
		//this.set(5, "");
		msgPackager = new Field63EBTTerminalData47Packager();
		this.setPackager(msgPackager);
	}

	public String getClerkID() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setClerkID(String value) throws ISOException
	{
		this.set(2, value);
	}

	public String getSupervisorID() throws ISOException
	{
		if (!hasField(3))
			return null;
		else
			return (String) this.getValue(3);
	}

	public void setSupervisorID(String value) throws ISOException
	{
		this.set(3, value);
	}

	public String getVoucherNumber() throws ISOException
	{
		if (!hasField(4))
			return null;
		else
			return (String) this.getValue(4);
	}

	public void setVoucherNumber(String value) throws ISOException
	{
		this.set(4, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public String getGenerationNumber() throws ISOException
	{
		if (!hasField(5))
			return null;
		else
			return (String) this.getValue(5);
	}

	public void setGenerationNumber(String value) throws ISOException
	{
		this.set(5, value);
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
