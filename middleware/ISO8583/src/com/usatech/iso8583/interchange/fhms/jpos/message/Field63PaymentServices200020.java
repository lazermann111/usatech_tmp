/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63PaymentServices200020Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

public class Field63PaymentServices200020 extends ISOMsg
{
	private static final long serialVersionUID = 3258135743145522229L;
	private static final int TLV_TAG = 20;
	private Field63PaymentServices200020Packager msgPackager;

	public Field63PaymentServices200020() throws ISOException
	{
		super();
		//this.set(2, "");
		//this.set(3, "");
		//this.set(4, "");
		//this.set(5, "");
		//this.set(6, "");
		msgPackager = new Field63PaymentServices200020Packager();
		this.setPackager(msgPackager);
	}

	public Field63PaymentServices200020(int fieldNumber) throws ISOException
	{
		this();
		setFieldNumber(fieldNumber);
	}

	public String getPS2000Indicator() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setPS2000Indicator(String value) throws ISOException
	{
		this.set(2, value);
	}

	public String getTransactionIdentifier() throws ISOException
	{
		if (!hasField(3))
			return null;
		else
			return (String) this.getValue(3);
	}

	public void setTransactionIdentifier(String value) throws ISOException
	{
		this.set(3, value);
	}

	public String getValidationCode() throws ISOException
	{
		if (!hasField(4))
			return null;
		else
			return (String) this.getValue(4);
	}

	public void setValidationCode(String value) throws ISOException
	{
		this.set(4, value);
	}

	public String getVisaResponseCode() throws ISOException
	{
		if (!hasField(5))
			return null;
		else
			return (String) this.getValue(5);
	}

	public void setVisaResponseCode(String value) throws ISOException
	{
		this.set(5, value);
	}

	public String getPOSEntryMode() throws ISOException
	{
		if (!hasField(6))
			return null;
		else
			return (String) this.getValue(6);
	}

	public void setPOSEntryMode(String value) throws ISOException
	{
		this.set(6, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
