/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

import com.usatech.iso8583.interchange.fhms.jpos.MerchantLinkFrameRelayChannel;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;
import com.usatech.iso8583.util.ISO8583Util;
import com.usatech.util.Conversions;

public class FHMSISOMsg extends ISOMsg
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(MerchantLinkFrameRelayChannel.class);

	private static final long serialVersionUID = 3257852069297009204L;
	
	protected String sequenceNumber = "0000";
	
	protected byte[] rawBytes;

	public FHMSISOMsg() throws ISOException
	{
		super();
		/*
		 this.set(2, "0");
		 this.set(3, "0");
		 this.set(4, "0");
		 this.set(11, "0");
		 this.set(12, "0");
		 this.set(13, "0");
		 this.set(14, "0");
		 this.set(22, "0");
		 this.set(24, "0");
		 this.set(25, "0");
		 this.set(35, "");
		 this.set(37, "");
		 this.set(38, "");
		 this.set(39, "");
		 this.set(41, "");
		 this.set(42, "");
		 this.set(45, "");
		 this.set(52, defaultPinData);
		 this.set(54, "");
		 */

		// set defaults
		this.set(24, "000"); // NII
		//this.set(61, "0000");
	}

	public FHMSISOMsg(ISOMsg m) throws ISOException
	{
		super();
		this.setPackager(m.getPackager());
		this.unpack(m.pack());
		this.setDirection(m.getDirection());
		this.setHeader(m.getHeader());
		this.setSource(m.getSource());
	}
	
	public void setRawBytes(byte[] bytes)
	{
		this.rawBytes = bytes;
	}
	
	public byte[] getRawBytes()
	{
		return rawBytes;
	}

	public String getPrimaryAccountNumber() throws ISOException
	{
		if (!hasField(2))
			return null;
		return (String) this.getValue(2);
	}

	public void setPrimaryAccountNumber(String value) throws ISOException
	{
		if (value == null || value.length() < 14 || value.length() > 16)
			throw new ISOException("Invalid Primary Account Number: " + value);

		this.set(2, value);
	}

	public String getProcessingCode() throws ISOException
	{
		if (!hasField(3))
			return null;
		return (String) this.getValue(3);
	}

	public void setProcessingCode(String value) throws ISOException
	{
		if (value == null || value.length() != 6)
			throw new ISOException("Invalid Processing Code: " + value);

		this.set(3, value);
	}

	public String getTransactionAmount() throws ISOException
	{
		if (!hasField(4))
			return null;
		return (String) this.getValue(4);
	}

	public void setTransactionAmount(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Transaction Amount: " + value);

		try
		{
			int amt = Integer.parseInt(value);
			if (amt < 0)
				throw new ISOException("Invalid Transaction Amount: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Amount: " + value);
		}

		this.set(4, value);
	}

	public String getSystemTraceNumber() throws ISOException
	{
		if (!hasField(11))
			return null;
		return (String) this.getValue(11);
	}

	public void setSystemTraceNumber(String value) throws ISOException
	{
		if (value == null || value.length() > 6)
			throw new ISOException("Invalid System Trace Number: " + value);

		try
		{
			int traceNo = Integer.parseInt(value);
			if (traceNo < 0 || traceNo > 999999)
				throw new ISOException("Invalid System Trace Number: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid System Trace Number: " + value);
		}

		this.set(11, value);
	}

	public String getTransactionLocalTime() throws ISOException
	{
		if (!hasField(12))
			return null;
		return (String) this.getValue(12);
	}

	public void setTransactionLocalTime(String value) throws ISOException
	{
		if (value == null || value.length() != 6)
			throw new ISOException("Invalid Transaction Local Time: " + value);

		try
		{
			int time = Integer.parseInt(value);
			if (time < 0 || time > 999999)
				throw new ISOException("Invalid Transaction Local Time: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Local Time: " + value);
		}

		this.set(12, value);
	}

	public String getTransactionLocalDate() throws ISOException
	{
		if (!hasField(13))
			return null;
		return (String) this.getValue(13);
	}

	public void setTransactionLocalDate(String value) throws ISOException
	{
		if (value == null || value.length() != 4)
			throw new ISOException("Invalid Expiration Date: " + value);

		try
		{
			int date = Integer.parseInt(value);
			if (date < 0 || date > 9999)
				throw new ISOException("Invalid Transaction Local Date: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Local Date: " + value);
		}

		this.set(13, value);
	}

	public String getExpirationDate() throws ISOException
	{
		if (!hasField(14))
			return null;
		return (String) this.getValue(14);
	}

	public void setExpirationDate(String value) throws ISOException
	{
		if (value == null || value.length() != 4)
			throw new ISOException("Invalid Expiration Date: " + value);

		try
		{
			int exp = Integer.parseInt(value);
			if (exp < 0 || exp > 9999)
				throw new ISOException("Invalid Expiration Date: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Expiration Date: " + value);
		}

		this.set(14, value);
	}

	public String getPointOfServiceEntryMode() throws ISOException
	{
		if (!hasField(22))
			return null;
		return (String) this.getValue(22);
	}

	public void setPointOfServiceEntryMode(String value) throws ISOException
	{
		if (value == null || value.length() != 3)
			throw new ISOException("Invalid Point Of Service Entry Mode: " + value);

		try
		{
			int entryMode = Integer.parseInt(value);
			if (entryMode < 0 || entryMode > 999)
				throw new ISOException("Invalid Point Of Service Entry Mode: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Point Of Service Entry Mode: " + value);
		}

		this.set(22, value);
	}

	public String getPointOfServiceConditionCode() throws ISOException
	{
		if (!hasField(25))
			return null;
		return (String) this.getValue(25);
	}

	public void setPointOfServiceConditionCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Point Of Service Condition Code: " + value);

		try
		{
			int code = Integer.parseInt(value);
			if (code != 0 && code != 8)
				throw new ISOException("Invalid Point Of Service Condition Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Point Of Service Condition Code: " + value);
		}

		this.set(25, value);
	}

	public String getTrack2Data() throws ISOException
	{
		if (!hasField(35))
			return null;
		return (String) this.getValue(35);
	}

	public void setTrack2Data(String value) throws ISOException
	{
		if (value == null || value.length() < 3 || value.length() > 39)
			throw new ISOException("Invalid Track2 Data: " + value);

		this.set(35, value);
	}

	public String getRetrievalReferenceNumber() throws ISOException
	{
		if (!hasField(37))
			return null;
		return (String) this.getValue(37);
	}

	public void setRetrievalReferenceNumber(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Retrieval Reference Number: " + value);

		this.set(37, value);
	}

	public String getAuthorizationIdentificationResponse() throws ISOException
	{
		if (!hasField(38))
			return null;
		return (String) this.getValue(38);
	}

	public void setAuthorizationIdentificationResponse(String value) throws ISOException
	{
		this.set(38, value);
	}

	public String getResponseCode() throws ISOException
	{
		if (!hasField(39))
			return null;
		return (String) this.getValue(39);
	}

	public void setResponseCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Response Code: " + value);

		try
		{
			int code = Integer.parseInt(value);
			if (code < 0 || code > 99)
				throw new ISOException("Invalid Response Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Response Code: " + value);
		}

		this.set(39, value);
	}

	public String getCardAcceptorTerminalID() throws ISOException
	{
		if (!hasField(41))
			return null;
		return (String) this.getValue(41);
	}

	public void setCardAcceptorTerminalID(String value) throws ISOException
	{
		if (value == null || !value.startsWith("0001") || value.length() != 8)
			throw new ISOException("Invalid Terminal ID: " + value);

		this.set(41, value);
	}

	public String getCardAcceptorAcquirerID() throws ISOException
	{
		if (!hasField(42))
			return null;
		return (String) this.getValue(42);
	}

	public void setCardAcceptorAcquirerID(String value) throws ISOException
	{
		if (value == null || !value.startsWith("000") || value.length() != 15)
			throw new ISOException("Invalid Acquirer ID: " + value);

		this.set(42, value);
	}

	public String getTrack1Data() throws ISOException
	{
		if (!hasField(45))
			return null;
		return (String) this.getValue(45);
	}

	public void setTrack1Data(String value) throws ISOException
	{
		if (value == null || value.length() < 3 || value.length() > 78)
			throw new ISOException("Invalid Track1 Data: " + value);

		this.set(45, value);
	}

	public byte[] getPINData() throws ISOException
	{
		if (!hasField(52))
			return null;
		return ISOUtil.hex2byte(this.getString(52));
	}

	public void setPINData(byte[] value) throws ISOException
	{
		// not used yet
		this.set(52, value);
	}

	public String getAdditionalAmounts() throws ISOException
	{
		if (!hasField(54))
			return null;
		return (String) this.getValue(54);
	}

	public void setAdditionalAmounts(String value) throws ISOException
	{
		this.set(54, value);
	}

	public String getOriginalAmount() throws ISOException
	{
		if (!hasField(60))
			return null;

		Field60OriginalAmount fld60Out = new Field60OriginalAmount();
		fld60Out.getFromISOMsg(this);
		return fld60Out.getOriginalAmount();
	}

	public void setOriginalAmount(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Original Amount: " + value);

		try
		{
			int amt = Integer.parseInt(value);
			if (amt < 0)
				throw new ISOException("Invalid Original Amount: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Original Amount: " + value);
		}

		//this.set(60, value);

		Field60OriginalAmount fld60Out = new Field60OriginalAmount();
		fld60Out.setOriginalAmount(value);
		fld60Out.addToISOMsg(this);
	}

	public String getOriginalMessageType() throws ISOException
	{
		if (!hasField(60))
			return null;

		Field60OriginalMessageData field60 = new Field60OriginalMessageData();
		field60.getFromISOMsg(this);
		return field60.getOriginalMessageType();
	}

	public String getOriginalTraceNumber() throws ISOException
	{
		if (!hasField(60))
			return null;

		Field60OriginalMessageData field60 = new Field60OriginalMessageData();
		field60.getFromISOMsg(this);
		return field60.getOriginalSystemsTraceAuditNumber();
	}

	public void setOriginalMessageData(String messageType, String traceNumber) throws ISOException
	{
		if (messageType == null || messageType.length() != 4)
			throw new ISOException("Invalid Original Message Type: " + messageType);

		if (traceNumber == null)
			throw new ISOException("Invalid Original Trace Number: " + traceNumber);

		Field60OriginalMessageData field60 = new Field60OriginalMessageData();
		field60.setOriginalMessageType(messageType);
		field60.setOriginalSystemsTraceAuditNumber(traceNumber);
		field60.addToISOMsg(this);
	}

	public String getCVV2ResultCode() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63CVV2Data16 cvv2Data = new Field63CVV2Data16();
		cvv2Data.getFromTLV(field63);
		return cvv2Data.getCode();
	}

	public void setCVV2RequestValue(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid CVV2 Request Value: " + value);

		Field63CVV2Data16 cvv2 = new Field63CVV2Data16();
		cvv2.setCode("10" + ISO8583Util.rightJustify(value, 4, ' '));

		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
		{
			field63 = new ISOTLVField(63);
			cvv2.addToTLV(field63);
			set(field63);
		}
		else
		{
			cvv2.addToTLV(field63);
		}
	}

	public String getAVSResponseAddressMatch() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63AVSResponse55 avsResponse = new Field63AVSResponse55();
		avsResponse.getFromTLV(field63);
		if (avsResponse == null)
			return null;

		return avsResponse.getAddressMatch();
	}

	public String getAVSResponseZipMatch() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63AVSResponse55 avsResponse = new Field63AVSResponse55();
		avsResponse.getFromTLV(field63);
		if (avsResponse == null)
			return null;

		return avsResponse.getZipCodeMatch();
	}

	public String getAVSResponseCode() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63AVSResponse55 avsResponse = new Field63AVSResponse55();
		avsResponse.getFromTLV(field63);
		if (avsResponse == null)
			return null;

		return avsResponse.getAddressResponseCode();
	}

	public void setAVSRequestData(String zip, String address) throws ISOException
	{
		if (zip == null || zip.length() > 9)
			throw new ISOException("Invalid AVS Zip Value: " + zip);

		Field63AddressVerificationData54 avsRequest = new Field63AddressVerificationData54();
		avsRequest.setZipCode(zip);
		avsRequest.setAddress(address == null ? " " : address);

		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
		{
			field63 = new ISOTLVField(63);
			avsRequest.addToTLV(field63);
			set(field63);
		}
		else
		{
			avsRequest.addToTLV(field63);
		}
	}

	private ISOTLVField getISOTLVField(int n)
	{
		if (!hasField(n))
			return null;

		Object o = this.getComponent(n);
		if (!(o instanceof ISOTLVField))
		{
			log.debug("getISOTLVField(" + n + ") return unexpected type " + o.getClass().getSimpleName() + " instead of expected ISOTLVField");
			return null;
		}

		return (ISOTLVField) o;
	}

	public String getAlternateHostResponse() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63AlternateHostResponse22 alternateHostResponse = new Field63AlternateHostResponse22();
		alternateHostResponse.getFromTLV(field63);
		if (alternateHostResponse == null)
			return null;

		return alternateHostResponse.getResponseText();
	}
	
	public void setAlternateHostResponse(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Alternate Host Response Value: " + value);

		Field63AlternateHostResponse22 alternateResponse = new Field63AlternateHostResponse22();
		alternateResponse.setResponseText(value);

		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
		{
			field63 = new ISOTLVField(63);
			alternateResponse.addToTLV(field63);
			set(field63);
		}
		else
		{
			alternateResponse.addToTLV(field63);
		}
	}

	public String getBatchNumber() throws ISOException
	{
		String mti = getMTI();
		if (mti == null)
			throw new ISOException("getBatchNumber() failed: Unable to determine MTI");

		if (mti.equals("0500") || mti.equals("0510"))
		{
			if (!this.hasField(60))
				return null;

			Field60BatchNumber batchNumber = new Field60BatchNumber();
			batchNumber.getFromISOMsg(this);
			return batchNumber.getBatchNumber();
		}
		if (mti.equals("0320") || mti.equals("0330"))
		{
			ISOTLVField field63 = getISOTLVField(63);
			if (field63 == null)
				return null;

			Field63BatchNumber37 batchNumber = new Field63BatchNumber37();
			batchNumber.getFromTLV(field63);
			if (batchNumber == null)
				return null;

			return batchNumber.getBatchNumber();
		}

		return null;
	}

	public void setBatchNumber(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Batch Number: " + value);

		try
		{
			int n = Integer.parseInt(value);
			if (n < 0) // || n > 999) ??
				throw new ISOException("Invalid Batch Number: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Batch Number: " + value);
		}

		String mti = getMTI();
		if (mti == null)
			throw new ISOException("setBatchNumber(" + value + ") failed: Unable to determine MTI");

		if (mti.equals("0500") || mti.equals("0510"))
		{
			Field60BatchNumber batchNumber = new Field60BatchNumber();
			batchNumber.setBatchNumber(value);
			batchNumber.addToISOMsg(this);
		}
		if (mti.equals("0320") || mti.equals("0330"))
		{
			Field63BatchNumber37 batchNumber = new Field63BatchNumber37();
			batchNumber.setBatchNumber(value);

			ISOTLVField field63 = getISOTLVField(63);
			if (field63 == null)
			{
				field63 = new ISOTLVField(63);
				batchNumber.addToTLV(field63);
				set(field63);
			}
			else
			{
				batchNumber.addToTLV(field63);
			}
		}
	}

	public void setReconciliationRequestTotals(String saleCnt, String saleAmt, String refundCnt, String refundAmt) throws ISOException
	{
		Field63ReconciliationRequestTotals totals = new Field63ReconciliationRequestTotals(63);
		totals.setCapturedSalesCount(saleCnt);
		totals.setSalesAmount(saleAmt);
		totals.setRefundCount(refundCnt);
		totals.setRefundAmount(refundAmt);
		totals.setDebitSalesCount("0");
		totals.setDebitSalesAmount("0");
		totals.setDebitRefundCount("0");
		totals.setDebitRefundAmount("0");
		totals.addToISOMsg(this);
	}
	
	public void setPS2000(String ps2000Indicator, String transactionIdentifier, String validationCode, String visaResponseCode, String posEntryMode) throws ISOException
	{
		Field63PaymentServices200020 ps2000 = new Field63PaymentServices200020(63);
		ps2000.setPS2000Indicator(ps2000Indicator);
		ps2000.setTransactionIdentifier(transactionIdentifier);
		ps2000.setValidationCode(validationCode);
		ps2000.setVisaResponseCode(visaResponseCode);
		ps2000.setPOSEntryMode(posEntryMode);
		
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
		{
			field63 = new ISOTLVField(63);
			ps2000.addToTLV(field63);
			set(field63);
		}
		else
		{
			ps2000.addToTLV(field63);
		}
	}

	public String getPS2000Indicator() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63PaymentServices200020 ps2000 = new Field63PaymentServices200020();
		ps2000.getFromTLV(field63);
		return ps2000.getPS2000Indicator();
	}
	
	public String getPS2000TransactionIdentifier() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63PaymentServices200020 ps2000 = new Field63PaymentServices200020();
		ps2000.getFromTLV(field63);
		return ps2000.getTransactionIdentifier();
	}

	public String getPS2000ValidationCode() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63PaymentServices200020 ps2000 = new Field63PaymentServices200020();
		ps2000.getFromTLV(field63);
		return ps2000.getValidationCode();
	}

	public String getPS2000ResponseCode() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63PaymentServices200020 ps2000 = new Field63PaymentServices200020();
		ps2000.getFromTLV(field63);
		return ps2000.getVisaResponseCode();
	}
	
	public String getPS2000EntryMode() throws ISOException
	{
		ISOTLVField field63 = getISOTLVField(63);
		if (field63 == null)
			return null;

		Field63PaymentServices200020 ps2000 = new Field63PaymentServices200020();
		ps2000.getFromTLV(field63);
		return ps2000.getPOSEntryMode();
	}

	// TODO: Implement get/set methods for rest of Field 63 elements

	public String getSequenceNumber() throws ISOException
	{
		return sequenceNumber;

		//		if(!hasField(61))
		//			return null;
		//		return (String) this.getValue(61);
	}

	public void setSequenceNumber(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Sequence Number: " + value);

		try
		{
			int seqNum = Integer.parseInt(value);
			if (seqNum < 0 || seqNum > 9999)
				throw new ISOException("Invalid Sequence Number: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Sequence Number: " + value);
		}

		//this.set(61, ISOUtil.zeropad(value, 4));
		this.sequenceNumber = value;
	}

	// toString copied mostly from ISOMsg class but modified for performance and to handle heartbeats
	public String toString()
	{

		StringBuilder s = new StringBuilder();
		if (isIncoming())
			s.append("<-- ");
		else if (isOutgoing())
			s.append("--> ");
		else
			s.append("    ");
		
		if(!isHeartBeat() && rawBytes != null)
		{
			s.append(Conversions.bytesToHex(rawBytes));
		}
		else
		{
			try
			{
				/*
				 if(getSequenceNumber() != null)
				 {
				 s.append('#');
				 s.append(getSequenceNumber());
				 s.append(' ');
				 }
				 */
	
				if (isHeartBeat())
				{
					s.append("HEARTBEAT");
				}
				else
				{
					s.append((String) getValue(0));
					if (hasField(11))
					{
						s.append(' ');
						s.append((String) getValue(11));
					}
					if (hasField(41))
					{
						s.append(' ');
						s.append((String) getValue(41));
					}
				}
			}
			catch (ISOException e)
			{
			}
		}

		return s.toString();
	}

	public boolean isHeartBeat()
	{
		return !hasField(0);
	}
}
