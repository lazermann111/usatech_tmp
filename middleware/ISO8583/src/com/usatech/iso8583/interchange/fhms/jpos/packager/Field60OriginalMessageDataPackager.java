/*
 * Created on May 18, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field60OriginalMessageDataPackager extends ISOBasePackager //FHMSSubFieldPackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharLeftZeroPadded(4, "ORIGINAL MESSAGE TYPE"), new FixedLengthCharLeftZeroPadded(6, "ORIGINAL SYSTEMS TRACE AUDIT NUMBER"), new FixedLengthCharRightSpacePadded(12, "RESERVED FOR FUTURE USE") };

	public Field60OriginalMessageDataPackager()
	{
		super();
		setFieldPackager(fld);
	}
}
