/*
 * Created on May 23, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63CashierData10Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63CashierData10 extends ISOMsg
{
	private static final long serialVersionUID = 3257844394139596086L;
	private static final int TLV_TAG = 10;
	private Field63CashierData10Packager msgPackager;

	public Field63CashierData10() throws ISOException
	{
		super();
		//this.set(2, "");
		msgPackager = new Field63CashierData10Packager();
		this.setPackager(msgPackager);
	}

	public String getCashierNumber() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setCashierNumber(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
