package com.usatech.iso8583.interchange.fhms;

import java.net.SocketTimeoutException;
import java.util.*;

import com.usatech.iso8583.*;
import com.usatech.iso8583.interchange.fhms.jpos.message.FHMSISOMsg;
import com.usatech.iso8583.transaction.ISO8583Transaction;

import org.apache.commons.logging.*;
import org.jpos.iso.ISOException;

public class SettlementAction extends FHMSAction
{
	private static Log log = LogFactory.getLog(SettlementAction.class);

	private static int[] requiredFields = { ISO8583Message.FIELD_ACQUIRER_ID, ISO8583Message.FIELD_TERMINAL_ID, ISO8583Message.FIELD_TRACE_NUMBER, ISO8583Message.FIELD_BATCH_NUMBER, ISO8583Message.FIELD_BATCH_TOTALS };

	public SettlementAction(FHMSInterchange interchange)
	{
		super(interchange);
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException
	{
		super.validateRequest(request, requiredFields);

		BatchTotals batchTotals = request.getBatchTotals();
		if (batchTotals.getCreditSaleCount() < 0 || batchTotals.getCreditSaleCount() > 999)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Sale Count");

		if (batchTotals.getCreditSaleAmount() < 0)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Sale Amount");

		if (batchTotals.getCreditRefundCount() < 0 || batchTotals.getCreditRefundCount() > 999)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Refund Count");

		if (batchTotals.getCreditRefundAmount() < 0)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Refund Amount");

		if (request.hasTransactionList())
		{
			Iterator listIter = request.getTransactionList().iterator();
			while (listIter.hasNext())
			{
				ISO8583Request subRequest = (ISO8583Request) listIter.next();

				// what to do?
			}
		}
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		ISO8583Request request = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);

		FHMSISOMsg isoRequest = null;

		try
		{
			validateRequest(request, requiredFields);

			isoRequest = new FHMSISOMsg();

			isoRequest.setMTI("0500");

			if (request.getTransactionType().equals(ISO8583Message.TRANSACTION_TYPE_SETTLEMENT_RETRY) || request.hasTransactionList())
				isoRequest.setProcessingCode("960000");
			else
				isoRequest.setProcessingCode("920000");

			isoRequest.setCardAcceptorAcquirerID(request.getAcquirerID());
			isoRequest.setCardAcceptorTerminalID(request.getTerminalID());
			isoRequest.setSystemTraceNumber(Integer.toString(request.getTraceNumber()));
			isoRequest.setBatchNumber(Integer.toString(request.getBatchNumber()));

			BatchTotals batchTotals = request.getBatchTotals();
			isoRequest.setReconciliationRequestTotals(Integer.toString(batchTotals.getCreditSaleCount()), Integer.toString(batchTotals.getCreditSaleAmount()), Integer.toString(batchTotals.getCreditRefundCount()), Integer.toString(batchTotals.getCreditRefundAmount()));
		}
		catch (ValidationException e)
		{
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		}
		catch (ISOException e)
		{
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Caught unexpected exception validating " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception validating request: " + e.getMessage());
		}

		if (request.hasTransactionList())
		{
			List uploads = request.getTransactionList();
			try
			{
				int uploadCounter = 0;
				int uploadCount = uploads.size();
				Iterator listIter = uploads.iterator();
				while (listIter.hasNext())
				{
					ISO8583Request subRequest = (ISO8583Request) listIter.next();

					log.info("Processing TransactionUploadRequest " + (++uploadCounter) + "/" + uploadCount + " for: " + transaction);

					ISO8583Transaction subTran = getInterchange().getTransactionManager().start(subRequest, getInterchange().getName());
					if (subTran == null)
					{
						log.error("TransactionUploadRequest " + uploadCounter + "/" + uploadCount + " failed: Failed to start a new sub-transaction, aborting processing and returing error response!");
						throw new ISOException("TransactionUploadRequest " + uploadCounter + "/" + uploadCount + " failed: Failed to start a new sub-transaction, aborting processing and returing error response!");
					}

					log.info("SUB-REQUEST: " + subTran + ": " + subRequest);
					ISO8583Response subResponse = getInterchange().process(subTran);
					getInterchange().getTransactionManager().end(subTran);
					log.info("SUB-RESPONSE: " + subTran + ": " + subResponse);

					if (subResponse.isInternalError())
					{
						log.error("TransactionUploadRequest " + uploadCounter + "/" + uploadCount + " failed: " + subResponse.getResponseMessage());
						throw new ISOException("TransactionUploadRequest " + uploadCounter + "/" + uploadCount + " failed: " + subResponse.getResponseMessage());
					}
				}
			}
			catch (ISOException e)
			{
				transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
				log.error(transaction + " sub-transaction failed input validation: " + e.getMessage(), e);
				return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Reqest sub-transaction failed input validation: " + e.getMessage());
			}
			catch (Throwable e)
			{
				transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
				log.error("Caught unexpected exception validating sub-transaction " + transaction + ": " + e.getMessage(), e);
				return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception validating sub-transaction request: " + e.getMessage());
			}
		}

		transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);

		FHMSISOMsg isoResponse = null;

		try
		{
			isoResponse = sendRequest(isoRequest, transaction, (request.getHostResponseTimeout()*1000));
		}
		catch (SocketTimeoutException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.warn("Timeout occured waiting for host response for " + transaction);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, "Timeout occured waiting for host response.");
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception transmitting " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting " + transaction + ": " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);

		ISO8583Response response = null;

		try
		{
			response = readResponse(isoResponse);
		}
		catch (ValidationException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host response failed input validation: " + e.getMessage());
		}
		catch (ISOException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host response failed input validation: " + e.getMessage());
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception while constructing response: " + transaction.getTransactionID() + ": " + e.getMessage() + ", response: " + isoResponse, e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);

		return response;
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		log.info("Skipping recovery of " + transaction + ": Not an online transaction");
		return true;
	}
	
	protected FHMSISOMsg createSimulatedResponse(FHMSISOMsg isoRequest)
	{
		try
		{
			FHMSISOMsg isoResponse = new FHMSISOMsg();
			
			isoResponse.setMTI("0510");
			isoResponse.setProcessingCode(isoRequest.getProcessingCode());
			isoResponse.setCardAcceptorAcquirerID(isoRequest.getCardAcceptorAcquirerID());
			isoResponse.setCardAcceptorTerminalID(isoRequest.getCardAcceptorTerminalID());
			isoResponse.setSystemTraceNumber(isoRequest.getSystemTraceNumber());
			Date now = new Date();
			isoResponse.setTransactionLocalDate(FHMSUtil.getMMDD(now));
			isoResponse.setTransactionLocalTime(FHMSUtil.getHHmmss(now));
			isoResponse.setResponseCode("00");
			isoResponse.setAlternateHostResponse("BATCH COMPLETE");
			
			return isoResponse;
		}
		catch(ISOException e)
		{
			log.error("Failed to create simulated response: " + e.getMessage(), e);
			return null;
		}
	}
}
