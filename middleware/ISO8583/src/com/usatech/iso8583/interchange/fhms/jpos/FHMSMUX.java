package com.usatech.iso8583.interchange.fhms.jpos;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

import com.usatech.iso8583.interchange.fhms.jpos.message.FHMSISOMsg;
import com.usatech.iso8583.jpos.USATISOChannel;
import com.usatech.iso8583.jpos.USATISOMUX;

public class FHMSMUX extends USATISOMUX
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(FHMSMUX.class);

	protected long lastReset = -99;
	protected AtomicInteger	sequenceNumberCounter = new AtomicInteger(1);

	public FHMSMUX(USATISOChannel channel, ThreadPoolExecutor threadPool, boolean doConnect)
	{
		super(channel, threadPool, doConnect);
	}

	protected synchronized String getKey(ISOMsg isoMsg) throws ISOException
	{
		if(getISOChannel().getConnectTime() > lastReset)
		{
			lastReset = System.currentTimeMillis();
			sequenceNumberCounter.set(1);
		}
		
		FHMSISOMsg fhmsMsg = (FHMSISOMsg) isoMsg;

		if (fhmsMsg.getDirection() == ISOMsg.INCOMING)
		{
			String key = fhmsMsg.getSequenceNumber();
			if (!key.equals("0000"))
				log.debug("Incoming MUX key: " + key); // don't log heartbeats
			return key;
		}

		String sequenceNumber = ISOUtil.zeropad(Integer.toString(sequenceNumberCounter.getAndIncrement()), 4);
		sequenceNumberCounter.compareAndSet(10000, 1);

		log.debug("Outgoing MUX key: " + sequenceNumber);

		fhmsMsg.setSequenceNumber(sequenceNumber);
		return sequenceNumber;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("FHMSMUX[");
		sb.append("persistent=false");
		sb.append(" doConnect="+super.getConnect());
		sb.append(" connected="+super.isConnected());
		sb.append(" terminating="+super.isTerminating());
		sb.append(" sequenceNumberCounter="+sequenceNumberCounter.get());
		sb.append("]");
		return sb.toString();
	}
}
