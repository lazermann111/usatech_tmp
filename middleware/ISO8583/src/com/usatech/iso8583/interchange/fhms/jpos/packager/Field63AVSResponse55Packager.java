/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63AVSResponse55Packager extends ISOBasePackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharRightSpacePadded(1, "ADDRESS MATCH"), new FixedLengthCharRightSpacePadded(1, "ZIP CODE MATCH"), new FixedLengthCharRightSpacePadded(1, "ADDRESS RESPONSE CODE") };

	public Field63AVSResponse55Packager()
	{
		super();
		setFieldPackager(this.fld);
	}
}
