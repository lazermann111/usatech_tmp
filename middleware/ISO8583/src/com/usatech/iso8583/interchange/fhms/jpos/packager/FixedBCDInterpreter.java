package com.usatech.iso8583.interchange.fhms.jpos.packager;

/**
 * Fixed version of jPOS's BCDInterpreter
 * 
 * <pre>
 * jPOS Bug Report:
 * 
 * jPOS version: All
 * Java version: All
 * OS: All
 * Stack Trace: Does not throw an exception
 * Test Program: Attached
 * 
 * Description: A bug exists in the BCDInterpreter using the RIGHT_PADDED_F format.  In a case where the String value 
 * being converted and padded ends with a 0 (zero), the RIGHT_PADDED_F BCDInterpreter replaces the trailing zero 
 * with an F, resulting in a corrupted value.
 * 
 * Solution: Replace the interpret method in BCDInterpreter with:
 * 
 * public void interpret(String data, byte[] b, int offset)
 * {
 * 		ISOUtil.str2bcd(data, leftPadded, b, offset);
 * 	
 * 		if (fPadded && !leftPadded && data.length()%2 == 1)
 * 			b[b.length - 1] |= (byte) (b[b.length - 1] << 4) == 0x00 ? 0x0F : 0x00;
 * }
 * </pre>
 */

import org.jpos.iso.*;

public class FixedBCDInterpreter implements Interpreter
{
	/** This BCDInterpreter sometimes adds a 0-nibble to the left. */
	public static final FixedBCDInterpreter	LEFT_PADDED		= new FixedBCDInterpreter(true, false);
	/** This BCDInterpreter sometimes adds a 0-nibble to the right. */
	public static final FixedBCDInterpreter	RIGHT_PADDED	= new FixedBCDInterpreter(false, false);
	/** This BCDInterpreter sometimes adds a F-nibble to the right. */
	public static final FixedBCDInterpreter	RIGHT_PADDED_F	= new FixedBCDInterpreter(false, true);
	/** This BCDInterpreter sometimes adds a F-nibble to the left. */
	public static final FixedBCDInterpreter	LEFT_PADDED_F	= new FixedBCDInterpreter(true, true);

	private boolean						leftPadded;
	private boolean						fPadded;

	/** Kept private. Only two instances are possible. */
	private FixedBCDInterpreter(boolean leftPadded, boolean fPadded)
	{
		this.leftPadded = leftPadded;
		this.fPadded = fPadded;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.jpos.iso.Interpreter#interpret(java.lang.String)
	 */
	public void interpret(String data, byte[] b, int offset)
	{
		ISOUtil.str2bcd(data, leftPadded, b, offset);
		
		if (fPadded && !leftPadded && data.length()%2 == 1)
			b[b.length - 1] |= (byte) (b[b.length - 1] << 4) == 0x00 ? 0x0F : 0x00;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.jpos.iso.Interpreter#uninterpret(byte[])
	 */
	public String uninterpret(byte[] rawData, int offset, int length)
	{
		return ISOUtil.bcd2str(rawData, offset, length, leftPadded);
	}

	/**
	 * Each numeric digit is packed into a nibble, so 2 digits per byte, plus the
	 * possibility of padding.
	 * 
	 * @see org.jpos.iso.Interpreter#getPackedLength(int)
	 */
	public int getPackedLength(int nDataUnits)
	{
		return (nDataUnits + 1) / 2;
	}
}

