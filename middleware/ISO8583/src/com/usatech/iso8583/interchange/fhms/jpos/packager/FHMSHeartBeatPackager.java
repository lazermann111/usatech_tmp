package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class FHMSHeartBeatPackager extends ISOBasePackager
{
	public FHMSHeartBeatPackager()
	{
		super();

	}

	protected int getMaxValidField()
	{
		return 0;
	}

	public byte[] pack(ISOComponent m) throws ISOException
	{
		return new byte[] {};
	}
}
