/**
 * Copyright *2000, GlobalPlatform, Inc., all rights reserved.
 * The Technology provided or described herein is subject to updates,
 * revisions, and extensions by GlobalPlatform.  Use of this information is
 * governed by the GlobalPlatform License Agreement and any use inconsistent
 * with that Agreement is strictly prohibited.
 **/

package com.usatech.iso8583.interchange.fhms.tlv;

import java.util.Vector;

/**
 * The <code>TLVCollection</code> class is used to store <code>ISORawTLV</code>
 * objects. It is usually used by 'inserting' in the <code>TLVCollection</code> different <code>ISORawTLV</code> objects which are decomposed into
 * primitive <code>ISORawTLV</code> objects ; these <code>ISORawTLV</code> objects are then stored.
 * @author Nicolas Juillard
 * @version 1.5.4 
 */

public class ISOTLVCollection
{
	Vector vector;

	private int tagToFind = 0;
	private int indexLastOccurrence = 0;
	private ISOLengthValue dummyLengthValue;

	static final int CAPACITY = 50;
	static final int INCREMENT = 20;

	private boolean doubleOK = true;

	/**
	 * Creates a new empty <code>ISOTLVCollection</code> object.
	 * @param doubleOK indicates if two <code>ISORawTLV</code> objects with the same tag can be inserted in this <code>ISOTLVCollection</code> object
	 */
	public ISOTLVCollection(boolean doubleOK)
	{
		vector = new Vector(CAPACITY, INCREMENT);
		this.doubleOK = doubleOK;
		dummyLengthValue = new ISOLengthValue();
	}

	/**
	 *Adds a primitive <code>ISORawTLV</code> to the <code>ISOTLVCollection</code> object.
	 *@param primitiveISORawTLV the <code>ISORawTLV</code> object to add.
	 *@exception TLVException INVALID_DATA if the ISORawTLV does not have a TLV coherent structure<br>
	 DOUBLE_ELEMENT if a <code>ISORawTLV</code> object with the same tag is already stored
	 */
	public void addPrimitiveTLV(ISORawTLV primitiveISORawTLV) throws ISOTLVException
	{
		primitiveISORawTLV.updateTagLength();
		vector.addElement(primitiveISORawTLV);
	}

	/**
	 *Adds a primitive <code>ISORawTLV</code> to the <code>ISOTLVCollection</code> object with the format
	 *template, tag and value as a byte array.
	 *@param template value of the template of the TLV to be added. If there is not template value, the value 0 should be used.
	 *@param tag value of the tag of the TLV to be added.
	 *@param value value as a byte array. The length of the TLV is the length of the value parameter.
	 *@exception TLVException INVALID_DATA if the ISORawTLV does not have a TLV coherent structure<br>
	 DOUBLE_ELEMENT if a <code>ISORawTLV</code> object with the same tag is already stored
	 */
	public void addTLV(int template, int tag, byte[] value) throws ISOTLVException
	{
		vector.addElement(new ISORawTLV(template, tag, value));
	}

	/**
	 *Adds a <code>ISORawTLV</code> to the <code>ISOTLVCollection</code> object.
	 *The <code>ISORawTLV</code> object is, if necessary, first splitted into a series of
	 *primitive <code>ISORawTLV</code> objects which are then added to the
	 *<code>ISOTLVCollection</code> object.
	 *@param constructedISORawTLV the <code>ISORawTLV</code> object to add.
	 *@exception TLVException INVALID_DATA if the ISORawTLV does not have a TLV coherent structure<br>
	 DOUBLE_ELEMENT if a <code>ISORawTLV</code> object with the same tag is already stored
	 */
	public void addConstructedTLV(ISORawTLV constructedISORawTLV) throws ISOTLVException
	{
		ISORawTLV subISORawTLV;
		byte[] value;
		int tag = constructedISORawTLV.getTag();
		int index, objectLength;

		constructedISORawTLV.updateTagLength();
		addISORawTLV(0, constructedISORawTLV.dataArray, 0, constructedISORawTLV.dataArrayLength, constructedISORawTLV.tagValue, constructedISORawTLV.valueIndex, constructedISORawTLV.lengthIndex);
	}

	/**
	 * Core methods used by the add... methods
	 * @param template value of the template of the surrounding TLV
	 * @param array byte array to be added
	 * @param offset offset in dataArray for the start of the search
	 * @param length length in dataArray from offset where to search
	 * @param tag value of the tag of the TLV which begins at offset
	 * @param valueIndex index of the value of the TLV which begins at offset
	 * @param tagLength length in bytes of the tag of the TLV which begins at offset
	 * @exception TLVException if the object does not respect the TLV structure
	 */

	void addISORawTLV(int template, byte[] array, int offset, int length, int tag, int valueIndex, int tagLength) throws ISOTLVException
	{
		ISORawTLV tlv;

		if (ISORawTLV.isPrimitiveTag(tag))
		{
			tlv = new ISORawTLV(array, offset, length);
			tlv.tagValue = tag;
			tlv.valueIndex = valueIndex;
			tlv.lengthIndex = tagLength;
			tlv.valueLength = length - valueIndex;
			tlv.template = template;
			vector.addElement(tlv);
		}
		else
		{
			int subTag, subTagLength, subObjectLength, index;

			index = offset + valueIndex;
			while (index < (offset + length))
			{
				ISORawTLV.getTag(array, index, dummyLengthValue);
				subTag = dummyLengthValue.value;
				subTagLength = dummyLengthValue.length;
				ISORawTLV.getLength(array, index + subTagLength, dummyLengthValue);
				subObjectLength = subTagLength + dummyLengthValue.length + dummyLengthValue.value;
				addISORawTLV(tag, array, index, subObjectLength, subTag, subTagLength + dummyLengthValue.length, subTagLength);
				index += subObjectLength;
			}
		}
	}

	/**
	 *finds the first occurrence of a TLV object with a given tag within the <code>ISOTLVCollection</code> object and returns the 
	 *corresponding <code>ISORawTLV</code> object. This method reinitializes the findNextTLV method.
	 *@param tag primitive tag to find in the <code>ISOTLVCollection</code> object.
	 *@return the <code>ISORawTLV</code> object corresponding to that tag. It returns <code>null</code> if the tag is not found
	 */
	public ISORawTLV findFirstTLV(int tag)
	{
		int i = 0;

		tagToFind = tag;
		while (i < vector.size())
		{
			ISORawTLV tlv = (ISORawTLV) (vector.elementAt(i));
			if (tlv.tagValue == tag)
			{
				indexLastOccurrence = i;
				return (tlv);
			}
			else
				i++;
		}
		indexLastOccurrence = i;
		return null;
	}

	/**
	 * finds the next <code>ISORawTLV</code> object within the <code>ISOTLVCollection</code> object and 
	 * with the tag initialized by the last call to <code>findFirstTLV</code> and returns it.
	 * @return the <code>ISORawTLV</code> object corresponding to that tag. It returns <code>null</code> if the tag is not found
	 */
	public ISORawTLV findNextTLV()
	{
		int i = indexLastOccurrence + 1;

		if (!doubleOK)
			return null;
		while (i < vector.size())
		{
			ISORawTLV tlv = (ISORawTLV) (vector.elementAt(i));
			if (tlv.tagValue == tagToFind)
			{
				indexLastOccurrence = i;
				return (tlv);
			}
			else
				i++;
		}
		return null;
	}

	/**
	 * Retrieves the data from the <code>ISOTLVCollection</code> object corresponding
	 * to the Data Object List (DOL) given as an argument. The DOL is primitive data object with a Value which contains a list
	 * of primitive objects tags + lengths. The method returns the
	 * Value matching these tags in the <code>ISOTLVCollection</code> object and concatenates them in the same order as the tags
	 * were read in the DOL. It is assumed that the objects in the <code>ISOTLVCollection</code> have the right format and length
	 * relatively to the DOL ; otherwise an exception is thrown.
	 * @param DOL the Data Object List encapsulated in a <code>ISORawTLV</code> object.
	 * @return a byte array containing the Value data corresponding to the tags
	 *		given in the DOL.<br>
	 *		By default, for a given tag, the first occurence of the PrimitiveTLV in <code>ISOTLVCollection</code> is used. If the tag is not present,
	 *		the Value will be filled with 0x00 with the length precised in the DOL.
	 * @exception TLVException INVALID_DATA if invalid data is encountered
	 */
	public byte[] getDOLData(ISORawTLV DOL) throws ISOTLVException
	{
		byte[] DOLArray;
		int index;
		Vector tagLengthStorage = new Vector(CAPACITY, INCREMENT);
		int DOLDataLength = 0;
		byte[] DOLdata;
		int i = 0;
		ISORawTLV tlv;
		TagLength tagLength;

		DOL.updateTagLength();
		index = DOL.valueIndex;
		DOLArray = DOL.dataArray;
		while (index < DOL.dataArrayLength)
		{
			tagLength = new TagLength();
			ISORawTLV.getTag(DOLArray, index, dummyLengthValue);
			tagLength.tag = dummyLengthValue.value;
			index += dummyLengthValue.length;
			ISORawTLV.getLength(DOLArray, index, dummyLengthValue);
			tagLength.length = dummyLengthValue.value;
			index += dummyLengthValue.length;
			DOLDataLength += tagLength.length;
			tagLengthStorage.addElement(tagLength);
		}

		DOLdata = new byte[DOLDataLength];
		index = 0;

		while (i < tagLengthStorage.size())
		{
			tagLength = (TagLength) (tagLengthStorage.elementAt(i));

			if ((tlv = findFirstTLV(tagLength.tag)) != null)
			{
				if (tlv.valueLength == tagLength.length)
				{
					System.arraycopy(tlv.dataArray, tlv.valueIndex, DOLdata, index, tagLength.length);
					index += tagLength.length;
				}
				else
					throw new ISOTLVException(ISOTLVException.INVALID_DATA);
			}
			else
			{
				for (int j = 0; j < tagLength.length; j++)
					DOLdata[index + j] = (byte) 0x00;
				index += tagLength.length;
			}
			i++;
		}
		return (DOLdata);
	}

	/**
	 * Deletes a <code>ISORawTLV</code> object within this <code>ISOTLVCollection</code>. It is generally used
	 * after getting back the element with the <code>find...TLV</code> methods.
	 * @param tlv <code>ISORawTLV</code> object to delete
	 * @exception TLVException INVALID_DATA if <code>ISORawTLV</code> is incorrect or not present in this <code>ISOTLVCollection</code> object.
	 */
	public void delete(ISORawTLV tlv) throws ISOTLVException
	{
		int i = 0;
		boolean result, isTLVfound = false;
		ISORawTLV tlv2;

		while (i < vector.size())
		{
			tlv2 = (ISORawTLV) (vector.elementAt(i));
			if (tlv2.equals(tlv))
			{
				vector.removeElement(tlv2);
				isTLVfound = true;
			}
			else
				i++;
		}
		if (!isTLVfound)
			throw new ISOTLVException(ISOTLVException.INVALID_DATA);
	}
}

//inner helper class

class TagLength
{
	int tag;
	int length;
}
