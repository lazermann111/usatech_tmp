/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63TipAmount38Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63TipAmount38 extends ISOMsg
{
	private static final long serialVersionUID = 3257849865911676981L;
	private static final int TLV_TAG = 38;
	private Field63TipAmount38Packager msgPackager;

	public Field63TipAmount38() throws ISOException
	{
		super();
		//this.set(2, "");
		msgPackager = new Field63TipAmount38Packager();
		this.setPackager(msgPackager);
	}

	public String getTipAmount() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setTipAmount(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
