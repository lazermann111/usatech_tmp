/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63TaxAmount39Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63TaxAmount39 extends ISOMsg
{
	private static final long serialVersionUID = 3257562906177777716L;
	private static final int TLV_TAG = 39;
	private Field63TaxAmount39Packager msgPackager;

	public Field63TaxAmount39() throws ISOException
	{
		super();
		//this.set(2, "");
		msgPackager = new Field63TaxAmount39Packager();
		this.setPackager(msgPackager);
	}

	public String getTaxAmount() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setTaxAmount(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
