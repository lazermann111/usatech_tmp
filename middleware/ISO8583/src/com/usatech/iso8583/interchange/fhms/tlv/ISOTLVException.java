/**
 * Copyright *2000, GlobalPlatform, Inc., all rights reserved.
 * The Technology provided or described herein is subject to updates,
 * revisions, and extensions by GlobalPlatform.  Use of this information is
 * governed by the GlobalPlatform License Agreement and any use inconsistent
 * with that Agreement is strictly prohibited.
 **/

package com.usatech.iso8583.interchange.fhms.tlv;

/**
 * @author Christophe Colas
 * @version 1.5.4 
 */

public class ISOTLVException extends RuntimeException
{

	private static final long serialVersionUID = 3256436997815088689L;

	byte code;

	/**
	 * Default exception code
	 */
	public static final byte DEFAULT = 0x00;

	/**
	 * Used when incoherent TLV data struture are encountered
	 */
	public static final byte INVALID_DATA = 0x01;

	/**
	 * Conversion Error
	 */
	public static final byte DATA_CONVERSION_ERROR = 0x02;

	/**
	 * 'findFirst...' was not called prior to 'findNext...'
	 */
	public static final byte FIND_NOT_INITIALIZED = 0x03;

	/**
	 * Double element was found
	 */
	public static final byte DOUBLE_ELEMENT = 0x04;

	ISOTLVException(byte code)
	{

		this.code = code;
	}

	public byte getCode()
	{
		return code;
	}

}
