/**
 * Copyright *2000, GlobalPlatform, Inc., all rights reserved.
 * The Technology provided or described herein is subject to updates,
 * revisions, and extensions by GlobalPlatform.  Use of this information is
 * governed by the GlobalPlatform License Agreement and any use inconsistent
 * with that Agreement is strictly prohibited.
 **/

package com.usatech.iso8583.interchange.fhms.tlv;

/**
 * This class defines a reference to a 'sub-part' of a <code>RawTLV</code> object. This class is used mainly in the <code>find...</code>
 * methods of <code>RawTLV</code> and prevents some unnecessary creation of temporary objects.
 * @author Christophe Colas
 * @version 1.5.4
 */
public class ISOSubRawTLV
{

	ISORawTLV tlv;
	int tagValue; // leading Tag value
	int valueLength; // length in bytes of the value fied
	int lengthIndex; // index in dataArray where the length is coded
	int valueIndex; // index in dataArray where the value is coded
	int template; // template of the RawTLV
	boolean isLengthUpdated = true; //true if valueLength is really the length coded in dataArray
	//false otherwise like, for example, if TLVs has been added
	int offset, length;

	ISOSubRawTLV(ISORawTLV tlv, int offset, int length, int template, int tag, int tagLength, int valueIndex)
	{
		this.tlv = tlv;
		this.offset = offset;
		this.length = length;
		this.tagValue = tag;
		this.valueIndex = valueIndex;
		this.valueLength = length - valueIndex;
		this.lengthIndex = tagLength;
		this.template = template;
	}

	/**
	 * Returns the value part of this TLV <code>ISOSubRawTLV</code> object.
	 * @return value V as a byte array
	 */
	public byte[] getValue()
	{
		byte[] array = new byte[valueLength];
		System.arraycopy(tlv.dataArray, offset + valueIndex, array, 0, valueLength);
		return array;
	}

	/**
	 * Creates and returns the <code>RawTLV</code> defined by this <code>ISOSubRawTLV</code> object.
	 * @return the conversion of this object into a <code>RawTLV</code>
	 */
	public ISORawTLV getRawTLV()
	{
		ISORawTLV rawTLV = new ISORawTLV(tlv.dataArray, offset, length);
		rawTLV.tagValue = tagValue;
		rawTLV.valueLength = valueLength;
		rawTLV.lengthIndex = lengthIndex;
		rawTLV.valueIndex = valueIndex;
		rawTLV.template = template;
		return rawTLV;
	}
}
