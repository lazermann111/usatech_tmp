/*
 * Created on May 18, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63ReconciliationRequestTotalsPackager extends ISOBasePackager //FHMSSubFieldPackager
{
	private ISOFieldPackager fld[] = 
	{ 
		new IFA_NUMERIC(0, "MTI PLACEHOLDER"), 
		new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), 
		new FixedLengthCharLeftZeroPadded(3, "CAPTURED SALES COUNT"), 
		new FixedLengthCharLeftZeroPadded(12, "SALES AMOUNT"), 
		new FixedLengthCharLeftZeroPadded(3, "REFUND COUNT"), 
		new FixedLengthCharLeftZeroPadded(12, "REFUND AMOUNT"), 
		new FixedLengthCharLeftZeroPadded(3, "DEBIT SALES COUNT"), 
		new FixedLengthCharLeftZeroPadded(12, "DEBIT SALES AMOUNT"),
		new FixedLengthCharLeftZeroPadded(3, "DEBIT REFUND COUNT"), 
		new FixedLengthCharLeftZeroPadded(12, "DEBIT REFUND AMOUNT"), 
		new FixedLengthBCDLeftZeroPadded(120, "UNUSED") // 120 because it's BCD
	};

	public Field63ReconciliationRequestTotalsPackager()
	{
		super();
		setFieldPackager(fld);
	}
}
