package com.usatech.iso8583.interchange.fhms.tlv;

/*
 * Copyright (c) 2000 jPOS.org.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *    "This product includes software developed by the jPOS project
 *    (http://www.jpos.org/)". Alternately, this acknowledgment may
 *    appear in the software itself, if and wherever such third-party
 *    acknowledgments normally appear.
 *
 * 4. The names "jPOS" and "jPOS.org" must not be used to endorse
 *    or promote products derived from this software without prior
 *    written permission. For written permission, please contact
 *    license@jpos.org.
 *
 * 5. Products derived from this software may not be called "jPOS",
 *    nor may "jPOS" appear in their name, without prior written
 *    permission of the jPOS project.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE JPOS PROJECT OR ITS CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the jPOS Project.  For more
 * information please see <http://www.jpos.org/>.
 */

import java.io.*;

import org.jpos.iso.*;

/**
 * ISOFieldPackager Binary LLLTLVBINARY
 *
 * @author bharavi gade
 * @see ISOComponent
 */
public class ISOIFB_LLLTLVBINARY extends ISOFieldPackager
{
	public ISOIFB_LLLTLVBINARY()
	{
		super();
	}

	/**
	 * @param len - field len
	 * @param description symbolic descrption
	 */
	public ISOIFB_LLLTLVBINARY(int len, String description)
	{
		super(len, description);
	}

	/**
	 * @param c - a component
	 * @return packed component
	 * @exception ISOException
	 */
	public byte[] pack(ISOComponent c) throws ISOException
	{
		int len = ((byte[]) c.getValue()).length;

		if (len > getLength() || len > 999) // paranoia settings
			throw new ISOException("invalid len " + len + " packing field " + (Integer) c.getKey());

		byte[] b = new byte[len + 2];
		String slen = ISOUtil.zeropad(Integer.toString(len), 3);

		byte[] l = ISOUtil.str2bcd(slen, true);
		b[0] = l[0];
		b[1] = l[1];
		System.arraycopy(c.getValue(), 0, b, 2, len);
		return b;
	}

	/**
	 * @param c - the Component to unpack
	 * @param b - binary image
	 * @param offset - starting offset within the binary image
	 * @return consumed bytes
	 * @exception ISOException
	 */
	public int unpack(ISOComponent c, byte[] b, int offset) throws ISOException
	{
		int len = (b[offset++] & 0x0F) * 100;
		len += (((b[offset] >> 4) & 0x0F) * 10) + (b[offset] & 0x0F);
		byte[] value = new byte[len];
		System.arraycopy(b, offset + 1, value, 0, len);
		c.setValue((Object) value);
		return len + 2;
	}

	public ISOComponent createComponent(int fieldNumber)
	{
		return new ISOTLVField(fieldNumber);
	}

	public int getMaxPackedLength()
	{
		return getLength() + 2;
	}

	public void unpack(ISOComponent c, InputStream in) throws IOException, ISOException
	{
		byte[] b = readBytes(in, 2);
		int len = (b[0] & 0x0F) * 100;
		len += (((b[1] >> 4) & 0x0F) * 10) + (b[1] & 0x0F);
		c.setValue((Object) readBytes(in, len));
	}
}
