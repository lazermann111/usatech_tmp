/*
 * Created on May 20, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63CashierData10Packager extends ISOBasePackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharRightSpacePadded(4, "CASHIER NUMBER") };

	public Field63CashierData10Packager()
	{
		super();
		setFieldPackager(this.fld);
	}
}
