package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class FixedLengthBCDLeftZeroPadded extends ISOStringFieldPackager
{
	public FixedLengthBCDLeftZeroPadded()
	{
		super(LeftPadder.ZERO_PADDER, BCDInterpreter.LEFT_PADDED, NullPrefixer.INSTANCE);
	}

	/**
	 * @param len - field len
	 * @param description symbolic descrption
	 */
	public FixedLengthBCDLeftZeroPadded(int len, String description)
	{
		super(len, description, LeftPadder.ZERO_PADDER, BCDInterpreter.LEFT_PADDED, NullPrefixer.INSTANCE);
	}
}
