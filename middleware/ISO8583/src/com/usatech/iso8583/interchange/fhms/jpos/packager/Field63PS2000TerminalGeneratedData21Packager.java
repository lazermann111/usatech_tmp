/*
 * Created on May 20, 2005
 */
/*
 * Created on May 20, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63PS2000TerminalGeneratedData21Packager extends ISOBasePackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharLeftZeroPadded(12, "AUTHORIZED AMOUNT") };

	public Field63PS2000TerminalGeneratedData21Packager()
	{
		super();
		setFieldPackager(this.fld);
	}
}
