/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field62PurchaseOrderNumberPackager;

import org.jpos.iso.*;

public class Field62PurchaseOrderNumber extends ISOMsg
{
	private static final long serialVersionUID = 4120849962629214775L;
	private Field62PurchaseOrderNumberPackager msgPackager;

	public Field62PurchaseOrderNumber() throws ISOException
	{
		super();
		setFieldNumber(62);
		//this.set(2, "");
		msgPackager = new Field62PurchaseOrderNumberPackager();
		this.setPackager(msgPackager);
	}

	public Field62PurchaseOrderNumber(int fieldNumber) throws ISOException
	{
		this();
		setFieldNumber(fieldNumber);
	}

	public String getPurchaseOrderNumber() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setPurchaseOrderNumber(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToISOMsg(ISOMsg msg) throws ISOException
	{
		msg.set(fieldNumber, this.pack());
	}

	public void getFromISOMsg(ISOMsg msg) throws ISOException
	{
		this.unpack(ISOUtil.hex2byte(msg.getString(fieldNumber)));
	}
}
