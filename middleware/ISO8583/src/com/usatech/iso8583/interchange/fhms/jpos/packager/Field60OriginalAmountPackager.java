/*
 * Created on May 18, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field60OriginalAmountPackager extends ISOBasePackager //FHMSSubFieldPackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"),
	//new FixedLengthCharRightSpacePadded(12, "ORIGINAL AMOUNT")
			new FixedLengthCharLeftZeroPadded(12, "ORIGINAL AMOUNT") };

	public Field60OriginalAmountPackager()
	{
		super();
		setFieldPackager(fld);
	}
}
