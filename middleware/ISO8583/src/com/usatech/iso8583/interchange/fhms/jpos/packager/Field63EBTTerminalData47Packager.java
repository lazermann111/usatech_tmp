/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63EBTTerminalData47Packager extends ISOBasePackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharRightSpacePadded(10, "CLERK ID"), new FixedLengthCharRightSpacePadded(10, "SUPERVISOR ID"), new FixedLengthCharRightSpacePadded(15, "VOUCHER NUMBER"), new FixedLengthCharRightSpacePadded(6, "GENERATION NUMBER") };

	public Field63EBTTerminalData47Packager()
	{
		super();
		setFieldPackager(this.fld);
	}
}
