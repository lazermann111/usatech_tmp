package com.usatech.iso8583.interchange.fhms;

import java.text.*;
import java.util.*;

import com.usatech.iso8583.*;

public class FHMSUtil
{
	// SimpleDateFormat is not thread safe!! - pcowan - 10/31/2006
	
	public static ThreadLocal<SimpleDateFormat> TRAN_DATE_FORMAT = new ThreadLocal<SimpleDateFormat>()
	{
		protected synchronized SimpleDateFormat initialValue()
		{
			return new SimpleDateFormat("MMdd");
		}
	};
	
	public static ThreadLocal<SimpleDateFormat> TRAN_TIME_FORMAT = new ThreadLocal<SimpleDateFormat>()
	{
		protected synchronized SimpleDateFormat initialValue()
		{
			return new SimpleDateFormat("HHmmss");
		}
	};
	
	public static ThreadLocal<SimpleDateFormat> TRAN_DATETIME_FORMAT = new ThreadLocal<SimpleDateFormat>()
	{
		protected synchronized SimpleDateFormat initialValue()
		{
			return new SimpleDateFormat("yyyyMMddHHmmss");
		}
	};

//	public static SimpleDateFormat TRAN_DATE_FORMAT = new SimpleDateFormat("MMdd");
//	public static SimpleDateFormat TRAN_TIME_FORMAT = new SimpleDateFormat("HHmmss");
//	public static SimpleDateFormat TRAN_DATETIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

	public static Date parseDateTime(String MMDD, String HHmmss) throws ParseException
	{
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		return TRAN_DATETIME_FORMAT.get().parse(Integer.toString(year) + MMDD + HHmmss);
	}

	public static String getMMDD(Date date)
	{
		return TRAN_DATE_FORMAT.get().format(date);
	}

	public static String getHHmmss(Date date)
	{
		return TRAN_TIME_FORMAT.get().format(date);
	}

	public static String buildPOSEntryModeString(String panEntryMode, PanData panData)
	{
		return buildPOSEntryModeString(panEntryMode, ISO8583Message.PIN_ENTRY_UNSPECIFIED, panData);
	}

	public static String buildPOSEntryModeString(String panEntryMode, String pinEntryCapability, PanData panData)
	{
		StringBuilder sb = new StringBuilder();

		if (panEntryMode.equalsIgnoreCase(ISO8583Message.ENTRY_MODE_MANUALLY_KEYED))
			sb.append("01");
		else if (panEntryMode.equalsIgnoreCase(ISO8583Message.ENTRY_MODE_SWIPED))
			sb.append("02");
		else if (panEntryMode.equalsIgnoreCase(ISO8583Message.ENTRY_MODE_CONTACTLESS_SWIPED))
		{
			// american express can not accept the rfid value 91 flag
			if(panData.getCardType() == PanData.AMERICAN_EXPRESS)
				sb.append("02");
			else
				sb.append("91");
		}
		else
			sb.append("00");

		if (pinEntryCapability == null)
			sb.append("0");
		else if (pinEntryCapability.equalsIgnoreCase(ISO8583Message.PIN_ENTRY_CAPABILITY))
			sb.append("1");
		else if (pinEntryCapability.equalsIgnoreCase(ISO8583Message.PIN_ENTRY_NO_CAPABILITY))
			sb.append("2");
		else
			sb.append("0");

		return sb.toString();
	}

	public static String buildPOSConditionCode(String posEnvironment)
	{
		if (posEnvironment.equalsIgnoreCase(ISO8583Message.POS_ENVIRONMENT_ATTENDED))
			return "00";
		else if (posEnvironment.equalsIgnoreCase(ISO8583Message.POS_ENVIRONMENT_MAIL_TELEPHONE))
			return "08";
		else
			return "00";
	}
}
