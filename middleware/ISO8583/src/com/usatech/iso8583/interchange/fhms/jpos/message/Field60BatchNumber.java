/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field60BatchNumberPackager;

import org.jpos.iso.*;

public class Field60BatchNumber extends ISOMsg
{
	private static final long serialVersionUID = 3689345524796897584L;
	private Field60BatchNumberPackager msgPackager;

	public Field60BatchNumber() throws ISOException
	{
		super();
		setFieldNumber(60);
		//this.set(2, "");
		msgPackager = new Field60BatchNumberPackager();
		this.setPackager(msgPackager);
	}

	public Field60BatchNumber(int fieldNumber) throws ISOException
	{
		this();
		setFieldNumber(fieldNumber);
	}

	public String getBatchNumber() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setBatchNumber(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToISOMsg(ISOMsg msg) throws ISOException
	{
		msg.set(fieldNumber, this.pack());
	}

	public void getFromISOMsg(ISOMsg msg) throws ISOException
	{
		this.unpack(ISOUtil.hex2byte(msg.getString(fieldNumber)));
	}
}
