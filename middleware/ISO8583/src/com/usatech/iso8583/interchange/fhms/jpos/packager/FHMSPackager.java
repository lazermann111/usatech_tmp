package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class FHMSPackager extends ISOBasePackager
{
	public FHMSPackager()
	{
		super();
		super.setFieldPackager(new ISOFieldPackager[getMaxValidField() + 1]);

		super.setFieldPackager(0, new FixedLengthBCDLeftZeroPadded(4, "MESSAGE TYPE INDICATOR"));
		super.setFieldPackager(1, new IFB_BITMAP(8, "BIT MAP"));
		super.setFieldPackager(2, new VariableLengthBCDRightFPadded(19, "PAN - PRIMARY ACCOUNT NUMBER"));
		super.setFieldPackager(3, new FixedLengthBCDLeftZeroPadded(6, "PROCESSING CODE"));
		super.setFieldPackager(4, new FixedLengthBCDLeftZeroPadded(12, "AMOUNT, TRANSACTION"));
		super.setFieldPackager(11, new FixedLengthBCDLeftZeroPadded(6, "SYSTEM TRACE NUMBER"));
		super.setFieldPackager(12, new FixedLengthBCDLeftZeroPadded(6, "TIME, LOCAL TRANSACTION"));
		super.setFieldPackager(13, new FixedLengthBCDLeftZeroPadded(4, "DATE, LOCAL TRANSACTION"));
		super.setFieldPackager(14, new FixedLengthBCDLeftZeroPadded(4, "DATE, EXPIRATION"));
		super.setFieldPackager(22, new FixedLengthBCDLeftZeroPadded(3, "POINT OF SERVICE ENTRY MODE"));
		super.setFieldPackager(24, new FixedLengthBCDLeftZeroPadded(3, "NETWORK INTERNATIONAL IDENTIFIER"));
		super.setFieldPackager(25, new FixedLengthBCDLeftZeroPadded(2, "POINT OF SERVICE CONDITION CODE"));
		//		super.setFieldPackager(35, new IFB_LLCHAR(37, "TRACK 2 DATA"));
		super.setFieldPackager(35, new VariableLengthBCDRightFPadded(37, "TRACK 2 DATA"));
		//		super.setFieldPackager(37, new FixedLengthCharRightSpacePadded(12, "RETRIEVAL REFERENCE NUMBER"));
		super.setFieldPackager(37, new FixedLengthCharLeftZeroPadded(12, "RETRIEVAL REFERENCE NUMBER"));
		super.setFieldPackager(38, new FixedLengthCharRightSpacePadded(6, "AUTHORIZATION IDENTIFICATION RESPONSE"));
		super.setFieldPackager(39, new FixedLengthCharRightSpacePadded(2, "RESPONSE CODE"));
		super.setFieldPackager(41, new FixedLengthCharRightSpacePadded(8, "CARD ACCEPTOR TERMINAL IDENTIFICATION"));
		//		super.setFieldPackager(41, new FixedLengthBCDLeftZeroPadded(8, "CARD ACCEPTOR TERMINAL IDENTIFICATION"));
		super.setFieldPackager(42, new FixedLengthCharRightSpacePadded(15, "CARD ACCEPTOR IDENTIFICATION CODE"));
		//		super.setFieldPackager(42, new FixedLengthBCDLeftZeroPadded(15, "CARD ACCEPTOR IDENTIFICATION CODE"));
		super.setFieldPackager(45, new IFB_LLCHAR(76, "TRACK 1 DATA"));
		super.setFieldPackager(52, new IFB_LLLBINARY(8, "PIN DATA"));
		super.setFieldPackager(54, new IFB_LLLCHAR(120, "ADDITIONAL AMOUNTS"));

		super.setFieldPackager(61, new NullFieldPackager()); //  we're using field 61 to store the sequence number, but it doesn't get packaged

		/*
		 super.setFieldPackager(60, new IFB_LLLBINARY(999, "RESERVED PRIVATE FIELD 60"));
		 super.setFieldPackager(62, new IFB_LLLBINARY(999, "RESERVED PRIVATE FIELD 62"));
		 super.setFieldPackager(63, new IFB_LLLBINARY(999, "RESERVED PRIVATE FIELD 63"));
		 */
	}

	protected int getMaxValidField()
	{
		return 63;
	}
}
