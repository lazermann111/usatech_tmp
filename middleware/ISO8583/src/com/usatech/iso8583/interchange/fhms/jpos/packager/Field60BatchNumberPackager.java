/*
 * Created on May 18, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field60BatchNumberPackager extends ISOBasePackager //FHMSSubFieldPackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"),
	//new FixedLengthCharRightSpacePadded(6, "BATCH NUMBER")
			new FixedLengthCharLeftZeroPadded(6, "BATCH NUMBER") };

	public Field60BatchNumberPackager()
	{
		super();
		setFieldPackager(fld);
	}
}
