/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63AddressVerificationData54Packager extends ISOBasePackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharRightSpacePadded(9, "ZIP CODE"), new FixedLengthCharRightSpacePadded(20, "ADDRESS") };

	public Field63AddressVerificationData54Packager()
	{
		super();
		setFieldPackager(this.fld);
	}
}
