package com.usatech.iso8583.interchange.fhms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequest;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.interchange.fhms.jpos.FHMSMUX;
import com.usatech.iso8583.interchange.fhms.jpos.FHMSPersistentMUX;
import com.usatech.iso8583.interchange.fhms.jpos.MerchantLinkFrameRelayChannel;
import com.usatech.iso8583.interchange.fhms.jpos.message.FHMSISOMsg;
import com.usatech.iso8583.interchange.fhms.jpos.packager.FHMSPackager;
import com.usatech.iso8583.jpos.SeparateLineLog4JListener;
import com.usatech.iso8583.jpos.USATChannelPool;
import com.usatech.iso8583.jpos.USATISOMUX;
import com.usatech.iso8583.transaction.HsqlISO8583TransactionManager;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;
import com.usatech.iso8583.util.ISO8583GatewayConfiguration;

public class FHMSInterchange implements ISO8583Interchange
{
	public static final String INTERCHANGE_NAME = "FHMS_TPS8583";

	private static org.apache.commons.logging.Log log = LogFactory.getLog(FHMSInterchange.class);

	public static void main(String args[]) throws Exception
	{
		Configuration config = new ISO8583GatewayConfiguration();
		ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		HsqlISO8583TransactionManager txnManager = new HsqlISO8583TransactionManager(threadPool, config);
		txnManager.start();
		FHMSInterchange interchange = new FHMSInterchange(threadPool, txnManager, config);

		interchange.start();

		Thread.sleep(25000);
		/*
		 System.out.println("Updating config");
		 Configuration newConfig = new ISO8583GatewayConfiguration();
		 interchange.setConfiguration(newConfig);

		 while(true)
		 Thread.sleep(15000);
		 */
		interchange.stop();
		//		txnManager.stop();
		//		threadPool.shutdownNow();
	}

	private boolean simulationMode = false;

	private boolean isStarted = false;
	private int responseTimeout;
	private long reconnectDelay;
	private Configuration config;

	private final ThreadPoolExecutor threadPool;
	private final ISO8583TransactionManager txnManager;
	private final Map<String, FHMSAction> actions = new HashMap<String, FHMSAction>();

	private List<String>hosts;
	private String testMerchant;
	private String testTerminal;
	private int jposLogLevel;

	private USATISOMUX mux;

	private ScheduledExecutorService executor;
	private ScheduledFuture future;

	public FHMSInterchange(ThreadPoolExecutor threadPool, ISO8583TransactionManager txnManager, Configuration config) throws ConfigurationException
	{
		this.threadPool = threadPool;
		this.config = config;
		this.txnManager = txnManager;

		if (config instanceof FileConfiguration)
		{
			((FileConfiguration) config).setReloadingStrategy(new ConfigReloader());
		}

		actions.put(ISO8583Message.TRANSACTION_TYPE_ADJUSTMENT, new AdjustAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_AUTHORIZATION, new AuthorizationAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_REFUND, new RefundAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_REVERSAL, new ReversalAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_SALE, new SaleAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_SETTLEMENT, new SettlementAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_SETTLEMENT_RETRY, new SettlementAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_TEST, new TestAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_UPLOAD, new UploadAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_VOID, new VoidAction(this));
	}

	public boolean isSimulationMode()
	{
		return simulationMode;
	}

	public String getName()
	{
		return INTERCHANGE_NAME;
	}

	public ISO8583TransactionManager getTransactionManager()
	{
		return txnManager;
	}

	public USATISOMUX getMUX()
	{
		return mux;
	}

	public ThreadPoolExecutor getThreadPool()
	{
		return threadPool;
	}

	@Override
	public String toString()
	{
		return "FHMSInterchange[started=" + isStarted() + " connected=" + isConnected() + "]";
	}

	public boolean canSend()
	{
		if(simulationMode)
			return true;

		if (mux == null)
			return false;
		if (mux.getISOChannel() == null)
			return false;
		if (mux.isTerminating())
			return false;
		if (!mux.isConnected()) {
			try {
				mux.getISOChannel().connect();
			} catch (Exception e) { }
			if (!mux.isConnected())
				return false;
		}

		return true;
	}

	public FHMSISOMsg send(FHMSISOMsg isoMsg)
	{
		return send(isoMsg, responseTimeout);
	}

	public FHMSISOMsg send(FHMSISOMsg isoMsg, int hostResponseTimeout)
	{
		ISORequest request = new ISORequest(isoMsg);

		mux.queue(request);

		if(hostResponseTimeout < 0)
			hostResponseTimeout = this.responseTimeout;

		return (FHMSISOMsg) request.getResponse(hostResponseTimeout);
	}

	public synchronized void setConfiguration(Configuration config)
	{
		if (isStarted)
			reloadConfig(config);
		else
			this.config = config;
	}

	public synchronized Configuration getConfiguration()
	{
		return config;
	}

	private synchronized void loadConfig()
	{
		log.info("Loading configuration...");

		simulationMode = config.getBoolean("fhms.simulationmode", false);
		if(simulationMode)
			log.warn("---------------------- SIMULATION MODE ENABLED! ----------------------");

		responseTimeout = config.getInt("fhms.response.timeout");
		if (responseTimeout < 0)
			log.warn("Configuration value fhms.response.timeout of " + responseTimeout + " looks invalid, interchange may not function properly!");

		hosts = new ArrayList<String>();
		for(Object h : config.getList("fhms.host"))
			if(h != null)
				hosts.add(h.toString());

		testMerchant = config.getString("fhms.test.merchantNumber");
		testTerminal = config.getString("fhms.test.terminalNumber");
		// note: you must use a new instance of org.jpos.util.Logger and Log4JListener for each loggable component or deadlock will result
		jposLogLevel = config.getInt("fhms.jpos.log4j.level", -1);
		reconnectDelay = config.getLong("fhms.mux.reconnectDelay");

		if(!simulationMode)
		{
			mux = loadMUX(hosts, testMerchant, testTerminal, jposLogLevel, true);
			log.info("Loaded new peristent MUX: " + mux);
		}
	}

	private USATISOMUX loadMUX(List<String> hosts, String testMerchant, String testTerminal, int jposLogLevel, boolean persistent)
	{
		FHMSPackager packager = new FHMSPackager();
		if (jposLogLevel > 0)
		{
			org.jpos.util.Logger packagerLogger = new org.jpos.util.Logger();
			packagerLogger.addListener(new SeparateLineLog4JListener(jposLogLevel));
			packager.setLogger(packagerLogger, "FHMSPackager");
		}

		USATChannelPool channelPool = new USATChannelPool();

		for (String host : hosts)
		{
			try
			{
				Scanner scanner = new Scanner(host);
				scanner.useDelimiter(":");
				String ip = scanner.next();
				int port = scanner.nextInt();
				MerchantLinkFrameRelayChannel channel = new MerchantLinkFrameRelayChannel(ip, port, packager);
				// set the socket read timeout to a value slightly larger than the heartbeat time,
				// which is set by merchantlink to 60 seconds by our request
				// if
				if(persistent)
					channel.setTimeout(140*1000);
				else
					channel.setTimeout(60*1000);
				channelPool.addChannel(channel);
				if (jposLogLevel > 0)
				{
					org.jpos.util.Logger channelLogger = new org.jpos.util.Logger();
					channelLogger.addListener(new SeparateLineLog4JListener(jposLogLevel));
					channel.setLogger(channelLogger, "MerchantLinkFrameRelayChannel");
				}
				log.info("Loaded new MerchantLinkFrameRelayChannel for " + host);
			}
			catch (Exception e)
			{
				log.error("Failed to load FHMS host: " + host + ": " + e.getMessage(), e);
			}
		}

		USATISOMUX myMUX = null;

		if(persistent)
		{
			myMUX = new FHMSPersistentMUX(channelPool, threadPool, true, testMerchant, testTerminal);
			myMUX.setReconnectDelay(reconnectDelay);
		}
		else
			myMUX = new FHMSMUX(channelPool, threadPool, false);

		myMUX.setISORequestListener(new UnmatchedResponseHandler());

		return myMUX;
	}

	private synchronized void reloadConfig(Configuration config)
	{
		this.config = config;

		log.info("Terminating Channels...");

		mux.terminate();

		try
		{
			Thread.sleep(1000);
		}
		catch (Exception e)
		{
		}

		loadConfig();
	}

	public synchronized void start()
	{
		if (isStarted)
			return;

		log.info("FHMSInterchange Starting up...");
		loadConfig();

		executor = Executors.newSingleThreadScheduledExecutor();

		isStarted = true;
	}

	public synchronized void stop()
	{
		if (!isStarted)
			return;

		log.info("FHMSInterchange Shutting down...");

		executor.shutdownNow();

		if(mux != null)
			mux.terminate();

		isStarted = false;
	}

	public boolean isStarted()
	{
		return isStarted;
	}

	public boolean isConnected()
	{
		if(simulationMode)
			return true;

		if (mux == null)
			return false;

		return mux.isConnected();
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		String transactionType = transaction.getRequest().getTransactionType();
		FHMSAction action = actions.get(transactionType);
		if (action == null)
		{
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Processing failed: Unsupported transaction type: " + transactionType);
			return new ISO8583Response(ISO8583Response.ERROR_UNSUPPORTED_TRANSACTION_TYPE, "Unsupported transaction type: " + transactionType);
		}

		if (!canSend())
		{
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Processing failed: Interchange is not currently connected to the host");
			return new ISO8583Response(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, "Interchange is not currently connected to the host");
		}

		return action.process(transaction);
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		String transactionType = transaction.getRequest().getTransactionType();
		FHMSAction action = actions.get(transactionType);
		if (action == null)
		{
			log.error("Recovery failed: Unsupported transaction type: " + transactionType);
			return true;
		}

		try
		{
			return action.recover(transaction);
		}
		catch (Throwable e)
		{
			log.error("Caught unexpected exception while attempting recovery of " + transaction + ": " + e.getMessage(), e);
			return false;
		}
	}

	private class UnmatchedResponseHandler implements ISORequestListener
	{
		public boolean process(ISOSource source, ISOMsg isoMsg)
		{
			if (isoMsg instanceof FHMSISOMsg)
			{
				try
				{
					FHMSISOMsg fhmsMsg = (FHMSISOMsg) isoMsg;
					if(fhmsMsg.isHeartBeat())
					{
						log.debug("Heartbeat IN : " + fhmsMsg);
					}
					else if(fhmsMsg.getMTI() != null && fhmsMsg.getMTI().equals("0810"))
					{
						log.debug("Test Transaction IN: " + fhmsMsg);
					}
				}
				catch(Exception e)
				{
					log.error("Failed to process Unmatched Response: " + isoMsg + ": " + e.getMessage(), e);
				}
			}
			else
			{
				//TODO: Handle this response somehow
				log.warn("Received unmatched message from FHMS Host: " + isoMsg);
			}

			return true;
		}
	}

	private class ConfigReloader extends org.apache.commons.configuration.reloading.FileChangedReloadingStrategy
	{
		private ConfigReloader()
		{
			super();
		}

		@Override
		public void reloadingPerformed()
		{
			super.reloadingPerformed();
			//log.warn("FHMSInterchange configuration change detected!");
			//reloadConfig(config);

			log.warn("FHMSInterchange configuration change detected! Please stop/start the interchange for the change to take effect...");
		}
	}
}
