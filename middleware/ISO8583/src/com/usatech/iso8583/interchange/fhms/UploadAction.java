package com.usatech.iso8583.interchange.fhms;

import java.net.SocketTimeoutException;
import java.util.Date;

import com.usatech.iso8583.*;
import com.usatech.iso8583.interchange.fhms.jpos.message.FHMSISOMsg;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.util.Util;

import org.apache.commons.logging.*;
import org.jpos.iso.ISOException;

public class UploadAction extends FHMSAction
{
	private static Log log = LogFactory.getLog(UploadAction.class);

	private static int[] requiredFields = { 
		ISO8583Message.FIELD_TRANSACTION_SUB_TYPE, 
		ISO8583Message.FIELD_ACQUIRER_ID, 
		ISO8583Message.FIELD_TERMINAL_ID, 
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_AMOUNT, 
		ISO8583Message.FIELD_POS_ENVIRONMENT, 
		ISO8583Message.FIELD_ENTRY_MODE, 
		ISO8583Message.FIELD_ONLINE, 
		ISO8583Message.FIELD_PAN_DATA, 
		ISO8583Message.FIELD_EFFECTIVE_DATE, 
		ISO8583Message.FIELD_RETRIEVAL_REFERENCE_NUMBER, 
//		ISO8583Message.FIELD_APPROVAL_CODE,
		ISO8583Message.FIELD_BATCH_NUMBER, 
		ISO8583Message.FIELD_ORIGINAL_TRACE_NUMBER 
	};

	public UploadAction(FHMSInterchange interchange)
	{
		super(interchange);
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException
	{
		super.validateRequest(request, requiredFields);

		String subType = request.getTransactionSubType();
		if (!(subType.equals(ISO8583Request.TRANSACTION_TYPE_SALE) || subType.equals(ISO8583Request.TRANSACTION_TYPE_REFUND)))
			throw new ValidationException("Invalid Upload Request Subtype: " + subType);
		
		// patch 08/02/2007 (1.3.12) - approval code is not required for refund uploads
		if (subType.equals(ISO8583Request.TRANSACTION_TYPE_SALE) && !request.hasField(ISO8583Message.FIELD_APPROVAL_CODE))
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_APPROVAL_CODE));
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		ISO8583Request request = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);

		FHMSISOMsg isoRequest = null;

		try
		{
			validateRequest(request, requiredFields);

			isoRequest = new FHMSISOMsg();
			isoRequest.setMTI("0320");

			if (request.getTransactionSubType().equals(ISO8583Message.TRANSACTION_TYPE_SALE))
				isoRequest.setProcessingCode("004000");
			else
				isoRequest.setProcessingCode("204000");

			if (request.isOnline())
				isoRequest.setOriginalMessageData("0200", Integer.toString(request.getOriginalTraceNumber()));
			else
				isoRequest.setOriginalMessageData("0220", Integer.toString(request.getOriginalTraceNumber()));
			
			if(request.hasPS2000())
			{
				PS2000 ps2000 = request.getPS2000();
				isoRequest.setPS2000(ps2000.getIndicator(), ps2000.getIdentifier(), ps2000.getValidationCode(), ps2000.getResponseCode(), ps2000.getEntryMode());
			}

			isoRequest.setPrimaryAccountNumber(request.getPanData().getPan());
			isoRequest.setExpirationDate(request.getPanData().getExpiration());
			isoRequest.setPointOfServiceEntryMode(FHMSUtil.buildPOSEntryModeString(request.getEntryMode(), request.getPinEntryCapability(), request.getPanData()));

			isoRequest.setTransactionLocalTime(FHMSUtil.getHHmmss(request.getEffectiveDate()));
			isoRequest.setTransactionLocalDate(FHMSUtil.getMMDD(request.getEffectiveDate()));

			isoRequest.setCardAcceptorAcquirerID(request.getAcquirerID());
			isoRequest.setCardAcceptorTerminalID(request.getTerminalID());
			isoRequest.setSystemTraceNumber(Integer.toString(request.getTraceNumber()));
			isoRequest.setTransactionAmount(Integer.toString(request.getAmount()));
			isoRequest.setPointOfServiceConditionCode(FHMSUtil.buildPOSConditionCode(request.getPosEnvironment()));

			isoRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
			isoRequest.setAuthorizationIdentificationResponse(request.getApprovalCode());
			isoRequest.setBatchNumber(Integer.toString(request.getBatchNumber()));

			isoRequest.setResponseCode("00");
		}
		catch (ValidationException e)
		{
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		}
		catch (ISOException e)
		{
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Caught unexpected exception validating " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception validating request: " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);

		FHMSISOMsg isoResponse = null;

		try
		{
			isoResponse = sendRequest(isoRequest, transaction, (request.getHostResponseTimeout()*1000));
		}
		catch (SocketTimeoutException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.warn("Timeout occured waiting for host response for " + transaction);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, "Timeout occured waiting for host response.");
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception transmitting " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting " + transaction + ": " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);

		ISO8583Response response = null;

		try
		{
			response = readResponse(isoResponse);
		}
		catch (ValidationException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host response failed input validation: " + e.getMessage());
		}
		catch (ISOException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host response failed input validation: " + e.getMessage());
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception while constructing response: " + transaction.getTransactionID() + ": " + e.getMessage() + ", response: " + isoResponse, e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);

		return response;
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		log.info("Skipping recovery of " + transaction + ": Not an online transaction");
		return true;
	}
	
	protected FHMSISOMsg createSimulatedResponse(FHMSISOMsg isoRequest)
	{
		try
		{
			FHMSISOMsg isoResponse = new FHMSISOMsg();
			
			isoResponse.setMTI("0330");
			isoResponse.setProcessingCode(isoRequest.getProcessingCode());
			isoResponse.setCardAcceptorAcquirerID(isoRequest.getCardAcceptorAcquirerID());
			isoResponse.setCardAcceptorTerminalID(isoRequest.getCardAcceptorTerminalID());
			isoResponse.setSystemTraceNumber(isoRequest.getSystemTraceNumber());
			isoResponse.setTransactionAmount(isoRequest.getTransactionAmount());
			
			if(isoRequest.getTransactionLocalDate() != null)
			{
				isoResponse.setTransactionLocalDate(isoRequest.getTransactionLocalDate());
				isoResponse.setTransactionLocalTime(isoRequest.getTransactionLocalTime());
			}
			else
			{
				Date now = new Date();
				isoResponse.setTransactionLocalDate(FHMSUtil.getMMDD(now));
				isoResponse.setTransactionLocalTime(FHMSUtil.getHHmmss(now));
			}
			
			if(isoRequest.getRetrievalReferenceNumber() != null)
				isoResponse.setRetrievalReferenceNumber(isoRequest.getRetrievalReferenceNumber());
			else
				isoResponse.setRetrievalReferenceNumber("0000000" + Util.getRandomInt(10000, 99999));
			
			if(isoRequest.getAuthorizationIdentificationResponse() != null)
				isoResponse.setAuthorizationIdentificationResponse(isoRequest.getAuthorizationIdentificationResponse());
			else
				isoResponse.setAuthorizationIdentificationResponse("T" + Util.getRandomInt(10000, 99999));
			
			isoResponse.setResponseCode("00");
			
			return isoResponse;
		}
		catch(ISOException e)
		{
			log.error("Failed to create simulated response: " + e.getMessage(), e);
			return null;
		}
	}
}
