/*
 * Created on May 23, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63PS2000TerminalGeneratedData21Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63PS2000TerminalGeneratedData21 extends ISOMsg
{
	private static final long serialVersionUID = 3258129146193588784L;
	private static final int TLV_TAG = 21;
	private Field63PS2000TerminalGeneratedData21Packager msgPackager;

	public Field63PS2000TerminalGeneratedData21() throws ISOException
	{
		super();
		//this.set(2, "");
		msgPackager = new Field63PS2000TerminalGeneratedData21Packager();
		this.setPackager(msgPackager);
	}

	public String getAuthorizedAmount() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setAuthorizedAmount(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
