/**
 * Copyright *2000, GlobalPlatform, Inc., all rights reserved.
 * The Technology provided or described herein is subject to updates,
 * revisions, and extensions by GlobalPlatform.  Use of this information is
 * governed by the GlobalPlatform License Agreement and any use inconsistent
 * with that Agreement is strictly prohibited.
 **/

package com.usatech.iso8583.interchange.fhms.tlv;

import org.jpos.iso.ISOUtil;

/**
 * The <code>ISORawTLV</code> class is used to represent raw TLV data generally acquired from 
 * the card. It can also be used to transfer structured data from a code module/service to another one.
 * A <code>ISORawTLV</code> object contains TLV-encoded objects, primitive
 * or constructed. A <code>ISORawTLV</code> object can be only a primitive data object but can be also a constructed
 * data object with one or several primitive data objects inside or even other constructed objects inside.</p>
 * With the <code>ISORawTLV(template)</code> constructor and the <code>addTLV</code> methods, <code>ISORawTLV</code> objects
 * can be built in order, for example, to transfer in a structured way different information through a byte array.
 * @author Nicolas Juillard, Christophe Colas, dkouznetsov - Modified for FHMS LTV format
 * @version 1.5.4 
 */

public class ISORawTLV
{

	final static int TEMPLATE_CAPACITY = 100;

	int tagValue = -1; // leading Tag value
	int valueLength = -1; // length in bytes of the value field
	int lengthIndex = 0; //-1;     // index in dataArray where the length is coded
	int valueIndex = 4; //-1;      // index in dataArray where the value is coded
	int dataArrayLength = -1; // length of significative data in dataArray (<= dataArray.length)
	int template; // template of the ISORawTLV

	byte[] dataArray;

	boolean isLengthUpdated = false; //true if valueLength is really the length coded in dataArray
	//false otherwise like, for example, if TLVs has been added

	ISOLengthValue dummyLengthValue; //temporary object to get back some values for some methods

	int rawTagToFind; //to be used only for find... methods
	OccurrenceCounter rawCounter = null; //to be used only for find... methods

	/**
	 * Creates a <code>ISORawTLV</code> object and from a tag and its value.
	 * @param template template tag for this <code>ISORawTLV</code>. If none available, can be set to 0.
	 * @param tag value of the tag
	 * @param value byte array containing the value of the created ISORawTLV. The length will be calculated from the length of this array.
	 */
	public ISORawTLV(int template, int tag, byte[] value) throws ISOTLVException
	{
		if (tag < 10 || tag > 99)
			throw new ISOTLVException(ISOTLVException.INVALID_DATA);

		dummyLengthValue = new ISOLengthValue();
		valueLength = value.length;
		dataArrayLength = valueLength + 4;
		dataArray = new byte[dataArrayLength];

		TLVLengthInt2Array(value.length + 2, dataArray, 0, dummyLengthValue);
		tagValue = tag;
		TLVTagInt2Array(tag, dataArray, 2, dummyLengthValue);

		System.arraycopy(value, 0, dataArray, valueIndex, valueLength);
		isLengthUpdated = true;
		this.template = template;
	}

	/**
	 * Creates an empty constructed <code>ISORawTLV</code> object and with the given template.
	 * For example, if template = 0x70, the corresponding <code>ISORawTLV</code> in byte array is '70 00'. This method is used
	 * for <code>ISORawTLV</code> building with the <code>addTLV</code> methods.
	 * @param template tag of the empty constructed data object to be created
	 */
	public ISORawTLV(int template) throws ISOTLVException
	{
		if (template < 10 || template > 99)
			throw new ISOTLVException(ISOTLVException.INVALID_DATA);

		dummyLengthValue = new ISOLengthValue();
		dataArray = new byte[TEMPLATE_CAPACITY];
		TLVTagInt2Array(template, dataArray, 0, dummyLengthValue);
		lengthIndex = dummyLengthValue.length;
		tagValue = template;
		dataArray[lengthIndex] = 0;
		valueIndex = lengthIndex;
		valueLength = 0;
		dataArrayLength = valueIndex;
		isLengthUpdated = true;
		this.template = 0;
	}

	/**
	 * Creates a new <code>ISORawTLV</code> object and initializes it
	 * with the data stored in a byte array given as an argument. Does not copy the byte array.
	 * @param data Raw TLV data stored in a byte array
	 */
	public ISORawTLV(byte[] data) throws ISOTLVException
	{
		dataArray = data;
		dataArrayLength = dataArray.length;
		tagValue = -1;
		valueLength = -1;
		lengthIndex = -1;
		valueIndex = -1;
		isLengthUpdated = true;
		template = 0;
		dummyLengthValue = new ISOLengthValue();
	}

	/**
	 * All the attributes have to be initialized...
	 */
	ISORawTLV(byte[] data, int offset, int length)
	{
		dummyLengthValue = new ISOLengthValue();
		dataArray = new byte[length];
		dataArrayLength = length;
		isLengthUpdated = true;
		System.arraycopy(data, offset, dataArray, 0, length);
	}

	/**
	 * Returns the template of this <code>ISORawTLV</code> object.
	 * @return template value
	 */
	public int getTemplate()
	{
		return template;
	}

	/**
	 * Returns the leading Tag of the <code>ISORawTLV</code> object.
	 *@return the Tag coded as int.
	 */
	public int getTag()
	{
		updateTag();
		return tagValue;
	}

	/**
	 * Returns the Value of this TLV <code>ISORawTLV</code> object.
	 * @return the byte array containing the TLV Value of this <code>ISORawTLV</code> object.
	 * @exception ISOTLVException INVALID_DATA if this <code>ISORawTLV</code> object contains incoherent data
	 */
	public byte[] getValue() throws ISOTLVException
	{
		updateTagLength();
		byte[] ISORawTLVvalue = new byte[valueLength];
		System.arraycopy(dataArray, valueIndex, ISORawTLVvalue, 0, valueLength);
		return (ISORawTLVvalue);
	}

	/**
	 * Returns the Value of this TLV <code>ISORawTLV</code> object.
	 * @return the byte array containing the TLV Value of this <code>ISORawTLV</code> object.
	 * @exception ISOTLVException INVALID_DATA if this <code>ISORawTLV</code> object contains incoherent data
	 */
	/*
	 public UByteArray getValueAsUByteArray() throws ISOTLVException
	 {
	 updateTagLength();
	 return (new UByteArray(dataArray,valueIndex,valueLength));
	 }
	 */

	/**
	 * Returns the data bytes of the <code>ISORawTLV</code> object.
	 * @return the byte array containing the data bytes of the <code>ISORawTLV</code> object.
	 */
	public byte[] getData()
	{
		byte[] tempArray;
		int lengthLength;

		if (isLengthUpdated && (dataArrayLength == dataArray.length))
			return dataArray;
		if (isLengthUpdated)
		{
			tempArray = new byte[dataArrayLength];
			System.arraycopy(dataArray, 0, tempArray, 0, dataArrayLength);
			dataArray = tempArray;
		}
		else
			updateArray(); // this method must return an array with the correct size
		return dataArray;
	}

	/**
	 * finds a given tag within the <code>ISORawTLV</code> object and returns the first
	 * <code>ISOSubRawTLV</code> object corresponding to that tag. The first occurence of the TLV is 
	 * returned (if present).
	 * @param tag the tag to find in the <code>ISORawTLV</code> object. It can be primitive or constructed.
	 * @return the <code>ISOSubRawTLV</code> object corresponding to that tag. If the tag is not found, returns null.
	 * @exception ISOTLVException INVALID_DATA if invalid TLV data structure is encountered
	 */
	public ISOSubRawTLV findFirstSubTLV(int tag) throws ISOTLVException
	{
		ISOSubRawTLV ISORawTLVFound;

		if (rawCounter == null)
			rawCounter = new OccurrenceCounter();
		rawTagToFind = tag;
		rawCounter.nbFound = 0;
		rawCounter.occurrence = 0;

		updateTagLength();
		ISORawTLVFound = findISORawTLV(tag, template, rawCounter, 0, dataArrayLength, tagValue, valueIndex, lengthIndex); //template set to 0
		if (ISORawTLVFound == null)
			rawCounter.nbFound = -1; //no more to found
		return ISORawTLVFound;
	}

	/**
	 * finds the next <code>ISOSubRawTLV<code> object given the tag initialized by the previous <code>findFirstSubTLV</code> call and returns it.
	 *
	 * @return the next <code>ISOSubRawTLV</code> object defined by the previously set tag.  If the tag is not anymore found, returns null.
	 * @exception ISOTLVException INVALID_DATA if invalid TLV data structure is encountered
	 */
	public ISOSubRawTLV findNextSubTLV() throws ISOTLVException
	{
		ISOSubRawTLV ISORawTLVFound;
		//could be optimized with maybe index instead of doing it again
		if (rawCounter == null)
			throw new ISOTLVException(ISOTLVException.FIND_NOT_INITIALIZED);
		if (rawCounter.nbFound == -1)
			return null;
		rawCounter.occurrence = 0;
		ISORawTLVFound = findISORawTLV(rawTagToFind, template, rawCounter, 0, dataArrayLength, tagValue, valueIndex, lengthIndex); //template set to 0
		if (ISORawTLVFound == null)
			rawCounter.nbFound = -1; //no more to found
		return ISORawTLVFound;
	}

	/**
	 * Core function of the 'find...' methods
	 * @param tagToFind value of the tag to find
	 * @param template value of the template of the surrounding TLV
	 * @param counter used to count the occurences of the TLV to find
	 * @param offset offset in dataArray for the start of the search
	 * @param length length in dataArray from offset where to search
	 * @param tag value of the tag of the TLV which begins at offset
	 * @param valueIndex index of the value of the TLV which begins at offset
	 * @param tagLength length in bytes of the tag of the TLV which begins at offset
	 * @return null if no TLV has been found and a ISOSubRawTLV object otherwise
	 * @exception ISOTLVException if the object does not respect the TLV structure
	 */

	ISOSubRawTLV findISORawTLV(int tagToFind, int template, OccurrenceCounter counter, int offset, int length, int tag, int valueIndex, int tagLength) throws ISOTLVException
	{
		ISOSubRawTLV foundISORawTLV = null;
		int index;
		int subTag, subTagLength, subObjectLength;

		if (tag == tagToFind)
		{
			counter.occurrence++;
			if (counter.occurrence > counter.nbFound)
			{
				counter.nbFound++;
				foundISORawTLV = new ISOSubRawTLV(this, offset, length, template, tag, tagLength, valueIndex);
				return foundISORawTLV;
			}
		}

		if (!isPrimitiveTag(tag))
		{
			index = offset + valueIndex;

			while ((index + 4) < (offset + length))
			{
				getTag(dataArray, index, dummyLengthValue);
				subTag = dummyLengthValue.value;
				subTagLength = dummyLengthValue.length;
				subObjectLength = subTagLength + dummyLengthValue.length + dummyLengthValue.value;
				getLength(dataArray, index, dummyLengthValue);
				subObjectLength = dummyLengthValue.length + dummyLengthValue.value;

				foundISORawTLV = findISORawTLV(tagToFind, tag, counter, index, subObjectLength, subTag, 4, subTagLength);
				if (foundISORawTLV != null)
					return (foundISORawTLV);
				else
					index += subObjectLength;
			}
		}

		return (null);
	}

	/**
	 * appends a <code>ISORawTLV</code> in this <code>ISORawTLV</code> object. The insertion is made at the
	 * end of the Value of the ISORawTLV. The tag of the ISORawTLV must define a constructed TLV.
	 * For example, if the current ISORawTLV is [T1L1 [T2L2V2] [T3L3 [T4L4V4] [T5L5V5] ] [T6L6V6] ] after the addition,
	 * ISORawTLV is [T1L1 [T2L2V2] [T3L3 [T4L4V4] [T5L5V5] ] [T6L6V6] [TLV]] where [TLV] is in fact another ISORawTLV object.
	 * L1 is adapted to the new global length of ISORawTLV.
	 *
	 * @param raw ISORawTLV object to be added.
	 * @exception ISOTLVException INVALID_DATA if the tag of the ISORawTLV does not define a constructed TLV.
	 */
	public void addTLV(ISORawTLV rawTLV) throws ISOTLVException
	{
		rawTLV.updateArray();
		updateTagLength();
		//depends only on dataArrayLength and valueLength
		if (rawTLV.dataArrayLength > dataArray.length - dataArrayLength)
		{
			byte[] tempArray;

			tempArray = new byte[dataArray.length + rawTLV.dataArrayLength * 5];
			System.arraycopy(dataArray, 0, tempArray, 0, dataArrayLength);
			dataArray = tempArray;
		}
		System.arraycopy(rawTLV.dataArray, 0, dataArray, dataArrayLength, rawTLV.dataArrayLength);
		dataArrayLength += rawTLV.dataArrayLength;
		valueLength += rawTLV.dataArrayLength;
		isLengthUpdated = false;
	}

	/**
	 * appends a <code>ISOSubRawTLV</code> in this <code>ISORawTLV</code> object. The insertion is made at the
	 * end of the Value of the ISORawTLV. The tag of the ISORawTLV must define a constructed TLV.
	 * For example, if the current ISORawTLV is [T1L1 [T2L2V2] [T3L3 [T4L4V4] [T5L5V5] ] [T6L6V6] ] after the addition,
	 * ISORawTLV is [T1L1 [T2L2V2] [T3L3 [T4L4V4] [T5L5V5] ] [T6L6V6] [TLV]] where [TLV] is in fact a ISOSubRawTLV object.
	 * L1 is adapted to the new global length of ISORawTLV.
	 *
	 * @param subTLV ISOSubRawTLV object to be added.
	 * @exception ISOTLVException INVALID_DATA if the tag of the ISORawTLV does not define a constructed TLV.
	 */
	public void addTLV(ISOSubRawTLV subTLV) throws ISOTLVException
	{
		updateTagLength();
		//depends only on dataArrayLength and valueLength
		if (subTLV.length > dataArray.length - dataArrayLength)
		{
			byte[] tempArray;

			tempArray = new byte[dataArray.length + subTLV.length * 5];
			System.arraycopy(dataArray, 0, tempArray, 0, dataArrayLength);
			dataArray = tempArray;
		}
		System.arraycopy(subTLV.tlv.dataArray, subTLV.offset, dataArray, dataArrayLength, subTLV.length);
		dataArrayLength += subTLV.length;
		valueLength += subTLV.length;
		isLengthUpdated = false;
	}

	/**
	 * Tests if a tag value defines a primitive or constructed TLV.
	 * @param tag tag value to be tested
	 * @return true if the tag is primitive, false otherwise
	 */

	static boolean isPrimitiveTag(int tag)
	{
		int firstByte;

		if (tag > 0xFF)
			firstByte = tag >> 8;
		else
			firstByte = tag;
		if ((firstByte & 0x20) == 0x20)
			return false; //Tag of CDO.
		else
			return true; //Tag of PDO.
	}

	/**
	 * Returns some tag information from a TLV structure byte array
	 * @param data byte array to be analyzed
	 * @param offset starting index of the analysis
	 * @param lengthValue used to return the length in bytes of the tag and its numeric value
	 */

	static void getTag(byte[] data, int offset, ISOLengthValue lengthValue)
	{
		byte[] tagBytes = new byte[2];
		System.arraycopy(data, offset + 2, tagBytes, 0, 2);
		lengthValue.length = 2;
		try
		{
			lengthValue.value = Integer.parseInt(new String(tagBytes));
		}
		catch (Exception e)
		{
			lengthValue.value = 0;
		}
	}

	/**
	 * The <code>getLength</code> method will determine the length of the TLV's Length field : 1, 2 or
	 * 3 bytes. It will also determine the value of this field, which will allow to know the length of
	 * the Value TLV's field.
	 * @param data is the TLV. It can be primitive or constructed.
	 * @param offset will give the position of the length field where to start the length analysis
	 * @param lengthValue used to return the length and the value of the TLV length field.
	 * @exception ISOTLVException INVALID_DATA if invalid TLV data structure is encountered
	 */

	static void getLength(byte[] data, int offset, ISOLengthValue lengthValue) throws ISOTLVException
	{
		lengthValue.length = 2;
		try
		{
			lengthValue.value = Integer.parseInt(ISOUtil.bcd2str(data, offset, 4, true));
		}
		catch (Exception e)
		{
			lengthValue.value = 0;
		}
	}

	// used to update the tag information variables from dataArray
	// (sets tagValue and lengthIndex)
	void updateTag()
	{
		if (tagValue == -1)
		{
			getTag(dataArray, 0, dummyLengthValue);
			tagValue = dummyLengthValue.value;
			lengthIndex = dummyLengthValue.length;
		}
	}

	// used to update the Tag and Length information variables from dataArray 
	//depends on lengthIndex
	//sets valueIndex, valueLength
	void updateTagLength() throws ISOTLVException
	{
		updateTag();
		if (valueIndex == -1)
		{
			if (!isLengthUpdated)
				throw new ISOTLVException(ISOTLVException.DEFAULT);
			getLength(dataArray, lengthIndex, dummyLengthValue);
			valueLength = dummyLengthValue.value;
			valueIndex = lengthIndex + dummyLengthValue.length;
			if (dataArrayLength - valueIndex != valueLength)
			{
				throw new ISOTLVException(ISOTLVException.INVALID_DATA);
			}
		}
	}

	//used to update the length if it is different between dataArray and the length variable
	// and update dataArray to its real length
	void updateArray()
	{
		byte[] tempArray;
		int lengthLength;

		if (!isLengthUpdated)
		{
			if (valueLength < 127)
				lengthLength = 1;
			else if ((valueLength >= 128) && (valueLength <= 255))
				lengthLength = 2;
			else if (valueLength < 65536)
				lengthLength = 3;
			else
				throw new ISOTLVException(ISOTLVException.INVALID_DATA);

			tempArray = new byte[lengthIndex + lengthLength + valueLength];
			System.arraycopy(dataArray, 0, tempArray, 0, lengthIndex);
			TLVLengthInt2Array(valueLength, tempArray, lengthIndex, dummyLengthValue);
			System.arraycopy(dataArray, valueIndex, tempArray, lengthIndex + lengthLength, valueLength);
			valueIndex = lengthIndex + dummyLengthValue.length; // Modified 06/2000
			dataArray = tempArray;
			dataArrayLength = dataArray.length;
			isLengthUpdated = true;
		}
	}

	/**
	 * Converts the TLV tag field coded as int in the given byte array
	 * @return the TLV tag fied as an array of bytes.
	 */
	static void TLVTagInt2Array(int tag, byte[] array, int offset, ISOLengthValue lengthValue)
	{
		System.arraycopy(String.valueOf(tag).getBytes(), 0, array, offset, 2);
		lengthValue.length = 2;
		lengthValue.value = tag;
	}

	/**
	 * Returns the TLV length field in byte array
	 * @return the TLV length fied as an array of bytes.
	 */
	static void TLVLengthInt2Array(int length, byte[] array, int offset, ISOLengthValue lengthValue)
	{
		byte[] bcdLength = ISOUtil.str2bcd((new Integer(length)).toString(), true);

		if (bcdLength.length == 1)
		{
			array[offset] = 0x00;
			array[offset + 1] = bcdLength[0];
		}
		else
		{
			array[offset] = bcdLength[0];
			array[offset + 1] = bcdLength[1];
		}

		lengthValue.length = 2;
		lengthValue.value = length;
	}

	// convert a byte to an integer
	static int byteToInt(byte i)
	{
		if (i < 0)
			return (i + 256);
		else
			return (i);
	}
}

class OccurrenceCounter
{
	int nbFound; // number of TLV already found; at the next iteration of 'find...Next' will look for the (nbFound+1) occurence
	int occurrence; // counts the number of occurrences of TLV found in the internal process of 'findISORawTLV'

	public String toString()
	{
		return "nbFound=" + nbFound + " occurrence=" + occurrence;
	}
}
