package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class NullFieldPackager extends ISOFieldPackager
{
	public NullFieldPackager()
	{
		super();
	}

	public int getMaxPackedLength()
	{
		return 0;
	}

	/**
	 * @param c - a component
	 * @return packed component
	 * @exception ISOException
	 */
	public byte[] pack(ISOComponent c) throws ISOException
	{
		return new byte[0];
	}

	public int unpack(ISOComponent c, byte[] b, int offset) throws ISOException
	{
		return 0;
	}
}
