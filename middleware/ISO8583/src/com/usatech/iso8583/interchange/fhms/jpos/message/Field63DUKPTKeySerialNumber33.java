/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63DUKPTKeySerialNumber33Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63DUKPTKeySerialNumber33 extends ISOMsg
{
	private static final long serialVersionUID = 3258411729154356021L;
	private static final int TLV_TAG = 33;
	private Field63DUKPTKeySerialNumber33Packager msgPackager;

	public Field63DUKPTKeySerialNumber33() throws ISOException
	{
		super();
		//this.set(2, "");
		msgPackager = new Field63DUKPTKeySerialNumber33Packager();
		this.setPackager(msgPackager);
	}

	public String getDUKPTKeySerialNumber() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setDUKPTKeySerialNumber(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
