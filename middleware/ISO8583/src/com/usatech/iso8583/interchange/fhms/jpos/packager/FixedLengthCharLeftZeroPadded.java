package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class FixedLengthCharLeftZeroPadded extends ISOStringFieldPackager
{
	public FixedLengthCharLeftZeroPadded()
	{
		super(0, null, LeftPadder.ZERO_PADDER, AsciiInterpreter.INSTANCE, NullPrefixer.INSTANCE);
	}

	/**
	 * @param len - field len
	 * @param description symbolic descrption
	 */
	public FixedLengthCharLeftZeroPadded(int len, String description)
	{
		super(len, description, LeftPadder.ZERO_PADDER, AsciiInterpreter.INSTANCE, NullPrefixer.INSTANCE);
	}
}
