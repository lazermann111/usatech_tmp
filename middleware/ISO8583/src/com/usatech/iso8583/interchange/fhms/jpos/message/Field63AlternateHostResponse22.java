/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63AlternateHostResponse22Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

/* First Horizon is returning responses like: "AUTH DECLINED                        605"
 * The extra number at the end is throwing off the trim, so I'm truncating the response to 35 characters. 
 */

public class Field63AlternateHostResponse22 extends ISOMsg
{
	private static final long serialVersionUID = 3256446897714835504L;

	private static final int TLV_TAG = 22;

	private Field63AlternateHostResponse22Packager msgPackager;

	public Field63AlternateHostResponse22()
	{
		super();
		msgPackager = new Field63AlternateHostResponse22Packager();
		this.setPackager(msgPackager);
	}

	public String getResponseText() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return ((String) this.getValue(2)).substring(0,35).trim();
	}

	public void setResponseText(String value) throws ISOException
	{
		this.set(2, value.trim());
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}

