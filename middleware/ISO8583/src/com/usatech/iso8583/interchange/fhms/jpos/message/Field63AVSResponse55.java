/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63AVSResponse55Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63AVSResponse55 extends ISOMsg
{
	private static final long serialVersionUID = 3906369316026005047L;

	private static final int TLV_TAG = 55;

	private Field63AVSResponse55Packager msgPackager;

	public Field63AVSResponse55() throws ISOException
	{
		super();
		//this.set(2, "");
		//this.set(3, "");
		//this.set(4, "");
		msgPackager = new Field63AVSResponse55Packager();
		this.setPackager(msgPackager);
	}

	public String getAddressMatch() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setAddressMatch(String value) throws ISOException
	{
		this.set(2, value);
	}

	public String getZipCodeMatch() throws ISOException
	{
		if (!hasField(3))
			return null;
		else
			return (String) this.getValue(3);
	}

	public void setZipCodeMatch(String value) throws ISOException
	{
		this.set(3, value);
	}

	public String getAddressResponseCode() throws ISOException
	{
		if (!hasField(4))
			return null;
		else
			return (String) this.getValue(4);
	}

	public void setAddressResponseCode(String value) throws ISOException
	{
		this.set(4, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
