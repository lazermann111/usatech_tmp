/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63PaymentServices200020Packager extends ISOBasePackager
{
	private ISOFieldPackager fld[] = 
	{ 
		new IFA_NUMERIC(0, "MTI PLACEHOLDER"), 
		new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), 
		new FixedLengthCharRightSpacePadded(1, "PS2000 INDICATOR"), 
		new FixedLengthCharRightSpacePadded(15, "TRANSACTION IDENTIFIER"), 
		new FixedLengthCharRightSpacePadded(4, "VALIDATION CODE"), 
		new FixedLengthCharRightSpacePadded(2, "VISA RESPONSE CODE"), 
		new FixedLengthCharRightSpacePadded(2, "POS ENTRY MODE") 
	};

	public Field63PaymentServices200020Packager()
	{
		super();
		setFieldPackager(this.fld);
	}
}
