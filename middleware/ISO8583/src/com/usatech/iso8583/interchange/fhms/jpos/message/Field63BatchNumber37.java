/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.message;

import com.usatech.iso8583.interchange.fhms.jpos.packager.Field63BatchNumber37Packager;
import com.usatech.iso8583.interchange.fhms.tlv.ISOTLVField;

import org.jpos.iso.*;

public class Field63BatchNumber37 extends ISOMsg
{
	private static final long serialVersionUID = 4051043064548373304L;

	private static final int TLV_TAG = 37;

	private Field63BatchNumber37Packager msgPackager;

	public Field63BatchNumber37() throws ISOException
	{
		super();
		//this.set(2, "");
		msgPackager = new Field63BatchNumber37Packager();
		this.setPackager(msgPackager);
	}

	public String getBatchNumber() throws ISOException
	{
		if (!hasField(2))
			return null;
		else
			return (String) this.getValue(2);
	}

	public void setBatchNumber(String value) throws ISOException
	{
		this.set(2, value);
	}

	public void addToTLV(ISOTLVField tlv) throws ISOException
	{
		tlv.addTLV(TLV_TAG, this.pack());
	}

	public void getFromTLV(ISOTLVField tlv) throws ISOException
	{
		byte[] b = tlv.getFirstTLV(TLV_TAG);
		if (b != null)
			this.unpack(b);
	}
}
