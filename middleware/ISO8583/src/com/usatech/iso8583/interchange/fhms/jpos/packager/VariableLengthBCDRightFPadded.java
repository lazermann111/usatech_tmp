package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

import com.usatech.util.*;

public class VariableLengthBCDRightFPadded extends ISOStringFieldPackager
{
	public VariableLengthBCDRightFPadded(int maxlen, String description)
	{
		super(maxlen, description, NullPadder.INSTANCE, FixedBCDInterpreter.RIGHT_PADDED_F, BcdPrefixer.LL);
	}
	
	private static final char[] HEX_CHARS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String bytesToHex(byte[] ba)
	{
		int len = ba.length;
		int j;
		int k;
		StringBuffer sb = new StringBuffer((len * 3));

		for (int i = 0; i < len; i++)
		{
			j = (ba[i] & 0xf0) >> 4;
			k = ba[i] & 0xf;

			sb.append(HEX_CHARS[j]);
			sb.append(HEX_CHARS[k]);
		}

		return sb.toString();
	}
}
