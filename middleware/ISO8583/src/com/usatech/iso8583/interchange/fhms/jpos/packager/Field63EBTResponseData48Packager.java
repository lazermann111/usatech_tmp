/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63EBTResponseData48Packager extends ISOBasePackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharRightSpacePadded(8, "EBT FOOD BALANCE"), new FixedLengthCharRightSpacePadded(8, "EBT CASH BALANCE"), new FixedLengthCharRightSpacePadded(8, "CASE NUMBER") };

	public Field63EBTResponseData48Packager()
	{
		super();
		setFieldPackager(this.fld);
	}
}
