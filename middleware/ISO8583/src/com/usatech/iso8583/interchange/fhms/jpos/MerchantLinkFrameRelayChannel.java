package com.usatech.iso8583.interchange.fhms.jpos;

import java.io.*;
import java.net.*;

import com.usatech.iso8583.interchange.fhms.jpos.message.FHMSISOMsg;
import com.usatech.iso8583.interchange.fhms.jpos.packager.*;
import com.usatech.iso8583.interchange.fhms.tlv.ISOIFB_LLLTLVBINARY;
import com.usatech.iso8583.jpos.USATISOChannel;
import com.usatech.util.Conversions;

import org.apache.commons.logging.LogFactory;
import org.jpos.iso.*;
import org.jpos.iso.ISOFilter.VetoException;
import org.jpos.iso.header.BaseHeader;
import org.jpos.util.*;

public class MerchantLinkFrameRelayChannel extends BaseChannel implements USATISOChannel
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(MerchantLinkFrameRelayChannel.class);

	// TPDU: ID (1 byte), Destination Address (2 bytes), Originator Address (2 bytes)
	private static final byte[] TPDU = { 0x60, 0x11, 0x11, 0x22, 0x22 };

	// 07/07/2005 - pcowan - Per Merchant Link, don't use STX and ETX or CRC over a frame...
	//	private static final byte STX = 0x02;
	//	private static final byte ETX = 0x03;

	private long connectTime = -1;
	private long lastSend	 = -1;
	private long lastReceive = -1;

	public MerchantLinkFrameRelayChannel(String host, int port, ISOPackager p)
	{
		super(host, port, p);
		//this.header = TPDU;
	}

	public MerchantLinkFrameRelayChannel(ISOPackager p) throws IOException
	{
		super(p);
		//this.header = TPDU;
	}

	public MerchantLinkFrameRelayChannel(ISOPackager p, ServerSocket serverSocket) throws IOException
	{
		super(p, serverSocket);
		//this.header = TPDU;
	}

	public String toString()
	{
		return "MerchantLinkFrameRelayChannel(" + super.getHost() + ":" + super.getPort() + ")";
	}

	public long getConnectTime()
	{
		return connectTime;
	}
	
	public long getLastSend()
	{
		return lastSend;
	}

	public long getLastReceive()
	{
		return lastReceive;
	}

	protected Socket newSocket() throws IOException
	{
		try
		{
			log.debug("Connecting to " + super.getHost() + ":" + super.getPort());
			//(new Exception()).printStackTrace();
			Socket s = super.newSocket();
			connectTime = System.currentTimeMillis();
			lastReceive = -1;
			log.info("Successfully connected to " + super.getHost() + ":" + super.getPort());
			return s;
		}
		catch (IOException e)
		{
			log.warn("Failed to connect to " + super.getHost() + ":" + super.getPort() + ": " + e.getMessage());
			throw e;
		}
	}
	
	public void disconnect() throws IOException
	{
		log.info("Disconnecting from " + super.getHost() + ":" + super.getPort());
		super.disconnect();
	}

	protected void sendMessageLength(int len) throws IOException
	{
		if (len > 0xFFFF)
			throw new IOException("Maximum length exceeded!");

		//serverOut.writeShort(len);
		serverOut.write(len >> 8);
		serverOut.write(len);
	}

	protected int getMessageLength() throws IOException, ISOException
	{
		//return serverIn.readShort();
		byte[] b = new byte[2];
		serverIn.readFully(b, 0, 2);
		return (int) (((((int) b[0]) & 0xFF) << 8) | (((int) b[1]) & 0xFF));
	}

	protected int getHeaderLength()
	{
		return TPDU.length;
	}

	protected ISOHeader getDynamicHeader(byte[] image)
	{
		return new BaseHeader(TPDU);
	}

	protected ISOMsg createMsg()
	{
		try
		{
			return new FHMSISOMsg();
		}
		catch (ISOException e)
		{
			LogEvent evt = new LogEvent(this, "createMsg");
			evt.addMessage(e);
			Logger.log(evt);
			return null;
		}
	}

	public void send(ISOMsg m) throws IOException, ISOException, VetoException
	{
		LogEvent evt = new LogEvent(this, "send");
		evt.addMessage(m);
		try
		{
			if (!isConnected())
				throw new ISOException("unconnected ISOChannel");

			String sequenceNumber = null;
			if (m instanceof FHMSISOMsg)
			{
				sequenceNumber = ((FHMSISOMsg) m).getSequenceNumber();
			}
			else
			{
				log.warn("Received request to send a non-FHMSISOMsg message: " + m.getClass().getName());
				sequenceNumber = "0000";
			}

			m.setDirection(ISOMsg.OUTGOING);
			m = applyOutgoingFilters(m, evt);
			m.setDirection(ISOMsg.OUTGOING); // filter may have drop this info
			byte[] b;
			synchronized (packager)
			{
				m.setPackager(getDynamicPackager(m));
				b = m.pack();
			}
			
			((FHMSISOMsg) m).setRawBytes(b);
			
			synchronized (serverOut)
			{
				if (m instanceof FHMSISOMsg && ((FHMSISOMsg) m).isHeartBeat())
				{
					// this is a heartbeat
					sendMessageLength(0);
					serverOut.write(sequenceNumber.getBytes(), 0, 4);
					log.debug("send: len=" + 0 + " seqnum=" + sequenceNumber + " tpdu=" + " bytes=");
				}
				else
				{
					sendMessageLength(b.length + TPDU.length);
					serverOut.write(sequenceNumber.getBytes(), 0, 4);
					//					serverOut.write(STX);
					serverOut.write(TPDU);
					serverOut.write(b, 0, b.length);
                    if (m instanceof FHMSISOMsg)
                        log.info("Sending request to FHMS, Trace Number=" + ((FHMSISOMsg) m).getSystemTraceNumber());
					log.debug("send: len=" + (b.length + TPDU.length) + " seqnum=" + sequenceNumber + " tpdu=" + Conversions.bytesToHex(TPDU) + " bytes=" + Conversions.bytesToHex(b));

					//					CRC16 objCRC = new CRC16(b);
					//					serverOut.write(objCRC.getCrcStr().getBytes());
					//					serverOut.write(ETX);
				}

				serverOut.flush();
			}
			cnt[TX]++;
			lastSend = System.currentTimeMillis();
			setChanged();
			notifyObservers(m);
		}
		catch (VetoException e)
		{
			log.debug("send failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw e;
		}
		catch (ISOException e)
		{
			log.debug("send failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw e;
		}
		catch (IOException e)
		{
			log.debug("send failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw e;
		}
		catch (Exception e)
		{
			log.debug("send failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw new ISOException("unexpected exception", e);
		}
		finally
		{
			Logger.log(evt);
		}
	}

	public ISOMsg receive() throws IOException, ISOException
	{
		int len = -1;
		byte[] b = null;
		byte[] header = null;
		LogEvent evt = new LogEvent(this, "receive");

		byte[] controlByte = new byte[1];
		byte[] sequenceNumber = new byte[4];

		Socket socket = getSocket();
		FHMSISOMsg m = (FHMSISOMsg) createMsg();
		m.setSource(this);
		try
		{
			if (!isConnected())
				throw new ISOException("unconnected ISOChannel");

			synchronized (serverIn)
			{
				len = getMessageLength();

				if (len == 0)
				{
					serverIn.readFully(sequenceNumber, 0, 4);
					// this is a heartbeat
					log.debug("recv: len=" + 0 + " seqnum=" + new String(sequenceNumber) + " tpdu=" + " bytes=");
				}
				else if (len > 0)
				{
					serverIn.readFully(sequenceNumber, 0, 4);
					//					serverIn.readFully(controlByte, 0, 1); // STX

					int hLen = getHeaderLength();
					if (len == -1)
					{
						if (hLen > 0)
						{
							header = readHeader(hLen);
						}
						b = streamReceive();
					}
					else if (len > 10 && len <= 4096)
					{
						if (hLen > 0)
						{
							// ignore message header (TPDU)
							// Note header length is not necessarily equal to hLen (see VAPChannel)
							header = readHeader(hLen);
							len -= header.length;
						}
						b = new byte[len];
						serverIn.readFully(b, 0, len);
						
						//						byte[] msgCRC = new byte[2];
						//						serverIn.readFully(msgCRC, 0, 2);
						//						serverIn.readFully(controlByte, 0, 1); // ETX

						//						CRC16 objCRC = new CRC16(b);
						//						byte calcCRC[] = objCRC.getCrcStr().getBytes();
						//						if (Arrays.equals(msgCRC, calcCRC) == false)
						//							throw new ISOException("Invalid CRC");
					}
					else if(len <= 10)
					{
						b = new byte[len];
						serverIn.readFully(b, 0, len);
						throw new ISOException("received bytes too short: len= " + len + "  seqnum=" + new String(sequenceNumber) + " bytes=" + Conversions.bytesToHex(b));
					}
					else
						throw new ISOException("received bytes too long: len= " + len);
						//throw new ISOException("receive length " + len + " seems strange");

					log.debug("recv: len=" + len + " seqnum=" + new String(sequenceNumber) + " tpdu=" + (header != null ? Conversions.bytesToHex(header) : "") + " bytes=" + Conversions.bytesToHex(b));
				}
				else
					throw new IOException("Invalid len = " + len);
			}
			if (len > 0)
			{
				m.setHeader(getDynamicHeader(header));
				if (b.length > 0 && !shouldIgnore(header)) // Ignore NULL messages
				{
					synchronized (packager)
					{
						m.setPackager(getDynamicPackager(b));
						m.unpack(b);
					}
                    
                    log.info("Received response from FHMS, Trace Number=" + m.getSystemTraceNumber());
				}
			}

			m.setRawBytes(b);
			m.setSequenceNumber(new String(sequenceNumber));
			m.setDirection(ISOMsg.INCOMING);
			m = (FHMSISOMsg) applyIncomingFilters(m, header, b, evt);
			m.setDirection(ISOMsg.INCOMING);
			//log.debug("recv isoMsg=" + m);
			evt.addMessage(m);
			cnt[RX]++;
			lastReceive = System.currentTimeMillis();
			setChanged();
			notifyObservers(m);
		}
		catch (ISOException e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));
			
			log.debug("receive failed: " + e.getMessage(), e);
			evt.addMessage(e);
			if (header != null)
			{
				evt.addMessage("--- header ---");
				evt.addMessage(ISOUtil.hexdump(header));
			}
			if (b != null)
			{
				evt.addMessage("--- data ---");
				evt.addMessage(ISOUtil.hexdump(b));
			}
			throw e;
		}
		catch (EOFException e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));

			log.debug("receive failed: " + e.getMessage(), e);
			if (socket != null)
				socket.close();
			evt.addMessage("<peer-disconnect/>");
			throw e;
		}
		catch (InterruptedIOException e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));

			log.debug("receive failed: " + e.getMessage(), e);
			if (socket != null)
				socket.close();
			evt.addMessage("<io-timeout/>");
			throw e;
		}
		catch (IOException e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));

			log.debug("receive failed: " + e.getMessage(), e);
			if (socket != null)
				socket.close();
			if (usable)
				evt.addMessage(e);
			throw e;
		}
		catch (Exception e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));

			log.debug("receive failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw new ISOException("unexpected exception", e);
		}
		finally
		{
			Logger.log(evt);
		}
		return m;
	}

	private FHMSPackager getFHMSPackager(String mti)
	{
		FHMSPackager basePackager = (FHMSPackager) packager;

		//private use field 60
		if (mti == null)
			return basePackager;

		if (mti.equals("0500"))
		{
			//Field60BatchNumberPackager subFieldPackager = new Field60BatchNumberPackager();
			//ISOMsgFieldPackager fieldPackager = new ISOMsgFieldPackager(new IFB_LLLCHAR(999, "BATCH NUMBER"), subFieldPackager);
			//basePackager.setFieldPackager(60, fieldPackager);
			basePackager.setFieldPackager(60, new IFB_LLLBINARY(999, "BATCH NUMBER"));
		}
		else if (mti.equals("0320"))
		{
			//Field60OriginalMessageDataPackager subFieldPackager = new Field60OriginalMessageDataPackager();
			//ISOMsgFieldPackager fieldPackager = new ISOMsgFieldPackager(new IFB_LLLCHAR(999, "ORIGINAL MESSAGE DATA"), subFieldPackager);
			//basePackager.setFieldPackager(60, fieldPackager);
			basePackager.setFieldPackager(60, new IFB_LLLBINARY(999, "ORIGINAL MESSAGE DATA"));
		}
		else if (mti.equals("0220") || mti.equals("0420") || mti.equals("0400"))
		{
			//Field60OriginalAmountPackager subFieldPackager = new Field60OriginalAmountPackager();
			//ISOMsgFieldPackager fieldPackager = new ISOMsgFieldPackager(new IFB_LLLCHAR(999, "ORIGINAL AMOUNT"), subFieldPackager);
			//basePackager.setFieldPackager(60, fieldPackager);
			basePackager.setFieldPackager(60, new IFB_LLLBINARY(999, "ORIGINAL AMOUNT"));
		}
		else
			basePackager.setFieldPackager(60, null);

		//private use field 62
		if (mti.equals("0100") || mti.equals("0200") || mti.equals("0220") || mti.equals("0320"))
		{
			//Field62PurchaseOrderNumberPackager subFieldPackager = new Field62PurchaseOrderNumberPackager();
			//ISOMsgFieldPackager fieldPackager = new ISOMsgFieldPackager(new IFB_LLLCHAR(999, "PURCHASE ORDER NUMBER"), subFieldPackager);
			//basePackager.setFieldPackager(62, fieldPackager);
			basePackager.setFieldPackager(62, new IFB_LLLBINARY(999, "PURCHASE ORDER NUMBER"));
		}
		else
			basePackager.setFieldPackager(62, null);

		//private use field 63
		if (mti.equals("0500"))
		{
			//Field63ReconciliationRequestTotalsPackager subFieldPackager = new Field63ReconciliationRequestTotalsPackager();
			//ISOMsgFieldPackager fieldPackager = new ISOMsgFieldPackager(new IFB_LLLCHAR(999, "RECONCILIATION REQUEST TOTALS"), subFieldPackager);
			//basePackager.setFieldPackager(63, fieldPackager);
			basePackager.setFieldPackager(63, new IFB_LLLBINARY(999, "RECONCILIATION REQUEST TOTALS"));
		}
		else if (mti.equals("0100") || mti.equals("0110") || mti.equals("0200") || mti.equals("0210") || mti.equals("0220") || mti.equals("0230") || mti.equals("0310") || mti.equals("0320") || mti.equals("0330") || mti.equals("0410") || mti.equals("0430") || mti.equals("0510") || mti.equals("0810"))
		{
			basePackager.setFieldPackager(63, new ISOIFB_LLLTLVBINARY(999, "ADDITIONAL DATA"));
		}
		else
			basePackager.setFieldPackager(63, null);

		return basePackager;
	}

	protected ISOPackager getDynamicPackager(ISOMsg m)
	{
		if (!m.hasField(0))
			return new FHMSHeartBeatPackager();

		String mti = null;
		try
		{
			mti = (String) m.getValue(0);
		}
		catch (ISOException e)
		{
			LogEvent evt = new LogEvent(this, "getDynamicPackager");
			evt.addMessage(e);
			Logger.log(evt);
		}

		return getFHMSPackager(mti);
	}

	protected ISOPackager getDynamicPackager(byte[] image)
	{
		String mti = null;
		try
		{
			mti = new String(ISOUtil.bcd2str(image, 0, 4, true));
		}
		catch (Exception e)
		{
			LogEvent evt = new LogEvent(this, "getDynamicPackager");
			evt.addMessage(e);
			Logger.log(evt);
		}

		if (mti == null)
			return new FHMSHeartBeatPackager();
		else
			return getFHMSPackager(mti);

	}

	/*
	 protected void sendMessageHeader(ISOMsg m, int len) throws IOException
	 {
	 serverOut.write(m.getString(61).getBytes());
	 serverOut.write(STX);
	 serverOut.write(TPDU);
	 }
	 
	 protected void sendMessageTrailler(ISOMsg m, int len) throws IOException
	 {   
	 byte[] b = null;
	 try 
	 {
	 b = m.pack();
	 } 
	 catch (ISOException e) 
	 {
	 throw new IOException("ISOException while packing ISOMsg in sendMessageTrailler()");
	 }
	 
	 CRC16 objCRC = new CRC16(b);
	 serverOut.write(objCRC.getCrcStr().getBytes());
	 serverOut.write(ETX);
	 }
	 */
}
