/*
 * Created on May 18, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field62PurchaseOrderNumberPackager extends ISOBasePackager //FHMSSubFieldPackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharRightSpacePadded(6, "PURCHASE ORDER NUMBER") };

	public Field62PurchaseOrderNumberPackager()
	{
		super();
		setFieldPackager(fld);
	}
}
