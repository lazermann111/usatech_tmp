/*
 * Created on May 24, 2005
 */
package com.usatech.iso8583.interchange.fhms.jpos.packager;

import org.jpos.iso.*;

public class Field63DUKPTKeySerialNumber33Packager extends ISOBasePackager
{
	private ISOFieldPackager fld[] = { new IFA_NUMERIC(0, "MTI PLACEHOLDER"), new IFA_NUMERIC(0, "BITMAP PLACEHOLDER"), new FixedLengthCharRightSpacePadded(20, "DUK/PT KEY SERIAL NUMBER") };

	public Field63DUKPTKeySerialNumber33Packager()
	{
		super();
		setFieldPackager(this.fld);
	}
}
