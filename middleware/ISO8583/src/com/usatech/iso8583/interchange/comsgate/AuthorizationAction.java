package com.usatech.iso8583.interchange.comsgate;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.interchange.comsgate.message.Request;
import com.usatech.iso8583.interchange.comsgate.message.Response;

public class AuthorizationAction extends ComsGateAction {
	private static int[] requiredFields = {
		ISO8583Message.FIELD_ACQUIRER_ID,
		ISO8583Message.FIELD_TERMINAL_ID,
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_AMOUNT,
		ISO8583Message.FIELD_POS_ENVIRONMENT, 
		ISO8583Message.FIELD_ENTRY_MODE
	};
	
	public AuthorizationAction(ComsGateInterchange interchange) {
		super(interchange);
	}
	
	protected void validateRequestSpecificFields(ISO8583Request request) throws ValidationException
	{
		validateRequestCardData(request);
	}
	
	protected void initTransaction(ISO8583Request isoRequest, Request request) throws ValidationException
	{
		request.setTransactionType("Auth Only");
		request.setTransactionId(getTransactionId(isoRequest, "1"));
		String amount = String.valueOf(getTransactionAmount(isoRequest));
		request.setAmountOfSale(amount);
		request.setSubtotal(amount);
		request.setGrandTotal(amount);
		if (isoRequest.hasTrackData())
		{			
			if (isoRequest.getTrackData().hasTrack2())
				request.setTrack2(isoRequest.getTrackData().getTrack2());
			else if (isoRequest.getTrackData().hasTrack1())
				request.setTrack1(isoRequest.getTrackData().getTrack1());
		}
		else
		{	
			request.setCardNumber(isoRequest.getPanData().getPan());			
			request.setExpDate(isoRequest.getPanData().getExpiration());
		}
	}

	protected int[] getRequiredFields() {
		return requiredFields;
	}		
	
	public static void main(String[] args) throws Exception {
		Request request = new Request();
		request.setVersion("2.6");
		request.setIdentification("WT.27680.0001");
		request.setMerchantId("ONNNKILEOPHEBMHTKWLMLDQLNYBFYCCAIBOEYEYKOYXGGCECXQVDWVAENGJXTXKJ");
		request.setTerminalId("0001");
		request.setTransactionType("Auth Only");
		request.setTransactionId(String.valueOf(Long.valueOf(System.currentTimeMillis() / 1000)));
		request.setDateAndTime("01/31/12 10:13:04");
		request.setTrack2("4003000123456781=1225");
		request.setCardPresent("1");
		request.setAmountOfSale("1.30");
		request.setSubtotal("1.30");
		request.setGrandTotal("1.30");
		request.setDataSource("03");
		request.setCardReaderType("0");
		
		PostMethod method = new PostMethod("https://webtest.comstarinteractive.com/comsgate.asp");
		String requestXML = Request.toXML(request);
		method.addParameter("TransactionData", requestXML);
		HttpClient client = new HttpClient();
		int code = client.executeMethod(null, method);
		if(code != HttpStatus.SC_OK)
        	throw new Exception("Request failed with status code " + code + " (" + method.getStatusText() + ")");
		String responseXML = method.getResponseBodyAsString();
		System.out.println(responseXML);		
		Response response = Response.fromXML(responseXML);		
		System.out.println(response.toString());
	}
}
