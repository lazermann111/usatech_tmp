package com.usatech.iso8583.interchange.comsgate;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.interchange.comsgate.message.Request;
import com.usatech.iso8583.interchange.comsgate.message.Response;
import com.usatech.iso8583.util.Helper;

public class ReversalAction extends ComsGateAction {
	private static int[] requiredFields = { 
		ISO8583Message.FIELD_ACQUIRER_ID, 
		ISO8583Message.FIELD_TERMINAL_ID, 
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_AMOUNT
	};
	
	public ReversalAction(ComsGateInterchange interchange) {
		super(interchange);
	}
	
	@Override
	protected void initTransaction(ISO8583Request isoRequest, Request request) throws ValidationException
	{
		request.setTransactionType("Reversal");
		request.setOriginalTransactionType("Auth Only");
		request.setTransactionId(getTransactionId(isoRequest, "4"));
		
		String gatewayTxnId = getMiscFieldValue(isoRequest, MISC_FIELD_GATEWAY_TXN_ID);
		if (!Helper.isBlank(gatewayTxnId))
			request.setOriginalId(gatewayTxnId);
		else
			request.setOriginalId(String.valueOf(isoRequest.getTraceNumber()) + "1");
		
		String originalGatewayReferenceNumber = getMiscFieldValue(isoRequest, MISC_FIELD_GATEWAY_REF_NUMBER);
		if (!Helper.isBlank(originalGatewayReferenceNumber))		
			request.setOriginalGatewayReferenceNumber(originalGatewayReferenceNumber);
				
		String originalAmount;
		if (isoRequest.hasOriginalAmount())
			originalAmount = String.valueOf(getOriginalAmount(isoRequest));
		else
			originalAmount = getMiscFieldValue(isoRequest, MISC_FIELD_AUTHORIZED_AMOUNT);
		if (!Helper.isBlank(originalAmount))
			request.setOriginalAmount(originalAmount);
		
		request.setAmountOfSale("0.00");
		request.setSubtotal("0.00");
		request.setGrandTotal("0.00");
	}

	@Override
	protected int[] getRequiredFields() {
		return requiredFields;
	}
	
	public static void main(String[] args) throws Exception {
		Request request = new Request();
		request.setVersion("2.6");
		request.setIdentification("WT.27680.0001");
		request.setMerchantId("ONNNKILEOPHEBMHTKWLMLDQLNYBFYCCAIBOEYEYKOYXGGCECXQVDWVAENGJXTXKJ");
		request.setTerminalId("0001");
		request.setTransactionType("Auth Only");
		request.setTransactionId(String.valueOf(Long.valueOf(System.currentTimeMillis() / 1000)));
		request.setDateAndTime("01/31/12 10:13:04");
		request.setTrack2("4003000123456781=1225");
		request.setCardPresent("1");
		request.setAmountOfSale("1.30");
		request.setSubtotal("1.30");
		request.setGrandTotal("1.30");
		request.setDataSource("03");
		request.setCardReaderType("0");
		
		PostMethod method = new PostMethod("https://webtest.comstarinteractive.com/comsgate.asp");
		String requestXML = Request.toXML(request);
		method.addParameter("TransactionData", requestXML);
		HttpClient client = new HttpClient();
		int code = client.executeMethod(null, method);
		if(code != HttpStatus.SC_OK)
        	throw new Exception("Request failed with status code " + code + " (" + method.getStatusText() + ")");
		String responseXML = method.getResponseBodyAsString();
		System.out.println(responseXML);		
		Response response = Response.fromXML(responseXML);		
		System.out.println(response.toString());
	}	
}
