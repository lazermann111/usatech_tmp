package com.usatech.iso8583.interchange.comsgate;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.ConnectionPoolTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.LogFactory;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.interchange.comsgate.message.Request;
import com.usatech.iso8583.interchange.comsgate.message.Response;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;
import com.usatech.iso8583.util.Helper;

public class ComsGateInterchange implements ISO8583Interchange
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(ComsGateInterchange.class);

	public static final String INTERCHANGE_NAME = "COMSGATE";
	public static final String COMSGATE_VERSION = "2.6";
	public static final String COMSGATE_DATA_SOURCE = "03";
	
	public static final String PROPERTY_REQUEST_URL = "comsgate.request.url";
	public static final String PROPERTY_REQUEST_IDENTIFICATION = "comsgate.request.identification";
	public static final String PROPERTY_REQUEST_CONNECTION_TIMEOUT = "comsgate.request.connectionTimeoutMs";
	public static final String PROPERTY_REQUEST_SOCKET_TIMEOUT = "comsgate.request.socketTimeoutMs";
	
	public static ThreadLocal<SimpleDateFormat> TRAN_DATETIME_FORMAT = new ThreadLocal<SimpleDateFormat>()
	{
		protected synchronized SimpleDateFormat initialValue()
		{
			return new SimpleDateFormat("MM/dd/yy HH:mm:ss");
		}
	};
	
	private boolean isStarted = false;

	private Configuration config;

	private final ReversalAction reversalAction;
	private final AuthorizationAction authorizationAction;
	private final ReturnAction returnAction;
	private final ForceAction forceAction;
	private final SaleAction saleAction;
	private final SettlementAction settlementAction;
	private final InquiryAction inquiryAction;
	
	private String requestUrl;
	private String requestIdentification;
	private int requestConnectionTimeoutMs;
	private int requestSocketTimeoutMs;
	
	private static SortedSet<String> connectionErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = -8044441696861035529L;
			{
				add(ComsGateAction.CG_RESPONSE_DEVICE_NOT_CONFIGURED);
				add(ComsGateAction.CG_RESPONSE_INVALID_MERCHANT_ACCOUNT);
				add(ComsGateAction.CG_RESPONSE_OUT_OF_SEQUENCE);
				add(ComsGateAction.CG_RESPONSE_PROCESSOR_UNAVAILABLE);
				add(ComsGateAction.CG_RESPONSE_INVALID_TRANSACTION_DATA);
				add(ComsGateAction.CG_RESPONSE_MERCHANT_ACCOUNT_DEACTIVE);
				add(ComsGateAction.CG_RESPONSE_TRANSACTION_NOT_SUPPORTED);
				add(ComsGateAction.CG_RESPONSE_CA_MERCHANT_NOT_ALLOWED);
				add(ComsGateAction.CG_RESPONSE_CA_INVALID_PROCESSOR_INFO);
				add(ComsGateAction.CG_RESPONSE_CA_NO_SUCH_TRANSACTION);
			}});
	
	private static SortedSet<String> responseTimeoutErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = 4373530379880711927L;

			{
				add(ComsGateAction.CG_RESPONSE_NO_RESPONSE_FROM_SERVER);
				add(ComsGateAction.CG_RESPONSE_INVALID_RESPONSE);
				add(ComsGateAction.CG_RESPONSE_SERVER_BUSY);
				add(ComsGateAction.CG_RESPONSE_CA_BATCH_CLOSE_IN_PROGRESS);
				add(ComsGateAction.CG_RESPONSE_PROCESSOR_ERROR);
				add(ComsGateAction.CG_RESPONSE_UNSPECIFIED_ERROR);
			}});

	public ComsGateInterchange(ThreadPoolExecutor threadPool, ISO8583TransactionManager txnManager, Configuration config) throws ConfigurationException 
	{
		this.config = config;
		reversalAction = new ReversalAction(this);
		authorizationAction = new AuthorizationAction(this);
		forceAction = new ForceAction(this);
		returnAction = new ReturnAction(this);
		saleAction = new SaleAction(this);
		settlementAction = new SettlementAction(this);
		inquiryAction = new InquiryAction(this);
		
		if (config instanceof FileConfiguration)
		{
			((FileConfiguration) config).setReloadingStrategy(new ConfigReloader());
		}		
	}
	
	private ComsGateAction comsgateActionFactory(ISO8583Transaction transaction)
	{
		String transactionType = transaction.getRequest().getTransactionType();
		ComsGateAction action = null;
		if(ISO8583Message.TRANSACTION_TYPE_AUTHORIZATION.equals(transactionType))
			action = authorizationAction;
		else if(ISO8583Message.TRANSACTION_TYPE_SALE.equals(transactionType)){
			if(transaction.getRequest().getAmount() == 0)
				action = reversalAction;
			else if(transaction.getRequest().hasApprovalCode())
				action = forceAction;
			else
				action = saleAction;
		} else if(ISO8583Message.TRANSACTION_TYPE_REFUND.equals(transactionType))
			action = returnAction;
		else if(ISO8583Message.TRANSACTION_TYPE_SETTLEMENT.equals(transactionType)
				|| ISO8583Message.TRANSACTION_TYPE_SETTLEMENT_RETRY.equals(transactionType))
			action = settlementAction;
		else if(ISO8583Message.TRANSACTION_TYPE_REVERSAL.equals(transactionType))
				action = reversalAction;
		if (action == reversalAction) {
			String originalGatewayReferenceNumber = reversalAction.getMiscFieldValue(transaction.getRequest(), ComsGateAction.MISC_FIELD_GATEWAY_REF_NUMBER);
			if (Helper.isBlank(originalGatewayReferenceNumber))
				action = inquiryAction;
		}
		return action;
	}
	
	public Response sendRequest(Request request) throws Exception 
	{
		PostMethod method = new PostMethod(requestUrl);
		String requestXML = Request.toXML(request);
		method.addParameter("TransactionData", requestXML);
		HttpClient client = new HttpClient();
		client.getHttpConnectionManager().getParams().setConnectionTimeout(requestConnectionTimeoutMs);
		client.getHttpConnectionManager().getParams().setSoTimeout(requestSocketTimeoutMs);
		
		int code = client.executeMethod(null, method);
		if(code != HttpStatus.SC_OK)
        	throw new Exception("ComsGate request failed with status code " + code + " (" + method.getStatusText() + ")");
		String responseXML = method.getResponseBodyAsString();
		return Response.fromXML(responseXML);
	}
	
	public String getName() {
		return INTERCHANGE_NAME;
	}

	public boolean isConnected() {
		return true;
	}

	public boolean isSimulationMode() {
		return false;
	}
	
	public synchronized void stop() {
		if (!isStarted)
			return;

		log.info("ComsGate Interchange Shutting down...");

		isStarted = false;
	}
	
	public boolean isStarted() {
		return isStarted;
	}
	
	private ISO8583Response getErrorISO8583Response (ISO8583Request isoRequest, String responseCode, String responseMessage) {
		return new ISO8583Response(responseCode, responseMessage);
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		checkConfigChanges();
		
		ISO8583Request isoRequest = transaction.getRequest();
		ISO8583Response isoResponse;
		String transactionType = transaction.getRequest().getTransactionType();
		ComsGateAction action = comsgateActionFactory(transaction);

		if (action == null) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Processing failed: Unsupported transaction type: " + transactionType);
			return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_UNSUPPORTED_TRANSACTION_TYPE, "Unsupported transaction type: " + transactionType);
		} else if (action instanceof SettlementAction) {
			isoResponse = new ISO8583Response();
			isoResponse.setResponseCode(ISO8583Response.SUCCESS);
			isoResponse.setTraceNumber(isoRequest.getTraceNumber());
			isoResponse.setAmount(isoRequest.getAmount());
			transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);
			return isoResponse;
		}
		
		while (true) {
			transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);
	
			Request request;
			try {
				action.validateRequest(isoRequest);
				request = createRequest(isoRequest, action);
				transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
			} catch (ValidationException e) {
				transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
				log.error(transaction + " failed input validation: " + e.getMessage(), e);
				return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
			} catch (Throwable e) {
				transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
				log.error("Error creating request for " + transaction + ": " + e.getMessage(), e);
				return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Error creating request: " + e.getMessage());
			}
			
			transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
					
			Response response;
			try {
				response = sendRequest(request);
				transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);
			} catch (Throwable e) {
				String connectionError = checkConnectionFailure(e);
				if (connectionError != null) {
					log.error(connectionError + " transmitting " + transaction);
					return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, connectionError);
				}
				transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				String readTimeoutError = checkReadTimeout(e);
				if (readTimeoutError != null) {
					log.error(readTimeoutError + " transmitting " + transaction);
					return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, readTimeoutError);
				}
				log.error("Error transmitting " + transaction + ": " + e.getMessage(), e);
				return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting request: " + e.getMessage());
			}
			
			transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);
	
			isoResponse = new ISO8583Response();
			try {
				if(action.processResponse(isoRequest, isoResponse, request, response)) {
					transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);
					if (responseTimeoutErrorCodes.contains(isoResponse.getResponseCode()))
						isoResponse.setResponseCode(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT);
					else if (action == inquiryAction) {
						if ("000".equalsIgnoreCase(isoResponse.getResponseCode())) {
							isoRequest.setMiscData(isoResponse.getMiscData());
							action = reversalAction;
							continue;
						} else
							isoResponse.setResponseCode(ISO8583Response.SUCCESS);
					}
					return isoResponse;
				} else {
					String responseCode = response.getResponseCode();
					StringBuilder sb = new StringBuilder("Error processing ").append(transaction)
						.append(", responseCode: ").append(responseCode);
					if (response.getResponseText() != null)
						sb.append(", responseText: ").append(response.getResponseText());
					log.error(sb);
					if (connectionErrorCodes.contains(responseCode))
						return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, sb.toString());
					else {	
						transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
						if (responseTimeoutErrorCodes.contains(responseCode))
							return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, sb.toString());
						else					
							return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_INTERNAL_ERROR, sb.toString());
					}
				}
			} catch (Throwable e) {
				transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				log.error("Error constructing response to " + transaction + ": " + e.getMessage() + ", response: " + isoResponse, e);
				return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
			}
		}
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		return true;
	}

	public void start() {
		if (isStarted)
			return;

		log.info("ComsGate Interchange Starting up...");
		loadConfig();

		isStarted = true;
	}	
	
	private synchronized void loadConfig() {
		log.info("Loading configuration...");		
		requestUrl = config.getString(PROPERTY_REQUEST_URL);
		requestIdentification = config.getString(PROPERTY_REQUEST_IDENTIFICATION);
		requestConnectionTimeoutMs = config.getInt(PROPERTY_REQUEST_CONNECTION_TIMEOUT);
		requestSocketTimeoutMs = config.getInt(PROPERTY_REQUEST_SOCKET_TIMEOUT);
	}

	public synchronized void setConfiguration(Configuration config)
	{
		if (isStarted)
			loadConfig();
		else
			this.config = config;
	}	
	
	public Configuration getConfiguration() {
		return config;
	}
	
	private String getCredentialValue(String credentialList, String credentialToken, String defaultCredentialValue) {
		int pos1 = credentialList.indexOf(credentialToken);
		if (pos1 < 0)
			return defaultCredentialValue;
		int pos2 = credentialList.indexOf(",", pos1);
		if (pos2 < 0)
			return credentialList.substring(pos1 + credentialToken.length());
		else
			return credentialList.substring(pos1 + credentialToken.length(), pos2);
	}
	
	private Request createRequest(ISO8583Request isoRequest, ComsGateAction action) throws ValidationException
	{
		String merchantCd = isoRequest.getAcquirerID();
		String terminalCd = isoRequest.getTerminalID();
		
		Request request = new Request();
		request.setResponseType("0");
		request.setVersion(COMSGATE_VERSION);
		request.setDataSource(COMSGATE_DATA_SOURCE);
		request.setIdentification(getCredentialValue(terminalCd, "IID=", getCredentialValue(merchantCd, "IID=", requestIdentification)));
		request.setMerchantId(getCredentialValue(terminalCd, "MID=", getCredentialValue(merchantCd, "MID=", merchantCd)));
		request.setTerminalId(getCredentialValue(terminalCd, "TID=", getCredentialValue(merchantCd, "TID=", terminalCd)));
		Date effectiveDate = isoRequest.getEffectiveDate() == null ? new Date() : isoRequest.getEffectiveDate();
		request.setDateAndTime(TRAN_DATETIME_FORMAT.get().format(effectiveDate));
		if (isoRequest.getPosEnvironment() != null 
				&& (isoRequest.getPosEnvironment().equalsIgnoreCase(ISO8583Message.POS_ENVIRONMENT_ECOMMERCE)
				|| isoRequest.getPosEnvironment().equalsIgnoreCase(ISO8583Message.POS_ENVIRONMENT_MAIL_TELEPHONE)))
			request.setCardPresent("0");
		else
			request.setCardPresent("1");
		if (isoRequest.getEntryMode() != null && isoRequest.getEntryMode().equalsIgnoreCase(ISO8583Message.ENTRY_MODE_CONTACTLESS_SWIPED))
			request.setCardReaderType("1");
		else
			request.setCardReaderType("0");
		action.initTransaction(isoRequest, request);
		
		StringBuilder sb = new StringBuilder(ComsGateAction.FS);
		if (!Helper.isBlank(request.getTransactionId()))
			sb.append(ComsGateAction.MISC_FIELD_GATEWAY_TXN_ID).append('=').append(request.getTransactionId()).append(ComsGateAction.FS);
		if (!Helper.isBlank(request.getGrandTotal()))
			sb.append(ComsGateAction.MISC_FIELD_AUTHORIZED_AMOUNT).append('=').append(request.getGrandTotal()).append(ComsGateAction.FS);
		isoRequest.setMiscData(sb.toString());		
		
		return request;
	}
	
	private String checkConnectionFailure(Throwable e)
	{
		Throwable cause = e;
		while (cause.getCause() != null)
		{
			cause = cause.getCause();
		}
		
		if (cause != null)
		{
			String causeMessage = cause.getMessage().toLowerCase();
			if (cause.getClass() == ConnectException.class
					|| cause.getClass() == ConnectionPoolTimeoutException.class
					|| cause.getClass() == ConnectTimeoutException.class
					|| cause.getClass() == MalformedURLException.class
					|| cause.getClass() == NoRouteToHostException.class
					|| (cause.getClass() == SocketException.class && causeMessage.indexOf("network is unreachable") > -1)
					|| (cause.getClass() == SocketTimeoutException.class && causeMessage.indexOf("connect timed out") > -1)
					|| cause.getClass() == UnknownHostException.class
					|| cause.getClass() == URIException.class
					|| cause.getClass() == URISyntaxException.class)
				return "Error connecting to host, " + cause.getClass().getName() + ": " + cause.getMessage();
		}
		return null;
	}
	
	private String checkReadTimeout(Throwable e)
	{
		Throwable cause = e;
		while (cause.getCause() != null)
		{
			cause = cause.getCause();
		}
		
		if (cause != null)
		{
			String causeMessage = cause.getMessage().toLowerCase();
			if (cause.getClass() == SocketTimeoutException.class && causeMessage.indexOf("read timed out") > -1)
				return "Timeout while waiting for host response, " + cause.getClass().getName() + ": " + cause.getMessage();
		}
		return null;
	}	
	
	private void checkConfigChanges()
	{
		//check configuration file for changes
		config.containsKey("ping");
	}
	
	private class ConfigReloader extends FileChangedReloadingStrategy
	{
		private ConfigReloader()
		{
			super();
		}

		@Override
		public void reloadingPerformed()
		{
			super.reloadingPerformed();

			log.warn("Configuration change detected");
			loadConfig();
		}
	}	
}
