package com.usatech.iso8583.interchange.comsgate;

import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.interchange.comsgate.message.Request;

public class SettlementAction extends ComsGateAction
{
	public SettlementAction(ComsGateInterchange interchange) {
		super(interchange);
	}
	
	protected void initTransaction(ISO8583Request isoRequest, Request request) throws ValidationException {
		
	}

	@Override
	protected int[] getRequiredFields() {
		return null;
	}
}
