package com.usatech.iso8583.interchange.comsgate;

import java.math.BigDecimal;
import java.text.ParseException;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.comsgate.message.Request;
import com.usatech.iso8583.interchange.comsgate.message.Response;
import com.usatech.iso8583.util.CreditCardValidator;
import com.usatech.iso8583.util.Helper;
import com.usatech.iso8583.util.ISO8583Util;

public abstract class ComsGateAction {
	private ComsGateInterchange interchange;
	
	public static final String GW_RESPONSE_CODE_PREFIX = "GW-";
	public static final char FS = 0x1C;	
	public static final String MISC_FIELD_GATEWAY_TXN_ID = "gwTxnId";
	public static final String MISC_FIELD_GATEWAY_REF_NUMBER = "gwRefNum";
	public static final String MISC_FIELD_PROCESSOR_RESPONSE_CD = "responseCd";
	public static final String MISC_FIELD_AUTHORIZED_AMOUNT = "authedAmt";
	
	public static final String CG_RESPONSE_APPROVED = "000";
	public static final String CG_RESPONSE_APPROVED_DUPLICATE = "094";
	public static final String CG_RESPONSE_DEVICE_NOT_CONFIGURED = "FE0";
	public static final String CG_RESPONSE_INVALID_MERCHANT_ACCOUNT = "FE1";
	public static final String CG_RESPONSE_OUT_OF_SEQUENCE = "FE2";
	public static final String CG_RESPONSE_PROCESSOR_UNAVAILABLE = "FE3";
	public static final String CG_RESPONSE_NO_RESPONSE_FROM_SERVER = "FE4";
	public static final String CG_RESPONSE_INVALID_RESPONSE = "FE5";
	public static final String CG_RESPONSE_INVALID_TRANSACTION_DATA = "FE6";
	public static final String CG_RESPONSE_SERVER_BUSY = "FE7";
	public static final String CG_RESPONSE_MERCHANT_ACCOUNT_DEACTIVE = "FE8";
	public static final String CG_RESPONSE_TRANSACTION_NOT_SUPPORTED = "FE9";
	public static final String CG_RESPONSE_CA_MERCHANT_NOT_ALLOWED = "FEA";
	public static final String CG_RESPONSE_CA_INVALID_PROCESSOR_INFO = "FEB";
	public static final String CG_RESPONSE_CA_BATCH_CLOSE_IN_PROGRESS = "FEC";
	public static final String CG_RESPONSE_CA_NO_SUCH_TRANSACTION = "FED";
	public static final String CG_RESPONSE_PROCESSOR_ERROR = "FFE";
	public static final String CG_RESPONSE_UNSPECIFIED_ERROR = "FFF";
	
	protected ComsGateAction(ComsGateInterchange interchange)
	{
		this.interchange = interchange;
	}
	
	abstract protected int[] getRequiredFields();
	abstract protected void initTransaction(ISO8583Request isoRequest, Request request) throws ValidationException;

	public ComsGateInterchange getInterchange() {
		return interchange;
	}
	
	public String getTransactionId(final ISO8583Request isoRequest, String suffix) {
		return ISO8583Util.rightJustify(new StringBuilder().append(isoRequest.getTraceNumber()).append(suffix).toString(), 9, '0');
	}
	
	public BigDecimal getTransactionAmount(final ISO8583Request isoRequest)
	{
		return BigDecimal.valueOf(isoRequest.getAmount()).movePointLeft(2);
	}
	
	public BigDecimal getOriginalAmount(final ISO8583Request isoRequest)
	{
		return BigDecimal.valueOf(isoRequest.getOriginalAmount()).movePointLeft(2);
	}
	
	public void setISOResponseAmount(Response response, ISO8583Response isoResponse) {
		if (Helper.isDouble(response.getAuthorizedAmount())) {
			String authedAmount = String.valueOf(BigDecimal.valueOf(Double.parseDouble(response.getAuthorizedAmount())).movePointRight(2));
			if (Helper.isInteger(authedAmount))
				isoResponse.setAmount(Integer.parseInt(authedAmount));
		}
	}
	
	public String getMiscFieldValue(final ISO8583Request isoRequest, String miscField)
	{
		if (isoRequest.hasMiscData()) { 
			String miscData = isoRequest.getMiscData();
			StringBuilder sb = new StringBuilder(FS).append(miscField).append('=');
			int fromIndex = miscData.indexOf(sb.toString());
			if (fromIndex > -1)
				return miscData.substring(miscData.indexOf('=', fromIndex) + 1, miscData.indexOf(FS, fromIndex));
		}
		return null;
	}
	
	public void validateRequest(ISO8583Request request) throws ValidationException
	{
		int[] requiredFields = getRequiredFields();
		if (requiredFields != null)
		{
			for (int i = 0; i < requiredFields.length; i++) {
				if (!request.hasField(requiredFields[i])){
					throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(requiredFields[i]));
				}
			}
		}

		if (request.hasTrackData()) {
			if (!request.getTrackData().hasTrack1() && !request.getTrackData().hasTrack2()){
				throw new ValidationException("Required Field Not Found: TrackData Tracks");
			}
		}

		if (request.hasPanData()) {
			if (request.getPanData().getPan() == null){
				throw new ValidationException("Required Field Not Found: PanData PAN");
			}
			
			if (request.getPanData().getExpiration() == null)
				throw new ValidationException("Required Field Not Found: PanData Expiration");

			try
			{
				CreditCardValidator.validatePanData(request.getPanData());
			}
			catch (NumberFormatException e)
			{
				throw new ValidationException("Invalid Pan Data: " + e.getMessage());
			}			
		}
		
		validateRequestSpecificFields(request);
	}
	
	protected void validateRequestCardData(ISO8583Request request) throws ValidationException
	{
		if (!request.hasPanData() && !request.hasTrackData())
			throw new ValidationException("Required Field Not Found: " 
					+ ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) 
					+ " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));
	}
	
	protected void validateRequestSpecificFields(ISO8583Request request) throws ValidationException
	{
		
	}
	
	protected boolean parseResponse(Response response, ISO8583Response isoResponse) throws ParseException
	{		
		StringBuilder responseMessage = new StringBuilder("ComsGate: ");
		String responseCode = response.getResponseCode();
		if (responseCode == null || responseCode.length() == 0)
			isoResponse.setResponseCode(ISO8583Response.ERROR_INTERNAL_ERROR);
		else {
			if (responseCode.equalsIgnoreCase(CG_RESPONSE_APPROVED) || responseCode.equalsIgnoreCase(CG_RESPONSE_APPROVED_DUPLICATE)) 
				isoResponse.setResponseCode(responseCode);				
			else
				isoResponse.setResponseCode(new StringBuilder(GW_RESPONSE_CODE_PREFIX).append(responseCode).toString());
			responseMessage.append(responseCode);
		}
		
		responseMessage.append(", Processor: ");
		if (response.getProcessorResponseCode() != null)
			responseMessage.append(response.getProcessorResponseCode());
		
		responseMessage.append(", Message: ");
		if (!Helper.isBlank(response.getResponseText()))			
			responseMessage.append(response.getResponseText());
		isoResponse.setResponseMessage(responseMessage.toString());
		
		if (response.getTransactionId() != null && response.getTransactionId().length() > 1) {
			String tranId = response.getTransactionId().substring(0, response.getTransactionId().length() - 1);
			if (Helper.isInteger(tranId))
				isoResponse.setTraceNumber(Integer.parseInt(tranId));
		}
		if (!Helper.isBlank(response.getReferenceNumber()))
			isoResponse.setRetrievalReferenceNumber(response.getReferenceNumber());
		if (!Helper.isBlank(response.getApprovalCode()))
			isoResponse.setApprovalCode(response.getApprovalCode());
		setISOResponseAmount(response, isoResponse);
		return true;
	}
	
	protected boolean processResponse(ISO8583Request isoRequest, ISO8583Response isoResponse, Request request, Response response) throws ParseException
	{
		boolean success = parseResponse(response, isoResponse);
		
		StringBuilder sb = new StringBuilder(FS);
		if (!Helper.isBlank(response.getTransactionId()))
			sb.append(MISC_FIELD_GATEWAY_TXN_ID).append('=').append(response.getTransactionId()).append(FS);
		else if (!Helper.isBlank(request.getTransactionId()))
			sb.append(MISC_FIELD_GATEWAY_TXN_ID).append('=').append(request.getTransactionId()).append(FS);
		if (!Helper.isBlank(response.getGatewayReferenceNumber()))
			sb.append(MISC_FIELD_GATEWAY_REF_NUMBER).append('=').append(response.getGatewayReferenceNumber()).append(FS);
		if (!Helper.isBlank(response.getProcessorResponseCode()))
			sb.append(MISC_FIELD_PROCESSOR_RESPONSE_CD).append('=').append(response.getProcessorResponseCode()).append(FS);
		if (!Helper.isBlank(response.getAuthorizedAmount()))
			sb.append(MISC_FIELD_AUTHORIZED_AMOUNT).append('=').append(response.getAuthorizedAmount()).append(FS);
		else if (!Helper.isBlank(request.getGrandTotal()))
			sb.append(MISC_FIELD_AUTHORIZED_AMOUNT).append('=').append(request.getGrandTotal()).append(FS);
		isoResponse.setMiscData(sb.toString());
		
		return success;
	}
}
