package com.usatech.iso8583.interchange.comsgate.message;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class Request {
	protected final static XStream xstream = new XStream(new DomDriver()) {
        // ignore unknown XML tags
		protected MapperWrapper wrapMapper(MapperWrapper next) {
            return new MapperWrapper(next) {
                public boolean shouldSerializeMember(@SuppressWarnings("rawtypes") Class definedIn, String fieldName) {
                    try {
                        return definedIn != Object.class || realClass(fieldName) != null;
                    } catch(CannotResolveClassException cnrce) {
                        return false;
                    }
                }
            };
        }
    };		
	
    static {
		xstream.alias("AB", Request.class);
		xstream.aliasField("4", Request.class, "responseType");
		xstream.aliasField("AS", Request.class, "version");
		xstream.aliasField("BB", Request.class, "merchantId");
		xstream.aliasField("BC", Request.class, "terminalId");
		xstream.aliasField("AP", Request.class, "identification");
		xstream.aliasField("AR", Request.class, "dataSource");
		xstream.aliasField("C", Request.class, "transactionType");
		xstream.aliasField("A", Request.class, "transactionId");
		xstream.aliasField("B", Request.class, "dateAndTime");
		xstream.aliasField("D", Request.class, "track1");
		xstream.aliasField("AJ", Request.class, "track2");
		xstream.aliasField("F", Request.class, "cardNumber");
		xstream.aliasField("G", Request.class, "expDate");
		xstream.aliasField("BA", Request.class, "cardReaderType");
		xstream.aliasField("AI", Request.class, "cardPresent");
		xstream.aliasField("T", Request.class, "amountOfSale");
		xstream.aliasField("V", Request.class, "subtotal");
		xstream.aliasField("Z", Request.class, "grandTotal");
		xstream.aliasField("AK", Request.class, "originalId");
		xstream.aliasField("AG", Request.class, "originalApprovalCode");
		xstream.aliasField("AL", Request.class, "originalTransactionType");
		xstream.aliasField("CB", Request.class, "originalGatewayReferenceNumber");
		xstream.aliasField("AH", Request.class, "originalAmount");
    }
    
	public static String toXML(Request request) {
		return xstream.toXML(request);
	}
	
	protected String responseType;
	protected String version;
	protected String merchantId;
	protected String terminalId;
	protected String identification;
	protected String dataSource;
	protected String transactionType;
	protected String transactionId;
	protected String dateAndTime;
	protected String track1;
	protected String track2;
	protected String cardNumber;
	protected String expDate;
	protected String cardReaderType;
	protected String cardPresent;
	protected String amountOfSale;
	protected String subtotal;
	protected String grandTotal;
	protected String originalId;
	protected String originalApprovalCode;
	protected String originalTransactionType;
	protected String originalGatewayReferenceNumber;
	protected String originalAmount;
	
	public String getResponseType() {
		return responseType;
	}
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public String getIdentification() {
		return identification;
	}
	public void setIdentification(String identification) {
		this.identification = identification;
	}
	public String getDataSource() {
		return dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getDateAndTime() {
		return dateAndTime;
	}
	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	public String getTrack1() {
		return track1;
	}
	public void setTrack1(String track1) {
		this.track1 = track1;
	}
	public String getTrack2() {
		return track2;
	}
	public void setTrack2(String track2) {
		this.track2 = track2;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getCardReaderType() {
		return cardReaderType;
	}
	public void setCardReaderType(String cardReaderType) {
		this.cardReaderType = cardReaderType;
	}
	public String getCardPresent() {
		return cardPresent;
	}
	public void setCardPresent(String cardPresent) {
		this.cardPresent = cardPresent;
	}
	public String getAmountOfSale() {
		return amountOfSale;
	}
	public void setAmountOfSale(String amountOfSale) {
		this.amountOfSale = amountOfSale;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getOriginalId() {
		return originalId;
	}
	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}
	public String getOriginalApprovalCode() {
		return originalApprovalCode;
	}
	public void setOriginalApprovalCode(String originalApprovalCode) {
		this.originalApprovalCode = originalApprovalCode;
	}
	public String getOriginalTransactionType() {
		return originalTransactionType;
	}
	public void setOriginalTransactionType(String originalTransactionType) {
		this.originalTransactionType = originalTransactionType;
	}
	public String getOriginalGatewayReferenceNumber() {
		return originalGatewayReferenceNumber;
	}
	public void setOriginalGatewayReferenceNumber(String originalGatewayReferenceNumber) {
		this.originalGatewayReferenceNumber = originalGatewayReferenceNumber;
	}
	public String getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(String originalAmount) {
		this.originalAmount = originalAmount;
	}
	@Override
	public String toString() {
		return "Request [responseType=" + responseType + ", version=" + version + ", merchantId=" + merchantId + ", terminalId=" + terminalId + ", identification=" + identification + ", dataSource="
				+ dataSource + ", transactionType=" + transactionType + ", transactionId=" + transactionId + ", dateAndTime=" + dateAndTime + ", track1=" + track1 + ", track2=" + track2
				+ ", cardNumber=" + cardNumber + ", expDate=" + expDate + ", cardReaderType=" + cardReaderType + ", cardPresent=" + cardPresent + ", amountOfSale=" + amountOfSale + ", subtotal="
				+ subtotal + ", grandTotal=" + grandTotal + ", originalId=" + originalId + ", originalApprovalCode=" + originalApprovalCode + ", originalTransactionType=" + originalTransactionType
				+ ", originalGatewayReferenceNumber=" + originalGatewayReferenceNumber + ", originalAmount=" + originalAmount + "]";
	}
}
