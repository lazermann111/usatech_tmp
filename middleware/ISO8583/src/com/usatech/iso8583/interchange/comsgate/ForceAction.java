package com.usatech.iso8583.interchange.comsgate;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.interchange.comsgate.message.Request;
import com.usatech.iso8583.util.Helper;

public class ForceAction extends ComsGateAction {
	private static int[] requiredFields = { 
		ISO8583Message.FIELD_ACQUIRER_ID, 
		ISO8583Message.FIELD_TERMINAL_ID, 
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_AMOUNT,
		ISO8583Message.FIELD_APPROVAL_CODE,
		ISO8583Message.FIELD_POS_ENVIRONMENT, 
		ISO8583Message.FIELD_ENTRY_MODE, 
		ISO8583Message.FIELD_ONLINE,
		ISO8583Message.FIELD_MISC_DATA
	};
	
	public ForceAction(ComsGateInterchange interchange) {
		super(interchange);
	}
	
	@Override
	protected void initTransaction(ISO8583Request isoRequest, Request request) throws ValidationException
	{
		request.setTransactionType("Force");
		request.setOriginalTransactionType("Auth Only");
		request.setTransactionId(getTransactionId(isoRequest, "2"));
		
		String gatewayTxnId = getMiscFieldValue(isoRequest, MISC_FIELD_GATEWAY_TXN_ID);
		if (!Helper.isBlank(gatewayTxnId))
			request.setOriginalId(gatewayTxnId);
		else {
			if (isoRequest.hasTrackData())
			{			
				if (isoRequest.getTrackData().hasTrack2())
					request.setTrack2(isoRequest.getTrackData().getTrack2());
				else if (isoRequest.getTrackData().hasTrack1())
					request.setTrack1(isoRequest.getTrackData().getTrack1());
			}
			else
			{	
				request.setCardNumber(isoRequest.getPanData().getPan());			
				request.setExpDate(isoRequest.getPanData().getExpiration());
			}
		}
		
		String originalGatewayReferenceNumber = getMiscFieldValue(isoRequest, MISC_FIELD_GATEWAY_REF_NUMBER);
		if (!Helper.isBlank(originalGatewayReferenceNumber))		
			request.setOriginalGatewayReferenceNumber(originalGatewayReferenceNumber);
		
		request.setOriginalApprovalCode(isoRequest.getApprovalCode());
		
		String originalAmount;
		if (isoRequest.hasOriginalAmount())
			originalAmount = String.valueOf(getOriginalAmount(isoRequest));
		else
			originalAmount = getMiscFieldValue(isoRequest, MISC_FIELD_AUTHORIZED_AMOUNT);
		if (!Helper.isBlank(originalAmount))
			request.setOriginalAmount(originalAmount);
		
		String amount = String.valueOf(getTransactionAmount(isoRequest));
		request.setAmountOfSale(amount);
		request.setSubtotal(amount);
		request.setGrandTotal(amount);
	}

	@Override
	protected int[] getRequiredFields() {
		return requiredFields;
	}
}

