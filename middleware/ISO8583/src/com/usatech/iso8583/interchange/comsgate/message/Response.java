package com.usatech.iso8583.interchange.comsgate.message;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class Response {	
	protected final static XStream xstream = new XStream(new DomDriver()) {
		// ignore unknown XML tags
        protected MapperWrapper wrapMapper(MapperWrapper next) {
            return new MapperWrapper(next) {
                public boolean shouldSerializeMember(@SuppressWarnings("rawtypes") Class definedIn, String fieldName) {
                    try {
                        return definedIn != Object.class || realClass(fieldName) != null;
                    } catch(CannotResolveClassException cnrce) {
                        return false;
                    }
                }
            };
        }
    };		
	
    static {
		xstream.alias("AB", Response.class);
		xstream.aliasField("A", Response.class, "transactionId");
		xstream.aliasField("AC", Response.class, "responseCode");
		xstream.aliasField("AY", Response.class, "processorResponseCode");
		xstream.aliasField("AD", Response.class, "responseText");
		xstream.aliasField("AF", Response.class, "approvalCode");
		xstream.aliasField("AS", Response.class, "referenceNumber");
		xstream.aliasField("BI", Response.class, "gatewayReferenceNumber");
		xstream.aliasField("BZ", Response.class, "authorizedAmount");
		xstream.aliasField("BX", Response.class, "cardType");
    }
    
    public static Response fromXML(String responseXML) {
		return (Response)xstream.fromXML(new StringBuilder("<AB>").append(responseXML).append("</AB>").toString());
	}
	
	protected String transactionId;
	protected String responseCode;
	protected String processorResponseCode;
	protected String responseText;
	protected String approvalCode;
	protected String referenceNumber;
	protected String gatewayReferenceNumber;
	protected String authorizedAmount;
	protected String cardType;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getProcessorResponseCode() {
		return processorResponseCode;
	}
	public void setProcessorResponseCode(String processorResponseCode) {
		this.processorResponseCode = processorResponseCode;
	}
	public String getResponseText() {
		return responseText;
	}
	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getGatewayReferenceNumber() {
		return gatewayReferenceNumber;
	}
	public void setGatewayReferenceNumber(String gatewayReferenceNumber) {
		this.gatewayReferenceNumber = gatewayReferenceNumber;
	}
	public String getAuthorizedAmount() {
		return authorizedAmount;
	}
	public void setAuthorizedAmount(String authorizedAmount) {
		this.authorizedAmount = authorizedAmount;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	@Override
	public String toString() {
		return "Response [transactionId=" + transactionId + ", responseCode=" + responseCode + ", processorResponseCode=" + processorResponseCode + ", responseText=" + responseText
				+ ", approvalCode=" + approvalCode + ", referenceNumber=" + referenceNumber + ", gatewayReferenceNumber=" + gatewayReferenceNumber + ", authorizedAmount=" + authorizedAmount
				+ ", cardType=" + cardType + "]";
	}
}
