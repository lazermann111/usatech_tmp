package com.usatech.iso8583.interchange.comsgate;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.interchange.comsgate.message.Request;
import com.usatech.iso8583.interchange.comsgate.message.Response;
import com.usatech.iso8583.util.Helper;

public class InquiryAction extends ComsGateAction {
	private static int[] requiredFields = { 
		ISO8583Message.FIELD_ACQUIRER_ID, 
		ISO8583Message.FIELD_TERMINAL_ID, 
		ISO8583Message.FIELD_TRACE_NUMBER
	};
	
	public InquiryAction(ComsGateInterchange interchange) {
		super(interchange);
	}
	
	@Override
	protected void initTransaction(ISO8583Request isoRequest, Request request) throws ValidationException
	{
		request.setTransactionType("Inquiry");
		request.setOriginalTransactionType("Auth Only");
		request.setTransactionId(getTransactionId(isoRequest, "1"));
		
		String originalAmount;
		if (isoRequest.hasOriginalAmount())
			originalAmount = String.valueOf(getOriginalAmount(isoRequest));
		else
			originalAmount = getMiscFieldValue(isoRequest, MISC_FIELD_AUTHORIZED_AMOUNT);
		if (!Helper.isBlank(originalAmount))
			request.setOriginalAmount(originalAmount);
		
		String cardNumber = null;
		if (isoRequest.hasTrackData())
		{			
			if (isoRequest.getTrackData().hasTrack2())
				cardNumber = Helper.getCardNumber(isoRequest.getTrackData().getTrack2());
			else if (isoRequest.getTrackData().hasTrack1())
				cardNumber = Helper.getCardNumber(isoRequest.getTrackData().getTrack1());
		}
		else
			cardNumber = isoRequest.getPanData().getPan();
		if (cardNumber != null && cardNumber.length() > 3)
			request.setCardNumber(cardNumber.substring(cardNumber.length() - 4, cardNumber.length()));
	}

	@Override
	protected int[] getRequiredFields() {
		return requiredFields;
	}
	
	public static void main(String[] args) throws Exception {
		Request request = new Request();
		request.setVersion("2.6");
		request.setIdentification("WT.27680.0001");
		request.setMerchantId("ONNNKILEOPHEBMHTKWLMLDQLNYBFYCCAIBOEYEYKOYXGGCECXQVDWVAENGJXTXKJ");
		request.setTerminalId("0001");
		request.setTransactionType("Inquiry");
		request.setOriginalTransactionType("Auth Only");
		request.setTransactionId("008159371");
		request.setOriginalAmount("1.20");
		request.setCardNumber("1111");
		
		PostMethod method = new PostMethod("https://webtest.comstarinteractive.com/comsgate.asp");
		String requestXML = Request.toXML(request);
		method.addParameter("TransactionData", requestXML);
		HttpClient client = new HttpClient();
		int code = client.executeMethod(null, method);
		if(code != HttpStatus.SC_OK)
        	throw new Exception("Request failed with status code " + code + " (" + method.getStatusText() + ")");
		String responseXML = method.getResponseBodyAsString();
		System.out.println(responseXML);		
		Response response = Response.fromXML(responseXML);		
		System.out.println(response.toString());
	}	
}
