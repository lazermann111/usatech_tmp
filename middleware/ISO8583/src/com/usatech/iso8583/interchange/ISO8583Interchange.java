package com.usatech.iso8583.interchange;

import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.transaction.ISO8583Transaction;

import org.picocontainer.Startable;

public interface ISO8583Interchange extends Startable, Configurable
{
	public String getName();

	public boolean isStarted();

	public boolean isConnected();

	public ISO8583Response process(ISO8583Transaction transaction);

	public boolean recover(ISO8583Transaction transaction);
	
	public boolean isSimulationMode();
}
