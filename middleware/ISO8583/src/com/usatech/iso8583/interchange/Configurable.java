package com.usatech.iso8583.interchange;

import org.apache.commons.configuration.Configuration;

public interface Configurable
{
	public void setConfiguration(Configuration config);

	public Configuration getConfiguration();
}
