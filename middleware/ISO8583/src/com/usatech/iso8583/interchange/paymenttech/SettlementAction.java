package com.usatech.iso8583.interchange.paymenttech;

import ibizptech.IBizPtechException;
import ibizptech.Ptsettle;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.iso8583.BatchTotals;
import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.transaction.ISO8583Transaction;

public class SettlementAction extends PaymentTechAction {
	
	private static Log log = LogFactory.getLog(SettlementAction.class);

	private static int[] requiredFields = { ISO8583Message.FIELD_ACQUIRER_ID, ISO8583Message.FIELD_TERMINAL_ID, ISO8583Message.FIELD_TRACE_NUMBER, ISO8583Message.FIELD_BATCH_NUMBER, ISO8583Message.FIELD_BATCH_TOTALS };

	public SettlementAction(PaymentTechInterchange interchange) {
		super(interchange);
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException {
		super.validateRequest(request, requiredFields);

		BatchTotals batchTotals = request.getBatchTotals();
		if (batchTotals.getCreditSaleCount() < 0 || batchTotals.getCreditSaleCount() > 999)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Sale Count");

		if (batchTotals.getCreditSaleAmount() < 0)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Sale Amount");

		if (batchTotals.getCreditRefundCount() < 0 || batchTotals.getCreditRefundCount() > 999)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Refund Count");

		if (batchTotals.getCreditRefundAmount() < 0)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Refund Amount");

		if (request.hasTransactionList())
		{
			Iterator listIter = request.getTransactionList().iterator();
			while (listIter.hasNext())
			{
				//ISO8583Request subRequest = (ISO8583Request) listIter.next();
				// what to do?
			}
		}
	}
	
	public ISO8583Response process(ISO8583Transaction transaction)
	{
		log.debug("Settlement Action is processing ... ");
		
		ISO8583Request request = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);

		Ptsettle settle = null;
		
		log.debug("Transaction ID: " + transaction.getTransactionID());
		log.debug("Transaction Type: " + request.getTransactionType());
		
		try {
			validateRequest(request, requiredFields);
			
			settle = setSettleNetConnectParameter(request);
			
			transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
			
		} catch (ValidationException e) {
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		} catch (IBizPtechException e) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error(transaction + "Caught unexpected exception while setting the IBiz API parameters: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Caught unexpected exception while setting the IBiz API parameters: " + e.getMessage());	
		} catch (Throwable e) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Caught unexpected exception validating " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception validating request: " + e.getMessage());
		}

		try {
			log.debug("Authorizing charge...");
		
			settle.batchInquiry();
		
			if(request.hasBatchTotals()){
				// The refund amount is not included in the settle.getResponseBatchNetAmount(), we just release the credit sale amount.
				if(request.getBatchTotals().getCreditSaleAmount() != 0){
					// Note IBiz JavaDoc -"There should be no descrepencies between terminal and host before releasing the batch
					// call BatchInquiry and compare the ResponseBatchNetAmount to your stored value to make sure there are no descrepencies before releasing the batch
					// TODO There is an known issue that it seems IBiz API(or Paymentech Test Server close connection after every operation call. So we initialize another bean here.
					settle = setSettleNetConnectParameter(request);
					int netAmount = request.getBatchTotals().getCreditSaleAmount() - request.getBatchTotals().getCreditRefundAmount();
					settle.batchRelease(PaymentechUtil.buildAmount(netAmount));
				}
			}
		
		} catch (IBizPtechException e) {
			log.error("Caught unexpected exception IBiz API processing " + transaction + ": " + e.getMessage(), e);
			if (e.getCode()==201 || e.getCode()==301 || e.getCode()==700){
				transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, "Timeout occured waiting for host response.");
			}
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception IBiz API processing request: " + e.getMessage());
		} catch (Throwable e) {
			log.error("Caught unexpected exception transmitting " + transaction + ": " + e.getMessage(), e);
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting " + transaction + ": " + e.getMessage());
		}	

		ISO8583Response isoResponse = null;
		
		transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);
		
		try {
			isoResponse = readResponse(settle, request);
		} catch (ValidationException e) {
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION,"Host response failed input validation: " + e.getMessage());
		} catch (Throwable e) {
			log.error("Caught unexpected exception while constructing response: " + transaction.getTransactionID() + ": " + e.getMessage() + ", response: " + isoResponse, e);
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
		}	

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);

		return isoResponse;

	}
	
	public boolean recover(ISO8583Transaction transaction)
	{
		log.info("Skipping recovery of " + transaction + ": Not an online transaction");
		return true;
	}
}
