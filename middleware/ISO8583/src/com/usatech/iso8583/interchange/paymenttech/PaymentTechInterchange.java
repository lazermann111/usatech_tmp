package com.usatech.iso8583.interchange.paymenttech;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.logging.LogFactory;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;

/**
 * This class is an implementation of an ISO8583Interchange. It is responsible
 * for routing the request, ISO8583Transaction, to the proper PaymentTechAction.
 * 
 * @author jmcpherson
 * 
 */
public class PaymentTechInterchange implements ISO8583Interchange {

	private static org.apache.commons.logging.Log log = LogFactory
			.getLog(PaymentTechInterchange.class);

	public static final String INTERCHANGE_NAME = "PAYMENTTECH";
	
	public int recoverCount = 0;
	public int successCount = 0;
	public int failCount = 0;
	public int totalTrans = 0;
	
	private boolean isStarted = false;
	private boolean simulationMode = false;
	
	// default time out in seconds
	private int defaultResponseTimeout = 15;

	private ScheduledExecutorService executor;

	private Configuration config;

	private ThreadPoolExecutor threadPool;

	private ISO8583TransactionManager txnManager;
	
	private Map<String, PaymentTechAction> actions = new HashMap<String, PaymentTechAction>();

	public PaymentTechInterchange(ThreadPoolExecutor threadPool,ISO8583TransactionManager txnManager, Configuration config)
			throws ConfigurationException {
		this.threadPool = threadPool;
		this.config = config;
		this.txnManager = txnManager;

		// if (config instanceof FileConfiguration)
		// {
		// ((FileConfiguration) config).setReloadingStrategy(new
		// ConfigReloader());
		// }
		
		this.simulationMode = config.getBoolean("paymentech.netconnect.simulateMode");

		actions.put(ISO8583Message.TRANSACTION_TYPE_SALE, new SaleAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_AUTHORIZATION, new AuthorizationAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_REFUND, new RefundAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_REVERSAL, new ReversalAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_SALE, new SaleAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_SETTLEMENT, new SettlementAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_SETTLEMENT_RETRY, new SettlementAction(this));
		actions.put(ISO8583Message.TRANSACTION_TYPE_VOID, new VoidAction(this));	
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatech.iso8583.interchange.ISO8583Interchange#getName()
	 */
	public String getName() {
		return INTERCHANGE_NAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatech.iso8583.interchange.ISO8583Interchange#isConnected()
	 */
	public boolean isConnected() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatech.iso8583.interchange.ISO8583Interchange#isSimulationMode()
	 */
	public boolean isSimulationMode() {
		return simulationMode;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.picocontainer.Startable#stop()
	 */
	public synchronized void stop() {
		if (!isStarted)
			return;

		log.info("Payment Tech Interchange Shutting down...");
		
		executor.shutdownNow();

		isStarted = false;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatech.iso8583.interchange.ISO8583Interchange#isStarted()
	 */
	public boolean isStarted() {
		return isStarted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatech.iso8583.interchange.ISO8583Interchange#process(com.usatech.iso8583.transaction.ISO8583Transaction)
	 */
	public ISO8583Response process(ISO8583Transaction transaction) {
		// This is where the ISO8583Transaction is routed to from the
		// IS9593Gateway.
		log.info("total tansactions are: " + ++this.totalTrans);
		String transactionType = transaction.getRequest().getTransactionType();
		PaymentTechAction action = actions.get(transactionType);

		if (action == null) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Processing failed: Unsupported transaction type: " + transactionType);
			return new ISO8583Response( ISO8583Response.ERROR_UNSUPPORTED_TRANSACTION_TYPE, "Unsupported transaction type: " + transactionType);
		}
		
		ISO8583Response response = action.process(transaction);
		if(this.simulationMode){
			if(transaction.getState() == ISO8583Transaction.STATE_RESPONSE_VALIDATED){
				log.debug("Success Rate (Transactions) :" + ++ this.successCount + "/" + this.totalTrans);
			}else{
				log.debug("Fail Rate (Transactions) :" + ++ this.failCount + "/" + this.totalTrans);
			}
		}
		
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatech.iso8583.interchange.ISO8583Interchange#recover(com.usatech.iso8583.transaction.ISO8583Transaction)
	 */
	public boolean recover(ISO8583Transaction transaction) {
		String transactionType = transaction.getRequest().getTransactionType();
		PaymentTechAction action = actions.get(transactionType);
		if (action == null) {
			log.error("Recovery failed: Unsupported transaction type: " + transactionType);
			return true;
		}

		try {
			return action.recover(transaction);
		} catch (Throwable e) {
			log.error( "Caught unexpected exception while attempting recovery of " + transaction + ": " + e.getMessage(), e);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.picocontainer.Startable#start()
	 */
	public void start() {
		if (isStarted)
			return;

		log.info("PaymentTechInterchange Starting up...");
		loadConfig();

		executor = Executors.newSingleThreadScheduledExecutor();
		isStarted = true;
	}

	private synchronized void loadConfig() {
		log.info("Loading configuration...");
		
		int timeout = config.getInt("paymentech.response.timeout");
		if ( !(timeout%1000 == 0) ){
			log.warn("Configuration value paymentech.response.timeout should be in the form of 'x000' (x seconds). Default Response Timeout is set to " +defaultResponseTimeout + " seconds");
		}
		else{
			defaultResponseTimeout = timeout/1000;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatech.iso8583.interchange.Configurable#getConfiguration()
	 */
	public Configuration getConfiguration() {
		return config;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.usatech.iso8583.interchange.Configurable#setConfiguration(org.apache.commons.configuration.Configuration)
	 */
	public void setConfiguration(Configuration config) {
		if (isStarted)
			reloadConfig(config);
		else
			this.config = config;
	}

	private void reloadConfig(Configuration config2) {
		this.config = config;

		log.info("reloading Config ...");

		try
		{
			Thread.sleep(1000);
		}
		catch (Exception e)
		{
		}

		loadConfig();
	}
	
	public int getDefaultResponseTimeout(){
		return defaultResponseTimeout;
	}
}
