package com.usatech.iso8583.interchange.paymenttech;

import ibizptech.PTCardType;
import ibizptech.Ptcharge;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;

public class PaymentechUtil {
	
	public static String buildAmount(int requestAmount) 
	{
		BigDecimal amountPennies = new BigDecimal(requestAmount);
		BigDecimal amountDollar = amountPennies.divide(new BigDecimal("100"));
		return amountDollar.setScale(2, RoundingMode.UNNECESSARY).toString();
	}
	
	public static int returnAmount(String paymentechAmount) 
	{
		BigDecimal amountPennies = new BigDecimal(paymentechAmount).multiply(new BigDecimal(100));
		return amountPennies.intValue();
	}
	
	public static int buildPinEntry(String requestPinEntry) 
	{
		if(requestPinEntry.equals(ISO8583Message.PIN_ENTRY_CAPABILITY)){
			return 1;
		}else if(requestPinEntry.equals(ISO8583Message.PIN_ENTRY_NO_CAPABILITY)){
			return 2;
		}else{
			return 0;
		}
	}
	
	public static int getEntryDataSource(ISO8583Request request, int track) throws ValidationException {
		String entryMode = request.getEntryMode();

		try {

			if (entryMode
					.equals(ISO8583Message.ENTRY_MODE_MANUALLY_KEYED)) {
				if (Integer.parseInt(request.getPanData().getExpiration().trim().substring(2, 4)) != 0
						&& Integer.parseInt(request.getPanData().getExpiration().trim().substring(0, 2)) != 0 
					&& request.getPanData().getPan() != null &&  !request.getPanData().getPan().equals("")) {
					return PTCardType.dsManuallyEntered;
				}else{
					throw new ValidationException("Caught unexpected exception while reading CardNumber/CardExpMonth/CardExpYear.");
				}
			}

			if (entryMode.equals(ISO8583Message.ENTRY_MODE_SWIPED)) {
				if(track == 1){
					return PTCardType.dsTrack1;
				}
				if(track == 2){
					return PTCardType.dsTrack2;
				}
				
				if(track ==0){
					return PTCardType.dsManuallyEntered;
				}
				
			}
			
			if (entryMode.equals(ISO8583Message.ENTRY_MODE_CONTACTLESS_SWIPED)) {
				if(track == 1){
					return PTCardType.dsTrack1FromRFID;
				}
				if(track == 2){
					return PTCardType.dsTrack2FromRFID;
				}
				if(track ==0){
					return PTCardType.dsManuallyEntered;
				}
			}
			
			return PTCardType.dsSwipeOriginUnknown;
		} catch (Exception e) {
			throw new ValidationException("Caught unexpected exception while getting Entry Data Source from request.");
		}
	}
}
