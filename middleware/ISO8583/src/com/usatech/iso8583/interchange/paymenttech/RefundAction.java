package com.usatech.iso8583.interchange.paymenttech;

import ibizptech.IBizPtechException;
import ibizptech.Ptcharge;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.transaction.ISO8583Transaction;

public class RefundAction extends PaymentTechAction
{
	private static Log log = LogFactory.getLog(RefundAction.class);

	private static int[] requiredFields = { ISO8583Message.FIELD_ACQUIRER_ID, ISO8583Message.FIELD_TERMINAL_ID, ISO8583Message.FIELD_TRACE_NUMBER, ISO8583Message.FIELD_AMOUNT, ISO8583Message.FIELD_POS_ENVIRONMENT, ISO8583Message.FIELD_ENTRY_MODE, ISO8583Message.FIELD_ONLINE };
	
	public RefundAction(PaymentTechInterchange interchange) {
		super(interchange);
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException
	{
		super.validateRequest(request, requiredFields);

		if (!request.hasPanData() && !request.hasTrackData())
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) + " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));

	}
	
	public ISO8583Response process(ISO8583Transaction transaction)
	{
		log.debug("Refund Action is processing ... ");
		
		ISO8583Request request = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);

		Ptcharge ptcharge1 = null;
		
		log.debug("Transaction ID: " + transaction.getTransactionID());
		log.debug("Transaction Type: " + request.getTransactionType());
		
		try
		{
			validateRequest(request, requiredFields);		
			ptcharge1 = retailSet(request);
			transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
		
		}catch (ValidationException e){
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		} catch (IBizPtechException e) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error(transaction + "Caught unexpected exception while setting the IBiz API parameters: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Caught unexpected exception while setting the IBiz API parameters: " + e.getMessage());	
		} 
		catch (Throwable e) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Caught unexpected exception validating " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception validating request: " + e.getMessage());
		}

		try{
			log.debug("Authorizing charge...");
		
			ptcharge1.credit();
			
		} catch (IBizPtechException e) {
			log.error("Caught unexpected exception IBiz API processing " + transaction + ": " + e.getMessage(), e);
			if (e.getCode()==201 || e.getCode()==301){
				transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, "Timeout occured waiting for host response.");
			}
			log.error("Caught unexpected exception while IBiz API processing " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception IBiz API processing request: " + e.getMessage());
		} catch (Throwable e) {
			log.error("Caught unexpected exception transmitting " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting " + transaction + ": " + e.getMessage());
		}
		
		ISO8583Response isoResponse = null;
		
		transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);
		
		try {
			isoResponse = readResponse(ptcharge1, request);
		} catch (ValidationException e) {
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION,"Host response failed input validation: " + e.getMessage());
		} catch (Throwable e) {
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception while constructing response: " + transaction.getTransactionID() + ": " + e.getMessage() + ", response: " + isoResponse, e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
		}
		

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);

		return isoResponse;

	}
	
	public boolean recover(ISO8583Transaction transaction)
	{

		log.debug("Refund Action is recovering ... ");
	
		ISO8583Request request = transaction.getRequest();
		
		log.debug("Transaction ID: " + transaction.getTransactionID());
		log.debug("Transaction Type: " + request.getTransactionType());
		
		ISO8583Response response;
		try {	
			request.setHostResponseTimeout(DEFAUT__RESPONSE_TIMEOUT);
			
			Ptcharge ptcharge1 = retailSet(request);
			ptcharge1.credit();
			
			if ( ptcharge1.getResponse().getRetrievalNumber() == null || ptcharge1.getResponse().getRetrievalNumber().trim()=="" )
			{
				log.error("Attempted recovery of " + transaction + " failed. Did not get Retrieval Number.");
				return false;
			}
			// Hopefully we will get the same response as the previous operation.
			response = readResponse(ptcharge1, request);
			// There is an known issue that it seems IBiz API(or Paymentech Test Server close connection after every operation call.
			ptcharge1 = retailSet(request);
			if(ptcharge1.getCard().getNumber() == null || ptcharge1.getCard().getNumber().equals("")){
				ptcharge1.getCard().setNumber(request.getTrackData().extractPANData().getPan());
			}
			// TODO Note: From the IBiz JavaDoc -"if you use a new instance of the bean or otherwise reset it's state, you must set the  LastRetrievalNumber  parameter as well. This need to be tested and confirmed."
			ptcharge1.voidTransaction(response.getRetrievalReferenceNumber(), "");
			
			if ( !ptcharge1.getResponse().getCode().equals("E") && !ptcharge1.getResponse().getCode().equals("A") )
			{
				log.error("Attempted recovery of " + transaction + " failed. Did not get Response Code.");
				return false;
			}
			response = readResponse(ptcharge1, request);
		} catch (ValidationException e){
			log.error("Attempted recovery of " + transaction + " failed. ValidationException: " + e.getMessage());
			return true;
		} catch (IBizPtechException e){
			if (e.getCode()==201 || e.getCode()==301){
				log.error("Attempted recovery of " + transaction + " failed. IBizPtechException: " + e.getCode() + "," + e.getMessage(), e);
				return false;
			}
			log.error("Attempted recovery of " + transaction + " failed. IBizPtechException: " + e.getCode() + "," + e.getMessage(), e);
			return true;
			
		} catch (Throwable e) {
			log.error("Attempted recovery of " + transaction + " failed: " + e.getMessage(), e);
			return true;
		}
		log.info("Attempted recovery of " + transaction + " received reversal response: " + response);
		return true;		
		
	}
}
