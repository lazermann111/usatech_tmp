package com.usatech.iso8583.interchange.paymenttech;

public class ValidationException extends Exception
{
	private static final long serialVersionUID = 2129366691708178073L;

	public ValidationException()
	{
		super();
	}

	public ValidationException(String msg)
	{
		super(msg);
	}

	public ValidationException(String msg, Exception e)
	{
		super(msg, e);
	}
}
