package com.usatech.iso8583.interchange.paymenttech;

import ibizptech.IBizPtechException;
import ibizptech.PTCardType;
import ibizptech.PTChargeResponse;
import ibizptech.PTSettleResponse;
import ibizptech.Ptcharge;
import ibizptech.Ptsettle;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.util.CreditCardValidator;

/**
 * This is an abstract class that defines a PaymentTechAction. All actions to
 * PaymentTech should be sub-classed from this abstract class.
 *
 * @author jmcpherson
 *
 */
public abstract class PaymentTechAction {

	private static Log log = LogFactory.getLog(PaymentTechAction.class);

	private final PaymentTechInterchange interchange;
	// default time out in seconds
	public static int DEFAUT__RESPONSE_TIMEOUT = 15;

	protected PaymentTechAction(PaymentTechInterchange interchange)
	{
		this.interchange = interchange;
		Configuration config = interchange.getConfiguration();
		int timeout = config.getInt("paymentech.response.timeout");
		if ( !(timeout%1000 == 0) ){
			log.warn("Configuration value paymentech.response.timeout should be in the form of 'x000' (x seconds). Default Response Timeout is set to " +DEFAUT__RESPONSE_TIMEOUT + " seconds");
		}
		else{
			DEFAUT__RESPONSE_TIMEOUT = timeout/1000;
		}
	}

	public abstract ISO8583Response process(ISO8583Transaction transaction);

	public abstract boolean recover(ISO8583Transaction transaction);

	protected void validateRequest(ISO8583Request request, int[] requiredFields)
			throws ValidationException {
		for (int i = 0; i < requiredFields.length; i++) {
			if (!request.hasField(requiredFields[i])){
				throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(requiredFields[i]));
			}
		}

		if (request.hasTrackData()) {
			if (!request.getTrackData().hasTrack1() && !request.getTrackData().hasTrack2()){
				throw new ValidationException("Required Field Not Found: TrackData Tracks");
			}

			try {
				CreditCardValidator.validateTrackData(request.getTrackData());
			} catch (NumberFormatException e) {
				throw new ValidationException("Invalid Track Data: "+ e.getMessage());
			}
		}

		if (request.hasPanData()) {
			if (request.getPanData().getPan() == null){
				throw new ValidationException("Required Field Not Found: PanData PAN");
			}

			if (request.getPanData().getExpiration() == null){
				throw new ValidationException("Required Field Not Found: PanData Expiration");
			}

			try {
				CreditCardValidator.validatePanData(request.getPanData());
			} catch (NumberFormatException e) {
				throw new ValidationException("Invalid Pan Data: " + e.getMessage());
			}
		}

		if (request.hasAvsAddress() && !request.hasAvsZip()){
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_AVS_ZIP)
					+ " is required with " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_AVS_ADDRESS));}
	}

	protected Ptcharge setNetConnectParameter(ISO8583Request request) throws ValidationException, IBizPtechException {
		Ptcharge ptcharge = new Ptcharge();
		Configuration config = interchange.getConfiguration();
		ptcharge.setRuntimeLicense(config.getString("paymentech.netconnect.license"));

		if(interchange.isSimulationMode()){
			ptcharge.setMerchantNumber(config.getString("paymentech.netconnect.merchantnum"));
			ptcharge.setTerminalNumber(config.getString("paymentech.netconnect.terminalnum"));
		}else{
			if(request.hasAcquirerID()){
				ptcharge.setMerchantNumber(request.getAcquirerID());
			}else{
				throw new ValidationException("no AcquirerID Exception");
			}

			if(request.hasTerminalID()){
				ptcharge.setTerminalNumber(request.getTerminalID());
			}else{
				throw new ValidationException("no TerminalID Exception");
			}
		}

		ptcharge.setClientNumber(config.getString("paymentech.netconnect.clientnum"));

		ptcharge.setUserId(config.getString("paymentech.netconnect.userid"));

		ptcharge.setPassword(config.getString("paymentech.netconnect.password"));

		ptcharge.setServer(config.getString("paymentech.netconnect.server"));

		boolean keepAlive = config.getBoolean("paymentech.netconnect.keepAlive");
		if (!keepAlive){
			ptcharge.config("KeepAlive=False");
		}else{
			ptcharge.config("KeepAlive=True");
		}
		ptcharge.setDuplicateChecking(Ptcharge.duReturnOriginalResponse);

		if (request.hasHostResponseTimeout()&& request.getHostResponseTimeout()>0){
			ptcharge.setTimeout(request.getHostResponseTimeout());
			log.debug("Host Response Timeout: " + request.getHostResponseTimeout() + " seconds");
		}
		else{
			ptcharge.setTimeout(DEFAUT__RESPONSE_TIMEOUT);
			log.debug("Host Response Timeout: " + DEFAUT__RESPONSE_TIMEOUT + " seconds");
		}
		return ptcharge;
	}

	protected Ptsettle setSettleNetConnectParameter(ISO8583Request request) throws ValidationException, IBizPtechException {
		Ptsettle settle1 = new Ptsettle();

		Configuration config = interchange.getConfiguration();
		settle1.setRuntimeLicense(config.getString("paymentech.netconnect.license"));
		settle1.setSequenceNumber(request.getTraceNumber());

		if(interchange.isSimulationMode()){
			settle1.setMerchantNumber(config.getString("paymentech.netconnect.merchantnum"));
			settle1.setTerminalNumber(config.getString("paymentech.netconnect.terminalnum"));
		}else{
			if(request.hasAcquirerID()){
				settle1.setMerchantNumber(request.getAcquirerID());
			}else{
				throw new ValidationException("no AcquirerID Exception");
			}

			if(request.hasTerminalID()){
				settle1.setTerminalNumber(request.getTerminalID());
			}else{
				throw new ValidationException("no TerminalID Exception");
			}
		}

		settle1.setClientNumber(config.getString("paymentech.netconnect.clientnum"));

		settle1.setUserId(config.getString("paymentech.netconnect.userid"));

		settle1.setPassword(config.getString("paymentech.netconnect.password"));

		settle1.setServer(config.getString("paymentech.netconnect.server"));

		boolean keepAlive = config.getBoolean("paymentech.netconnect.keepAlive");
		if (!keepAlive){
			settle1.config("KeepAlive=False");
		}else{
			settle1.config("KeepAlive=True");
		}

		if (request.hasHostResponseTimeout()&& request.getHostResponseTimeout()>0){
			settle1.setTimeout(request.getHostResponseTimeout());
			log.debug("Host Response Timeout: " + request.getHostResponseTimeout() + "seconds");
		}
		else{
			settle1.setTimeout(DEFAUT__RESPONSE_TIMEOUT);
			log.debug("Host Response Timeout: " + DEFAUT__RESPONSE_TIMEOUT + " seconds");
		}

		return settle1;
	}

	protected ISO8583Response readResponse(Ptcharge ptcharge, ISO8583Request request) throws ValidationException{
		ISO8583Response response = new ISO8583Response();

		PTChargeResponse ibizResponse = ptcharge.getResponse();
		log.debug("***** Transaction results *****");

		if (ibizResponse.getAVSResult() != null && !ibizResponse.getAVSResult().trim().equals("")) {
			response.setAvsResult(ibizResponse.getAVSResult());
			log.debug("Response AVS Result: " + ibizResponse.getAVSResult());
		}

		log.debug("Response Code: " + ibizResponse.getCode());
		log.debug("Approval Code: " + ibizResponse.getApprovalCode());

		if (ibizResponse.getCode() == null || ibizResponse.getCode().trim().equals(""))
			throw new ValidationException("Response Did Not Contain a Required Field: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_RESPONSE_CODE));

		if(ibizResponse.getCode().trim().equals("A")){
			response.setResponseCode("00");
			if (ibizResponse.getApprovalCode()!=null && !ibizResponse.getApprovalCode().trim().equals(""))
				response.setApprovalCode(ibizResponse.getApprovalCode());
		}else {	// ResponseCode == "E"
			if (ibizResponse.getApprovalCode()!=null && !ibizResponse.getApprovalCode().trim().equals(""))
				response.setResponseCode(ibizResponse.getApprovalCode());
			else
				response.setResponseCode("GE04");
		}

		if (ibizResponse.getCVVResult() != null && !ibizResponse.getCVVResult().trim().equals("")) {
			response.setCvv2Result(ibizResponse.getCVVResult());
			log.debug("Response CVV2 Result: " + ibizResponse.getCVVResult());
		}

		// Retrieval Ref Number
		if (ibizResponse.getRetrievalNumber() != null && !ibizResponse.getRetrievalNumber().trim().equals("")) {
			response.setRetrievalReferenceNumber(ibizResponse.getRetrievalNumber());
			log.debug("Response Retrieval Reference Number: " + ibizResponse.getRetrievalNumber());
		}

		if (ibizResponse.getText() != null && !ibizResponse.getText().trim().equals("")) {
			response.setResponseMessage(ibizResponse.getText().trim());
			log.debug("Response Text: " + ibizResponse.getText().trim());
		}

		if(request.hasEffectiveDate()){
			response.setEffectiveDate(request.getEffectiveDate());
		}
		if(ibizResponse.getAVSResult() != null && !ibizResponse.getAVSResult().trim().equals("")) {
			response.setAvsResult(ibizResponse.getAVSResult());
			log.debug("Response AVS Result: " + ibizResponse.getAVSResult());
		}
		/*TODO Refer to ISO8583Response.java, the AvsZipMatch and AvsAddressMatch are one character, there should be some mapping.
		if (ptcharge.getCustomerZip() != null && !ptcharge.getCustomerZip().trim().equals("")) {
			response.setAvsZipMatch(ptcharge.getCustomerZip());
			log.debug("Response AVS Zip Match: " + ptcharge.getResponseCVVResult());
		}

		if (ptcharge.getCustomerAddress() != null && !ptcharge.getCustomerAddress().trim().equals("")) {
			response.setAvsAddressMatch(ptcharge.getCustomerAddress());
			log.debug("Response AVS Address Match: " + ptcharge.getResponseCVVResult());
		}
		*/

		if (request.getAmount()!=0){
			log.debug("Response Transaction Amount: " + ptcharge.getTransactionAmount());
			response.setAmount(PaymentechUtil.returnAmount(ptcharge.getTransactionAmount()));
		}

		if (ibizResponse.getSequenceNumber()!=null){
			response.setTraceNumber(Integer.valueOf(ibizResponse.getSequenceNumber()));
		}

		return response;
	}

	protected ISO8583Response readResponse(Ptsettle settle, ISO8583Request request) throws ValidationException{
		ISO8583Response response = new ISO8583Response();

		PTSettleResponse ibizResponse = settle.getResponse();
		log.debug("***** Transaction results *****");

		if (ibizResponse.getBatchClose() != null) {
			log.debug("Response Batch Close: " + ibizResponse.getBatchClose());
		}

		if (ibizResponse.getBatchNetAmount() != null) {
			response.setAmount(PaymentechUtil.returnAmount(ibizResponse.getBatchNetAmount()));
			log.debug("Response Batch Net Amount: " + ibizResponse.getBatchNetAmount());
		}

		if (ibizResponse.getBatchNumber() != null) {
			log.debug("Response Batch Number: " + ibizResponse.getBatchNumber());
		}

		if (ibizResponse.getBatchOpen() != null) {
			log.debug("Response Batch Open: " + ibizResponse.getBatchOpen());
		}

		log.debug("Response Batch TransCount: " + ibizResponse.getBatchTransCount());

		if (ibizResponse.getCode() != null) {
			if(ibizResponse.getText() != null){
				response.setResponseMessage(ibizResponse.getText() );
			}

			if(ibizResponse.getCode().trim().equals("A")){
				response.setResponseCode("00");
			}else{
				response.setResponseCode("GE04");
			}

			log.debug("Response Code : " + ibizResponse.getCode());
			log.debug("Response Text : " + ibizResponse.getText());
		}else{
			response.setResponseCode("GE04");
		}

		if (ibizResponse.getData() != null) {
			log.debug("Response Data: " + ibizResponse.getData());
		}

		log.debug("Response Inquiry Count: " + ibizResponse.getInquiryCount());

		if (ibizResponse.getSequenceNumber() != null) {
			response.setTraceNumber(Integer.valueOf(ibizResponse.getSequenceNumber()));
			log.debug("Response Sequence Number: " + ibizResponse.getSequenceNumber());
		}


		return response;
	}


	protected Ptcharge retailSet(ISO8583Request request) throws ValidationException, IBizPtechException
	{
		Ptcharge ptcharge1 = setNetConnectParameter(request);

		ptcharge1.setIndustryType(Ptcharge.itRetail);
		ptcharge1.setSequenceNumber(request.getTraceNumber());

		int track = 0;

		if (request.hasTrackData())
		{
			if (request.getTrackData().hasTrack2())
			{
				ptcharge1.setCard(new PTCardType(request.getTrackData().getTrack2(),PTCardType.dsTrack2));
				track = 2;
			}
			else if (request.getTrackData().hasTrack1())
			{
				ptcharge1.setCard(new PTCardType(request.getTrackData().getTrack1(),PTCardType.dsTrack1));
				track = 1;
			}
		}
		else
		{
			ptcharge1.setCard(new PTCardType());
			ptcharge1.getCard().setNumber(request.getPanData().getPan());
			ptcharge1.getCard().setExpMonth(Integer.parseInt(request.getPanData().getExpiration().trim().substring(2, 4)));
			ptcharge1.getCard().setExpYear(Integer.parseInt(request.getPanData().getExpiration().trim().substring(0, 2)));
			if (request.hasCvv2()) {
				ptcharge1.getCard().setCVVPresence(1);
				ptcharge1.getCard().setCVVData(request.getCvv2());
			}
			if (request.hasAvsZip())
				ptcharge1.setCustomerZip(request.getAvsZip());
			if (request.hasAvsAddress())
				ptcharge1.setCustomerAddress(request.getAvsAddress());
		}
		ptcharge1.setTransactionAmount(PaymentechUtil.buildAmount(request.getAmount()));
		log.debug("Transaction Amount: " + ptcharge1.getTransactionAmount());

		if(request.hasAvsAddress()){
			ptcharge1.setCustomerAddress(request.getAvsAddress());
		}

		if(request.hasAvsZip()){
			ptcharge1.setCustomerZip(request.getAvsZip());
		}

		if (request.hasCvv2()){
			ptcharge1.getCard().setCVVData(request.getCvv2());
		}

		ptcharge1.getCard().setEntryDataSource(PaymentechUtil.getEntryDataSource(
				request, track));

		return ptcharge1;
	}
}
