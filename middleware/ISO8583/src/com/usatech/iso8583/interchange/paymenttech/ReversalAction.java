package com.usatech.iso8583.interchange.paymenttech;

import ibizptech.IBizPtechException;
import ibizptech.Ptcharge;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.transaction.ISO8583Transaction;

public class ReversalAction extends PaymentTechAction
{
	private static Log log = LogFactory.getLog(ReversalAction.class);

	private static int[] requiredFields = { ISO8583Message.FIELD_TRANSACTION_SUB_TYPE, ISO8583Message.FIELD_ACQUIRER_ID, ISO8583Message.FIELD_TERMINAL_ID, ISO8583Message.FIELD_TRACE_NUMBER, ISO8583Message.FIELD_AMOUNT, ISO8583Message.FIELD_ENTRY_MODE, ISO8583Message.FIELD_RETRIEVAL_REFERENCE_NUMBER};

	public ReversalAction(PaymentTechInterchange interchange) {
		super(interchange);
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException
	{
		super.validateRequest(request, requiredFields);
		
		if (!request.hasPanData() && !request.hasTrackData())
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) + " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));

		String subType = request.getTransactionSubType();
		// Note: Authorization Action can not reversal, because the authOnly() operation doesn't add it to the current batch.
		if ( !(subType.equals(ISO8583Request.TRANSACTION_TYPE_SALE) || subType.equals(ISO8583Request.TRANSACTION_TYPE_REFUND)) ) 
			throw new ValidationException("Invalid Reversal Request Subtype: " + subType);
	}
	
	public ISO8583Response process(ISO8583Transaction transaction) {
		log.debug("Reversal Action is processing ... ");
		
		ISO8583Request request = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);

		Ptcharge ptcharge1 = null;
		
		log.debug("Transaction ID: " + transaction.getTransactionID());
		log.debug("Transaction Type: " + request.getTransactionType());
		
		try {
			validateRequest(request, requiredFields);
			
			ptcharge1 = retailSet(request);
			if (ptcharge1.getCard().getNumber()==null || ptcharge1.getCard().getNumber().equals("")){
				ptcharge1.getCard().setNumber(request.getTrackData().extractPANData().getPan());
			}
			
			transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
			
		} catch (ValidationException e) {
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		} catch (IBizPtechException e) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error(transaction + "Caught unexpected exception while setting the IBiz API parameters: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Caught unexpected exception while setting the IBiz API parameters: " + e.getMessage());	
		} catch (Throwable e) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Caught unexpected exception validating " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception validating request: " + e.getMessage());
		}

		try{
			log.debug("Authorizing charge...");
			//TODO From the IBiz JavaDoc -"if you use a new instance of the bean or otherwise reset it's state, you must set the  LastRetrievalNumber  parameter as well."
			ptcharge1.voidTransaction(request.getRetrievalReferenceNumber(), "");
			
		}catch (IBizPtechException e) {
			log.error("Caught unexpected exception netconnect processing " + transaction + ": " + e.getMessage(), e);
			if (e.getCode()==201 || e.getCode()==301 || e.getCode()==700){
				transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, "Timeout occured waiting for host response.");
			}
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception netconnect processing request: " + e.getMessage());
		} catch (Throwable e) {
			log.error("Caught unexpected exception transmitting " + transaction + ": " + e.getMessage(), e);
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting " + transaction + ": " + e.getMessage());
		}
		
		transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);

		ISO8583Response isoResponse = null;

		try
		{
			isoResponse = readResponse(ptcharge1, request);
		} catch (ValidationException e) {
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage() + ", response: " + isoResponse);
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION,"Host response failed input validation: " + e.getMessage());
		} catch (Throwable e) {
			log.error("Caught unexpected exception while constructing response: " + transaction.getTransactionID() + ": " + e.getMessage() + ", response: " + isoResponse, e);
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
		}
		
		transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);

		return isoResponse;
	}
	
	public boolean recover(ISO8583Transaction transaction)
	{
		log.info("Skipping recovery of " + transaction + ": Not an online transaction");
		return true;
	}
}
