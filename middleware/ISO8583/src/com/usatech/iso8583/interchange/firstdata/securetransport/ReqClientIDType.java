/**
 * ReqClientIDType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class ReqClientIDType  implements java.io.Serializable {
    private java.lang.String DID;

    private java.lang.String app;

    private java.lang.String auth;

    private java.lang.String clientRef;

    public ReqClientIDType() {
    }

    public ReqClientIDType(
           java.lang.String DID,
           java.lang.String app,
           java.lang.String auth,
           java.lang.String clientRef) {
           this.DID = DID;
           this.app = app;
           this.auth = auth;
           this.clientRef = clientRef;
    }


    /**
     * Gets the DID value for this ReqClientIDType.
     * 
     * @return DID
     */
    public java.lang.String getDID() {
        return DID;
    }


    /**
     * Sets the DID value for this ReqClientIDType.
     * 
     * @param DID
     */
    public void setDID(java.lang.String DID) {
        this.DID = DID;
    }


    /**
     * Gets the app value for this ReqClientIDType.
     * 
     * @return app
     */
    public java.lang.String getApp() {
        return app;
    }


    /**
     * Sets the app value for this ReqClientIDType.
     * 
     * @param app
     */
    public void setApp(java.lang.String app) {
        this.app = app;
    }


    /**
     * Gets the auth value for this ReqClientIDType.
     * 
     * @return auth
     */
    public java.lang.String getAuth() {
        return auth;
    }


    /**
     * Sets the auth value for this ReqClientIDType.
     * 
     * @param auth
     */
    public void setAuth(java.lang.String auth) {
        this.auth = auth;
    }


    /**
     * Gets the clientRef value for this ReqClientIDType.
     * 
     * @return clientRef
     */
    public java.lang.String getClientRef() {
        return clientRef;
    }


    /**
     * Sets the clientRef value for this ReqClientIDType.
     * 
     * @param clientRef
     */
    public void setClientRef(java.lang.String clientRef) {
        this.clientRef = clientRef;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReqClientIDType)) return false;
        ReqClientIDType other = (ReqClientIDType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DID==null && other.getDID()==null) || 
             (this.DID!=null &&
              this.DID.equals(other.getDID()))) &&
            ((this.app==null && other.getApp()==null) || 
             (this.app!=null &&
              this.app.equals(other.getApp()))) &&
            ((this.auth==null && other.getAuth()==null) || 
             (this.auth!=null &&
              this.auth.equals(other.getAuth()))) &&
            ((this.clientRef==null && other.getClientRef()==null) || 
             (this.clientRef!=null &&
              this.clientRef.equals(other.getClientRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDID() != null) {
            _hashCode += getDID().hashCode();
        }
        if (getApp() != null) {
            _hashCode += getApp().hashCode();
        }
        if (getAuth() != null) {
            _hashCode += getAuth().hashCode();
        }
        if (getClientRef() != null) {
            _hashCode += getClientRef().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReqClientIDType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ReqClientIDType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("app");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "App"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("auth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Auth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ClientRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
