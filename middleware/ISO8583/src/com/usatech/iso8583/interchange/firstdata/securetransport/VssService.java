/**
 * VssService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public interface VssService extends javax.xml.rpc.Service {
    public java.lang.String getvssServicePortAddress();

    public com.usatech.iso8583.interchange.firstdata.securetransport.VssPortType getvssServicePort() throws javax.xml.rpc.ServiceException;

    public com.usatech.iso8583.interchange.firstdata.securetransport.VssPortType getvssServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
