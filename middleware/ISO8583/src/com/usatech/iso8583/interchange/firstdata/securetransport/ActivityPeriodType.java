/**
 * ActivityPeriodType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class ActivityPeriodType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ActivityPeriodType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "current-period";
    public static final java.lang.String _value2 = "force-cutoff";
    public static final java.lang.String _value3 = "last-period";
    public static final java.lang.String _value4 = "second-last-period";
    public static final java.lang.String _value5 = "third-last-period";
    public static final java.lang.String _value6 = "current-shift-reset";
    public static final ActivityPeriodType value1 = new ActivityPeriodType(_value1);
    public static final ActivityPeriodType value2 = new ActivityPeriodType(_value2);
    public static final ActivityPeriodType value3 = new ActivityPeriodType(_value3);
    public static final ActivityPeriodType value4 = new ActivityPeriodType(_value4);
    public static final ActivityPeriodType value5 = new ActivityPeriodType(_value5);
    public static final ActivityPeriodType value6 = new ActivityPeriodType(_value6);
    public java.lang.String getValue() { return _value_;}
    public static ActivityPeriodType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ActivityPeriodType enumeration = (ActivityPeriodType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ActivityPeriodType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ActivityPeriodType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ActivityPeriodType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
