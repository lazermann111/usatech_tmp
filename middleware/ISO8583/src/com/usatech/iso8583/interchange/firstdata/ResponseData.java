package com.usatech.iso8583.interchange.firstdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ResponseData
{
	private boolean success = false;
	private String statusCode = "";
	private String returnCode = "";
	private String responseCode = null;
	private Integer amount = null;
	private String approvalCode = null;
	private Integer traceNumber = null;
	private String responseMessage = null;
	private Date effectiveDate = null;
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public void setAmount(BigDecimal amount) {
		if (amount != null)
			this.amount = Integer.valueOf(amount.movePointRight(2).intValue());
	}	
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public Integer getTraceNumber() {
		return traceNumber;
	}
	public void setTraceNumber(int traceNumber) {
		this.traceNumber = traceNumber;
	}
	public void setTraceNumber(BigInteger traceNumber) {
		if (traceNumber != null)
			this.traceNumber = traceNumber.intValue();
	}	
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) throws ParseException
	{
		if (effectiveDate != null)
		{
			SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");
			Date transmissionDate = sdf.parse(effectiveDate);
			Calendar now = Calendar.getInstance();
			int yearNow = now.get(Calendar.YEAR);
			Calendar tranCal = Calendar.getInstance();
			tranCal.setTime(transmissionDate);
			tranCal.set(Calendar.YEAR, yearNow);
			
			// if date is more than 180 days in the future, use last year
			if (tranCal.getTimeInMillis() - now.getTimeInMillis() > 180 * 24 * 60 * 60 * 1000)
				tranCal.set(Calendar.YEAR, yearNow - 1);
			
			this.effectiveDate = tranCal.getTime();
		}
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
}
