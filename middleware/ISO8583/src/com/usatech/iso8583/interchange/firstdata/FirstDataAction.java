package com.usatech.iso8583.interchange.firstdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType;
import com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType;
import com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType;
import com.usatech.iso8583.interchange.firstdata.securetransport.ResponseType;
import com.usatech.iso8583.interchange.firstdata.securetransport.TransactionType;
import com.usatech.iso8583.transaction.ISO8583Transaction;

public abstract class FirstDataAction {
	private FirstDataInterchange interchange;
	
	protected FirstDataAction(FirstDataInterchange interchange)
	{
		this.interchange = interchange;
	}
	
	abstract protected int[] getRequiredFields();
	abstract protected void initTransaction(ISO8583Request isoRequest, TransactionType transaction, String TPPID);
	abstract protected void initResponse(ResponseData responseData, ResponseType response) throws ParseException;
	abstract protected String getOriginalTransactionName();

	public FirstDataInterchange getInterchange() {
		return interchange;
	}
	
	public String getInvoiceNumber(final ISO8583Request isoRequest)
	{
		return Integer.toString(isoRequest.getTraceNumber());
	}
	
	public POSConditionCodeType getPOSConditionCodeType(final ISO8583Request isoRequest)
	{
		return isoRequest.getEntryMode().equals(ISO8583Message.ENTRY_MODE_MANUALLY_KEYED)
			? POSConditionCodeType.fromString("customer-card-not-present")
			: POSConditionCodeType.fromString("customer-card-present");
	}
	
	public POSSecurityConditionType getPOSSecurityConditionType()
	{
		return POSSecurityConditionType.fromString("no-concern");
	}
	
	public POSTerminalTypeType getPOSTerminalTypeType()
	{
		return POSTerminalTypeType.fromString("on-premise-point-of-service");
	}
	
	public BigInteger getSystemTraceNumber(final ISO8583Request isoRequest)
	{
		return BigInteger.valueOf(isoRequest.getTraceNumber());
	}
	
	public BigDecimal getTransactionAmount(final ISO8583Request isoRequest)
	{
		return BigDecimal.valueOf(isoRequest.getAmount()).movePointLeft(2);
	}
	
	public String getTransactionDate(final Date effectiveDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("MMdd");
		return sdf.format(effectiveDate);
	}
	
	public String getTransactionTime(final Date effectiveDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
		return sdf.format(effectiveDate);
	}
	
	public String getTransmissionDateTime(final Date effectiveDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");
		return sdf.format(effectiveDate);
	}
	
	public BigInteger getAccountNumber(final ISO8583Request isoRequest)
	{
		return new BigInteger(isoRequest.getPanData().getPan());
	}
	
	public String getExpirationDate(final ISO8583Request isoRequest)
	{
		return isoRequest.getPanData().getExpiration().trim();
	}
	
	public void validateRequest(ISO8583Request request) throws ValidationException
	{
		int[] requiredFields = getRequiredFields();
		if (requiredFields != null)
		{
			for (int i = 0; i < requiredFields.length; i++) {
				if (!request.hasField(requiredFields[i])){
					throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(requiredFields[i]));
				}
			}
		}

		if (request.hasTrackData()) {
			if (!request.getTrackData().hasTrack1() && !request.getTrackData().hasTrack2()){
				throw new ValidationException("Required Field Not Found: TrackData Tracks");
			}
		}

		if (request.hasPanData()) {
			if (request.getPanData().getPan() == null){
				throw new ValidationException("Required Field Not Found: PanData PAN");
			}
		}
		
		validateRequestSpecificFields(request);
	}
	
	protected void validateRequestCardData(ISO8583Request request) throws ValidationException
	{
		if (!request.hasPanData() && !request.hasTrackData())
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) + " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));
	}
	
	protected void validateRequestSpecificFields(ISO8583Request request) throws ValidationException
	{
		
	}
	
	public ISO8583Response process(ISO8583Transaction transaction)
	{
		return null;
	}
	
	public boolean requiresRecovery()
	{
		return true;
	}
}
