package com.usatech.iso8583.interchange.firstdata;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

import javax.xml.rpc.ServiceException;

//import org.apache.axis.AxisProperties;
import org.apache.axis.client.Stub;
//import org.apache.axis.components.net.DefaultCommonsHTTPClientProperties;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.ConnectionPoolTimeoutException;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.logging.LogFactory;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.interchange.firstdata.securetransport.ErrorType;
import com.usatech.iso8583.interchange.firstdata.securetransport.ReqClientIDType;
import com.usatech.iso8583.interchange.firstdata.securetransport.RequestType;
import com.usatech.iso8583.interchange.firstdata.securetransport.ResponseType;
import com.usatech.iso8583.interchange.firstdata.securetransport.TransactionType;
import com.usatech.iso8583.interchange.firstdata.securetransport.VssPortType;
import com.usatech.iso8583.interchange.firstdata.securetransport.VssServiceLocator;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;

/**
 * This class is an implementation of the FirstDataInterchange. It is responsible
 * for routing the request, ISO8583Transaction, to the proper FirstDataAction.
 */
public class FirstDataInterchange implements ISO8583Interchange
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(FirstDataInterchange.class);

	public static final String INTERCHANGE_NAME = "FIRSTDATA";
	
	public static final String PROPERTY_REQUEST_URL = "firstdata.request.url";
	public static final String PROPERTY_REQUEST_DID = "firstdata.request.DID";
	public static final String PROPERTY_REQUEST_APP = "firstdata.request.app";
	public static final String PROPERTY_REQUEST_APP_VERSION = "firstdata.request.appVersion";
	public static final String PROPERTY_REQUEST_SERVICE_ID = "firstdata.request.serviceId";
	public static final String PROPERTY_REQUEST_TPPID = "firstdata.request.TPPID";
	public static final String PROPERTY_RECOVERY_HOURS = "firstdata.recovery.hours";
	
	private boolean isStarted = false;

	private ScheduledExecutorService executor;

	private Configuration config;

	private final TimeoutReversalAction torAction;
	private final AuthorizationAction authorizationAction;
	private final CaptureAction captureAction;
	private final RefundAction refundAction;
	private final ChargeAction chargeAction;
	private final SettlementAction settlementAction;
	
	private VssServiceLocator vssLocator = null;
	
	private String vssUrl;
	private String vssDid;
	private String vssApp;
	private String vssAppVersion;
	private String vssServiceId;
	private String vssTppId;
	private int recoveryHours;
	
	private static SortedSet<String> vxnConnectionErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = -8044441696861035529L;
			{
				add("200");//Host Busy
				add("201");//Host Unavailable
				add("202");//Host Connect Error
			}});

	private static SortedSet<String> vxnReadTimeoutErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = -3957325618427906325L;
			{
				add("203");//Host Drop
				add("205");//No Response
				add("405");//Secure Transport Network Timeout
				add("505");//Network Error
			}});
	
	private static SortedSet<String> statusTimeoutErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = -2595952042332944439L;

			{
				add("Timeout");
				add("OtherError");
				add("InternalError");
			}});
	
	private static SortedSet<String> responseTimeoutErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = 4373530379880711927L;

			{
				add("19"); //Try again / Miscellaneous data error
				add("90"); //System Error
				add("91"); //Issuer or switch is inoperative / Time-out
				add("96"); //System malfunction / Miscellaneous processing error
			}});	

	public FirstDataInterchange(ThreadPoolExecutor threadPool, ISO8583TransactionManager txnManager, Configuration config) throws ConfigurationException 
	{
		this.config = config;
		torAction = new TimeoutReversalAction(this);
		authorizationAction = new AuthorizationAction(this);
		captureAction = new CaptureAction(this);
		refundAction = new RefundAction(this);
		chargeAction = new ChargeAction(this);
		settlementAction = new SettlementAction(this);
		
		if (config instanceof FileConfiguration)
		{
			((FileConfiguration) config).setReloadingStrategy(new ConfigReloader());
		}		
	}
	
	private FirstDataAction firstDataActionFactory(ISO8583Transaction transaction)
	{
		String transactionType = transaction.getRequest().getTransactionType();
		if(ISO8583Message.TRANSACTION_TYPE_AUTHORIZATION.equals(transactionType)){
			return authorizationAction;
		}else if(ISO8583Message.TRANSACTION_TYPE_SALE.equals(transactionType)){
			if(transaction.getRequest().hasApprovalCode()){
				return captureAction;
			}else{
				return chargeAction;
			}
		}else if(ISO8583Message.TRANSACTION_TYPE_REFUND.equals(transactionType)){
			return refundAction;
		}else if(ISO8583Message.TRANSACTION_TYPE_SETTLEMENT.equals(transactionType)){
			return settlementAction;
		}
		return null;
	}
	
	public ResponseType sendRequest(RequestType request) throws ServiceException, RemoteException, ErrorType 
	{
		VssPortType	vssPort = vssLocator.getvssServicePort();
		Stub vssStub = (Stub) vssPort;
		//set timeout to 0 for configuration properties to take effect
		vssStub.setTimeout(0);
		return vssPort.vxnTransaction(request);
	}
	
	public String getName() {
		return INTERCHANGE_NAME;
	}

	public boolean isConnected() {
		return true;
	}

	public boolean isSimulationMode() {
		return false;
	}
	
	public synchronized void stop() {
		if (!isStarted)
			return;

		log.info("First Data Interchange Shutting down...");
		
		executor.shutdownNow();

		isStarted = false;
	}
	
	public boolean isStarted() {
		return isStarted;
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		checkConfigChanges();
		
		String transactionType = transaction.getRequest().getTransactionType();
		FirstDataAction action = firstDataActionFactory(transaction);

		if (action == null) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Processing failed: Unsupported transaction type: " + transactionType);
			return new ISO8583Response( ISO8583Response.ERROR_UNSUPPORTED_TRANSACTION_TYPE, "Unsupported transaction type: " + transactionType);
		}
				
		ISO8583Request isoRequest = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);

		RequestType request;
		try {
			action.validateRequest(isoRequest);
			request = createRequest(isoRequest, action);
			transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
		} catch (ValidationException e) {
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		} catch (Throwable e) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Error creating request for " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Error creating request: " + e.getMessage());
		}
		
		transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
		
		ISO8583Response isoResponse = action.process(transaction);
		if (isoResponse != null)
			return isoResponse;
		
		ResponseType response;
		try {
			response = sendRequest(request);
			transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);
		} catch (ErrorType e) {
			String vxnError = "VxnError transmitting " + transaction + ", ReturnCode: " + e.getReturnCode() + ", StatusCode: " + e.getStatus().getStatusCode();
			log.error(vxnError);
			if (vxnConnectionErrorCodes.contains(e.getReturnCode()))
				return new ISO8583Response(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, vxnError);
			else {
				transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				if (vxnReadTimeoutErrorCodes.contains(e.getReturnCode())
						|| statusTimeoutErrorCodes.contains(e.getStatus().getStatusCode()))
					return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, vxnError);
				else	
					return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, vxnError);
			}
		} catch (Throwable e) {
			String connectionError = checkConnectionFailure(e);
			if (connectionError != null) {
				log.error(connectionError + " transmitting " + transaction);
				return new ISO8583Response(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, connectionError);
			}
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			String readTimeoutError = checkReadTimeout(e);
			if (readTimeoutError != null) {
				log.error(readTimeoutError + " transmitting " + transaction);
				return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, readTimeoutError);
			}
			log.error("Error transmitting " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting request: " + e.getMessage());
		}
		
		transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);

		isoResponse = new ISO8583Response();
		try {
			ResponseData responseData = new ResponseData();			
			parseResponse(response, responseData, isoResponse, action);
			if(responseData.isSuccess())
			{
				transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);
				return isoResponse;
			}
			else
			{
				String vxnError = "VxnError transmitting " + transaction + ", ReturnCode: " + responseData.getReturnCode() + ", StatusCode: " + responseData.getStatusCode()
					+ ", ResponseCode: " + responseData.getResponseCode() + ", ResponseMessage: " + responseData.getResponseMessage();
				log.error(vxnError);
				if (vxnConnectionErrorCodes.contains(responseData.getReturnCode()))
					return new ISO8583Response(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, vxnError);
				else {	
					transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
					if (vxnReadTimeoutErrorCodes.contains(responseData.getReturnCode())
							|| statusTimeoutErrorCodes.contains(responseData.getStatusCode())
							|| responseTimeoutErrorCodes.contains(responseData.getResponseCode()))
						return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, vxnError);
					else					
						return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, vxnError);
				}
			}
		} catch (Throwable e) {
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Error constructing response to " + transaction + ": " + e.getMessage() + ", response: " + isoResponse, e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
		}
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		checkConfigChanges();
		
		String transactionType = transaction.getRequest().getTransactionType();
		FirstDataAction action = firstDataActionFactory(transaction);
		if (action == null) {
			log.error("Recovery failed: Unsupported transaction type: " + transactionType);
			return true;
		}
		
		if (action.requiresRecovery() == false)
		{
			log.info("Recovery of " + transaction + " is not required");
			return true;
		}

		ISO8583Request isoRequest = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);
		
		if (Calendar.getInstance().getTimeInMillis() - isoRequest.getGatewayTimestamp().getTime() > recoveryHours * 60 * 60 * 1000)
		{
			log.warn("Attempted recovery of " + transaction + ": Gateway Timestamp is older than " + recoveryHours + " hours, recovery expired and will not be retried");
			return true;
		}
			
		RequestType request;
		try {
			torAction.validateRequest(isoRequest);
			request = createRequest(isoRequest, torAction);
		} catch (ValidationException e) {
			log.error("Attempted recovery of " + transaction + " failed input validation: " + e.getMessage(), e);
			return true;
		} catch (Throwable e) {
			log.error("Attempted recovery of " + transaction + " failed constructing request: " + e.getMessage(), e);
			return true;
		}
		
		request.getTransaction().getGiftCardTimeoutReversal().setOriginalTransactionName(action.getOriginalTransactionName());		
		
		ResponseType response;
		try {
			response = sendRequest(request);
		} catch (Throwable e) {
			String error = "Attempted recovery of " + transaction + " failed: ";
			String connectionError = checkConnectionFailure(e);
			if (connectionError != null)
				log.error(error + connectionError);
			else {
				String readTimeoutError = checkReadTimeout(e);
				if (readTimeoutError != null)
					log.error(error + readTimeoutError);
				else
					log.error(error + e.getMessage(), e);
			}
			return false;
		}

		ISO8583Response isoResponse = new ISO8583Response();
		try {
			ResponseData responseData = new ResponseData();
			parseResponse(response, responseData, isoResponse, torAction);
			if(responseData.isSuccess())
			{
				log.info("Attempted recovery of " + transaction + " succeeded, received reversal response: " + isoResponse);
				return true;
			}
			else
			{
				log.info("Attempted recovery of " + transaction + " failed, ReturnCode: " + responseData.getReturnCode() + ", StatusCode: " + responseData.getStatusCode());
				return false;
			}
		} catch (Throwable e) {
			log.info("Attempted recovery of " + transaction + " failed, error constructing response: " + isoResponse, e);
			return false;
		}
	}

	public void start() {
		if (isStarted)
			return;

		log.info("FirstDataInterchange Starting up...");
		loadConfig();

		executor = Executors.newSingleThreadScheduledExecutor();
		isStarted = true;
	}	
	
	private synchronized void loadConfig() {
		log.info("Loading configuration...");
		
		vssUrl = config.getString(PROPERTY_REQUEST_URL);
		if (vssLocator == null)
			vssLocator = new VssServiceLocator();
		vssLocator.setvssServicePortEndpointAddress(vssUrl);
		
		vssDid = config.getString(PROPERTY_REQUEST_DID);
		vssApp = config.getString(PROPERTY_REQUEST_APP);
		vssAppVersion = config.getString(PROPERTY_REQUEST_APP_VERSION);
		vssServiceId = config.getString(PROPERTY_REQUEST_SERVICE_ID);
		vssTppId = config.getString(PROPERTY_REQUEST_TPPID);
		recoveryHours = config.getInt(PROPERTY_RECOVERY_HOURS);
		
		/* this has been moved to OutsideAuthorityLayerService.properties
		AxisProperties.setProperty(DefaultCommonsHTTPClientProperties.MAXIMUM_TOTAL_CONNECTIONS_PROPERTY_KEY,
				config.getString(DefaultCommonsHTTPClientProperties.MAXIMUM_TOTAL_CONNECTIONS_PROPERTY_KEY));
		AxisProperties.setProperty(DefaultCommonsHTTPClientProperties.MAXIMUM_CONNECTIONS_PER_HOST_PROPERTY_KEY,
				config.getString(DefaultCommonsHTTPClientProperties.MAXIMUM_CONNECTIONS_PER_HOST_PROPERTY_KEY));
		AxisProperties.setProperty(DefaultCommonsHTTPClientProperties.CONNECTION_POOL_TIMEOUT_KEY,
				config.getString(DefaultCommonsHTTPClientProperties.CONNECTION_POOL_TIMEOUT_KEY));
		AxisProperties.setProperty(DefaultCommonsHTTPClientProperties.CONNECTION_DEFAULT_CONNECTION_TIMEOUT_KEY,
				config.getString(DefaultCommonsHTTPClientProperties.CONNECTION_DEFAULT_CONNECTION_TIMEOUT_KEY));
		AxisProperties.setProperty(DefaultCommonsHTTPClientProperties.CONNECTION_DEFAULT_SO_TIMEOUT_KEY,
				config.getString(DefaultCommonsHTTPClientProperties.CONNECTION_DEFAULT_SO_TIMEOUT_KEY));*/
	}

	public synchronized void setConfiguration(Configuration config)
	{
		if (isStarted)
			loadConfig();
		else
			this.config = config;
	}	
	
	public Configuration getConfiguration() {
		return config;
	}
	
	private RequestType createRequest(ISO8583Request isoRequest, FirstDataAction action)
	{
		RequestType request = new RequestType();		
		ReqClientIDType reqClientID = new ReqClientIDType();
		reqClientID.setDID(vssDid);
		reqClientID.setApp(vssApp);
		reqClientID.setAuth(isoRequest.getAcquirerID() + "|" + isoRequest.getTerminalID());
		DecimalFormat decimalFormat = new DecimalFormat("0000000");// 7 0s
		reqClientID.setClientRef(decimalFormat.format(isoRequest.getTraceNumber()) + "v" + vssAppVersion);
		request.setReqClientID(reqClientID);
		
		TransactionType transaction = new TransactionType();
		transaction.setServiceID(vssServiceId);
		action.initTransaction(isoRequest, transaction, vssTppId);
		request.setTransaction(transaction);
		return request;
	}
	
	private void parseResponse(ResponseType response, ResponseData responseData, ISO8583Response isoResponse, FirstDataAction action) throws ParseException
	{
		if (response.getStatus() != null && response.getStatus().getStatusCode() != null)
			responseData.setStatusCode(response.getStatus().getStatusCode());
		if (response.getTransactionResponse() != null && response.getTransactionResponse().getReturnCode() != null)
			responseData.setReturnCode(response.getTransactionResponse().getReturnCode());

		if(responseData.getStatusCode() != null && "OK".equals(responseData.getStatusCode())
				&& responseData.getReturnCode() != null && "000".equals(responseData.getReturnCode()))
		{
			action.initResponse(responseData, response);
			
			if (responseData.getResponseCode() != null && "00".equals(responseData.getResponseCode())
					|| action instanceof TimeoutReversalAction && !responseTimeoutErrorCodes.contains(responseData.getResponseCode()))
				responseData.setSuccess(true);
					
			if (responseData.getResponseCode() != null)
				isoResponse.setResponseCode(responseData.getResponseCode());
			if (responseData.getResponseMessage() != null)
				isoResponse.setResponseMessage(responseData.getResponseMessage());
			if (responseData.getTraceNumber() != null)
				isoResponse.setTraceNumber(responseData.getTraceNumber().intValue());
			if (responseData.getAmount() != null)	
				isoResponse.setAmount(responseData.getAmount().intValue());
			if (responseData.getEffectiveDate() != null)
				isoResponse.setEffectiveDate(responseData.getEffectiveDate());
			if (responseData.getApprovalCode() != null)
				isoResponse.setApprovalCode(responseData.getApprovalCode());
		}
	}
	
	private String checkConnectionFailure(Throwable e)
	{
		Throwable cause = e;
		while (cause.getCause() != null)
		{
			cause = cause.getCause();
		}
		
		if (cause != null)
		{
			String causeMessage = cause.getMessage().toLowerCase();
			if (cause.getClass() == ConnectException.class
					|| cause.getClass() == ConnectionPoolTimeoutException.class
					|| cause.getClass() == ConnectTimeoutException.class
					|| cause.getClass() == MalformedURLException.class
					|| cause.getClass() == NoRouteToHostException.class
					|| (cause.getClass() == SocketException.class && causeMessage.indexOf("network is unreachable") > -1)
					|| (cause.getClass() == SocketTimeoutException.class && causeMessage.indexOf("connect timed out") > -1)
					|| cause.getClass() == UnknownHostException.class
					|| cause.getClass() == URIException.class
					|| cause.getClass() == URISyntaxException.class)
				return "Error connecting to host, " + cause.getClass().getName() + ": " + cause.getMessage();
		}
		return null;
	}
	
	private String checkReadTimeout(Throwable e)
	{
		Throwable cause = e;
		while (cause.getCause() != null)
		{
			cause = cause.getCause();
		}
		
		if (cause != null)
		{
			String causeMessage = cause.getMessage().toLowerCase();
			if (cause.getClass() == SocketTimeoutException.class && causeMessage.indexOf("read timed out") > -1)
				return "Timeout while waiting for host response, " + cause.getClass().getName() + ": " + cause.getMessage();
		}
		return null;
	}	
	
	private void checkConfigChanges()
	{
		//check configuration file for changes
		config.containsKey("ping");
	}
	
	private class ConfigReloader extends FileChangedReloadingStrategy
	{
		private ConfigReloader()
		{
			super();
		}

		@Override
		public void reloadingPerformed()
		{
			super.reloadingPerformed();

			log.warn("Configuration change detected");
			loadConfig();
		}
	}	
}
