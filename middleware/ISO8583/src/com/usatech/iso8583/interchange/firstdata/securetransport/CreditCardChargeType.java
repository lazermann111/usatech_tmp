/**
 * CreditCardChargeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class CreditCardChargeType  implements java.io.Serializable {
    private java.math.BigInteger accountNumber;

    private java.lang.String addressLine1;

    private java.lang.String cardVerificationValue;

    private java.lang.String commercialCardType;

    private java.math.BigInteger dutyAmount;

    private java.lang.String expirationDate;

    private java.math.BigInteger freightAmount;

    private java.lang.String invoiceNumber;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType;

    private com.usatech.iso8583.interchange.firstdata.securetransport.PartialAuthorizationIndicatorType partialAuthorizationIndicator;

    private java.lang.String purchaseOrderNumber;

    private java.lang.Boolean recurringFlag;

    private java.lang.String serviceProviderIdEchoValue;

    private java.lang.String shipFromZIP;

    private java.lang.String shipToZIP;

    private java.math.BigDecimal surchargeAmount;

    private java.math.BigInteger systemTraceNumber;

    private java.math.BigDecimal taxAmount;

    private java.lang.String taxExempt;

    private java.lang.String track1;

    private java.lang.String track2;

    private java.math.BigDecimal transactionAmount;

    private java.lang.String transactionDate;

    private java.lang.String transactionTime;

    private java.lang.String transmissionDateTime;

    private java.lang.String TPPID;

    private java.lang.String UCAF;

    private com.usatech.iso8583.interchange.firstdata.securetransport.UCAFSecurityLevelType UCAFSecurityLevel;

    private java.lang.String vbVCAVV;

    private java.lang.String vbVXID;

    private java.lang.String ZIPCode;

    private java.lang.String driverNumber;

    private java.lang.String odometerReading;

    private java.lang.String vehicleNumber;

    private com.usatech.iso8583.interchange.firstdata.securetransport.PetroleumItemType[] petroleumItems;

    public CreditCardChargeType() {
    }

    public CreditCardChargeType(
           java.math.BigInteger accountNumber,
           java.lang.String addressLine1,
           java.lang.String cardVerificationValue,
           java.lang.String commercialCardType,
           java.math.BigInteger dutyAmount,
           java.lang.String expirationDate,
           java.math.BigInteger freightAmount,
           java.lang.String invoiceNumber,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType,
           com.usatech.iso8583.interchange.firstdata.securetransport.PartialAuthorizationIndicatorType partialAuthorizationIndicator,
           java.lang.String purchaseOrderNumber,
           java.lang.Boolean recurringFlag,
           java.lang.String serviceProviderIdEchoValue,
           java.lang.String shipFromZIP,
           java.lang.String shipToZIP,
           java.math.BigDecimal surchargeAmount,
           java.math.BigInteger systemTraceNumber,
           java.math.BigDecimal taxAmount,
           java.lang.String taxExempt,
           java.lang.String track1,
           java.lang.String track2,
           java.math.BigDecimal transactionAmount,
           java.lang.String transactionDate,
           java.lang.String transactionTime,
           java.lang.String transmissionDateTime,
           java.lang.String TPPID,
           java.lang.String UCAF,
           com.usatech.iso8583.interchange.firstdata.securetransport.UCAFSecurityLevelType UCAFSecurityLevel,
           java.lang.String vbVCAVV,
           java.lang.String vbVXID,
           java.lang.String ZIPCode,
           java.lang.String driverNumber,
           java.lang.String odometerReading,
           java.lang.String vehicleNumber,
           com.usatech.iso8583.interchange.firstdata.securetransport.PetroleumItemType[] petroleumItems) {
           this.accountNumber = accountNumber;
           this.addressLine1 = addressLine1;
           this.cardVerificationValue = cardVerificationValue;
           this.commercialCardType = commercialCardType;
           this.dutyAmount = dutyAmount;
           this.expirationDate = expirationDate;
           this.freightAmount = freightAmount;
           this.invoiceNumber = invoiceNumber;
           this.POSConditionCode = POSConditionCode;
           this.POSPANEntryMode = POSPANEntryMode;
           this.POSPINEntryMode = POSPINEntryMode;
           this.POSSecurityCondition = POSSecurityCondition;
           this.POSTerminalType = POSTerminalType;
           this.partialAuthorizationIndicator = partialAuthorizationIndicator;
           this.purchaseOrderNumber = purchaseOrderNumber;
           this.recurringFlag = recurringFlag;
           this.serviceProviderIdEchoValue = serviceProviderIdEchoValue;
           this.shipFromZIP = shipFromZIP;
           this.shipToZIP = shipToZIP;
           this.surchargeAmount = surchargeAmount;
           this.systemTraceNumber = systemTraceNumber;
           this.taxAmount = taxAmount;
           this.taxExempt = taxExempt;
           this.track1 = track1;
           this.track2 = track2;
           this.transactionAmount = transactionAmount;
           this.transactionDate = transactionDate;
           this.transactionTime = transactionTime;
           this.transmissionDateTime = transmissionDateTime;
           this.TPPID = TPPID;
           this.UCAF = UCAF;
           this.UCAFSecurityLevel = UCAFSecurityLevel;
           this.vbVCAVV = vbVCAVV;
           this.vbVXID = vbVXID;
           this.ZIPCode = ZIPCode;
           this.driverNumber = driverNumber;
           this.odometerReading = odometerReading;
           this.vehicleNumber = vehicleNumber;
           this.petroleumItems = petroleumItems;
    }


    /**
     * Gets the accountNumber value for this CreditCardChargeType.
     * 
     * @return accountNumber
     */
    public java.math.BigInteger getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this CreditCardChargeType.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.math.BigInteger accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the addressLine1 value for this CreditCardChargeType.
     * 
     * @return addressLine1
     */
    public java.lang.String getAddressLine1() {
        return addressLine1;
    }


    /**
     * Sets the addressLine1 value for this CreditCardChargeType.
     * 
     * @param addressLine1
     */
    public void setAddressLine1(java.lang.String addressLine1) {
        this.addressLine1 = addressLine1;
    }


    /**
     * Gets the cardVerificationValue value for this CreditCardChargeType.
     * 
     * @return cardVerificationValue
     */
    public java.lang.String getCardVerificationValue() {
        return cardVerificationValue;
    }


    /**
     * Sets the cardVerificationValue value for this CreditCardChargeType.
     * 
     * @param cardVerificationValue
     */
    public void setCardVerificationValue(java.lang.String cardVerificationValue) {
        this.cardVerificationValue = cardVerificationValue;
    }


    /**
     * Gets the commercialCardType value for this CreditCardChargeType.
     * 
     * @return commercialCardType
     */
    public java.lang.String getCommercialCardType() {
        return commercialCardType;
    }


    /**
     * Sets the commercialCardType value for this CreditCardChargeType.
     * 
     * @param commercialCardType
     */
    public void setCommercialCardType(java.lang.String commercialCardType) {
        this.commercialCardType = commercialCardType;
    }


    /**
     * Gets the dutyAmount value for this CreditCardChargeType.
     * 
     * @return dutyAmount
     */
    public java.math.BigInteger getDutyAmount() {
        return dutyAmount;
    }


    /**
     * Sets the dutyAmount value for this CreditCardChargeType.
     * 
     * @param dutyAmount
     */
    public void setDutyAmount(java.math.BigInteger dutyAmount) {
        this.dutyAmount = dutyAmount;
    }


    /**
     * Gets the expirationDate value for this CreditCardChargeType.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this CreditCardChargeType.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the freightAmount value for this CreditCardChargeType.
     * 
     * @return freightAmount
     */
    public java.math.BigInteger getFreightAmount() {
        return freightAmount;
    }


    /**
     * Sets the freightAmount value for this CreditCardChargeType.
     * 
     * @param freightAmount
     */
    public void setFreightAmount(java.math.BigInteger freightAmount) {
        this.freightAmount = freightAmount;
    }


    /**
     * Gets the invoiceNumber value for this CreditCardChargeType.
     * 
     * @return invoiceNumber
     */
    public java.lang.String getInvoiceNumber() {
        return invoiceNumber;
    }


    /**
     * Sets the invoiceNumber value for this CreditCardChargeType.
     * 
     * @param invoiceNumber
     */
    public void setInvoiceNumber(java.lang.String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    /**
     * Gets the POSConditionCode value for this CreditCardChargeType.
     * 
     * @return POSConditionCode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType getPOSConditionCode() {
        return POSConditionCode;
    }


    /**
     * Sets the POSConditionCode value for this CreditCardChargeType.
     * 
     * @param POSConditionCode
     */
    public void setPOSConditionCode(com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode) {
        this.POSConditionCode = POSConditionCode;
    }


    /**
     * Gets the POSPANEntryMode value for this CreditCardChargeType.
     * 
     * @return POSPANEntryMode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType getPOSPANEntryMode() {
        return POSPANEntryMode;
    }


    /**
     * Sets the POSPANEntryMode value for this CreditCardChargeType.
     * 
     * @param POSPANEntryMode
     */
    public void setPOSPANEntryMode(com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode) {
        this.POSPANEntryMode = POSPANEntryMode;
    }


    /**
     * Gets the POSPINEntryMode value for this CreditCardChargeType.
     * 
     * @return POSPINEntryMode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType getPOSPINEntryMode() {
        return POSPINEntryMode;
    }


    /**
     * Sets the POSPINEntryMode value for this CreditCardChargeType.
     * 
     * @param POSPINEntryMode
     */
    public void setPOSPINEntryMode(com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode) {
        this.POSPINEntryMode = POSPINEntryMode;
    }


    /**
     * Gets the POSSecurityCondition value for this CreditCardChargeType.
     * 
     * @return POSSecurityCondition
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType getPOSSecurityCondition() {
        return POSSecurityCondition;
    }


    /**
     * Sets the POSSecurityCondition value for this CreditCardChargeType.
     * 
     * @param POSSecurityCondition
     */
    public void setPOSSecurityCondition(com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition) {
        this.POSSecurityCondition = POSSecurityCondition;
    }


    /**
     * Gets the POSTerminalType value for this CreditCardChargeType.
     * 
     * @return POSTerminalType
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType getPOSTerminalType() {
        return POSTerminalType;
    }


    /**
     * Sets the POSTerminalType value for this CreditCardChargeType.
     * 
     * @param POSTerminalType
     */
    public void setPOSTerminalType(com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType) {
        this.POSTerminalType = POSTerminalType;
    }


    /**
     * Gets the partialAuthorizationIndicator value for this CreditCardChargeType.
     * 
     * @return partialAuthorizationIndicator
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.PartialAuthorizationIndicatorType getPartialAuthorizationIndicator() {
        return partialAuthorizationIndicator;
    }


    /**
     * Sets the partialAuthorizationIndicator value for this CreditCardChargeType.
     * 
     * @param partialAuthorizationIndicator
     */
    public void setPartialAuthorizationIndicator(com.usatech.iso8583.interchange.firstdata.securetransport.PartialAuthorizationIndicatorType partialAuthorizationIndicator) {
        this.partialAuthorizationIndicator = partialAuthorizationIndicator;
    }


    /**
     * Gets the purchaseOrderNumber value for this CreditCardChargeType.
     * 
     * @return purchaseOrderNumber
     */
    public java.lang.String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }


    /**
     * Sets the purchaseOrderNumber value for this CreditCardChargeType.
     * 
     * @param purchaseOrderNumber
     */
    public void setPurchaseOrderNumber(java.lang.String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }


    /**
     * Gets the recurringFlag value for this CreditCardChargeType.
     * 
     * @return recurringFlag
     */
    public java.lang.Boolean getRecurringFlag() {
        return recurringFlag;
    }


    /**
     * Sets the recurringFlag value for this CreditCardChargeType.
     * 
     * @param recurringFlag
     */
    public void setRecurringFlag(java.lang.Boolean recurringFlag) {
        this.recurringFlag = recurringFlag;
    }


    /**
     * Gets the serviceProviderIdEchoValue value for this CreditCardChargeType.
     * 
     * @return serviceProviderIdEchoValue
     */
    public java.lang.String getServiceProviderIdEchoValue() {
        return serviceProviderIdEchoValue;
    }


    /**
     * Sets the serviceProviderIdEchoValue value for this CreditCardChargeType.
     * 
     * @param serviceProviderIdEchoValue
     */
    public void setServiceProviderIdEchoValue(java.lang.String serviceProviderIdEchoValue) {
        this.serviceProviderIdEchoValue = serviceProviderIdEchoValue;
    }


    /**
     * Gets the shipFromZIP value for this CreditCardChargeType.
     * 
     * @return shipFromZIP
     */
    public java.lang.String getShipFromZIP() {
        return shipFromZIP;
    }


    /**
     * Sets the shipFromZIP value for this CreditCardChargeType.
     * 
     * @param shipFromZIP
     */
    public void setShipFromZIP(java.lang.String shipFromZIP) {
        this.shipFromZIP = shipFromZIP;
    }


    /**
     * Gets the shipToZIP value for this CreditCardChargeType.
     * 
     * @return shipToZIP
     */
    public java.lang.String getShipToZIP() {
        return shipToZIP;
    }


    /**
     * Sets the shipToZIP value for this CreditCardChargeType.
     * 
     * @param shipToZIP
     */
    public void setShipToZIP(java.lang.String shipToZIP) {
        this.shipToZIP = shipToZIP;
    }


    /**
     * Gets the surchargeAmount value for this CreditCardChargeType.
     * 
     * @return surchargeAmount
     */
    public java.math.BigDecimal getSurchargeAmount() {
        return surchargeAmount;
    }


    /**
     * Sets the surchargeAmount value for this CreditCardChargeType.
     * 
     * @param surchargeAmount
     */
    public void setSurchargeAmount(java.math.BigDecimal surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }


    /**
     * Gets the systemTraceNumber value for this CreditCardChargeType.
     * 
     * @return systemTraceNumber
     */
    public java.math.BigInteger getSystemTraceNumber() {
        return systemTraceNumber;
    }


    /**
     * Sets the systemTraceNumber value for this CreditCardChargeType.
     * 
     * @param systemTraceNumber
     */
    public void setSystemTraceNumber(java.math.BigInteger systemTraceNumber) {
        this.systemTraceNumber = systemTraceNumber;
    }


    /**
     * Gets the taxAmount value for this CreditCardChargeType.
     * 
     * @return taxAmount
     */
    public java.math.BigDecimal getTaxAmount() {
        return taxAmount;
    }


    /**
     * Sets the taxAmount value for this CreditCardChargeType.
     * 
     * @param taxAmount
     */
    public void setTaxAmount(java.math.BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }


    /**
     * Gets the taxExempt value for this CreditCardChargeType.
     * 
     * @return taxExempt
     */
    public java.lang.String getTaxExempt() {
        return taxExempt;
    }


    /**
     * Sets the taxExempt value for this CreditCardChargeType.
     * 
     * @param taxExempt
     */
    public void setTaxExempt(java.lang.String taxExempt) {
        this.taxExempt = taxExempt;
    }


    /**
     * Gets the track1 value for this CreditCardChargeType.
     * 
     * @return track1
     */
    public java.lang.String getTrack1() {
        return track1;
    }


    /**
     * Sets the track1 value for this CreditCardChargeType.
     * 
     * @param track1
     */
    public void setTrack1(java.lang.String track1) {
        this.track1 = track1;
    }


    /**
     * Gets the track2 value for this CreditCardChargeType.
     * 
     * @return track2
     */
    public java.lang.String getTrack2() {
        return track2;
    }


    /**
     * Sets the track2 value for this CreditCardChargeType.
     * 
     * @param track2
     */
    public void setTrack2(java.lang.String track2) {
        this.track2 = track2;
    }


    /**
     * Gets the transactionAmount value for this CreditCardChargeType.
     * 
     * @return transactionAmount
     */
    public java.math.BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    /**
     * Sets the transactionAmount value for this CreditCardChargeType.
     * 
     * @param transactionAmount
     */
    public void setTransactionAmount(java.math.BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    /**
     * Gets the transactionDate value for this CreditCardChargeType.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this CreditCardChargeType.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the transactionTime value for this CreditCardChargeType.
     * 
     * @return transactionTime
     */
    public java.lang.String getTransactionTime() {
        return transactionTime;
    }


    /**
     * Sets the transactionTime value for this CreditCardChargeType.
     * 
     * @param transactionTime
     */
    public void setTransactionTime(java.lang.String transactionTime) {
        this.transactionTime = transactionTime;
    }


    /**
     * Gets the transmissionDateTime value for this CreditCardChargeType.
     * 
     * @return transmissionDateTime
     */
    public java.lang.String getTransmissionDateTime() {
        return transmissionDateTime;
    }


    /**
     * Sets the transmissionDateTime value for this CreditCardChargeType.
     * 
     * @param transmissionDateTime
     */
    public void setTransmissionDateTime(java.lang.String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }


    /**
     * Gets the TPPID value for this CreditCardChargeType.
     * 
     * @return TPPID
     */
    public java.lang.String getTPPID() {
        return TPPID;
    }


    /**
     * Sets the TPPID value for this CreditCardChargeType.
     * 
     * @param TPPID
     */
    public void setTPPID(java.lang.String TPPID) {
        this.TPPID = TPPID;
    }


    /**
     * Gets the UCAF value for this CreditCardChargeType.
     * 
     * @return UCAF
     */
    public java.lang.String getUCAF() {
        return UCAF;
    }


    /**
     * Sets the UCAF value for this CreditCardChargeType.
     * 
     * @param UCAF
     */
    public void setUCAF(java.lang.String UCAF) {
        this.UCAF = UCAF;
    }


    /**
     * Gets the UCAFSecurityLevel value for this CreditCardChargeType.
     * 
     * @return UCAFSecurityLevel
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.UCAFSecurityLevelType getUCAFSecurityLevel() {
        return UCAFSecurityLevel;
    }


    /**
     * Sets the UCAFSecurityLevel value for this CreditCardChargeType.
     * 
     * @param UCAFSecurityLevel
     */
    public void setUCAFSecurityLevel(com.usatech.iso8583.interchange.firstdata.securetransport.UCAFSecurityLevelType UCAFSecurityLevel) {
        this.UCAFSecurityLevel = UCAFSecurityLevel;
    }


    /**
     * Gets the vbVCAVV value for this CreditCardChargeType.
     * 
     * @return vbVCAVV
     */
    public java.lang.String getVbVCAVV() {
        return vbVCAVV;
    }


    /**
     * Sets the vbVCAVV value for this CreditCardChargeType.
     * 
     * @param vbVCAVV
     */
    public void setVbVCAVV(java.lang.String vbVCAVV) {
        this.vbVCAVV = vbVCAVV;
    }


    /**
     * Gets the vbVXID value for this CreditCardChargeType.
     * 
     * @return vbVXID
     */
    public java.lang.String getVbVXID() {
        return vbVXID;
    }


    /**
     * Sets the vbVXID value for this CreditCardChargeType.
     * 
     * @param vbVXID
     */
    public void setVbVXID(java.lang.String vbVXID) {
        this.vbVXID = vbVXID;
    }


    /**
     * Gets the ZIPCode value for this CreditCardChargeType.
     * 
     * @return ZIPCode
     */
    public java.lang.String getZIPCode() {
        return ZIPCode;
    }


    /**
     * Sets the ZIPCode value for this CreditCardChargeType.
     * 
     * @param ZIPCode
     */
    public void setZIPCode(java.lang.String ZIPCode) {
        this.ZIPCode = ZIPCode;
    }


    /**
     * Gets the driverNumber value for this CreditCardChargeType.
     * 
     * @return driverNumber
     */
    public java.lang.String getDriverNumber() {
        return driverNumber;
    }


    /**
     * Sets the driverNumber value for this CreditCardChargeType.
     * 
     * @param driverNumber
     */
    public void setDriverNumber(java.lang.String driverNumber) {
        this.driverNumber = driverNumber;
    }


    /**
     * Gets the odometerReading value for this CreditCardChargeType.
     * 
     * @return odometerReading
     */
    public java.lang.String getOdometerReading() {
        return odometerReading;
    }


    /**
     * Sets the odometerReading value for this CreditCardChargeType.
     * 
     * @param odometerReading
     */
    public void setOdometerReading(java.lang.String odometerReading) {
        this.odometerReading = odometerReading;
    }


    /**
     * Gets the vehicleNumber value for this CreditCardChargeType.
     * 
     * @return vehicleNumber
     */
    public java.lang.String getVehicleNumber() {
        return vehicleNumber;
    }


    /**
     * Sets the vehicleNumber value for this CreditCardChargeType.
     * 
     * @param vehicleNumber
     */
    public void setVehicleNumber(java.lang.String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }


    /**
     * Gets the petroleumItems value for this CreditCardChargeType.
     * 
     * @return petroleumItems
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.PetroleumItemType[] getPetroleumItems() {
        return petroleumItems;
    }


    /**
     * Sets the petroleumItems value for this CreditCardChargeType.
     * 
     * @param petroleumItems
     */
    public void setPetroleumItems(com.usatech.iso8583.interchange.firstdata.securetransport.PetroleumItemType[] petroleumItems) {
        this.petroleumItems = petroleumItems;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreditCardChargeType)) return false;
        CreditCardChargeType other = (CreditCardChargeType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.addressLine1==null && other.getAddressLine1()==null) || 
             (this.addressLine1!=null &&
              this.addressLine1.equals(other.getAddressLine1()))) &&
            ((this.cardVerificationValue==null && other.getCardVerificationValue()==null) || 
             (this.cardVerificationValue!=null &&
              this.cardVerificationValue.equals(other.getCardVerificationValue()))) &&
            ((this.commercialCardType==null && other.getCommercialCardType()==null) || 
             (this.commercialCardType!=null &&
              this.commercialCardType.equals(other.getCommercialCardType()))) &&
            ((this.dutyAmount==null && other.getDutyAmount()==null) || 
             (this.dutyAmount!=null &&
              this.dutyAmount.equals(other.getDutyAmount()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.freightAmount==null && other.getFreightAmount()==null) || 
             (this.freightAmount!=null &&
              this.freightAmount.equals(other.getFreightAmount()))) &&
            ((this.invoiceNumber==null && other.getInvoiceNumber()==null) || 
             (this.invoiceNumber!=null &&
              this.invoiceNumber.equals(other.getInvoiceNumber()))) &&
            ((this.POSConditionCode==null && other.getPOSConditionCode()==null) || 
             (this.POSConditionCode!=null &&
              this.POSConditionCode.equals(other.getPOSConditionCode()))) &&
            ((this.POSPANEntryMode==null && other.getPOSPANEntryMode()==null) || 
             (this.POSPANEntryMode!=null &&
              this.POSPANEntryMode.equals(other.getPOSPANEntryMode()))) &&
            ((this.POSPINEntryMode==null && other.getPOSPINEntryMode()==null) || 
             (this.POSPINEntryMode!=null &&
              this.POSPINEntryMode.equals(other.getPOSPINEntryMode()))) &&
            ((this.POSSecurityCondition==null && other.getPOSSecurityCondition()==null) || 
             (this.POSSecurityCondition!=null &&
              this.POSSecurityCondition.equals(other.getPOSSecurityCondition()))) &&
            ((this.POSTerminalType==null && other.getPOSTerminalType()==null) || 
             (this.POSTerminalType!=null &&
              this.POSTerminalType.equals(other.getPOSTerminalType()))) &&
            ((this.partialAuthorizationIndicator==null && other.getPartialAuthorizationIndicator()==null) || 
             (this.partialAuthorizationIndicator!=null &&
              this.partialAuthorizationIndicator.equals(other.getPartialAuthorizationIndicator()))) &&
            ((this.purchaseOrderNumber==null && other.getPurchaseOrderNumber()==null) || 
             (this.purchaseOrderNumber!=null &&
              this.purchaseOrderNumber.equals(other.getPurchaseOrderNumber()))) &&
            ((this.recurringFlag==null && other.getRecurringFlag()==null) || 
             (this.recurringFlag!=null &&
              this.recurringFlag.equals(other.getRecurringFlag()))) &&
            ((this.serviceProviderIdEchoValue==null && other.getServiceProviderIdEchoValue()==null) || 
             (this.serviceProviderIdEchoValue!=null &&
              this.serviceProviderIdEchoValue.equals(other.getServiceProviderIdEchoValue()))) &&
            ((this.shipFromZIP==null && other.getShipFromZIP()==null) || 
             (this.shipFromZIP!=null &&
              this.shipFromZIP.equals(other.getShipFromZIP()))) &&
            ((this.shipToZIP==null && other.getShipToZIP()==null) || 
             (this.shipToZIP!=null &&
              this.shipToZIP.equals(other.getShipToZIP()))) &&
            ((this.surchargeAmount==null && other.getSurchargeAmount()==null) || 
             (this.surchargeAmount!=null &&
              this.surchargeAmount.equals(other.getSurchargeAmount()))) &&
            ((this.systemTraceNumber==null && other.getSystemTraceNumber()==null) || 
             (this.systemTraceNumber!=null &&
              this.systemTraceNumber.equals(other.getSystemTraceNumber()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.taxExempt==null && other.getTaxExempt()==null) || 
             (this.taxExempt!=null &&
              this.taxExempt.equals(other.getTaxExempt()))) &&
            ((this.track1==null && other.getTrack1()==null) || 
             (this.track1!=null &&
              this.track1.equals(other.getTrack1()))) &&
            ((this.track2==null && other.getTrack2()==null) || 
             (this.track2!=null &&
              this.track2.equals(other.getTrack2()))) &&
            ((this.transactionAmount==null && other.getTransactionAmount()==null) || 
             (this.transactionAmount!=null &&
              this.transactionAmount.equals(other.getTransactionAmount()))) &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.transactionTime==null && other.getTransactionTime()==null) || 
             (this.transactionTime!=null &&
              this.transactionTime.equals(other.getTransactionTime()))) &&
            ((this.transmissionDateTime==null && other.getTransmissionDateTime()==null) || 
             (this.transmissionDateTime!=null &&
              this.transmissionDateTime.equals(other.getTransmissionDateTime()))) &&
            ((this.TPPID==null && other.getTPPID()==null) || 
             (this.TPPID!=null &&
              this.TPPID.equals(other.getTPPID()))) &&
            ((this.UCAF==null && other.getUCAF()==null) || 
             (this.UCAF!=null &&
              this.UCAF.equals(other.getUCAF()))) &&
            ((this.UCAFSecurityLevel==null && other.getUCAFSecurityLevel()==null) || 
             (this.UCAFSecurityLevel!=null &&
              this.UCAFSecurityLevel.equals(other.getUCAFSecurityLevel()))) &&
            ((this.vbVCAVV==null && other.getVbVCAVV()==null) || 
             (this.vbVCAVV!=null &&
              this.vbVCAVV.equals(other.getVbVCAVV()))) &&
            ((this.vbVXID==null && other.getVbVXID()==null) || 
             (this.vbVXID!=null &&
              this.vbVXID.equals(other.getVbVXID()))) &&
            ((this.ZIPCode==null && other.getZIPCode()==null) || 
             (this.ZIPCode!=null &&
              this.ZIPCode.equals(other.getZIPCode()))) &&
            ((this.driverNumber==null && other.getDriverNumber()==null) || 
             (this.driverNumber!=null &&
              this.driverNumber.equals(other.getDriverNumber()))) &&
            ((this.odometerReading==null && other.getOdometerReading()==null) || 
             (this.odometerReading!=null &&
              this.odometerReading.equals(other.getOdometerReading()))) &&
            ((this.vehicleNumber==null && other.getVehicleNumber()==null) || 
             (this.vehicleNumber!=null &&
              this.vehicleNumber.equals(other.getVehicleNumber()))) &&
            ((this.petroleumItems==null && other.getPetroleumItems()==null) || 
             (this.petroleumItems!=null &&
              java.util.Arrays.equals(this.petroleumItems, other.getPetroleumItems())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getAddressLine1() != null) {
            _hashCode += getAddressLine1().hashCode();
        }
        if (getCardVerificationValue() != null) {
            _hashCode += getCardVerificationValue().hashCode();
        }
        if (getCommercialCardType() != null) {
            _hashCode += getCommercialCardType().hashCode();
        }
        if (getDutyAmount() != null) {
            _hashCode += getDutyAmount().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getFreightAmount() != null) {
            _hashCode += getFreightAmount().hashCode();
        }
        if (getInvoiceNumber() != null) {
            _hashCode += getInvoiceNumber().hashCode();
        }
        if (getPOSConditionCode() != null) {
            _hashCode += getPOSConditionCode().hashCode();
        }
        if (getPOSPANEntryMode() != null) {
            _hashCode += getPOSPANEntryMode().hashCode();
        }
        if (getPOSPINEntryMode() != null) {
            _hashCode += getPOSPINEntryMode().hashCode();
        }
        if (getPOSSecurityCondition() != null) {
            _hashCode += getPOSSecurityCondition().hashCode();
        }
        if (getPOSTerminalType() != null) {
            _hashCode += getPOSTerminalType().hashCode();
        }
        if (getPartialAuthorizationIndicator() != null) {
            _hashCode += getPartialAuthorizationIndicator().hashCode();
        }
        if (getPurchaseOrderNumber() != null) {
            _hashCode += getPurchaseOrderNumber().hashCode();
        }
        if (getRecurringFlag() != null) {
            _hashCode += getRecurringFlag().hashCode();
        }
        if (getServiceProviderIdEchoValue() != null) {
            _hashCode += getServiceProviderIdEchoValue().hashCode();
        }
        if (getShipFromZIP() != null) {
            _hashCode += getShipFromZIP().hashCode();
        }
        if (getShipToZIP() != null) {
            _hashCode += getShipToZIP().hashCode();
        }
        if (getSurchargeAmount() != null) {
            _hashCode += getSurchargeAmount().hashCode();
        }
        if (getSystemTraceNumber() != null) {
            _hashCode += getSystemTraceNumber().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getTaxExempt() != null) {
            _hashCode += getTaxExempt().hashCode();
        }
        if (getTrack1() != null) {
            _hashCode += getTrack1().hashCode();
        }
        if (getTrack2() != null) {
            _hashCode += getTrack2().hashCode();
        }
        if (getTransactionAmount() != null) {
            _hashCode += getTransactionAmount().hashCode();
        }
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getTransactionTime() != null) {
            _hashCode += getTransactionTime().hashCode();
        }
        if (getTransmissionDateTime() != null) {
            _hashCode += getTransmissionDateTime().hashCode();
        }
        if (getTPPID() != null) {
            _hashCode += getTPPID().hashCode();
        }
        if (getUCAF() != null) {
            _hashCode += getUCAF().hashCode();
        }
        if (getUCAFSecurityLevel() != null) {
            _hashCode += getUCAFSecurityLevel().hashCode();
        }
        if (getVbVCAVV() != null) {
            _hashCode += getVbVCAVV().hashCode();
        }
        if (getVbVXID() != null) {
            _hashCode += getVbVXID().hashCode();
        }
        if (getZIPCode() != null) {
            _hashCode += getZIPCode().hashCode();
        }
        if (getDriverNumber() != null) {
            _hashCode += getDriverNumber().hashCode();
        }
        if (getOdometerReading() != null) {
            _hashCode += getOdometerReading().hashCode();
        }
        if (getVehicleNumber() != null) {
            _hashCode += getVehicleNumber().hashCode();
        }
        if (getPetroleumItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPetroleumItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPetroleumItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditCardChargeType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardChargeType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AccountNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressLine1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AddressLine1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AddressLine1Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardVerificationValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CardVerificationValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CardVerificationValueType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commercialCardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CommercialCardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dutyAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DutyAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ExpirationDateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("freightAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FreightAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoiceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "InvoiceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "InvoiceNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSConditionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCodeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSPANEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPANEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPANEntryModeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSPINEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPINEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPINEntryModeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSSecurityCondition");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSSecurityCondition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSSecurityConditionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTerminalType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalTypeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partialAuthorizationIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "PartialAuthorizationIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "PartialAuthorizationIndicatorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchaseOrderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "PurchaseOrderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "PurchaseOrderType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recurringFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "RecurringFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceProviderIdEchoValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ServiceProviderIdEchoValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ServiceProviderIdEchoValueType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipFromZIP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ShipFromZIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ZipType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToZIP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ShipToZIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ZipType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SurchargeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SurchargeAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemTraceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceAuditNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TaxAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxExempt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TaxExempt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("track1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track1Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("track2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track2Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmountType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionDateType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionTimeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTimeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TPPID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TPPID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TPPIDType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UCAF");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "UCAF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "UCAFType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UCAFSecurityLevel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "UCAFSecurityLevel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "UCAFSecurityLevelType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vbVCAVV");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "VbVCAVV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "VbVCAVVType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vbVXID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "VbVXID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "VbVXIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIPCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ZIPCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ZIPCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driverNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DriverNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DriverNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("odometerReading");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "OdometerReading"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "OdometerReadingType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vehicleNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "VehicleNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "VehicleNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("petroleumItems");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "PetroleumItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "PetroleumItemsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
