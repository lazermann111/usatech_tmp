/**
 * RequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class RequestType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.firstdata.securetransport.ReqClientIDType reqClientID;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TransactionType transaction;

    private java.lang.String version;  // attribute

    private java.math.BigInteger clientTimeout;  // attribute

    public RequestType() {
    }

    public RequestType(
           com.usatech.iso8583.interchange.firstdata.securetransport.ReqClientIDType reqClientID,
           com.usatech.iso8583.interchange.firstdata.securetransport.TransactionType transaction,
           java.lang.String version,
           java.math.BigInteger clientTimeout) {
           this.reqClientID = reqClientID;
           this.transaction = transaction;
           this.version = version;
           this.clientTimeout = clientTimeout;
    }


    /**
     * Gets the reqClientID value for this RequestType.
     * 
     * @return reqClientID
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.ReqClientIDType getReqClientID() {
        return reqClientID;
    }


    /**
     * Sets the reqClientID value for this RequestType.
     * 
     * @param reqClientID
     */
    public void setReqClientID(com.usatech.iso8583.interchange.firstdata.securetransport.ReqClientIDType reqClientID) {
        this.reqClientID = reqClientID;
    }


    /**
     * Gets the transaction value for this RequestType.
     * 
     * @return transaction
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TransactionType getTransaction() {
        return transaction;
    }


    /**
     * Sets the transaction value for this RequestType.
     * 
     * @param transaction
     */
    public void setTransaction(com.usatech.iso8583.interchange.firstdata.securetransport.TransactionType transaction) {
        this.transaction = transaction;
    }


    /**
     * Gets the version value for this RequestType.
     * 
     * @return version
     */
    public java.lang.String getVersion() {
        return version;
    }


    /**
     * Sets the version value for this RequestType.
     * 
     * @param version
     */
    public void setVersion(java.lang.String version) {
        this.version = version;
    }


    /**
     * Gets the clientTimeout value for this RequestType.
     * 
     * @return clientTimeout
     */
    public java.math.BigInteger getClientTimeout() {
        return clientTimeout;
    }


    /**
     * Sets the clientTimeout value for this RequestType.
     * 
     * @param clientTimeout
     */
    public void setClientTimeout(java.math.BigInteger clientTimeout) {
        this.clientTimeout = clientTimeout;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestType)) return false;
        RequestType other = (RequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.reqClientID==null && other.getReqClientID()==null) || 
             (this.reqClientID!=null &&
              this.reqClientID.equals(other.getReqClientID()))) &&
            ((this.transaction==null && other.getTransaction()==null) || 
             (this.transaction!=null &&
              this.transaction.equals(other.getTransaction()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.clientTimeout==null && other.getClientTimeout()==null) || 
             (this.clientTimeout!=null &&
              this.clientTimeout.equals(other.getClientTimeout())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReqClientID() != null) {
            _hashCode += getReqClientID().hashCode();
        }
        if (getTransaction() != null) {
            _hashCode += getTransaction().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getClientTimeout() != null) {
            _hashCode += getClientTimeout().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "RequestType"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("version");
        attrField.setXmlName(new javax.xml.namespace.QName("", "Version"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">RequestType>Version"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("clientTimeout");
        attrField.setXmlName(new javax.xml.namespace.QName("", "ClientTimeout"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">RequestType>ClientTimeout"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reqClientID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ReqClientID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ReqClientIDType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transaction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Transaction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
