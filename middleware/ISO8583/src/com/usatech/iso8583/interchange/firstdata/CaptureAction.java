package com.usatech.iso8583.interchange.firstdata;

import java.text.ParseException;
import java.util.Date;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureResponseType;
import com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureType;
import com.usatech.iso8583.interchange.firstdata.securetransport.ResponseType;
import com.usatech.iso8583.interchange.firstdata.securetransport.TransactionType;

public class CaptureAction extends FirstDataAction {
	private static int[] requiredFields = 
	{ 	ISO8583Message.FIELD_APPROVAL_CODE, //ApprovalCode
		ISO8583Message.FIELD_TRACE_NUMBER, //InvoiceNumber
		ISO8583Message.FIELD_ENTRY_MODE, //POSConditionCode
		ISO8583Message.FIELD_AMOUNT, // TransactionAmount
		ISO8583Message.FIELD_EFFECTIVE_DATE, // TransactionDate
	};
	
	public CaptureAction(FirstDataInterchange interchange) {
		super(interchange);
	}

	protected void validateRequestSpecificFields(ISO8583Request request) throws ValidationException
	{
		validateRequestCardData(request);	
	}
	
	@Override
	protected void initTransaction(ISO8583Request isoRequest, TransactionType transaction, String TPPID)
	{
		Date effectiveDate = new Date();
		isoRequest.setEffectiveDate(effectiveDate);
		
		GiftCardCaptureType transactionRequest = new GiftCardCaptureType();
		transactionRequest.setTPPID(TPPID);
		transactionRequest.setApprovalCode(isoRequest.getApprovalCode());
		transactionRequest.setInvoiceNumber(getInvoiceNumber(isoRequest));		
		transactionRequest.setPOSConditionCode(getPOSConditionCodeType(isoRequest));		
		transactionRequest.setPOSSecurityCondition(getPOSSecurityConditionType());
		transactionRequest.setPOSTerminalType(getPOSTerminalTypeType());		
		transactionRequest.setSystemTraceNumber(getSystemTraceNumber(isoRequest));		
		transactionRequest.setTransactionAmount(getTransactionAmount(isoRequest));		
		transactionRequest.setTransactionDate(getTransactionDate(effectiveDate));
		transactionRequest.setTransactionTime(getTransactionTime(effectiveDate));
		transactionRequest.setTransmissionDateTime(getTransmissionDateTime(effectiveDate));		

		if (isoRequest.hasTrackData())
		{
			if (isoRequest.getTrackData().hasTrack2())
				transactionRequest.setTrack2(isoRequest.getTrackData().getTrack2());
			else if (isoRequest.getTrackData().hasTrack1())
				transactionRequest.setTrack1(isoRequest.getTrackData().getTrack1());
		}
		else
		{	
			transactionRequest.setAccountNumber(getAccountNumber(isoRequest));			
			transactionRequest.setExpirationDate(getExpirationDate(isoRequest));
		}
		
		transaction.setGiftCardCapture(transactionRequest);
	}

	@Override
	protected void initResponse(ResponseData responseData, ResponseType response) throws ParseException
	{
		GiftCardCaptureResponseType messageResponse = response.getTransactionResponse().getGiftCardCaptureResponse();
		responseData.setResponseCode(messageResponse.getResponseCode());
		responseData.setResponseMessage(messageResponse.getResponseMessage());
		responseData.setTraceNumber(messageResponse.getSystemTraceNumber());
		responseData.setAmount(messageResponse.getTransactionAmount());
	}

	@Override
	protected int[] getRequiredFields() {
		return requiredFields;
	}

	@Override
	protected String getOriginalTransactionName() {
		return "GiftCardCapture";
	}
}

