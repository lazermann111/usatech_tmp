package com.usatech.iso8583.interchange.firstdata;

import java.text.ParseException;

import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.firstdata.securetransport.ResponseType;
import com.usatech.iso8583.interchange.firstdata.securetransport.TransactionType;
import com.usatech.iso8583.transaction.ISO8583Transaction;

public class SettlementAction extends FirstDataAction
{
	public SettlementAction(FirstDataInterchange interchange) {
		super(interchange);
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		ISO8583Request request = transaction.getRequest();
		
		ISO8583Response response = new ISO8583Response();		
		response.setResponseCode(ISO8583Response.SUCCESS);
		response.setTraceNumber(request.getTraceNumber());
		response.setAmount(request.getAmount());
		
		transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);
		return response;
	}

	@Override
	protected String getOriginalTransactionName() {
		return null;
	}

	@Override
	protected int[] getRequiredFields() {
		return null;
	}

	@Override
	protected void initTransaction(ISO8583Request isoRequest, TransactionType transaction, String TPPID) {

	}

	@Override
	protected void initResponse(ResponseData responseData, ResponseType response) throws ParseException {

	}
	
	public boolean requiresRecovery()
	{
		return false;
	}
}
