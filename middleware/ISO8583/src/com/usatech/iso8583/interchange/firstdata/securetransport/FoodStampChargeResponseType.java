/**
 * FoodStampChargeResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class FoodStampChargeResponseType  implements java.io.Serializable {
    private java.lang.String AVSResultCode;

    private java.math.BigInteger accountNumber;

    private java.lang.String addressLine1;

    private java.lang.String approvalCode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.AuthorizerNetworkType authorizerNetwork;

    private java.lang.String authorizerNetworkResultCode;

    private java.math.BigDecimal balanceAmountCashBenefit;

    private java.math.BigDecimal balanceAmountFoodStamp;

    private java.lang.String expirationDate;

    private java.lang.String hostReferenceNumber;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType;

    private java.lang.String responseCode;

    private java.lang.String responseMessage;

    private java.lang.String settlementDate;

    private java.math.BigDecimal surchargeAmount;

    private java.math.BigInteger systemTraceNumber;

    private java.math.BigDecimal taxAmount;

    private java.lang.String track2;

    private java.math.BigDecimal transactionAmount;

    private java.lang.String transactionDate;

    private java.lang.String transactionTime;

    private java.lang.String transmissionDateTime;

    private java.lang.String tripleDESPseudoTID;

    private java.lang.String ZIPCode;

    public FoodStampChargeResponseType() {
    }

    public FoodStampChargeResponseType(
           java.lang.String AVSResultCode,
           java.math.BigInteger accountNumber,
           java.lang.String addressLine1,
           java.lang.String approvalCode,
           com.usatech.iso8583.interchange.firstdata.securetransport.AuthorizerNetworkType authorizerNetwork,
           java.lang.String authorizerNetworkResultCode,
           java.math.BigDecimal balanceAmountCashBenefit,
           java.math.BigDecimal balanceAmountFoodStamp,
           java.lang.String expirationDate,
           java.lang.String hostReferenceNumber,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType,
           java.lang.String responseCode,
           java.lang.String responseMessage,
           java.lang.String settlementDate,
           java.math.BigDecimal surchargeAmount,
           java.math.BigInteger systemTraceNumber,
           java.math.BigDecimal taxAmount,
           java.lang.String track2,
           java.math.BigDecimal transactionAmount,
           java.lang.String transactionDate,
           java.lang.String transactionTime,
           java.lang.String transmissionDateTime,
           java.lang.String tripleDESPseudoTID,
           java.lang.String ZIPCode) {
           this.AVSResultCode = AVSResultCode;
           this.accountNumber = accountNumber;
           this.addressLine1 = addressLine1;
           this.approvalCode = approvalCode;
           this.authorizerNetwork = authorizerNetwork;
           this.authorizerNetworkResultCode = authorizerNetworkResultCode;
           this.balanceAmountCashBenefit = balanceAmountCashBenefit;
           this.balanceAmountFoodStamp = balanceAmountFoodStamp;
           this.expirationDate = expirationDate;
           this.hostReferenceNumber = hostReferenceNumber;
           this.POSConditionCode = POSConditionCode;
           this.POSPANEntryMode = POSPANEntryMode;
           this.POSPINEntryMode = POSPINEntryMode;
           this.POSSecurityCondition = POSSecurityCondition;
           this.POSTerminalType = POSTerminalType;
           this.responseCode = responseCode;
           this.responseMessage = responseMessage;
           this.settlementDate = settlementDate;
           this.surchargeAmount = surchargeAmount;
           this.systemTraceNumber = systemTraceNumber;
           this.taxAmount = taxAmount;
           this.track2 = track2;
           this.transactionAmount = transactionAmount;
           this.transactionDate = transactionDate;
           this.transactionTime = transactionTime;
           this.transmissionDateTime = transmissionDateTime;
           this.tripleDESPseudoTID = tripleDESPseudoTID;
           this.ZIPCode = ZIPCode;
    }


    /**
     * Gets the AVSResultCode value for this FoodStampChargeResponseType.
     * 
     * @return AVSResultCode
     */
    public java.lang.String getAVSResultCode() {
        return AVSResultCode;
    }


    /**
     * Sets the AVSResultCode value for this FoodStampChargeResponseType.
     * 
     * @param AVSResultCode
     */
    public void setAVSResultCode(java.lang.String AVSResultCode) {
        this.AVSResultCode = AVSResultCode;
    }


    /**
     * Gets the accountNumber value for this FoodStampChargeResponseType.
     * 
     * @return accountNumber
     */
    public java.math.BigInteger getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this FoodStampChargeResponseType.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.math.BigInteger accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the addressLine1 value for this FoodStampChargeResponseType.
     * 
     * @return addressLine1
     */
    public java.lang.String getAddressLine1() {
        return addressLine1;
    }


    /**
     * Sets the addressLine1 value for this FoodStampChargeResponseType.
     * 
     * @param addressLine1
     */
    public void setAddressLine1(java.lang.String addressLine1) {
        this.addressLine1 = addressLine1;
    }


    /**
     * Gets the approvalCode value for this FoodStampChargeResponseType.
     * 
     * @return approvalCode
     */
    public java.lang.String getApprovalCode() {
        return approvalCode;
    }


    /**
     * Sets the approvalCode value for this FoodStampChargeResponseType.
     * 
     * @param approvalCode
     */
    public void setApprovalCode(java.lang.String approvalCode) {
        this.approvalCode = approvalCode;
    }


    /**
     * Gets the authorizerNetwork value for this FoodStampChargeResponseType.
     * 
     * @return authorizerNetwork
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.AuthorizerNetworkType getAuthorizerNetwork() {
        return authorizerNetwork;
    }


    /**
     * Sets the authorizerNetwork value for this FoodStampChargeResponseType.
     * 
     * @param authorizerNetwork
     */
    public void setAuthorizerNetwork(com.usatech.iso8583.interchange.firstdata.securetransport.AuthorizerNetworkType authorizerNetwork) {
        this.authorizerNetwork = authorizerNetwork;
    }


    /**
     * Gets the authorizerNetworkResultCode value for this FoodStampChargeResponseType.
     * 
     * @return authorizerNetworkResultCode
     */
    public java.lang.String getAuthorizerNetworkResultCode() {
        return authorizerNetworkResultCode;
    }


    /**
     * Sets the authorizerNetworkResultCode value for this FoodStampChargeResponseType.
     * 
     * @param authorizerNetworkResultCode
     */
    public void setAuthorizerNetworkResultCode(java.lang.String authorizerNetworkResultCode) {
        this.authorizerNetworkResultCode = authorizerNetworkResultCode;
    }


    /**
     * Gets the balanceAmountCashBenefit value for this FoodStampChargeResponseType.
     * 
     * @return balanceAmountCashBenefit
     */
    public java.math.BigDecimal getBalanceAmountCashBenefit() {
        return balanceAmountCashBenefit;
    }


    /**
     * Sets the balanceAmountCashBenefit value for this FoodStampChargeResponseType.
     * 
     * @param balanceAmountCashBenefit
     */
    public void setBalanceAmountCashBenefit(java.math.BigDecimal balanceAmountCashBenefit) {
        this.balanceAmountCashBenefit = balanceAmountCashBenefit;
    }


    /**
     * Gets the balanceAmountFoodStamp value for this FoodStampChargeResponseType.
     * 
     * @return balanceAmountFoodStamp
     */
    public java.math.BigDecimal getBalanceAmountFoodStamp() {
        return balanceAmountFoodStamp;
    }


    /**
     * Sets the balanceAmountFoodStamp value for this FoodStampChargeResponseType.
     * 
     * @param balanceAmountFoodStamp
     */
    public void setBalanceAmountFoodStamp(java.math.BigDecimal balanceAmountFoodStamp) {
        this.balanceAmountFoodStamp = balanceAmountFoodStamp;
    }


    /**
     * Gets the expirationDate value for this FoodStampChargeResponseType.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this FoodStampChargeResponseType.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the hostReferenceNumber value for this FoodStampChargeResponseType.
     * 
     * @return hostReferenceNumber
     */
    public java.lang.String getHostReferenceNumber() {
        return hostReferenceNumber;
    }


    /**
     * Sets the hostReferenceNumber value for this FoodStampChargeResponseType.
     * 
     * @param hostReferenceNumber
     */
    public void setHostReferenceNumber(java.lang.String hostReferenceNumber) {
        this.hostReferenceNumber = hostReferenceNumber;
    }


    /**
     * Gets the POSConditionCode value for this FoodStampChargeResponseType.
     * 
     * @return POSConditionCode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType getPOSConditionCode() {
        return POSConditionCode;
    }


    /**
     * Sets the POSConditionCode value for this FoodStampChargeResponseType.
     * 
     * @param POSConditionCode
     */
    public void setPOSConditionCode(com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode) {
        this.POSConditionCode = POSConditionCode;
    }


    /**
     * Gets the POSPANEntryMode value for this FoodStampChargeResponseType.
     * 
     * @return POSPANEntryMode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType getPOSPANEntryMode() {
        return POSPANEntryMode;
    }


    /**
     * Sets the POSPANEntryMode value for this FoodStampChargeResponseType.
     * 
     * @param POSPANEntryMode
     */
    public void setPOSPANEntryMode(com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode) {
        this.POSPANEntryMode = POSPANEntryMode;
    }


    /**
     * Gets the POSPINEntryMode value for this FoodStampChargeResponseType.
     * 
     * @return POSPINEntryMode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType getPOSPINEntryMode() {
        return POSPINEntryMode;
    }


    /**
     * Sets the POSPINEntryMode value for this FoodStampChargeResponseType.
     * 
     * @param POSPINEntryMode
     */
    public void setPOSPINEntryMode(com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode) {
        this.POSPINEntryMode = POSPINEntryMode;
    }


    /**
     * Gets the POSSecurityCondition value for this FoodStampChargeResponseType.
     * 
     * @return POSSecurityCondition
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType getPOSSecurityCondition() {
        return POSSecurityCondition;
    }


    /**
     * Sets the POSSecurityCondition value for this FoodStampChargeResponseType.
     * 
     * @param POSSecurityCondition
     */
    public void setPOSSecurityCondition(com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition) {
        this.POSSecurityCondition = POSSecurityCondition;
    }


    /**
     * Gets the POSTerminalType value for this FoodStampChargeResponseType.
     * 
     * @return POSTerminalType
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType getPOSTerminalType() {
        return POSTerminalType;
    }


    /**
     * Sets the POSTerminalType value for this FoodStampChargeResponseType.
     * 
     * @param POSTerminalType
     */
    public void setPOSTerminalType(com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType) {
        this.POSTerminalType = POSTerminalType;
    }


    /**
     * Gets the responseCode value for this FoodStampChargeResponseType.
     * 
     * @return responseCode
     */
    public java.lang.String getResponseCode() {
        return responseCode;
    }


    /**
     * Sets the responseCode value for this FoodStampChargeResponseType.
     * 
     * @param responseCode
     */
    public void setResponseCode(java.lang.String responseCode) {
        this.responseCode = responseCode;
    }


    /**
     * Gets the responseMessage value for this FoodStampChargeResponseType.
     * 
     * @return responseMessage
     */
    public java.lang.String getResponseMessage() {
        return responseMessage;
    }


    /**
     * Sets the responseMessage value for this FoodStampChargeResponseType.
     * 
     * @param responseMessage
     */
    public void setResponseMessage(java.lang.String responseMessage) {
        this.responseMessage = responseMessage;
    }


    /**
     * Gets the settlementDate value for this FoodStampChargeResponseType.
     * 
     * @return settlementDate
     */
    public java.lang.String getSettlementDate() {
        return settlementDate;
    }


    /**
     * Sets the settlementDate value for this FoodStampChargeResponseType.
     * 
     * @param settlementDate
     */
    public void setSettlementDate(java.lang.String settlementDate) {
        this.settlementDate = settlementDate;
    }


    /**
     * Gets the surchargeAmount value for this FoodStampChargeResponseType.
     * 
     * @return surchargeAmount
     */
    public java.math.BigDecimal getSurchargeAmount() {
        return surchargeAmount;
    }


    /**
     * Sets the surchargeAmount value for this FoodStampChargeResponseType.
     * 
     * @param surchargeAmount
     */
    public void setSurchargeAmount(java.math.BigDecimal surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }


    /**
     * Gets the systemTraceNumber value for this FoodStampChargeResponseType.
     * 
     * @return systemTraceNumber
     */
    public java.math.BigInteger getSystemTraceNumber() {
        return systemTraceNumber;
    }


    /**
     * Sets the systemTraceNumber value for this FoodStampChargeResponseType.
     * 
     * @param systemTraceNumber
     */
    public void setSystemTraceNumber(java.math.BigInteger systemTraceNumber) {
        this.systemTraceNumber = systemTraceNumber;
    }


    /**
     * Gets the taxAmount value for this FoodStampChargeResponseType.
     * 
     * @return taxAmount
     */
    public java.math.BigDecimal getTaxAmount() {
        return taxAmount;
    }


    /**
     * Sets the taxAmount value for this FoodStampChargeResponseType.
     * 
     * @param taxAmount
     */
    public void setTaxAmount(java.math.BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }


    /**
     * Gets the track2 value for this FoodStampChargeResponseType.
     * 
     * @return track2
     */
    public java.lang.String getTrack2() {
        return track2;
    }


    /**
     * Sets the track2 value for this FoodStampChargeResponseType.
     * 
     * @param track2
     */
    public void setTrack2(java.lang.String track2) {
        this.track2 = track2;
    }


    /**
     * Gets the transactionAmount value for this FoodStampChargeResponseType.
     * 
     * @return transactionAmount
     */
    public java.math.BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    /**
     * Sets the transactionAmount value for this FoodStampChargeResponseType.
     * 
     * @param transactionAmount
     */
    public void setTransactionAmount(java.math.BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    /**
     * Gets the transactionDate value for this FoodStampChargeResponseType.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this FoodStampChargeResponseType.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the transactionTime value for this FoodStampChargeResponseType.
     * 
     * @return transactionTime
     */
    public java.lang.String getTransactionTime() {
        return transactionTime;
    }


    /**
     * Sets the transactionTime value for this FoodStampChargeResponseType.
     * 
     * @param transactionTime
     */
    public void setTransactionTime(java.lang.String transactionTime) {
        this.transactionTime = transactionTime;
    }


    /**
     * Gets the transmissionDateTime value for this FoodStampChargeResponseType.
     * 
     * @return transmissionDateTime
     */
    public java.lang.String getTransmissionDateTime() {
        return transmissionDateTime;
    }


    /**
     * Sets the transmissionDateTime value for this FoodStampChargeResponseType.
     * 
     * @param transmissionDateTime
     */
    public void setTransmissionDateTime(java.lang.String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }


    /**
     * Gets the tripleDESPseudoTID value for this FoodStampChargeResponseType.
     * 
     * @return tripleDESPseudoTID
     */
    public java.lang.String getTripleDESPseudoTID() {
        return tripleDESPseudoTID;
    }


    /**
     * Sets the tripleDESPseudoTID value for this FoodStampChargeResponseType.
     * 
     * @param tripleDESPseudoTID
     */
    public void setTripleDESPseudoTID(java.lang.String tripleDESPseudoTID) {
        this.tripleDESPseudoTID = tripleDESPseudoTID;
    }


    /**
     * Gets the ZIPCode value for this FoodStampChargeResponseType.
     * 
     * @return ZIPCode
     */
    public java.lang.String getZIPCode() {
        return ZIPCode;
    }


    /**
     * Sets the ZIPCode value for this FoodStampChargeResponseType.
     * 
     * @param ZIPCode
     */
    public void setZIPCode(java.lang.String ZIPCode) {
        this.ZIPCode = ZIPCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FoodStampChargeResponseType)) return false;
        FoodStampChargeResponseType other = (FoodStampChargeResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.AVSResultCode==null && other.getAVSResultCode()==null) || 
             (this.AVSResultCode!=null &&
              this.AVSResultCode.equals(other.getAVSResultCode()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.addressLine1==null && other.getAddressLine1()==null) || 
             (this.addressLine1!=null &&
              this.addressLine1.equals(other.getAddressLine1()))) &&
            ((this.approvalCode==null && other.getApprovalCode()==null) || 
             (this.approvalCode!=null &&
              this.approvalCode.equals(other.getApprovalCode()))) &&
            ((this.authorizerNetwork==null && other.getAuthorizerNetwork()==null) || 
             (this.authorizerNetwork!=null &&
              this.authorizerNetwork.equals(other.getAuthorizerNetwork()))) &&
            ((this.authorizerNetworkResultCode==null && other.getAuthorizerNetworkResultCode()==null) || 
             (this.authorizerNetworkResultCode!=null &&
              this.authorizerNetworkResultCode.equals(other.getAuthorizerNetworkResultCode()))) &&
            ((this.balanceAmountCashBenefit==null && other.getBalanceAmountCashBenefit()==null) || 
             (this.balanceAmountCashBenefit!=null &&
              this.balanceAmountCashBenefit.equals(other.getBalanceAmountCashBenefit()))) &&
            ((this.balanceAmountFoodStamp==null && other.getBalanceAmountFoodStamp()==null) || 
             (this.balanceAmountFoodStamp!=null &&
              this.balanceAmountFoodStamp.equals(other.getBalanceAmountFoodStamp()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.hostReferenceNumber==null && other.getHostReferenceNumber()==null) || 
             (this.hostReferenceNumber!=null &&
              this.hostReferenceNumber.equals(other.getHostReferenceNumber()))) &&
            ((this.POSConditionCode==null && other.getPOSConditionCode()==null) || 
             (this.POSConditionCode!=null &&
              this.POSConditionCode.equals(other.getPOSConditionCode()))) &&
            ((this.POSPANEntryMode==null && other.getPOSPANEntryMode()==null) || 
             (this.POSPANEntryMode!=null &&
              this.POSPANEntryMode.equals(other.getPOSPANEntryMode()))) &&
            ((this.POSPINEntryMode==null && other.getPOSPINEntryMode()==null) || 
             (this.POSPINEntryMode!=null &&
              this.POSPINEntryMode.equals(other.getPOSPINEntryMode()))) &&
            ((this.POSSecurityCondition==null && other.getPOSSecurityCondition()==null) || 
             (this.POSSecurityCondition!=null &&
              this.POSSecurityCondition.equals(other.getPOSSecurityCondition()))) &&
            ((this.POSTerminalType==null && other.getPOSTerminalType()==null) || 
             (this.POSTerminalType!=null &&
              this.POSTerminalType.equals(other.getPOSTerminalType()))) &&
            ((this.responseCode==null && other.getResponseCode()==null) || 
             (this.responseCode!=null &&
              this.responseCode.equals(other.getResponseCode()))) &&
            ((this.responseMessage==null && other.getResponseMessage()==null) || 
             (this.responseMessage!=null &&
              this.responseMessage.equals(other.getResponseMessage()))) &&
            ((this.settlementDate==null && other.getSettlementDate()==null) || 
             (this.settlementDate!=null &&
              this.settlementDate.equals(other.getSettlementDate()))) &&
            ((this.surchargeAmount==null && other.getSurchargeAmount()==null) || 
             (this.surchargeAmount!=null &&
              this.surchargeAmount.equals(other.getSurchargeAmount()))) &&
            ((this.systemTraceNumber==null && other.getSystemTraceNumber()==null) || 
             (this.systemTraceNumber!=null &&
              this.systemTraceNumber.equals(other.getSystemTraceNumber()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.track2==null && other.getTrack2()==null) || 
             (this.track2!=null &&
              this.track2.equals(other.getTrack2()))) &&
            ((this.transactionAmount==null && other.getTransactionAmount()==null) || 
             (this.transactionAmount!=null &&
              this.transactionAmount.equals(other.getTransactionAmount()))) &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.transactionTime==null && other.getTransactionTime()==null) || 
             (this.transactionTime!=null &&
              this.transactionTime.equals(other.getTransactionTime()))) &&
            ((this.transmissionDateTime==null && other.getTransmissionDateTime()==null) || 
             (this.transmissionDateTime!=null &&
              this.transmissionDateTime.equals(other.getTransmissionDateTime()))) &&
            ((this.tripleDESPseudoTID==null && other.getTripleDESPseudoTID()==null) || 
             (this.tripleDESPseudoTID!=null &&
              this.tripleDESPseudoTID.equals(other.getTripleDESPseudoTID()))) &&
            ((this.ZIPCode==null && other.getZIPCode()==null) || 
             (this.ZIPCode!=null &&
              this.ZIPCode.equals(other.getZIPCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAVSResultCode() != null) {
            _hashCode += getAVSResultCode().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getAddressLine1() != null) {
            _hashCode += getAddressLine1().hashCode();
        }
        if (getApprovalCode() != null) {
            _hashCode += getApprovalCode().hashCode();
        }
        if (getAuthorizerNetwork() != null) {
            _hashCode += getAuthorizerNetwork().hashCode();
        }
        if (getAuthorizerNetworkResultCode() != null) {
            _hashCode += getAuthorizerNetworkResultCode().hashCode();
        }
        if (getBalanceAmountCashBenefit() != null) {
            _hashCode += getBalanceAmountCashBenefit().hashCode();
        }
        if (getBalanceAmountFoodStamp() != null) {
            _hashCode += getBalanceAmountFoodStamp().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getHostReferenceNumber() != null) {
            _hashCode += getHostReferenceNumber().hashCode();
        }
        if (getPOSConditionCode() != null) {
            _hashCode += getPOSConditionCode().hashCode();
        }
        if (getPOSPANEntryMode() != null) {
            _hashCode += getPOSPANEntryMode().hashCode();
        }
        if (getPOSPINEntryMode() != null) {
            _hashCode += getPOSPINEntryMode().hashCode();
        }
        if (getPOSSecurityCondition() != null) {
            _hashCode += getPOSSecurityCondition().hashCode();
        }
        if (getPOSTerminalType() != null) {
            _hashCode += getPOSTerminalType().hashCode();
        }
        if (getResponseCode() != null) {
            _hashCode += getResponseCode().hashCode();
        }
        if (getResponseMessage() != null) {
            _hashCode += getResponseMessage().hashCode();
        }
        if (getSettlementDate() != null) {
            _hashCode += getSettlementDate().hashCode();
        }
        if (getSurchargeAmount() != null) {
            _hashCode += getSurchargeAmount().hashCode();
        }
        if (getSystemTraceNumber() != null) {
            _hashCode += getSystemTraceNumber().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getTrack2() != null) {
            _hashCode += getTrack2().hashCode();
        }
        if (getTransactionAmount() != null) {
            _hashCode += getTransactionAmount().hashCode();
        }
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getTransactionTime() != null) {
            _hashCode += getTransactionTime().hashCode();
        }
        if (getTransmissionDateTime() != null) {
            _hashCode += getTransmissionDateTime().hashCode();
        }
        if (getTripleDESPseudoTID() != null) {
            _hashCode += getTripleDESPseudoTID().hashCode();
        }
        if (getZIPCode() != null) {
            _hashCode += getZIPCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FoodStampChargeResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampChargeResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSResultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AVSResultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AVSResultCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AccountNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressLine1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AddressLine1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AddressLine1Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ApprovalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ApprovalCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizerNetwork");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AuthorizerNetwork"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AuthorizerNetworkType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizerNetworkResultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AuthorizerNetworkResultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AuthorizerNetworkResultCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceAmountCashBenefit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "BalanceAmountCashBenefit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceAmountFoodStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "BalanceAmountFoodStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ExpirationDateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostReferenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "HostReferenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "RetrievalReferenceNumber"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSConditionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSPANEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPANEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPANEntryModeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSPINEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPINEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPINEntryModeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSSecurityCondition");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSSecurityCondition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSSecurityConditionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTerminalType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalTypeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseCodeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AdditionalResponseDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("settlementDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SettlementDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionDateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SurchargeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SurchargeAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemTraceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceAuditNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TaxAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("track2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track2Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionDateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionTimeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTimeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tripleDESPseudoTID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TripleDESPseudoTID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TripleDESPseudoTIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIPCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ZIPCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ZIPCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
