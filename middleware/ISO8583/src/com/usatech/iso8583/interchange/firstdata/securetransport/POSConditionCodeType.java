/**
 * POSConditionCodeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class POSConditionCodeType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected POSConditionCodeType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "customer-card-present";
    public static final java.lang.String _value2 = "customer-present-card-not-present";
    public static final java.lang.String _value3 = "customer-not-present-card-present";
    public static final java.lang.String _value4 = "customer-card-not-present";
    public static final java.lang.String _value5 = "mail-phone-order";
    public static final java.lang.String _value6 = "preauthorized-purchase";
    public static final java.lang.String _value7 = "recurring-payment";
    public static final POSConditionCodeType value1 = new POSConditionCodeType(_value1);
    public static final POSConditionCodeType value2 = new POSConditionCodeType(_value2);
    public static final POSConditionCodeType value3 = new POSConditionCodeType(_value3);
    public static final POSConditionCodeType value4 = new POSConditionCodeType(_value4);
    public static final POSConditionCodeType value5 = new POSConditionCodeType(_value5);
    public static final POSConditionCodeType value6 = new POSConditionCodeType(_value6);
    public static final POSConditionCodeType value7 = new POSConditionCodeType(_value7);
    public java.lang.String getValue() { return _value_;}
    public static POSConditionCodeType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        POSConditionCodeType enumeration = (POSConditionCodeType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static POSConditionCodeType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(POSConditionCodeType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCodeType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
