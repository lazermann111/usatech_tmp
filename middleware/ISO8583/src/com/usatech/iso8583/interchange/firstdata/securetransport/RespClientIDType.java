/**
 * RespClientIDType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class RespClientIDType  implements java.io.Serializable {
    private java.lang.String DID;

    private java.lang.String clientRef;

    public RespClientIDType() {
    }

    public RespClientIDType(
           java.lang.String DID,
           java.lang.String clientRef) {
           this.DID = DID;
           this.clientRef = clientRef;
    }


    /**
     * Gets the DID value for this RespClientIDType.
     * 
     * @return DID
     */
    public java.lang.String getDID() {
        return DID;
    }


    /**
     * Sets the DID value for this RespClientIDType.
     * 
     * @param DID
     */
    public void setDID(java.lang.String DID) {
        this.DID = DID;
    }


    /**
     * Gets the clientRef value for this RespClientIDType.
     * 
     * @return clientRef
     */
    public java.lang.String getClientRef() {
        return clientRef;
    }


    /**
     * Sets the clientRef value for this RespClientIDType.
     * 
     * @param clientRef
     */
    public void setClientRef(java.lang.String clientRef) {
        this.clientRef = clientRef;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespClientIDType)) return false;
        RespClientIDType other = (RespClientIDType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DID==null && other.getDID()==null) || 
             (this.DID!=null &&
              this.DID.equals(other.getDID()))) &&
            ((this.clientRef==null && other.getClientRef()==null) || 
             (this.clientRef!=null &&
              this.clientRef.equals(other.getClientRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDID() != null) {
            _hashCode += getDID().hashCode();
        }
        if (getClientRef() != null) {
            _hashCode += getClientRef().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespClientIDType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "RespClientIDType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ClientRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
