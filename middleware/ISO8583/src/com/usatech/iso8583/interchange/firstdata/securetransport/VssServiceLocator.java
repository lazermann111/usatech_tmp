/**
 * VssServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class VssServiceLocator extends org.apache.axis.client.Service implements com.usatech.iso8583.interchange.firstdata.securetransport.VssService {

    public VssServiceLocator() {
    }


    public VssServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public VssServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for vssServicePort
    private java.lang.String vssServicePort_address = "https://stg.dw.us.fdcnet.biz/vs/services/vxnservice";

    public java.lang.String getvssServicePortAddress() {
        return vssServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String vssServicePortWSDDServiceName = "vssServicePort";

    public java.lang.String getvssServicePortWSDDServiceName() {
        return vssServicePortWSDDServiceName;
    }

    public void setvssServicePortWSDDServiceName(java.lang.String name) {
        vssServicePortWSDDServiceName = name;
    }

    public com.usatech.iso8583.interchange.firstdata.securetransport.VssPortType getvssServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(vssServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getvssServicePort(endpoint);
    }

    public com.usatech.iso8583.interchange.firstdata.securetransport.VssPortType getvssServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.iso8583.interchange.firstdata.securetransport.VssSoapBindingStub _stub = new com.usatech.iso8583.interchange.firstdata.securetransport.VssSoapBindingStub(portAddress, this);
            _stub.setPortName(getvssServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setvssServicePortEndpointAddress(java.lang.String address) {
        vssServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.iso8583.interchange.firstdata.securetransport.VssPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.iso8583.interchange.firstdata.securetransport.VssSoapBindingStub _stub = new com.usatech.iso8583.interchange.firstdata.securetransport.VssSoapBindingStub(new java.net.URL(vssServicePort_address), this);
                _stub.setPortName(getvssServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("vssServicePort".equals(inputPortName)) {
            return getvssServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnservice.wsdl", "vssService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnservice.wsdl", "vssServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("vssServicePort".equals(portName)) {
            setvssServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
