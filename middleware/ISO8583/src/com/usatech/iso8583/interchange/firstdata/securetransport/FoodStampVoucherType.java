/**
 * FoodStampVoucherType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class FoodStampVoucherType  implements java.io.Serializable {
    private java.math.BigInteger accountNumber;

    private java.lang.String approvalCode;

    private java.lang.String expirationDate;

    private java.lang.String hostReferenceNumber;

    private java.lang.String invoiceNumber;

    private java.lang.String keySerialNumber;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType;

    private java.math.BigDecimal surchargeAmount;

    private java.math.BigInteger systemTraceNumber;

    private java.math.BigDecimal taxAmount;

    private java.lang.String track1;

    private java.lang.String track2;

    private java.math.BigDecimal transactionAmount;

    private java.lang.String transactionDate;

    private java.lang.String transactionTime;

    private java.lang.String transmissionDateTime;

    private java.lang.String tripleDESPseudoTID;

    private java.lang.String voucherNumber;

    private java.lang.String TPPID;

    public FoodStampVoucherType() {
    }

    public FoodStampVoucherType(
           java.math.BigInteger accountNumber,
           java.lang.String approvalCode,
           java.lang.String expirationDate,
           java.lang.String hostReferenceNumber,
           java.lang.String invoiceNumber,
           java.lang.String keySerialNumber,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType,
           java.math.BigDecimal surchargeAmount,
           java.math.BigInteger systemTraceNumber,
           java.math.BigDecimal taxAmount,
           java.lang.String track1,
           java.lang.String track2,
           java.math.BigDecimal transactionAmount,
           java.lang.String transactionDate,
           java.lang.String transactionTime,
           java.lang.String transmissionDateTime,
           java.lang.String tripleDESPseudoTID,
           java.lang.String voucherNumber,
           java.lang.String TPPID) {
           this.accountNumber = accountNumber;
           this.approvalCode = approvalCode;
           this.expirationDate = expirationDate;
           this.hostReferenceNumber = hostReferenceNumber;
           this.invoiceNumber = invoiceNumber;
           this.keySerialNumber = keySerialNumber;
           this.POSConditionCode = POSConditionCode;
           this.POSPANEntryMode = POSPANEntryMode;
           this.POSPINEntryMode = POSPINEntryMode;
           this.POSSecurityCondition = POSSecurityCondition;
           this.POSTerminalType = POSTerminalType;
           this.surchargeAmount = surchargeAmount;
           this.systemTraceNumber = systemTraceNumber;
           this.taxAmount = taxAmount;
           this.track1 = track1;
           this.track2 = track2;
           this.transactionAmount = transactionAmount;
           this.transactionDate = transactionDate;
           this.transactionTime = transactionTime;
           this.transmissionDateTime = transmissionDateTime;
           this.tripleDESPseudoTID = tripleDESPseudoTID;
           this.voucherNumber = voucherNumber;
           this.TPPID = TPPID;
    }


    /**
     * Gets the accountNumber value for this FoodStampVoucherType.
     * 
     * @return accountNumber
     */
    public java.math.BigInteger getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this FoodStampVoucherType.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.math.BigInteger accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the approvalCode value for this FoodStampVoucherType.
     * 
     * @return approvalCode
     */
    public java.lang.String getApprovalCode() {
        return approvalCode;
    }


    /**
     * Sets the approvalCode value for this FoodStampVoucherType.
     * 
     * @param approvalCode
     */
    public void setApprovalCode(java.lang.String approvalCode) {
        this.approvalCode = approvalCode;
    }


    /**
     * Gets the expirationDate value for this FoodStampVoucherType.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this FoodStampVoucherType.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the hostReferenceNumber value for this FoodStampVoucherType.
     * 
     * @return hostReferenceNumber
     */
    public java.lang.String getHostReferenceNumber() {
        return hostReferenceNumber;
    }


    /**
     * Sets the hostReferenceNumber value for this FoodStampVoucherType.
     * 
     * @param hostReferenceNumber
     */
    public void setHostReferenceNumber(java.lang.String hostReferenceNumber) {
        this.hostReferenceNumber = hostReferenceNumber;
    }


    /**
     * Gets the invoiceNumber value for this FoodStampVoucherType.
     * 
     * @return invoiceNumber
     */
    public java.lang.String getInvoiceNumber() {
        return invoiceNumber;
    }


    /**
     * Sets the invoiceNumber value for this FoodStampVoucherType.
     * 
     * @param invoiceNumber
     */
    public void setInvoiceNumber(java.lang.String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    /**
     * Gets the keySerialNumber value for this FoodStampVoucherType.
     * 
     * @return keySerialNumber
     */
    public java.lang.String getKeySerialNumber() {
        return keySerialNumber;
    }


    /**
     * Sets the keySerialNumber value for this FoodStampVoucherType.
     * 
     * @param keySerialNumber
     */
    public void setKeySerialNumber(java.lang.String keySerialNumber) {
        this.keySerialNumber = keySerialNumber;
    }


    /**
     * Gets the POSConditionCode value for this FoodStampVoucherType.
     * 
     * @return POSConditionCode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType getPOSConditionCode() {
        return POSConditionCode;
    }


    /**
     * Sets the POSConditionCode value for this FoodStampVoucherType.
     * 
     * @param POSConditionCode
     */
    public void setPOSConditionCode(com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode) {
        this.POSConditionCode = POSConditionCode;
    }


    /**
     * Gets the POSPANEntryMode value for this FoodStampVoucherType.
     * 
     * @return POSPANEntryMode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType getPOSPANEntryMode() {
        return POSPANEntryMode;
    }


    /**
     * Sets the POSPANEntryMode value for this FoodStampVoucherType.
     * 
     * @param POSPANEntryMode
     */
    public void setPOSPANEntryMode(com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode) {
        this.POSPANEntryMode = POSPANEntryMode;
    }


    /**
     * Gets the POSPINEntryMode value for this FoodStampVoucherType.
     * 
     * @return POSPINEntryMode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType getPOSPINEntryMode() {
        return POSPINEntryMode;
    }


    /**
     * Sets the POSPINEntryMode value for this FoodStampVoucherType.
     * 
     * @param POSPINEntryMode
     */
    public void setPOSPINEntryMode(com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode) {
        this.POSPINEntryMode = POSPINEntryMode;
    }


    /**
     * Gets the POSSecurityCondition value for this FoodStampVoucherType.
     * 
     * @return POSSecurityCondition
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType getPOSSecurityCondition() {
        return POSSecurityCondition;
    }


    /**
     * Sets the POSSecurityCondition value for this FoodStampVoucherType.
     * 
     * @param POSSecurityCondition
     */
    public void setPOSSecurityCondition(com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition) {
        this.POSSecurityCondition = POSSecurityCondition;
    }


    /**
     * Gets the POSTerminalType value for this FoodStampVoucherType.
     * 
     * @return POSTerminalType
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType getPOSTerminalType() {
        return POSTerminalType;
    }


    /**
     * Sets the POSTerminalType value for this FoodStampVoucherType.
     * 
     * @param POSTerminalType
     */
    public void setPOSTerminalType(com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType) {
        this.POSTerminalType = POSTerminalType;
    }


    /**
     * Gets the surchargeAmount value for this FoodStampVoucherType.
     * 
     * @return surchargeAmount
     */
    public java.math.BigDecimal getSurchargeAmount() {
        return surchargeAmount;
    }


    /**
     * Sets the surchargeAmount value for this FoodStampVoucherType.
     * 
     * @param surchargeAmount
     */
    public void setSurchargeAmount(java.math.BigDecimal surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }


    /**
     * Gets the systemTraceNumber value for this FoodStampVoucherType.
     * 
     * @return systemTraceNumber
     */
    public java.math.BigInteger getSystemTraceNumber() {
        return systemTraceNumber;
    }


    /**
     * Sets the systemTraceNumber value for this FoodStampVoucherType.
     * 
     * @param systemTraceNumber
     */
    public void setSystemTraceNumber(java.math.BigInteger systemTraceNumber) {
        this.systemTraceNumber = systemTraceNumber;
    }


    /**
     * Gets the taxAmount value for this FoodStampVoucherType.
     * 
     * @return taxAmount
     */
    public java.math.BigDecimal getTaxAmount() {
        return taxAmount;
    }


    /**
     * Sets the taxAmount value for this FoodStampVoucherType.
     * 
     * @param taxAmount
     */
    public void setTaxAmount(java.math.BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }


    /**
     * Gets the track1 value for this FoodStampVoucherType.
     * 
     * @return track1
     */
    public java.lang.String getTrack1() {
        return track1;
    }


    /**
     * Sets the track1 value for this FoodStampVoucherType.
     * 
     * @param track1
     */
    public void setTrack1(java.lang.String track1) {
        this.track1 = track1;
    }


    /**
     * Gets the track2 value for this FoodStampVoucherType.
     * 
     * @return track2
     */
    public java.lang.String getTrack2() {
        return track2;
    }


    /**
     * Sets the track2 value for this FoodStampVoucherType.
     * 
     * @param track2
     */
    public void setTrack2(java.lang.String track2) {
        this.track2 = track2;
    }


    /**
     * Gets the transactionAmount value for this FoodStampVoucherType.
     * 
     * @return transactionAmount
     */
    public java.math.BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    /**
     * Sets the transactionAmount value for this FoodStampVoucherType.
     * 
     * @param transactionAmount
     */
    public void setTransactionAmount(java.math.BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    /**
     * Gets the transactionDate value for this FoodStampVoucherType.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this FoodStampVoucherType.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the transactionTime value for this FoodStampVoucherType.
     * 
     * @return transactionTime
     */
    public java.lang.String getTransactionTime() {
        return transactionTime;
    }


    /**
     * Sets the transactionTime value for this FoodStampVoucherType.
     * 
     * @param transactionTime
     */
    public void setTransactionTime(java.lang.String transactionTime) {
        this.transactionTime = transactionTime;
    }


    /**
     * Gets the transmissionDateTime value for this FoodStampVoucherType.
     * 
     * @return transmissionDateTime
     */
    public java.lang.String getTransmissionDateTime() {
        return transmissionDateTime;
    }


    /**
     * Sets the transmissionDateTime value for this FoodStampVoucherType.
     * 
     * @param transmissionDateTime
     */
    public void setTransmissionDateTime(java.lang.String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }


    /**
     * Gets the tripleDESPseudoTID value for this FoodStampVoucherType.
     * 
     * @return tripleDESPseudoTID
     */
    public java.lang.String getTripleDESPseudoTID() {
        return tripleDESPseudoTID;
    }


    /**
     * Sets the tripleDESPseudoTID value for this FoodStampVoucherType.
     * 
     * @param tripleDESPseudoTID
     */
    public void setTripleDESPseudoTID(java.lang.String tripleDESPseudoTID) {
        this.tripleDESPseudoTID = tripleDESPseudoTID;
    }


    /**
     * Gets the voucherNumber value for this FoodStampVoucherType.
     * 
     * @return voucherNumber
     */
    public java.lang.String getVoucherNumber() {
        return voucherNumber;
    }


    /**
     * Sets the voucherNumber value for this FoodStampVoucherType.
     * 
     * @param voucherNumber
     */
    public void setVoucherNumber(java.lang.String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }


    /**
     * Gets the TPPID value for this FoodStampVoucherType.
     * 
     * @return TPPID
     */
    public java.lang.String getTPPID() {
        return TPPID;
    }


    /**
     * Sets the TPPID value for this FoodStampVoucherType.
     * 
     * @param TPPID
     */
    public void setTPPID(java.lang.String TPPID) {
        this.TPPID = TPPID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FoodStampVoucherType)) return false;
        FoodStampVoucherType other = (FoodStampVoucherType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.approvalCode==null && other.getApprovalCode()==null) || 
             (this.approvalCode!=null &&
              this.approvalCode.equals(other.getApprovalCode()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.hostReferenceNumber==null && other.getHostReferenceNumber()==null) || 
             (this.hostReferenceNumber!=null &&
              this.hostReferenceNumber.equals(other.getHostReferenceNumber()))) &&
            ((this.invoiceNumber==null && other.getInvoiceNumber()==null) || 
             (this.invoiceNumber!=null &&
              this.invoiceNumber.equals(other.getInvoiceNumber()))) &&
            ((this.keySerialNumber==null && other.getKeySerialNumber()==null) || 
             (this.keySerialNumber!=null &&
              this.keySerialNumber.equals(other.getKeySerialNumber()))) &&
            ((this.POSConditionCode==null && other.getPOSConditionCode()==null) || 
             (this.POSConditionCode!=null &&
              this.POSConditionCode.equals(other.getPOSConditionCode()))) &&
            ((this.POSPANEntryMode==null && other.getPOSPANEntryMode()==null) || 
             (this.POSPANEntryMode!=null &&
              this.POSPANEntryMode.equals(other.getPOSPANEntryMode()))) &&
            ((this.POSPINEntryMode==null && other.getPOSPINEntryMode()==null) || 
             (this.POSPINEntryMode!=null &&
              this.POSPINEntryMode.equals(other.getPOSPINEntryMode()))) &&
            ((this.POSSecurityCondition==null && other.getPOSSecurityCondition()==null) || 
             (this.POSSecurityCondition!=null &&
              this.POSSecurityCondition.equals(other.getPOSSecurityCondition()))) &&
            ((this.POSTerminalType==null && other.getPOSTerminalType()==null) || 
             (this.POSTerminalType!=null &&
              this.POSTerminalType.equals(other.getPOSTerminalType()))) &&
            ((this.surchargeAmount==null && other.getSurchargeAmount()==null) || 
             (this.surchargeAmount!=null &&
              this.surchargeAmount.equals(other.getSurchargeAmount()))) &&
            ((this.systemTraceNumber==null && other.getSystemTraceNumber()==null) || 
             (this.systemTraceNumber!=null &&
              this.systemTraceNumber.equals(other.getSystemTraceNumber()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.track1==null && other.getTrack1()==null) || 
             (this.track1!=null &&
              this.track1.equals(other.getTrack1()))) &&
            ((this.track2==null && other.getTrack2()==null) || 
             (this.track2!=null &&
              this.track2.equals(other.getTrack2()))) &&
            ((this.transactionAmount==null && other.getTransactionAmount()==null) || 
             (this.transactionAmount!=null &&
              this.transactionAmount.equals(other.getTransactionAmount()))) &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.transactionTime==null && other.getTransactionTime()==null) || 
             (this.transactionTime!=null &&
              this.transactionTime.equals(other.getTransactionTime()))) &&
            ((this.transmissionDateTime==null && other.getTransmissionDateTime()==null) || 
             (this.transmissionDateTime!=null &&
              this.transmissionDateTime.equals(other.getTransmissionDateTime()))) &&
            ((this.tripleDESPseudoTID==null && other.getTripleDESPseudoTID()==null) || 
             (this.tripleDESPseudoTID!=null &&
              this.tripleDESPseudoTID.equals(other.getTripleDESPseudoTID()))) &&
            ((this.voucherNumber==null && other.getVoucherNumber()==null) || 
             (this.voucherNumber!=null &&
              this.voucherNumber.equals(other.getVoucherNumber()))) &&
            ((this.TPPID==null && other.getTPPID()==null) || 
             (this.TPPID!=null &&
              this.TPPID.equals(other.getTPPID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getApprovalCode() != null) {
            _hashCode += getApprovalCode().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getHostReferenceNumber() != null) {
            _hashCode += getHostReferenceNumber().hashCode();
        }
        if (getInvoiceNumber() != null) {
            _hashCode += getInvoiceNumber().hashCode();
        }
        if (getKeySerialNumber() != null) {
            _hashCode += getKeySerialNumber().hashCode();
        }
        if (getPOSConditionCode() != null) {
            _hashCode += getPOSConditionCode().hashCode();
        }
        if (getPOSPANEntryMode() != null) {
            _hashCode += getPOSPANEntryMode().hashCode();
        }
        if (getPOSPINEntryMode() != null) {
            _hashCode += getPOSPINEntryMode().hashCode();
        }
        if (getPOSSecurityCondition() != null) {
            _hashCode += getPOSSecurityCondition().hashCode();
        }
        if (getPOSTerminalType() != null) {
            _hashCode += getPOSTerminalType().hashCode();
        }
        if (getSurchargeAmount() != null) {
            _hashCode += getSurchargeAmount().hashCode();
        }
        if (getSystemTraceNumber() != null) {
            _hashCode += getSystemTraceNumber().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getTrack1() != null) {
            _hashCode += getTrack1().hashCode();
        }
        if (getTrack2() != null) {
            _hashCode += getTrack2().hashCode();
        }
        if (getTransactionAmount() != null) {
            _hashCode += getTransactionAmount().hashCode();
        }
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getTransactionTime() != null) {
            _hashCode += getTransactionTime().hashCode();
        }
        if (getTransmissionDateTime() != null) {
            _hashCode += getTransmissionDateTime().hashCode();
        }
        if (getTripleDESPseudoTID() != null) {
            _hashCode += getTripleDESPseudoTID().hashCode();
        }
        if (getVoucherNumber() != null) {
            _hashCode += getVoucherNumber().hashCode();
        }
        if (getTPPID() != null) {
            _hashCode += getTPPID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FoodStampVoucherType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampVoucherType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AccountNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ApprovalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ApprovalCodeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ExpirationDateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostReferenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "HostReferenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "RetrievalReferenceNumber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoiceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "InvoiceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "InvoiceNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keySerialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "KeySerialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "KeySerialNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSConditionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCodeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSPANEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPANEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPANEntryModeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSPINEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPINEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPINEntryModeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSSecurityCondition");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSSecurityCondition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSSecurityConditionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTerminalType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalTypeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SurchargeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SurchargeAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemTraceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceAuditNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TaxAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("track1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track1Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("track2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Track2Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmountType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionDateType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionTimeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTimeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tripleDESPseudoTID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TripleDESPseudoTID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TripleDESPseudoTIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucherNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "VoucherNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "VoucherNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TPPID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TPPID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TPPIDType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
