/**
 * TransactionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class TransactionType  implements java.io.Serializable {
    private java.lang.String serviceID;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardAuthorize creditCardAuthorize;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCapture creditCardCapture;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardChargeType creditCardCharge;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCreditType creditCardCredit;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardRefund creditCardRefund;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardSettle creditCardSettle;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TimeoutReversalType creditCardTimeoutReversal;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardVoid creditCardVoid;

    private com.usatech.iso8583.interchange.firstdata.securetransport.Totals totals;

    private com.usatech.iso8583.interchange.firstdata.securetransport.SystemCheck systemCheck;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardAuthorizeType debitCardAuthorize;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeType debitCardCharge;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeType debitCardCredit;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessType debitCardChargePINless;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessType debitCardCreditPINless;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardCaptureType debitCardCapture;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardVoidType debitCardVoid;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardTimeoutReversalType debitCardTimeoutReversal;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardAuthorizeType giftCardAuthorize;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardActivateType giftCardActivate;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceInquiryType giftCardBalanceInquiry;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceIncreaseType giftCardBalanceIncrease;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardDeactivateType giftCardDeactivate;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardChargeType giftCardCharge;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditType giftCardCredit;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditType giftCardRefund;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureType giftCardCapture;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardVoidType giftCardVoid;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardTimeoutReversalType giftCardTimeoutReversal;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckRiskManagementType teleCheckRiskManagement;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAChargeType teleCheckECACharge;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAVoidType teleCheckECAVoid;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECATimeoutReversalType teleCheckECATimeoutReversal;

    private com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeType foodStampCharge;

    private com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeType foodStampCredit;

    private com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampVoucherType foodStampVoucher;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryType foodStampBalanceInquiry;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitChargeType cashBenefitCharge;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryType cashBenefitBalanceInquiry;

    private java.lang.String payloadFormat;  // attribute

    public TransactionType() {
    }

    public TransactionType(
           java.lang.String serviceID,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardAuthorize creditCardAuthorize,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCapture creditCardCapture,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardChargeType creditCardCharge,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCreditType creditCardCredit,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardRefund creditCardRefund,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardSettle creditCardSettle,
           com.usatech.iso8583.interchange.firstdata.securetransport.TimeoutReversalType creditCardTimeoutReversal,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardVoid creditCardVoid,
           com.usatech.iso8583.interchange.firstdata.securetransport.Totals totals,
           com.usatech.iso8583.interchange.firstdata.securetransport.SystemCheck systemCheck,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardAuthorizeType debitCardAuthorize,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeType debitCardCharge,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeType debitCardCredit,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessType debitCardChargePINless,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessType debitCardCreditPINless,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardCaptureType debitCardCapture,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardVoidType debitCardVoid,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardTimeoutReversalType debitCardTimeoutReversal,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardAuthorizeType giftCardAuthorize,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardActivateType giftCardActivate,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceInquiryType giftCardBalanceInquiry,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceIncreaseType giftCardBalanceIncrease,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardDeactivateType giftCardDeactivate,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardChargeType giftCardCharge,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditType giftCardCredit,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditType giftCardRefund,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureType giftCardCapture,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardVoidType giftCardVoid,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardTimeoutReversalType giftCardTimeoutReversal,
           com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckRiskManagementType teleCheckRiskManagement,
           com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAChargeType teleCheckECACharge,
           com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAVoidType teleCheckECAVoid,
           com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECATimeoutReversalType teleCheckECATimeoutReversal,
           com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeType foodStampCharge,
           com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeType foodStampCredit,
           com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampVoucherType foodStampVoucher,
           com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryType foodStampBalanceInquiry,
           com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitChargeType cashBenefitCharge,
           com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryType cashBenefitBalanceInquiry,
           java.lang.String payloadFormat) {
           this.serviceID = serviceID;
           this.creditCardAuthorize = creditCardAuthorize;
           this.creditCardCapture = creditCardCapture;
           this.creditCardCharge = creditCardCharge;
           this.creditCardCredit = creditCardCredit;
           this.creditCardRefund = creditCardRefund;
           this.creditCardSettle = creditCardSettle;
           this.creditCardTimeoutReversal = creditCardTimeoutReversal;
           this.creditCardVoid = creditCardVoid;
           this.totals = totals;
           this.systemCheck = systemCheck;
           this.debitCardAuthorize = debitCardAuthorize;
           this.debitCardCharge = debitCardCharge;
           this.debitCardCredit = debitCardCredit;
           this.debitCardChargePINless = debitCardChargePINless;
           this.debitCardCreditPINless = debitCardCreditPINless;
           this.debitCardCapture = debitCardCapture;
           this.debitCardVoid = debitCardVoid;
           this.debitCardTimeoutReversal = debitCardTimeoutReversal;
           this.giftCardAuthorize = giftCardAuthorize;
           this.giftCardActivate = giftCardActivate;
           this.giftCardBalanceInquiry = giftCardBalanceInquiry;
           this.giftCardBalanceIncrease = giftCardBalanceIncrease;
           this.giftCardDeactivate = giftCardDeactivate;
           this.giftCardCharge = giftCardCharge;
           this.giftCardCredit = giftCardCredit;
           this.giftCardRefund = giftCardRefund;
           this.giftCardCapture = giftCardCapture;
           this.giftCardVoid = giftCardVoid;
           this.giftCardTimeoutReversal = giftCardTimeoutReversal;
           this.teleCheckRiskManagement = teleCheckRiskManagement;
           this.teleCheckECACharge = teleCheckECACharge;
           this.teleCheckECAVoid = teleCheckECAVoid;
           this.teleCheckECATimeoutReversal = teleCheckECATimeoutReversal;
           this.foodStampCharge = foodStampCharge;
           this.foodStampCredit = foodStampCredit;
           this.foodStampVoucher = foodStampVoucher;
           this.foodStampBalanceInquiry = foodStampBalanceInquiry;
           this.cashBenefitCharge = cashBenefitCharge;
           this.cashBenefitBalanceInquiry = cashBenefitBalanceInquiry;
           this.payloadFormat = payloadFormat;
    }


    /**
     * Gets the serviceID value for this TransactionType.
     * 
     * @return serviceID
     */
    public java.lang.String getServiceID() {
        return serviceID;
    }


    /**
     * Sets the serviceID value for this TransactionType.
     * 
     * @param serviceID
     */
    public void setServiceID(java.lang.String serviceID) {
        this.serviceID = serviceID;
    }


    /**
     * Gets the creditCardAuthorize value for this TransactionType.
     * 
     * @return creditCardAuthorize
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardAuthorize getCreditCardAuthorize() {
        return creditCardAuthorize;
    }


    /**
     * Sets the creditCardAuthorize value for this TransactionType.
     * 
     * @param creditCardAuthorize
     */
    public void setCreditCardAuthorize(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardAuthorize creditCardAuthorize) {
        this.creditCardAuthorize = creditCardAuthorize;
    }


    /**
     * Gets the creditCardCapture value for this TransactionType.
     * 
     * @return creditCardCapture
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCapture getCreditCardCapture() {
        return creditCardCapture;
    }


    /**
     * Sets the creditCardCapture value for this TransactionType.
     * 
     * @param creditCardCapture
     */
    public void setCreditCardCapture(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCapture creditCardCapture) {
        this.creditCardCapture = creditCardCapture;
    }


    /**
     * Gets the creditCardCharge value for this TransactionType.
     * 
     * @return creditCardCharge
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardChargeType getCreditCardCharge() {
        return creditCardCharge;
    }


    /**
     * Sets the creditCardCharge value for this TransactionType.
     * 
     * @param creditCardCharge
     */
    public void setCreditCardCharge(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardChargeType creditCardCharge) {
        this.creditCardCharge = creditCardCharge;
    }


    /**
     * Gets the creditCardCredit value for this TransactionType.
     * 
     * @return creditCardCredit
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCreditType getCreditCardCredit() {
        return creditCardCredit;
    }


    /**
     * Sets the creditCardCredit value for this TransactionType.
     * 
     * @param creditCardCredit
     */
    public void setCreditCardCredit(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCreditType creditCardCredit) {
        this.creditCardCredit = creditCardCredit;
    }


    /**
     * Gets the creditCardRefund value for this TransactionType.
     * 
     * @return creditCardRefund
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardRefund getCreditCardRefund() {
        return creditCardRefund;
    }


    /**
     * Sets the creditCardRefund value for this TransactionType.
     * 
     * @param creditCardRefund
     */
    public void setCreditCardRefund(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardRefund creditCardRefund) {
        this.creditCardRefund = creditCardRefund;
    }


    /**
     * Gets the creditCardSettle value for this TransactionType.
     * 
     * @return creditCardSettle
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardSettle getCreditCardSettle() {
        return creditCardSettle;
    }


    /**
     * Sets the creditCardSettle value for this TransactionType.
     * 
     * @param creditCardSettle
     */
    public void setCreditCardSettle(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardSettle creditCardSettle) {
        this.creditCardSettle = creditCardSettle;
    }


    /**
     * Gets the creditCardTimeoutReversal value for this TransactionType.
     * 
     * @return creditCardTimeoutReversal
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TimeoutReversalType getCreditCardTimeoutReversal() {
        return creditCardTimeoutReversal;
    }


    /**
     * Sets the creditCardTimeoutReversal value for this TransactionType.
     * 
     * @param creditCardTimeoutReversal
     */
    public void setCreditCardTimeoutReversal(com.usatech.iso8583.interchange.firstdata.securetransport.TimeoutReversalType creditCardTimeoutReversal) {
        this.creditCardTimeoutReversal = creditCardTimeoutReversal;
    }


    /**
     * Gets the creditCardVoid value for this TransactionType.
     * 
     * @return creditCardVoid
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardVoid getCreditCardVoid() {
        return creditCardVoid;
    }


    /**
     * Sets the creditCardVoid value for this TransactionType.
     * 
     * @param creditCardVoid
     */
    public void setCreditCardVoid(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardVoid creditCardVoid) {
        this.creditCardVoid = creditCardVoid;
    }


    /**
     * Gets the totals value for this TransactionType.
     * 
     * @return totals
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.Totals getTotals() {
        return totals;
    }


    /**
     * Sets the totals value for this TransactionType.
     * 
     * @param totals
     */
    public void setTotals(com.usatech.iso8583.interchange.firstdata.securetransport.Totals totals) {
        this.totals = totals;
    }


    /**
     * Gets the systemCheck value for this TransactionType.
     * 
     * @return systemCheck
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.SystemCheck getSystemCheck() {
        return systemCheck;
    }


    /**
     * Sets the systemCheck value for this TransactionType.
     * 
     * @param systemCheck
     */
    public void setSystemCheck(com.usatech.iso8583.interchange.firstdata.securetransport.SystemCheck systemCheck) {
        this.systemCheck = systemCheck;
    }


    /**
     * Gets the debitCardAuthorize value for this TransactionType.
     * 
     * @return debitCardAuthorize
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardAuthorizeType getDebitCardAuthorize() {
        return debitCardAuthorize;
    }


    /**
     * Sets the debitCardAuthorize value for this TransactionType.
     * 
     * @param debitCardAuthorize
     */
    public void setDebitCardAuthorize(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardAuthorizeType debitCardAuthorize) {
        this.debitCardAuthorize = debitCardAuthorize;
    }


    /**
     * Gets the debitCardCharge value for this TransactionType.
     * 
     * @return debitCardCharge
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeType getDebitCardCharge() {
        return debitCardCharge;
    }


    /**
     * Sets the debitCardCharge value for this TransactionType.
     * 
     * @param debitCardCharge
     */
    public void setDebitCardCharge(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeType debitCardCharge) {
        this.debitCardCharge = debitCardCharge;
    }


    /**
     * Gets the debitCardCredit value for this TransactionType.
     * 
     * @return debitCardCredit
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeType getDebitCardCredit() {
        return debitCardCredit;
    }


    /**
     * Sets the debitCardCredit value for this TransactionType.
     * 
     * @param debitCardCredit
     */
    public void setDebitCardCredit(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeType debitCardCredit) {
        this.debitCardCredit = debitCardCredit;
    }


    /**
     * Gets the debitCardChargePINless value for this TransactionType.
     * 
     * @return debitCardChargePINless
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessType getDebitCardChargePINless() {
        return debitCardChargePINless;
    }


    /**
     * Sets the debitCardChargePINless value for this TransactionType.
     * 
     * @param debitCardChargePINless
     */
    public void setDebitCardChargePINless(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessType debitCardChargePINless) {
        this.debitCardChargePINless = debitCardChargePINless;
    }


    /**
     * Gets the debitCardCreditPINless value for this TransactionType.
     * 
     * @return debitCardCreditPINless
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessType getDebitCardCreditPINless() {
        return debitCardCreditPINless;
    }


    /**
     * Sets the debitCardCreditPINless value for this TransactionType.
     * 
     * @param debitCardCreditPINless
     */
    public void setDebitCardCreditPINless(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessType debitCardCreditPINless) {
        this.debitCardCreditPINless = debitCardCreditPINless;
    }


    /**
     * Gets the debitCardCapture value for this TransactionType.
     * 
     * @return debitCardCapture
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardCaptureType getDebitCardCapture() {
        return debitCardCapture;
    }


    /**
     * Sets the debitCardCapture value for this TransactionType.
     * 
     * @param debitCardCapture
     */
    public void setDebitCardCapture(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardCaptureType debitCardCapture) {
        this.debitCardCapture = debitCardCapture;
    }


    /**
     * Gets the debitCardVoid value for this TransactionType.
     * 
     * @return debitCardVoid
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardVoidType getDebitCardVoid() {
        return debitCardVoid;
    }


    /**
     * Sets the debitCardVoid value for this TransactionType.
     * 
     * @param debitCardVoid
     */
    public void setDebitCardVoid(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardVoidType debitCardVoid) {
        this.debitCardVoid = debitCardVoid;
    }


    /**
     * Gets the debitCardTimeoutReversal value for this TransactionType.
     * 
     * @return debitCardTimeoutReversal
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardTimeoutReversalType getDebitCardTimeoutReversal() {
        return debitCardTimeoutReversal;
    }


    /**
     * Sets the debitCardTimeoutReversal value for this TransactionType.
     * 
     * @param debitCardTimeoutReversal
     */
    public void setDebitCardTimeoutReversal(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardTimeoutReversalType debitCardTimeoutReversal) {
        this.debitCardTimeoutReversal = debitCardTimeoutReversal;
    }


    /**
     * Gets the giftCardAuthorize value for this TransactionType.
     * 
     * @return giftCardAuthorize
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardAuthorizeType getGiftCardAuthorize() {
        return giftCardAuthorize;
    }


    /**
     * Sets the giftCardAuthorize value for this TransactionType.
     * 
     * @param giftCardAuthorize
     */
    public void setGiftCardAuthorize(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardAuthorizeType giftCardAuthorize) {
        this.giftCardAuthorize = giftCardAuthorize;
    }


    /**
     * Gets the giftCardActivate value for this TransactionType.
     * 
     * @return giftCardActivate
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardActivateType getGiftCardActivate() {
        return giftCardActivate;
    }


    /**
     * Sets the giftCardActivate value for this TransactionType.
     * 
     * @param giftCardActivate
     */
    public void setGiftCardActivate(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardActivateType giftCardActivate) {
        this.giftCardActivate = giftCardActivate;
    }


    /**
     * Gets the giftCardBalanceInquiry value for this TransactionType.
     * 
     * @return giftCardBalanceInquiry
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceInquiryType getGiftCardBalanceInquiry() {
        return giftCardBalanceInquiry;
    }


    /**
     * Sets the giftCardBalanceInquiry value for this TransactionType.
     * 
     * @param giftCardBalanceInquiry
     */
    public void setGiftCardBalanceInquiry(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceInquiryType giftCardBalanceInquiry) {
        this.giftCardBalanceInquiry = giftCardBalanceInquiry;
    }


    /**
     * Gets the giftCardBalanceIncrease value for this TransactionType.
     * 
     * @return giftCardBalanceIncrease
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceIncreaseType getGiftCardBalanceIncrease() {
        return giftCardBalanceIncrease;
    }


    /**
     * Sets the giftCardBalanceIncrease value for this TransactionType.
     * 
     * @param giftCardBalanceIncrease
     */
    public void setGiftCardBalanceIncrease(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceIncreaseType giftCardBalanceIncrease) {
        this.giftCardBalanceIncrease = giftCardBalanceIncrease;
    }


    /**
     * Gets the giftCardDeactivate value for this TransactionType.
     * 
     * @return giftCardDeactivate
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardDeactivateType getGiftCardDeactivate() {
        return giftCardDeactivate;
    }


    /**
     * Sets the giftCardDeactivate value for this TransactionType.
     * 
     * @param giftCardDeactivate
     */
    public void setGiftCardDeactivate(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardDeactivateType giftCardDeactivate) {
        this.giftCardDeactivate = giftCardDeactivate;
    }


    /**
     * Gets the giftCardCharge value for this TransactionType.
     * 
     * @return giftCardCharge
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardChargeType getGiftCardCharge() {
        return giftCardCharge;
    }


    /**
     * Sets the giftCardCharge value for this TransactionType.
     * 
     * @param giftCardCharge
     */
    public void setGiftCardCharge(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardChargeType giftCardCharge) {
        this.giftCardCharge = giftCardCharge;
    }


    /**
     * Gets the giftCardCredit value for this TransactionType.
     * 
     * @return giftCardCredit
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditType getGiftCardCredit() {
        return giftCardCredit;
    }


    /**
     * Sets the giftCardCredit value for this TransactionType.
     * 
     * @param giftCardCredit
     */
    public void setGiftCardCredit(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditType giftCardCredit) {
        this.giftCardCredit = giftCardCredit;
    }


    /**
     * Gets the giftCardRefund value for this TransactionType.
     * 
     * @return giftCardRefund
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditType getGiftCardRefund() {
        return giftCardRefund;
    }


    /**
     * Sets the giftCardRefund value for this TransactionType.
     * 
     * @param giftCardRefund
     */
    public void setGiftCardRefund(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditType giftCardRefund) {
        this.giftCardRefund = giftCardRefund;
    }


    /**
     * Gets the giftCardCapture value for this TransactionType.
     * 
     * @return giftCardCapture
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureType getGiftCardCapture() {
        return giftCardCapture;
    }


    /**
     * Sets the giftCardCapture value for this TransactionType.
     * 
     * @param giftCardCapture
     */
    public void setGiftCardCapture(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureType giftCardCapture) {
        this.giftCardCapture = giftCardCapture;
    }


    /**
     * Gets the giftCardVoid value for this TransactionType.
     * 
     * @return giftCardVoid
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardVoidType getGiftCardVoid() {
        return giftCardVoid;
    }


    /**
     * Sets the giftCardVoid value for this TransactionType.
     * 
     * @param giftCardVoid
     */
    public void setGiftCardVoid(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardVoidType giftCardVoid) {
        this.giftCardVoid = giftCardVoid;
    }


    /**
     * Gets the giftCardTimeoutReversal value for this TransactionType.
     * 
     * @return giftCardTimeoutReversal
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardTimeoutReversalType getGiftCardTimeoutReversal() {
        return giftCardTimeoutReversal;
    }


    /**
     * Sets the giftCardTimeoutReversal value for this TransactionType.
     * 
     * @param giftCardTimeoutReversal
     */
    public void setGiftCardTimeoutReversal(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardTimeoutReversalType giftCardTimeoutReversal) {
        this.giftCardTimeoutReversal = giftCardTimeoutReversal;
    }


    /**
     * Gets the teleCheckRiskManagement value for this TransactionType.
     * 
     * @return teleCheckRiskManagement
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckRiskManagementType getTeleCheckRiskManagement() {
        return teleCheckRiskManagement;
    }


    /**
     * Sets the teleCheckRiskManagement value for this TransactionType.
     * 
     * @param teleCheckRiskManagement
     */
    public void setTeleCheckRiskManagement(com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckRiskManagementType teleCheckRiskManagement) {
        this.teleCheckRiskManagement = teleCheckRiskManagement;
    }


    /**
     * Gets the teleCheckECACharge value for this TransactionType.
     * 
     * @return teleCheckECACharge
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAChargeType getTeleCheckECACharge() {
        return teleCheckECACharge;
    }


    /**
     * Sets the teleCheckECACharge value for this TransactionType.
     * 
     * @param teleCheckECACharge
     */
    public void setTeleCheckECACharge(com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAChargeType teleCheckECACharge) {
        this.teleCheckECACharge = teleCheckECACharge;
    }


    /**
     * Gets the teleCheckECAVoid value for this TransactionType.
     * 
     * @return teleCheckECAVoid
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAVoidType getTeleCheckECAVoid() {
        return teleCheckECAVoid;
    }


    /**
     * Sets the teleCheckECAVoid value for this TransactionType.
     * 
     * @param teleCheckECAVoid
     */
    public void setTeleCheckECAVoid(com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAVoidType teleCheckECAVoid) {
        this.teleCheckECAVoid = teleCheckECAVoid;
    }


    /**
     * Gets the teleCheckECATimeoutReversal value for this TransactionType.
     * 
     * @return teleCheckECATimeoutReversal
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECATimeoutReversalType getTeleCheckECATimeoutReversal() {
        return teleCheckECATimeoutReversal;
    }


    /**
     * Sets the teleCheckECATimeoutReversal value for this TransactionType.
     * 
     * @param teleCheckECATimeoutReversal
     */
    public void setTeleCheckECATimeoutReversal(com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECATimeoutReversalType teleCheckECATimeoutReversal) {
        this.teleCheckECATimeoutReversal = teleCheckECATimeoutReversal;
    }


    /**
     * Gets the foodStampCharge value for this TransactionType.
     * 
     * @return foodStampCharge
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeType getFoodStampCharge() {
        return foodStampCharge;
    }


    /**
     * Sets the foodStampCharge value for this TransactionType.
     * 
     * @param foodStampCharge
     */
    public void setFoodStampCharge(com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeType foodStampCharge) {
        this.foodStampCharge = foodStampCharge;
    }


    /**
     * Gets the foodStampCredit value for this TransactionType.
     * 
     * @return foodStampCredit
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeType getFoodStampCredit() {
        return foodStampCredit;
    }


    /**
     * Sets the foodStampCredit value for this TransactionType.
     * 
     * @param foodStampCredit
     */
    public void setFoodStampCredit(com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeType foodStampCredit) {
        this.foodStampCredit = foodStampCredit;
    }


    /**
     * Gets the foodStampVoucher value for this TransactionType.
     * 
     * @return foodStampVoucher
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampVoucherType getFoodStampVoucher() {
        return foodStampVoucher;
    }


    /**
     * Sets the foodStampVoucher value for this TransactionType.
     * 
     * @param foodStampVoucher
     */
    public void setFoodStampVoucher(com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampVoucherType foodStampVoucher) {
        this.foodStampVoucher = foodStampVoucher;
    }


    /**
     * Gets the foodStampBalanceInquiry value for this TransactionType.
     * 
     * @return foodStampBalanceInquiry
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryType getFoodStampBalanceInquiry() {
        return foodStampBalanceInquiry;
    }


    /**
     * Sets the foodStampBalanceInquiry value for this TransactionType.
     * 
     * @param foodStampBalanceInquiry
     */
    public void setFoodStampBalanceInquiry(com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryType foodStampBalanceInquiry) {
        this.foodStampBalanceInquiry = foodStampBalanceInquiry;
    }


    /**
     * Gets the cashBenefitCharge value for this TransactionType.
     * 
     * @return cashBenefitCharge
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitChargeType getCashBenefitCharge() {
        return cashBenefitCharge;
    }


    /**
     * Sets the cashBenefitCharge value for this TransactionType.
     * 
     * @param cashBenefitCharge
     */
    public void setCashBenefitCharge(com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitChargeType cashBenefitCharge) {
        this.cashBenefitCharge = cashBenefitCharge;
    }


    /**
     * Gets the cashBenefitBalanceInquiry value for this TransactionType.
     * 
     * @return cashBenefitBalanceInquiry
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryType getCashBenefitBalanceInquiry() {
        return cashBenefitBalanceInquiry;
    }


    /**
     * Sets the cashBenefitBalanceInquiry value for this TransactionType.
     * 
     * @param cashBenefitBalanceInquiry
     */
    public void setCashBenefitBalanceInquiry(com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryType cashBenefitBalanceInquiry) {
        this.cashBenefitBalanceInquiry = cashBenefitBalanceInquiry;
    }


    /**
     * Gets the payloadFormat value for this TransactionType.
     * 
     * @return payloadFormat
     */
    public java.lang.String getPayloadFormat() {
        return payloadFormat;
    }


    /**
     * Sets the payloadFormat value for this TransactionType.
     * 
     * @param payloadFormat
     */
    public void setPayloadFormat(java.lang.String payloadFormat) {
        this.payloadFormat = payloadFormat;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionType)) return false;
        TransactionType other = (TransactionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.serviceID==null && other.getServiceID()==null) || 
             (this.serviceID!=null &&
              this.serviceID.equals(other.getServiceID()))) &&
            ((this.creditCardAuthorize==null && other.getCreditCardAuthorize()==null) || 
             (this.creditCardAuthorize!=null &&
              this.creditCardAuthorize.equals(other.getCreditCardAuthorize()))) &&
            ((this.creditCardCapture==null && other.getCreditCardCapture()==null) || 
             (this.creditCardCapture!=null &&
              this.creditCardCapture.equals(other.getCreditCardCapture()))) &&
            ((this.creditCardCharge==null && other.getCreditCardCharge()==null) || 
             (this.creditCardCharge!=null &&
              this.creditCardCharge.equals(other.getCreditCardCharge()))) &&
            ((this.creditCardCredit==null && other.getCreditCardCredit()==null) || 
             (this.creditCardCredit!=null &&
              this.creditCardCredit.equals(other.getCreditCardCredit()))) &&
            ((this.creditCardRefund==null && other.getCreditCardRefund()==null) || 
             (this.creditCardRefund!=null &&
              this.creditCardRefund.equals(other.getCreditCardRefund()))) &&
            ((this.creditCardSettle==null && other.getCreditCardSettle()==null) || 
             (this.creditCardSettle!=null &&
              this.creditCardSettle.equals(other.getCreditCardSettle()))) &&
            ((this.creditCardTimeoutReversal==null && other.getCreditCardTimeoutReversal()==null) || 
             (this.creditCardTimeoutReversal!=null &&
              this.creditCardTimeoutReversal.equals(other.getCreditCardTimeoutReversal()))) &&
            ((this.creditCardVoid==null && other.getCreditCardVoid()==null) || 
             (this.creditCardVoid!=null &&
              this.creditCardVoid.equals(other.getCreditCardVoid()))) &&
            ((this.totals==null && other.getTotals()==null) || 
             (this.totals!=null &&
              this.totals.equals(other.getTotals()))) &&
            ((this.systemCheck==null && other.getSystemCheck()==null) || 
             (this.systemCheck!=null &&
              this.systemCheck.equals(other.getSystemCheck()))) &&
            ((this.debitCardAuthorize==null && other.getDebitCardAuthorize()==null) || 
             (this.debitCardAuthorize!=null &&
              this.debitCardAuthorize.equals(other.getDebitCardAuthorize()))) &&
            ((this.debitCardCharge==null && other.getDebitCardCharge()==null) || 
             (this.debitCardCharge!=null &&
              this.debitCardCharge.equals(other.getDebitCardCharge()))) &&
            ((this.debitCardCredit==null && other.getDebitCardCredit()==null) || 
             (this.debitCardCredit!=null &&
              this.debitCardCredit.equals(other.getDebitCardCredit()))) &&
            ((this.debitCardChargePINless==null && other.getDebitCardChargePINless()==null) || 
             (this.debitCardChargePINless!=null &&
              this.debitCardChargePINless.equals(other.getDebitCardChargePINless()))) &&
            ((this.debitCardCreditPINless==null && other.getDebitCardCreditPINless()==null) || 
             (this.debitCardCreditPINless!=null &&
              this.debitCardCreditPINless.equals(other.getDebitCardCreditPINless()))) &&
            ((this.debitCardCapture==null && other.getDebitCardCapture()==null) || 
             (this.debitCardCapture!=null &&
              this.debitCardCapture.equals(other.getDebitCardCapture()))) &&
            ((this.debitCardVoid==null && other.getDebitCardVoid()==null) || 
             (this.debitCardVoid!=null &&
              this.debitCardVoid.equals(other.getDebitCardVoid()))) &&
            ((this.debitCardTimeoutReversal==null && other.getDebitCardTimeoutReversal()==null) || 
             (this.debitCardTimeoutReversal!=null &&
              this.debitCardTimeoutReversal.equals(other.getDebitCardTimeoutReversal()))) &&
            ((this.giftCardAuthorize==null && other.getGiftCardAuthorize()==null) || 
             (this.giftCardAuthorize!=null &&
              this.giftCardAuthorize.equals(other.getGiftCardAuthorize()))) &&
            ((this.giftCardActivate==null && other.getGiftCardActivate()==null) || 
             (this.giftCardActivate!=null &&
              this.giftCardActivate.equals(other.getGiftCardActivate()))) &&
            ((this.giftCardBalanceInquiry==null && other.getGiftCardBalanceInquiry()==null) || 
             (this.giftCardBalanceInquiry!=null &&
              this.giftCardBalanceInquiry.equals(other.getGiftCardBalanceInquiry()))) &&
            ((this.giftCardBalanceIncrease==null && other.getGiftCardBalanceIncrease()==null) || 
             (this.giftCardBalanceIncrease!=null &&
              this.giftCardBalanceIncrease.equals(other.getGiftCardBalanceIncrease()))) &&
            ((this.giftCardDeactivate==null && other.getGiftCardDeactivate()==null) || 
             (this.giftCardDeactivate!=null &&
              this.giftCardDeactivate.equals(other.getGiftCardDeactivate()))) &&
            ((this.giftCardCharge==null && other.getGiftCardCharge()==null) || 
             (this.giftCardCharge!=null &&
              this.giftCardCharge.equals(other.getGiftCardCharge()))) &&
            ((this.giftCardCredit==null && other.getGiftCardCredit()==null) || 
             (this.giftCardCredit!=null &&
              this.giftCardCredit.equals(other.getGiftCardCredit()))) &&
            ((this.giftCardRefund==null && other.getGiftCardRefund()==null) || 
             (this.giftCardRefund!=null &&
              this.giftCardRefund.equals(other.getGiftCardRefund()))) &&
            ((this.giftCardCapture==null && other.getGiftCardCapture()==null) || 
             (this.giftCardCapture!=null &&
              this.giftCardCapture.equals(other.getGiftCardCapture()))) &&
            ((this.giftCardVoid==null && other.getGiftCardVoid()==null) || 
             (this.giftCardVoid!=null &&
              this.giftCardVoid.equals(other.getGiftCardVoid()))) &&
            ((this.giftCardTimeoutReversal==null && other.getGiftCardTimeoutReversal()==null) || 
             (this.giftCardTimeoutReversal!=null &&
              this.giftCardTimeoutReversal.equals(other.getGiftCardTimeoutReversal()))) &&
            ((this.teleCheckRiskManagement==null && other.getTeleCheckRiskManagement()==null) || 
             (this.teleCheckRiskManagement!=null &&
              this.teleCheckRiskManagement.equals(other.getTeleCheckRiskManagement()))) &&
            ((this.teleCheckECACharge==null && other.getTeleCheckECACharge()==null) || 
             (this.teleCheckECACharge!=null &&
              this.teleCheckECACharge.equals(other.getTeleCheckECACharge()))) &&
            ((this.teleCheckECAVoid==null && other.getTeleCheckECAVoid()==null) || 
             (this.teleCheckECAVoid!=null &&
              this.teleCheckECAVoid.equals(other.getTeleCheckECAVoid()))) &&
            ((this.teleCheckECATimeoutReversal==null && other.getTeleCheckECATimeoutReversal()==null) || 
             (this.teleCheckECATimeoutReversal!=null &&
              this.teleCheckECATimeoutReversal.equals(other.getTeleCheckECATimeoutReversal()))) &&
            ((this.foodStampCharge==null && other.getFoodStampCharge()==null) || 
             (this.foodStampCharge!=null &&
              this.foodStampCharge.equals(other.getFoodStampCharge()))) &&
            ((this.foodStampCredit==null && other.getFoodStampCredit()==null) || 
             (this.foodStampCredit!=null &&
              this.foodStampCredit.equals(other.getFoodStampCredit()))) &&
            ((this.foodStampVoucher==null && other.getFoodStampVoucher()==null) || 
             (this.foodStampVoucher!=null &&
              this.foodStampVoucher.equals(other.getFoodStampVoucher()))) &&
            ((this.foodStampBalanceInquiry==null && other.getFoodStampBalanceInquiry()==null) || 
             (this.foodStampBalanceInquiry!=null &&
              this.foodStampBalanceInquiry.equals(other.getFoodStampBalanceInquiry()))) &&
            ((this.cashBenefitCharge==null && other.getCashBenefitCharge()==null) || 
             (this.cashBenefitCharge!=null &&
              this.cashBenefitCharge.equals(other.getCashBenefitCharge()))) &&
            ((this.cashBenefitBalanceInquiry==null && other.getCashBenefitBalanceInquiry()==null) || 
             (this.cashBenefitBalanceInquiry!=null &&
              this.cashBenefitBalanceInquiry.equals(other.getCashBenefitBalanceInquiry()))) &&
            ((this.payloadFormat==null && other.getPayloadFormat()==null) || 
             (this.payloadFormat!=null &&
              this.payloadFormat.equals(other.getPayloadFormat())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getServiceID() != null) {
            _hashCode += getServiceID().hashCode();
        }
        if (getCreditCardAuthorize() != null) {
            _hashCode += getCreditCardAuthorize().hashCode();
        }
        if (getCreditCardCapture() != null) {
            _hashCode += getCreditCardCapture().hashCode();
        }
        if (getCreditCardCharge() != null) {
            _hashCode += getCreditCardCharge().hashCode();
        }
        if (getCreditCardCredit() != null) {
            _hashCode += getCreditCardCredit().hashCode();
        }
        if (getCreditCardRefund() != null) {
            _hashCode += getCreditCardRefund().hashCode();
        }
        if (getCreditCardSettle() != null) {
            _hashCode += getCreditCardSettle().hashCode();
        }
        if (getCreditCardTimeoutReversal() != null) {
            _hashCode += getCreditCardTimeoutReversal().hashCode();
        }
        if (getCreditCardVoid() != null) {
            _hashCode += getCreditCardVoid().hashCode();
        }
        if (getTotals() != null) {
            _hashCode += getTotals().hashCode();
        }
        if (getSystemCheck() != null) {
            _hashCode += getSystemCheck().hashCode();
        }
        if (getDebitCardAuthorize() != null) {
            _hashCode += getDebitCardAuthorize().hashCode();
        }
        if (getDebitCardCharge() != null) {
            _hashCode += getDebitCardCharge().hashCode();
        }
        if (getDebitCardCredit() != null) {
            _hashCode += getDebitCardCredit().hashCode();
        }
        if (getDebitCardChargePINless() != null) {
            _hashCode += getDebitCardChargePINless().hashCode();
        }
        if (getDebitCardCreditPINless() != null) {
            _hashCode += getDebitCardCreditPINless().hashCode();
        }
        if (getDebitCardCapture() != null) {
            _hashCode += getDebitCardCapture().hashCode();
        }
        if (getDebitCardVoid() != null) {
            _hashCode += getDebitCardVoid().hashCode();
        }
        if (getDebitCardTimeoutReversal() != null) {
            _hashCode += getDebitCardTimeoutReversal().hashCode();
        }
        if (getGiftCardAuthorize() != null) {
            _hashCode += getGiftCardAuthorize().hashCode();
        }
        if (getGiftCardActivate() != null) {
            _hashCode += getGiftCardActivate().hashCode();
        }
        if (getGiftCardBalanceInquiry() != null) {
            _hashCode += getGiftCardBalanceInquiry().hashCode();
        }
        if (getGiftCardBalanceIncrease() != null) {
            _hashCode += getGiftCardBalanceIncrease().hashCode();
        }
        if (getGiftCardDeactivate() != null) {
            _hashCode += getGiftCardDeactivate().hashCode();
        }
        if (getGiftCardCharge() != null) {
            _hashCode += getGiftCardCharge().hashCode();
        }
        if (getGiftCardCredit() != null) {
            _hashCode += getGiftCardCredit().hashCode();
        }
        if (getGiftCardRefund() != null) {
            _hashCode += getGiftCardRefund().hashCode();
        }
        if (getGiftCardCapture() != null) {
            _hashCode += getGiftCardCapture().hashCode();
        }
        if (getGiftCardVoid() != null) {
            _hashCode += getGiftCardVoid().hashCode();
        }
        if (getGiftCardTimeoutReversal() != null) {
            _hashCode += getGiftCardTimeoutReversal().hashCode();
        }
        if (getTeleCheckRiskManagement() != null) {
            _hashCode += getTeleCheckRiskManagement().hashCode();
        }
        if (getTeleCheckECACharge() != null) {
            _hashCode += getTeleCheckECACharge().hashCode();
        }
        if (getTeleCheckECAVoid() != null) {
            _hashCode += getTeleCheckECAVoid().hashCode();
        }
        if (getTeleCheckECATimeoutReversal() != null) {
            _hashCode += getTeleCheckECATimeoutReversal().hashCode();
        }
        if (getFoodStampCharge() != null) {
            _hashCode += getFoodStampCharge().hashCode();
        }
        if (getFoodStampCredit() != null) {
            _hashCode += getFoodStampCredit().hashCode();
        }
        if (getFoodStampVoucher() != null) {
            _hashCode += getFoodStampVoucher().hashCode();
        }
        if (getFoodStampBalanceInquiry() != null) {
            _hashCode += getFoodStampBalanceInquiry().hashCode();
        }
        if (getCashBenefitCharge() != null) {
            _hashCode += getCashBenefitCharge().hashCode();
        }
        if (getCashBenefitBalanceInquiry() != null) {
            _hashCode += getCashBenefitBalanceInquiry().hashCode();
        }
        if (getPayloadFormat() != null) {
            _hashCode += getPayloadFormat().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionType"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("payloadFormat");
        attrField.setXmlName(new javax.xml.namespace.QName("", "PayloadFormat"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">TransactionType>PayloadFormat"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ServiceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardAuthorize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardAuthorize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardAuthorize"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardCapture");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardCapture"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardCapture"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardCharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardCharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardCredit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardCredit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardCreditType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardRefund");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardRefund"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardRefund"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardSettle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardSettle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardSettle"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardTimeoutReversal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardTimeoutReversal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TimeoutReversalType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardVoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardVoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardVoid"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totals");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Totals"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">Totals"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCheck");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemCheck"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">SystemCheck"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardAuthorize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardAuthorize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardAuthorizeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardCharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardCredit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCredit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardChargePINless");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargePINless"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargePINlessType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardCreditPINless");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCreditPINless"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargePINlessType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardCapture");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCapture"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCaptureType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardVoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardVoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardVoidType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardTimeoutReversal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardTimeoutReversal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardTimeoutReversalType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardAuthorize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardAuthorize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardAuthorizeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardActivate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardActivate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardActivateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardBalanceInquiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardBalanceInquiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardBalanceInquiryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardBalanceIncrease");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardBalanceIncrease"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardBalanceIncreaseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardDeactivate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardDeactivate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardDeactivateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardCharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardCredit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCredit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCreditType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardRefund");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardRefund"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCreditType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardCapture");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCapture"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCaptureType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardVoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardVoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardVoidType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardTimeoutReversal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardTimeoutReversal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardTimeoutReversalType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teleCheckRiskManagement");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckRiskManagement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckRiskManagementType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teleCheckECACharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECACharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECAChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teleCheckECAVoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECAVoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECAVoidType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teleCheckECATimeoutReversal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECATimeoutReversal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECATimeoutReversalType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foodStampCharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampCharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foodStampCredit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampCredit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foodStampVoucher");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampVoucher"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampVoucherType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foodStampBalanceInquiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampBalanceInquiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitBalanceInquiryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashBenefitCharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitCharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitChargeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashBenefitBalanceInquiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitBalanceInquiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitBalanceInquiryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
