/**
 * AuthorizerNetworkType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class AuthorizerNetworkType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected AuthorizerNetworkType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "other";
    public static final java.lang.String _value2 = "american-express";
    public static final java.lang.String _value3 = "visa";
    public static final java.lang.String _value4 = "mastercard";
    public static final java.lang.String _value5 = "discover-network";
    public static final java.lang.String _value6 = "valero";
    public static final java.lang.String _value7 = "pulse";
    public static final java.lang.String _value8 = "star-se";
    public static final java.lang.String _value9 = "certegy";
    public static final java.lang.String _value10 = "telecheck-eca";
    public static final java.lang.String _value11 = "telecheck";
    public static final java.lang.String _value12 = "negative-floor";
    public static final java.lang.String _value13 = "buypass";
    public static final java.lang.String _value14 = "airtime";
    public static final java.lang.String _value15 = "lncomm";
    public static final java.lang.String _value16 = "star-ne";
    public static final java.lang.String _value17 = "safeway";
    public static final java.lang.String _value18 = "telecheck-cit";
    public static final java.lang.String _value19 = "texaco";
    public static final java.lang.String _value20 = "fleetcor";
    public static final java.lang.String _value21 = "home-depot-prox";
    public static final java.lang.String _value22 = "starw";
    public static final java.lang.String _value23 = "diners-club";
    public static final java.lang.String _value24 = "fdr-albertsons";
    public static final java.lang.String _value25 = "nyce";
    public static final java.lang.String _value26 = "sun-oil";
    public static final java.lang.String _value27 = "a-toda-hora-banco-popular-ebt";
    public static final java.lang.String _value28 = "maestro";
    public static final java.lang.String _value29 = "bank-of-america-paypoint";
    public static final java.lang.String _value30 = "wells-fargo-paypoint";
    public static final java.lang.String _value31 = "bank-of-america-direct";
    public static final java.lang.String _value32 = "wells-fargo-direct";
    public static final java.lang.String _value33 = "paypoint-gift-card";
    public static final java.lang.String _value34 = "paypoint-check";
    public static final java.lang.String _value35 = "interlink";
    public static final java.lang.String _value36 = "bank-of-americaordeluxe-ebt";
    public static final java.lang.String _value37 = "transactive-ebt-tx";
    public static final java.lang.String _value38 = "the-home-depot-gift-card";
    public static final java.lang.String _value39 = "valuelink";
    public static final java.lang.String _value40 = "wex-iso";
    public static final java.lang.String _value41 = "shazam";
    public static final java.lang.String _value42 = "deluxe-ebt";
    public static final java.lang.String _value43 = "alaska";
    public static final java.lang.String _value44 = "transactive-ebt-il";
    public static final java.lang.String _value45 = "a-toda-hora-global-ebt";
    public static final java.lang.String _value46 = "northeast-coalition-of-states-ebt";
    public static final java.lang.String _value47 = "western-states-ebt-association";
    public static final java.lang.String _value48 = "southern-alliance-of-states-ebt";
    public static final java.lang.String _value49 = "lockheed-martin-ins";
    public static final java.lang.String _value50 = "armed-forces-financial-network";
    public static final java.lang.String _value51 = "accel";
    public static final java.lang.String _value52 = "a-toda-hora-banco-popular";
    public static final java.lang.String _value53 = "stored-value-systems";
    public static final java.lang.String _value54 = "gascard";
    public static final java.lang.String _value55 = "sinclair";
    public static final java.lang.String _value56 = "a-toda-hora-global";
    public static final java.lang.String _value57 = "acs-direct";
    public static final java.lang.String _value58 = "buycheck";
    public static final java.lang.String _value59 = "debitman";
    public static final java.lang.String _value60 = "eds";
    public static final java.lang.String _value61 = "concord-ach";
    public static final java.lang.String _value62 = "credit-union-24";
    public static final java.lang.String _value63 = "mci";
    public static final java.lang.String _value64 = "lml-check-service";
    public static final java.lang.String _value65 = "star-chek-direct-check";
    public static final java.lang.String _value66 = "presolutions";
    public static final java.lang.String _value67 = "alliance-data-systems";
    public static final java.lang.String _value68 = "star-chek-direct-debit";
    public static final java.lang.String _value69 = "voyager";
    public static final java.lang.String _value70 = "unocal";
    public static final java.lang.String _value71 = "fleet-one";
    public static final java.lang.String _value72 = "jeanie";
    public static final java.lang.String _value73 = "nets";
    public static final AuthorizerNetworkType value1 = new AuthorizerNetworkType(_value1);
    public static final AuthorizerNetworkType value2 = new AuthorizerNetworkType(_value2);
    public static final AuthorizerNetworkType value3 = new AuthorizerNetworkType(_value3);
    public static final AuthorizerNetworkType value4 = new AuthorizerNetworkType(_value4);
    public static final AuthorizerNetworkType value5 = new AuthorizerNetworkType(_value5);
    public static final AuthorizerNetworkType value6 = new AuthorizerNetworkType(_value6);
    public static final AuthorizerNetworkType value7 = new AuthorizerNetworkType(_value7);
    public static final AuthorizerNetworkType value8 = new AuthorizerNetworkType(_value8);
    public static final AuthorizerNetworkType value9 = new AuthorizerNetworkType(_value9);
    public static final AuthorizerNetworkType value10 = new AuthorizerNetworkType(_value10);
    public static final AuthorizerNetworkType value11 = new AuthorizerNetworkType(_value11);
    public static final AuthorizerNetworkType value12 = new AuthorizerNetworkType(_value12);
    public static final AuthorizerNetworkType value13 = new AuthorizerNetworkType(_value13);
    public static final AuthorizerNetworkType value14 = new AuthorizerNetworkType(_value14);
    public static final AuthorizerNetworkType value15 = new AuthorizerNetworkType(_value15);
    public static final AuthorizerNetworkType value16 = new AuthorizerNetworkType(_value16);
    public static final AuthorizerNetworkType value17 = new AuthorizerNetworkType(_value17);
    public static final AuthorizerNetworkType value18 = new AuthorizerNetworkType(_value18);
    public static final AuthorizerNetworkType value19 = new AuthorizerNetworkType(_value19);
    public static final AuthorizerNetworkType value20 = new AuthorizerNetworkType(_value20);
    public static final AuthorizerNetworkType value21 = new AuthorizerNetworkType(_value21);
    public static final AuthorizerNetworkType value22 = new AuthorizerNetworkType(_value22);
    public static final AuthorizerNetworkType value23 = new AuthorizerNetworkType(_value23);
    public static final AuthorizerNetworkType value24 = new AuthorizerNetworkType(_value24);
    public static final AuthorizerNetworkType value25 = new AuthorizerNetworkType(_value25);
    public static final AuthorizerNetworkType value26 = new AuthorizerNetworkType(_value26);
    public static final AuthorizerNetworkType value27 = new AuthorizerNetworkType(_value27);
    public static final AuthorizerNetworkType value28 = new AuthorizerNetworkType(_value28);
    public static final AuthorizerNetworkType value29 = new AuthorizerNetworkType(_value29);
    public static final AuthorizerNetworkType value30 = new AuthorizerNetworkType(_value30);
    public static final AuthorizerNetworkType value31 = new AuthorizerNetworkType(_value31);
    public static final AuthorizerNetworkType value32 = new AuthorizerNetworkType(_value32);
    public static final AuthorizerNetworkType value33 = new AuthorizerNetworkType(_value33);
    public static final AuthorizerNetworkType value34 = new AuthorizerNetworkType(_value34);
    public static final AuthorizerNetworkType value35 = new AuthorizerNetworkType(_value35);
    public static final AuthorizerNetworkType value36 = new AuthorizerNetworkType(_value36);
    public static final AuthorizerNetworkType value37 = new AuthorizerNetworkType(_value37);
    public static final AuthorizerNetworkType value38 = new AuthorizerNetworkType(_value38);
    public static final AuthorizerNetworkType value39 = new AuthorizerNetworkType(_value39);
    public static final AuthorizerNetworkType value40 = new AuthorizerNetworkType(_value40);
    public static final AuthorizerNetworkType value41 = new AuthorizerNetworkType(_value41);
    public static final AuthorizerNetworkType value42 = new AuthorizerNetworkType(_value42);
    public static final AuthorizerNetworkType value43 = new AuthorizerNetworkType(_value43);
    public static final AuthorizerNetworkType value44 = new AuthorizerNetworkType(_value44);
    public static final AuthorizerNetworkType value45 = new AuthorizerNetworkType(_value45);
    public static final AuthorizerNetworkType value46 = new AuthorizerNetworkType(_value46);
    public static final AuthorizerNetworkType value47 = new AuthorizerNetworkType(_value47);
    public static final AuthorizerNetworkType value48 = new AuthorizerNetworkType(_value48);
    public static final AuthorizerNetworkType value49 = new AuthorizerNetworkType(_value49);
    public static final AuthorizerNetworkType value50 = new AuthorizerNetworkType(_value50);
    public static final AuthorizerNetworkType value51 = new AuthorizerNetworkType(_value51);
    public static final AuthorizerNetworkType value52 = new AuthorizerNetworkType(_value52);
    public static final AuthorizerNetworkType value53 = new AuthorizerNetworkType(_value53);
    public static final AuthorizerNetworkType value54 = new AuthorizerNetworkType(_value54);
    public static final AuthorizerNetworkType value55 = new AuthorizerNetworkType(_value55);
    public static final AuthorizerNetworkType value56 = new AuthorizerNetworkType(_value56);
    public static final AuthorizerNetworkType value57 = new AuthorizerNetworkType(_value57);
    public static final AuthorizerNetworkType value58 = new AuthorizerNetworkType(_value58);
    public static final AuthorizerNetworkType value59 = new AuthorizerNetworkType(_value59);
    public static final AuthorizerNetworkType value60 = new AuthorizerNetworkType(_value60);
    public static final AuthorizerNetworkType value61 = new AuthorizerNetworkType(_value61);
    public static final AuthorizerNetworkType value62 = new AuthorizerNetworkType(_value62);
    public static final AuthorizerNetworkType value63 = new AuthorizerNetworkType(_value63);
    public static final AuthorizerNetworkType value64 = new AuthorizerNetworkType(_value64);
    public static final AuthorizerNetworkType value65 = new AuthorizerNetworkType(_value65);
    public static final AuthorizerNetworkType value66 = new AuthorizerNetworkType(_value66);
    public static final AuthorizerNetworkType value67 = new AuthorizerNetworkType(_value67);
    public static final AuthorizerNetworkType value68 = new AuthorizerNetworkType(_value68);
    public static final AuthorizerNetworkType value69 = new AuthorizerNetworkType(_value69);
    public static final AuthorizerNetworkType value70 = new AuthorizerNetworkType(_value70);
    public static final AuthorizerNetworkType value71 = new AuthorizerNetworkType(_value71);
    public static final AuthorizerNetworkType value72 = new AuthorizerNetworkType(_value72);
    public static final AuthorizerNetworkType value73 = new AuthorizerNetworkType(_value73);
    public java.lang.String getValue() { return _value_;}
    public static AuthorizerNetworkType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        AuthorizerNetworkType enumeration = (AuthorizerNetworkType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static AuthorizerNetworkType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthorizerNetworkType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AuthorizerNetworkType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
