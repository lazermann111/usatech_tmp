/**
 * TotalsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class TotalsResponse  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.firstdata.securetransport.ActivityPeriodType activityPeriod;

    private java.lang.String responseCode;

    private java.lang.String responseMessage;

    private java.math.BigDecimal processedAmount;

    private com.usatech.iso8583.interchange.firstdata.securetransport.SettlementCodeType settlementCode;

    private java.math.BigInteger systemTraceNumber;

    private java.lang.String transmissionDateTime;

    public TotalsResponse() {
    }

    public TotalsResponse(
           com.usatech.iso8583.interchange.firstdata.securetransport.ActivityPeriodType activityPeriod,
           java.lang.String responseCode,
           java.lang.String responseMessage,
           java.math.BigDecimal processedAmount,
           com.usatech.iso8583.interchange.firstdata.securetransport.SettlementCodeType settlementCode,
           java.math.BigInteger systemTraceNumber,
           java.lang.String transmissionDateTime) {
           this.activityPeriod = activityPeriod;
           this.responseCode = responseCode;
           this.responseMessage = responseMessage;
           this.processedAmount = processedAmount;
           this.settlementCode = settlementCode;
           this.systemTraceNumber = systemTraceNumber;
           this.transmissionDateTime = transmissionDateTime;
    }


    /**
     * Gets the activityPeriod value for this TotalsResponse.
     * 
     * @return activityPeriod
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.ActivityPeriodType getActivityPeriod() {
        return activityPeriod;
    }


    /**
     * Sets the activityPeriod value for this TotalsResponse.
     * 
     * @param activityPeriod
     */
    public void setActivityPeriod(com.usatech.iso8583.interchange.firstdata.securetransport.ActivityPeriodType activityPeriod) {
        this.activityPeriod = activityPeriod;
    }


    /**
     * Gets the responseCode value for this TotalsResponse.
     * 
     * @return responseCode
     */
    public java.lang.String getResponseCode() {
        return responseCode;
    }


    /**
     * Sets the responseCode value for this TotalsResponse.
     * 
     * @param responseCode
     */
    public void setResponseCode(java.lang.String responseCode) {
        this.responseCode = responseCode;
    }


    /**
     * Gets the responseMessage value for this TotalsResponse.
     * 
     * @return responseMessage
     */
    public java.lang.String getResponseMessage() {
        return responseMessage;
    }


    /**
     * Sets the responseMessage value for this TotalsResponse.
     * 
     * @param responseMessage
     */
    public void setResponseMessage(java.lang.String responseMessage) {
        this.responseMessage = responseMessage;
    }


    /**
     * Gets the processedAmount value for this TotalsResponse.
     * 
     * @return processedAmount
     */
    public java.math.BigDecimal getProcessedAmount() {
        return processedAmount;
    }


    /**
     * Sets the processedAmount value for this TotalsResponse.
     * 
     * @param processedAmount
     */
    public void setProcessedAmount(java.math.BigDecimal processedAmount) {
        this.processedAmount = processedAmount;
    }


    /**
     * Gets the settlementCode value for this TotalsResponse.
     * 
     * @return settlementCode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.SettlementCodeType getSettlementCode() {
        return settlementCode;
    }


    /**
     * Sets the settlementCode value for this TotalsResponse.
     * 
     * @param settlementCode
     */
    public void setSettlementCode(com.usatech.iso8583.interchange.firstdata.securetransport.SettlementCodeType settlementCode) {
        this.settlementCode = settlementCode;
    }


    /**
     * Gets the systemTraceNumber value for this TotalsResponse.
     * 
     * @return systemTraceNumber
     */
    public java.math.BigInteger getSystemTraceNumber() {
        return systemTraceNumber;
    }


    /**
     * Sets the systemTraceNumber value for this TotalsResponse.
     * 
     * @param systemTraceNumber
     */
    public void setSystemTraceNumber(java.math.BigInteger systemTraceNumber) {
        this.systemTraceNumber = systemTraceNumber;
    }


    /**
     * Gets the transmissionDateTime value for this TotalsResponse.
     * 
     * @return transmissionDateTime
     */
    public java.lang.String getTransmissionDateTime() {
        return transmissionDateTime;
    }


    /**
     * Sets the transmissionDateTime value for this TotalsResponse.
     * 
     * @param transmissionDateTime
     */
    public void setTransmissionDateTime(java.lang.String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TotalsResponse)) return false;
        TotalsResponse other = (TotalsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.activityPeriod==null && other.getActivityPeriod()==null) || 
             (this.activityPeriod!=null &&
              this.activityPeriod.equals(other.getActivityPeriod()))) &&
            ((this.responseCode==null && other.getResponseCode()==null) || 
             (this.responseCode!=null &&
              this.responseCode.equals(other.getResponseCode()))) &&
            ((this.responseMessage==null && other.getResponseMessage()==null) || 
             (this.responseMessage!=null &&
              this.responseMessage.equals(other.getResponseMessage()))) &&
            ((this.processedAmount==null && other.getProcessedAmount()==null) || 
             (this.processedAmount!=null &&
              this.processedAmount.equals(other.getProcessedAmount()))) &&
            ((this.settlementCode==null && other.getSettlementCode()==null) || 
             (this.settlementCode!=null &&
              this.settlementCode.equals(other.getSettlementCode()))) &&
            ((this.systemTraceNumber==null && other.getSystemTraceNumber()==null) || 
             (this.systemTraceNumber!=null &&
              this.systemTraceNumber.equals(other.getSystemTraceNumber()))) &&
            ((this.transmissionDateTime==null && other.getTransmissionDateTime()==null) || 
             (this.transmissionDateTime!=null &&
              this.transmissionDateTime.equals(other.getTransmissionDateTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActivityPeriod() != null) {
            _hashCode += getActivityPeriod().hashCode();
        }
        if (getResponseCode() != null) {
            _hashCode += getResponseCode().hashCode();
        }
        if (getResponseMessage() != null) {
            _hashCode += getResponseMessage().hashCode();
        }
        if (getProcessedAmount() != null) {
            _hashCode += getProcessedAmount().hashCode();
        }
        if (getSettlementCode() != null) {
            _hashCode += getSettlementCode().hashCode();
        }
        if (getSystemTraceNumber() != null) {
            _hashCode += getSystemTraceNumber().hashCode();
        }
        if (getTransmissionDateTime() != null) {
            _hashCode += getTransmissionDateTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TotalsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">TotalsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityPeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ActivityPeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ActivityPeriodType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseCodeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "AdditionalResponseDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ProcessedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ProcessedAmountType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("settlementCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SettlementCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SettlementCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemTraceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceAuditNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTimeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
