/**
 * TeleCheckECAVoidType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class TeleCheckECAVoidType  implements java.io.Serializable {
    private java.lang.String approvalCode;

    private java.lang.String billingControlNumber;

    private java.lang.String birthDate;

    private java.lang.String cashierNumber;

    private java.lang.String checkNumber;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CheckTypeType checkType;

    private java.lang.String extendedMICR;

    private java.lang.String hostReferenceNumber;

    private java.lang.String identificationNumber;

    private java.lang.String identificationState;

    private java.lang.String MICR;

    private java.lang.String originalTransactionName;

    private java.lang.String phoneNumber;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition;

    private com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType;

    private java.lang.String productCode;

    private java.lang.String responseCode;

    private java.math.BigInteger systemTraceNumber;

    private java.lang.String traceID;

    private java.math.BigDecimal transactionAmount;

    private java.lang.String transactionDate;

    private java.lang.String transactionTime;

    private java.lang.String transmissionDateTime;

    private java.lang.String TPPID;

    public TeleCheckECAVoidType() {
    }

    public TeleCheckECAVoidType(
           java.lang.String approvalCode,
           java.lang.String billingControlNumber,
           java.lang.String birthDate,
           java.lang.String cashierNumber,
           java.lang.String checkNumber,
           com.usatech.iso8583.interchange.firstdata.securetransport.CheckTypeType checkType,
           java.lang.String extendedMICR,
           java.lang.String hostReferenceNumber,
           java.lang.String identificationNumber,
           java.lang.String identificationState,
           java.lang.String MICR,
           java.lang.String originalTransactionName,
           java.lang.String phoneNumber,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition,
           com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType,
           java.lang.String productCode,
           java.lang.String responseCode,
           java.math.BigInteger systemTraceNumber,
           java.lang.String traceID,
           java.math.BigDecimal transactionAmount,
           java.lang.String transactionDate,
           java.lang.String transactionTime,
           java.lang.String transmissionDateTime,
           java.lang.String TPPID) {
           this.approvalCode = approvalCode;
           this.billingControlNumber = billingControlNumber;
           this.birthDate = birthDate;
           this.cashierNumber = cashierNumber;
           this.checkNumber = checkNumber;
           this.checkType = checkType;
           this.extendedMICR = extendedMICR;
           this.hostReferenceNumber = hostReferenceNumber;
           this.identificationNumber = identificationNumber;
           this.identificationState = identificationState;
           this.MICR = MICR;
           this.originalTransactionName = originalTransactionName;
           this.phoneNumber = phoneNumber;
           this.POSConditionCode = POSConditionCode;
           this.POSPANEntryMode = POSPANEntryMode;
           this.POSPINEntryMode = POSPINEntryMode;
           this.POSSecurityCondition = POSSecurityCondition;
           this.POSTerminalType = POSTerminalType;
           this.productCode = productCode;
           this.responseCode = responseCode;
           this.systemTraceNumber = systemTraceNumber;
           this.traceID = traceID;
           this.transactionAmount = transactionAmount;
           this.transactionDate = transactionDate;
           this.transactionTime = transactionTime;
           this.transmissionDateTime = transmissionDateTime;
           this.TPPID = TPPID;
    }


    /**
     * Gets the approvalCode value for this TeleCheckECAVoidType.
     * 
     * @return approvalCode
     */
    public java.lang.String getApprovalCode() {
        return approvalCode;
    }


    /**
     * Sets the approvalCode value for this TeleCheckECAVoidType.
     * 
     * @param approvalCode
     */
    public void setApprovalCode(java.lang.String approvalCode) {
        this.approvalCode = approvalCode;
    }


    /**
     * Gets the billingControlNumber value for this TeleCheckECAVoidType.
     * 
     * @return billingControlNumber
     */
    public java.lang.String getBillingControlNumber() {
        return billingControlNumber;
    }


    /**
     * Sets the billingControlNumber value for this TeleCheckECAVoidType.
     * 
     * @param billingControlNumber
     */
    public void setBillingControlNumber(java.lang.String billingControlNumber) {
        this.billingControlNumber = billingControlNumber;
    }


    /**
     * Gets the birthDate value for this TeleCheckECAVoidType.
     * 
     * @return birthDate
     */
    public java.lang.String getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this TeleCheckECAVoidType.
     * 
     * @param birthDate
     */
    public void setBirthDate(java.lang.String birthDate) {
        this.birthDate = birthDate;
    }


    /**
     * Gets the cashierNumber value for this TeleCheckECAVoidType.
     * 
     * @return cashierNumber
     */
    public java.lang.String getCashierNumber() {
        return cashierNumber;
    }


    /**
     * Sets the cashierNumber value for this TeleCheckECAVoidType.
     * 
     * @param cashierNumber
     */
    public void setCashierNumber(java.lang.String cashierNumber) {
        this.cashierNumber = cashierNumber;
    }


    /**
     * Gets the checkNumber value for this TeleCheckECAVoidType.
     * 
     * @return checkNumber
     */
    public java.lang.String getCheckNumber() {
        return checkNumber;
    }


    /**
     * Sets the checkNumber value for this TeleCheckECAVoidType.
     * 
     * @param checkNumber
     */
    public void setCheckNumber(java.lang.String checkNumber) {
        this.checkNumber = checkNumber;
    }


    /**
     * Gets the checkType value for this TeleCheckECAVoidType.
     * 
     * @return checkType
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CheckTypeType getCheckType() {
        return checkType;
    }


    /**
     * Sets the checkType value for this TeleCheckECAVoidType.
     * 
     * @param checkType
     */
    public void setCheckType(com.usatech.iso8583.interchange.firstdata.securetransport.CheckTypeType checkType) {
        this.checkType = checkType;
    }


    /**
     * Gets the extendedMICR value for this TeleCheckECAVoidType.
     * 
     * @return extendedMICR
     */
    public java.lang.String getExtendedMICR() {
        return extendedMICR;
    }


    /**
     * Sets the extendedMICR value for this TeleCheckECAVoidType.
     * 
     * @param extendedMICR
     */
    public void setExtendedMICR(java.lang.String extendedMICR) {
        this.extendedMICR = extendedMICR;
    }


    /**
     * Gets the hostReferenceNumber value for this TeleCheckECAVoidType.
     * 
     * @return hostReferenceNumber
     */
    public java.lang.String getHostReferenceNumber() {
        return hostReferenceNumber;
    }


    /**
     * Sets the hostReferenceNumber value for this TeleCheckECAVoidType.
     * 
     * @param hostReferenceNumber
     */
    public void setHostReferenceNumber(java.lang.String hostReferenceNumber) {
        this.hostReferenceNumber = hostReferenceNumber;
    }


    /**
     * Gets the identificationNumber value for this TeleCheckECAVoidType.
     * 
     * @return identificationNumber
     */
    public java.lang.String getIdentificationNumber() {
        return identificationNumber;
    }


    /**
     * Sets the identificationNumber value for this TeleCheckECAVoidType.
     * 
     * @param identificationNumber
     */
    public void setIdentificationNumber(java.lang.String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }


    /**
     * Gets the identificationState value for this TeleCheckECAVoidType.
     * 
     * @return identificationState
     */
    public java.lang.String getIdentificationState() {
        return identificationState;
    }


    /**
     * Sets the identificationState value for this TeleCheckECAVoidType.
     * 
     * @param identificationState
     */
    public void setIdentificationState(java.lang.String identificationState) {
        this.identificationState = identificationState;
    }


    /**
     * Gets the MICR value for this TeleCheckECAVoidType.
     * 
     * @return MICR
     */
    public java.lang.String getMICR() {
        return MICR;
    }


    /**
     * Sets the MICR value for this TeleCheckECAVoidType.
     * 
     * @param MICR
     */
    public void setMICR(java.lang.String MICR) {
        this.MICR = MICR;
    }


    /**
     * Gets the originalTransactionName value for this TeleCheckECAVoidType.
     * 
     * @return originalTransactionName
     */
    public java.lang.String getOriginalTransactionName() {
        return originalTransactionName;
    }


    /**
     * Sets the originalTransactionName value for this TeleCheckECAVoidType.
     * 
     * @param originalTransactionName
     */
    public void setOriginalTransactionName(java.lang.String originalTransactionName) {
        this.originalTransactionName = originalTransactionName;
    }


    /**
     * Gets the phoneNumber value for this TeleCheckECAVoidType.
     * 
     * @return phoneNumber
     */
    public java.lang.String getPhoneNumber() {
        return phoneNumber;
    }


    /**
     * Sets the phoneNumber value for this TeleCheckECAVoidType.
     * 
     * @param phoneNumber
     */
    public void setPhoneNumber(java.lang.String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    /**
     * Gets the POSConditionCode value for this TeleCheckECAVoidType.
     * 
     * @return POSConditionCode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType getPOSConditionCode() {
        return POSConditionCode;
    }


    /**
     * Sets the POSConditionCode value for this TeleCheckECAVoidType.
     * 
     * @param POSConditionCode
     */
    public void setPOSConditionCode(com.usatech.iso8583.interchange.firstdata.securetransport.POSConditionCodeType POSConditionCode) {
        this.POSConditionCode = POSConditionCode;
    }


    /**
     * Gets the POSPANEntryMode value for this TeleCheckECAVoidType.
     * 
     * @return POSPANEntryMode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType getPOSPANEntryMode() {
        return POSPANEntryMode;
    }


    /**
     * Sets the POSPANEntryMode value for this TeleCheckECAVoidType.
     * 
     * @param POSPANEntryMode
     */
    public void setPOSPANEntryMode(com.usatech.iso8583.interchange.firstdata.securetransport.POSPANEntryModeType POSPANEntryMode) {
        this.POSPANEntryMode = POSPANEntryMode;
    }


    /**
     * Gets the POSPINEntryMode value for this TeleCheckECAVoidType.
     * 
     * @return POSPINEntryMode
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType getPOSPINEntryMode() {
        return POSPINEntryMode;
    }


    /**
     * Sets the POSPINEntryMode value for this TeleCheckECAVoidType.
     * 
     * @param POSPINEntryMode
     */
    public void setPOSPINEntryMode(com.usatech.iso8583.interchange.firstdata.securetransport.POSPINEntryModeType POSPINEntryMode) {
        this.POSPINEntryMode = POSPINEntryMode;
    }


    /**
     * Gets the POSSecurityCondition value for this TeleCheckECAVoidType.
     * 
     * @return POSSecurityCondition
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType getPOSSecurityCondition() {
        return POSSecurityCondition;
    }


    /**
     * Sets the POSSecurityCondition value for this TeleCheckECAVoidType.
     * 
     * @param POSSecurityCondition
     */
    public void setPOSSecurityCondition(com.usatech.iso8583.interchange.firstdata.securetransport.POSSecurityConditionType POSSecurityCondition) {
        this.POSSecurityCondition = POSSecurityCondition;
    }


    /**
     * Gets the POSTerminalType value for this TeleCheckECAVoidType.
     * 
     * @return POSTerminalType
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType getPOSTerminalType() {
        return POSTerminalType;
    }


    /**
     * Sets the POSTerminalType value for this TeleCheckECAVoidType.
     * 
     * @param POSTerminalType
     */
    public void setPOSTerminalType(com.usatech.iso8583.interchange.firstdata.securetransport.POSTerminalTypeType POSTerminalType) {
        this.POSTerminalType = POSTerminalType;
    }


    /**
     * Gets the productCode value for this TeleCheckECAVoidType.
     * 
     * @return productCode
     */
    public java.lang.String getProductCode() {
        return productCode;
    }


    /**
     * Sets the productCode value for this TeleCheckECAVoidType.
     * 
     * @param productCode
     */
    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }


    /**
     * Gets the responseCode value for this TeleCheckECAVoidType.
     * 
     * @return responseCode
     */
    public java.lang.String getResponseCode() {
        return responseCode;
    }


    /**
     * Sets the responseCode value for this TeleCheckECAVoidType.
     * 
     * @param responseCode
     */
    public void setResponseCode(java.lang.String responseCode) {
        this.responseCode = responseCode;
    }


    /**
     * Gets the systemTraceNumber value for this TeleCheckECAVoidType.
     * 
     * @return systemTraceNumber
     */
    public java.math.BigInteger getSystemTraceNumber() {
        return systemTraceNumber;
    }


    /**
     * Sets the systemTraceNumber value for this TeleCheckECAVoidType.
     * 
     * @param systemTraceNumber
     */
    public void setSystemTraceNumber(java.math.BigInteger systemTraceNumber) {
        this.systemTraceNumber = systemTraceNumber;
    }


    /**
     * Gets the traceID value for this TeleCheckECAVoidType.
     * 
     * @return traceID
     */
    public java.lang.String getTraceID() {
        return traceID;
    }


    /**
     * Sets the traceID value for this TeleCheckECAVoidType.
     * 
     * @param traceID
     */
    public void setTraceID(java.lang.String traceID) {
        this.traceID = traceID;
    }


    /**
     * Gets the transactionAmount value for this TeleCheckECAVoidType.
     * 
     * @return transactionAmount
     */
    public java.math.BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    /**
     * Sets the transactionAmount value for this TeleCheckECAVoidType.
     * 
     * @param transactionAmount
     */
    public void setTransactionAmount(java.math.BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    /**
     * Gets the transactionDate value for this TeleCheckECAVoidType.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this TeleCheckECAVoidType.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the transactionTime value for this TeleCheckECAVoidType.
     * 
     * @return transactionTime
     */
    public java.lang.String getTransactionTime() {
        return transactionTime;
    }


    /**
     * Sets the transactionTime value for this TeleCheckECAVoidType.
     * 
     * @param transactionTime
     */
    public void setTransactionTime(java.lang.String transactionTime) {
        this.transactionTime = transactionTime;
    }


    /**
     * Gets the transmissionDateTime value for this TeleCheckECAVoidType.
     * 
     * @return transmissionDateTime
     */
    public java.lang.String getTransmissionDateTime() {
        return transmissionDateTime;
    }


    /**
     * Sets the transmissionDateTime value for this TeleCheckECAVoidType.
     * 
     * @param transmissionDateTime
     */
    public void setTransmissionDateTime(java.lang.String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }


    /**
     * Gets the TPPID value for this TeleCheckECAVoidType.
     * 
     * @return TPPID
     */
    public java.lang.String getTPPID() {
        return TPPID;
    }


    /**
     * Sets the TPPID value for this TeleCheckECAVoidType.
     * 
     * @param TPPID
     */
    public void setTPPID(java.lang.String TPPID) {
        this.TPPID = TPPID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TeleCheckECAVoidType)) return false;
        TeleCheckECAVoidType other = (TeleCheckECAVoidType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.approvalCode==null && other.getApprovalCode()==null) || 
             (this.approvalCode!=null &&
              this.approvalCode.equals(other.getApprovalCode()))) &&
            ((this.billingControlNumber==null && other.getBillingControlNumber()==null) || 
             (this.billingControlNumber!=null &&
              this.billingControlNumber.equals(other.getBillingControlNumber()))) &&
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate()))) &&
            ((this.cashierNumber==null && other.getCashierNumber()==null) || 
             (this.cashierNumber!=null &&
              this.cashierNumber.equals(other.getCashierNumber()))) &&
            ((this.checkNumber==null && other.getCheckNumber()==null) || 
             (this.checkNumber!=null &&
              this.checkNumber.equals(other.getCheckNumber()))) &&
            ((this.checkType==null && other.getCheckType()==null) || 
             (this.checkType!=null &&
              this.checkType.equals(other.getCheckType()))) &&
            ((this.extendedMICR==null && other.getExtendedMICR()==null) || 
             (this.extendedMICR!=null &&
              this.extendedMICR.equals(other.getExtendedMICR()))) &&
            ((this.hostReferenceNumber==null && other.getHostReferenceNumber()==null) || 
             (this.hostReferenceNumber!=null &&
              this.hostReferenceNumber.equals(other.getHostReferenceNumber()))) &&
            ((this.identificationNumber==null && other.getIdentificationNumber()==null) || 
             (this.identificationNumber!=null &&
              this.identificationNumber.equals(other.getIdentificationNumber()))) &&
            ((this.identificationState==null && other.getIdentificationState()==null) || 
             (this.identificationState!=null &&
              this.identificationState.equals(other.getIdentificationState()))) &&
            ((this.MICR==null && other.getMICR()==null) || 
             (this.MICR!=null &&
              this.MICR.equals(other.getMICR()))) &&
            ((this.originalTransactionName==null && other.getOriginalTransactionName()==null) || 
             (this.originalTransactionName!=null &&
              this.originalTransactionName.equals(other.getOriginalTransactionName()))) &&
            ((this.phoneNumber==null && other.getPhoneNumber()==null) || 
             (this.phoneNumber!=null &&
              this.phoneNumber.equals(other.getPhoneNumber()))) &&
            ((this.POSConditionCode==null && other.getPOSConditionCode()==null) || 
             (this.POSConditionCode!=null &&
              this.POSConditionCode.equals(other.getPOSConditionCode()))) &&
            ((this.POSPANEntryMode==null && other.getPOSPANEntryMode()==null) || 
             (this.POSPANEntryMode!=null &&
              this.POSPANEntryMode.equals(other.getPOSPANEntryMode()))) &&
            ((this.POSPINEntryMode==null && other.getPOSPINEntryMode()==null) || 
             (this.POSPINEntryMode!=null &&
              this.POSPINEntryMode.equals(other.getPOSPINEntryMode()))) &&
            ((this.POSSecurityCondition==null && other.getPOSSecurityCondition()==null) || 
             (this.POSSecurityCondition!=null &&
              this.POSSecurityCondition.equals(other.getPOSSecurityCondition()))) &&
            ((this.POSTerminalType==null && other.getPOSTerminalType()==null) || 
             (this.POSTerminalType!=null &&
              this.POSTerminalType.equals(other.getPOSTerminalType()))) &&
            ((this.productCode==null && other.getProductCode()==null) || 
             (this.productCode!=null &&
              this.productCode.equals(other.getProductCode()))) &&
            ((this.responseCode==null && other.getResponseCode()==null) || 
             (this.responseCode!=null &&
              this.responseCode.equals(other.getResponseCode()))) &&
            ((this.systemTraceNumber==null && other.getSystemTraceNumber()==null) || 
             (this.systemTraceNumber!=null &&
              this.systemTraceNumber.equals(other.getSystemTraceNumber()))) &&
            ((this.traceID==null && other.getTraceID()==null) || 
             (this.traceID!=null &&
              this.traceID.equals(other.getTraceID()))) &&
            ((this.transactionAmount==null && other.getTransactionAmount()==null) || 
             (this.transactionAmount!=null &&
              this.transactionAmount.equals(other.getTransactionAmount()))) &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.transactionTime==null && other.getTransactionTime()==null) || 
             (this.transactionTime!=null &&
              this.transactionTime.equals(other.getTransactionTime()))) &&
            ((this.transmissionDateTime==null && other.getTransmissionDateTime()==null) || 
             (this.transmissionDateTime!=null &&
              this.transmissionDateTime.equals(other.getTransmissionDateTime()))) &&
            ((this.TPPID==null && other.getTPPID()==null) || 
             (this.TPPID!=null &&
              this.TPPID.equals(other.getTPPID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApprovalCode() != null) {
            _hashCode += getApprovalCode().hashCode();
        }
        if (getBillingControlNumber() != null) {
            _hashCode += getBillingControlNumber().hashCode();
        }
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        if (getCashierNumber() != null) {
            _hashCode += getCashierNumber().hashCode();
        }
        if (getCheckNumber() != null) {
            _hashCode += getCheckNumber().hashCode();
        }
        if (getCheckType() != null) {
            _hashCode += getCheckType().hashCode();
        }
        if (getExtendedMICR() != null) {
            _hashCode += getExtendedMICR().hashCode();
        }
        if (getHostReferenceNumber() != null) {
            _hashCode += getHostReferenceNumber().hashCode();
        }
        if (getIdentificationNumber() != null) {
            _hashCode += getIdentificationNumber().hashCode();
        }
        if (getIdentificationState() != null) {
            _hashCode += getIdentificationState().hashCode();
        }
        if (getMICR() != null) {
            _hashCode += getMICR().hashCode();
        }
        if (getOriginalTransactionName() != null) {
            _hashCode += getOriginalTransactionName().hashCode();
        }
        if (getPhoneNumber() != null) {
            _hashCode += getPhoneNumber().hashCode();
        }
        if (getPOSConditionCode() != null) {
            _hashCode += getPOSConditionCode().hashCode();
        }
        if (getPOSPANEntryMode() != null) {
            _hashCode += getPOSPANEntryMode().hashCode();
        }
        if (getPOSPINEntryMode() != null) {
            _hashCode += getPOSPINEntryMode().hashCode();
        }
        if (getPOSSecurityCondition() != null) {
            _hashCode += getPOSSecurityCondition().hashCode();
        }
        if (getPOSTerminalType() != null) {
            _hashCode += getPOSTerminalType().hashCode();
        }
        if (getProductCode() != null) {
            _hashCode += getProductCode().hashCode();
        }
        if (getResponseCode() != null) {
            _hashCode += getResponseCode().hashCode();
        }
        if (getSystemTraceNumber() != null) {
            _hashCode += getSystemTraceNumber().hashCode();
        }
        if (getTraceID() != null) {
            _hashCode += getTraceID().hashCode();
        }
        if (getTransactionAmount() != null) {
            _hashCode += getTransactionAmount().hashCode();
        }
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getTransactionTime() != null) {
            _hashCode += getTransactionTime().hashCode();
        }
        if (getTransmissionDateTime() != null) {
            _hashCode += getTransmissionDateTime().hashCode();
        }
        if (getTPPID() != null) {
            _hashCode += getTPPID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TeleCheckECAVoidType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECAVoidType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ApprovalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ApprovalCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billingControlNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "BillingControlNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "BillingControlNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "BirthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "YearMonthDayType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashierNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashierNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashierNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CheckNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CheckNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CheckType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CheckTypeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendedMICR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ExtendedMICR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ExtendedMICRType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostReferenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "HostReferenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "RetrievalReferenceNumber"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificationNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "IdentificationNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "IdentificationNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificationState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "IdentificationState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "StateProvinceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MICR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "MICR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "MICRType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalTransactionName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "OriginalTransactionName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "PhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "PhoneNumberType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSConditionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSConditionCodeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSPANEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPANEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPANEntryModeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSPINEntryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPINEntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSPINEntryModeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSSecurityCondition");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSSecurityCondition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSSecurityConditionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTerminalType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalTypeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ProductCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ProductCodeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseCodeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemTraceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemTraceAuditNumberType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("traceID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TraceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TraceIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionAmountType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionDateType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "LocalTransactionTimeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransmissionDateTimeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TPPID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TPPID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TPPIDType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
