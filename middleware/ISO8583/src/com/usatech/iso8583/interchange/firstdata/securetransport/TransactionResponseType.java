/**
 * TransactionResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class TransactionResponseType  implements java.io.Serializable {
    private java.lang.String returnCode;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardAuthorizeResponse creditCardAuthorizeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCaptureResponse creditCardCaptureResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardChargeResponseType creditCardChargeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCreditResponseType creditCardCreditResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardRefundResponse creditCardRefundResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardSettleResponse creditCardSettleResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TimeoutReversalResponseType creditCardTimeoutReversalResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardVoidResponse creditCardVoidResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TotalsResponse totalsResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.SystemCheckResponse systemCheckResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardAuthorizeResponseType debitCardAuthorizeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeResponseType debitCardChargeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeResponseType debitCardCreditResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessResponseType debitCardChargePINlessResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessResponseType debitCardCreditPINlessResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardCaptureResponseType debitCardCaptureResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardVoidResponseType debitCardVoidResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardTimeoutReversalResponseType debitCardTimeoutReversalResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardAuthorizeResponseType giftCardAuthorizeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardActivateResponseType giftCardActivateResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceInquiryResponseType giftCardBalanceInquiryResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceIncreaseResponseType giftCardBalanceIncreaseResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardDeactivateResponseType giftCardDeactivateResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardChargeResponseType giftCardChargeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditResponseType giftCardCreditResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditResponseType giftCardRefundResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureResponseType giftCardCaptureResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardVoidResponseType giftCardVoidResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardTimeoutReversalResponseType giftCardTimeoutReversalResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckRiskManagementResponseType teleCheckRiskManagementResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAChargeResponseType teleCheckECAChargeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAVoidResponseType teleCheckECAVoidResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECATimeoutReversalResponseType teleCheckECATimeoutReversalResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeResponseType foodStampChargeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeResponseType foodStampCreditResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampVoucherResponseType foodStampVoucherResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryResponseType foodStampBalanceInquiryResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitChargeResponseType cashBenefitChargeResponse;

    private com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryResponseType cashBenefitBalanceInquiryResponse;

    public TransactionResponseType() {
    }

    public TransactionResponseType(
           java.lang.String returnCode,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardAuthorizeResponse creditCardAuthorizeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCaptureResponse creditCardCaptureResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardChargeResponseType creditCardChargeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCreditResponseType creditCardCreditResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardRefundResponse creditCardRefundResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardSettleResponse creditCardSettleResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.TimeoutReversalResponseType creditCardTimeoutReversalResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardVoidResponse creditCardVoidResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.TotalsResponse totalsResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.SystemCheckResponse systemCheckResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardAuthorizeResponseType debitCardAuthorizeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeResponseType debitCardChargeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeResponseType debitCardCreditResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessResponseType debitCardChargePINlessResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessResponseType debitCardCreditPINlessResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardCaptureResponseType debitCardCaptureResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardVoidResponseType debitCardVoidResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardTimeoutReversalResponseType debitCardTimeoutReversalResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardAuthorizeResponseType giftCardAuthorizeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardActivateResponseType giftCardActivateResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceInquiryResponseType giftCardBalanceInquiryResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceIncreaseResponseType giftCardBalanceIncreaseResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardDeactivateResponseType giftCardDeactivateResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardChargeResponseType giftCardChargeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditResponseType giftCardCreditResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditResponseType giftCardRefundResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureResponseType giftCardCaptureResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardVoidResponseType giftCardVoidResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardTimeoutReversalResponseType giftCardTimeoutReversalResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckRiskManagementResponseType teleCheckRiskManagementResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAChargeResponseType teleCheckECAChargeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAVoidResponseType teleCheckECAVoidResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECATimeoutReversalResponseType teleCheckECATimeoutReversalResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeResponseType foodStampChargeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeResponseType foodStampCreditResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampVoucherResponseType foodStampVoucherResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryResponseType foodStampBalanceInquiryResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitChargeResponseType cashBenefitChargeResponse,
           com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryResponseType cashBenefitBalanceInquiryResponse) {
           this.returnCode = returnCode;
           this.creditCardAuthorizeResponse = creditCardAuthorizeResponse;
           this.creditCardCaptureResponse = creditCardCaptureResponse;
           this.creditCardChargeResponse = creditCardChargeResponse;
           this.creditCardCreditResponse = creditCardCreditResponse;
           this.creditCardRefundResponse = creditCardRefundResponse;
           this.creditCardSettleResponse = creditCardSettleResponse;
           this.creditCardTimeoutReversalResponse = creditCardTimeoutReversalResponse;
           this.creditCardVoidResponse = creditCardVoidResponse;
           this.totalsResponse = totalsResponse;
           this.systemCheckResponse = systemCheckResponse;
           this.debitCardAuthorizeResponse = debitCardAuthorizeResponse;
           this.debitCardChargeResponse = debitCardChargeResponse;
           this.debitCardCreditResponse = debitCardCreditResponse;
           this.debitCardChargePINlessResponse = debitCardChargePINlessResponse;
           this.debitCardCreditPINlessResponse = debitCardCreditPINlessResponse;
           this.debitCardCaptureResponse = debitCardCaptureResponse;
           this.debitCardVoidResponse = debitCardVoidResponse;
           this.debitCardTimeoutReversalResponse = debitCardTimeoutReversalResponse;
           this.giftCardAuthorizeResponse = giftCardAuthorizeResponse;
           this.giftCardActivateResponse = giftCardActivateResponse;
           this.giftCardBalanceInquiryResponse = giftCardBalanceInquiryResponse;
           this.giftCardBalanceIncreaseResponse = giftCardBalanceIncreaseResponse;
           this.giftCardDeactivateResponse = giftCardDeactivateResponse;
           this.giftCardChargeResponse = giftCardChargeResponse;
           this.giftCardCreditResponse = giftCardCreditResponse;
           this.giftCardRefundResponse = giftCardRefundResponse;
           this.giftCardCaptureResponse = giftCardCaptureResponse;
           this.giftCardVoidResponse = giftCardVoidResponse;
           this.giftCardTimeoutReversalResponse = giftCardTimeoutReversalResponse;
           this.teleCheckRiskManagementResponse = teleCheckRiskManagementResponse;
           this.teleCheckECAChargeResponse = teleCheckECAChargeResponse;
           this.teleCheckECAVoidResponse = teleCheckECAVoidResponse;
           this.teleCheckECATimeoutReversalResponse = teleCheckECATimeoutReversalResponse;
           this.foodStampChargeResponse = foodStampChargeResponse;
           this.foodStampCreditResponse = foodStampCreditResponse;
           this.foodStampVoucherResponse = foodStampVoucherResponse;
           this.foodStampBalanceInquiryResponse = foodStampBalanceInquiryResponse;
           this.cashBenefitChargeResponse = cashBenefitChargeResponse;
           this.cashBenefitBalanceInquiryResponse = cashBenefitBalanceInquiryResponse;
    }


    /**
     * Gets the returnCode value for this TransactionResponseType.
     * 
     * @return returnCode
     */
    public java.lang.String getReturnCode() {
        return returnCode;
    }


    /**
     * Sets the returnCode value for this TransactionResponseType.
     * 
     * @param returnCode
     */
    public void setReturnCode(java.lang.String returnCode) {
        this.returnCode = returnCode;
    }


    /**
     * Gets the creditCardAuthorizeResponse value for this TransactionResponseType.
     * 
     * @return creditCardAuthorizeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardAuthorizeResponse getCreditCardAuthorizeResponse() {
        return creditCardAuthorizeResponse;
    }


    /**
     * Sets the creditCardAuthorizeResponse value for this TransactionResponseType.
     * 
     * @param creditCardAuthorizeResponse
     */
    public void setCreditCardAuthorizeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardAuthorizeResponse creditCardAuthorizeResponse) {
        this.creditCardAuthorizeResponse = creditCardAuthorizeResponse;
    }


    /**
     * Gets the creditCardCaptureResponse value for this TransactionResponseType.
     * 
     * @return creditCardCaptureResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCaptureResponse getCreditCardCaptureResponse() {
        return creditCardCaptureResponse;
    }


    /**
     * Sets the creditCardCaptureResponse value for this TransactionResponseType.
     * 
     * @param creditCardCaptureResponse
     */
    public void setCreditCardCaptureResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCaptureResponse creditCardCaptureResponse) {
        this.creditCardCaptureResponse = creditCardCaptureResponse;
    }


    /**
     * Gets the creditCardChargeResponse value for this TransactionResponseType.
     * 
     * @return creditCardChargeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardChargeResponseType getCreditCardChargeResponse() {
        return creditCardChargeResponse;
    }


    /**
     * Sets the creditCardChargeResponse value for this TransactionResponseType.
     * 
     * @param creditCardChargeResponse
     */
    public void setCreditCardChargeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardChargeResponseType creditCardChargeResponse) {
        this.creditCardChargeResponse = creditCardChargeResponse;
    }


    /**
     * Gets the creditCardCreditResponse value for this TransactionResponseType.
     * 
     * @return creditCardCreditResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCreditResponseType getCreditCardCreditResponse() {
        return creditCardCreditResponse;
    }


    /**
     * Sets the creditCardCreditResponse value for this TransactionResponseType.
     * 
     * @param creditCardCreditResponse
     */
    public void setCreditCardCreditResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardCreditResponseType creditCardCreditResponse) {
        this.creditCardCreditResponse = creditCardCreditResponse;
    }


    /**
     * Gets the creditCardRefundResponse value for this TransactionResponseType.
     * 
     * @return creditCardRefundResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardRefundResponse getCreditCardRefundResponse() {
        return creditCardRefundResponse;
    }


    /**
     * Sets the creditCardRefundResponse value for this TransactionResponseType.
     * 
     * @param creditCardRefundResponse
     */
    public void setCreditCardRefundResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardRefundResponse creditCardRefundResponse) {
        this.creditCardRefundResponse = creditCardRefundResponse;
    }


    /**
     * Gets the creditCardSettleResponse value for this TransactionResponseType.
     * 
     * @return creditCardSettleResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardSettleResponse getCreditCardSettleResponse() {
        return creditCardSettleResponse;
    }


    /**
     * Sets the creditCardSettleResponse value for this TransactionResponseType.
     * 
     * @param creditCardSettleResponse
     */
    public void setCreditCardSettleResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardSettleResponse creditCardSettleResponse) {
        this.creditCardSettleResponse = creditCardSettleResponse;
    }


    /**
     * Gets the creditCardTimeoutReversalResponse value for this TransactionResponseType.
     * 
     * @return creditCardTimeoutReversalResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TimeoutReversalResponseType getCreditCardTimeoutReversalResponse() {
        return creditCardTimeoutReversalResponse;
    }


    /**
     * Sets the creditCardTimeoutReversalResponse value for this TransactionResponseType.
     * 
     * @param creditCardTimeoutReversalResponse
     */
    public void setCreditCardTimeoutReversalResponse(com.usatech.iso8583.interchange.firstdata.securetransport.TimeoutReversalResponseType creditCardTimeoutReversalResponse) {
        this.creditCardTimeoutReversalResponse = creditCardTimeoutReversalResponse;
    }


    /**
     * Gets the creditCardVoidResponse value for this TransactionResponseType.
     * 
     * @return creditCardVoidResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardVoidResponse getCreditCardVoidResponse() {
        return creditCardVoidResponse;
    }


    /**
     * Sets the creditCardVoidResponse value for this TransactionResponseType.
     * 
     * @param creditCardVoidResponse
     */
    public void setCreditCardVoidResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CreditCardVoidResponse creditCardVoidResponse) {
        this.creditCardVoidResponse = creditCardVoidResponse;
    }


    /**
     * Gets the totalsResponse value for this TransactionResponseType.
     * 
     * @return totalsResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TotalsResponse getTotalsResponse() {
        return totalsResponse;
    }


    /**
     * Sets the totalsResponse value for this TransactionResponseType.
     * 
     * @param totalsResponse
     */
    public void setTotalsResponse(com.usatech.iso8583.interchange.firstdata.securetransport.TotalsResponse totalsResponse) {
        this.totalsResponse = totalsResponse;
    }


    /**
     * Gets the systemCheckResponse value for this TransactionResponseType.
     * 
     * @return systemCheckResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.SystemCheckResponse getSystemCheckResponse() {
        return systemCheckResponse;
    }


    /**
     * Sets the systemCheckResponse value for this TransactionResponseType.
     * 
     * @param systemCheckResponse
     */
    public void setSystemCheckResponse(com.usatech.iso8583.interchange.firstdata.securetransport.SystemCheckResponse systemCheckResponse) {
        this.systemCheckResponse = systemCheckResponse;
    }


    /**
     * Gets the debitCardAuthorizeResponse value for this TransactionResponseType.
     * 
     * @return debitCardAuthorizeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardAuthorizeResponseType getDebitCardAuthorizeResponse() {
        return debitCardAuthorizeResponse;
    }


    /**
     * Sets the debitCardAuthorizeResponse value for this TransactionResponseType.
     * 
     * @param debitCardAuthorizeResponse
     */
    public void setDebitCardAuthorizeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardAuthorizeResponseType debitCardAuthorizeResponse) {
        this.debitCardAuthorizeResponse = debitCardAuthorizeResponse;
    }


    /**
     * Gets the debitCardChargeResponse value for this TransactionResponseType.
     * 
     * @return debitCardChargeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeResponseType getDebitCardChargeResponse() {
        return debitCardChargeResponse;
    }


    /**
     * Sets the debitCardChargeResponse value for this TransactionResponseType.
     * 
     * @param debitCardChargeResponse
     */
    public void setDebitCardChargeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeResponseType debitCardChargeResponse) {
        this.debitCardChargeResponse = debitCardChargeResponse;
    }


    /**
     * Gets the debitCardCreditResponse value for this TransactionResponseType.
     * 
     * @return debitCardCreditResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeResponseType getDebitCardCreditResponse() {
        return debitCardCreditResponse;
    }


    /**
     * Sets the debitCardCreditResponse value for this TransactionResponseType.
     * 
     * @param debitCardCreditResponse
     */
    public void setDebitCardCreditResponse(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargeResponseType debitCardCreditResponse) {
        this.debitCardCreditResponse = debitCardCreditResponse;
    }


    /**
     * Gets the debitCardChargePINlessResponse value for this TransactionResponseType.
     * 
     * @return debitCardChargePINlessResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessResponseType getDebitCardChargePINlessResponse() {
        return debitCardChargePINlessResponse;
    }


    /**
     * Sets the debitCardChargePINlessResponse value for this TransactionResponseType.
     * 
     * @param debitCardChargePINlessResponse
     */
    public void setDebitCardChargePINlessResponse(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessResponseType debitCardChargePINlessResponse) {
        this.debitCardChargePINlessResponse = debitCardChargePINlessResponse;
    }


    /**
     * Gets the debitCardCreditPINlessResponse value for this TransactionResponseType.
     * 
     * @return debitCardCreditPINlessResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessResponseType getDebitCardCreditPINlessResponse() {
        return debitCardCreditPINlessResponse;
    }


    /**
     * Sets the debitCardCreditPINlessResponse value for this TransactionResponseType.
     * 
     * @param debitCardCreditPINlessResponse
     */
    public void setDebitCardCreditPINlessResponse(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardChargePINlessResponseType debitCardCreditPINlessResponse) {
        this.debitCardCreditPINlessResponse = debitCardCreditPINlessResponse;
    }


    /**
     * Gets the debitCardCaptureResponse value for this TransactionResponseType.
     * 
     * @return debitCardCaptureResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardCaptureResponseType getDebitCardCaptureResponse() {
        return debitCardCaptureResponse;
    }


    /**
     * Sets the debitCardCaptureResponse value for this TransactionResponseType.
     * 
     * @param debitCardCaptureResponse
     */
    public void setDebitCardCaptureResponse(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardCaptureResponseType debitCardCaptureResponse) {
        this.debitCardCaptureResponse = debitCardCaptureResponse;
    }


    /**
     * Gets the debitCardVoidResponse value for this TransactionResponseType.
     * 
     * @return debitCardVoidResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardVoidResponseType getDebitCardVoidResponse() {
        return debitCardVoidResponse;
    }


    /**
     * Sets the debitCardVoidResponse value for this TransactionResponseType.
     * 
     * @param debitCardVoidResponse
     */
    public void setDebitCardVoidResponse(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardVoidResponseType debitCardVoidResponse) {
        this.debitCardVoidResponse = debitCardVoidResponse;
    }


    /**
     * Gets the debitCardTimeoutReversalResponse value for this TransactionResponseType.
     * 
     * @return debitCardTimeoutReversalResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardTimeoutReversalResponseType getDebitCardTimeoutReversalResponse() {
        return debitCardTimeoutReversalResponse;
    }


    /**
     * Sets the debitCardTimeoutReversalResponse value for this TransactionResponseType.
     * 
     * @param debitCardTimeoutReversalResponse
     */
    public void setDebitCardTimeoutReversalResponse(com.usatech.iso8583.interchange.firstdata.securetransport.DebitCardTimeoutReversalResponseType debitCardTimeoutReversalResponse) {
        this.debitCardTimeoutReversalResponse = debitCardTimeoutReversalResponse;
    }


    /**
     * Gets the giftCardAuthorizeResponse value for this TransactionResponseType.
     * 
     * @return giftCardAuthorizeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardAuthorizeResponseType getGiftCardAuthorizeResponse() {
        return giftCardAuthorizeResponse;
    }


    /**
     * Sets the giftCardAuthorizeResponse value for this TransactionResponseType.
     * 
     * @param giftCardAuthorizeResponse
     */
    public void setGiftCardAuthorizeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardAuthorizeResponseType giftCardAuthorizeResponse) {
        this.giftCardAuthorizeResponse = giftCardAuthorizeResponse;
    }


    /**
     * Gets the giftCardActivateResponse value for this TransactionResponseType.
     * 
     * @return giftCardActivateResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardActivateResponseType getGiftCardActivateResponse() {
        return giftCardActivateResponse;
    }


    /**
     * Sets the giftCardActivateResponse value for this TransactionResponseType.
     * 
     * @param giftCardActivateResponse
     */
    public void setGiftCardActivateResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardActivateResponseType giftCardActivateResponse) {
        this.giftCardActivateResponse = giftCardActivateResponse;
    }


    /**
     * Gets the giftCardBalanceInquiryResponse value for this TransactionResponseType.
     * 
     * @return giftCardBalanceInquiryResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceInquiryResponseType getGiftCardBalanceInquiryResponse() {
        return giftCardBalanceInquiryResponse;
    }


    /**
     * Sets the giftCardBalanceInquiryResponse value for this TransactionResponseType.
     * 
     * @param giftCardBalanceInquiryResponse
     */
    public void setGiftCardBalanceInquiryResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceInquiryResponseType giftCardBalanceInquiryResponse) {
        this.giftCardBalanceInquiryResponse = giftCardBalanceInquiryResponse;
    }


    /**
     * Gets the giftCardBalanceIncreaseResponse value for this TransactionResponseType.
     * 
     * @return giftCardBalanceIncreaseResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceIncreaseResponseType getGiftCardBalanceIncreaseResponse() {
        return giftCardBalanceIncreaseResponse;
    }


    /**
     * Sets the giftCardBalanceIncreaseResponse value for this TransactionResponseType.
     * 
     * @param giftCardBalanceIncreaseResponse
     */
    public void setGiftCardBalanceIncreaseResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardBalanceIncreaseResponseType giftCardBalanceIncreaseResponse) {
        this.giftCardBalanceIncreaseResponse = giftCardBalanceIncreaseResponse;
    }


    /**
     * Gets the giftCardDeactivateResponse value for this TransactionResponseType.
     * 
     * @return giftCardDeactivateResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardDeactivateResponseType getGiftCardDeactivateResponse() {
        return giftCardDeactivateResponse;
    }


    /**
     * Sets the giftCardDeactivateResponse value for this TransactionResponseType.
     * 
     * @param giftCardDeactivateResponse
     */
    public void setGiftCardDeactivateResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardDeactivateResponseType giftCardDeactivateResponse) {
        this.giftCardDeactivateResponse = giftCardDeactivateResponse;
    }


    /**
     * Gets the giftCardChargeResponse value for this TransactionResponseType.
     * 
     * @return giftCardChargeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardChargeResponseType getGiftCardChargeResponse() {
        return giftCardChargeResponse;
    }


    /**
     * Sets the giftCardChargeResponse value for this TransactionResponseType.
     * 
     * @param giftCardChargeResponse
     */
    public void setGiftCardChargeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardChargeResponseType giftCardChargeResponse) {
        this.giftCardChargeResponse = giftCardChargeResponse;
    }


    /**
     * Gets the giftCardCreditResponse value for this TransactionResponseType.
     * 
     * @return giftCardCreditResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditResponseType getGiftCardCreditResponse() {
        return giftCardCreditResponse;
    }


    /**
     * Sets the giftCardCreditResponse value for this TransactionResponseType.
     * 
     * @param giftCardCreditResponse
     */
    public void setGiftCardCreditResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditResponseType giftCardCreditResponse) {
        this.giftCardCreditResponse = giftCardCreditResponse;
    }


    /**
     * Gets the giftCardRefundResponse value for this TransactionResponseType.
     * 
     * @return giftCardRefundResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditResponseType getGiftCardRefundResponse() {
        return giftCardRefundResponse;
    }


    /**
     * Sets the giftCardRefundResponse value for this TransactionResponseType.
     * 
     * @param giftCardRefundResponse
     */
    public void setGiftCardRefundResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCreditResponseType giftCardRefundResponse) {
        this.giftCardRefundResponse = giftCardRefundResponse;
    }


    /**
     * Gets the giftCardCaptureResponse value for this TransactionResponseType.
     * 
     * @return giftCardCaptureResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureResponseType getGiftCardCaptureResponse() {
        return giftCardCaptureResponse;
    }


    /**
     * Sets the giftCardCaptureResponse value for this TransactionResponseType.
     * 
     * @param giftCardCaptureResponse
     */
    public void setGiftCardCaptureResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardCaptureResponseType giftCardCaptureResponse) {
        this.giftCardCaptureResponse = giftCardCaptureResponse;
    }


    /**
     * Gets the giftCardVoidResponse value for this TransactionResponseType.
     * 
     * @return giftCardVoidResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardVoidResponseType getGiftCardVoidResponse() {
        return giftCardVoidResponse;
    }


    /**
     * Sets the giftCardVoidResponse value for this TransactionResponseType.
     * 
     * @param giftCardVoidResponse
     */
    public void setGiftCardVoidResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardVoidResponseType giftCardVoidResponse) {
        this.giftCardVoidResponse = giftCardVoidResponse;
    }


    /**
     * Gets the giftCardTimeoutReversalResponse value for this TransactionResponseType.
     * 
     * @return giftCardTimeoutReversalResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardTimeoutReversalResponseType getGiftCardTimeoutReversalResponse() {
        return giftCardTimeoutReversalResponse;
    }


    /**
     * Sets the giftCardTimeoutReversalResponse value for this TransactionResponseType.
     * 
     * @param giftCardTimeoutReversalResponse
     */
    public void setGiftCardTimeoutReversalResponse(com.usatech.iso8583.interchange.firstdata.securetransport.GiftCardTimeoutReversalResponseType giftCardTimeoutReversalResponse) {
        this.giftCardTimeoutReversalResponse = giftCardTimeoutReversalResponse;
    }


    /**
     * Gets the teleCheckRiskManagementResponse value for this TransactionResponseType.
     * 
     * @return teleCheckRiskManagementResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckRiskManagementResponseType getTeleCheckRiskManagementResponse() {
        return teleCheckRiskManagementResponse;
    }


    /**
     * Sets the teleCheckRiskManagementResponse value for this TransactionResponseType.
     * 
     * @param teleCheckRiskManagementResponse
     */
    public void setTeleCheckRiskManagementResponse(com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckRiskManagementResponseType teleCheckRiskManagementResponse) {
        this.teleCheckRiskManagementResponse = teleCheckRiskManagementResponse;
    }


    /**
     * Gets the teleCheckECAChargeResponse value for this TransactionResponseType.
     * 
     * @return teleCheckECAChargeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAChargeResponseType getTeleCheckECAChargeResponse() {
        return teleCheckECAChargeResponse;
    }


    /**
     * Sets the teleCheckECAChargeResponse value for this TransactionResponseType.
     * 
     * @param teleCheckECAChargeResponse
     */
    public void setTeleCheckECAChargeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAChargeResponseType teleCheckECAChargeResponse) {
        this.teleCheckECAChargeResponse = teleCheckECAChargeResponse;
    }


    /**
     * Gets the teleCheckECAVoidResponse value for this TransactionResponseType.
     * 
     * @return teleCheckECAVoidResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAVoidResponseType getTeleCheckECAVoidResponse() {
        return teleCheckECAVoidResponse;
    }


    /**
     * Sets the teleCheckECAVoidResponse value for this TransactionResponseType.
     * 
     * @param teleCheckECAVoidResponse
     */
    public void setTeleCheckECAVoidResponse(com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECAVoidResponseType teleCheckECAVoidResponse) {
        this.teleCheckECAVoidResponse = teleCheckECAVoidResponse;
    }


    /**
     * Gets the teleCheckECATimeoutReversalResponse value for this TransactionResponseType.
     * 
     * @return teleCheckECATimeoutReversalResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECATimeoutReversalResponseType getTeleCheckECATimeoutReversalResponse() {
        return teleCheckECATimeoutReversalResponse;
    }


    /**
     * Sets the teleCheckECATimeoutReversalResponse value for this TransactionResponseType.
     * 
     * @param teleCheckECATimeoutReversalResponse
     */
    public void setTeleCheckECATimeoutReversalResponse(com.usatech.iso8583.interchange.firstdata.securetransport.TeleCheckECATimeoutReversalResponseType teleCheckECATimeoutReversalResponse) {
        this.teleCheckECATimeoutReversalResponse = teleCheckECATimeoutReversalResponse;
    }


    /**
     * Gets the foodStampChargeResponse value for this TransactionResponseType.
     * 
     * @return foodStampChargeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeResponseType getFoodStampChargeResponse() {
        return foodStampChargeResponse;
    }


    /**
     * Sets the foodStampChargeResponse value for this TransactionResponseType.
     * 
     * @param foodStampChargeResponse
     */
    public void setFoodStampChargeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeResponseType foodStampChargeResponse) {
        this.foodStampChargeResponse = foodStampChargeResponse;
    }


    /**
     * Gets the foodStampCreditResponse value for this TransactionResponseType.
     * 
     * @return foodStampCreditResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeResponseType getFoodStampCreditResponse() {
        return foodStampCreditResponse;
    }


    /**
     * Sets the foodStampCreditResponse value for this TransactionResponseType.
     * 
     * @param foodStampCreditResponse
     */
    public void setFoodStampCreditResponse(com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampChargeResponseType foodStampCreditResponse) {
        this.foodStampCreditResponse = foodStampCreditResponse;
    }


    /**
     * Gets the foodStampVoucherResponse value for this TransactionResponseType.
     * 
     * @return foodStampVoucherResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampVoucherResponseType getFoodStampVoucherResponse() {
        return foodStampVoucherResponse;
    }


    /**
     * Sets the foodStampVoucherResponse value for this TransactionResponseType.
     * 
     * @param foodStampVoucherResponse
     */
    public void setFoodStampVoucherResponse(com.usatech.iso8583.interchange.firstdata.securetransport.FoodStampVoucherResponseType foodStampVoucherResponse) {
        this.foodStampVoucherResponse = foodStampVoucherResponse;
    }


    /**
     * Gets the foodStampBalanceInquiryResponse value for this TransactionResponseType.
     * 
     * @return foodStampBalanceInquiryResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryResponseType getFoodStampBalanceInquiryResponse() {
        return foodStampBalanceInquiryResponse;
    }


    /**
     * Sets the foodStampBalanceInquiryResponse value for this TransactionResponseType.
     * 
     * @param foodStampBalanceInquiryResponse
     */
    public void setFoodStampBalanceInquiryResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryResponseType foodStampBalanceInquiryResponse) {
        this.foodStampBalanceInquiryResponse = foodStampBalanceInquiryResponse;
    }


    /**
     * Gets the cashBenefitChargeResponse value for this TransactionResponseType.
     * 
     * @return cashBenefitChargeResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitChargeResponseType getCashBenefitChargeResponse() {
        return cashBenefitChargeResponse;
    }


    /**
     * Sets the cashBenefitChargeResponse value for this TransactionResponseType.
     * 
     * @param cashBenefitChargeResponse
     */
    public void setCashBenefitChargeResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitChargeResponseType cashBenefitChargeResponse) {
        this.cashBenefitChargeResponse = cashBenefitChargeResponse;
    }


    /**
     * Gets the cashBenefitBalanceInquiryResponse value for this TransactionResponseType.
     * 
     * @return cashBenefitBalanceInquiryResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryResponseType getCashBenefitBalanceInquiryResponse() {
        return cashBenefitBalanceInquiryResponse;
    }


    /**
     * Sets the cashBenefitBalanceInquiryResponse value for this TransactionResponseType.
     * 
     * @param cashBenefitBalanceInquiryResponse
     */
    public void setCashBenefitBalanceInquiryResponse(com.usatech.iso8583.interchange.firstdata.securetransport.CashBenefitBalanceInquiryResponseType cashBenefitBalanceInquiryResponse) {
        this.cashBenefitBalanceInquiryResponse = cashBenefitBalanceInquiryResponse;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionResponseType)) return false;
        TransactionResponseType other = (TransactionResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.returnCode==null && other.getReturnCode()==null) || 
             (this.returnCode!=null &&
              this.returnCode.equals(other.getReturnCode()))) &&
            ((this.creditCardAuthorizeResponse==null && other.getCreditCardAuthorizeResponse()==null) || 
             (this.creditCardAuthorizeResponse!=null &&
              this.creditCardAuthorizeResponse.equals(other.getCreditCardAuthorizeResponse()))) &&
            ((this.creditCardCaptureResponse==null && other.getCreditCardCaptureResponse()==null) || 
             (this.creditCardCaptureResponse!=null &&
              this.creditCardCaptureResponse.equals(other.getCreditCardCaptureResponse()))) &&
            ((this.creditCardChargeResponse==null && other.getCreditCardChargeResponse()==null) || 
             (this.creditCardChargeResponse!=null &&
              this.creditCardChargeResponse.equals(other.getCreditCardChargeResponse()))) &&
            ((this.creditCardCreditResponse==null && other.getCreditCardCreditResponse()==null) || 
             (this.creditCardCreditResponse!=null &&
              this.creditCardCreditResponse.equals(other.getCreditCardCreditResponse()))) &&
            ((this.creditCardRefundResponse==null && other.getCreditCardRefundResponse()==null) || 
             (this.creditCardRefundResponse!=null &&
              this.creditCardRefundResponse.equals(other.getCreditCardRefundResponse()))) &&
            ((this.creditCardSettleResponse==null && other.getCreditCardSettleResponse()==null) || 
             (this.creditCardSettleResponse!=null &&
              this.creditCardSettleResponse.equals(other.getCreditCardSettleResponse()))) &&
            ((this.creditCardTimeoutReversalResponse==null && other.getCreditCardTimeoutReversalResponse()==null) || 
             (this.creditCardTimeoutReversalResponse!=null &&
              this.creditCardTimeoutReversalResponse.equals(other.getCreditCardTimeoutReversalResponse()))) &&
            ((this.creditCardVoidResponse==null && other.getCreditCardVoidResponse()==null) || 
             (this.creditCardVoidResponse!=null &&
              this.creditCardVoidResponse.equals(other.getCreditCardVoidResponse()))) &&
            ((this.totalsResponse==null && other.getTotalsResponse()==null) || 
             (this.totalsResponse!=null &&
              this.totalsResponse.equals(other.getTotalsResponse()))) &&
            ((this.systemCheckResponse==null && other.getSystemCheckResponse()==null) || 
             (this.systemCheckResponse!=null &&
              this.systemCheckResponse.equals(other.getSystemCheckResponse()))) &&
            ((this.debitCardAuthorizeResponse==null && other.getDebitCardAuthorizeResponse()==null) || 
             (this.debitCardAuthorizeResponse!=null &&
              this.debitCardAuthorizeResponse.equals(other.getDebitCardAuthorizeResponse()))) &&
            ((this.debitCardChargeResponse==null && other.getDebitCardChargeResponse()==null) || 
             (this.debitCardChargeResponse!=null &&
              this.debitCardChargeResponse.equals(other.getDebitCardChargeResponse()))) &&
            ((this.debitCardCreditResponse==null && other.getDebitCardCreditResponse()==null) || 
             (this.debitCardCreditResponse!=null &&
              this.debitCardCreditResponse.equals(other.getDebitCardCreditResponse()))) &&
            ((this.debitCardChargePINlessResponse==null && other.getDebitCardChargePINlessResponse()==null) || 
             (this.debitCardChargePINlessResponse!=null &&
              this.debitCardChargePINlessResponse.equals(other.getDebitCardChargePINlessResponse()))) &&
            ((this.debitCardCreditPINlessResponse==null && other.getDebitCardCreditPINlessResponse()==null) || 
             (this.debitCardCreditPINlessResponse!=null &&
              this.debitCardCreditPINlessResponse.equals(other.getDebitCardCreditPINlessResponse()))) &&
            ((this.debitCardCaptureResponse==null && other.getDebitCardCaptureResponse()==null) || 
             (this.debitCardCaptureResponse!=null &&
              this.debitCardCaptureResponse.equals(other.getDebitCardCaptureResponse()))) &&
            ((this.debitCardVoidResponse==null && other.getDebitCardVoidResponse()==null) || 
             (this.debitCardVoidResponse!=null &&
              this.debitCardVoidResponse.equals(other.getDebitCardVoidResponse()))) &&
            ((this.debitCardTimeoutReversalResponse==null && other.getDebitCardTimeoutReversalResponse()==null) || 
             (this.debitCardTimeoutReversalResponse!=null &&
              this.debitCardTimeoutReversalResponse.equals(other.getDebitCardTimeoutReversalResponse()))) &&
            ((this.giftCardAuthorizeResponse==null && other.getGiftCardAuthorizeResponse()==null) || 
             (this.giftCardAuthorizeResponse!=null &&
              this.giftCardAuthorizeResponse.equals(other.getGiftCardAuthorizeResponse()))) &&
            ((this.giftCardActivateResponse==null && other.getGiftCardActivateResponse()==null) || 
             (this.giftCardActivateResponse!=null &&
              this.giftCardActivateResponse.equals(other.getGiftCardActivateResponse()))) &&
            ((this.giftCardBalanceInquiryResponse==null && other.getGiftCardBalanceInquiryResponse()==null) || 
             (this.giftCardBalanceInquiryResponse!=null &&
              this.giftCardBalanceInquiryResponse.equals(other.getGiftCardBalanceInquiryResponse()))) &&
            ((this.giftCardBalanceIncreaseResponse==null && other.getGiftCardBalanceIncreaseResponse()==null) || 
             (this.giftCardBalanceIncreaseResponse!=null &&
              this.giftCardBalanceIncreaseResponse.equals(other.getGiftCardBalanceIncreaseResponse()))) &&
            ((this.giftCardDeactivateResponse==null && other.getGiftCardDeactivateResponse()==null) || 
             (this.giftCardDeactivateResponse!=null &&
              this.giftCardDeactivateResponse.equals(other.getGiftCardDeactivateResponse()))) &&
            ((this.giftCardChargeResponse==null && other.getGiftCardChargeResponse()==null) || 
             (this.giftCardChargeResponse!=null &&
              this.giftCardChargeResponse.equals(other.getGiftCardChargeResponse()))) &&
            ((this.giftCardCreditResponse==null && other.getGiftCardCreditResponse()==null) || 
             (this.giftCardCreditResponse!=null &&
              this.giftCardCreditResponse.equals(other.getGiftCardCreditResponse()))) &&
            ((this.giftCardRefundResponse==null && other.getGiftCardRefundResponse()==null) || 
             (this.giftCardRefundResponse!=null &&
              this.giftCardRefundResponse.equals(other.getGiftCardRefundResponse()))) &&
            ((this.giftCardCaptureResponse==null && other.getGiftCardCaptureResponse()==null) || 
             (this.giftCardCaptureResponse!=null &&
              this.giftCardCaptureResponse.equals(other.getGiftCardCaptureResponse()))) &&
            ((this.giftCardVoidResponse==null && other.getGiftCardVoidResponse()==null) || 
             (this.giftCardVoidResponse!=null &&
              this.giftCardVoidResponse.equals(other.getGiftCardVoidResponse()))) &&
            ((this.giftCardTimeoutReversalResponse==null && other.getGiftCardTimeoutReversalResponse()==null) || 
             (this.giftCardTimeoutReversalResponse!=null &&
              this.giftCardTimeoutReversalResponse.equals(other.getGiftCardTimeoutReversalResponse()))) &&
            ((this.teleCheckRiskManagementResponse==null && other.getTeleCheckRiskManagementResponse()==null) || 
             (this.teleCheckRiskManagementResponse!=null &&
              this.teleCheckRiskManagementResponse.equals(other.getTeleCheckRiskManagementResponse()))) &&
            ((this.teleCheckECAChargeResponse==null && other.getTeleCheckECAChargeResponse()==null) || 
             (this.teleCheckECAChargeResponse!=null &&
              this.teleCheckECAChargeResponse.equals(other.getTeleCheckECAChargeResponse()))) &&
            ((this.teleCheckECAVoidResponse==null && other.getTeleCheckECAVoidResponse()==null) || 
             (this.teleCheckECAVoidResponse!=null &&
              this.teleCheckECAVoidResponse.equals(other.getTeleCheckECAVoidResponse()))) &&
            ((this.teleCheckECATimeoutReversalResponse==null && other.getTeleCheckECATimeoutReversalResponse()==null) || 
             (this.teleCheckECATimeoutReversalResponse!=null &&
              this.teleCheckECATimeoutReversalResponse.equals(other.getTeleCheckECATimeoutReversalResponse()))) &&
            ((this.foodStampChargeResponse==null && other.getFoodStampChargeResponse()==null) || 
             (this.foodStampChargeResponse!=null &&
              this.foodStampChargeResponse.equals(other.getFoodStampChargeResponse()))) &&
            ((this.foodStampCreditResponse==null && other.getFoodStampCreditResponse()==null) || 
             (this.foodStampCreditResponse!=null &&
              this.foodStampCreditResponse.equals(other.getFoodStampCreditResponse()))) &&
            ((this.foodStampVoucherResponse==null && other.getFoodStampVoucherResponse()==null) || 
             (this.foodStampVoucherResponse!=null &&
              this.foodStampVoucherResponse.equals(other.getFoodStampVoucherResponse()))) &&
            ((this.foodStampBalanceInquiryResponse==null && other.getFoodStampBalanceInquiryResponse()==null) || 
             (this.foodStampBalanceInquiryResponse!=null &&
              this.foodStampBalanceInquiryResponse.equals(other.getFoodStampBalanceInquiryResponse()))) &&
            ((this.cashBenefitChargeResponse==null && other.getCashBenefitChargeResponse()==null) || 
             (this.cashBenefitChargeResponse!=null &&
              this.cashBenefitChargeResponse.equals(other.getCashBenefitChargeResponse()))) &&
            ((this.cashBenefitBalanceInquiryResponse==null && other.getCashBenefitBalanceInquiryResponse()==null) || 
             (this.cashBenefitBalanceInquiryResponse!=null &&
              this.cashBenefitBalanceInquiryResponse.equals(other.getCashBenefitBalanceInquiryResponse())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReturnCode() != null) {
            _hashCode += getReturnCode().hashCode();
        }
        if (getCreditCardAuthorizeResponse() != null) {
            _hashCode += getCreditCardAuthorizeResponse().hashCode();
        }
        if (getCreditCardCaptureResponse() != null) {
            _hashCode += getCreditCardCaptureResponse().hashCode();
        }
        if (getCreditCardChargeResponse() != null) {
            _hashCode += getCreditCardChargeResponse().hashCode();
        }
        if (getCreditCardCreditResponse() != null) {
            _hashCode += getCreditCardCreditResponse().hashCode();
        }
        if (getCreditCardRefundResponse() != null) {
            _hashCode += getCreditCardRefundResponse().hashCode();
        }
        if (getCreditCardSettleResponse() != null) {
            _hashCode += getCreditCardSettleResponse().hashCode();
        }
        if (getCreditCardTimeoutReversalResponse() != null) {
            _hashCode += getCreditCardTimeoutReversalResponse().hashCode();
        }
        if (getCreditCardVoidResponse() != null) {
            _hashCode += getCreditCardVoidResponse().hashCode();
        }
        if (getTotalsResponse() != null) {
            _hashCode += getTotalsResponse().hashCode();
        }
        if (getSystemCheckResponse() != null) {
            _hashCode += getSystemCheckResponse().hashCode();
        }
        if (getDebitCardAuthorizeResponse() != null) {
            _hashCode += getDebitCardAuthorizeResponse().hashCode();
        }
        if (getDebitCardChargeResponse() != null) {
            _hashCode += getDebitCardChargeResponse().hashCode();
        }
        if (getDebitCardCreditResponse() != null) {
            _hashCode += getDebitCardCreditResponse().hashCode();
        }
        if (getDebitCardChargePINlessResponse() != null) {
            _hashCode += getDebitCardChargePINlessResponse().hashCode();
        }
        if (getDebitCardCreditPINlessResponse() != null) {
            _hashCode += getDebitCardCreditPINlessResponse().hashCode();
        }
        if (getDebitCardCaptureResponse() != null) {
            _hashCode += getDebitCardCaptureResponse().hashCode();
        }
        if (getDebitCardVoidResponse() != null) {
            _hashCode += getDebitCardVoidResponse().hashCode();
        }
        if (getDebitCardTimeoutReversalResponse() != null) {
            _hashCode += getDebitCardTimeoutReversalResponse().hashCode();
        }
        if (getGiftCardAuthorizeResponse() != null) {
            _hashCode += getGiftCardAuthorizeResponse().hashCode();
        }
        if (getGiftCardActivateResponse() != null) {
            _hashCode += getGiftCardActivateResponse().hashCode();
        }
        if (getGiftCardBalanceInquiryResponse() != null) {
            _hashCode += getGiftCardBalanceInquiryResponse().hashCode();
        }
        if (getGiftCardBalanceIncreaseResponse() != null) {
            _hashCode += getGiftCardBalanceIncreaseResponse().hashCode();
        }
        if (getGiftCardDeactivateResponse() != null) {
            _hashCode += getGiftCardDeactivateResponse().hashCode();
        }
        if (getGiftCardChargeResponse() != null) {
            _hashCode += getGiftCardChargeResponse().hashCode();
        }
        if (getGiftCardCreditResponse() != null) {
            _hashCode += getGiftCardCreditResponse().hashCode();
        }
        if (getGiftCardRefundResponse() != null) {
            _hashCode += getGiftCardRefundResponse().hashCode();
        }
        if (getGiftCardCaptureResponse() != null) {
            _hashCode += getGiftCardCaptureResponse().hashCode();
        }
        if (getGiftCardVoidResponse() != null) {
            _hashCode += getGiftCardVoidResponse().hashCode();
        }
        if (getGiftCardTimeoutReversalResponse() != null) {
            _hashCode += getGiftCardTimeoutReversalResponse().hashCode();
        }
        if (getTeleCheckRiskManagementResponse() != null) {
            _hashCode += getTeleCheckRiskManagementResponse().hashCode();
        }
        if (getTeleCheckECAChargeResponse() != null) {
            _hashCode += getTeleCheckECAChargeResponse().hashCode();
        }
        if (getTeleCheckECAVoidResponse() != null) {
            _hashCode += getTeleCheckECAVoidResponse().hashCode();
        }
        if (getTeleCheckECATimeoutReversalResponse() != null) {
            _hashCode += getTeleCheckECATimeoutReversalResponse().hashCode();
        }
        if (getFoodStampChargeResponse() != null) {
            _hashCode += getFoodStampChargeResponse().hashCode();
        }
        if (getFoodStampCreditResponse() != null) {
            _hashCode += getFoodStampCreditResponse().hashCode();
        }
        if (getFoodStampVoucherResponse() != null) {
            _hashCode += getFoodStampVoucherResponse().hashCode();
        }
        if (getFoodStampBalanceInquiryResponse() != null) {
            _hashCode += getFoodStampBalanceInquiryResponse().hashCode();
        }
        if (getCashBenefitChargeResponse() != null) {
            _hashCode += getCashBenefitChargeResponse().hashCode();
        }
        if (getCashBenefitBalanceInquiryResponse() != null) {
            _hashCode += getCashBenefitBalanceInquiryResponse().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ReturnCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardAuthorizeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardAuthorizeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardAuthorizeResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardCaptureResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardCaptureResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardCaptureResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardChargeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardChargeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardChargeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardCreditResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardCreditResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardCreditResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardRefundResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardRefundResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardRefundResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardSettleResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardSettleResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardSettleResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardTimeoutReversalResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardTimeoutReversalResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TimeoutReversalResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardVoidResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CreditCardVoidResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">CreditCardVoidResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalsResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TotalsResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">TotalsResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCheckResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "SystemCheckResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", ">SystemCheckResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardAuthorizeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardAuthorizeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardAuthorizeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardChargeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardCreditResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCreditResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardChargePINlessResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargePINlessResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargePINlessResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardCreditPINlessResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCreditPINlessResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardChargePINlessResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardCaptureResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCaptureResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardCaptureResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardVoidResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardVoidResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardVoidResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCardTimeoutReversalResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardTimeoutReversalResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "DebitCardTimeoutReversalResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardAuthorizeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardAuthorizeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardAuthorizeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardActivateResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardActivateResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardActivateResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardBalanceInquiryResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardBalanceInquiryResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardBalanceInquiryResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardBalanceIncreaseResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardBalanceIncreaseResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardBalanceIncreaseResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardDeactivateResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardDeactivateResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardDeactivateResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardChargeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardChargeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardChargeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardCreditResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCreditResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCreditResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardRefundResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardRefundResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCreditResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardCaptureResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCaptureResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardCaptureResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardVoidResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardVoidResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardVoidResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardTimeoutReversalResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardTimeoutReversalResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "GiftCardTimeoutReversalResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teleCheckRiskManagementResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckRiskManagementResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckRiskManagementResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teleCheckECAChargeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECAChargeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECAChargeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teleCheckECAVoidResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECAVoidResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECAVoidResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teleCheckECATimeoutReversalResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECATimeoutReversalResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TeleCheckECATimeoutReversalResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foodStampChargeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampChargeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampChargeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foodStampCreditResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampCreditResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampChargeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foodStampVoucherResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampVoucherResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampVoucherResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foodStampBalanceInquiryResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "FoodStampBalanceInquiryResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitBalanceInquiryResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashBenefitChargeResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitChargeResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitChargeResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashBenefitBalanceInquiryResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitBalanceInquiryResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "CashBenefitBalanceInquiryResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
