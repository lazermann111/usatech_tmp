/**
 * POSTerminalTypeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class POSTerminalTypeType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected POSTerminalTypeType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "on-premise-administrative";
    public static final java.lang.String _value2 = "off-premise-administrative";
    public static final java.lang.String _value3 = "on-premise-point-of-service";
    public static final java.lang.String _value4 = "off-premise-point-of-service";
    public static final java.lang.String _value5 = "on-premise-atm";
    public static final java.lang.String _value6 = "off-premise-atm";
    public static final java.lang.String _value7 = "on-premise-home";
    public static final java.lang.String _value8 = "off-premise-home";
    public static final java.lang.String _value9 = "on-premise-ecr";
    public static final java.lang.String _value10 = "off-premise-ecr";
    public static final java.lang.String _value11 = "on-premise-dial";
    public static final java.lang.String _value12 = "off-premise-dial";
    public static final java.lang.String _value13 = "on-premise-traveler-check";
    public static final java.lang.String _value14 = "off-premise-traveler-check";
    public static final java.lang.String _value15 = "on-premise-fuel";
    public static final java.lang.String _value16 = "off-premise-fuel";
    public static final java.lang.String _value17 = "on-premise-scrip";
    public static final java.lang.String _value18 = "off-premise-scrip";
    public static final java.lang.String _value19 = "on-premise-coupon";
    public static final java.lang.String _value20 = "off-premise-coupon";
    public static final java.lang.String _value21 = "on-premise-ticket";
    public static final java.lang.String _value22 = "off-premise-ticket";
    public static final java.lang.String _value23 = "on-premise-point-of-banking";
    public static final java.lang.String _value24 = "off-premise-point-of-banking";
    public static final java.lang.String _value25 = "voice-response-unit";
    public static final java.lang.String _value26 = "internet-verify-by-visa-or-3des";
    public static final java.lang.String _value27 = "internet-3des";
    public static final java.lang.String _value28 = "internet-ssl";
    public static final java.lang.String _value29 = "internet-nonsecure";
    public static final POSTerminalTypeType value1 = new POSTerminalTypeType(_value1);
    public static final POSTerminalTypeType value2 = new POSTerminalTypeType(_value2);
    public static final POSTerminalTypeType value3 = new POSTerminalTypeType(_value3);
    public static final POSTerminalTypeType value4 = new POSTerminalTypeType(_value4);
    public static final POSTerminalTypeType value5 = new POSTerminalTypeType(_value5);
    public static final POSTerminalTypeType value6 = new POSTerminalTypeType(_value6);
    public static final POSTerminalTypeType value7 = new POSTerminalTypeType(_value7);
    public static final POSTerminalTypeType value8 = new POSTerminalTypeType(_value8);
    public static final POSTerminalTypeType value9 = new POSTerminalTypeType(_value9);
    public static final POSTerminalTypeType value10 = new POSTerminalTypeType(_value10);
    public static final POSTerminalTypeType value11 = new POSTerminalTypeType(_value11);
    public static final POSTerminalTypeType value12 = new POSTerminalTypeType(_value12);
    public static final POSTerminalTypeType value13 = new POSTerminalTypeType(_value13);
    public static final POSTerminalTypeType value14 = new POSTerminalTypeType(_value14);
    public static final POSTerminalTypeType value15 = new POSTerminalTypeType(_value15);
    public static final POSTerminalTypeType value16 = new POSTerminalTypeType(_value16);
    public static final POSTerminalTypeType value17 = new POSTerminalTypeType(_value17);
    public static final POSTerminalTypeType value18 = new POSTerminalTypeType(_value18);
    public static final POSTerminalTypeType value19 = new POSTerminalTypeType(_value19);
    public static final POSTerminalTypeType value20 = new POSTerminalTypeType(_value20);
    public static final POSTerminalTypeType value21 = new POSTerminalTypeType(_value21);
    public static final POSTerminalTypeType value22 = new POSTerminalTypeType(_value22);
    public static final POSTerminalTypeType value23 = new POSTerminalTypeType(_value23);
    public static final POSTerminalTypeType value24 = new POSTerminalTypeType(_value24);
    public static final POSTerminalTypeType value25 = new POSTerminalTypeType(_value25);
    public static final POSTerminalTypeType value26 = new POSTerminalTypeType(_value26);
    public static final POSTerminalTypeType value27 = new POSTerminalTypeType(_value27);
    public static final POSTerminalTypeType value28 = new POSTerminalTypeType(_value28);
    public static final POSTerminalTypeType value29 = new POSTerminalTypeType(_value29);
    public java.lang.String getValue() { return _value_;}
    public static POSTerminalTypeType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        POSTerminalTypeType enumeration = (POSTerminalTypeType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static POSTerminalTypeType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(POSTerminalTypeType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "POSTerminalTypeType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
