/**
 * ResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.firstdata.securetransport;

public class ResponseType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.firstdata.securetransport.RespClientIDType respClientID;

    private com.usatech.iso8583.interchange.firstdata.securetransport.StatusType status;

    private com.usatech.iso8583.interchange.firstdata.securetransport.TransactionResponseType transactionResponse;

    public ResponseType() {
    }

    public ResponseType(
           com.usatech.iso8583.interchange.firstdata.securetransport.RespClientIDType respClientID,
           com.usatech.iso8583.interchange.firstdata.securetransport.StatusType status,
           com.usatech.iso8583.interchange.firstdata.securetransport.TransactionResponseType transactionResponse) {
           this.respClientID = respClientID;
           this.status = status;
           this.transactionResponse = transactionResponse;
    }


    /**
     * Gets the respClientID value for this ResponseType.
     * 
     * @return respClientID
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.RespClientIDType getRespClientID() {
        return respClientID;
    }


    /**
     * Sets the respClientID value for this ResponseType.
     * 
     * @param respClientID
     */
    public void setRespClientID(com.usatech.iso8583.interchange.firstdata.securetransport.RespClientIDType respClientID) {
        this.respClientID = respClientID;
    }


    /**
     * Gets the status value for this ResponseType.
     * 
     * @return status
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.StatusType getStatus() {
        return status;
    }


    /**
     * Sets the status value for this ResponseType.
     * 
     * @param status
     */
    public void setStatus(com.usatech.iso8583.interchange.firstdata.securetransport.StatusType status) {
        this.status = status;
    }


    /**
     * Gets the transactionResponse value for this ResponseType.
     * 
     * @return transactionResponse
     */
    public com.usatech.iso8583.interchange.firstdata.securetransport.TransactionResponseType getTransactionResponse() {
        return transactionResponse;
    }


    /**
     * Sets the transactionResponse value for this ResponseType.
     * 
     * @param transactionResponse
     */
    public void setTransactionResponse(com.usatech.iso8583.interchange.firstdata.securetransport.TransactionResponseType transactionResponse) {
        this.transactionResponse = transactionResponse;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResponseType)) return false;
        ResponseType other = (ResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.respClientID==null && other.getRespClientID()==null) || 
             (this.respClientID!=null &&
              this.respClientID.equals(other.getRespClientID()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.transactionResponse==null && other.getTransactionResponse()==null) || 
             (this.transactionResponse!=null &&
              this.transactionResponse.equals(other.getTransactionResponse())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRespClientID() != null) {
            _hashCode += getRespClientID().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getTransactionResponse() != null) {
            _hashCode += getTransactionResponse().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "ResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respClientID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "RespClientID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "RespClientIDType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "StatusType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionResponse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionResponse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://datawire.net/vxnservice/vxnxml.xsd", "TransactionResponseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
