package com.usatech.iso8583.interchange.tns;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.iso8583.jpos.USATISOChannel;
import com.usatech.iso8583.jpos.USATISOMUX;

public class TNSMUX extends USATISOMUX {
	private static Log log = Log.getLog();

	protected long lastReset = -99;
	protected AtomicLong msgKey = new AtomicLong(currentSeconds());
	
	protected long heartbeatInterval = 180 * 1000;
	protected long heartbeatInitialDelay = 1000;
	protected long inactivityTimeout = 200 * 1000;

	private ScheduledExecutorService connectionManagerExecutor;
	private ScheduledFuture connectionManagerFuture;

	public TNSMUX(USATISOChannel channel, ThreadPoolExecutor threadPool, boolean doConnect) {
		super(channel, threadPool, doConnect);
	}
	
	public long getHeartbeatInterval() {
		return heartbeatInterval;
	}

	public void setHeartbeatInterval(long heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}

	public long getHeartbeatInitialDelay() {
		return heartbeatInitialDelay;
	}

	public void setHeartbeatInitialDelay(long heartbeatInitialDelay) {
		this.heartbeatInitialDelay = heartbeatInitialDelay;
	}

	public long getInactivityTimeout() {
		return inactivityTimeout;
	}

	public void setInactivityTimeout(long inactivityTimeout) {
		this.inactivityTimeout = inactivityTimeout;
	}

	public synchronized void start() {
		connect();
		if(connectionManagerExecutor != null)
			return;

		log.info("Starting up...");

		connectionManagerExecutor = Executors.newSingleThreadScheduledExecutor();
		connectionManagerFuture = connectionManagerExecutor.scheduleWithFixedDelay(new PersistentConnectionManager(), heartbeatInitialDelay, 1000, TimeUnit.MILLISECONDS);
	}

	public void terminate() {
		super.terminate();
		log.warn("Shutting down...");
		connectionManagerExecutor.shutdownNow();
	}

	protected synchronized String getKey(ISOMsg isoMsg) throws ISOException {
		if(getISOChannel().getConnectTime() > lastReset) {
			lastReset = System.currentTimeMillis();
			msgKey.set(currentSeconds());
		}

		TNSMsg tnsMsg = (TNSMsg) isoMsg;
		if(tnsMsg.getDirection() == ISOMsg.INCOMING)
			return tnsMsg.getEchoData();

		String key = String.valueOf(msgKey.getAndIncrement());
		msgKey.compareAndSet(Long.MAX_VALUE, currentSeconds());

		tnsMsg.setEchoData(key);
		return key;
	}

	public String toString() {
		return format("TNSMUX[doConnect={0},connected={1},terminating={2},msgKey={3}]", getConnect(), isConnected(), isTerminating(), msgKey.get());
	}
	
	protected class PersistentConnectionManager implements Runnable {
		public void run() {
			if(!isConnected())
				return;

			USATISOChannel channel = getISOChannel();
			
			long receiveET = System.currentTimeMillis() - (channel.getLastReceive() > 0 ? channel.getLastReceive() : channel.getConnectTime() > 0 ? channel.getConnectTime() : System.currentTimeMillis());
			long sendET = System.currentTimeMillis() - channel.getLastSend();
			if(receiveET > inactivityTimeout) {
				log.warn("Have not received a message from the host in the last {0}ms!  Forcing reconnect...", receiveET);
				try {
					getISOChannel().reconnect();
				} catch(IOException e) {
					log.error("Failed to force reconnect: {0}", e.getMessage(), e);
				}
			} else if(sendET > heartbeatInterval) {
				try {
					TNSMsg heartbeat = new TNSMsg();
					heartbeat.setDirection(ISOMsg.OUTGOING);
					heartbeat.setMTI("0800");

					Date date = new Date();
					heartbeat.setTransmissionDateTime(formatDate(date, "MMddHHmmss"));
					String time = formatDate(date, "HHmmss");
					heartbeat.setSystemTraceNumber(time);
					heartbeat.setTransactionLocalTime(time);
					String formattedDate = formatDate(date, "MMdd");
					heartbeat.setTransactionLocalDate(formattedDate);
					heartbeat.setNetworkManagementInformationCode("301"); //Echo test

					send(heartbeat);

					if (log.isDebugEnabled())
						log.debug("Sent heartbeat");
				} catch(Exception e) {
					log.warn("Failed to send heartbeat", e);
				}
			}
		}
	}
	
	protected String formatDate(Date date, String format) throws IllegalArgumentException {
		if(date == null)
			return null;
		return ((DateFormat) ConvertUtils.getFormat("DATE", format)).format(date);
	}
	
	protected long currentSeconds() {
		return System.currentTimeMillis() / 1000;
	}
}
