package com.usatech.iso8583.interchange.tns;

import java.math.BigInteger;

import org.jpos.iso.ISOComponent;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.layers.common.MessageResponseUtils;

public class TNSMsg extends ISOMsg
{
	public String getPrimaryAccountNumber() throws ISOException
	{
		if (!hasField(2))
			return null;
		return (String) this.getValue(2);
	}

	public void setPrimaryAccountNumber(String value) throws ISOException
	{
		if (value == null || value.length() < 13 || value.length() > 19)
			throw new ISOException("Invalid Primary Account Number: " + value);

		this.set(2, value);
	}

	public String getProcessingCode() throws ISOException
	{
		if (!hasField(3))
			return null;
		return (String) this.getValue(3);
	}

	public void setProcessingCode(String value) throws ISOException
	{
		if (value == null || value.length() != 6)
			throw new ISOException("Invalid Processing Code: " + value);

		this.set(3, value);
	}

	public String getTransactionAmount() throws ISOException
	{
		if (!hasField(4))
			return null;
		return (String) this.getValue(4);
	}

	public void setTransactionAmount(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Transaction Amount: " + value);

		try
		{
			int amt = Integer.parseInt(value);
			if (amt < 0)
				throw new ISOException("Invalid Transaction Amount: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Amount: " + value);
		}

		this.set(4, value);
	}
	
	public String getTransmissionDateTime() throws ISOException
	{
		if (!hasField(7))
			return null;
		return (String) this.getValue(7);
	}

	public void setTransmissionDateTime(String value) throws ISOException
	{
		if (value == null || value.length() != 10)
			throw new ISOException("Invalid Transmission Date and Time: " + value);

		try
		{
			long time = Long.parseLong(value);
			if (time < 0 || time > 9999999999L)
				throw new ISOException("Invalid Transmission Date and Time: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transmission Date and Time: " + value);
		}

		this.set(7, value);
	}	

	public String getSystemTraceNumber() throws ISOException
	{
		if (!hasField(11))
			return null;
		return (String) this.getValue(11);
	}

	public void setSystemTraceNumber(String value) throws ISOException
	{
		if (value == null || value.length() > 6)
			throw new ISOException("Invalid System Trace Number: " + value);

		try
		{
			int traceNo = Integer.parseInt(value);
			if (traceNo < 0 || traceNo > 999999)
				throw new ISOException("Invalid System Trace Number: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid System Trace Number: " + value);
		}

		this.set(11, value);
	}

	public String getTransactionLocalTime() throws ISOException
	{
		if (!hasField(12))
			return null;
		return (String) this.getValue(12);
	}

	public void setTransactionLocalTime(String value) throws ISOException
	{
		if (value == null || value.length() != 6)
			throw new ISOException("Invalid Transaction Local Time: " + value);

		try
		{
			int time = Integer.parseInt(value);
			if (time < 0 || time > 999999)
				throw new ISOException("Invalid Transaction Local Time: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Local Time: " + value);
		}

		this.set(12, value);
	}

	public String getTransactionLocalDate() throws ISOException
	{
		if (!hasField(13))
			return null;
		return (String) this.getValue(13);
	}

	public void setTransactionLocalDate(String value) throws ISOException
	{
		if (value == null || value.length() != 4)
			throw new ISOException("Invalid Transaction Local Date: " + value);

		try
		{
			int date = Integer.parseInt(value);
			if (date < 0 || date > 9999)
				throw new ISOException("Invalid Transaction Local Date: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Local Date: " + value);
		}

		this.set(13, value);
	}

	public String getExpirationDate() throws ISOException
	{
		if (!hasField(14))
			return null;
		return (String) this.getValue(14);
	}

	public void setExpirationDate(String value) throws ISOException
	{
		if (value == null || value.length() != 4)
			throw new ISOException("Invalid Expiration Date: " + value);

		try
		{
			int exp = Integer.parseInt(value);
			if (exp < 0 || exp > 9999)
				throw new ISOException("Invalid Expiration Date: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Expiration Date: " + value);
		}

		this.set(14, value);
	}
	
	public String getSettlementDate() throws ISOException
	{
		if (!hasField(15))
			return null;
		return (String) this.getValue(15);
	}

	public void setSettlementDate(String value) throws ISOException
	{
		if (value == null || value.length() != 4)
			throw new ISOException("Invalid Settlement Date: " + value);

		try
		{
			int date = Integer.parseInt(value);
			if (date < 0 || date > 9999)
				throw new ISOException("Invalid Settlement Date: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Settlement Date: " + value);
		}

		this.set(15, value);
	}
	
	public String getMerchantType() throws ISOException {
		if(!hasField(18))
			return null;
		return (String) getValue(18);
	}

	public void setMerchantType(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid Merchant Type: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Merchant Type: " + value + "; must be all digits");
		set(18, value);
	}

	public String getPointOfServiceEntryMode() throws ISOException
	{
		if (!hasField(22))
			return null;
		return (String) this.getValue(22);
	}

	public void setPointOfServiceEntryMode(String value) throws ISOException
	{
		if (value == null || value.length() != 3)
			throw new ISOException("Invalid Point Of Service Entry Mode: " + value);

		try
		{
			int entryMode = Integer.parseInt(value);
			if (entryMode < 0 || entryMode > 999)
				throw new ISOException("Invalid Point Of Service Entry Mode: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Point Of Service Entry Mode: " + value);
		}

		this.set(22, value);
	}
	
	public String getCardSequenceNumber() throws ISOException
	{
		if (!hasField(23))
			return null;
		return (String) this.getValue(23);
	}

	public void setCardSequenceNumber(String value) throws ISOException
	{
		if (value == null || value.length() != 3)
			throw new ISOException("Invalid Card Sequence Number: " + value);

		try
		{
			int intValue = Integer.parseInt(value);
			if (intValue < 0 || intValue > 999)
				throw new ISOException("Invalid Card Sequence Number: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Card Sequence Number: " + value);
		}

		this.set(23, value);
	}	

	public String getPointOfServiceConditionCode() throws ISOException
	{
		if (!hasField(25))
			return null;
		return (String) this.getValue(25);
	}

	public void setPointOfServiceConditionCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Point Of Service Condition Code: " + value);

		try
		{
			int code = Integer.parseInt(value);
			if (code < 0 || code > 99)
				throw new ISOException("Invalid Point Of Service Condition Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Point Of Service Condition Code: " + value);
		}

		this.set(25, value);
	}
	
	public String getTransactionFee() throws ISOException {
		if(!hasField(28))
			return null;
		return (String) getValue(28);
	}

	public void setTransactionFee(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 9)
			throw new ISOException("Invalid Transaction Fee: " + value);
		set(28, value);
	}
	
	public String getTranProcessingFee() throws ISOException {
		if(!hasField(30))
			return null;
		return (String) getValue(30);
	}

	public void setTranProcessingFee(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 9)
			throw new ISOException("Invalid Tran Processing Fee: " + value);
		set(30, value);
	}
	
	public String getAcquiringInstitutionIDCode() throws ISOException
	{
		if (!hasField(32))
			return null;
		return (String) this.getValue(32);
	}

	public void setAcquiringInstitutionIDCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Acquiring Institution ID Code: " + value);

		try
		{
			long code = Long.parseLong(value);
			if (code < 0 || code > 99999999999L)
				throw new ISOException("Invalid Acquiring Institution ID Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Acquiring Institution ID Code: " + value);
		}

		this.set(32, value);
	}
	
	public String getForwardingInstitutionIDCode() throws ISOException
	{
		if (!hasField(33))
			return null;
		return (String) this.getValue(33);
	}

	public void setForwardingInstitutionIDCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Forwarding Institution ID Code: " + value);

		try
		{
			long code = Long.parseLong(value);
			if (code < 0 || code > 99999999999L)
				throw new ISOException("Invalid Forwarding Institution ID Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Forwarding Institution ID Code: " + value);
		}

		this.set(33, value);
	}

	public String getTrack2Data() throws ISOException
	{
		if (!hasField(35))
			return null;
		return (String) this.getValue(35);
	}

	public void setTrack2Data(String value) throws ISOException
	{
		if (value == null || value.length() < 3 || value.length() > 37)
			throw new ISOException("Invalid Track2 Data: " + value);

		this.set(35, value);
	}

	public String getRetrievalReferenceNumber() throws ISOException
	{
		if (!hasField(37))
			return null;
		return (String) this.getValue(37);
	}

	public void setRetrievalReferenceNumber(String value) throws ISOException
	{
		if (value == null || value.length() > 12)
			throw new ISOException("Invalid Retrieval Reference Number: " + value);

		this.set(37, value);
	}

	public String getAuthorizationIDResponse() throws ISOException
	{
		if (!hasField(38))
			return null;
		return (String) this.getValue(38);
	}

	public void setAuthorizationIDResponse(String value) throws ISOException
	{
		this.set(38, value);
	}

	public String getResponseCode() throws ISOException
	{
		if (!hasField(39))
			return null;
		return (String) this.getValue(39);
	}

	public void setResponseCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Response Code: " + value);

		try
		{
			int code = Integer.parseInt(value);
			if (code < 0 || code > 99)
				throw new ISOException("Invalid Response Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Response Code: " + value);
		}

		this.set(39, value);
	}
	
	public String getServiceRestrictionCode() throws ISOException
	{
		if (!hasField(40))
			return null;
		return (String) this.getValue(40);
	}

	public void setServiceRestrictionCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Service Restriction Code: " + value);

		try
		{
			int code = Integer.parseInt(value);
			if (code < 0 || code > 999)
				throw new ISOException("Invalid Service Restriction Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Service Restriction Code: " + value);
		}

		this.set(40, value);
	}	

	public String getCardAcceptorTerminalID() throws ISOException
	{
		if (!hasField(41))
			return null;
		return (String) this.getValue(41);
	}

	public void setCardAcceptorTerminalID(String value) throws ISOException
	{
		if (value == null || value.length() > 8)
			throw new ISOException("Invalid Card Acceptor Terminal ID: " + value);

		this.set(41, value);
	}

	public String getCardAcceptorIDCode() throws ISOException
	{
		if (!hasField(42))
			return null;
		return (String) this.getValue(42);
	}

	public void setCardAcceptorIDCode(String value) throws ISOException
	{
		if (value == null || value.length() > 15)
			throw new ISOException("Invalid Card Acceptor ID Code: " + value);

		this.set(42, value);
	}
	
	public String getCardAcceptorNameLocation() throws ISOException
	{
		if (!hasField(43))
			return null;
		return (String) this.getValue(43);
	}

	public void setCardAcceptorNameLocation(String value) throws ISOException
	{
		if (value == null || value.length() != 40)
			throw new ISOException("Invalid Card Acceptor Name Location: " + value);

		this.set(43, value);
	}	

	public String getTrack1Data() throws ISOException
	{
		if (!hasField(45))
			return null;
		return (String) this.getValue(45);
	}

	public void setTrack1Data(String value) throws ISOException
	{
		if (value == null || value.length() < 3 || value.length() > 76)
			throw new ISOException("Invalid Track1 Data: " + value);

		this.set(45, value);
	}
	
	public String getAdditionalData() throws ISOException
	{
		if (!hasField(48))
			return null;
		return (String) this.getValue(48);
	}

	public void setAdditionalData(String value) throws ISOException
	{
		if(value == null || value.length() > 999)
			throw new ISOException("Invalid Additional Data: " + value);

		this.set(48, value);
	}
	
	public String getCurrencyCode() throws ISOException {
		if(!hasField(49))
			return null;
		return (String) getValue(49);
	}

	public void setCurrencyCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Currency Code, Transaction: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Currency Code, Transaction: " + value + "; must be all digits");
		set(49, value);
	}

	public String getAdditionalAmounts() throws ISOException
	{
		if (!hasField(54))
			return null;
		return (String) this.getValue(54);
	}

	public void setAdditionalAmounts(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 120)
			throw new ISOException("Invalid Additional Amounts: " + value);	
		
		this.set(54, value);
	}
	
	public String getMessageReasonCode() throws ISOException
	{
		if (!hasField(56))
			return null;
		return (String) this.getValue(56);
	}

	public void setMessageReasonCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Message Reason Code: " + value);

		try
		{
			int number = Integer.parseInt(value);
			if (number < 0 || number > 9999)
				throw new ISOException("Invalid Message Reason Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Message Reason Code: " + value);
		}

		this.set(56, value);
	}	
	
	public String getEchoData() throws ISOException
	{
		if (!hasField(59))
			return null;
		return (String) this.getValue(59);
	}

	public void setEchoData(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 255)
			throw new ISOException("Invalid Echo Data: " + value);

		this.set(59, value);
	}
	
	public String getNetworkManagementInformationCode() throws ISOException
	{
		if (!hasField(70))
			return null;
		return (String) this.getValue(70);
	}

	public void setNetworkManagementInformationCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Network Management Information Code: " + value);

		try
		{
			int code = Integer.parseInt(value);
			if (code < 0 || code > 999)
				throw new ISOException("Invalid Network Management Information Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Network Management Information Code: " + value);
		}

		this.set(70, value);
	}	
	
	public String getOriginalDataElements() throws ISOException
	{
		if (!hasField(90))
			return null;
		return (String) this.getValue(90);
	}

	public void setOriginalDataElements(String value) throws ISOException
	{
		if (value == null || value.length() != 42)
			throw new ISOException("Invalid Original Data Elements: " + value);

		try
		{
			BigInteger number = new BigInteger(value);
			if (number.compareTo(BigInteger.ZERO) < 0)
				throw new ISOException("Invalid Original Data Elements: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Original Data Elements: " + value);
		}

		this.set(90, value);
	}
	
	public String getOriginalAmounts() throws ISOException
	{
		if (!hasField(95))
			return null;
		return (String) this.getValue(95);
	}

	public void setOriginalAmounts(String value) throws ISOException
	{
		if (value == null || value.length() != 42)
			throw new ISOException("Invalid Original Amounts: " + value);

		this.set(95, value);
	}
	
	public String getReceivingInstitutionIDCode() throws ISOException
	{
		if (!hasField(100))
			return null;
		return (String) this.getValue(100);
	}

	public void setReceivingInstitutionIDCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Receiving Institution ID Code: " + value);

		try
		{
			long code = Long.parseLong(value);
			if (code < 0 || code > 99999999999L)
				throw new ISOException("Invalid Receiving Institution ID Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Receiving Institution ID Code: " + value);
		}

		this.set(100, value);
	}	

	public String getPOSDataCode() throws ISOException
	{
		if (!hasField(123))
			return null;
		return (String) this.getValue(123);
	}

	public void setPOSDataCode(String value) throws ISOException
	{
		if (value == null || value.length() != 15)
			throw new ISOException("Invalid POS Data Code: " + value);		

		this.set(123, value);
	}
	
	public String getNetworkManagementInformation() throws ISOException
	{
		if (!hasField(125))
			return null;
		return (String) this.getValue(125);
	}

	public void setNetworkManagementInformation(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 40)
			throw new ISOException("Invalid Network Management Information: " + value);		
		
		this.set(125, value);
	}
	
  public String getStructuredData() throws ISOException
  {
  	ISOMsg field127 = (ISOMsg) getComponent(127);
  	if (field127 == null)
  		return null;
  	return (String) field127.getValue(22);
  }
  
	public void setStructuredData(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Structured Data: " + value);

		ISOMsg field127 = (ISOMsg) getComponent(127);
		if (field127 == null) {
			field127 = new ISOMsg(127);
			field127.set(22, value);
			this.set(field127);
		} else
			field127.set(22, value);
	}
	
  public String getICCData() throws ISOException
  {
  	ISOMsg field127 = (ISOMsg) getComponent(127);
  	if (field127 == null)
  		return null;
  	return (String) field127.getValue(25);
  }
  
	public void setICCData(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid ICCData: " + value);

		ISOMsg field127 = (ISOMsg) getComponent(127);
		if (field127 == null) {
			field127 = new ISOMsg(127);
			field127.set(25, value);
			this.set(field127);
		} else
			field127.set(25, value);
	}	
	
  public String getExtendedTransactionType() throws ISOException
  {
  	ISOMsg field127 = (ISOMsg) getComponent(127);
  	if (field127 == null)
  		return null;
  	return (String) field127.getValue(33);
  }
  
	public void setExtendedTransactionType(String value) throws ISOException
	{
		if (value == null || value.length() != 4)
			throw new ISOException("Invalid Extended Transaction Type: " + value);		

		ISOMsg field127 = (ISOMsg) getComponent(127);
		if (field127 == null) {
			field127 = new ISOMsg(127);
			field127.set(33, value);
			this.set(field127);
		} else
			field127.set(33, value);
	}  

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		try {
			for(int k = 0; k <= maxField; k++) {
				ISOComponent comp = getComponent(k);
				if(comp != null) {
					sb.append(k).append('=');
					Object value = comp.getValue();
					if(value != null) {
						switch(k) {
							case ISO8583Message.BIT_TRACK_2_DATA:
							case ISO8583Message.BIT_TRACK_1_DATA:
								sb.append(MessageResponseUtils.maskTrackData(value.toString(), false));
								break;
							case ISO8583Message.BIT_PRIMARY_ACCOUNT_NUMBER:
								sb.append(MessageResponseUtils.maskCardNumber(value.toString(), false));
								break;
							case ISO8583Message.BIT_EXPIRATION_DATE:
							case ISO8583Message.BIT_PAN_SEQUENCE_NUMBER:
								for(int i = 0; i < value.toString().length(); i++)
									sb.append('*');
								break;
							default:
								if (value instanceof ISOMsg) {
									sb.append('[');
									for(int i = 0; i <= maxField; i++) {
										ISOComponent subComp = ((ISOMsg) value).getComponent(i);
										if (subComp != null) {
											sb.append(i).append('=');
											Object subValue = subComp.getValue();
											if (subValue != null) {
												sb.append(subValue.toString());
												sb.append("; ");
											}
										}
									}
									sb.append(']');
								} else
									sb.append(value.toString());
						}
					}
					sb.append("; ");
				}
			}
		} catch(ISOException e) {
			sb.append('<').append(e.getClass().getName()).append(": ").append(e.getMessage()).append('>');
		}
		sb.append(']');

		return sb.toString();
	}
}
