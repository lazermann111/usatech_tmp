package com.usatech.iso8583.interchange.tns;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class TNSStructuredData {
	protected Map<String, String> values = new HashMap<String, String>();

	public String generateStructuredData() {
		StringBuilder sb = new StringBuilder();
		for (String key: values.keySet()) {
			String value = values.get(key);
			if (!StringUtils.isBlank(value)) {
				String keyLength = String.valueOf(key.length());
				sb.append(keyLength.length());
				sb.append(keyLength);
				sb.append(key);
				String valueLength = String.valueOf(value.length());
				sb.append(valueLength.length());
				sb.append(valueLength);
				sb.append(value);
			}
		}
		return sb.toString();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (String key: values.keySet()) {
			if (sb.length() > 0)
				sb.append(", ");
			sb.append(key);
			sb.append("=");
			sb.append(values.get(key));
		}
		sb.append(']');
		return sb.toString();
	}		
	
	public String getTermAddress() {
		return values.get("TermAddress");
	}
	
	public void setTermAddress(String termAddress) {
		values.put("TermAddress", termAddress);
	}

	public String getTermState() {
		return values.get("TermState");
	}

	public void setTermState(String termState) {
		values.put("TermState", termState);
	}

	public String getTermCity() {
		return values.get("TermCity");
	}

	public void setTermCity(String termCity) {
		values.put("TermCity", termCity);
	}

	public String getTermZipCode() {
		return values.get("TermZipCode");
	}

	public void setTermZipCode(String termZipCode) {
		values.put("TermZipCode", termZipCode);
	}

	public String getTermContactFirstName() {
		return values.get("TermContactFirstName");
	}

	public void setTermContactFirstName(String termContactFirstName) {
		values.put("TermContactFirstName", termContactFirstName);
	}

	public String getTermContactLastName() {
		return values.get("TermContactLastName");
	}

	public void setTermContactLastName(String termContactLastName) {
		values.put("TermContactLastName", termContactLastName);
	}

	public String getTermContactPhone() {
		return values.get("TermContactPhone");
	}

	public void setTermContactPhone(String termContactPhone) {
		values.put("TermContactPhone", termContactPhone);
	}

	public String getCenterName() {
		return values.get("CenterName");
	}

	public void setCenterName(String centerName) {
		values.put("CenterName", centerName);
	}

	public String getCenterNr() {
		return values.get("CenterNr");
	}

	public void setCenterNr(String centerNr) {
		values.put("CenterNr", centerNr);
	}
	
	public String getInteracAuthIdRsp() {
		return values.get("InteracAuthIdRsp");
	}

	public void setInteracAuthIdRsp(String interacAuthIdRsp) {
		values.put("InteracAuthIdRsp", interacAuthIdRsp);
	}	
}
