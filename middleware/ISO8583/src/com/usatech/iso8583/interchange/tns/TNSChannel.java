package com.usatech.iso8583.interchange.tns;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOFilter.VetoException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.channel.PostChannel;
import org.jpos.util.Logger;

import simple.io.Log;

import com.usatech.iso8583.jpos.USATISOChannel;

public class TNSChannel extends PostChannel implements USATISOChannel {
	private static Log log = Log.getLog();

	protected long connectTime = -1;
	protected long lastSend = -1;
	protected long lastReceive = -1;

	public TNSChannel(String host, int port, ISOPackager p) {
		super(host, port, p);
	}

	public TNSChannel(ISOPackager p) throws IOException {
		super(p);
	}

	public TNSChannel(ISOPackager p, ServerSocket serverSocket) throws IOException {
		super(p, serverSocket);
	}

	public String toString() {
		return format("TNSChannel[{0}:{1}]", getHost() , getPort());
	}

	public long getConnectTime() {
		return connectTime;
	}

	// Subclass has a bug that appends the socket host and port to the realm every time it is re-connected
	public void setLogger(Logger logger, String realm) {
		this.logger = logger;
		if(realm == null || !realm.matches(".*/[^:]+:\\d+/[^:]+:\\d+"))
			this.realm = realm;
	}

	public long getLastSend() {
		return lastSend;
	}

	public long getLastReceive() {
		return lastReceive;
	}
	
	@Override
	protected Socket newSocket() throws IOException {
		try {
			Socket s = super.newSocket();
			connectTime = System.currentTimeMillis();
			lastReceive = -1;
			log.info("Successfully connected to {0}:{1}", getHost(), getPort());
			return s;
		} catch(IOException e) {
			log.warn("Failed to connect to {0}:{1}", getHost(), getPort());
			throw e;
		}
	}

	@Override
	public void disconnect() throws IOException {
		log.info("Disconnecting from {0}:{1}", getHost(), getPort());
		super.disconnect();
	}

	@Override
  public void connect() throws IOException {
		log.info("Connecting to {0}:{1}", getHost(), getPort());
		super.connect();
		log.info("Connected to {0}:{1}", getHost(), getPort());
	}

	@Override
  public void reconnect() throws IOException {
		log.info("Reconnecting...");
		super.reconnect();
	}

	@Override
	protected ISOMsg createMsg() {
		return new TNSMsg();
	}

	@Override
	public void send(ISOMsg msg) throws IOException, ISOException, VetoException {
			super.send(msg);
			lastSend = System.currentTimeMillis();
	}

	public ISOMsg receive() throws IOException, ISOException {
		ISOMsg msg;
		try {
			msg = super.receive();
			if ("0810".equalsIgnoreCase(msg.getMTI())) {
				if (log.isDebugEnabled())
					log.debug("Received heartbeat");
			}
			lastReceive = System.currentTimeMillis();
		} catch(IOException e) {
				Socket socket = getSocket();
				if (socket != null)
					socket.close();
				throw e;
		}
		return msg;
	}
}
