package com.usatech.iso8583.interchange.tns;

import java.io.StringWriter;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class TNSICCData {
	protected final static XStream xstream = new XStream(new DomDriver()) {
    // ignore unknown XML tags
		protected MapperWrapper wrapMapper(MapperWrapper next) {
            return new MapperWrapper(next) {
                public boolean shouldSerializeMember(@SuppressWarnings("rawtypes") Class definedIn, String fieldName) {
                    try {
                        return definedIn != Object.class || realClass(fieldName) != null;
                    } catch(CannotResolveClassException cnrce) {
                        return false;
                    }
                }
            };
        }
    };
	
    static {
    	xstream.alias("IccRequest", TNSICCData.class);
    }
    
	public static String toXML(TNSICCData request) {
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><IccData>");
		StringWriter sw = new StringWriter();
		// CompactWriter disables pretty XML printing
		xstream.marshal(request, new CompactWriter(sw));
		sb.append(sw.toString());
		sb.append("</IccData>");
		return sb.toString();
	}
	
	protected String AmountAuthorized;
	protected String AmountOther;
	protected String ApplicationInterchangeProfile;
	protected String ApplicationTransactionCounter;
	protected String Cryptogram;
	protected String CryptogramInformationData;
	protected String CvmResults;
	protected String IssuerApplicationData;
	protected String TerminalCapabilities;
	protected String TerminalCountryCode;
	protected String TerminalType;
	protected String TerminalVerificationResult;
	protected String TransactionCurrencyCode;
	protected String TransactionDate;
	protected String TransactionType;
	protected String UnpredictableNumber;

	public String getAmountAuthorized() {
		return AmountAuthorized;
	}

	public void setAmountAuthorized(String amountAuthorized) {
		AmountAuthorized = amountAuthorized;
	}

	public String getAmountOther() {
		return AmountOther;
	}

	public void setAmountOther(String amountOther) {
		AmountOther = amountOther;
	}

	public String getApplicationInterchangeProfile() {
		return ApplicationInterchangeProfile;
	}

	public void setApplicationInterchangeProfile(String applicationInterchangeProfile) {
		ApplicationInterchangeProfile = applicationInterchangeProfile;
	}

	public String getApplicationTransactionCounter() {
		return ApplicationTransactionCounter;
	}

	public void setApplicationTransactionCounter(String applicationTransactionCounter) {
		ApplicationTransactionCounter = applicationTransactionCounter;
	}

	public String getCryptogram() {
		return Cryptogram;
	}

	public void setCryptogram(String cryptogram) {
		Cryptogram = cryptogram;
	}

	public String getCryptogramInformationData() {
		return CryptogramInformationData;
	}

	public void setCryptogramInformationData(String cryptogramInformationData) {
		CryptogramInformationData = cryptogramInformationData;
	}

	public String getCvmResults() {
		return CvmResults;
	}

	public void setCvmResults(String cvmResults) {
		CvmResults = cvmResults;
	}

	public String getIssuerApplicationData() {
		return IssuerApplicationData;
	}

	public void setIssuerApplicationData(String issuerApplicationData) {
		IssuerApplicationData = issuerApplicationData;
	}

	public String getTerminalCapabilities() {
		return TerminalCapabilities;
	}

	public void setTerminalCapabilities(String terminalCapabilities) {
		TerminalCapabilities = terminalCapabilities;
	}

	public String getTerminalCountryCode() {
		return TerminalCountryCode;
	}

	public void setTerminalCountryCode(String terminalCountryCode) {
		TerminalCountryCode = terminalCountryCode;
	}

	public String getTerminalType() {
		return TerminalType;
	}

	public void setTerminalType(String terminalType) {
		TerminalType = terminalType;
	}

	public String getTerminalVerificationResult() {
		return TerminalVerificationResult;
	}

	public void setTerminalVerificationResult(String terminalVerificationResult) {
		TerminalVerificationResult = terminalVerificationResult;
	}

	public String getTransactionCurrencyCode() {
		return TransactionCurrencyCode;
	}

	public void setTransactionCurrencyCode(String transactionCurrencyCode) {
		TransactionCurrencyCode = transactionCurrencyCode;
	}

	public String getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}

	public String getTransactionType() {
		return TransactionType;
	}

	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}

	public String getUnpredictableNumber() {
		return UnpredictableNumber;
	}

	public void setUnpredictableNumber(String unpredictableNumber) {
		UnpredictableNumber = unpredictableNumber;
	}

	@Override
	public String toString() {
		return new StringBuilder("IccRequest [AmountAuthorized=").append(AmountAuthorized)
				.append(", AmountOther=").append(AmountOther)
				.append(", ApplicationInterchangeProfile=").append(ApplicationInterchangeProfile)
				.append(", ApplicationTransactionCounter=").append(ApplicationTransactionCounter)
				.append(", Cryptogram=").append(Cryptogram)
				.append(", CryptogramInformationData=").append(CryptogramInformationData)
				.append(", CvmResults=").append(CvmResults)
				.append(", IssuerApplicationData=").append(IssuerApplicationData)
				.append(", TerminalCapabilities=").append(TerminalCapabilities)
				.append(", TerminalCountryCode=").append(TerminalCountryCode)
				.append(", TerminalType=").append(TerminalType)
				.append(", TerminalVerificationResult=").append(TerminalVerificationResult)
				.append(", TransactionCurrencyCode=").append(TransactionCurrencyCode)
				.append(", TransactionDate=").append(TransactionDate)
				.append(", TransactionType=").append(TransactionType)
				.append(", UnpredictableNumber=").append(UnpredictableNumber)
				.append("]").toString();
	}
}
