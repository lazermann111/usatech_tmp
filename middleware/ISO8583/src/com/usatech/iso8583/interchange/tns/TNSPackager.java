package com.usatech.iso8583.interchange.tns;

import org.jpos.iso.IFA_AMOUNT;
import org.jpos.iso.IFA_BINARY;
import org.jpos.iso.IFA_LLCHAR;
import org.jpos.iso.IFA_LLLCHAR;
import org.jpos.iso.IFA_LLLLCHAR;
import org.jpos.iso.IFA_LLLLLCHAR;
import org.jpos.iso.IFA_LLLLLLBINARY;
import org.jpos.iso.IFA_LLNUM;
import org.jpos.iso.IFA_NUMERIC;
import org.jpos.iso.IFB_BINARY;
import org.jpos.iso.IFB_BITMAP;
import org.jpos.iso.IF_CHAR;
import org.jpos.iso.ISOBasePackager;
import org.jpos.iso.ISOFieldPackager;
import org.jpos.iso.ISOMsgFieldPackager;
import org.jpos.util.Logger;

public class TNSPackager extends ISOBasePackager {
	    protected PostPrivatePackager p127 = new PostPrivatePackager();
	    protected ISOFieldPackager fld[] = {
		    new IFA_NUMERIC (  4, "MESSAGE TYPE INDICATOR"), //0
		    new IFB_BITMAP  ( 16, "BIT MAP"), //1
		    new IFA_LLNUM   ( 19, "PAN - PRIMARY ACCOUNT NUMBER"), //2
		    new IFA_NUMERIC (  6, "PROCESSING CODE"), //3
		    new IFA_NUMERIC ( 12, "AMOUNT, TRANSACTION"), //4
		    new IFA_NUMERIC ( 12, "AMOUNT, SETTLEMENT"), //5
		    new IFA_NUMERIC ( 12, "AMOUNT, CARDHOLDER BILLING"), //6
		    new IFA_NUMERIC ( 10, "TRANSMISSION DATE AND TIME"), //7
		    new IFA_NUMERIC (  8, "AMOUNT, CARDHOLDER BILLING FEE"), //8
		    new IFA_NUMERIC (  8, "CONVERSION RATE, SETTLEMENT"), //9
		    new IFA_NUMERIC (  8, "CONVERSION RATE, CARDHOLDER BILLING"), //10
		    new IFA_NUMERIC (  6, "SYSTEM TRACE AUDIT NUMBER"), //11
		    new IFA_NUMERIC (  6, "TIME, LOCAL TRANSACTION"), //12
		    new IFA_NUMERIC (  4, "DATE, LOCAL TRANSACTION"), //13
		    new IFA_NUMERIC (  4, "DATE, EXPIRATION"), //14
		    new IFA_NUMERIC (  4, "DATE, SETTLEMENT"), //15
		    new IFA_NUMERIC (  4, "DATE, CONVERSION"), //16
		    new IFA_NUMERIC (  4, "DATE, CAPTURE"), //17
		    new IFA_NUMERIC (  4, "MERCHANT TYPE"), //18
		    new IFA_NUMERIC (  3, "ACQUIRING INSTITUTION COUNTRY CODE"), //19
		    new IFA_NUMERIC (  3, "PAN EXTENDED COUNTRY CODE"), //20
		    new IFA_NUMERIC (  3, "FORWARDING INSTITUTION COUNTRY CODE"), //21
		    new IFA_NUMERIC (  3, "POINT OF SERVICE ENTRY MODE"), //22
		    new IFA_NUMERIC (  3, "CARD SEQUENCE NUMBER"), //23
		    new IFA_NUMERIC (  3, "NETWORK INTERNATIONAL IDENTIFIEER"), //24
		    new IFA_NUMERIC (  2, "POINT OF SERVICE CONDITION CODE"), //25
		    new IFA_NUMERIC (  2, "POINT OF SERVICE PIN CAPTURE CODE"), //26
		    new IFA_NUMERIC (  1, "AUTHORIZATION IDENTIFICATION RESP LEN"), //27
		    new IFA_AMOUNT  (  9, "AMOUNT, TRANSACTION FEE"), //28
		    new IFA_AMOUNT  (  9, "AMOUNT, SETTLEMENT FEE"), //29
		    new IFA_AMOUNT  (  9, "AMOUNT, TRANSACTION PROCESSING FEE"), //30
		    new IFA_AMOUNT  (  9, "AMOUNT, SETTLEMENT PROCESSING FEE"), //31
		    new IFA_LLNUM   ( 11, "ACQUIRING INSTITUTION IDENT CODE"), //32
		    new IFA_LLNUM   ( 11, "FORWARDING INSTITUTION IDENT CODE"), //33
		    new IFA_LLCHAR  ( 28, "PAN EXTENDED"), //34
		    new IFA_LLNUM   ( 37, "TRACK 2 DATA"), //35
		    new IFA_LLLCHAR (104, "TRACK 3 DATA"), //36
		    new IF_CHAR     ( 12, "RETRIEVAL REFERENCE NUMBER"), //37
		    new IF_CHAR     (  6, "AUTHORIZATION ID RESPONSE"), //38
		    new IF_CHAR     (  2, "RESPONSE CODE"), //39
		    new IF_CHAR     (  3, "SERVICE RESTRICTION CODE"), //40
		    new IF_CHAR     (  8, "CARD ACCEPTOR TERMINAL ID"), //41
		    new IF_CHAR     ( 15, "CARD ACCEPTOR ID CODE" ), //42
		    new IF_CHAR     ( 40, "CARD ACCEPTOR NAME/LOCATION"), //43
		    new IFA_LLCHAR  ( 25, "ADITIONAL RESPONSE DATA"), //44
		    new IFA_LLCHAR  ( 76, "TRACK 1 DATA"), //45
		    new IFA_LLLCHAR (999, "ADITIONAL DATA - ISO"), //46
		    new IFA_LLLCHAR (999, "ADITIONAL DATA - NATIONAL"), //47
		    new IFA_LLLCHAR (999, "ADITIONAL DATA - PRIVATE"), //48
		    new IF_CHAR     (  3, "CURRENCY CODE, TRANSACTION"), //49
		    new IF_CHAR     (  3, "CURRENCY CODE, SETTLEMENT"), //50
		    new IF_CHAR     (  3, "CURRENCY CODE, CARDHOLDER BILLING"), //51
		    new IFB_BINARY  (  8, "PIN DATA"), //52
		    new IFB_BINARY  ( 48, "SECURITY RELATED CONTROL INFORMATION"), //53
		    new IFA_LLLCHAR (120, "ADDITIONAL AMOUNTS"), //54
		    new IFA_LLLCHAR (999, "RESERVED ISO"), //55
		    new IFA_LLLCHAR (999, "RESERVED ISO"), //56
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL"), //57
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL"), //58
		    new IFA_LLLCHAR (255, "ECHO DATA"), //59
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE"), //60
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE"), //61
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE"), //62
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE"), //63
		    new IFA_BINARY  (  8, "MESSAGE AUTHENTICATION CODE FIELD"), //64
		    new IFA_BINARY  (  8, "BITMAP, EXTENDED"), //65
		    new IFA_NUMERIC (  1, "SETTLEMENT CODE"), //66
		    new IFA_NUMERIC (  2, "EXTENDED PAYMENT CODE"), //67
		    new IFA_NUMERIC (  3, "RECEIVING INSTITUTION COUNTRY CODE"), //68
		    new IFA_NUMERIC (  3, "SETTLEMENT INSTITUTION COUNTRY CODE"), //69
		    new IFA_NUMERIC (  3, "NETWORK MANAGEMENT INFORMATION CODE"), //70
		    new IFA_NUMERIC (  4, "MESSAGE NUMBER"), //71
		    new IFA_NUMERIC (  4, "MESSAGE NUMBER LAST"), //72
		    new IFA_NUMERIC (  6, "DATE ACTION"), //73
		    new IFA_NUMERIC ( 10, "CREDITS NUMBER"), //74
		    new IFA_NUMERIC ( 10, "CREDITS REVERSAL NUMBER"), //75
		    new IFA_NUMERIC ( 10, "DEBITS NUMBER"), //76
		    new IFA_NUMERIC ( 10, "DEBITS REVERSAL NUMBER"), //77
		    new IFA_NUMERIC ( 10, "TRANSFER NUMBER"), //78
		    new IFA_NUMERIC ( 10, "TRANSFER REVERSAL NUMBER"), //79
		    new IFA_NUMERIC ( 10, "INQUIRIES NUMBER"), //80
		    new IFA_NUMERIC ( 10, "AUTHORIZATION NUMBER"), //81
		    new IFA_NUMERIC ( 12, "CREDITS, PROCESSING FEE AMOUNT"), //82
		    new IFA_NUMERIC ( 12, "CREDITS, TRANSACTION FEE AMOUNT"), //83
		    new IFA_NUMERIC ( 12, "DEBITS, PROCESSING FEE AMOUNT"), //84
		    new IFA_NUMERIC ( 12, "DEBITS, TRANSACTION FEE AMOUNT"), //85
		    new IFA_NUMERIC ( 16, "CREDITS, AMOUNT"), //86
		    new IFA_NUMERIC ( 16, "CREDITS, REVERSAL AMOUNT"), //87
		    new IFA_NUMERIC ( 16, "DEBITS, AMOUNT"), //88
		    new IFA_NUMERIC ( 16, "DEBITS, REVERSAL AMOUNT"), //89
		    new IFA_NUMERIC ( 42, "ORIGINAL DATA ELEMENTS"), //90
		    new IF_CHAR     (  1, "FILE UPDATE CODE"), //91
		    new IF_CHAR     (  2, "FILE SECURITY CODE"), //92
		    new IF_CHAR     (  5, "RESPONSE INDICATOR"), //93
		    new IF_CHAR     (  7, "SERVICE INDICATOR"), //94
		    new IF_CHAR     ( 42, "REPLACEMENT AMOUNTS"), //95
		    new IFA_BINARY  ( 8, "MESSAGE SECURITY CODE"), //96
		    new IFA_AMOUNT  ( 17, "AMOUNT, NET SETTLEMENT"), //97
		    new IF_CHAR     ( 25, "PAYEE"), //98
		    new IFA_LLNUM   ( 11, "SETTLEMENT INSTITUTION IDENT CODE"), //99
		    new IFA_LLNUM   ( 11, "RECEIVING INSTITUTION IDENT CODE"), //100
		    new IFA_LLCHAR  ( 17, "FILE NAME"), //101
		    new IFA_LLCHAR  ( 28, "ACCOUNT IDENTIFICATION 1"), //102
		    new IFA_LLCHAR  ( 28, "ACCOUNT IDENTIFICATION 2"), //103
		    new IFA_LLLCHAR (100, "TRANSACTION DESCRIPTION"), //104
		    new IFA_LLLCHAR (999, "RESERVED ISO USE"), //105
		    new IFA_LLLCHAR (999, "RESERVED ISO USE"), //106
		    new IFA_LLLCHAR (999, "RESERVED ISO USE"), //107
		    new IFA_LLLCHAR (999, "RESERVED ISO USE"), //108
		    new IFA_LLLCHAR (999, "RESERVED ISO USE"), //109
		    new IFA_LLLCHAR (999, "RESERVED ISO USE"), //110
		    new IFA_LLLCHAR (999, "RESERVED ISO USE"), //111
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL USE"), //112
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL USE"), //113
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL USE"   ), //114
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL USE"), //115
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL USE"  ), //116
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL USE"), //117
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL USE"), //118
		    new IFA_LLLCHAR (999, "RESERVED NATIONAL USE"), //119
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE USE"), //120
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE USE"), //121
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE USE"), //122
		    new IFA_LLLCHAR (999, "POS DATA CODE"), //123
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE USE"), //124
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE USE"), //125
		    new IFA_LLLCHAR (999, "RESERVED PRIVATE USE"), //126
		    new ISOMsgFieldPackager(
		        new IFA_LLLLLLBINARY (999999, "RESERVED PRIVATE USE"),
		        p127
		    ), //127
		    new IFB_BINARY  (  8, "MAC EXTENDED") //128
		};
		protected static class PostPrivatePackager extends ISOBasePackager
		{ 
		  protected ISOFieldPackager fld127[] = 
		  {
		      new IF_CHAR     (0,   "PLACEHOLDER"), //0
		      new IFB_BITMAP  (8,   "BITMAP"), //1
		      new IFA_LLCHAR  (32,  "SWITCH KEY"), //2
		      new IF_CHAR     (48,  "ROUTING INFORMATION"), //3
		      new IF_CHAR     (22,  "POS DATA"), //4
		      new IF_CHAR     (73,  "SERVICE STATION DATA"), //5
		      new IFA_NUMERIC (2,   "AUTHORIZATION PROFILE"), //6
		      new IFA_LLCHAR  (50,  "CHECK DATA"), //7
		      new IFA_LLLCHAR (128, "RETENTION DATA"), //8
		      new IFA_LLLCHAR (255, "ADDITIONAL NODE DATA"), //9
		      new IFA_NUMERIC (3,   "CVV2"), //10
		      new IFA_LLCHAR  (32,  "ORIGINAL KEY"), //11
		      new IFA_LLCHAR  (25,  "TERMINAL OWNDER"), //12
		      new IF_CHAR     (17,  "POS GEOGRAPHIC DATA"), //13
		      new IF_CHAR     (8,   "SPONSOR BANK"), //14
		      new IFA_LLCHAR  (29,  "AVS REQUEST"), //15
		      new IF_CHAR     (1,   "AVS RESPONSE"), //16
		      new IFA_LLCHAR  (50,  "CARDHOLDER INFORMATION"), //17
		      new IFA_LLCHAR  (50,  "VALIDATION DATA"), //18
		      new IF_CHAR     (45,  "BANK DETAILS"), //19
		      new IFA_NUMERIC (8,   "AUTHORIZER DATE SETTLEMENT"), //20 
		      new IFA_LLCHAR  (12,  "RECORD IDENTIFICATION"), //21
		      new IFA_LLLLLCHAR(99999, "STRUCTURED DATA"), //22
		      new IF_CHAR     (253, "PAYEE NAME AND ADDRESS"), //23 
		      new IFA_LLCHAR  (28,  "PAYER ACCOUNT INFORMATION"), //24
		      new IFA_LLLLCHAR(8000,"ICC DATA"), //25
          new IFA_LLCHAR  (12,  "ORIGINAL NODE"), //26
          new IF_CHAR     (1,   "CARD VERIFICATION RESULT"), //27
          new IFA_NUMERIC (4,   "AMERICAN EXPRESS CARD IDENTIFIER (CID)"), //28
          new IFB_BINARY  (40,  "3D SECURE DATA"), //29
          new IF_CHAR     (1,   "3D SECURE RESULT"), //30
          new IFA_LLCHAR  (11,  "ISSUER NETWORK ID"), //31
          new IFB_BINARY  (33,  "UCAF DATA"), //32
          new IFA_NUMERIC (4,   "EXTENDED TRANSACTION TYPE"), //33
          new IFA_NUMERIC (2,   "ACCOUNT TYPE QUALIFIERS"), //34
          new IFA_LLCHAR  (11,  "ACQUIRER NETWORK ID"), //35
          new IF_CHAR     (0,   "PLACEHOLDER"), //36
          new IF_CHAR     (0,   "PLACEHOLDER"), //37
          new IF_CHAR     (0,   "PLACEHOLDER"), //38
          new IF_CHAR     (2,   "ORIGINAL RESPONSE CODE") //39
		      };

		      protected PostPrivatePackager()
		      {   super();
		          setFieldPackager(fld127);
		      }
		}
    public TNSPackager() {
        super();
        setFieldPackager(fld);
    }
    public void setLogger (Logger logger, String realm) {
        super.setLogger (logger, realm);
        p127.setLogger (logger, realm + ".PostPrivatePackager");
    }
}