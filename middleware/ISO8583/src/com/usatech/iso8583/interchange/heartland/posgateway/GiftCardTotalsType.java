/**
 * GiftCardTotalsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class GiftCardTotalsType  implements java.io.Serializable {
    private int rspCode;

    private java.lang.String rspText;

    private java.lang.Integer saleCnt;

    private java.math.BigDecimal saleAmt;

    private java.lang.Integer activateCnt;

    private java.math.BigDecimal activateAmt;

    private java.lang.Integer addValueCnt;

    private java.math.BigDecimal addValueAmt;

    private java.lang.Integer voidCnt;

    private java.math.BigDecimal voidAmt;

    private java.lang.Integer deactivateCnt;

    private java.math.BigDecimal deactivateAmt;

    public GiftCardTotalsType() {
    }

    public GiftCardTotalsType(
           int rspCode,
           java.lang.String rspText,
           java.lang.Integer saleCnt,
           java.math.BigDecimal saleAmt,
           java.lang.Integer activateCnt,
           java.math.BigDecimal activateAmt,
           java.lang.Integer addValueCnt,
           java.math.BigDecimal addValueAmt,
           java.lang.Integer voidCnt,
           java.math.BigDecimal voidAmt,
           java.lang.Integer deactivateCnt,
           java.math.BigDecimal deactivateAmt) {
           this.rspCode = rspCode;
           this.rspText = rspText;
           this.saleCnt = saleCnt;
           this.saleAmt = saleAmt;
           this.activateCnt = activateCnt;
           this.activateAmt = activateAmt;
           this.addValueCnt = addValueCnt;
           this.addValueAmt = addValueAmt;
           this.voidCnt = voidCnt;
           this.voidAmt = voidAmt;
           this.deactivateCnt = deactivateCnt;
           this.deactivateAmt = deactivateAmt;
    }


    /**
     * Gets the rspCode value for this GiftCardTotalsType.
     * 
     * @return rspCode
     */
    public int getRspCode() {
        return rspCode;
    }


    /**
     * Sets the rspCode value for this GiftCardTotalsType.
     * 
     * @param rspCode
     */
    public void setRspCode(int rspCode) {
        this.rspCode = rspCode;
    }


    /**
     * Gets the rspText value for this GiftCardTotalsType.
     * 
     * @return rspText
     */
    public java.lang.String getRspText() {
        return rspText;
    }


    /**
     * Sets the rspText value for this GiftCardTotalsType.
     * 
     * @param rspText
     */
    public void setRspText(java.lang.String rspText) {
        this.rspText = rspText;
    }


    /**
     * Gets the saleCnt value for this GiftCardTotalsType.
     * 
     * @return saleCnt
     */
    public java.lang.Integer getSaleCnt() {
        return saleCnt;
    }


    /**
     * Sets the saleCnt value for this GiftCardTotalsType.
     * 
     * @param saleCnt
     */
    public void setSaleCnt(java.lang.Integer saleCnt) {
        this.saleCnt = saleCnt;
    }


    /**
     * Gets the saleAmt value for this GiftCardTotalsType.
     * 
     * @return saleAmt
     */
    public java.math.BigDecimal getSaleAmt() {
        return saleAmt;
    }


    /**
     * Sets the saleAmt value for this GiftCardTotalsType.
     * 
     * @param saleAmt
     */
    public void setSaleAmt(java.math.BigDecimal saleAmt) {
        this.saleAmt = saleAmt;
    }


    /**
     * Gets the activateCnt value for this GiftCardTotalsType.
     * 
     * @return activateCnt
     */
    public java.lang.Integer getActivateCnt() {
        return activateCnt;
    }


    /**
     * Sets the activateCnt value for this GiftCardTotalsType.
     * 
     * @param activateCnt
     */
    public void setActivateCnt(java.lang.Integer activateCnt) {
        this.activateCnt = activateCnt;
    }


    /**
     * Gets the activateAmt value for this GiftCardTotalsType.
     * 
     * @return activateAmt
     */
    public java.math.BigDecimal getActivateAmt() {
        return activateAmt;
    }


    /**
     * Sets the activateAmt value for this GiftCardTotalsType.
     * 
     * @param activateAmt
     */
    public void setActivateAmt(java.math.BigDecimal activateAmt) {
        this.activateAmt = activateAmt;
    }


    /**
     * Gets the addValueCnt value for this GiftCardTotalsType.
     * 
     * @return addValueCnt
     */
    public java.lang.Integer getAddValueCnt() {
        return addValueCnt;
    }


    /**
     * Sets the addValueCnt value for this GiftCardTotalsType.
     * 
     * @param addValueCnt
     */
    public void setAddValueCnt(java.lang.Integer addValueCnt) {
        this.addValueCnt = addValueCnt;
    }


    /**
     * Gets the addValueAmt value for this GiftCardTotalsType.
     * 
     * @return addValueAmt
     */
    public java.math.BigDecimal getAddValueAmt() {
        return addValueAmt;
    }


    /**
     * Sets the addValueAmt value for this GiftCardTotalsType.
     * 
     * @param addValueAmt
     */
    public void setAddValueAmt(java.math.BigDecimal addValueAmt) {
        this.addValueAmt = addValueAmt;
    }


    /**
     * Gets the voidCnt value for this GiftCardTotalsType.
     * 
     * @return voidCnt
     */
    public java.lang.Integer getVoidCnt() {
        return voidCnt;
    }


    /**
     * Sets the voidCnt value for this GiftCardTotalsType.
     * 
     * @param voidCnt
     */
    public void setVoidCnt(java.lang.Integer voidCnt) {
        this.voidCnt = voidCnt;
    }


    /**
     * Gets the voidAmt value for this GiftCardTotalsType.
     * 
     * @return voidAmt
     */
    public java.math.BigDecimal getVoidAmt() {
        return voidAmt;
    }


    /**
     * Sets the voidAmt value for this GiftCardTotalsType.
     * 
     * @param voidAmt
     */
    public void setVoidAmt(java.math.BigDecimal voidAmt) {
        this.voidAmt = voidAmt;
    }


    /**
     * Gets the deactivateCnt value for this GiftCardTotalsType.
     * 
     * @return deactivateCnt
     */
    public java.lang.Integer getDeactivateCnt() {
        return deactivateCnt;
    }


    /**
     * Sets the deactivateCnt value for this GiftCardTotalsType.
     * 
     * @param deactivateCnt
     */
    public void setDeactivateCnt(java.lang.Integer deactivateCnt) {
        this.deactivateCnt = deactivateCnt;
    }


    /**
     * Gets the deactivateAmt value for this GiftCardTotalsType.
     * 
     * @return deactivateAmt
     */
    public java.math.BigDecimal getDeactivateAmt() {
        return deactivateAmt;
    }


    /**
     * Sets the deactivateAmt value for this GiftCardTotalsType.
     * 
     * @param deactivateAmt
     */
    public void setDeactivateAmt(java.math.BigDecimal deactivateAmt) {
        this.deactivateAmt = deactivateAmt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GiftCardTotalsType)) return false;
        GiftCardTotalsType other = (GiftCardTotalsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.rspCode == other.getRspCode() &&
            ((this.rspText==null && other.getRspText()==null) || 
             (this.rspText!=null &&
              this.rspText.equals(other.getRspText()))) &&
            ((this.saleCnt==null && other.getSaleCnt()==null) || 
             (this.saleCnt!=null &&
              this.saleCnt.equals(other.getSaleCnt()))) &&
            ((this.saleAmt==null && other.getSaleAmt()==null) || 
             (this.saleAmt!=null &&
              this.saleAmt.equals(other.getSaleAmt()))) &&
            ((this.activateCnt==null && other.getActivateCnt()==null) || 
             (this.activateCnt!=null &&
              this.activateCnt.equals(other.getActivateCnt()))) &&
            ((this.activateAmt==null && other.getActivateAmt()==null) || 
             (this.activateAmt!=null &&
              this.activateAmt.equals(other.getActivateAmt()))) &&
            ((this.addValueCnt==null && other.getAddValueCnt()==null) || 
             (this.addValueCnt!=null &&
              this.addValueCnt.equals(other.getAddValueCnt()))) &&
            ((this.addValueAmt==null && other.getAddValueAmt()==null) || 
             (this.addValueAmt!=null &&
              this.addValueAmt.equals(other.getAddValueAmt()))) &&
            ((this.voidCnt==null && other.getVoidCnt()==null) || 
             (this.voidCnt!=null &&
              this.voidCnt.equals(other.getVoidCnt()))) &&
            ((this.voidAmt==null && other.getVoidAmt()==null) || 
             (this.voidAmt!=null &&
              this.voidAmt.equals(other.getVoidAmt()))) &&
            ((this.deactivateCnt==null && other.getDeactivateCnt()==null) || 
             (this.deactivateCnt!=null &&
              this.deactivateCnt.equals(other.getDeactivateCnt()))) &&
            ((this.deactivateAmt==null && other.getDeactivateAmt()==null) || 
             (this.deactivateAmt!=null &&
              this.deactivateAmt.equals(other.getDeactivateAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getRspCode();
        if (getRspText() != null) {
            _hashCode += getRspText().hashCode();
        }
        if (getSaleCnt() != null) {
            _hashCode += getSaleCnt().hashCode();
        }
        if (getSaleAmt() != null) {
            _hashCode += getSaleAmt().hashCode();
        }
        if (getActivateCnt() != null) {
            _hashCode += getActivateCnt().hashCode();
        }
        if (getActivateAmt() != null) {
            _hashCode += getActivateAmt().hashCode();
        }
        if (getAddValueCnt() != null) {
            _hashCode += getAddValueCnt().hashCode();
        }
        if (getAddValueAmt() != null) {
            _hashCode += getAddValueAmt().hashCode();
        }
        if (getVoidCnt() != null) {
            _hashCode += getVoidCnt().hashCode();
        }
        if (getVoidAmt() != null) {
            _hashCode += getVoidAmt().hashCode();
        }
        if (getDeactivateCnt() != null) {
            _hashCode += getDeactivateCnt().hashCode();
        }
        if (getDeactivateAmt() != null) {
            _hashCode += getDeactivateAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GiftCardTotalsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardTotalsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activateCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ActivateCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activateAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ActivateAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addValueCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AddValueCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addValueAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AddValueAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voidCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "VoidCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voidAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "VoidAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deactivateCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeactivateCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deactivateAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeactivateAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
