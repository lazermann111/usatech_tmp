/**
 * GiftCardDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class GiftCardDataType  implements java.io.Serializable {
    private java.lang.String trackData;

    private java.lang.String cardNbr;

    private com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData;

    public GiftCardDataType() {
    }

    public GiftCardDataType(
           java.lang.String trackData,
           java.lang.String cardNbr,
           com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData) {
           this.trackData = trackData;
           this.cardNbr = cardNbr;
           this.encryptionData = encryptionData;
    }


    /**
     * Gets the trackData value for this GiftCardDataType.
     * 
     * @return trackData
     */
    public java.lang.String getTrackData() {
        return trackData;
    }


    /**
     * Sets the trackData value for this GiftCardDataType.
     * 
     * @param trackData
     */
    public void setTrackData(java.lang.String trackData) {
        this.trackData = trackData;
    }


    /**
     * Gets the cardNbr value for this GiftCardDataType.
     * 
     * @return cardNbr
     */
    public java.lang.String getCardNbr() {
        return cardNbr;
    }


    /**
     * Sets the cardNbr value for this GiftCardDataType.
     * 
     * @param cardNbr
     */
    public void setCardNbr(java.lang.String cardNbr) {
        this.cardNbr = cardNbr;
    }


    /**
     * Gets the encryptionData value for this GiftCardDataType.
     * 
     * @return encryptionData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType getEncryptionData() {
        return encryptionData;
    }


    /**
     * Sets the encryptionData value for this GiftCardDataType.
     * 
     * @param encryptionData
     */
    public void setEncryptionData(com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData) {
        this.encryptionData = encryptionData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GiftCardDataType)) return false;
        GiftCardDataType other = (GiftCardDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.trackData==null && other.getTrackData()==null) || 
             (this.trackData!=null &&
              this.trackData.equals(other.getTrackData()))) &&
            ((this.cardNbr==null && other.getCardNbr()==null) || 
             (this.cardNbr!=null &&
              this.cardNbr.equals(other.getCardNbr()))) &&
            ((this.encryptionData==null && other.getEncryptionData()==null) || 
             (this.encryptionData!=null &&
              this.encryptionData.equals(other.getEncryptionData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTrackData() != null) {
            _hashCode += getTrackData().hashCode();
        }
        if (getCardNbr() != null) {
            _hashCode += getCardNbr().hashCode();
        }
        if (getEncryptionData() != null) {
            _hashCode += getEncryptionData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GiftCardDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TrackData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptionData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
