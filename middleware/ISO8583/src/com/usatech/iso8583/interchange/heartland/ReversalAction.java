package com.usatech.iso8583.interchange.heartland;

import java.text.ParseException;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType;
import com.usatech.iso8583.interchange.heartland.posgateway.CardDataType;
import com.usatech.iso8583.interchange.heartland.posgateway.CreditReversalReqBlock1Type;
import com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReversalReqType;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10Header;

public class ReversalAction extends HeartlandAction {
	private static int[] requiredFields = { 
		ISO8583Message.FIELD_ACQUIRER_ID, 
		ISO8583Message.FIELD_TERMINAL_ID, 
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_AMOUNT
	};
	
	public ReversalAction(HeartlandInterchange interchange) {
		super(interchange);
	}
	
	@Override
	protected void initTransaction(ISO8583Request isoRequest, PosRequestVer10Transaction transaction) throws ValidationException
	{
		PosCreditReversalReqType tranRequest = new PosCreditReversalReqType();
		CreditReversalReqBlock1Type tranBlock = new CreditReversalReqBlock1Type();
		// authorization amount
		tranBlock.setAmt(getOriginalAmount(isoRequest));
		// settlement amount or new authorized amount after reversal occurs
		tranBlock.setAuthAmt(getTransactionAmount(isoRequest));
		
		String gatewayTxnId = getMiscFieldValue(isoRequest, MISC_FIELD_GATEWAY_TXN_ID);
		if (gatewayTxnId != null && gatewayTxnId.length() > 0)
			tranBlock.setGatewayTxnId(Integer.valueOf(gatewayTxnId));
		else {
			CardDataType cardData = new CardDataType();
			if (isoRequest.hasTrackData())
				cardData.setTrackData(getTrackData(isoRequest));
			else if (isoRequest.hasPanData())
				cardData.setManualEntry(getManualEntry(isoRequest));
			else
				throw new ValidationException("Required Field Not Found: " + MISC_FIELD_GATEWAY_TXN_ID
						+ " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) 
						+ " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));
			tranBlock.setCardData(cardData);
		}
						
		tranRequest.setBlock1(tranBlock);
		transaction.setCreditReversal(tranRequest);
	}

	@Override
	protected boolean parseResponse(PosResponseVer10 resV10, ISO8583Response isoResponse) throws ParseException
	{
		PosResponseVer10Header resV10Header = resV10.getHeader();
		if (resV10Header.getSiteTrace() != null)
			isoResponse.setTraceNumber(Integer.valueOf(resV10Header.getSiteTrace()));
		// we have to treat certain response codes triggered by retransmissions due to timeouts as success...
		if (resV10Header.getGatewayRspCode() == GW_RESPONSE_OK 
				&& resV10.getTransaction() != null
				&& resV10.getTransaction().getCreditReversal() != null) {
			AuthRspStatusType messageResponse = resV10.getTransaction().getCreditReversal();
			String rspCode = messageResponse.getRspCode();
			if (rspCode.equals(ISSUER_RESPONSE_NO_ACTION_TAKEN) || rspCode.equals(ISSUER_RESPONSE_NO_ACTION_TAKEN_DUPLICATE))
				isoResponse.setResponseCode(ISO8583Response.SUCCESS);
			else
				isoResponse.setResponseCode(messageResponse.getRspCode());
			if (messageResponse.getRspText() != null)
				isoResponse.setResponseMessage(messageResponse.getRspText());
			if (messageResponse.getRefNbr() != null)
				isoResponse.setRetrievalReferenceNumber(messageResponse.getRefNbr());
			if (messageResponse.getAuthCode() != null)
				isoResponse.setApprovalCode(messageResponse.getAuthCode());
			if (messageResponse.getAuthAmt() != null)
				isoResponse.setAmount(messageResponse.getAuthAmt().movePointRight(2).intValue());
		} else {
			switch (resV10Header.getGatewayRspCode()) {
				case GW_RESPONSE_INVALID_ORIGINAL_TRAN:
				case GW_RESPONSE_INVALID_REVERSAL_AMOUNT:
					isoResponse.setResponseCode(ISO8583Response.SUCCESS);
					break;
				default:
					isoResponse.setResponseCode(new StringBuilder(GW_RESPONSE_CODE_PREFIX).append(resV10Header.getGatewayRspCode()).toString());
			}
			if (resV10Header.getGatewayRspMsg() != null)
				isoResponse.setResponseMessage(resV10Header.getGatewayRspMsg());
			isoResponse.setRetrievalReferenceNumber(String.valueOf(resV10Header.getGatewayTxnId()));
		}
		return true;
	}

	@Override
	protected int[] getRequiredFields() {
		return requiredFields;
	}
}
