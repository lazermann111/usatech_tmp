/**
 * PosReportOpenAuthsRspTypeDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportOpenAuthsRspTypeDetails  implements java.io.Serializable {
    private int deviceId;

    private int gatewayTxnId;

    private java.lang.String txnStatus;

    private java.util.Calendar txnUtcDT;

    private java.lang.String siteTrace;

    private java.lang.String debitCreditInd;

    private java.lang.String saleReturnInd;

    private java.lang.String cardSwiped;

    private java.lang.String cardType;

    private java.lang.String maskedCardNbr;

    private java.lang.String cardHolderLastName;

    private java.lang.String cardHolderFirstName;

    private java.lang.String cardHolderAddr;

    private java.lang.String cardHolderCity;

    private java.lang.String cardHolderState;

    private java.lang.String cardHolderZip;

    private java.lang.String cardHolderPhone;

    private java.lang.String cardHolderEmail;

    private java.math.BigDecimal amt;

    private java.math.BigDecimal authAmt;

    private java.math.BigDecimal settlementAmt;

    private java.math.BigDecimal gratuityAmtInfo;

    private java.math.BigDecimal cashbackAmtInfo;

    private java.lang.String rspCode;

    private java.lang.String rspText;

    private java.lang.String authCode;

    private java.lang.String AVSRsltCode;

    private java.lang.String CVVRsltCode;

    private java.lang.String directMktInvoiceNbr;

    private int directMktShipMonth;

    private int directMktShipDay;

    private java.lang.String CPCCardHolderPONbr;

    private java.lang.String CPCTaxType;

    private java.math.BigDecimal CPCTaxAmt;

    private com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion;

    private java.util.Calendar txnDT;

    public PosReportOpenAuthsRspTypeDetails() {
    }

    public PosReportOpenAuthsRspTypeDetails(
           int deviceId,
           int gatewayTxnId,
           java.lang.String txnStatus,
           java.util.Calendar txnUtcDT,
           java.lang.String siteTrace,
           java.lang.String debitCreditInd,
           java.lang.String saleReturnInd,
           java.lang.String cardSwiped,
           java.lang.String cardType,
           java.lang.String maskedCardNbr,
           java.lang.String cardHolderLastName,
           java.lang.String cardHolderFirstName,
           java.lang.String cardHolderAddr,
           java.lang.String cardHolderCity,
           java.lang.String cardHolderState,
           java.lang.String cardHolderZip,
           java.lang.String cardHolderPhone,
           java.lang.String cardHolderEmail,
           java.math.BigDecimal amt,
           java.math.BigDecimal authAmt,
           java.math.BigDecimal settlementAmt,
           java.math.BigDecimal gratuityAmtInfo,
           java.math.BigDecimal cashbackAmtInfo,
           java.lang.String rspCode,
           java.lang.String rspText,
           java.lang.String authCode,
           java.lang.String AVSRsltCode,
           java.lang.String CVVRsltCode,
           java.lang.String directMktInvoiceNbr,
           int directMktShipMonth,
           int directMktShipDay,
           java.lang.String CPCCardHolderPONbr,
           java.lang.String CPCTaxType,
           java.math.BigDecimal CPCTaxAmt,
           com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion,
           java.util.Calendar txnDT) {
           this.deviceId = deviceId;
           this.gatewayTxnId = gatewayTxnId;
           this.txnStatus = txnStatus;
           this.txnUtcDT = txnUtcDT;
           this.siteTrace = siteTrace;
           this.debitCreditInd = debitCreditInd;
           this.saleReturnInd = saleReturnInd;
           this.cardSwiped = cardSwiped;
           this.cardType = cardType;
           this.maskedCardNbr = maskedCardNbr;
           this.cardHolderLastName = cardHolderLastName;
           this.cardHolderFirstName = cardHolderFirstName;
           this.cardHolderAddr = cardHolderAddr;
           this.cardHolderCity = cardHolderCity;
           this.cardHolderState = cardHolderState;
           this.cardHolderZip = cardHolderZip;
           this.cardHolderPhone = cardHolderPhone;
           this.cardHolderEmail = cardHolderEmail;
           this.amt = amt;
           this.authAmt = authAmt;
           this.settlementAmt = settlementAmt;
           this.gratuityAmtInfo = gratuityAmtInfo;
           this.cashbackAmtInfo = cashbackAmtInfo;
           this.rspCode = rspCode;
           this.rspText = rspText;
           this.authCode = authCode;
           this.AVSRsltCode = AVSRsltCode;
           this.CVVRsltCode = CVVRsltCode;
           this.directMktInvoiceNbr = directMktInvoiceNbr;
           this.directMktShipMonth = directMktShipMonth;
           this.directMktShipDay = directMktShipDay;
           this.CPCCardHolderPONbr = CPCCardHolderPONbr;
           this.CPCTaxType = CPCTaxType;
           this.CPCTaxAmt = CPCTaxAmt;
           this.tzConversion = tzConversion;
           this.txnDT = txnDT;
    }


    /**
     * Gets the deviceId value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param deviceId
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the gatewayTxnId value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return gatewayTxnId
     */
    public int getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(int gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the txnStatus value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return txnStatus
     */
    public java.lang.String getTxnStatus() {
        return txnStatus;
    }


    /**
     * Sets the txnStatus value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param txnStatus
     */
    public void setTxnStatus(java.lang.String txnStatus) {
        this.txnStatus = txnStatus;
    }


    /**
     * Gets the txnUtcDT value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return txnUtcDT
     */
    public java.util.Calendar getTxnUtcDT() {
        return txnUtcDT;
    }


    /**
     * Sets the txnUtcDT value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param txnUtcDT
     */
    public void setTxnUtcDT(java.util.Calendar txnUtcDT) {
        this.txnUtcDT = txnUtcDT;
    }


    /**
     * Gets the siteTrace value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return siteTrace
     */
    public java.lang.String getSiteTrace() {
        return siteTrace;
    }


    /**
     * Sets the siteTrace value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param siteTrace
     */
    public void setSiteTrace(java.lang.String siteTrace) {
        this.siteTrace = siteTrace;
    }


    /**
     * Gets the debitCreditInd value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return debitCreditInd
     */
    public java.lang.String getDebitCreditInd() {
        return debitCreditInd;
    }


    /**
     * Sets the debitCreditInd value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param debitCreditInd
     */
    public void setDebitCreditInd(java.lang.String debitCreditInd) {
        this.debitCreditInd = debitCreditInd;
    }


    /**
     * Gets the saleReturnInd value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return saleReturnInd
     */
    public java.lang.String getSaleReturnInd() {
        return saleReturnInd;
    }


    /**
     * Sets the saleReturnInd value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param saleReturnInd
     */
    public void setSaleReturnInd(java.lang.String saleReturnInd) {
        this.saleReturnInd = saleReturnInd;
    }


    /**
     * Gets the cardSwiped value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardSwiped
     */
    public java.lang.String getCardSwiped() {
        return cardSwiped;
    }


    /**
     * Sets the cardSwiped value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardSwiped
     */
    public void setCardSwiped(java.lang.String cardSwiped) {
        this.cardSwiped = cardSwiped;
    }


    /**
     * Gets the cardType value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the maskedCardNbr value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return maskedCardNbr
     */
    public java.lang.String getMaskedCardNbr() {
        return maskedCardNbr;
    }


    /**
     * Sets the maskedCardNbr value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param maskedCardNbr
     */
    public void setMaskedCardNbr(java.lang.String maskedCardNbr) {
        this.maskedCardNbr = maskedCardNbr;
    }


    /**
     * Gets the cardHolderLastName value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardHolderLastName
     */
    public java.lang.String getCardHolderLastName() {
        return cardHolderLastName;
    }


    /**
     * Sets the cardHolderLastName value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardHolderLastName
     */
    public void setCardHolderLastName(java.lang.String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }


    /**
     * Gets the cardHolderFirstName value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardHolderFirstName
     */
    public java.lang.String getCardHolderFirstName() {
        return cardHolderFirstName;
    }


    /**
     * Sets the cardHolderFirstName value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardHolderFirstName
     */
    public void setCardHolderFirstName(java.lang.String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }


    /**
     * Gets the cardHolderAddr value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardHolderAddr
     */
    public java.lang.String getCardHolderAddr() {
        return cardHolderAddr;
    }


    /**
     * Sets the cardHolderAddr value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardHolderAddr
     */
    public void setCardHolderAddr(java.lang.String cardHolderAddr) {
        this.cardHolderAddr = cardHolderAddr;
    }


    /**
     * Gets the cardHolderCity value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardHolderCity
     */
    public java.lang.String getCardHolderCity() {
        return cardHolderCity;
    }


    /**
     * Sets the cardHolderCity value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardHolderCity
     */
    public void setCardHolderCity(java.lang.String cardHolderCity) {
        this.cardHolderCity = cardHolderCity;
    }


    /**
     * Gets the cardHolderState value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardHolderState
     */
    public java.lang.String getCardHolderState() {
        return cardHolderState;
    }


    /**
     * Sets the cardHolderState value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardHolderState
     */
    public void setCardHolderState(java.lang.String cardHolderState) {
        this.cardHolderState = cardHolderState;
    }


    /**
     * Gets the cardHolderZip value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardHolderZip
     */
    public java.lang.String getCardHolderZip() {
        return cardHolderZip;
    }


    /**
     * Sets the cardHolderZip value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardHolderZip
     */
    public void setCardHolderZip(java.lang.String cardHolderZip) {
        this.cardHolderZip = cardHolderZip;
    }


    /**
     * Gets the cardHolderPhone value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardHolderPhone
     */
    public java.lang.String getCardHolderPhone() {
        return cardHolderPhone;
    }


    /**
     * Sets the cardHolderPhone value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardHolderPhone
     */
    public void setCardHolderPhone(java.lang.String cardHolderPhone) {
        this.cardHolderPhone = cardHolderPhone;
    }


    /**
     * Gets the cardHolderEmail value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cardHolderEmail
     */
    public java.lang.String getCardHolderEmail() {
        return cardHolderEmail;
    }


    /**
     * Sets the cardHolderEmail value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cardHolderEmail
     */
    public void setCardHolderEmail(java.lang.String cardHolderEmail) {
        this.cardHolderEmail = cardHolderEmail;
    }


    /**
     * Gets the amt value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the authAmt value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return authAmt
     */
    public java.math.BigDecimal getAuthAmt() {
        return authAmt;
    }


    /**
     * Sets the authAmt value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param authAmt
     */
    public void setAuthAmt(java.math.BigDecimal authAmt) {
        this.authAmt = authAmt;
    }


    /**
     * Gets the settlementAmt value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return settlementAmt
     */
    public java.math.BigDecimal getSettlementAmt() {
        return settlementAmt;
    }


    /**
     * Sets the settlementAmt value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param settlementAmt
     */
    public void setSettlementAmt(java.math.BigDecimal settlementAmt) {
        this.settlementAmt = settlementAmt;
    }


    /**
     * Gets the gratuityAmtInfo value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return gratuityAmtInfo
     */
    public java.math.BigDecimal getGratuityAmtInfo() {
        return gratuityAmtInfo;
    }


    /**
     * Sets the gratuityAmtInfo value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param gratuityAmtInfo
     */
    public void setGratuityAmtInfo(java.math.BigDecimal gratuityAmtInfo) {
        this.gratuityAmtInfo = gratuityAmtInfo;
    }


    /**
     * Gets the cashbackAmtInfo value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return cashbackAmtInfo
     */
    public java.math.BigDecimal getCashbackAmtInfo() {
        return cashbackAmtInfo;
    }


    /**
     * Sets the cashbackAmtInfo value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param cashbackAmtInfo
     */
    public void setCashbackAmtInfo(java.math.BigDecimal cashbackAmtInfo) {
        this.cashbackAmtInfo = cashbackAmtInfo;
    }


    /**
     * Gets the rspCode value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return rspCode
     */
    public java.lang.String getRspCode() {
        return rspCode;
    }


    /**
     * Sets the rspCode value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param rspCode
     */
    public void setRspCode(java.lang.String rspCode) {
        this.rspCode = rspCode;
    }


    /**
     * Gets the rspText value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return rspText
     */
    public java.lang.String getRspText() {
        return rspText;
    }


    /**
     * Sets the rspText value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param rspText
     */
    public void setRspText(java.lang.String rspText) {
        this.rspText = rspText;
    }


    /**
     * Gets the authCode value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return authCode
     */
    public java.lang.String getAuthCode() {
        return authCode;
    }


    /**
     * Sets the authCode value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param authCode
     */
    public void setAuthCode(java.lang.String authCode) {
        this.authCode = authCode;
    }


    /**
     * Gets the AVSRsltCode value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return AVSRsltCode
     */
    public java.lang.String getAVSRsltCode() {
        return AVSRsltCode;
    }


    /**
     * Sets the AVSRsltCode value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param AVSRsltCode
     */
    public void setAVSRsltCode(java.lang.String AVSRsltCode) {
        this.AVSRsltCode = AVSRsltCode;
    }


    /**
     * Gets the CVVRsltCode value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return CVVRsltCode
     */
    public java.lang.String getCVVRsltCode() {
        return CVVRsltCode;
    }


    /**
     * Sets the CVVRsltCode value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param CVVRsltCode
     */
    public void setCVVRsltCode(java.lang.String CVVRsltCode) {
        this.CVVRsltCode = CVVRsltCode;
    }


    /**
     * Gets the directMktInvoiceNbr value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return directMktInvoiceNbr
     */
    public java.lang.String getDirectMktInvoiceNbr() {
        return directMktInvoiceNbr;
    }


    /**
     * Sets the directMktInvoiceNbr value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param directMktInvoiceNbr
     */
    public void setDirectMktInvoiceNbr(java.lang.String directMktInvoiceNbr) {
        this.directMktInvoiceNbr = directMktInvoiceNbr;
    }


    /**
     * Gets the directMktShipMonth value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return directMktShipMonth
     */
    public int getDirectMktShipMonth() {
        return directMktShipMonth;
    }


    /**
     * Sets the directMktShipMonth value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param directMktShipMonth
     */
    public void setDirectMktShipMonth(int directMktShipMonth) {
        this.directMktShipMonth = directMktShipMonth;
    }


    /**
     * Gets the directMktShipDay value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return directMktShipDay
     */
    public int getDirectMktShipDay() {
        return directMktShipDay;
    }


    /**
     * Sets the directMktShipDay value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param directMktShipDay
     */
    public void setDirectMktShipDay(int directMktShipDay) {
        this.directMktShipDay = directMktShipDay;
    }


    /**
     * Gets the CPCCardHolderPONbr value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return CPCCardHolderPONbr
     */
    public java.lang.String getCPCCardHolderPONbr() {
        return CPCCardHolderPONbr;
    }


    /**
     * Sets the CPCCardHolderPONbr value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param CPCCardHolderPONbr
     */
    public void setCPCCardHolderPONbr(java.lang.String CPCCardHolderPONbr) {
        this.CPCCardHolderPONbr = CPCCardHolderPONbr;
    }


    /**
     * Gets the CPCTaxType value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return CPCTaxType
     */
    public java.lang.String getCPCTaxType() {
        return CPCTaxType;
    }


    /**
     * Sets the CPCTaxType value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param CPCTaxType
     */
    public void setCPCTaxType(java.lang.String CPCTaxType) {
        this.CPCTaxType = CPCTaxType;
    }


    /**
     * Gets the CPCTaxAmt value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return CPCTaxAmt
     */
    public java.math.BigDecimal getCPCTaxAmt() {
        return CPCTaxAmt;
    }


    /**
     * Sets the CPCTaxAmt value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param CPCTaxAmt
     */
    public void setCPCTaxAmt(java.math.BigDecimal CPCTaxAmt) {
        this.CPCTaxAmt = CPCTaxAmt;
    }


    /**
     * Gets the tzConversion value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return tzConversion
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType getTzConversion() {
        return tzConversion;
    }


    /**
     * Sets the tzConversion value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param tzConversion
     */
    public void setTzConversion(com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
        this.tzConversion = tzConversion;
    }


    /**
     * Gets the txnDT value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @return txnDT
     */
    public java.util.Calendar getTxnDT() {
        return txnDT;
    }


    /**
     * Sets the txnDT value for this PosReportOpenAuthsRspTypeDetails.
     * 
     * @param txnDT
     */
    public void setTxnDT(java.util.Calendar txnDT) {
        this.txnDT = txnDT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportOpenAuthsRspTypeDetails)) return false;
        PosReportOpenAuthsRspTypeDetails other = (PosReportOpenAuthsRspTypeDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.deviceId == other.getDeviceId() &&
            this.gatewayTxnId == other.getGatewayTxnId() &&
            ((this.txnStatus==null && other.getTxnStatus()==null) || 
             (this.txnStatus!=null &&
              this.txnStatus.equals(other.getTxnStatus()))) &&
            ((this.txnUtcDT==null && other.getTxnUtcDT()==null) || 
             (this.txnUtcDT!=null &&
              this.txnUtcDT.equals(other.getTxnUtcDT()))) &&
            ((this.siteTrace==null && other.getSiteTrace()==null) || 
             (this.siteTrace!=null &&
              this.siteTrace.equals(other.getSiteTrace()))) &&
            ((this.debitCreditInd==null && other.getDebitCreditInd()==null) || 
             (this.debitCreditInd!=null &&
              this.debitCreditInd.equals(other.getDebitCreditInd()))) &&
            ((this.saleReturnInd==null && other.getSaleReturnInd()==null) || 
             (this.saleReturnInd!=null &&
              this.saleReturnInd.equals(other.getSaleReturnInd()))) &&
            ((this.cardSwiped==null && other.getCardSwiped()==null) || 
             (this.cardSwiped!=null &&
              this.cardSwiped.equals(other.getCardSwiped()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.maskedCardNbr==null && other.getMaskedCardNbr()==null) || 
             (this.maskedCardNbr!=null &&
              this.maskedCardNbr.equals(other.getMaskedCardNbr()))) &&
            ((this.cardHolderLastName==null && other.getCardHolderLastName()==null) || 
             (this.cardHolderLastName!=null &&
              this.cardHolderLastName.equals(other.getCardHolderLastName()))) &&
            ((this.cardHolderFirstName==null && other.getCardHolderFirstName()==null) || 
             (this.cardHolderFirstName!=null &&
              this.cardHolderFirstName.equals(other.getCardHolderFirstName()))) &&
            ((this.cardHolderAddr==null && other.getCardHolderAddr()==null) || 
             (this.cardHolderAddr!=null &&
              this.cardHolderAddr.equals(other.getCardHolderAddr()))) &&
            ((this.cardHolderCity==null && other.getCardHolderCity()==null) || 
             (this.cardHolderCity!=null &&
              this.cardHolderCity.equals(other.getCardHolderCity()))) &&
            ((this.cardHolderState==null && other.getCardHolderState()==null) || 
             (this.cardHolderState!=null &&
              this.cardHolderState.equals(other.getCardHolderState()))) &&
            ((this.cardHolderZip==null && other.getCardHolderZip()==null) || 
             (this.cardHolderZip!=null &&
              this.cardHolderZip.equals(other.getCardHolderZip()))) &&
            ((this.cardHolderPhone==null && other.getCardHolderPhone()==null) || 
             (this.cardHolderPhone!=null &&
              this.cardHolderPhone.equals(other.getCardHolderPhone()))) &&
            ((this.cardHolderEmail==null && other.getCardHolderEmail()==null) || 
             (this.cardHolderEmail!=null &&
              this.cardHolderEmail.equals(other.getCardHolderEmail()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.authAmt==null && other.getAuthAmt()==null) || 
             (this.authAmt!=null &&
              this.authAmt.equals(other.getAuthAmt()))) &&
            ((this.settlementAmt==null && other.getSettlementAmt()==null) || 
             (this.settlementAmt!=null &&
              this.settlementAmt.equals(other.getSettlementAmt()))) &&
            ((this.gratuityAmtInfo==null && other.getGratuityAmtInfo()==null) || 
             (this.gratuityAmtInfo!=null &&
              this.gratuityAmtInfo.equals(other.getGratuityAmtInfo()))) &&
            ((this.cashbackAmtInfo==null && other.getCashbackAmtInfo()==null) || 
             (this.cashbackAmtInfo!=null &&
              this.cashbackAmtInfo.equals(other.getCashbackAmtInfo()))) &&
            ((this.rspCode==null && other.getRspCode()==null) || 
             (this.rspCode!=null &&
              this.rspCode.equals(other.getRspCode()))) &&
            ((this.rspText==null && other.getRspText()==null) || 
             (this.rspText!=null &&
              this.rspText.equals(other.getRspText()))) &&
            ((this.authCode==null && other.getAuthCode()==null) || 
             (this.authCode!=null &&
              this.authCode.equals(other.getAuthCode()))) &&
            ((this.AVSRsltCode==null && other.getAVSRsltCode()==null) || 
             (this.AVSRsltCode!=null &&
              this.AVSRsltCode.equals(other.getAVSRsltCode()))) &&
            ((this.CVVRsltCode==null && other.getCVVRsltCode()==null) || 
             (this.CVVRsltCode!=null &&
              this.CVVRsltCode.equals(other.getCVVRsltCode()))) &&
            ((this.directMktInvoiceNbr==null && other.getDirectMktInvoiceNbr()==null) || 
             (this.directMktInvoiceNbr!=null &&
              this.directMktInvoiceNbr.equals(other.getDirectMktInvoiceNbr()))) &&
            this.directMktShipMonth == other.getDirectMktShipMonth() &&
            this.directMktShipDay == other.getDirectMktShipDay() &&
            ((this.CPCCardHolderPONbr==null && other.getCPCCardHolderPONbr()==null) || 
             (this.CPCCardHolderPONbr!=null &&
              this.CPCCardHolderPONbr.equals(other.getCPCCardHolderPONbr()))) &&
            ((this.CPCTaxType==null && other.getCPCTaxType()==null) || 
             (this.CPCTaxType!=null &&
              this.CPCTaxType.equals(other.getCPCTaxType()))) &&
            ((this.CPCTaxAmt==null && other.getCPCTaxAmt()==null) || 
             (this.CPCTaxAmt!=null &&
              this.CPCTaxAmt.equals(other.getCPCTaxAmt()))) &&
            ((this.tzConversion==null && other.getTzConversion()==null) || 
             (this.tzConversion!=null &&
              this.tzConversion.equals(other.getTzConversion()))) &&
            ((this.txnDT==null && other.getTxnDT()==null) || 
             (this.txnDT!=null &&
              this.txnDT.equals(other.getTxnDT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getDeviceId();
        _hashCode += getGatewayTxnId();
        if (getTxnStatus() != null) {
            _hashCode += getTxnStatus().hashCode();
        }
        if (getTxnUtcDT() != null) {
            _hashCode += getTxnUtcDT().hashCode();
        }
        if (getSiteTrace() != null) {
            _hashCode += getSiteTrace().hashCode();
        }
        if (getDebitCreditInd() != null) {
            _hashCode += getDebitCreditInd().hashCode();
        }
        if (getSaleReturnInd() != null) {
            _hashCode += getSaleReturnInd().hashCode();
        }
        if (getCardSwiped() != null) {
            _hashCode += getCardSwiped().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getMaskedCardNbr() != null) {
            _hashCode += getMaskedCardNbr().hashCode();
        }
        if (getCardHolderLastName() != null) {
            _hashCode += getCardHolderLastName().hashCode();
        }
        if (getCardHolderFirstName() != null) {
            _hashCode += getCardHolderFirstName().hashCode();
        }
        if (getCardHolderAddr() != null) {
            _hashCode += getCardHolderAddr().hashCode();
        }
        if (getCardHolderCity() != null) {
            _hashCode += getCardHolderCity().hashCode();
        }
        if (getCardHolderState() != null) {
            _hashCode += getCardHolderState().hashCode();
        }
        if (getCardHolderZip() != null) {
            _hashCode += getCardHolderZip().hashCode();
        }
        if (getCardHolderPhone() != null) {
            _hashCode += getCardHolderPhone().hashCode();
        }
        if (getCardHolderEmail() != null) {
            _hashCode += getCardHolderEmail().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getAuthAmt() != null) {
            _hashCode += getAuthAmt().hashCode();
        }
        if (getSettlementAmt() != null) {
            _hashCode += getSettlementAmt().hashCode();
        }
        if (getGratuityAmtInfo() != null) {
            _hashCode += getGratuityAmtInfo().hashCode();
        }
        if (getCashbackAmtInfo() != null) {
            _hashCode += getCashbackAmtInfo().hashCode();
        }
        if (getRspCode() != null) {
            _hashCode += getRspCode().hashCode();
        }
        if (getRspText() != null) {
            _hashCode += getRspText().hashCode();
        }
        if (getAuthCode() != null) {
            _hashCode += getAuthCode().hashCode();
        }
        if (getAVSRsltCode() != null) {
            _hashCode += getAVSRsltCode().hashCode();
        }
        if (getCVVRsltCode() != null) {
            _hashCode += getCVVRsltCode().hashCode();
        }
        if (getDirectMktInvoiceNbr() != null) {
            _hashCode += getDirectMktInvoiceNbr().hashCode();
        }
        _hashCode += getDirectMktShipMonth();
        _hashCode += getDirectMktShipDay();
        if (getCPCCardHolderPONbr() != null) {
            _hashCode += getCPCCardHolderPONbr().hashCode();
        }
        if (getCPCTaxType() != null) {
            _hashCode += getCPCTaxType().hashCode();
        }
        if (getCPCTaxAmt() != null) {
            _hashCode += getCPCTaxAmt().hashCode();
        }
        if (getTzConversion() != null) {
            _hashCode += getTzConversion().hashCode();
        }
        if (getTxnDT() != null) {
            _hashCode += getTxnDT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportOpenAuthsRspTypeDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportOpenAuthsRspType>Details"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteTrace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteTrace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCreditInd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitCreditInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleReturnInd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleReturnInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardSwiped");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardSwiped"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maskedCardNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MaskedCardNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderZip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderZip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("settlementAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SettlementAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gratuityAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GratuityAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashbackAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CashbackAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSRsltCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AVSRsltCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVVRsltCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CVVRsltCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktInvoiceNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktInvoiceNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktShipMonth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktShipMonth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktShipDay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktShipDay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCCardHolderPONbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCCardHolderPONbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCTaxType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCTaxType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCTaxAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCTaxAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tzConversion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TzConversion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "tzoneConversionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
