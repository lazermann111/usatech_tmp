/**
 * PosRequestVer10Transaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosRequestVer10Transaction  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditVoidReqType creditVoid;

    private java.lang.String giftCardCurrentDayTotals;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineSaleReqType creditOfflineSale;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReturnReqType creditReturn;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReversalReqType creditReversal;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditSaleReqType creditSale;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditTxnEditReqType creditTxnEdit;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosDebitAddValueReqType debitAddValue;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceReqType giftCardReplace;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditIncrementalAuthReqType creditIncrementalAuth;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReturnReqType debitReturn;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReversalReqType debitReversal;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosDebitSaleReqType debitSale;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosEBTBalanceInquiryReqType EBTBalanceInquiry;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidBalanceInquiryReqType prePaidBalanceInquiry;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditCPCEditReqType creditCPCEdit;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBenefitWithdrawalReqType EBTCashBenefitWithdrawal;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSPurchaseReqType EBTFSPurchase;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSReturnReqType EBTFSReturn;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSVoucherReqType EBTVoucherPurchase;

    private java.lang.Object endToEndTest;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateReqType giftCardActivate;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueReqType giftCardAddValue;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceReqType giftCardBalance;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryReqType reportBatchHistory;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateReqType giftCardDeactivate;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineAuthReqType creditOfflineAuth;

    private java.lang.String giftCardPreviousDayTotals;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleReqType giftCardSale;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidReqType giftCardVoid;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidAddValueReqType prePaidAddValue;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBackPurchaseReqType EBTCashBackPurchase;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosRecurringBillReqType recurringBilling;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityReqType reportActivity;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailReqType reportBatchDetail;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAuthReqType creditAuth;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsReqType reportOpenAuths;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAddToBatchReqType creditAddToBatch;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailReqType reportTxnDetail;

    private java.lang.String testCredentials;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryReqType reportBatchSummary;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchReqType reportSearch;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosAddAttachmentReqType addAttachment;

    private java.lang.String batchClose;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAccountVerifyReqType creditAccountVerify;

    public PosRequestVer10Transaction() {
    }

    public PosRequestVer10Transaction(
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditVoidReqType creditVoid,
           java.lang.String giftCardCurrentDayTotals,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineSaleReqType creditOfflineSale,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReturnReqType creditReturn,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReversalReqType creditReversal,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditSaleReqType creditSale,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditTxnEditReqType creditTxnEdit,
           com.usatech.iso8583.interchange.heartland.posgateway.PosDebitAddValueReqType debitAddValue,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceReqType giftCardReplace,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditIncrementalAuthReqType creditIncrementalAuth,
           com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReturnReqType debitReturn,
           com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReversalReqType debitReversal,
           com.usatech.iso8583.interchange.heartland.posgateway.PosDebitSaleReqType debitSale,
           com.usatech.iso8583.interchange.heartland.posgateway.PosEBTBalanceInquiryReqType EBTBalanceInquiry,
           com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidBalanceInquiryReqType prePaidBalanceInquiry,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditCPCEditReqType creditCPCEdit,
           com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBenefitWithdrawalReqType EBTCashBenefitWithdrawal,
           com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSPurchaseReqType EBTFSPurchase,
           com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSReturnReqType EBTFSReturn,
           com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSVoucherReqType EBTVoucherPurchase,
           java.lang.Object endToEndTest,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateReqType giftCardActivate,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueReqType giftCardAddValue,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceReqType giftCardBalance,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryReqType reportBatchHistory,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateReqType giftCardDeactivate,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineAuthReqType creditOfflineAuth,
           java.lang.String giftCardPreviousDayTotals,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleReqType giftCardSale,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidReqType giftCardVoid,
           com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidAddValueReqType prePaidAddValue,
           com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBackPurchaseReqType EBTCashBackPurchase,
           com.usatech.iso8583.interchange.heartland.posgateway.PosRecurringBillReqType recurringBilling,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityReqType reportActivity,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailReqType reportBatchDetail,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAuthReqType creditAuth,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsReqType reportOpenAuths,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAddToBatchReqType creditAddToBatch,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailReqType reportTxnDetail,
           java.lang.String testCredentials,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryReqType reportBatchSummary,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchReqType reportSearch,
           com.usatech.iso8583.interchange.heartland.posgateway.PosAddAttachmentReqType addAttachment,
           java.lang.String batchClose,
           com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAccountVerifyReqType creditAccountVerify) {
           this.creditVoid = creditVoid;
           this.giftCardCurrentDayTotals = giftCardCurrentDayTotals;
           this.creditOfflineSale = creditOfflineSale;
           this.creditReturn = creditReturn;
           this.creditReversal = creditReversal;
           this.creditSale = creditSale;
           this.creditTxnEdit = creditTxnEdit;
           this.debitAddValue = debitAddValue;
           this.giftCardReplace = giftCardReplace;
           this.creditIncrementalAuth = creditIncrementalAuth;
           this.debitReturn = debitReturn;
           this.debitReversal = debitReversal;
           this.debitSale = debitSale;
           this.EBTBalanceInquiry = EBTBalanceInquiry;
           this.prePaidBalanceInquiry = prePaidBalanceInquiry;
           this.creditCPCEdit = creditCPCEdit;
           this.EBTCashBenefitWithdrawal = EBTCashBenefitWithdrawal;
           this.EBTFSPurchase = EBTFSPurchase;
           this.EBTFSReturn = EBTFSReturn;
           this.EBTVoucherPurchase = EBTVoucherPurchase;
           this.endToEndTest = endToEndTest;
           this.giftCardActivate = giftCardActivate;
           this.giftCardAddValue = giftCardAddValue;
           this.giftCardBalance = giftCardBalance;
           this.reportBatchHistory = reportBatchHistory;
           this.giftCardDeactivate = giftCardDeactivate;
           this.creditOfflineAuth = creditOfflineAuth;
           this.giftCardPreviousDayTotals = giftCardPreviousDayTotals;
           this.giftCardSale = giftCardSale;
           this.giftCardVoid = giftCardVoid;
           this.prePaidAddValue = prePaidAddValue;
           this.EBTCashBackPurchase = EBTCashBackPurchase;
           this.recurringBilling = recurringBilling;
           this.reportActivity = reportActivity;
           this.reportBatchDetail = reportBatchDetail;
           this.creditAuth = creditAuth;
           this.reportOpenAuths = reportOpenAuths;
           this.creditAddToBatch = creditAddToBatch;
           this.reportTxnDetail = reportTxnDetail;
           this.testCredentials = testCredentials;
           this.reportBatchSummary = reportBatchSummary;
           this.reportSearch = reportSearch;
           this.addAttachment = addAttachment;
           this.batchClose = batchClose;
           this.creditAccountVerify = creditAccountVerify;
    }


    /**
     * Gets the creditVoid value for this PosRequestVer10Transaction.
     * 
     * @return creditVoid
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditVoidReqType getCreditVoid() {
        return creditVoid;
    }


    /**
     * Sets the creditVoid value for this PosRequestVer10Transaction.
     * 
     * @param creditVoid
     */
    public void setCreditVoid(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditVoidReqType creditVoid) {
        this.creditVoid = creditVoid;
    }


    /**
     * Gets the giftCardCurrentDayTotals value for this PosRequestVer10Transaction.
     * 
     * @return giftCardCurrentDayTotals
     */
    public java.lang.String getGiftCardCurrentDayTotals() {
        return giftCardCurrentDayTotals;
    }


    /**
     * Sets the giftCardCurrentDayTotals value for this PosRequestVer10Transaction.
     * 
     * @param giftCardCurrentDayTotals
     */
    public void setGiftCardCurrentDayTotals(java.lang.String giftCardCurrentDayTotals) {
        this.giftCardCurrentDayTotals = giftCardCurrentDayTotals;
    }


    /**
     * Gets the creditOfflineSale value for this PosRequestVer10Transaction.
     * 
     * @return creditOfflineSale
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineSaleReqType getCreditOfflineSale() {
        return creditOfflineSale;
    }


    /**
     * Sets the creditOfflineSale value for this PosRequestVer10Transaction.
     * 
     * @param creditOfflineSale
     */
    public void setCreditOfflineSale(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineSaleReqType creditOfflineSale) {
        this.creditOfflineSale = creditOfflineSale;
    }


    /**
     * Gets the creditReturn value for this PosRequestVer10Transaction.
     * 
     * @return creditReturn
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReturnReqType getCreditReturn() {
        return creditReturn;
    }


    /**
     * Sets the creditReturn value for this PosRequestVer10Transaction.
     * 
     * @param creditReturn
     */
    public void setCreditReturn(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReturnReqType creditReturn) {
        this.creditReturn = creditReturn;
    }


    /**
     * Gets the creditReversal value for this PosRequestVer10Transaction.
     * 
     * @return creditReversal
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReversalReqType getCreditReversal() {
        return creditReversal;
    }


    /**
     * Sets the creditReversal value for this PosRequestVer10Transaction.
     * 
     * @param creditReversal
     */
    public void setCreditReversal(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReversalReqType creditReversal) {
        this.creditReversal = creditReversal;
    }


    /**
     * Gets the creditSale value for this PosRequestVer10Transaction.
     * 
     * @return creditSale
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditSaleReqType getCreditSale() {
        return creditSale;
    }


    /**
     * Sets the creditSale value for this PosRequestVer10Transaction.
     * 
     * @param creditSale
     */
    public void setCreditSale(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditSaleReqType creditSale) {
        this.creditSale = creditSale;
    }


    /**
     * Gets the creditTxnEdit value for this PosRequestVer10Transaction.
     * 
     * @return creditTxnEdit
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditTxnEditReqType getCreditTxnEdit() {
        return creditTxnEdit;
    }


    /**
     * Sets the creditTxnEdit value for this PosRequestVer10Transaction.
     * 
     * @param creditTxnEdit
     */
    public void setCreditTxnEdit(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditTxnEditReqType creditTxnEdit) {
        this.creditTxnEdit = creditTxnEdit;
    }


    /**
     * Gets the debitAddValue value for this PosRequestVer10Transaction.
     * 
     * @return debitAddValue
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosDebitAddValueReqType getDebitAddValue() {
        return debitAddValue;
    }


    /**
     * Sets the debitAddValue value for this PosRequestVer10Transaction.
     * 
     * @param debitAddValue
     */
    public void setDebitAddValue(com.usatech.iso8583.interchange.heartland.posgateway.PosDebitAddValueReqType debitAddValue) {
        this.debitAddValue = debitAddValue;
    }


    /**
     * Gets the giftCardReplace value for this PosRequestVer10Transaction.
     * 
     * @return giftCardReplace
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceReqType getGiftCardReplace() {
        return giftCardReplace;
    }


    /**
     * Sets the giftCardReplace value for this PosRequestVer10Transaction.
     * 
     * @param giftCardReplace
     */
    public void setGiftCardReplace(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceReqType giftCardReplace) {
        this.giftCardReplace = giftCardReplace;
    }


    /**
     * Gets the creditIncrementalAuth value for this PosRequestVer10Transaction.
     * 
     * @return creditIncrementalAuth
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditIncrementalAuthReqType getCreditIncrementalAuth() {
        return creditIncrementalAuth;
    }


    /**
     * Sets the creditIncrementalAuth value for this PosRequestVer10Transaction.
     * 
     * @param creditIncrementalAuth
     */
    public void setCreditIncrementalAuth(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditIncrementalAuthReqType creditIncrementalAuth) {
        this.creditIncrementalAuth = creditIncrementalAuth;
    }


    /**
     * Gets the debitReturn value for this PosRequestVer10Transaction.
     * 
     * @return debitReturn
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReturnReqType getDebitReturn() {
        return debitReturn;
    }


    /**
     * Sets the debitReturn value for this PosRequestVer10Transaction.
     * 
     * @param debitReturn
     */
    public void setDebitReturn(com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReturnReqType debitReturn) {
        this.debitReturn = debitReturn;
    }


    /**
     * Gets the debitReversal value for this PosRequestVer10Transaction.
     * 
     * @return debitReversal
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReversalReqType getDebitReversal() {
        return debitReversal;
    }


    /**
     * Sets the debitReversal value for this PosRequestVer10Transaction.
     * 
     * @param debitReversal
     */
    public void setDebitReversal(com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReversalReqType debitReversal) {
        this.debitReversal = debitReversal;
    }


    /**
     * Gets the debitSale value for this PosRequestVer10Transaction.
     * 
     * @return debitSale
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosDebitSaleReqType getDebitSale() {
        return debitSale;
    }


    /**
     * Sets the debitSale value for this PosRequestVer10Transaction.
     * 
     * @param debitSale
     */
    public void setDebitSale(com.usatech.iso8583.interchange.heartland.posgateway.PosDebitSaleReqType debitSale) {
        this.debitSale = debitSale;
    }


    /**
     * Gets the EBTBalanceInquiry value for this PosRequestVer10Transaction.
     * 
     * @return EBTBalanceInquiry
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosEBTBalanceInquiryReqType getEBTBalanceInquiry() {
        return EBTBalanceInquiry;
    }


    /**
     * Sets the EBTBalanceInquiry value for this PosRequestVer10Transaction.
     * 
     * @param EBTBalanceInquiry
     */
    public void setEBTBalanceInquiry(com.usatech.iso8583.interchange.heartland.posgateway.PosEBTBalanceInquiryReqType EBTBalanceInquiry) {
        this.EBTBalanceInquiry = EBTBalanceInquiry;
    }


    /**
     * Gets the prePaidBalanceInquiry value for this PosRequestVer10Transaction.
     * 
     * @return prePaidBalanceInquiry
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidBalanceInquiryReqType getPrePaidBalanceInquiry() {
        return prePaidBalanceInquiry;
    }


    /**
     * Sets the prePaidBalanceInquiry value for this PosRequestVer10Transaction.
     * 
     * @param prePaidBalanceInquiry
     */
    public void setPrePaidBalanceInquiry(com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidBalanceInquiryReqType prePaidBalanceInquiry) {
        this.prePaidBalanceInquiry = prePaidBalanceInquiry;
    }


    /**
     * Gets the creditCPCEdit value for this PosRequestVer10Transaction.
     * 
     * @return creditCPCEdit
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditCPCEditReqType getCreditCPCEdit() {
        return creditCPCEdit;
    }


    /**
     * Sets the creditCPCEdit value for this PosRequestVer10Transaction.
     * 
     * @param creditCPCEdit
     */
    public void setCreditCPCEdit(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditCPCEditReqType creditCPCEdit) {
        this.creditCPCEdit = creditCPCEdit;
    }


    /**
     * Gets the EBTCashBenefitWithdrawal value for this PosRequestVer10Transaction.
     * 
     * @return EBTCashBenefitWithdrawal
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBenefitWithdrawalReqType getEBTCashBenefitWithdrawal() {
        return EBTCashBenefitWithdrawal;
    }


    /**
     * Sets the EBTCashBenefitWithdrawal value for this PosRequestVer10Transaction.
     * 
     * @param EBTCashBenefitWithdrawal
     */
    public void setEBTCashBenefitWithdrawal(com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBenefitWithdrawalReqType EBTCashBenefitWithdrawal) {
        this.EBTCashBenefitWithdrawal = EBTCashBenefitWithdrawal;
    }


    /**
     * Gets the EBTFSPurchase value for this PosRequestVer10Transaction.
     * 
     * @return EBTFSPurchase
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSPurchaseReqType getEBTFSPurchase() {
        return EBTFSPurchase;
    }


    /**
     * Sets the EBTFSPurchase value for this PosRequestVer10Transaction.
     * 
     * @param EBTFSPurchase
     */
    public void setEBTFSPurchase(com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSPurchaseReqType EBTFSPurchase) {
        this.EBTFSPurchase = EBTFSPurchase;
    }


    /**
     * Gets the EBTFSReturn value for this PosRequestVer10Transaction.
     * 
     * @return EBTFSReturn
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSReturnReqType getEBTFSReturn() {
        return EBTFSReturn;
    }


    /**
     * Sets the EBTFSReturn value for this PosRequestVer10Transaction.
     * 
     * @param EBTFSReturn
     */
    public void setEBTFSReturn(com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSReturnReqType EBTFSReturn) {
        this.EBTFSReturn = EBTFSReturn;
    }


    /**
     * Gets the EBTVoucherPurchase value for this PosRequestVer10Transaction.
     * 
     * @return EBTVoucherPurchase
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSVoucherReqType getEBTVoucherPurchase() {
        return EBTVoucherPurchase;
    }


    /**
     * Sets the EBTVoucherPurchase value for this PosRequestVer10Transaction.
     * 
     * @param EBTVoucherPurchase
     */
    public void setEBTVoucherPurchase(com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSVoucherReqType EBTVoucherPurchase) {
        this.EBTVoucherPurchase = EBTVoucherPurchase;
    }


    /**
     * Gets the endToEndTest value for this PosRequestVer10Transaction.
     * 
     * @return endToEndTest
     */
    public java.lang.Object getEndToEndTest() {
        return endToEndTest;
    }


    /**
     * Sets the endToEndTest value for this PosRequestVer10Transaction.
     * 
     * @param endToEndTest
     */
    public void setEndToEndTest(java.lang.Object endToEndTest) {
        this.endToEndTest = endToEndTest;
    }


    /**
     * Gets the giftCardActivate value for this PosRequestVer10Transaction.
     * 
     * @return giftCardActivate
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateReqType getGiftCardActivate() {
        return giftCardActivate;
    }


    /**
     * Sets the giftCardActivate value for this PosRequestVer10Transaction.
     * 
     * @param giftCardActivate
     */
    public void setGiftCardActivate(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateReqType giftCardActivate) {
        this.giftCardActivate = giftCardActivate;
    }


    /**
     * Gets the giftCardAddValue value for this PosRequestVer10Transaction.
     * 
     * @return giftCardAddValue
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueReqType getGiftCardAddValue() {
        return giftCardAddValue;
    }


    /**
     * Sets the giftCardAddValue value for this PosRequestVer10Transaction.
     * 
     * @param giftCardAddValue
     */
    public void setGiftCardAddValue(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueReqType giftCardAddValue) {
        this.giftCardAddValue = giftCardAddValue;
    }


    /**
     * Gets the giftCardBalance value for this PosRequestVer10Transaction.
     * 
     * @return giftCardBalance
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceReqType getGiftCardBalance() {
        return giftCardBalance;
    }


    /**
     * Sets the giftCardBalance value for this PosRequestVer10Transaction.
     * 
     * @param giftCardBalance
     */
    public void setGiftCardBalance(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceReqType giftCardBalance) {
        this.giftCardBalance = giftCardBalance;
    }


    /**
     * Gets the reportBatchHistory value for this PosRequestVer10Transaction.
     * 
     * @return reportBatchHistory
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryReqType getReportBatchHistory() {
        return reportBatchHistory;
    }


    /**
     * Sets the reportBatchHistory value for this PosRequestVer10Transaction.
     * 
     * @param reportBatchHistory
     */
    public void setReportBatchHistory(com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryReqType reportBatchHistory) {
        this.reportBatchHistory = reportBatchHistory;
    }


    /**
     * Gets the giftCardDeactivate value for this PosRequestVer10Transaction.
     * 
     * @return giftCardDeactivate
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateReqType getGiftCardDeactivate() {
        return giftCardDeactivate;
    }


    /**
     * Sets the giftCardDeactivate value for this PosRequestVer10Transaction.
     * 
     * @param giftCardDeactivate
     */
    public void setGiftCardDeactivate(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateReqType giftCardDeactivate) {
        this.giftCardDeactivate = giftCardDeactivate;
    }


    /**
     * Gets the creditOfflineAuth value for this PosRequestVer10Transaction.
     * 
     * @return creditOfflineAuth
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineAuthReqType getCreditOfflineAuth() {
        return creditOfflineAuth;
    }


    /**
     * Sets the creditOfflineAuth value for this PosRequestVer10Transaction.
     * 
     * @param creditOfflineAuth
     */
    public void setCreditOfflineAuth(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineAuthReqType creditOfflineAuth) {
        this.creditOfflineAuth = creditOfflineAuth;
    }


    /**
     * Gets the giftCardPreviousDayTotals value for this PosRequestVer10Transaction.
     * 
     * @return giftCardPreviousDayTotals
     */
    public java.lang.String getGiftCardPreviousDayTotals() {
        return giftCardPreviousDayTotals;
    }


    /**
     * Sets the giftCardPreviousDayTotals value for this PosRequestVer10Transaction.
     * 
     * @param giftCardPreviousDayTotals
     */
    public void setGiftCardPreviousDayTotals(java.lang.String giftCardPreviousDayTotals) {
        this.giftCardPreviousDayTotals = giftCardPreviousDayTotals;
    }


    /**
     * Gets the giftCardSale value for this PosRequestVer10Transaction.
     * 
     * @return giftCardSale
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleReqType getGiftCardSale() {
        return giftCardSale;
    }


    /**
     * Sets the giftCardSale value for this PosRequestVer10Transaction.
     * 
     * @param giftCardSale
     */
    public void setGiftCardSale(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleReqType giftCardSale) {
        this.giftCardSale = giftCardSale;
    }


    /**
     * Gets the giftCardVoid value for this PosRequestVer10Transaction.
     * 
     * @return giftCardVoid
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidReqType getGiftCardVoid() {
        return giftCardVoid;
    }


    /**
     * Sets the giftCardVoid value for this PosRequestVer10Transaction.
     * 
     * @param giftCardVoid
     */
    public void setGiftCardVoid(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidReqType giftCardVoid) {
        this.giftCardVoid = giftCardVoid;
    }


    /**
     * Gets the prePaidAddValue value for this PosRequestVer10Transaction.
     * 
     * @return prePaidAddValue
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidAddValueReqType getPrePaidAddValue() {
        return prePaidAddValue;
    }


    /**
     * Sets the prePaidAddValue value for this PosRequestVer10Transaction.
     * 
     * @param prePaidAddValue
     */
    public void setPrePaidAddValue(com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidAddValueReqType prePaidAddValue) {
        this.prePaidAddValue = prePaidAddValue;
    }


    /**
     * Gets the EBTCashBackPurchase value for this PosRequestVer10Transaction.
     * 
     * @return EBTCashBackPurchase
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBackPurchaseReqType getEBTCashBackPurchase() {
        return EBTCashBackPurchase;
    }


    /**
     * Sets the EBTCashBackPurchase value for this PosRequestVer10Transaction.
     * 
     * @param EBTCashBackPurchase
     */
    public void setEBTCashBackPurchase(com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBackPurchaseReqType EBTCashBackPurchase) {
        this.EBTCashBackPurchase = EBTCashBackPurchase;
    }


    /**
     * Gets the recurringBilling value for this PosRequestVer10Transaction.
     * 
     * @return recurringBilling
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosRecurringBillReqType getRecurringBilling() {
        return recurringBilling;
    }


    /**
     * Sets the recurringBilling value for this PosRequestVer10Transaction.
     * 
     * @param recurringBilling
     */
    public void setRecurringBilling(com.usatech.iso8583.interchange.heartland.posgateway.PosRecurringBillReqType recurringBilling) {
        this.recurringBilling = recurringBilling;
    }


    /**
     * Gets the reportActivity value for this PosRequestVer10Transaction.
     * 
     * @return reportActivity
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityReqType getReportActivity() {
        return reportActivity;
    }


    /**
     * Sets the reportActivity value for this PosRequestVer10Transaction.
     * 
     * @param reportActivity
     */
    public void setReportActivity(com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityReqType reportActivity) {
        this.reportActivity = reportActivity;
    }


    /**
     * Gets the reportBatchDetail value for this PosRequestVer10Transaction.
     * 
     * @return reportBatchDetail
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailReqType getReportBatchDetail() {
        return reportBatchDetail;
    }


    /**
     * Sets the reportBatchDetail value for this PosRequestVer10Transaction.
     * 
     * @param reportBatchDetail
     */
    public void setReportBatchDetail(com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailReqType reportBatchDetail) {
        this.reportBatchDetail = reportBatchDetail;
    }


    /**
     * Gets the creditAuth value for this PosRequestVer10Transaction.
     * 
     * @return creditAuth
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAuthReqType getCreditAuth() {
        return creditAuth;
    }


    /**
     * Sets the creditAuth value for this PosRequestVer10Transaction.
     * 
     * @param creditAuth
     */
    public void setCreditAuth(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAuthReqType creditAuth) {
        this.creditAuth = creditAuth;
    }


    /**
     * Gets the reportOpenAuths value for this PosRequestVer10Transaction.
     * 
     * @return reportOpenAuths
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsReqType getReportOpenAuths() {
        return reportOpenAuths;
    }


    /**
     * Sets the reportOpenAuths value for this PosRequestVer10Transaction.
     * 
     * @param reportOpenAuths
     */
    public void setReportOpenAuths(com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsReqType reportOpenAuths) {
        this.reportOpenAuths = reportOpenAuths;
    }


    /**
     * Gets the creditAddToBatch value for this PosRequestVer10Transaction.
     * 
     * @return creditAddToBatch
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAddToBatchReqType getCreditAddToBatch() {
        return creditAddToBatch;
    }


    /**
     * Sets the creditAddToBatch value for this PosRequestVer10Transaction.
     * 
     * @param creditAddToBatch
     */
    public void setCreditAddToBatch(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAddToBatchReqType creditAddToBatch) {
        this.creditAddToBatch = creditAddToBatch;
    }


    /**
     * Gets the reportTxnDetail value for this PosRequestVer10Transaction.
     * 
     * @return reportTxnDetail
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailReqType getReportTxnDetail() {
        return reportTxnDetail;
    }


    /**
     * Sets the reportTxnDetail value for this PosRequestVer10Transaction.
     * 
     * @param reportTxnDetail
     */
    public void setReportTxnDetail(com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailReqType reportTxnDetail) {
        this.reportTxnDetail = reportTxnDetail;
    }


    /**
     * Gets the testCredentials value for this PosRequestVer10Transaction.
     * 
     * @return testCredentials
     */
    public java.lang.String getTestCredentials() {
        return testCredentials;
    }


    /**
     * Sets the testCredentials value for this PosRequestVer10Transaction.
     * 
     * @param testCredentials
     */
    public void setTestCredentials(java.lang.String testCredentials) {
        this.testCredentials = testCredentials;
    }


    /**
     * Gets the reportBatchSummary value for this PosRequestVer10Transaction.
     * 
     * @return reportBatchSummary
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryReqType getReportBatchSummary() {
        return reportBatchSummary;
    }


    /**
     * Sets the reportBatchSummary value for this PosRequestVer10Transaction.
     * 
     * @param reportBatchSummary
     */
    public void setReportBatchSummary(com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryReqType reportBatchSummary) {
        this.reportBatchSummary = reportBatchSummary;
    }


    /**
     * Gets the reportSearch value for this PosRequestVer10Transaction.
     * 
     * @return reportSearch
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchReqType getReportSearch() {
        return reportSearch;
    }


    /**
     * Sets the reportSearch value for this PosRequestVer10Transaction.
     * 
     * @param reportSearch
     */
    public void setReportSearch(com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchReqType reportSearch) {
        this.reportSearch = reportSearch;
    }


    /**
     * Gets the addAttachment value for this PosRequestVer10Transaction.
     * 
     * @return addAttachment
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosAddAttachmentReqType getAddAttachment() {
        return addAttachment;
    }


    /**
     * Sets the addAttachment value for this PosRequestVer10Transaction.
     * 
     * @param addAttachment
     */
    public void setAddAttachment(com.usatech.iso8583.interchange.heartland.posgateway.PosAddAttachmentReqType addAttachment) {
        this.addAttachment = addAttachment;
    }


    /**
     * Gets the batchClose value for this PosRequestVer10Transaction.
     * 
     * @return batchClose
     */
    public java.lang.String getBatchClose() {
        return batchClose;
    }


    /**
     * Sets the batchClose value for this PosRequestVer10Transaction.
     * 
     * @param batchClose
     */
    public void setBatchClose(java.lang.String batchClose) {
        this.batchClose = batchClose;
    }


    /**
     * Gets the creditAccountVerify value for this PosRequestVer10Transaction.
     * 
     * @return creditAccountVerify
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAccountVerifyReqType getCreditAccountVerify() {
        return creditAccountVerify;
    }


    /**
     * Sets the creditAccountVerify value for this PosRequestVer10Transaction.
     * 
     * @param creditAccountVerify
     */
    public void setCreditAccountVerify(com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAccountVerifyReqType creditAccountVerify) {
        this.creditAccountVerify = creditAccountVerify;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosRequestVer10Transaction)) return false;
        PosRequestVer10Transaction other = (PosRequestVer10Transaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.creditVoid==null && other.getCreditVoid()==null) || 
             (this.creditVoid!=null &&
              this.creditVoid.equals(other.getCreditVoid()))) &&
            ((this.giftCardCurrentDayTotals==null && other.getGiftCardCurrentDayTotals()==null) || 
             (this.giftCardCurrentDayTotals!=null &&
              this.giftCardCurrentDayTotals.equals(other.getGiftCardCurrentDayTotals()))) &&
            ((this.creditOfflineSale==null && other.getCreditOfflineSale()==null) || 
             (this.creditOfflineSale!=null &&
              this.creditOfflineSale.equals(other.getCreditOfflineSale()))) &&
            ((this.creditReturn==null && other.getCreditReturn()==null) || 
             (this.creditReturn!=null &&
              this.creditReturn.equals(other.getCreditReturn()))) &&
            ((this.creditReversal==null && other.getCreditReversal()==null) || 
             (this.creditReversal!=null &&
              this.creditReversal.equals(other.getCreditReversal()))) &&
            ((this.creditSale==null && other.getCreditSale()==null) || 
             (this.creditSale!=null &&
              this.creditSale.equals(other.getCreditSale()))) &&
            ((this.creditTxnEdit==null && other.getCreditTxnEdit()==null) || 
             (this.creditTxnEdit!=null &&
              this.creditTxnEdit.equals(other.getCreditTxnEdit()))) &&
            ((this.debitAddValue==null && other.getDebitAddValue()==null) || 
             (this.debitAddValue!=null &&
              this.debitAddValue.equals(other.getDebitAddValue()))) &&
            ((this.giftCardReplace==null && other.getGiftCardReplace()==null) || 
             (this.giftCardReplace!=null &&
              this.giftCardReplace.equals(other.getGiftCardReplace()))) &&
            ((this.creditIncrementalAuth==null && other.getCreditIncrementalAuth()==null) || 
             (this.creditIncrementalAuth!=null &&
              this.creditIncrementalAuth.equals(other.getCreditIncrementalAuth()))) &&
            ((this.debitReturn==null && other.getDebitReturn()==null) || 
             (this.debitReturn!=null &&
              this.debitReturn.equals(other.getDebitReturn()))) &&
            ((this.debitReversal==null && other.getDebitReversal()==null) || 
             (this.debitReversal!=null &&
              this.debitReversal.equals(other.getDebitReversal()))) &&
            ((this.debitSale==null && other.getDebitSale()==null) || 
             (this.debitSale!=null &&
              this.debitSale.equals(other.getDebitSale()))) &&
            ((this.EBTBalanceInquiry==null && other.getEBTBalanceInquiry()==null) || 
             (this.EBTBalanceInquiry!=null &&
              this.EBTBalanceInquiry.equals(other.getEBTBalanceInquiry()))) &&
            ((this.prePaidBalanceInquiry==null && other.getPrePaidBalanceInquiry()==null) || 
             (this.prePaidBalanceInquiry!=null &&
              this.prePaidBalanceInquiry.equals(other.getPrePaidBalanceInquiry()))) &&
            ((this.creditCPCEdit==null && other.getCreditCPCEdit()==null) || 
             (this.creditCPCEdit!=null &&
              this.creditCPCEdit.equals(other.getCreditCPCEdit()))) &&
            ((this.EBTCashBenefitWithdrawal==null && other.getEBTCashBenefitWithdrawal()==null) || 
             (this.EBTCashBenefitWithdrawal!=null &&
              this.EBTCashBenefitWithdrawal.equals(other.getEBTCashBenefitWithdrawal()))) &&
            ((this.EBTFSPurchase==null && other.getEBTFSPurchase()==null) || 
             (this.EBTFSPurchase!=null &&
              this.EBTFSPurchase.equals(other.getEBTFSPurchase()))) &&
            ((this.EBTFSReturn==null && other.getEBTFSReturn()==null) || 
             (this.EBTFSReturn!=null &&
              this.EBTFSReturn.equals(other.getEBTFSReturn()))) &&
            ((this.EBTVoucherPurchase==null && other.getEBTVoucherPurchase()==null) || 
             (this.EBTVoucherPurchase!=null &&
              this.EBTVoucherPurchase.equals(other.getEBTVoucherPurchase()))) &&
            ((this.endToEndTest==null && other.getEndToEndTest()==null) || 
             (this.endToEndTest!=null &&
              this.endToEndTest.equals(other.getEndToEndTest()))) &&
            ((this.giftCardActivate==null && other.getGiftCardActivate()==null) || 
             (this.giftCardActivate!=null &&
              this.giftCardActivate.equals(other.getGiftCardActivate()))) &&
            ((this.giftCardAddValue==null && other.getGiftCardAddValue()==null) || 
             (this.giftCardAddValue!=null &&
              this.giftCardAddValue.equals(other.getGiftCardAddValue()))) &&
            ((this.giftCardBalance==null && other.getGiftCardBalance()==null) || 
             (this.giftCardBalance!=null &&
              this.giftCardBalance.equals(other.getGiftCardBalance()))) &&
            ((this.reportBatchHistory==null && other.getReportBatchHistory()==null) || 
             (this.reportBatchHistory!=null &&
              this.reportBatchHistory.equals(other.getReportBatchHistory()))) &&
            ((this.giftCardDeactivate==null && other.getGiftCardDeactivate()==null) || 
             (this.giftCardDeactivate!=null &&
              this.giftCardDeactivate.equals(other.getGiftCardDeactivate()))) &&
            ((this.creditOfflineAuth==null && other.getCreditOfflineAuth()==null) || 
             (this.creditOfflineAuth!=null &&
              this.creditOfflineAuth.equals(other.getCreditOfflineAuth()))) &&
            ((this.giftCardPreviousDayTotals==null && other.getGiftCardPreviousDayTotals()==null) || 
             (this.giftCardPreviousDayTotals!=null &&
              this.giftCardPreviousDayTotals.equals(other.getGiftCardPreviousDayTotals()))) &&
            ((this.giftCardSale==null && other.getGiftCardSale()==null) || 
             (this.giftCardSale!=null &&
              this.giftCardSale.equals(other.getGiftCardSale()))) &&
            ((this.giftCardVoid==null && other.getGiftCardVoid()==null) || 
             (this.giftCardVoid!=null &&
              this.giftCardVoid.equals(other.getGiftCardVoid()))) &&
            ((this.prePaidAddValue==null && other.getPrePaidAddValue()==null) || 
             (this.prePaidAddValue!=null &&
              this.prePaidAddValue.equals(other.getPrePaidAddValue()))) &&
            ((this.EBTCashBackPurchase==null && other.getEBTCashBackPurchase()==null) || 
             (this.EBTCashBackPurchase!=null &&
              this.EBTCashBackPurchase.equals(other.getEBTCashBackPurchase()))) &&
            ((this.recurringBilling==null && other.getRecurringBilling()==null) || 
             (this.recurringBilling!=null &&
              this.recurringBilling.equals(other.getRecurringBilling()))) &&
            ((this.reportActivity==null && other.getReportActivity()==null) || 
             (this.reportActivity!=null &&
              this.reportActivity.equals(other.getReportActivity()))) &&
            ((this.reportBatchDetail==null && other.getReportBatchDetail()==null) || 
             (this.reportBatchDetail!=null &&
              this.reportBatchDetail.equals(other.getReportBatchDetail()))) &&
            ((this.creditAuth==null && other.getCreditAuth()==null) || 
             (this.creditAuth!=null &&
              this.creditAuth.equals(other.getCreditAuth()))) &&
            ((this.reportOpenAuths==null && other.getReportOpenAuths()==null) || 
             (this.reportOpenAuths!=null &&
              this.reportOpenAuths.equals(other.getReportOpenAuths()))) &&
            ((this.creditAddToBatch==null && other.getCreditAddToBatch()==null) || 
             (this.creditAddToBatch!=null &&
              this.creditAddToBatch.equals(other.getCreditAddToBatch()))) &&
            ((this.reportTxnDetail==null && other.getReportTxnDetail()==null) || 
             (this.reportTxnDetail!=null &&
              this.reportTxnDetail.equals(other.getReportTxnDetail()))) &&
            ((this.testCredentials==null && other.getTestCredentials()==null) || 
             (this.testCredentials!=null &&
              this.testCredentials.equals(other.getTestCredentials()))) &&
            ((this.reportBatchSummary==null && other.getReportBatchSummary()==null) || 
             (this.reportBatchSummary!=null &&
              this.reportBatchSummary.equals(other.getReportBatchSummary()))) &&
            ((this.reportSearch==null && other.getReportSearch()==null) || 
             (this.reportSearch!=null &&
              this.reportSearch.equals(other.getReportSearch()))) &&
            ((this.addAttachment==null && other.getAddAttachment()==null) || 
             (this.addAttachment!=null &&
              this.addAttachment.equals(other.getAddAttachment()))) &&
            ((this.batchClose==null && other.getBatchClose()==null) || 
             (this.batchClose!=null &&
              this.batchClose.equals(other.getBatchClose()))) &&
            ((this.creditAccountVerify==null && other.getCreditAccountVerify()==null) || 
             (this.creditAccountVerify!=null &&
              this.creditAccountVerify.equals(other.getCreditAccountVerify())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreditVoid() != null) {
            _hashCode += getCreditVoid().hashCode();
        }
        if (getGiftCardCurrentDayTotals() != null) {
            _hashCode += getGiftCardCurrentDayTotals().hashCode();
        }
        if (getCreditOfflineSale() != null) {
            _hashCode += getCreditOfflineSale().hashCode();
        }
        if (getCreditReturn() != null) {
            _hashCode += getCreditReturn().hashCode();
        }
        if (getCreditReversal() != null) {
            _hashCode += getCreditReversal().hashCode();
        }
        if (getCreditSale() != null) {
            _hashCode += getCreditSale().hashCode();
        }
        if (getCreditTxnEdit() != null) {
            _hashCode += getCreditTxnEdit().hashCode();
        }
        if (getDebitAddValue() != null) {
            _hashCode += getDebitAddValue().hashCode();
        }
        if (getGiftCardReplace() != null) {
            _hashCode += getGiftCardReplace().hashCode();
        }
        if (getCreditIncrementalAuth() != null) {
            _hashCode += getCreditIncrementalAuth().hashCode();
        }
        if (getDebitReturn() != null) {
            _hashCode += getDebitReturn().hashCode();
        }
        if (getDebitReversal() != null) {
            _hashCode += getDebitReversal().hashCode();
        }
        if (getDebitSale() != null) {
            _hashCode += getDebitSale().hashCode();
        }
        if (getEBTBalanceInquiry() != null) {
            _hashCode += getEBTBalanceInquiry().hashCode();
        }
        if (getPrePaidBalanceInquiry() != null) {
            _hashCode += getPrePaidBalanceInquiry().hashCode();
        }
        if (getCreditCPCEdit() != null) {
            _hashCode += getCreditCPCEdit().hashCode();
        }
        if (getEBTCashBenefitWithdrawal() != null) {
            _hashCode += getEBTCashBenefitWithdrawal().hashCode();
        }
        if (getEBTFSPurchase() != null) {
            _hashCode += getEBTFSPurchase().hashCode();
        }
        if (getEBTFSReturn() != null) {
            _hashCode += getEBTFSReturn().hashCode();
        }
        if (getEBTVoucherPurchase() != null) {
            _hashCode += getEBTVoucherPurchase().hashCode();
        }
        if (getEndToEndTest() != null) {
            _hashCode += getEndToEndTest().hashCode();
        }
        if (getGiftCardActivate() != null) {
            _hashCode += getGiftCardActivate().hashCode();
        }
        if (getGiftCardAddValue() != null) {
            _hashCode += getGiftCardAddValue().hashCode();
        }
        if (getGiftCardBalance() != null) {
            _hashCode += getGiftCardBalance().hashCode();
        }
        if (getReportBatchHistory() != null) {
            _hashCode += getReportBatchHistory().hashCode();
        }
        if (getGiftCardDeactivate() != null) {
            _hashCode += getGiftCardDeactivate().hashCode();
        }
        if (getCreditOfflineAuth() != null) {
            _hashCode += getCreditOfflineAuth().hashCode();
        }
        if (getGiftCardPreviousDayTotals() != null) {
            _hashCode += getGiftCardPreviousDayTotals().hashCode();
        }
        if (getGiftCardSale() != null) {
            _hashCode += getGiftCardSale().hashCode();
        }
        if (getGiftCardVoid() != null) {
            _hashCode += getGiftCardVoid().hashCode();
        }
        if (getPrePaidAddValue() != null) {
            _hashCode += getPrePaidAddValue().hashCode();
        }
        if (getEBTCashBackPurchase() != null) {
            _hashCode += getEBTCashBackPurchase().hashCode();
        }
        if (getRecurringBilling() != null) {
            _hashCode += getRecurringBilling().hashCode();
        }
        if (getReportActivity() != null) {
            _hashCode += getReportActivity().hashCode();
        }
        if (getReportBatchDetail() != null) {
            _hashCode += getReportBatchDetail().hashCode();
        }
        if (getCreditAuth() != null) {
            _hashCode += getCreditAuth().hashCode();
        }
        if (getReportOpenAuths() != null) {
            _hashCode += getReportOpenAuths().hashCode();
        }
        if (getCreditAddToBatch() != null) {
            _hashCode += getCreditAddToBatch().hashCode();
        }
        if (getReportTxnDetail() != null) {
            _hashCode += getReportTxnDetail().hashCode();
        }
        if (getTestCredentials() != null) {
            _hashCode += getTestCredentials().hashCode();
        }
        if (getReportBatchSummary() != null) {
            _hashCode += getReportBatchSummary().hashCode();
        }
        if (getReportSearch() != null) {
            _hashCode += getReportSearch().hashCode();
        }
        if (getAddAttachment() != null) {
            _hashCode += getAddAttachment().hashCode();
        }
        if (getBatchClose() != null) {
            _hashCode += getBatchClose().hashCode();
        }
        if (getCreditAccountVerify() != null) {
            _hashCode += getCreditAccountVerify().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosRequestVer10Transaction.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosRequest>Ver1.0>Transaction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditVoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditVoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditVoidReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardCurrentDayTotals");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardCurrentDayTotals"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditOfflineSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditOfflineSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditOfflineSaleReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditReturnReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditReversal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditReversal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditReversalReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditSaleReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditTxnEdit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditTxnEdit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditTxnEditReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitAddValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitAddValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosDebitAddValueReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardReplace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardReplace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardReplaceReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditIncrementalAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditIncrementalAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditIncrementalAuthReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosDebitReturnReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitReversal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitReversal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosDebitReversalReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosDebitSaleReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTBalanceInquiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTBalanceInquiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTBalanceInquiryReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prePaidBalanceInquiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PrePaidBalanceInquiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosPrePaidBalanceInquiryReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCPCEdit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditCPCEdit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditCPCEditReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTCashBenefitWithdrawal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTCashBenefitWithdrawal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTCashBenefitWithdrawalReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTFSPurchase");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTFSPurchase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTFSPurchaseReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTFSReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTFSReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTFSReturnReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTVoucherPurchase");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTVoucherPurchase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTFSVoucherReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endToEndTest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EndToEndTest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardActivate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardActivate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardActivateReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardAddValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardAddValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardAddValueReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardBalanceReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportBatchHistory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportBatchHistory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchHistoryReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardDeactivate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardDeactivate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardDeactivateReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditOfflineAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditOfflineAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditOfflineAuthReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardPreviousDayTotals");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardPreviousDayTotals"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardSaleReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardVoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardVoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardVoidReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prePaidAddValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PrePaidAddValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosPrePaidAddValueReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTCashBackPurchase");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTCashBackPurchase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTCashBackPurchaseReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recurringBilling");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RecurringBilling"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosRecurringBillReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportActivity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportActivity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportActivityReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportBatchDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportBatchDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchDetailReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditAuthReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportOpenAuths");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportOpenAuths"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportOpenAuthsReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditAddToBatch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAddToBatch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditAddToBatchReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportTxnDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportTxnDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportTxnDetailReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("testCredentials");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TestCredentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportBatchSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportBatchSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchSummaryReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportSearch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportSearch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportSearchReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addAttachment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AddAttachment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosAddAttachmentReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchClose");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchClose"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditAccountVerify");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAccountVerify"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditAccountVerifyReqType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
