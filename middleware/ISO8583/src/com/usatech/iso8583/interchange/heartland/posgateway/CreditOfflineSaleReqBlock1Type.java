/**
 * CreditOfflineSaleReqBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class CreditOfflineSaleReqBlock1Type  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData;

    private java.math.BigDecimal amt;

    private java.math.BigDecimal gratuityAmtInfo;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType CPCReq;

    private java.lang.String offlineAuthCode;

    private com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData;

    private com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType directMktData;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup;

    private com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataType lodgingData;

    private com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationType autoSubstantiation;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields;

    public CreditOfflineSaleReqBlock1Type() {
    }

    public CreditOfflineSaleReqBlock1Type(
           com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData,
           java.math.BigDecimal amt,
           java.math.BigDecimal gratuityAmtInfo,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType CPCReq,
           java.lang.String offlineAuthCode,
           com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData,
           com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType directMktData,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup,
           com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataType lodgingData,
           com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationType autoSubstantiation,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
           this.cardData = cardData;
           this.amt = amt;
           this.gratuityAmtInfo = gratuityAmtInfo;
           this.CPCReq = CPCReq;
           this.offlineAuthCode = offlineAuthCode;
           this.cardHolderData = cardHolderData;
           this.directMktData = directMktData;
           this.allowDup = allowDup;
           this.lodgingData = lodgingData;
           this.autoSubstantiation = autoSubstantiation;
           this.additionalTxnFields = additionalTxnFields;
    }


    /**
     * Gets the cardData value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return cardData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardDataType getCardData() {
        return cardData;
    }


    /**
     * Sets the cardData value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param cardData
     */
    public void setCardData(com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData) {
        this.cardData = cardData;
    }


    /**
     * Gets the amt value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the gratuityAmtInfo value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return gratuityAmtInfo
     */
    public java.math.BigDecimal getGratuityAmtInfo() {
        return gratuityAmtInfo;
    }


    /**
     * Sets the gratuityAmtInfo value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param gratuityAmtInfo
     */
    public void setGratuityAmtInfo(java.math.BigDecimal gratuityAmtInfo) {
        this.gratuityAmtInfo = gratuityAmtInfo;
    }


    /**
     * Gets the CPCReq value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return CPCReq
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getCPCReq() {
        return CPCReq;
    }


    /**
     * Sets the CPCReq value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param CPCReq
     */
    public void setCPCReq(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType CPCReq) {
        this.CPCReq = CPCReq;
    }


    /**
     * Gets the offlineAuthCode value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return offlineAuthCode
     */
    public java.lang.String getOfflineAuthCode() {
        return offlineAuthCode;
    }


    /**
     * Sets the offlineAuthCode value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param offlineAuthCode
     */
    public void setOfflineAuthCode(java.lang.String offlineAuthCode) {
        this.offlineAuthCode = offlineAuthCode;
    }


    /**
     * Gets the cardHolderData value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return cardHolderData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType getCardHolderData() {
        return cardHolderData;
    }


    /**
     * Sets the cardHolderData value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param cardHolderData
     */
    public void setCardHolderData(com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData) {
        this.cardHolderData = cardHolderData;
    }


    /**
     * Gets the directMktData value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return directMktData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType getDirectMktData() {
        return directMktData;
    }


    /**
     * Sets the directMktData value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param directMktData
     */
    public void setDirectMktData(com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType directMktData) {
        this.directMktData = directMktData;
    }


    /**
     * Gets the allowDup value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return allowDup
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getAllowDup() {
        return allowDup;
    }


    /**
     * Sets the allowDup value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param allowDup
     */
    public void setAllowDup(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup) {
        this.allowDup = allowDup;
    }


    /**
     * Gets the lodgingData value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return lodgingData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataType getLodgingData() {
        return lodgingData;
    }


    /**
     * Sets the lodgingData value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param lodgingData
     */
    public void setLodgingData(com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataType lodgingData) {
        this.lodgingData = lodgingData;
    }


    /**
     * Gets the autoSubstantiation value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return autoSubstantiation
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationType getAutoSubstantiation() {
        return autoSubstantiation;
    }


    /**
     * Sets the autoSubstantiation value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param autoSubstantiation
     */
    public void setAutoSubstantiation(com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationType autoSubstantiation) {
        this.autoSubstantiation = autoSubstantiation;
    }


    /**
     * Gets the additionalTxnFields value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @return additionalTxnFields
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType getAdditionalTxnFields() {
        return additionalTxnFields;
    }


    /**
     * Sets the additionalTxnFields value for this CreditOfflineSaleReqBlock1Type.
     * 
     * @param additionalTxnFields
     */
    public void setAdditionalTxnFields(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
        this.additionalTxnFields = additionalTxnFields;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreditOfflineSaleReqBlock1Type)) return false;
        CreditOfflineSaleReqBlock1Type other = (CreditOfflineSaleReqBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardData==null && other.getCardData()==null) || 
             (this.cardData!=null &&
              this.cardData.equals(other.getCardData()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.gratuityAmtInfo==null && other.getGratuityAmtInfo()==null) || 
             (this.gratuityAmtInfo!=null &&
              this.gratuityAmtInfo.equals(other.getGratuityAmtInfo()))) &&
            ((this.CPCReq==null && other.getCPCReq()==null) || 
             (this.CPCReq!=null &&
              this.CPCReq.equals(other.getCPCReq()))) &&
            ((this.offlineAuthCode==null && other.getOfflineAuthCode()==null) || 
             (this.offlineAuthCode!=null &&
              this.offlineAuthCode.equals(other.getOfflineAuthCode()))) &&
            ((this.cardHolderData==null && other.getCardHolderData()==null) || 
             (this.cardHolderData!=null &&
              this.cardHolderData.equals(other.getCardHolderData()))) &&
            ((this.directMktData==null && other.getDirectMktData()==null) || 
             (this.directMktData!=null &&
              this.directMktData.equals(other.getDirectMktData()))) &&
            ((this.allowDup==null && other.getAllowDup()==null) || 
             (this.allowDup!=null &&
              this.allowDup.equals(other.getAllowDup()))) &&
            ((this.lodgingData==null && other.getLodgingData()==null) || 
             (this.lodgingData!=null &&
              this.lodgingData.equals(other.getLodgingData()))) &&
            ((this.autoSubstantiation==null && other.getAutoSubstantiation()==null) || 
             (this.autoSubstantiation!=null &&
              this.autoSubstantiation.equals(other.getAutoSubstantiation()))) &&
            ((this.additionalTxnFields==null && other.getAdditionalTxnFields()==null) || 
             (this.additionalTxnFields!=null &&
              this.additionalTxnFields.equals(other.getAdditionalTxnFields())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardData() != null) {
            _hashCode += getCardData().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getGratuityAmtInfo() != null) {
            _hashCode += getGratuityAmtInfo().hashCode();
        }
        if (getCPCReq() != null) {
            _hashCode += getCPCReq().hashCode();
        }
        if (getOfflineAuthCode() != null) {
            _hashCode += getOfflineAuthCode().hashCode();
        }
        if (getCardHolderData() != null) {
            _hashCode += getCardHolderData().hashCode();
        }
        if (getDirectMktData() != null) {
            _hashCode += getDirectMktData().hashCode();
        }
        if (getAllowDup() != null) {
            _hashCode += getAllowDup().hashCode();
        }
        if (getLodgingData() != null) {
            _hashCode += getLodgingData().hashCode();
        }
        if (getAutoSubstantiation() != null) {
            _hashCode += getAutoSubstantiation().hashCode();
        }
        if (getAdditionalTxnFields() != null) {
            _hashCode += getAdditionalTxnFields().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditOfflineSaleReqBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditOfflineSaleReqBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gratuityAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GratuityAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCReq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offlineAuthCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "OfflineAuthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowDup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AllowDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lodgingData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("autoSubstantiation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AutoSubstantiation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AutoSubstantiationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalTxnFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFieldsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
