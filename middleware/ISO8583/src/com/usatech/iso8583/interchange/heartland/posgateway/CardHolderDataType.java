/**
 * CardHolderDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class CardHolderDataType  implements java.io.Serializable {
    private java.lang.String cardHolderFirstName;

    private java.lang.String cardHolderLastName;

    private java.lang.String cardHolderAddr;

    private java.lang.String cardHolderCity;

    private java.lang.String cardHolderState;

    private java.lang.String cardHolderZip;

    private java.lang.String cardHolderPhone;

    private java.lang.String cardHolderEmail;

    public CardHolderDataType() {
    }

    public CardHolderDataType(
           java.lang.String cardHolderFirstName,
           java.lang.String cardHolderLastName,
           java.lang.String cardHolderAddr,
           java.lang.String cardHolderCity,
           java.lang.String cardHolderState,
           java.lang.String cardHolderZip,
           java.lang.String cardHolderPhone,
           java.lang.String cardHolderEmail) {
           this.cardHolderFirstName = cardHolderFirstName;
           this.cardHolderLastName = cardHolderLastName;
           this.cardHolderAddr = cardHolderAddr;
           this.cardHolderCity = cardHolderCity;
           this.cardHolderState = cardHolderState;
           this.cardHolderZip = cardHolderZip;
           this.cardHolderPhone = cardHolderPhone;
           this.cardHolderEmail = cardHolderEmail;
    }


    /**
     * Gets the cardHolderFirstName value for this CardHolderDataType.
     * 
     * @return cardHolderFirstName
     */
    public java.lang.String getCardHolderFirstName() {
        return cardHolderFirstName;
    }


    /**
     * Sets the cardHolderFirstName value for this CardHolderDataType.
     * 
     * @param cardHolderFirstName
     */
    public void setCardHolderFirstName(java.lang.String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }


    /**
     * Gets the cardHolderLastName value for this CardHolderDataType.
     * 
     * @return cardHolderLastName
     */
    public java.lang.String getCardHolderLastName() {
        return cardHolderLastName;
    }


    /**
     * Sets the cardHolderLastName value for this CardHolderDataType.
     * 
     * @param cardHolderLastName
     */
    public void setCardHolderLastName(java.lang.String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }


    /**
     * Gets the cardHolderAddr value for this CardHolderDataType.
     * 
     * @return cardHolderAddr
     */
    public java.lang.String getCardHolderAddr() {
        return cardHolderAddr;
    }


    /**
     * Sets the cardHolderAddr value for this CardHolderDataType.
     * 
     * @param cardHolderAddr
     */
    public void setCardHolderAddr(java.lang.String cardHolderAddr) {
        this.cardHolderAddr = cardHolderAddr;
    }


    /**
     * Gets the cardHolderCity value for this CardHolderDataType.
     * 
     * @return cardHolderCity
     */
    public java.lang.String getCardHolderCity() {
        return cardHolderCity;
    }


    /**
     * Sets the cardHolderCity value for this CardHolderDataType.
     * 
     * @param cardHolderCity
     */
    public void setCardHolderCity(java.lang.String cardHolderCity) {
        this.cardHolderCity = cardHolderCity;
    }


    /**
     * Gets the cardHolderState value for this CardHolderDataType.
     * 
     * @return cardHolderState
     */
    public java.lang.String getCardHolderState() {
        return cardHolderState;
    }


    /**
     * Sets the cardHolderState value for this CardHolderDataType.
     * 
     * @param cardHolderState
     */
    public void setCardHolderState(java.lang.String cardHolderState) {
        this.cardHolderState = cardHolderState;
    }


    /**
     * Gets the cardHolderZip value for this CardHolderDataType.
     * 
     * @return cardHolderZip
     */
    public java.lang.String getCardHolderZip() {
        return cardHolderZip;
    }


    /**
     * Sets the cardHolderZip value for this CardHolderDataType.
     * 
     * @param cardHolderZip
     */
    public void setCardHolderZip(java.lang.String cardHolderZip) {
        this.cardHolderZip = cardHolderZip;
    }


    /**
     * Gets the cardHolderPhone value for this CardHolderDataType.
     * 
     * @return cardHolderPhone
     */
    public java.lang.String getCardHolderPhone() {
        return cardHolderPhone;
    }


    /**
     * Sets the cardHolderPhone value for this CardHolderDataType.
     * 
     * @param cardHolderPhone
     */
    public void setCardHolderPhone(java.lang.String cardHolderPhone) {
        this.cardHolderPhone = cardHolderPhone;
    }


    /**
     * Gets the cardHolderEmail value for this CardHolderDataType.
     * 
     * @return cardHolderEmail
     */
    public java.lang.String getCardHolderEmail() {
        return cardHolderEmail;
    }


    /**
     * Sets the cardHolderEmail value for this CardHolderDataType.
     * 
     * @param cardHolderEmail
     */
    public void setCardHolderEmail(java.lang.String cardHolderEmail) {
        this.cardHolderEmail = cardHolderEmail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardHolderDataType)) return false;
        CardHolderDataType other = (CardHolderDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardHolderFirstName==null && other.getCardHolderFirstName()==null) || 
             (this.cardHolderFirstName!=null &&
              this.cardHolderFirstName.equals(other.getCardHolderFirstName()))) &&
            ((this.cardHolderLastName==null && other.getCardHolderLastName()==null) || 
             (this.cardHolderLastName!=null &&
              this.cardHolderLastName.equals(other.getCardHolderLastName()))) &&
            ((this.cardHolderAddr==null && other.getCardHolderAddr()==null) || 
             (this.cardHolderAddr!=null &&
              this.cardHolderAddr.equals(other.getCardHolderAddr()))) &&
            ((this.cardHolderCity==null && other.getCardHolderCity()==null) || 
             (this.cardHolderCity!=null &&
              this.cardHolderCity.equals(other.getCardHolderCity()))) &&
            ((this.cardHolderState==null && other.getCardHolderState()==null) || 
             (this.cardHolderState!=null &&
              this.cardHolderState.equals(other.getCardHolderState()))) &&
            ((this.cardHolderZip==null && other.getCardHolderZip()==null) || 
             (this.cardHolderZip!=null &&
              this.cardHolderZip.equals(other.getCardHolderZip()))) &&
            ((this.cardHolderPhone==null && other.getCardHolderPhone()==null) || 
             (this.cardHolderPhone!=null &&
              this.cardHolderPhone.equals(other.getCardHolderPhone()))) &&
            ((this.cardHolderEmail==null && other.getCardHolderEmail()==null) || 
             (this.cardHolderEmail!=null &&
              this.cardHolderEmail.equals(other.getCardHolderEmail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardHolderFirstName() != null) {
            _hashCode += getCardHolderFirstName().hashCode();
        }
        if (getCardHolderLastName() != null) {
            _hashCode += getCardHolderLastName().hashCode();
        }
        if (getCardHolderAddr() != null) {
            _hashCode += getCardHolderAddr().hashCode();
        }
        if (getCardHolderCity() != null) {
            _hashCode += getCardHolderCity().hashCode();
        }
        if (getCardHolderState() != null) {
            _hashCode += getCardHolderState().hashCode();
        }
        if (getCardHolderZip() != null) {
            _hashCode += getCardHolderZip().hashCode();
        }
        if (getCardHolderPhone() != null) {
            _hashCode += getCardHolderPhone().hashCode();
        }
        if (getCardHolderEmail() != null) {
            _hashCode += getCardHolderEmail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardHolderDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderZip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderZip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
