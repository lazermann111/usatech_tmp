package com.usatech.iso8583.interchange.heartland;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ThreadPoolExecutor;

import javax.xml.rpc.ServiceException;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.ConnectionPoolTimeoutException;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.logging.LogFactory;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_BindingStub;
import com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType;
import com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayServiceLocator;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequest;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Header;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponse;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10Header;
import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;

public class HeartlandInterchange implements ISO8583Interchange
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(HeartlandInterchange.class);

	public static final String INTERCHANGE_NAME = "HEARTLAND";
	public static final String POS_GW_DEVELOPER_ID = "002674";
	public static final String POS_GW_VERSION_NUMBER = "0870";
	
	public static final String PROPERTY_REQUEST_URL = "heartland.request.url";
	public static final String PROPERTY_REQUEST_LICENSE_ID = "heartland.request.licenseid";
	public static final String PROPERTY_REQUEST_USER_ID = "heartland.request.userid";
	public static final String PROPERTY_REQUEST_PASSWORD = "heartland.request.password";
	
	private boolean isStarted = false;

	private Configuration config;

	private final ReversalAction reversalAction;
	private final AuthorizationAction authorizationAction;
	private final AddToBatchAction addToBatchAction;
	private final RefundAction refundAction;
	private final SaleAction saleAction;
	private final SettlementAction settlementAction;
	
	private PosGatewayServiceLocator posGatewayServiceLocator = null;
	
	private String requestUrl;
	private int requestLicenseId;
	private String requestUserId;
	private String requestPassword;
	
	private static SortedSet<String> gatewayConnectionErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = -8044441696861035529L;
			{
				add("GW-2");//Authentication error
				add("GW-21");//Unauthorized
			}});
	
	private static SortedSet<String> gatewayResponseTimeoutErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = 4373530379880711927L;

			{
				add("GW-1");//POS Gateway error
				add("GW1"); //Gateway system error
			}});
	
	private static SortedSet<String> responseTimeoutErrorCodes = Collections.unmodifiableSortedSet(
			new TreeSet<String>(){
				private static final long serialVersionUID = 4373530379880711928L;
			{
				add("91"); //NO REPLY, time out
				add("96"); //SYSTEM ERROR
			}});

	public HeartlandInterchange(ThreadPoolExecutor threadPool, ISO8583TransactionManager txnManager, Configuration config) throws ConfigurationException 
	{
		this.config = config;
		reversalAction = new ReversalAction(this);
		authorizationAction = new AuthorizationAction(this);
		saleAction = new SaleAction(this);
		refundAction = new RefundAction(this);
		addToBatchAction = new AddToBatchAction(this);
		settlementAction = new SettlementAction(this);
		
		if (config instanceof FileConfiguration)
		{
			((FileConfiguration) config).setReloadingStrategy(new ConfigReloader());
		}		
	}
	
	private HeartlandAction heartlandActionFactory(ISO8583Transaction transaction)
	{
		String transactionType = transaction.getRequest().getTransactionType();
		if(ISO8583Message.TRANSACTION_TYPE_AUTHORIZATION.equals(transactionType))
			return authorizationAction;
		else if(ISO8583Message.TRANSACTION_TYPE_SALE.equals(transactionType)){
			if(transaction.getRequest().getAmount() == 0)
				return reversalAction;
			else if(transaction.getRequest().hasApprovalCode())
				return addToBatchAction;
			else
				return saleAction;
		} else if(ISO8583Message.TRANSACTION_TYPE_REFUND.equals(transactionType))
			return refundAction;
		else if(ISO8583Message.TRANSACTION_TYPE_SETTLEMENT.equals(transactionType)
				|| ISO8583Message.TRANSACTION_TYPE_SETTLEMENT_RETRY.equals(transactionType))
			return settlementAction;
		else if(ISO8583Message.TRANSACTION_TYPE_REVERSAL.equals(transactionType))
			return reversalAction;
		return null;
	}
	
	public PosResponse sendRequest(PosRequest request) throws ServiceException, RemoteException 
	{
		PosGatewayInterface_PortType posGatewayPort = posGatewayServiceLocator.getPosGatewayInterface();
		PosGatewayInterface_BindingStub posGatewayStub = (PosGatewayInterface_BindingStub) posGatewayPort;
		//set timeout to 0 for configuration properties to take effect
		posGatewayStub.setTimeout(0);
		return posGatewayPort.doTransaction(request);
	}
	
	public String getName() {
		return INTERCHANGE_NAME;
	}

	public boolean isConnected() {
		return true;
	}

	public boolean isSimulationMode() {
		return false;
	}
	
	public synchronized void stop() {
		if (!isStarted)
			return;

		log.info("Heartland Interchange Shutting down...");

		isStarted = false;
	}
	
	public boolean isStarted() {
		return isStarted;
	}
	
	private ISO8583Response getErrorISO8583Response (ISO8583Request isoRequest, String responseCode, String responseMessage) {
		return new ISO8583Response(responseCode, responseMessage);
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		checkConfigChanges();
		
		ISO8583Request isoRequest = transaction.getRequest();
		String transactionType = transaction.getRequest().getTransactionType();
		HeartlandAction action = heartlandActionFactory(transaction);

		if (action == null) {
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Processing failed: Unsupported transaction type: " + transactionType);
			return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_UNSUPPORTED_TRANSACTION_TYPE, "Unsupported transaction type: " + transactionType);
		}
		
		while (true) {
			transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);
	
			PosRequest request;
			try {
				action.validateRequest(isoRequest);
				request = createRequest(isoRequest, action);
				transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
			} catch (ValidationException e) {
				transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
				log.error(transaction + " failed input validation: " + e.getMessage(), e);
				return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
			} catch (Throwable e) {
				transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
				log.error("Error creating request for " + transaction + ": " + e.getMessage(), e);
				return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Error creating request: " + e.getMessage());
			}
			
			transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);
			
			ISO8583Response isoResponse;
			PosResponse response;
			try {
				response = sendRequest(request);
				transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);
			} catch (Throwable e) {
				String connectionError = checkConnectionFailure(e);
				if (connectionError != null) {
					log.error(connectionError + " transmitting " + transaction);
					return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, connectionError);
				}
				transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				String readTimeoutError = checkReadTimeout(e);
				if (readTimeoutError != null) {
					log.error(readTimeoutError + " transmitting " + transaction);
					return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, readTimeoutError);
				}
				log.error("Error transmitting " + transaction + ": " + e.getMessage(), e);
				return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting request: " + e.getMessage());
			}
			
			transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);
	
			isoResponse = new ISO8583Response();
			try {
				PosResponseVer10 resV10 = response.getVer10();
				PosResponseVer10Header resV10Header = resV10.getHeader();
				if(action.processResponse(isoRequest, isoResponse, resV10)) {
					transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);
					if (responseTimeoutErrorCodes.contains(isoResponse.getResponseCode()))
						isoResponse.setResponseCode(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT);
					// if sale amount < auth amount, Heartland wants a reversal in addition to CreditAddToBatch
					if (action == addToBatchAction && isoRequest.getAmount() < isoRequest.getOriginalAmount()) {
						action = reversalAction;
						continue;
					} else
						return isoResponse;
				} else {
					StringBuilder sb = new StringBuilder("Error processing ").append(transaction)
						.append(", gatewayResponseCode: ").append(resV10Header.getGatewayRspCode());
					if (resV10Header.getGatewayRspMsg() != null)
						sb.append(", gatewayResponseMessage: ").append(resV10Header.getGatewayRspMsg());
					log.error(sb);
					if (gatewayConnectionErrorCodes.contains(String.valueOf(resV10Header.getGatewayRspCode())))
						return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, sb.toString());
					else {	
						transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
						if (gatewayResponseTimeoutErrorCodes.contains(String.valueOf(resV10Header.getGatewayRspCode())))
							return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, sb.toString());
						else					
							return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_INTERNAL_ERROR, sb.toString());
					}
				}
			} catch (Throwable e) {
				transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
				log.error("Error constructing response to " + transaction + ": " + e.getMessage() + ", response: " + isoResponse, e);
				return getErrorISO8583Response(isoRequest, ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
			}
		}
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		return true;
	}

	public void start() {
		if (isStarted)
			return;

		log.info("Heartland Interchange Starting up...");
		loadConfig();

		isStarted = true;
	}	
	
	private synchronized void loadConfig() {
		log.info("Loading configuration...");
		
		requestUrl = config.getString(PROPERTY_REQUEST_URL);
		if (posGatewayServiceLocator == null)
			posGatewayServiceLocator = new PosGatewayServiceLocator();
		posGatewayServiceLocator.setPosGatewayInterfaceEndpointAddress(requestUrl);
		
		requestLicenseId = config.getInt(PROPERTY_REQUEST_LICENSE_ID);
		requestUserId = config.getString(PROPERTY_REQUEST_USER_ID);
		requestPassword = config.getString(PROPERTY_REQUEST_PASSWORD);
	}

	public synchronized void setConfiguration(Configuration config)
	{
		if (isStarted)
			loadConfig();
		else
			this.config = config;
	}	
	
	public Configuration getConfiguration() {
		return config;
	}
	
	private String getCredentialValue(String credentialList, String credentialToken, String defaultCredentialValue) {
		int pos1 = credentialList.indexOf(credentialToken);
		if (pos1 < 0)
			return defaultCredentialValue;
		int pos2 = credentialList.indexOf(",", pos1);
		if (pos2 < 0)
			return credentialList.substring(pos1 + credentialToken.length());
		else
			return credentialList.substring(pos1 + credentialToken.length(), pos2);
	}
	
	private PosRequest createRequest(ISO8583Request isoRequest, HeartlandAction action) throws ValidationException
	{
		String merchantCd = isoRequest.getAcquirerID();
		String terminalCd = isoRequest.getTerminalID();
		
		PosRequestVer10Header requestVer10Header = new PosRequestVer10Header();
		requestVer10Header.setDeveloperID(POS_GW_DEVELOPER_ID);
		requestVer10Header.setVersionNbr(POS_GW_VERSION_NUMBER);
		
		requestVer10Header.setUserName(getCredentialValue(terminalCd, "User=", getCredentialValue(merchantCd, "User=", requestUserId)));
		if (isoRequest.hasTerminalEncryptKey())
			requestVer10Header.setPassword(isoRequest.getTerminalEncryptKey());
		else
			requestVer10Header.setPassword(requestPassword);
		requestVer10Header.setLicenseId(Integer.parseInt(getCredentialValue(terminalCd, "License=", getCredentialValue(merchantCd, "License=", String.valueOf(requestLicenseId)))));
		requestVer10Header.setSiteId(Integer.parseInt(getCredentialValue(terminalCd, "Site=", getCredentialValue(merchantCd, "Site=", merchantCd))));
		requestVer10Header.setDeviceId(Integer.parseInt(getCredentialValue(terminalCd, "Device=", getCredentialValue(merchantCd, "Device=", terminalCd))));
		requestVer10Header.setSiteTrace(String.valueOf(isoRequest.getTraceNumber()));
		
		PosRequestVer10 requestVer10 = new PosRequestVer10();
		requestVer10.setHeader(requestVer10Header);
		PosRequestVer10Transaction transaction = new PosRequestVer10Transaction();
		action.initTransaction(isoRequest, transaction);		
		requestVer10.setTransaction(transaction);
				
		PosRequest request = new PosRequest();
		request.setVer10(requestVer10);
		return request;
	}
	
	private String checkConnectionFailure(Throwable e)
	{
		Throwable cause = e;
		while (cause.getCause() != null)
		{
			cause = cause.getCause();
		}
		
		if (cause != null)
		{
			String causeMessage = cause.getMessage().toLowerCase();
			if (cause.getClass() == ConnectException.class
					|| cause.getClass() == ConnectionPoolTimeoutException.class
					|| cause.getClass() == ConnectTimeoutException.class
					|| cause.getClass() == MalformedURLException.class
					|| cause.getClass() == NoRouteToHostException.class
					|| (cause.getClass() == SocketException.class && causeMessage.indexOf("network is unreachable") > -1)
					|| (cause.getClass() == SocketTimeoutException.class && causeMessage.indexOf("connect timed out") > -1)
					|| cause.getClass() == UnknownHostException.class
					|| cause.getClass() == URIException.class
					|| cause.getClass() == URISyntaxException.class)
				return "Error connecting to host, " + cause.getClass().getName() + ": " + cause.getMessage();
		}
		return null;
	}
	
	private String checkReadTimeout(Throwable e)
	{
		Throwable cause = e;
		while (cause.getCause() != null)
		{
			cause = cause.getCause();
		}
		
		if (cause != null)
		{
			String causeMessage = cause.getMessage().toLowerCase();
			if (cause.getClass() == SocketTimeoutException.class && causeMessage.indexOf("read timed out") > -1)
				return "Timeout while waiting for host response, " + cause.getClass().getName() + ": " + cause.getMessage();
		}
		return null;
	}	
	
	private void checkConfigChanges()
	{
		//check configuration file for changes
		config.containsKey("ping");
	}
	
	private class ConfigReloader extends FileChangedReloadingStrategy
	{
		private ConfigReloader()
		{
			super();
		}

		@Override
		public void reloadingPerformed()
		{
			super.reloadingPerformed();

			log.warn("Configuration change detected");
			loadConfig();
		}
	}	
}
