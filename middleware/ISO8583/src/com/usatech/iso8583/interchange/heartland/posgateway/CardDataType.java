/**
 * CardDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class CardDataType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeTrackData trackData;

    private com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeManualEntry manualEntry;

    private com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData;

    public CardDataType() {
    }

    public CardDataType(
           com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeTrackData trackData,
           com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeManualEntry manualEntry,
           com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData) {
           this.trackData = trackData;
           this.manualEntry = manualEntry;
           this.encryptionData = encryptionData;
    }


    /**
     * Gets the trackData value for this CardDataType.
     * 
     * @return trackData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeTrackData getTrackData() {
        return trackData;
    }


    /**
     * Sets the trackData value for this CardDataType.
     * 
     * @param trackData
     */
    public void setTrackData(com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeTrackData trackData) {
        this.trackData = trackData;
    }


    /**
     * Gets the manualEntry value for this CardDataType.
     * 
     * @return manualEntry
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeManualEntry getManualEntry() {
        return manualEntry;
    }


    /**
     * Sets the manualEntry value for this CardDataType.
     * 
     * @param manualEntry
     */
    public void setManualEntry(com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeManualEntry manualEntry) {
        this.manualEntry = manualEntry;
    }


    /**
     * Gets the encryptionData value for this CardDataType.
     * 
     * @return encryptionData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType getEncryptionData() {
        return encryptionData;
    }


    /**
     * Sets the encryptionData value for this CardDataType.
     * 
     * @param encryptionData
     */
    public void setEncryptionData(com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData) {
        this.encryptionData = encryptionData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardDataType)) return false;
        CardDataType other = (CardDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.trackData==null && other.getTrackData()==null) || 
             (this.trackData!=null &&
              this.trackData.equals(other.getTrackData()))) &&
            ((this.manualEntry==null && other.getManualEntry()==null) || 
             (this.manualEntry!=null &&
              this.manualEntry.equals(other.getManualEntry()))) &&
            ((this.encryptionData==null && other.getEncryptionData()==null) || 
             (this.encryptionData!=null &&
              this.encryptionData.equals(other.getEncryptionData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTrackData() != null) {
            _hashCode += getTrackData().hashCode();
        }
        if (getManualEntry() != null) {
            _hashCode += getManualEntry().hashCode();
        }
        if (getEncryptionData() != null) {
            _hashCode += getEncryptionData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TrackData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">CardDataType>TrackData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manualEntry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ManualEntry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">CardDataType>ManualEntry"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptionData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
