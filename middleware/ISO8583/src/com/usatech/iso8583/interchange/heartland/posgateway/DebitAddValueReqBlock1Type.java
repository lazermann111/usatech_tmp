/**
 * DebitAddValueReqBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class DebitAddValueReqBlock1Type  implements java.io.Serializable {
    private java.lang.String trackData;

    private java.math.BigDecimal amt;

    private java.lang.String pinBlock;

    private com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup;

    private com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields;

    public DebitAddValueReqBlock1Type() {
    }

    public DebitAddValueReqBlock1Type(
           java.lang.String trackData,
           java.math.BigDecimal amt,
           java.lang.String pinBlock,
           com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup,
           com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
           this.trackData = trackData;
           this.amt = amt;
           this.pinBlock = pinBlock;
           this.cardHolderData = cardHolderData;
           this.allowDup = allowDup;
           this.encryptionData = encryptionData;
           this.additionalTxnFields = additionalTxnFields;
    }


    /**
     * Gets the trackData value for this DebitAddValueReqBlock1Type.
     * 
     * @return trackData
     */
    public java.lang.String getTrackData() {
        return trackData;
    }


    /**
     * Sets the trackData value for this DebitAddValueReqBlock1Type.
     * 
     * @param trackData
     */
    public void setTrackData(java.lang.String trackData) {
        this.trackData = trackData;
    }


    /**
     * Gets the amt value for this DebitAddValueReqBlock1Type.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this DebitAddValueReqBlock1Type.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the pinBlock value for this DebitAddValueReqBlock1Type.
     * 
     * @return pinBlock
     */
    public java.lang.String getPinBlock() {
        return pinBlock;
    }


    /**
     * Sets the pinBlock value for this DebitAddValueReqBlock1Type.
     * 
     * @param pinBlock
     */
    public void setPinBlock(java.lang.String pinBlock) {
        this.pinBlock = pinBlock;
    }


    /**
     * Gets the cardHolderData value for this DebitAddValueReqBlock1Type.
     * 
     * @return cardHolderData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType getCardHolderData() {
        return cardHolderData;
    }


    /**
     * Sets the cardHolderData value for this DebitAddValueReqBlock1Type.
     * 
     * @param cardHolderData
     */
    public void setCardHolderData(com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData) {
        this.cardHolderData = cardHolderData;
    }


    /**
     * Gets the allowDup value for this DebitAddValueReqBlock1Type.
     * 
     * @return allowDup
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getAllowDup() {
        return allowDup;
    }


    /**
     * Sets the allowDup value for this DebitAddValueReqBlock1Type.
     * 
     * @param allowDup
     */
    public void setAllowDup(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup) {
        this.allowDup = allowDup;
    }


    /**
     * Gets the encryptionData value for this DebitAddValueReqBlock1Type.
     * 
     * @return encryptionData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType getEncryptionData() {
        return encryptionData;
    }


    /**
     * Sets the encryptionData value for this DebitAddValueReqBlock1Type.
     * 
     * @param encryptionData
     */
    public void setEncryptionData(com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData) {
        this.encryptionData = encryptionData;
    }


    /**
     * Gets the additionalTxnFields value for this DebitAddValueReqBlock1Type.
     * 
     * @return additionalTxnFields
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType getAdditionalTxnFields() {
        return additionalTxnFields;
    }


    /**
     * Sets the additionalTxnFields value for this DebitAddValueReqBlock1Type.
     * 
     * @param additionalTxnFields
     */
    public void setAdditionalTxnFields(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
        this.additionalTxnFields = additionalTxnFields;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DebitAddValueReqBlock1Type)) return false;
        DebitAddValueReqBlock1Type other = (DebitAddValueReqBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.trackData==null && other.getTrackData()==null) || 
             (this.trackData!=null &&
              this.trackData.equals(other.getTrackData()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.pinBlock==null && other.getPinBlock()==null) || 
             (this.pinBlock!=null &&
              this.pinBlock.equals(other.getPinBlock()))) &&
            ((this.cardHolderData==null && other.getCardHolderData()==null) || 
             (this.cardHolderData!=null &&
              this.cardHolderData.equals(other.getCardHolderData()))) &&
            ((this.allowDup==null && other.getAllowDup()==null) || 
             (this.allowDup!=null &&
              this.allowDup.equals(other.getAllowDup()))) &&
            ((this.encryptionData==null && other.getEncryptionData()==null) || 
             (this.encryptionData!=null &&
              this.encryptionData.equals(other.getEncryptionData()))) &&
            ((this.additionalTxnFields==null && other.getAdditionalTxnFields()==null) || 
             (this.additionalTxnFields!=null &&
              this.additionalTxnFields.equals(other.getAdditionalTxnFields())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTrackData() != null) {
            _hashCode += getTrackData().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getPinBlock() != null) {
            _hashCode += getPinBlock().hashCode();
        }
        if (getCardHolderData() != null) {
            _hashCode += getCardHolderData().hashCode();
        }
        if (getAllowDup() != null) {
            _hashCode += getAllowDup().hashCode();
        }
        if (getEncryptionData() != null) {
            _hashCode += getEncryptionData().hashCode();
        }
        if (getAdditionalTxnFields() != null) {
            _hashCode += getAdditionalTxnFields().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DebitAddValueReqBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitAddValueReqBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TrackData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pinBlock");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PinBlock"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowDup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AllowDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptionData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalTxnFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFieldsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
