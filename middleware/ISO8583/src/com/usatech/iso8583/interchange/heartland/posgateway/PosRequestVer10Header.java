/**
 * PosRequestVer10Header.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosRequestVer10Header  implements java.io.Serializable {
    private int licenseId;

    private int siteId;

    private int deviceId;

    private java.lang.String userName;

    private java.lang.String password;

    private java.lang.String siteTrace;

    private java.lang.String developerID;

    private java.lang.String versionNbr;

    private java.lang.String clerkID;

    private com.usatech.iso8583.interchange.heartland.posgateway.GPSCoordinatesType GPSCoordinates;

    public PosRequestVer10Header() {
    }

    public PosRequestVer10Header(
           int licenseId,
           int siteId,
           int deviceId,
           java.lang.String userName,
           java.lang.String password,
           java.lang.String siteTrace,
           java.lang.String developerID,
           java.lang.String versionNbr,
           java.lang.String clerkID,
           com.usatech.iso8583.interchange.heartland.posgateway.GPSCoordinatesType GPSCoordinates) {
           this.licenseId = licenseId;
           this.siteId = siteId;
           this.deviceId = deviceId;
           this.userName = userName;
           this.password = password;
           this.siteTrace = siteTrace;
           this.developerID = developerID;
           this.versionNbr = versionNbr;
           this.clerkID = clerkID;
           this.GPSCoordinates = GPSCoordinates;
    }


    /**
     * Gets the licenseId value for this PosRequestVer10Header.
     * 
     * @return licenseId
     */
    public int getLicenseId() {
        return licenseId;
    }


    /**
     * Sets the licenseId value for this PosRequestVer10Header.
     * 
     * @param licenseId
     */
    public void setLicenseId(int licenseId) {
        this.licenseId = licenseId;
    }


    /**
     * Gets the siteId value for this PosRequestVer10Header.
     * 
     * @return siteId
     */
    public int getSiteId() {
        return siteId;
    }


    /**
     * Sets the siteId value for this PosRequestVer10Header.
     * 
     * @param siteId
     */
    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }


    /**
     * Gets the deviceId value for this PosRequestVer10Header.
     * 
     * @return deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosRequestVer10Header.
     * 
     * @param deviceId
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the userName value for this PosRequestVer10Header.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this PosRequestVer10Header.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
     * Gets the password value for this PosRequestVer10Header.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this PosRequestVer10Header.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the siteTrace value for this PosRequestVer10Header.
     * 
     * @return siteTrace
     */
    public java.lang.String getSiteTrace() {
        return siteTrace;
    }


    /**
     * Sets the siteTrace value for this PosRequestVer10Header.
     * 
     * @param siteTrace
     */
    public void setSiteTrace(java.lang.String siteTrace) {
        this.siteTrace = siteTrace;
    }


    /**
     * Gets the developerID value for this PosRequestVer10Header.
     * 
     * @return developerID
     */
    public java.lang.String getDeveloperID() {
        return developerID;
    }


    /**
     * Sets the developerID value for this PosRequestVer10Header.
     * 
     * @param developerID
     */
    public void setDeveloperID(java.lang.String developerID) {
        this.developerID = developerID;
    }


    /**
     * Gets the versionNbr value for this PosRequestVer10Header.
     * 
     * @return versionNbr
     */
    public java.lang.String getVersionNbr() {
        return versionNbr;
    }


    /**
     * Sets the versionNbr value for this PosRequestVer10Header.
     * 
     * @param versionNbr
     */
    public void setVersionNbr(java.lang.String versionNbr) {
        this.versionNbr = versionNbr;
    }


    /**
     * Gets the clerkID value for this PosRequestVer10Header.
     * 
     * @return clerkID
     */
    public java.lang.String getClerkID() {
        return clerkID;
    }


    /**
     * Sets the clerkID value for this PosRequestVer10Header.
     * 
     * @param clerkID
     */
    public void setClerkID(java.lang.String clerkID) {
        this.clerkID = clerkID;
    }


    /**
     * Gets the GPSCoordinates value for this PosRequestVer10Header.
     * 
     * @return GPSCoordinates
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.GPSCoordinatesType getGPSCoordinates() {
        return GPSCoordinates;
    }


    /**
     * Sets the GPSCoordinates value for this PosRequestVer10Header.
     * 
     * @param GPSCoordinates
     */
    public void setGPSCoordinates(com.usatech.iso8583.interchange.heartland.posgateway.GPSCoordinatesType GPSCoordinates) {
        this.GPSCoordinates = GPSCoordinates;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosRequestVer10Header)) return false;
        PosRequestVer10Header other = (PosRequestVer10Header) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.licenseId == other.getLicenseId() &&
            this.siteId == other.getSiteId() &&
            this.deviceId == other.getDeviceId() &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.siteTrace==null && other.getSiteTrace()==null) || 
             (this.siteTrace!=null &&
              this.siteTrace.equals(other.getSiteTrace()))) &&
            ((this.developerID==null && other.getDeveloperID()==null) || 
             (this.developerID!=null &&
              this.developerID.equals(other.getDeveloperID()))) &&
            ((this.versionNbr==null && other.getVersionNbr()==null) || 
             (this.versionNbr!=null &&
              this.versionNbr.equals(other.getVersionNbr()))) &&
            ((this.clerkID==null && other.getClerkID()==null) || 
             (this.clerkID!=null &&
              this.clerkID.equals(other.getClerkID()))) &&
            ((this.GPSCoordinates==null && other.getGPSCoordinates()==null) || 
             (this.GPSCoordinates!=null &&
              this.GPSCoordinates.equals(other.getGPSCoordinates())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getLicenseId();
        _hashCode += getSiteId();
        _hashCode += getDeviceId();
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getSiteTrace() != null) {
            _hashCode += getSiteTrace().hashCode();
        }
        if (getDeveloperID() != null) {
            _hashCode += getDeveloperID().hashCode();
        }
        if (getVersionNbr() != null) {
            _hashCode += getVersionNbr().hashCode();
        }
        if (getClerkID() != null) {
            _hashCode += getClerkID().hashCode();
        }
        if (getGPSCoordinates() != null) {
            _hashCode += getGPSCoordinates().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosRequestVer10Header.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosRequest>Ver1.0>Header"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LicenseId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "UserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteTrace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteTrace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("developerID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeveloperID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "VersionNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clerkID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ClerkID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GPSCoordinates");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GPSCoordinates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GPSCoordinatesType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
