/**
 * CardDataTypeManualEntry.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class CardDataTypeManualEntry  implements java.io.Serializable {
    private java.lang.String cardNbr;

    private int expMonth;

    private int expYear;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType cardPresent;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType readerPresent;

    private java.lang.String CVV2;

    private com.usatech.iso8583.interchange.heartland.posgateway.Cvv2Status CVV2Status;

    public CardDataTypeManualEntry() {
    }

    public CardDataTypeManualEntry(
           java.lang.String cardNbr,
           int expMonth,
           int expYear,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType cardPresent,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType readerPresent,
           java.lang.String CVV2,
           com.usatech.iso8583.interchange.heartland.posgateway.Cvv2Status CVV2Status) {
           this.cardNbr = cardNbr;
           this.expMonth = expMonth;
           this.expYear = expYear;
           this.cardPresent = cardPresent;
           this.readerPresent = readerPresent;
           this.CVV2 = CVV2;
           this.CVV2Status = CVV2Status;
    }


    /**
     * Gets the cardNbr value for this CardDataTypeManualEntry.
     * 
     * @return cardNbr
     */
    public java.lang.String getCardNbr() {
        return cardNbr;
    }


    /**
     * Sets the cardNbr value for this CardDataTypeManualEntry.
     * 
     * @param cardNbr
     */
    public void setCardNbr(java.lang.String cardNbr) {
        this.cardNbr = cardNbr;
    }


    /**
     * Gets the expMonth value for this CardDataTypeManualEntry.
     * 
     * @return expMonth
     */
    public int getExpMonth() {
        return expMonth;
    }


    /**
     * Sets the expMonth value for this CardDataTypeManualEntry.
     * 
     * @param expMonth
     */
    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }


    /**
     * Gets the expYear value for this CardDataTypeManualEntry.
     * 
     * @return expYear
     */
    public int getExpYear() {
        return expYear;
    }


    /**
     * Sets the expYear value for this CardDataTypeManualEntry.
     * 
     * @param expYear
     */
    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }


    /**
     * Gets the cardPresent value for this CardDataTypeManualEntry.
     * 
     * @return cardPresent
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getCardPresent() {
        return cardPresent;
    }


    /**
     * Sets the cardPresent value for this CardDataTypeManualEntry.
     * 
     * @param cardPresent
     */
    public void setCardPresent(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType cardPresent) {
        this.cardPresent = cardPresent;
    }


    /**
     * Gets the readerPresent value for this CardDataTypeManualEntry.
     * 
     * @return readerPresent
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getReaderPresent() {
        return readerPresent;
    }


    /**
     * Sets the readerPresent value for this CardDataTypeManualEntry.
     * 
     * @param readerPresent
     */
    public void setReaderPresent(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType readerPresent) {
        this.readerPresent = readerPresent;
    }


    /**
     * Gets the CVV2 value for this CardDataTypeManualEntry.
     * 
     * @return CVV2
     */
    public java.lang.String getCVV2() {
        return CVV2;
    }


    /**
     * Sets the CVV2 value for this CardDataTypeManualEntry.
     * 
     * @param CVV2
     */
    public void setCVV2(java.lang.String CVV2) {
        this.CVV2 = CVV2;
    }


    /**
     * Gets the CVV2Status value for this CardDataTypeManualEntry.
     * 
     * @return CVV2Status
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.Cvv2Status getCVV2Status() {
        return CVV2Status;
    }


    /**
     * Sets the CVV2Status value for this CardDataTypeManualEntry.
     * 
     * @param CVV2Status
     */
    public void setCVV2Status(com.usatech.iso8583.interchange.heartland.posgateway.Cvv2Status CVV2Status) {
        this.CVV2Status = CVV2Status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardDataTypeManualEntry)) return false;
        CardDataTypeManualEntry other = (CardDataTypeManualEntry) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardNbr==null && other.getCardNbr()==null) || 
             (this.cardNbr!=null &&
              this.cardNbr.equals(other.getCardNbr()))) &&
            this.expMonth == other.getExpMonth() &&
            this.expYear == other.getExpYear() &&
            ((this.cardPresent==null && other.getCardPresent()==null) || 
             (this.cardPresent!=null &&
              this.cardPresent.equals(other.getCardPresent()))) &&
            ((this.readerPresent==null && other.getReaderPresent()==null) || 
             (this.readerPresent!=null &&
              this.readerPresent.equals(other.getReaderPresent()))) &&
            ((this.CVV2==null && other.getCVV2()==null) || 
             (this.CVV2!=null &&
              this.CVV2.equals(other.getCVV2()))) &&
            ((this.CVV2Status==null && other.getCVV2Status()==null) || 
             (this.CVV2Status!=null &&
              this.CVV2Status.equals(other.getCVV2Status())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardNbr() != null) {
            _hashCode += getCardNbr().hashCode();
        }
        _hashCode += getExpMonth();
        _hashCode += getExpYear();
        if (getCardPresent() != null) {
            _hashCode += getCardPresent().hashCode();
        }
        if (getReaderPresent() != null) {
            _hashCode += getReaderPresent().hashCode();
        }
        if (getCVV2() != null) {
            _hashCode += getCVV2().hashCode();
        }
        if (getCVV2Status() != null) {
            _hashCode += getCVV2Status().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardDataTypeManualEntry.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">CardDataType>ManualEntry"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expMonth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExpMonth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expYear");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExpYear"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardPresent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardPresent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("readerPresent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReaderPresent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVV2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CVV2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVV2Status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CVV2Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "cvv2Status"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
