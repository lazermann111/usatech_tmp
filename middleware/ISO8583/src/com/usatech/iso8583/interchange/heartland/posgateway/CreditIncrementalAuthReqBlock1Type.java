/**
 * CreditIncrementalAuthReqBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class CreditIncrementalAuthReqBlock1Type  implements java.io.Serializable {
    private int gatewayTxnId;

    private java.math.BigDecimal amt;

    private com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType lodgingData;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields;

    public CreditIncrementalAuthReqBlock1Type() {
    }

    public CreditIncrementalAuthReqBlock1Type(
           int gatewayTxnId,
           java.math.BigDecimal amt,
           com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType lodgingData,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
           this.gatewayTxnId = gatewayTxnId;
           this.amt = amt;
           this.lodgingData = lodgingData;
           this.additionalTxnFields = additionalTxnFields;
    }


    /**
     * Gets the gatewayTxnId value for this CreditIncrementalAuthReqBlock1Type.
     * 
     * @return gatewayTxnId
     */
    public int getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this CreditIncrementalAuthReqBlock1Type.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(int gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the amt value for this CreditIncrementalAuthReqBlock1Type.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this CreditIncrementalAuthReqBlock1Type.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the lodgingData value for this CreditIncrementalAuthReqBlock1Type.
     * 
     * @return lodgingData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType getLodgingData() {
        return lodgingData;
    }


    /**
     * Sets the lodgingData value for this CreditIncrementalAuthReqBlock1Type.
     * 
     * @param lodgingData
     */
    public void setLodgingData(com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType lodgingData) {
        this.lodgingData = lodgingData;
    }


    /**
     * Gets the additionalTxnFields value for this CreditIncrementalAuthReqBlock1Type.
     * 
     * @return additionalTxnFields
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType getAdditionalTxnFields() {
        return additionalTxnFields;
    }


    /**
     * Sets the additionalTxnFields value for this CreditIncrementalAuthReqBlock1Type.
     * 
     * @param additionalTxnFields
     */
    public void setAdditionalTxnFields(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
        this.additionalTxnFields = additionalTxnFields;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreditIncrementalAuthReqBlock1Type)) return false;
        CreditIncrementalAuthReqBlock1Type other = (CreditIncrementalAuthReqBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.gatewayTxnId == other.getGatewayTxnId() &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.lodgingData==null && other.getLodgingData()==null) || 
             (this.lodgingData!=null &&
              this.lodgingData.equals(other.getLodgingData()))) &&
            ((this.additionalTxnFields==null && other.getAdditionalTxnFields()==null) || 
             (this.additionalTxnFields!=null &&
              this.additionalTxnFields.equals(other.getAdditionalTxnFields())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGatewayTxnId();
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getLodgingData() != null) {
            _hashCode += getLodgingData().hashCode();
        }
        if (getAdditionalTxnFields() != null) {
            _hashCode += getAdditionalTxnFields().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditIncrementalAuthReqBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditIncrementalAuthReqBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lodgingData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExtraChargesDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalTxnFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFieldsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
