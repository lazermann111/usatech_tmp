/**
 * PosReportBatchHistoryRspTypeHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportBatchHistoryRspTypeHeader  implements java.io.Serializable {
    private java.util.Calendar rptStartUtcDT;

    private java.util.Calendar rptEndUtcDT;

    private int siteId;

    private java.lang.String merchName;

    private java.lang.Integer deviceId;

    private int batchCnt;

    private java.math.BigDecimal batchAmt;

    public PosReportBatchHistoryRspTypeHeader() {
    }

    public PosReportBatchHistoryRspTypeHeader(
           java.util.Calendar rptStartUtcDT,
           java.util.Calendar rptEndUtcDT,
           int siteId,
           java.lang.String merchName,
           java.lang.Integer deviceId,
           int batchCnt,
           java.math.BigDecimal batchAmt) {
           this.rptStartUtcDT = rptStartUtcDT;
           this.rptEndUtcDT = rptEndUtcDT;
           this.siteId = siteId;
           this.merchName = merchName;
           this.deviceId = deviceId;
           this.batchCnt = batchCnt;
           this.batchAmt = batchAmt;
    }


    /**
     * Gets the rptStartUtcDT value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @return rptStartUtcDT
     */
    public java.util.Calendar getRptStartUtcDT() {
        return rptStartUtcDT;
    }


    /**
     * Sets the rptStartUtcDT value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @param rptStartUtcDT
     */
    public void setRptStartUtcDT(java.util.Calendar rptStartUtcDT) {
        this.rptStartUtcDT = rptStartUtcDT;
    }


    /**
     * Gets the rptEndUtcDT value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @return rptEndUtcDT
     */
    public java.util.Calendar getRptEndUtcDT() {
        return rptEndUtcDT;
    }


    /**
     * Sets the rptEndUtcDT value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @param rptEndUtcDT
     */
    public void setRptEndUtcDT(java.util.Calendar rptEndUtcDT) {
        this.rptEndUtcDT = rptEndUtcDT;
    }


    /**
     * Gets the siteId value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @return siteId
     */
    public int getSiteId() {
        return siteId;
    }


    /**
     * Sets the siteId value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @param siteId
     */
    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }


    /**
     * Gets the merchName value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @return merchName
     */
    public java.lang.String getMerchName() {
        return merchName;
    }


    /**
     * Sets the merchName value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @param merchName
     */
    public void setMerchName(java.lang.String merchName) {
        this.merchName = merchName;
    }


    /**
     * Gets the deviceId value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @return deviceId
     */
    public java.lang.Integer getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @param deviceId
     */
    public void setDeviceId(java.lang.Integer deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the batchCnt value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @return batchCnt
     */
    public int getBatchCnt() {
        return batchCnt;
    }


    /**
     * Sets the batchCnt value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @param batchCnt
     */
    public void setBatchCnt(int batchCnt) {
        this.batchCnt = batchCnt;
    }


    /**
     * Gets the batchAmt value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @return batchAmt
     */
    public java.math.BigDecimal getBatchAmt() {
        return batchAmt;
    }


    /**
     * Sets the batchAmt value for this PosReportBatchHistoryRspTypeHeader.
     * 
     * @param batchAmt
     */
    public void setBatchAmt(java.math.BigDecimal batchAmt) {
        this.batchAmt = batchAmt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportBatchHistoryRspTypeHeader)) return false;
        PosReportBatchHistoryRspTypeHeader other = (PosReportBatchHistoryRspTypeHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rptStartUtcDT==null && other.getRptStartUtcDT()==null) || 
             (this.rptStartUtcDT!=null &&
              this.rptStartUtcDT.equals(other.getRptStartUtcDT()))) &&
            ((this.rptEndUtcDT==null && other.getRptEndUtcDT()==null) || 
             (this.rptEndUtcDT!=null &&
              this.rptEndUtcDT.equals(other.getRptEndUtcDT()))) &&
            this.siteId == other.getSiteId() &&
            ((this.merchName==null && other.getMerchName()==null) || 
             (this.merchName!=null &&
              this.merchName.equals(other.getMerchName()))) &&
            ((this.deviceId==null && other.getDeviceId()==null) || 
             (this.deviceId!=null &&
              this.deviceId.equals(other.getDeviceId()))) &&
            this.batchCnt == other.getBatchCnt() &&
            ((this.batchAmt==null && other.getBatchAmt()==null) || 
             (this.batchAmt!=null &&
              this.batchAmt.equals(other.getBatchAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRptStartUtcDT() != null) {
            _hashCode += getRptStartUtcDT().hashCode();
        }
        if (getRptEndUtcDT() != null) {
            _hashCode += getRptEndUtcDT().hashCode();
        }
        _hashCode += getSiteId();
        if (getMerchName() != null) {
            _hashCode += getMerchName().hashCode();
        }
        if (getDeviceId() != null) {
            _hashCode += getDeviceId().hashCode();
        }
        _hashCode += getBatchCnt();
        if (getBatchAmt() != null) {
            _hashCode += getBatchAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportBatchHistoryRspTypeHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchHistoryRspType>Header"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rptStartUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RptStartUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rptEndUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RptEndUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
