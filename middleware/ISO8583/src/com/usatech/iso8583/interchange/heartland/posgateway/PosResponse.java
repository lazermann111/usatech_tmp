/**
 * PosResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosResponse  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10 ver10;

    private java.lang.String rootUrl;  // attribute

    public PosResponse() {
    }

    public PosResponse(
           com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10 ver10,
           java.lang.String rootUrl) {
           this.ver10 = ver10;
           this.rootUrl = rootUrl;
    }


    /**
     * Gets the ver10 value for this PosResponse.
     * 
     * @return ver10
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10 getVer10() {
        return ver10;
    }


    /**
     * Sets the ver10 value for this PosResponse.
     * 
     * @param ver10
     */
    public void setVer10(com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10 ver10) {
        this.ver10 = ver10;
    }


    /**
     * Gets the rootUrl value for this PosResponse.
     * 
     * @return rootUrl
     */
    public java.lang.String getRootUrl() {
        return rootUrl;
    }


    /**
     * Sets the rootUrl value for this PosResponse.
     * 
     * @param rootUrl
     */
    public void setRootUrl(java.lang.String rootUrl) {
        this.rootUrl = rootUrl;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosResponse)) return false;
        PosResponse other = (PosResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ver10==null && other.getVer10()==null) || 
             (this.ver10!=null &&
              this.ver10.equals(other.getVer10()))) &&
            ((this.rootUrl==null && other.getRootUrl()==null) || 
             (this.rootUrl!=null &&
              this.rootUrl.equals(other.getRootUrl())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVer10() != null) {
            _hashCode += getVer10().hashCode();
        }
        if (getRootUrl() != null) {
            _hashCode += getRootUrl().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosResponse"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("rootUrl");
        attrField.setXmlName(new javax.xml.namespace.QName("", "rootUrl"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ver10");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Ver1.0"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>PosResponse>Ver1.0"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
