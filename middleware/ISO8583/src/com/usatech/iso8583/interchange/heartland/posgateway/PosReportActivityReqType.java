/**
 * PosReportActivityReqType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportActivityReqType  implements java.io.Serializable {
    private java.util.Calendar rptStartUtcDT;

    private java.util.Calendar rptEndUtcDT;

    private java.lang.Integer deviceId;

    private com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion;

    public PosReportActivityReqType() {
    }

    public PosReportActivityReqType(
           java.util.Calendar rptStartUtcDT,
           java.util.Calendar rptEndUtcDT,
           java.lang.Integer deviceId,
           com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
           this.rptStartUtcDT = rptStartUtcDT;
           this.rptEndUtcDT = rptEndUtcDT;
           this.deviceId = deviceId;
           this.tzConversion = tzConversion;
    }


    /**
     * Gets the rptStartUtcDT value for this PosReportActivityReqType.
     * 
     * @return rptStartUtcDT
     */
    public java.util.Calendar getRptStartUtcDT() {
        return rptStartUtcDT;
    }


    /**
     * Sets the rptStartUtcDT value for this PosReportActivityReqType.
     * 
     * @param rptStartUtcDT
     */
    public void setRptStartUtcDT(java.util.Calendar rptStartUtcDT) {
        this.rptStartUtcDT = rptStartUtcDT;
    }


    /**
     * Gets the rptEndUtcDT value for this PosReportActivityReqType.
     * 
     * @return rptEndUtcDT
     */
    public java.util.Calendar getRptEndUtcDT() {
        return rptEndUtcDT;
    }


    /**
     * Sets the rptEndUtcDT value for this PosReportActivityReqType.
     * 
     * @param rptEndUtcDT
     */
    public void setRptEndUtcDT(java.util.Calendar rptEndUtcDT) {
        this.rptEndUtcDT = rptEndUtcDT;
    }


    /**
     * Gets the deviceId value for this PosReportActivityReqType.
     * 
     * @return deviceId
     */
    public java.lang.Integer getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportActivityReqType.
     * 
     * @param deviceId
     */
    public void setDeviceId(java.lang.Integer deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the tzConversion value for this PosReportActivityReqType.
     * 
     * @return tzConversion
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType getTzConversion() {
        return tzConversion;
    }


    /**
     * Sets the tzConversion value for this PosReportActivityReqType.
     * 
     * @param tzConversion
     */
    public void setTzConversion(com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
        this.tzConversion = tzConversion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportActivityReqType)) return false;
        PosReportActivityReqType other = (PosReportActivityReqType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rptStartUtcDT==null && other.getRptStartUtcDT()==null) || 
             (this.rptStartUtcDT!=null &&
              this.rptStartUtcDT.equals(other.getRptStartUtcDT()))) &&
            ((this.rptEndUtcDT==null && other.getRptEndUtcDT()==null) || 
             (this.rptEndUtcDT!=null &&
              this.rptEndUtcDT.equals(other.getRptEndUtcDT()))) &&
            ((this.deviceId==null && other.getDeviceId()==null) || 
             (this.deviceId!=null &&
              this.deviceId.equals(other.getDeviceId()))) &&
            ((this.tzConversion==null && other.getTzConversion()==null) || 
             (this.tzConversion!=null &&
              this.tzConversion.equals(other.getTzConversion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRptStartUtcDT() != null) {
            _hashCode += getRptStartUtcDT().hashCode();
        }
        if (getRptEndUtcDT() != null) {
            _hashCode += getRptEndUtcDT().hashCode();
        }
        if (getDeviceId() != null) {
            _hashCode += getDeviceId().hashCode();
        }
        if (getTzConversion() != null) {
            _hashCode += getTzConversion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportActivityReqType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportActivityReqType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rptStartUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RptStartUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rptEndUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RptEndUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tzConversion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TzConversion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "tzoneConversionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
