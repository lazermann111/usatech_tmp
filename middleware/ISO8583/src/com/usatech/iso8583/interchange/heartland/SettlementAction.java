package com.usatech.iso8583.interchange.heartland;

import java.text.ParseException;

import com.usatech.iso8583.BatchTotals;
import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.heartland.posgateway.PosBatchCloseRspType;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10Header;

public class SettlementAction extends HeartlandAction
{
	private static int[] requiredFields = { 
		ISO8583Message.FIELD_ACQUIRER_ID, 
		ISO8583Message.FIELD_TERMINAL_ID, 
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_BATCH_NUMBER, 
		ISO8583Message.FIELD_BATCH_TOTALS 
	};	
	
	public SettlementAction(HeartlandInterchange interchange) {
		super(interchange);
	}
	
	protected void validateRequestSpecificFields(ISO8583Request request) throws ValidationException
	{
		BatchTotals batchTotals = request.getBatchTotals();
		if (batchTotals.getCreditSaleCount() < 0)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Sale Count");

		if (batchTotals.getCreditSaleAmount() < 0)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Sale Amount");

		if (batchTotals.getCreditRefundCount() < 0)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Refund Count");

		if (batchTotals.getCreditRefundAmount() < 0)
			throw new ValidationException("Invalid Batch Totals Amount: Credit Refund Amount");
	}

	@Override
	protected void initTransaction(ISO8583Request isoRequest, PosRequestVer10Transaction transaction) throws ValidationException
	{
		transaction.setBatchClose("");
	}

	@Override
	protected boolean parseResponse(PosResponseVer10 resV10, ISO8583Response isoResponse) throws ParseException
	{
		PosResponseVer10Header resV10Header = resV10.getHeader();
		if (resV10Header.getSiteTrace() != null)
			isoResponse.setTraceNumber(Integer.valueOf(resV10Header.getSiteTrace()));
		// we have to treat certain response codes triggered by retransmissions due to timeouts as success...
		if (resV10Header.getGatewayRspCode() == GW_RESPONSE_OK
				&& resV10.getTransaction() != null
				&& resV10.getTransaction().getBatchClose() != null) {
			PosBatchCloseRspType messageResponse = resV10.getTransaction().getBatchClose();
			isoResponse.setResponseCode(ISO8583Response.SUCCESS);
			StringBuilder sb = new StringBuilder(resV10Header.getGatewayRspMsg() != null ? resV10Header.getGatewayRspMsg() : "Success")
				.append(", batchId: ").append(messageResponse.getBatchId())
				.append(", tranCount: ").append(messageResponse.getTxnCnt())
				.append(", totalAmount: ").append(messageResponse.getTotalAmt());
			isoResponse.setResponseMessage(sb.toString());
			isoResponse.setRetrievalReferenceNumber(String.valueOf(messageResponse.getBatchId()));
		} else {
			switch (resV10Header.getGatewayRspCode()) {
				case GW_RESPONSE_NO_CURRENT_BATCH:
					isoResponse.setResponseCode(ISO8583Response.SUCCESS);
					break;
				default:
					isoResponse.setResponseCode(new StringBuilder(GW_RESPONSE_CODE_PREFIX).append(resV10Header.getGatewayRspCode()).toString());
			}
			if (resV10Header.getGatewayRspMsg() != null)
				isoResponse.setResponseMessage(resV10Header.getGatewayRspMsg());
			isoResponse.setRetrievalReferenceNumber(String.valueOf(resV10Header.getGatewayTxnId()));
		}
		return true;
	}
	
	@Override
	protected int[] getRequiredFields() {
		return requiredFields;
	}
}
