/**
 * LodgingDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class LodgingDataType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.PrestigiousPropertyType prestigiousPropertyLimit;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType noShow;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdvancedDepositTypeType advancedDepositType;

    private com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType lodgingDataEdit;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType preferredCustomer;

    public LodgingDataType() {
    }

    public LodgingDataType(
           com.usatech.iso8583.interchange.heartland.posgateway.PrestigiousPropertyType prestigiousPropertyLimit,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType noShow,
           com.usatech.iso8583.interchange.heartland.posgateway.AdvancedDepositTypeType advancedDepositType,
           com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType lodgingDataEdit,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType preferredCustomer) {
           this.prestigiousPropertyLimit = prestigiousPropertyLimit;
           this.noShow = noShow;
           this.advancedDepositType = advancedDepositType;
           this.lodgingDataEdit = lodgingDataEdit;
           this.preferredCustomer = preferredCustomer;
    }


    /**
     * Gets the prestigiousPropertyLimit value for this LodgingDataType.
     * 
     * @return prestigiousPropertyLimit
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PrestigiousPropertyType getPrestigiousPropertyLimit() {
        return prestigiousPropertyLimit;
    }


    /**
     * Sets the prestigiousPropertyLimit value for this LodgingDataType.
     * 
     * @param prestigiousPropertyLimit
     */
    public void setPrestigiousPropertyLimit(com.usatech.iso8583.interchange.heartland.posgateway.PrestigiousPropertyType prestigiousPropertyLimit) {
        this.prestigiousPropertyLimit = prestigiousPropertyLimit;
    }


    /**
     * Gets the noShow value for this LodgingDataType.
     * 
     * @return noShow
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getNoShow() {
        return noShow;
    }


    /**
     * Sets the noShow value for this LodgingDataType.
     * 
     * @param noShow
     */
    public void setNoShow(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType noShow) {
        this.noShow = noShow;
    }


    /**
     * Gets the advancedDepositType value for this LodgingDataType.
     * 
     * @return advancedDepositType
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdvancedDepositTypeType getAdvancedDepositType() {
        return advancedDepositType;
    }


    /**
     * Sets the advancedDepositType value for this LodgingDataType.
     * 
     * @param advancedDepositType
     */
    public void setAdvancedDepositType(com.usatech.iso8583.interchange.heartland.posgateway.AdvancedDepositTypeType advancedDepositType) {
        this.advancedDepositType = advancedDepositType;
    }


    /**
     * Gets the lodgingDataEdit value for this LodgingDataType.
     * 
     * @return lodgingDataEdit
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType getLodgingDataEdit() {
        return lodgingDataEdit;
    }


    /**
     * Sets the lodgingDataEdit value for this LodgingDataType.
     * 
     * @param lodgingDataEdit
     */
    public void setLodgingDataEdit(com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType lodgingDataEdit) {
        this.lodgingDataEdit = lodgingDataEdit;
    }


    /**
     * Gets the preferredCustomer value for this LodgingDataType.
     * 
     * @return preferredCustomer
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getPreferredCustomer() {
        return preferredCustomer;
    }


    /**
     * Sets the preferredCustomer value for this LodgingDataType.
     * 
     * @param preferredCustomer
     */
    public void setPreferredCustomer(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType preferredCustomer) {
        this.preferredCustomer = preferredCustomer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LodgingDataType)) return false;
        LodgingDataType other = (LodgingDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.prestigiousPropertyLimit==null && other.getPrestigiousPropertyLimit()==null) || 
             (this.prestigiousPropertyLimit!=null &&
              this.prestigiousPropertyLimit.equals(other.getPrestigiousPropertyLimit()))) &&
            ((this.noShow==null && other.getNoShow()==null) || 
             (this.noShow!=null &&
              this.noShow.equals(other.getNoShow()))) &&
            ((this.advancedDepositType==null && other.getAdvancedDepositType()==null) || 
             (this.advancedDepositType!=null &&
              this.advancedDepositType.equals(other.getAdvancedDepositType()))) &&
            ((this.lodgingDataEdit==null && other.getLodgingDataEdit()==null) || 
             (this.lodgingDataEdit!=null &&
              this.lodgingDataEdit.equals(other.getLodgingDataEdit()))) &&
            ((this.preferredCustomer==null && other.getPreferredCustomer()==null) || 
             (this.preferredCustomer!=null &&
              this.preferredCustomer.equals(other.getPreferredCustomer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPrestigiousPropertyLimit() != null) {
            _hashCode += getPrestigiousPropertyLimit().hashCode();
        }
        if (getNoShow() != null) {
            _hashCode += getNoShow().hashCode();
        }
        if (getAdvancedDepositType() != null) {
            _hashCode += getAdvancedDepositType().hashCode();
        }
        if (getLodgingDataEdit() != null) {
            _hashCode += getLodgingDataEdit().hashCode();
        }
        if (getPreferredCustomer() != null) {
            _hashCode += getPreferredCustomer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LodgingDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prestigiousPropertyLimit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PrestigiousPropertyLimit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "prestigiousPropertyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("noShow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "NoShow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("advancedDepositType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdvancedDepositType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "advancedDepositTypeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lodgingDataEdit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataEdit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataEditType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preferredCustomer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PreferredCustomer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
