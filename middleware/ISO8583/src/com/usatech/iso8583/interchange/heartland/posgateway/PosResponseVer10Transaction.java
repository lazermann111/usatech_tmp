/**
 * PosResponseVer10Transaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosResponseVer10Transaction  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType prePaidAddValue;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailRspType reportBatchDetail;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryRspType reportBatchHistory;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspType reportBatchSummary;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsRspType reportOpenAuths;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchRspType reportSearch;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspType reportTxnDetail;

    private java.lang.String testCredentials;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTBalanceInquiry;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosBatchCloseRspType batchClose;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditAccountVerify;

    private java.lang.String creditAddToBatch;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditAuth;

    private java.lang.String creditCPCEdit;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditIncrementalAuth;

    private java.lang.String creditOfflineAuth;

    private java.lang.String creditOfflineSale;

    private java.lang.String creditReturn;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditReversal;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditSale;

    private java.lang.String creditTxnEdit;

    private java.lang.String creditVoid;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitAddValue;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitReturn;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitReversal;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitSale;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceRspType giftCardBalance;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTCashBackPurchase;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTCashBenefitWithdrawal;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTFSPurchase;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTFSReturn;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTVoucherPurchase;

    private java.lang.Object endToEndTest;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateRspType giftCardActivate;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueRspType giftCardAddValue;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleRspType giftCardSale;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityRspType reportActivity;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateRspType giftCardDeactivate;

    private com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType giftCardPreviousDayTotals;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceRspType giftCardReplace;

    private com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType giftCardCurrentDayTotals;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidRspType giftCardVoid;

    private java.lang.Object addAttachment;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType prePaidBalanceInquiry;

    private com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType recurringBilling;

    public PosResponseVer10Transaction() {
    }

    public PosResponseVer10Transaction(
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType prePaidAddValue,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailRspType reportBatchDetail,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryRspType reportBatchHistory,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspType reportBatchSummary,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsRspType reportOpenAuths,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchRspType reportSearch,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspType reportTxnDetail,
           java.lang.String testCredentials,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTBalanceInquiry,
           com.usatech.iso8583.interchange.heartland.posgateway.PosBatchCloseRspType batchClose,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditAccountVerify,
           java.lang.String creditAddToBatch,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditAuth,
           java.lang.String creditCPCEdit,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditIncrementalAuth,
           java.lang.String creditOfflineAuth,
           java.lang.String creditOfflineSale,
           java.lang.String creditReturn,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditReversal,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditSale,
           java.lang.String creditTxnEdit,
           java.lang.String creditVoid,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitAddValue,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitReturn,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitReversal,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitSale,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceRspType giftCardBalance,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTCashBackPurchase,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTCashBenefitWithdrawal,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTFSPurchase,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTFSReturn,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTVoucherPurchase,
           java.lang.Object endToEndTest,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateRspType giftCardActivate,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueRspType giftCardAddValue,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleRspType giftCardSale,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityRspType reportActivity,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateRspType giftCardDeactivate,
           com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType giftCardPreviousDayTotals,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceRspType giftCardReplace,
           com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType giftCardCurrentDayTotals,
           com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidRspType giftCardVoid,
           java.lang.Object addAttachment,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType prePaidBalanceInquiry,
           com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType recurringBilling) {
           this.prePaidAddValue = prePaidAddValue;
           this.reportBatchDetail = reportBatchDetail;
           this.reportBatchHistory = reportBatchHistory;
           this.reportBatchSummary = reportBatchSummary;
           this.reportOpenAuths = reportOpenAuths;
           this.reportSearch = reportSearch;
           this.reportTxnDetail = reportTxnDetail;
           this.testCredentials = testCredentials;
           this.EBTBalanceInquiry = EBTBalanceInquiry;
           this.batchClose = batchClose;
           this.creditAccountVerify = creditAccountVerify;
           this.creditAddToBatch = creditAddToBatch;
           this.creditAuth = creditAuth;
           this.creditCPCEdit = creditCPCEdit;
           this.creditIncrementalAuth = creditIncrementalAuth;
           this.creditOfflineAuth = creditOfflineAuth;
           this.creditOfflineSale = creditOfflineSale;
           this.creditReturn = creditReturn;
           this.creditReversal = creditReversal;
           this.creditSale = creditSale;
           this.creditTxnEdit = creditTxnEdit;
           this.creditVoid = creditVoid;
           this.debitAddValue = debitAddValue;
           this.debitReturn = debitReturn;
           this.debitReversal = debitReversal;
           this.debitSale = debitSale;
           this.giftCardBalance = giftCardBalance;
           this.EBTCashBackPurchase = EBTCashBackPurchase;
           this.EBTCashBenefitWithdrawal = EBTCashBenefitWithdrawal;
           this.EBTFSPurchase = EBTFSPurchase;
           this.EBTFSReturn = EBTFSReturn;
           this.EBTVoucherPurchase = EBTVoucherPurchase;
           this.endToEndTest = endToEndTest;
           this.giftCardActivate = giftCardActivate;
           this.giftCardAddValue = giftCardAddValue;
           this.giftCardSale = giftCardSale;
           this.reportActivity = reportActivity;
           this.giftCardDeactivate = giftCardDeactivate;
           this.giftCardPreviousDayTotals = giftCardPreviousDayTotals;
           this.giftCardReplace = giftCardReplace;
           this.giftCardCurrentDayTotals = giftCardCurrentDayTotals;
           this.giftCardVoid = giftCardVoid;
           this.addAttachment = addAttachment;
           this.prePaidBalanceInquiry = prePaidBalanceInquiry;
           this.recurringBilling = recurringBilling;
    }


    /**
     * Gets the prePaidAddValue value for this PosResponseVer10Transaction.
     * 
     * @return prePaidAddValue
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getPrePaidAddValue() {
        return prePaidAddValue;
    }


    /**
     * Sets the prePaidAddValue value for this PosResponseVer10Transaction.
     * 
     * @param prePaidAddValue
     */
    public void setPrePaidAddValue(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType prePaidAddValue) {
        this.prePaidAddValue = prePaidAddValue;
    }


    /**
     * Gets the reportBatchDetail value for this PosResponseVer10Transaction.
     * 
     * @return reportBatchDetail
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailRspType getReportBatchDetail() {
        return reportBatchDetail;
    }


    /**
     * Sets the reportBatchDetail value for this PosResponseVer10Transaction.
     * 
     * @param reportBatchDetail
     */
    public void setReportBatchDetail(com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailRspType reportBatchDetail) {
        this.reportBatchDetail = reportBatchDetail;
    }


    /**
     * Gets the reportBatchHistory value for this PosResponseVer10Transaction.
     * 
     * @return reportBatchHistory
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryRspType getReportBatchHistory() {
        return reportBatchHistory;
    }


    /**
     * Sets the reportBatchHistory value for this PosResponseVer10Transaction.
     * 
     * @param reportBatchHistory
     */
    public void setReportBatchHistory(com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryRspType reportBatchHistory) {
        this.reportBatchHistory = reportBatchHistory;
    }


    /**
     * Gets the reportBatchSummary value for this PosResponseVer10Transaction.
     * 
     * @return reportBatchSummary
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspType getReportBatchSummary() {
        return reportBatchSummary;
    }


    /**
     * Sets the reportBatchSummary value for this PosResponseVer10Transaction.
     * 
     * @param reportBatchSummary
     */
    public void setReportBatchSummary(com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspType reportBatchSummary) {
        this.reportBatchSummary = reportBatchSummary;
    }


    /**
     * Gets the reportOpenAuths value for this PosResponseVer10Transaction.
     * 
     * @return reportOpenAuths
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsRspType getReportOpenAuths() {
        return reportOpenAuths;
    }


    /**
     * Sets the reportOpenAuths value for this PosResponseVer10Transaction.
     * 
     * @param reportOpenAuths
     */
    public void setReportOpenAuths(com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsRspType reportOpenAuths) {
        this.reportOpenAuths = reportOpenAuths;
    }


    /**
     * Gets the reportSearch value for this PosResponseVer10Transaction.
     * 
     * @return reportSearch
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchRspType getReportSearch() {
        return reportSearch;
    }


    /**
     * Sets the reportSearch value for this PosResponseVer10Transaction.
     * 
     * @param reportSearch
     */
    public void setReportSearch(com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchRspType reportSearch) {
        this.reportSearch = reportSearch;
    }


    /**
     * Gets the reportTxnDetail value for this PosResponseVer10Transaction.
     * 
     * @return reportTxnDetail
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspType getReportTxnDetail() {
        return reportTxnDetail;
    }


    /**
     * Sets the reportTxnDetail value for this PosResponseVer10Transaction.
     * 
     * @param reportTxnDetail
     */
    public void setReportTxnDetail(com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspType reportTxnDetail) {
        this.reportTxnDetail = reportTxnDetail;
    }


    /**
     * Gets the testCredentials value for this PosResponseVer10Transaction.
     * 
     * @return testCredentials
     */
    public java.lang.String getTestCredentials() {
        return testCredentials;
    }


    /**
     * Sets the testCredentials value for this PosResponseVer10Transaction.
     * 
     * @param testCredentials
     */
    public void setTestCredentials(java.lang.String testCredentials) {
        this.testCredentials = testCredentials;
    }


    /**
     * Gets the EBTBalanceInquiry value for this PosResponseVer10Transaction.
     * 
     * @return EBTBalanceInquiry
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getEBTBalanceInquiry() {
        return EBTBalanceInquiry;
    }


    /**
     * Sets the EBTBalanceInquiry value for this PosResponseVer10Transaction.
     * 
     * @param EBTBalanceInquiry
     */
    public void setEBTBalanceInquiry(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTBalanceInquiry) {
        this.EBTBalanceInquiry = EBTBalanceInquiry;
    }


    /**
     * Gets the batchClose value for this PosResponseVer10Transaction.
     * 
     * @return batchClose
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosBatchCloseRspType getBatchClose() {
        return batchClose;
    }


    /**
     * Sets the batchClose value for this PosResponseVer10Transaction.
     * 
     * @param batchClose
     */
    public void setBatchClose(com.usatech.iso8583.interchange.heartland.posgateway.PosBatchCloseRspType batchClose) {
        this.batchClose = batchClose;
    }


    /**
     * Gets the creditAccountVerify value for this PosResponseVer10Transaction.
     * 
     * @return creditAccountVerify
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getCreditAccountVerify() {
        return creditAccountVerify;
    }


    /**
     * Sets the creditAccountVerify value for this PosResponseVer10Transaction.
     * 
     * @param creditAccountVerify
     */
    public void setCreditAccountVerify(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditAccountVerify) {
        this.creditAccountVerify = creditAccountVerify;
    }


    /**
     * Gets the creditAddToBatch value for this PosResponseVer10Transaction.
     * 
     * @return creditAddToBatch
     */
    public java.lang.String getCreditAddToBatch() {
        return creditAddToBatch;
    }


    /**
     * Sets the creditAddToBatch value for this PosResponseVer10Transaction.
     * 
     * @param creditAddToBatch
     */
    public void setCreditAddToBatch(java.lang.String creditAddToBatch) {
        this.creditAddToBatch = creditAddToBatch;
    }


    /**
     * Gets the creditAuth value for this PosResponseVer10Transaction.
     * 
     * @return creditAuth
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getCreditAuth() {
        return creditAuth;
    }


    /**
     * Sets the creditAuth value for this PosResponseVer10Transaction.
     * 
     * @param creditAuth
     */
    public void setCreditAuth(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditAuth) {
        this.creditAuth = creditAuth;
    }


    /**
     * Gets the creditCPCEdit value for this PosResponseVer10Transaction.
     * 
     * @return creditCPCEdit
     */
    public java.lang.String getCreditCPCEdit() {
        return creditCPCEdit;
    }


    /**
     * Sets the creditCPCEdit value for this PosResponseVer10Transaction.
     * 
     * @param creditCPCEdit
     */
    public void setCreditCPCEdit(java.lang.String creditCPCEdit) {
        this.creditCPCEdit = creditCPCEdit;
    }


    /**
     * Gets the creditIncrementalAuth value for this PosResponseVer10Transaction.
     * 
     * @return creditIncrementalAuth
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getCreditIncrementalAuth() {
        return creditIncrementalAuth;
    }


    /**
     * Sets the creditIncrementalAuth value for this PosResponseVer10Transaction.
     * 
     * @param creditIncrementalAuth
     */
    public void setCreditIncrementalAuth(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditIncrementalAuth) {
        this.creditIncrementalAuth = creditIncrementalAuth;
    }


    /**
     * Gets the creditOfflineAuth value for this PosResponseVer10Transaction.
     * 
     * @return creditOfflineAuth
     */
    public java.lang.String getCreditOfflineAuth() {
        return creditOfflineAuth;
    }


    /**
     * Sets the creditOfflineAuth value for this PosResponseVer10Transaction.
     * 
     * @param creditOfflineAuth
     */
    public void setCreditOfflineAuth(java.lang.String creditOfflineAuth) {
        this.creditOfflineAuth = creditOfflineAuth;
    }


    /**
     * Gets the creditOfflineSale value for this PosResponseVer10Transaction.
     * 
     * @return creditOfflineSale
     */
    public java.lang.String getCreditOfflineSale() {
        return creditOfflineSale;
    }


    /**
     * Sets the creditOfflineSale value for this PosResponseVer10Transaction.
     * 
     * @param creditOfflineSale
     */
    public void setCreditOfflineSale(java.lang.String creditOfflineSale) {
        this.creditOfflineSale = creditOfflineSale;
    }


    /**
     * Gets the creditReturn value for this PosResponseVer10Transaction.
     * 
     * @return creditReturn
     */
    public java.lang.String getCreditReturn() {
        return creditReturn;
    }


    /**
     * Sets the creditReturn value for this PosResponseVer10Transaction.
     * 
     * @param creditReturn
     */
    public void setCreditReturn(java.lang.String creditReturn) {
        this.creditReturn = creditReturn;
    }


    /**
     * Gets the creditReversal value for this PosResponseVer10Transaction.
     * 
     * @return creditReversal
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getCreditReversal() {
        return creditReversal;
    }


    /**
     * Sets the creditReversal value for this PosResponseVer10Transaction.
     * 
     * @param creditReversal
     */
    public void setCreditReversal(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditReversal) {
        this.creditReversal = creditReversal;
    }


    /**
     * Gets the creditSale value for this PosResponseVer10Transaction.
     * 
     * @return creditSale
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getCreditSale() {
        return creditSale;
    }


    /**
     * Sets the creditSale value for this PosResponseVer10Transaction.
     * 
     * @param creditSale
     */
    public void setCreditSale(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType creditSale) {
        this.creditSale = creditSale;
    }


    /**
     * Gets the creditTxnEdit value for this PosResponseVer10Transaction.
     * 
     * @return creditTxnEdit
     */
    public java.lang.String getCreditTxnEdit() {
        return creditTxnEdit;
    }


    /**
     * Sets the creditTxnEdit value for this PosResponseVer10Transaction.
     * 
     * @param creditTxnEdit
     */
    public void setCreditTxnEdit(java.lang.String creditTxnEdit) {
        this.creditTxnEdit = creditTxnEdit;
    }


    /**
     * Gets the creditVoid value for this PosResponseVer10Transaction.
     * 
     * @return creditVoid
     */
    public java.lang.String getCreditVoid() {
        return creditVoid;
    }


    /**
     * Sets the creditVoid value for this PosResponseVer10Transaction.
     * 
     * @param creditVoid
     */
    public void setCreditVoid(java.lang.String creditVoid) {
        this.creditVoid = creditVoid;
    }


    /**
     * Gets the debitAddValue value for this PosResponseVer10Transaction.
     * 
     * @return debitAddValue
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getDebitAddValue() {
        return debitAddValue;
    }


    /**
     * Sets the debitAddValue value for this PosResponseVer10Transaction.
     * 
     * @param debitAddValue
     */
    public void setDebitAddValue(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitAddValue) {
        this.debitAddValue = debitAddValue;
    }


    /**
     * Gets the debitReturn value for this PosResponseVer10Transaction.
     * 
     * @return debitReturn
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getDebitReturn() {
        return debitReturn;
    }


    /**
     * Sets the debitReturn value for this PosResponseVer10Transaction.
     * 
     * @param debitReturn
     */
    public void setDebitReturn(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitReturn) {
        this.debitReturn = debitReturn;
    }


    /**
     * Gets the debitReversal value for this PosResponseVer10Transaction.
     * 
     * @return debitReversal
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getDebitReversal() {
        return debitReversal;
    }


    /**
     * Sets the debitReversal value for this PosResponseVer10Transaction.
     * 
     * @param debitReversal
     */
    public void setDebitReversal(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitReversal) {
        this.debitReversal = debitReversal;
    }


    /**
     * Gets the debitSale value for this PosResponseVer10Transaction.
     * 
     * @return debitSale
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getDebitSale() {
        return debitSale;
    }


    /**
     * Sets the debitSale value for this PosResponseVer10Transaction.
     * 
     * @param debitSale
     */
    public void setDebitSale(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType debitSale) {
        this.debitSale = debitSale;
    }


    /**
     * Gets the giftCardBalance value for this PosResponseVer10Transaction.
     * 
     * @return giftCardBalance
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceRspType getGiftCardBalance() {
        return giftCardBalance;
    }


    /**
     * Sets the giftCardBalance value for this PosResponseVer10Transaction.
     * 
     * @param giftCardBalance
     */
    public void setGiftCardBalance(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceRspType giftCardBalance) {
        this.giftCardBalance = giftCardBalance;
    }


    /**
     * Gets the EBTCashBackPurchase value for this PosResponseVer10Transaction.
     * 
     * @return EBTCashBackPurchase
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getEBTCashBackPurchase() {
        return EBTCashBackPurchase;
    }


    /**
     * Sets the EBTCashBackPurchase value for this PosResponseVer10Transaction.
     * 
     * @param EBTCashBackPurchase
     */
    public void setEBTCashBackPurchase(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTCashBackPurchase) {
        this.EBTCashBackPurchase = EBTCashBackPurchase;
    }


    /**
     * Gets the EBTCashBenefitWithdrawal value for this PosResponseVer10Transaction.
     * 
     * @return EBTCashBenefitWithdrawal
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getEBTCashBenefitWithdrawal() {
        return EBTCashBenefitWithdrawal;
    }


    /**
     * Sets the EBTCashBenefitWithdrawal value for this PosResponseVer10Transaction.
     * 
     * @param EBTCashBenefitWithdrawal
     */
    public void setEBTCashBenefitWithdrawal(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTCashBenefitWithdrawal) {
        this.EBTCashBenefitWithdrawal = EBTCashBenefitWithdrawal;
    }


    /**
     * Gets the EBTFSPurchase value for this PosResponseVer10Transaction.
     * 
     * @return EBTFSPurchase
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getEBTFSPurchase() {
        return EBTFSPurchase;
    }


    /**
     * Sets the EBTFSPurchase value for this PosResponseVer10Transaction.
     * 
     * @param EBTFSPurchase
     */
    public void setEBTFSPurchase(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTFSPurchase) {
        this.EBTFSPurchase = EBTFSPurchase;
    }


    /**
     * Gets the EBTFSReturn value for this PosResponseVer10Transaction.
     * 
     * @return EBTFSReturn
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getEBTFSReturn() {
        return EBTFSReturn;
    }


    /**
     * Sets the EBTFSReturn value for this PosResponseVer10Transaction.
     * 
     * @param EBTFSReturn
     */
    public void setEBTFSReturn(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTFSReturn) {
        this.EBTFSReturn = EBTFSReturn;
    }


    /**
     * Gets the EBTVoucherPurchase value for this PosResponseVer10Transaction.
     * 
     * @return EBTVoucherPurchase
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getEBTVoucherPurchase() {
        return EBTVoucherPurchase;
    }


    /**
     * Sets the EBTVoucherPurchase value for this PosResponseVer10Transaction.
     * 
     * @param EBTVoucherPurchase
     */
    public void setEBTVoucherPurchase(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType EBTVoucherPurchase) {
        this.EBTVoucherPurchase = EBTVoucherPurchase;
    }


    /**
     * Gets the endToEndTest value for this PosResponseVer10Transaction.
     * 
     * @return endToEndTest
     */
    public java.lang.Object getEndToEndTest() {
        return endToEndTest;
    }


    /**
     * Sets the endToEndTest value for this PosResponseVer10Transaction.
     * 
     * @param endToEndTest
     */
    public void setEndToEndTest(java.lang.Object endToEndTest) {
        this.endToEndTest = endToEndTest;
    }


    /**
     * Gets the giftCardActivate value for this PosResponseVer10Transaction.
     * 
     * @return giftCardActivate
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateRspType getGiftCardActivate() {
        return giftCardActivate;
    }


    /**
     * Sets the giftCardActivate value for this PosResponseVer10Transaction.
     * 
     * @param giftCardActivate
     */
    public void setGiftCardActivate(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateRspType giftCardActivate) {
        this.giftCardActivate = giftCardActivate;
    }


    /**
     * Gets the giftCardAddValue value for this PosResponseVer10Transaction.
     * 
     * @return giftCardAddValue
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueRspType getGiftCardAddValue() {
        return giftCardAddValue;
    }


    /**
     * Sets the giftCardAddValue value for this PosResponseVer10Transaction.
     * 
     * @param giftCardAddValue
     */
    public void setGiftCardAddValue(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueRspType giftCardAddValue) {
        this.giftCardAddValue = giftCardAddValue;
    }


    /**
     * Gets the giftCardSale value for this PosResponseVer10Transaction.
     * 
     * @return giftCardSale
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleRspType getGiftCardSale() {
        return giftCardSale;
    }


    /**
     * Sets the giftCardSale value for this PosResponseVer10Transaction.
     * 
     * @param giftCardSale
     */
    public void setGiftCardSale(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleRspType giftCardSale) {
        this.giftCardSale = giftCardSale;
    }


    /**
     * Gets the reportActivity value for this PosResponseVer10Transaction.
     * 
     * @return reportActivity
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityRspType getReportActivity() {
        return reportActivity;
    }


    /**
     * Sets the reportActivity value for this PosResponseVer10Transaction.
     * 
     * @param reportActivity
     */
    public void setReportActivity(com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityRspType reportActivity) {
        this.reportActivity = reportActivity;
    }


    /**
     * Gets the giftCardDeactivate value for this PosResponseVer10Transaction.
     * 
     * @return giftCardDeactivate
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateRspType getGiftCardDeactivate() {
        return giftCardDeactivate;
    }


    /**
     * Sets the giftCardDeactivate value for this PosResponseVer10Transaction.
     * 
     * @param giftCardDeactivate
     */
    public void setGiftCardDeactivate(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateRspType giftCardDeactivate) {
        this.giftCardDeactivate = giftCardDeactivate;
    }


    /**
     * Gets the giftCardPreviousDayTotals value for this PosResponseVer10Transaction.
     * 
     * @return giftCardPreviousDayTotals
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType getGiftCardPreviousDayTotals() {
        return giftCardPreviousDayTotals;
    }


    /**
     * Sets the giftCardPreviousDayTotals value for this PosResponseVer10Transaction.
     * 
     * @param giftCardPreviousDayTotals
     */
    public void setGiftCardPreviousDayTotals(com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType giftCardPreviousDayTotals) {
        this.giftCardPreviousDayTotals = giftCardPreviousDayTotals;
    }


    /**
     * Gets the giftCardReplace value for this PosResponseVer10Transaction.
     * 
     * @return giftCardReplace
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceRspType getGiftCardReplace() {
        return giftCardReplace;
    }


    /**
     * Sets the giftCardReplace value for this PosResponseVer10Transaction.
     * 
     * @param giftCardReplace
     */
    public void setGiftCardReplace(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceRspType giftCardReplace) {
        this.giftCardReplace = giftCardReplace;
    }


    /**
     * Gets the giftCardCurrentDayTotals value for this PosResponseVer10Transaction.
     * 
     * @return giftCardCurrentDayTotals
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType getGiftCardCurrentDayTotals() {
        return giftCardCurrentDayTotals;
    }


    /**
     * Sets the giftCardCurrentDayTotals value for this PosResponseVer10Transaction.
     * 
     * @param giftCardCurrentDayTotals
     */
    public void setGiftCardCurrentDayTotals(com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType giftCardCurrentDayTotals) {
        this.giftCardCurrentDayTotals = giftCardCurrentDayTotals;
    }


    /**
     * Gets the giftCardVoid value for this PosResponseVer10Transaction.
     * 
     * @return giftCardVoid
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidRspType getGiftCardVoid() {
        return giftCardVoid;
    }


    /**
     * Sets the giftCardVoid value for this PosResponseVer10Transaction.
     * 
     * @param giftCardVoid
     */
    public void setGiftCardVoid(com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidRspType giftCardVoid) {
        this.giftCardVoid = giftCardVoid;
    }


    /**
     * Gets the addAttachment value for this PosResponseVer10Transaction.
     * 
     * @return addAttachment
     */
    public java.lang.Object getAddAttachment() {
        return addAttachment;
    }


    /**
     * Sets the addAttachment value for this PosResponseVer10Transaction.
     * 
     * @param addAttachment
     */
    public void setAddAttachment(java.lang.Object addAttachment) {
        this.addAttachment = addAttachment;
    }


    /**
     * Gets the prePaidBalanceInquiry value for this PosResponseVer10Transaction.
     * 
     * @return prePaidBalanceInquiry
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getPrePaidBalanceInquiry() {
        return prePaidBalanceInquiry;
    }


    /**
     * Sets the prePaidBalanceInquiry value for this PosResponseVer10Transaction.
     * 
     * @param prePaidBalanceInquiry
     */
    public void setPrePaidBalanceInquiry(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType prePaidBalanceInquiry) {
        this.prePaidBalanceInquiry = prePaidBalanceInquiry;
    }


    /**
     * Gets the recurringBilling value for this PosResponseVer10Transaction.
     * 
     * @return recurringBilling
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType getRecurringBilling() {
        return recurringBilling;
    }


    /**
     * Sets the recurringBilling value for this PosResponseVer10Transaction.
     * 
     * @param recurringBilling
     */
    public void setRecurringBilling(com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType recurringBilling) {
        this.recurringBilling = recurringBilling;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosResponseVer10Transaction)) return false;
        PosResponseVer10Transaction other = (PosResponseVer10Transaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.prePaidAddValue==null && other.getPrePaidAddValue()==null) || 
             (this.prePaidAddValue!=null &&
              this.prePaidAddValue.equals(other.getPrePaidAddValue()))) &&
            ((this.reportBatchDetail==null && other.getReportBatchDetail()==null) || 
             (this.reportBatchDetail!=null &&
              this.reportBatchDetail.equals(other.getReportBatchDetail()))) &&
            ((this.reportBatchHistory==null && other.getReportBatchHistory()==null) || 
             (this.reportBatchHistory!=null &&
              this.reportBatchHistory.equals(other.getReportBatchHistory()))) &&
            ((this.reportBatchSummary==null && other.getReportBatchSummary()==null) || 
             (this.reportBatchSummary!=null &&
              this.reportBatchSummary.equals(other.getReportBatchSummary()))) &&
            ((this.reportOpenAuths==null && other.getReportOpenAuths()==null) || 
             (this.reportOpenAuths!=null &&
              this.reportOpenAuths.equals(other.getReportOpenAuths()))) &&
            ((this.reportSearch==null && other.getReportSearch()==null) || 
             (this.reportSearch!=null &&
              this.reportSearch.equals(other.getReportSearch()))) &&
            ((this.reportTxnDetail==null && other.getReportTxnDetail()==null) || 
             (this.reportTxnDetail!=null &&
              this.reportTxnDetail.equals(other.getReportTxnDetail()))) &&
            ((this.testCredentials==null && other.getTestCredentials()==null) || 
             (this.testCredentials!=null &&
              this.testCredentials.equals(other.getTestCredentials()))) &&
            ((this.EBTBalanceInquiry==null && other.getEBTBalanceInquiry()==null) || 
             (this.EBTBalanceInquiry!=null &&
              this.EBTBalanceInquiry.equals(other.getEBTBalanceInquiry()))) &&
            ((this.batchClose==null && other.getBatchClose()==null) || 
             (this.batchClose!=null &&
              this.batchClose.equals(other.getBatchClose()))) &&
            ((this.creditAccountVerify==null && other.getCreditAccountVerify()==null) || 
             (this.creditAccountVerify!=null &&
              this.creditAccountVerify.equals(other.getCreditAccountVerify()))) &&
            ((this.creditAddToBatch==null && other.getCreditAddToBatch()==null) || 
             (this.creditAddToBatch!=null &&
              this.creditAddToBatch.equals(other.getCreditAddToBatch()))) &&
            ((this.creditAuth==null && other.getCreditAuth()==null) || 
             (this.creditAuth!=null &&
              this.creditAuth.equals(other.getCreditAuth()))) &&
            ((this.creditCPCEdit==null && other.getCreditCPCEdit()==null) || 
             (this.creditCPCEdit!=null &&
              this.creditCPCEdit.equals(other.getCreditCPCEdit()))) &&
            ((this.creditIncrementalAuth==null && other.getCreditIncrementalAuth()==null) || 
             (this.creditIncrementalAuth!=null &&
              this.creditIncrementalAuth.equals(other.getCreditIncrementalAuth()))) &&
            ((this.creditOfflineAuth==null && other.getCreditOfflineAuth()==null) || 
             (this.creditOfflineAuth!=null &&
              this.creditOfflineAuth.equals(other.getCreditOfflineAuth()))) &&
            ((this.creditOfflineSale==null && other.getCreditOfflineSale()==null) || 
             (this.creditOfflineSale!=null &&
              this.creditOfflineSale.equals(other.getCreditOfflineSale()))) &&
            ((this.creditReturn==null && other.getCreditReturn()==null) || 
             (this.creditReturn!=null &&
              this.creditReturn.equals(other.getCreditReturn()))) &&
            ((this.creditReversal==null && other.getCreditReversal()==null) || 
             (this.creditReversal!=null &&
              this.creditReversal.equals(other.getCreditReversal()))) &&
            ((this.creditSale==null && other.getCreditSale()==null) || 
             (this.creditSale!=null &&
              this.creditSale.equals(other.getCreditSale()))) &&
            ((this.creditTxnEdit==null && other.getCreditTxnEdit()==null) || 
             (this.creditTxnEdit!=null &&
              this.creditTxnEdit.equals(other.getCreditTxnEdit()))) &&
            ((this.creditVoid==null && other.getCreditVoid()==null) || 
             (this.creditVoid!=null &&
              this.creditVoid.equals(other.getCreditVoid()))) &&
            ((this.debitAddValue==null && other.getDebitAddValue()==null) || 
             (this.debitAddValue!=null &&
              this.debitAddValue.equals(other.getDebitAddValue()))) &&
            ((this.debitReturn==null && other.getDebitReturn()==null) || 
             (this.debitReturn!=null &&
              this.debitReturn.equals(other.getDebitReturn()))) &&
            ((this.debitReversal==null && other.getDebitReversal()==null) || 
             (this.debitReversal!=null &&
              this.debitReversal.equals(other.getDebitReversal()))) &&
            ((this.debitSale==null && other.getDebitSale()==null) || 
             (this.debitSale!=null &&
              this.debitSale.equals(other.getDebitSale()))) &&
            ((this.giftCardBalance==null && other.getGiftCardBalance()==null) || 
             (this.giftCardBalance!=null &&
              this.giftCardBalance.equals(other.getGiftCardBalance()))) &&
            ((this.EBTCashBackPurchase==null && other.getEBTCashBackPurchase()==null) || 
             (this.EBTCashBackPurchase!=null &&
              this.EBTCashBackPurchase.equals(other.getEBTCashBackPurchase()))) &&
            ((this.EBTCashBenefitWithdrawal==null && other.getEBTCashBenefitWithdrawal()==null) || 
             (this.EBTCashBenefitWithdrawal!=null &&
              this.EBTCashBenefitWithdrawal.equals(other.getEBTCashBenefitWithdrawal()))) &&
            ((this.EBTFSPurchase==null && other.getEBTFSPurchase()==null) || 
             (this.EBTFSPurchase!=null &&
              this.EBTFSPurchase.equals(other.getEBTFSPurchase()))) &&
            ((this.EBTFSReturn==null && other.getEBTFSReturn()==null) || 
             (this.EBTFSReturn!=null &&
              this.EBTFSReturn.equals(other.getEBTFSReturn()))) &&
            ((this.EBTVoucherPurchase==null && other.getEBTVoucherPurchase()==null) || 
             (this.EBTVoucherPurchase!=null &&
              this.EBTVoucherPurchase.equals(other.getEBTVoucherPurchase()))) &&
            ((this.endToEndTest==null && other.getEndToEndTest()==null) || 
             (this.endToEndTest!=null &&
              this.endToEndTest.equals(other.getEndToEndTest()))) &&
            ((this.giftCardActivate==null && other.getGiftCardActivate()==null) || 
             (this.giftCardActivate!=null &&
              this.giftCardActivate.equals(other.getGiftCardActivate()))) &&
            ((this.giftCardAddValue==null && other.getGiftCardAddValue()==null) || 
             (this.giftCardAddValue!=null &&
              this.giftCardAddValue.equals(other.getGiftCardAddValue()))) &&
            ((this.giftCardSale==null && other.getGiftCardSale()==null) || 
             (this.giftCardSale!=null &&
              this.giftCardSale.equals(other.getGiftCardSale()))) &&
            ((this.reportActivity==null && other.getReportActivity()==null) || 
             (this.reportActivity!=null &&
              this.reportActivity.equals(other.getReportActivity()))) &&
            ((this.giftCardDeactivate==null && other.getGiftCardDeactivate()==null) || 
             (this.giftCardDeactivate!=null &&
              this.giftCardDeactivate.equals(other.getGiftCardDeactivate()))) &&
            ((this.giftCardPreviousDayTotals==null && other.getGiftCardPreviousDayTotals()==null) || 
             (this.giftCardPreviousDayTotals!=null &&
              this.giftCardPreviousDayTotals.equals(other.getGiftCardPreviousDayTotals()))) &&
            ((this.giftCardReplace==null && other.getGiftCardReplace()==null) || 
             (this.giftCardReplace!=null &&
              this.giftCardReplace.equals(other.getGiftCardReplace()))) &&
            ((this.giftCardCurrentDayTotals==null && other.getGiftCardCurrentDayTotals()==null) || 
             (this.giftCardCurrentDayTotals!=null &&
              this.giftCardCurrentDayTotals.equals(other.getGiftCardCurrentDayTotals()))) &&
            ((this.giftCardVoid==null && other.getGiftCardVoid()==null) || 
             (this.giftCardVoid!=null &&
              this.giftCardVoid.equals(other.getGiftCardVoid()))) &&
            ((this.addAttachment==null && other.getAddAttachment()==null) || 
             (this.addAttachment!=null &&
              this.addAttachment.equals(other.getAddAttachment()))) &&
            ((this.prePaidBalanceInquiry==null && other.getPrePaidBalanceInquiry()==null) || 
             (this.prePaidBalanceInquiry!=null &&
              this.prePaidBalanceInquiry.equals(other.getPrePaidBalanceInquiry()))) &&
            ((this.recurringBilling==null && other.getRecurringBilling()==null) || 
             (this.recurringBilling!=null &&
              this.recurringBilling.equals(other.getRecurringBilling())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPrePaidAddValue() != null) {
            _hashCode += getPrePaidAddValue().hashCode();
        }
        if (getReportBatchDetail() != null) {
            _hashCode += getReportBatchDetail().hashCode();
        }
        if (getReportBatchHistory() != null) {
            _hashCode += getReportBatchHistory().hashCode();
        }
        if (getReportBatchSummary() != null) {
            _hashCode += getReportBatchSummary().hashCode();
        }
        if (getReportOpenAuths() != null) {
            _hashCode += getReportOpenAuths().hashCode();
        }
        if (getReportSearch() != null) {
            _hashCode += getReportSearch().hashCode();
        }
        if (getReportTxnDetail() != null) {
            _hashCode += getReportTxnDetail().hashCode();
        }
        if (getTestCredentials() != null) {
            _hashCode += getTestCredentials().hashCode();
        }
        if (getEBTBalanceInquiry() != null) {
            _hashCode += getEBTBalanceInquiry().hashCode();
        }
        if (getBatchClose() != null) {
            _hashCode += getBatchClose().hashCode();
        }
        if (getCreditAccountVerify() != null) {
            _hashCode += getCreditAccountVerify().hashCode();
        }
        if (getCreditAddToBatch() != null) {
            _hashCode += getCreditAddToBatch().hashCode();
        }
        if (getCreditAuth() != null) {
            _hashCode += getCreditAuth().hashCode();
        }
        if (getCreditCPCEdit() != null) {
            _hashCode += getCreditCPCEdit().hashCode();
        }
        if (getCreditIncrementalAuth() != null) {
            _hashCode += getCreditIncrementalAuth().hashCode();
        }
        if (getCreditOfflineAuth() != null) {
            _hashCode += getCreditOfflineAuth().hashCode();
        }
        if (getCreditOfflineSale() != null) {
            _hashCode += getCreditOfflineSale().hashCode();
        }
        if (getCreditReturn() != null) {
            _hashCode += getCreditReturn().hashCode();
        }
        if (getCreditReversal() != null) {
            _hashCode += getCreditReversal().hashCode();
        }
        if (getCreditSale() != null) {
            _hashCode += getCreditSale().hashCode();
        }
        if (getCreditTxnEdit() != null) {
            _hashCode += getCreditTxnEdit().hashCode();
        }
        if (getCreditVoid() != null) {
            _hashCode += getCreditVoid().hashCode();
        }
        if (getDebitAddValue() != null) {
            _hashCode += getDebitAddValue().hashCode();
        }
        if (getDebitReturn() != null) {
            _hashCode += getDebitReturn().hashCode();
        }
        if (getDebitReversal() != null) {
            _hashCode += getDebitReversal().hashCode();
        }
        if (getDebitSale() != null) {
            _hashCode += getDebitSale().hashCode();
        }
        if (getGiftCardBalance() != null) {
            _hashCode += getGiftCardBalance().hashCode();
        }
        if (getEBTCashBackPurchase() != null) {
            _hashCode += getEBTCashBackPurchase().hashCode();
        }
        if (getEBTCashBenefitWithdrawal() != null) {
            _hashCode += getEBTCashBenefitWithdrawal().hashCode();
        }
        if (getEBTFSPurchase() != null) {
            _hashCode += getEBTFSPurchase().hashCode();
        }
        if (getEBTFSReturn() != null) {
            _hashCode += getEBTFSReturn().hashCode();
        }
        if (getEBTVoucherPurchase() != null) {
            _hashCode += getEBTVoucherPurchase().hashCode();
        }
        if (getEndToEndTest() != null) {
            _hashCode += getEndToEndTest().hashCode();
        }
        if (getGiftCardActivate() != null) {
            _hashCode += getGiftCardActivate().hashCode();
        }
        if (getGiftCardAddValue() != null) {
            _hashCode += getGiftCardAddValue().hashCode();
        }
        if (getGiftCardSale() != null) {
            _hashCode += getGiftCardSale().hashCode();
        }
        if (getReportActivity() != null) {
            _hashCode += getReportActivity().hashCode();
        }
        if (getGiftCardDeactivate() != null) {
            _hashCode += getGiftCardDeactivate().hashCode();
        }
        if (getGiftCardPreviousDayTotals() != null) {
            _hashCode += getGiftCardPreviousDayTotals().hashCode();
        }
        if (getGiftCardReplace() != null) {
            _hashCode += getGiftCardReplace().hashCode();
        }
        if (getGiftCardCurrentDayTotals() != null) {
            _hashCode += getGiftCardCurrentDayTotals().hashCode();
        }
        if (getGiftCardVoid() != null) {
            _hashCode += getGiftCardVoid().hashCode();
        }
        if (getAddAttachment() != null) {
            _hashCode += getAddAttachment().hashCode();
        }
        if (getPrePaidBalanceInquiry() != null) {
            _hashCode += getPrePaidBalanceInquiry().hashCode();
        }
        if (getRecurringBilling() != null) {
            _hashCode += getRecurringBilling().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosResponseVer10Transaction.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosResponse>Ver1.0>Transaction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prePaidAddValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PrePaidAddValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportBatchDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportBatchDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchDetailRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportBatchHistory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportBatchHistory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchHistoryRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportBatchSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportBatchSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchSummaryRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportOpenAuths");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportOpenAuths"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportOpenAuthsRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportSearch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportSearch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportSearchRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportTxnDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportTxnDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportTxnDetailRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("testCredentials");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TestCredentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTBalanceInquiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTBalanceInquiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchClose");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchClose"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosBatchCloseRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditAccountVerify");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAccountVerify"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditAddToBatch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAddToBatch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCPCEdit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditCPCEdit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditIncrementalAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditIncrementalAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditOfflineAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditOfflineAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditOfflineSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditOfflineSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditReversal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditReversal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditTxnEdit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditTxnEdit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditVoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditVoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitAddValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitAddValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitReversal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitReversal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardBalanceRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTCashBackPurchase");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTCashBackPurchase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTCashBenefitWithdrawal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTCashBenefitWithdrawal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTFSPurchase");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTFSPurchase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTFSReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTFSReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EBTVoucherPurchase");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTVoucherPurchase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endToEndTest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EndToEndTest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardActivate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardActivate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardActivateRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardAddValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardAddValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardAddValueRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardSale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardSaleRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reportActivity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportActivity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportActivityRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardDeactivate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardDeactivate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardDeactivateRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardPreviousDayTotals");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardPreviousDayTotals"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardTotalsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardReplace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardReplace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardReplaceRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardCurrentDayTotals");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardCurrentDayTotals"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardTotalsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardVoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardVoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardVoidRspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addAttachment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AddAttachment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prePaidBalanceInquiry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PrePaidBalanceInquiry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recurringBilling");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RecurringBilling"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
