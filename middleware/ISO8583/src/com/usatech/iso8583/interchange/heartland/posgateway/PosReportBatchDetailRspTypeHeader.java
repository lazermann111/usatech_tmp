/**
 * PosReportBatchDetailRspTypeHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportBatchDetailRspTypeHeader  implements java.io.Serializable {
    private int siteId;

    private java.lang.String merchName;

    private int deviceId;

    private int batchId;

    private java.lang.String batchStatus;

    private int batchSeqNbr;

    private java.util.Calendar openUtcDT;

    private java.util.Calendar closeUtcDT;

    private int openTxnId;

    private java.lang.Integer closeTxnId;

    private int batchTxnCnt;

    private java.math.BigDecimal batchTxnAmt;

    public PosReportBatchDetailRspTypeHeader() {
    }

    public PosReportBatchDetailRspTypeHeader(
           int siteId,
           java.lang.String merchName,
           int deviceId,
           int batchId,
           java.lang.String batchStatus,
           int batchSeqNbr,
           java.util.Calendar openUtcDT,
           java.util.Calendar closeUtcDT,
           int openTxnId,
           java.lang.Integer closeTxnId,
           int batchTxnCnt,
           java.math.BigDecimal batchTxnAmt) {
           this.siteId = siteId;
           this.merchName = merchName;
           this.deviceId = deviceId;
           this.batchId = batchId;
           this.batchStatus = batchStatus;
           this.batchSeqNbr = batchSeqNbr;
           this.openUtcDT = openUtcDT;
           this.closeUtcDT = closeUtcDT;
           this.openTxnId = openTxnId;
           this.closeTxnId = closeTxnId;
           this.batchTxnCnt = batchTxnCnt;
           this.batchTxnAmt = batchTxnAmt;
    }


    /**
     * Gets the siteId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return siteId
     */
    public int getSiteId() {
        return siteId;
    }


    /**
     * Sets the siteId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param siteId
     */
    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }


    /**
     * Gets the merchName value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return merchName
     */
    public java.lang.String getMerchName() {
        return merchName;
    }


    /**
     * Sets the merchName value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param merchName
     */
    public void setMerchName(java.lang.String merchName) {
        this.merchName = merchName;
    }


    /**
     * Gets the deviceId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param deviceId
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the batchId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return batchId
     */
    public int getBatchId() {
        return batchId;
    }


    /**
     * Sets the batchId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param batchId
     */
    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }


    /**
     * Gets the batchStatus value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return batchStatus
     */
    public java.lang.String getBatchStatus() {
        return batchStatus;
    }


    /**
     * Sets the batchStatus value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param batchStatus
     */
    public void setBatchStatus(java.lang.String batchStatus) {
        this.batchStatus = batchStatus;
    }


    /**
     * Gets the batchSeqNbr value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return batchSeqNbr
     */
    public int getBatchSeqNbr() {
        return batchSeqNbr;
    }


    /**
     * Sets the batchSeqNbr value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param batchSeqNbr
     */
    public void setBatchSeqNbr(int batchSeqNbr) {
        this.batchSeqNbr = batchSeqNbr;
    }


    /**
     * Gets the openUtcDT value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return openUtcDT
     */
    public java.util.Calendar getOpenUtcDT() {
        return openUtcDT;
    }


    /**
     * Sets the openUtcDT value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param openUtcDT
     */
    public void setOpenUtcDT(java.util.Calendar openUtcDT) {
        this.openUtcDT = openUtcDT;
    }


    /**
     * Gets the closeUtcDT value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return closeUtcDT
     */
    public java.util.Calendar getCloseUtcDT() {
        return closeUtcDT;
    }


    /**
     * Sets the closeUtcDT value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param closeUtcDT
     */
    public void setCloseUtcDT(java.util.Calendar closeUtcDT) {
        this.closeUtcDT = closeUtcDT;
    }


    /**
     * Gets the openTxnId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return openTxnId
     */
    public int getOpenTxnId() {
        return openTxnId;
    }


    /**
     * Sets the openTxnId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param openTxnId
     */
    public void setOpenTxnId(int openTxnId) {
        this.openTxnId = openTxnId;
    }


    /**
     * Gets the closeTxnId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return closeTxnId
     */
    public java.lang.Integer getCloseTxnId() {
        return closeTxnId;
    }


    /**
     * Sets the closeTxnId value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param closeTxnId
     */
    public void setCloseTxnId(java.lang.Integer closeTxnId) {
        this.closeTxnId = closeTxnId;
    }


    /**
     * Gets the batchTxnCnt value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return batchTxnCnt
     */
    public int getBatchTxnCnt() {
        return batchTxnCnt;
    }


    /**
     * Sets the batchTxnCnt value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param batchTxnCnt
     */
    public void setBatchTxnCnt(int batchTxnCnt) {
        this.batchTxnCnt = batchTxnCnt;
    }


    /**
     * Gets the batchTxnAmt value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @return batchTxnAmt
     */
    public java.math.BigDecimal getBatchTxnAmt() {
        return batchTxnAmt;
    }


    /**
     * Sets the batchTxnAmt value for this PosReportBatchDetailRspTypeHeader.
     * 
     * @param batchTxnAmt
     */
    public void setBatchTxnAmt(java.math.BigDecimal batchTxnAmt) {
        this.batchTxnAmt = batchTxnAmt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportBatchDetailRspTypeHeader)) return false;
        PosReportBatchDetailRspTypeHeader other = (PosReportBatchDetailRspTypeHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.siteId == other.getSiteId() &&
            ((this.merchName==null && other.getMerchName()==null) || 
             (this.merchName!=null &&
              this.merchName.equals(other.getMerchName()))) &&
            this.deviceId == other.getDeviceId() &&
            this.batchId == other.getBatchId() &&
            ((this.batchStatus==null && other.getBatchStatus()==null) || 
             (this.batchStatus!=null &&
              this.batchStatus.equals(other.getBatchStatus()))) &&
            this.batchSeqNbr == other.getBatchSeqNbr() &&
            ((this.openUtcDT==null && other.getOpenUtcDT()==null) || 
             (this.openUtcDT!=null &&
              this.openUtcDT.equals(other.getOpenUtcDT()))) &&
            ((this.closeUtcDT==null && other.getCloseUtcDT()==null) || 
             (this.closeUtcDT!=null &&
              this.closeUtcDT.equals(other.getCloseUtcDT()))) &&
            this.openTxnId == other.getOpenTxnId() &&
            ((this.closeTxnId==null && other.getCloseTxnId()==null) || 
             (this.closeTxnId!=null &&
              this.closeTxnId.equals(other.getCloseTxnId()))) &&
            this.batchTxnCnt == other.getBatchTxnCnt() &&
            ((this.batchTxnAmt==null && other.getBatchTxnAmt()==null) || 
             (this.batchTxnAmt!=null &&
              this.batchTxnAmt.equals(other.getBatchTxnAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSiteId();
        if (getMerchName() != null) {
            _hashCode += getMerchName().hashCode();
        }
        _hashCode += getDeviceId();
        _hashCode += getBatchId();
        if (getBatchStatus() != null) {
            _hashCode += getBatchStatus().hashCode();
        }
        _hashCode += getBatchSeqNbr();
        if (getOpenUtcDT() != null) {
            _hashCode += getOpenUtcDT().hashCode();
        }
        if (getCloseUtcDT() != null) {
            _hashCode += getCloseUtcDT().hashCode();
        }
        _hashCode += getOpenTxnId();
        if (getCloseTxnId() != null) {
            _hashCode += getCloseTxnId().hashCode();
        }
        _hashCode += getBatchTxnCnt();
        if (getBatchTxnAmt() != null) {
            _hashCode += getBatchTxnAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportBatchDetailRspTypeHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchDetailRspType>Header"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchSeqNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchSeqNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "OpenUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closeUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CloseUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "OpenTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closeTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CloseTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchTxnCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchTxnCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchTxnAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchTxnAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
