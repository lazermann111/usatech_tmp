/**
 * PosGatewayService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public interface PosGatewayService extends javax.xml.rpc.Service {
    public java.lang.String getPosGatewayInterfaceAddress();

    public com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType getPosGatewayInterface() throws javax.xml.rpc.ServiceException;

    public com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType getPosGatewayInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getPosGatewayInterface1Address();

    public com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType getPosGatewayInterface1() throws javax.xml.rpc.ServiceException;

    public com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType getPosGatewayInterface1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
