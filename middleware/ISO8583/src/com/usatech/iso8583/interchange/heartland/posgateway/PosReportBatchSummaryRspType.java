/**
 * PosReportBatchSummaryRspType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportBatchSummaryRspType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeHeader header;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeDetails[] details;

    public PosReportBatchSummaryRspType() {
    }

    public PosReportBatchSummaryRspType(
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeHeader header,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeDetails[] details) {
           this.header = header;
           this.details = details;
    }


    /**
     * Gets the header value for this PosReportBatchSummaryRspType.
     * 
     * @return header
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeHeader getHeader() {
        return header;
    }


    /**
     * Sets the header value for this PosReportBatchSummaryRspType.
     * 
     * @param header
     */
    public void setHeader(com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeHeader header) {
        this.header = header;
    }


    /**
     * Gets the details value for this PosReportBatchSummaryRspType.
     * 
     * @return details
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeDetails[] getDetails() {
        return details;
    }


    /**
     * Sets the details value for this PosReportBatchSummaryRspType.
     * 
     * @param details
     */
    public void setDetails(com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeDetails[] details) {
        this.details = details;
    }

    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeDetails getDetails(int i) {
        return this.details[i];
    }

    public void setDetails(int i, com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeDetails _value) {
        this.details[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportBatchSummaryRspType)) return false;
        PosReportBatchSummaryRspType other = (PosReportBatchSummaryRspType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.header==null && other.getHeader()==null) || 
             (this.header!=null &&
              this.header.equals(other.getHeader()))) &&
            ((this.details==null && other.getDetails()==null) || 
             (this.details!=null &&
              java.util.Arrays.equals(this.details, other.getDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHeader() != null) {
            _hashCode += getHeader().hashCode();
        }
        if (getDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportBatchSummaryRspType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchSummaryRspType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Header"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchSummaryRspType>Header"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("details");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Details"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchSummaryRspType>Details"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
