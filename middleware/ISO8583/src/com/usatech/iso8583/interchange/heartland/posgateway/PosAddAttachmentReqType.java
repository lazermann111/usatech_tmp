/**
 * PosAddAttachmentReqType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosAddAttachmentReqType  implements java.io.Serializable {
    private int gatewayTxnId;

    private com.usatech.iso8583.interchange.heartland.posgateway.AttachmentDataType[] block1;

    public PosAddAttachmentReqType() {
    }

    public PosAddAttachmentReqType(
           int gatewayTxnId,
           com.usatech.iso8583.interchange.heartland.posgateway.AttachmentDataType[] block1) {
           this.gatewayTxnId = gatewayTxnId;
           this.block1 = block1;
    }


    /**
     * Gets the gatewayTxnId value for this PosAddAttachmentReqType.
     * 
     * @return gatewayTxnId
     */
    public int getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this PosAddAttachmentReqType.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(int gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the block1 value for this PosAddAttachmentReqType.
     * 
     * @return block1
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AttachmentDataType[] getBlock1() {
        return block1;
    }


    /**
     * Sets the block1 value for this PosAddAttachmentReqType.
     * 
     * @param block1
     */
    public void setBlock1(com.usatech.iso8583.interchange.heartland.posgateway.AttachmentDataType[] block1) {
        this.block1 = block1;
    }

    public com.usatech.iso8583.interchange.heartland.posgateway.AttachmentDataType getBlock1(int i) {
        return this.block1[i];
    }

    public void setBlock1(int i, com.usatech.iso8583.interchange.heartland.posgateway.AttachmentDataType _value) {
        this.block1[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosAddAttachmentReqType)) return false;
        PosAddAttachmentReqType other = (PosAddAttachmentReqType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.gatewayTxnId == other.getGatewayTxnId() &&
            ((this.block1==null && other.getBlock1()==null) || 
             (this.block1!=null &&
              java.util.Arrays.equals(this.block1, other.getBlock1())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGatewayTxnId();
        if (getBlock1() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBlock1());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBlock1(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosAddAttachmentReqType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosAddAttachmentReqType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("block1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Block1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
