/**
 * AttachmentDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class AttachmentDataType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.AttachmentTypeType attachmentType;

    private java.lang.String attachmentData;

    private com.usatech.iso8583.interchange.heartland.posgateway.AttachmentFormatType attachmentFormat;

    public AttachmentDataType() {
    }

    public AttachmentDataType(
           com.usatech.iso8583.interchange.heartland.posgateway.AttachmentTypeType attachmentType,
           java.lang.String attachmentData,
           com.usatech.iso8583.interchange.heartland.posgateway.AttachmentFormatType attachmentFormat) {
           this.attachmentType = attachmentType;
           this.attachmentData = attachmentData;
           this.attachmentFormat = attachmentFormat;
    }


    /**
     * Gets the attachmentType value for this AttachmentDataType.
     * 
     * @return attachmentType
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AttachmentTypeType getAttachmentType() {
        return attachmentType;
    }


    /**
     * Sets the attachmentType value for this AttachmentDataType.
     * 
     * @param attachmentType
     */
    public void setAttachmentType(com.usatech.iso8583.interchange.heartland.posgateway.AttachmentTypeType attachmentType) {
        this.attachmentType = attachmentType;
    }


    /**
     * Gets the attachmentData value for this AttachmentDataType.
     * 
     * @return attachmentData
     */
    public java.lang.String getAttachmentData() {
        return attachmentData;
    }


    /**
     * Sets the attachmentData value for this AttachmentDataType.
     * 
     * @param attachmentData
     */
    public void setAttachmentData(java.lang.String attachmentData) {
        this.attachmentData = attachmentData;
    }


    /**
     * Gets the attachmentFormat value for this AttachmentDataType.
     * 
     * @return attachmentFormat
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AttachmentFormatType getAttachmentFormat() {
        return attachmentFormat;
    }


    /**
     * Sets the attachmentFormat value for this AttachmentDataType.
     * 
     * @param attachmentFormat
     */
    public void setAttachmentFormat(com.usatech.iso8583.interchange.heartland.posgateway.AttachmentFormatType attachmentFormat) {
        this.attachmentFormat = attachmentFormat;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AttachmentDataType)) return false;
        AttachmentDataType other = (AttachmentDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.attachmentType==null && other.getAttachmentType()==null) || 
             (this.attachmentType!=null &&
              this.attachmentType.equals(other.getAttachmentType()))) &&
            ((this.attachmentData==null && other.getAttachmentData()==null) || 
             (this.attachmentData!=null &&
              this.attachmentData.equals(other.getAttachmentData()))) &&
            ((this.attachmentFormat==null && other.getAttachmentFormat()==null) || 
             (this.attachmentFormat!=null &&
              this.attachmentFormat.equals(other.getAttachmentFormat())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAttachmentType() != null) {
            _hashCode += getAttachmentType().hashCode();
        }
        if (getAttachmentData() != null) {
            _hashCode += getAttachmentData().hashCode();
        }
        if (getAttachmentFormat() != null) {
            _hashCode += getAttachmentFormat().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AttachmentDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "attachmentTypeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "attachmentFormatType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
