/**
 * CreditAccountVerifyBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class CreditAccountVerifyBlock1Type  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData;

    private com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData;

    public CreditAccountVerifyBlock1Type() {
    }

    public CreditAccountVerifyBlock1Type(
           com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData,
           com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData) {
           this.cardData = cardData;
           this.cardHolderData = cardHolderData;
    }


    /**
     * Gets the cardData value for this CreditAccountVerifyBlock1Type.
     * 
     * @return cardData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardDataType getCardData() {
        return cardData;
    }


    /**
     * Sets the cardData value for this CreditAccountVerifyBlock1Type.
     * 
     * @param cardData
     */
    public void setCardData(com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData) {
        this.cardData = cardData;
    }


    /**
     * Gets the cardHolderData value for this CreditAccountVerifyBlock1Type.
     * 
     * @return cardHolderData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType getCardHolderData() {
        return cardHolderData;
    }


    /**
     * Sets the cardHolderData value for this CreditAccountVerifyBlock1Type.
     * 
     * @param cardHolderData
     */
    public void setCardHolderData(com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData) {
        this.cardHolderData = cardHolderData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreditAccountVerifyBlock1Type)) return false;
        CreditAccountVerifyBlock1Type other = (CreditAccountVerifyBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardData==null && other.getCardData()==null) || 
             (this.cardData!=null &&
              this.cardData.equals(other.getCardData()))) &&
            ((this.cardHolderData==null && other.getCardHolderData()==null) || 
             (this.cardHolderData!=null &&
              this.cardHolderData.equals(other.getCardHolderData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardData() != null) {
            _hashCode += getCardData().hashCode();
        }
        if (getCardHolderData() != null) {
            _hashCode += getCardHolderData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditAccountVerifyBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAccountVerifyBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
