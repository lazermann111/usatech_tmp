/**
 * PosRequestVer10.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosRequestVer10  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Header header;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction transaction;

    public PosRequestVer10() {
    }

    public PosRequestVer10(
           com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Header header,
           com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction transaction) {
           this.header = header;
           this.transaction = transaction;
    }


    /**
     * Gets the header value for this PosRequestVer10.
     * 
     * @return header
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Header getHeader() {
        return header;
    }


    /**
     * Sets the header value for this PosRequestVer10.
     * 
     * @param header
     */
    public void setHeader(com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Header header) {
        this.header = header;
    }


    /**
     * Gets the transaction value for this PosRequestVer10.
     * 
     * @return transaction
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction getTransaction() {
        return transaction;
    }


    /**
     * Sets the transaction value for this PosRequestVer10.
     * 
     * @param transaction
     */
    public void setTransaction(com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction transaction) {
        this.transaction = transaction;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosRequestVer10)) return false;
        PosRequestVer10 other = (PosRequestVer10) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.header==null && other.getHeader()==null) || 
             (this.header!=null &&
              this.header.equals(other.getHeader()))) &&
            ((this.transaction==null && other.getTransaction()==null) || 
             (this.transaction!=null &&
              this.transaction.equals(other.getTransaction())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHeader() != null) {
            _hashCode += getHeader().hashCode();
        }
        if (getTransaction() != null) {
            _hashCode += getTransaction().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosRequestVer10.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>PosRequest>Ver1.0"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Header"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosRequest>Ver1.0>Header"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transaction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Transaction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosRequest>Ver1.0>Transaction"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
