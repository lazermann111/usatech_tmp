/**
 * PrestigiousPropertyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PrestigiousPropertyType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PrestigiousPropertyType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _NOT_PARTICIPATING = "NOT_PARTICIPATING";
    public static final java.lang.String _LIMIT_500 = "LIMIT_500";
    public static final java.lang.String _LIMIT_1000 = "LIMIT_1000";
    public static final java.lang.String _LIMIT_1500 = "LIMIT_1500";
    public static final PrestigiousPropertyType NOT_PARTICIPATING = new PrestigiousPropertyType(_NOT_PARTICIPATING);
    public static final PrestigiousPropertyType LIMIT_500 = new PrestigiousPropertyType(_LIMIT_500);
    public static final PrestigiousPropertyType LIMIT_1000 = new PrestigiousPropertyType(_LIMIT_1000);
    public static final PrestigiousPropertyType LIMIT_1500 = new PrestigiousPropertyType(_LIMIT_1500);
    public java.lang.String getValue() { return _value_;}
    public static PrestigiousPropertyType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PrestigiousPropertyType enumeration = (PrestigiousPropertyType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PrestigiousPropertyType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrestigiousPropertyType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "prestigiousPropertyType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
