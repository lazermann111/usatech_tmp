/**
 * PosResponseVer10Header.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosResponseVer10Header  implements java.io.Serializable {
    private java.lang.Integer licenseId;

    private java.lang.Integer siteId;

    private java.lang.Integer deviceId;

    private java.lang.String siteTrace;

    private int gatewayTxnId;

    private int gatewayRspCode;

    private java.lang.String gatewayRspMsg;

    private java.util.Calendar rspDT;

    public PosResponseVer10Header() {
    }

    public PosResponseVer10Header(
           java.lang.Integer licenseId,
           java.lang.Integer siteId,
           java.lang.Integer deviceId,
           java.lang.String siteTrace,
           int gatewayTxnId,
           int gatewayRspCode,
           java.lang.String gatewayRspMsg,
           java.util.Calendar rspDT) {
           this.licenseId = licenseId;
           this.siteId = siteId;
           this.deviceId = deviceId;
           this.siteTrace = siteTrace;
           this.gatewayTxnId = gatewayTxnId;
           this.gatewayRspCode = gatewayRspCode;
           this.gatewayRspMsg = gatewayRspMsg;
           this.rspDT = rspDT;
    }


    /**
     * Gets the licenseId value for this PosResponseVer10Header.
     * 
     * @return licenseId
     */
    public java.lang.Integer getLicenseId() {
        return licenseId;
    }


    /**
     * Sets the licenseId value for this PosResponseVer10Header.
     * 
     * @param licenseId
     */
    public void setLicenseId(java.lang.Integer licenseId) {
        this.licenseId = licenseId;
    }


    /**
     * Gets the siteId value for this PosResponseVer10Header.
     * 
     * @return siteId
     */
    public java.lang.Integer getSiteId() {
        return siteId;
    }


    /**
     * Sets the siteId value for this PosResponseVer10Header.
     * 
     * @param siteId
     */
    public void setSiteId(java.lang.Integer siteId) {
        this.siteId = siteId;
    }


    /**
     * Gets the deviceId value for this PosResponseVer10Header.
     * 
     * @return deviceId
     */
    public java.lang.Integer getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosResponseVer10Header.
     * 
     * @param deviceId
     */
    public void setDeviceId(java.lang.Integer deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the siteTrace value for this PosResponseVer10Header.
     * 
     * @return siteTrace
     */
    public java.lang.String getSiteTrace() {
        return siteTrace;
    }


    /**
     * Sets the siteTrace value for this PosResponseVer10Header.
     * 
     * @param siteTrace
     */
    public void setSiteTrace(java.lang.String siteTrace) {
        this.siteTrace = siteTrace;
    }


    /**
     * Gets the gatewayTxnId value for this PosResponseVer10Header.
     * 
     * @return gatewayTxnId
     */
    public int getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this PosResponseVer10Header.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(int gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the gatewayRspCode value for this PosResponseVer10Header.
     * 
     * @return gatewayRspCode
     */
    public int getGatewayRspCode() {
        return gatewayRspCode;
    }


    /**
     * Sets the gatewayRspCode value for this PosResponseVer10Header.
     * 
     * @param gatewayRspCode
     */
    public void setGatewayRspCode(int gatewayRspCode) {
        this.gatewayRspCode = gatewayRspCode;
    }


    /**
     * Gets the gatewayRspMsg value for this PosResponseVer10Header.
     * 
     * @return gatewayRspMsg
     */
    public java.lang.String getGatewayRspMsg() {
        return gatewayRspMsg;
    }


    /**
     * Sets the gatewayRspMsg value for this PosResponseVer10Header.
     * 
     * @param gatewayRspMsg
     */
    public void setGatewayRspMsg(java.lang.String gatewayRspMsg) {
        this.gatewayRspMsg = gatewayRspMsg;
    }


    /**
     * Gets the rspDT value for this PosResponseVer10Header.
     * 
     * @return rspDT
     */
    public java.util.Calendar getRspDT() {
        return rspDT;
    }


    /**
     * Sets the rspDT value for this PosResponseVer10Header.
     * 
     * @param rspDT
     */
    public void setRspDT(java.util.Calendar rspDT) {
        this.rspDT = rspDT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosResponseVer10Header)) return false;
        PosResponseVer10Header other = (PosResponseVer10Header) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.licenseId==null && other.getLicenseId()==null) || 
             (this.licenseId!=null &&
              this.licenseId.equals(other.getLicenseId()))) &&
            ((this.siteId==null && other.getSiteId()==null) || 
             (this.siteId!=null &&
              this.siteId.equals(other.getSiteId()))) &&
            ((this.deviceId==null && other.getDeviceId()==null) || 
             (this.deviceId!=null &&
              this.deviceId.equals(other.getDeviceId()))) &&
            ((this.siteTrace==null && other.getSiteTrace()==null) || 
             (this.siteTrace!=null &&
              this.siteTrace.equals(other.getSiteTrace()))) &&
            this.gatewayTxnId == other.getGatewayTxnId() &&
            this.gatewayRspCode == other.getGatewayRspCode() &&
            ((this.gatewayRspMsg==null && other.getGatewayRspMsg()==null) || 
             (this.gatewayRspMsg!=null &&
              this.gatewayRspMsg.equals(other.getGatewayRspMsg()))) &&
            ((this.rspDT==null && other.getRspDT()==null) || 
             (this.rspDT!=null &&
              this.rspDT.equals(other.getRspDT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLicenseId() != null) {
            _hashCode += getLicenseId().hashCode();
        }
        if (getSiteId() != null) {
            _hashCode += getSiteId().hashCode();
        }
        if (getDeviceId() != null) {
            _hashCode += getDeviceId().hashCode();
        }
        if (getSiteTrace() != null) {
            _hashCode += getSiteTrace().hashCode();
        }
        _hashCode += getGatewayTxnId();
        _hashCode += getGatewayRspCode();
        if (getGatewayRspMsg() != null) {
            _hashCode += getGatewayRspMsg().hashCode();
        }
        if (getRspDT() != null) {
            _hashCode += getRspDT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosResponseVer10Header.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosResponse>Ver1.0>Header"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LicenseId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteTrace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteTrace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayRspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayRspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayRspMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayRspMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
