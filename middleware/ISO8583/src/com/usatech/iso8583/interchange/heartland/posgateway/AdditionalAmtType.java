/**
 * AdditionalAmtType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class AdditionalAmtType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.AmtTypeType amtType;

    private java.math.BigDecimal amt;

    public AdditionalAmtType() {
    }

    public AdditionalAmtType(
           com.usatech.iso8583.interchange.heartland.posgateway.AmtTypeType amtType,
           java.math.BigDecimal amt) {
           this.amtType = amtType;
           this.amt = amt;
    }


    /**
     * Gets the amtType value for this AdditionalAmtType.
     * 
     * @return amtType
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AmtTypeType getAmtType() {
        return amtType;
    }


    /**
     * Sets the amtType value for this AdditionalAmtType.
     * 
     * @param amtType
     */
    public void setAmtType(com.usatech.iso8583.interchange.heartland.posgateway.AmtTypeType amtType) {
        this.amtType = amtType;
    }


    /**
     * Gets the amt value for this AdditionalAmtType.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this AdditionalAmtType.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdditionalAmtType)) return false;
        AdditionalAmtType other = (AdditionalAmtType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.amtType==null && other.getAmtType()==null) || 
             (this.amtType!=null &&
              this.amtType.equals(other.getAmtType()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAmtType() != null) {
            _hashCode += getAmtType().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdditionalAmtType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalAmtType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amtType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AmtType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "amtTypeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
