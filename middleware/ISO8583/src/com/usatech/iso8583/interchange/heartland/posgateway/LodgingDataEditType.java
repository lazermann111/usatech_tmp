/**
 * LodgingDataEditType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class LodgingDataEditType  implements java.io.Serializable {
    private java.lang.String folioNumber;

    private java.lang.Integer duration;

    private java.util.Date checkInDate;

    private java.util.Date checkOutDate;

    private java.math.BigDecimal rate;

    private com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType extraCharges;

    private java.math.BigDecimal extraChargeAmtInfo;

    public LodgingDataEditType() {
    }

    public LodgingDataEditType(
           java.lang.String folioNumber,
           java.lang.Integer duration,
           java.util.Date checkInDate,
           java.util.Date checkOutDate,
           java.math.BigDecimal rate,
           com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType extraCharges,
           java.math.BigDecimal extraChargeAmtInfo) {
           this.folioNumber = folioNumber;
           this.duration = duration;
           this.checkInDate = checkInDate;
           this.checkOutDate = checkOutDate;
           this.rate = rate;
           this.extraCharges = extraCharges;
           this.extraChargeAmtInfo = extraChargeAmtInfo;
    }


    /**
     * Gets the folioNumber value for this LodgingDataEditType.
     * 
     * @return folioNumber
     */
    public java.lang.String getFolioNumber() {
        return folioNumber;
    }


    /**
     * Sets the folioNumber value for this LodgingDataEditType.
     * 
     * @param folioNumber
     */
    public void setFolioNumber(java.lang.String folioNumber) {
        this.folioNumber = folioNumber;
    }


    /**
     * Gets the duration value for this LodgingDataEditType.
     * 
     * @return duration
     */
    public java.lang.Integer getDuration() {
        return duration;
    }


    /**
     * Sets the duration value for this LodgingDataEditType.
     * 
     * @param duration
     */
    public void setDuration(java.lang.Integer duration) {
        this.duration = duration;
    }


    /**
     * Gets the checkInDate value for this LodgingDataEditType.
     * 
     * @return checkInDate
     */
    public java.util.Date getCheckInDate() {
        return checkInDate;
    }


    /**
     * Sets the checkInDate value for this LodgingDataEditType.
     * 
     * @param checkInDate
     */
    public void setCheckInDate(java.util.Date checkInDate) {
        this.checkInDate = checkInDate;
    }


    /**
     * Gets the checkOutDate value for this LodgingDataEditType.
     * 
     * @return checkOutDate
     */
    public java.util.Date getCheckOutDate() {
        return checkOutDate;
    }


    /**
     * Sets the checkOutDate value for this LodgingDataEditType.
     * 
     * @param checkOutDate
     */
    public void setCheckOutDate(java.util.Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }


    /**
     * Gets the rate value for this LodgingDataEditType.
     * 
     * @return rate
     */
    public java.math.BigDecimal getRate() {
        return rate;
    }


    /**
     * Sets the rate value for this LodgingDataEditType.
     * 
     * @param rate
     */
    public void setRate(java.math.BigDecimal rate) {
        this.rate = rate;
    }


    /**
     * Gets the extraCharges value for this LodgingDataEditType.
     * 
     * @return extraCharges
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType getExtraCharges() {
        return extraCharges;
    }


    /**
     * Sets the extraCharges value for this LodgingDataEditType.
     * 
     * @param extraCharges
     */
    public void setExtraCharges(com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType extraCharges) {
        this.extraCharges = extraCharges;
    }


    /**
     * Gets the extraChargeAmtInfo value for this LodgingDataEditType.
     * 
     * @return extraChargeAmtInfo
     */
    public java.math.BigDecimal getExtraChargeAmtInfo() {
        return extraChargeAmtInfo;
    }


    /**
     * Sets the extraChargeAmtInfo value for this LodgingDataEditType.
     * 
     * @param extraChargeAmtInfo
     */
    public void setExtraChargeAmtInfo(java.math.BigDecimal extraChargeAmtInfo) {
        this.extraChargeAmtInfo = extraChargeAmtInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LodgingDataEditType)) return false;
        LodgingDataEditType other = (LodgingDataEditType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.folioNumber==null && other.getFolioNumber()==null) || 
             (this.folioNumber!=null &&
              this.folioNumber.equals(other.getFolioNumber()))) &&
            ((this.duration==null && other.getDuration()==null) || 
             (this.duration!=null &&
              this.duration.equals(other.getDuration()))) &&
            ((this.checkInDate==null && other.getCheckInDate()==null) || 
             (this.checkInDate!=null &&
              this.checkInDate.equals(other.getCheckInDate()))) &&
            ((this.checkOutDate==null && other.getCheckOutDate()==null) || 
             (this.checkOutDate!=null &&
              this.checkOutDate.equals(other.getCheckOutDate()))) &&
            ((this.rate==null && other.getRate()==null) || 
             (this.rate!=null &&
              this.rate.equals(other.getRate()))) &&
            ((this.extraCharges==null && other.getExtraCharges()==null) || 
             (this.extraCharges!=null &&
              this.extraCharges.equals(other.getExtraCharges()))) &&
            ((this.extraChargeAmtInfo==null && other.getExtraChargeAmtInfo()==null) || 
             (this.extraChargeAmtInfo!=null &&
              this.extraChargeAmtInfo.equals(other.getExtraChargeAmtInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFolioNumber() != null) {
            _hashCode += getFolioNumber().hashCode();
        }
        if (getDuration() != null) {
            _hashCode += getDuration().hashCode();
        }
        if (getCheckInDate() != null) {
            _hashCode += getCheckInDate().hashCode();
        }
        if (getCheckOutDate() != null) {
            _hashCode += getCheckOutDate().hashCode();
        }
        if (getRate() != null) {
            _hashCode += getRate().hashCode();
        }
        if (getExtraCharges() != null) {
            _hashCode += getExtraCharges().hashCode();
        }
        if (getExtraChargeAmtInfo() != null) {
            _hashCode += getExtraChargeAmtInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LodgingDataEditType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataEditType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folioNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "FolioNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Duration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkInDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CheckInDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkOutDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CheckOutDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Rate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extraCharges");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExtraCharges"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExtraChargesDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extraChargeAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExtraChargeAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
