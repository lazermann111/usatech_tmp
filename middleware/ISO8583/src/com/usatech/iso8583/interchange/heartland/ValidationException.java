package com.usatech.iso8583.interchange.heartland;

public class ValidationException extends Exception
{
	private static final long serialVersionUID = -4611922966647688521L;

	public ValidationException()
	{
		super();
	}

	public ValidationException(String msg)
	{
		super(msg);
	}

	public ValidationException(String msg, Exception e)
	{
		super(msg, e);
	}
}
