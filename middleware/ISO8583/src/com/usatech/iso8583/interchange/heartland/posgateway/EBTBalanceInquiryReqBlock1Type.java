/**
 * EBTBalanceInquiryReqBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class EBTBalanceInquiryReqBlock1Type  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData;

    private java.math.BigDecimal amt;

    private java.lang.String pinBlock;

    private com.usatech.iso8583.interchange.heartland.posgateway.EBTBalanceInquiryType balanceInquiryType;

    public EBTBalanceInquiryReqBlock1Type() {
    }

    public EBTBalanceInquiryReqBlock1Type(
           com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData,
           java.math.BigDecimal amt,
           java.lang.String pinBlock,
           com.usatech.iso8583.interchange.heartland.posgateway.EBTBalanceInquiryType balanceInquiryType) {
           this.cardData = cardData;
           this.amt = amt;
           this.pinBlock = pinBlock;
           this.balanceInquiryType = balanceInquiryType;
    }


    /**
     * Gets the cardData value for this EBTBalanceInquiryReqBlock1Type.
     * 
     * @return cardData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardDataType getCardData() {
        return cardData;
    }


    /**
     * Sets the cardData value for this EBTBalanceInquiryReqBlock1Type.
     * 
     * @param cardData
     */
    public void setCardData(com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData) {
        this.cardData = cardData;
    }


    /**
     * Gets the amt value for this EBTBalanceInquiryReqBlock1Type.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this EBTBalanceInquiryReqBlock1Type.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the pinBlock value for this EBTBalanceInquiryReqBlock1Type.
     * 
     * @return pinBlock
     */
    public java.lang.String getPinBlock() {
        return pinBlock;
    }


    /**
     * Sets the pinBlock value for this EBTBalanceInquiryReqBlock1Type.
     * 
     * @param pinBlock
     */
    public void setPinBlock(java.lang.String pinBlock) {
        this.pinBlock = pinBlock;
    }


    /**
     * Gets the balanceInquiryType value for this EBTBalanceInquiryReqBlock1Type.
     * 
     * @return balanceInquiryType
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.EBTBalanceInquiryType getBalanceInquiryType() {
        return balanceInquiryType;
    }


    /**
     * Sets the balanceInquiryType value for this EBTBalanceInquiryReqBlock1Type.
     * 
     * @param balanceInquiryType
     */
    public void setBalanceInquiryType(com.usatech.iso8583.interchange.heartland.posgateway.EBTBalanceInquiryType balanceInquiryType) {
        this.balanceInquiryType = balanceInquiryType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EBTBalanceInquiryReqBlock1Type)) return false;
        EBTBalanceInquiryReqBlock1Type other = (EBTBalanceInquiryReqBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardData==null && other.getCardData()==null) || 
             (this.cardData!=null &&
              this.cardData.equals(other.getCardData()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.pinBlock==null && other.getPinBlock()==null) || 
             (this.pinBlock!=null &&
              this.pinBlock.equals(other.getPinBlock()))) &&
            ((this.balanceInquiryType==null && other.getBalanceInquiryType()==null) || 
             (this.balanceInquiryType!=null &&
              this.balanceInquiryType.equals(other.getBalanceInquiryType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardData() != null) {
            _hashCode += getCardData().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getPinBlock() != null) {
            _hashCode += getPinBlock().hashCode();
        }
        if (getBalanceInquiryType() != null) {
            _hashCode += getBalanceInquiryType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EBTBalanceInquiryReqBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTBalanceInquiryReqBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pinBlock");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PinBlock"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceInquiryType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BalanceInquiryType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTBalanceInquiryType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
