/**
 * PosReportSearchRspTypeDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportSearchRspTypeDetails  implements java.io.Serializable {
    private int gatewayTxnId;

    private int deviceId;

    private java.lang.String serviceName;

    private java.util.Calendar txnUtcDT;

    private java.lang.String txnStatus;

    private java.lang.String siteTrace;

    private java.lang.String debitCreditInd;

    private java.lang.String saleReturnInd;

    private java.lang.String cardType;

    private java.lang.String maskedCardNbr;

    private java.math.BigDecimal amt;

    private java.math.BigDecimal authAmt;

    private java.math.BigDecimal settlementAmt;

    private java.math.BigDecimal cashbackAmtInfo;

    private java.math.BigDecimal gratuityAmtInfo;

    private java.lang.String rspCode;

    private java.lang.String rspText;

    private java.lang.String authCode;

    private com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion;

    private java.util.Calendar txnDT;

    public PosReportSearchRspTypeDetails() {
    }

    public PosReportSearchRspTypeDetails(
           int gatewayTxnId,
           int deviceId,
           java.lang.String serviceName,
           java.util.Calendar txnUtcDT,
           java.lang.String txnStatus,
           java.lang.String siteTrace,
           java.lang.String debitCreditInd,
           java.lang.String saleReturnInd,
           java.lang.String cardType,
           java.lang.String maskedCardNbr,
           java.math.BigDecimal amt,
           java.math.BigDecimal authAmt,
           java.math.BigDecimal settlementAmt,
           java.math.BigDecimal cashbackAmtInfo,
           java.math.BigDecimal gratuityAmtInfo,
           java.lang.String rspCode,
           java.lang.String rspText,
           java.lang.String authCode,
           com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion,
           java.util.Calendar txnDT) {
           this.gatewayTxnId = gatewayTxnId;
           this.deviceId = deviceId;
           this.serviceName = serviceName;
           this.txnUtcDT = txnUtcDT;
           this.txnStatus = txnStatus;
           this.siteTrace = siteTrace;
           this.debitCreditInd = debitCreditInd;
           this.saleReturnInd = saleReturnInd;
           this.cardType = cardType;
           this.maskedCardNbr = maskedCardNbr;
           this.amt = amt;
           this.authAmt = authAmt;
           this.settlementAmt = settlementAmt;
           this.cashbackAmtInfo = cashbackAmtInfo;
           this.gratuityAmtInfo = gratuityAmtInfo;
           this.rspCode = rspCode;
           this.rspText = rspText;
           this.authCode = authCode;
           this.tzConversion = tzConversion;
           this.txnDT = txnDT;
    }


    /**
     * Gets the gatewayTxnId value for this PosReportSearchRspTypeDetails.
     * 
     * @return gatewayTxnId
     */
    public int getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this PosReportSearchRspTypeDetails.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(int gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the deviceId value for this PosReportSearchRspTypeDetails.
     * 
     * @return deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportSearchRspTypeDetails.
     * 
     * @param deviceId
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the serviceName value for this PosReportSearchRspTypeDetails.
     * 
     * @return serviceName
     */
    public java.lang.String getServiceName() {
        return serviceName;
    }


    /**
     * Sets the serviceName value for this PosReportSearchRspTypeDetails.
     * 
     * @param serviceName
     */
    public void setServiceName(java.lang.String serviceName) {
        this.serviceName = serviceName;
    }


    /**
     * Gets the txnUtcDT value for this PosReportSearchRspTypeDetails.
     * 
     * @return txnUtcDT
     */
    public java.util.Calendar getTxnUtcDT() {
        return txnUtcDT;
    }


    /**
     * Sets the txnUtcDT value for this PosReportSearchRspTypeDetails.
     * 
     * @param txnUtcDT
     */
    public void setTxnUtcDT(java.util.Calendar txnUtcDT) {
        this.txnUtcDT = txnUtcDT;
    }


    /**
     * Gets the txnStatus value for this PosReportSearchRspTypeDetails.
     * 
     * @return txnStatus
     */
    public java.lang.String getTxnStatus() {
        return txnStatus;
    }


    /**
     * Sets the txnStatus value for this PosReportSearchRspTypeDetails.
     * 
     * @param txnStatus
     */
    public void setTxnStatus(java.lang.String txnStatus) {
        this.txnStatus = txnStatus;
    }


    /**
     * Gets the siteTrace value for this PosReportSearchRspTypeDetails.
     * 
     * @return siteTrace
     */
    public java.lang.String getSiteTrace() {
        return siteTrace;
    }


    /**
     * Sets the siteTrace value for this PosReportSearchRspTypeDetails.
     * 
     * @param siteTrace
     */
    public void setSiteTrace(java.lang.String siteTrace) {
        this.siteTrace = siteTrace;
    }


    /**
     * Gets the debitCreditInd value for this PosReportSearchRspTypeDetails.
     * 
     * @return debitCreditInd
     */
    public java.lang.String getDebitCreditInd() {
        return debitCreditInd;
    }


    /**
     * Sets the debitCreditInd value for this PosReportSearchRspTypeDetails.
     * 
     * @param debitCreditInd
     */
    public void setDebitCreditInd(java.lang.String debitCreditInd) {
        this.debitCreditInd = debitCreditInd;
    }


    /**
     * Gets the saleReturnInd value for this PosReportSearchRspTypeDetails.
     * 
     * @return saleReturnInd
     */
    public java.lang.String getSaleReturnInd() {
        return saleReturnInd;
    }


    /**
     * Sets the saleReturnInd value for this PosReportSearchRspTypeDetails.
     * 
     * @param saleReturnInd
     */
    public void setSaleReturnInd(java.lang.String saleReturnInd) {
        this.saleReturnInd = saleReturnInd;
    }


    /**
     * Gets the cardType value for this PosReportSearchRspTypeDetails.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this PosReportSearchRspTypeDetails.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the maskedCardNbr value for this PosReportSearchRspTypeDetails.
     * 
     * @return maskedCardNbr
     */
    public java.lang.String getMaskedCardNbr() {
        return maskedCardNbr;
    }


    /**
     * Sets the maskedCardNbr value for this PosReportSearchRspTypeDetails.
     * 
     * @param maskedCardNbr
     */
    public void setMaskedCardNbr(java.lang.String maskedCardNbr) {
        this.maskedCardNbr = maskedCardNbr;
    }


    /**
     * Gets the amt value for this PosReportSearchRspTypeDetails.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this PosReportSearchRspTypeDetails.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the authAmt value for this PosReportSearchRspTypeDetails.
     * 
     * @return authAmt
     */
    public java.math.BigDecimal getAuthAmt() {
        return authAmt;
    }


    /**
     * Sets the authAmt value for this PosReportSearchRspTypeDetails.
     * 
     * @param authAmt
     */
    public void setAuthAmt(java.math.BigDecimal authAmt) {
        this.authAmt = authAmt;
    }


    /**
     * Gets the settlementAmt value for this PosReportSearchRspTypeDetails.
     * 
     * @return settlementAmt
     */
    public java.math.BigDecimal getSettlementAmt() {
        return settlementAmt;
    }


    /**
     * Sets the settlementAmt value for this PosReportSearchRspTypeDetails.
     * 
     * @param settlementAmt
     */
    public void setSettlementAmt(java.math.BigDecimal settlementAmt) {
        this.settlementAmt = settlementAmt;
    }


    /**
     * Gets the cashbackAmtInfo value for this PosReportSearchRspTypeDetails.
     * 
     * @return cashbackAmtInfo
     */
    public java.math.BigDecimal getCashbackAmtInfo() {
        return cashbackAmtInfo;
    }


    /**
     * Sets the cashbackAmtInfo value for this PosReportSearchRspTypeDetails.
     * 
     * @param cashbackAmtInfo
     */
    public void setCashbackAmtInfo(java.math.BigDecimal cashbackAmtInfo) {
        this.cashbackAmtInfo = cashbackAmtInfo;
    }


    /**
     * Gets the gratuityAmtInfo value for this PosReportSearchRspTypeDetails.
     * 
     * @return gratuityAmtInfo
     */
    public java.math.BigDecimal getGratuityAmtInfo() {
        return gratuityAmtInfo;
    }


    /**
     * Sets the gratuityAmtInfo value for this PosReportSearchRspTypeDetails.
     * 
     * @param gratuityAmtInfo
     */
    public void setGratuityAmtInfo(java.math.BigDecimal gratuityAmtInfo) {
        this.gratuityAmtInfo = gratuityAmtInfo;
    }


    /**
     * Gets the rspCode value for this PosReportSearchRspTypeDetails.
     * 
     * @return rspCode
     */
    public java.lang.String getRspCode() {
        return rspCode;
    }


    /**
     * Sets the rspCode value for this PosReportSearchRspTypeDetails.
     * 
     * @param rspCode
     */
    public void setRspCode(java.lang.String rspCode) {
        this.rspCode = rspCode;
    }


    /**
     * Gets the rspText value for this PosReportSearchRspTypeDetails.
     * 
     * @return rspText
     */
    public java.lang.String getRspText() {
        return rspText;
    }


    /**
     * Sets the rspText value for this PosReportSearchRspTypeDetails.
     * 
     * @param rspText
     */
    public void setRspText(java.lang.String rspText) {
        this.rspText = rspText;
    }


    /**
     * Gets the authCode value for this PosReportSearchRspTypeDetails.
     * 
     * @return authCode
     */
    public java.lang.String getAuthCode() {
        return authCode;
    }


    /**
     * Sets the authCode value for this PosReportSearchRspTypeDetails.
     * 
     * @param authCode
     */
    public void setAuthCode(java.lang.String authCode) {
        this.authCode = authCode;
    }


    /**
     * Gets the tzConversion value for this PosReportSearchRspTypeDetails.
     * 
     * @return tzConversion
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType getTzConversion() {
        return tzConversion;
    }


    /**
     * Sets the tzConversion value for this PosReportSearchRspTypeDetails.
     * 
     * @param tzConversion
     */
    public void setTzConversion(com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
        this.tzConversion = tzConversion;
    }


    /**
     * Gets the txnDT value for this PosReportSearchRspTypeDetails.
     * 
     * @return txnDT
     */
    public java.util.Calendar getTxnDT() {
        return txnDT;
    }


    /**
     * Sets the txnDT value for this PosReportSearchRspTypeDetails.
     * 
     * @param txnDT
     */
    public void setTxnDT(java.util.Calendar txnDT) {
        this.txnDT = txnDT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportSearchRspTypeDetails)) return false;
        PosReportSearchRspTypeDetails other = (PosReportSearchRspTypeDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.gatewayTxnId == other.getGatewayTxnId() &&
            this.deviceId == other.getDeviceId() &&
            ((this.serviceName==null && other.getServiceName()==null) || 
             (this.serviceName!=null &&
              this.serviceName.equals(other.getServiceName()))) &&
            ((this.txnUtcDT==null && other.getTxnUtcDT()==null) || 
             (this.txnUtcDT!=null &&
              this.txnUtcDT.equals(other.getTxnUtcDT()))) &&
            ((this.txnStatus==null && other.getTxnStatus()==null) || 
             (this.txnStatus!=null &&
              this.txnStatus.equals(other.getTxnStatus()))) &&
            ((this.siteTrace==null && other.getSiteTrace()==null) || 
             (this.siteTrace!=null &&
              this.siteTrace.equals(other.getSiteTrace()))) &&
            ((this.debitCreditInd==null && other.getDebitCreditInd()==null) || 
             (this.debitCreditInd!=null &&
              this.debitCreditInd.equals(other.getDebitCreditInd()))) &&
            ((this.saleReturnInd==null && other.getSaleReturnInd()==null) || 
             (this.saleReturnInd!=null &&
              this.saleReturnInd.equals(other.getSaleReturnInd()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.maskedCardNbr==null && other.getMaskedCardNbr()==null) || 
             (this.maskedCardNbr!=null &&
              this.maskedCardNbr.equals(other.getMaskedCardNbr()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.authAmt==null && other.getAuthAmt()==null) || 
             (this.authAmt!=null &&
              this.authAmt.equals(other.getAuthAmt()))) &&
            ((this.settlementAmt==null && other.getSettlementAmt()==null) || 
             (this.settlementAmt!=null &&
              this.settlementAmt.equals(other.getSettlementAmt()))) &&
            ((this.cashbackAmtInfo==null && other.getCashbackAmtInfo()==null) || 
             (this.cashbackAmtInfo!=null &&
              this.cashbackAmtInfo.equals(other.getCashbackAmtInfo()))) &&
            ((this.gratuityAmtInfo==null && other.getGratuityAmtInfo()==null) || 
             (this.gratuityAmtInfo!=null &&
              this.gratuityAmtInfo.equals(other.getGratuityAmtInfo()))) &&
            ((this.rspCode==null && other.getRspCode()==null) || 
             (this.rspCode!=null &&
              this.rspCode.equals(other.getRspCode()))) &&
            ((this.rspText==null && other.getRspText()==null) || 
             (this.rspText!=null &&
              this.rspText.equals(other.getRspText()))) &&
            ((this.authCode==null && other.getAuthCode()==null) || 
             (this.authCode!=null &&
              this.authCode.equals(other.getAuthCode()))) &&
            ((this.tzConversion==null && other.getTzConversion()==null) || 
             (this.tzConversion!=null &&
              this.tzConversion.equals(other.getTzConversion()))) &&
            ((this.txnDT==null && other.getTxnDT()==null) || 
             (this.txnDT!=null &&
              this.txnDT.equals(other.getTxnDT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGatewayTxnId();
        _hashCode += getDeviceId();
        if (getServiceName() != null) {
            _hashCode += getServiceName().hashCode();
        }
        if (getTxnUtcDT() != null) {
            _hashCode += getTxnUtcDT().hashCode();
        }
        if (getTxnStatus() != null) {
            _hashCode += getTxnStatus().hashCode();
        }
        if (getSiteTrace() != null) {
            _hashCode += getSiteTrace().hashCode();
        }
        if (getDebitCreditInd() != null) {
            _hashCode += getDebitCreditInd().hashCode();
        }
        if (getSaleReturnInd() != null) {
            _hashCode += getSaleReturnInd().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getMaskedCardNbr() != null) {
            _hashCode += getMaskedCardNbr().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getAuthAmt() != null) {
            _hashCode += getAuthAmt().hashCode();
        }
        if (getSettlementAmt() != null) {
            _hashCode += getSettlementAmt().hashCode();
        }
        if (getCashbackAmtInfo() != null) {
            _hashCode += getCashbackAmtInfo().hashCode();
        }
        if (getGratuityAmtInfo() != null) {
            _hashCode += getGratuityAmtInfo().hashCode();
        }
        if (getRspCode() != null) {
            _hashCode += getRspCode().hashCode();
        }
        if (getRspText() != null) {
            _hashCode += getRspText().hashCode();
        }
        if (getAuthCode() != null) {
            _hashCode += getAuthCode().hashCode();
        }
        if (getTzConversion() != null) {
            _hashCode += getTzConversion().hashCode();
        }
        if (getTxnDT() != null) {
            _hashCode += getTxnDT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportSearchRspTypeDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportSearchRspType>Details"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ServiceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteTrace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteTrace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCreditInd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitCreditInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleReturnInd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleReturnInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maskedCardNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MaskedCardNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("settlementAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SettlementAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashbackAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CashbackAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gratuityAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GratuityAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tzConversion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TzConversion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "tzoneConversionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
