/**
 * PosBatchCloseRspType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosBatchCloseRspType  implements java.io.Serializable {
    private int batchId;

    private int txnCnt;

    private java.math.BigDecimal totalAmt;

    public PosBatchCloseRspType() {
    }

    public PosBatchCloseRspType(
           int batchId,
           int txnCnt,
           java.math.BigDecimal totalAmt) {
           this.batchId = batchId;
           this.txnCnt = txnCnt;
           this.totalAmt = totalAmt;
    }


    /**
     * Gets the batchId value for this PosBatchCloseRspType.
     * 
     * @return batchId
     */
    public int getBatchId() {
        return batchId;
    }


    /**
     * Sets the batchId value for this PosBatchCloseRspType.
     * 
     * @param batchId
     */
    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }


    /**
     * Gets the txnCnt value for this PosBatchCloseRspType.
     * 
     * @return txnCnt
     */
    public int getTxnCnt() {
        return txnCnt;
    }


    /**
     * Sets the txnCnt value for this PosBatchCloseRspType.
     * 
     * @param txnCnt
     */
    public void setTxnCnt(int txnCnt) {
        this.txnCnt = txnCnt;
    }


    /**
     * Gets the totalAmt value for this PosBatchCloseRspType.
     * 
     * @return totalAmt
     */
    public java.math.BigDecimal getTotalAmt() {
        return totalAmt;
    }


    /**
     * Sets the totalAmt value for this PosBatchCloseRspType.
     * 
     * @param totalAmt
     */
    public void setTotalAmt(java.math.BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosBatchCloseRspType)) return false;
        PosBatchCloseRspType other = (PosBatchCloseRspType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.batchId == other.getBatchId() &&
            this.txnCnt == other.getTxnCnt() &&
            ((this.totalAmt==null && other.getTotalAmt()==null) || 
             (this.totalAmt!=null &&
              this.totalAmt.equals(other.getTotalAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getBatchId();
        _hashCode += getTxnCnt();
        if (getTotalAmt() != null) {
            _hashCode += getTotalAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosBatchCloseRspType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosBatchCloseRspType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TotalAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
