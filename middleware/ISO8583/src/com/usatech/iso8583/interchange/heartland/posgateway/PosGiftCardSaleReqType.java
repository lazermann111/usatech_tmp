/**
 * PosGiftCardSaleReqType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosGiftCardSaleReqType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.GiftCardSaleReqBlock1Type block1;

    public PosGiftCardSaleReqType() {
    }

    public PosGiftCardSaleReqType(
           com.usatech.iso8583.interchange.heartland.posgateway.GiftCardSaleReqBlock1Type block1) {
           this.block1 = block1;
    }


    /**
     * Gets the block1 value for this PosGiftCardSaleReqType.
     * 
     * @return block1
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.GiftCardSaleReqBlock1Type getBlock1() {
        return block1;
    }


    /**
     * Sets the block1 value for this PosGiftCardSaleReqType.
     * 
     * @param block1
     */
    public void setBlock1(com.usatech.iso8583.interchange.heartland.posgateway.GiftCardSaleReqBlock1Type block1) {
        this.block1 = block1;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosGiftCardSaleReqType)) return false;
        PosGiftCardSaleReqType other = (PosGiftCardSaleReqType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.block1==null && other.getBlock1()==null) || 
             (this.block1!=null &&
              this.block1.equals(other.getBlock1())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBlock1() != null) {
            _hashCode += getBlock1().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosGiftCardSaleReqType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardSaleReqType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("block1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Block1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardSaleReqBlock1Type"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
