/**
 * PosReportTxnDetailReqType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportTxnDetailReqType  implements java.io.Serializable {
    private int txnId;

    private com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion;

    public PosReportTxnDetailReqType() {
    }

    public PosReportTxnDetailReqType(
           int txnId,
           com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
           this.txnId = txnId;
           this.tzConversion = tzConversion;
    }


    /**
     * Gets the txnId value for this PosReportTxnDetailReqType.
     * 
     * @return txnId
     */
    public int getTxnId() {
        return txnId;
    }


    /**
     * Sets the txnId value for this PosReportTxnDetailReqType.
     * 
     * @param txnId
     */
    public void setTxnId(int txnId) {
        this.txnId = txnId;
    }


    /**
     * Gets the tzConversion value for this PosReportTxnDetailReqType.
     * 
     * @return tzConversion
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType getTzConversion() {
        return tzConversion;
    }


    /**
     * Sets the tzConversion value for this PosReportTxnDetailReqType.
     * 
     * @param tzConversion
     */
    public void setTzConversion(com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
        this.tzConversion = tzConversion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportTxnDetailReqType)) return false;
        PosReportTxnDetailReqType other = (PosReportTxnDetailReqType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.txnId == other.getTxnId() &&
            ((this.tzConversion==null && other.getTzConversion()==null) || 
             (this.tzConversion!=null &&
              this.tzConversion.equals(other.getTzConversion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getTxnId();
        if (getTzConversion() != null) {
            _hashCode += getTzConversion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportTxnDetailReqType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportTxnDetailReqType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tzConversion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TzConversion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "tzoneConversionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
