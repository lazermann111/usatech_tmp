/**
 * PosReportBatchSummaryRspTypeHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportBatchSummaryRspTypeHeader  implements java.io.Serializable {
    private int siteId;

    private java.lang.String merchName;

    private int deviceId;

    private int batchId;

    private java.lang.String batchStatus;

    private int batchSeqNbr;

    private java.util.Calendar openUtcDT;

    private java.util.Calendar closeUtcDT;

    private int openTxnId;

    private java.lang.Integer closeTxnId;

    private int creditCnt;

    private java.math.BigDecimal creditAmt;

    private int debitCnt;

    private java.math.BigDecimal debitAmt;

    private int saleCnt;

    private java.math.BigDecimal saleAmt;

    private int returnCnt;

    private java.math.BigDecimal returnAmt;

    private int totalCnt;

    private java.math.BigDecimal totalAmt;

    private java.math.BigDecimal totalGratuityAmtInfo;

    public PosReportBatchSummaryRspTypeHeader() {
    }

    public PosReportBatchSummaryRspTypeHeader(
           int siteId,
           java.lang.String merchName,
           int deviceId,
           int batchId,
           java.lang.String batchStatus,
           int batchSeqNbr,
           java.util.Calendar openUtcDT,
           java.util.Calendar closeUtcDT,
           int openTxnId,
           java.lang.Integer closeTxnId,
           int creditCnt,
           java.math.BigDecimal creditAmt,
           int debitCnt,
           java.math.BigDecimal debitAmt,
           int saleCnt,
           java.math.BigDecimal saleAmt,
           int returnCnt,
           java.math.BigDecimal returnAmt,
           int totalCnt,
           java.math.BigDecimal totalAmt,
           java.math.BigDecimal totalGratuityAmtInfo) {
           this.siteId = siteId;
           this.merchName = merchName;
           this.deviceId = deviceId;
           this.batchId = batchId;
           this.batchStatus = batchStatus;
           this.batchSeqNbr = batchSeqNbr;
           this.openUtcDT = openUtcDT;
           this.closeUtcDT = closeUtcDT;
           this.openTxnId = openTxnId;
           this.closeTxnId = closeTxnId;
           this.creditCnt = creditCnt;
           this.creditAmt = creditAmt;
           this.debitCnt = debitCnt;
           this.debitAmt = debitAmt;
           this.saleCnt = saleCnt;
           this.saleAmt = saleAmt;
           this.returnCnt = returnCnt;
           this.returnAmt = returnAmt;
           this.totalCnt = totalCnt;
           this.totalAmt = totalAmt;
           this.totalGratuityAmtInfo = totalGratuityAmtInfo;
    }


    /**
     * Gets the siteId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return siteId
     */
    public int getSiteId() {
        return siteId;
    }


    /**
     * Sets the siteId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param siteId
     */
    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }


    /**
     * Gets the merchName value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return merchName
     */
    public java.lang.String getMerchName() {
        return merchName;
    }


    /**
     * Sets the merchName value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param merchName
     */
    public void setMerchName(java.lang.String merchName) {
        this.merchName = merchName;
    }


    /**
     * Gets the deviceId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param deviceId
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the batchId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return batchId
     */
    public int getBatchId() {
        return batchId;
    }


    /**
     * Sets the batchId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param batchId
     */
    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }


    /**
     * Gets the batchStatus value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return batchStatus
     */
    public java.lang.String getBatchStatus() {
        return batchStatus;
    }


    /**
     * Sets the batchStatus value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param batchStatus
     */
    public void setBatchStatus(java.lang.String batchStatus) {
        this.batchStatus = batchStatus;
    }


    /**
     * Gets the batchSeqNbr value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return batchSeqNbr
     */
    public int getBatchSeqNbr() {
        return batchSeqNbr;
    }


    /**
     * Sets the batchSeqNbr value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param batchSeqNbr
     */
    public void setBatchSeqNbr(int batchSeqNbr) {
        this.batchSeqNbr = batchSeqNbr;
    }


    /**
     * Gets the openUtcDT value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return openUtcDT
     */
    public java.util.Calendar getOpenUtcDT() {
        return openUtcDT;
    }


    /**
     * Sets the openUtcDT value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param openUtcDT
     */
    public void setOpenUtcDT(java.util.Calendar openUtcDT) {
        this.openUtcDT = openUtcDT;
    }


    /**
     * Gets the closeUtcDT value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return closeUtcDT
     */
    public java.util.Calendar getCloseUtcDT() {
        return closeUtcDT;
    }


    /**
     * Sets the closeUtcDT value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param closeUtcDT
     */
    public void setCloseUtcDT(java.util.Calendar closeUtcDT) {
        this.closeUtcDT = closeUtcDT;
    }


    /**
     * Gets the openTxnId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return openTxnId
     */
    public int getOpenTxnId() {
        return openTxnId;
    }


    /**
     * Sets the openTxnId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param openTxnId
     */
    public void setOpenTxnId(int openTxnId) {
        this.openTxnId = openTxnId;
    }


    /**
     * Gets the closeTxnId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return closeTxnId
     */
    public java.lang.Integer getCloseTxnId() {
        return closeTxnId;
    }


    /**
     * Sets the closeTxnId value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param closeTxnId
     */
    public void setCloseTxnId(java.lang.Integer closeTxnId) {
        this.closeTxnId = closeTxnId;
    }


    /**
     * Gets the creditCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return creditCnt
     */
    public int getCreditCnt() {
        return creditCnt;
    }


    /**
     * Sets the creditCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param creditCnt
     */
    public void setCreditCnt(int creditCnt) {
        this.creditCnt = creditCnt;
    }


    /**
     * Gets the creditAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return creditAmt
     */
    public java.math.BigDecimal getCreditAmt() {
        return creditAmt;
    }


    /**
     * Sets the creditAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param creditAmt
     */
    public void setCreditAmt(java.math.BigDecimal creditAmt) {
        this.creditAmt = creditAmt;
    }


    /**
     * Gets the debitCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return debitCnt
     */
    public int getDebitCnt() {
        return debitCnt;
    }


    /**
     * Sets the debitCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param debitCnt
     */
    public void setDebitCnt(int debitCnt) {
        this.debitCnt = debitCnt;
    }


    /**
     * Gets the debitAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return debitAmt
     */
    public java.math.BigDecimal getDebitAmt() {
        return debitAmt;
    }


    /**
     * Sets the debitAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param debitAmt
     */
    public void setDebitAmt(java.math.BigDecimal debitAmt) {
        this.debitAmt = debitAmt;
    }


    /**
     * Gets the saleCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return saleCnt
     */
    public int getSaleCnt() {
        return saleCnt;
    }


    /**
     * Sets the saleCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param saleCnt
     */
    public void setSaleCnt(int saleCnt) {
        this.saleCnt = saleCnt;
    }


    /**
     * Gets the saleAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return saleAmt
     */
    public java.math.BigDecimal getSaleAmt() {
        return saleAmt;
    }


    /**
     * Sets the saleAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param saleAmt
     */
    public void setSaleAmt(java.math.BigDecimal saleAmt) {
        this.saleAmt = saleAmt;
    }


    /**
     * Gets the returnCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return returnCnt
     */
    public int getReturnCnt() {
        return returnCnt;
    }


    /**
     * Sets the returnCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param returnCnt
     */
    public void setReturnCnt(int returnCnt) {
        this.returnCnt = returnCnt;
    }


    /**
     * Gets the returnAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return returnAmt
     */
    public java.math.BigDecimal getReturnAmt() {
        return returnAmt;
    }


    /**
     * Sets the returnAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param returnAmt
     */
    public void setReturnAmt(java.math.BigDecimal returnAmt) {
        this.returnAmt = returnAmt;
    }


    /**
     * Gets the totalCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return totalCnt
     */
    public int getTotalCnt() {
        return totalCnt;
    }


    /**
     * Sets the totalCnt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param totalCnt
     */
    public void setTotalCnt(int totalCnt) {
        this.totalCnt = totalCnt;
    }


    /**
     * Gets the totalAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return totalAmt
     */
    public java.math.BigDecimal getTotalAmt() {
        return totalAmt;
    }


    /**
     * Sets the totalAmt value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param totalAmt
     */
    public void setTotalAmt(java.math.BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }


    /**
     * Gets the totalGratuityAmtInfo value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @return totalGratuityAmtInfo
     */
    public java.math.BigDecimal getTotalGratuityAmtInfo() {
        return totalGratuityAmtInfo;
    }


    /**
     * Sets the totalGratuityAmtInfo value for this PosReportBatchSummaryRspTypeHeader.
     * 
     * @param totalGratuityAmtInfo
     */
    public void setTotalGratuityAmtInfo(java.math.BigDecimal totalGratuityAmtInfo) {
        this.totalGratuityAmtInfo = totalGratuityAmtInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportBatchSummaryRspTypeHeader)) return false;
        PosReportBatchSummaryRspTypeHeader other = (PosReportBatchSummaryRspTypeHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.siteId == other.getSiteId() &&
            ((this.merchName==null && other.getMerchName()==null) || 
             (this.merchName!=null &&
              this.merchName.equals(other.getMerchName()))) &&
            this.deviceId == other.getDeviceId() &&
            this.batchId == other.getBatchId() &&
            ((this.batchStatus==null && other.getBatchStatus()==null) || 
             (this.batchStatus!=null &&
              this.batchStatus.equals(other.getBatchStatus()))) &&
            this.batchSeqNbr == other.getBatchSeqNbr() &&
            ((this.openUtcDT==null && other.getOpenUtcDT()==null) || 
             (this.openUtcDT!=null &&
              this.openUtcDT.equals(other.getOpenUtcDT()))) &&
            ((this.closeUtcDT==null && other.getCloseUtcDT()==null) || 
             (this.closeUtcDT!=null &&
              this.closeUtcDT.equals(other.getCloseUtcDT()))) &&
            this.openTxnId == other.getOpenTxnId() &&
            ((this.closeTxnId==null && other.getCloseTxnId()==null) || 
             (this.closeTxnId!=null &&
              this.closeTxnId.equals(other.getCloseTxnId()))) &&
            this.creditCnt == other.getCreditCnt() &&
            ((this.creditAmt==null && other.getCreditAmt()==null) || 
             (this.creditAmt!=null &&
              this.creditAmt.equals(other.getCreditAmt()))) &&
            this.debitCnt == other.getDebitCnt() &&
            ((this.debitAmt==null && other.getDebitAmt()==null) || 
             (this.debitAmt!=null &&
              this.debitAmt.equals(other.getDebitAmt()))) &&
            this.saleCnt == other.getSaleCnt() &&
            ((this.saleAmt==null && other.getSaleAmt()==null) || 
             (this.saleAmt!=null &&
              this.saleAmt.equals(other.getSaleAmt()))) &&
            this.returnCnt == other.getReturnCnt() &&
            ((this.returnAmt==null && other.getReturnAmt()==null) || 
             (this.returnAmt!=null &&
              this.returnAmt.equals(other.getReturnAmt()))) &&
            this.totalCnt == other.getTotalCnt() &&
            ((this.totalAmt==null && other.getTotalAmt()==null) || 
             (this.totalAmt!=null &&
              this.totalAmt.equals(other.getTotalAmt()))) &&
            ((this.totalGratuityAmtInfo==null && other.getTotalGratuityAmtInfo()==null) || 
             (this.totalGratuityAmtInfo!=null &&
              this.totalGratuityAmtInfo.equals(other.getTotalGratuityAmtInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSiteId();
        if (getMerchName() != null) {
            _hashCode += getMerchName().hashCode();
        }
        _hashCode += getDeviceId();
        _hashCode += getBatchId();
        if (getBatchStatus() != null) {
            _hashCode += getBatchStatus().hashCode();
        }
        _hashCode += getBatchSeqNbr();
        if (getOpenUtcDT() != null) {
            _hashCode += getOpenUtcDT().hashCode();
        }
        if (getCloseUtcDT() != null) {
            _hashCode += getCloseUtcDT().hashCode();
        }
        _hashCode += getOpenTxnId();
        if (getCloseTxnId() != null) {
            _hashCode += getCloseTxnId().hashCode();
        }
        _hashCode += getCreditCnt();
        if (getCreditAmt() != null) {
            _hashCode += getCreditAmt().hashCode();
        }
        _hashCode += getDebitCnt();
        if (getDebitAmt() != null) {
            _hashCode += getDebitAmt().hashCode();
        }
        _hashCode += getSaleCnt();
        if (getSaleAmt() != null) {
            _hashCode += getSaleAmt().hashCode();
        }
        _hashCode += getReturnCnt();
        if (getReturnAmt() != null) {
            _hashCode += getReturnAmt().hashCode();
        }
        _hashCode += getTotalCnt();
        if (getTotalAmt() != null) {
            _hashCode += getTotalAmt().hashCode();
        }
        if (getTotalGratuityAmtInfo() != null) {
            _hashCode += getTotalGratuityAmtInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportBatchSummaryRspTypeHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchSummaryRspType>Header"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchSeqNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "BatchSeqNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "OpenUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closeUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CloseUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "OpenTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("closeTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CloseTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReturnCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReturnAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TotalCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TotalAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalGratuityAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TotalGratuityAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
