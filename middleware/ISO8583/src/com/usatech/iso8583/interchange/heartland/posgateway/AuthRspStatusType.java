/**
 * AuthRspStatusType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class AuthRspStatusType  implements java.io.Serializable {
    private java.lang.String rspCode;

    private java.lang.String rspText;

    private java.lang.String authCode;

    private java.lang.String AVSRsltCode;

    private java.lang.String CVVRsltCode;

    private java.lang.String CPCInd;

    private java.lang.String refNbr;

    private java.math.BigDecimal availableBalance;

    private java.math.BigDecimal authAmt;

    private com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType AVSResultCodeAction;

    private com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType CVVResultCodeAction;

    private java.lang.String cardType;

    private java.lang.String AVSRsltText;

    private java.lang.String CVVRsltText;

    public AuthRspStatusType() {
    }

    public AuthRspStatusType(
           java.lang.String rspCode,
           java.lang.String rspText,
           java.lang.String authCode,
           java.lang.String AVSRsltCode,
           java.lang.String CVVRsltCode,
           java.lang.String CPCInd,
           java.lang.String refNbr,
           java.math.BigDecimal availableBalance,
           java.math.BigDecimal authAmt,
           com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType AVSResultCodeAction,
           com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType CVVResultCodeAction,
           java.lang.String cardType,
           java.lang.String AVSRsltText,
           java.lang.String CVVRsltText) {
           this.rspCode = rspCode;
           this.rspText = rspText;
           this.authCode = authCode;
           this.AVSRsltCode = AVSRsltCode;
           this.CVVRsltCode = CVVRsltCode;
           this.CPCInd = CPCInd;
           this.refNbr = refNbr;
           this.availableBalance = availableBalance;
           this.authAmt = authAmt;
           this.AVSResultCodeAction = AVSResultCodeAction;
           this.CVVResultCodeAction = CVVResultCodeAction;
           this.cardType = cardType;
           this.AVSRsltText = AVSRsltText;
           this.CVVRsltText = CVVRsltText;
    }


    /**
     * Gets the rspCode value for this AuthRspStatusType.
     * 
     * @return rspCode
     */
    public java.lang.String getRspCode() {
        return rspCode;
    }


    /**
     * Sets the rspCode value for this AuthRspStatusType.
     * 
     * @param rspCode
     */
    public void setRspCode(java.lang.String rspCode) {
        this.rspCode = rspCode;
    }


    /**
     * Gets the rspText value for this AuthRspStatusType.
     * 
     * @return rspText
     */
    public java.lang.String getRspText() {
        return rspText;
    }


    /**
     * Sets the rspText value for this AuthRspStatusType.
     * 
     * @param rspText
     */
    public void setRspText(java.lang.String rspText) {
        this.rspText = rspText;
    }


    /**
     * Gets the authCode value for this AuthRspStatusType.
     * 
     * @return authCode
     */
    public java.lang.String getAuthCode() {
        return authCode;
    }


    /**
     * Sets the authCode value for this AuthRspStatusType.
     * 
     * @param authCode
     */
    public void setAuthCode(java.lang.String authCode) {
        this.authCode = authCode;
    }


    /**
     * Gets the AVSRsltCode value for this AuthRspStatusType.
     * 
     * @return AVSRsltCode
     */
    public java.lang.String getAVSRsltCode() {
        return AVSRsltCode;
    }


    /**
     * Sets the AVSRsltCode value for this AuthRspStatusType.
     * 
     * @param AVSRsltCode
     */
    public void setAVSRsltCode(java.lang.String AVSRsltCode) {
        this.AVSRsltCode = AVSRsltCode;
    }


    /**
     * Gets the CVVRsltCode value for this AuthRspStatusType.
     * 
     * @return CVVRsltCode
     */
    public java.lang.String getCVVRsltCode() {
        return CVVRsltCode;
    }


    /**
     * Sets the CVVRsltCode value for this AuthRspStatusType.
     * 
     * @param CVVRsltCode
     */
    public void setCVVRsltCode(java.lang.String CVVRsltCode) {
        this.CVVRsltCode = CVVRsltCode;
    }


    /**
     * Gets the CPCInd value for this AuthRspStatusType.
     * 
     * @return CPCInd
     */
    public java.lang.String getCPCInd() {
        return CPCInd;
    }


    /**
     * Sets the CPCInd value for this AuthRspStatusType.
     * 
     * @param CPCInd
     */
    public void setCPCInd(java.lang.String CPCInd) {
        this.CPCInd = CPCInd;
    }


    /**
     * Gets the refNbr value for this AuthRspStatusType.
     * 
     * @return refNbr
     */
    public java.lang.String getRefNbr() {
        return refNbr;
    }


    /**
     * Sets the refNbr value for this AuthRspStatusType.
     * 
     * @param refNbr
     */
    public void setRefNbr(java.lang.String refNbr) {
        this.refNbr = refNbr;
    }


    /**
     * Gets the availableBalance value for this AuthRspStatusType.
     * 
     * @return availableBalance
     */
    public java.math.BigDecimal getAvailableBalance() {
        return availableBalance;
    }


    /**
     * Sets the availableBalance value for this AuthRspStatusType.
     * 
     * @param availableBalance
     */
    public void setAvailableBalance(java.math.BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }


    /**
     * Gets the authAmt value for this AuthRspStatusType.
     * 
     * @return authAmt
     */
    public java.math.BigDecimal getAuthAmt() {
        return authAmt;
    }


    /**
     * Sets the authAmt value for this AuthRspStatusType.
     * 
     * @param authAmt
     */
    public void setAuthAmt(java.math.BigDecimal authAmt) {
        this.authAmt = authAmt;
    }


    /**
     * Gets the AVSResultCodeAction value for this AuthRspStatusType.
     * 
     * @return AVSResultCodeAction
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType getAVSResultCodeAction() {
        return AVSResultCodeAction;
    }


    /**
     * Sets the AVSResultCodeAction value for this AuthRspStatusType.
     * 
     * @param AVSResultCodeAction
     */
    public void setAVSResultCodeAction(com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType AVSResultCodeAction) {
        this.AVSResultCodeAction = AVSResultCodeAction;
    }


    /**
     * Gets the CVVResultCodeAction value for this AuthRspStatusType.
     * 
     * @return CVVResultCodeAction
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType getCVVResultCodeAction() {
        return CVVResultCodeAction;
    }


    /**
     * Sets the CVVResultCodeAction value for this AuthRspStatusType.
     * 
     * @param CVVResultCodeAction
     */
    public void setCVVResultCodeAction(com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType CVVResultCodeAction) {
        this.CVVResultCodeAction = CVVResultCodeAction;
    }


    /**
     * Gets the cardType value for this AuthRspStatusType.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this AuthRspStatusType.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the AVSRsltText value for this AuthRspStatusType.
     * 
     * @return AVSRsltText
     */
    public java.lang.String getAVSRsltText() {
        return AVSRsltText;
    }


    /**
     * Sets the AVSRsltText value for this AuthRspStatusType.
     * 
     * @param AVSRsltText
     */
    public void setAVSRsltText(java.lang.String AVSRsltText) {
        this.AVSRsltText = AVSRsltText;
    }


    /**
     * Gets the CVVRsltText value for this AuthRspStatusType.
     * 
     * @return CVVRsltText
     */
    public java.lang.String getCVVRsltText() {
        return CVVRsltText;
    }


    /**
     * Sets the CVVRsltText value for this AuthRspStatusType.
     * 
     * @param CVVRsltText
     */
    public void setCVVRsltText(java.lang.String CVVRsltText) {
        this.CVVRsltText = CVVRsltText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthRspStatusType)) return false;
        AuthRspStatusType other = (AuthRspStatusType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rspCode==null && other.getRspCode()==null) || 
             (this.rspCode!=null &&
              this.rspCode.equals(other.getRspCode()))) &&
            ((this.rspText==null && other.getRspText()==null) || 
             (this.rspText!=null &&
              this.rspText.equals(other.getRspText()))) &&
            ((this.authCode==null && other.getAuthCode()==null) || 
             (this.authCode!=null &&
              this.authCode.equals(other.getAuthCode()))) &&
            ((this.AVSRsltCode==null && other.getAVSRsltCode()==null) || 
             (this.AVSRsltCode!=null &&
              this.AVSRsltCode.equals(other.getAVSRsltCode()))) &&
            ((this.CVVRsltCode==null && other.getCVVRsltCode()==null) || 
             (this.CVVRsltCode!=null &&
              this.CVVRsltCode.equals(other.getCVVRsltCode()))) &&
            ((this.CPCInd==null && other.getCPCInd()==null) || 
             (this.CPCInd!=null &&
              this.CPCInd.equals(other.getCPCInd()))) &&
            ((this.refNbr==null && other.getRefNbr()==null) || 
             (this.refNbr!=null &&
              this.refNbr.equals(other.getRefNbr()))) &&
            ((this.availableBalance==null && other.getAvailableBalance()==null) || 
             (this.availableBalance!=null &&
              this.availableBalance.equals(other.getAvailableBalance()))) &&
            ((this.authAmt==null && other.getAuthAmt()==null) || 
             (this.authAmt!=null &&
              this.authAmt.equals(other.getAuthAmt()))) &&
            ((this.AVSResultCodeAction==null && other.getAVSResultCodeAction()==null) || 
             (this.AVSResultCodeAction!=null &&
              this.AVSResultCodeAction.equals(other.getAVSResultCodeAction()))) &&
            ((this.CVVResultCodeAction==null && other.getCVVResultCodeAction()==null) || 
             (this.CVVResultCodeAction!=null &&
              this.CVVResultCodeAction.equals(other.getCVVResultCodeAction()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.AVSRsltText==null && other.getAVSRsltText()==null) || 
             (this.AVSRsltText!=null &&
              this.AVSRsltText.equals(other.getAVSRsltText()))) &&
            ((this.CVVRsltText==null && other.getCVVRsltText()==null) || 
             (this.CVVRsltText!=null &&
              this.CVVRsltText.equals(other.getCVVRsltText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRspCode() != null) {
            _hashCode += getRspCode().hashCode();
        }
        if (getRspText() != null) {
            _hashCode += getRspText().hashCode();
        }
        if (getAuthCode() != null) {
            _hashCode += getAuthCode().hashCode();
        }
        if (getAVSRsltCode() != null) {
            _hashCode += getAVSRsltCode().hashCode();
        }
        if (getCVVRsltCode() != null) {
            _hashCode += getCVVRsltCode().hashCode();
        }
        if (getCPCInd() != null) {
            _hashCode += getCPCInd().hashCode();
        }
        if (getRefNbr() != null) {
            _hashCode += getRefNbr().hashCode();
        }
        if (getAvailableBalance() != null) {
            _hashCode += getAvailableBalance().hashCode();
        }
        if (getAuthAmt() != null) {
            _hashCode += getAuthAmt().hashCode();
        }
        if (getAVSResultCodeAction() != null) {
            _hashCode += getAVSResultCodeAction().hashCode();
        }
        if (getCVVResultCodeAction() != null) {
            _hashCode += getCVVResultCodeAction().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getAVSRsltText() != null) {
            _hashCode += getAVSRsltText().hashCode();
        }
        if (getCVVRsltText() != null) {
            _hashCode += getCVVRsltText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthRspStatusType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSRsltCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AVSRsltCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVVRsltCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CVVRsltCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCInd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RefNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AvailableBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSResultCodeAction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AVSResultCodeAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "resultCodeActionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVVResultCodeAction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CVVResultCodeAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "resultCodeActionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSRsltText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AVSRsltText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVVRsltText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CVVRsltText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
