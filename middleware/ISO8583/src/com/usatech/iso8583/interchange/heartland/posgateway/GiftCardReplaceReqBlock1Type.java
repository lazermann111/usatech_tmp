/**
 * GiftCardReplaceReqBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class GiftCardReplaceReqBlock1Type  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType oldCardData;

    private com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType newCardData;

    public GiftCardReplaceReqBlock1Type() {
    }

    public GiftCardReplaceReqBlock1Type(
           com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType oldCardData,
           com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType newCardData) {
           this.oldCardData = oldCardData;
           this.newCardData = newCardData;
    }


    /**
     * Gets the oldCardData value for this GiftCardReplaceReqBlock1Type.
     * 
     * @return oldCardData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType getOldCardData() {
        return oldCardData;
    }


    /**
     * Sets the oldCardData value for this GiftCardReplaceReqBlock1Type.
     * 
     * @param oldCardData
     */
    public void setOldCardData(com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType oldCardData) {
        this.oldCardData = oldCardData;
    }


    /**
     * Gets the newCardData value for this GiftCardReplaceReqBlock1Type.
     * 
     * @return newCardData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType getNewCardData() {
        return newCardData;
    }


    /**
     * Sets the newCardData value for this GiftCardReplaceReqBlock1Type.
     * 
     * @param newCardData
     */
    public void setNewCardData(com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType newCardData) {
        this.newCardData = newCardData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GiftCardReplaceReqBlock1Type)) return false;
        GiftCardReplaceReqBlock1Type other = (GiftCardReplaceReqBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.oldCardData==null && other.getOldCardData()==null) || 
             (this.oldCardData!=null &&
              this.oldCardData.equals(other.getOldCardData()))) &&
            ((this.newCardData==null && other.getNewCardData()==null) || 
             (this.newCardData!=null &&
              this.newCardData.equals(other.getNewCardData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOldCardData() != null) {
            _hashCode += getOldCardData().hashCode();
        }
        if (getNewCardData() != null) {
            _hashCode += getNewCardData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GiftCardReplaceReqBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardReplaceReqBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oldCardData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "OldCardData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newCardData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "NewCardData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
