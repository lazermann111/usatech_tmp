package com.usatech.iso8583.interchange.heartland;

import java.text.ParseException;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType;
import com.usatech.iso8583.interchange.heartland.posgateway.CardDataType;
import com.usatech.iso8583.interchange.heartland.posgateway.CreditAuthReqBlock1Type;
import com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAuthReqType;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10Header;

public class AuthorizationAction extends HeartlandAction {
	private static int[] requiredFields = {
		ISO8583Message.FIELD_ACQUIRER_ID,
		ISO8583Message.FIELD_TERMINAL_ID,
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_AMOUNT,
		ISO8583Message.FIELD_POS_ENVIRONMENT, 
		ISO8583Message.FIELD_ENTRY_MODE
	};
	
	public AuthorizationAction(HeartlandInterchange interchange) {
		super(interchange);
	}
	
	protected void validateRequestSpecificFields(ISO8583Request request) throws ValidationException
	{
		validateRequestCardData(request);
	}
	
	protected void initTransaction(ISO8583Request isoRequest, PosRequestVer10Transaction transaction) throws ValidationException
	{
		PosCreditAuthReqType tranRequest = new PosCreditAuthReqType();
		CreditAuthReqBlock1Type tranBlock = new CreditAuthReqBlock1Type();
		tranBlock.setAllowDup(TRUE);
		tranBlock.setAllowPartialAuth(FALSE);
		tranBlock.setAmt(getTransactionAmount(isoRequest));
				
		CardDataType cardData = new CardDataType();
		if (isoRequest.hasTrackData())
			cardData.setTrackData(getTrackData(isoRequest));
		else if (isoRequest.hasPanData())
			cardData.setManualEntry(getManualEntry(isoRequest));
		tranBlock.setCardData(cardData);
		tranRequest.setBlock1(tranBlock);
		transaction.setCreditAuth(tranRequest);
	}

	@Override
	protected boolean parseResponse(PosResponseVer10 resV10, ISO8583Response isoResponse) throws ParseException
	{	
		PosResponseVer10Header resV10Header = resV10.getHeader();
		if (resV10Header.getSiteTrace() != null)
			isoResponse.setTraceNumber(Integer.valueOf(resV10Header.getSiteTrace()));
		if (resV10Header.getGatewayRspCode() == GW_RESPONSE_OK 
				&& resV10.getTransaction() != null 
				&& resV10.getTransaction().getCreditAuth() != null) {	
			AuthRspStatusType messageResponse = resV10.getTransaction().getCreditAuth();
			isoResponse.setResponseCode(messageResponse.getRspCode());
			if (messageResponse.getRspText() != null)
				isoResponse.setResponseMessage(messageResponse.getRspText());
			if (messageResponse.getRefNbr() != null)
				isoResponse.setRetrievalReferenceNumber(messageResponse.getRefNbr());
			if (messageResponse.getAuthCode() != null)
				isoResponse.setApprovalCode(messageResponse.getAuthCode());
			if (messageResponse.getAuthAmt() != null)
				isoResponse.setAmount(messageResponse.getAuthAmt().movePointRight(2).intValue());
		} else {
			isoResponse.setResponseCode(new StringBuilder(GW_RESPONSE_CODE_PREFIX).append(resV10Header.getGatewayRspCode()).toString());
			if (resV10Header.getGatewayRspMsg() != null)
				isoResponse.setResponseMessage(resV10Header.getGatewayRspMsg());
			isoResponse.setRetrievalReferenceNumber(String.valueOf(resV10Header.getGatewayTxnId()));
		}
		return true;
	}

	protected int[] getRequiredFields() {
		return requiredFields;
	}
}
