/**
 * CreditReturnReqBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class CreditReturnReqBlock1Type  implements java.io.Serializable {
    private java.lang.Integer gatewayTxnId;

    private com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData;

    private java.math.BigDecimal amt;

    private com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData;

    private com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType directMktData;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup;

    private com.usatech.iso8583.interchange.heartland.posgateway.ECommerceType ecommerce;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields;

    public CreditReturnReqBlock1Type() {
    }

    public CreditReturnReqBlock1Type(
           java.lang.Integer gatewayTxnId,
           com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData,
           java.math.BigDecimal amt,
           com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData,
           com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType directMktData,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup,
           com.usatech.iso8583.interchange.heartland.posgateway.ECommerceType ecommerce,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
           this.gatewayTxnId = gatewayTxnId;
           this.cardData = cardData;
           this.amt = amt;
           this.cardHolderData = cardHolderData;
           this.directMktData = directMktData;
           this.allowDup = allowDup;
           this.ecommerce = ecommerce;
           this.additionalTxnFields = additionalTxnFields;
    }


    /**
     * Gets the gatewayTxnId value for this CreditReturnReqBlock1Type.
     * 
     * @return gatewayTxnId
     */
    public java.lang.Integer getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this CreditReturnReqBlock1Type.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(java.lang.Integer gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the cardData value for this CreditReturnReqBlock1Type.
     * 
     * @return cardData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardDataType getCardData() {
        return cardData;
    }


    /**
     * Sets the cardData value for this CreditReturnReqBlock1Type.
     * 
     * @param cardData
     */
    public void setCardData(com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData) {
        this.cardData = cardData;
    }


    /**
     * Gets the amt value for this CreditReturnReqBlock1Type.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this CreditReturnReqBlock1Type.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the cardHolderData value for this CreditReturnReqBlock1Type.
     * 
     * @return cardHolderData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType getCardHolderData() {
        return cardHolderData;
    }


    /**
     * Sets the cardHolderData value for this CreditReturnReqBlock1Type.
     * 
     * @param cardHolderData
     */
    public void setCardHolderData(com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData) {
        this.cardHolderData = cardHolderData;
    }


    /**
     * Gets the directMktData value for this CreditReturnReqBlock1Type.
     * 
     * @return directMktData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType getDirectMktData() {
        return directMktData;
    }


    /**
     * Sets the directMktData value for this CreditReturnReqBlock1Type.
     * 
     * @param directMktData
     */
    public void setDirectMktData(com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType directMktData) {
        this.directMktData = directMktData;
    }


    /**
     * Gets the allowDup value for this CreditReturnReqBlock1Type.
     * 
     * @return allowDup
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getAllowDup() {
        return allowDup;
    }


    /**
     * Sets the allowDup value for this CreditReturnReqBlock1Type.
     * 
     * @param allowDup
     */
    public void setAllowDup(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup) {
        this.allowDup = allowDup;
    }


    /**
     * Gets the ecommerce value for this CreditReturnReqBlock1Type.
     * 
     * @return ecommerce
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.ECommerceType getEcommerce() {
        return ecommerce;
    }


    /**
     * Sets the ecommerce value for this CreditReturnReqBlock1Type.
     * 
     * @param ecommerce
     */
    public void setEcommerce(com.usatech.iso8583.interchange.heartland.posgateway.ECommerceType ecommerce) {
        this.ecommerce = ecommerce;
    }


    /**
     * Gets the additionalTxnFields value for this CreditReturnReqBlock1Type.
     * 
     * @return additionalTxnFields
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType getAdditionalTxnFields() {
        return additionalTxnFields;
    }


    /**
     * Sets the additionalTxnFields value for this CreditReturnReqBlock1Type.
     * 
     * @param additionalTxnFields
     */
    public void setAdditionalTxnFields(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
        this.additionalTxnFields = additionalTxnFields;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreditReturnReqBlock1Type)) return false;
        CreditReturnReqBlock1Type other = (CreditReturnReqBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.gatewayTxnId==null && other.getGatewayTxnId()==null) || 
             (this.gatewayTxnId!=null &&
              this.gatewayTxnId.equals(other.getGatewayTxnId()))) &&
            ((this.cardData==null && other.getCardData()==null) || 
             (this.cardData!=null &&
              this.cardData.equals(other.getCardData()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.cardHolderData==null && other.getCardHolderData()==null) || 
             (this.cardHolderData!=null &&
              this.cardHolderData.equals(other.getCardHolderData()))) &&
            ((this.directMktData==null && other.getDirectMktData()==null) || 
             (this.directMktData!=null &&
              this.directMktData.equals(other.getDirectMktData()))) &&
            ((this.allowDup==null && other.getAllowDup()==null) || 
             (this.allowDup!=null &&
              this.allowDup.equals(other.getAllowDup()))) &&
            ((this.ecommerce==null && other.getEcommerce()==null) || 
             (this.ecommerce!=null &&
              this.ecommerce.equals(other.getEcommerce()))) &&
            ((this.additionalTxnFields==null && other.getAdditionalTxnFields()==null) || 
             (this.additionalTxnFields!=null &&
              this.additionalTxnFields.equals(other.getAdditionalTxnFields())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGatewayTxnId() != null) {
            _hashCode += getGatewayTxnId().hashCode();
        }
        if (getCardData() != null) {
            _hashCode += getCardData().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getCardHolderData() != null) {
            _hashCode += getCardHolderData().hashCode();
        }
        if (getDirectMktData() != null) {
            _hashCode += getDirectMktData().hashCode();
        }
        if (getAllowDup() != null) {
            _hashCode += getAllowDup().hashCode();
        }
        if (getEcommerce() != null) {
            _hashCode += getEcommerce().hashCode();
        }
        if (getAdditionalTxnFields() != null) {
            _hashCode += getAdditionalTxnFields().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditReturnReqBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditReturnReqBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowDup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AllowDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ecommerce");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Ecommerce"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "eCommerceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalTxnFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFieldsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
