package com.usatech.iso8583.interchange.heartland;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.interchange.heartland.posgateway.CardDataType;
import com.usatech.iso8583.interchange.heartland.posgateway.CreditReturnReqBlock1Type;
import com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReturnReqType;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction;

public class RefundAction extends HeartlandAction {
	private static int[] requiredFields = { 
		ISO8583Message.FIELD_ACQUIRER_ID, 
		ISO8583Message.FIELD_TERMINAL_ID, 
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_AMOUNT, 
		ISO8583Message.FIELD_POS_ENVIRONMENT, 
		ISO8583Message.FIELD_ENTRY_MODE, 
		ISO8583Message.FIELD_ONLINE
	};
	
	public RefundAction(HeartlandInterchange interchange) {
		super(interchange);
	}
	
	protected void initTransaction(ISO8583Request isoRequest, PosRequestVer10Transaction transaction) throws ValidationException
	{
		PosCreditReturnReqType tranRequest = new PosCreditReturnReqType();
		CreditReturnReqBlock1Type tranBlock = new CreditReturnReqBlock1Type();
		tranBlock.setAllowDup(TRUE);
		tranBlock.setAmt(getTransactionAmount(isoRequest));
		
		String gatewayTxnId = getMiscFieldValue(isoRequest, MISC_FIELD_GATEWAY_TXN_ID);
		if (gatewayTxnId != null && gatewayTxnId.length() > 0)
			tranBlock.setGatewayTxnId(Integer.valueOf(gatewayTxnId));
		else {
			CardDataType cardData = new CardDataType();
			if (isoRequest.hasTrackData())
				cardData.setTrackData(getTrackData(isoRequest));
			else if (isoRequest.hasPanData())
				cardData.setManualEntry(getManualEntry(isoRequest));
			else
				throw new ValidationException("Required Field Not Found: " + MISC_FIELD_GATEWAY_TXN_ID
						+ " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) 
						+ " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));
			tranBlock.setCardData(cardData);
		}
		
		tranRequest.setBlock1(tranBlock);
		transaction.setCreditReturn(tranRequest);
	}

	@Override
	protected int[] getRequiredFields() {
		return requiredFields;
	}
}
