/**
 * AutoSubstantiationType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class AutoSubstantiationType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType firstAdditionalAmtInfo;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType secondAdditionalAmtInfo;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType thirdAdditionalAmtInfo;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType fourthAdditionalAmtInfo;

    private java.lang.String merchantVerificationValue;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType realTimeSubstantiation;

    public AutoSubstantiationType() {
    }

    public AutoSubstantiationType(
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType firstAdditionalAmtInfo,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType secondAdditionalAmtInfo,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType thirdAdditionalAmtInfo,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType fourthAdditionalAmtInfo,
           java.lang.String merchantVerificationValue,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType realTimeSubstantiation) {
           this.firstAdditionalAmtInfo = firstAdditionalAmtInfo;
           this.secondAdditionalAmtInfo = secondAdditionalAmtInfo;
           this.thirdAdditionalAmtInfo = thirdAdditionalAmtInfo;
           this.fourthAdditionalAmtInfo = fourthAdditionalAmtInfo;
           this.merchantVerificationValue = merchantVerificationValue;
           this.realTimeSubstantiation = realTimeSubstantiation;
    }


    /**
     * Gets the firstAdditionalAmtInfo value for this AutoSubstantiationType.
     * 
     * @return firstAdditionalAmtInfo
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType getFirstAdditionalAmtInfo() {
        return firstAdditionalAmtInfo;
    }


    /**
     * Sets the firstAdditionalAmtInfo value for this AutoSubstantiationType.
     * 
     * @param firstAdditionalAmtInfo
     */
    public void setFirstAdditionalAmtInfo(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType firstAdditionalAmtInfo) {
        this.firstAdditionalAmtInfo = firstAdditionalAmtInfo;
    }


    /**
     * Gets the secondAdditionalAmtInfo value for this AutoSubstantiationType.
     * 
     * @return secondAdditionalAmtInfo
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType getSecondAdditionalAmtInfo() {
        return secondAdditionalAmtInfo;
    }


    /**
     * Sets the secondAdditionalAmtInfo value for this AutoSubstantiationType.
     * 
     * @param secondAdditionalAmtInfo
     */
    public void setSecondAdditionalAmtInfo(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType secondAdditionalAmtInfo) {
        this.secondAdditionalAmtInfo = secondAdditionalAmtInfo;
    }


    /**
     * Gets the thirdAdditionalAmtInfo value for this AutoSubstantiationType.
     * 
     * @return thirdAdditionalAmtInfo
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType getThirdAdditionalAmtInfo() {
        return thirdAdditionalAmtInfo;
    }


    /**
     * Sets the thirdAdditionalAmtInfo value for this AutoSubstantiationType.
     * 
     * @param thirdAdditionalAmtInfo
     */
    public void setThirdAdditionalAmtInfo(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType thirdAdditionalAmtInfo) {
        this.thirdAdditionalAmtInfo = thirdAdditionalAmtInfo;
    }


    /**
     * Gets the fourthAdditionalAmtInfo value for this AutoSubstantiationType.
     * 
     * @return fourthAdditionalAmtInfo
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType getFourthAdditionalAmtInfo() {
        return fourthAdditionalAmtInfo;
    }


    /**
     * Sets the fourthAdditionalAmtInfo value for this AutoSubstantiationType.
     * 
     * @param fourthAdditionalAmtInfo
     */
    public void setFourthAdditionalAmtInfo(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType fourthAdditionalAmtInfo) {
        this.fourthAdditionalAmtInfo = fourthAdditionalAmtInfo;
    }


    /**
     * Gets the merchantVerificationValue value for this AutoSubstantiationType.
     * 
     * @return merchantVerificationValue
     */
    public java.lang.String getMerchantVerificationValue() {
        return merchantVerificationValue;
    }


    /**
     * Sets the merchantVerificationValue value for this AutoSubstantiationType.
     * 
     * @param merchantVerificationValue
     */
    public void setMerchantVerificationValue(java.lang.String merchantVerificationValue) {
        this.merchantVerificationValue = merchantVerificationValue;
    }


    /**
     * Gets the realTimeSubstantiation value for this AutoSubstantiationType.
     * 
     * @return realTimeSubstantiation
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getRealTimeSubstantiation() {
        return realTimeSubstantiation;
    }


    /**
     * Sets the realTimeSubstantiation value for this AutoSubstantiationType.
     * 
     * @param realTimeSubstantiation
     */
    public void setRealTimeSubstantiation(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType realTimeSubstantiation) {
        this.realTimeSubstantiation = realTimeSubstantiation;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AutoSubstantiationType)) return false;
        AutoSubstantiationType other = (AutoSubstantiationType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.firstAdditionalAmtInfo==null && other.getFirstAdditionalAmtInfo()==null) || 
             (this.firstAdditionalAmtInfo!=null &&
              this.firstAdditionalAmtInfo.equals(other.getFirstAdditionalAmtInfo()))) &&
            ((this.secondAdditionalAmtInfo==null && other.getSecondAdditionalAmtInfo()==null) || 
             (this.secondAdditionalAmtInfo!=null &&
              this.secondAdditionalAmtInfo.equals(other.getSecondAdditionalAmtInfo()))) &&
            ((this.thirdAdditionalAmtInfo==null && other.getThirdAdditionalAmtInfo()==null) || 
             (this.thirdAdditionalAmtInfo!=null &&
              this.thirdAdditionalAmtInfo.equals(other.getThirdAdditionalAmtInfo()))) &&
            ((this.fourthAdditionalAmtInfo==null && other.getFourthAdditionalAmtInfo()==null) || 
             (this.fourthAdditionalAmtInfo!=null &&
              this.fourthAdditionalAmtInfo.equals(other.getFourthAdditionalAmtInfo()))) &&
            ((this.merchantVerificationValue==null && other.getMerchantVerificationValue()==null) || 
             (this.merchantVerificationValue!=null &&
              this.merchantVerificationValue.equals(other.getMerchantVerificationValue()))) &&
            ((this.realTimeSubstantiation==null && other.getRealTimeSubstantiation()==null) || 
             (this.realTimeSubstantiation!=null &&
              this.realTimeSubstantiation.equals(other.getRealTimeSubstantiation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFirstAdditionalAmtInfo() != null) {
            _hashCode += getFirstAdditionalAmtInfo().hashCode();
        }
        if (getSecondAdditionalAmtInfo() != null) {
            _hashCode += getSecondAdditionalAmtInfo().hashCode();
        }
        if (getThirdAdditionalAmtInfo() != null) {
            _hashCode += getThirdAdditionalAmtInfo().hashCode();
        }
        if (getFourthAdditionalAmtInfo() != null) {
            _hashCode += getFourthAdditionalAmtInfo().hashCode();
        }
        if (getMerchantVerificationValue() != null) {
            _hashCode += getMerchantVerificationValue().hashCode();
        }
        if (getRealTimeSubstantiation() != null) {
            _hashCode += getRealTimeSubstantiation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AutoSubstantiationType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AutoSubstantiationType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstAdditionalAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "FirstAdditionalAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalAmtType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secondAdditionalAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SecondAdditionalAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalAmtType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("thirdAdditionalAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ThirdAdditionalAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalAmtType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fourthAdditionalAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "FourthAdditionalAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalAmtType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantVerificationValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchantVerificationValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("realTimeSubstantiation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RealTimeSubstantiation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
