/**
 * PosReportTxnDetailRspType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportTxnDetailRspType  implements java.io.Serializable {
    private int gatewayTxnId;

    private int siteId;

    private java.lang.String merchName;

    private int deviceId;

    private java.lang.String userName;

    private java.lang.String serviceName;

    private int gatewayRspCode;

    private java.lang.String gatewayRspMsg;

    private java.util.Calendar reqUtcDT;

    private java.util.Calendar reqDT;

    private java.util.Calendar rspUtcDT;

    private java.util.Calendar rspDT;

    private java.lang.String siteTrace;

    private int originalGatewayTxnId;

    private java.lang.String merchNbr;

    private int termOrdinal;

    private java.lang.String merchAddr1;

    private java.lang.String merchAddr2;

    private java.lang.String merchCity;

    private java.lang.String merchState;

    private java.lang.String merchZip;

    private java.lang.String merchPhone;

    private com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion;

    private com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspTypeData data;

    public PosReportTxnDetailRspType() {
    }

    public PosReportTxnDetailRspType(
           int gatewayTxnId,
           int siteId,
           java.lang.String merchName,
           int deviceId,
           java.lang.String userName,
           java.lang.String serviceName,
           int gatewayRspCode,
           java.lang.String gatewayRspMsg,
           java.util.Calendar reqUtcDT,
           java.util.Calendar reqDT,
           java.util.Calendar rspUtcDT,
           java.util.Calendar rspDT,
           java.lang.String siteTrace,
           int originalGatewayTxnId,
           java.lang.String merchNbr,
           int termOrdinal,
           java.lang.String merchAddr1,
           java.lang.String merchAddr2,
           java.lang.String merchCity,
           java.lang.String merchState,
           java.lang.String merchZip,
           java.lang.String merchPhone,
           com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion,
           com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspTypeData data) {
           this.gatewayTxnId = gatewayTxnId;
           this.siteId = siteId;
           this.merchName = merchName;
           this.deviceId = deviceId;
           this.userName = userName;
           this.serviceName = serviceName;
           this.gatewayRspCode = gatewayRspCode;
           this.gatewayRspMsg = gatewayRspMsg;
           this.reqUtcDT = reqUtcDT;
           this.reqDT = reqDT;
           this.rspUtcDT = rspUtcDT;
           this.rspDT = rspDT;
           this.siteTrace = siteTrace;
           this.originalGatewayTxnId = originalGatewayTxnId;
           this.merchNbr = merchNbr;
           this.termOrdinal = termOrdinal;
           this.merchAddr1 = merchAddr1;
           this.merchAddr2 = merchAddr2;
           this.merchCity = merchCity;
           this.merchState = merchState;
           this.merchZip = merchZip;
           this.merchPhone = merchPhone;
           this.tzConversion = tzConversion;
           this.data = data;
    }


    /**
     * Gets the gatewayTxnId value for this PosReportTxnDetailRspType.
     * 
     * @return gatewayTxnId
     */
    public int getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this PosReportTxnDetailRspType.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(int gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the siteId value for this PosReportTxnDetailRspType.
     * 
     * @return siteId
     */
    public int getSiteId() {
        return siteId;
    }


    /**
     * Sets the siteId value for this PosReportTxnDetailRspType.
     * 
     * @param siteId
     */
    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }


    /**
     * Gets the merchName value for this PosReportTxnDetailRspType.
     * 
     * @return merchName
     */
    public java.lang.String getMerchName() {
        return merchName;
    }


    /**
     * Sets the merchName value for this PosReportTxnDetailRspType.
     * 
     * @param merchName
     */
    public void setMerchName(java.lang.String merchName) {
        this.merchName = merchName;
    }


    /**
     * Gets the deviceId value for this PosReportTxnDetailRspType.
     * 
     * @return deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportTxnDetailRspType.
     * 
     * @param deviceId
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the userName value for this PosReportTxnDetailRspType.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this PosReportTxnDetailRspType.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
     * Gets the serviceName value for this PosReportTxnDetailRspType.
     * 
     * @return serviceName
     */
    public java.lang.String getServiceName() {
        return serviceName;
    }


    /**
     * Sets the serviceName value for this PosReportTxnDetailRspType.
     * 
     * @param serviceName
     */
    public void setServiceName(java.lang.String serviceName) {
        this.serviceName = serviceName;
    }


    /**
     * Gets the gatewayRspCode value for this PosReportTxnDetailRspType.
     * 
     * @return gatewayRspCode
     */
    public int getGatewayRspCode() {
        return gatewayRspCode;
    }


    /**
     * Sets the gatewayRspCode value for this PosReportTxnDetailRspType.
     * 
     * @param gatewayRspCode
     */
    public void setGatewayRspCode(int gatewayRspCode) {
        this.gatewayRspCode = gatewayRspCode;
    }


    /**
     * Gets the gatewayRspMsg value for this PosReportTxnDetailRspType.
     * 
     * @return gatewayRspMsg
     */
    public java.lang.String getGatewayRspMsg() {
        return gatewayRspMsg;
    }


    /**
     * Sets the gatewayRspMsg value for this PosReportTxnDetailRspType.
     * 
     * @param gatewayRspMsg
     */
    public void setGatewayRspMsg(java.lang.String gatewayRspMsg) {
        this.gatewayRspMsg = gatewayRspMsg;
    }


    /**
     * Gets the reqUtcDT value for this PosReportTxnDetailRspType.
     * 
     * @return reqUtcDT
     */
    public java.util.Calendar getReqUtcDT() {
        return reqUtcDT;
    }


    /**
     * Sets the reqUtcDT value for this PosReportTxnDetailRspType.
     * 
     * @param reqUtcDT
     */
    public void setReqUtcDT(java.util.Calendar reqUtcDT) {
        this.reqUtcDT = reqUtcDT;
    }


    /**
     * Gets the reqDT value for this PosReportTxnDetailRspType.
     * 
     * @return reqDT
     */
    public java.util.Calendar getReqDT() {
        return reqDT;
    }


    /**
     * Sets the reqDT value for this PosReportTxnDetailRspType.
     * 
     * @param reqDT
     */
    public void setReqDT(java.util.Calendar reqDT) {
        this.reqDT = reqDT;
    }


    /**
     * Gets the rspUtcDT value for this PosReportTxnDetailRspType.
     * 
     * @return rspUtcDT
     */
    public java.util.Calendar getRspUtcDT() {
        return rspUtcDT;
    }


    /**
     * Sets the rspUtcDT value for this PosReportTxnDetailRspType.
     * 
     * @param rspUtcDT
     */
    public void setRspUtcDT(java.util.Calendar rspUtcDT) {
        this.rspUtcDT = rspUtcDT;
    }


    /**
     * Gets the rspDT value for this PosReportTxnDetailRspType.
     * 
     * @return rspDT
     */
    public java.util.Calendar getRspDT() {
        return rspDT;
    }


    /**
     * Sets the rspDT value for this PosReportTxnDetailRspType.
     * 
     * @param rspDT
     */
    public void setRspDT(java.util.Calendar rspDT) {
        this.rspDT = rspDT;
    }


    /**
     * Gets the siteTrace value for this PosReportTxnDetailRspType.
     * 
     * @return siteTrace
     */
    public java.lang.String getSiteTrace() {
        return siteTrace;
    }


    /**
     * Sets the siteTrace value for this PosReportTxnDetailRspType.
     * 
     * @param siteTrace
     */
    public void setSiteTrace(java.lang.String siteTrace) {
        this.siteTrace = siteTrace;
    }


    /**
     * Gets the originalGatewayTxnId value for this PosReportTxnDetailRspType.
     * 
     * @return originalGatewayTxnId
     */
    public int getOriginalGatewayTxnId() {
        return originalGatewayTxnId;
    }


    /**
     * Sets the originalGatewayTxnId value for this PosReportTxnDetailRspType.
     * 
     * @param originalGatewayTxnId
     */
    public void setOriginalGatewayTxnId(int originalGatewayTxnId) {
        this.originalGatewayTxnId = originalGatewayTxnId;
    }


    /**
     * Gets the merchNbr value for this PosReportTxnDetailRspType.
     * 
     * @return merchNbr
     */
    public java.lang.String getMerchNbr() {
        return merchNbr;
    }


    /**
     * Sets the merchNbr value for this PosReportTxnDetailRspType.
     * 
     * @param merchNbr
     */
    public void setMerchNbr(java.lang.String merchNbr) {
        this.merchNbr = merchNbr;
    }


    /**
     * Gets the termOrdinal value for this PosReportTxnDetailRspType.
     * 
     * @return termOrdinal
     */
    public int getTermOrdinal() {
        return termOrdinal;
    }


    /**
     * Sets the termOrdinal value for this PosReportTxnDetailRspType.
     * 
     * @param termOrdinal
     */
    public void setTermOrdinal(int termOrdinal) {
        this.termOrdinal = termOrdinal;
    }


    /**
     * Gets the merchAddr1 value for this PosReportTxnDetailRspType.
     * 
     * @return merchAddr1
     */
    public java.lang.String getMerchAddr1() {
        return merchAddr1;
    }


    /**
     * Sets the merchAddr1 value for this PosReportTxnDetailRspType.
     * 
     * @param merchAddr1
     */
    public void setMerchAddr1(java.lang.String merchAddr1) {
        this.merchAddr1 = merchAddr1;
    }


    /**
     * Gets the merchAddr2 value for this PosReportTxnDetailRspType.
     * 
     * @return merchAddr2
     */
    public java.lang.String getMerchAddr2() {
        return merchAddr2;
    }


    /**
     * Sets the merchAddr2 value for this PosReportTxnDetailRspType.
     * 
     * @param merchAddr2
     */
    public void setMerchAddr2(java.lang.String merchAddr2) {
        this.merchAddr2 = merchAddr2;
    }


    /**
     * Gets the merchCity value for this PosReportTxnDetailRspType.
     * 
     * @return merchCity
     */
    public java.lang.String getMerchCity() {
        return merchCity;
    }


    /**
     * Sets the merchCity value for this PosReportTxnDetailRspType.
     * 
     * @param merchCity
     */
    public void setMerchCity(java.lang.String merchCity) {
        this.merchCity = merchCity;
    }


    /**
     * Gets the merchState value for this PosReportTxnDetailRspType.
     * 
     * @return merchState
     */
    public java.lang.String getMerchState() {
        return merchState;
    }


    /**
     * Sets the merchState value for this PosReportTxnDetailRspType.
     * 
     * @param merchState
     */
    public void setMerchState(java.lang.String merchState) {
        this.merchState = merchState;
    }


    /**
     * Gets the merchZip value for this PosReportTxnDetailRspType.
     * 
     * @return merchZip
     */
    public java.lang.String getMerchZip() {
        return merchZip;
    }


    /**
     * Sets the merchZip value for this PosReportTxnDetailRspType.
     * 
     * @param merchZip
     */
    public void setMerchZip(java.lang.String merchZip) {
        this.merchZip = merchZip;
    }


    /**
     * Gets the merchPhone value for this PosReportTxnDetailRspType.
     * 
     * @return merchPhone
     */
    public java.lang.String getMerchPhone() {
        return merchPhone;
    }


    /**
     * Sets the merchPhone value for this PosReportTxnDetailRspType.
     * 
     * @param merchPhone
     */
    public void setMerchPhone(java.lang.String merchPhone) {
        this.merchPhone = merchPhone;
    }


    /**
     * Gets the tzConversion value for this PosReportTxnDetailRspType.
     * 
     * @return tzConversion
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType getTzConversion() {
        return tzConversion;
    }


    /**
     * Sets the tzConversion value for this PosReportTxnDetailRspType.
     * 
     * @param tzConversion
     */
    public void setTzConversion(com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
        this.tzConversion = tzConversion;
    }


    /**
     * Gets the data value for this PosReportTxnDetailRspType.
     * 
     * @return data
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspTypeData getData() {
        return data;
    }


    /**
     * Sets the data value for this PosReportTxnDetailRspType.
     * 
     * @param data
     */
    public void setData(com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspTypeData data) {
        this.data = data;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportTxnDetailRspType)) return false;
        PosReportTxnDetailRspType other = (PosReportTxnDetailRspType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.gatewayTxnId == other.getGatewayTxnId() &&
            this.siteId == other.getSiteId() &&
            ((this.merchName==null && other.getMerchName()==null) || 
             (this.merchName!=null &&
              this.merchName.equals(other.getMerchName()))) &&
            this.deviceId == other.getDeviceId() &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.serviceName==null && other.getServiceName()==null) || 
             (this.serviceName!=null &&
              this.serviceName.equals(other.getServiceName()))) &&
            this.gatewayRspCode == other.getGatewayRspCode() &&
            ((this.gatewayRspMsg==null && other.getGatewayRspMsg()==null) || 
             (this.gatewayRspMsg!=null &&
              this.gatewayRspMsg.equals(other.getGatewayRspMsg()))) &&
            ((this.reqUtcDT==null && other.getReqUtcDT()==null) || 
             (this.reqUtcDT!=null &&
              this.reqUtcDT.equals(other.getReqUtcDT()))) &&
            ((this.reqDT==null && other.getReqDT()==null) || 
             (this.reqDT!=null &&
              this.reqDT.equals(other.getReqDT()))) &&
            ((this.rspUtcDT==null && other.getRspUtcDT()==null) || 
             (this.rspUtcDT!=null &&
              this.rspUtcDT.equals(other.getRspUtcDT()))) &&
            ((this.rspDT==null && other.getRspDT()==null) || 
             (this.rspDT!=null &&
              this.rspDT.equals(other.getRspDT()))) &&
            ((this.siteTrace==null && other.getSiteTrace()==null) || 
             (this.siteTrace!=null &&
              this.siteTrace.equals(other.getSiteTrace()))) &&
            this.originalGatewayTxnId == other.getOriginalGatewayTxnId() &&
            ((this.merchNbr==null && other.getMerchNbr()==null) || 
             (this.merchNbr!=null &&
              this.merchNbr.equals(other.getMerchNbr()))) &&
            this.termOrdinal == other.getTermOrdinal() &&
            ((this.merchAddr1==null && other.getMerchAddr1()==null) || 
             (this.merchAddr1!=null &&
              this.merchAddr1.equals(other.getMerchAddr1()))) &&
            ((this.merchAddr2==null && other.getMerchAddr2()==null) || 
             (this.merchAddr2!=null &&
              this.merchAddr2.equals(other.getMerchAddr2()))) &&
            ((this.merchCity==null && other.getMerchCity()==null) || 
             (this.merchCity!=null &&
              this.merchCity.equals(other.getMerchCity()))) &&
            ((this.merchState==null && other.getMerchState()==null) || 
             (this.merchState!=null &&
              this.merchState.equals(other.getMerchState()))) &&
            ((this.merchZip==null && other.getMerchZip()==null) || 
             (this.merchZip!=null &&
              this.merchZip.equals(other.getMerchZip()))) &&
            ((this.merchPhone==null && other.getMerchPhone()==null) || 
             (this.merchPhone!=null &&
              this.merchPhone.equals(other.getMerchPhone()))) &&
            ((this.tzConversion==null && other.getTzConversion()==null) || 
             (this.tzConversion!=null &&
              this.tzConversion.equals(other.getTzConversion()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGatewayTxnId();
        _hashCode += getSiteId();
        if (getMerchName() != null) {
            _hashCode += getMerchName().hashCode();
        }
        _hashCode += getDeviceId();
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getServiceName() != null) {
            _hashCode += getServiceName().hashCode();
        }
        _hashCode += getGatewayRspCode();
        if (getGatewayRspMsg() != null) {
            _hashCode += getGatewayRspMsg().hashCode();
        }
        if (getReqUtcDT() != null) {
            _hashCode += getReqUtcDT().hashCode();
        }
        if (getReqDT() != null) {
            _hashCode += getReqDT().hashCode();
        }
        if (getRspUtcDT() != null) {
            _hashCode += getRspUtcDT().hashCode();
        }
        if (getRspDT() != null) {
            _hashCode += getRspDT().hashCode();
        }
        if (getSiteTrace() != null) {
            _hashCode += getSiteTrace().hashCode();
        }
        _hashCode += getOriginalGatewayTxnId();
        if (getMerchNbr() != null) {
            _hashCode += getMerchNbr().hashCode();
        }
        _hashCode += getTermOrdinal();
        if (getMerchAddr1() != null) {
            _hashCode += getMerchAddr1().hashCode();
        }
        if (getMerchAddr2() != null) {
            _hashCode += getMerchAddr2().hashCode();
        }
        if (getMerchCity() != null) {
            _hashCode += getMerchCity().hashCode();
        }
        if (getMerchState() != null) {
            _hashCode += getMerchState().hashCode();
        }
        if (getMerchZip() != null) {
            _hashCode += getMerchZip().hashCode();
        }
        if (getMerchPhone() != null) {
            _hashCode += getMerchPhone().hashCode();
        }
        if (getTzConversion() != null) {
            _hashCode += getTzConversion().hashCode();
        }
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportTxnDetailRspType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportTxnDetailRspType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "UserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ServiceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayRspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayRspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayRspMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayRspMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reqUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReqUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reqDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReqDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteTrace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteTrace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalGatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "OriginalGatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("termOrdinal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TermOrdinal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchAddr1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchAddr1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchAddr2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchAddr2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchZip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchZip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tzConversion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TzConversion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "tzoneConversionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportTxnDetailRspType>Data"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
