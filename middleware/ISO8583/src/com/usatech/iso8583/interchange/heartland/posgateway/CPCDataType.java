/**
 * CPCDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class CPCDataType  implements java.io.Serializable {
    private java.lang.String cardHolderPONbr;

    private com.usatech.iso8583.interchange.heartland.posgateway.TaxTypeType taxType;

    private java.math.BigDecimal taxAmt;

    public CPCDataType() {
    }

    public CPCDataType(
           java.lang.String cardHolderPONbr,
           com.usatech.iso8583.interchange.heartland.posgateway.TaxTypeType taxType,
           java.math.BigDecimal taxAmt) {
           this.cardHolderPONbr = cardHolderPONbr;
           this.taxType = taxType;
           this.taxAmt = taxAmt;
    }


    /**
     * Gets the cardHolderPONbr value for this CPCDataType.
     * 
     * @return cardHolderPONbr
     */
    public java.lang.String getCardHolderPONbr() {
        return cardHolderPONbr;
    }


    /**
     * Sets the cardHolderPONbr value for this CPCDataType.
     * 
     * @param cardHolderPONbr
     */
    public void setCardHolderPONbr(java.lang.String cardHolderPONbr) {
        this.cardHolderPONbr = cardHolderPONbr;
    }


    /**
     * Gets the taxType value for this CPCDataType.
     * 
     * @return taxType
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.TaxTypeType getTaxType() {
        return taxType;
    }


    /**
     * Sets the taxType value for this CPCDataType.
     * 
     * @param taxType
     */
    public void setTaxType(com.usatech.iso8583.interchange.heartland.posgateway.TaxTypeType taxType) {
        this.taxType = taxType;
    }


    /**
     * Gets the taxAmt value for this CPCDataType.
     * 
     * @return taxAmt
     */
    public java.math.BigDecimal getTaxAmt() {
        return taxAmt;
    }


    /**
     * Sets the taxAmt value for this CPCDataType.
     * 
     * @param taxAmt
     */
    public void setTaxAmt(java.math.BigDecimal taxAmt) {
        this.taxAmt = taxAmt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CPCDataType)) return false;
        CPCDataType other = (CPCDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardHolderPONbr==null && other.getCardHolderPONbr()==null) || 
             (this.cardHolderPONbr!=null &&
              this.cardHolderPONbr.equals(other.getCardHolderPONbr()))) &&
            ((this.taxType==null && other.getTaxType()==null) || 
             (this.taxType!=null &&
              this.taxType.equals(other.getTaxType()))) &&
            ((this.taxAmt==null && other.getTaxAmt()==null) || 
             (this.taxAmt!=null &&
              this.taxAmt.equals(other.getTaxAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardHolderPONbr() != null) {
            _hashCode += getCardHolderPONbr().hashCode();
        }
        if (getTaxType() != null) {
            _hashCode += getTaxType().hashCode();
        }
        if (getTaxAmt() != null) {
            _hashCode += getTaxAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CPCDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderPONbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderPONbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TaxType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "taxTypeType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TaxAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
