package com.usatech.iso8583.interchange.heartland;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.heartland.posgateway.BooleanType;
import com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeManualEntry;
import com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeTrackData;
import com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeTrackDataMethod;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10;
import com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10Header;
import com.usatech.iso8583.util.CreditCardValidator;

public abstract class HeartlandAction {
	private HeartlandInterchange interchange;
	
	public static final String GW_RESPONSE_CODE_PREFIX = "GW";
	public static final char FS = 0x1C;
	public static final BooleanType TRUE = BooleanType.fromValue("Y");
	public static final BooleanType FALSE = BooleanType.fromValue("N");
	
	public static final String MISC_FIELD_GATEWAY_TXN_ID = "gatewayTxnId";
	
	public static final int GW_RESPONSE_POS_GATEWAY_ERROR = -1;
	public static final int GW_RESPONSE_AUTHENTICATION_ERROR = -2;
	public static final int GW_RESPONSE_UNAUTHORIZED = -21;
	public static final int GW_RESPONSE_OK = 0;
	public static final int GW_RESPONSE_GATEWAY_SYSTEM_ERROR = 1;
	public static final int GW_RESPONSE_DUPLICATE_TRAN = 2;
	public static final int GW_RESPONSE_INVALID_ORIGINAL_TRAN = 3;
	public static final int GW_RESPONSE_TRAN_ALREADY_IN_BATCH = 4;
	public static final int GW_RESPONSE_NO_CURRENT_BATCH = 5;
	public static final int GW_RESPONSE_TRAN_NOT_IN_BATCH = 6;
	public static final int GW_RESPONSE_INVALID_REPORT_PARAMS = 7;
	public static final int GW_RESPONSE_BAD_TRACK_DATA = 8;
	public static final int GW_RESPONSE_NO_TRAN_IN_BATCH = 9;
	public static final int GW_RESPONSE_EMPTY_REPORT = 10;
	public static final int GW_RESPONSE_ORIGINAL_TRAN_NOT_CPC = 11;
	public static final int GW_RESPONSE_INVALID_CPC_DATA = 12;
	public static final int GW_RESPONSE_INVALID_EDIT_DATA = 13;
	public static final int GW_RESPONSE_INVALID_CARD_NUMBER = 14;
	public static final int GW_RESPONSE_BATCH_CLOSE_IN_PROGRESS = 15;
	public static final int GW_RESPONSE_INVALID_SHIP_DATE = 16;
	public static final int GW_RESPONSE_INVALID_ENCRYPTION_VERSION = 17;
	public static final int GW_RESPONSE_E3_MSR_FAILURE = 18;
	public static final int GW_RESPONSE_INVALID_REVERSAL_AMOUNT = 19;
	
	public static final String ISSUER_RESPONSE_NO_ACTION_TAKEN = "76";
	public static final String ISSUER_RESPONSE_NO_ACTION_TAKEN_DUPLICATE = "77";
	
	protected HeartlandAction(HeartlandInterchange interchange)
	{
		this.interchange = interchange;
	}
	
	abstract protected int[] getRequiredFields();
	abstract protected void initTransaction(ISO8583Request isoRequest, PosRequestVer10Transaction transaction) throws ValidationException;

	public HeartlandInterchange getInterchange() {
		return interchange;
	}
	
	public BigDecimal getTransactionAmount(final ISO8583Request isoRequest)
	{
		return BigDecimal.valueOf(isoRequest.getAmount()).movePointLeft(2);
	}
	
	public BigDecimal getOriginalAmount(final ISO8583Request isoRequest)
	{
		return BigDecimal.valueOf(isoRequest.getOriginalAmount()).movePointLeft(2);
	}
	
	public String getExpirationDate(final ISO8583Request isoRequest)
	{
		return isoRequest.getPanData().getExpiration().trim();
	}
	
	public int getExpirationMonth(final ISO8583Request isoRequest)
	{
		return Integer.valueOf(getExpirationDate(isoRequest).substring(2, 4));
	}
	
	public int getExpirationYear(final ISO8583Request isoRequest)
	{
		int twoDigitExpYear = Integer.valueOf(getExpirationDate(isoRequest).substring(0, 2));
		Calendar calendar = Calendar.getInstance();
		int curYear = calendar.get(Calendar.YEAR);
		int twoDigitCurYear = curYear % 100;
		int expYear = curYear - twoDigitCurYear + twoDigitExpYear;
		if (twoDigitExpYear - twoDigitCurYear > 50)
			expYear -= 100;
		return expYear;
	}
	
	public CardDataTypeTrackData getTrackData(final ISO8583Request isoRequest)
	{
		CardDataTypeTrackData trackData = new CardDataTypeTrackData();
		StringBuilder sb = new StringBuilder();
		// Heartland POS Gateway requires the track start and end sentinels
		if (isoRequest.getTrackData().hasTrack2()) {
			sb.append(isoRequest.getTrackData().getTrack2());
			if (sb.charAt(0) != ';')
				sb.insert(0, ';');
		} else if (isoRequest.getTrackData().hasTrack1()) {
			sb.append(isoRequest.getTrackData().getTrack1());
			if (sb.charAt(0) != '%')
				sb.insert(0, '%');
		}
		if (sb.charAt(sb.length() - 1) != '?')
			sb.append('?');
		trackData.set_value(sb.toString());
		if (isoRequest.getEntryMode().equalsIgnoreCase(ISO8583Message.ENTRY_MODE_CONTACTLESS_SWIPED))
			trackData.setMethod(CardDataTypeTrackDataMethod.proximity);
		else
			trackData.setMethod(CardDataTypeTrackDataMethod.swipe);
		return trackData;
	}
	
	public CardDataTypeManualEntry getManualEntry(final ISO8583Request isoRequest)
	{
		CardDataTypeManualEntry manualEntry = new CardDataTypeManualEntry();
		manualEntry.setCardPresent(FALSE);
		manualEntry.setReaderPresent(FALSE);
		manualEntry.setCardNbr(isoRequest.getPanData().getPan());			
		manualEntry.setExpMonth(getExpirationMonth(isoRequest));
		manualEntry.setExpYear(getExpirationYear(isoRequest));
		return manualEntry;		
	}
	
	public String getMiscFieldValue(final ISO8583Request isoRequest, String miscField)
	{
		if (isoRequest.hasMiscData()) { 
			String miscData = isoRequest.getMiscData();
			StringBuilder sb = new StringBuilder(FS).append(miscField).append('=');
			int fromIndex = miscData.indexOf(sb.toString());
			if (fromIndex > -1)
				return miscData.substring(miscData.indexOf('=', fromIndex) + 1, miscData.indexOf(FS, fromIndex));
		}
		return null;
	}
	
	public void validateRequest(ISO8583Request request) throws ValidationException
	{
		int[] requiredFields = getRequiredFields();
		if (requiredFields != null)
		{
			for (int i = 0; i < requiredFields.length; i++) {
				if (!request.hasField(requiredFields[i])){
					throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(requiredFields[i]));
				}
			}
		}

		if (request.hasTrackData()) {
			if (!request.getTrackData().hasTrack1() && !request.getTrackData().hasTrack2()){
				throw new ValidationException("Required Field Not Found: TrackData Tracks");
			}
		}

		if (request.hasPanData()) {
			if (request.getPanData().getPan() == null){
				throw new ValidationException("Required Field Not Found: PanData PAN");
			}
			
			if (request.getPanData().getExpiration() == null)
				throw new ValidationException("Required Field Not Found: PanData Expiration");

			try
			{
				CreditCardValidator.validatePanData(request.getPanData());
			}
			catch (NumberFormatException e)
			{
				throw new ValidationException("Invalid Pan Data: " + e.getMessage());
			}			
		}
		
		validateRequestSpecificFields(request);
	}
	
	protected void validateRequestCardData(ISO8583Request request) throws ValidationException
	{
		if (!request.hasPanData() && !request.hasTrackData())
			throw new ValidationException("Required Field Not Found: " 
					+ ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) 
					+ " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));
	}
	
	protected void validateRequestSpecificFields(ISO8583Request request) throws ValidationException
	{
		
	}
	
	protected boolean parseResponse(PosResponseVer10 resV10, ISO8583Response isoResponse) throws ParseException
	{
		PosResponseVer10Header resV10Header = resV10.getHeader();
		if (resV10Header.getSiteTrace() != null)
			isoResponse.setTraceNumber(Integer.valueOf(resV10Header.getSiteTrace()));
		// we have to treat certain response codes triggered by retransmissions due to timeouts as success...
		switch (resV10Header.getGatewayRspCode()) {
			case GW_RESPONSE_OK:
			case GW_RESPONSE_TRAN_ALREADY_IN_BATCH:
				isoResponse.setResponseCode(ISO8583Response.SUCCESS);
				break;
			default:
				isoResponse.setResponseCode(new StringBuilder(GW_RESPONSE_CODE_PREFIX).append(resV10Header.getGatewayRspCode()).toString());
		}
		if (resV10Header.getGatewayRspMsg() != null)
			isoResponse.setResponseMessage(resV10Header.getGatewayRspMsg());
		isoResponse.setRetrievalReferenceNumber(String.valueOf(resV10Header.getGatewayTxnId()));
		return true;
	}
	
	protected boolean processResponse(ISO8583Request isoRequest, ISO8583Response isoResponse, PosResponseVer10 resV10) throws ParseException
	{
		PosResponseVer10Header resV10Header = resV10.getHeader();
		boolean success = parseResponse(resV10, isoResponse);
		
		StringBuilder sb = new StringBuilder(FS)
			.append(MISC_FIELD_GATEWAY_TXN_ID).append('=').append(resV10Header.getGatewayTxnId()).append(FS);
		isoResponse.setMiscData(sb.toString());
		
		return success;
	}	
}
