/**
 * DirectMktDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class DirectMktDataType  implements java.io.Serializable {
    private java.lang.String directMktInvoiceNbr;

    private int directMktShipMonth;

    private int directMktShipDay;

    public DirectMktDataType() {
    }

    public DirectMktDataType(
           java.lang.String directMktInvoiceNbr,
           int directMktShipMonth,
           int directMktShipDay) {
           this.directMktInvoiceNbr = directMktInvoiceNbr;
           this.directMktShipMonth = directMktShipMonth;
           this.directMktShipDay = directMktShipDay;
    }


    /**
     * Gets the directMktInvoiceNbr value for this DirectMktDataType.
     * 
     * @return directMktInvoiceNbr
     */
    public java.lang.String getDirectMktInvoiceNbr() {
        return directMktInvoiceNbr;
    }


    /**
     * Sets the directMktInvoiceNbr value for this DirectMktDataType.
     * 
     * @param directMktInvoiceNbr
     */
    public void setDirectMktInvoiceNbr(java.lang.String directMktInvoiceNbr) {
        this.directMktInvoiceNbr = directMktInvoiceNbr;
    }


    /**
     * Gets the directMktShipMonth value for this DirectMktDataType.
     * 
     * @return directMktShipMonth
     */
    public int getDirectMktShipMonth() {
        return directMktShipMonth;
    }


    /**
     * Sets the directMktShipMonth value for this DirectMktDataType.
     * 
     * @param directMktShipMonth
     */
    public void setDirectMktShipMonth(int directMktShipMonth) {
        this.directMktShipMonth = directMktShipMonth;
    }


    /**
     * Gets the directMktShipDay value for this DirectMktDataType.
     * 
     * @return directMktShipDay
     */
    public int getDirectMktShipDay() {
        return directMktShipDay;
    }


    /**
     * Sets the directMktShipDay value for this DirectMktDataType.
     * 
     * @param directMktShipDay
     */
    public void setDirectMktShipDay(int directMktShipDay) {
        this.directMktShipDay = directMktShipDay;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DirectMktDataType)) return false;
        DirectMktDataType other = (DirectMktDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.directMktInvoiceNbr==null && other.getDirectMktInvoiceNbr()==null) || 
             (this.directMktInvoiceNbr!=null &&
              this.directMktInvoiceNbr.equals(other.getDirectMktInvoiceNbr()))) &&
            this.directMktShipMonth == other.getDirectMktShipMonth() &&
            this.directMktShipDay == other.getDirectMktShipDay();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDirectMktInvoiceNbr() != null) {
            _hashCode += getDirectMktInvoiceNbr().hashCode();
        }
        _hashCode += getDirectMktShipMonth();
        _hashCode += getDirectMktShipDay();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DirectMktDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktInvoiceNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktInvoiceNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktShipMonth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktShipMonth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktShipDay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktShipDay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
