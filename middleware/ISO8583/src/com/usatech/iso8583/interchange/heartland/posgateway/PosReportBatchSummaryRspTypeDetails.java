/**
 * PosReportBatchSummaryRspTypeDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportBatchSummaryRspTypeDetails  implements java.io.Serializable {
    private java.lang.String cardType;

    private int creditCnt;

    private java.math.BigDecimal creditAmt;

    private int debitCnt;

    private java.math.BigDecimal debitAmt;

    private int saleCnt;

    private java.math.BigDecimal saleAmt;

    private int returnCnt;

    private java.math.BigDecimal returnAmt;

    private int totalCnt;

    private java.math.BigDecimal totalAmt;

    private java.math.BigDecimal totalGratuityAmtInfo;

    public PosReportBatchSummaryRspTypeDetails() {
    }

    public PosReportBatchSummaryRspTypeDetails(
           java.lang.String cardType,
           int creditCnt,
           java.math.BigDecimal creditAmt,
           int debitCnt,
           java.math.BigDecimal debitAmt,
           int saleCnt,
           java.math.BigDecimal saleAmt,
           int returnCnt,
           java.math.BigDecimal returnAmt,
           int totalCnt,
           java.math.BigDecimal totalAmt,
           java.math.BigDecimal totalGratuityAmtInfo) {
           this.cardType = cardType;
           this.creditCnt = creditCnt;
           this.creditAmt = creditAmt;
           this.debitCnt = debitCnt;
           this.debitAmt = debitAmt;
           this.saleCnt = saleCnt;
           this.saleAmt = saleAmt;
           this.returnCnt = returnCnt;
           this.returnAmt = returnAmt;
           this.totalCnt = totalCnt;
           this.totalAmt = totalAmt;
           this.totalGratuityAmtInfo = totalGratuityAmtInfo;
    }


    /**
     * Gets the cardType value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the creditCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return creditCnt
     */
    public int getCreditCnt() {
        return creditCnt;
    }


    /**
     * Sets the creditCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param creditCnt
     */
    public void setCreditCnt(int creditCnt) {
        this.creditCnt = creditCnt;
    }


    /**
     * Gets the creditAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return creditAmt
     */
    public java.math.BigDecimal getCreditAmt() {
        return creditAmt;
    }


    /**
     * Sets the creditAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param creditAmt
     */
    public void setCreditAmt(java.math.BigDecimal creditAmt) {
        this.creditAmt = creditAmt;
    }


    /**
     * Gets the debitCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return debitCnt
     */
    public int getDebitCnt() {
        return debitCnt;
    }


    /**
     * Sets the debitCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param debitCnt
     */
    public void setDebitCnt(int debitCnt) {
        this.debitCnt = debitCnt;
    }


    /**
     * Gets the debitAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return debitAmt
     */
    public java.math.BigDecimal getDebitAmt() {
        return debitAmt;
    }


    /**
     * Sets the debitAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param debitAmt
     */
    public void setDebitAmt(java.math.BigDecimal debitAmt) {
        this.debitAmt = debitAmt;
    }


    /**
     * Gets the saleCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return saleCnt
     */
    public int getSaleCnt() {
        return saleCnt;
    }


    /**
     * Sets the saleCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param saleCnt
     */
    public void setSaleCnt(int saleCnt) {
        this.saleCnt = saleCnt;
    }


    /**
     * Gets the saleAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return saleAmt
     */
    public java.math.BigDecimal getSaleAmt() {
        return saleAmt;
    }


    /**
     * Sets the saleAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param saleAmt
     */
    public void setSaleAmt(java.math.BigDecimal saleAmt) {
        this.saleAmt = saleAmt;
    }


    /**
     * Gets the returnCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return returnCnt
     */
    public int getReturnCnt() {
        return returnCnt;
    }


    /**
     * Sets the returnCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param returnCnt
     */
    public void setReturnCnt(int returnCnt) {
        this.returnCnt = returnCnt;
    }


    /**
     * Gets the returnAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return returnAmt
     */
    public java.math.BigDecimal getReturnAmt() {
        return returnAmt;
    }


    /**
     * Sets the returnAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param returnAmt
     */
    public void setReturnAmt(java.math.BigDecimal returnAmt) {
        this.returnAmt = returnAmt;
    }


    /**
     * Gets the totalCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return totalCnt
     */
    public int getTotalCnt() {
        return totalCnt;
    }


    /**
     * Sets the totalCnt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param totalCnt
     */
    public void setTotalCnt(int totalCnt) {
        this.totalCnt = totalCnt;
    }


    /**
     * Gets the totalAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return totalAmt
     */
    public java.math.BigDecimal getTotalAmt() {
        return totalAmt;
    }


    /**
     * Sets the totalAmt value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param totalAmt
     */
    public void setTotalAmt(java.math.BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }


    /**
     * Gets the totalGratuityAmtInfo value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @return totalGratuityAmtInfo
     */
    public java.math.BigDecimal getTotalGratuityAmtInfo() {
        return totalGratuityAmtInfo;
    }


    /**
     * Sets the totalGratuityAmtInfo value for this PosReportBatchSummaryRspTypeDetails.
     * 
     * @param totalGratuityAmtInfo
     */
    public void setTotalGratuityAmtInfo(java.math.BigDecimal totalGratuityAmtInfo) {
        this.totalGratuityAmtInfo = totalGratuityAmtInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportBatchSummaryRspTypeDetails)) return false;
        PosReportBatchSummaryRspTypeDetails other = (PosReportBatchSummaryRspTypeDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            this.creditCnt == other.getCreditCnt() &&
            ((this.creditAmt==null && other.getCreditAmt()==null) || 
             (this.creditAmt!=null &&
              this.creditAmt.equals(other.getCreditAmt()))) &&
            this.debitCnt == other.getDebitCnt() &&
            ((this.debitAmt==null && other.getDebitAmt()==null) || 
             (this.debitAmt!=null &&
              this.debitAmt.equals(other.getDebitAmt()))) &&
            this.saleCnt == other.getSaleCnt() &&
            ((this.saleAmt==null && other.getSaleAmt()==null) || 
             (this.saleAmt!=null &&
              this.saleAmt.equals(other.getSaleAmt()))) &&
            this.returnCnt == other.getReturnCnt() &&
            ((this.returnAmt==null && other.getReturnAmt()==null) || 
             (this.returnAmt!=null &&
              this.returnAmt.equals(other.getReturnAmt()))) &&
            this.totalCnt == other.getTotalCnt() &&
            ((this.totalAmt==null && other.getTotalAmt()==null) || 
             (this.totalAmt!=null &&
              this.totalAmt.equals(other.getTotalAmt()))) &&
            ((this.totalGratuityAmtInfo==null && other.getTotalGratuityAmtInfo()==null) || 
             (this.totalGratuityAmtInfo!=null &&
              this.totalGratuityAmtInfo.equals(other.getTotalGratuityAmtInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        _hashCode += getCreditCnt();
        if (getCreditAmt() != null) {
            _hashCode += getCreditAmt().hashCode();
        }
        _hashCode += getDebitCnt();
        if (getDebitAmt() != null) {
            _hashCode += getDebitAmt().hashCode();
        }
        _hashCode += getSaleCnt();
        if (getSaleAmt() != null) {
            _hashCode += getSaleAmt().hashCode();
        }
        _hashCode += getReturnCnt();
        if (getReturnAmt() != null) {
            _hashCode += getReturnAmt().hashCode();
        }
        _hashCode += getTotalCnt();
        if (getTotalAmt() != null) {
            _hashCode += getTotalAmt().hashCode();
        }
        if (getTotalGratuityAmtInfo() != null) {
            _hashCode += getTotalGratuityAmtInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportBatchSummaryRspTypeDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchSummaryRspType>Details"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReturnCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReturnAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TotalCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TotalAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalGratuityAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TotalGratuityAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
