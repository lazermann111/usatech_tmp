/**
 * PosReportSearchReqType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportSearchReqType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType returnHeaderOnly;

    private java.util.Calendar rptStartUtcDT;

    private java.util.Calendar rptEndUtcDT;

    private java.lang.Integer deviceId;

    private com.usatech.iso8583.interchange.heartland.posgateway.ReportSearchCriteriaType criteria;

    private com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion;

    public PosReportSearchReqType() {
    }

    public PosReportSearchReqType(
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType returnHeaderOnly,
           java.util.Calendar rptStartUtcDT,
           java.util.Calendar rptEndUtcDT,
           java.lang.Integer deviceId,
           com.usatech.iso8583.interchange.heartland.posgateway.ReportSearchCriteriaType criteria,
           com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
           this.returnHeaderOnly = returnHeaderOnly;
           this.rptStartUtcDT = rptStartUtcDT;
           this.rptEndUtcDT = rptEndUtcDT;
           this.deviceId = deviceId;
           this.criteria = criteria;
           this.tzConversion = tzConversion;
    }


    /**
     * Gets the returnHeaderOnly value for this PosReportSearchReqType.
     * 
     * @return returnHeaderOnly
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getReturnHeaderOnly() {
        return returnHeaderOnly;
    }


    /**
     * Sets the returnHeaderOnly value for this PosReportSearchReqType.
     * 
     * @param returnHeaderOnly
     */
    public void setReturnHeaderOnly(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType returnHeaderOnly) {
        this.returnHeaderOnly = returnHeaderOnly;
    }


    /**
     * Gets the rptStartUtcDT value for this PosReportSearchReqType.
     * 
     * @return rptStartUtcDT
     */
    public java.util.Calendar getRptStartUtcDT() {
        return rptStartUtcDT;
    }


    /**
     * Sets the rptStartUtcDT value for this PosReportSearchReqType.
     * 
     * @param rptStartUtcDT
     */
    public void setRptStartUtcDT(java.util.Calendar rptStartUtcDT) {
        this.rptStartUtcDT = rptStartUtcDT;
    }


    /**
     * Gets the rptEndUtcDT value for this PosReportSearchReqType.
     * 
     * @return rptEndUtcDT
     */
    public java.util.Calendar getRptEndUtcDT() {
        return rptEndUtcDT;
    }


    /**
     * Sets the rptEndUtcDT value for this PosReportSearchReqType.
     * 
     * @param rptEndUtcDT
     */
    public void setRptEndUtcDT(java.util.Calendar rptEndUtcDT) {
        this.rptEndUtcDT = rptEndUtcDT;
    }


    /**
     * Gets the deviceId value for this PosReportSearchReqType.
     * 
     * @return deviceId
     */
    public java.lang.Integer getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportSearchReqType.
     * 
     * @param deviceId
     */
    public void setDeviceId(java.lang.Integer deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the criteria value for this PosReportSearchReqType.
     * 
     * @return criteria
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.ReportSearchCriteriaType getCriteria() {
        return criteria;
    }


    /**
     * Sets the criteria value for this PosReportSearchReqType.
     * 
     * @param criteria
     */
    public void setCriteria(com.usatech.iso8583.interchange.heartland.posgateway.ReportSearchCriteriaType criteria) {
        this.criteria = criteria;
    }


    /**
     * Gets the tzConversion value for this PosReportSearchReqType.
     * 
     * @return tzConversion
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType getTzConversion() {
        return tzConversion;
    }


    /**
     * Sets the tzConversion value for this PosReportSearchReqType.
     * 
     * @param tzConversion
     */
    public void setTzConversion(com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
        this.tzConversion = tzConversion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportSearchReqType)) return false;
        PosReportSearchReqType other = (PosReportSearchReqType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.returnHeaderOnly==null && other.getReturnHeaderOnly()==null) || 
             (this.returnHeaderOnly!=null &&
              this.returnHeaderOnly.equals(other.getReturnHeaderOnly()))) &&
            ((this.rptStartUtcDT==null && other.getRptStartUtcDT()==null) || 
             (this.rptStartUtcDT!=null &&
              this.rptStartUtcDT.equals(other.getRptStartUtcDT()))) &&
            ((this.rptEndUtcDT==null && other.getRptEndUtcDT()==null) || 
             (this.rptEndUtcDT!=null &&
              this.rptEndUtcDT.equals(other.getRptEndUtcDT()))) &&
            ((this.deviceId==null && other.getDeviceId()==null) || 
             (this.deviceId!=null &&
              this.deviceId.equals(other.getDeviceId()))) &&
            ((this.criteria==null && other.getCriteria()==null) || 
             (this.criteria!=null &&
              this.criteria.equals(other.getCriteria()))) &&
            ((this.tzConversion==null && other.getTzConversion()==null) || 
             (this.tzConversion!=null &&
              this.tzConversion.equals(other.getTzConversion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReturnHeaderOnly() != null) {
            _hashCode += getReturnHeaderOnly().hashCode();
        }
        if (getRptStartUtcDT() != null) {
            _hashCode += getRptStartUtcDT().hashCode();
        }
        if (getRptEndUtcDT() != null) {
            _hashCode += getRptEndUtcDT().hashCode();
        }
        if (getDeviceId() != null) {
            _hashCode += getDeviceId().hashCode();
        }
        if (getCriteria() != null) {
            _hashCode += getCriteria().hashCode();
        }
        if (getTzConversion() != null) {
            _hashCode += getTzConversion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportSearchReqType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportSearchReqType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnHeaderOnly");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReturnHeaderOnly"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rptStartUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RptStartUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rptEndUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RptEndUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criteria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Criteria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportSearchCriteriaType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tzConversion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TzConversion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "tzoneConversionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
