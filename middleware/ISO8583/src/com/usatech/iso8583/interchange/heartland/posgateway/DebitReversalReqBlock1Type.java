/**
 * DebitReversalReqBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class DebitReversalReqBlock1Type  implements java.io.Serializable {
    private java.lang.Integer gatewayTxnId;

    private java.lang.String trackData;

    private java.math.BigDecimal amt;

    private java.math.BigDecimal authAmt;

    private com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields;

    public DebitReversalReqBlock1Type() {
    }

    public DebitReversalReqBlock1Type(
           java.lang.Integer gatewayTxnId,
           java.lang.String trackData,
           java.math.BigDecimal amt,
           java.math.BigDecimal authAmt,
           com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
           this.gatewayTxnId = gatewayTxnId;
           this.trackData = trackData;
           this.amt = amt;
           this.authAmt = authAmt;
           this.encryptionData = encryptionData;
           this.additionalTxnFields = additionalTxnFields;
    }


    /**
     * Gets the gatewayTxnId value for this DebitReversalReqBlock1Type.
     * 
     * @return gatewayTxnId
     */
    public java.lang.Integer getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this DebitReversalReqBlock1Type.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(java.lang.Integer gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the trackData value for this DebitReversalReqBlock1Type.
     * 
     * @return trackData
     */
    public java.lang.String getTrackData() {
        return trackData;
    }


    /**
     * Sets the trackData value for this DebitReversalReqBlock1Type.
     * 
     * @param trackData
     */
    public void setTrackData(java.lang.String trackData) {
        this.trackData = trackData;
    }


    /**
     * Gets the amt value for this DebitReversalReqBlock1Type.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this DebitReversalReqBlock1Type.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the authAmt value for this DebitReversalReqBlock1Type.
     * 
     * @return authAmt
     */
    public java.math.BigDecimal getAuthAmt() {
        return authAmt;
    }


    /**
     * Sets the authAmt value for this DebitReversalReqBlock1Type.
     * 
     * @param authAmt
     */
    public void setAuthAmt(java.math.BigDecimal authAmt) {
        this.authAmt = authAmt;
    }


    /**
     * Gets the encryptionData value for this DebitReversalReqBlock1Type.
     * 
     * @return encryptionData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType getEncryptionData() {
        return encryptionData;
    }


    /**
     * Sets the encryptionData value for this DebitReversalReqBlock1Type.
     * 
     * @param encryptionData
     */
    public void setEncryptionData(com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType encryptionData) {
        this.encryptionData = encryptionData;
    }


    /**
     * Gets the additionalTxnFields value for this DebitReversalReqBlock1Type.
     * 
     * @return additionalTxnFields
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType getAdditionalTxnFields() {
        return additionalTxnFields;
    }


    /**
     * Sets the additionalTxnFields value for this DebitReversalReqBlock1Type.
     * 
     * @param additionalTxnFields
     */
    public void setAdditionalTxnFields(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
        this.additionalTxnFields = additionalTxnFields;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DebitReversalReqBlock1Type)) return false;
        DebitReversalReqBlock1Type other = (DebitReversalReqBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.gatewayTxnId==null && other.getGatewayTxnId()==null) || 
             (this.gatewayTxnId!=null &&
              this.gatewayTxnId.equals(other.getGatewayTxnId()))) &&
            ((this.trackData==null && other.getTrackData()==null) || 
             (this.trackData!=null &&
              this.trackData.equals(other.getTrackData()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.authAmt==null && other.getAuthAmt()==null) || 
             (this.authAmt!=null &&
              this.authAmt.equals(other.getAuthAmt()))) &&
            ((this.encryptionData==null && other.getEncryptionData()==null) || 
             (this.encryptionData!=null &&
              this.encryptionData.equals(other.getEncryptionData()))) &&
            ((this.additionalTxnFields==null && other.getAdditionalTxnFields()==null) || 
             (this.additionalTxnFields!=null &&
              this.additionalTxnFields.equals(other.getAdditionalTxnFields())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGatewayTxnId() != null) {
            _hashCode += getGatewayTxnId().hashCode();
        }
        if (getTrackData() != null) {
            _hashCode += getTrackData().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getAuthAmt() != null) {
            _hashCode += getAuthAmt().hashCode();
        }
        if (getEncryptionData() != null) {
            _hashCode += getEncryptionData().hashCode();
        }
        if (getAdditionalTxnFields() != null) {
            _hashCode += getAdditionalTxnFields().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DebitReversalReqBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitReversalReqBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TrackData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptionData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalTxnFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFieldsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
