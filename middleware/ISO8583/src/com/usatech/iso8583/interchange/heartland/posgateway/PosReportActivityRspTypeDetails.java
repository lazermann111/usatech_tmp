/**
 * PosReportActivityRspTypeDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportActivityRspTypeDetails  implements java.io.Serializable {
    private int deviceId;

    private java.lang.String userName;

    private java.lang.String serviceName;

    private int gatewayTxnId;

    private java.util.Calendar txnUtcDT;

    private int originalGatewayTxnId;

    private java.lang.String siteTrace;

    private int gatewayRspCode;

    private java.lang.String gatewayRspMsg;

    private java.lang.String status;

    private java.lang.String issuerRspCode;

    private java.lang.String issuerRspText;

    private java.lang.String maskedCardNbr;

    private java.math.BigDecimal amt;

    private com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion;

    private java.util.Calendar txnDT;

    private java.math.BigDecimal authAmt;

    private java.math.BigDecimal settlementAmt;

    public PosReportActivityRspTypeDetails() {
    }

    public PosReportActivityRspTypeDetails(
           int deviceId,
           java.lang.String userName,
           java.lang.String serviceName,
           int gatewayTxnId,
           java.util.Calendar txnUtcDT,
           int originalGatewayTxnId,
           java.lang.String siteTrace,
           int gatewayRspCode,
           java.lang.String gatewayRspMsg,
           java.lang.String status,
           java.lang.String issuerRspCode,
           java.lang.String issuerRspText,
           java.lang.String maskedCardNbr,
           java.math.BigDecimal amt,
           com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion,
           java.util.Calendar txnDT,
           java.math.BigDecimal authAmt,
           java.math.BigDecimal settlementAmt) {
           this.deviceId = deviceId;
           this.userName = userName;
           this.serviceName = serviceName;
           this.gatewayTxnId = gatewayTxnId;
           this.txnUtcDT = txnUtcDT;
           this.originalGatewayTxnId = originalGatewayTxnId;
           this.siteTrace = siteTrace;
           this.gatewayRspCode = gatewayRspCode;
           this.gatewayRspMsg = gatewayRspMsg;
           this.status = status;
           this.issuerRspCode = issuerRspCode;
           this.issuerRspText = issuerRspText;
           this.maskedCardNbr = maskedCardNbr;
           this.amt = amt;
           this.tzConversion = tzConversion;
           this.txnDT = txnDT;
           this.authAmt = authAmt;
           this.settlementAmt = settlementAmt;
    }


    /**
     * Gets the deviceId value for this PosReportActivityRspTypeDetails.
     * 
     * @return deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportActivityRspTypeDetails.
     * 
     * @param deviceId
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the userName value for this PosReportActivityRspTypeDetails.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this PosReportActivityRspTypeDetails.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
     * Gets the serviceName value for this PosReportActivityRspTypeDetails.
     * 
     * @return serviceName
     */
    public java.lang.String getServiceName() {
        return serviceName;
    }


    /**
     * Sets the serviceName value for this PosReportActivityRspTypeDetails.
     * 
     * @param serviceName
     */
    public void setServiceName(java.lang.String serviceName) {
        this.serviceName = serviceName;
    }


    /**
     * Gets the gatewayTxnId value for this PosReportActivityRspTypeDetails.
     * 
     * @return gatewayTxnId
     */
    public int getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this PosReportActivityRspTypeDetails.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(int gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the txnUtcDT value for this PosReportActivityRspTypeDetails.
     * 
     * @return txnUtcDT
     */
    public java.util.Calendar getTxnUtcDT() {
        return txnUtcDT;
    }


    /**
     * Sets the txnUtcDT value for this PosReportActivityRspTypeDetails.
     * 
     * @param txnUtcDT
     */
    public void setTxnUtcDT(java.util.Calendar txnUtcDT) {
        this.txnUtcDT = txnUtcDT;
    }


    /**
     * Gets the originalGatewayTxnId value for this PosReportActivityRspTypeDetails.
     * 
     * @return originalGatewayTxnId
     */
    public int getOriginalGatewayTxnId() {
        return originalGatewayTxnId;
    }


    /**
     * Sets the originalGatewayTxnId value for this PosReportActivityRspTypeDetails.
     * 
     * @param originalGatewayTxnId
     */
    public void setOriginalGatewayTxnId(int originalGatewayTxnId) {
        this.originalGatewayTxnId = originalGatewayTxnId;
    }


    /**
     * Gets the siteTrace value for this PosReportActivityRspTypeDetails.
     * 
     * @return siteTrace
     */
    public java.lang.String getSiteTrace() {
        return siteTrace;
    }


    /**
     * Sets the siteTrace value for this PosReportActivityRspTypeDetails.
     * 
     * @param siteTrace
     */
    public void setSiteTrace(java.lang.String siteTrace) {
        this.siteTrace = siteTrace;
    }


    /**
     * Gets the gatewayRspCode value for this PosReportActivityRspTypeDetails.
     * 
     * @return gatewayRspCode
     */
    public int getGatewayRspCode() {
        return gatewayRspCode;
    }


    /**
     * Sets the gatewayRspCode value for this PosReportActivityRspTypeDetails.
     * 
     * @param gatewayRspCode
     */
    public void setGatewayRspCode(int gatewayRspCode) {
        this.gatewayRspCode = gatewayRspCode;
    }


    /**
     * Gets the gatewayRspMsg value for this PosReportActivityRspTypeDetails.
     * 
     * @return gatewayRspMsg
     */
    public java.lang.String getGatewayRspMsg() {
        return gatewayRspMsg;
    }


    /**
     * Sets the gatewayRspMsg value for this PosReportActivityRspTypeDetails.
     * 
     * @param gatewayRspMsg
     */
    public void setGatewayRspMsg(java.lang.String gatewayRspMsg) {
        this.gatewayRspMsg = gatewayRspMsg;
    }


    /**
     * Gets the status value for this PosReportActivityRspTypeDetails.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this PosReportActivityRspTypeDetails.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the issuerRspCode value for this PosReportActivityRspTypeDetails.
     * 
     * @return issuerRspCode
     */
    public java.lang.String getIssuerRspCode() {
        return issuerRspCode;
    }


    /**
     * Sets the issuerRspCode value for this PosReportActivityRspTypeDetails.
     * 
     * @param issuerRspCode
     */
    public void setIssuerRspCode(java.lang.String issuerRspCode) {
        this.issuerRspCode = issuerRspCode;
    }


    /**
     * Gets the issuerRspText value for this PosReportActivityRspTypeDetails.
     * 
     * @return issuerRspText
     */
    public java.lang.String getIssuerRspText() {
        return issuerRspText;
    }


    /**
     * Sets the issuerRspText value for this PosReportActivityRspTypeDetails.
     * 
     * @param issuerRspText
     */
    public void setIssuerRspText(java.lang.String issuerRspText) {
        this.issuerRspText = issuerRspText;
    }


    /**
     * Gets the maskedCardNbr value for this PosReportActivityRspTypeDetails.
     * 
     * @return maskedCardNbr
     */
    public java.lang.String getMaskedCardNbr() {
        return maskedCardNbr;
    }


    /**
     * Sets the maskedCardNbr value for this PosReportActivityRspTypeDetails.
     * 
     * @param maskedCardNbr
     */
    public void setMaskedCardNbr(java.lang.String maskedCardNbr) {
        this.maskedCardNbr = maskedCardNbr;
    }


    /**
     * Gets the amt value for this PosReportActivityRspTypeDetails.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this PosReportActivityRspTypeDetails.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the tzConversion value for this PosReportActivityRspTypeDetails.
     * 
     * @return tzConversion
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType getTzConversion() {
        return tzConversion;
    }


    /**
     * Sets the tzConversion value for this PosReportActivityRspTypeDetails.
     * 
     * @param tzConversion
     */
    public void setTzConversion(com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType tzConversion) {
        this.tzConversion = tzConversion;
    }


    /**
     * Gets the txnDT value for this PosReportActivityRspTypeDetails.
     * 
     * @return txnDT
     */
    public java.util.Calendar getTxnDT() {
        return txnDT;
    }


    /**
     * Sets the txnDT value for this PosReportActivityRspTypeDetails.
     * 
     * @param txnDT
     */
    public void setTxnDT(java.util.Calendar txnDT) {
        this.txnDT = txnDT;
    }


    /**
     * Gets the authAmt value for this PosReportActivityRspTypeDetails.
     * 
     * @return authAmt
     */
    public java.math.BigDecimal getAuthAmt() {
        return authAmt;
    }


    /**
     * Sets the authAmt value for this PosReportActivityRspTypeDetails.
     * 
     * @param authAmt
     */
    public void setAuthAmt(java.math.BigDecimal authAmt) {
        this.authAmt = authAmt;
    }


    /**
     * Gets the settlementAmt value for this PosReportActivityRspTypeDetails.
     * 
     * @return settlementAmt
     */
    public java.math.BigDecimal getSettlementAmt() {
        return settlementAmt;
    }


    /**
     * Sets the settlementAmt value for this PosReportActivityRspTypeDetails.
     * 
     * @param settlementAmt
     */
    public void setSettlementAmt(java.math.BigDecimal settlementAmt) {
        this.settlementAmt = settlementAmt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportActivityRspTypeDetails)) return false;
        PosReportActivityRspTypeDetails other = (PosReportActivityRspTypeDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.deviceId == other.getDeviceId() &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.serviceName==null && other.getServiceName()==null) || 
             (this.serviceName!=null &&
              this.serviceName.equals(other.getServiceName()))) &&
            this.gatewayTxnId == other.getGatewayTxnId() &&
            ((this.txnUtcDT==null && other.getTxnUtcDT()==null) || 
             (this.txnUtcDT!=null &&
              this.txnUtcDT.equals(other.getTxnUtcDT()))) &&
            this.originalGatewayTxnId == other.getOriginalGatewayTxnId() &&
            ((this.siteTrace==null && other.getSiteTrace()==null) || 
             (this.siteTrace!=null &&
              this.siteTrace.equals(other.getSiteTrace()))) &&
            this.gatewayRspCode == other.getGatewayRspCode() &&
            ((this.gatewayRspMsg==null && other.getGatewayRspMsg()==null) || 
             (this.gatewayRspMsg!=null &&
              this.gatewayRspMsg.equals(other.getGatewayRspMsg()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.issuerRspCode==null && other.getIssuerRspCode()==null) || 
             (this.issuerRspCode!=null &&
              this.issuerRspCode.equals(other.getIssuerRspCode()))) &&
            ((this.issuerRspText==null && other.getIssuerRspText()==null) || 
             (this.issuerRspText!=null &&
              this.issuerRspText.equals(other.getIssuerRspText()))) &&
            ((this.maskedCardNbr==null && other.getMaskedCardNbr()==null) || 
             (this.maskedCardNbr!=null &&
              this.maskedCardNbr.equals(other.getMaskedCardNbr()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.tzConversion==null && other.getTzConversion()==null) || 
             (this.tzConversion!=null &&
              this.tzConversion.equals(other.getTzConversion()))) &&
            ((this.txnDT==null && other.getTxnDT()==null) || 
             (this.txnDT!=null &&
              this.txnDT.equals(other.getTxnDT()))) &&
            ((this.authAmt==null && other.getAuthAmt()==null) || 
             (this.authAmt!=null &&
              this.authAmt.equals(other.getAuthAmt()))) &&
            ((this.settlementAmt==null && other.getSettlementAmt()==null) || 
             (this.settlementAmt!=null &&
              this.settlementAmt.equals(other.getSettlementAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getDeviceId();
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getServiceName() != null) {
            _hashCode += getServiceName().hashCode();
        }
        _hashCode += getGatewayTxnId();
        if (getTxnUtcDT() != null) {
            _hashCode += getTxnUtcDT().hashCode();
        }
        _hashCode += getOriginalGatewayTxnId();
        if (getSiteTrace() != null) {
            _hashCode += getSiteTrace().hashCode();
        }
        _hashCode += getGatewayRspCode();
        if (getGatewayRspMsg() != null) {
            _hashCode += getGatewayRspMsg().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getIssuerRspCode() != null) {
            _hashCode += getIssuerRspCode().hashCode();
        }
        if (getIssuerRspText() != null) {
            _hashCode += getIssuerRspText().hashCode();
        }
        if (getMaskedCardNbr() != null) {
            _hashCode += getMaskedCardNbr().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getTzConversion() != null) {
            _hashCode += getTzConversion().hashCode();
        }
        if (getTxnDT() != null) {
            _hashCode += getTxnDT().hashCode();
        }
        if (getAuthAmt() != null) {
            _hashCode += getAuthAmt().hashCode();
        }
        if (getSettlementAmt() != null) {
            _hashCode += getSettlementAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportActivityRspTypeDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportActivityRspType>Details"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "UserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ServiceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnUtcDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnUtcDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalGatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "OriginalGatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteTrace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteTrace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayRspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayRspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayRspMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayRspMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuerRspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "IssuerRspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuerRspText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "IssuerRspText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maskedCardNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MaskedCardNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tzConversion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TzConversion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "tzoneConversionType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnDT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnDT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("settlementAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SettlementAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
