/**
 * ExtraChargesDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class ExtraChargesDataType  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType restaurant;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType giftShop;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType miniBar;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType telephone;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType other;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType laundry;

    public ExtraChargesDataType() {
    }

    public ExtraChargesDataType(
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType restaurant,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType giftShop,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType miniBar,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType telephone,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType other,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType laundry) {
           this.restaurant = restaurant;
           this.giftShop = giftShop;
           this.miniBar = miniBar;
           this.telephone = telephone;
           this.other = other;
           this.laundry = laundry;
    }


    /**
     * Gets the restaurant value for this ExtraChargesDataType.
     * 
     * @return restaurant
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getRestaurant() {
        return restaurant;
    }


    /**
     * Sets the restaurant value for this ExtraChargesDataType.
     * 
     * @param restaurant
     */
    public void setRestaurant(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType restaurant) {
        this.restaurant = restaurant;
    }


    /**
     * Gets the giftShop value for this ExtraChargesDataType.
     * 
     * @return giftShop
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getGiftShop() {
        return giftShop;
    }


    /**
     * Sets the giftShop value for this ExtraChargesDataType.
     * 
     * @param giftShop
     */
    public void setGiftShop(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType giftShop) {
        this.giftShop = giftShop;
    }


    /**
     * Gets the miniBar value for this ExtraChargesDataType.
     * 
     * @return miniBar
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getMiniBar() {
        return miniBar;
    }


    /**
     * Sets the miniBar value for this ExtraChargesDataType.
     * 
     * @param miniBar
     */
    public void setMiniBar(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType miniBar) {
        this.miniBar = miniBar;
    }


    /**
     * Gets the telephone value for this ExtraChargesDataType.
     * 
     * @return telephone
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getTelephone() {
        return telephone;
    }


    /**
     * Sets the telephone value for this ExtraChargesDataType.
     * 
     * @param telephone
     */
    public void setTelephone(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType telephone) {
        this.telephone = telephone;
    }


    /**
     * Gets the other value for this ExtraChargesDataType.
     * 
     * @return other
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getOther() {
        return other;
    }


    /**
     * Sets the other value for this ExtraChargesDataType.
     * 
     * @param other
     */
    public void setOther(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType other) {
        this.other = other;
    }


    /**
     * Gets the laundry value for this ExtraChargesDataType.
     * 
     * @return laundry
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getLaundry() {
        return laundry;
    }


    /**
     * Sets the laundry value for this ExtraChargesDataType.
     * 
     * @param laundry
     */
    public void setLaundry(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType laundry) {
        this.laundry = laundry;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExtraChargesDataType)) return false;
        ExtraChargesDataType other = (ExtraChargesDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.restaurant==null && other.getRestaurant()==null) || 
             (this.restaurant!=null &&
              this.restaurant.equals(other.getRestaurant()))) &&
            ((this.giftShop==null && other.getGiftShop()==null) || 
             (this.giftShop!=null &&
              this.giftShop.equals(other.getGiftShop()))) &&
            ((this.miniBar==null && other.getMiniBar()==null) || 
             (this.miniBar!=null &&
              this.miniBar.equals(other.getMiniBar()))) &&
            ((this.telephone==null && other.getTelephone()==null) || 
             (this.telephone!=null &&
              this.telephone.equals(other.getTelephone()))) &&
            ((this.other==null && other.getOther()==null) || 
             (this.other!=null &&
              this.other.equals(other.getOther()))) &&
            ((this.laundry==null && other.getLaundry()==null) || 
             (this.laundry!=null &&
              this.laundry.equals(other.getLaundry())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRestaurant() != null) {
            _hashCode += getRestaurant().hashCode();
        }
        if (getGiftShop() != null) {
            _hashCode += getGiftShop().hashCode();
        }
        if (getMiniBar() != null) {
            _hashCode += getMiniBar().hashCode();
        }
        if (getTelephone() != null) {
            _hashCode += getTelephone().hashCode();
        }
        if (getOther() != null) {
            _hashCode += getOther().hashCode();
        }
        if (getLaundry() != null) {
            _hashCode += getLaundry().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExtraChargesDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExtraChargesDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("restaurant");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Restaurant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftShop");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftShop"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("miniBar");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MiniBar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telephone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Telephone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("other");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Other"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("laundry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Laundry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
