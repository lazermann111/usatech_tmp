/**
 * PosCreditTxnEditReqType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosCreditTxnEditReqType  implements java.io.Serializable {
    private int gatewayTxnId;

    private java.math.BigDecimal amt;

    private java.math.BigDecimal gratuityAmtInfo;

    private com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType lodgingDataEdit;

    public PosCreditTxnEditReqType() {
    }

    public PosCreditTxnEditReqType(
           int gatewayTxnId,
           java.math.BigDecimal amt,
           java.math.BigDecimal gratuityAmtInfo,
           com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType lodgingDataEdit) {
           this.gatewayTxnId = gatewayTxnId;
           this.amt = amt;
           this.gratuityAmtInfo = gratuityAmtInfo;
           this.lodgingDataEdit = lodgingDataEdit;
    }


    /**
     * Gets the gatewayTxnId value for this PosCreditTxnEditReqType.
     * 
     * @return gatewayTxnId
     */
    public int getGatewayTxnId() {
        return gatewayTxnId;
    }


    /**
     * Sets the gatewayTxnId value for this PosCreditTxnEditReqType.
     * 
     * @param gatewayTxnId
     */
    public void setGatewayTxnId(int gatewayTxnId) {
        this.gatewayTxnId = gatewayTxnId;
    }


    /**
     * Gets the amt value for this PosCreditTxnEditReqType.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this PosCreditTxnEditReqType.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the gratuityAmtInfo value for this PosCreditTxnEditReqType.
     * 
     * @return gratuityAmtInfo
     */
    public java.math.BigDecimal getGratuityAmtInfo() {
        return gratuityAmtInfo;
    }


    /**
     * Sets the gratuityAmtInfo value for this PosCreditTxnEditReqType.
     * 
     * @param gratuityAmtInfo
     */
    public void setGratuityAmtInfo(java.math.BigDecimal gratuityAmtInfo) {
        this.gratuityAmtInfo = gratuityAmtInfo;
    }


    /**
     * Gets the lodgingDataEdit value for this PosCreditTxnEditReqType.
     * 
     * @return lodgingDataEdit
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType getLodgingDataEdit() {
        return lodgingDataEdit;
    }


    /**
     * Sets the lodgingDataEdit value for this PosCreditTxnEditReqType.
     * 
     * @param lodgingDataEdit
     */
    public void setLodgingDataEdit(com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType lodgingDataEdit) {
        this.lodgingDataEdit = lodgingDataEdit;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosCreditTxnEditReqType)) return false;
        PosCreditTxnEditReqType other = (PosCreditTxnEditReqType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.gatewayTxnId == other.getGatewayTxnId() &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.gratuityAmtInfo==null && other.getGratuityAmtInfo()==null) || 
             (this.gratuityAmtInfo!=null &&
              this.gratuityAmtInfo.equals(other.getGratuityAmtInfo()))) &&
            ((this.lodgingDataEdit==null && other.getLodgingDataEdit()==null) || 
             (this.lodgingDataEdit!=null &&
              this.lodgingDataEdit.equals(other.getLodgingDataEdit())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGatewayTxnId();
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getGratuityAmtInfo() != null) {
            _hashCode += getGratuityAmtInfo().hashCode();
        }
        if (getLodgingDataEdit() != null) {
            _hashCode += getLodgingDataEdit().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosCreditTxnEditReqType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditTxnEditReqType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gatewayTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GatewayTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gratuityAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GratuityAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lodgingDataEdit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataEdit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataEditType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
