/**
 * PosGatewayInterface1Stub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosGatewayInterface1Stub extends org.apache.axis.client.Stub implements com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[1];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DoTransaction");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosRequest"), com.usatech.iso8583.interchange.heartland.posgateway.PosRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosResponse"));
        oper.setReturnClass(com.usatech.iso8583.interchange.heartland.posgateway.PosResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

    }

    public PosGatewayInterface1Stub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public PosGatewayInterface1Stub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public PosGatewayInterface1Stub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosRequest>Ver1.0>Header");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Header.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosRequest>Ver1.0>Transaction");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosResponse>Ver1.0>Header");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10Header.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>>PosResponse>Ver1.0>Transaction");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10Transaction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>CardDataType>TrackData>method");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeTrackDataMethod.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>PosRequest>Ver1.0");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>PosResponse>Ver1.0");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosResponseVer10.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">CardDataType>ManualEntry");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeManualEntry.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">CardDataType>TrackData");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CardDataTypeTrackData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportActivityRspType>Details");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityRspTypeDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportActivityRspType>Header");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityRspTypeHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchDetailRspType>Details");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailRspTypeDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchDetailRspType>Header");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailRspTypeHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchHistoryRspType>Details");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryRspTypeDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchHistoryRspType>Header");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryRspTypeHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchSummaryRspType>Details");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportBatchSummaryRspType>Header");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspTypeHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportOpenAuthsRspType>Details");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsRspTypeDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportOpenAuthsRspType>Header");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsRspTypeHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportSearchRspType>Details");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchRspTypeDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportSearchRspType>Header");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchRspTypeHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportTxnDetailRspType>Data");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspTypeData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosRequest");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosResponse");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalAmtType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AdditionalAmtType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFieldsType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "advancedDepositTypeType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AdvancedDepositTypeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "amtTypeType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AmtTypeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AttachmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "attachmentFormatType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AttachmentFormatType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "attachmentTypeType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AttachmentTypeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthRspStatusType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AuthRspStatusType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AutoSubstantiationReportType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationReportType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AutoSubstantiationType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.BooleanType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CardDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CPCDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAccountVerifyBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CreditAccountVerifyBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditAuthReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CreditAuthReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditIncrementalAuthReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CreditIncrementalAuthReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditOfflineAuthReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CreditOfflineAuthReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditOfflineSaleReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CreditOfflineSaleReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditReturnReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CreditReturnReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditReversalReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CreditReversalReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CreditSaleReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.CreditSaleReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "cvv2Status");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.Cvv2Status.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitAddValueReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.DebitAddValueReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitReturnReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.DebitReturnReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitReversalReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.DebitReversalReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitSaleReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.DebitSaleReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.DirectMktDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTBalanceInquiryReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.EBTBalanceInquiryReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTBalanceInquiryType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.EBTBalanceInquiryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTCashBackPurchaseReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.EBTCashBackPurchaseReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTCashBenefitWithdrawalReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.EBTCashBenefitWithdrawalReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTFSPurchaseReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.EBTFSPurchaseReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTFSReturnReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.EBTFSReturnReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTFSVoucherReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.EBTFSVoucherReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "eCommerceType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.ECommerceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.EncryptionDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExtraChargesDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.ExtraChargesDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardActivateReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardActivateReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardAddValueReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardAddValueReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardBalanceReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardBalanceReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardDeactivateReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardDeactivateReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardReplaceReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardReplaceReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardSaleReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardSaleReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardTotalsType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardTotalsType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GiftCardVoidReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GiftCardVoidReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GPSCoordinatesType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.GPSCoordinatesType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataEditType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataEditType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "LodgingDataType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.LodgingDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosAddAttachmentReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosAddAttachmentReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosBatchCloseRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosBatchCloseRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditAccountVerifyReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAccountVerifyReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditAddToBatchReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAddToBatchReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditAuthReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAuthReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditCPCEditReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditCPCEditReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditIncrementalAuthReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditIncrementalAuthReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditOfflineAuthReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineAuthReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditOfflineSaleReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditOfflineSaleReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditReturnReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReturnReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditReversalReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditReversalReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditSaleReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditSaleReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditTxnEditReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditTxnEditReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosCreditVoidReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosCreditVoidReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosDebitAddValueReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosDebitAddValueReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosDebitReturnReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReturnReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosDebitReversalReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosDebitReversalReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosDebitSaleReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosDebitSaleReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTBalanceInquiryReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosEBTBalanceInquiryReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTCashBackPurchaseReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBackPurchaseReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTCashBenefitWithdrawalReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosEBTCashBenefitWithdrawalReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTFSPurchaseReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSPurchaseReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTFSReturnReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSReturnReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosEBTFSVoucherReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosEBTFSVoucherReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardActivateReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardActivateRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardActivateRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardAddValueReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardAddValueRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardAddValueRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardBalanceReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardBalanceRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardBalanceRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardDeactivateReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardDeactivateRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardDeactivateRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardReplaceReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardReplaceRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardReplaceRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardSaleReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardSaleRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardSaleRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardVoidReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosGiftCardVoidRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosGiftCardVoidRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosPrePaidAddValueReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidAddValueReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosPrePaidBalanceInquiryReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosPrePaidBalanceInquiryReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosRecurringBillReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosRecurringBillReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportActivityReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportActivityRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportActivityRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchDetailReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchDetailRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchDetailRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchHistoryReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchHistoryRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchHistoryRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchSummaryReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportBatchSummaryRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportBatchSummaryRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportOpenAuthsReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportOpenAuthsRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportOpenAuthsRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportSearchReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportSearchRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportSearchRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportTxnDetailReqType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailReqType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PosReportTxnDetailRspType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PosReportTxnDetailRspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PrePaidAddValueReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PrePaidAddValueReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PrePaidBalanceInquiryReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PrePaidBalanceInquiryReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "prestigiousPropertyType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.PrestigiousPropertyType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RecurringBillReqBlock1Type");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.RecurringBillReqBlock1Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportSearchCriteriaType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.ReportSearchCriteriaType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReportSearchSiteTraceCriteriaType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.ReportSearchSiteTraceCriteriaType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "resultCodeActionType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.ResultCodeActionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "taxTypeType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.TaxTypeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "tzoneConversionType");
            cachedSerQNames.add(qName);
            cls = com.usatech.iso8583.interchange.heartland.posgateway.TzoneConversionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.usatech.iso8583.interchange.heartland.posgateway.PosResponse doTransaction(com.usatech.iso8583.interchange.heartland.posgateway.PosRequest posRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DoTransaction"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {posRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.usatech.iso8583.interchange.heartland.posgateway.PosResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.usatech.iso8583.interchange.heartland.posgateway.PosResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.usatech.iso8583.interchange.heartland.posgateway.PosResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
