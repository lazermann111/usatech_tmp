/**
 * PosReportOpenAuthsRspTypeHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportOpenAuthsRspTypeHeader  implements java.io.Serializable {
    private int siteId;

    private java.lang.String merchName;

    private java.lang.Integer deviceId;

    private int txnCnt;

    private java.math.BigDecimal txnAmt;

    public PosReportOpenAuthsRspTypeHeader() {
    }

    public PosReportOpenAuthsRspTypeHeader(
           int siteId,
           java.lang.String merchName,
           java.lang.Integer deviceId,
           int txnCnt,
           java.math.BigDecimal txnAmt) {
           this.siteId = siteId;
           this.merchName = merchName;
           this.deviceId = deviceId;
           this.txnCnt = txnCnt;
           this.txnAmt = txnAmt;
    }


    /**
     * Gets the siteId value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @return siteId
     */
    public int getSiteId() {
        return siteId;
    }


    /**
     * Sets the siteId value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @param siteId
     */
    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }


    /**
     * Gets the merchName value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @return merchName
     */
    public java.lang.String getMerchName() {
        return merchName;
    }


    /**
     * Sets the merchName value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @param merchName
     */
    public void setMerchName(java.lang.String merchName) {
        this.merchName = merchName;
    }


    /**
     * Gets the deviceId value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @return deviceId
     */
    public java.lang.Integer getDeviceId() {
        return deviceId;
    }


    /**
     * Sets the deviceId value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @param deviceId
     */
    public void setDeviceId(java.lang.Integer deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * Gets the txnCnt value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @return txnCnt
     */
    public int getTxnCnt() {
        return txnCnt;
    }


    /**
     * Sets the txnCnt value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @param txnCnt
     */
    public void setTxnCnt(int txnCnt) {
        this.txnCnt = txnCnt;
    }


    /**
     * Gets the txnAmt value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @return txnAmt
     */
    public java.math.BigDecimal getTxnAmt() {
        return txnAmt;
    }


    /**
     * Sets the txnAmt value for this PosReportOpenAuthsRspTypeHeader.
     * 
     * @param txnAmt
     */
    public void setTxnAmt(java.math.BigDecimal txnAmt) {
        this.txnAmt = txnAmt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportOpenAuthsRspTypeHeader)) return false;
        PosReportOpenAuthsRspTypeHeader other = (PosReportOpenAuthsRspTypeHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.siteId == other.getSiteId() &&
            ((this.merchName==null && other.getMerchName()==null) || 
             (this.merchName!=null &&
              this.merchName.equals(other.getMerchName()))) &&
            ((this.deviceId==null && other.getDeviceId()==null) || 
             (this.deviceId!=null &&
              this.deviceId.equals(other.getDeviceId()))) &&
            this.txnCnt == other.getTxnCnt() &&
            ((this.txnAmt==null && other.getTxnAmt()==null) || 
             (this.txnAmt!=null &&
              this.txnAmt.equals(other.getTxnAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSiteId();
        if (getMerchName() != null) {
            _hashCode += getMerchName().hashCode();
        }
        if (getDeviceId() != null) {
            _hashCode += getDeviceId().hashCode();
        }
        _hashCode += getTxnCnt();
        if (getTxnAmt() != null) {
            _hashCode += getTxnAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportOpenAuthsRspTypeHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportOpenAuthsRspType>Header"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SiteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MerchName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DeviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnCnt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnCnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
