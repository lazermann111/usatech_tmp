/**
 * PosGatewayServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosGatewayServiceLocator extends org.apache.axis.client.Service implements com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayService {

    public PosGatewayServiceLocator() {
    }


    public PosGatewayServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PosGatewayServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PosGatewayInterface
    private java.lang.String PosGatewayInterface_address = "https://posgateway.cert.secureexchange.net/Hps.Exchange.PosGateway.UAT/PosGatewayService.asmx";

    public java.lang.String getPosGatewayInterfaceAddress() {
        return PosGatewayInterface_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PosGatewayInterfaceWSDDServiceName = "PosGatewayInterface";

    public java.lang.String getPosGatewayInterfaceWSDDServiceName() {
        return PosGatewayInterfaceWSDDServiceName;
    }

    public void setPosGatewayInterfaceWSDDServiceName(java.lang.String name) {
        PosGatewayInterfaceWSDDServiceName = name;
    }

    public com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType getPosGatewayInterface() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PosGatewayInterface_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPosGatewayInterface(endpoint);
    }

    public com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType getPosGatewayInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_BindingStub _stub = new com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_BindingStub(portAddress, this);
            _stub.setPortName(getPosGatewayInterfaceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPosGatewayInterfaceEndpointAddress(java.lang.String address) {
        PosGatewayInterface_address = address;
    }


    // Use to get a proxy class for PosGatewayInterface1
    private java.lang.String PosGatewayInterface1_address = "https://posgateway.cert.secureexchange.net/Hps.Exchange.PosGateway.UAT/PosGatewayService.asmx";

    public java.lang.String getPosGatewayInterface1Address() {
        return PosGatewayInterface1_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PosGatewayInterface1WSDDServiceName = "PosGatewayInterface1";

    public java.lang.String getPosGatewayInterface1WSDDServiceName() {
        return PosGatewayInterface1WSDDServiceName;
    }

    public void setPosGatewayInterface1WSDDServiceName(java.lang.String name) {
        PosGatewayInterface1WSDDServiceName = name;
    }

    public com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType getPosGatewayInterface1() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PosGatewayInterface1_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPosGatewayInterface1(endpoint);
    }

    public com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType getPosGatewayInterface1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface1Stub _stub = new com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface1Stub(portAddress, this);
            _stub.setPortName(getPosGatewayInterface1WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPosGatewayInterface1EndpointAddress(java.lang.String address) {
        PosGatewayInterface1_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_BindingStub _stub = new com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_BindingStub(new java.net.URL(PosGatewayInterface_address), this);
                _stub.setPortName(getPosGatewayInterfaceWSDDServiceName());
                return _stub;
            }
            if (com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface1Stub _stub = new com.usatech.iso8583.interchange.heartland.posgateway.PosGatewayInterface1Stub(new java.net.URL(PosGatewayInterface1_address), this);
                _stub.setPortName(getPosGatewayInterface1WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PosGatewayInterface".equals(inputPortName)) {
            return getPosGatewayInterface();
        }
        else if ("PosGatewayInterface1".equals(inputPortName)) {
            return getPosGatewayInterface1();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway/PosGatewayService/", "PosGatewayService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway/PosGatewayService/", "PosGatewayInterface"));
            ports.add(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway/PosGatewayService/", "PosGatewayInterface1"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PosGatewayInterface".equals(portName)) {
            setPosGatewayInterfaceEndpointAddress(address);
        }
        else 
if ("PosGatewayInterface1".equals(portName)) {
            setPosGatewayInterface1EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
