/**
 * EncryptionDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class EncryptionDataType  implements java.io.Serializable {
    private java.lang.String version;

    private java.lang.String encryptedTrackNumber;

    private java.lang.String KTB;

    private java.lang.String KSN;

    public EncryptionDataType() {
    }

    public EncryptionDataType(
           java.lang.String version,
           java.lang.String encryptedTrackNumber,
           java.lang.String KTB,
           java.lang.String KSN) {
           this.version = version;
           this.encryptedTrackNumber = encryptedTrackNumber;
           this.KTB = KTB;
           this.KSN = KSN;
    }


    /**
     * Gets the version value for this EncryptionDataType.
     * 
     * @return version
     */
    public java.lang.String getVersion() {
        return version;
    }


    /**
     * Sets the version value for this EncryptionDataType.
     * 
     * @param version
     */
    public void setVersion(java.lang.String version) {
        this.version = version;
    }


    /**
     * Gets the encryptedTrackNumber value for this EncryptionDataType.
     * 
     * @return encryptedTrackNumber
     */
    public java.lang.String getEncryptedTrackNumber() {
        return encryptedTrackNumber;
    }


    /**
     * Sets the encryptedTrackNumber value for this EncryptionDataType.
     * 
     * @param encryptedTrackNumber
     */
    public void setEncryptedTrackNumber(java.lang.String encryptedTrackNumber) {
        this.encryptedTrackNumber = encryptedTrackNumber;
    }


    /**
     * Gets the KTB value for this EncryptionDataType.
     * 
     * @return KTB
     */
    public java.lang.String getKTB() {
        return KTB;
    }


    /**
     * Sets the KTB value for this EncryptionDataType.
     * 
     * @param KTB
     */
    public void setKTB(java.lang.String KTB) {
        this.KTB = KTB;
    }


    /**
     * Gets the KSN value for this EncryptionDataType.
     * 
     * @return KSN
     */
    public java.lang.String getKSN() {
        return KSN;
    }


    /**
     * Sets the KSN value for this EncryptionDataType.
     * 
     * @param KSN
     */
    public void setKSN(java.lang.String KSN) {
        this.KSN = KSN;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EncryptionDataType)) return false;
        EncryptionDataType other = (EncryptionDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.encryptedTrackNumber==null && other.getEncryptedTrackNumber()==null) || 
             (this.encryptedTrackNumber!=null &&
              this.encryptedTrackNumber.equals(other.getEncryptedTrackNumber()))) &&
            ((this.KTB==null && other.getKTB()==null) || 
             (this.KTB!=null &&
              this.KTB.equals(other.getKTB()))) &&
            ((this.KSN==null && other.getKSN()==null) || 
             (this.KSN!=null &&
              this.KSN.equals(other.getKSN())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getEncryptedTrackNumber() != null) {
            _hashCode += getEncryptedTrackNumber().hashCode();
        }
        if (getKTB() != null) {
            _hashCode += getKTB().hashCode();
        }
        if (getKSN() != null) {
            _hashCode += getKSN().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EncryptionDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptionDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("encryptedTrackNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EncryptedTrackNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("KTB");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "KTB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("KSN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "KSN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
