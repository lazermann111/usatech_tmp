/**
 * EBTFSVoucherReqBlock1Type.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class EBTFSVoucherReqBlock1Type  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData;

    private java.math.BigDecimal amt;

    private java.lang.String pinBlock;

    private com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup;

    private java.lang.String exprDate;

    private java.lang.String electronicVoucherSerialNbr;

    private java.lang.String voucherApprovalCd;

    private java.lang.String primaryAcctNbr;

    public EBTFSVoucherReqBlock1Type() {
    }

    public EBTFSVoucherReqBlock1Type(
           com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData,
           java.math.BigDecimal amt,
           java.lang.String pinBlock,
           com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup,
           java.lang.String exprDate,
           java.lang.String electronicVoucherSerialNbr,
           java.lang.String voucherApprovalCd,
           java.lang.String primaryAcctNbr) {
           this.cardData = cardData;
           this.amt = amt;
           this.pinBlock = pinBlock;
           this.cardHolderData = cardHolderData;
           this.allowDup = allowDup;
           this.exprDate = exprDate;
           this.electronicVoucherSerialNbr = electronicVoucherSerialNbr;
           this.voucherApprovalCd = voucherApprovalCd;
           this.primaryAcctNbr = primaryAcctNbr;
    }


    /**
     * Gets the cardData value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return cardData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardDataType getCardData() {
        return cardData;
    }


    /**
     * Sets the cardData value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param cardData
     */
    public void setCardData(com.usatech.iso8583.interchange.heartland.posgateway.CardDataType cardData) {
        this.cardData = cardData;
    }


    /**
     * Gets the amt value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the pinBlock value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return pinBlock
     */
    public java.lang.String getPinBlock() {
        return pinBlock;
    }


    /**
     * Sets the pinBlock value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param pinBlock
     */
    public void setPinBlock(java.lang.String pinBlock) {
        this.pinBlock = pinBlock;
    }


    /**
     * Gets the cardHolderData value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return cardHolderData
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType getCardHolderData() {
        return cardHolderData;
    }


    /**
     * Sets the cardHolderData value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param cardHolderData
     */
    public void setCardHolderData(com.usatech.iso8583.interchange.heartland.posgateway.CardHolderDataType cardHolderData) {
        this.cardHolderData = cardHolderData;
    }


    /**
     * Gets the allowDup value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return allowDup
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getAllowDup() {
        return allowDup;
    }


    /**
     * Sets the allowDup value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param allowDup
     */
    public void setAllowDup(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowDup) {
        this.allowDup = allowDup;
    }


    /**
     * Gets the exprDate value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return exprDate
     */
    public java.lang.String getExprDate() {
        return exprDate;
    }


    /**
     * Sets the exprDate value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param exprDate
     */
    public void setExprDate(java.lang.String exprDate) {
        this.exprDate = exprDate;
    }


    /**
     * Gets the electronicVoucherSerialNbr value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return electronicVoucherSerialNbr
     */
    public java.lang.String getElectronicVoucherSerialNbr() {
        return electronicVoucherSerialNbr;
    }


    /**
     * Sets the electronicVoucherSerialNbr value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param electronicVoucherSerialNbr
     */
    public void setElectronicVoucherSerialNbr(java.lang.String electronicVoucherSerialNbr) {
        this.electronicVoucherSerialNbr = electronicVoucherSerialNbr;
    }


    /**
     * Gets the voucherApprovalCd value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return voucherApprovalCd
     */
    public java.lang.String getVoucherApprovalCd() {
        return voucherApprovalCd;
    }


    /**
     * Sets the voucherApprovalCd value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param voucherApprovalCd
     */
    public void setVoucherApprovalCd(java.lang.String voucherApprovalCd) {
        this.voucherApprovalCd = voucherApprovalCd;
    }


    /**
     * Gets the primaryAcctNbr value for this EBTFSVoucherReqBlock1Type.
     * 
     * @return primaryAcctNbr
     */
    public java.lang.String getPrimaryAcctNbr() {
        return primaryAcctNbr;
    }


    /**
     * Sets the primaryAcctNbr value for this EBTFSVoucherReqBlock1Type.
     * 
     * @param primaryAcctNbr
     */
    public void setPrimaryAcctNbr(java.lang.String primaryAcctNbr) {
        this.primaryAcctNbr = primaryAcctNbr;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EBTFSVoucherReqBlock1Type)) return false;
        EBTFSVoucherReqBlock1Type other = (EBTFSVoucherReqBlock1Type) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardData==null && other.getCardData()==null) || 
             (this.cardData!=null &&
              this.cardData.equals(other.getCardData()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.pinBlock==null && other.getPinBlock()==null) || 
             (this.pinBlock!=null &&
              this.pinBlock.equals(other.getPinBlock()))) &&
            ((this.cardHolderData==null && other.getCardHolderData()==null) || 
             (this.cardHolderData!=null &&
              this.cardHolderData.equals(other.getCardHolderData()))) &&
            ((this.allowDup==null && other.getAllowDup()==null) || 
             (this.allowDup!=null &&
              this.allowDup.equals(other.getAllowDup()))) &&
            ((this.exprDate==null && other.getExprDate()==null) || 
             (this.exprDate!=null &&
              this.exprDate.equals(other.getExprDate()))) &&
            ((this.electronicVoucherSerialNbr==null && other.getElectronicVoucherSerialNbr()==null) || 
             (this.electronicVoucherSerialNbr!=null &&
              this.electronicVoucherSerialNbr.equals(other.getElectronicVoucherSerialNbr()))) &&
            ((this.voucherApprovalCd==null && other.getVoucherApprovalCd()==null) || 
             (this.voucherApprovalCd!=null &&
              this.voucherApprovalCd.equals(other.getVoucherApprovalCd()))) &&
            ((this.primaryAcctNbr==null && other.getPrimaryAcctNbr()==null) || 
             (this.primaryAcctNbr!=null &&
              this.primaryAcctNbr.equals(other.getPrimaryAcctNbr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardData() != null) {
            _hashCode += getCardData().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getPinBlock() != null) {
            _hashCode += getPinBlock().hashCode();
        }
        if (getCardHolderData() != null) {
            _hashCode += getCardHolderData().hashCode();
        }
        if (getAllowDup() != null) {
            _hashCode += getAllowDup().hashCode();
        }
        if (getExprDate() != null) {
            _hashCode += getExprDate().hashCode();
        }
        if (getElectronicVoucherSerialNbr() != null) {
            _hashCode += getElectronicVoucherSerialNbr().hashCode();
        }
        if (getVoucherApprovalCd() != null) {
            _hashCode += getVoucherApprovalCd().hashCode();
        }
        if (getPrimaryAcctNbr() != null) {
            _hashCode += getPrimaryAcctNbr().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EBTFSVoucherReqBlock1Type.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "EBTFSVoucherReqBlock1Type"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pinBlock");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PinBlock"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowDup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AllowDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exprDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ExprDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("electronicVoucherSerialNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ElectronicVoucherSerialNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucherApprovalCd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "VoucherApprovalCd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryAcctNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "PrimaryAcctNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
