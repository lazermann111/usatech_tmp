/**
 * PosReportTxnDetailRspTypeData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosReportTxnDetailRspTypeData  implements java.io.Serializable {
    private java.lang.String txnStatus;

    private java.lang.String cardType;

    private java.lang.String maskedCardNbr;

    private java.lang.String cardPresent;

    private java.lang.String readerPresent;

    private java.lang.String cardSwiped;

    private java.lang.String debitCreditInd;

    private java.lang.String saleReturnInd;

    private java.lang.String CVVReq;

    private java.lang.String CVVRsltCode;

    private java.math.BigDecimal amt;

    private java.math.BigDecimal authAmt;

    private java.math.BigDecimal gratuityAmtInfo;

    private java.math.BigDecimal cashbackAmtInfo;

    private java.lang.String cardHolderLastName;

    private java.lang.String cardHolderFirstName;

    private java.lang.String cardHolderAddr;

    private java.lang.String cardHolderCity;

    private java.lang.String cardHolderState;

    private java.lang.String cardHolderZip;

    private java.lang.String cardHolderPhone;

    private java.lang.String cardHolderEmail;

    private java.lang.String AVSRsltCode;

    private java.lang.String CPCReq;

    private java.lang.String CPCInd;

    private java.lang.String rspCode;

    private java.lang.String rspText;

    private java.lang.String authCode;

    private java.lang.String reqACI;

    private java.lang.String rspACI;

    private java.lang.String mktSpecDataId;

    private java.lang.String txnCode;

    private java.lang.String acctDataSrc;

    private java.lang.String authSrcCode;

    private java.lang.String issTxnId;

    private java.lang.String issValidationCode;

    private java.lang.String cardHolderIdCode;

    private java.lang.String networkIdCode;

    private java.lang.String refNbr;

    private int txnSeqNbr;

    private java.lang.String directMktInvoiceNbr;

    private int directMktShipMonth;

    private int directMktShipDay;

    private java.lang.String CPCCardHolderPONbr;

    private java.lang.String CPCTaxType;

    private java.math.BigDecimal CPCTaxAmt;

    private java.math.BigDecimal settlementAmt;

    private com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowPartialAuth;

    private com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationReportType autoSubstantiation;

    private java.lang.String attachmentType;

    private java.lang.String attachmentData;

    private java.lang.String attachmentFormat;

    private com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields;

    public PosReportTxnDetailRspTypeData() {
    }

    public PosReportTxnDetailRspTypeData(
           java.lang.String txnStatus,
           java.lang.String cardType,
           java.lang.String maskedCardNbr,
           java.lang.String cardPresent,
           java.lang.String readerPresent,
           java.lang.String cardSwiped,
           java.lang.String debitCreditInd,
           java.lang.String saleReturnInd,
           java.lang.String CVVReq,
           java.lang.String CVVRsltCode,
           java.math.BigDecimal amt,
           java.math.BigDecimal authAmt,
           java.math.BigDecimal gratuityAmtInfo,
           java.math.BigDecimal cashbackAmtInfo,
           java.lang.String cardHolderLastName,
           java.lang.String cardHolderFirstName,
           java.lang.String cardHolderAddr,
           java.lang.String cardHolderCity,
           java.lang.String cardHolderState,
           java.lang.String cardHolderZip,
           java.lang.String cardHolderPhone,
           java.lang.String cardHolderEmail,
           java.lang.String AVSRsltCode,
           java.lang.String CPCReq,
           java.lang.String CPCInd,
           java.lang.String rspCode,
           java.lang.String rspText,
           java.lang.String authCode,
           java.lang.String reqACI,
           java.lang.String rspACI,
           java.lang.String mktSpecDataId,
           java.lang.String txnCode,
           java.lang.String acctDataSrc,
           java.lang.String authSrcCode,
           java.lang.String issTxnId,
           java.lang.String issValidationCode,
           java.lang.String cardHolderIdCode,
           java.lang.String networkIdCode,
           java.lang.String refNbr,
           int txnSeqNbr,
           java.lang.String directMktInvoiceNbr,
           int directMktShipMonth,
           int directMktShipDay,
           java.lang.String CPCCardHolderPONbr,
           java.lang.String CPCTaxType,
           java.math.BigDecimal CPCTaxAmt,
           java.math.BigDecimal settlementAmt,
           com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowPartialAuth,
           com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationReportType autoSubstantiation,
           java.lang.String attachmentType,
           java.lang.String attachmentData,
           java.lang.String attachmentFormat,
           com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
           this.txnStatus = txnStatus;
           this.cardType = cardType;
           this.maskedCardNbr = maskedCardNbr;
           this.cardPresent = cardPresent;
           this.readerPresent = readerPresent;
           this.cardSwiped = cardSwiped;
           this.debitCreditInd = debitCreditInd;
           this.saleReturnInd = saleReturnInd;
           this.CVVReq = CVVReq;
           this.CVVRsltCode = CVVRsltCode;
           this.amt = amt;
           this.authAmt = authAmt;
           this.gratuityAmtInfo = gratuityAmtInfo;
           this.cashbackAmtInfo = cashbackAmtInfo;
           this.cardHolderLastName = cardHolderLastName;
           this.cardHolderFirstName = cardHolderFirstName;
           this.cardHolderAddr = cardHolderAddr;
           this.cardHolderCity = cardHolderCity;
           this.cardHolderState = cardHolderState;
           this.cardHolderZip = cardHolderZip;
           this.cardHolderPhone = cardHolderPhone;
           this.cardHolderEmail = cardHolderEmail;
           this.AVSRsltCode = AVSRsltCode;
           this.CPCReq = CPCReq;
           this.CPCInd = CPCInd;
           this.rspCode = rspCode;
           this.rspText = rspText;
           this.authCode = authCode;
           this.reqACI = reqACI;
           this.rspACI = rspACI;
           this.mktSpecDataId = mktSpecDataId;
           this.txnCode = txnCode;
           this.acctDataSrc = acctDataSrc;
           this.authSrcCode = authSrcCode;
           this.issTxnId = issTxnId;
           this.issValidationCode = issValidationCode;
           this.cardHolderIdCode = cardHolderIdCode;
           this.networkIdCode = networkIdCode;
           this.refNbr = refNbr;
           this.txnSeqNbr = txnSeqNbr;
           this.directMktInvoiceNbr = directMktInvoiceNbr;
           this.directMktShipMonth = directMktShipMonth;
           this.directMktShipDay = directMktShipDay;
           this.CPCCardHolderPONbr = CPCCardHolderPONbr;
           this.CPCTaxType = CPCTaxType;
           this.CPCTaxAmt = CPCTaxAmt;
           this.settlementAmt = settlementAmt;
           this.allowPartialAuth = allowPartialAuth;
           this.autoSubstantiation = autoSubstantiation;
           this.attachmentType = attachmentType;
           this.attachmentData = attachmentData;
           this.attachmentFormat = attachmentFormat;
           this.additionalTxnFields = additionalTxnFields;
    }


    /**
     * Gets the txnStatus value for this PosReportTxnDetailRspTypeData.
     * 
     * @return txnStatus
     */
    public java.lang.String getTxnStatus() {
        return txnStatus;
    }


    /**
     * Sets the txnStatus value for this PosReportTxnDetailRspTypeData.
     * 
     * @param txnStatus
     */
    public void setTxnStatus(java.lang.String txnStatus) {
        this.txnStatus = txnStatus;
    }


    /**
     * Gets the cardType value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the maskedCardNbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @return maskedCardNbr
     */
    public java.lang.String getMaskedCardNbr() {
        return maskedCardNbr;
    }


    /**
     * Sets the maskedCardNbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @param maskedCardNbr
     */
    public void setMaskedCardNbr(java.lang.String maskedCardNbr) {
        this.maskedCardNbr = maskedCardNbr;
    }


    /**
     * Gets the cardPresent value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardPresent
     */
    public java.lang.String getCardPresent() {
        return cardPresent;
    }


    /**
     * Sets the cardPresent value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardPresent
     */
    public void setCardPresent(java.lang.String cardPresent) {
        this.cardPresent = cardPresent;
    }


    /**
     * Gets the readerPresent value for this PosReportTxnDetailRspTypeData.
     * 
     * @return readerPresent
     */
    public java.lang.String getReaderPresent() {
        return readerPresent;
    }


    /**
     * Sets the readerPresent value for this PosReportTxnDetailRspTypeData.
     * 
     * @param readerPresent
     */
    public void setReaderPresent(java.lang.String readerPresent) {
        this.readerPresent = readerPresent;
    }


    /**
     * Gets the cardSwiped value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardSwiped
     */
    public java.lang.String getCardSwiped() {
        return cardSwiped;
    }


    /**
     * Sets the cardSwiped value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardSwiped
     */
    public void setCardSwiped(java.lang.String cardSwiped) {
        this.cardSwiped = cardSwiped;
    }


    /**
     * Gets the debitCreditInd value for this PosReportTxnDetailRspTypeData.
     * 
     * @return debitCreditInd
     */
    public java.lang.String getDebitCreditInd() {
        return debitCreditInd;
    }


    /**
     * Sets the debitCreditInd value for this PosReportTxnDetailRspTypeData.
     * 
     * @param debitCreditInd
     */
    public void setDebitCreditInd(java.lang.String debitCreditInd) {
        this.debitCreditInd = debitCreditInd;
    }


    /**
     * Gets the saleReturnInd value for this PosReportTxnDetailRspTypeData.
     * 
     * @return saleReturnInd
     */
    public java.lang.String getSaleReturnInd() {
        return saleReturnInd;
    }


    /**
     * Sets the saleReturnInd value for this PosReportTxnDetailRspTypeData.
     * 
     * @param saleReturnInd
     */
    public void setSaleReturnInd(java.lang.String saleReturnInd) {
        this.saleReturnInd = saleReturnInd;
    }


    /**
     * Gets the CVVReq value for this PosReportTxnDetailRspTypeData.
     * 
     * @return CVVReq
     */
    public java.lang.String getCVVReq() {
        return CVVReq;
    }


    /**
     * Sets the CVVReq value for this PosReportTxnDetailRspTypeData.
     * 
     * @param CVVReq
     */
    public void setCVVReq(java.lang.String CVVReq) {
        this.CVVReq = CVVReq;
    }


    /**
     * Gets the CVVRsltCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return CVVRsltCode
     */
    public java.lang.String getCVVRsltCode() {
        return CVVRsltCode;
    }


    /**
     * Sets the CVVRsltCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param CVVRsltCode
     */
    public void setCVVRsltCode(java.lang.String CVVRsltCode) {
        this.CVVRsltCode = CVVRsltCode;
    }


    /**
     * Gets the amt value for this PosReportTxnDetailRspTypeData.
     * 
     * @return amt
     */
    public java.math.BigDecimal getAmt() {
        return amt;
    }


    /**
     * Sets the amt value for this PosReportTxnDetailRspTypeData.
     * 
     * @param amt
     */
    public void setAmt(java.math.BigDecimal amt) {
        this.amt = amt;
    }


    /**
     * Gets the authAmt value for this PosReportTxnDetailRspTypeData.
     * 
     * @return authAmt
     */
    public java.math.BigDecimal getAuthAmt() {
        return authAmt;
    }


    /**
     * Sets the authAmt value for this PosReportTxnDetailRspTypeData.
     * 
     * @param authAmt
     */
    public void setAuthAmt(java.math.BigDecimal authAmt) {
        this.authAmt = authAmt;
    }


    /**
     * Gets the gratuityAmtInfo value for this PosReportTxnDetailRspTypeData.
     * 
     * @return gratuityAmtInfo
     */
    public java.math.BigDecimal getGratuityAmtInfo() {
        return gratuityAmtInfo;
    }


    /**
     * Sets the gratuityAmtInfo value for this PosReportTxnDetailRspTypeData.
     * 
     * @param gratuityAmtInfo
     */
    public void setGratuityAmtInfo(java.math.BigDecimal gratuityAmtInfo) {
        this.gratuityAmtInfo = gratuityAmtInfo;
    }


    /**
     * Gets the cashbackAmtInfo value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cashbackAmtInfo
     */
    public java.math.BigDecimal getCashbackAmtInfo() {
        return cashbackAmtInfo;
    }


    /**
     * Sets the cashbackAmtInfo value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cashbackAmtInfo
     */
    public void setCashbackAmtInfo(java.math.BigDecimal cashbackAmtInfo) {
        this.cashbackAmtInfo = cashbackAmtInfo;
    }


    /**
     * Gets the cardHolderLastName value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderLastName
     */
    public java.lang.String getCardHolderLastName() {
        return cardHolderLastName;
    }


    /**
     * Sets the cardHolderLastName value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderLastName
     */
    public void setCardHolderLastName(java.lang.String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }


    /**
     * Gets the cardHolderFirstName value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderFirstName
     */
    public java.lang.String getCardHolderFirstName() {
        return cardHolderFirstName;
    }


    /**
     * Sets the cardHolderFirstName value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderFirstName
     */
    public void setCardHolderFirstName(java.lang.String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }


    /**
     * Gets the cardHolderAddr value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderAddr
     */
    public java.lang.String getCardHolderAddr() {
        return cardHolderAddr;
    }


    /**
     * Sets the cardHolderAddr value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderAddr
     */
    public void setCardHolderAddr(java.lang.String cardHolderAddr) {
        this.cardHolderAddr = cardHolderAddr;
    }


    /**
     * Gets the cardHolderCity value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderCity
     */
    public java.lang.String getCardHolderCity() {
        return cardHolderCity;
    }


    /**
     * Sets the cardHolderCity value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderCity
     */
    public void setCardHolderCity(java.lang.String cardHolderCity) {
        this.cardHolderCity = cardHolderCity;
    }


    /**
     * Gets the cardHolderState value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderState
     */
    public java.lang.String getCardHolderState() {
        return cardHolderState;
    }


    /**
     * Sets the cardHolderState value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderState
     */
    public void setCardHolderState(java.lang.String cardHolderState) {
        this.cardHolderState = cardHolderState;
    }


    /**
     * Gets the cardHolderZip value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderZip
     */
    public java.lang.String getCardHolderZip() {
        return cardHolderZip;
    }


    /**
     * Sets the cardHolderZip value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderZip
     */
    public void setCardHolderZip(java.lang.String cardHolderZip) {
        this.cardHolderZip = cardHolderZip;
    }


    /**
     * Gets the cardHolderPhone value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderPhone
     */
    public java.lang.String getCardHolderPhone() {
        return cardHolderPhone;
    }


    /**
     * Sets the cardHolderPhone value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderPhone
     */
    public void setCardHolderPhone(java.lang.String cardHolderPhone) {
        this.cardHolderPhone = cardHolderPhone;
    }


    /**
     * Gets the cardHolderEmail value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderEmail
     */
    public java.lang.String getCardHolderEmail() {
        return cardHolderEmail;
    }


    /**
     * Sets the cardHolderEmail value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderEmail
     */
    public void setCardHolderEmail(java.lang.String cardHolderEmail) {
        this.cardHolderEmail = cardHolderEmail;
    }


    /**
     * Gets the AVSRsltCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return AVSRsltCode
     */
    public java.lang.String getAVSRsltCode() {
        return AVSRsltCode;
    }


    /**
     * Sets the AVSRsltCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param AVSRsltCode
     */
    public void setAVSRsltCode(java.lang.String AVSRsltCode) {
        this.AVSRsltCode = AVSRsltCode;
    }


    /**
     * Gets the CPCReq value for this PosReportTxnDetailRspTypeData.
     * 
     * @return CPCReq
     */
    public java.lang.String getCPCReq() {
        return CPCReq;
    }


    /**
     * Sets the CPCReq value for this PosReportTxnDetailRspTypeData.
     * 
     * @param CPCReq
     */
    public void setCPCReq(java.lang.String CPCReq) {
        this.CPCReq = CPCReq;
    }


    /**
     * Gets the CPCInd value for this PosReportTxnDetailRspTypeData.
     * 
     * @return CPCInd
     */
    public java.lang.String getCPCInd() {
        return CPCInd;
    }


    /**
     * Sets the CPCInd value for this PosReportTxnDetailRspTypeData.
     * 
     * @param CPCInd
     */
    public void setCPCInd(java.lang.String CPCInd) {
        this.CPCInd = CPCInd;
    }


    /**
     * Gets the rspCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return rspCode
     */
    public java.lang.String getRspCode() {
        return rspCode;
    }


    /**
     * Sets the rspCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param rspCode
     */
    public void setRspCode(java.lang.String rspCode) {
        this.rspCode = rspCode;
    }


    /**
     * Gets the rspText value for this PosReportTxnDetailRspTypeData.
     * 
     * @return rspText
     */
    public java.lang.String getRspText() {
        return rspText;
    }


    /**
     * Sets the rspText value for this PosReportTxnDetailRspTypeData.
     * 
     * @param rspText
     */
    public void setRspText(java.lang.String rspText) {
        this.rspText = rspText;
    }


    /**
     * Gets the authCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return authCode
     */
    public java.lang.String getAuthCode() {
        return authCode;
    }


    /**
     * Sets the authCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param authCode
     */
    public void setAuthCode(java.lang.String authCode) {
        this.authCode = authCode;
    }


    /**
     * Gets the reqACI value for this PosReportTxnDetailRspTypeData.
     * 
     * @return reqACI
     */
    public java.lang.String getReqACI() {
        return reqACI;
    }


    /**
     * Sets the reqACI value for this PosReportTxnDetailRspTypeData.
     * 
     * @param reqACI
     */
    public void setReqACI(java.lang.String reqACI) {
        this.reqACI = reqACI;
    }


    /**
     * Gets the rspACI value for this PosReportTxnDetailRspTypeData.
     * 
     * @return rspACI
     */
    public java.lang.String getRspACI() {
        return rspACI;
    }


    /**
     * Sets the rspACI value for this PosReportTxnDetailRspTypeData.
     * 
     * @param rspACI
     */
    public void setRspACI(java.lang.String rspACI) {
        this.rspACI = rspACI;
    }


    /**
     * Gets the mktSpecDataId value for this PosReportTxnDetailRspTypeData.
     * 
     * @return mktSpecDataId
     */
    public java.lang.String getMktSpecDataId() {
        return mktSpecDataId;
    }


    /**
     * Sets the mktSpecDataId value for this PosReportTxnDetailRspTypeData.
     * 
     * @param mktSpecDataId
     */
    public void setMktSpecDataId(java.lang.String mktSpecDataId) {
        this.mktSpecDataId = mktSpecDataId;
    }


    /**
     * Gets the txnCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return txnCode
     */
    public java.lang.String getTxnCode() {
        return txnCode;
    }


    /**
     * Sets the txnCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param txnCode
     */
    public void setTxnCode(java.lang.String txnCode) {
        this.txnCode = txnCode;
    }


    /**
     * Gets the acctDataSrc value for this PosReportTxnDetailRspTypeData.
     * 
     * @return acctDataSrc
     */
    public java.lang.String getAcctDataSrc() {
        return acctDataSrc;
    }


    /**
     * Sets the acctDataSrc value for this PosReportTxnDetailRspTypeData.
     * 
     * @param acctDataSrc
     */
    public void setAcctDataSrc(java.lang.String acctDataSrc) {
        this.acctDataSrc = acctDataSrc;
    }


    /**
     * Gets the authSrcCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return authSrcCode
     */
    public java.lang.String getAuthSrcCode() {
        return authSrcCode;
    }


    /**
     * Sets the authSrcCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param authSrcCode
     */
    public void setAuthSrcCode(java.lang.String authSrcCode) {
        this.authSrcCode = authSrcCode;
    }


    /**
     * Gets the issTxnId value for this PosReportTxnDetailRspTypeData.
     * 
     * @return issTxnId
     */
    public java.lang.String getIssTxnId() {
        return issTxnId;
    }


    /**
     * Sets the issTxnId value for this PosReportTxnDetailRspTypeData.
     * 
     * @param issTxnId
     */
    public void setIssTxnId(java.lang.String issTxnId) {
        this.issTxnId = issTxnId;
    }


    /**
     * Gets the issValidationCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return issValidationCode
     */
    public java.lang.String getIssValidationCode() {
        return issValidationCode;
    }


    /**
     * Sets the issValidationCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param issValidationCode
     */
    public void setIssValidationCode(java.lang.String issValidationCode) {
        this.issValidationCode = issValidationCode;
    }


    /**
     * Gets the cardHolderIdCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return cardHolderIdCode
     */
    public java.lang.String getCardHolderIdCode() {
        return cardHolderIdCode;
    }


    /**
     * Sets the cardHolderIdCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param cardHolderIdCode
     */
    public void setCardHolderIdCode(java.lang.String cardHolderIdCode) {
        this.cardHolderIdCode = cardHolderIdCode;
    }


    /**
     * Gets the networkIdCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @return networkIdCode
     */
    public java.lang.String getNetworkIdCode() {
        return networkIdCode;
    }


    /**
     * Sets the networkIdCode value for this PosReportTxnDetailRspTypeData.
     * 
     * @param networkIdCode
     */
    public void setNetworkIdCode(java.lang.String networkIdCode) {
        this.networkIdCode = networkIdCode;
    }


    /**
     * Gets the refNbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @return refNbr
     */
    public java.lang.String getRefNbr() {
        return refNbr;
    }


    /**
     * Sets the refNbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @param refNbr
     */
    public void setRefNbr(java.lang.String refNbr) {
        this.refNbr = refNbr;
    }


    /**
     * Gets the txnSeqNbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @return txnSeqNbr
     */
    public int getTxnSeqNbr() {
        return txnSeqNbr;
    }


    /**
     * Sets the txnSeqNbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @param txnSeqNbr
     */
    public void setTxnSeqNbr(int txnSeqNbr) {
        this.txnSeqNbr = txnSeqNbr;
    }


    /**
     * Gets the directMktInvoiceNbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @return directMktInvoiceNbr
     */
    public java.lang.String getDirectMktInvoiceNbr() {
        return directMktInvoiceNbr;
    }


    /**
     * Sets the directMktInvoiceNbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @param directMktInvoiceNbr
     */
    public void setDirectMktInvoiceNbr(java.lang.String directMktInvoiceNbr) {
        this.directMktInvoiceNbr = directMktInvoiceNbr;
    }


    /**
     * Gets the directMktShipMonth value for this PosReportTxnDetailRspTypeData.
     * 
     * @return directMktShipMonth
     */
    public int getDirectMktShipMonth() {
        return directMktShipMonth;
    }


    /**
     * Sets the directMktShipMonth value for this PosReportTxnDetailRspTypeData.
     * 
     * @param directMktShipMonth
     */
    public void setDirectMktShipMonth(int directMktShipMonth) {
        this.directMktShipMonth = directMktShipMonth;
    }


    /**
     * Gets the directMktShipDay value for this PosReportTxnDetailRspTypeData.
     * 
     * @return directMktShipDay
     */
    public int getDirectMktShipDay() {
        return directMktShipDay;
    }


    /**
     * Sets the directMktShipDay value for this PosReportTxnDetailRspTypeData.
     * 
     * @param directMktShipDay
     */
    public void setDirectMktShipDay(int directMktShipDay) {
        this.directMktShipDay = directMktShipDay;
    }


    /**
     * Gets the CPCCardHolderPONbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @return CPCCardHolderPONbr
     */
    public java.lang.String getCPCCardHolderPONbr() {
        return CPCCardHolderPONbr;
    }


    /**
     * Sets the CPCCardHolderPONbr value for this PosReportTxnDetailRspTypeData.
     * 
     * @param CPCCardHolderPONbr
     */
    public void setCPCCardHolderPONbr(java.lang.String CPCCardHolderPONbr) {
        this.CPCCardHolderPONbr = CPCCardHolderPONbr;
    }


    /**
     * Gets the CPCTaxType value for this PosReportTxnDetailRspTypeData.
     * 
     * @return CPCTaxType
     */
    public java.lang.String getCPCTaxType() {
        return CPCTaxType;
    }


    /**
     * Sets the CPCTaxType value for this PosReportTxnDetailRspTypeData.
     * 
     * @param CPCTaxType
     */
    public void setCPCTaxType(java.lang.String CPCTaxType) {
        this.CPCTaxType = CPCTaxType;
    }


    /**
     * Gets the CPCTaxAmt value for this PosReportTxnDetailRspTypeData.
     * 
     * @return CPCTaxAmt
     */
    public java.math.BigDecimal getCPCTaxAmt() {
        return CPCTaxAmt;
    }


    /**
     * Sets the CPCTaxAmt value for this PosReportTxnDetailRspTypeData.
     * 
     * @param CPCTaxAmt
     */
    public void setCPCTaxAmt(java.math.BigDecimal CPCTaxAmt) {
        this.CPCTaxAmt = CPCTaxAmt;
    }


    /**
     * Gets the settlementAmt value for this PosReportTxnDetailRspTypeData.
     * 
     * @return settlementAmt
     */
    public java.math.BigDecimal getSettlementAmt() {
        return settlementAmt;
    }


    /**
     * Sets the settlementAmt value for this PosReportTxnDetailRspTypeData.
     * 
     * @param settlementAmt
     */
    public void setSettlementAmt(java.math.BigDecimal settlementAmt) {
        this.settlementAmt = settlementAmt;
    }


    /**
     * Gets the allowPartialAuth value for this PosReportTxnDetailRspTypeData.
     * 
     * @return allowPartialAuth
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.BooleanType getAllowPartialAuth() {
        return allowPartialAuth;
    }


    /**
     * Sets the allowPartialAuth value for this PosReportTxnDetailRspTypeData.
     * 
     * @param allowPartialAuth
     */
    public void setAllowPartialAuth(com.usatech.iso8583.interchange.heartland.posgateway.BooleanType allowPartialAuth) {
        this.allowPartialAuth = allowPartialAuth;
    }


    /**
     * Gets the autoSubstantiation value for this PosReportTxnDetailRspTypeData.
     * 
     * @return autoSubstantiation
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationReportType getAutoSubstantiation() {
        return autoSubstantiation;
    }


    /**
     * Sets the autoSubstantiation value for this PosReportTxnDetailRspTypeData.
     * 
     * @param autoSubstantiation
     */
    public void setAutoSubstantiation(com.usatech.iso8583.interchange.heartland.posgateway.AutoSubstantiationReportType autoSubstantiation) {
        this.autoSubstantiation = autoSubstantiation;
    }


    /**
     * Gets the attachmentType value for this PosReportTxnDetailRspTypeData.
     * 
     * @return attachmentType
     */
    public java.lang.String getAttachmentType() {
        return attachmentType;
    }


    /**
     * Sets the attachmentType value for this PosReportTxnDetailRspTypeData.
     * 
     * @param attachmentType
     */
    public void setAttachmentType(java.lang.String attachmentType) {
        this.attachmentType = attachmentType;
    }


    /**
     * Gets the attachmentData value for this PosReportTxnDetailRspTypeData.
     * 
     * @return attachmentData
     */
    public java.lang.String getAttachmentData() {
        return attachmentData;
    }


    /**
     * Sets the attachmentData value for this PosReportTxnDetailRspTypeData.
     * 
     * @param attachmentData
     */
    public void setAttachmentData(java.lang.String attachmentData) {
        this.attachmentData = attachmentData;
    }


    /**
     * Gets the attachmentFormat value for this PosReportTxnDetailRspTypeData.
     * 
     * @return attachmentFormat
     */
    public java.lang.String getAttachmentFormat() {
        return attachmentFormat;
    }


    /**
     * Sets the attachmentFormat value for this PosReportTxnDetailRspTypeData.
     * 
     * @param attachmentFormat
     */
    public void setAttachmentFormat(java.lang.String attachmentFormat) {
        this.attachmentFormat = attachmentFormat;
    }


    /**
     * Gets the additionalTxnFields value for this PosReportTxnDetailRspTypeData.
     * 
     * @return additionalTxnFields
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType getAdditionalTxnFields() {
        return additionalTxnFields;
    }


    /**
     * Sets the additionalTxnFields value for this PosReportTxnDetailRspTypeData.
     * 
     * @param additionalTxnFields
     */
    public void setAdditionalTxnFields(com.usatech.iso8583.interchange.heartland.posgateway.AdditionalTxnFieldsType additionalTxnFields) {
        this.additionalTxnFields = additionalTxnFields;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosReportTxnDetailRspTypeData)) return false;
        PosReportTxnDetailRspTypeData other = (PosReportTxnDetailRspTypeData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.txnStatus==null && other.getTxnStatus()==null) || 
             (this.txnStatus!=null &&
              this.txnStatus.equals(other.getTxnStatus()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.maskedCardNbr==null && other.getMaskedCardNbr()==null) || 
             (this.maskedCardNbr!=null &&
              this.maskedCardNbr.equals(other.getMaskedCardNbr()))) &&
            ((this.cardPresent==null && other.getCardPresent()==null) || 
             (this.cardPresent!=null &&
              this.cardPresent.equals(other.getCardPresent()))) &&
            ((this.readerPresent==null && other.getReaderPresent()==null) || 
             (this.readerPresent!=null &&
              this.readerPresent.equals(other.getReaderPresent()))) &&
            ((this.cardSwiped==null && other.getCardSwiped()==null) || 
             (this.cardSwiped!=null &&
              this.cardSwiped.equals(other.getCardSwiped()))) &&
            ((this.debitCreditInd==null && other.getDebitCreditInd()==null) || 
             (this.debitCreditInd!=null &&
              this.debitCreditInd.equals(other.getDebitCreditInd()))) &&
            ((this.saleReturnInd==null && other.getSaleReturnInd()==null) || 
             (this.saleReturnInd!=null &&
              this.saleReturnInd.equals(other.getSaleReturnInd()))) &&
            ((this.CVVReq==null && other.getCVVReq()==null) || 
             (this.CVVReq!=null &&
              this.CVVReq.equals(other.getCVVReq()))) &&
            ((this.CVVRsltCode==null && other.getCVVRsltCode()==null) || 
             (this.CVVRsltCode!=null &&
              this.CVVRsltCode.equals(other.getCVVRsltCode()))) &&
            ((this.amt==null && other.getAmt()==null) || 
             (this.amt!=null &&
              this.amt.equals(other.getAmt()))) &&
            ((this.authAmt==null && other.getAuthAmt()==null) || 
             (this.authAmt!=null &&
              this.authAmt.equals(other.getAuthAmt()))) &&
            ((this.gratuityAmtInfo==null && other.getGratuityAmtInfo()==null) || 
             (this.gratuityAmtInfo!=null &&
              this.gratuityAmtInfo.equals(other.getGratuityAmtInfo()))) &&
            ((this.cashbackAmtInfo==null && other.getCashbackAmtInfo()==null) || 
             (this.cashbackAmtInfo!=null &&
              this.cashbackAmtInfo.equals(other.getCashbackAmtInfo()))) &&
            ((this.cardHolderLastName==null && other.getCardHolderLastName()==null) || 
             (this.cardHolderLastName!=null &&
              this.cardHolderLastName.equals(other.getCardHolderLastName()))) &&
            ((this.cardHolderFirstName==null && other.getCardHolderFirstName()==null) || 
             (this.cardHolderFirstName!=null &&
              this.cardHolderFirstName.equals(other.getCardHolderFirstName()))) &&
            ((this.cardHolderAddr==null && other.getCardHolderAddr()==null) || 
             (this.cardHolderAddr!=null &&
              this.cardHolderAddr.equals(other.getCardHolderAddr()))) &&
            ((this.cardHolderCity==null && other.getCardHolderCity()==null) || 
             (this.cardHolderCity!=null &&
              this.cardHolderCity.equals(other.getCardHolderCity()))) &&
            ((this.cardHolderState==null && other.getCardHolderState()==null) || 
             (this.cardHolderState!=null &&
              this.cardHolderState.equals(other.getCardHolderState()))) &&
            ((this.cardHolderZip==null && other.getCardHolderZip()==null) || 
             (this.cardHolderZip!=null &&
              this.cardHolderZip.equals(other.getCardHolderZip()))) &&
            ((this.cardHolderPhone==null && other.getCardHolderPhone()==null) || 
             (this.cardHolderPhone!=null &&
              this.cardHolderPhone.equals(other.getCardHolderPhone()))) &&
            ((this.cardHolderEmail==null && other.getCardHolderEmail()==null) || 
             (this.cardHolderEmail!=null &&
              this.cardHolderEmail.equals(other.getCardHolderEmail()))) &&
            ((this.AVSRsltCode==null && other.getAVSRsltCode()==null) || 
             (this.AVSRsltCode!=null &&
              this.AVSRsltCode.equals(other.getAVSRsltCode()))) &&
            ((this.CPCReq==null && other.getCPCReq()==null) || 
             (this.CPCReq!=null &&
              this.CPCReq.equals(other.getCPCReq()))) &&
            ((this.CPCInd==null && other.getCPCInd()==null) || 
             (this.CPCInd!=null &&
              this.CPCInd.equals(other.getCPCInd()))) &&
            ((this.rspCode==null && other.getRspCode()==null) || 
             (this.rspCode!=null &&
              this.rspCode.equals(other.getRspCode()))) &&
            ((this.rspText==null && other.getRspText()==null) || 
             (this.rspText!=null &&
              this.rspText.equals(other.getRspText()))) &&
            ((this.authCode==null && other.getAuthCode()==null) || 
             (this.authCode!=null &&
              this.authCode.equals(other.getAuthCode()))) &&
            ((this.reqACI==null && other.getReqACI()==null) || 
             (this.reqACI!=null &&
              this.reqACI.equals(other.getReqACI()))) &&
            ((this.rspACI==null && other.getRspACI()==null) || 
             (this.rspACI!=null &&
              this.rspACI.equals(other.getRspACI()))) &&
            ((this.mktSpecDataId==null && other.getMktSpecDataId()==null) || 
             (this.mktSpecDataId!=null &&
              this.mktSpecDataId.equals(other.getMktSpecDataId()))) &&
            ((this.txnCode==null && other.getTxnCode()==null) || 
             (this.txnCode!=null &&
              this.txnCode.equals(other.getTxnCode()))) &&
            ((this.acctDataSrc==null && other.getAcctDataSrc()==null) || 
             (this.acctDataSrc!=null &&
              this.acctDataSrc.equals(other.getAcctDataSrc()))) &&
            ((this.authSrcCode==null && other.getAuthSrcCode()==null) || 
             (this.authSrcCode!=null &&
              this.authSrcCode.equals(other.getAuthSrcCode()))) &&
            ((this.issTxnId==null && other.getIssTxnId()==null) || 
             (this.issTxnId!=null &&
              this.issTxnId.equals(other.getIssTxnId()))) &&
            ((this.issValidationCode==null && other.getIssValidationCode()==null) || 
             (this.issValidationCode!=null &&
              this.issValidationCode.equals(other.getIssValidationCode()))) &&
            ((this.cardHolderIdCode==null && other.getCardHolderIdCode()==null) || 
             (this.cardHolderIdCode!=null &&
              this.cardHolderIdCode.equals(other.getCardHolderIdCode()))) &&
            ((this.networkIdCode==null && other.getNetworkIdCode()==null) || 
             (this.networkIdCode!=null &&
              this.networkIdCode.equals(other.getNetworkIdCode()))) &&
            ((this.refNbr==null && other.getRefNbr()==null) || 
             (this.refNbr!=null &&
              this.refNbr.equals(other.getRefNbr()))) &&
            this.txnSeqNbr == other.getTxnSeqNbr() &&
            ((this.directMktInvoiceNbr==null && other.getDirectMktInvoiceNbr()==null) || 
             (this.directMktInvoiceNbr!=null &&
              this.directMktInvoiceNbr.equals(other.getDirectMktInvoiceNbr()))) &&
            this.directMktShipMonth == other.getDirectMktShipMonth() &&
            this.directMktShipDay == other.getDirectMktShipDay() &&
            ((this.CPCCardHolderPONbr==null && other.getCPCCardHolderPONbr()==null) || 
             (this.CPCCardHolderPONbr!=null &&
              this.CPCCardHolderPONbr.equals(other.getCPCCardHolderPONbr()))) &&
            ((this.CPCTaxType==null && other.getCPCTaxType()==null) || 
             (this.CPCTaxType!=null &&
              this.CPCTaxType.equals(other.getCPCTaxType()))) &&
            ((this.CPCTaxAmt==null && other.getCPCTaxAmt()==null) || 
             (this.CPCTaxAmt!=null &&
              this.CPCTaxAmt.equals(other.getCPCTaxAmt()))) &&
            ((this.settlementAmt==null && other.getSettlementAmt()==null) || 
             (this.settlementAmt!=null &&
              this.settlementAmt.equals(other.getSettlementAmt()))) &&
            ((this.allowPartialAuth==null && other.getAllowPartialAuth()==null) || 
             (this.allowPartialAuth!=null &&
              this.allowPartialAuth.equals(other.getAllowPartialAuth()))) &&
            ((this.autoSubstantiation==null && other.getAutoSubstantiation()==null) || 
             (this.autoSubstantiation!=null &&
              this.autoSubstantiation.equals(other.getAutoSubstantiation()))) &&
            ((this.attachmentType==null && other.getAttachmentType()==null) || 
             (this.attachmentType!=null &&
              this.attachmentType.equals(other.getAttachmentType()))) &&
            ((this.attachmentData==null && other.getAttachmentData()==null) || 
             (this.attachmentData!=null &&
              this.attachmentData.equals(other.getAttachmentData()))) &&
            ((this.attachmentFormat==null && other.getAttachmentFormat()==null) || 
             (this.attachmentFormat!=null &&
              this.attachmentFormat.equals(other.getAttachmentFormat()))) &&
            ((this.additionalTxnFields==null && other.getAdditionalTxnFields()==null) || 
             (this.additionalTxnFields!=null &&
              this.additionalTxnFields.equals(other.getAdditionalTxnFields())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTxnStatus() != null) {
            _hashCode += getTxnStatus().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getMaskedCardNbr() != null) {
            _hashCode += getMaskedCardNbr().hashCode();
        }
        if (getCardPresent() != null) {
            _hashCode += getCardPresent().hashCode();
        }
        if (getReaderPresent() != null) {
            _hashCode += getReaderPresent().hashCode();
        }
        if (getCardSwiped() != null) {
            _hashCode += getCardSwiped().hashCode();
        }
        if (getDebitCreditInd() != null) {
            _hashCode += getDebitCreditInd().hashCode();
        }
        if (getSaleReturnInd() != null) {
            _hashCode += getSaleReturnInd().hashCode();
        }
        if (getCVVReq() != null) {
            _hashCode += getCVVReq().hashCode();
        }
        if (getCVVRsltCode() != null) {
            _hashCode += getCVVRsltCode().hashCode();
        }
        if (getAmt() != null) {
            _hashCode += getAmt().hashCode();
        }
        if (getAuthAmt() != null) {
            _hashCode += getAuthAmt().hashCode();
        }
        if (getGratuityAmtInfo() != null) {
            _hashCode += getGratuityAmtInfo().hashCode();
        }
        if (getCashbackAmtInfo() != null) {
            _hashCode += getCashbackAmtInfo().hashCode();
        }
        if (getCardHolderLastName() != null) {
            _hashCode += getCardHolderLastName().hashCode();
        }
        if (getCardHolderFirstName() != null) {
            _hashCode += getCardHolderFirstName().hashCode();
        }
        if (getCardHolderAddr() != null) {
            _hashCode += getCardHolderAddr().hashCode();
        }
        if (getCardHolderCity() != null) {
            _hashCode += getCardHolderCity().hashCode();
        }
        if (getCardHolderState() != null) {
            _hashCode += getCardHolderState().hashCode();
        }
        if (getCardHolderZip() != null) {
            _hashCode += getCardHolderZip().hashCode();
        }
        if (getCardHolderPhone() != null) {
            _hashCode += getCardHolderPhone().hashCode();
        }
        if (getCardHolderEmail() != null) {
            _hashCode += getCardHolderEmail().hashCode();
        }
        if (getAVSRsltCode() != null) {
            _hashCode += getAVSRsltCode().hashCode();
        }
        if (getCPCReq() != null) {
            _hashCode += getCPCReq().hashCode();
        }
        if (getCPCInd() != null) {
            _hashCode += getCPCInd().hashCode();
        }
        if (getRspCode() != null) {
            _hashCode += getRspCode().hashCode();
        }
        if (getRspText() != null) {
            _hashCode += getRspText().hashCode();
        }
        if (getAuthCode() != null) {
            _hashCode += getAuthCode().hashCode();
        }
        if (getReqACI() != null) {
            _hashCode += getReqACI().hashCode();
        }
        if (getRspACI() != null) {
            _hashCode += getRspACI().hashCode();
        }
        if (getMktSpecDataId() != null) {
            _hashCode += getMktSpecDataId().hashCode();
        }
        if (getTxnCode() != null) {
            _hashCode += getTxnCode().hashCode();
        }
        if (getAcctDataSrc() != null) {
            _hashCode += getAcctDataSrc().hashCode();
        }
        if (getAuthSrcCode() != null) {
            _hashCode += getAuthSrcCode().hashCode();
        }
        if (getIssTxnId() != null) {
            _hashCode += getIssTxnId().hashCode();
        }
        if (getIssValidationCode() != null) {
            _hashCode += getIssValidationCode().hashCode();
        }
        if (getCardHolderIdCode() != null) {
            _hashCode += getCardHolderIdCode().hashCode();
        }
        if (getNetworkIdCode() != null) {
            _hashCode += getNetworkIdCode().hashCode();
        }
        if (getRefNbr() != null) {
            _hashCode += getRefNbr().hashCode();
        }
        _hashCode += getTxnSeqNbr();
        if (getDirectMktInvoiceNbr() != null) {
            _hashCode += getDirectMktInvoiceNbr().hashCode();
        }
        _hashCode += getDirectMktShipMonth();
        _hashCode += getDirectMktShipDay();
        if (getCPCCardHolderPONbr() != null) {
            _hashCode += getCPCCardHolderPONbr().hashCode();
        }
        if (getCPCTaxType() != null) {
            _hashCode += getCPCTaxType().hashCode();
        }
        if (getCPCTaxAmt() != null) {
            _hashCode += getCPCTaxAmt().hashCode();
        }
        if (getSettlementAmt() != null) {
            _hashCode += getSettlementAmt().hashCode();
        }
        if (getAllowPartialAuth() != null) {
            _hashCode += getAllowPartialAuth().hashCode();
        }
        if (getAutoSubstantiation() != null) {
            _hashCode += getAutoSubstantiation().hashCode();
        }
        if (getAttachmentType() != null) {
            _hashCode += getAttachmentType().hashCode();
        }
        if (getAttachmentData() != null) {
            _hashCode += getAttachmentData().hashCode();
        }
        if (getAttachmentFormat() != null) {
            _hashCode += getAttachmentFormat().hashCode();
        }
        if (getAdditionalTxnFields() != null) {
            _hashCode += getAdditionalTxnFields().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosReportTxnDetailRspTypeData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosReportTxnDetailRspType>Data"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maskedCardNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MaskedCardNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardPresent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardPresent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("readerPresent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReaderPresent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardSwiped");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardSwiped"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debitCreditInd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DebitCreditInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleReturnInd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SaleReturnInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVVReq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CVVReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVVRsltCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CVVRsltCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Amt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gratuityAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "GratuityAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashbackAmtInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CashbackAmtInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderAddr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderAddr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderZip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderZip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSRsltCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AVSRsltCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCReq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCInd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reqACI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "ReqACI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rspACI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RspACI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mktSpecDataId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "MktSpecDataId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acctDataSrc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AcctDataSrc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authSrcCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AuthSrcCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issTxnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "IssTxnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issValidationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "IssValidationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderIdCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CardHolderIdCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("networkIdCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "NetworkIdCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "RefNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnSeqNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "TxnSeqNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktInvoiceNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktInvoiceNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktShipMonth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktShipMonth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directMktShipDay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "DirectMktShipDay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCCardHolderPONbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCCardHolderPONbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCTaxType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCTaxType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPCTaxAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "CPCTaxAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("settlementAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "SettlementAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowPartialAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AllowPartialAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "booleanType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("autoSubstantiation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AutoSubstantiation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AutoSubstantiationReportType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AttachmentFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalTxnFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "AdditionalTxnFieldsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
