/**
 * PosRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class PosRequest  implements java.io.Serializable {
    private com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10 ver10;

    private java.lang.String clientType;  // attribute

    private java.lang.String clientVer;  // attribute

    public PosRequest() {
    }

    public PosRequest(
           com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10 ver10,
           java.lang.String clientType,
           java.lang.String clientVer) {
           this.ver10 = ver10;
           this.clientType = clientType;
           this.clientVer = clientVer;
    }


    /**
     * Gets the ver10 value for this PosRequest.
     * 
     * @return ver10
     */
    public com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10 getVer10() {
        return ver10;
    }


    /**
     * Sets the ver10 value for this PosRequest.
     * 
     * @param ver10
     */
    public void setVer10(com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10 ver10) {
        this.ver10 = ver10;
    }


    /**
     * Gets the clientType value for this PosRequest.
     * 
     * @return clientType
     */
    public java.lang.String getClientType() {
        return clientType;
    }


    /**
     * Sets the clientType value for this PosRequest.
     * 
     * @param clientType
     */
    public void setClientType(java.lang.String clientType) {
        this.clientType = clientType;
    }


    /**
     * Gets the clientVer value for this PosRequest.
     * 
     * @return clientVer
     */
    public java.lang.String getClientVer() {
        return clientVer;
    }


    /**
     * Sets the clientVer value for this PosRequest.
     * 
     * @param clientVer
     */
    public void setClientVer(java.lang.String clientVer) {
        this.clientVer = clientVer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PosRequest)) return false;
        PosRequest other = (PosRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ver10==null && other.getVer10()==null) || 
             (this.ver10!=null &&
              this.ver10.equals(other.getVer10()))) &&
            ((this.clientType==null && other.getClientType()==null) || 
             (this.clientType!=null &&
              this.clientType.equals(other.getClientType()))) &&
            ((this.clientVer==null && other.getClientVer()==null) || 
             (this.clientVer!=null &&
              this.clientVer.equals(other.getClientVer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVer10() != null) {
            _hashCode += getVer10().hashCode();
        }
        if (getClientType() != null) {
            _hashCode += getClientType().hashCode();
        }
        if (getClientVer() != null) {
            _hashCode += getClientVer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PosRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">PosRequest"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("clientType");
        attrField.setXmlName(new javax.xml.namespace.QName("", "clientType"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("clientVer");
        attrField.setXmlName(new javax.xml.namespace.QName("", "clientVer"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ver10");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "Ver1.0"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", ">>PosRequest>Ver1.0"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
