package com.usatech.iso8583.interchange.heartland;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.interchange.heartland.posgateway.PosCreditAddToBatchReqType;
import com.usatech.iso8583.interchange.heartland.posgateway.PosRequestVer10Transaction;

public class AddToBatchAction extends HeartlandAction {
	private static int[] requiredFields = { 
		ISO8583Message.FIELD_ACQUIRER_ID, 
		ISO8583Message.FIELD_TERMINAL_ID, 
		ISO8583Message.FIELD_TRACE_NUMBER, 
		ISO8583Message.FIELD_AMOUNT, 
		ISO8583Message.FIELD_POS_ENVIRONMENT, 
		ISO8583Message.FIELD_ENTRY_MODE, 
		ISO8583Message.FIELD_ONLINE,
		ISO8583Message.FIELD_MISC_DATA
	};
	
	public AddToBatchAction(HeartlandInterchange interchange) {
		super(interchange);
	}
	
	@Override
	protected void initTransaction(ISO8583Request isoRequest, PosRequestVer10Transaction transaction) throws ValidationException
	{
		PosCreditAddToBatchReqType tranRequest = new PosCreditAddToBatchReqType();
		String gatewayTxnId = getMiscFieldValue(isoRequest, MISC_FIELD_GATEWAY_TXN_ID);
		if (gatewayTxnId == null || gatewayTxnId.length() == 0)
			throw new ValidationException("Required Field Not Found: " + MISC_FIELD_GATEWAY_TXN_ID);
		tranRequest.setGatewayTxnId(Integer.valueOf(gatewayTxnId));
		tranRequest.setAmt(getTransactionAmount(isoRequest));
		transaction.setCreditAddToBatch(tranRequest);
	}

	@Override
	protected int[] getRequiredFields() {
		return requiredFields;
	}
}

