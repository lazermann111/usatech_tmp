/**
 * AmtTypeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.iso8583.interchange.heartland.posgateway;

public class AmtTypeType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected AmtTypeType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _TOTAL_HEALTHCARE_AMT = "TOTAL_HEALTHCARE_AMT";
    public static final java.lang.String _SUBTOTAL_PRESCRIPTION_AMT = "SUBTOTAL_PRESCRIPTION_AMT";
    public static final java.lang.String _SUBTOTAL_VISION__OPTICAL_AMT = "SUBTOTAL_VISION__OPTICAL_AMT";
    public static final java.lang.String _SUBTOTAL_CLINIC_OR_OTHER_AMT = "SUBTOTAL_CLINIC_OR_OTHER_AMT";
    public static final java.lang.String _SUBTOTAL_DENTAL_AMT = "SUBTOTAL_DENTAL_AMT";
    public static final AmtTypeType TOTAL_HEALTHCARE_AMT = new AmtTypeType(_TOTAL_HEALTHCARE_AMT);
    public static final AmtTypeType SUBTOTAL_PRESCRIPTION_AMT = new AmtTypeType(_SUBTOTAL_PRESCRIPTION_AMT);
    public static final AmtTypeType SUBTOTAL_VISION__OPTICAL_AMT = new AmtTypeType(_SUBTOTAL_VISION__OPTICAL_AMT);
    public static final AmtTypeType SUBTOTAL_CLINIC_OR_OTHER_AMT = new AmtTypeType(_SUBTOTAL_CLINIC_OR_OTHER_AMT);
    public static final AmtTypeType SUBTOTAL_DENTAL_AMT = new AmtTypeType(_SUBTOTAL_DENTAL_AMT);
    public java.lang.String getValue() { return _value_;}
    public static AmtTypeType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        AmtTypeType enumeration = (AmtTypeType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static AmtTypeType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AmtTypeType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Hps.Exchange.PosGateway", "amtTypeType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
