package com.usatech.iso8583.interchange.elavon.jpos;

import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

import org.apache.commons.logging.LogFactory;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOFilter.VetoException;
import org.jpos.iso.ISOHeader;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.header.BaseHeader;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

import com.usatech.iso8583.interchange.elavon.jpos.message.ElavonISOMsg;
import com.usatech.iso8583.interchange.elavon.jpos.packager.ElavonHeartBeatPackager;
import com.usatech.iso8583.jpos.USATISOChannel;
import com.usatech.util.Conversions;

public class ElavonFrameRelayChannel extends BaseChannel implements USATISOChannel
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(ElavonFrameRelayChannel.class);

	private static final String ISO_HEADER = "ISO000MNIFKVGYA";
	//private static final byte[] HEARTBEAT_ID = { 0x00, 0x11 };
	//private static final byte[] REQUEST_ID = { 0x00, 0x31 };
	private static final byte[] RESPONSE_ID = { 0x00, 0x30 };
	private static final byte[] HEARTBEAT = { (byte) 0xAA, 0x77, 0x00, 0x0D, 0x00, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03 };
	private static final byte[] SYNC_BE = { (byte) 0xAA, 0x77 };
	private static final byte[] SYNC_LE = { (byte) 0xAA, 0x55 };
	private static final byte[] REQUEST_HEADER = { 0x00, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	private static final int BASE_LENGTH = 13;
	private static final int BASE_WITH_HEADER_LENGTH = 28;
	private static final byte ETX = 0x03;

	private long connectTime = -1;
	private long lastSend	 = -1;
	private long lastReceive = -1;

	public ElavonFrameRelayChannel(String host, int port, ISOPackager p)
	{
		super(host, port, p);
	}

	public ElavonFrameRelayChannel(ISOPackager p) throws IOException
	{
		super(p);
	}

	public ElavonFrameRelayChannel(ISOPackager p, ServerSocket serverSocket) throws IOException
	{
		super(p, serverSocket);
	}

	public String toString()
	{
		return "ElavonFrameRelayChannel(" + super.getHost() + ":" + super.getPort() + ")";
	}

	public long getConnectTime()
	{
		return connectTime;
	}
	
	public long getLastSend()
	{
		return lastSend;
	}

	public long getLastReceive()
	{
		return lastReceive;
	}

	protected Socket newSocket() throws IOException
	{
		try
		{
			if (log.isDebugEnabled())
				log.debug("Connecting to " + super.getHost() + ":" + super.getPort());
			Socket s = super.newSocket();
			connectTime = System.currentTimeMillis();
			lastReceive = -1;
			log.info("Successfully connected to " + super.getHost() + ":" + super.getPort());
			return s;
		}
		catch (IOException e)
		{
			log.warn("Failed to connect to " + super.getHost() + ":" + super.getPort() + ": " + e.getMessage());
			throw e;
		}
	}
	
	public void disconnect() throws IOException
	{
		log.info("Disconnecting from " + super.getHost() + ":" + super.getPort());
		super.disconnect();
	}

	protected void sendMessageLength(int len) throws IOException
	{
		if (len > 0xFFFF)
			throw new IOException("Maximum length exceeded!");

		serverOut.write(SYNC_BE);
		serverOut.write(len >> 8);		
		serverOut.write(len);
	}

	protected int getMessageLength() throws IOException, ISOException
	{
		byte[] b = new byte[2];
		serverIn.readFully(b, 0, 2);
		if(Arrays.equals(b, SYNC_BE) || Arrays.equals(b, SYNC_LE)){
			serverIn.readFully(b, 0, 2);
			return (int) (((((int) b[0]) & 0xFF) << 8) | (((int) b[1]) & 0xFF));
		} else
			throw new ISOException("Received invalid sync bytes: " + Conversions.bytesToHex(b));
	}

	protected int getHeaderLength()
	{
		return ISO_HEADER.length();
	}

	protected ISOHeader getDynamicHeader(byte[] image)
	{
		return new BaseHeader(ISO_HEADER.getBytes());
	}

	protected ISOMsg createMsg()
	{
		try
		{
			return new ElavonISOMsg();
		}
		catch (ISOException e)
		{
			LogEvent evt = new LogEvent(this, "createMsg");
			evt.addMessage(e);
			Logger.log(evt);
			return null;
		}
	}

	public void send(ISOMsg m) throws IOException, ISOException, VetoException
	{
		LogEvent evt = new LogEvent(this, "send");
		evt.addMessage(m);
		try
		{
			if (!isConnected())
				throw new ISOException("unconnected ISOChannel");

			m.setDirection(ISOMsg.OUTGOING);
			m = applyOutgoingFilters(m, evt);
			m.setDirection(ISOMsg.OUTGOING); // filter may have dropped this info
			byte[] b;
			synchronized (packager)
			{
				m.setPackager(getDynamicPackager(m));
				b = m.pack();
			}
			
			((ElavonISOMsg) m).setRawBytes(b);
			byte[] header = m.getHeader();
			if(header == null || header.length == 0)
				header = ISO_HEADER.getBytes();
			synchronized (serverOut)
			{
				if (m instanceof ElavonISOMsg && ((ElavonISOMsg) m).isHeartBeat())
				{
					// this is a heartbeat
					serverOut.write(HEARTBEAT);
					if (log.isDebugEnabled())
						log.debug("sent heartbeat: len=" + HEARTBEAT.length);
				}
				else
				{
					sendMessageLength(b.length + header.length + BASE_LENGTH);
					serverOut.write(REQUEST_HEADER);
					serverOut.write(header);
					serverOut.write(b, 0, b.length);
					serverOut.write(ETX);
                    if (m instanceof ElavonISOMsg)
                        log.info("Sending request to Elavon, Trace Number=" + ((ElavonISOMsg) m).getSystemTraceNumber());
					if (log.isDebugEnabled())
						log.debug("send: len=" + b.length + ", hex=" + Conversions.bytesToHex(b) + ", ascii=" + new String(b));
				}

				serverOut.flush();
			}
			cnt[TX]++;
			lastSend = System.currentTimeMillis();
			setChanged();
			notifyObservers(m);
		}
		catch (VetoException e)
		{
			if (log.isDebugEnabled())
				log.debug("send failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw e;
		}
		catch (ISOException e)
		{
			if (log.isDebugEnabled())
				log.debug("send failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw e;
		}
		catch (IOException e)
		{
			if (log.isDebugEnabled())
				log.debug("send failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw e;
		}
		catch (Exception e)
		{
			if (log.isDebugEnabled())
				log.debug("send failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw new ISOException("unexpected exception", e);
		}
		finally
		{
			Logger.log(evt);
		}
	}

	public ISOMsg receive() throws IOException, ISOException
	{
		int len = -1;
		byte[] b = null;
		byte[] header = null;
		LogEvent evt = new LogEvent(this, "receive");
		
		byte[] heartbeat = new byte[11];

		Socket socket = getSocket();
		ElavonISOMsg m = (ElavonISOMsg) createMsg();
		m.setSource(this);
		try
		{
			if (!isConnected())
				throw new ISOException("unconnected ISOChannel");

			synchronized (serverIn)
			{
				len = getMessageLength();

				if (len == BASE_LENGTH)
				{
					// this is a heartbeat					
					serverIn.readFully(heartbeat, 0, BASE_LENGTH - 2);
					if (log.isDebugEnabled())
						log.debug("received heartbeat: " + Conversions.bytesToHex(heartbeat));
				}
				else if (len > BASE_LENGTH)
				{
					byte[] messageId = new byte[2];
					serverIn.readFully(messageId, 0, 2);
					if (!Arrays.equals(messageId, RESPONSE_ID))
						throw new ISOException("Received invalid response message id: " + Conversions.bytesToHex(messageId));

					if (len <= 4096)
					{
						header = readHeader(BASE_LENGTH - 5);
						len -= header.length + 5;
						b = new byte[len];
						serverIn.readFully(b, 0, len);
						byte etx = serverIn.readByte();
						if (etx != ETX)
							throw new ISOException("Received invalid byte instead of ETX: " + etx);
					}
					else if(len <= 10)
					{
						b = new byte[len];
						serverIn.readFully(b, 0, len);
						throw new ISOException("received bytes too short: len= " + len + " bytes=" + Conversions.bytesToHex(b));
					}
					else
						throw new ISOException("received bytes too long: len= " + len);

					if (log.isDebugEnabled())
						log.debug("recv: len=" + len + ", bytes=" + Conversions.bytesToHex(b) + ", ascii=" + new String(b));
				}
				else
					throw new IOException("Received invalid message length: " + len);
			}
			if (len > BASE_LENGTH)
			{
				m.setHeader(getDynamicHeader(header));
				if (b.length > 0 && !shouldIgnore(header)) // Ignore NULL messages
				{
					synchronized (packager)
					{
						m.setPackager(getDynamicPackager(b));
						m.unpack(b);
					}
                    
                    log.info("Received response from Elavon, Trace Number=" + m.getSystemTraceNumber());
				}
			}

			m.setRawBytes(b);
			m.setDirection(ISOMsg.INCOMING);
			m = (ElavonISOMsg) applyIncomingFilters(m, header, b, evt);
			m.setDirection(ISOMsg.INCOMING);
			//log.debug("recv isoMsg=" + m);
			evt.addMessage(m);
			cnt[RX]++;
			lastReceive = System.currentTimeMillis();
			setChanged();
			notifyObservers(m);
		}
		catch (ISOException e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));
			
			if (log.isDebugEnabled())
				log.debug("receive failed: " + e.getMessage(), e);
			evt.addMessage(e);
			if (header != null)
			{
				evt.addMessage("--- header ---");
				evt.addMessage(ISOUtil.hexdump(header));
			}
			if (b != null)
			{
				evt.addMessage("--- data ---");
				evt.addMessage(ISOUtil.hexdump(b));
			}
			throw e;
		}
		catch (EOFException e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));

			if (log.isDebugEnabled())
				log.debug("receive failed: " + e.getMessage(), e);
			if (socket != null)
				socket.close();
			evt.addMessage("<peer-disconnect/>");
			throw e;
		}
		catch (InterruptedIOException e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));

			if (log.isDebugEnabled())
				log.debug("receive failed: " + e.getMessage(), e);
			if (socket != null)
				socket.close();
			evt.addMessage("<io-timeout/>");
			throw e;
		}
		catch (IOException e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));

			if (log.isDebugEnabled())
				log.debug("receive failed: " + e.getMessage(), e);
			if (socket != null)
				socket.close();
			if (usable)
				evt.addMessage(e);
			throw e;
		}
		catch (Exception e)
		{
			if(b != null)
				log.warn("receive failed for bytes: " + Conversions.bytesToHex(b));

			if (log.isDebugEnabled())
				log.debug("receive failed: " + e.getMessage(), e);
			evt.addMessage(e);
			throw new ISOException("unexpected exception", e);
		}
		finally
		{
			Logger.log(evt);
		}
		return m;
	}

	protected ISOPackager getDynamicPackager(ISOMsg m)
	{
		if (!m.hasField(0))
			return new ElavonHeartBeatPackager();

		return packager;
	}

	protected ISOPackager getDynamicPackager(byte[] image)
	{
		String mti = null;
		try
		{
			mti = new String(ISOUtil.bcd2str(image, 0, 4, true));
		}
		catch (Exception e)
		{
			LogEvent evt = new LogEvent(this, "getDynamicPackager");
			evt.addMessage(e);
			Logger.log(evt);
		}

		if (mti == null)
			return new ElavonHeartBeatPackager();
		else
			return packager;

	}
}
