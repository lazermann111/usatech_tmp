package com.usatech.iso8583.interchange.elavon.jpos.packager;

import org.jpos.iso.*;

public class ElavonHeartBeatPackager extends ISOBasePackager
{
	public ElavonHeartBeatPackager()
	{
		super();

	}

	protected int getMaxValidField()
	{
		return 0;
	}

	public byte[] pack(ISOComponent m) throws ISOException
	{
		return new byte[] {};
	}
}
