package com.usatech.iso8583.interchange.elavon;

import java.net.SocketTimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOException;

import simple.text.StringUtils;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.elavon.jpos.message.ElavonISOMsg;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.util.ISO8583Util;

public class AuthorizationAction extends ElavonAction
{
	private static Log log = LogFactory.getLog(AuthorizationAction.class);

	private static int[] requiredFields = { ISO8583Message.FIELD_ACQUIRER_ID, ISO8583Message.FIELD_TERMINAL_ID, ISO8583Message.FIELD_TRACE_NUMBER, ISO8583Message.FIELD_AMOUNT, ISO8583Message.FIELD_POS_ENVIRONMENT, ISO8583Message.FIELD_ENTRY_MODE };
	
	protected static boolean rightJustifyVisaCvv = false;
	protected static int specificationVersion = 1031;// v.1.031 is the oldest we used; v.1.035 includes everything but a new mobile indicator (Transaction Indicators, Terminal Type = 05); v.1.037 is
														// as advanced as this currently supports

	public AuthorizationAction(ElavonInterchange interchange)
	{
		super(interchange);
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException
	{
		super.validateRequest(request, requiredFields);

		if (!request.hasPanData() && !request.hasTrackData())
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_PAN_DATA) + " or " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACK_DATA));
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		ISO8583Request request = transaction.getRequest();
		transaction.setState(ISO8583Transaction.STATE_REQUEST_UNVALIDATED);

		ElavonISOMsg isoRequest = null;

		try
		{
			validateRequest(request, requiredFields);

			isoRequest = new ElavonISOMsg();
			if(specificationVersion >= 1035) {
				isoRequest.setHeader(ElavonUtil.getISOHeader(request.getPosEnvironment()));
				isoRequest.setPointOfServiceConditionCode("06");
				isoRequest.setTransactionIndicators(ElavonUtil.buildTransactionIndicators(request.isPartialAuthAllowed(), request.getPosEnvironment(), request.getPosEntryCapability(), request.getPinEntryCapability(), request.getPosIdentifier(), specificationVersion));

				if(ISO8583Message.POS_ENVIRONMENT_MAIL_TELEPHONE.equalsIgnoreCase(request.getPosEnvironment()))
					isoRequest.setPaymentIndicator("01");
				else if(ISO8583Message.POS_ENVIRONMENT_ECOMMERCE.equalsIgnoreCase(request.getPosEnvironment()))
					isoRequest.setPaymentIndicator("07");

				if(request.hasInvoiceNumber())
					isoRequest.setInvoiceNumber(request.getInvoiceNumber().length() > 25 ? request.getInvoiceNumber().substring(0, 25) : request.getInvoiceNumber());
				if(specificationVersion >= 1039) {
					String dynamicMerchantData = ElavonUtil.buildDynamicMerchantData(request);
					if(!StringUtils.isBlank(dynamicMerchantData))
						isoRequest.setDynamicMerchantData(dynamicMerchantData);
				}
			} else {
				isoRequest.setPointOfServiceConditionCode(ElavonUtil.buildPOSConditionCode(request.getPosEnvironment()));
				char[] pai = new char[] { request.isPartialAuthAllowed() ? '1' : '0', '0', '3', '0', '3' };
				isoRequest.setTransactionIndicators(new String(pai));
			}
			isoRequest.setMTI("0100");
			isoRequest.setProcessingCode("003000");
			//Elavon ISO8583 spec has the following two fields swapped
			isoRequest.setCardAcceptorAcquirerID(request.getTerminalID());
			isoRequest.setCardAcceptorTerminalID(request.getAcquirerID());
			isoRequest.setSystemTraceNumber(Integer.toString(request.getTraceNumber()));
			isoRequest.setTransactionAmount(Integer.toString(request.getAmount()));

			if (request.hasCvv2()) {
				String paddedCvv;
				if(rightJustifyVisaCvv && request.getPanData() != null && request.getPanData().getPan() != null && request.getPanData().getPan().startsWith("4"))
					paddedCvv = ISO8583Util.rightJustify(request.getCvv2(), 4, ' ');
				else
					paddedCvv = request.getCvv2();
				isoRequest.setCardholderVerificationData("1" + paddedCvv);
			}

			if (request.hasAvsZip())
				isoRequest.setAddressVerificationData(ISO8583Util.leftJustify(request.getAvsZip().replaceAll("[^A-Za-z0-9]", ""), 9, ' ') + ISO8583Util.leftJustify(request.hasAvsAddress() ? request.getAvsAddress().replaceAll("[^A-Za-z0-9 ]", "").replaceAll(" {2,}", " ") : "", 20, ' '));

			if (request.hasTrackData())
			{
				if (request.getTrackData().hasTrack1())
					isoRequest.setTrack1Data(request.getTrackData().getTrack1());
				if (request.getTrackData().hasTrack2())
					isoRequest.setTrack2Data(request.getTrackData().getTrack2());
			}
			else
			{
				isoRequest.setPrimaryAccountNumber(request.getPanData().getPan());
				isoRequest.setExpirationDate(request.getPanData().getExpiration());
			}
			isoRequest.setPointOfServiceEntryMode(ElavonUtil.buildPOSEntryModeString(request.getEntryMode()));
		}
		catch (ValidationException e)
		{
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		}
		catch (ISOException e)
		{
			transaction.setState(ISO8583Transaction.STATE_REQUEST_FAILED_VALIDATION);
			log.error(transaction + " failed input validation: " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_CLIENT_REQUEST_FAILED_VALIDATION, "Request failed input validation: " + e.getMessage());
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Caught unexpected exception validating " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Caught unexpected exception validating request: " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_REQUEST_VALIDATED);

		ElavonISOMsg isoResponse = null;

		try
		{
			isoResponse = sendRequest(isoRequest, transaction, (request.getHostResponseTimeout()*1000));
		}
		catch (SocketTimeoutException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.warn("Timeout occured waiting for host response for " + transaction);
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_TIMEOUT, "Timeout occured waiting for host response.");
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception transmitting " + transaction + ": " + e.getMessage(), e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error transmitting " + transaction + ": " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_UNVALIDATED);

		ISO8583Response response = null;

		try
		{
			response = readResponse(isoResponse);
		}
		catch (ValidationException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host response failed input validation: " + e.getMessage() + ", response: " + isoResponse);
		}
		catch (ISOException e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Response to " + transaction + " failed input validation: " + e.getMessage());
			return new ISO8583Response(ISO8583Response.ERROR_HOST_RESPONSE_FAILED_VALIDATION, "Host response failed input validation: " + e.getMessage() + ", response: " + isoResponse);
		}
		catch (Throwable e)
		{
			transaction.setState(ISO8583Transaction.STATE_NEEDS_RECOVERY);
			log.error("Caught unexpected exception while constructing response: " + transaction.getTransactionID() + ": " + e.getMessage() + ", response: " + isoResponse, e);
			return new ISO8583Response(ISO8583Response.ERROR_INTERNAL_ERROR, "Error constructing response: " + e.getMessage());
		}

		transaction.setState(ISO8583Transaction.STATE_RESPONSE_VALIDATED);

		return response;
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		//reversals will be done by POSMLayer
		return true;
	}

	public static boolean isRightJustifyVisaCvv() {
		return rightJustifyVisaCvv;
	}

	public static void setRightJustifyVisaCvv(boolean rightJustifyVisaCvv) {
		AuthorizationAction.rightJustifyVisaCvv = rightJustifyVisaCvv;
	}

	public static int getSpecificationVersion() {
		return specificationVersion;
	}

	public static void setSpecificationVersion(int specificationVersion) {
		AuthorizationAction.specificationVersion = specificationVersion;
	}
}
