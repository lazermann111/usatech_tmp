package com.usatech.iso8583.interchange.elavon;

import java.net.SocketTimeoutException;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.elavon.jpos.message.ElavonISOMsg;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.util.CreditCardValidator;

public abstract class ElavonAction
{
	public abstract ISO8583Response process(ISO8583Transaction transaction);

	public abstract boolean recover(ISO8583Transaction transaction);

	private ElavonInterchange interchange;
	private final char FS = 0x1C;

	protected ElavonAction(ElavonInterchange interchange)
	{
		this.interchange = interchange;
	}

	public ElavonInterchange getInterchange()
	{
		return interchange;
	}

	protected void validateRequest(ISO8583Request request, int[] requiredFields) throws ValidationException
	{
		for (int i = 0; i < requiredFields.length; i++)
		{
			if (!request.hasField(requiredFields[i]))
				throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(requiredFields[i]));
		}

		if (request.hasTrackData())
		{
			if (!request.getTrackData().hasTrack1() && !request.getTrackData().hasTrack2())
				throw new ValidationException("Required Field Not Found: TrackData Tracks");

			try
			{
				CreditCardValidator.validateTrackData(request.getTrackData());
			}
			catch (NumberFormatException e)
			{
				throw new ValidationException("Invalid Track Data: " + e.getMessage());
			}
		}

		if (request.hasPanData())
		{
			if (request.getPanData().getPan() == null)
				throw new ValidationException("Required Field Not Found: PanData PAN");

			if (request.getPanData().getExpiration() == null)
				throw new ValidationException("Required Field Not Found: PanData Expiration");

			try
			{
				CreditCardValidator.validatePanData(request.getPanData());
			}
			catch (NumberFormatException e)
			{
				throw new ValidationException("Invalid Pan Data: " + e.getMessage());
			}
		}

		if (request.hasAvsAddress() && !request.hasAvsZip())
			throw new ValidationException("Required Field Not Found: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_AVS_ZIP) + " is required with " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_AVS_ADDRESS));
	}

	protected ElavonISOMsg sendRequest(ElavonISOMsg isoRequest, ISO8583Transaction transaction) throws Exception
	{
		return sendRequest(isoRequest, transaction, -1);
	}
	
	protected ElavonISOMsg sendRequest(ElavonISOMsg isoRequest, ISO8583Transaction transaction, int hostResponseTimeout) throws Exception
	{
		ElavonISOMsg isoResponse = null;

		
		transaction.setState(ISO8583Transaction.STATE_REQUEST_TRANSMITTED);
		
		// this will queue the request and block until we get a response or timeout
		isoResponse = interchange.send(isoRequest, hostResponseTimeout);

		if (isoResponse == null)
			throw new SocketTimeoutException();

		return isoResponse;
	}

	protected ISO8583Response readResponse(ElavonISOMsg isoResponse) throws Exception
	{
		return readResponse(isoResponse, true);
	}

	protected ISO8583Response readResponse(ElavonISOMsg isoResponse, boolean requireEffectiveDate) throws Exception
	{
		ISO8583Response response = new ISO8583Response();
		
		//Action Code
		String responseCode = isoResponse.getResponseCode();
		if (responseCode == null)
			throw new ValidationException("Response Did Not Contain a Required Field: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_RESPONSE_CODE));
		response.setResponseCode(responseCode);
		boolean approved = responseCode.equalsIgnoreCase("00");
		
		//Systems Trace Audit Nbr
		String systemTraceNumber = isoResponse.getSystemTraceNumber();
		if (systemTraceNumber == null)
			throw new ValidationException("Response Did Not Contain a Required Field: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_TRACE_NUMBER));
		response.setTraceNumber(Integer.parseInt(systemTraceNumber));		

		String approvalCode = isoResponse.getAuthorizationIdentificationResponse();
		if (approvalCode != null)
			response.setApprovalCode(approvalCode);
		else if (approved)
			throw new ValidationException("Response Did Not Contain a Required Field: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_APPROVAL_CODE));
		
		String tranLocalDate = isoResponse.getTransactionLocalDate();
		String tranLocalTime = isoResponse.getTransactionLocalTime();
		if (tranLocalDate != null && tranLocalTime != null)
			response.setEffectiveDate(ElavonUtil.parseDateTime(tranLocalDate, tranLocalTime));
		else if(approved && requireEffectiveDate)
			throw new ValidationException("Response Did Not Contain a Required Field: " + ISO8583Message.FIELD_NAMES.get(ISO8583Message.FIELD_EFFECTIVE_DATE));
		
		String transactionAmount = isoResponse.getTransactionAmount();
		if (transactionAmount != null)
			response.setAmount(Integer.parseInt(transactionAmount));		

		String cvv2ResultCode = isoResponse.getCVV2ResultCode(); 
		if (cvv2ResultCode != null)
			response.setCvv2Result(cvv2ResultCode);

		String avsResponseCode = isoResponse.getAVSResponseCode();
		if (avsResponseCode != null)
			response.setAvsResult(avsResponseCode);
		
		String retrievalReferenceNumber = isoResponse.getRetrievalReferenceNumber();
		if (retrievalReferenceNumber != null)
			response.setRetrievalReferenceNumber(retrievalReferenceNumber);
		
		StringBuilder sb = new StringBuilder();
		
		String elavonAuthorizationData = isoResponse.getElavonAuthorizationData();
		if (elavonAuthorizationData != null) {
			sb.append("elavonAuthorizationData=");
			sb.append(elavonAuthorizationData);
			sb.append(FS);
			
			if (elavonAuthorizationData.length() > 10)
				response.setResponseMessage(elavonAuthorizationData.substring(10).trim());
		}
		
		String additionalAmounts = isoResponse.getAdditionalAmounts();
		if (additionalAmounts != null) {
			sb.append("additionalAmounts=");
			sb.append(additionalAmounts);
			sb.append(FS);
		}
		
		String cpsData = isoResponse.getCPSData();
		if (cpsData != null) {
			sb.append("CPSData=");
			sb.append(cpsData);
			sb.append(FS);
		}
		
		String merchantDefinedData = isoResponse.getMerchantDefinedData();
		if (merchantDefinedData != null) {
			sb.append("merchantDefinedData=");
			sb.append(merchantDefinedData);
			sb.append(FS);
		}
		
		if (sb.length() > 0)
			response.setMiscData(sb.toString());

		return response;
	}
}
