package com.usatech.iso8583.interchange.elavon;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequest;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Response;
import com.usatech.iso8583.interchange.ISO8583Interchange;
import com.usatech.iso8583.interchange.elavon.jpos.ElavonFrameRelayChannel;
import com.usatech.iso8583.interchange.elavon.jpos.ElavonMUX;
import com.usatech.iso8583.interchange.elavon.jpos.ElavonPersistentMUX;
import com.usatech.iso8583.interchange.elavon.jpos.message.ElavonISOMsg;
import com.usatech.iso8583.interchange.elavon.jpos.packager.ElavonPackager;
import com.usatech.iso8583.jpos.SeparateLineLog4JListener;
import com.usatech.iso8583.jpos.USATChannelPool;
import com.usatech.iso8583.jpos.USATISOMUX;
import com.usatech.iso8583.transaction.HsqlISO8583TransactionManager;
import com.usatech.iso8583.transaction.ISO8583Transaction;
import com.usatech.iso8583.transaction.ISO8583TransactionManager;
import com.usatech.iso8583.util.ISO8583GatewayConfiguration;

public class ElavonInterchange implements ISO8583Interchange
{
	public static final String INTERCHANGE_NAME = "Elavon_ISO8583";

	private static org.apache.commons.logging.Log log = LogFactory.getLog(ElavonInterchange.class);

	public static void main(String args[]) throws Exception
	{
		Configuration config = new ISO8583GatewayConfiguration();
		ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		HsqlISO8583TransactionManager txnManager = new HsqlISO8583TransactionManager(threadPool, config);
		txnManager.start();
		ElavonInterchange interchange = new ElavonInterchange(threadPool, txnManager, config);

		interchange.start();

		Thread.sleep(25000);
		interchange.stop();
	}

	private boolean isStarted = false;
	private int responseTimeout;
	private long reconnectDelay;
	private Configuration config;

	private final ThreadPoolExecutor threadPool;
	private final ISO8583TransactionManager txnManager;
	private final Map<String, ElavonAction> actions = new HashMap<String, ElavonAction>();

	private List<String>hosts;
	private int jposLogLevel;

	private USATISOMUX mux;

	private ScheduledExecutorService executor;

	public ElavonInterchange(ThreadPoolExecutor threadPool, ISO8583TransactionManager txnManager, Configuration config) throws ConfigurationException
	{
		this.threadPool = threadPool;
		this.config = config;
		this.txnManager = txnManager;

		if (config instanceof FileConfiguration)
		{
			((FileConfiguration) config).setReloadingStrategy(new ConfigReloader());
		}

		actions.put(ISO8583Message.TRANSACTION_TYPE_AUTHORIZATION, new AuthorizationAction(this));
	}
	
	public boolean isSimulationMode()
	{
		return false;
	}	

	public String getName()
	{
		return INTERCHANGE_NAME;
	}

	public ISO8583TransactionManager getTransactionManager()
	{
		return txnManager;
	}

	public USATISOMUX getMUX()
	{
		return mux;
	}

	public ThreadPoolExecutor getThreadPool()
	{
		return threadPool;
	}

	@Override
	public String toString()
	{
		return "ElavonInterchange[started=" + isStarted() + " connected=" + isConnected() + "]";
	}

	public boolean canSend()
	{
		if (mux == null)
			return false;
		if (mux.getISOChannel() == null)
			return false;
		if (mux.isTerminating())
			return false;
		if (!mux.isConnected()) {
			try {
				mux.getISOChannel().connect();
			} catch (Exception e) { }
			if (!mux.isConnected())
				return false;
		}

		return true;
	}

	public ElavonISOMsg send(ElavonISOMsg isoMsg)
	{
		return send(isoMsg, responseTimeout);
	}

	public ElavonISOMsg send(ElavonISOMsg isoMsg, int hostResponseTimeout)
	{
		ISORequest request = new ISORequest(isoMsg);

		mux.queue(request);

		if(hostResponseTimeout < 0)
			hostResponseTimeout = this.responseTimeout;

		ElavonISOMsg response = (ElavonISOMsg) request.getResponse(hostResponseTimeout);
		if(log.isDebugEnabled() && !isoMsg.isHeartBeat()) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(baos);
			request.dump(ps, "");
			ps.flush();
			log.debug(baos.toString());
		}
		return response;
	}

	public synchronized void setConfiguration(Configuration config)
	{
		if (isStarted)
			reloadConfig(config);
		else
			this.config = config;
	}

	public synchronized Configuration getConfiguration()
	{
		return config;
	}

	private synchronized void loadConfig()
	{
		log.info("Loading configuration...");

		responseTimeout = config.getInt("elavon.response.timeout");
		if (responseTimeout < 0)
			log.warn("Configuration value elavon.response.timeout of " + responseTimeout + " looks invalid, interchange may not function properly!");

		hosts = new ArrayList<String>();
		for(Object h : config.getList("elavon.host"))
			if(h != null)
				hosts.add(h.toString());
		// note: you must use a new instance of org.jpos.util.Logger and Log4JListener for each loggable component or deadlock will result
		jposLogLevel = config.getInt("elavon.jpos.log4j.level", -1);
		reconnectDelay = config.getLong("elavon.mux.reconnectDelay");

		mux = loadMUX(hosts, null, null, jposLogLevel, true);
		log.info("Loaded new peristent MUX: " + mux);
	}

	private USATISOMUX loadMUX(List<String> hosts, String testMerchant, String testTerminal, int jposLogLevel, boolean persistent)
	{
		ElavonPackager packager = new ElavonPackager();
		if (jposLogLevel > 0)
		{
			org.jpos.util.Logger packagerLogger = new org.jpos.util.Logger();
			packagerLogger.addListener(new SeparateLineLog4JListener(jposLogLevel));
			packager.setLogger(packagerLogger, "ElavonPackager");
		}

		USATChannelPool channelPool = new USATChannelPool();

		for (String host : hosts)
		{
			try
			{
				Scanner scanner = new Scanner(host);
				scanner.useDelimiter(":");
				String ip = scanner.next();
				int port = scanner.nextInt();
				ElavonFrameRelayChannel channel = new ElavonFrameRelayChannel(ip, port, packager);
				// set the socket read timeout to a value slightly larger than the heartbeat time
				if(persistent)
					channel.setTimeout(140*1000);
				else
					channel.setTimeout(60*1000);
				channelPool.addChannel(channel);
				if (jposLogLevel > 0)
				{
					org.jpos.util.Logger channelLogger = new org.jpos.util.Logger();
					channelLogger.addListener(new SeparateLineLog4JListener(jposLogLevel));
					channel.setLogger(channelLogger, "ElavonFrameRelayChannel");
				}
				log.info("Loaded new ElavonFrameRelayChannel for " + host);
			}
			catch (Exception e)
			{
				log.error("Failed to load Elavon host: " + host + ": " + e.getMessage(), e);
			}
		}

		USATISOMUX myMUX = null;

		if(persistent)
		{
			myMUX = new ElavonPersistentMUX(channelPool, threadPool, true, testMerchant, testTerminal);
			myMUX.setReconnectDelay(reconnectDelay);
		}
		else
			myMUX = new ElavonMUX(channelPool, threadPool, false);

		myMUX.setISORequestListener(new UnmatchedResponseHandler());

		return myMUX;
	}

	private synchronized void reloadConfig(Configuration config)
	{
		this.config = config;

		log.info("Terminating Channels...");

		mux.terminate();

		try
		{
			Thread.sleep(1000);
		}
		catch (Exception e)
		{
		}

		loadConfig();
	}

	public synchronized void start()
	{
		if (isStarted)
			return;

		log.info("ElavonInterchange Starting up...");
		loadConfig();

		executor = Executors.newSingleThreadScheduledExecutor();

		isStarted = true;
	}

	public synchronized void stop()
	{
		if (!isStarted)
			return;

		log.info("ElavonInterchange Shutting down...");

		executor.shutdownNow();

		if(mux != null)
			mux.terminate();

		isStarted = false;
	}

	public boolean isStarted()
	{
		return isStarted;
	}

	public boolean isConnected()
	{
		if (mux == null)
			return false;

		return mux.isConnected();
	}

	public ISO8583Response process(ISO8583Transaction transaction)
	{
		String transactionType = transaction.getRequest().getTransactionType();
		ElavonAction action = actions.get(transactionType);
		if (action == null)
		{
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Processing failed: Unsupported transaction type: " + transactionType);
			return new ISO8583Response(ISO8583Response.ERROR_UNSUPPORTED_TRANSACTION_TYPE, "Unsupported transaction type: " + transactionType);
		}

		if (!canSend())
		{
			transaction.setState(ISO8583Transaction.STATE_ERROR_PRE_TRANSMIT);
			log.error("Processing failed: Interchange is not currently connected to the host");
			return new ISO8583Response(ISO8583Response.ERROR_HOST_CONNECTION_FAILURE, "Interchange is not currently connected to the host");
		}

		return action.process(transaction);
	}

	public boolean recover(ISO8583Transaction transaction)
	{
		String transactionType = transaction.getRequest().getTransactionType();
		ElavonAction action = actions.get(transactionType);
		if (action == null)
		{
			log.error("Recovery failed: Unsupported transaction type: " + transactionType);
			return true;
		}

		try
		{
			return action.recover(transaction);
		}
		catch (Throwable e)
		{
			log.error("Caught unexpected exception while attempting recovery of " + transaction + ": " + e.getMessage(), e);
			return false;
		}
	}

	private class UnmatchedResponseHandler implements ISORequestListener
	{
		public boolean process(ISOSource source, ISOMsg isoMsg)
		{
			if (isoMsg instanceof ElavonISOMsg)
			{
				try
				{
					ElavonISOMsg elavonMsg = (ElavonISOMsg) isoMsg;
					if(elavonMsg.isHeartBeat())
					{
						log.debug("Heartbeat IN : " + elavonMsg);
					}
					else if(elavonMsg.getMTI() != null && elavonMsg.getMTI().equals("0810"))
					{
						log.debug("Test Transaction IN: " + elavonMsg);
					}
				}
				catch(Exception e)
				{
					log.error("Failed to process Unmatched Response: " + isoMsg + ": " + e.getMessage(), e);
				}
			}
			else
			{
				//TODO: Handle this response somehow
				log.warn("Received unmatched message from Elavon Host: " + isoMsg);
			}

			return true;
		}
	}

	private class ConfigReloader extends org.apache.commons.configuration.reloading.FileChangedReloadingStrategy
	{
		private ConfigReloader()
		{
			super();
		}

		@Override
		public void reloadingPerformed()
		{
			super.reloadingPerformed();
			log.warn("ElavonInterchange configuration change detected! Please stop/start the interchange for the change to take effect...");
		}
	}
}
