package com.usatech.iso8583.interchange.elavon.jpos;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

import com.usatech.iso8583.interchange.elavon.jpos.message.ElavonISOMsg;
import com.usatech.iso8583.jpos.USATISOChannel;
import com.usatech.iso8583.jpos.USATISOMUX;

public class ElavonMUX extends USATISOMUX
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(ElavonMUX.class);

	protected long lastReset = -99;
	protected AtomicInteger	sequenceNumberCounter = new AtomicInteger(1);

	public ElavonMUX(USATISOChannel channel, ThreadPoolExecutor threadPool, boolean doConnect)
	{
		super(channel, threadPool, doConnect);
	}

	protected synchronized String getKey(ISOMsg isoMsg) throws ISOException
	{
		if(getISOChannel().getConnectTime() > lastReset)
		{
			lastReset = System.currentTimeMillis();
			sequenceNumberCounter.set(1);
		}
		
		ElavonISOMsg elavonMsg = (ElavonISOMsg) isoMsg;

		if (elavonMsg.getDirection() == ISOMsg.INCOMING)
		{
			String key = elavonMsg.getSequenceNumber();
			if (log.isDebugEnabled() && key != null)
				log.debug("Incoming MUX key: " + key); // don't log heartbeats
			return key;
		}

		String sequenceNumber = ISOUtil.zeropad(Integer.toString(sequenceNumberCounter.getAndIncrement()), 6);
		sequenceNumberCounter.compareAndSet(1000000, 1);

		if (log.isDebugEnabled())
			log.debug("Outgoing MUX key: " + sequenceNumber);

		elavonMsg.setSequenceNumber(sequenceNumber);
		return sequenceNumber;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("ElavonMUX[");
		sb.append("persistent=false");
		sb.append(" doConnect="+super.getConnect());
		sb.append(" connected="+super.isConnected());
		sb.append(" terminating="+super.isTerminating());
		sb.append(" sequenceNumberCounter="+sequenceNumberCounter.get());
		sb.append("]");
		return sb.toString();
	}
}
