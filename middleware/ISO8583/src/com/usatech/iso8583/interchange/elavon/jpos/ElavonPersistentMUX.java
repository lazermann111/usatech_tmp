package com.usatech.iso8583.interchange.elavon.jpos;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.LogFactory;
import org.jpos.iso.ISOMsg;

import com.usatech.iso8583.interchange.elavon.jpos.message.ElavonISOMsg;
import com.usatech.iso8583.jpos.USATISOChannel;

public class ElavonPersistentMUX extends ElavonMUX
{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(ElavonPersistentMUX.class);

	private String testMerchant;
	private String testTerminal;

	private ScheduledExecutorService executor;
	private ScheduledFuture future;

	private long lastTestSent = -99;

	public ElavonPersistentMUX(USATISOChannel channel, ThreadPoolExecutor threadPool, boolean doConnect, String testMerchant, String testTerminal)
	{
		super(channel, threadPool, doConnect);
		
		this.testMerchant = testMerchant;
		this.testTerminal = testTerminal;
		
		executor = Executors.newSingleThreadScheduledExecutor();
		future = executor.scheduleWithFixedDelay(new PersistentSender(), 5, 5, TimeUnit.SECONDS);
	}
	
	public void terminate()
	{
		super.terminate();
		
		log.warn("ElavonPersistentMUX Shutting down...");
		
		executor.shutdownNow();
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("ElavonMUX[");
		sb.append("persistent=true");
		sb.append(" doConnect="+super.getConnect());
		sb.append(" connected="+super.isConnected());
		sb.append(" terminating="+super.isTerminating());
		sb.append(" sequenceNumberCounter="+sequenceNumberCounter.get());
		sb.append("]");
		return sb.toString();
	}

	protected class PersistentSender implements Runnable
	{
		public void run()
		{
			if (!isConnected())
				return;
	
			long receiveET = System.currentTimeMillis() - (getISOChannel().getLastReceive() > 0 ? getISOChannel().getLastReceive() : getISOChannel().getConnectTime());
			long sendET = System.currentTimeMillis() - getISOChannel().getLastSend();
			if (receiveET > 90000)
			{
				log.warn("Have not received a message from the host in the last " + receiveET + "ms!  Forcing reconnect...");
	
				try
				{
					getISOChannel().reconnect();
				}
				catch (IOException e)
				{
					log.error("Failed to force reconnect: " + e.getMessage(), e);
				}
			}
			else if (sendET > 30000)
			{
				ElavonISOMsg isoMsg = null;
	
				try
				{
					isoMsg = new ElavonISOMsg();
					isoMsg.setDirection(ISOMsg.OUTGOING);
					if (log.isDebugEnabled())
						log.debug("Heartbeat OUT : " + isoMsg);
					send(isoMsg);
				}
				catch (Exception e)
				{
					log.error("Failed to send Heartbeat:  " + isoMsg + ": " + e.getMessage(), e);
				}
			}
		}
	}
}
