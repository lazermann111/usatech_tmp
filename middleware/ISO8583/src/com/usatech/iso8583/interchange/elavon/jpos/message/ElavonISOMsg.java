package com.usatech.iso8583.interchange.elavon.jpos.message;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

public class ElavonISOMsg extends ISOMsg
{
	protected byte[] rawBytes;

	public ElavonISOMsg() throws ISOException
	{
		super();
	}

	public ElavonISOMsg(ISOMsg m) throws ISOException
	{
		super();
		this.setPackager(m.getPackager());
		this.unpack(m.pack());
		this.setDirection(m.getDirection());
		this.setHeader(m.getHeader());
		this.setSource(m.getSource());
	}
	
	public void setRawBytes(byte[] bytes)
	{
		this.rawBytes = bytes;
	}
	
	public byte[] getRawBytes()
	{
		return rawBytes;
	}

	public String getPrimaryAccountNumber() throws ISOException
	{
		if (!hasField(2))
			return null;
		return (String) this.getValue(2);
	}

	public void setPrimaryAccountNumber(String value) throws ISOException
	{
		if (value == null || value.length() < 14 || value.length() > 16)
			throw new ISOException("Invalid Primary Account Number: " + value);

		this.set(2, value);
	}

	public String getProcessingCode() throws ISOException
	{
		if (!hasField(3))
			return null;
		return (String) this.getValue(3);
	}

	public void setProcessingCode(String value) throws ISOException
	{
		if (value == null || value.length() != 6)
			throw new ISOException("Invalid Processing Code: " + value);

		this.set(3, value);
	}

	public String getTransactionAmount() throws ISOException
	{
		if (!hasField(4))
			return null;
		return (String) this.getValue(4);
	}

	public void setTransactionAmount(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Transaction Amount: " + value);

		try
		{
			int amt = Integer.parseInt(value);
			if (amt < 0)
				throw new ISOException("Invalid Transaction Amount: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Amount: " + value);
		}

		this.set(4, value);
	}

	public String getSystemTraceNumber() throws ISOException
	{
		if (!hasField(11))
			return null;
		return (String) this.getValue(11);
	}

	public void setSystemTraceNumber(String value) throws ISOException
	{
		if (value == null || value.length() > 6)
			throw new ISOException("Invalid System Trace Number: " + value);

		try
		{
			int traceNo = Integer.parseInt(value);
			if (traceNo < 0 || traceNo > 999999)
				throw new ISOException("Invalid System Trace Number: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid System Trace Number: " + value);
		}

		this.set(11, value);
	}

	public String getTransactionLocalTime() throws ISOException
	{
		if (!hasField(12))
			return null;
		return (String) this.getValue(12);
	}

	public void setTransactionLocalTime(String value) throws ISOException
	{
		if (value == null || value.length() != 6)
			throw new ISOException("Invalid Transaction Local Time: " + value);

		try
		{
			int time = Integer.parseInt(value);
			if (time < 0 || time > 999999)
				throw new ISOException("Invalid Transaction Local Time: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Local Time: " + value);
		}

		this.set(12, value);
	}

	public String getTransactionLocalDate() throws ISOException
	{
		if (!hasField(13))
			return null;
		return (String) this.getValue(13);
	}

	public void setTransactionLocalDate(String value) throws ISOException
	{
		if (value == null || value.length() != 6)
			throw new ISOException("Invalid Transaction Local Date: " + value);

		try
		{
			int date = Integer.parseInt(value);
			if (date < 0 || date > 999999)
				throw new ISOException("Invalid Transaction Local Date: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Transaction Local Date: " + value);
		}

		this.set(13, value);
	}

	public String getExpirationDate() throws ISOException
	{
		if (!hasField(14))
			return null;
		return (String) this.getValue(14);
	}

	public void setExpirationDate(String value) throws ISOException
	{
		if (value == null || value.length() != 4)
			throw new ISOException("Invalid Expiration Date: " + value);

		try
		{
			int exp = Integer.parseInt(value);
			if (exp < 0 || exp > 9999)
				throw new ISOException("Invalid Expiration Date: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Expiration Date: " + value);
		}

		this.set(14, value);
	}

	public String getPointOfServiceEntryMode() throws ISOException
	{
		if (!hasField(22))
			return null;
		return (String) this.getValue(22);
	}

	public void setPointOfServiceEntryMode(String value) throws ISOException
	{
		if (value == null || value.length() != 2)
			throw new ISOException("Invalid Point Of Service Entry Mode: " + value);

		try
		{
			int entryMode = Integer.parseInt(value);
			if (entryMode < 0 || entryMode > 99)
				throw new ISOException("Invalid Point Of Service Entry Mode: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Point Of Service Entry Mode: " + value);
		}

		this.set(22, value);
	}

	public String getPointOfServiceConditionCode() throws ISOException
	{
		if (!hasField(25))
			return null;
		return (String) this.getValue(25);
	}

	public void setPointOfServiceConditionCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Point Of Service Condition Code: " + value);

		try
		{
			int code = Integer.parseInt(value);
			if (code < 0 || code > 99)
				throw new ISOException("Invalid Point Of Service Condition Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Point Of Service Condition Code: " + value);
		}

		this.set(25, value);
	}

	public String getTrack2Data() throws ISOException
	{
		if (!hasField(35))
			return null;
		return (String) this.getValue(35);
	}

	public void setTrack2Data(String value) throws ISOException
	{
		if (value == null || value.length() < 3 || value.length() > 37)
			throw new ISOException("Invalid Track2 Data: " + value);

		this.set(35, value);
	}

	public String getRetrievalReferenceNumber() throws ISOException
	{
		if (!hasField(37))
			return null;
		return (String) this.getValue(37);
	}

	public void setRetrievalReferenceNumber(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Retrieval Reference Number: " + value);

		this.set(37, value);
	}

	public String getAuthorizationIdentificationResponse() throws ISOException
	{
		if (!hasField(38))
			return null;
		return (String) this.getValue(38);
	}

	public void setAuthorizationIdentificationResponse(String value) throws ISOException
	{
		this.set(38, value);
	}

	public String getResponseCode() throws ISOException
	{
		if (!hasField(39))
			return null;
		return (String) this.getValue(39);
	}

	public void setResponseCode(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Response Code: " + value);

		try
		{
			int code = Integer.parseInt(value);
			if (code < 0 || code > 99)
				throw new ISOException("Invalid Response Code: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Response Code: " + value);
		}

		this.set(39, value);
	}

	public String getCardAcceptorTerminalID() throws ISOException
	{
		if (!hasField(41))
			return null;
		return (String) this.getValue(41);
	}

	public void setCardAcceptorTerminalID(String value) throws ISOException
	{
		if (value == null) // || value.length() != 6)
			throw new ISOException("Invalid Terminal ID: " + value);

		this.set(41, value);
	}

	public String getCardAcceptorAcquirerID() throws ISOException
	{
		if (!hasField(42))
			return null;
		return (String) this.getValue(42);
	}

	public void setCardAcceptorAcquirerID(String value) throws ISOException
	{
		if (value == null) // || value.length() != 16)
			throw new ISOException("Invalid Acquirer ID: " + value);

		this.set(42, value);
	}

	public String getTrack1Data() throws ISOException
	{
		if (!hasField(45))
			return null;
		return (String) this.getValue(45);
	}

	public void setTrack1Data(String value) throws ISOException
	{
		if (value == null || value.length() < 3 || value.length() > 76)
			throw new ISOException("Invalid Track1 Data: " + value);

		this.set(45, value);
	}
	
	public String getTransactionIndicators() throws ISOException
	{
		if (!hasField(47))
			return null;
		return (String) this.getValue(47);
	}

	public void setTransactionIndicators(String value) throws ISOException
	{
		if(value == null || value.length() > 18)
			throw new ISOException("Invalid Transaction Indicators: " + value);

		this.set(47, value);
	}
	
	public String getElavonAuthorizationData() throws ISOException
	{
		if (!hasField(48))
			return null;
		return (String) this.getValue(48);
	}

	public void setElavonAuthorizationData(String value) throws ISOException
	{
		if(value == null || value.length() > 50)
			throw new ISOException("Invalid Elavon Authorization Data: " + value);

		this.set(48, value);
	}

	public String getAdditionalAmounts() throws ISOException
	{
		if (!hasField(54))
			return null;
		return (String) this.getValue(54);
	}

	public void setAdditionalAmounts(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 100)
			throw new ISOException("Invalid Additional Amounts: " + value);	
		
		this.set(54, value);
	}
	
	public String getPaymentIndicator() throws ISOException
	{
		if (!hasField(60))
			return null;
		return (String) this.getValue(60);
	}

	public void setPaymentIndicator(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 6)
			throw new ISOException("Invalid Payment Indicator: " + value);		
		
		this.set(60, value);
	}
	
	public String getCPSData() throws ISOException
	{
		if (!hasField(61))
			return null;
		return (String) this.getValue(61);
	}

	public void setCPSData(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 23)
			throw new ISOException("Invalid CPS Data: " + value);		
		
		this.set(61, value);
	}
	
	public String getSequenceNumber() throws ISOException
	{
		return getMerchantDefinedData();		
	}

	public void setSequenceNumber(String value) throws ISOException
	{
		if (value == null)
			throw new ISOException("Invalid Sequence Number: " + value);

		try
		{
			long seqNum = Long.parseLong(value);
			if (seqNum < 0 || seqNum > 999999)
				throw new ISOException("Invalid Sequence Number: " + value);
		}
		catch (NumberFormatException e)
		{
			throw new ISOException("Invalid Sequence Number: " + value);
		}

		setMerchantDefinedData(value);
	}	
	
	public String getMerchantDefinedData() throws ISOException
	{
		if (!hasField(115))
			return null;
		return (String) this.getValue(115);
	}

	public void setMerchantDefinedData(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 30)
			throw new ISOException("Invalid Merchant Defined Data: " + value);		
		
		this.set(115, value);
	}
	
	public String getAddressVerificationData() throws ISOException
	{
		if (!hasField(123))
			return null;
		return (String) this.getValue(123);
	}

	public void setAddressVerificationData(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 29)
			throw new ISOException("Invalid Address Verification Data: " + value);		
		
		this.set(123, value);
	}
	
	public String getCardholderVerificationData() throws ISOException
	{
		if (!hasField(124))
			return null;
		return (String) this.getValue(124);
	}

	public void setCardholderVerificationData(String value) throws ISOException
	{
		if(value == null || value.length() < 1 || value.length() > 53)
			throw new ISOException("Invalid Cardholder Verification Data: " + value);		
		
		this.set(124, value);
	}
	
	public String getPurchaseCardData() throws ISOException
	{
		if (!hasField(125))
			return null;
		return (String) this.getValue(125);
	}

	public void setPurchaseCardData(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 27)
			throw new ISOException("Invalid Purchase Card Data: " + value);		
		
		this.set(125, value);
	}
	
	public String getInvoiceNumber() throws ISOException
	{
		if (!hasField(126))
			return null;
		return (String) this.getValue(126);
	}

	public void setInvoiceNumber(String value) throws ISOException
	{
		if (value == null || value.length() < 1 || value.length() > 25)
			throw new ISOException("Invalid Invoice Number: " + value);		
		
		this.set(126, value);
	}
	
	public String getCVV2ResultCode() throws ISOException
	{
		String elavonAuthorizationData = getElavonAuthorizationData();
		if (elavonAuthorizationData != null)
			return elavonAuthorizationData.substring(7, 8);
		else
			return null;
	}
	
	public String getAVSResponseCode() throws ISOException
	{
		String elavonAuthorizationData = getElavonAuthorizationData();
		if (elavonAuthorizationData != null)
			return elavonAuthorizationData.substring(6, 7);
		else
			return null;
	}	

	public String getDynamicMerchantData() throws ISOException {
		if(!hasField(109))
			return null;
		return (String) this.getValue(109);
	}

	public void setDynamicMerchantData(String value) throws ISOException {
		if(value == null || value.length() < 1 || value.length() > 182)
			throw new ISOException("Invalid Dynamic Merchant Data: " + value);

		this.set(109, value);
	}

	// toString copied mostly from ISOMsg class but modified for performance and to handle heartbeats
	public String toString()
	{

		StringBuilder s = new StringBuilder();
		if (isIncoming())
			s.append("<-- ");
		else if (isOutgoing())
			s.append("--> ");
		else
			s.append("    ");
		
		if(!isHeartBeat() && rawBytes != null)
		{
			s.append("'");
			s.append(new String(rawBytes));
			s.append("'");
		}
		else
		{
			try
			{
				if (isHeartBeat())
				{
					s.append("HEARTBEAT");
				}
				else
				{
					s.append((String) getValue(0));
					if (hasField(11))
					{
						s.append(' ');
						s.append((String) getValue(11));
					}
					if (hasField(41))
					{
						s.append(' ');
						s.append((String) getValue(41));
					}
				}
			}
			catch (ISOException e)
			{
			}
		}

		return s.toString();
	}

	public boolean isHeartBeat()
	{
		return !hasField(0);
	}
}
