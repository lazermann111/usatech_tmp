package com.usatech.iso8583.interchange.elavon.jpos.packager;

import org.jpos.iso.IFA_BINARY;
import org.jpos.iso.IFA_BITMAP;
import org.jpos.iso.IFA_LLCHAR;
import org.jpos.iso.IFA_LLLCHAR;
import org.jpos.iso.IFA_LLLNUM;
import org.jpos.iso.IFA_LLNUM;
import org.jpos.iso.IFA_NUMERIC;
import org.jpos.iso.IF_CHAR;
import org.jpos.iso.ISOBasePackager;
import org.jpos.iso.ISOFieldPackager;

public class ElavonPackager extends ISOBasePackager
{
	public ElavonPackager()
	{
		super();
		super.setFieldPackager(new ISOFieldPackager[getMaxValidField() + 1]);

		super.setFieldPackager(0, new IFA_NUMERIC(4, "MESSAGE TYPE INDICATOR"));
		super.setFieldPackager(1, new IFA_BITMAP(16, "BIT MAP"));
		super.setFieldPackager(2, new IFA_LLNUM(19, "PAN - PRIMARY ACCOUNT NUMBER"));
		super.setFieldPackager(3, new IFA_NUMERIC(6, "PROCESSING CODE"));
		super.setFieldPackager(4, new IFA_NUMERIC(12, "AMOUNT, TRANSACTION"));
		super.setFieldPackager(11, new IFA_NUMERIC(6, "SYSTEM TRACE NUMBER"));
		super.setFieldPackager(12, new IFA_NUMERIC(6, "TIME, LOCAL TRANSACTION"));
		super.setFieldPackager(13, new IFA_NUMERIC(6, "DATE, LOCAL TRANSACTION"));
		super.setFieldPackager(14, new IFA_NUMERIC(4, "DATE, EXPIRATION"));
		super.setFieldPackager(22, new IFA_NUMERIC(2, "POINT OF SERVICE ENTRY MODE"));
		super.setFieldPackager(25, new IFA_NUMERIC(2, "POINT OF SERVICE CONDITION CODE"));
		super.setFieldPackager(27, new IFA_NUMERIC(1, "TOKEN INDICATOR"));
		super.setFieldPackager(32, new IFA_LLNUM(11, "ACQUIRING INSTITUTION ID"));
		super.setFieldPackager(35, new IFA_LLCHAR(37, "TRACK 2 DATA"));
		super.setFieldPackager(37, new IF_CHAR(12, "RETRIEVAL REFERENCE NUMBER"));
		super.setFieldPackager(38, new IF_CHAR(6, "APPROVAL CODE"));
		super.setFieldPackager(39, new IF_CHAR(2, "RESPONSE CODE"));
		super.setFieldPackager(41, new IFA_NUMERIC(6, "CARD ACCEPTOR TERMINAL IDENTIFICATION"));
		super.setFieldPackager(42, new IFA_NUMERIC(16, "CARD ACCEPTOR IDENTIFICATION CODE"));
		super.setFieldPackager(45, new IFA_LLCHAR(76, "TRACK 1 DATA"));
		super.setFieldPackager(47, new IFA_LLLCHAR(18, "TRANSACTION INDICATORS"));
		super.setFieldPackager(48, new IFA_LLLCHAR(3, "ELAVON AUTHORIZATION DATA"));
		super.setFieldPackager(54, new IFA_LLLCHAR(100, "ADDITIONAL AMOUNTS"));
		super.setFieldPackager(60, new IFA_LLLNUM(6, "PAYMENT INDICATOR"));
		super.setFieldPackager(61, new IFA_LLLCHAR(23, "CPS DATA"));
		super.setFieldPackager(65, new IFA_BINARY(1, "BITMAP, EXTENDED"));
		super.setFieldPackager(109, new IFA_LLLCHAR(182, "DYNAMIC MERCHANT DATA"));
		super.setFieldPackager(115, new IFA_LLLCHAR(30, "MERCHANT DEFINED DATA"));
		super.setFieldPackager(123, new IFA_LLLCHAR(29, "ADDRESS VERIFICATION DATA"));
		super.setFieldPackager(124, new IFA_LLLCHAR(53, "CARDHOLDER VERIFICATION DATA"));
		super.setFieldPackager(125, new IFA_LLLCHAR(27, "PURCHASE CARD DATA"));
		super.setFieldPackager(126, new IFA_LLLCHAR(25, "INVOICE NUMBER"));
	}

	protected int getMaxValidField()
	{
		return 127;
	}
}
