package com.usatech.iso8583.interchange.elavon;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.CollectionUtils;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.iso8583.ISO8583Request;

public class ElavonUtil
{	
	protected static final byte[] ISO_HEADER_STANDARD = "ISO000MNIFKVGYA".getBytes();
	protected static final byte[] ISO_HEADER_MAIL_TELEPHONE = "ISO000MNIFKVMYA".getBytes();
	protected static final byte[] ISO_HEADER_ECOMMERCE = "ISO000MNIFKVIYA".getBytes();
	protected static final NavigableMap<String, String> posIdentifierPrefixMap = new TreeMap<String, String>();
	
	public static ThreadLocal<SimpleDateFormat> TRAN_DATE_FORMAT = new ThreadLocal<SimpleDateFormat>()
	{
		protected synchronized SimpleDateFormat initialValue()
		{
			return new SimpleDateFormat("MMddyy");
		}
	};
	
	public static ThreadLocal<SimpleDateFormat> TRAN_TIME_FORMAT = new ThreadLocal<SimpleDateFormat>()
	{
		protected synchronized SimpleDateFormat initialValue()
		{
			return new SimpleDateFormat("HHmmss");
		}
	};
	
	public static ThreadLocal<SimpleDateFormat> TRAN_DATETIME_FORMAT = new ThreadLocal<SimpleDateFormat>()
	{
		protected synchronized SimpleDateFormat initialValue()
		{
			return new SimpleDateFormat("MMddyyHHmmss");
		}
	};

	public static Date parseDateTime(String MMddyy, String HHmmss) throws ParseException
	{
		return TRAN_DATETIME_FORMAT.get().parse(MMddyy + HHmmss);
	}

	public static String getMMDD(Date date)
	{
		return TRAN_DATE_FORMAT.get().format(date);
	}

	public static String getHHmmss(Date date)
	{
		return TRAN_TIME_FORMAT.get().format(date);
	}

	public static String buildPOSEntryModeString(String panEntryMode)
	{
		if (panEntryMode.equalsIgnoreCase(ISO8583Message.ENTRY_MODE_MANUALLY_KEYED))
			return "01";
		else if (panEntryMode.equalsIgnoreCase(ISO8583Message.ENTRY_MODE_SWIPED))
			return "90";
		else if (panEntryMode.equalsIgnoreCase(ISO8583Message.ENTRY_MODE_CONTACTLESS_SWIPED))
			return "91";
		else
			return "00";
	}

	public static String buildPOSConditionCode(String posEnvironment)
	{
		if (posEnvironment.equalsIgnoreCase(ISO8583Message.POS_ENVIRONMENT_MAIL_TELEPHONE))
			return "08";
		else if (posEnvironment.equalsIgnoreCase(ISO8583Message.POS_ENVIRONMENT_ECOMMERCE))
			return "59";
		else
			return "06";
	}

	public static String buildTransactionIndicators(boolean partialAuthAllowed, String posEnvironment, String posEntryCapability, String pinEntryCapability, String posIdentifier, int specificationVersion) throws ValidationException {
		StringBuilder sb = new StringBuilder();
		// Partial Auth Indicator
		if(partialAuthAllowed)
			sb.append('1');
		else
			sb.append('0');

		// Terminal Type and CAT Indicator
		if(ISO8583Message.POS_ENVIRONMENT_UNATTENDED.equalsIgnoreCase(posEnvironment))
			sb.append("0303");
		else if(ISO8583Message.POS_ENVIRONMENT_ATTENDED.equalsIgnoreCase(posEnvironment))
			sb.append("0000");
		else if(ISO8583Message.POS_ENVIRONMENT_MOBILE.equalsIgnoreCase(posEnvironment)) {
			if(specificationVersion >= 1040)
				sb.append("0500");
			else
				sb.append("0000");
		} else if(ISO8583Message.POS_ENVIRONMENT_MAIL_TELEPHONE.equalsIgnoreCase(posEnvironment))
			sb.append("0000");
		else if(ISO8583Message.POS_ENVIRONMENT_ECOMMERCE.equalsIgnoreCase(posEnvironment))
			sb.append("0405");
		else
			sb.append("0303");

		// Reserved
		sb.append("  ");

		// POS Entry Capability
		if(ISO8583Message.POS_ENVIRONMENT_ECOMMERCE.equalsIgnoreCase(posEnvironment))
			sb.append("01");
		else if(posEntryCapability == null || posEntryCapability.isEmpty()) {
			if(ISO8583Message.PIN_ENTRY_CAPABILITY.equalsIgnoreCase(pinEntryCapability))
				sb.append("01");
			else
				sb.append("02");
		} else if(posEntryCapability.contains(ISO8583Message.POS_CAPABILITY_MICR))
			sb.append("06");
		else if(posEntryCapability.contains(ISO8583Message.POS_CAPABILITY_EMV_DUAL))
			sb.append("05");
		else if(posEntryCapability.contains(ISO8583Message.POS_CAPABILITY_EMV_CONTACT))
			sb.append("04");
		else if(posEntryCapability.contains(ISO8583Message.POS_CAPABILITY_PROXIMITY))
			sb.append("03");
		else if(posEntryCapability.contains(ISO8583Message.POS_CAPABILITY_MAG_SWIPE))
			sb.append("02");
		else if(posEntryCapability.contains(ISO8583Message.POS_CAPABILITY_MANUAL_ONLY))
			sb.append("01");
		else
			sb.append("02");

		// PIN Entry Capability
		if(ISO8583Message.POS_ENVIRONMENT_ECOMMERCE.equalsIgnoreCase(posEnvironment))
			sb.append("2");
		else if(ISO8583Message.PIN_ENTRY_CAPABILITY.equalsIgnoreCase(pinEntryCapability))
			sb.append("8");
		else if(ISO8583Message.PIN_ENTRY_NO_CAPABILITY.equalsIgnoreCase(pinEntryCapability))
			sb.append("2");
		else
			sb.append("0");

		// POS Lane Number - must be all digits
		if(posIdentifier != null && !ISO8583Message.POS_ENVIRONMENT_MAIL_TELEPHONE.equalsIgnoreCase(posEnvironment) && !ISO8583Message.POS_ENVIRONMENT_ECOMMERCE.equalsIgnoreCase(posEnvironment)) {
			if(posIdentifierPrefixMap.isEmpty()) { // use default mapping of converting each non-digit character to a digit
				for(int i = Math.max(posIdentifier.length() - 8, 0); i < posIdentifier.length(); i++) {
					char ch = posIdentifier.charAt(i);
					if(ch < '0' || ch > '9')
						ch = (char) ('0' + (ch % 10));
					sb.append(ch);
				}
			} else {
				Map.Entry<String, String> entry = CollectionUtils.getBestMatch(posIdentifierPrefixMap, posIdentifier);
				if(entry == null)
					throw new ValidationException("No prefix mapping for POS Identifier was found for '" + posIdentifier + "'");
				int replaced = entry.getKey().length();
				if(posIdentifier.length() - replaced > 8)
					throw new ValidationException("POS Identifier is too long '" + posIdentifier + "'; may only be " + (8 + entry.getValue().length() - replaced) + " characters");	
				sb.append(entry.getValue());
				for(int i = replaced; i < posIdentifier.length(); i++) {
					char ch = posIdentifier.charAt(i);
					if(ch < '0' || ch > '9')
						throw new ValidationException("POS Identifier  '" + posIdentifier + "' must be all digits after position " + (i + 1));
					sb.append(ch);
				}
			}
		}

		return sb.toString();
	}

	public static byte[] getISOHeader(String posEnvironment) {
		if(posEnvironment.equalsIgnoreCase(ISO8583Message.POS_ENVIRONMENT_MAIL_TELEPHONE))
			return ISO_HEADER_MAIL_TELEPHONE;
		else if(posEnvironment.equalsIgnoreCase(ISO8583Message.POS_ENVIRONMENT_ECOMMERCE))
			return ISO_HEADER_ECOMMERCE;
		else
			return ISO_HEADER_STANDARD;
	}

	public static String getPosIdentifierPrefix(String prefix) {
		return posIdentifierPrefixMap.get(prefix);
	}

	public static void setPosIdentifierPrefix(String prefix, String result) throws ValidationException {
		if(prefix == null)
			return;
		if(result == null)
			posIdentifierPrefixMap.remove(prefix);
		else {
			// verify result is all digits
			for(int i = 0; i < result.length(); i++) {
				char ch = result.charAt(i);
				if(ch < '0' || ch > '9')
					throw new ValidationException("Result of posIdentifierPrefix '" + prefix + "' must be all digits, not '" + result + "'");
			}
			posIdentifierPrefixMap.put(prefix, result);
		}
	}

	protected static final Set<String> validCountryCds = new HashSet<String>(Collections.singleton("US"));
	protected static final Set<String> usTerritories = new HashSet<String>(Arrays.asList(new String[] { "AS", "FM", "GU", "PR", "PW", "MH", "MP", "VI" }));
	protected static final Pattern nonAlpaNumPattern = Pattern.compile("[^0-9A-Za-z]+");

	public static String[] getValidCountryCds() {
		return validCountryCds.toArray(new String[validCountryCds.size()]);
	}

	public static void setValidCountryCds(String[] countryCds) {
		validCountryCds.clear();
		if(countryCds != null)
			for(String cc : countryCds)
				validCountryCds.add(cc);
	}

	public static String[] getUsTerritories() {
		return usTerritories.toArray(new String[usTerritories.size()]);
	}

	public static void setUsTerritories(String[] stateCds) {
		usTerritories.clear();
		if(stateCds != null)
			for(String sc : stateCds)
				usTerritories.add(sc);
	}

	public static String buildDynamicMerchantData(ISO8583Request request) {
		StringBuilder sb = new StringBuilder(182);
		String doingBusinessAs = request.getDoingBusinessAs();
		if(!StringUtils.isBlank(doingBusinessAs)) {
			sb.append("USA*");
			StringUtils.appendPadded(sb, doingBusinessAs, ' ', 21, Justification.LEFT);
		} else
			StringUtils.appendPadded(sb, null, ' ', 25, Justification.LEFT);
		String countryCd = request.getCountryCode();
		if(countryCd != null && (countryCd = countryCd.trim()).length() > 0 && validCountryCds.contains(countryCd)) {
			StringUtils.appendPadded(sb, request.getStreet(), ' ', 30, Justification.LEFT);
			StringUtils.appendPadded(sb, request.getCity(), ' ', 13, Justification.LEFT);
			String stateCd = request.getState();
			if("US".equalsIgnoreCase(countryCd) && usTerritories.contains(stateCd)) {
				countryCd = stateCd;
				stateCd = null;
			}
			StringUtils.appendPadded(sb, stateCd, ' ', 2, Justification.LEFT);
			String postal = request.getPostal();
			if(!StringUtils.isBlank(postal))
				postal = nonAlpaNumPattern.matcher(postal).replaceAll("");
			StringUtils.appendPadded(sb, postal, ' ', 9, Justification.LEFT); // strip punctuation
			if(countryCd.length() == 2)
				try {
					countryCd = new Locale("", countryCd).getISO3Country();
				} catch(MissingResourceException e) {
					// ignore
				}
			StringUtils.appendPadded(sb, countryCd, ' ', 3, Justification.LEFT); // Convert to 3-digit
		} else
			StringUtils.appendPadded(sb, null, ' ', 57, Justification.LEFT);
		String csPhone;
		if(request.getPhone() != null)
			csPhone = request.getPhone().replaceAll("[^0-9]", "");
		else
			csPhone = null;
		StringUtils.appendPadded(sb, csPhone, ' ', 20, Justification.LEFT);
		StringUtils.appendPadded(sb, request.getEmail(), ' ', 70, Justification.LEFT);
		Long customerId = request.getCustomerId();
		if(customerId != null)
			StringUtils.appendPadded(sb, customerId.toString(), ' ', 10, Justification.RIGHT);
		else
			StringUtils.appendPadded(sb, null, ' ', 10, Justification.RIGHT);
		return sb.toString();
	}

}
