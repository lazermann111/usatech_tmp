package com.usatech.iso8583.interchange.tandem;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;

import simple.io.ByteArrayUtils;
import simple.text.StringUtils;

public class Bitmap192x8Field extends ISOField {
	protected long[] bitmap = new long[3];
	protected byte[] values = new byte[64];
	protected static final long BIT_ONE = 1L << 64;

	public Bitmap192x8Field() {
		super();
	}

	public Bitmap192x8Field(int fieldNumber) {
		super(fieldNumber);
	}

	@Override
	public String getValue() {
		if(bitmap[0] == 0)
			return "0000000000000000";
		int n = 16;
		int mapMax = 1;
		n += 2 * Long.bitCount(bitmap[0]);
		if(bitmap[1] != 0) {
			mapMax = 2;
			n += 14;
			n += 2 * Long.bitCount(bitmap[1]);
			if(bitmap[2] != 0) {
				mapMax = 3;
				n += 14;
				n += 2 * Long.bitCount(bitmap[2]);				
			}
		}
		StringBuilder sb = new StringBuilder(n);
		StringUtils.appendHex(sb, bitmap[0]);
		if(bitmap[1] != 0) {
			StringUtils.appendHex(sb, bitmap[1]);
			if(bitmap[2] != 0)
				StringUtils.appendHex(sb, bitmap[2]);
		}
		for(int m = 0; m < mapMax; m++) {
			for(int b = 63; b > 0; b--) {
				long mask = 1L << (b - 1);
				if((bitmap[m] & mask) == mask)
					StringUtils.appendHex(sb, values[m * 64 + (64 - b)]);
			}
		}
		return sb.toString();
	}

	protected long readHexLong(String hex, int offset) {
		long l = 0;
		for(int i = 0; i < 8; i++) {
			long b = ByteArrayUtils.hexValue(hex.charAt(i * 2 + offset), hex.charAt(i * 2 + offset + 1)) & 0xFF;
			l += (b << (8 * (7 - i)));
		}
		return l;
	}

	protected byte readHexByte(String hex, int offset) {
		return ByteArrayUtils.hexValue(hex.charAt(offset), hex.charAt(offset + 1));
	}

	public void setValue(Object obj) throws ISOException {
		// parse
		String value = (String) obj;
		bitmap[0] = readHexLong(value, 0);
		int offset = 16;
		int max = 64;
		if((bitmap[0] & BIT_ONE) == BIT_ONE) {
			bitmap[1] = readHexLong(value, offset);
			offset += 16;
			max += 64;
			if((bitmap[1] & BIT_ONE) == BIT_ONE) {
				bitmap[2] = readHexLong(value, offset);
				offset += 16;
				max += 64;
			} else
				bitmap[2] = 0;
		} else {
			bitmap[1] = 0;
			bitmap[2] = 0;
		}
		if(max > values.length)
			values = new byte[max];
		for(int i = 2; i <= max; i++) {
			if((i % 64) == 1)
				continue;
			int mapIndex = (i - 1) / 64;
			long tagMask = 1L << (64 - (i % 64));
			if(offset < value.length() && (bitmap[mapIndex] & tagMask) == tagMask) {
				values[i - 1] = readHexByte(value, offset);
				offset += 2;
			} else
				values[i - 1] = 0;
		}
	}

	/**
	 * @param tagBit
	 *            - Little Endian 1-based bit number (bit 1 is the most significant bit)
	 * @param valueBit
	 *            - Big Endian 1-based bit number (bit 1 is the least significant bit)
	 * @return
	 * @throws ISOException
	 */
	public boolean getBit(int tagBit, int valueBit) throws ISOException {
		if(tagBit < 1 || tagBit > 192)
			throw new ISOException("Invalid tag bit; must be between 1 and 192");
		if(((tagBit - 1) % 64) == 0)
			throw new ISOException("May not set tag bit " + tagBit + " - it is reserved");
		if(valueBit < 1 || valueBit > 8)
			throw new ISOException("Invalid value bit; must be between 1 and 8");
		if(tagBit > values.length)
			return false;
		long tagMask = 1L << (64 - (tagBit % 64));
		int valueMask = 1 << (valueBit - 1);
		int mapIndex = (tagBit - 1) / 64;
		return (bitmap[mapIndex] & tagMask) == tagMask && (values[tagBit - 1] & valueMask) == valueMask;
	}

	/**
	 * @param tagBit
	 *            - Little Endian 1-based bit number (bit 1 is the most significant bit)
	 * @param valueBit
	 *            - Big Endian 1-based bit number (bit 1 is the least significant bit)
	 * @param on
	 * @throws ISOException
	 */
	public void setBit(int tagBit, int valueBit, boolean on) throws ISOException {
		if(tagBit < 1 || tagBit > 192)
			throw new ISOException("Invalid tag bit; must be between 1 and 192");
		if(((tagBit - 1) % 64) == 0)
			throw new ISOException("May not set tag bit " + tagBit + " - it is reserved");
		if(valueBit < 1 || valueBit > 8)
			throw new ISOException("Invalid value bit; must be between 1 and 8");
		if(tagBit > values.length) {
			byte[] tmp = new byte[((tagBit - 1) / 64) * 64 + 64];
			System.arraycopy(values, 0, tmp, 0, values.length);
			values = tmp;
		}
		long tagMask = 1L << (64 - (tagBit % 64));
		int valueMask = 1 << (valueBit - 1);
		int mapIndex = (tagBit - 1) / 64;
		if(on) {
			if(values[tagBit - 1] == 0) {
				bitmap[mapIndex] |= tagMask;
				for(int m = mapIndex; m > 0; m--)
					bitmap[m - 1] |= BIT_ONE;
			}
			values[tagBit - 1] |= valueMask;
		} else {
			values[tagBit - 1] &= ~valueMask;
			if(values[tagBit - 1] == 0)
				bitmap[mapIndex] &= ~tagMask;
		}
	}

	@Override
	public byte[] getBytes() {
		return getValue().getBytes();
	}

	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeShort(fieldNumber);
		out.writeUTF(getValue());
	}

	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		fieldNumber = in.readShort();
		try {
			setValue(in.readUTF());
		} catch(ISOException e) {
			throw new IOException(e);
		}
	}
}
