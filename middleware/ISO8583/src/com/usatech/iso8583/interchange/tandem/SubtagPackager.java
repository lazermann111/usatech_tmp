package com.usatech.iso8583.interchange.tandem;

import org.jpos.iso.AsciiInterpreter;
import org.jpos.iso.AsciiPrefixer;
import org.jpos.iso.ISOComponent;
import org.jpos.iso.ISOStringFieldPackager;
import org.jpos.iso.NullPadder;

public class SubtagPackager extends ISOStringFieldPackager {
	protected final int maxLength;
	protected final int subtagLengthBytes;

	public SubtagPackager(int len, String description, AsciiPrefixer asciiPrefixer, int subtagLengthBytes) {
		super(len, description, NullPadder.INSTANCE, AsciiInterpreter.INSTANCE, asciiPrefixer);
		this.subtagLengthBytes = subtagLengthBytes;
		this.maxLength = ((int) Math.pow(10, asciiPrefixer.getPackedLength())) - 1;
		checkLength(len, maxLength);
	}

	public void setLength(int len) {
		checkLength(len, maxLength);
		super.setLength(len);
	}

	@Override
	public ISOComponent createComponent(int fieldNumber) {
		return new SubtagField(fieldNumber, subtagLengthBytes);
	}
}
