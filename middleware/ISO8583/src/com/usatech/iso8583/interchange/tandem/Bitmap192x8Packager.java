package com.usatech.iso8583.interchange.tandem;

import org.jpos.iso.AsciiInterpreter;
import org.jpos.iso.AsciiPrefixer;
import org.jpos.iso.ISOComponent;
import org.jpos.iso.ISOStringFieldPackager;
import org.jpos.iso.NullPadder;

public class Bitmap192x8Packager extends ISOStringFieldPackager {
	protected final int maxLength;

	public Bitmap192x8Packager(int len, String description, AsciiPrefixer asciiPrefixer) {
		super(len, description, NullPadder.INSTANCE, AsciiInterpreter.INSTANCE, asciiPrefixer);
		this.maxLength = ((int) Math.pow(10, asciiPrefixer.getPackedLength())) - 1;
		checkLength(len, maxLength);
	}

	public void setLength(int len) {
		checkLength(len, maxLength);
		super.setLength(len);
	}

	@Override
	public ISOComponent createComponent(int fieldNumber) {
		return new Bitmap192x8Field(fieldNumber);
	}
}
