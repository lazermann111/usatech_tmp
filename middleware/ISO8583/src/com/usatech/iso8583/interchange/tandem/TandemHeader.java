package com.usatech.iso8583.interchange.tandem;

import org.jpos.iso.ISOException;
import org.jpos.iso.header.BaseHeader;

public class TandemHeader extends BaseHeader {
	private static final long serialVersionUID = 1L;
	
	protected int messageLength;
	protected int sequenceNumber;

	public TandemHeader(int sequenceNumber) throws ISOException {
		super();
	}
	
	public TandemHeader(byte[] bytes) throws ISOException {
		super(bytes);
	}

	public byte[] pack() {
		return header;
	}

	public int unpack(byte[] header) {
		int len = super.unpack(header);
		
		return len;
}
	

}
