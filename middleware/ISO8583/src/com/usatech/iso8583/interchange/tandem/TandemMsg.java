package com.usatech.iso8583.interchange.tandem;

import java.util.HashSet;
import java.util.Set;

import org.jpos.iso.ISOComponent;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

import com.usatech.iso8583.ISO8583Message;
import com.usatech.layers.common.MessageResponseUtils;

public class TandemMsg extends ISOMsg {
	public static int BIT_55_MAX_LENGTH = 513;
	protected static final Set<String> PROTECTED_BITS = new HashSet<String>();
	static {
		PROTECTED_BITS.add("2");
		PROTECTED_BITS.add("14");
		PROTECTED_BITS.add("23");
		PROTECTED_BITS.add("35");
		PROTECTED_BITS.add("45");
		PROTECTED_BITS.add("48.A1");
		PROTECTED_BITS.add("48.C1");
	}

	public static boolean isBitProtected(int bit) {
		return PROTECTED_BITS.contains(String.valueOf(bit));
	}

	public static boolean isBitProtected(int bit, String subtagKey) {
		return PROTECTED_BITS.contains(new StringBuilder().append(bit).append('.').append(subtagKey).toString());
	}

	protected int sequenceNumber;

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public boolean isHeartBeat() {
		return !hasField(0);
	}

	public void set(int fldno, String value) throws ISOException {
		if(value == null)
			unset(fldno);
		else if(isBitProtected(fldno))
			set(new MaskedField(fldno, value));
		else
			set(new ISOField(fldno, value));
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		/*
		if(isIncoming())
			sb.append("<-- ");
		else if(isOutgoing())
			sb.append("--> ");
		else
			sb.append("    ");
		*/
		sb.append('[');
		try {
			for(int k = 0; k <= maxField; k++) {
				ISOComponent comp = getComponent(k);
				if(comp != null) {
					if(isBitProtected(k)) {
						sb.append(k).append('=');
						Object value = comp.getValue();
						if(value != null) {
							switch(k) {
								case ISO8583Message.BIT_TRACK_2_DATA:
								case ISO8583Message.BIT_TRACK_1_DATA:
									sb.append(MessageResponseUtils.maskTrackData(value.toString(), false));
									break;
								case ISO8583Message.BIT_PRIMARY_ACCOUNT_NUMBER:
									sb.append(MessageResponseUtils.maskCardNumber(value.toString(), false));
									break;
								default:
									for(int i = 0; i < value.toString().length(); i++)
										sb.append('*');
							}
						}
					} else if(comp instanceof SubtagField) {
						SubtagField sf = (SubtagField) comp;
						for(String sk : sf.getSubtagKeys()) {
							sb.append(k).append('.').append(sk).append('=');
							String subvalue = sf.getSubtag(sk);
							if(subvalue == null)
								continue;
							if(isBitProtected(k, sk))
								for(int i = 0; i < subvalue.length(); i++)
									sb.append('*');
							else
								sb.append(subvalue);
							sb.append("; ");
						}
						continue;
					} else {
						sb.append(k).append('=');
						Object value = comp.getValue();
						if(value != null)
							sb.append(value.toString());
					}
					sb.append("; ");
				}
			}
		} catch(ISOException e) {
			sb.append('<').append(e.getClass().getName()).append(": ").append(e.getMessage()).append('>');
		}
		if(fields.size() > 0)
			sb.setLength(sb.length() - 2);
		sb.append(']');

		return sb.toString();
	}

	/*
	 * Bit 44 Subtag P1 has variable length fields.  This has been excluded from the spreadsheet.
	 */
	public String getEMVPublicKeyAID() throws ISOException {
		if (!hasField(44))
			return null;
		return ((SubtagField) getComponent(44)).getSubtagPiece("P1", 0, 10);
	}

	public void setEMVPublicKeyAID(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 10)
			throw new ISOException(
					"Invalid EMV Public Key Application Identifier: " + value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException(
					"Invalid EMV Public Key Application Identifier: " + value
							+ "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(44);
		if (sub == null) {
			sub = new SubtagField(44, 2);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, 0, 10, 10);
	}

	public String getEMVPublicKeyIndex() throws ISOException {
		if (!hasField(44))
			return null;
		return ((SubtagField) getComponent(44)).getSubtagPiece("P1", 10, 2);
	}

	public void setEMVPublicKeyIndex(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid EMV Public Key Index: " + value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV Public Key Index: " + value
					+ "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(44);
		if (sub == null) {
			sub = new SubtagField(44, 2);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, 10, 2, 2);
	}

	public String getEMVPublicKeyModulusLength() throws ISOException {
		if (!hasField(44))
			return null;
		return ((SubtagField) getComponent(44)).getSubtagPiece("P1", 12, 4);
	}

	public void setEMVPublicKeyModulusLength(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid EMV Public Key Modulus Length: "
					+ value);
		if (!value.matches("\\d+"))
			throw new ISOException("Invalid EMV Public Key Modulus Length: "
					+ value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(44);
		if (sub == null) {
			sub = new SubtagField(44, 2);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, 12, 4, 4);
	}

	public String getEMVPublicKeyModulus() throws ISOException {
		if (!hasField(44))
			return null;
		int modulusLength = getPublicKeyModulusLength();
		return ((SubtagField) getComponent(44)).getSubtagPiece("P1", getPublicKeyModulusOffset(), modulusLength);
	}
	public void setEMVPublicKeyModulus(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 3072)
			throw new ISOException("Invalid EMV Public Key Modulus: " + value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV Public Key Modulus: " + value
					+ "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(44);
		if (sub == null) {
			sub = new SubtagField(44, 2);
			set(sub);
		}
		int modulusLength = getPublicKeyModulusLength();
		sub.setSubtagPiece("P1", value, getPublicKeyModulusOffset(), modulusLength, 3072);
	}
	private int getPublicKeyModulusOffset() {
		return 16;
	}
	private int getPublicKeyModulusLength() throws NumberFormatException, ISOException {
		return Integer.parseInt(getEMVPublicKeyModulusLength());
	}

	public String getEMVPublicKeyExponentLength() throws ISOException {
		if (!hasField(44))
			return null;
		return ((SubtagField) getComponent(44)).getSubtagPiece("P1", getPublicKeyExponentLengthOffset(), 2);
	}
	public void setEMVPublicKeyExponentLength(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid EMV Public Key Exponent Length: "
					+ value);
		if (!value.matches("\\d+"))
			throw new ISOException("Invalid EMV Public Key Exponent Length: "
					+ value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(44);
		if (sub == null) {
			sub = new SubtagField(44, 2);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, getPublicKeyExponentLengthOffset(), getPublicKeyExponentLengthLength(), 2);
	}
	private int getPublicKeyExponentLengthLength() {
		return 2;
	}
	private int getPublicKeyExponentLengthOffset() throws NumberFormatException, ISOException {
		int publicKeyModulusOffset = 16;
		return publicKeyModulusOffset + getPublicKeyModulusLength();
	}

	public String getEMVPublicKeyExponent() throws ISOException {
		if (!hasField(44))
			return null;

		return ((SubtagField) getComponent(44)).getSubtagPiece("P1", getPublicKeyExponentOffset(), getPublicKeyExponentLength());
	}
	public void setEMVPublicKeyExponent(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 10)
			throw new ISOException("Invalid EMV Public Key Exponent: " + value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV Public Key Exponent: " + value
					+ "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(44);
		if (sub == null) {
			sub = new SubtagField(44, 2);
			set(sub);
		}
		
		sub.setSubtagPiece("P1", value, getPublicKeyExponentOffset(), getPublicKeyExponentLength(), 10);
	}
	private int getPublicKeyExponentLength() throws NumberFormatException, ISOException {
		int exponentLength = Integer.parseInt(getEMVPublicKeyExponentLength());
		return exponentLength;
	}
	private int getPublicKeyExponentOffset() throws NumberFormatException, ISOException {
		int exponentLengthLength = getPublicKeyExponentLengthLength();
		int exponentLengthOffset = getPublicKeyExponentLengthOffset();
		return exponentLengthOffset + exponentLengthLength;
	}

	public String getEMVPublicKeyChecksumLength() throws ISOException {
		if (!hasField(44))
			return null;
		return ((SubtagField) getComponent(44)).getSubtagPiece("P1", getEMVPublicKeyChecksumLengthOffset(), getPublicKeyChecksumLengthLength());
	}
	public void setEMVPublicKeyChecksumLength(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid EMV Public Key Checksum Length: "
					+ value);
		if (!value.matches("\\d+"))
			throw new ISOException("Invalid EMV Public Key Checksum Length: "
					+ value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(44);
		if (sub == null) {
			sub = new SubtagField(44, 2);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, getEMVPublicKeyChecksumLengthOffset(), getPublicKeyChecksumLengthLength(), 2);
	}
	private int getPublicKeyChecksumLengthLength() {
		return 2;
	}
	private int getEMVPublicKeyChecksumLengthOffset() throws NumberFormatException, ISOException {
		int exponentOffset = getPublicKeyExponentOffset();
		int exponentLength = Integer.parseInt(getEMVPublicKeyExponentLength());
		return exponentOffset + exponentLength;
	}

	public String getEMVPublicKeyChecksum() throws ISOException {
		if (!hasField(44))
			return null;
		return ((SubtagField) getComponent(44)).getSubtagPiece("P1", getPublicKeyChecksumOffset(), getPublicKeyChecksumLength());
	}
	public void setEMVPublicKeyChecksum(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 99)
			throw new ISOException("Invalid EMV Public Key Checksum: " + value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV Public Key Checksum: " + value
					+ "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(44);
		if (sub == null) {
			sub = new SubtagField(44, 2);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, getPublicKeyChecksumOffset(), getPublicKeyChecksumLength(), 99);
	}
	private int getPublicKeyChecksumLength() throws NumberFormatException, ISOException {
		int checksumLength = Integer.parseInt(getEMVPublicKeyChecksumLength());
		return checksumLength;
	}
	private int getPublicKeyChecksumOffset() throws NumberFormatException, ISOException {
		int checksumLengthOffset = getEMVPublicKeyChecksumLengthOffset();
		int checksumLength = getPublicKeyChecksumLengthLength();
		return checksumLengthOffset + checksumLength;
	}

	
	// Generated content below
	public String getPrimaryAccountNumber() throws ISOException {
		if(!hasField(2))
			return null;
		return (String) getValue(2);
	}

	public void setPrimaryAccountNumber(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 19)
			throw new ISOException("Invalid Primary Account Number: " + ISOUtil.protect(value));
		value = value.toUpperCase();
		set(2, value);
	}

	public String getProcessingCode() throws ISOException {
		if(!hasField(3))
			return null;
		return (String) getValue(3);
	}

	public void setProcessingCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 6)
			throw new ISOException("Invalid Processing Code: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Processing Code: " + value + "; must be all digits");
		set(3, value);
	}

	public String getTransactionAmount() throws ISOException {
		if(!hasField(4))
			return null;
		return (String) getValue(4);
	}

	public void setTransactionAmount(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 12)
			throw new ISOException("Invalid Transaction Amount: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Transaction Amount: " + value + "; must be all digits");
		set(4, value);
	}

	public String getTransactionUTCTimestamp() throws ISOException {
		if(!hasField(7))
			return null;
		return (String) getValue(7);
	}

	public String getSystemTraceAuditNumber() throws ISOException {
		if (!hasField(11))
			return null;
		return (String) getValue(11);
	}

	public void setSystemTraceAuditNumber(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 6)
			throw new ISOException("Invalid System Trace Audit Number: " + value);
		if (!value.matches("\\d+"))
			throw new ISOException("Invalid System Trace Audit Number: " + value + "; must be all digits");
		set(11, value);
	}
	
	public void setTransactionUTCTimestamp(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 14)
			throw new ISOException("Invalid Transaction Date and Time: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Transaction Date and Time: " + value + "; must be all digits");
		set(7, value);
	}

	public String getTransactionLocalTime() throws ISOException {
		if(!hasField(12))
			return null;
		return (String) getValue(12);
	}

	public void setTransactionLocalTime(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 6)
			throw new ISOException("Invalid Time, Local Transaction: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Time, Local Transaction: " + value + "; must be all digits");
		set(12, value);
	}

	public String getTransactionLocalDate() throws ISOException {
		if(!hasField(13))
			return null;
		return (String) getValue(13);
	}

	public void setTransactionLocalDate(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 8)
			throw new ISOException("Invalid Date, Local Transaction: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Date, Local Transaction: " + value + "; must be all digits");
		set(13, value);
	}

	public String getExpirationDate() throws ISOException {
		if(!hasField(14))
			return null;
		return (String) getValue(14);
	}

	public void setExpirationDate(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid Expiration Date: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Expiration Date: " + value + "; must be all digits");
		set(14, value);
	}

	public String getSettlementDate() throws ISOException {
		if(!hasField(15))
			return null;
		return (String) getValue(15);
	}

	public void setSettlementDate(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid Date, Settlement: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Date, Settlement: " + value + "; must be all digits");
		set(15, value);
	}

	public String getMerchantCategoryCode() throws ISOException {
		if(!hasField(18))
			return null;
		return (String) getValue(18);
	}

	public void setMerchantCategoryCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid Merchant Category Code: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Merchant Category Code: " + value + "; must be all digits");
		set(18, value);
	}

	public String getPOSEntryMode() throws ISOException {
		if(!hasField(22))
			return null;
		return (String) getValue(22);
	}

	public void setPOSEntryMode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid POS Entry Mode: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid POS Entry Mode: " + value + "; must be all digits");
		set(22, value);
	}

	public String getPanSequenceNumber() throws ISOException {
		if(!hasField(23))
			return null;
		return (String) getValue(23);
	}

	public void setPanSequenceNumber(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid EMV/Contactless PAN Sequence Number: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid EMV/Contactless PAN Sequence Number: " + value + "; must be all digits");
		set(23, value);
	}

	public String getPOSConditionCode() throws ISOException {
		if(!hasField(25))
			return null;
		return (String) getValue(25);
	}

	public void setPOSConditionCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid POS Condition Code: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid POS Condition Code: " + value + "; must be all digits");
		set(25, value);
	}

	public String getTrack2Data() throws ISOException {
		if(!hasField(35))
			return null;
		return (String) getValue(35);
	}

	public void setTrack2Data(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 37)
			throw new ISOException("Invalid Track 2 data: " + ISOUtil.protect(value));
		value = value.toUpperCase();
		set(35, value);
	}

	public String getRetrievalReferenceNumber() throws ISOException {
		if(!hasField(37))
			return null;
		return (String) getValue(37);
	}

	public void setRetrievalReferenceNumber(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 12)
			throw new ISOException("Invalid Retrieval Reference Number: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Retrieval Reference Number: " + value + "; special characters not allowed");
		set(37, value);
	}

	public String getApprovalCode() throws ISOException {
		if(!hasField(38))
			return null;
		return (String) getValue(38);
	}

	public void setApprovalCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 6)
			throw new ISOException("Invalid Authorization ID Response: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Authorization ID Response: " + value + "; special characters not allowed");
		set(38, value);
	}

	public String getResponseCode() throws ISOException {
		if(!hasField(39))
			return null;
		return (String) getValue(39);
	}

	public void setResponseCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Response Code: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Response Code: " + value + "; special characters not allowed");
		set(39, value);
	}

	public String getCardAcceptorTerminalId() throws ISOException {
		if(!hasField(41))
			return null;
		return (String) getValue(41);
	}

	public void setCardAcceptorTerminalId(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Card Acquirer Terminal Id: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Card Acquirer Terminal Id: " + value + "; must be all digits");
		set(41, value);
	}

	public String getCardAcceptorAcquirerId() throws ISOException {
		if(!hasField(42))
			return null;
		return (String) getValue(42);
	}

	public void setCardAcceptorAcquirerId(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 12)
			throw new ISOException("Invalid Card Acquirer ID: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Card Acquirer ID: " + value + "; must be all digits");
		set(42, value);
	}
	
	public String getTrack1Data() throws ISOException {
		if(!hasField(45))
			return null;
		return (String) getValue(45);
	}

	public void setTrack1Data(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 76)
			throw new ISOException("Invalid Track 1 Data: " + ISOUtil.protect(value));
		value = value.toUpperCase();
		set(45, value);
	}

	public String getAvsPostal() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("A1", 0, 9);
	}

	public void setAvsPostal(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 9)
			throw new ISOException("Invalid AVS Request Data - Postal Code: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid AVS Request Data - Postal Code: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("A1", value, 0, 9, 9);
	}

	public String getAvsAddress() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("A1", 9, 20);
	}

	public void setAvsAddress(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 20)
			throw new ISOException("Invalid AVS Request Data - Address: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid AVS Request Data - Address: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("A1", value, 9, 20, 9);
	}

	public String getAvsResponse() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("A2");
	}

	public void setAvsResponse(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid AVS Response Data: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid AVS Response Data: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("A2", value);
	}

	public String getEMVAID() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("A7");
	}

	public void setEMVAID(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 32)
			throw new ISOException("Invalid EMV/Contactless AID: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV/Contactless AID: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("A7", value);
	}

	public String getAccountBalanceCount() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("B3", 0, 2);
	}

	public void setAccountBalanceCount(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Credit Account Balance Information - Number of Accounts: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Credit Account Balance Information - Number of Accounts: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("B3", value, 0, 2, 40);
	}

	public String getAccountBalanceCurrencySource1() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("B3", 2, 3);
	}

	public void setAccountBalanceCurrencySource1(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Credit Account Balance Information - Credit Account Balance Indicator 1: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid Credit Account Balance Information - Credit Account Balance Indicator 1: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("B3", value, 2, 3, 40);
	}

	public String getAccountBalanceCurrencyCode1() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("B3", 5, 3);
	}

	public void setAccountBalanceCurrencyCode1(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Credit Account Balance Information - Currency Code: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Credit Account Balance Information - Currency Code: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("B3", value, 5, 3, 40);
	}

	public String getAccountBalanceAmount1() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("B3", 8, 13);
	}

	public void setAccountBalanceAmount1(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 13)
			throw new ISOException("Invalid Credit Account Balance Information - Balance Amount: " + value);
		if(!value.matches("[+-][0-9]+"))
			throw new ISOException("Invalid Credit Account Balance Information - Balance Amount: " + value + "; signed integer only");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("B3", value, 8, 13, 40);
	}

	public String getAccountBalanceCurrencySource2() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("B3", 21, 3);
	}

	public void setAccountBalanceCurrencySource2(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Credit Account Balance Information - AccountBalanceCurrencySource2: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid Credit Account Balance Information - AccountBalanceCurrencySource2: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("B3", value, 21, 3, 40);
	}

	public String getAccountBalanceCurrencyCode2() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("B3", 24, 3);
	}

	public void setAccountBalanceCurrencyCode2(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Credit Account Balance Information - Currency Code: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Credit Account Balance Information - Currency Code: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("B3", value, 24, 3, 40);
	}

	public String getAccountBalanceAmount2() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("B3", 27, 13);
	}

	public void setAccountBalanceAmount2(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 13)
			throw new ISOException("Invalid Credit Account Balance Information - Balance Amount: " + value);
		if(!value.matches("[+-][0-9]+"))
			throw new ISOException("Invalid Credit Account Balance Information - Balance Amount: " + value + "; signed integer only");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("B3", value, 27, 13, 40);
	}

	public String getCvvData() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("C1");
	}

	public void setCvvData(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid Cardholder Verification Data (CVD) Request Data: " + ISOUtil.protect(value));
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Cardholder Verification Data (CVD) Request Data: " + ISOUtil.protect(value) + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("C1", value);
	}

	public String getCvvResponse() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("C3");
	}

	public void setCvvResponse(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Cardholder Verification Response Data : " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid Cardholder Verification Response Data : " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("C3", value);
	}

	public String getEMVPOSParameterDownloadRequestStatus() throws ISOException {
		if (!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("C7");
	}

	public void setEMVPOSParameterDownloadRequestStatus(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid EMV POS Parameter Downlod Request Status: " + value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV POS Parameter Downlod Request Status: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if (sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("C7", value);
	}	
	
	public String getDataEntrySource() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("D1");
	}

	public void setDataEntrySource(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Data Entry Source: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Data Entry Source: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("D1", value);
	}

	public String getDuplicateCheckingIndicator() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("D6");
	}

	public void setDuplicateCheckingIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Duplicate Transaction Checking Indicator: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Duplicate Transaction Checking Indicator: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("D6", value);
	}
	
	public String getSoftDescriptor() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("D7");
	}

	public void setSoftDescriptor(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 25)
			throw new ISOException("Invalid Soft Descriptor: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&*-]+"))
			throw new ISOException("Invalid Soft Descriptor: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("D7", value);
	}

	public String getEMVParameterDownloadRequired() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("D8");
	}

	public void setEMVParameterDownloadRequired(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid EMV Parameter Download Required Indicator: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV Parameter Download Required Indicator: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("D8", value);
	}

	public String getEMVParameterDownloadResponseStatusIndicator()
			throws ISOException {
		if (!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("D9");
	}

	public void setEMVParameterDownloadResponseStatusIndicator(String value)
			throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid EMV Parameter Download Response Status Indicator: "	+ value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV Parameter Download Response Status Indicator: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if (sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("D9", value);
	}
	
	public String getGeolocationType() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("GE", 0, 2);
	}

	public void setGeolocationType(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Geolocation Type Indicator: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Geolocation Type Indicator: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("GE", value, 0, 2, 28);
	}

	public String getGeolocationLatitude() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("GE", 2, 10);
	}

	public void setGeolocationLatitude(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 10)
			throw new ISOException("Invalid Geolocation Latitude: " + value);
		if(!value.matches("[+-][0-9]+(?:[.][0-9]+)?"))
			throw new ISOException("Invalid Geolocation Latitude: " + value + "; signed decimal only");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("GE", value, 2, 10, 28);
		sub.setSubtagPiece("GE", ",", 12, 1, 28);
	}

	public String getGeolocationLongitude() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("GE", 13, 10);
	}

	public void setGeolocationLongitude(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 10)
			throw new ISOException("Invalid Geolocation Longitude: " + value);
		if(!value.matches("[+-][0-9]+(?:[.][0-9]+)?"))
			throw new ISOException("Invalid Geolocation Longitude: " + value + "; signed decimal only");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("GE", value, 13, 10, 28);
		sub.setSubtagPiece("GE", ",", 12, 1, 28);
	}

	public String getGeolocationTimezone() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("GE", 23, 5);
	}

	public void setGeolocationTimezone(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 5)
			throw new ISOException("Invalid Geolocation Political Time Zone: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Geolocation Political Time Zone: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("GE", value, 23, 5, 28);
	}

	public String getMCInterchangeData() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("M1");
	}

	public void setMCInterchangeData(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 17)
			throw new ISOException("Invalid MasterCard Interchange Data: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid MasterCard Interchange Data: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("M1", value);
	}

	public String getMCInterchangeCompliance() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("M1", 0, 1);
	}

	public void setMCInterchangeCompliance(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid MasterCard Interchange Compliance Indicator: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid MasterCard Interchange Compliance Indicator: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("M1", value, 0, 1, 17);
	}

	public String getMCBankNetRefNumber() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("M1", 1, 9);
	}

	public void setMCBankNetRefNumber(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 9)
			throw new ISOException("Invalid MasterCard BankNet Reference Number: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid MasterCard BankNet Reference Number: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("M1", value, 1, 9, 17);
	}

	public String getMCBankNetDate() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("M1", 10, 4);
	}

	public void setMCBankNetDate(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid MasterCard BankNet Date: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid MasterCard BankNet Date: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("M1", value, 10, 4, 17);
	}

	public String getMCCVSErrorIndicator() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("M1", 14, 1);
	}

	public void setMCCVSErrorIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid MasterCard CVC Error Indicator: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid MasterCard CVC Error Indicator: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("M1", value, 14, 1, 17);
	}

	public String getMCPOSChanged() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("M1", 15, 1);
	}

	public void setMCPOSChanged(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid MasterCard POS Validation Code / CVC Status Change : " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid MasterCard POS Validation Code / CVC Status Change : " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("M1", value, 15, 1, 17);
	}

	public String getMCMagStripeQualityIndicator() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("M1", 16, 1);
	}

	public void setMCMagStripeQualityIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid MasterCard Mag stripe quality indicator : " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid MasterCard Mag stripe quality indicator : " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("M1", value, 16, 1, 17);
	}

	public String getDeviceId() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("P2");
	}

	public void setDeviceId(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid Device ID/Pump ID/Lane/MC Term Identifier: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Device ID/Pump ID/Lane/MC Term Identifier: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("P2", value);
	}

	public String getEMVFallbackIndicatorAID() throws ISOException {
		if (!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("P5", 0, 10);
	}

	public void setEMVFallbackIndicatorAID(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 10)
			throw new ISOException(
					"Invalid EMV Fall Back Indicator Application Identifier: "
							+ value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException(
					"Invalid EMV Fall Back Indicator Application Identifier: "
							+ value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if (sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("P5", value, 0, 10, 10);
	}

	public String getEMVFallbackIndicator() throws ISOException {
		if (!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("P5", 10, 1);
	}

	public void setEMVFallbackIndicator(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid EMV Fall Back Indicator: " + value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid EMV Fall Back Indicator: " + value
					+ "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if (sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("P5", value, 10, 1, 1);
	}
	
	public String getEMVOfflineFloorLimitAID() throws ISOException {
		if (!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("P6", 0, 10);
	}

	public void setEMVOfflineFloorLimitAID(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 10)
			throw new ISOException(
					"Invalid EMV Offline Floor Limit Application Identifier: "
							+ value);
		value = value.toUpperCase();
		if (!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException(
					"Invalid EMV Offline Floor Limit Application Identifier: "
							+ value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if (sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("P6", value, 0, 10, 10);
	}

	public String getEMVOffliineFloorLimitBiasedRandomSelectionLimit()
			throws ISOException {
		if (!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("P6", 10, 14);
	}

	public void setEMVOffliineFloorLimitBiasedRandomSelectionLimit(String value)
			throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 14)
			throw new ISOException(
					"Invalid EMV Offline Floor Limit Biased Random Selection Limit: "
							+ value);
		if (!value.matches("\\d+"))
			throw new ISOException(
					"Invalid EMV Offline Floor Limit Biased Random Selection Limit: "
							+ value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if (sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("P6", value, 10, 14, 14);
	}

	public String getEMVOfflineFloorLimit() throws ISOException {
		if (!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("P6", 24, 14);
	}

	public void setEMVOfflineFloorLimit(String value) throws ISOException {
		if (value == null || value.length() == 0 || value.length() > 14)
			throw new ISOException("Invalid EMV Offline Floor Limit: " + value);
		if (!value.matches("\\d+"))
			throw new ISOException("Invalid EMV Offline Floor Limit: " + value
					+ "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if (sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("P6", value, 24, 14, 14);
	}
	
	public String getEnhancedAuthRequestIndicator() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("P8");
	}

	public void setEnhancedAuthRequestIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Enhanced Authorization Request Indicator: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Enhanced Authorization Request Indicator: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("P8", value);
	}

	public String getPreAuthIndicator() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("P9");
	}

	public void setPreAuthIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Pre-Authorization Indicator: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Pre-Authorization Indicator: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("P9", value);
	}

	public String getEnhancedAuthResponseIndicator() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("Q8", 0, 2);
	}

	public void setEnhancedAuthResponseIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Enhanced Authorization Response Indicator: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Enhanced Authorization Response Indicator: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("Q8", value, 0, 2, 3);
	}

	public String getEnhancedAuthResponseCardType() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("Q8", 2, 1);
	}

	public void setEnhancedAuthResponseCardType(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Enhanced Authorization Response Card Type: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid Enhanced Authorization Response Card Type: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("Q8", value, 2, 1, 3);
	}

	public String getCustomerDefinedData() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("R1");
	}

	public void setCustomerDefinedData(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 30)
			throw new ISOException("Invalid Customer Defined Data Field: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Customer Defined Data Field: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("R1", value);
	}

	public String getRecurringPaymentIndicator() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("R2");
	}

	public void setRecurringPaymentIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Recurring Payment Indicator : " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Recurring Payment Indicator : " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("R2", value);
	}

	public String getReversalReasonCode() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("R3");
	}

	public void setReversalReasonCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Reversal Reason Code: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Reversal Reason Code: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("R3", value);
	}

	public String getPartialReversalAmount() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("R8");
	}

	public void setPartialReversalAmount(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 12)
			throw new ISOException("Invalid Reversal Amount, Partial: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Reversal Amount, Partial: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("R8", value);
	}

	public String getCardType() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("S1");
	}

	public void setCardType(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Card Type Information: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid Card Type Information: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("S1", value);
	}

	public String getAuthResponseInfo() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("S2");
	}

	public void setAuthResponseInfo(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 5)
			throw new ISOException("Invalid Authorization Response Information: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Authorization Response Information: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("S2", value);
	}

	public String getACI() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("V1", 0, 1);
	}

	public void setACI(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Authorization Characteristis Indicator: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid Authorization Characteristis Indicator: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("V1", value, 0, 1, 16);
	}

	public String getTransactionId() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("V1", 1, 15);
	}

	public void setTransactionId(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 15)
			throw new ISOException("Invalid Unique Transaction ID: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Unique Transaction ID: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtagPiece("V1", value, 1, 15, 16);
	}

	public String getDowngradeInfo() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("V5");
	}

	public void setDowngradeInfo(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid ChaseNet and Visa Authorization Downgrade Information: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid ChaseNet and Visa Authorization Downgrade Information: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("V5", value);
	}

	public String getAuthorizationType() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("V7");
	}

	public void setAuthorizationType(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Credit Authorization Type Data: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid Credit Authorization Type Data: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("V7", value);
	}

	public String getCommercialCardIndicator() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("V8");
	}

	public void setCommercialCardIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid ChaseNet and Visa Commercial Card Indicator : " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid ChaseNet and Visa Commercial Card Indicator : " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("V8", value);
	}

	public String getAuthorizationSource() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("V9");
	}

	public void setAuthorizationSource(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid ChaseNet and Visa Authorization Source: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z]+"))
			throw new ISOException("Invalid ChaseNet and Visa Authorization Source: " + value + "; letters only allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("V9", value);
	}

	public String getContactlessAuthResult() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("VC");
	}

	public void setContactlessAuthResult(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid  EMV/Contactless Card Authentication Results Code: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid  EMV/Contactless Card Authentication Results Code: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("VC", value);
	}

	public String getCardLevelResult() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("VD");
	}

	public void setCardLevelResult(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid ChaseNet and Visa Card Level Results Code: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid ChaseNet and Visa Card Level Results Code: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("VD", value);
	}

	public String getCVVResult() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("VE");
	}

	public void setCVVResult(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid ChaseNet and Visa CVV/iCVV/CAM Results Code: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid ChaseNet and Visa CVV/iCVV/CAM Results Code: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("VE", value);
	}

	public String getSpendQualifiedResult() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtag("VS");
	}

	public void setSpendQualifiedResult(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Spend Qualified Results Code: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Spend Qualified Results Code: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(48);
		if(sub == null) {
			sub = new SubtagField(48, 2);
			set(sub);
		}
		sub.setSubtag("VS", value);
	}
	
	public String getMessageFormatErrorInformation() throws ISOException {
		if(!hasField(48))
			return null;
		return ((SubtagField) getComponent(48)).getSubtagPiece("ZZ", 0, 5);
	}

	public String getCurrencyCode() throws ISOException {
		if(!hasField(49))
			return null;
		return (String) getValue(49);
	}

	public void setCurrencyCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Currency Code, Transaction: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Currency Code, Transaction: " + value + "; must be all digits");
		set(49, value);
	}

	public String getPIN() throws ISOException {
		if(!hasField(52))
			return null;
		return (String) getValue(52);
	}

	public void setPIN(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 16)
			throw new ISOException("Invalid Personal Identification Number (PIN): " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Personal Identification Number (PIN): " + value + "; special characters not allowed");
		set(52, value);
	}

	public String getKSN() throws ISOException {
		if(!hasField(53))
			return null;
		return (String) getValue(53);
	}

	public void setKSN(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 16)
			throw new ISOException("Invalid Security Related Control Information (KSN): " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Security Related Control Information (KSN): " + value + "; special characters not allowed");
		set(53, value);
	}

	public String getAdditionalAmount() throws ISOException {
		if(!hasField(54))
			return null;
		return (String) getValue(54);
	}

	public void setAdditionalAmount(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 12)
			throw new ISOException("Invalid Additional Amount: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Additional Amount: " + value + "; must be all digits");
		set(54, value);
	}

	public String getChipCardData() throws ISOException {
		if(!hasField(55))
			return null;
		return (String) getValue(55);
	}

	public void setChipCardData(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > BIT_55_MAX_LENGTH)
			throw new ISOException("Invalid EMV/Contactless Chip Card Data: " + value);
		value = value.toUpperCase();
		set(55, value);
	}

	public String getAttendedIndicator() throws ISOException {
		if(!hasField(60))
			return null;
		return ((SubtagField) getComponent(60)).getSubtagPiece("A1", 0, 2);
	}

	public void setAttendedIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Attended Terminal Data: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Attended Terminal Data: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(60);
		if(sub == null) {
			sub = new SubtagField(60, 3);
			set(sub);
		}
		sub.setSubtagPiece("A1", value, 0, 2, 21);
	}

	public String getLocationIndicator() throws ISOException {
		if(!hasField(60))
			return null;
		return ((SubtagField) getComponent(60)).getSubtagPiece("A1", 2, 2);
	}

	public void setLocationIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Terminal Location: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Terminal Location: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(60);
		if(sub == null) {
			sub = new SubtagField(60, 3);
			set(sub);
		}
		sub.setSubtagPiece("A1", value, 2, 2, 21);
	}

	public String getCardholderAttendence() throws ISOException {
		if(!hasField(60))
			return null;
		return ((SubtagField) getComponent(60)).getSubtagPiece("A1", 4, 2);
	}

	public void setCardholderAttendence(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Cardholder Attendence: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Cardholder Attendence: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(60);
		if(sub == null) {
			sub = new SubtagField(60, 3);
			set(sub);
		}
		sub.setSubtagPiece("A1", value, 4, 2, 21);
	}

	public String getCardPresentIndicator() throws ISOException {
		if(!hasField(60))
			return null;
		return ((SubtagField) getComponent(60)).getSubtagPiece("A1", 6, 1);
	}

	public void setCardPresentIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Card Present Indicator: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Card Present Indicator: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(60);
		if(sub == null) {
			sub = new SubtagField(60, 3);
			set(sub);
		}
		sub.setSubtagPiece("A1", value, 6, 1, 21);
	}

	public String getCATIndicator() throws ISOException {
		if(!hasField(60))
			return null;
		return ((SubtagField) getComponent(60)).getSubtagPiece("A1", 7, 2);
	}

	public void setCATIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Cardholder Activated Terminal (CAT) Information: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Cardholder Activated Terminal (CAT) Information: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(60);
		if(sub == null) {
			sub = new SubtagField(60, 3);
			set(sub);
		}
		sub.setSubtagPiece("A1", value, 7, 2, 21);
	}

	public String getEntryCapability() throws ISOException {
		if(!hasField(60))
			return null;
		return ((SubtagField) getComponent(60)).getSubtagPiece("A1", 9, 2);
	}

	public void setEntryCapability(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Terminal Entry Capability: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Terminal Entry Capability: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(60);
		if(sub == null) {
			sub = new SubtagField(60, 3);
			set(sub);
		}
		sub.setSubtagPiece("A1", value, 9, 2, 21);
	}


	public String getDoingBusinessAs() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 0, 25);
	}

	public void setDoingBusinessAs(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 25)
			throw new ISOException("Invalid DBA Name: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&*-]+"))
			throw new ISOException("Invalid DBA Name: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 0, 25, 157);
	}

	public String getCustomerId() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 25, 8);
	}

	public void setCustomerId(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 8)
			throw new ISOException("Invalid Sub Merchant ID : " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Sub Merchant ID : " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 25, 8, 157);
	}

	public String getStreet() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 33, 25);
	}

	public void setStreet(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 25)
			throw new ISOException("Invalid Street: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Street: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 33, 25, 157);
	}

	public String getCity() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 58, 15);
	}

	public void setCity(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 15)
			throw new ISOException("Invalid City: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid City: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 58, 15, 157);
	}

	public String getStateCd() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 73, 3);
	}

	public void setStateCd(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid State: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid State: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 73, 3, 157);
	}

	public String getPostal() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 76, 9);
	}

	public void setPostal(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 9)
			throw new ISOException("Invalid Postal Code: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Postal Code: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 76, 9, 157);
	}

	public String getCountryNum() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 85, 3);
	}

	public void setCountryNum(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Country Code: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Country Code: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 85, 3, 157);
	}

	public String getPhone() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 108, 10);
	}

	public void setPhone(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 10)
			throw new ISOException("Invalid Contact Info: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Contact Info: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 108, 10, 157);
	}

	public String getEmail() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 118, 19);
	}

	public void setEmail(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 19)
			throw new ISOException("Invalid Email: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9_ ./,$@&-]+"))
			throw new ISOException("Invalid Email: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 118, 19, 157);
	}

	public String getSellerId() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("AG", 137, 20);
	}

	public void setSellerId(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 20)
			throw new ISOException("Invalid Seller ID: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Seller ID: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("AG", value, 137, 20, 157);
	}

	public String getHostReferenceInfo() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtag("H1");
	}

	public void setHostReferenceInfo(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 16)
			throw new ISOException("Invalid Host Transaction Reference Information: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Host Transaction Reference Information: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtag("H1", value);
	}

	public String getHostResponseData() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtag("H2");
	}

	public void setHostResponseData(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 32)
			throw new ISOException("Invalid Host Transaction Terminal Response Data: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Host Transaction Terminal Response Data: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtag("H2", value);
	}

	public String getHostErrorNumber() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtag("H3");
	}

	public void setHostErrorNumber(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 5)
			throw new ISOException("Invalid Host Transaction Error Number: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Host Transaction Error Number: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtag("H3", value);
	}

	public String getVendorIdentifier() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("P1", 0, 4);
	}

	public void setVendorIdentifier(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid Hardware / Vendor Identifier: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Hardware / Vendor Identifier: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, 0, 4, 28);
	}

	public String getSoftwareIdentifier() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("P1", 4, 4);
	}

	public void setSoftwareIdentifier(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 4)
			throw new ISOException("Invalid Software Identifier: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Software Identifier: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, 4, 4, 28);
	}

	public String getHardwareSerialNumber() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtagPiece("P1", 8, 20);
	}

	public void setHardwareSerialNumber(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 20)
			throw new ISOException("Invalid Hardware Serial Number: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Hardware Serial Number: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtagPiece("P1", value, 8, 20, 28);
	}

	public String getPOSCapabilities2() throws ISOException {
		if(!hasField(62))
			return null;
		return ((SubtagField) getComponent(62)).getSubtag("P2");
	}

	public void setPOSCapabilities2(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 999)
			throw new ISOException("Invalid POS/VAR Capabilities 2: " + value);
		value = value.toUpperCase();
		SubtagField sub = (SubtagField) getComponent(62);
		if(sub == null) {
			sub = new SubtagField(62, 3);
			set(sub);
		}
		sub.setSubtag("P2", value);
	}

	public String getEcommerceOrderNumber() throws ISOException {
		if(!hasField(63))
			return null;
		return ((SubtagField) getComponent(63)).getSubtagPiece("E1", 0, 16);
	}

	public void setEcommerceOrderNumber(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 16)
			throw new ISOException("Invalid Electronic Commerce Industry Data - Order Number: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Electronic Commerce Industry Data - Order Number: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(63);
		if(sub == null) {
			sub = new SubtagField(63, 3);
			set(sub);
		}
		sub.setSubtagPiece("E1", value, 0, 16, 20);
	}

	public String getEcommerceSecurityIndicator() throws ISOException {
		if(!hasField(63))
			return null;
		return ((SubtagField) getComponent(63)).getSubtagPiece("E1", 16, 2);
	}

	public void setEcommerceSecurityIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Electronic Commerce Industry Data - Security Indicator: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Electronic Commerce Industry Data - Security Indicator: " + value + "; must be all digits");
		SubtagField sub = (SubtagField) getComponent(63);
		if(sub == null) {
			sub = new SubtagField(63, 3);
			set(sub);
		}
		sub.setSubtagPiece("E1", value, 16, 2, 20);
	}

	public String getEcommerceGoodsIndicator() throws ISOException {
		if(!hasField(63))
			return null;
		return ((SubtagField) getComponent(63)).getSubtagPiece("E1", 18, 2);
	}

	public void setEcommerceGoodsIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 2)
			throw new ISOException("Invalid Electronic Commerce Industry Data - Goods Indicator: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Electronic Commerce Industry Data - Goods Indicator: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(63);
		if(sub == null) {
			sub = new SubtagField(63, 3);
			set(sub);
		}
		sub.setSubtagPiece("E1", value, 18, 2, 20);
	}

	public String getMotoOrderNumber() throws ISOException {
		if(!hasField(63))
			return null;
		return ((SubtagField) getComponent(63)).getSubtagPiece("M1", 0, 9);
	}

	public void setMotoOrderNumber(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 9)
			throw new ISOException("Invalid Mail Order Industry Data - OrderNumber: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Mail Order Industry Data - OrderNumber: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(63);
		if(sub == null) {
			sub = new SubtagField(63, 3);
			set(sub);
		}
		sub.setSubtagPiece("M1", value, 0, 9, 10);
	}

	public String getMotoTypeIndicator() throws ISOException {
		if(!hasField(63))
			return null;
		return ((SubtagField) getComponent(63)).getSubtagPiece("M1", 9, 1);
	}

	public void setMotoTypeIndicator(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 1)
			throw new ISOException("Invalid Mail Order Industry Data - Type Indicator: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Mail Order Industry Data - Type Indicator: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(63);
		if(sub == null) {
			sub = new SubtagField(63, 3);
			set(sub);
		}
		sub.setSubtagPiece("M1", value, 9, 1, 10);
	}
	
	public String getRetailIndustryDataInvoiceNumber() throws ISOException {
		if(!hasField(63))
			return null;
		return ((SubtagField) getComponent(63)).getSubtagPiece("R2", 0, 6);
	}
	
	public void setRetailIndustryDataInvoiceNumber(String value) throws ISOException {
		if(value == null || !value.matches("[0-9]{6}"))
			throw new ISOException("Invalid Retail Industry Data Invoice Number: " + value);
		SubtagField sub = (SubtagField) getComponent(63);
		if(sub == null) {
			sub = new SubtagField(63, 3);
			set(sub);
		}
		sub.setSubtagPiece("R2", value, 0, 6, 26);
	}
	
	public String getRetailIndustryDataTranInformation() throws ISOException {
		if(!hasField(63))
			return null;
		return ((SubtagField) getComponent(63)).getSubtagPiece("R2", 6, 20);
	}
	
	public void setRetailIndustryDataTranInformation(String value) throws ISOException {
		if(value == null || value.length() != 20)
			throw new ISOException("Invalid Retail Industry Data Tran Information: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Retail Industry Data Tran Information: " + value + "; special characters not allowed");
		SubtagField sub = (SubtagField) getComponent(63);
		if(sub == null) {
			sub = new SubtagField(63, 3);
			set(sub);
		}
		sub.setSubtagPiece("R2", value, 6, 20, 26);
	}		

	public String getNetworkManagementCode() throws ISOException {
		if(!hasField(70))
			return null;
		return (String) getValue(70);
	}

	public void setNetworkManagementCode(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 3)
			throw new ISOException("Invalid Network Management Information Code: " + value);
		if(!value.matches("\\d+"))
			throw new ISOException("Invalid Network Management Information Code: " + value + "; must be all digits");
		set(70, value);
	}

	public String getOriginalTransactionData() throws ISOException {
		if(!hasField(90))
			return null;
		return (String) getValue(90);
	}

	public void setOriginalTransactionData(String value) throws ISOException {
		if(value == null || value.length() == 0 || value.length() > 46)
			throw new ISOException("Invalid Original Transaction Data: " + value);
		value = value.toUpperCase();
		if(!value.matches("[A-Z0-9 ./,$@&-]+"))
			throw new ISOException("Invalid Original Transaction Data: " + value + "; special characters not allowed");
		set(90, value);
	}

}
