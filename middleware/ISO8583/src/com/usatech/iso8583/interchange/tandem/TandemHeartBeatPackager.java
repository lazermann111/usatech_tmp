package com.usatech.iso8583.interchange.tandem;

import org.jpos.iso.ISOBasePackager;
import org.jpos.iso.ISOComponent;
import org.jpos.iso.ISOException;

public class TandemHeartBeatPackager extends ISOBasePackager {
	public TandemHeartBeatPackager() {
		super();
	}

	@Override
	protected int getMaxValidField() {
		return 0;
	}

	@Override
	public byte[] pack(ISOComponent m) throws ISOException {
		return new byte[] {};
	}
	
	@Override
	public int unpack (ISOComponent m, byte[] b) throws ISOException {
		return 0;
	}
}
