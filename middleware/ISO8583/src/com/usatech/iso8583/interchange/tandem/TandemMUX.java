package com.usatech.iso8583.interchange.tandem;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import simple.io.Log;

import com.usatech.iso8583.jpos.USATISOChannel;
import com.usatech.iso8583.jpos.USATISOMUX;

public class TandemMUX extends USATISOMUX {
	private static Log log = Log.getLog();

	protected long lastReset = -99;
	protected AtomicInteger sequenceNumberCounter = new AtomicInteger(1);
	
	protected long heartbeatInterval = 180 * 1000;
	protected long heartbeatInitialDelay = 1000;
	protected long inactivityTimeout = 200 * 1000;

	private ScheduledExecutorService connectionManagerExecutor;
	private ScheduledFuture connectionManagerFuture;

	public TandemMUX(USATISOChannel channel, ThreadPoolExecutor threadPool, boolean doConnect) {
		super(channel, threadPool, doConnect);
	}
	
	public long getHeartbeatInterval() {
		return heartbeatInterval;
	}

	public void setHeartbeatInterval(long heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}

	public long getHeartbeatInitialDelay() {
		return heartbeatInitialDelay;
	}

	public void setHeartbeatInitialDelay(long heartbeatInitialDelay) {
		this.heartbeatInitialDelay = heartbeatInitialDelay;
	}

	public long getInactivityTimeout() {
		return inactivityTimeout;
	}

	public void setInactivityTimeout(long inactivityTimeout) {
		this.inactivityTimeout = inactivityTimeout;
	}

	public synchronized void start() {
		connect();
		if(connectionManagerExecutor != null)
			return;

		log.info("Starting up...");

		connectionManagerExecutor = Executors.newSingleThreadScheduledExecutor();
		connectionManagerFuture = connectionManagerExecutor.scheduleWithFixedDelay(new PersistentConnectionManager(), heartbeatInitialDelay, 1000, TimeUnit.MILLISECONDS);
	}

	public void terminate() {
		super.terminate();
		log.warn("Shutting down...");
		connectionManagerExecutor.shutdownNow();
	}

	protected synchronized String getKey(ISOMsg isoMsg) throws ISOException {
		if(getISOChannel().getConnectTime() > lastReset) {
			lastReset = System.currentTimeMillis();
			sequenceNumberCounter.set(1);
		}

		TandemMsg tandemMsg = (TandemMsg) isoMsg;
		if(tandemMsg.getDirection() == ISOMsg.INCOMING) {
			return Integer.toString(tandemMsg.getSequenceNumber());
		}
		
		int sequenceNumber = sequenceNumberCounter.getAndIncrement();
		sequenceNumberCounter.compareAndSet(0xFFFF, 1);

		tandemMsg.setSequenceNumber(sequenceNumber);
		return Integer.toString(sequenceNumber);
	}

	public String toString() {
		return format("TandemMUX[doConnect={0},connected={1},terminating={2},sequenceNumberCounter={3}]", getConnect(), isConnected(), isTerminating(), sequenceNumberCounter.get());
	}
	
	protected class PersistentConnectionManager implements Runnable {
		public void run() {
			if(!isConnected())
				return;

			USATISOChannel channel = getISOChannel();
			
			long receiveET = System.currentTimeMillis() - (channel.getLastReceive() > 0 ? channel.getLastReceive() : channel.getConnectTime() > 0 ? channel.getConnectTime() : System.currentTimeMillis());
			long sendET = System.currentTimeMillis() - channel.getLastSend();
			if(receiveET > inactivityTimeout) {
				log.warn("Have not received a message from the host in the last {0}ms!  Forcing reconnect...", receiveET);
				try {
					getISOChannel().reconnect();
				} catch(IOException e) {
					log.error("Failed to force reconnect: {0}", e.getMessage(), e);
				}
			} else if(sendET > heartbeatInterval) {
				try {
					TandemMsg heartbeat = new TandemMsg();
					heartbeat.setDirection(ISOMsg.OUTGOING);
					send(heartbeat);
				} catch(Exception e) {
					log.warn("Failed to send heartbeat", e);
				}
			}
		}
	}
}
