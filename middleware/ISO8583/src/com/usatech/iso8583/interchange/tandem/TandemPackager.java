package com.usatech.iso8583.interchange.tandem;

import org.jpos.iso.AsciiPrefixer;
import org.jpos.iso.IFA_LLCHAR;
import org.jpos.iso.IFA_LLLCHAR;
import org.jpos.iso.IFA_NUMERIC;
import org.jpos.iso.IFB_BITMAP;
import org.jpos.iso.IF_CHAR;
import org.jpos.iso.ISOBasePackager;
import org.jpos.iso.ISOFieldPackager;

public class TandemPackager extends ISOBasePackager {
	public TandemPackager() {
		super();
		super.setFieldPackager(new ISOFieldPackager[91]);
		super.setFieldPackager(0, new IFA_NUMERIC(4, "MESSAGE TYPE INDICATOR"));
		super.setFieldPackager(1, new IFB_BITMAP(16, "BIT MAP"));
		super.setFieldPackager(2, new IFA_LLCHAR(19, "PRIMARY ACCOUNT NUMBER"));
		super.setFieldPackager(3, new IFA_NUMERIC(6, "PROCESSING CODE"));
		super.setFieldPackager(4, new IFA_NUMERIC(12, "TRANSACTION AMOUNT"));
		super.setFieldPackager(7, new IFA_NUMERIC(14, "TRANSACTION DATE AND TIME"));
		super.setFieldPackager(11, new IFA_NUMERIC(6, "SYSTEMS TRACE AUDIT NUMBER"));
		super.setFieldPackager(12, new IFA_NUMERIC(6, "TIME, LOCAL TRANSACTION"));
		super.setFieldPackager(13, new IFA_NUMERIC(8, "DATE, LOCAL TRANSACTION"));
		super.setFieldPackager(14, new IFA_NUMERIC(4, "EXPIRATION DATE"));
		super.setFieldPackager(15, new IFA_NUMERIC(4, "DATE, SETTLEMENT"));
		super.setFieldPackager(18, new IFA_NUMERIC(4, "MERCHANT CATEGORY CODE"));
		super.setFieldPackager(22, new IFA_NUMERIC(3, "POS ENTRY MODE"));
		super.setFieldPackager(23, new IFA_NUMERIC(2, "EMV/CONTACTLESS PAN SEQUENCE NUMBER"));
		super.setFieldPackager(25, new IFA_NUMERIC(2, "POS CONDITION CODE"));
		super.setFieldPackager(35, new IFA_LLCHAR(37, "TRACK 2 DATA"));
		super.setFieldPackager(37, new IF_CHAR(12, "RETRIEVAL REFERENCE NUMBER"));
		super.setFieldPackager(38, new IF_CHAR(6, "AUTHORIZATION ID RESPONSE"));
		super.setFieldPackager(39, new IF_CHAR(2, "RESPONSE CODE"));
		super.setFieldPackager(41, new IFA_NUMERIC(3, "CARD ACQUIRER TERMINAL ID"));
		super.setFieldPackager(42, new IFA_NUMERIC(12, "CARD ACQUIRER ID"));
		super.setFieldPackager(44, new SubtagPackager(9999, "ADDITIONAL RESPONSE DATA", AsciiPrefixer.LLLL, 4));
		super.setFieldPackager(45, new IFA_LLCHAR(76, "TRACK 1 DATA"));
		super.setFieldPackager(48, new SubtagPackager(999, "ADDITIONAL AUTHORIZATION DATA", AsciiPrefixer.LLL, 2));
		super.setFieldPackager(49, new IFA_NUMERIC(3, "CURRENCY CODE, TRANSACTION"));
		super.setFieldPackager(52, new IF_CHAR(16, "PERSONAL IDENTIFICATION NUMBER (PIN)"));
		super.setFieldPackager(53, new IF_CHAR(16, "SECURITY RELATED CONTROL INFORMATION (KSN)"));
		super.setFieldPackager(54, new IFA_NUMERIC(12, "ADDITIONAL AMOUNT"));
		super.setFieldPackager(55, new IFA_LLLCHAR(513, "EMV/CONTACTLESS CHIP CARD DATA"));
		super.setFieldPackager(60, new SubtagPackager(999, "RESERVED NATIONAL - 1", AsciiPrefixer.LLL, 3));
		super.setFieldPackager(62, new SubtagPackager(999, "RESERVED PRIVATE - 2", AsciiPrefixer.LLL, 3));
		super.setFieldPackager(63, new SubtagPackager(999, "RESERVED PRIVATE - 3", AsciiPrefixer.LLL, 3));
		super.setFieldPackager(70, new IFA_NUMERIC(3, "NETWORK MANAGEMENT INFORMATION CODE"));
		super.setFieldPackager(90, new IF_CHAR(46, "ORIGINAL TRANSACTION DATA"));
	}

}
