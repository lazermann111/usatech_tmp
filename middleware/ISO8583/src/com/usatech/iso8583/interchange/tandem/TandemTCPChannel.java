package com.usatech.iso8583.interchange.tandem;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOFilter.VetoException;
import org.jpos.iso.ISOHeader;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOUtil;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;

import simple.io.Log;

import com.usatech.iso8583.jpos.USATISOChannel;

public class TandemTCPChannel extends BaseChannel implements USATISOChannel {
	private static Log log = Log.getLog();

	private static final byte[] CPNS_RESERVED_BYTES = {0x00,0x00,0x00,0x00};
	private static final byte[] PASS_THRU_INDENTIFIER_BYTES = {0x24,0x24,0x55,0x50,0x54,0x44}; // $$UPTD
	private static final byte[] SEQUENCE_NUMBER_LENGTH_BYTES = {0x30,0x30,0x32}; // 002

	protected long connectTime = -1;
	protected long lastSend = -1;
	protected long lastReceive = -1;

	public TandemTCPChannel(String host, int port, ISOPackager p) {
		super(host, port, p);
	}

	public TandemTCPChannel(ISOPackager p) throws IOException {
		super(p);
	}

	public TandemTCPChannel(ISOPackager p, ServerSocket serverSocket) throws IOException {
		super(p, serverSocket);
	}

	public String toString() {
		return format("TandemTCPChannel[{0}:{1}]", getHost() , getPort());
	}

	public long getConnectTime() {
		return connectTime;
	}

	// Subclass has a bug that appends the socket host and port to the realm every time it is re-connected
	public void setLogger(Logger logger, String realm) {
		this.logger = logger;
		if(realm == null || !realm.matches(".*/[^:]+:\\d+/[^:]+:\\d+"))
			this.realm = realm;
	}

	public long getLastSend() {
		return lastSend;
	}

	public long getLastReceive() {
		return lastReceive;
	}
	
	@Override
	protected Socket newSocket() throws IOException {
		try {
			Socket s = super.newSocket();
			connectTime = System.currentTimeMillis();
			lastReceive = -1;
			log.info("Successfully connected to {0}:{1}", getHost(), getPort());
			return s;
		} catch(IOException e) {
			log.warn("Failed to connect to {0}:{1}", getHost(), getPort());
			throw e;
		}
	}

	@Override
	public void disconnect() throws IOException {
		log.info("Disconnecting from {0}:{1}", getHost(), getPort());
		super.disconnect();
	}

	@Override
  public void connect() throws IOException {
		log.info("Connecting to {0}:{1}", getHost(), getPort());
		super.connect();
		log.info("Connected to {0}:{1}", getHost(), getPort());
	}

	@Override
  public void reconnect() throws IOException {
		log.info("Reconnecting...");
		super.reconnect();
	}
	
	@Override
	protected void sendMessageLength(int len) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	protected int getMessageLength() throws IOException, ISOException {
		throw new UnsupportedOperationException();
	}

	@Override
	protected int getHeaderLength() {
		throw new UnsupportedOperationException();
	}

	@Override
	protected ISOHeader getDynamicHeader(byte[] image) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected ISOPackager getDynamicPackager(ISOMsg m) {
		if(((TandemMsg) m).isHeartBeat())
			return new TandemHeartBeatPackager();
		else return packager;
	}

	@Override
	protected ISOPackager getDynamicPackager(byte[] bytes) {
		if (bytes.length == 0)
			return new TandemHeartBeatPackager();
		return packager;
	}

	@Override
	protected ISOMsg createMsg() {
		return new TandemMsg();
	}

	@Override
	public void send(ISOMsg msg) throws IOException, ISOException, VetoException {
		LogEvent logEvent = new LogEvent(this, "send");
		
		int sequenceNumber = -1;
		int messageLength = -1;
		
		try {
			if(!isConnected()) {
				logEvent.addMessage(msg);
				throw new ISOException("Unconnected ISOChannel");
			}

			msg.setDirection(ISOMsg.OUTGOING);
			msg = applyOutgoingFilters(msg, logEvent);
			msg.setDirection(ISOMsg.OUTGOING); // filter may have dropped this info
			
			TandemMsg tandemMsg = (TandemMsg) msg;
			logEvent.addMessage(msg);

			if (tandemMsg.isHeartBeat()) {
				synchronized(serverOut) {
					serverOut.writeShort(0);
					serverOut.write(CPNS_RESERVED_BYTES);
					serverOut.flush();
				}
				
				log.debug("Sent heartbeat");
			} else {
				byte[] payload;
				synchronized(packager) {
					tandemMsg.setPackager(getDynamicPackager(tandemMsg));
					payload = tandemMsg.pack();
				}
				byte[] header = tandemMsg.getHeader();
				sequenceNumber = tandemMsg.getSequenceNumber();
				messageLength = payload.length + 11 + (header == null ? 0 : header.length);
				synchronized(serverOut) {
					serverOut.writeShort(messageLength); // 2 byte unsigned short, big endian
					serverOut.write(CPNS_RESERVED_BYTES); // 4 bytes but not included in length calculation
					serverOut.write(PASS_THRU_INDENTIFIER_BYTES); // 6 bytes "$$UPTD"
					serverOut.write(SEQUENCE_NUMBER_LENGTH_BYTES); // 3 bytes "002"
					serverOut.writeShort(sequenceNumber); // 2 byte unsigned short, big endian
					if(header != null)
						serverOut.write(header);
					serverOut.write(payload);
					serverOut.flush();
				}
				
				log.debug("Sent {0}: sequence={1}, length={2}", msg.getMTI(), sequenceNumber, messageLength+6);
			}
			
			cnt[TX]++;
			lastSend = System.currentTimeMillis();
			setChanged();
			notifyObservers(msg);
		} catch(Exception e) {
			String errorMsg = format("Send {0} failed: sequence={1}, length={2}", msg.getMTI(), sequenceNumber, messageLength+6);
			log.warn(errorMsg, e);
			logEvent.addMessage(errorMsg);
			logEvent.addMessage(e);
			throw new ISOException(errorMsg, e);
		} finally {
			Logger.log(logEvent);
		}
	}

	public ISOMsg receive() throws IOException, ISOException {
		int messageLength = -1;
		byte[] payload = null;
		int sequenceNumber = -1;
		
		LogEvent logEvent = new LogEvent(this, "receive");

		Socket socket = getSocket();
		
		TandemMsg tandemMsg = (TandemMsg) createMsg();
		tandemMsg.setSource(this);
		
		try {
			if(!isConnected())
				throw new ISOException("Unconnected ISOChannel");

			synchronized(serverIn) {
				messageLength = serverIn.readUnsignedShort(); // 2 bytes
				serverIn.readInt(); // just swallow 4 bytes, CPNS reserved
				if (messageLength == 0) {
					payload = new byte[0];
					log.debug("Received heartbeat");
				} else if (messageLength <= 11) {
					payload = new byte[messageLength];
					serverIn.readFully(payload);
					throw new ISOException(format("Received invalid message: not enough data, length={0}", messageLength));
				} else {
					byte[] passThruHeader = new byte[9];
					serverIn.readFully(passThruHeader); // 9 bytes, should be $$UPTD002 (validate this)?
					tandemMsg.setSequenceNumber(serverIn.readUnsignedShort()); // 2 bytes
					byte[] header = new byte[20];
					serverIn.readFully(header);
					tandemMsg.setHeader(header);
					payload = new byte[messageLength - 11 - header.length];
					serverIn.readFully(payload);
					log.debug("Received {0} bytes, sequenceNumber={1}", messageLength + 6, sequenceNumber);
				}
			}

			synchronized(packager) {
				tandemMsg.setPackager(getDynamicPackager(payload));
				tandemMsg.unpack(payload);
			}

			tandemMsg.setDirection(ISOMsg.INCOMING);
			tandemMsg = (TandemMsg) applyIncomingFilters(tandemMsg, header, payload, logEvent);
			tandemMsg.setDirection(ISOMsg.INCOMING);
			
			cnt[RX]++;
			lastReceive = System.currentTimeMillis();
			logEvent.addMessage(tandemMsg);
			setChanged();
			notifyObservers(tandemMsg);
		} catch(Exception e) {
			String errorMsg = format("Receive failed: sequence={1}, length={2}", sequenceNumber, messageLength+6);
			log.debug(errorMsg, e);
			if(payload != null)
				log.debug("--- data ---\n{0}", ISOUtil.hexdump(payload));
			
			logEvent.addMessage(e);
			if(payload != null) {
				logEvent.addMessage("--- data ---");
				logEvent.addMessage(ISOUtil.hexdump(payload));
			}
			
			if (e instanceof IOException) {
				socket.close();
				throw (IOException) e;
			}
			
			throw new ISOException(errorMsg, e);
		} finally {
			Logger.log(logEvent);
		}
		
		return tandemMsg;
	}
}
