package com.usatech.iso8583.interchange.tandem;

import java.io.PrintStream;

import org.jpos.iso.ISOField;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.XMLPackager;

public class MaskedField extends ISOField {
	public MaskedField() {
	}

	public MaskedField(int n) {
		super(n);
	}

	public MaskedField(int n, String v) {
		super(n, v);
	}

	@Override
	public void dump(PrintStream p, String indent) {
		p.println(indent + "<" + XMLPackager.ISOFIELD_TAG + " " + XMLPackager.ID_ATTR + "=\"" + fieldNumber + "\" " + XMLPackager.VALUE_ATTR + "=\"" + ISOUtil.normalize(ISOUtil.protect(value)) + "\"/>");
	}
}
