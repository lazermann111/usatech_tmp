package com.usatech.iso8583.interchange.tandem;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.XMLPackager;

public class SubtagField extends ISOField {
	protected final Map<String, SubtagValue> subtags = new LinkedHashMap<String, SubtagValue>();
	protected Set<String> subtagKeys = null;
	protected final int keyLength;
	protected final int lengthBytes;
	protected final int maxValueLength;

	protected class SubtagValue {
		protected char[] array;
		protected String value;

		public String getValue() {
			if(array == null)
				return value;
			int l = array.length;
			while(l > 0 && Character.isWhitespace(array[l - 1]))
				l--;
			return new String(array, 0, l);
		}

		public void setValue(String value) {
			this.value = value;
			array = null;
		}

		public String getValuePiece(int offset, int length) {
			if(value != null) {
				if(value.length() <= offset)
					return null;
				if(value.length() < offset + length)
					length = value.length() - offset;
				while(length > 0 && Character.isWhitespace(value.charAt(offset + length - 1)))
					length--;
				return value.substring(offset, offset + length);
			}
			if(array != null) {
				if(array.length <= offset)
					return null;
				if(array.length < offset + length)
					length = array.length - offset;
				while(length > 0 && Character.isWhitespace(array[offset + length - 1]))
					length--;
				return new String(array, offset, length);
			}
			return null;
		}

		public void setValuePiece(String value, int offset, int length, int maxLength) {
			if(offset + length > maxLength)
				maxLength = offset + length;
			if(array == null) {
				array = new char[maxLength];
				Arrays.fill(array, 0, array.length, ' ');
			} else if(array.length < offset + length) {
				char[] tmp = new char[maxLength];
				Arrays.fill(tmp, array.length, tmp.length, ' ');
				System.arraycopy(array, 0, tmp, 0, array.length);
				array = tmp;
			} else
				Arrays.fill(array, offset, offset + length, ' ');
			if(value != null && value.length() > 0)
				value.getChars(0, Math.min(length, value.length()), array, offset);
		}

		public StringBuilder appendTo(StringBuilder sb, boolean mask) {
			if(mask) {
				int length = value != null ? value.length() : array != null ? array.length : 0;
				for(int i = 0; i < length; i++)
					sb.append('_');
			} else if(value != null)
				sb.append(value);
			else if(array != null)
				sb.append(array);
			return sb;
		}
	}
	public SubtagField(int fieldNumber, int lengthBytes) {
		super(fieldNumber);
		this.keyLength = 2;
		this.lengthBytes = lengthBytes;
		this.maxValueLength = ((int) Math.pow(10, lengthBytes)) - 1;
	}

	public void dump(PrintStream p, String indent) {
		p.println(indent + "<" + XMLPackager.ISOFIELD_TAG + " " + XMLPackager.ID_ATTR + "=\"" + fieldNumber + "\" " + XMLPackager.VALUE_ATTR + "=\"" + ISOUtil.normalize(appendValue(new StringBuilder(), true).toString()) + "\"/>");
	}

	@Override
	public String getValue() {
		return appendValue(new StringBuilder(), false).toString();
	}

	protected StringBuilder appendValue(StringBuilder sb, boolean maskSensitive) {
		for(Map.Entry<String, SubtagValue> entry : subtags.entrySet()) {
			sb.append(entry.getKey());
			for(int i = 0; i < lengthBytes; i++)
				sb.append('0');
			int start = sb.length();
			entry.getValue().appendTo(sb, maskSensitive && TandemMsg.isBitProtected(fieldNumber, entry.getKey()));
			int end = sb.length();
			String lenStr = Integer.toString(end - start);
			sb.replace(start - lenStr.length(), start, lenStr);
		}
		return sb;
	}
	public void setValue(Object obj) throws ISOException {
		// parse
		String value = (String) obj;
		int offset = 0;
		while(offset < value.length()) {
			if(offset + keyLength + lengthBytes > value.length())
				throw new ISOException("Value is incorrect length; '" + value.substring(offset) + "' at position " + (offset + 1) + " is unexpected");
			// read key
			String key = value.substring(offset, offset + keyLength);
			offset += keyLength;
			// read length
			int len;
			try {
				len = Integer.parseInt(value.substring(offset, offset + lengthBytes));
			} catch(NumberFormatException e) {
				throw new ISOException("Invalid length string '" + value.substring(offset, offset + lengthBytes) + "' for subtag " + key + " at position " + (offset + 1), e);
			}
			offset += lengthBytes;
			if(offset + len > value.length())
				throw new ISOException("Value is incorrect length; Subtag '" + key + "' is set for data of length " + len + " but only " + (value.length() - offset) + " is avaiable at position " + (offset + 1));

			// read value
			setSubtag(key, value.substring(offset, offset + len));
			offset += len;
		}
	}

	@Override
	public byte[] getBytes() {
		return getValue().getBytes();
	}

	public Set<String> getSubtagKeys() {
		if(subtagKeys == null)
			subtagKeys = Collections.unmodifiableSet(subtags.keySet());
		return subtagKeys;
	}

	public String getSubtag(String key) {
		SubtagValue sv = subtags.get(key.toUpperCase());
		return sv == null ? null : sv.getValue();
	}

	public void setSubtag(String key, String value) throws ISOException {
		if(key.length() != keyLength)
			throw new ISOException("Subtag key must be " + keyLength + " digits");
		key = key.toUpperCase();
		if(!key.matches("[A-Z0-9]{" + keyLength + "}"))
			throw new ISOException("Subtag key must be " + keyLength + " letters or digits");
		if(value == null || value.length() == 0)
			subtags.remove(key);
		else if(value.length() > maxValueLength)
			throw new ISOException("Subtag value must be less than " + maxValueLength);
		else {
			SubtagValue sv = subtags.get(key);
			if(sv == null) {
				sv = new SubtagValue();
				subtags.put(key, sv);
			}
			sv.setValue(value);
		}
	}

	public String getSubtagPiece(String key, int offset, int length) {
		SubtagValue sv = subtags.get(key.toUpperCase());
		return sv == null ? null : sv.getValuePiece(offset, length);
	}

	public void setSubtagPiece(String key, String value, int offset, int length, int maxLength) throws ISOException {
		if(key.length() != keyLength)
			throw new ISOException("Subtag key must be " + keyLength + " digits");
		key = key.toUpperCase();
		if(!key.matches("[A-Z0-9]{" + keyLength + "}"))
			throw new ISOException("Subtag key must be " + keyLength + " letters or digits");
		if(value == null || value.length() == 0)
			subtags.remove(key);
		else if(value.length() > maxValueLength)
			throw new ISOException("Subtag value must be less than " + maxValueLength);
		else {
			SubtagValue sv = subtags.get(key);
			if(sv == null) {
				sv = new SubtagValue();
				subtags.put(key, sv);
			}
			sv.setValuePiece(value, offset, length, maxLength);
		}
	}

	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeShort(fieldNumber);
		out.writeUTF(getValue());
	}

	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		fieldNumber = in.readShort();
		try {
			setValue(in.readUTF());
		} catch(ISOException e) {
			throw new IOException(e);
		}
	}
}
