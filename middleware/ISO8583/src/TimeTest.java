import java.util.*;

public class TimeTest
{
	public static void main(String args[]) throws Exception
	{
		long millis = Long.parseLong(args[0]);
		Date date = new Date(millis*1000);
		System.out.println(date);
	}
}
