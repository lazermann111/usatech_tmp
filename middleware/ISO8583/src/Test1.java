import java.util.*;

public class Test1
{
	public static void main(String args[])
	{
		String s = "AUTH DECLINED                        605";
		System.out.println("\"" + s + "\" " + s.length());
		System.out.println("\"" + s.substring(0,35) + "\" " + s.substring(0,35).length());
		System.out.println("\"" + s.substring(0,35).trim() + "\" " + s.substring(0,35).trim().length());
	}
	
	private static void testLinkedList1(int iterations)
	{
		LinkedList<String> ll = new LinkedList<String>();
		for(int i=0; i<3; i++)
		{
			ll.addLast("MESSAGE"+i);
		}
		
		for(int i=0; i<iterations; i++)
		{
			for(String s : ll)
			{
				s.length();
			}
		}
	}
	
	private static void testArray(int iterations)
	{
		String[] array = new String[3];
		for(int i=0; i<3; i++)
		{
			array[i] = "MESSAGE"+i;
		}
		
		int len = array.length;
		
		for(int i=0; i<iterations; i++)
		{
			for(int j=0; j<array.length; j++)
			{
				array[j].length();
			}
		}
	}
}
