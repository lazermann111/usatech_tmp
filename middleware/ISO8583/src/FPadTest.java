import org.jpos.iso.*;

public class FPadTest
{
	public static void main(String args[]) throws Exception
	{
		String number = "1234123412341230";
		ISOStringFieldPackager p = new ISOStringFieldPackager(19, "PAN", NullPadder.INSTANCE, BCDInterpreter.RIGHT_PADDED_F, BcdPrefixer.LL);
		ISOField f = new ISOField (2, number);
		byte[] b = p.pack(f);
		System.out.println("  " + number);
		System.out.println(bytesToHex(b));
	}
	
	private static final char[] HEX_CHARS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String bytesToHex(byte[] ba)
	{
		int len = ba.length;
		int j;
		int k;
		StringBuffer sb = new StringBuffer((len * 3));

		for (int i = 0; i < len; i++)
		{
			j = (ba[i] & 0xf0) >> 4;
			k = ba[i] & 0xf;

			sb.append(HEX_CHARS[j]);
			sb.append(HEX_CHARS[k]);
		}

		return sb.toString();
	}
}
