package com.usatech.authoritylayer.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicReference;

import org.jpos.iso.BaseChannel;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import simple.io.Log;
import simple.util.concurrent.WaitFor;

import com.usatech.iso8583.interchange.tandem.TandemMUX;
import com.usatech.iso8583.interchange.tandem.TandemPackager;
import com.usatech.iso8583.interchange.tandem.TandemTCPChannel;

public class TandemNetworkTests 
{
	public static final byte[] HEARTBEAT_BYTES = {0x00,0x00,0x00,0x00,0x00,0x00};

	private static final Log log = Log.getLog();
	
	private static ThreadPoolExecutor threadPool;
	
	@BeforeClass
	public static void init()
	{
		threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	}

	@AfterClass
	public static void shutdown()
	{
		threadPool.shutdownNow();
	}
	
	@Test
	public void testStartupShutdown() throws Exception
	{
		final Socket mockSocket = mock(Socket.class);
		final InputStream mockInputStream = mock(InputStream.class);
		final OutputStream mockOutputStream = mock(OutputStream.class, withSettings().verboseLogging());
		
		when(mockSocket.getInputStream()).thenReturn(mockInputStream);
		when(mockSocket.getOutputStream()).thenReturn(mockOutputStream);
		
		final Object lockObject = new Object();
		
		when(mockInputStream.read()).then(new Answer<Byte>() {
			public Byte answer(InvocationOnMock invocation) throws Throwable {
				synchronized(lockObject) {
					lockObject.wait();
				}
				return -1;
			}
		});
		
		when(mockInputStream.read(any(byte[].class))).then(new Answer<Integer>() {
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				synchronized(lockObject) {
					lockObject.wait();
				}
				return -1;
			}
		});
		
		when(mockInputStream.read(any(byte[].class), anyInt(), anyInt())).then(new Answer<Integer>() {
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				synchronized(lockObject) {
					lockObject.wait();
				}
				return -1;
			}
		});
		
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) throws Throwable {
				synchronized(lockObject) {
					lockObject.notifyAll();
				}
				return null;
			}
		}).when(mockInputStream).close();
		
		TandemTCPChannel channel = new TandemTCPChannel("127.0.0.1", 12000, new TandemPackager()) {
			@Override
			protected Socket newSocket() throws IOException {
				connectTime = System.currentTimeMillis();
				lastReceive = -1;
				return mockSocket;
			}
		};
		
		channel.setTimeout(20000);
		
		TandemMUX mux = new TandemMUX(channel, threadPool, true);
		mux.setHeartbeatInitialDelay(10 * 20000); // don't want to send heartbeats for this test
		mux.start();
		
		if (!WaitFor.waitForTrue(mux, "isConnected", 2000))
			assertTrue("connect took too long", false);
		
		Thread.sleep(1000);
		
		assertTrue(mux.isConnected());
		assertTrue(mux.isReceiverRunning());
		assertTrue(mux.isTransmitterRunning());
		assertFalse(mux.isTerminating());
		assertFalse(mux.isTerminated());
		assertTrue(channel.getConnectTime() > 0);

		mux.terminate();

		assertTrue(mux.isTerminating());

		if (!WaitFor.waitForTrue(mux, "isTerminated", 2000))
			assertTrue("terminate took too long", false);

		assertFalse(mux.isConnected());
		assertFalse(mux.isReceiverRunning());
		assertFalse(mux.isTransmitterRunning());
		assertTrue(mux.isTerminated());
		assertTrue(channel.getConnectTime() > 0);
		
		verify(mockSocket, atLeastOnce()).setSoTimeout(20000);
		verify(mockSocket).getInputStream();
		verify(mockSocket).getOutputStream();
		verify(mockInputStream).read(any(byte[].class), anyInt(), anyInt());
		verify(mockSocket, atLeastOnce()).close();
		verify(mockSocket, atLeast(0)).getInetAddress();
		verify(mockSocket, atLeast(0)).getPort();
		
		verify(mockOutputStream, atLeast(0)).flush(); // FilterOutputStream.close calls flush
		verify(mockInputStream).close();
		verify(mockOutputStream).close();
		
		verifyNoMoreInteractions(mockSocket);
		verifyNoMoreInteractions(mockInputStream);
		verifyNoMoreInteractions(mockOutputStream);
	}
	
	@Test
	public void testReadHeartbeat() throws Exception
	{
		final Socket mockSocket = mock(Socket.class);
		final InputStream mockInputStream = mock(InputStream.class);
		final OutputStream mockOutputStream = mock(OutputStream.class, withSettings().verboseLogging());
		
		when(mockSocket.getInputStream()).thenReturn(mockInputStream);
		when(mockSocket.getOutputStream()).thenReturn(mockOutputStream);
		
		final HeartbeatSender heartbeatSender = new HeartbeatSender(180 * 1000);
		
		when(mockInputStream.read(any(byte[].class), anyInt(), anyInt())).then(new Answer<Integer>() {
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				byte[] bytes = invocation.getArgumentAt(0, byte[].class);
				int off = invocation.getArgumentAt(1, Integer.class);
				int len = invocation.getArgumentAt(2, Integer.class);
				return heartbeatSender.read(bytes, off, len);
			}
		});
		
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) throws Throwable {
				heartbeatSender.close();
				return null;
			}
		}).when(mockInputStream).close();		
		
		TandemTCPChannel channel = new TandemTCPChannel("127.0.0.1", 12000, new TandemPackager()) {
			@Override
			protected Socket newSocket() throws IOException {
				connectTime = System.currentTimeMillis();
				lastReceive = -1;
				return mockSocket;
			}
		};
		
		TandemMUX mux = new TandemMUX(channel, threadPool, true);
		mux.setHeartbeatInitialDelay(10 * 20000); // don't want to send heartbeats for this test
		mux.start();
		
		if (!WaitFor.waitForTrue(mux, "isConnected", 2000))
			assertTrue("connect took too long", false);
		
		Thread.sleep(1000);
		
		assertTrue(mux.getConnectTime() > 0);
		assertTrue(mux.getConnectionCount() == 1);
		assertTrue(mux.getTransmitCount() == 0);
		assertTrue(mux.getReceiveCount() == 1);
		assertTrue(channel.getLastReceive() > (System.currentTimeMillis() - 5000));
		assertTrue(channel.getCounters()[BaseChannel.CONNECT] == 1);
		assertTrue(channel.getCounters()[BaseChannel.TX] == 0);
		assertTrue(channel.getCounters()[BaseChannel.RX] == 1);
		
		verify(mockInputStream, times(2)).read(any(byte[].class), anyInt(), anyInt());

		mux.terminate();
		
		if (!WaitFor.waitForTrue(mux, "isTerminated", 2000))
			assertTrue("terminate took too long", false);
		
		verify(mockInputStream).close();
		
		verifyNoMoreInteractions(mockInputStream);
		
	}
	
	@Test
	public void testWriteHeartbeat() throws Exception
	{
		final Socket mockSocket = mock(Socket.class);
		final InputStream mockInputStream = mock(InputStream.class);
		final OutputStream mockOutputStream = mock(OutputStream.class);
		
		when(mockSocket.getInputStream()).thenReturn(mockInputStream);
		when(mockSocket.getOutputStream()).thenReturn(mockOutputStream);
		
		final Object lockObject = new Object();

		when(mockInputStream.read(any(byte[].class), anyInt(), anyInt())).then(new Answer<Integer>() {
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				synchronized(lockObject) {
					lockObject.wait();
				}
				return -1;
			}
		});
		
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) throws Throwable {
				synchronized(lockObject) {
					lockObject.notifyAll();
				}
				return null;
			}
		}).when(mockInputStream).close();
		
		TandemTCPChannel channel = new TandemTCPChannel("127.0.0.1", 12000, new TandemPackager()) {
			@Override
			protected Socket newSocket() throws IOException {
				connectTime = System.currentTimeMillis();
				lastReceive = -1;
				return mockSocket;
			}
		};
		
		TandemMUX mux = new TandemMUX(channel, threadPool, true);
		mux.setHeartbeatInterval(180 * 1000);
		mux.start();
		
		if (!WaitFor.waitForTrue(mux, "isConnected", 2000))
			assertTrue("connect took too long", false);
		
		Thread.sleep(2000);

		mux.terminate();
		
		if (!WaitFor.waitForTrue(mux, "isTerminated", 2000))
			assertTrue("terminate took too long", false);
		
		ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
		verify(mockOutputStream).write(captor.capture(), eq(0), eq(6));
		
		byte[] captured = captor.getValue();
		for(int i=0; i<6; i++) {
			assertTrue(captured[i] == 0x00); // this isn't a great test because it's hard to tell the difference between a heartbeat and an empty buffer since they are both filled with 0x00
		}
		
		verify(mockOutputStream, atLeastOnce()).flush();
		verify(mockOutputStream, atLeastOnce()).close();
		
		verifyNoMoreInteractions(mockOutputStream);
	}
	
	public static class HeartbeatSender
	{
		private long heartbeatInterval;
		private long lastHeartbeat = 0;
		private AtomicReference<Thread> currentThread = new AtomicReference<Thread>(); // this could be improved to handle concurrent reads
		
		public HeartbeatSender(long heartbeatInterval) {
			this.heartbeatInterval = heartbeatInterval;
		}
		
		public int read(byte[] bytes, int off, int len) {
			try {
				currentThread.set(Thread.currentThread());
				long et = System.currentTimeMillis() - lastHeartbeat;
				if (et < heartbeatInterval) {
					long nextHeartbeat = lastHeartbeat + heartbeatInterval;
					try {
						Thread.sleep(nextHeartbeat - System.currentTimeMillis());
					} catch (InterruptedException e) {
						return -1;
					}
				}
				
				lastHeartbeat = System.currentTimeMillis();
				int ret = len > HEARTBEAT_BYTES.length ? HEARTBEAT_BYTES.length : len;
				System.arraycopy(HEARTBEAT_BYTES, 0, bytes, off, ret);
				return ret;
			} finally {
				currentThread.set(null);
			}
		}
		
		public void close()
		{
			Thread t = currentThread.get();
			if (t != null)
				t.interrupt();
		}
	}

}
