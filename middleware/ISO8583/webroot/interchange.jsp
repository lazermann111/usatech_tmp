<%@ include file="includes.jsp" %> 
<%@ include file="header.jsp" %> 

<% if(request.getParameter("interchangeName") == null) { %>
<span class="error">Required Parameter Not Found: name</span>
<%@ include file="footer.jsp" %> 
<% return; } %> 

<% ISO8583Interchange interchange = gateway.getInterchanges().get(request.getParameter("interchangeName")); %>
<% if(interchange == null) { %>
<span class="error">Interchange Not Found!</span>
<%@ include file="footer.jsp" %> 
<% return; } %> 

<% if(action.equalsIgnoreCase("stop")) { 
     interchange.stop();
     try { Thread.sleep(2000); } catch(Exception e) {}
     response.sendRedirect(basePath+"interchange.jsp?interchangeName="+request.getParameter("interchangeName"));
     return;
   } else if(action.equalsIgnoreCase("start")) {
     interchange.start();
     try { Thread.sleep(2000); } catch(Exception e) {}
     response.sendRedirect(basePath+"interchange.jsp?interchangeName="+request.getParameter("interchangeName"));
     return;
   } 
%>

<center><span class="big_title">ISO8583 Interchange Admin</span></center>
<hr>
<table class="invisible">
 <tr><td>Name: </td><td><%=interchange.getName()%></td></tr>
<% if(interchange.isStarted()) { %>
 <tr><td>Started: </td><td bgcolor="green"><font color="white"><b><%=interchange.isStarted()%></b></font></td></tr>
 <tr><td>Control: </td><td bgcolor="red"><a href="interchange.jsp?interchangeName=<%=interchange.getName()%>&action=stop"><font color="white"><b>Stop</b></font></a></td></tr>
<% } else { %>
 <tr><td>Started: </td><td bgcolor="red"><font color="white"><b><%=interchange.isStarted()%></b></font></td></tr>
 <tr><td>Control: </td><td bgcolor="greeN"><a href="interchange.jsp?interchangeName=<%=interchange.getName()%>&action=start"><font color="white"><b>Start</b></font></a></td></tr>
<% } %>
 <tr><td nowrap>Implementation Class: </td><td><%=interchange.getClass().getName()%></td></tr>
 <tr><td>Connected: </td><td><%=interchange.isConnected()%></td></tr>
 <tr><td>Simulation Mode: </td><td><%=interchange.isSimulationMode()%></td></tr>
 <tr>
  <td valign="top">
   Configuration: <% Configuration interchangeConfig = interchange.getConfiguration(); %>
  </td>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td colspan="2">
   <table cellspacing="0" cellpadding="0" width="100%" class="invisible">
  <% Iterator configIter = interchangeConfig.getKeys();
     while(configIter.hasNext()) {
       String key = (String) configIter.next();
       String value = interchangeConfig.getString(key); %>
       <tr>
        <td>&nbsp;&nbsp;</td>
        <td nowrap><%=key%>: &nbsp;</td>
        <td><%=value%></td>
       </tr>
  <% } %>
   </table>
  </td>
 </tr>
<% if(interchange instanceof FHMSInterchange) { 
     FHMSInterchange fhmsInterchange = (FHMSInterchange) interchange;
     USATISOMUX mux = fhmsInterchange.getMUX();
     ISO8583TransactionManager transactionManager = fhmsInterchange.getTransactionManager();
     Map<String, USATISOMUX> exclusiveConnections = fhmsInterchange.getExclusiveConnections();
     Queue<USATISOMUX> exclusiveConnectionPool = fhmsInterchange.getExclusiveConnectionPool();
     ThreadPoolExecutor threadPool = fhmsInterchange.getThreadPool();
     USATISOChannel muxChannel = mux.getISOChannel();
%>
 <tr>
  <td valign="top">
   Thread Pool: 
  </td>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td colspan="2">
   <table cellspacing="0" cellpadding="0" width="100%" class="invisible">
    <tr><td>&nbsp;&nbsp;</td><td>Active: </td><td width="100%"><%=threadPool.getActiveCount()%></td></tr>
    <tr><td>&nbsp;</td><td nowrap>Completed Tasks: &nbsp;</td><td><%=threadPool.getCompletedTaskCount()%></td></tr>
    <tr><td>&nbsp;</td><td>Current Size: </td><td><%=threadPool.getPoolSize()%></td></tr>
    <tr><td>&nbsp;</td><td>Core Size: </td><td><%=threadPool.getCorePoolSize()%></td></tr>
    <tr><td>&nbsp;</td><td>Maximum Size: </td><td><%=threadPool.getMaximumPoolSize()%></td></tr>
    <tr><td>&nbsp;</td><td>Largest Size: </td><td><%=threadPool.getLargestPoolSize()%></td></tr>
    <tr><td>&nbsp;</td><td>Shutdown: </td><td><%=threadPool.isShutdown()%></td></tr>
    <tr><td>&nbsp;</td><td>Terminated: </td><td><%=threadPool.isTerminated()%></td></tr>
   </table>
  </td>
 </tr>
 <tr>
  <td>Persistent MUX: </td>
  <td><%=mux%></td>
 </tr>
 <tr>
  <td>In-Use Transient MUXs: </td>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td colspan="2">
   <table cellspacing="0" cellpadding="0" width="100%" class="invisible">
<% for(String key : exclusiveConnections.keySet()) { 
     USATISOMUX myMUX = exclusiveConnections.get(key); %>
    <tr><td>&nbsp;</td><td nowrap><%=key%>:&nbsp; </td><td><%=myMUX%></td></tr>
<% } %>
   </table>
  </td>
 </tr>
 <tr>
  <td>Free Transient MUXs: </td>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td colspan="2">
   <table cellspacing="0" cellpadding="0" width="100%" class="invisible">
<% for(USATISOMUX myMUX : exclusiveConnectionPool) {  %>
    <tr><td>&nbsp;</td><td><%=myMUX%></td></tr>
<% } %>
   </table>
  </td>
 </tr>
<% } %>

</table>

<%@ include file="footer.jsp" %> 
