<%@ include file="includes.jsp" %> 
<%@ include file="header.jsp" %> 

<% ThreadPoolExecutor threadPool = gateway.getThreadPool(); 
   Map<String, ISO8583Interchange> interchanges = gateway.getInterchanges(); %>

<center><span class="big_title">ISO8583 Gateway Admin</span></center>
<hr>
<table class="invisible">
 <tr><td>Implementation Class: </td><td><%=gateway.getClass().getName()%></td></tr>
 <tr><td>Reported Version: </td><td><%=gateway.getVersion()%></td></tr>
 <tr>
  <td valign="top">Thread Pool Status: </td>
  <td>
   <li>Active Count: <%=threadPool.getActiveCount()%></li>
   <li>Completed Tasks: <%=threadPool.getCompletedTaskCount()%></li>
   <li>Core Size: <%=threadPool.getCorePoolSize()%></li>
   <li>Largest Size: <%=threadPool.getLargestPoolSize()%></li>
   <li>Max Size: <%=threadPool.getMaximumPoolSize()%></li>
   <li>Current Size: <%=threadPool.getPoolSize()%></li>
   <li>Task Count: <%=threadPool.getTaskCount()%></li>
   <li>KeepAlive Seconds: <%=threadPool.getKeepAliveTime(TimeUnit.SECONDS)%></li>
  </td>
 </tr>
 <tr>
  <td>Registered Interchanges: </td>
  <td>
  <% for(Map.Entry<String, ISO8583Interchange> entry : interchanges.entrySet()) { %>
   <li><a href="interchange.jsp?interchangeName=<%=entry.getKey()%>"><%=entry.getKey()%></a></li>
  <% } %>
  </td>
 </tr>
</table>

<%@ include file="footer.jsp" %> 
