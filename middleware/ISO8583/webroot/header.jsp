<!-- BEGIN HEADER -->
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	String title = "USA Technologies ISO8583 Gateway Admin";
	ISO8583Gateway gateway = (ISO8583Gateway) config.getServletContext().getAttribute("ISO8583Gateway");
	String action = request.getParameter("action");
	if(action == null)
	  action = "";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
 <base href="<%=basePath%>">
 <title>ISO8583 Gateway Admin</title>
 <meta http-equiv="pragma" content="no-cache">
 <meta http-equiv="cache-control" content="no-cache">
 <meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="include/style.css">
</head>

<BODY>

<CENTER>
<BR>
<TABLE CLASS="container">
 <TR>
  <TD valign="top">
   <%@ include file="leftmenu.jsp" %> 
  </TD>
  <TD valign="top">
   <TABLE CLASS="maincontent">
    <TR>
     <TD valign="top">

<% if(gateway == null) { %>
<span class="error">Gateway Not Found!</span>
<%@ include file="footer.jsp" %> 
<% return; } %> 
  
<!-- END HEADER -->
