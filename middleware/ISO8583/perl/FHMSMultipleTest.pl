#!/usr/local/bin/perl

use FHMSISO8583;
use strict;

my $acquirer_id 	= '000000192000903';
my $terminal_id 	= '00010455';

print "\n\n";

my ($method_name, $reps, $trace_number, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10, $arg11, $arg12, $arg13, $arg14, $arg15) = @ARGV;

my $loop_index;

for($loop_index = 0; $loop_index < $reps; $loop_index++)
{
	my @return = &{eval "\\\&FHMSISO8583\::$method_name"}($acquirer_id, $terminal_id, ($trace_number+$loop_index), $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10, $arg11, $arg12, $arg13, $arg14, $arg15);
	print join(", ", @return);
	print "\n\n";
	sleep(1);
}
