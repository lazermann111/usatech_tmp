#!/usr/local/bin/perl -w
use SOAP::Lite +trace;
use strict;

my %params = (
	_gateway_addr => "localhost",
	_gateway_port => "8080"
);

#_iso8583_get_version(\%params);
_iso8583_authorize(\%params);

 
########################################################################
# SOAP methods
########################################################################
sub _iso8583_authorize
{
	#my $self = shift;
	my ($d_href) = @_;	#assumes data keys are identical to data type names; datetimes are gm epoch sec

	# setup the server connection
	my $iso8583_request = SOAP::Lite
		#-> soapversion(1.2)
		-> proxy("http://$d_href->{_gateway_addr}:$d_href->{_gateway_port}/axis/services/iso8583gateway")
		-> uri('urn:webservice.iso8583.usatech.com')
		#-> encprefix('')
		#-> envprefix('soap')
		#-> on_fault( sub { return undef; } )
        -> xmlschema(2001)
        -> readable ('true')
        ;

	$iso8583_request->transport->timeout(60);

	my $response = $iso8583_request->process
	(
		SOAP::Data->name('request')
		->type('namesp2:CardPresentSaleRequest')
		->uri('http://sale.model.iso8583.usatech.com')
		->value
		(
			\SOAP::Data->value
			(
				SOAP::Data->name('acquirerID')->type('string')->value('000000192000903'),
    			SOAP::Data->name('terminalID')->type('string')->value('00010455'),
    			SOAP::Data->name('traceNumber')->type('int')->value(123456),
    			SOAP::Data->name('amount')->type('int')->value(100),
   				SOAP::Data->name('PANData')
				->type('namesp3:TrackData')
				->uri('http://model.iso8583.usatech.com')
   				->value
            	(
					\SOAP::Data->value
	            	(
	            		SOAP::Data->name('PAN')->type('string')->value('5471140000000003'),
	            		SOAP::Data->name('expiration')->type('string')->value('0512'),
        	    		SOAP::Data->name('entryMode')->type('string')->value('010')
            		)
            	)
    		)
		),
		SOAP::Data->name('interchangeName')->type('string')->value('FHMS_TPS8583')
    );

	if(defined $response)
	{
		if($response->fault)
		{
           	my $fault_code    = $response->faultcode;
            my $fault_string  = $response->faultstring;
            my $resp_msg = "Authorization Failed!  SOAP Fault detected.";
            print "$resp_msg\n";
			print "faultcode  : $fault_code\n";
			print "faultstring: $fault_string\n";

            my $errstr;
			if (ref($response->faultdetail) eq 'HASH')
			 {
			     my %faultdetail = %{$response->faultdetail};
			     $errstr .= "exceptionName=" . $faultdetail{exceptionName} . "\n";
			     my %fault = %{$faultdetail{fault}};
			     foreach my $key (keys %fault)
			     {
			             $errstr .= $key . "=" . $fault{$key} . "\n";
			     }
			 }
			 else
			 {
			     $errstr .= "faultdetail=" . $response->faultdetail;
			 }
			 print $errstr;
		}
		else
		{
            my $result = $response->result;
	    	my %responseDetails = %{$result};
	    	
	    	print "Response trace number: " . $responseDetails{traceNumber} . "\n";
	    	print "Response amount: " . $responseDetails{amount} . "\n";
	    	
	    	#foreach my $key (keys %responseDetails)
	    	#{
	    	#	print $key . "=" . $responseDetails{$key} . "\n";
	    	#}
		}
	}
	else
	{
    	print "Authorization Failed!  Could not connect to the iso8583 Gateway!\n";
	}
}

sub _iso8583_get_version
{
	#my $self = shift;
	my ($d_href) = @_;	#assumes data keys are identical to data type names; datetimes are gm epoch sec

	# setup the server connection
	my $iso8583_request = SOAP::Lite
		#-> soapversion(1.2)
		-> proxy("http://$d_href->{_gateway_addr}:$d_href->{_gateway_port}/axis/services/iso8583gateway")
		-> uri('urn:webservice.iso8583.usatech.com')
		#-> encprefix('')
		#-> envprefix('soap')
		#-> on_fault( sub { return undef; } )
        -> xmlschema(2001)
        -> readable ('true')
        ;

	$iso8583_request->transport->timeout(60);

	my $response = $iso8583_request->getVersion();

	if(defined $response)
	{
		if($response->fault)
		{
           	my $fault_code    = $response->faultcode;
            my $fault_string  = $response->faultstring;
            my $resp_msg = "getVersion Failed!  SOAP Fault detected.";
            print "$resp_msg\n";
			print "faultcode  : $fault_code\n";
			print "faultstring: $fault_string\n";

            my $errstr;
			if (ref($response->faultdetail) eq 'HASH')
			 {
			     my %faultdetail = %{$response->faultdetail};
			     $errstr .= "exceptionName=" . $faultdetail{exceptionName} . "\n";
			     my %fault = %{$faultdetail{fault}};
			     foreach my $key (keys %fault)
			     {
			             $errstr .= $key . "=" . $fault{$key} . "\n";
			     }
			 }
			 else
			 {
			     $errstr .= "faultdetail=" . $response->faultdetail;
			 }
			 print $errstr;
		}
		else
		{
            print "ISO-8583 Gateway Version: " . $response->result . "\n";
		}
	}
	else
	{
    	print "getVersion Failed!  Could not connect to the iso8583 Gateway!\n";
	}
}