#!/usr/local/bin/perl

use FHMSISO8583;
use strict;

my $acquirer_id 	= '000000192000903';
my $terminal_id 	= '00010455';

#my $acquirer_id 	= '000000020030003';
#my $terminal_id 	= '00010850';

#my $acquirer_id 	= '000000020030024';
#my $terminal_id 	= '00010005';

#my $acquirer_id 	= '000000020030033';
#my $terminal_id 	= '00010005';

#my $acquirer_id 	= '000000020030027';
#my $terminal_id 	= '00010005';

#my $acquirer_id 	= '000000020030035';
#my $terminal_id 	= '00010005';

#my $acquirer_id 	= '000000020030029';
#my $terminal_id 	= '00010005';

print "\n\n";

my ($method_name, $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10, $arg11, $arg12, $arg13, $arg14, $arg15) = @ARGV;

print localtime() . " method_name = $method_name\n";

my @return = &{eval "\\\&FHMSISO8583\::$method_name"}($acquirer_id, $terminal_id, $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10, $arg11, $arg12, $arg13, $arg14, $arg15);

print "\n\n";
print join(", ", @return);

