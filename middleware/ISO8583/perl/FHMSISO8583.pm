package FHMSISO8583;

use SOAP::Lite +trace;
#use SOAP::Lite;
use Data::Dumper;
use strict;

# First Horizon ISO8583Gatway Interface Module (Prototype)
# pcowan - 10/11/2005
# 
# acquirer_id		: Merchant Number, Always Required, Assigned by FHMS, 15 digits
# terminal_id		: Terminal Number, Always Required, Assigned by FHMS, 8 digits
# trace_number		: Trace Audit Number, Always Reqired, Assigned by client for tracking; Echoed by FHMS but not used, 6 digits max
# amount			: Transaction Amount, Required where defined, Value in pennies (no decimal, all numeric), 12 digits max
# pos_environment	: Point of Sale Environment, Required where defined
#						A = Attended (Retail)
#						M = Mail/Telephone Order
# entry_mode		: Point of Sale Entry Mode, Required where defined
#						M = Manual Entry via Keyboard/Keypad
#						S = Swiped - Magnetic Stripe 
#						R = Contactless Swipe (RFID)
# tracks			: Magstripe Data from Swipe or RFID, Required where defined, Can be either Track1 or Track2 data
# pan				: Primary Account Number, Required where defined, Credit card number
# expiration		: Credit card expiration date, Required where defined, YYMM format
# cvv2				: Credit card CVV2 or CID value, not required, 4 digits max
# avs_zip			: Address verification Zip code, not required, 9 digits max
# avs_address		: Address verification Address, not required, 20 digits max, Numeric portion of address before first consonant of street address
# effective_date	: Timestamp of transaction retured from FHMS, required where defined
# rrn				: Retrieval Reference Number, required where deined, FHMS tracking number returned on all messages
# approval_code		: Transaction approval code, required where defined, value returned from FHMS on approved authorizations and sales when not provided
# batch_number		: Settlement batch number, required where defined, terminal assigned number 1-999
# sale_cnt			: Count of sales in the batch, required where defined
# sale_amt			: Total amount of sales in the batch, Value in pennies (no decimal, all numeric)
# refund_cnt		: Count of refunds in the batch, required where defined
# refund_amt		: Total amount of refunds in the batch, Value in pennies (no decimal, all numeric)
# sub_type			: Type of original transaction in an upload, Required where defined
#						"SALE" for a Sale
#						"REFUND" for a Refund
#

#my $gateway_url = 'http://127.0.0.1:9100/ISO8583/services/iso8583gateway';
#my $gateway_url = 'http://10.0.0.63:9101/ISO8583/services/iso8583gateway';
my $gateway_url = 'http://10.0.0.63:9100/ISO8583/services/iso8583gateway';
#my $gateway_url = 'http://127.0.0.1:8080/ISO8583/services/iso8583gateway';
#my $gateway_url = 'http://129.41.132.162:9100/ISO8583/services/iso8583gateway';
my $gateway_timeout = 230;
my $interchange_name = 'FHMS_TPS8583';

#my $ps2000Indicator 		= 'E';
my $ps2000Indicator 		= undef;
my $ps2000Identifier 		= '086130592212617';
my $ps2000ValidationCode 	= 'QDST';
my $ps2000ResponseCode 		= '  ';
my $ps2000EntryMode 		= '  ';

# Used to upload a transaction in a batch out-of-balance state
sub transaction_upload
{
	my ($acquirer_id, $terminal_id, $trace_number, $original_trace_number, $sub_type, $batch_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('UPLOAD');
	push @parameters, SOAP::Data->name('transactionSubType')->type('string')->value($sub_type);
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('originalTraceNumber')->type('int')->value($original_trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);

	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
	
	if(defined $ps2000Indicator)
	{
		push @parameters, SOAP::Data->name('PS2000')
							->type('namesp2:PS2000')
							->uri('http://iso8583.usatech.com')
	  						->value
	        	   			(
								\SOAP::Data->value
	            				(
	       	    					SOAP::Data->name('indicator')->type('string')->value($ps2000Indicator),
	       	    					SOAP::Data->name('identifier')->type('string')->value($ps2000Identifier),
	       	    					SOAP::Data->name('validationCode')->type('string')->value($ps2000ValidationCode),
	       	    					SOAP::Data->name('responseCode')->type('string')->value($ps2000ResponseCode),
	       	    					SOAP::Data->name('entryMode')->type('string')->value($ps2000EntryMode),
		           				)
	    	       			);
	}
    
	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);
	push @parameters, SOAP::Data->name('batchNumber')->type('int')->value($batch_number);

	return &send_request(\@parameters);
}

# Used to settle (close) a batch 
sub settlement
{
	my ($acquirer_id, $terminal_id, $trace_number, $batch_number, $sale_cnt, $sale_amt, $refund_cnt, $refund_amt) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('SETTLEMENT');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('batchNumber')->type('int')->value($batch_number);

	push @parameters, SOAP::Data->name('batchTotals')
						->type('namesp1:BatchTotals')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('creditSaleCount')->type('int')->value($sale_cnt),
       	    					SOAP::Data->name('creditSaleAmount')->type('int')->value($sale_amt),
       	    					SOAP::Data->name('creditRefundCount')->type('int')->value($refund_cnt),
       	    					SOAP::Data->name('creditRefundAmount')->type('int')->value($refund_amt),
	           				)
    	       			);
    
	return &send_request(\@parameters);
}

# Used to settle (close) a batch in an out-of-balance state after uploading all transactions in the batch
sub settlement_retry
{
	my ($acquirer_id, $terminal_id, $trace_number, $batch_number, $sale_cnt, $sale_amt, $refund_cnt, $refund_amt) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('SETTLEMENT_RETRY');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('batchNumber')->type('int')->value($batch_number);

	push @parameters, SOAP::Data->name('batchTotals')
						->type('namesp1:BatchTotals')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('creditSaleCount')->type('int')->value($sale_cnt),
       	    					SOAP::Data->name('creditSaleAmount')->type('int')->value($sale_amt),
       	    					SOAP::Data->name('creditRefundCount')->type('int')->value($refund_cnt),
       	    					SOAP::Data->name('creditRefundAmount')->type('int')->value($refund_amt),
	           				)
    	       			);
    
	return &send_request(\@parameters);
}

# Used to cancel an authorization that has not been sale'd
sub reverse_authorization
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('REVERSAL');
	push @parameters, SOAP::Data->name('transactionSubType')->type('string')->value('AUTHORIZATION');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);

	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    
	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);

	return &send_request(\@parameters);
}

# Used to cancel a sale in the current batch, (remove from batch totals)
sub reverse_sale
{
#	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('REVERSAL');
	push @parameters, SOAP::Data->name('transactionSubType')->type('string')->value('SALE');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
#	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);

	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    
#	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn) if(defined $rrn);
#	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);

	return &send_request(\@parameters);
}

# Used to cancel a sale in the current batch, (remove from batch totals)
sub void_sale
{
#	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('VOID');
	push @parameters, SOAP::Data->name('transactionSubType')->type('string')->value('SALE');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);

	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    
	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn) if(defined $rrn);
#	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);

	return &send_request(\@parameters);
}

# Used to cancel a refund in the current batch, (remove from batch totals)
sub reverse_refund
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('REVERSAL');
	push @parameters, SOAP::Data->name('transactionSubType')->type('string')->value('REFUND');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);

	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    
	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);

	return &send_request(\@parameters);
}

# NOT IMPLEMENT BY FHMS!
#sub adjust_authorization
#{
#	my ($acquirer_id, $terminal_id, $trace_number, $new_amount, $original_amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;
#	
#	my @parameters;
#	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('ADJUSTMENT');
#	push @parameters, SOAP::Data->name('transactionSubType')->type('string')->value('AUTHORIZATION');
#	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
#	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
#	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
#	push @parameters, SOAP::Data->name('amount')->type('int')->value($new_amount);
#	push @parameters, SOAP::Data->name('originalAmount')->type('int')->value($original_amount);
#	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
#	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
#	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
#	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);
#
#	push @parameters, SOAP::Data->name('panData')
#						->type('namesp1:PanData')
#						->uri('http://iso8583.usatech.com')
# 						->value
#        	   			(
#							\SOAP::Data->value
#           				(
#								SOAP::Data->name('pan')->type('string')->value($pan),
#								SOAP::Data->name('expiration')->type('string')->value($expiration)
#							)
#						);
#
#	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn);
#	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);
#
#	return &send_request(\@parameters);
#}

# Used to adjust the amount of a sale in the current batch, (adjust batch totals)
sub adjust_sale
{
	my ($acquirer_id, $terminal_id, $trace_number, $new_amount, $original_amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('ADJUSTMENT');
	push @parameters, SOAP::Data->name('transactionSubType')->type('string')->value('SALE');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($new_amount);
	push @parameters, SOAP::Data->name('originalAmount')->type('int')->value($original_amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);

	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    
	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);

	return &send_request(\@parameters);
}

# Used to adjust the amount of a refund in the current batch, (adjust batch totals)
sub adjust_refund
{
	my ($acquirer_id, $terminal_id, $trace_number, $new_amount, $original_amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('ADJUSTMENT');
	push @parameters, SOAP::Data->name('transactionSubType')->type('string')->value('REFUND');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($new_amount);
	push @parameters, SOAP::Data->name('originalAmount')->type('int')->value($original_amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);

	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    
	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);

	return &send_request(\@parameters);
}

# Used to create a sale using track data where the final sale amount is know and there was no prior auth
sub sale_track
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $tracks, $cvv2, $avs_zip, $avs_address) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('SALE');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);
	
	push @parameters, SOAP::Data->name('trackData')
						->type('namesp1:TrackData')
						->uri('http://iso8583.usatech.com')
   						->value
            			(
							\SOAP::Data->value
	            			(
        	    				SOAP::Data->name('tracks')->type('string')->value($tracks)
            				)
            			);
            			
	push @parameters, SOAP::Data->name('cvv2')->type('string')->value($cvv2) if(defined $cvv2);
	push @parameters, SOAP::Data->name('avsZip')->type('string')->value($avs_zip) if(defined $avs_zip);
	push @parameters, SOAP::Data->name('avsAddress')->type('string')->value($avs_address) if(defined $avs_address);
	
	return &send_request(\@parameters);
}

# Used to create a sale where track data is not available and and the final sale amount is know and there was no prior auth
sub sale_pan
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $cvv2, $avs_zip, $avs_address) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('SALE');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);
	
	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    	       			
	push @parameters, SOAP::Data->name('cvv2')->type('string')->value($cvv2) if(defined $cvv2);
	push @parameters, SOAP::Data->name('avsZip')->type('string')->value($avs_zip) if(defined $avs_zip);
	push @parameters, SOAP::Data->name('avsAddress')->type('string')->value($avs_address) if(defined $avs_address);
    	       			
	return &send_request(\@parameters);
}

# Used to create a sale where track data is not available and and the final sale amount is know and an authrization code was obtained manually
sub sale_pan_force
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $approval_code) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('SALE');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(0);
	
	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    	       			
	if(defined $ps2000Indicator)
	{
		push @parameters, SOAP::Data->name('PS2000')
							->type('namesp2:PS2000')
							->uri('http://iso8583.usatech.com')
	  						->value
	        	   			(
								\SOAP::Data->value
	            				(
	       	    					SOAP::Data->name('indicator')->type('string')->value($ps2000Indicator),
	       	    					SOAP::Data->name('identifier')->type('string')->value($ps2000Identifier),
	       	    					SOAP::Data->name('validationCode')->type('string')->value($ps2000ValidationCode),
	       	    					SOAP::Data->name('responseCode')->type('string')->value($ps2000ResponseCode),
	       	    					SOAP::Data->name('entryMode')->type('string')->value($ps2000EntryMode),
		           				)
	    	       			);
	}
    	       			
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);

	return &send_request(\@parameters);
}


# Used to create a sale using track data and the final sale amount is know and an authrization code was obtained manually
sub sale_track_force
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $tracks, $effective_date, $approval_code) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('SALE');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(0);
	
	push @parameters, SOAP::Data->name('trackData')
						->type('namesp1:TrackData')
						->uri('http://iso8583.usatech.com')
   						->value
            			(
							\SOAP::Data->value
	            			(
        	    				SOAP::Data->name('tracks')->type('string')->value($tracks)
            				)
            			);
            			
	if(defined $ps2000Indicator)
	{
		push @parameters, SOAP::Data->name('PS2000')
							->type('namesp2:PS2000')
							->uri('http://iso8583.usatech.com')
	  						->value
	        	   			(
								\SOAP::Data->value
	            				(
	       	    					SOAP::Data->name('indicator')->type('string')->value($ps2000Indicator),
	       	    					SOAP::Data->name('identifier')->type('string')->value($ps2000Identifier),
	       	    					SOAP::Data->name('validationCode')->type('string')->value($ps2000ValidationCode),
	       	    					SOAP::Data->name('responseCode')->type('string')->value($ps2000ResponseCode),
	       	    					SOAP::Data->name('entryMode')->type('string')->value($ps2000EntryMode),
		           				)
	    	       			);
	}
    	       			
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);

	return &send_request(\@parameters);
}

# Used to complete a prior authorization when the final sale amount is known
sub sale_authorized
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $rrn, $approval_code) = @_;


	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('SALE');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
   	push @parameters, SOAP::Data->name('retrievalReferenceNumber')->type('string')->value($rrn);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(0);
	
	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);

	if(defined $ps2000Indicator)
	{
		push @parameters, SOAP::Data->name('PS2000')
							->type('namesp2:PS2000')
							->uri('http://iso8583.usatech.com')
	  						->value
	        	   			(
								\SOAP::Data->value
	            				(
	       	    					SOAP::Data->name('indicator')->type('string')->value($ps2000Indicator),
	       	    					SOAP::Data->name('identifier')->type('string')->value($ps2000Identifier),
	       	    					SOAP::Data->name('validationCode')->type('string')->value($ps2000ValidationCode),
	       	    					SOAP::Data->name('responseCode')->type('string')->value($ps2000ResponseCode),
	       	    					SOAP::Data->name('entryMode')->type('string')->value($ps2000EntryMode),
		           				)
	    	       			);
	}

	return &send_request(\@parameters);
}

# Used to create a refund using track data
sub refund_track
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $tracks) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('REFUND');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);
	
	push @parameters, SOAP::Data->name('trackData')
						->type('namesp1:TrackData')
						->uri('http://iso8583.usatech.com')
   						->value
            			(
							\SOAP::Data->value
	            			(
        	    				SOAP::Data->name('tracks')->type('string')->value($tracks)
            				)
            			);
	
	return &send_request(\@parameters);
}

# Used to create a refund where track data is not available
sub refund_pan
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('REFUND');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);
	
	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    	       			
	return &send_request(\@parameters);
}

# Used to create a refund where track data is not available and an authrization code was obtained manually
sub refund_force
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $effective_date, $approval_code) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('REFUND');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('effectiveDate')->type('dateTime')->value($effective_date);
	push @parameters, SOAP::Data->name('approvalCode')->type('string')->value($approval_code);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(0);
	
	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    	       			
	return &send_request(\@parameters);
}

# Used to obtain a pre authorization using track data
sub authorization_track
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $tracks, $cvv2, $avs_zip, $avs_address) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('AUTHORIZATION');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);
	
	push @parameters, SOAP::Data->name('trackData')
						->type('namesp1:TrackData')
						->uri('http://iso8583.usatech.com')
   						->value
            			(
							\SOAP::Data->value
	            			(
        	    				SOAP::Data->name('tracks')->type('string')->value($tracks)
            				)
            			);
        
	push @parameters, SOAP::Data->name('cvv2')->type('string')->value($cvv2) if(defined $cvv2);
	push @parameters, SOAP::Data->name('avsZip')->type('string')->value($avs_zip) if(defined $avs_zip);
	push @parameters, SOAP::Data->name('avsAddress')->type('string')->value($avs_address) if(defined $avs_address);
	
	push @parameters, SOAP::Data->name('hostResponseTimeout')->type('int')->value(10);
	
	return &send_request(\@parameters);
}

# Used to obtain a pre authorization where track data is not available
sub authorization_pan
{
	my ($acquirer_id, $terminal_id, $trace_number, $amount, $pos_environment, $entry_mode, $pan, $expiration, $cvv2, $avs_zip, $avs_address) = @_;

	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('AUTHORIZATION');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('amount')->type('int')->value($amount);
	push @parameters, SOAP::Data->name('posEnvironment')->type('string')->value($pos_environment);
	push @parameters, SOAP::Data->name('entryMode')->type('string')->value($entry_mode);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(1);
	
	push @parameters, SOAP::Data->name('panData')
						->type('namesp1:PanData')
						->uri('http://iso8583.usatech.com')
  						->value
        	   			(
							\SOAP::Data->value
            				(
       	    					SOAP::Data->name('pan')->type('string')->value($pan),
       	    					SOAP::Data->name('expiration')->type('string')->value($expiration)
	           				)
    	       			);
    	       			
	push @parameters, SOAP::Data->name('cvv2')->type('string')->value($cvv2) if(defined $cvv2);
	push @parameters, SOAP::Data->name('avsZip')->type('string')->value($avs_zip) if(defined $avs_zip);
	push @parameters, SOAP::Data->name('avsAddress')->type('string')->value($avs_address) if(defined $avs_address);

	return &send_request(\@parameters);
}

# A simple test of FHMS, used to verify connectivity
sub test_transaction
{
	my ($acquirer_id, $terminal_id, $trace_number) = @_;
	
	my @parameters;
	push @parameters, SOAP::Data->name('transactionType')->type('string')->value('TEST');
	push @parameters, SOAP::Data->name('acquirerID')->type('string')->value($acquirer_id);
	push @parameters, SOAP::Data->name('terminalID')->type('string')->value($terminal_id);
	push @parameters, SOAP::Data->name('traceNumber')->type('int')->value($trace_number);
	push @parameters, SOAP::Data->name('online')->type('boolean')->value(0);
	
	return &send_request(\@parameters);
}

sub send_request
{
	my ($parameters) = @_;
	
	# setup the server connection
	my $request = SOAP::Lite
		-> proxy($gateway_url)
		-> uri('urn:iso8583.usatech.com')
        -> xmlschema(2001)
        -> readable ('true');
        
	$request->transport->timeout($gateway_timeout);
	
	#print "PARAMETERS: " . Dumper($parameters) . "\n";
	
	my $response = $request->process
	(
		SOAP::Data->name('request')
		->type('namesp1:ISO8583Request')
		->uri('http://iso8583.usatech.com')
		->value(\SOAP::Data->value(@$parameters)),
		SOAP::Data->name('interchangeName')->type('string')->value($interchange_name)
    );
    
	if(defined $response)
	{
		if($response->fault)
		{
           	my $fault_code    = $response->faultcode;
            my $fault_string  = $response->faultstring;
            my $resp_msg = "Request Failed!  SOAP Fault detected. ";

			if($response->faultdetail)
			{
				$resp_msg .= "faultdetail=" . $response->faultdetail;
			}
			
			return (0, $fault_code, $fault_string, $resp_msg);
		}
		else
		{
	    	my $result = $response->result;
	    	return (1, Dumper($result))
		}
	}
	else
	{
		my $str = "Request Failed!  Could not connect to the ISO8583 Gateway!";
		return (0, 0, $str, $str);
	}
}

1;
