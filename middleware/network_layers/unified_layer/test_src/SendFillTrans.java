import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.usatech.networklayer.NetworkLayerException;
import com.usatech.networklayer.ReRixMessage;
import com.usatech.networklayer.TestClient;
import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.util.Conversions;
import com.usatech.util.Util;


public class SendFillTrans {
	protected String host = "usaapd1";
	protected int port = 443;
	protected String evNumber = "EV035179";
	protected byte[] encryptKey = "1095270400165625".getBytes();
	protected int msg = 1;
	protected int protocolVersion = 3;
	protected TransmissionProtocol protocol;
	protected Socket socket;
	protected DataInputStream input;
	protected DataOutputStream output;
	protected boolean useTestClient = false;

	protected static char[] counterPadding = "00000000".toCharArray();

	public SendFillTrans(String host, int port, String evNumber, byte[] encryptKey) {
		super();
		this.host = host;
		this.port = port;
		this.evNumber = evNumber;
		this.encryptKey = encryptKey;
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		sendFills();
	}
	public static void sendFills() throws Exception {
		//*
		String host = "10.0.0.63";
		int port = 443;
		String evNumber = "EV035179";
		String serialNumber = "G5060278";
		byte[] encryptKey = "1095270400165625".getBytes();
		String card = "4300123412341017=12100001";
		/*/
		String host = "eccnet01.usatech.com"; //, 192.168.4.61";
		int port = 443;
		String evNumber = "EV083445";
		String serialNumber = "G5087253";
		String encryptKey = "1505132544113750";
		String card = "4300123412341017=12100001";
		*/
		SendFillTrans sft = new SendFillTrans(host, port, evNumber, encryptKey);
		//sft.useTestClient = true;

		int fills = 1;
		float minFillDays = 1.0f;
		float maxFillDays = 4.0f;
		int minTransPerFill = 3;
		int maxTransPerFill = 10;
		Date startDate = new Date(2008-1900, 6, 27);
		int cashCount = 9;
		int cashTotal = 350;
		int creditCount = 20;
		int creditTotal = 1450;
		int otherCount = 0;
		int otherTotal = 0;
		int byteCount = 17365;

		int minAddMS = (int)(minFillDays * 24 * 60 * 60 * 1000 / (maxTransPerFill + 1));
		int maxAddMS = (int)(maxFillDays * 24 * 60 * 60 * 1000 / (minTransPerFill + 1));
		sft.startCommunication();
		for(int i = 0; i < fills; i++) {
			String message;
			/*cashCount = 0;
			cashTotal = 0;
			creditCount = 0;
			creditTotal = 0;
			otherCount = 0;
			otherTotal = 0;
			byteCount = 0;
			*/
			Date targetDate = new Date(startDate.getTime() + getRandom((int)(minFillDays * 24 * 60 * 60 * 1000), (int)(maxFillDays * 24 * 60 * 60 * 1000)));
			while(startDate.before(targetDate)) {
				startDate.setTime(startDate.getTime() + getRandom(minAddMS, maxAddMS));
				double d = Math.random();
				int cents = 100 + (25 * (int)(10 * Math.random()));
				if(d < 0.35) { //credit
					int[] items = new int[getRandom(1, 4)];
					for(int k = 0; k < items.length; k++) {
						items[k] = (int)(256 * Math.random());
					}
					cents = cents * items.length;
					message = constructLocalBatchMessage(startDate, 'C', cents, card, items);
					creditCount += items.length;
					creditTotal += cents;
				} else if(d < 0.95) {
					int item = (int)(256 * Math.random());
					message = constructCashMessage(startDate, cents, item);
					cashCount++;
					cashTotal += cents;
				} else {
					int[] items = new int[getRandom(1, 4)];
					for(int k = 0; k < items.length; k++) {
						items[k] = (int)(256 * Math.random());
					}
					cents = cents * items.length;
					message = constructLocalBatchMessage(startDate, 'S', cents, card, items);
					otherCount += items.length;
					otherTotal += cents;
				}
				sft.sendMessage(message);
			}
			startDate.setTime(startDate.getTime() + getRandom(minAddMS, maxAddMS));
			if(Math.random() > 0.70) {
				//This does not work b/c each call to sendMessage is in new session
				message = constructFillV1Message(startDate, serialNumber);
				sft.sendMessage(message);
				message = constructCountersMessage(cashCount, cashTotal, creditCount, creditTotal, otherCount, otherTotal, byteCount);
				sft.sendMessage(message);
			} else {
				message = constructFillV2Message(startDate, cashCount, cashTotal, creditCount, creditTotal, otherCount, otherTotal, byteCount, card);
				sft.sendMessage(message);
			}
		}
		sft.stopCommunication();
	}
	protected static String constructLocalBatchMessage(Date startDate, char cardType, int cents, String card, int[] items) {
		StringBuilder sb = new StringBuilder();
		sb.append("A3");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		appendHex(sb, cardType, 2);
		appendHex(sb, cents, 8);
		appendHex(sb, card.length(), 2);
		sb.append(toHex(card.getBytes()));
		sb.append("4E");
		appendHex(sb, (items.length >> 2) + 1 /* byte length of items*/, 2);
		for(int i = 0; i < items.length; i++) {
			appendHex(sb, items[i], 2);
			appendHex(sb, cents/items.length /*unit price*/, 6);
		}
		return sb.toString();
	}

	protected static String constructCashMessage(Date startDate, int cents, int item) {
		StringBuilder sb = new StringBuilder();
		sb.append("96");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		appendHex(sb, cents, 6);
		appendHex(sb, item, 2);
		return sb.toString();
	}
	protected static int getRandom(int min, int max) {
		return min + (int)(Math.random() * (max-min));
	}

	protected static String constructFillV2Message(Date startDate, int cashCount, int cashTotal,
			int creditCount, int creditTotal, int otherCount, int otherTotal, int byteCount,
			String card) {
		StringBuilder sb = new StringBuilder();
		sb.append("A8");
		padCounter(sb, cashCount);
		padCounter(sb, cashTotal);
		padCounter(sb, creditCount);
		padCounter(sb, creditTotal);
		padCounter(sb, otherCount);
		padCounter(sb, otherTotal);
		padCounter(sb, byteCount);
		padCounter(sb, 1/*attempted sessions*/);
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		sb.append("46");
		appendHex(sb, card.length(), 2);
		sb.append(toHex(card.getBytes()));
		return  sb.toString();
	}

	protected static SimpleDateFormat bcdDateFormat = new SimpleDateFormat("HHmmssMMddyyyy");
	protected static String constructFillV1Message(Date startDate, String serialNumber) {
		StringBuilder sb = new StringBuilder();
		sb.append("93");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		sb.append(bcdDateFormat.format(startDate));
		sb.append("49"); //"I"
		appendHex(sb, 0, 6);
		appendHex(sb, 0, 4);
		appendHex(sb, 14 + serialNumber.length(), 2);
		sb.append(toHex("Err.DEXFill-->".getBytes()));
		sb.append(toHex(serialNumber.getBytes()));
		sb.append("2000");
		return sb.toString();
	}
	protected static String constructCountersMessage(int cashCount, int cashTotal,
			int creditCount, int creditTotal, int otherCount, int otherTotal, int byteCount) {
		StringBuilder sb = new StringBuilder();
		sb.append("86");
		padCounter(sb, cashCount);
		padCounter(sb, cashTotal);
		padCounter(sb, creditCount);
		padCounter(sb, creditTotal);
		padCounter(sb, otherCount);
		padCounter(sb, otherTotal);
		padCounter(sb, byteCount);
		padCounter(sb, 1/*attempted sessions*/);
		sb.append("00000000");
		return sb.toString();
	}
	protected static final char[] HEX_DIGITS = {
    	'0' , '1' , '2' , '3' , '4' , '5' ,
    	'6' , '7' , '8' , '9' , 'A' , 'B' ,
    	'C' , 'D' , 'E' , 'F'};

	public static StringBuilder appendHex(StringBuilder sb, int num, int length) {
		return padLeft(sb, Integer.toHexString(num), length);
	}
	public static StringBuilder appendHex(StringBuilder sb, long num, int length) {
		return padLeft(sb, Long.toHexString(num), length);
	}
	public static String toHex(byte[] bytes) {
    	if(bytes == null) return null;
    	char[] result = new char[bytes.length*2];
    	for(int i = 0; i < bytes.length; i++) {
    		result[i*2] = HEX_DIGITS[(bytes[i] >> 4) & 0x0f];
    		result[i*2+1] = HEX_DIGITS[bytes[i] & 0x0f];
    	}
    	return new String(result);
    }
	protected static StringBuilder padCounter(StringBuilder sb, int counter) {
		return padLeft(sb, String.valueOf(counter), 8);
	}
	protected static StringBuilder padLeft(StringBuilder sb, String s, int length) {
		if(s.length() > length)sb.append(s.substring(s.length() - length));
		else {
			if(s.length() < length) sb.append(counterPadding, 0, length - s.length());
			sb.append(s);
		}
		return sb;
	}
	protected void sendMessage(String message) throws Exception {
		if(useTestClient) {
		TestClient.main(new String[] {
				host, String.valueOf(port), "3", evNumber, Conversions.bytesToHex(encryptKey), appendHex(new StringBuilder(), msg++, 2).append(message).toString()
		});
		} else {
			String evNumber;
			if (protocolVersion == 2)
				evNumber = new String(Conversions.hexToByteArray(this.evNumber));
	        else
	        	evNumber = this.evNumber;

		byte[] data = Conversions.hexToByteArray(appendHex(new StringBuilder(), msg++, 2).append(message).toString());
		ReRixMessage msg = new ReRixMessage(data, evNumber);
		Util.output("<- " + msg.getHexData());
		protocol.transmitToServer(msg, output, encryptKey);

		output.flush();

        switch(protocolVersion) {
			case 2:
			case 4:
				break;
			default:
				int version = input.read();
		}

		while(true) {
			try {
				ReRixMessage inMsg = protocol.receiveFromServer(input, evNumber, encryptKey);
				if(inMsg == null)
					break;
				Util.output("-> " + inMsg.getHexData());
			} catch(Exception e) {
				break;
			}
		}
		}
	}

	protected void startCommunication() throws ClassNotFoundException, NetworkLayerException, UnknownHostException, IOException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		if(useTestClient) return;
		int readTimeout = 30000;
		int writeTimeout = 30000;

		Class<?>[] parameterTypes = { Integer.TYPE, Integer.TYPE };
		Object[] constructorArgs = { new Integer(readTimeout), new Integer(writeTimeout) };
		String className = "com.usatech.networklayer.net.TransmissionProtocolV" + protocolVersion;
		try	{
			Class<?> protocolClass = Class.forName(className);
			Constructor<?> constructor = protocolClass.getConstructor(parameterTypes);
			protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
		} catch(InstantiationException e) {
			throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
		}
		socket = new Socket(host, port);
		socket.setSoTimeout(10000);

		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
	}

	protected void stopCommunication() throws IOException {
		if(useTestClient) return;
		input.close();
		output.close();
		socket.close();
	}
}
