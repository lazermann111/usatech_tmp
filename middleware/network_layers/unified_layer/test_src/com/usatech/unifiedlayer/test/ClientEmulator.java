package com.usatech.unifiedlayer.test;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.networklayer.NetworkLayerException;
import com.usatech.networklayer.ReRixMessage;
import com.usatech.networklayer.TestClient;
import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.util.Conversions;


public class ClientEmulator {
	private static Log log = LogFactory.getLog(ClientEmulator.class);
	protected String host = "usaapd1";
	protected int port = 443;
	protected String evNumber = "EV035179";
	protected byte[] encryptKey = "1095270400165625".getBytes();
	protected int msg = 1;
	protected int protocolVersion = 3;
	protected TransmissionProtocol protocol;
	protected Socket socket;
	protected DataInputStream input;
	protected DataOutputStream output;
	protected boolean useTestClient = false;

	protected static char[] counterPadding = "00000000".toCharArray();

	public ClientEmulator(String host, int port, String evNumber, byte[] encryptKey) {
		super();
		this.host = host;
		this.port = port;
		this.evNumber = evNumber;
		this.encryptKey = encryptKey;
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		testPing();
		//testLocalBatch();
		//testAuthInternal((byte)4);
		testAuthFHMS((byte)4);
		testAuth((byte)4, 'M', "CURRENCY");
		//testAuthAquaFill();
		//testAuthFHMSDev();
		//testGenReqUSR();
		//testAuthInternal();
		//testPingDev();
		//testGenResponseDev();
		//testAuthInternalDev();
		//testPing();
		//sendFills(5, 3);
		//testLocalBatch();
		//testAuth();
		//testAuthPaymentech();
		//testAuthBlackboard();
		//testAuthAramark();
		//testAuthAquaFill();
	}
	public static void testPing() throws Exception {
		String host = "127.0.0.1";
		int port = 443;
		String evNumber = "EV035194";
		String serialNumber = "G5060281";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(0);
		for(int i = 0; i < 5; i++) {
			ce.sendMessage("", 1);
		}
		ce.stopCommunication();
	}
	public static void testPingDev() throws Exception {
		String host = "usaapd1";
		int port = 443;
		String evNumber = "EV035194";
		String serialNumber = "G5060281";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(0);
		for(int i = 0; i < 5; i++) {
			ce.sendMessage("", 1);
		}
		ce.stopCommunication();
	}
	public static void testGenResponseDev() throws Exception {
		String host = "usaapd1";
		int port = 443;
		String evNumber = "EV035194";
		String serialNumber = "G5060281";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(7);
		ce.sendMessage(constructGenRespV4_1Message(0, "Round-trip Test", 0), 1);
		ce.stopCommunication();
	}
	public static void testGenReqUSR() throws Exception {
		String host = "localhost";
		int port = 443;
		String evNumber = "EV035194";
		String serialNumber = "G5060281";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(7);
		ce.sendMessage(constructGenReq_USR_V4_1Message(), 1);
		ce.sendMessage(constructGenReq_USR_V4_1Message(), 1);
		ce.stopCommunication();
	}
	/**
	 * @param i
	 * @param string
	 * @param j
	 * @return
	 */
	public static String constructGenRespV4_1Message(int resultCode, String responseMessage, int actionCode) {
		StringBuilder sb = new StringBuilder();
		sb.append("CB");
		String time = Long.toHexString(System.currentTimeMillis() / 1000);
		padLeft(sb, time, 8);
		appendHex(sb, resultCode, 2);
		if(responseMessage == null) {
			appendHex(sb, 0, 2);
		} else {
			appendHex(sb, responseMessage.length(), 2);
			sb.append(toHex(responseMessage.getBytes()));
		}
		appendHex(sb, actionCode, 2);
		return sb.toString();
	}

	public static String constructGenReq_USR_V4_1Message() {
		StringBuilder sb = new StringBuilder();
		sb.append("CA");
		String time = Long.toHexString(System.currentTimeMillis() / 1000);
		padLeft(sb, time, 8);
		appendHex(sb, 0, 2);
		appendHex(sb, 0, 4);
		return sb.toString();
	}
	public static void testLocalBatch() throws Exception {
		String host = "127.0.0.1";
		int port = 443;
		String evNumber = "EV033475";
		String serialNumber = "G5059910";
		byte[] encryptKey = "1773076481679196".getBytes();
		String card = "4300123412341017=12100001";
		Date startDate = new Date(2008-1900, 0, 2);

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.sendTranAndFillMessages((byte)1, evNumber, serialNumber, encryptKey, card, 1, startDate);
	}
	public static void testAuthFHMS(byte version) throws Exception {
		testAuth(version, version == 3 ? 'C' : 'S', "4300123412341017=12100001");
	}
	public static void testAuth(byte version, char type, String card) throws Exception {
		String host = "127.0.0.1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		Date startDate = new Date();
		String message;
		switch(version) {
			case 3:
				message = constructAuthV3Message(startDate, type, 6, card, "");
				break;
			case 4:
				message = constructAuthV4Message(startDate, type, 7, card, null);
				break;
			default:
				throw new Exception("Version " + version + " Auth is not supported");
		}
		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(7);
		ce.sendMessage(message, 1);
		ce.stopCommunication();
	}
	public static void testAuthFHMSDev() throws Exception {
		String host = "usaapd1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "4300123412341017=12100001";
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 44, 10);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message(startDate, 'C', 10, card, ""), 1);
		ce.stopCommunication();
	}
	public static void testAuthBlackboard() throws Exception {
		String host = "127.0.0.1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "6329082000000000055";
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 38, 21);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message(startDate, 'S', 1, card, ""), 1);
		ce.stopCommunication();
	}
	public static void testAuthAramark() throws Exception {
		String host = "127.0.0.1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		String card = "20000000000021";
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 38, 23);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message(startDate, 'S', 1, card, ""), 1);
		ce.stopCommunication();
	}

	public static void testAuthPaymentech() throws Exception {
		String host = "127.0.0.1";
		int port = 444;
		String evNumber = "EV100052";
		byte[] encryptKey = "8382709761112244".getBytes();
		String card = "4300123412341017=12100001";
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 38, 17);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message(startDate, 'R', 1000, card, ""), 1);
		ce.stopCommunication();
	}

	public static void testAuthAquaFill() throws Exception {
		String host = "127.0.0.1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		//String card = "010992389"; //success
		String card = "310992389"; //partial
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 38, 39);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message(startDate, 'S', 900, card, ""), 1);
		ce.stopCommunication();
	}
	public static void testAuthInternal(byte version) throws Exception {
		//^6396211([0-9]{1})([0-9]{9})([0-9]{1})=([0-9]{4})([0-9]{5})(\??)$
		testAuth(version, 'S', "639621130020001130=010812345");
	}
	public static void testAuthInternalDev() throws Exception {
		String host = "usaapd1";
		int port = 444;
		String evNumber = "EV035194";
		byte[] encryptKey = "3022848002877358".getBytes();
		//String card = "010992389"; //success
		//^6396211([0-9]{1})([0-9]{9})([0-9]{1})=([0-9]{4})([0-9]{5})(\??)$
		String card = "639621130020001130=010812345";
		Calendar cal = Calendar.getInstance();
		cal.set(2008, 9, 2, 10, 44, 8);
		Date startDate = cal.getTime();

		ClientEmulator ce = new ClientEmulator(host, port, evNumber, encryptKey);
		ce.startCommunication(3);
		ce.sendMessage(constructAuthV3Message(startDate, 'S', 900, card, ""), 1);
		ce.stopCommunication();
	}

	protected void sendTranAndFillMessages(byte protocolId, String evNumber, String serialNumber, byte[] encryptKey, String card, int fills, Date startDate) throws Exception {
		float minFillDays = 1.0f;
		float maxFillDays = 4.0f;
		int minTransPerFill = 3;
		int maxTransPerFill = 10;
		/*
select --a.*, HC.HOST_COUNTER_PARAMETER, hc.HOST_COUNTER_VALUE,
'int ' || CASE WHEN HC.HOST_COUNTER_PARAMETER like 'CURRENCY_%' THEN 'cash'
WHEN HC.HOST_COUNTER_PARAMETER like 'CASHLESS_%' THEN 'credit'
WHEN HC.HOST_COUNTER_PARAMETER like 'PASSCARD_%' THEN 'other'
END || CASE WHEN HC.HOST_COUNTER_PARAMETER like '%_TRANSACTION' THEN 'Count'
WHEN HC.HOST_COUNTER_PARAMETER like '%_MONEY%' THEN 'Total'
END || ' = ' || hc.HOST_COUNTER_VALUE || ';'
FROM (select * from (
select e.event_id, DECODE(ed_v.EVENT_DETAIL_VALUE, '1', CE.EVENT_ID, '2', E.EVENT_ID) counter_event_id,
d.device_name, d.device_serial_cd, ed_v.EVENT_DETAIL_VALUE VERSION_VALUE,
NVL(ced_ts.EVENT_DETAIL_VALUE_TS, ed_ts.EVENT_DETAIL_VALUE_TS) COUNTER_EVENT_TS, pa.action_id
from device.device d
join device.host h on d.device_id = h.device_id
join device.event e on h.host_id = e.host_id
 JOIN DEVICE.EVENT_DETAIL ed_v ON E.EVENT_ID = ed_v.EVENT_ID AND ed_v.EVENT_DETAIL_TYPE_ID = 1 -- Event Type Version
JOIN DEVICE.EVENT_DETAIL ed_ts ON E.EVENT_ID = ed_ts.EVENT_ID AND ed_ts.EVENT_DETAIL_TYPE_ID = 2 -- Host Event Timestamp
left outer join (DEVICE.EVENT_NL_DEVICE_SESSION EES
			  JOIN DEVICE.EVENT_NL_DEVICE_SESSION CES ON EES.SESSION_ID = CES.SESSION_ID AND EES.EVENT_ID != CES.EVENT_ID
			  JOIN DEVICE.EVENT CE ON CES.EVENT_ID = CE.EVENT_ID
			   AND CE.EVENT_TYPE_ID = 1
              left outer JOIN DEVICE.EVENT_DETAIL ced_ts ON CE.EVENT_ID = ced_ts.EVENT_ID AND ced_ts.EVENT_DETAIL_TYPE_ID = 2) --Host Event Timestamp
on EES.EVENT_ID = E.EVENT_ID
 LEFT OUTER JOIN (PSS.TRAN T
		  JOIN PSS.CONSUMER_ACCT_PERMISSION CAP ON T.CONSUMER_ACCT_ID = CAP.CONSUMER_ACCT_ID
		  JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
		  ) ON E.EVENT_GLOBAL_TRANS_CD = T.TRAN_GLOBAL_TRANS_CD
 WHERE d.device_name = 'EV035194'
 ORDER BY NVL(ced_ts.EVENT_DETAIL_VALUE_TS, ed_ts.EVENT_DETAIL_VALUE_TS) DESC)
 where rownum = 1) a
left outer JOIN (DEVICE.HOST_COUNTER_EVENT HCE
JOIN DEVICE.HOST_COUNTER HC ON HC.HOST_COUNTER_ID = HCE.HOST_COUNTER_ID AND (HC.HOST_COUNTER_PARAMETER LIKE '%_MONEY' OR HC.HOST_COUNTER_PARAMETER LIKE '%_TRANSACTION'))
 ON a.COUNTER_EVENT_ID = HCE.EVENT_ID;
 */
		int cashCount = 0;
		int cashTotal = 0;
		int creditCount = 0;
		int creditTotal = 0;
		int otherCount = 0;
		int otherTotal = 0;

		int byteCount = 0;

		int rollover = 1000000;

		int minAddMS = (int)(minFillDays * 24 * 60 * 60 * 1000 / (maxTransPerFill + 1));
		int maxAddMS = (int)(maxFillDays * 24 * 60 * 60 * 1000 / (minTransPerFill + 1));
		for(int i = 0; i < fills; i++) {
			startCommunication(protocolId);
			String message;
			/*cashCount = 0;
			cashTotal = 0;
			creditCount = 0;
			creditTotal = 0;
			otherCount = 0;
			otherTotal = 0;
			byteCount = 0;
			*/
			Date targetDate = new Date(startDate.getTime() + getRandom((int)(minFillDays * 24 * 60 * 60 * 1000), (int)(maxFillDays * 24 * 60 * 60 * 1000)));
			while(startDate.before(targetDate)) {
				startDate.setTime(startDate.getTime() + getRandom(minAddMS, maxAddMS));
				double d = Math.random();
				int cents = 100 + (25 * (int)(10 * Math.random()));
				if(d < 0.35) { //credit
					int[] items = new int[getRandom(1, 4)];
					for(int k = 0; k < items.length; k++) {
						items[k] = (int)(100 * Math.random());
					}
					cents = cents * items.length;
					message = constructLocalBatchMessage(startDate, 'C', cents, card, items);
					creditCount = (creditCount + items.length) % rollover;
					creditTotal = (creditTotal + cents) % rollover;
				} else if(d < 0.95) {
					int item = (int)(100 * Math.random());
					message = constructCashMessage(startDate, cents, item);
					cashCount = (cashCount + 1) % rollover;
					cashTotal = (cashTotal + cents) % rollover;
				} else {
					int[] items = new int[getRandom(1, 4)];
					for(int k = 0; k < items.length; k++) {
						items[k] = (int)(100 * Math.random());
					}
					cents = cents * items.length;
					message = constructLocalBatchMessage(startDate, 'S', cents, card, items);
					otherCount = (otherCount + items.length) % rollover;
					otherTotal = (otherTotal + cents) % rollover;
				}
				sendMessage(message, 1);
			}
			startDate.setTime(startDate.getTime() + getRandom(minAddMS, maxAddMS));
			if(Math.random() > 0.70) {
				message = constructFillV1Message(startDate, serialNumber);
				sendMessage(message, 1);
				message = constructCountersMessage(cashCount, cashTotal, creditCount, creditTotal, otherCount, otherTotal, byteCount);
				sendMessage(message, 1);
			} else {
				message = constructFillV2Message(startDate, cashCount, cashTotal, creditCount, creditTotal, otherCount, otherTotal, byteCount, card);
				sendMessage(message, 1);
			}
			stopCommunication();
		}
	}

	public static void sendFills(int fills, int protocolVersion) throws Exception {
		String host = "10.0.0.63";
		int port = 443;
		// String evNumber = "EV033586"; String serialNumber = "G5059952"; byte[] encryptKey ="1160871936175643".getBytes();
		// String evNumber = "EV033616"; String serialNumber = "M1004454"; byte[] encryptKey ="1914699776573767".getBytes();
		// String evNumber = "EV035194"; String serialNumber = "G5060281"; byte[] encryptKey ="3022848002877358".getBytes();
		String evNumber = "EV033712";
		String serialNumber = "G5060002";
		byte[] encryptKey = "6267207686941245".getBytes();

		String card = "4300123412341017=12100001";
		Date startDate = new Date(2008-1900, 0, 1);

		//EV033586,
		/*
		String host = "10.0.0.63";
		int port = 443;
		String evNumber = "EV035179";
		String serialNumber = "G5060278";
		byte[] encryptKey ="1095270400165625".getBytes();
		String card = "4300123412341017=12100001";
		/* /
		String host = "eccnet01.usatech.com"; //, 192.168.4.61";
		int port = 443;
		String evNumber = "EV083445";
		String serialNumber = "G5087253";
		byte[] encryptKey ="1505132544113750".getBytes();
		String card = "4300123412341017=12100001";
		*/
		ClientEmulator sft = new ClientEmulator(host, port, evNumber, encryptKey);
		//sft.useTestClient = true;
		sft.sendTranAndFillMessages((byte)protocolVersion, evNumber, serialNumber, encryptKey, card, fills, startDate);
	}
	public static String constructLocalBatchMessage(Date startDate, char cardType, int cents, String card, int[] items) {
		StringBuilder sb = new StringBuilder();
		sb.append("A3");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		appendHex(sb, cardType, 2);
		appendHex(sb, cents, 8);
		appendHex(sb, card.length(), 2);
		sb.append(toHex(card.getBytes()));
		sb.append("4E");
		appendHex(sb, (items.length >> 2) + 1 /* byte length of items*/, 2);
		for(int i = 0; i < items.length; i++) {
			padLeft(sb, String.valueOf(items[i]), 2);
			appendHex(sb, cents/items.length /*unit price*/, 6);
		}
		return sb.toString();
	}

	public static String constructAuthV3Message(Date startDate, char cardType, int cents, String card, String pin) {
		StringBuilder sb = new StringBuilder();
		sb.append("A0");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		appendHex(sb, cardType, 2);
		appendHex(sb, cents, 8);
		appendHex(sb, pin.length(), 2);
		sb.append(toHex(pin.getBytes()));
		sb.append(toHex(card.getBytes()));
		return sb.toString();
	}
	public static String constructAuthV4Message(Date startDate, char entryType, int cents, String card, String pin) {
		StringBuilder sb = new StringBuilder();
		sb.append("C2");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		appendHex(sb, entryType, 2);
		String amt = String.valueOf(cents);
		appendHex(sb, amt.length(), 2);
		sb.append(toHex(amt.getBytes()));
		if(pin != null && pin.length() > 0) {
			appendHex(sb, pin.length() + 4, 2); // validation data
			sb.append(toHex("pin=".getBytes()));
			sb.append(toHex(pin.getBytes()));
		} else
			appendHex(sb, 0, 2); // validation data
		appendHex(sb, 0, 2); // card reader type
		appendHex(sb, 0, 2); // track 1
		appendHex(sb, card.length(), 2); // track 2
		sb.append(toHex(card.getBytes()));
		appendHex(sb, 0, 2); // track 3
		return sb.toString();
	}
	public static String constructCashMessage(Date startDate, int cents, int item) {
		StringBuilder sb = new StringBuilder();
		sb.append("96");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		appendHex(sb, cents, 6);
		appendHex(sb, item, 2);
		return sb.toString();
	}
	protected static int getRandom(int min, int max) {
		return min + (int)(Math.random() * (max-min));
	}

	public static String constructFillV2Message(Date startDate, int cashCount, int cashTotal,
			int creditCount, int creditTotal, int otherCount, int otherTotal, int byteCount,
			String card) {
		StringBuilder sb = new StringBuilder();
		sb.append("A8");
		padCounter(sb, cashCount);
		padCounter(sb, cashTotal);
		padCounter(sb, creditCount);
		padCounter(sb, creditTotal);
		padCounter(sb, otherCount);
		padCounter(sb, otherTotal);
		padCounter(sb, byteCount);
		padCounter(sb, 1/*attempted sessions*/);
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		padLeft(sb, time, 8);
		sb.append("46");
		appendHex(sb, card.length(), 2);
		sb.append(toHex(card.getBytes()));
		return  sb.toString();
	}

	public static String constructUpdateStatusRequestMessage(long messageId, short bitmap) {
		StringBuilder sb = new StringBuilder();
		sb.append("CA");
		padLeft(sb, Long.toHexString(messageId), 8);
		appendHex(sb, 0, 2);
		appendHex(sb, bitmap, 4);
		return sb.toString();
	}
	protected static SimpleDateFormat bcdDateFormat = new SimpleDateFormat("HHmmssMMddyyyy");
	public static String constructFillV1Message(Date startDate, String serialNumber) {
		StringBuilder sb = new StringBuilder();
		sb.append("93");
		String time = Long.toHexString(startDate.getTime() / 1000);
		padLeft(sb, time, 8);
		sb.append(bcdDateFormat.format(startDate));
		sb.append("49"); //"I"
		appendHex(sb, 0, 6);
		appendHex(sb, 0, 4);
		appendHex(sb, 14 + serialNumber.length(), 2);
		sb.append(toHex("Err.DEXFill-->".getBytes()));
		sb.append(toHex(serialNumber.getBytes()));
		sb.append("2000");
		return sb.toString();
	}
	public static String constructCountersMessage(int cashCount, int cashTotal,
			int creditCount, int creditTotal, int otherCount, int otherTotal, int byteCount) {
		StringBuilder sb = new StringBuilder();
		sb.append("86");
		padCounter(sb, cashCount);
		padCounter(sb, cashTotal);
		padCounter(sb, creditCount);
		padCounter(sb, creditTotal);
		padCounter(sb, otherCount);
		padCounter(sb, otherTotal);
		padCounter(sb, byteCount);
		padCounter(sb, 1/*attempted sessions*/);
		sb.append("00000000");
		return sb.toString();
	}
	protected static final char[] HEX_DIGITS = {
    	'0' , '1' , '2' , '3' , '4' , '5' ,
    	'6' , '7' , '8' , '9' , 'A' , 'B' ,
    	'C' , 'D' , 'E' , 'F'};

	public static StringBuilder appendHex(StringBuilder sb, int num, int length) {
		return padLeft(sb, Integer.toHexString(num), length);
	}
	public static StringBuilder appendHex(StringBuilder sb, long num, int length) {
		return padLeft(sb, Long.toHexString(num), length);
	}
	public static String toHex(byte[] bytes) {
    	if(bytes == null) return null;
    	char[] result = new char[bytes.length*2];
    	for(int i = 0; i < bytes.length; i++) {
    		result[i*2] = HEX_DIGITS[(bytes[i] >> 4) & 0x0f];
    		result[i*2+1] = HEX_DIGITS[bytes[i] & 0x0f];
    	}
    	return new String(result);
    }
	protected static StringBuilder padCounter(StringBuilder sb, int counter) {
		return padLeft(sb, String.valueOf(counter), 8);
	}
	protected static StringBuilder padLeft(StringBuilder sb, String s, int length) {
		if(s.length() > length)sb.append(s.substring(s.length() - length));
		else {
			if(s.length() < length) sb.append(counterPadding, 0, length - s.length());
			sb.append(s);
		}
		return sb;
	}
	public void sendMessage(String message, int maxReplies) throws Exception {
		if(useTestClient) {
		TestClient.main(new String[] {
				host, String.valueOf(port), "3", evNumber, Conversions.bytesToHex(encryptKey), appendHex(new StringBuilder(), msg++, 2).append(message).toString()
		});
		} else {
			String evNumber;
			if (protocolVersion == 2)
				evNumber = new String(Conversions.hexToByteArray(this.evNumber));
	        else
	        	evNumber = this.evNumber;

		byte[] data = Conversions.hexToByteArray(appendHex(new StringBuilder(), msg++, 2).append(message).toString());
		ReRixMessage msg = new ReRixMessage(data, evNumber);
		log.info("<- " + msg.getHexData());
		protocol.transmitToServer(msg, output, encryptKey);

		output.flush();

        switch(protocolVersion) {
			case 2:
			case 4:
				break;
			default:
				int version = input.read();
		}

		for(int i = 0; i < maxReplies; i++) {
			try {
				ReRixMessage inMsg = protocol.receiveFromServer(input, evNumber, encryptKey);
				if(inMsg == null)
					break;
				log.info("-> " + inMsg.getHexData());
			} catch(Exception e) {
				log.warn("While reading response for device " + evNumber, e);
				break;
			}
		}
		}
	}

	public void startCommunication(int protocolVersion) throws ClassNotFoundException, NetworkLayerException, UnknownHostException, IOException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		this.protocolVersion = protocolVersion;
		if(useTestClient) return;
		int readTimeout = 30000;
		int writeTimeout = 30000;

		Class<?>[] parameterTypes = { Integer.TYPE, Integer.TYPE };
		Object[] constructorArgs = { new Integer(readTimeout), new Integer(writeTimeout) };
		String className = "com.usatech.networklayer.net.TransmissionProtocolV" + protocolVersion;
		try	{
			Class<?> protocolClass = Class.forName(className);
			Constructor<?> constructor = protocolClass.getConstructor(parameterTypes);
			protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
		} catch(InstantiationException e) {
			throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
		}
		socket = new Socket(host, port);
		socket.setSoTimeout(10000);

		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
	}

	public void stopCommunication() throws IOException {
		if(useTestClient) return;
		input.close();
		output.close();
		socket.close();
	}

	public String getEvNumber() {
		return evNumber;
	}

	public void setEvNumber(String evNumber) {
		this.evNumber = evNumber;
	}

	public byte[] getEncryptKey() {
		return encryptKey;
	}

	public void setEncryptKey(byte[] encryptKey) {
		this.encryptKey = encryptKey;
	}
}
