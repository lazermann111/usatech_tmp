#!/usr/bin/perl

package SendLine;

use strict;

use IO::Socket;
use IO::Select;
use Time::HiRes qw(time);

my $host = shift(@ARGV);
my $port = shift(@ARGV);
my $line = shift(@ARGV);
$line = pack("H*", $line);

my $startTime = time();

my $socket = new IO::Socket::INET (PeerAddr => $host.':'.$port);
my $select = new IO::Select();

if(!defined $socket )
{
	print "Failed to connect to server\n";
	exit;
}

$select->add($socket);

print "Sending line: " . unpack("H*", $line) . "\n";
if($socket->send($line) != length($line))
{
	print "Failed to send request\n";
	$socket->close();
	exit;
}

my $responseBuffer;
$socket->recv($responseBuffer, 500);

my $execTime = time() - $startTime;

my $response = $responseBuffer;

print "Response     	: $response\n";
print "Response hex   	: ". unpack("H*", $response) . "\n";
print "Execution Time	: $execTime seconds\n"; 

#$socket->recv($responseBuffer, 500);
#
#$execTime = time() - $startTime;
#
#$response = $responseBuffer;
#
#print "Response     	: $response\n";
#print "Execution Time	: $execTime seconds\n"; 

my $line2 = shift(@ARGV);
if (defined $line2)
{
	sleep 5;
	$startTime = time();

	$line2 = pack("H*", $line2);

	print "Sending line: " . unpack("H*", $line2) . "\n";
	if($socket->send($line2) != length($line2))
	{
		print "Failed to send request\n";
		$socket->close();
		exit;
	}

	$socket->recv($responseBuffer, 500);

	$execTime = time() - $startTime;

	$response = $responseBuffer;

	print "Response     	: $response\n";
	print "Response hex   	: ". unpack("H*", $response) . "\n";
	print "Execution Time	: $execTime seconds\n"; 
}


