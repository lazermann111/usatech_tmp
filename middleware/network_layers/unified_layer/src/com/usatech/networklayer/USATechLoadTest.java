package com.usatech.networklayer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.lang.reflect.Constructor;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.usatech.db.SQL;
import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.util.Conversions;
import com.usatech.util.Util;

public class USATechLoadTest
{
	private static int				readTimeout		= 30000;
	private static int				writeTimeout	= 30000;
	
	private String					server;
	private int						port;
	private TransmissionProtocol	protocol;

	private ThreadPoolExecutor		threadPool;

	public static void main(String args[]) throws Exception
	{
		String server = args[0];
		int port = Integer.parseInt(args[1]);
		int protocolID = Integer.parseInt(args[2]);
		int numStartThreads = Integer.parseInt(args[3]);
		int numAddThreads = Integer.parseInt(args[4]);
		int threadAddMillis = Integer.parseInt(args[5]);
		String fileName = args[6];
		
		new USATechLoadTest(server, port, protocolID, numStartThreads, numAddThreads, threadAddMillis, new File(fileName));
	}

	public USATechLoadTest(String server, int port, int protocolID, int numStartThreads, int numAddThreads, int threadAddMillis, File inputFile) throws Exception
	{
		this.server = server;
		this.port = port;

		Class[] parameterTypes = { Integer.TYPE, Integer.TYPE };
		Object[] constructorArgs = { new Integer(readTimeout), new Integer(writeTimeout) };

		String className = "com.usatech.networklayer.net.TransmissionProtocolV" + protocolID;

		try
		{
			Class protocolClass = Class.forName(className);
			Constructor constructor = protocolClass.getConstructor(parameterTypes);
			protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
		}
		catch (InstantiationException e)
		{
			throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
		}

		// this pre-initializes the database connections 
		SQL.getConnectionManager();

		threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(numStartThreads);

		Scanner fileScanner = new Scanner(inputFile);
		while (fileScanner.hasNextLine())
		{
			String line = fileScanner.nextLine();
			if (line.length() <= 0 || line.startsWith("#"))
				continue;
			threadPool.execute(new ClientThread(line, protocolID));
		}
		
		while(threadPool.getActiveCount() > 0)
		{
			Thread.sleep(threadAddMillis);
			
			//Util.output("Adding " + numAddThreads + " threads...");
			threadPool.setCorePoolSize(threadPool.getPoolSize()+numAddThreads);
		}

		threadPool.shutdown();
		threadPool.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
	}

	private class ClientThread implements Runnable
	{
		private String	line;
        private int     protocolID;

		private ClientThread(String line, int protocolID)
		{
			this.line = line;
            this.protocolID = protocolID;
		}

		public void run()
		{
			Socket s = null;
			DataInputStream is = null;
			DataOutputStream os = null;
			
			long threadNum = Thread.currentThread().getId();
			
			try
			{
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter(" ");
                
                String machineID;                
                if (protocolID == 2)
                    machineID = new String(Conversions.hexToByteArray(lineScanner.next()));
                else
                    machineID = lineScanner.next();
                
				byte[] key = Conversions.hexToByteArray(lineScanner.next());
				int reps = lineScanner.nextInt();

				List messages = new ArrayList();
				while (lineScanner.hasNext())
				{
					messages.add(new ReRixMessage(Conversions.hexToByteArray(lineScanner.next()), machineID));
				}
				
				int toSend = messages.size();

				for (int i = 0; i < reps; i++)
				{
					s = new Socket(server, port);
					//Util.output("New Socket: " + s.getLocalPort() + " -> " + s.getInetAddress().getHostAddress() + ":" + s.getPort());
					s.setSoTimeout(readTimeout);

					is = new DataInputStream(new BufferedInputStream(s.getInputStream()));
					os = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));
					
					int received = 0;
					int errors = 0;

					long st = System.currentTimeMillis();
					Iterator iter = messages.iterator();
					while(iter.hasNext())
					{
						ReRixMessage msg = (ReRixMessage) iter.next();
						
						//Util.output("<- " + machineID + ": " + msg.getHexData());
						protocol.transmitToServer(msg, os, key);
						os.flush();

						try
						{
                            switch (protocolID)
                            {
                            case 2:
                            case 4:
                                break;
                            default:
                                int version = is.read();
                            }
                            
							ReRixMessage inMsg = protocol.receiveFromServer(is, machineID, key);
							if (inMsg == null)
								break;

							//Util.output("-> " + machineID + ": " + inMsg.getHexData());
							received++;
						}
						catch (Exception e)
						{
							errors++;
							//break;
						}
					}
					long et = (System.currentTimeMillis() - st);
					
					System.out.println("GRAPH\t"+Util.DATE_TIME_FORMATTER.format(new Date())+"\t"+threadNum+"\t"+threadPool.getActiveCount()+"\t"+toSend+"\t"+received+"\t"+errors+"\t"+et+"\t"+((float)et)/(float)received);

					is.close();
					os.close();
					s.close();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace(System.out);
			}
			finally
			{
				if (is != null)
					try
					{
						is.close();
					}
					catch (Exception e)
					{
					}

				if (os != null)
					try
					{
						os.close();
					}
					catch (Exception e)
					{
					}

				if (s != null)
					try
					{
						s.close();
					}
					catch (Exception e)
					{
					}
			}
		}
	}
}
