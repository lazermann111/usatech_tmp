package com.usatech.networklayer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.net.Socket;
import java.util.StringTokenizer;

import com.usatech.db.SQL;
import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.util.Conversions;
import com.usatech.util.Util;

public class MessageResender
{
	public static void main(String args[]) throws Exception
	{
		if (args.length != 5)
		{
			// 0 1 2 3 4
			System.out.println("Usage: com.usatech.networklayer.MessageResender <server ip> <server port> <protocol> <delay> <file>");
			System.exit(1);
		}

		// init the db
		SQL.getConnectionManager();

		int readTimeout = 40000;
		int writeTimeout = 40000;
		int sleepTime = Integer.parseInt(args[3]);

		Class[] parameterTypes = { Integer.TYPE, Integer.TYPE };
		Integer[] constructorArgs = { new Integer(readTimeout), new Integer(writeTimeout) };

		String className = "com.usatech.networklayer.net.TransmissionProtocolV" + args[2];
		TransmissionProtocol protocol = null;

		try
		{
			Class protocolClass = Class.forName(className);
			Constructor constructor = protocolClass.getConstructor(parameterTypes);
			protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
		}
		catch (InstantiationException e)
		{
			throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
		}

		File inputFile = new File(args[4]);
		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		String line = null;
		while ((line = reader.readLine()) != null)
		{
			StringTokenizer st = new StringTokenizer(line, " ");
			String machineID = st.nextToken();
			byte[] key = st.nextToken().getBytes();
			String hex = st.nextToken();
			byte[] data = Conversions.hexToByteArray(hex);

			ReRixMessage msg = new ReRixMessage(data, machineID);

			try
			{
				Socket s = new Socket(args[0], Integer.parseInt(args[1]));
				s.setSoTimeout(readTimeout);

				DataInputStream is = new DataInputStream(new BufferedInputStream(s.getInputStream()));
				DataOutputStream os = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));

				Util.output("<- " + msg.getHexData());
				protocol.transmitToServer(msg, os, key);

				os.flush();

				int version = is.read();

				ReRixMessage inMsg = protocol.receiveFromServer(is, machineID, key);
				if (inMsg == null)
					break;

				Util.output("-> " + inMsg.getHexData());

				is.close();
				os.close();
				s.close();

				if (sleepTime > 0)
					Thread.sleep(sleepTime);
			}
			catch (Exception e)
			{
				e.printStackTrace(System.out);
			}
		}
	}
}
