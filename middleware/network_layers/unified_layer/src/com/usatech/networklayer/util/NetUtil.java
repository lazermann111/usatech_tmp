package com.usatech.networklayer.util;

import java.io.*;
import java.text.*;
import java.lang.reflect.Constructor;

import java.net.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Properties;

public class NetUtil
{
	private static final char[]	HEX_CHARS	= { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static final String	LB			= System.getProperty("line.separator");
	
	public static String formatBytes(byte[] data)
	{
		return formatBytes(data, false);
	}

	public static String formatBytes(byte[] data, boolean allHex)
	{
		if (data == null)
			return "null";

		if (data.length == 0)
			return "";

		if (allHex)
			return bytesToHex(data, ',');

		int len = data.length;

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; i++)
		{
			if (Character.isLetterOrDigit((char) data[i]) || (data[i] >= 32 && data[i] <= 126))
				sb.append((char) data[i]);
			else
				sb.append(byteToHex(data[i]));

			if (i < (len - 1))
				sb.append(',');
		}

		return sb.toString();
	}

	public static String bytesToHex(byte[] data, char sep)
	{
		int len = data.length;
		StringBuilder sb = new StringBuilder((len * 3));
		for (int i = 0; i < len; i++)
		{
			sb.append(byteToHex(data[i]));
			if (i < (len - 1))
				sb.append(sep);
		}

		return sb.toString();
	}

	public static String bytesToHex(byte[] data)
	{
		if(data == null)
			return "null";
		
		int len = data.length;
		StringBuilder sb = new StringBuilder((len * 3));
		for (int i = 0; i < len; i++)
		{
			sb.append(byteToHex(data[i]));
		}

		return sb.toString();
	}

	public static String bytesToHex(byte[] data, int pos, int len)
	{
		if(data == null)
			return "null";
		
		StringBuilder sb = new StringBuilder((len * 2));
		for (int i = pos; i < (pos+len); i++)
		{
			sb.append(byteToHex(data[i]));
		}

		return sb.toString();
	}

	public static String byteToHex(byte b)
	{
		return Character.toString(HEX_CHARS[((b & 0xf0) >> 4)]) + Character.toString(HEX_CHARS[(b & 0xf)]);
	}

	public static byte[] hexToByteArray(String hex) throws NumberFormatException
	{
		if ((hex.length() % 2) != 0)
			throw new NumberFormatException("The string has the wrong length, not pairs of hex representations.");

		int len = (hex.length() / 2);
		byte[] ba = new byte[len];
		int pos = 0;
		for (int i = 0; i < len; i++)
		{
			ba[i] = hexToByte(hex.substring(pos, pos + 2).toCharArray());
			pos += 2;
		}

		return ba;
	}

	public static byte hexToByte(char[] hex) throws NumberFormatException
	{
		if (hex.length != 2)
			throw new NumberFormatException("Invalid number of digits in " + new String(hex));

		int i = 0;
		byte nibble;

		if ((hex[i] >= '0') && (hex[i] <= '9'))
		{
			nibble = (byte) ((hex[i] - '0') << 4);
		}
		else if ((hex[i] >= 'A') && (hex[i] <= 'F'))
		{
			nibble = (byte) ((hex[i] - ('A' - 0x0A)) << 4);
		}
		else if ((hex[i] >= 'a') && (hex[i] <= 'f'))
		{
			nibble = (byte) ((hex[i] - ('a' - 0x0A)) << 4);
		}
		else
		{
			throw new NumberFormatException(hex[i] + " is not a hexadecimal string.");
		}

		i++;

		if ((hex[i] >= '0') && (hex[i] <= '9'))
		{
			nibble = (byte) (nibble | (hex[i] - '0'));
		}
		else if ((hex[i] >= 'A') && (hex[i] <= 'F'))
		{
			nibble = (byte) (nibble | (hex[i] - ('A' - 0x0A)));
		}
		else if ((hex[i] >= 'a') && (hex[i] <= 'f'))
		{
			nibble = (byte) (nibble | (hex[i] - ('a' - 0x0A)));
		}
		else
		{
			throw new NumberFormatException(hex[i] + " is not a hexadecimal string.");
		}

		return nibble;
	}

	public static String arrayToString(int[] array, char sep)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++)
		{
			sb.append(array[i]);
			if (i < (array.length - 1))
				sb.append(sep);
		}
		return sb.toString();
	}

	public static String arrayToString(byte[] array, char sep)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++)
		{
			sb.append(array[i]);
			if (i < (array.length - 1))
				sb.append(sep);
		}
		return sb.toString();
	}

	public static String formatSocketAddress(SocketAddress sa)
	{
		return ((InetSocketAddress) sa).getAddress().getHostAddress() + ":" + ((InetSocketAddress) sa).getPort();
	}

	public static String dumpBuffer(ByteBuffer buffer)
	{
		return dumpBuffer(buffer, false);
	}

	public static String dumpBuffer(ByteBuffer buffer, boolean allHex)
	{
		StringBuilder sb = new StringBuilder();
		buffer.mark();
		while (buffer.hasRemaining())
		{
			byte b = buffer.get();
			if (!allHex && Character.isLetterOrDigit((char) b))
				sb.append((char) b);
			else
				sb.append(byteToHex(b));
			if (buffer.hasRemaining())
				sb.append(",");
		}
		buffer.reset();
		return sb.toString();
	}
}