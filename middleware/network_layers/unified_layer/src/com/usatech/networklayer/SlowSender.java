package com.usatech.networklayer;

import java.io.*;
import java.net.Socket;

import com.usatech.util.Conversions;

public class SlowSender
{
	public static void main(String args[]) throws Exception
	{
		byte[] bytes = Conversions.hexToByteArray(args[3]);
		Socket s = new Socket(args[0], Integer.parseInt(args[1]));
		long waitTime = Long.parseLong(args[2]);
		OutputStream os = s.getOutputStream();
		InputStream is = s.getInputStream();

		System.out.print("<- ");

		for (int i = 0; i < bytes.length; i++)
		{
			os.write(bytes[i]);
			System.out.print(Conversions.byteToHex(bytes[i]));
			os.flush();
			Thread.sleep(waitTime);
		}

		System.out.println();

		System.out.print("-> ");

		while (true)
		{
			int b = is.read();
			if (b < 0)
				break;
			System.out.print(Conversions.byteToHex((byte) b));
		}

		System.out.println();
		System.out.println("CLOSED");
	}
}
