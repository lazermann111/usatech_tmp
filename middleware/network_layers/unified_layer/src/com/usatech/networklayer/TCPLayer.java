package com.usatech.networklayer;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.*;
import java.sql.Types;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

import com.usatech.db.*;
import com.usatech.net.TCPServer;
import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.networklayer.net.TransmissionProtocolV2;
import com.usatech.networklayer.net.TransmissionProtocolV4;
import com.usatech.util.Util;

import org.apache.commons.logging.*;
import org.apache.log4j.PropertyConfigurator;

public class TCPLayer extends TCPServer
{
	private static Log			log							= LogFactory.getLog(TCPLayer.class);
	
	private static final String version 		= "2.4.6";
	private static final String releaseDate 	= "09/09/2008";
	
	// this makes it impossible to run more than 1 TCPLayer inside a single JVM, but allows us to access the properties 
	// from other classes in a static manner, which is more convenient than passing properties objects in each constructor
	protected static Properties		props;
	
	protected Map				protocols					= new HashMap();
	protected int				socketTimeout				= 0;
	protected int				outputTimeout				= 0;
	protected boolean			startedUp					= false;
	protected int				maxConnections				= 0;
	protected String			maxConnectionExceptionRegexStr;
    protected int               networkLayerTypeID          = UNIFIED_LAYER;
    protected String            networkLayerTypeName        = "UnifiedLayer";

	protected Pattern 			maxConnectionExceptionPat;

	protected int				networkLayerID				= 0;
	protected AtomicLong		connectionCounter			= new AtomicLong();
	
	protected ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();
    
    public static final int     UNIFIED_LAYER               = 0;
    public static final int     USANET2_LAYER               = 1;
    public static final int     USANET3_LAYER               = 2;

    static
    {
        PropertyConfigurator.configureAndWatch("conf/log4j.properties", 10000);
    }

	public static void main(String args[])
	{
		if (args.length != 1)
		{
			System.out.println("Usage: com.usatech.networklayer.TCPLayer <properties file>");
			System.exit(1);
		}

		props = Util.loadProperties(args[0]);
		if (props == null)
		{
			System.out.println("Usage: com.usatech.networklayer.TCPLayer <properties file>");
			System.exit(1);
		}

		int port = Util.getIntOrExit(props, "socket.port");

		try
		{
			new TCPLayer(port);
		}
		catch (Exception e)
		{
			System.out.println("Network Layer failed to start.  Caught exception: " + e);
			e.printStackTrace(System.out);
			System.exit(1);
		}
	}
	
	public static Properties getProps()
	{
		return props;
	}

	public TCPLayer(int port) throws Exception
	{
		super(port);
        
        String configLayerType = Util.getStringOrExit(props, "layer.type");
        
        if (configLayerType.equalsIgnoreCase("Layer3"))
        {
            networkLayerTypeID = USANET3_LAYER;
            networkLayerTypeName = "Layer3";
        }
        else if (configLayerType.equalsIgnoreCase("Layer2"))
        {
            networkLayerTypeID = USANET2_LAYER;
            networkLayerTypeName = "Layer2";
        }
		
		log.info(networkLayerTypeName + " [TCP] v" + version + " (" + releaseDate + ") Copyright (c) 2003-2007 USA Technologies.  All rights reserved.");
        
		networkLayerID = getLayerID(Util.getStringOrExit(props, "layerid.persist.file"));
		if (networkLayerID < 0)
		{
			throw new Exception("Failed to get a network layer id!");
		}

		NetLayerSession.setNetLayerID(networkLayerID);

		socketTimeout = Util.getIntOrExit(props, "socket.read.timeout");
		outputTimeout = Util.getIntOrExit(props, "output.write.timeout");
		
		maxConnections = Util.getIntOrExit(props, "max.connections");

		maxConnectionExceptionRegexStr = Util.getStringOrExit(props, "max.connection.exception.regex");
		maxConnectionExceptionPat = Pattern.compile(maxConnectionExceptionRegexStr);
        
        int packetRetrySec, packetRetryNoiseSec;

		for (int i = 0; i < 255; i++)
		{
			String propName = "transmission.protocol." + i;
			String className = props.getProperty(propName);

			Class[] parameterTypes = { Integer.TYPE, Integer.TYPE };
			Integer[] constructorArgs = { new Integer(socketTimeout), new Integer(outputTimeout) };

			if (className != null)
			{
				try
				{
                    TransmissionProtocol protocol;
                    String protocolName = null;
                    
                    switch (i)
                    {    
                    case 2: // USANet2
                        packetRetrySec = Util.getIntOrExit(props, "packet.retry.sec");
                        packetRetryNoiseSec = Util.getIntOrExit(props, "packet.retry.noise.sec");
                        protocol = new TransmissionProtocolV2(socketTimeout, outputTimeout, packetRetrySec * 1000, packetRetryNoiseSec * 1000);
                        protocolName = TransmissionProtocolV2.class.getName();
                        break;
                    case 4: // USANet3
                        packetRetrySec = Util.getIntOrExit(props, "packet.retry.sec");
                        packetRetryNoiseSec = Util.getIntOrExit(props, "packet.retry.noise.sec");
                        protocol = new TransmissionProtocolV4(socketTimeout, outputTimeout, packetRetrySec * 1000, packetRetryNoiseSec * 1000);
                        protocolName = TransmissionProtocolV4.class.getName();
                        break;
                    default: // Unified Layer
                        Class protocolClass = Class.forName(className);
					    Constructor constructor = protocolClass.getConstructor(parameterTypes);
					    protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
                        protocolName = protocolClass.getName();
                    }
					
                    protocols.put(new Integer(protocol.getID()), protocol);
					log.info("Loaded protocol " + protocol.getID() + " = " + protocolName);
				}
				catch (InstantiationException e)
				{
					throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
				}
			}
		}

		if (protocols.size() == 0)
		{
			throw new NetworkLayerException("No transmission protocols defined in properties!");
		}

		Runtime.getRuntime().addShutdownHook(new ShutdownThread());
		SQL.getConnectionManager().setReleaseOnShutdown(false);

		log.info("NetLayer " + networkLayerID + " ready to accept connections on port " + port);

		startedUp = true;
	}

	protected void handleConnection(Socket socket)
	{
		while (!startedUp)
		{
			try
			{
				Thread.sleep(50);
			}
			catch (Exception e)
			{
			}
		}
		
		synchronized(this)
		{
			int connectionCount = NetLayerSession.getSessionCount();
			if(connectionCount >= maxConnections)
			{
				String remoteAddr = socket.getInetAddress().getHostAddress();
				if(maxConnectionExceptionPat.matcher(remoteAddr).matches())
				{
					log.warn("Connection from " + remoteAddr + " matches maximum connection exception: allowing over limit! (" + connectionCount + " open)");
				}
				else
				{
					String name = socket.getInetAddress().getHostAddress() + ":" + socket.getPort();
					log.warn("Connection from " + name + " exceeded maximum connections! (" + connectionCount + " open)");
					try
					{
						socket.close();
					}
					catch(Exception e) {}
					
					return;
				}
			}
		
            switch (networkLayerTypeID)
            {
	            case UNIFIED_LAYER:
	                new ClientConnection(threadPool, socket, connectionCounter.getAndIncrement(), socketTimeout, outputTimeout);
	                break;
	            case USANET3_LAYER:
	                new USANet3ClientConnection(threadPool, socket, connectionCounter.getAndIncrement(), socketTimeout, outputTimeout);
	                break;
	            case USANET2_LAYER:
	                new USANet2ClientConnection(threadPool, socket, connectionCounter.getAndIncrement(), socketTimeout, outputTimeout);
	                break;
            }
		}
	}

	private int getLayerID(String fileName)
	{
		int id = -1;
		
		log.info("Finding my Network Layer ID");

		File idFile = new File(fileName);
		if (idFile.exists() && idFile.canRead())
		{
			try
			{
				String s = Util.readContentsToString(idFile);
				id = Integer.parseInt(s);
			}
			catch (Exception e)
			{
				log.error("Failed to load network layer id from file " + fileName + ": " + e.getMessage());
				return -1;
			}
		}

		String location = null;

		try
		{
			location = InetAddress.getLocalHost().getHostAddress() + ":" + super.getPort();
		}
		catch (Exception e)
		{
			location = "unknown:" + super.getPort();
		}

		Vector spArgs = new Vector();
		spArgs.addElement(new Integer(id));
		spArgs.addElement(networkLayerTypeName + " at " + location);
        OutParameter idParam = new OutParameter(Types.INTEGER);
        spArgs.add(idParam);
        StoredProcedure sp = new StoredProcedure("{call engine.sp_start_net_layer(?, ?, ?)}", spArgs);
		
        if (!sp.execute())
		{
        	if(sp.getException() != null)
        		log.error("Failed to call SP_START_NET_LAYER: " + id + ": " + sp.getException().getMessage(), sp.getException());
        	else
        		log.error("Failed to call SP_START_NET_LAYER: " + id);
			sp.close();
			return -1;
		}

		id = idParam.getInt();
		sp.close();

		if (id < 0)
		{
			log.error("Failed to get id from SP_START_NET_LAYER: " + id);
			return -1;
		}

		try
		{
			Util.writeContentsFromString(Integer.toString(id), idFile);
		}
		catch (Exception e)
		{
			log.error("Failed to write network layer id to file " + fileName + ": " + e.getMessage());
		}

		return id;
	}

	private void markStopped(int id)
	{
		Vector spArgs = new Vector();
		spArgs.addElement(new Integer(id));
        StoredProcedure sp = new StoredProcedure("{call engine.sp_stop_net_layer(?)}", spArgs);
        
		if (!sp.execute())
		{
			if(sp.getException() != null)
				log.error("Failed to call SP_STOP_NET_LAYER: " + id + ": " + sp.getException().getMessage(), sp.getException());
			else
				log.error("Failed to call SP_STOP_NET_LAYER: " + id);
		}
		
		sp.close();
	}

	private class ShutdownThread extends Thread
	{
		public void run()
		{
			log.info("NetLayer " + networkLayerID + " shutting down...");

			TCPLayer.super.exit();
			try
			{
				TCPLayer.super.getServerSocket().close();
			}
			catch (Exception e)
			{
			}
			
			NetLayerSession.shutdownExecutors();

			markStopped(networkLayerID);

			SQL.getConnectionManager().release(true);
		}
	}
}
