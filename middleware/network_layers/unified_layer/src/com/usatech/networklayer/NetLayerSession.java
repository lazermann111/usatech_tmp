package com.usatech.networklayer;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.db.OutParameter;
import com.usatech.db.Query;
import com.usatech.db.StoredProcedure;
import com.usatech.db.Update;
import com.usatech.util.Conversions;
import com.usatech.util.Util;

public class NetLayerSession
{
	private static Log		log				= LogFactory.getLog(NetLayerSession.class);
	private static Log		asynchStatsLog	= LogFactory.getLog("AsynchExecStats");
	
	public static final char	MESSAGE_INBOUND			= 'I';
	public static final char	MESSAGE_OUTBOUND		= 'O';

	private static ExecutorService						inboundExecutor		= Executors.newSingleThreadExecutor();
	private static ScheduledExecutorService				outboundExecutor 	= Executors.newSingleThreadScheduledExecutor();
	private static Map<Long, List<ReRixMessage>>		outboundMap 		= new HashMap<Long, List<ReRixMessage>>();
	private static AtomicInteger 						sessionCounter		= new AtomicInteger();
	
	private static int netLayerID;
	
	private static ThreadPoolExecutor	startSessionThreadPool;
	private static int					startSessionTimeout;

	private static ThreadPoolExecutor	finishSessionThreadPool;

	static
	{
		int numThreadStart = Util.getIntOrExit(TCPLayer.getProps(), "netLayerSession.startSession.threads"); 
		startSessionThreadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(numThreadStart);
		startSessionTimeout = Util.getIntOrExit(TCPLayer.getProps(), "netLayerSession.startSession.timeout.seconds"); 
		
		int numThreadFinish = Util.getIntOrExit(TCPLayer.getProps(), "netLayerSession.finishSession.threads"); 
		finishSessionThreadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(numThreadFinish);

		//outboundExecutor.scheduleWithFixedDelay(new OutboundPollingThread(), 500, 500, TimeUnit.MILLISECONDS);
		outboundExecutor.scheduleAtFixedRate(new OutboundPollingThread(), 500, 500, TimeUnit.MILLISECONDS);
	}
	
	public static void setNetLayerID(int _netLayerID)
	{
		netLayerID = _netLayerID;
	}
	
	public static int getSessionCount()
	{
		return sessionCounter.get();
	}
	
	public static void shutdownExecutors()
	{
		if(inboundExecutor != null)
			inboundExecutor.shutdown();
		
		if(outboundExecutor != null)
			outboundExecutor.shutdown();
		
		if(startSessionThreadPool != null)
			startSessionThreadPool.shutdown();
		
		if(finishSessionThreadPool != null)
			finishSessionThreadPool.shutdown();
	}
	
	private String	sessionKey;
	private String	clientAddress;
	private int		clientPort;
    private String  networkLayer = "UnifiedLayer";
    private char    callinStatus;

	protected HashMap<String, NetLayerSubSession>	subSessions	= new HashMap<String, NetLayerSubSession>();

	protected NetLayerSession(String sessionKey, String clientAddress, int clientPort)
	{
		this.sessionKey = sessionKey;
		this.clientAddress = clientAddress;
		this.clientPort = clientPort;
		
		sessionCounter.incrementAndGet();
	}

	public void finish(String networkLayer, char callinStatus)
	{
        this.networkLayer = networkLayer;
        this.callinStatus = callinStatus;
        
		synchronized(subSessions)
		{
			for(NetLayerSubSession subSession : subSessions.values())
				subSession.finish();
		}
		
		sessionCounter.decrementAndGet();
	}

	public void putMessage(ReRixMessage message) throws Exception
	{
		NetLayerSubSession subSession = getSubSession(message.getEVNumber());
		if(subSession != null)
			subSession.putMessage(message);
	}
    
    public long getSessionID(String evNumber) throws Exception
    {
		NetLayerSubSession subSession = getSubSession(evNumber);
		if(subSession != null)
			return subSession.getSessionID();
		else
			return -1;
    }
    
    public byte getNextInternalMsgNo(String evNumber)
    {
        NetLayerSubSession subSession = getSubSession(evNumber);
        if(subSession != null)
            return subSession.getNextInternalMsgNo();
        else
            return 0;
    }
    
	public byte[] getEncryptionKey(String evNumber)
    {
        NetLayerSubSession subSession = getSubSession(evNumber);
        if(subSession != null)
            return subSession.getEncryptionKey();
        else
            return null;
    }
    
	public void setEncryptionKey(String evNumber, byte[] encryptionKey)
    {
        NetLayerSubSession subSession = getSubSession(evNumber);
        if(subSession != null)
            subSession.setEncryptionKey(encryptionKey);
    }
    
    public boolean getCachedKeyFlag(String evNumber)
    {
        NetLayerSubSession subSession = getSubSession(evNumber);
        if(subSession != null)
            return subSession.getCachedKeyFlag();
        else
            return false;
    }
    
    public void setCachedKeyFlag(String evNumber, boolean cachedKeyFlag)
    {
        NetLayerSubSession subSession = getSubSession(evNumber);
        if(subSession != null)
            subSession.setCachedKeyFlag(cachedKeyFlag);
    }    
	
	public List<ReRixMessage> getMessages()
	{
		List<ReRixMessage> messages = new ArrayList<ReRixMessage>();
		
		synchronized(subSessions)
		{
			for(NetLayerSubSession subSession : subSessions.values())
			{
				ReRixMessage[] array = subSession.getMessages();
				if(array!= null)
				{
					for(ReRixMessage message : array)
						messages.add(message);
				}
			}
		}
		
		return messages;
	}
	
	private NetLayerSubSession getSubSession(String evNumber)
	{
		synchronized (subSessions)
		{
			NetLayerSubSession subSession = subSessions.get(evNumber);
			if (subSession == null)
			{
				subSession = new NetLayerSubSession(evNumber);
				
				if(subSession.getSessionID() == -1)
					return null;
				
				subSessions.put(evNumber, subSession);
			}
			return subSession;
		}
	}

	private class NetLayerSubSession implements Callable<long[]>
	{
		private String	evNumber;
		private String	subSessionKey;
		private long	sessionID  				= -1;
		private int		engineID  				= -1;

		private int		inboundMessageCount		= 0;
		private int		outboundMessageCount	= 0;
		private int		inboundByteCount		= 0;
		private int		outboundByteCount		= 0;
        private byte    internalMsgNo           = -1;
        
		private byte[] encryptionKey = null;
        private boolean cachedKeyFlag           = false;

		private NetLayerSubSession(String evNumber)
		{
			this.evNumber = evNumber;
			this.subSessionKey = sessionKey + "-" + evNumber;
			
			Future<long[]> future = null;
			
			try
			{
				long st = System.currentTimeMillis();
				
				future = startSessionThreadPool.submit(this);
				long[] ids = future.get(startSessionTimeout, TimeUnit.SECONDS);
				long et = (System.currentTimeMillis() - st);
				
				sessionID = ids[0];
				engineID = (int) ids[1];

				if(sessionID == -1 || engineID == -1)
					return;
				
				if(asynchStatsLog.isDebugEnabled())
					asynchStatsLog.debug("startSession: " + et + "," + startSessionThreadPool.getQueue().size() + "," + startSessionThreadPool.getActiveCount());
				
				synchronized(outboundMap)
				{
					outboundMap.put(sessionID, new ArrayList<ReRixMessage>());
				}
				
				log.info(sessionKey + ": new NetLayerSubSession(evNumber=" + evNumber + " sessionID=" + sessionID + ")");
			}
			catch(ExecutionException e)
			{
				log.error(sessionKey + ": Failed to start netLayer session: " + subSessionKey + ": " + e.getCause(), e.getCause());
				asynchStatsLog.warn("startSession: ," + startSessionThreadPool.getQueue().size() + "," + startSessionThreadPool.getActiveCount());
			}
			catch(TimeoutException e)
			{
				log.error(sessionKey + ": Timeout while starting netLayer session: " + subSessionKey + ": " + e.getMessage(), e);
				asynchStatsLog.warn("startSession: ," + startSessionThreadPool.getQueue().size() + "," + startSessionThreadPool.getActiveCount());
				if(future != null)
				{
					if(future.cancel(false))
						log.warn(sessionKey + ": Future task cancelled successfully");
					else
						log.warn(sessionKey + ": Failed to cancel future task! (isDone="+future.isDone() + ")");
				}
			}
			catch(CancellationException e)
			{
				
			}
			catch(InterruptedException e)
			{

			}
		}
		
		private String getEVNumber()
		{
			return evNumber;
		}

		private String getSubSessionKey()
		{
			return subSessionKey;
		}

		private long getSessionID()
		{
			return sessionID;
		}
        
        public synchronized byte getNextInternalMsgNo()
        {
            if (internalMsgNo == 127)
                internalMsgNo = -1;
            return ++internalMsgNo;
        }
        
		private byte[] getEncryptionKey()
        {
            return encryptionKey;
        }
        
		private void setEncryptionKey(byte[] encryptionKey)
        {
            this.encryptionKey = encryptionKey;
        }
        
        private boolean getCachedKeyFlag()
        {
            return cachedKeyFlag;
        }
        
        private void setCachedKeyFlag(boolean cachedKeyFlag)
        {
            this.cachedKeyFlag = cachedKeyFlag;
        }

		private void putMessage(ReRixMessage message)
		{
            if (message.getMessageTypeCode() == ReRixMessage.EXTERNAL_MESSAGE)
            {
                inboundMessageCount++;
                inboundByteCount += message.getData().length;
            }

			message.setSessionID(sessionID);
			message.setNetLayerID(netLayerID);
			message.setEngineID(engineID);
            message.setSessionKey(sessionKey);
			
			inboundExecutor.execute(message);
		}
		
		private ReRixMessage[] getMessages()
		{
			ReRixMessage[] array = null;
			
			synchronized(outboundMap)
			{
				List<ReRixMessage> messages = outboundMap.get(sessionID);
				if(messages.isEmpty())
					return null;
				
				array = new ReRixMessage[messages.size()];
				messages.toArray(array);
				messages.clear();
			}
			
			for(ReRixMessage message : array)
			{
                if (message.getMessageTypeCode() == ReRixMessage.EXTERNAL_MESSAGE)
                {
                    outboundMessageCount++;
                    outboundByteCount += message.getData().length;
                }
			}
			
			return array;
		}
		
		private void finish()
		{
			if(sessionID > 0)
            {
                internalMsgNo = getNextInternalMsgNo();
				finishSessionThreadPool.submit(this);
            }
		}

		public long[] call()
		{
			if(sessionID > 0)
			{
				finish_asynch();
				return null;
			}
			else
			{
				return start_asynch();
			}
		}

		private long[] start_asynch()
		{
			long sessionID = -1;
			int engineID = -1;
			
            try
            {
    			sessionID = startDeviceSession();
    			if(sessionID != -1)
    				engineID = getBestLogicEngineID(sessionID);
            }
            catch (Exception e)
            {
                log.error("Exception in start_asynch: " + e.getMessage());
            }            
			
			long[] a = {sessionID,engineID};
			return a;
		}
		
		private long startDeviceSession()
		{
			Vector spArgs = new Vector();
			spArgs.addElement(new Integer(netLayerID));
			spArgs.addElement(evNumber);
			spArgs.addElement(clientAddress);
			spArgs.addElement(new Integer(clientPort));
            OutParameter outParam = new OutParameter(Types.BIGINT);
            spArgs.addElement(outParam);
            StoredProcedure sp = new StoredProcedure("{call engine.sp_start_device_session(?, ?, ?, ?, ?)}", spArgs);
            
            if (!sp.execute())
            {
            	if(sp.getException() != null)
            		log.error(sessionKey + ": Failed to call SP_START_DEVICE_SESSION: " + subSessionKey + ": " + sp.getException().getMessage(), sp.getException());
            	else
            		log.error(sessionKey + ": Failed to call SP_START_DEVICE_SESSION: " + subSessionKey);
            }
            
            sp.close();

            if(outParam != null)
            	return outParam.getLong();
            else
            	return -1;			
		}

		private int getBestLogicEngineID(long sessionID)
		{
			String querySQL = "select engine.sf_select_best_logic_engine(?)";
			Vector queryArgs = new Vector();
			queryArgs.addElement(new Long(sessionID));
			
			Query query = new Query(querySQL, queryArgs);
			if (!query.execute())
			{
				if(query.getException() != null)
					log.error("getBestLogicEngine failed: " + query.getException().getMessage(), query.getException());
				else
					log.error("getBestLogicEngine failed");

				query.close();
				return -1;
			}
			
			query.next();
			int engineID = query.getInt(1);
			query.close();
			return engineID;	
		}
		
		private void finish_asynch()
		{
            try
            {
                if (evNumber.equalsIgnoreCase(ReRixMessage.NET_LAYER_MACHINE_ID) == false)
                {
        			Vector spArgs = new Vector();
        			spArgs.addElement(new Long(sessionID));
        			spArgs.addElement(new Integer(inboundMessageCount));
        			spArgs.addElement(new Integer(outboundMessageCount));
        			spArgs.addElement(new Integer(inboundByteCount));
        			spArgs.addElement(new Integer(outboundByteCount));
                    spArgs.addElement(evNumber);
                    spArgs.addElement(new Integer(internalMsgNo));
                    spArgs.addElement(new Integer(netLayerID));
                    spArgs.addElement(new Integer(engineID));                            
                    spArgs.addElement(networkLayer);
                    spArgs.addElement(clientAddress);
                    spArgs.addElement(new Integer(clientPort));
                    spArgs.addElement(callinStatus);
                    StoredProcedure sp = new StoredProcedure("{call engine.sp_stop_device_session(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}", spArgs);
                    
        			if (!sp.execute())
        			{
                    	if(sp.getException() != null)
                    		log.error(sessionKey + ": Failed to call SP_STOP_DEVICE_SESSION: " + subSessionKey+ ": " + sp.getException().getMessage(), sp.getException());
                    	else
                    		log.error(sessionKey + ": Failed to call SP_STOP_DEVICE_SESSION: " + subSessionKey);
        			}
        			
        			sp.close();
                }
    			
    			synchronized(outboundMap)
    			{
    				outboundMap.remove(sessionID);
    			}
            }
            catch (Exception e)
            {
                log.error("Exception in finish_asynch: " + e.getMessage());
            }
		}
	}

	private static class OutboundPollingThread implements Runnable
	{
		public void run()
		{
			if(sessionCounter.get() <= 0)
			{
				//log.debug("Polling skipped: No active sessions");
				return;
			}

			//log.debug("Polling database for outbound messages...");
			
			String firstUpdateSQL = "update engine.machine_cmd_outbound set execute_cd = ? where net_layer_id = ? AND execute_cd = ?";
			Vector firstUpdateArgs = new Vector();
			firstUpdateArgs.addElement("I");
			firstUpdateArgs.addElement(new Integer(netLayerID));
			firstUpdateArgs.addElement("N");
			
			Update firstUpdate = new Update(firstUpdateSQL, firstUpdateArgs);
			if(!firstUpdate.execute())
			{
				if(firstUpdate.getException() != null)
					log.error("firstUpdate failed: " + firstUpdate.getException().getMessage(), firstUpdate.getException());
				else
					log.error("firstUpdate failed");
				
				firstUpdate.close();
				return;
			}
			
			firstUpdate.close();
			log.debug("Database has " + firstUpdate.getResultCode() + " waiting outbound message(s)...");
			
			if(firstUpdate.getResultCode() <= 0)
				return;
			
			String querySQL = "select command_id, modem_id, command, session_id from engine.machine_cmd_outbound where net_layer_id = ? and execute_cd = ? order by command_id";
			Vector queryArgs = new Vector();
			queryArgs.addElement(new Integer(netLayerID));
			queryArgs.addElement("I");
			
			Query query = new Query(querySQL, queryArgs);
			if (!query.execute())
			{
				if(query.getException() != null)
					log.error("query failed: " + query.getException().getMessage(), query.getException());
				else
					log.error("query failed");

				query.close();
				return;
			}
			
			Set<ReRixMessage> messageSet = new HashSet<ReRixMessage>();

			while (query.next())
			{
				String messageID = query.getString(1);
				String machineID = query.getString(2);
				String command = query.getString(3);
				long sessionID = query.getLong(4);

				byte[] bytes = Conversions.hexToByteArray(command);

				ReRixMessage message = new ReRixMessage(bytes, machineID, messageID);
				message.setNetLayerID(netLayerID);
				message.setSessionID(sessionID);
                
                if (bytes.length > 2 && bytes[1] == ReRixMessage.INTERNAL_RESPONSE)
                    message.setMessageTypeCode(bytes[2]);
				
				if(!messageSet.add(message))
					log.warn("Detected duplicate message in resultSet: " +  message);
			}
			
			for(ReRixMessage message: messageSet)
			{
				synchronized(outboundMap)
				{
					List<ReRixMessage> list = outboundMap.get(message.getSessionID());
					if(list == null)
					{
						log.warn("Got an outbound message for an unknown session: " + message.getSessionID() + " " + message);
						continue;
					}
					
					list.add(message);
				}
			}
			
			query.close();
			
			String secondUpdateSQL = "update engine.machine_cmd_outbound set execute_cd = ? where net_layer_id = ? AND execute_cd = ?";
			Vector secondUpdateArgs = new Vector();
			secondUpdateArgs.addElement("Y");
			secondUpdateArgs.addElement(new Integer(netLayerID));
			secondUpdateArgs.addElement("I");
			
			Update secondUpdate = new Update(secondUpdateSQL, secondUpdateArgs);
			if(!secondUpdate.execute())
			{
				if(secondUpdate.getException() != null)
					log.error("secondUpdate failed: " + secondUpdate.getException().getMessage(), secondUpdate.getException());
				else
					log.error("secondUpdate failed");
				
				secondUpdate.close();
				return;
			}
			
			if(secondUpdate.getResultCode() != firstUpdate.getResultCode())
				log.warn("Second update result != first update result! (" + secondUpdate.getResultCode() + " != " + firstUpdate.getResultCode() + ")");
			
			secondUpdate.close();
		}
	}
}
