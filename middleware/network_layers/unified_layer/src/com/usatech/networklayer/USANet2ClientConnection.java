package com.usatech.networklayer;

import java.io.*;
import java.net.*;
import java.util.concurrent.*;

import com.usatech.networklayer.net.*;
import com.usatech.util.Conversions;

import org.apache.commons.logging.*;

public class USANet2ClientConnection extends ClientConnection
{
    protected static Log                log                 = LogFactory.getLog(USANet2ClientConnection.class);
    protected static final int          PROTOCOL_ID         = 2;
    protected static final int          IN_BUFFER_SIZE      = 512;

    protected void createHandlers() throws IOException
    {
        outputHandler = new USANet2OutputHandler();
        inputHandler =  new USANet2InputHandler();
    }
    
    public USANet2ClientConnection(ThreadPoolExecutor threadPool, Socket socket, long connectionNum, int socketTimeout, int outputTimeout)
    {
        super(threadPool, socket, connectionNum, socketTimeout, outputTimeout);
        networkLayer = "Layer2";
    }

    protected class USANet2InputHandler extends InputHandler
    {
        public void run()
        {
            if (log.isDebugEnabled())
                log.debug(name + ": Input thread starting...");
            
            byte[] bytes = new byte[IN_BUFFER_SIZE];
            int numBytes;
            boolean closeConnectionFlag = false;

            try
            {
                while (!stopFlag)
                {
                    if (log.isDebugEnabled())
                        log.debug(name + ": Reading from socket...");
                    
                    try
                    {
                        numBytes = bis.read(bytes, 0, IN_BUFFER_SIZE);
                    }
                    catch (SocketTimeoutException ste)
                    {
                        long timeSinceLastIO = System.currentTimeMillis() - lastIO;
                        if (timeSinceLastIO < socketTimeout)
                        {
                            // if we have written something in less than the timeOut, don't exit read again
                            // allow extra time for the client to receive the outbound message
                            setSocketTimeout((int) (socketTimeout - timeSinceLastIO + 2000));
                            continue;
                        }
                        else
                        {
                            throw ste;
                        }
                    }

                    if (numBytes == -1)
                        throw new EOFException("End Of Stream");
                    
                    if (numBytes == 0)
                        continue;

                    lastIO = System.currentTimeMillis();
                    
                    if (log.isDebugEnabled())
                        log.debug(name + ": Read " + numBytes + " bytes");

                    TransmissionProtocol thisProtocol = TransmissionProtocol.getProtocol(PROTOCOL_ID);
                    if (thisProtocol == null)
                    {
                        callinStatus = STATUS_FAILED;
                        throw new NetworkLayerException("No transmission protocol defined for ID " + PROTOCOL_ID);
                    }

                    lastProtocol = thisProtocol;
                    setInBuffer(new String (bytes, 0, numBytes));

                    if (log.isDebugEnabled())
                        log.debug(name + ": Attempting to process message using " + thisProtocol.getName());

                    USANetMessage inMessage = null;
                    while (true)
                    {
                        try
                        {
                            inMessage = (USANetMessage) thisProtocol.receiveFromClient(bis, connection);
                        }
                        catch (Exception e)
                        {   
                            callinStatus = STATUS_FAILED;
                            throw e;
                        }
                    
                        if (inMessage == null)
                        {
                            callinStatus = STATUS_FAILED;
                            throw new NetworkLayerException("Failed to receive a message, closing connection.");
                        }

                        int inMessageType = inMessage.getMessageType();
                        if (inMessageType == USANetMessage.MESSAGE_TYPE_LD_PING)
                        {
                            byte msgID = inMessage.getMessageID().getBytes()[0];
                            log.info(name + ": Health check: " + getInBuffer() + ":" + Conversions.byteToHex(msgID));
                            byte[] msgData = {getNextInternalMsgNo(ReRixMessage.NET_LAYER_MACHINE_ID), ReRixMessage.INTERNAL_REQUEST, ReRixMessage.INTERNAL_HEALTH_CHECK, msgID};
                            enqueueMessage(new ReRixMessage(ReRixMessage.INTERNAL_HEALTH_CHECK, msgData, ReRixMessage.NET_LAYER_MACHINE_ID, Integer.toString(msgData[0]), 4));
                            break;
                        }
                        else if (inMessageType == USANetMessage.MESSAGE_TYPE_HANGUP)
                        {
                            closeConnectionFlag = true;
                            break;
                        }
                        else
                        {
                            log.info(name + ": " + thisProtocol.getName() + " rcvd " + inMessage.toString() + " " + inMessage.getTransmissionLength() + " bytes");
                            enqueueMessage(inMessage);
                        }
                        
                        setReceivedValidPacket(true);
                        
                        if (getInBuffer().length() == 0)
                            break;
                    }
                    
                    if (closeConnectionFlag == true)
                        break;
                    
                    setSocketTimeout(socketTimeout);
                }
            }
            catch (Exception e)
            {
                handleException(e);
            }
            finally
            {
                close();
                if (log.isDebugEnabled())
                    log.debug(name + ": Input thread exiting...");
            }
        }
    }
    
    protected class USANet2OutputHandler extends OutputHandler
    {
        protected void writeResponse(ReRixMessage outboundMessage) throws Exception
        {
            try
            {           
                lastProtocol.transmitToClient(outboundMessage, bos, connection);
                bos.flush();
            }
            catch (Exception e)
            {
                callinStatus = STATUS_FAILED;
                throw e;
            }

            lastIO = System.currentTimeMillis();
            responseCount++;

            log.info(name + ": " + lastProtocol.getName() + " sent " + outboundMessage.toString() + " " + outboundMessage.getTransmissionLength() + " bytes");
        }
    }
}
