//USANet3
package com.usatech.networklayer.net;

import java.io.*;
import java.net.*;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.networklayer.*;
import com.usatech.util.*;
import com.usatech.util.crypto.*;

public class TransmissionProtocolV4 extends TransmissionProtocol
{
    private static Log              log                         = LogFactory.getLog(TransmissionProtocolV4.class);
    
    private static final int        IN_BUFFER_SIZE              = 2048;
    
    private static final String     HANGUP_REGEX_STANDARD       = "^\\x00?\\+\\+\\+";
    private static final String     HANGUP_REGEX_MULTITECH      = "^AT#CONNECTIONSTOP";
    private static final String     HANGUP_REGEX_AS5300         = "\\x9e\\x86\\x9e\\x86\\x9e\\x86";
    private static final String     HANGUP_REGEX_LANTRONIX      = "^\\xff?Logout";
    
    private static final String     HANGUP_STRING_STANDARD      = "+++";
    private static final String     HANGUP_STRING_MULTITECH     = "AT#CONNECTIONSTOP";
    private static final String     HANGUP_STRING_AS5300        = "9e869e869e86";
    private static final String     HANGUP_STRING_LANTRONIX     = "Logout";

    private static final String     HIGH_ASCII_REGEX            = "([\\x80-\\xff]+)";
    private static final String     G4_NOISE_REGEX              = "(\\x0d\\x02\\x40\\x52.*)";
    private static final String     LINE_NOISE_REGEX            = "(.+?)(\\x31\\x35\\x38.*\\x0a.*)";
    private static final String     NON_UUENCODE_REGEX          = "([^\\x20-\\x60\\x0a]+)";
    
    private static final String     VALID_PACKET_REGEX          = "^\\x31\\x35\\x38.{12,}\\x0a";
    private static Pattern          validPacketPattern;

    private static Pattern          hangupPatternStandard;
    private static Pattern          hangupPatternMultitech;
    private static Pattern          hangupPatternAS5300;
    private static Pattern          hangupPatternLantronix;

    private static Pattern          highASCIIPattern;
    private static Pattern          G4NoisePattern;
    private static Pattern          lineNoisePattern;
    private static Pattern          nonUUEncodePattern;
    
    private static Pattern          loadBalancerPingPattern;
    
    private long                    packetRetryMilliSec         = 8000;
    private long                    packetRetryNoiseMilliSec    = 45000;

    public static final String      CRYPT_NAME                  = "TEA_CRC16";
    public static final byte        PACKET_DELIMITER            = 0x0A;

    private static Crypt            crypt                       = CryptUtil.getCrypt(CRYPT_NAME);
    
    private void initObjects()
    {
        initProperties((byte) 4, "TransmissionProtocolV4", "UUEncode([8 byte evNumber]XTEA([1 byte command length][n byte rerix command][2 byte checksum][n byte padding]))0x0A");

        hangupPatternStandard = Pattern.compile(HANGUP_REGEX_STANDARD);
        hangupPatternMultitech = Pattern.compile(HANGUP_REGEX_MULTITECH);
        hangupPatternAS5300 = Pattern.compile(HANGUP_REGEX_AS5300);
        hangupPatternLantronix = Pattern.compile(HANGUP_REGEX_LANTRONIX);

        highASCIIPattern = Pattern.compile(HIGH_ASCII_REGEX);
        G4NoisePattern = Pattern.compile(G4_NOISE_REGEX, Pattern.DOTALL);
        lineNoisePattern = Pattern.compile(LINE_NOISE_REGEX, Pattern.DOTALL);
        nonUUEncodePattern = Pattern.compile(NON_UUENCODE_REGEX);
        
        validPacketPattern = Pattern.compile(VALID_PACKET_REGEX, Pattern.DOTALL);
        loadBalancerPingPattern = Pattern.compile(USANetMessage.LOAD_BALANCER_PING_REGEX, Pattern.DOTALL);
    }

    public TransmissionProtocolV4(int readTimeout, int writeTimeout)
    {
        super(readTimeout, writeTimeout);
        initObjects();
    }
    
    public TransmissionProtocolV4(int readTimeout, int writeTimeout, long packetRetryMilliSec, long packetRetryNoiseMilliSec)
    {
        super(readTimeout, writeTimeout);
        initObjects();
        this.packetRetryMilliSec = packetRetryMilliSec;
        this.packetRetryNoiseMilliSec = packetRetryNoiseMilliSec;
    }

    public USANetMessage receiveFromClient(DataInputStream in, ClientConnection connection) throws IOException, NetworkLayerException
    {       
        int numBytes, delimiterPos;
        String receivedPacket, lineNoise;
        int readIncompleteCount = 0;
        long remainingReadTime;
        Matcher patternMatcher;
        byte[] bytes = null;
        long currentTime = System.currentTimeMillis();
        long readTimeout = currentTime + packetRetryMilliSec;
        long readNoiseTimeout = currentTime + packetRetryNoiseMilliSec;
        
        String inBuffer = connection.getInBuffer();
        String cnnName = connection.getName();
        boolean receivedLineNoise = connection.getReceivedLineNoise();
        boolean receivedValidPacket = connection.getReceivedValidPacket();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(inBuffer.getBytes());

        while (true)
        {   
            // load balancer health check handling
            patternMatcher = loadBalancerPingPattern.matcher(inBuffer);
            if (patternMatcher.find() == true)
            {
                connection.setInBuffer(patternMatcher.group(2));
                return new USANetMessage(USANetMessage.MESSAGE_TYPE_LD_PING, patternMatcher.group(3));
            }
            
            if (log.isDebugEnabled())
                log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
            
            // filter out high ascii to attempt to compensate for line-noise
            StringBuilder sbLineNoise = null;
            patternMatcher = highASCIIPattern.matcher(inBuffer);
            while (true)
            {
                if (patternMatcher.find() == true)
                {
                    lineNoise = patternMatcher.group(1);
                    
                    if (sbLineNoise == null)
                        sbLineNoise = new StringBuilder(lineNoise);
                    else
                        sbLineNoise.append(lineNoise);
                    
                    if (checkHangup(lineNoise, cnnName) == true)
                    {
                        lineNoise = sbLineNoise.toString();
                        log.warn(cnnName + ": receiveFromClient: Filtered out " + lineNoise.length() + " high ascii character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
                        return new USANetMessage(USANetMessage.MESSAGE_TYPE_HANGUP);
                    }
                }
                else
                    break;
            }

            if (sbLineNoise != null)
            {    
                receivedLineNoise = true;
                lineNoise = sbLineNoise.toString();
                inBuffer = patternMatcher.replaceAll("");
                
                log.warn(cnnName + ": receiveFromClient: Filtered out " + lineNoise.length() + " high ascii character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
                if (log.isDebugEnabled())
                    log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
            }
            
            // filter out garbage data that some ePorts produce
            patternMatcher = G4NoisePattern.matcher(inBuffer);
            if (patternMatcher.find() == true)
            {
                lineNoise = patternMatcher.group(1);
                inBuffer = patternMatcher.replaceFirst("");

                log.warn(cnnName + ": receiveFromClient: Filtered out " + lineNoise.length() + " garbage character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
                if (log.isDebugEnabled())
                    log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
                
                if (checkHangup(lineNoise, cnnName) == true)
                    return new USANetMessage(USANetMessage.MESSAGE_TYPE_HANGUP);
            }
            
            if (checkHangup(inBuffer, cnnName) == true)
                return new USANetMessage(USANetMessage.MESSAGE_TYPE_HANGUP);
            
            // filter out garbage ascii to attempt to compensate for line-noise
            if (validPacketPattern.matcher(inBuffer).find() == false)
            {
                patternMatcher = lineNoisePattern.matcher(inBuffer);
                if (patternMatcher.find() == true)
                {
                    receivedLineNoise = true;
                    lineNoise = patternMatcher.group(1);
                    inBuffer = patternMatcher.group(2);
                    
                    log.warn(cnnName + ": receiveFromClient: Filtered out " + lineNoise.length() + " garbage character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
                    if (log.isDebugEnabled())
                        log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
                } 
            }

            while (inBuffer != null)
            {                
                delimiterPos = inBuffer.indexOf(PACKET_DELIMITER);
    
                if (delimiterPos == -1)
                    break;
                     
                // do packet validation in an effort to filter out junk data and line noise
                receivedPacket = inBuffer.substring(0, delimiterPos + 1);
                inBuffer = inBuffer.substring(delimiterPos + 1);
                
                if (log.isDebugEnabled())
                {
                    log.debug(cnnName + ": receiveFromClient: Received packet: " + receivedPacket.length() + " byte(s): " + Conversions.bytesToHex(receivedPacket.getBytes()));
                    log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
                }

                if (validPacketPattern.matcher(receivedPacket).find() == true)
                {
                    // filter out non-UUEncode characters to attempt to compensate for line-noise
                    StringBuilder sbNonUUEncode = null;
                    patternMatcher = nonUUEncodePattern.matcher(receivedPacket);
                    while (true)
                    {
                        if (patternMatcher.find() == true)
                        {                            
                            if (sbNonUUEncode == null)
                                sbNonUUEncode = new StringBuilder(patternMatcher.group(1));
                            else
                                sbNonUUEncode.append(patternMatcher.group(1));
                        }
                        else
                            break;
                    }

                    if (sbNonUUEncode != null)
                    {    
                        receivedLineNoise = true;
                        lineNoise = sbNonUUEncode.toString();
                        delimiterPos -= lineNoise.length();
                        receivedPacket = patternMatcher.replaceAll("");
                        
                        log.warn(cnnName + ": receiveFromClient: Filtered out " + lineNoise.length() + " non-UUEncode character(s): " + Conversions.bytesToHex(lineNoise.getBytes()));
                        if (log.isDebugEnabled())
                            log.debug(cnnName + ": receiveFromClient: Received packet: " + receivedPacket.length() + " byte(s): " + Conversions.bytesToHex(receivedPacket.getBytes()));
                    }

                	connection.setInBuffer(inBuffer);
                    connection.setReceivedLineNoise(receivedLineNoise);
                	return createMessage(cnnName + ": receiveFromClient", receivedPacket.substring(0, delimiterPos), connection);
                }
                else
                {
                    receivedLineNoise = true;
                    log.warn(cnnName + ": receiveFromClient: Packet failed validation, discarded " + receivedPacket.length() + " garbage character(s): " + Conversions.bytesToHex(receivedPacket.getBytes()));
                }
            }
            
            while (true)
            {
                if (readIncompleteCount == 0)
                {
                    bytes = new byte[IN_BUFFER_SIZE];
                    connection.setSocketTimeout(1000);
                }
                
                readIncompleteCount++;
                
                remainingReadTime = (receivedLineNoise == true || receivedValidPacket == true ? readNoiseTimeout : readTimeout) - System.currentTimeMillis();
                if (remainingReadTime > 0)
                {
                    if (log.isDebugEnabled())
                        log.debug(cnnName + ": receiveFromClient: Failed to receive complete packet, attempt " + readIncompleteCount + " (retry expires in " + remainingReadTime + " ms).");
                }
                else
                {
                    if (log.isDebugEnabled())
                        log.debug(cnnName + ": receiveFromClient: Failed to receive complete packet, timeout reached... aborting!");
                    return null;
                }
                
                try
                {
                    numBytes = in.read(bytes, 0, IN_BUFFER_SIZE);
                }
                catch (SocketTimeoutException ste)
                {   
                    if (log.isDebugEnabled())
                        log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
                    continue;
                }
        
                if (numBytes > 0)
                {                   
                    inBuffer += new String(bytes, 0, numBytes);
                    break;
                }
                else if (numBytes == -1)
                {
                    log.warn(cnnName + ": receiveFromClient: End Of Stream");
                    return new USANetMessage(USANetMessage.MESSAGE_TYPE_HANGUP);
                }
            }
        }
    }
    
    public ReRixMessage receiveFromServer(DataInputStream in, String unitID, byte[] key) throws IOException, NetworkLayerException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int b = -1;

        while(true)
        {
            b = in.read();

            if(b == -1)
                throw new NetworkLayerException("End of stream encountered while reading message bytes.");
            else if(b == PACKET_DELIMITER)
                break;
            else
                baos.write(b);
        }

        byte[] bytes = baos.toByteArray();

        if (log.isDebugEnabled())
            log.debug("receiveFromServer: " + unitID + ": Encoded: " + Conversions.bytesToHex(bytes));

        String inBuffer = new String(bytes);
        byte[] data = PortedUtils.uudecode(inBuffer);
        byte[] rerix = decryptData(log, "receiveFromServer", unitID, data, crypt, null, key);
        return new ReRixMessage(rerix, unitID, Integer.toString(SignUtils.toUnsigned(rerix[0])), inBuffer.length() + 1);
    }
    
    public void transmitToClient(ReRixMessage message, DataOutputStream out, ClientConnection connection) throws IOException, NetworkLayerException
    {
        byte[] data = message.getData();
        String unitID = message.getEVNumber();
        String cnnName = connection.getName();
        byte[] encrypted = encryptData(log, cnnName + ": transmitToClient", unitID, data, crypt, connection, null);
        String encoded = PortedUtils.uuencode(encrypted);

        if (log.isDebugEnabled())
            log.debug(cnnName + ": transmitToClient: " + unitID + ": Encoded: " + Conversions.bytesToHex(encoded.getBytes()));

        out.write(encoded.getBytes());
        out.write(PACKET_DELIMITER);
        message.setTransmissionLength(encoded.length() + 1);
    }

    public void transmitToServer(ReRixMessage message, DataOutputStream out, byte[] key) throws IOException, NetworkLayerException
    {
        byte[] data = message.getData();
        String unitID = message.getEVNumber();
        byte[] encrypted = encryptData(log, "transmitToServer", unitID, data, crypt, null, key);
        byte[] outArray = new byte[encrypted.length + 8];
        System.arraycopy(unitID.getBytes(), 0, outArray, 0, unitID.length());
        System.arraycopy(encrypted, 0, outArray, 8, encrypted.length);

        String encoded = PortedUtils.uuencode(outArray);

        if (log.isDebugEnabled())
            log.debug("transmitToServer: " + unitID + ": Encoded: " + Conversions.bytesToHex(encoded.getBytes()));

        out.write(encoded.getBytes());
        out.write(PACKET_DELIMITER);
        message.setTransmissionLength(encoded.length() + 1);
    }

    private static boolean checkHangup(String inBuffer, String cnnName)
    {
        if (inBuffer.length() < 3)
            return false;

        String hangupString = null;

        if (hangupPatternStandard.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_STANDARD;
        else if (hangupPatternMultitech.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_MULTITECH;
        else if (hangupPatternAS5300.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_AS5300;
        else if (hangupPatternLantronix.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_LANTRONIX;

        if (hangupString != null)
        {
            log.info(cnnName + ": checkHangup: Caught hangup string (" + hangupString + "). Closing the connection...");
            return true;
        }

        return false;
    }
    
    private USANetMessage createMessage(String logPrefix, String inBuffer, ClientConnection connection) throws NetworkLayerException
    {
        if (log.isDebugEnabled())
            log.debug(logPrefix + ": Encoded: " + Conversions.bytesToHex(inBuffer.getBytes()));

        byte[] data = PortedUtils.uudecode(inBuffer);

        String unitID = new String(data, 0, 8);
        byte[] encrypted = new byte[data.length - 8];
        System.arraycopy(data, 8, encrypted, 0, encrypted.length);

        byte[] rerix = null;
        try
        {
            rerix = decryptData(log, logPrefix, unitID, encrypted, crypt, connection, null);
        }
        catch (NetworkLayerException e)
        {
            if (e.getExceptionType() == NetworkLayerException.EXCEPTION_NO_ENCRYPTION_KEY)
                throw e;
            
            log.error(logPrefix + ": " + e.getMessage());
            log.warn(logPrefix + ": Creating decryption failure message for " + unitID);
            byte[] msgData = {connection.getNextInternalMsgNo(unitID), ReRixMessage.INTERNAL_REQUEST, ReRixMessage.INTERNAL_DECRYPTION_FAILURE};
            return new USANetMessage(ReRixMessage.INTERNAL_DECRYPTION_FAILURE, msgData, unitID, Integer.toString(msgData[0]), 3);                
        }
        
        if (rerix == null || rerix.length == 0)
            return null;

        return new USANetMessage(rerix, unitID, Integer.toString(SignUtils.toUnsigned(rerix[0])), inBuffer.length() + 1);
    }
}
