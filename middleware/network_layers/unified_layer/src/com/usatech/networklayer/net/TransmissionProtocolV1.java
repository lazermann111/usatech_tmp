package com.usatech.networklayer.net;

import java.io.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.networklayer.*;
import com.usatech.util.*;

public class TransmissionProtocolV1 extends TransmissionProtocol
{
    private static Log             log             = LogFactory.getLog(TransmissionProtocolV1.class);
    
	public TransmissionProtocolV1(int readTimeout, int writeTimeout)
	{
		super(readTimeout, writeTimeout);
        initProperties((byte) 1, "TransmissionProtocolV1", "[2 byte length][8 byte identifier][n byte rerix command][1 byte checksum]");
	}

	public ReRixMessage receiveFromClient(DataInputStream in, ClientConnection connection) throws IOException, NetworkLayerException
	{
		byte[] lengthArray = new byte[2];
		try
		{
			in.readFully(lengthArray);
		}
		catch (EOFException e)
		{
			throw new NetworkLayerException("End of stream encountered while reading length bytes.");
		}

		short expectedLength = Conversions.byteArrayToShort(lengthArray);

		byte[] data = new byte[expectedLength];
		try
		{
			in.readFully(data);
		}
		catch (EOFException e)
		{
			throw new NetworkLayerException("End of stream encountered while reading message bytes.");
		}

		String unitID = new String(data, 0, 8);
		byte checkSum = data[data.length - 1];
		byte[] rerix = new byte[data.length - 9];
		System.arraycopy(data, 8, rerix, 0, rerix.length);

		byte[] checkSumData = new byte[data.length - 1];
		System.arraycopy(data, 0, checkSumData, 0, checkSumData.length);

		byte calculatedChecksum = calcCheckSum(checkSumData);

		log.debug("receiveFromClient(" + unitID + "): length " + expectedLength + ", checksum " + checkSum + ", data " + Conversions.bytesToHex(data));

		if (checkSum != calculatedChecksum)
		{
			throw new NetworkLayerException("Checksums do not match (" + checkSum + " != " + calculatedChecksum + ") for " + unitID);
		}

		return new ReRixMessage(rerix, unitID, Integer.toString(SignUtils.toUnsigned(rerix[0])), expectedLength + 3);
	}

	public ReRixMessage receiveFromServer(DataInputStream in, String unitID, byte[] key) throws IOException, NetworkLayerException
	{
		byte[] lengthArray = new byte[2];
		try
		{
			in.readFully(lengthArray);
		}
		catch (EOFException e)
		{
			throw new NetworkLayerException("End of stream encountered while reading length bytes.");
		}

		short expectedLength = Conversions.byteArrayToShort(lengthArray);

		byte[] data = new byte[expectedLength];
		try
		{
			in.readFully(data);
		}
		catch (EOFException e)
		{
			throw new NetworkLayerException("End of stream encountered while reading message bytes.");
		}

		byte checkSum = data[data.length - 1];
		byte[] rerix = new byte[data.length - 1];
		System.arraycopy(data, 0, rerix, 0, rerix.length);
		byte calculatedChecksum = calcCheckSum(rerix);

		log.debug("receiveFromServer(" + unitID + "): receiveFromServer: length " + expectedLength + ", checksum " + checkSum + ", data " + Conversions.bytesToHex(data));

		if (checkSum != calculatedChecksum)
		{
			throw new NetworkLayerException("Checksums do not match (" + checkSum + " != " + calculatedChecksum + ") for " + unitID);
		}

		return new ReRixMessage(rerix, unitID, Integer.toString(SignUtils.toUnsigned(rerix[0])), expectedLength + 3);
	}

	public void transmitToClient(ReRixMessage message, DataOutputStream out, ClientConnection connection) throws IOException, NetworkLayerException
	{
		byte[] data = message.getData();
		String unitID = message.getEVNumber();

		byte checkSum = calcCheckSum(data);
		byte[] array = new byte[data.length + 4];

		array[0] = getID();

		byte[] dataLengthBytes = Conversions.shortToByteArray(data.length + 1);
		array[1] = dataLengthBytes[0];
		array[2] = dataLengthBytes[1];

		System.arraycopy(data, 0, array, 3, data.length);
		array[array.length - 1] = checkSum;

		log.debug("transmitToClient(" + unitID + "): transmitToClient: " + Conversions.bytesToHex(array));

		out.write(array);
		message.setTransmissionLength(array.length);
	}

	public void transmitToServer(ReRixMessage message, DataOutputStream out, byte[] key) throws IOException, NetworkLayerException
	{
		byte[] data = message.getData();
		String unitID = message.getEVNumber();

		byte[] unitBytes = message.getEVNumber().getBytes();
		byte[] checkSumBytes = new byte[unitBytes.length + data.length];

		System.arraycopy(unitBytes, 0, checkSumBytes, 0, unitBytes.length);
		System.arraycopy(data, 0, checkSumBytes, unitBytes.length, data.length);

		byte checkSum = calcCheckSum(checkSumBytes);
		byte[] array = new byte[checkSumBytes.length + 4];

		array[0] = getID();

		byte[] dataLengthBytes = Conversions.shortToByteArray(checkSumBytes.length + 1);
		array[1] = dataLengthBytes[0];
		array[2] = dataLengthBytes[1];

		System.arraycopy(checkSumBytes, 0, array, 3, checkSumBytes.length);
		array[array.length - 1] = checkSum;

		log.debug("transmitToServer(" + unitID + "): transmitToServer: " + Conversions.bytesToHex(array));

		out.write(array);
		message.setTransmissionLength(array.length);
	}

	public static byte calcCheckSum(byte[] data)
	{
		byte sum = 0;
		for (int i = 0; i < data.length; i++)
		{
			sum += data[i];
		}
		return sum;
	}
}
