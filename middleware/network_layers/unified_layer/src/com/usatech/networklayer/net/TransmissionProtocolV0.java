package com.usatech.networklayer.net;

import java.io.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.networklayer.*;
import com.usatech.util.Conversions;

public class TransmissionProtocolV0 extends TransmissionProtocol
{
    private static Log             log             = LogFactory.getLog(TransmissionProtocolV0.class);
    
	public TransmissionProtocolV0(int readTimeout, int writeTimeout)
	{
		super(readTimeout, writeTimeout);
        initProperties((byte) 0, "TransmissionProtocolV0", "No Content - Heartbeat Only");
	}

	public ReRixMessage receiveFromClient(DataInputStream in, ClientConnection connection) throws IOException, NetworkLayerException
	{
		return receive(in);
	}

	public ReRixMessage receiveFromServer(DataInputStream in, String unitID, byte[] key) throws IOException, NetworkLayerException
	{
		return receive(in);
	}

	public ReRixMessage receive(DataInputStream in) throws IOException, NetworkLayerException
	{
		byte[] inData = new byte[9];
		try
		{
			in.readFully(inData);
		}
		catch (EOFException e)
		{
			throw new NetworkLayerException("End of stream encountered while reading message bytes.");
		}

		String unitID = new String(inData, 0, 8).trim();
		byte[] data = new byte[1];
		data[0] = inData[8];

		log.debug("receive  " + unitID + ": PING " + Conversions.bytesToHex(data));

		return new ReRixMessage(data, unitID, true, 10);
	}

	public void transmitToClient(ReRixMessage message, DataOutputStream out, ClientConnection connection) throws IOException, NetworkLayerException
	{
		transmit(message, out);
	}

	public void transmitToServer(ReRixMessage message, DataOutputStream out, byte[] key) throws IOException, NetworkLayerException
	{
		transmit(message, out);
	}

	public void transmit(ReRixMessage message, DataOutputStream out) throws IOException, NetworkLayerException
	{
		String unitID = message.getEVNumber();

		byte[] array = new byte[10];

		array[0] = getID();

		byte[] unitBytes = new byte[8];
		System.arraycopy(unitID.getBytes(), 0, unitBytes, 0, unitID.length()); // this will pad with 0x00 if length < 8
		System.arraycopy(unitBytes, 0, array, 1, unitBytes.length);

		byte[] data = message.getData();
		array[9] = data[0];

		log.debug("transmit " + unitID + ": PING " + Conversions.bytesToHex(data));

		out.write(array);
		message.setTransmissionLength(array.length);
	}
}
