package com.usatech.networklayer.net;

import java.io.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.networklayer.*;
import com.usatech.util.crypto.*;

public class TransmissionProtocolV7 extends TransmissionProtocol
{
    private static Log             log             = LogFactory.getLog(TransmissionProtocolV7.class);
    
	public static final String     CRYPT_NAME      = "AES_CBC_CRC16";
	
	private static Crypt           crypt           = CryptUtil.getCrypt(CRYPT_NAME);

	public TransmissionProtocolV7(int readTimeout, int writeTimeout)
	{
		super(readTimeout, writeTimeout);
        initProperties((byte) 7, "TransmissionProtocolV7", "[2 byte length][8 byte identifier]AES_CBC([4 byte nonce][2 byte command length][n byte rerix command][2 byte checksum])");
	}

    public ReRixMessage receiveFromClient(DataInputStream in, ClientConnection connection) throws IOException, NetworkLayerException
    {
        return receiveFromClient(log, crypt, in, connection);
    }

    public ReRixMessage receiveFromServer(DataInputStream in, String unitID, byte[] key) throws IOException, NetworkLayerException
    {
        return receiveFromServer(log, crypt, in, unitID, key);
    }

    public void transmitToClient(ReRixMessage message, DataOutputStream out, ClientConnection connection) throws IOException, NetworkLayerException
    {
        transmitToClient(log, crypt, message, out, connection);
    }

    public void transmitToServer(ReRixMessage message, DataOutputStream out, byte[] key) throws IOException, NetworkLayerException
    {
        transmitToServer(log, crypt, message, out, key);
    }
}
