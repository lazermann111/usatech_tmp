//USANet2
package com.usatech.networklayer.net;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.networklayer.ClientConnection;
import com.usatech.networklayer.NetworkLayerException;
import com.usatech.networklayer.ReRixMessage;
import com.usatech.networklayer.USANetMessage;
import com.usatech.util.Conversions;
import com.usatech.util.PortedUtils;
import com.usatech.util.SignUtils;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptUtil;

public class TransmissionProtocolV2 extends TransmissionProtocol
{
    private static Log              log                         = LogFactory.getLog(TransmissionProtocolV2.class);
    
    private static final int        IN_BUFFER_SIZE              = 512;
    
    private static Pattern          serialNumberPattern;
    private static final String     SERIAL_NUMBER_REGEX         = "^[0-9A-F]{8}(63){4}$";
    
    private static final String     HANGUP_REGEX_STANDARD       = "^\\x00?\\+\\+\\+";
    private static final String     HANGUP_REGEX_MULTITECH      = "^AT#CONNECTIONSTOP";
    private static final String     HANGUP_REGEX_AS5300         = "\\x9e\\x86\\x9e\\x86\\x9e\\x86";
    private static final String     HANGUP_REGEX_ANYDATA        = "^AT\\x0d";
    
    private static final String     HANGUP_STRING_STANDARD      = "+++";
    private static final String     HANGUP_STRING_MULTITECH     = "AT#CONNECTIONSTOP";
    private static final String     HANGUP_STRING_AS5300        = "9e869e869e86";
    private static final String     HANGUP_STRING_ANYDATA       = "41540d";
    
    private static final String     MODEM_NOISE_REGEX           = "^(ATE0\\x0d)";
    
    private static final String     VALID_PACKET_REGEX          = "^.{15,}\\x0a";
    private static Pattern          validPacketPattern;
    
    private static Pattern          hangupPatternStandard;
    private static Pattern          hangupPatternMultitech;
    private static Pattern          hangupPatternAS5300;
    private static Pattern          hangupPatternAnyDATA;
    
    private static Pattern          loadBalancerPingPattern;
    private static Pattern          modemNoisePattern;
    
    private long                    packetRetryMilliSec         = 8000;
    private long                    packetRetryNoiseMilliSec    = 45000;
    
	public static final String	    CRYPT_NAME	                = "TEA_CRC16";
    public static final byte        PACKET_DELIMITER            = 0x0A;
    public static final int         DEVICE_TYPE                 = 4;

	private static Crypt            crypt                       = CryptUtil.getCrypt(CRYPT_NAME);

    private void initObjects()
    {
        initProperties((byte) 2, "TransmissionProtocolV2", "UUEncode([8 byte serialNumber]XTEA([1 byte command length][n byte rerix command][2 byte checksum][n byte padding]))0x0A");
        
        serialNumberPattern = Pattern.compile(SERIAL_NUMBER_REGEX);
        
        hangupPatternStandard = Pattern.compile(HANGUP_REGEX_STANDARD);
        hangupPatternMultitech = Pattern.compile(HANGUP_REGEX_MULTITECH);
        hangupPatternAS5300 = Pattern.compile(HANGUP_REGEX_AS5300);
        hangupPatternAnyDATA = Pattern.compile(HANGUP_REGEX_ANYDATA);   
        
        validPacketPattern = Pattern.compile(VALID_PACKET_REGEX, Pattern.DOTALL);
        loadBalancerPingPattern = Pattern.compile(USANetMessage.LOAD_BALANCER_PING_REGEX, Pattern.DOTALL);
        modemNoisePattern = Pattern.compile(MODEM_NOISE_REGEX);
    }
    
	public TransmissionProtocolV2(int readTimeout, int writeTimeout)
	{
        super(readTimeout, writeTimeout);
		initObjects();
	}
    
    public TransmissionProtocolV2(int readTimeout, int writeTimeout, long packetRetryMilliSec, long packetRetryNoiseMilliSec)
    {
        super(readTimeout, writeTimeout);
        initObjects();
        this.packetRetryMilliSec = packetRetryMilliSec;
        this.packetRetryNoiseMilliSec = packetRetryNoiseMilliSec;
    }

    public USANetMessage receiveFromClient(DataInputStream in, ClientConnection connection) throws IOException, NetworkLayerException
    {
        int numBytes, delimiterPos;
        String receivedPacket;
        int readIncompleteCount = 0;
        long remainingReadTime;
        Matcher patternMatcher;
        byte[] bytes = null;
        long currentTime = System.currentTimeMillis();
        long readTimeout = currentTime + packetRetryMilliSec;
        long readNoiseTimeout = currentTime + packetRetryNoiseMilliSec;
        
        String inBuffer = connection.getInBuffer();
        String cnnName = connection.getName();
        boolean receivedValidPacket = connection.getReceivedValidPacket();
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(inBuffer.getBytes());
               
        while (true)
        {
            // load balancer health check handling
            patternMatcher = loadBalancerPingPattern.matcher(inBuffer);
            if (patternMatcher.find() == true)
            {
            	connection.setInBuffer(patternMatcher.group(2));
                return new USANetMessage(USANetMessage.MESSAGE_TYPE_LD_PING, patternMatcher.group(3));
            }
            
            if (log.isDebugEnabled())
                log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
            
            // filter out modem noise
            patternMatcher = modemNoisePattern.matcher(inBuffer);
            if (patternMatcher.find() == true)
            {
                String modemNoise = patternMatcher.group(1);
                inBuffer = patternMatcher.replaceFirst("");

                log.warn(cnnName + ": receiveFromClient: Filtered out " + modemNoise.length() + " garbage character(s): " + Conversions.bytesToHex(modemNoise.getBytes()));
                if (log.isDebugEnabled())
                    log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
            }
                        
            if (checkHangup(inBuffer, cnnName) == true)
                return new USANetMessage(USANetMessage.MESSAGE_TYPE_HANGUP);
            
            while (inBuffer != null)
            {
                delimiterPos = inBuffer.indexOf(PACKET_DELIMITER);
                    
                if (delimiterPos == -1)
                    break;
                
                // do packet validation in an effort to filter out junk data and line noise
                receivedPacket = inBuffer.substring(0, delimiterPos + 1);
                inBuffer = inBuffer.substring(delimiterPos + 1);
                
                if (log.isDebugEnabled())
                {
                    log.debug(cnnName + ": receiveFromClient: Received packet: " + receivedPacket.length() + " byte(s): " + Conversions.bytesToHex(receivedPacket.getBytes()));
                    log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
                }
                
                if (validPacketPattern.matcher(receivedPacket).find() == true)
                {
                	connection.setInBuffer(inBuffer);
                    return createMessage(cnnName + ": receiveFromClient", receivedPacket.substring(0, delimiterPos), connection);
                }
                else
                    log.warn(cnnName + ": receiveFromClient: Packet failed validation, discarded " + receivedPacket.length() + " garbage character(s): " + Conversions.bytesToHex(receivedPacket.getBytes()));
            }
            
            while (true)
            {
                if (readIncompleteCount == 0)
                {
                    bytes = new byte[IN_BUFFER_SIZE];
                    connection.setSocketTimeout(1000);
                }
                
                readIncompleteCount++;
            
                remainingReadTime = (receivedValidPacket == true ? readNoiseTimeout : readTimeout) - System.currentTimeMillis();
                if (remainingReadTime > 0)
                {
                    if (log.isDebugEnabled())
                        log.debug(cnnName + ": receiveFromClient: Failed to receive complete packet, attempt " + readIncompleteCount + " (retry expires in " + remainingReadTime + " ms).");
                }
                else
                {
                    if (log.isDebugEnabled())
                        log.debug(cnnName + ": receiveFromClient: Failed to receive complete packet, timeout reached... aborting!");
                    return null;
                }
                
                try
                {
                    numBytes = in.read(bytes, 0, IN_BUFFER_SIZE);
                }
                catch (SocketTimeoutException ste)
                {   
                    if (log.isDebugEnabled())
                        log.debug(cnnName + ": receiveFromClient: Input buffer: " + inBuffer.length() + " byte(s): " + Conversions.bytesToHex(inBuffer.getBytes()));
                    continue;
                }
    
                if (numBytes > 0)
                {                   
                    inBuffer += new String(bytes, 0, numBytes);
					break;
                }
                else if (numBytes == -1)
                {
                    log.warn(cnnName + ": receiveFromClient: End Of Stream");
                    return new USANetMessage(USANetMessage.MESSAGE_TYPE_HANGUP);
                }
            }
        }
    }    

	public ReRixMessage receiveFromServer(DataInputStream in, String unitID, byte[] key) throws IOException, NetworkLayerException
	{
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int b = -1;

        while(true)
        {
            b = in.read();

            if(b == -1)
                throw new NetworkLayerException("End of stream encountered while reading message bytes.");
            else if(b == PACKET_DELIMITER)
                break;
            else
                baos.write(b);
        }

        byte[] bytes = baos.toByteArray();

        if (log.isDebugEnabled())
            log.debug("receiveFromServer: " + unitID + ": Encoded: " + Conversions.bytesToHex(bytes));

        String serialNumberHex = Conversions.bytesToHex(unitID.getBytes());
        String inBuffer = new String(bytes);
		byte[] data = PortedUtils.uudecode(inBuffer);
        
		byte[] encryptionKey = (unitID + unitID).getBytes();
        byte[] rerix = decryptData(log, "receiveFromServer", serialNumberHex, data, crypt, null, encryptionKey);
        return new ReRixMessage(rerix, unitID, Integer.toString(SignUtils.toUnsigned(rerix[0])), inBuffer.length() + 1);
	}
    
    public void transmitToClient(ReRixMessage message, DataOutputStream out, ClientConnection connection) throws IOException, NetworkLayerException
	{
		byte[] data = message.getData();
		String unitID = message.getEVNumber();
        String cnnName = connection.getName();        
		byte[] key = connection.getEncryptionKey();
        
        if (key == null)
            throw new NetworkLayerException(cnnName + ": transmitToClient: " + unitID + ": Encryption key is null");
      
        byte[] encrypted = encryptData(log, cnnName + ": transmitToClient", unitID, data, crypt, null, key);
        String encoded = PortedUtils.uuencode(encrypted);

        if (log.isDebugEnabled())
            log.debug(cnnName + ": transmitToClient: " + unitID + ": Encoded: " + Conversions.bytesToHex(encoded.getBytes()));

        out.write(encoded.getBytes());
        out.write(PACKET_DELIMITER);
        message.setTransmissionLength(encoded.length() + 1);
	}

    public void transmitToServer(ReRixMessage message, DataOutputStream out, byte[] key) throws IOException, NetworkLayerException
	{
		byte[] data = message.getData();
		String unitID = message.getEVNumber();
        String serialNumberHex = Conversions.bytesToHex(unitID.getBytes());
        
		byte[] encryptionKey = (unitID + unitID).getBytes();
        byte[] encrypted = encryptData(log, "transmitToServer", serialNumberHex, data, crypt, null, encryptionKey);
        byte[] outArray = new byte[encrypted.length + 8];
        System.arraycopy(unitID.getBytes(), 0, outArray, 0, unitID.length());
        System.arraycopy(encrypted, 0, outArray, 8, encrypted.length);

        String encoded = PortedUtils.uuencode(outArray);

        if (log.isDebugEnabled())
            log.debug("transmitToServer: " + serialNumberHex + ": Encoded: " + Conversions.bytesToHex(encoded.getBytes()));

        out.write(encoded.getBytes());
        out.write(PACKET_DELIMITER);
        message.setTransmissionLength(encoded.length() + 1);
	}
    
    private static boolean checkHangup(String inBuffer, String cnnName)
    {
        if (inBuffer.length() < 3)
            return false;
                
        String hangupString = null;
        
        if (hangupPatternStandard.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_STANDARD;
        else if (hangupPatternAnyDATA.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_ANYDATA;
        else if (hangupPatternAS5300.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_AS5300;
        else if (hangupPatternMultitech.matcher(inBuffer).find() == true)
            hangupString = HANGUP_STRING_MULTITECH;

        if (hangupString != null)
        {
            log.info(cnnName + ": checkHangup: Caught hangup string (" + hangupString + "). Closing the connection...");
            return true;
        }
        
        return false;
    }
    
    private USANetMessage createMessage(String logPrefix, String inBuffer, ClientConnection connection) throws NetworkLayerException
    {
        if (log.isDebugEnabled())
            log.debug(logPrefix + ": Encoded: " + Conversions.bytesToHex(inBuffer.getBytes()));

        byte[] data = PortedUtils.uudecode(new String(inBuffer));

        String serialNumber = new String(data, 0, 8);
        String serialNumberHex = Conversions.bytesToHex(serialNumber.getBytes());
        
        if (serialNumberPattern.matcher(serialNumberHex).matches() == false)
            throw new NetworkLayerException("Received invalid serial number " + serialNumberHex);
            
        byte[] encrypted = new byte[data.length - 8];
        System.arraycopy(data, 8, encrypted, 0, encrypted.length);
		byte[] key = (serialNumber + serialNumber).getBytes();

        byte[] rerix = decryptData(log, logPrefix, serialNumberHex, encrypted, crypt, null, key);
        if (rerix == null || rerix.length == 0)
            return null;
        
        connection.setEncryptionKey(key);

        return new USANetMessage(rerix, serialNumberHex, Integer.toString(SignUtils.toUnsigned(rerix[0])), inBuffer.length() + 1);
    }
}
