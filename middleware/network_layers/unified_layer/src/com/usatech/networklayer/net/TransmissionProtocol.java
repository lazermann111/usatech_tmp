package com.usatech.networklayer.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.security.InvalidKeyException;

import org.apache.commons.logging.Log;

import com.usatech.networklayer.ClientConnection;
import com.usatech.networklayer.KeyManager;
import com.usatech.networklayer.NetworkLayerException;
import com.usatech.networklayer.ReRixMessage;
import com.usatech.util.Conversions;
import com.usatech.util.SignUtils;
import com.usatech.util.crypto.CRCException;
import com.usatech.util.crypto.Crypt;
import com.usatech.util.crypto.CryptException;

public abstract class TransmissionProtocol
{
    protected static TransmissionProtocol[] protocols   = new TransmissionProtocol[255];

    protected int                           readTimeout;
    protected int                           writeTimeout;
    protected byte                          formatID;
    protected String                        name;
    protected String                        description;

    public TransmissionProtocol(int readTimeout, int writeTimeout)
    {
        this.readTimeout = readTimeout;
        this.writeTimeout = writeTimeout;
    }
    
    protected void initProperties(byte formatID, String name, String description)
    {
        this.formatID = formatID;
        this.name = name;
        this.description = description;        

        protocols[formatID] = this;
        protocols[(30 + formatID)] = this; // add it as ASCII too
    }

    public void setReadTimeout(int readTimeout)
    {
        this.readTimeout = readTimeout;
    }

    public void setWriteTimeout(int writeTimeout)
    {
        this.writeTimeout = writeTimeout;
    }

    public int getReadTimeout()
    {
        return readTimeout;
    }

    public int getWriteTimeout()
    {
        return writeTimeout;
    }

    public byte getID()
    {
        return formatID;
    }

    public String getName()
    {
        return name;
    }     

    public String getDescription()
    {
        return description;
    }
    
    public abstract ReRixMessage receiveFromClient(DataInputStream in, ClientConnection connection) throws IOException, NetworkLayerException;

    public abstract void transmitToClient(ReRixMessage message, DataOutputStream out, ClientConnection connection) throws IOException, NetworkLayerException;

    public abstract void transmitToServer(ReRixMessage message, DataOutputStream out, byte[] key) throws IOException, NetworkLayerException;

    public abstract ReRixMessage receiveFromServer(DataInputStream in, String unitID, byte[] key) throws IOException, NetworkLayerException;
    
    public static TransmissionProtocol getProtocol(int protocolID)
    {
        if (protocolID > protocols.length)
            return null;

        return protocols[protocolID];
    }
    
    protected static byte[] decryptData(Log log, String logPrefix, String unitID, byte[] encrypted, Crypt crypt, ClientConnection connection, byte[] encryptionKey) throws NetworkLayerException
    {
        byte[] decrypted = null;
		byte[] key = encryptionKey;
        
        if (log.isDebugEnabled())
            log.debug(logPrefix + ": " + unitID + ": Encrypted: " + Conversions.bytesToHex(encrypted));
        
        while(true)
        {
            if (connection != null)
            {
                key = KeyManager.getKey(unitID, connection);
                if (key == null)
                    throw new NetworkLayerException(NetworkLayerException.EXCEPTION_NO_ENCRYPTION_KEY, "Failed to lookup encryption key for " + unitID);
            }
            
            if (log.isDebugEnabled())
				log.debug(logPrefix + ": " + unitID + ": Encryption Key: " + key + ": Key Hex: " + Conversions.bytesToHex(key));

            try
            {
				decrypted = crypt.decrypt(encrypted, key);
                break;
            }
            catch(Exception e)
            {
                if (connection != null && connection.getCachedKeyFlag(unitID) == true && unitID.equals(ReRixMessage.DEFAULT_MACHINE_ID) == false)
                {
                    log.warn(logPrefix + ": Decrypt failed! Reloading " + unitID + " encryption key");
                    KeyManager.clearCache(unitID);
                    continue;
                }
                
                if(e instanceof InvalidKeyException)
					throw new NetworkLayerException("Failed to decrypt message (" + Conversions.bytesToHex(encrypted) + ") from " + unitID + ": Invalid Key: " + key + ": Key Hex: " + Conversions.bytesToHex(key), e);
                else if(e instanceof CRCException)
                    throw new NetworkLayerException("Failed to decrypt message (" + Conversions.bytesToHex(encrypted) + ") from " + unitID + ": CRC Check Failed!", e);
                else if(e instanceof CryptException)
                    throw new NetworkLayerException("Failed to decrypt message (" + Conversions.bytesToHex(encrypted) + ") from " + unitID + ": Decryption Algorithm Failure!", e);
                else
                    throw new NetworkLayerException("Caught unexpected exception decrypting message (" + Conversions.bytesToHex(encrypted) + ") : " + e, e);
            }
        }
        
        if (log.isDebugEnabled())
            log.debug(logPrefix + ": " + unitID + ": Decrypted: " + Conversions.bytesToHex(decrypted));
        
        return decrypted;
    }
    
	protected static byte[] encryptData(Log log, String logPrefix, String unitID, byte[] data, Crypt crypt, ClientConnection connection, byte[] encryptionKey) throws NetworkLayerException
    {
        byte[] encrypted = null;
		byte[] key = encryptionKey;
        
        if (log.isDebugEnabled())
            log.debug(logPrefix + ": " + unitID + ": Decrypted: " + Conversions.bytesToHex(data));
        
        while(true)
        {
            if (connection != null)
            {
                key = KeyManager.getKey(unitID, connection);
                if (key == null)
                    throw new NetworkLayerException(NetworkLayerException.EXCEPTION_NO_ENCRYPTION_KEY, "Failed to lookup encryption key for " + unitID);
            }
    
            if (log.isDebugEnabled())
				log.debug(logPrefix + ": " + unitID + ": Encryption Key: " + key + ": Key Hex: " + Conversions.bytesToHex(key));
    
            try
            {
				encrypted = crypt.encrypt(data, key);
                break;
            }
            catch(Exception e)
            {
                if(connection != null && connection.getCachedKeyFlag(unitID) == true && unitID.equals(ReRixMessage.DEFAULT_MACHINE_ID) == false)
                {
                    log.warn(logPrefix + ": Encrypt failed! Reloading " + unitID + " encryption key");
                    KeyManager.clearCache(unitID);
                    continue;
                }
                
                if(e instanceof InvalidKeyException)
					throw new NetworkLayerException("Failed to encrypt message (" + Conversions.bytesToHex(data) + ") : Invalid Key: " + key + ": Key Hex: " + Conversions.bytesToHex(key), e);
                else if(e instanceof CRCException)
                    throw new NetworkLayerException("Failed to encrypt message (" + Conversions.bytesToHex(data) + ") : CRC Check Failed!", e);
                else if(e instanceof CryptException)
                    throw new NetworkLayerException("Failed to encrypt message (" + Conversions.bytesToHex(data) + ") : Decryption Algorithm Failure!", e);
                else
                    throw new NetworkLayerException("Caught unexpected exception encrypting message (" + Conversions.bytesToHex(data) + ") : " + e, e);
            }
        }
        
        if (log.isDebugEnabled())
            log.debug(logPrefix + ": " + unitID + ": Encrypted: " + Conversions.bytesToHex(encrypted));
        
        return encrypted;
    }
    
    protected ReRixMessage receiveFromClient(Log log, Crypt crypt, DataInputStream in, ClientConnection connection) throws IOException, NetworkLayerException
    {
        byte[] lengthArray = new byte[2];
        try
        {
            in.readFully(lengthArray);
        }
        catch (EOFException e)
        {
            throw new NetworkLayerException("End of stream encountered while reading length bytes.");
        }

        short expectedLength = Conversions.byteArrayToShort(lengthArray);

        byte[] data = new byte[expectedLength];
        try
        {
            in.readFully(data);
        }
        catch (EOFException e)
        {
            throw new NetworkLayerException("End of stream encountered while reading message bytes.");
        }

        String unitID = new String(data, 0, 8).trim();
        byte[] encrypted = new byte[data.length - 8];
        System.arraycopy(data, 8, encrypted, 0, encrypted.length);
        
        byte[] rerix = decryptData(log, "receiveFromClient", unitID, encrypted, crypt, connection, null);
        return new ReRixMessage(rerix, unitID, Integer.toString(SignUtils.toUnsigned(rerix[0])), expectedLength + 3);
    }

    protected ReRixMessage receiveFromServer(Log log, Crypt crypt, DataInputStream in, String unitID, byte[] key) throws IOException, NetworkLayerException
    {
        byte[] lengthArray = new byte[2];
        try
        {
            in.readFully(lengthArray);
        }
        catch (EOFException e)
        {
            throw new NetworkLayerException("End of stream encountered while reading length bytes.");
        }

        short expectedLength = Conversions.byteArrayToShort(lengthArray);

        byte[] encrypted = new byte[expectedLength];
        try
        {
            in.readFully(encrypted);
        }
        catch (EOFException e)
        {
            throw new NetworkLayerException("End of stream encountered while reading message bytes.");
        }

        byte[] rerix = decryptData(log, "receiveFromServer", unitID, encrypted, crypt, null, key);
        return new ReRixMessage(rerix, unitID, Integer.toString(SignUtils.toUnsigned(rerix[0])), expectedLength + 3);
    }

    protected void transmitToClient(Log log, Crypt crypt, ReRixMessage message, DataOutputStream out, ClientConnection connection) throws IOException, NetworkLayerException
    {
        byte[] data = message.getData();
        String unitID = message.getEVNumber();

        byte[] encrypted = encryptData(log, "transmitToClient", unitID, data, crypt, connection, null);
        byte[] array = new byte[encrypted.length + 3];

        array[0] = formatID;
        SignUtils.writeShort((short) encrypted.length, array, 1);

        System.arraycopy(encrypted, 0, array, 3, encrypted.length);

        if (log.isDebugEnabled())
            log.debug("transmitToClient " + unitID + ": Header: " + Conversions.bytesToHex(array, 0, 3));

        out.write(array);
        message.setTransmissionLength(array.length);
    }

	protected void transmitToServer(Log log, Crypt crypt, ReRixMessage message, DataOutputStream out, byte[] key) throws IOException, NetworkLayerException
    {
        byte[] data = message.getData();
        String unitID = message.getEVNumber();      
        byte[] encrypted = encryptData(log, "transmitToServer", unitID, data, crypt, null, key);
        byte[] array = new byte[encrypted.length + 11];

        array[0] = formatID;

        byte[] dataLengthBytes = Conversions.shortToByteArray(encrypted.length + 8);
        array[1] = dataLengthBytes[0];
        array[2] = dataLengthBytes[1];

        byte[] unitBytes = new byte[8];
        System.arraycopy(unitID.getBytes(), 0, unitBytes, 0, unitID.length()); // this will pad with 0x00 if length < 8
        System.arraycopy(unitBytes, 0, array, 3, unitBytes.length);
        System.arraycopy(encrypted, 0, array, 11, encrypted.length);

        if (log.isDebugEnabled())
            log.debug("transmitToServer " + unitID + ": Header: " + Conversions.bytesToHex(array, 0, 11));

        out.write(array);
        message.setTransmissionLength(array.length);
    }
    
    protected ReRixMessage receive(Log log, Crypt crypt, DataInputStream in, ClientConnection connection, byte[] key) throws IOException, NetworkLayerException
    {
        byte[] lengthArray = new byte[2];
        try
        {
            in.readFully(lengthArray);
        }
        catch (EOFException e)
        {
            throw new NetworkLayerException("End of stream encountered while reading length bytes.");
        }

        short expectedLength = Conversions.byteArrayToShort(lengthArray);

        byte[] data = new byte[expectedLength];
        try
        {
            in.readFully(data);
        }
        catch (EOFException e)
        {
            throw new NetworkLayerException("End of stream encountered while reading message bytes.");
        }

        String unitID = new String(data, 0, 8).trim();
        byte[] encrypted = new byte[data.length - 8];
        System.arraycopy(data, 8, encrypted, 0, encrypted.length);

        byte[] rerix = decryptData(log, "receive", unitID, encrypted, crypt, connection, key);
        return new ReRixMessage(rerix, unitID, Integer.toString(SignUtils.toUnsigned(rerix[0])), expectedLength + 3);
    }
    
	protected void transmit(Log log, Crypt crypt, ReRixMessage message, DataOutputStream out, ClientConnection connection, byte[] key) throws IOException, NetworkLayerException
    {
        byte[] data = message.getData();
        String unitID = message.getEVNumber();

        byte[] encrypted = encryptData(log, "transmit", unitID, data, crypt, connection, key);
        byte[] array = new byte[encrypted.length + 11];

        array[0] = getID();

        byte[] dataLengthBytes = Conversions.shortToByteArray(encrypted.length + 8);
        array[1] = dataLengthBytes[0];
        array[2] = dataLengthBytes[1];

        byte[] unitBytes = new byte[8];
        System.arraycopy(unitID.getBytes(), 0, unitBytes, 0, unitID.length()); // this will pad with 0x00 if length < 8
        System.arraycopy(unitBytes, 0, array, 3, unitBytes.length);
        System.arraycopy(encrypted, 0, array, 11, encrypted.length);

        if (log.isDebugEnabled())
            log.debug("transmit " + unitID + ": Header: " + Conversions.bytesToHex(array, 0, 11));

        out.write(array);
        message.setTransmissionLength(array.length);
    }    
}
