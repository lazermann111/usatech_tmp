package com.usatech.networklayer.net;

import java.io.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.networklayer.*;
import com.usatech.util.crypto.*;

public class TransmissionProtocolV8 extends TransmissionProtocol
{
    private static Log             log             = LogFactory.getLog(TransmissionProtocolV8.class);
    
	public static final String     CRYPT_NAME      = "AES_CBC_CRC16";

	private static Crypt           crypt           = CryptUtil.getCrypt(CRYPT_NAME);

	public TransmissionProtocolV8(int readTimeout, int writeTimeout)
	{
		super(readTimeout, writeTimeout);
        initProperties((byte) 8, "TransmissionProtocolV8", "[2 byte length][8 byte identifier]AES_CBC([4 byte nonce][2 byte command length][n byte rerix command][2 byte checksum])");
	}

	// in this protocol client and server transmission formats are exactly the same

	public ReRixMessage receiveFromClient(DataInputStream in, ClientConnection connection) throws IOException, NetworkLayerException
	{
		return receive(log, crypt, in, connection, null);
	}

	public ReRixMessage receiveFromServer(DataInputStream in, String unitID, byte[] key) throws IOException, NetworkLayerException
	{
		return receive(log, crypt, in, null, key);
	}

	public void transmitToClient(ReRixMessage message, DataOutputStream out, ClientConnection connection) throws IOException, NetworkLayerException
	{
		transmit(log, crypt, message, out, connection, null);
	}

	public void transmitToServer(ReRixMessage message, DataOutputStream out, byte[] key) throws IOException, NetworkLayerException
	{
		transmit(log, crypt, message, out, null, key);
	}
}
