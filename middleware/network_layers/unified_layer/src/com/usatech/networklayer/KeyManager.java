package com.usatech.networklayer;

import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.util.Cache;
import com.usatech.util.Conversions;
import com.usatech.util.LockSet;
import com.usatech.util.Util;

public class KeyManager
{
	private static Log		log				= LogFactory.getLog(KeyManager.class);
	
	private static Cache 				keyCache;	
	private static LockSet				lockSet = new LockSet(500);
    private static Pattern              machineIDPattern;
	
	static
	{
        machineIDPattern = Pattern.compile(ReRixMessage.MACHINE_ID_REGEX);
        InputStream is = KeyManager.class.getResourceAsStream("/keymanager.properties");
		Properties props = new Properties();
		try 
		{
			props.load(is);
			
			int maxKeys = Util.getIntOrExit(props, "keyManager.keyCache.maxSize");
			int maxHours = Util.getIntOrExit(props, "keyManager.keyCache.maxHours");
			keyCache = new Cache(maxKeys, (maxHours * 60 * 60 * 1000));
		}
		catch (Exception e) 
		{
			log.error("Can't read the properties file.  Make sure keymanager.properties is in the CLASSPATH", e);
		}
		finally
		{
			try
			{
				is.close();
			}
			catch(Exception e2) {}
		}
	}

	public static byte[] getKey(String evNumber, ClientConnection connection)
	{
		if (evNumber.equals(ReRixMessage.DEFAULT_MACHINE_ID))
			return ReRixMessage.DEFAULT_ENCRYPTION_KEY.getBytes();
        
        String logPrefix = connection.getName() + ": getKey: ";
        
        if (machineIDPattern.matcher(evNumber).matches() == false)
        {
            log.error(logPrefix + "Invalid evNumber: " + evNumber + ", hex: " + Conversions.bytesToHex(evNumber.getBytes()));
            return null;
        }
		
		// we don't want to synch on a single object here, as it results in virtual single threading... We also can't synch
		// on the string object itself.  Although java caches strings, there can be multiple copies of equivilent strings,
		// and synchronized is done on the object.  Thus we need to use a LockSet, which insures that 2 equivilent string
		// will return the same object to synchronize on
		synchronized(lockSet.acquireLock(evNumber))
		{
			byte[] key = null;
			key = (byte[]) keyCache.get(evNumber);
			
			if (key != null)
			{
                if (log.isDebugEnabled())
                    log.debug(logPrefix + "Returning cached encryption key for " + evNumber);
                connection.setCachedKeyFlag(evNumber, true);
				return key;
			}
            
            connection.setCachedKeyFlag(evNumber, false);
            connection.setEncryptionKey(evNumber, null);            
            
            if (log.isDebugEnabled())
                log.debug(logPrefix + "Creating key request message for " + evNumber);
            byte[] msgData = {connection.getNextInternalMsgNo(evNumber), ReRixMessage.INTERNAL_REQUEST, ReRixMessage.INTERNAL_KEY_REQUEST};
            ReRixMessage message = new ReRixMessage(ReRixMessage.INTERNAL_KEY_REQUEST, msgData, evNumber, Integer.toString(msgData[0]), 3);
            try
            {
                connection.enqueueMessage(message);
                connection.inputHandlerWait();
                key = connection.getEncryptionKey(evNumber);
            }
            catch (Exception e)
            {                
                log.error(logPrefix + "Caught exception while inserting key request message into the queue: " + e.getMessage());  
            }
			
			if (key == null)
				log.warn(logPrefix + "No key found for " + evNumber);
			else
			{
				keyCache.put(evNumber, key);
                if (log.isDebugEnabled())
                    log.debug(logPrefix + "Returning fresh encryption key for " + evNumber);
			}

			return key;
		}
	}
	
	public static void clearCache(String evNumber)
	{
		synchronized(lockSet.acquireLock(evNumber))
		{
			keyCache.remove(evNumber);
		}
	}

	public static void clearCache()
	{
		synchronized(keyCache)
		{
			keyCache.clear();
		}
	}
}
