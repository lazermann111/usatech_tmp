package com.usatech.networklayer;

import java.util.Vector;

import com.usatech.db.*;
import com.usatech.util.*;

import org.apache.commons.logging.*;

public class ReRixMessage implements Runnable
{
	private static Log			log						= LogFactory.getLog(ReRixMessage.class);

	public static final String	DEFAULT_MACHINE_ID		= "EV000000";
	public static final String	DEFAULT_ENCRYPTION_KEY	= "9265027739274401";
    
    public static final String  MACHINE_ID_REGEX        = "^EV(\\d){6}$";
    
    public static final String  NET_LAYER_MACHINE_ID            = "NETLAYER";
    public static final String  LOAD_BALANCER_MACHINE_ID        = "LD-BLNCR";
    public static final byte    PING_FORMAT_CODE                = 0x00;
    
    public static final int     EXTERNAL_MESSAGE                = 9999;
    public static final int     INTERNAL_REQUEST                = 0x00;
    public static final int     INTERNAL_RESPONSE               = 0x01;
    
    public static final int     INTERNAL_DECRYPTION_FAILURE     = 0x00;
    public static final int     INTERNAL_KEY_REQUEST            = 0x01;
    public static final int     INTERNAL_NET_LAYER_START        = 0x02;
    public static final int     INTERNAL_NET_LAYER_STOP         = 0x03;
    public static final int     INTERNAL_SESSION_END            = 0x04;
    public static final int     INTERNAL_HEALTH_CHECK           = 0x05;

	protected String			messageID;
	protected String			evNumber;
	protected byte[]			data;
	protected int				transmissionLength;

	protected long				sessionID;
	protected int				netLayerID;
	protected int				engineID;
    protected String            sessionKey                      = null;
	
	protected Exception			exception;

	protected boolean			isPing					= false;
    protected int               messageTypeCode         = EXTERNAL_MESSAGE;
    
	public ReRixMessage(String evNumber)
	{
		this.evNumber = evNumber;
	}

	public ReRixMessage(byte[] data, String evNumber)
	{
		this.data = data;
		this.evNumber = evNumber;
	}
    
    public ReRixMessage(byte[] data, String evNumber, boolean isPing, int transmissionLength)
    {
        this.data = data;
        this.evNumber = evNumber;
        this.isPing = isPing;
        this.transmissionLength = transmissionLength;
    }    

	public ReRixMessage(byte[] data, String evNumber, String messageID)
	{
		this.data = data;
		this.evNumber = evNumber;
		this.messageID = messageID;
	}
    
    public ReRixMessage(byte[] data, String evNumber, String messageID, int transmissionLength)
    {
        this.data = data;
        this.evNumber = evNumber;
        this.messageID = messageID;
        this.transmissionLength = transmissionLength;
    }
    
    public ReRixMessage(int messageTypeCode, byte[] data, String evNumber, String messageID)
    {
        this.messageTypeCode = messageTypeCode;
        this.data = data;
        this.evNumber = evNumber;
        this.messageID = messageID;
    } 
    
    public ReRixMessage(int messageTypeCode, byte[] data, String evNumber, String messageID, int transmissionLength)
    {
        this.messageTypeCode = messageTypeCode;
        this.data = data;
        this.evNumber = evNumber;
        this.messageID = messageID;
        this.transmissionLength = transmissionLength;
    }                 

	public boolean isPing()
	{
		return isPing;
	}
	
	public void setIsPing(boolean isPing)
	{
		this.isPing = isPing;
	}

	public void setTransmissionLength(int transmissionLength)
	{
		this.transmissionLength = transmissionLength;
	}

	public int getTransmissionLength()
	{
		return transmissionLength;
	}

	public String getEVNumber()
	{
		return evNumber;
	}

	public String getMessageID()
	{
		return messageID;
	}

	public byte[] getData()
	{
		return data;
	}
	
	public long getSessionID()
	{
		return sessionID;
	}
	
	public int getNetworkLayerID()
	{
		return netLayerID;
	}
	
	public int getEngineID()
	{
		return engineID;
	}
    
    public String getSessionKey()
    {
        return sessionKey;
    }

	public String getHexData()
	{
		return Conversions.bytesToHex(data);
	}
    
    public int getMessageTypeCode()
    {
        return messageTypeCode;
    }    

	public void setEVNumber(String evNumber)
	{
		this.evNumber = evNumber;
	}

	public void setMessageID(String messageID)
	{
		this.messageID = messageID;
	}

	public void setData(byte[] data)
	{
		this.data = data;
	}
	
	public void setSessionID(long sessionID)
	{
		this.sessionID = sessionID;
	}
	
	public void setNetLayerID(int netLayerID)
	{
		this.netLayerID = netLayerID;
	}
	
	public void setEngineID(int engineID)
	{
		this.engineID = engineID;
	}
    
    public void setSessionKey(String sessionKey)
    {
        this.sessionKey = sessionKey;
    }
    
    public void setMessageTypeCode(int messageTypeCode)
    {
        this.messageTypeCode = messageTypeCode;
    }    

	public Exception getException()
	{
		return exception;
	}

	public String toString()
	{
		if(isPing)
			return evNumber + ":" + Conversions.bytesToHex(data);
		else
			return evNumber + ":" + messageID;
	}
    
    public String toString(boolean isPing)
    {
        this.isPing = isPing;
        return toString();
    }

	public boolean equals(Object o)
	{
		if (!(o instanceof ReRixMessage))
			return false;

		ReRixMessage temp = (ReRixMessage) o;

		if (temp.getData() == null || getData() == null)
			return false;

		return Conversions.bytesToHex(getData()).equals(Conversions.bytesToHex(temp.getData()));
	}

	public void run()
	{
		String sql = "insert into engine.machine_cmd_inbound(machine_id, inbound_command, inbound_msg_no, session_id, net_layer_id, logic_engine_id) values (?,?,?,?,?,?)";
		int msgNo = SignUtils.toUnsigned(data[0]);
		String hex = Conversions.bytesToHex(data, 1, (data.length-1));
		
		StringBuilder sb = new StringBuilder();
		sb.append(sessionKey);
        sb.append(": ");
        sb.append("Inserting: ");
		sb.append(evNumber);
		sb.append(",");
		sb.append(msgNo);
		sb.append(",");
		sb.append(sessionID);
		sb.append(",");
		sb.append(netLayerID);
		sb.append(",");
		sb.append(engineID);
		sb.append(",");
		sb.append((data.length-1));
		sb.append(" bytes");

		//log.info("Inserting: " + evNumber + " " + msgNo + " " + sessionID + " " + netLayerID + " " + engineID + " "+ (data.length-1) + " bytes");
		log.info(sb.toString());

		Vector args = new Vector();
		args.addElement(evNumber);
		args.addElement(hex);
		args.addElement(new Integer(msgNo));
		args.addElement(new Long(sessionID));
		args.addElement(new Integer(netLayerID));
		args.addElement(new Integer(engineID));

		Update update = new Update(sql, args);
		if (!update.execute())
		{
			update.close();
			if(update.getException() != null)
				log.error("Insert Failed: " + evNumber + " " + msgNo + " " + (data.length-1) + " bytes: " + update.getException().getMessage(), update.getException());
			else
				log.error("Insert Failed: " + evNumber + " " + msgNo + " " + (data.length-1) + " bytes");
		}

		update.close();
	}
}
