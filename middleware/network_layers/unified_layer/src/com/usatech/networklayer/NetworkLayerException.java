package com.usatech.networklayer;

public class NetworkLayerException extends Exception
{
    public static final int     EXCEPTION_GENERIC                   = 0;
    public static final int     EXCEPTION_NO_ENCRYPTION_KEY         = 1;
    
    private int                 exceptionType                       = EXCEPTION_GENERIC;
    
	public NetworkLayerException(String message)
	{
		super(message);
	}

	public NetworkLayerException(String message, Throwable cause)
	{
		super(message, cause);
	}
    
    public NetworkLayerException(int exceptionType, String message)
    {
        super(message);
        this.exceptionType = exceptionType;
    }    
    
    public int getExceptionType()
    {
        return exceptionType;
    }    
}
