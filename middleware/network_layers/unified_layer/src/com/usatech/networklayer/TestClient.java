package com.usatech.networklayer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.lang.reflect.Constructor;
import java.net.Socket;

import com.usatech.networklayer.net.TransmissionProtocol;
import com.usatech.util.Conversions;
import com.usatech.util.Util;

public class TestClient
{
	public static void main(String args[]) throws Exception
	{
		if (args.length != 6)
		{
			System.out.println("Usage: com.usatech.networklayer.TestClient <server ip> <server port> <protocol> <ev number> <hex key> <hex data>");
			System.exit(1);
		}

        int protocolID = Integer.parseInt(args[2]);
		String machineID;
        
        if (protocolID == 2)
            machineID = new String(Conversions.hexToByteArray(args[3]));
        else
            machineID = args[3];
        
		byte[] key = Conversions.hexToByteArray(args[4]);
		String hex = args[5];
		byte[] data = Conversions.hexToByteArray(hex);

		int readTimeout = 30000;
		int writeTimeout = 30000;

		Class[] parameterTypes = { Integer.TYPE, Integer.TYPE };
		Integer[] constructorArgs = { new Integer(readTimeout), new Integer(writeTimeout) };

		String className = "com.usatech.networklayer.net.TransmissionProtocolV" + protocolID;
		TransmissionProtocol protocol = null;

		try
		{
			Class protocolClass = Class.forName(className);
			Constructor constructor = protocolClass.getConstructor(parameterTypes);
			protocol = (TransmissionProtocol) constructor.newInstance(constructorArgs);
		}
		catch (InstantiationException e)
		{
			throw new NetworkLayerException("Failed to create TransmissionProtocol " + className, e);
		}

		ReRixMessage msg = new ReRixMessage(data, machineID);

		Socket s = new Socket(args[0], Integer.parseInt(args[1]));
		s.setSoTimeout(10000);

		DataInputStream is = new DataInputStream(new BufferedInputStream(s.getInputStream()));
		DataOutputStream os = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));

		Util.output("<- " + msg.getHexData());
		protocol.transmitToServer(msg, os, key);

		os.flush();

        switch (protocolID)
        {
        case 2:
        case 4:
            break;
        default:
            int version = is.read();
        }

		while (true)
		{
			try
			{
				ReRixMessage inMsg = protocol.receiveFromServer(is, machineID, key);
				if (inMsg == null)
					break;

				Util.output("-> " + inMsg.getHexData());
			}
			catch (Exception e)
			{
				break;
			}
		}

		is.close();
		os.close();
		s.close();
	}
}
