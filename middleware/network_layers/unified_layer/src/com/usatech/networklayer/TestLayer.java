package com.usatech.networklayer;

import java.io.*;
import java.net.Socket;

import com.usatech.net.TCPServer;
import com.usatech.util.*;

public class TestLayer extends TCPServer
{
	public static void main(String args[]) throws Exception
	{
		if (args.length != 1)
		{
			System.out.println("Usage: com.usatech.networklayer.TestLayer <port>");
			System.exit(1);
		}

		int port = Integer.parseInt(args[0]);

		try
		{
			new TestLayer(port);
		}
		catch (Exception e)
		{
			System.out.println("Network Layer failed to start.  Caught exception: " + e);
			e.printStackTrace(System.out);
		}
	}

	public TestLayer(int port) throws Exception
	{
		super(port);

		Util.output("TestLayer", "Ready to accept connections on port " + port);
	}

	protected void handleConnection(Socket socket)
	{
		System.out.println();
		System.out.println("New connection from " + socket.getInetAddress());
		System.out.println("----------------------------------------------");
		System.out.println();

		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;

		try
		{
			socket.setSoTimeout(25000);

			bis = new BufferedInputStream(socket.getInputStream());
			bos = new BufferedOutputStream(socket.getOutputStream());

			while (true)
			{
				byte b = (byte) bis.read();
				if (b == -1)
					break;
				else
					System.out.print(Conversions.byteToHex(b) + " ");
			}

			System.out.println();
			System.out.println();
			System.out.println("----------------------------------------------");
			System.out.println("End of Stream");
			System.out.println();

		}
		catch (Exception e)
		{
			System.out.println();
			System.out.println();
			System.out.println("----------------------------------------------");
			System.out.println(e.toString());
			System.out.println();
			// e.printStackTrace(System.out);
		}
		finally
		{

			Util.output("TCPLayer", "Closing connection to " + socket.getInetAddress());
			if (bis != null)
			{
				try
				{
					bis.close();
				}
				catch (Exception e)
				{
				}
			}

			if (bos != null)
			{
				try
				{
					bos.close();
				}
				catch (Exception e)
				{
				}
			}

			if (socket != null)
			{
				try
				{
					socket.close();
				}
				catch (Exception e)
				{
				}
			}
		}
	}
}
