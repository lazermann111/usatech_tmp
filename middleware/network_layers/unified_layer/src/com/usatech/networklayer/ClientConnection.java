package com.usatech.networklayer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.usatech.networklayer.net.TransmissionProtocol;

public class ClientConnection
{
	protected static Log				log                     = LogFactory.getLog(ClientConnection.class);
    
    protected static final char STATUS_FAILED                   = 'U';
    protected static final char STATUS_UNKNOWN                  = 'X';
	
    protected ThreadPoolExecutor        threadPool;
	protected Socket					socket;
	protected String					remoteAddr;
	protected int						remotePort;
    protected String                    networkLayer            = "UnifiedLayer";
	protected DataInputStream		    bis;
	protected DataOutputStream		    bos;
	protected String					name;
	protected InputHandler              inputHandler;
	protected OutputHandler			    outputHandler;
	protected long					    connectionStart;
	protected TransmissionProtocol	    lastProtocol;
	protected boolean					stopFlag                = false;
	protected boolean					closed			        = false;
	protected int						socketTimeout;
	protected int						outputTimeout;
	protected long					    lastIO;
	protected NetLayerSession			session;
    protected ClientConnection          connection;
    protected String                    inBuffer                = "";
	protected byte[] encryptionKey = null;
    protected boolean                   receivedLineNoise       = false;
    protected boolean                   receivedValidPacket     = false;
    protected char                      callinStatus            = STATUS_UNKNOWN;

    protected void createHandlers() throws IOException
    {
        outputHandler = new OutputHandler();
        inputHandler =  new InputHandler();
    }
    
	public ClientConnection(ThreadPoolExecutor threadPool, Socket socket, long connectionNum, int socketTimeout, int outputTimeout)
	{
		try
		{
			connectionStart = System.currentTimeMillis();

            this.threadPool = threadPool;
			this.socket = socket;
			this.socketTimeout = socketTimeout;
			this.outputTimeout = outputTimeout;

			this.socket.setSoTimeout(this.socketTimeout);

			lastIO = System.currentTimeMillis();

			remoteAddr = socket.getInetAddress().getHostAddress();
			remotePort = socket.getPort();
			name = remoteAddr + "-" + connectionNum;

			bis = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
			bos = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
			
			session = new NetLayerSession(name, remoteAddr, remotePort);
            this.connection = this;
            
            log.info(name + ": New connection, active sessions: " + NetLayerSession.getSessionCount() + " -----------------------");
            
            createHandlers();
            threadPool.execute(outputHandler);
		}
		catch (Exception e)
		{
			handleException(e);
			close();
		}
	}

	public Socket getSocket()
	{
		return socket;
	}

	public String getRemoteAddr()
	{
		return remoteAddr;
	}

	public DataInputStream getInputStream()
	{
		return bis;
	}

	public DataOutputStream getOutputStream()
	{
		return bos;
	}

	public synchronized String getName()
	{
		return name;
	}

	public InputHandler getInputHandler()
	{
		return inputHandler;
	}

	public OutputHandler getOutputHandler()
	{
		return outputHandler;
	}

	public long getConnectionStart()
	{
		return connectionStart;
	}

	public long getET()
	{
		return (System.currentTimeMillis() - connectionStart);
	}

	public TransmissionProtocol getLastProtocol()
	{
		return lastProtocol;
	}
    
    public synchronized String getInBuffer()
    {
        return inBuffer;
    }
    
	public synchronized byte[] getEncryptionKey()
    {
        return encryptionKey;
    }    
        
    public synchronized byte[] getEncryptionKey(String evNumber)
    {
        return session.getEncryptionKey(evNumber);
    }
    
    public synchronized boolean getCachedKeyFlag(String evNumber)
    {
        return session.getCachedKeyFlag(evNumber);
    }    
    
    public synchronized boolean getReceivedLineNoise()
    {
        return receivedLineNoise;
    }
    
    public synchronized boolean getReceivedValidPacket()
    {
        return receivedValidPacket;
    }
    
    public synchronized void setSocketTimeout(int socketTimeout) throws SocketException
    {
        if (socket.getSoTimeout() != socketTimeout)
            socket.setSoTimeout(socketTimeout);
    }
    
    public synchronized void setInBuffer(String inBuffer)
    {
        this.inBuffer = inBuffer;
    }
    
	public synchronized void setEncryptionKey(byte[] encryptionKey)
    {
        this.encryptionKey = encryptionKey;
    }    
    
	public synchronized void setEncryptionKey(String evNumber, byte[] encryptionKey)
    {
        session.setEncryptionKey(evNumber, encryptionKey);
    }
    
    public synchronized void setCachedKeyFlag(String evNumber, boolean cachedKeyFlag)
    {
        session.setCachedKeyFlag(evNumber, cachedKeyFlag);
    }    
    
    public synchronized void setReceivedLineNoise(boolean receivedLineNoise)
    {
        this.receivedLineNoise = receivedLineNoise;
    }
    
    public synchronized void setReceivedValidPacket(boolean receivedValidPacket)
    {
        this.receivedValidPacket = receivedValidPacket;
    }
    
    public void inputHandlerWait()
    {       
        try
        {
            synchronized (inputHandler)
            {
                inputHandler.wait(outputTimeout);
            }
        }
        catch (InterruptedException e)
        {           
        }
    }
    
    public void enqueueMessage(ReRixMessage inMessage) throws Exception
    {
        session.putMessage(inMessage);
        
        synchronized(outputHandler)
        {
            outputHandler.notify();
        }
    }
    
    public byte getNextInternalMsgNo(String evNumber)
    {
        return session.getNextInternalMsgNo(evNumber);
    }

	protected void setStopFlag(boolean stopFlag)
	{
        this.stopFlag = stopFlag;
        
        synchronized(inputHandler)
        {
            inputHandler.notify();
        }        
        
        synchronized(outputHandler)
        {
            outputHandler.notify();
        }
	}

	public void close()
	{
		synchronized (this)
		{
			if (closed)
				return;

			closed = true;
			log.info(name + ": Closing connection (" + getET() + " ms), active sessions: " + (NetLayerSession.getSessionCount() - 1) + " ---------");

			setStopFlag(true);

			if (bos != null)
			{
				try
				{
					bos.flush();
				}
				catch (Exception ex)
				{
				}
				try
				{
					bos.close();
				}
				catch (Exception ex)
				{
				}
			}

			if (bis != null)
			{
				try
				{
					bis.close();
				}
				catch (Exception ex)
				{
				}
			}

			if (socket != null)
			{
				try
				{
					socket.close();
				}
				catch (Exception ex)
				{
				}
			}

			session.finish(networkLayer, callinStatus);
		}
	}

	protected void handleException(Exception e)
	{
		if (closed)
			return;

		if(e instanceof EOFException)
			log.warn(name + ": End Of Stream");
		else if(e instanceof SocketTimeoutException)
			log.warn(name + ": Read Timeout");
		else if(e instanceof SocketException && e.getMessage().equals("Connection reset"))
			log.warn(name + ": Connection Reset");
		else
			log.error(name + ": Caught unexpected exception: " + e.getMessage(), e);
	}

	protected class InputHandler implements Runnable
	{
		public void run()
		{
            if (log.isDebugEnabled())
                log.debug(name + ": Input thread starting...");

			try
			{
				while (!stopFlag)
				{
                    if (log.isDebugEnabled())
                        log.debug(name + ": Reading from socket...");

					int protocolID = -1;

					try
					{
						protocolID = bis.read();
					}
					catch (SocketTimeoutException ste)
					{
                        long timeSinceLastIO = System.currentTimeMillis() - lastIO;
                        if (timeSinceLastIO < socketTimeout)
                        {
							// if we have written something in less than the timeOut, don't exit read again
                            // allow extra time for the client to receive the outbound message
                            setSocketTimeout((int) (socketTimeout - timeSinceLastIO + 2000));                            
							continue;
						}
						else
						{
							throw ste;
						}
					}

					if (protocolID == -1)
						throw new EOFException("End Of Stream");

					lastIO = System.currentTimeMillis();

					TransmissionProtocol thisProtocol = TransmissionProtocol.getProtocol(protocolID);
					if (thisProtocol == null)
					{
						callinStatus = STATUS_FAILED;
						throw new NetworkLayerException("No transmission protocol defined for ID " + protocolID);
					}

					if (protocolID > 0)
						lastProtocol = thisProtocol;

                    if (log.isDebugEnabled())
                        log.debug(name + ": Attempting to process message using " + thisProtocol.getName());

					ReRixMessage inMessage = null;

					try
					{
						inMessage = thisProtocol.receiveFromClient(bis, connection);
					}
					catch (Exception e)
					{
						callinStatus = STATUS_FAILED;
						throw e;
					}

					if (inMessage == null)
					{
						callinStatus = STATUS_FAILED;
						throw new NetworkLayerException("Failed to receive a message, closing connection.");
					}

                    String evNumber = inMessage.getEVNumber();
					if (inMessage.isPing())
					{
                        if (evNumber.equals(ReRixMessage.LOAD_BALANCER_MACHINE_ID))
                        {
                            log.info(name + ": Health check: " + inMessage.toString());
                            byte[] msgData = {getNextInternalMsgNo(ReRixMessage.NET_LAYER_MACHINE_ID), ReRixMessage.INTERNAL_REQUEST, ReRixMessage.INTERNAL_HEALTH_CHECK, inMessage.getData()[0]};                          
                            enqueueMessage(new ReRixMessage(ReRixMessage.INTERNAL_HEALTH_CHECK, msgData, ReRixMessage.NET_LAYER_MACHINE_ID, Integer.toString(msgData[0]), 4));
                        }
                        else
                        {
                            ReRixMessage pingResponse = new ReRixMessage(inMessage.getData(), evNumber, inMessage.getMessageID());                            
                            log.info(name + ": PING " + inMessage.toString(true));
    
                            try
                            {
                                thisProtocol.transmitToClient(pingResponse, bos, null);
                                bos.flush();
                            }
                            catch (Exception e)
                            {
                                callinStatus = STATUS_FAILED;
                                throw e;
                            }
                        }
					}
					else
					{
						log.info(name + ": " + thisProtocol.getName() + " rcvd " + inMessage.toString() + " " + inMessage.getTransmissionLength() + " bytes");
                        if (networkLayer.contains(".") == false)
                            networkLayer += "." + thisProtocol.getID();
                        enqueueMessage(inMessage);
					}
                    
                    setSocketTimeout(socketTimeout);
				}
			}
			catch (Exception e)
			{
				handleException(e);
			}
			finally
			{
				close();
                if (log.isDebugEnabled())
                    log.debug(name + ": Input thread exiting...");
			}
		}
	}

	protected class OutputHandler implements Runnable
	{
		protected int	responseCount	= 0;
		protected long	startTime		= System.currentTimeMillis();

		protected void writeResponse(ReRixMessage outboundMessage) throws Exception
		{
			try
			{
				lastProtocol.transmitToClient(outboundMessage, bos, connection);
				bos.flush();
			}
			catch (Exception e)
			{
                callinStatus = STATUS_FAILED;
				throw e;
			}

			lastIO = System.currentTimeMillis();
			responseCount++;

			log.info(name + ": " + lastProtocol.getName() + " sent " + outboundMessage.toString() + " " + outboundMessage.getTransmissionLength() + " bytes");
		}

		public void run()
		{
            if (log.isDebugEnabled())
                log.debug(name + ": Output thread starting...");
            
			try
			{
				synchronized (this)
				{
                    threadPool.execute(inputHandler);
					this.wait();
				}
			}
			catch (InterruptedException e)
			{
			}


			try
			{
				while (!stopFlag)
				{
					//log.debug(name + ": Polling for outbound messages (" + ((outputTimeout - (System.currentTimeMillis() - lastIO)) / 1000) + ") ...");
					
					List<ReRixMessage> messages = session.getMessages();
					//log.debug(name + ": Session has " + messages.size() + " waiting outbound message(s)...");

					if (!messages.isEmpty())
					{
                        boolean closeConnectionFlag = false;
                        for(ReRixMessage message : messages)
                        {
                            int msgType = message.getMessageTypeCode();
                            if (msgType == ReRixMessage.INTERNAL_KEY_REQUEST)
                            {
                                String msgData = new String(message.getData());
                                if (msgData.length() > 3)
									setEncryptionKey(message.getEVNumber(), msgData.substring(3).getBytes());
                                synchronized(inputHandler)
                                {
                                    inputHandler.notify();
                                }
                            }
                            else if (msgType == ReRixMessage.INTERNAL_HEALTH_CHECK)
                            {                   
                                String checkResult;
                                byte[] appLayerResponse = message.getData();                                                           
                                bos.write(ReRixMessage.PING_FORMAT_CODE);
                                bos.write(ReRixMessage.LOAD_BALANCER_MACHINE_ID.getBytes(), 0, ReRixMessage.LOAD_BALANCER_MACHINE_ID.length());
                                if (appLayerResponse.length > 3)
                                {
                                    bos.write(appLayerResponse, 3, appLayerResponse.length - 3);
                                    checkResult = "succeeded";
                                }
                                else
                                    checkResult = "failed";
                                bos.flush();
                                log.info(name + ": Health check " + checkResult);
                                closeConnectionFlag = true;
                                break;
                            }
                            else if (msgType == ReRixMessage.INTERNAL_DECRYPTION_FAILURE)
                            {                               
                                log.warn(name + ": Device " + message.getEVNumber() + " is NOT on END bug list, closing connection...");
                                callinStatus = STATUS_FAILED;
                                closeConnectionFlag = true;
                                break;
                            }
                            else
                                writeResponse(message);
                        }
                        if (closeConnectionFlag == true)
                            break;
					}

					if ((System.currentTimeMillis() - lastIO) < outputTimeout)
					{
						try
						{
							Thread.sleep(200);
						}
						catch (Exception e)
						{
						}
					}
					else
					{
						if (!stopFlag)
						{
                            if (log.isDebugEnabled())
                                log.debug(name + ": Waiting for more inbound messages...");
                            
							try
							{
								synchronized (this)
								{
									this.wait();
								}
							}
							catch (InterruptedException e)
							{
							}
						}
					}
				}
			}
			catch (Exception e)
			{
				handleException(e);
			}
			finally
			{
				close();
                if (log.isDebugEnabled())
                    log.debug(name + ": Output thread exiting...");
			}
		}
	}
}
