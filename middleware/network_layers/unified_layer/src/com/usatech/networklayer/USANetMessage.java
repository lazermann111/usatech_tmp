package com.usatech.networklayer;

public class USANetMessage extends ReRixMessage 
{    
    public static final int     MESSAGE_TYPE_RERIX          = 0;
    public static final int     MESSAGE_TYPE_HANGUP         = 1;
    public static final int     MESSAGE_TYPE_LINE_NOISE     = 2;
    public static final int     MESSAGE_TYPE_END_BUG_FIX    = 3;
    public static final int     MESSAGE_TYPE_LD_PING        = 4;
    
    public static final String  LOAD_BALANCER_PING_REGEX    = "^(\\x00)(LD-BLNCR)(.)$";
    
    private int                 messageType                 = MESSAGE_TYPE_RERIX;
    private String              encryptionKey               = null;
    private String              inBuffer                    = null;
    private boolean             receivedLineNoise           = false;
    
    public USANetMessage(String evNumber)
    {
        super(evNumber);
    }

    public USANetMessage(byte[] data, String evNumber)
    {
        super(data, evNumber);
    }

    public USANetMessage(byte[] data, String evNumber, String messageID)
    {
        super(data, evNumber, messageID);
    }
    
    public USANetMessage(int messageTypeCode, byte[] data, String evNumber, String messageID)
    {
        super(messageTypeCode, data, evNumber, messageID);
    }   
    
    public USANetMessage(int messageTypeCode, byte[] data, String evNumber, String messageID, int transmissionLength)
    {
        super(messageTypeCode, data, evNumber, messageID, transmissionLength);
    } 
    
    public USANetMessage(int messageType)
    {
        super(null);
        this.messageType = messageType;
    }
        
    public USANetMessage(int messageType, String messageID)
    {
        super(null);
        this.messageType = messageType;
        this.messageID = messageID;
    }
    
    public USANetMessage(int messageType, String inBuffer, String messageID)
    {
        super(null);
        this.messageType = messageType;
        this.inBuffer = inBuffer;
        this.messageID = messageID;
    }
    
    public USANetMessage(byte[] data, String evNumber, String encryptionKey, String messageID)
    {
        super(data, evNumber, messageID);
        this.encryptionKey = encryptionKey;
    }
    
    public USANetMessage(byte[] data, String evNumber, String messageID, int transmissionLength)
    {
        super(data, evNumber, messageID, transmissionLength);
    }     
    
    public int getMessageType()
    {
        return messageType;
    }
    
    public String getEncryptionKey()
    {
        return encryptionKey;
    }
    
    public String getInBuffer()
    {
        return inBuffer;
    }
    
    public boolean getReceivedLineNoise()
    {
        return receivedLineNoise;
    }
    
    public void setMessageType(int messageType)
    {
        this.messageType = messageType;
    }
    
    public void setEncryptionKey(String encryptionKey)
    {
        this.encryptionKey = encryptionKey;
    }
    
    public void setInBuffer(String inBuffer)
    {
        this.inBuffer = inBuffer;
    }
    
    public void setReceivedLineNoise(boolean receivedLineNoise)
    {
        this.receivedLineNoise = receivedLineNoise;
    }
}
