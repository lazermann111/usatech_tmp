#!/usr/bin/perl -w

use strict;
use Net::SMTP;
use Time::HiRes qw(gettimeofday);

my $net_layer_log = '/opt/Layer3/layer3.log';
my $end_bug_list_threshold = 5;
my $smtp_server = 'mailhost';
my $email_from = 'rerixengine@usatech.com';
my $email_to = 'dkouznetsov@usatech.com';
#my $email_to = 'pcowan@usatech.com,dkouznetsov@usatech.com';
#my $email_to = 'pcowan@usatech.com,dkouznetsov@usatech.com,jlouvet@usatech.com';

process_decrypt_failures();


sub process_decrypt_failures
{
	my @epoch_sec = gettimeofday();
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($epoch_sec[0]);
	my $timestamp = sprintf("%02d-%02d-%02d", ($mon + 1), $mday, substr($year + 1900, -2));

	my $data = `tail -1000000 $net_layer_log | egrep "$timestamp.*10.24.77.150.*NOT on END bug list"`;
	chomp($data);

	my %bag;
	my @lines = split /\n/, $data;
	return if @lines == 0;

	foreach my $line (@lines)
	{
		my $ev = substr(substr($line, -31), 0, 8);
		my $count = $bag{$ev};
		if(defined $count)
		{
			$bag{$ev} = $count + 1;
		}
		else
		{
			$bag{$ev} = 1;
		}
	}

	my ($output, $report_output);
	foreach my $ev (sort {$bag{$b} <=> $bag{$a}} keys %bag)
	{
		my $count = $bag{$ev};
	
		if($count > $end_bug_list_threshold)
		{
			$output .= "$ev\n";
		}
		
		$report_output .= "$ev     $count\n";
	}

	my $hostname = `hostname`;
	chomp($hostname);
	my $msg_subject = "Layer3 Decryption Failures ($hostname $timestamp)";
	my $msg_txt = "On $timestamp the following devices transmitted transactions to $hostname that failed decryption. This usually indicates an END bug problem.\n\nEV Number   Count\n\n$report_output\n\n";

	if($output)
	{
		print $output;
		$msg_txt .= "The following EV Numbers were automatically added to the FIX list:\n\n$output\n\n";
	}

	send_email_detailed($email_from, $email_from, $email_to, $email_to, $msg_subject, $msg_txt);
}


sub send_email_detailed
{
	my ($from_addr, $from_name, $to_addrs, $to_names, $subject, $content) = @_;
	my @to_addr_array = split(/,/, $to_addrs);
	my @to_name_array = split(/,/, $to_names);
	
	# check that every address has a name
	if($#to_addr_array ne $#to_name_array)
	{
		warn "send_email_detailed: Length of address array not equal to length of name array!\n";
		return 0;
	}

	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_addr_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 

	my $to_line;
	for(my $index = 0; $index <= $#to_addr_array; $index++)
	{
		$to_line = $to_line . "\"$to_name_array[$index]\" <$to_addr_array[$index]>";
		if($index < $#to_addr_array)
		{
			$to_line = $to_line . ", ";
		}
	}
	
	$smtp->datasend("To: $to_line\n");
	$smtp->datasend("From: $from_name <$from_addr>\n");
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("Content-Type: text/plain\n");
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();
	
	return 1;
}
