#!/bin/perl -w

use strict;
use IO::Socket;
use IO::Select;
use Shared;

# all these can be access outside of this module by ThisModuleName:: operator
use Evend::Database::Database;        # Access to the Database

sub logit($);
sub alive_alert;

my $DATABASE = Evend::Database::Database->new(
							print_query			=> 1, 
							execute_flatfile	=> 1, 
							debug				=> 1, 
							debug_die			=> 0);

alive_alert();
exit;

sub alive_alert()
{
	logit ("Performing alive_alert()");
	my $request;
	my $key = 'E4111111';
	my $msgnbr = pack("H*","03");
	my $type = pack("H*","5c");
	my $timestamp = time;
	my $response;
	my $responseBuffer;

	logit ("msgnbr (asc)       = '$msgnbr'");
	logit ("msgnbr (hex)       = " . unpack("h*",$msgnbr) );
	logit ("type               = $type");

	$request = pack("aa", $msgnbr, $type);
	logit ("request (asc) = '$request'");
	logit ("request (hex) = '" . unpack("H*", $request));

	my $ptechSocket = new IO::Socket::INET ( PeerAddr => '10.0.0.63:14105' ); 
	my $ptechSelect = new IO::Select();

	if( ! defined $ptechSocket )
	{
	  # failed to connect to server
	  logit ( "Failed to connect to server");
	  exit;
	}

	$ptechSelect->add($ptechSocket);
	
	logit "key          = '" . $key . "'";
	$request = PackEncrypted ($request, unpack("H*",$key), $DATABASE);
	logit "The encrypted data str : '" . $request . "'";
	logit "The encrypted data hex : '" . unpack("H*",$request) . "'";
	$request = $key . $request;
	$request = uuencode($request) . "\x0a";
	logit "The uuencoded data str : '" . $request . "'";
	logit "The uuencoded data hex : '" . unpack("H*",$request) . "'";

	if( $ptechSocket->send( $request ) != length($request) )
	{
	  # failed to send request
	  logit ("Failed to send request");
	  $ptechSocket->close();
	  exit;
	}
	$ptechSocket->recv($responseBuffer, 500);
	
	$response .= $responseBuffer;

	logit ("\n*********** SERVER RESPONSE ***************");
	logit ('The response str = ' . $response);
	logit ('The response hex = ' . unpack("H*",$response));
	
	$response = uudecode($response);
	logit ('The uudecoded response str = ' . $response);
	logit ('The uudecoded response hex = ' . unpack("H*",$response));
	
	my $reply = UnpackEncrypted($response, unpack("H*",$key), $DATABASE);
	my $crc = $reply->[0];
	my $out = $reply->[1];
	logit ('The crc check                = ' . $crc);
	logit ("key          				 = '" . $key . "'");
	logit ("The unencrypted response str = '" . $out . "'");
	logit ('The unencrypted response hex = ' . unpack("H*",$out));
	
	$ptechSocket->close();	
}

sub logit($)
{
	my $line = shift;

	print "[" . localtime() . "] $line\n";
}

sub uuencode
{
   my @byte = split('', shift);

   my $len = scalar @byte;

   for(my $i = 0; $i < $len; $i++)
   {
      $byte[$i] = ord($byte[$i]);
   }

   my ($final, $ptr);
   for(my $i = 0; $i < $len; $i += 3)
   {
      $final .= sprintf("%c", 32 + (($byte[$i] >> 2) & 0x3f));
      if( defined $byte[$i+1] )
      {
         $final .= sprintf("%c", 32 + ((($byte[$i]<<4) |
										((($byte[$i+1]>>4)&0xf))) & 0x3f));
         if( defined $byte[$i+2] )
         {
            $final .= sprintf("%c", 32 + (((($byte[$i+1]<<2)) |
											((($byte[$i+2]>>6)&0x3))) &0x3f));
            $final .= sprintf("%c", 32 + (($byte[$i+2]) & 0x3f));
         }
         else
         {
            $final .= sprintf("%c", 32 + (($byte[$i+1]<<2) & 0x3f));
         }
      }
      else
      {
         $final .= sprintf("%c", 32 + (($byte[$i]<<4) & 0x3f));
      }
   }

   return $final;
}

sub uudecode
{
   my @byte = split //, $_[0];

   #my $header = shift @byte;
   #my $footer = pop @byte;

   my $val;
   my $final;
   my $mod;

   for(my $i = 0; $i < @byte; $i++)
   {
      $mod = $i % 4;
      my $info = ord($byte[$i]) - 32;

      if ($mod == 0) {
         $val = ($info & 0x3f) << 2;
      }
      elsif ($mod == 1) {
         $val |= ($info >> 4) & 0x03;
         $final .= sprintf("%c", $val);
         $val  = ($info & 0x0f) << 4;
      }
      elsif ($mod == 2) {
         $val |= ($info >> 2) & 0x0f;
         $final .= sprintf("%c", $val);
         $val  = ($info & 0x03) << 6;
      }
      elsif ($mod == 3) {
         $val |= ($info & 0x3f);
         $final .= sprintf("%c", $val);
      }
   }

   #return $final . $footer;
   return $final;
}









