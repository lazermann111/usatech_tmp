#!/sbin/sh

DATE=`date`
echo "inittab restarting USATestComm on: $DATE" >>/opt/USAdev/USATestComm.err
sleep 10
/opt/USAdev/USATestComm.pl >>/opt/USAdev/USATestComm.log 2>>/opt/USAdev/USATestComm.err
