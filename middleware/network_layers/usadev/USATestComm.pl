#!/usr/local/bin/perl -w

use strict;

require Net::Daemon;

package USATestServer;

use IO::Socket;

use lib '/opt/USAdev';

use USATestComm;

use vars qw (@ISA);
@ISA = qw(Net::Daemon); # inherit from Net::Daemon

sub Run ($) 
{
	my($self) = @_;

	my $sock = $self->{'socket'};

	my $ssn = '';

	print "Got connection at " . localtime() . "\n";

	USATestComm::Run($sock);
}
  
package main;

my $server = USATestServer->new({'pidfile' => 'none','localport' => 14107},"");

$server->Bind();

