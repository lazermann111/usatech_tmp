#!/bin/perl -w
#---------------------------------------------------------------------------------------------
# Change History
#
# Version       Date            Programmer      Description
# -------       ----------      ----------      ----------------------------------------------
# 1.00          02/06/2003      T Shannon       First Version
#						

package USANet2DB;

use strict;

use Shared;

use Evend::Database::Database;        # Access to the Database

use IO::Socket;
use IO::Select;

sub getPacket ($);

sub logit($);

my $bool = 0;

sub Run ($)
{
	my $sock = shift;
	my $msg;

	$| = 1;

	$msg = "Connected from " . $sock->peerhost . ":" . $sock->peerport;
	logit($msg);

	my $TIMEOUT = 60*3;

	logit ("Attempting to create handle...");
	my $DATABASE = Evend::Database::Database->new(
							print_query			=> 1, 
							execute_flatfile	=> 1, 
							debug				=> 1, 
							debug_die			=> 0);
	logit ("Created database handle...");

	my $machine_id;
	my $network = 'U';
	my $ssn;
	my $key;
	my $unpackedkey;
	my $Initkey;

	my $rh_set;
	my $wh_set;

	my $rh;
	my $wh;

	my $rerix;
	my $msg_no;

	my $read_set = new IO::Select();
	my $write_set = new IO::Select();
	$read_set->add($sock);

	my $next_command_check = time() + 5;
	my $last_command_resend;

	my $outBuffer;
	my $inBuffer;

	my $outCommand;

	my $outCommandID;

	my $watchdog = time;	
	
	my $whilecnt;

	while(1)
	{
		my $select_t;

		if( defined $outCommandID )
		{
			$select_t = ($last_command_resend - time()) > 0 ?
								$last_command_resend - time() : 0;
		}
		else
		{
			$select_t = ($next_command_check - time()) > 0 ?
								$next_command_check - time() : 0;
		}

		#logit ("Selecting $select_t\n");

		($rh_set, $wh_set) =
						IO::Select->select($read_set, $write_set,
											undef, $select_t);

		if( $watchdog + $TIMEOUT < time )
		{
			$sock->close();

			logit ("Timeout " . localtime() );

			return;
		}

		foreach $rh (@$rh_set)
		{
			my $tempBuffer;

			$watchdog = time;

			$rh->recv($tempBuffer, 512);

			if( length( $tempBuffer ) == 0 )
			{
				$sock->close();

				logit ("Connection Lost " . localtime());

				return;
			}
			else
			{
				$inBuffer .= $tempBuffer;

				my $packet;
				my $result;
				my $loopcnt = 0;
				
				logit ("inBuffer: '" . $inBuffer . "'");
				logit ("inBuffer hex: '" . unpack('H*', $inBuffer) . "'");
				logit ("Got data... resetting inactivity timer to zero.");
				$whilecnt = 0;

				# 64-bit decodes the inbound the packet
				($packet, $inBuffer) = getPacket($inBuffer);
				logit ("decoded inBuffer hex: '" . unpack('H*', $packet) . "'");
				logit ("chomped inBuffer str: '$packet'");

				if($inBuffer =~ s/^\++\++\++//)
				{
					logit "Received +++ escape sequence... ending connection";
					return;
				}

				# the first 8 bytes of the uudecoded packet contains the SSN; use that to decrypt the packet
				$key = uc(unpack("H*",substr($packet, 0, 8)));
				logit ("decryption key = $key");
								
				# get the machine id 
				my $array_ref = $DATABASE->select(
						table			=> 'Rerix_modem_to_serial',
						select_columns	=> 'modem_id, machine_id',
						where_columns	=> ['network=?',
											'upper(modem_id) = ?'],
						where_values	=> [$network,$key]);

				if( defined $array_ref->[0][1] )
				{
					$machine_id = $array_ref->[0][1];
				}
				else
				{
					print "New machine, fetching new Machine ID\n";

					$array_ref = $DATABASE->select(
							table			=> 'dual',
							select_columns	=> 'rerix_machine_id_v2');

					$machine_id = $array_ref->[0][0];

					# first, create the unpacked modem id (ssn)
					#$unpackedkey = unpack("H*", $key);

					# next, connect the modem to the machine number
					$DATABASE->insert(
							table			=> 'Rerix_modem_to_serial',
							insert_columns	=> 'modem_id, machine_id, '.
												'network, ' .
												'MACHINE_INDEX',
							#insert_values	=> [uc($unpackedkey), $machine_id,
							insert_values	=> [$key, $machine_id,
												$network, 0]);

					$Initkey = $key;
					logit "USANet2DB.pm - using Initkey : $Initkey";
				}
						
				logit ("SSN# (asc)   '" . $key . "', Machine ID# " . $machine_id);
				#logit ("SSN# (hex) 	 '" . unpack("H*",$key) . "', Machine ID# " . $machine_id);
				
				# if there are any existing command replies in the outbound queue for 
				# this client delete them the database before continuing
				my ($err_nbr, $err_msg);
				my $raw_handle = $DATABASE->{handle};
				my $csr = $raw_handle->prepare(q{BEGIN
				    				sp_flush_outbound_cmd_queue(?, ?, ?);
				                    		END;
				            			});
				$csr->bind_param(1, $array_ref->[0][1]);
				$csr->bind_param(2, $err_nbr);
				$csr->bind_param(3, $err_msg);	
				$csr->execute;
				
				print "Results from deletion of outbound queue:\n";
				print "\terr_nbr	: $err_nbr\n";
				print "\terr_msg	: $err_msg\n";

				
				# decrypt the packet
				logit ("about to unpack the data...");
				logit ("packet = '" . substr($packet,8) . "'");
				logit ("key    = $key");
				#($result, $packet) = @{UnpackEncrypted(substr($packet,8), unpack("H*",$key), $DATABASE, $Initkey)};
				($result, $packet) = @{UnpackEncrypted(substr($packet,8), $key, $DATABASE, $Initkey)};
				logit ("result = $result");
				logit ("packet = $packet");				

				if ($result == 0)
				{
					logit "Decryption failure for SSN " . $key;
				}
				
				#chop $packet;
				
				while( length($packet) )
				{
					my $msgnbr = substr($packet, 0, 1);
					logit "*** New Command Received ***\n";
					logit "The packet str is       : '" . $packet . "'\n";
					logit "The packet hex is       : '" . unpack("H*", $packet) . "'\n";
					logit "The msgnbr byte hex is  : '" . unpack("H*", substr($packet, 0, 1)) . "'";
					logit "The command byte hex is : '" . unpack("H*", substr($packet, 1, 1)) . "'";
					logit "The inBuffer str is     : '" . $inBuffer;
					logit "The inBuffer hex is     : '" . unpack("H*", $inBuffer) . "'\n";
					logit "The inBuffer length is  : " . length($inBuffer);
					
					$rerix = unpack("H*", substr($packet,1));
					$msg_no = ord(substr($packet,0,1));
					if( defined $machine_id )
					{
						logit ("About to insert command into database...");
						$DATABASE->insert(
							table			=>'machine_command_inbound',
							insert_columns	=> 'machine_id, ' .
												'inbound_command,' .
												'inbound_msg_no',
							insert_values	=> [$machine_id,
												$rerix,
												$msg_no]);
						logit ("...insert complete");

					}
					else
					{
						logit "ReRix sent without identifying, closing";
						return;
					}
				
					# 64-bit decodes the inbound the packet
					if (length($inBuffer))
					{
						#logit ("More data in the inBuffer : $inBuffer");
						# the first 8 bytes of the uudecoded packet contains the SSN; use that to decrypt the packet
						#($packet, $inBuffer) = getPacket($inBuffer);
						#$key = substr($packet, 0, 8); T. Shannon 7/24/2003

						# decrypt the packet
						#($result, $packet) = @{UnpackEncrypted(	$packet, unpack("H*",$key), $DATABASE )};
						#($result, $packet) = @{UnpackEncrypted($packet, $key)};
		
						#if ($result == 0)
						#{
						#	logit "Encryption failure for SSN " . $key;
						#}
					#}
					#else
					#{
						$packet = '';
					}
					
					$packet = '';
					
					if ($loopcnt++ > 2)
					{
						logit ("Exceeded loopcnt... exiting loop");
						last;
					}
				} # while ( defined $packet )
			}
			$next_command_check = 0;
		}

		foreach $wh (@$wh_set)
		{
			#$outBuffer = PackEncrypted($outBuffer);

			logit ("Sending reply to client (ASCII): '" . $outBuffer . "'");
			logit ("Sending reply to client (hex): '" . unpack("H*", $outBuffer) . "'");
			#my $sent = $wh->send(uuencode($outBuffer));
			my $sent = $wh->send($outBuffer);

			$watchdog = time;

			logit ("Sent: " . substr($outBuffer, 0, $sent));
			$outBuffer = substr $outBuffer, $sent;


			# remove it from the select()
			if( length( $outBuffer ) == 0 )
			{
				$write_set->remove( $wh );
			}
		}

		if( ($next_command_check <= time()) and (!defined $outCommandID) and
			(defined $machine_id) )
		{
			$next_command_check = time() + 1;

			$DATABASE->{print_query} = 1;
			logit "Checking machine_command for $machine_id";

			my $commandref = $DATABASE->select(
									table			=> 'Machine_Command',
									select_columns	=> 'COMMAND_ID, MODEM_ID,'.
														'COMMAND, Execute_Cd',
									order			=> 'Command_ID',
									where_columns	=> ['(Execute_cd = ? or ' .
														'Execute_cd = ?)',
														'modem_id = ?'],
									where_values	=> ['N',
														"H$network",
														$machine_id]
								);

			$DATABASE->{print_query} = 1;

			if( defined $commandref->[0] )
			{
				my ($command_id, $modem_id,
					$command, $execute_cd) = @{$commandref->[0]};

				if( $execute_cd eq 'N' )
				{
					logit  "Sending $command_id: $command";
					logit  "Sending $command_id: " . unpack("H*", $command);

					$outCommand = pack('H*' ,$command);

					#$outCommand = PackEncrypted($outCommand, unpack("H*",$key), $DATABASE, $Initkey);
					$outCommand = PackEncrypted($outCommand, $key, $DATABASE, $Initkey);
					logit "The unpacked encrypted data: " . unpack("H*", $outCommand);
					
					$outCommand = uuencode($outCommand) . "\x0a";
					logit "The unpacked encoded data  : " . unpack("H*", $outCommand);

					$outBuffer .= $outCommand;

					$write_set->add( $sock );

					$last_command_resend = time() + 5;

					$DATABASE->update(
									table			=> 'Machine_Command',
									update_columns	=> 'Execute_Cd',
									update_values	=> ['Y'],
									where_columns	=> ['Command_ID = ?'],
									where_values	=> [$command_id]
								);
				}
				elsif( $execute_cd eq "H$network" )
				{
					#my $array_ref = $DATABASE->select(
					#				table			=> 'Rerix_modem_to_serial',
					#				select_columns	=> 'modem_id, machine_id',
					#				where_columns	=> ['network=?',
					#									'modem_id = ?'],
					#				where_values	=> [$network, $unpackedkey]
					#			);

					logit "Changed networks or key... Retrieving key from Rerix_modem_to_serial.";
					my $array_ref = $DATABASE->select(
									table			=> 'Rerix_modem_to_serial',
									select_columns	=> 'modem_id, machine_id',
									where_columns	=> ['network=?',
														'modem_id = ?'],
									where_values	=> [$network, $key]
								);

					if( defined $array_ref->[0] )
					{
						$machine_id = $array_ref->[0][1];
					}
					else
					{
						logit "Confused, no EV# for this connection, closing";
						return;
					}
					logit "HHHHHHHHHHHH Network";
					$DATABASE->update(
									table			=> 'Machine_Command',
									update_columns	=> 'Execute_Cd',
									update_values	=> ['Y'],
									where_columns	=> ['Command_ID = ?'],
									where_values	=> [$command_id]
								);
				}
			}
		}
		elsif( $next_command_check <= time() )
		{
			$next_command_check = time() + 1;
		}
		
		if ($whilecnt++ > 100)
		{
			logit ("Exceeded inactivity timeout... closing the session.");
			return;
		}
	}
		
}



sub getPacket ($)
{
	my $inBuffer = shift;
	logit "getPacket() inBuffer = '" . $inBuffer . "'";

	my $packet;
	my $byte;

	while( length($inBuffer) )
	{
		$byte = substr $inBuffer, 0, 1;
		$inBuffer = substr $inBuffer, 1;

		$packet = $byte;

		while( length($inBuffer) )
		{
			$byte = substr $inBuffer, 0, 1;
			$inBuffer = substr $inBuffer, 1;

			if( $byte eq "\x0a" )
			{
				$packet .= $byte;

				# the following removes the CR char that the G4 produces
				if ((length($inBuffer) > 1))
				{
					if (substr($inBuffer,0,1) eq "\x0d")
					{
						$inBuffer = substr $inBuffer, 1;
						logit "Found hex 0x0d in inBuffer";
					}
				}
				logit "getPacket() - The inBuffer : $inBuffer";
				return (uudecode($packet), $inBuffer);
			}
			else
			{
				$packet .= $byte;
			}
		}

		# the packet is incomplete
		return (undef, $packet);
	}

	return( undef, '');
}

sub check ($)
{
	my $packet = shift;

	return(undef) if !defined $packet;

	my $crc = ord(substr($packet,1,1))<<8 | ord(substr($packet,2,1));

	my $datablock = substr(substr($packet, 3), 0, -1);

	my $calcrc = getcrc16i($datablock);

	logit ( sprintf('Got Checksum: 0x%4.4X Computed: 0x%4.4X' ."\n", $crc, $calcrc));

	if( ($crc == $calcrc) or ($crc == 0) )
	{
		return($packet);
	}
	else
	{
		logit "Bad CRC\n";
		return(undef);
	}
}

sub uuencode
{
   my @byte = split('', shift);

   my $len = scalar @byte;

   for(my $i = 0; $i < $len; $i++)
   {
      $byte[$i] = ord($byte[$i]);
   }

   my ($final, $ptr);
   for(my $i = 0; $i < $len; $i += 3)
   {
      $final .= sprintf("%c", 32 + (($byte[$i] >> 2) & 0x3f));
      if( defined $byte[$i+1] )
      {
         $final .= sprintf("%c", 32 + ((($byte[$i]<<4) |
										((($byte[$i+1]>>4)&0xf))) & 0x3f));
         if( defined $byte[$i+2] )
         {
            $final .= sprintf("%c", 32 + (((($byte[$i+1]<<2)) |
											((($byte[$i+2]>>6)&0x3))) &0x3f));
            $final .= sprintf("%c", 32 + (($byte[$i+2]) & 0x3f));
         }
         else
         {
            $final .= sprintf("%c", 32 + (($byte[$i+1]<<2) & 0x3f));
         }
      }
      else
      {
         $final .= sprintf("%c", 32 + (($byte[$i]<<4) & 0x3f));
      }
   }

   return $final;
}

sub uudecode
{
   my @byte = split //, $_[0];

   my $footer = pop @byte;

   my $val;
   my $final;
   my $mod;

   for(my $i = 0; $i < @byte; $i++)
   {
      $mod = $i % 4;
      my $info = ord($byte[$i]) - 32;

      if ($mod == 0) {
         $val = ($info & 0x3f) << 2;
      }
      elsif ($mod == 1) {
         $val |= ($info >> 4) & 0x03;
         $final .= sprintf("%c", $val);
         $val  = ($info & 0x0f) << 4;
      }
      elsif ($mod == 2) {
         $val |= ($info >> 2) & 0x0f;
         $final .= sprintf("%c", $val);
         $val  = ($info & 0x03) << 6;
      }
      elsif ($mod == 3) {
         $val |= ($info & 0x3f);
         $final .= sprintf("%c", $val);
      }
   }

   return $final . $footer;
}


sub logit($)
{
	my $line = shift;

	print "[" . localtime() . "] $line\n";
}
1;
