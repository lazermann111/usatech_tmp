#!/sbin/sh

DATE=`date`
echo "inittab restarting USANet2 on: $DATE" >>/opt/USAdev/run/syslog.log
sleep 2
/opt/USAdev/run/USANet2.pl >>/opt/USAdev/run/stdout.log 2>>/opt/USAdev/run/stderr.err
