#!/usr/local/bin/perl -w

use strict;

require Net::Daemon;

package USANet2Layer;

use IO::Socket;

use lib '/opt/USAdev/run/';

use USANet2DB;

use vars qw (@ISA);
@ISA = qw(Net::Daemon); # inherit from Net::Daemon

sub Run ($) 
{
	my($self) = @_;

	my $sock = $self->{'socket'};

	my $ssn = '';

	print "Got connection at " . localtime() . "\n";

	USANet2DB::Run($sock);
}
  
package main;

my $server = USANet2Layer->new({'pidfile' => 'none','localport' => 14105},"");

$server->Bind();

