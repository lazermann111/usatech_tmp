#!/usr/bin/perl -w

package Shared;   # Define the best name here

# These are packages that are needed by some of the shared functions
use Evend::Misc::crc;          # Needed for getcrc16s
use Evend::Crypt::TEA;         # Needed for PackEncrypted
use Evend::Database::Database; # Needed for Database calls

# This allows the functions defined here to be placed into the other
# modules symbol tables, to prevent having to fully qualify every call
require Exporter;

# This is needed for the Exporter to work
@ISA    = qw(Exporter);
@EXPORT =qw(MakeHex MakeString getcrc16s getcrc16i PackEncrypted UnpackEncrypted); 

# Be anal about syntax
use strict;

sub logit;

# GetCounter exists in both packserver and SCRmod, but the functions
# are just two different distinct functions with the same name and 
# similar but not identical tasks.

########################################################
# MakeHex($message)
########################################################
# Description:  Convert a stream of Chars into 
#               Hex values
#
# Inputs:       A character string
#
# Outputs:      A string of Hex digits
#
# History:
# Date      Author   Changes Made
# ------------------------------------------
# 8/29/2000 wcampbel Moved to Shared.pm
#                    Compliance with coding standards
########################################################
# MakeHex and MakeString are used in all three
# components.  No dependencies on other functions
# Opposite of MakeString
########################################################
sub MakeHex
{
   my ($message) = @_;
   my @chars = unpack  'H*', $message;
   return join '', @chars;
}

########################################################
# MakeString($message)
########################################################
# Description:  Convert a stream of Hex values into a 
#               Char stream
#
# Inputs:       A string of Hex digits
# 
# Outputs:      A character string 
#
# History:
# Date      Author   Changes Made
# -------------------------------------------
# 8/29/2000 wcampbel Moved to Shared.pm
#                    Compliance with coding standards
########################################################
# MakeHex and MakeString are used in all three
# components.  No dependencies on other functions
# Opposite of MakeHex
########################################################
sub MakeString
{
   my ($message) = @_;
   return pack 'H*', $message;
}

########################################################
# getcrc16s($message)
########################################################
# Description:  Obtains the CRC16 of the message 
#
# Inputs:       A byte stream
#
# Outputs:      The CRC16 value of the message
#               as a string of 2 characters
# 
# History:
# Date      Author   Changes Made
# --------------------------------------------
# 8/19/2000 wcampbel Moved to Shared.pm
#                    Compliance with coding standards
########################################################
# getcrc16s is used in SCRMod and ReRix
# Depends on Evend::Misc::crc
########################################################
sub getcrc16s
{
   my ($message)=@_;
   return pack "C*", Evend::Misc::crc::CalcCrc($message, length $message);
}


####################################################################
# getcrc16i($message)
####################################################################
# Description:  Get the CRC16 as an Integer
#
# Input:        A message
#
# Output:       The CRC16 expressed as an Integer
#
# History:
# Date      Author   Changes Made
# --------------------------------------------------------
# 9/7/00    wcampbel Compliance with coding standards
sub getcrc16i
{
	my ($message)=@_;
	my ($out);
	$out=0;
	foreach (Evend::Misc::crc::CalcCrc($message, length $message))
	{
		$out<<=8;
		$out|=$_;
	}
	return $out;
}


#########################################################
# PackEncrypted($message, $unit, $DATABASE)
#########################################################
# Description:  Pack the message into encrypted form
#
# Inputs:       The message as a string, the unit ID
#               as a string, and a handle to the Database
#
# Outputs:      The message, length, and CRC as a string
#               in encrypted form
#
# History:
# Date      Author   Changes Made
# -----------------------------------------------
# 8/29/2000 wcampbel Moved to Shared.pm
#                    Compliance with coding standards
#########################################################
# PackEncrypted is used in SCRMod and ReRix, 
# Depends on Evend::Crypt::TEA directly, and indirectly
# to Evend::Misc::crc through the getcrc16s call
# The encryption key is supplied by the database
#########################################################
# NOTE:  All calls to PackEncrypted in ReRix.pm
#        will have to be modified in order to pass
#        the Database handle into this function
#########################################################
sub PackEncrypted
{
   my ($message, $unit, $DATABASE, $Initkey) = @_;
   my ($key, $len, $crc, @list_key, $tea);

   # Get the Silicon Serial Number to build the key
   my $array_ref = $DATABASE->select(
               table            => 'rerix_initialization',
               select_columns   => 'SILICON_SERIAL_NUMBER',
               order            => 'INITIALIZATION_ID',
               where_columns    => [ 'MODEM_ID = ?'],
               where_values     => [$unit]
                     ); 
   logit "PackEncrypted() using database key = $array_ref->[0][0]";
   
	if (defined $Initkey) # Use the Initkey?
	{
		# Got the number now build a key in the right order
		$key = &MakeString($Initkey);
		logit "PackEncrypted() using Initkey = $Initkey, key = $key";
		@list_key =( 
		         (ord(substr($key,3,1))<<24) | 
		         (ord(substr($key,2,1))<<16) | 
		         (ord(substr($key,1,1))<<8) | 
		         ord(substr($key,0,1))
		         ,
		         (ord(substr($key,7,1))<<24) | 
		         (ord(substr($key,6,1))<<16) | 
		         (ord(substr($key,5,1))<<8) | 
		         ord(substr($key,4,1))
		        );
		# The Key has the duplicated part
		push @list_key, @list_key;
	}
	elsif (defined @$array_ref && defined $array_ref->[0][0]) # Pick the Last?
	#if (defined @$array_ref && defined $array_ref->[0][0]) # Pick the Last?
	{
		# Got the number now build a key in the right order
		$key = &MakeString($array_ref->[0][0]);
		logit "PackEncrypted() using calced key $key";
		logit "message = $message";
		@list_key =( 
		         (ord(substr($key,3,1))<<24) | 
		         (ord(substr($key,2,1))<<16) | 
		         (ord(substr($key,1,1))<<8) | 
		         ord(substr($key,0,1))
		         ,
		         (ord(substr($key,7,1))<<24) | 
		         (ord(substr($key,6,1))<<16) | 
		         (ord(substr($key,5,1))<<8) | 
		         ord(substr($key,4,1))
		        );
		# The Key has the duplicated part
		push @list_key, @list_key;
	}
  else
	{
		# Well, I got to do something if I can't get a key
		print qq(No key defined for $unit.  Using "default" Key\n);
		@list_key = (0x12345678, 0x8654321);
		push @list_key, @list_key;
	}
	
  # set the key
  $tea = Evend::Crypt::TEA->new(Key=>\@list_key);
  # get the length of the message as a character
  $len = chr(length $message);
  # Compute the CRC16 of the message
  $crc = getcrc16s($message);
  # build the message with the length and the crc
  $message = $len . $message . $crc;
  # Do magic
  return $tea->EncodeStream($message);
}


#########################################################
# PackEncrypted($message, $unit, $DATABASE)
#########################################################
# Description:  Unpack the message from encrypted form
#
# Inputs:       Encrypted Message
#
# Outputs:      ([0,1].message)
#               First argument is weather the CRC matched
#
# History:
# Date      Author   Changes Made
# -----------------------------------------------
# 
# Oct 5 2000  MHS  Made 
#########################################################
# UnpackEncrypted is used in ReRix, 
# Depends on Evend::Crypt::TEA directly, and indirectly
# to Evend::Misc::crc through the getcrc16s call
# The encryption key is supplied by the database
#########################################################
# Notes: 
#########################################################
sub UnpackEncrypted
{
   my ($message, $unit, $DATABASE, $Initkey) = @_;
   my ($key, $len, $crc, $ccrc, @list_key, $tea);
   my ($out);

   # Get the Silicon Serial Number to build the key
#   warn "Message:   " . MakeHex($message) . "\n";
	logit "UnpackEncrypted() - unit = $unit";
   my $array_ref = $DATABASE->select(
               table            => 'rerix_initialization',
               select_columns   => 'SILICON_SERIAL_NUMBER',
               order            => 'INITIALIZATION_ID',
               where_columns    => [ 'MODEM_ID = ?'],
               where_values     => [$unit]
                     );  
  
  	logit "Initkey = $Initkey";
	if (defined $Initkey) # Pick the Last?
	{
	 # Got the number now build a key in the right order
	 $key = &MakeString($Initkey);
	 logit "UnpackEncrypted() using Initkey = $Initkey, key = $key";
	 @list_key =( 
	             (ord(substr($key,3,1))<<24) | 
	             (ord(substr($key,2,1))<<16) | 
	             (ord(substr($key,1,1))<<8) | 
	             ord(substr($key,0,1))
	             ,
	             (ord(substr($key,7,1))<<24) | 
	             (ord(substr($key,6,1))<<16) | 
	             (ord(substr($key,5,1))<<8) | 
	             ord(substr($key,4,1))
	            );
	 # The Key has the duplicated part
	 push @list_key, @list_key;
	}
	elsif (defined @$array_ref && defined $array_ref->[0][0]) # Pick the Last?
	{
	     # Got the number now build a key in the right order
	     $key = &MakeString($array_ref->[0][0]);
 		 logit "UnpackEncrypted() using calced key $key";
	     @list_key =( 
	                 (ord(substr($key,3,1))<<24) | 
	                 (ord(substr($key,2,1))<<16) | 
	                 (ord(substr($key,1,1))<<8) | 
	                 ord(substr($key,0,1))
	                 ,
	                 (ord(substr($key,7,1))<<24) | 
	                 (ord(substr($key,6,1))<<16) | 
	                 (ord(substr($key,5,1))<<8) | 
	                 ord(substr($key,4,1))
	                );
	     # The Key has the duplicated part
	     push @list_key, @list_key;
	 }
	else
	{
	 # Well, I got to do something if I can't get a key
	 print qq(No key defined for $unit.  Using "default" Key\n);
	 $DATABASE->insert(
	       table           => 'Machine_Command',
	       insert_columns  => 'Modem_ID, Command, Execute_cd',
	       insert_values   => [$unit, &MakeHex('e'. chr(1)), 'R']
	                      );
	
	 @list_key = (0x12345678, 0x8654321);
	 push @list_key, @list_key;
	}

	  # set the key
  $tea = Evend::Crypt::TEA->new(Key=>\@list_key);
  $out = $tea->DecodeStream($message);
#  warn "Decode:   " . MakeHex($out) . "\n";

  $len = ord($out);
#  warn "Length: $len\n";
  $out = substr($out,1);
  $crc = substr($out,$len,2);
#  warn "CRC: " . MakeHex($crc) . "\n";
  $out = substr($out,0,$len);
  $ccrc = getcrc16s($out);
#  warn "Computer: ". MakeHex($ccrc) . "\n";

  return [($crc eq $ccrc)?1:0, $out];
}

sub logit($)
{
	my $line = shift;

	print "[" . localtime() . "] $line\n";
}
# Don't remove
1;
