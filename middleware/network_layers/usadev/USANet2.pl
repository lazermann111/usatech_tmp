#!/usr/local/bin/perl -w

use strict;

require Net::Daemon;

package USANet2Layer;

use IO::Socket;

use lib '/opt/USAtest/USATestSystem';

use USANet2;

use vars qw (@ISA);
@ISA = qw(Net::Daemon); # inherit from Net::Daemon

sub Run ($) 
{
	my($self) = @_;

	my $sock = $self->{'socket'};

	my $ssn = '';

	print "Got connection at " . localtime() . "\n";

	USANet2::Run($sock);
}
  
package main;

my $server = USANet2Layer->new({'pidfile' => 'none','localport' => 14109},"");

$server->Bind();

