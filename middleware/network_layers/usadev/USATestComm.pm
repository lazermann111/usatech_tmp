#!/bin/perl -w
#---------------------------------------------------------------------------------------------
# Change History
#
# Version       Date            Programmer      Description
# -------       ----------      ----------      ----------------------------------------------
# 1.00          02/06/2003      T Shannon       First Version
#						

package USATestComm;

use strict;

use Shared;

use Evend::Database::Database;        # Access to the Database

use IO::Socket;
use IO::Select;

sub getPacket ($);

# stubs for testing commands
sub testSettings($);	#0x33h
sub testSettingsAck($);	#0x3ch
sub testReqSettings($);	#0x33h
sub testInitialize($);	#0x49h
sub testAuth20($);		#0x5eh
sub testAuthResp20($);	#0x60h
sub testNetAuth20($);	#0x2ah
sub testForceAuth20($);	#0x2bh

sub logit($);

my $bool = 0;

sub Run ($)
{
	my $sock = shift;
	my $msg;

	$| = 1;

	$msg = "Connected from " . $sock->peerhost . ":" . $sock->peerport;
	logit($msg);

	my $TIMEOUT = 60*3;

	my $machine_id;
	my $network = 'P';
	my $ssn;

	my $rh_set;
	my $wh_set;

	my $rh;
	my $wh;

	my $sequence_in;
	my $sequence_out = 5;
	my $rerix;

	my $read_set = new IO::Select();
	my $write_set = new IO::Select();
	$read_set->add($sock);

	my $next_command_check = time() + 5;
	my $last_command_resend;

	my $outBuffer;
	my $inBuffer;

	my $outCommand;

	my $outCommandID;

	my $watchdog = time;	
	
	#my $msgnbr;

	while(1)
	{
		my $select_t;

		if( defined $outCommandID )
		{
			$select_t = ($last_command_resend - time()) > 0 ?
								$last_command_resend - time() : 0;
		}
		else
		{
			$select_t = ($next_command_check - time()) > 0 ?
								$next_command_check - time() : 0;
		}

		#logit ("Selecting $select_t\n");

		($rh_set, $wh_set) =
						IO::Select->select($read_set, $write_set,
											undef, $select_t);

		if( $watchdog + $TIMEOUT < time )
		{
			$sock->close();

			logit ("Timeout " . localtime() . "\n");

			return;
		}

		foreach $rh (@$rh_set)
		{
			my $tempBuffer;

			$watchdog = time;

			$rh->recv($tempBuffer, 512);

			if( length( $tempBuffer ) == 0 )
			{
				$sock->close();

				logit ("Connection Lost " . localtime() . "\n");

				return;
			}
			else
			{
				$inBuffer .= $tempBuffer;

				my $packet;
				
				logit ("inBuffer: " . $inBuffer . "\n");
				logit ("inBuffer hex: '" . unpack('H*', $inBuffer) . "'\n");

				logit "The message number is " . unpack("H*", substr($inBuffer, 0, 1));

				($packet, $inBuffer) = getPacket($inBuffer);
				
				
				chomp $packet;
				chop $packet;
				
				logit ("chomped inBuffer hex: '" . unpack('H*', $packet) . "'\n");

				
				while( defined $packet )
				{
					my $msgnbr = substr($packet, 0, 1);
					logit "*** New Command Received ***\n";
					logit "The msgnbr byte hex is " . unpack("H*", substr($packet, 0, 1)) . "\n";
					logit "The command byte hex is " . unpack("H*", substr($packet, 1, 1)) . "\n";
					my $str;
					if( ord(substr($packet, 1, 1)) == 0x33 )
					{
						# do Settings 0x33h command
						logit ("Received a 033h packet - '" . $packet . "'\n");
						$str = testSettings($packet);
						$outBuffer .= $msgnbr . $str;
						$write_set->add( $sock );
					}
					elsif( ord(substr($packet, 1, 1)) == 0x3c )
					{
						# do SettingsAck 0x3ch command
						logit ("Received a 03ch packet - '" . $packet . "'\n");
						$str = testSettingsAck($packet);
						$outBuffer .= $msgnbr . $str;
						$write_set->add( $sock );
					}
					elsif( ord(substr($packet, 1, 1)) == 0x49 )
					{
						# do Initialize 0x49h command
						logit ("Received a 049h packet - '" . $packet . "'\n");
						$str = testInitialize($packet);
						$outBuffer .= $msgnbr . $str;
						$write_set->add( $sock );
					}
					elsif( ord(substr($packet, 1, 1)) == 0x5e )
					{
						# do Auth20 0x5eh command
						$str = testAuth20($packet);
						$outBuffer .= $msgnbr . $str;
						$write_set->add( $sock );
					}
					elsif( ord(substr($packet, 1, 1)) == 0x60 )
					{
						# do Auth20 Response 0x60h command
						logit ("Received a 060h packet - '" . $packet . "'\n");
						$str = testAuthResp20($packet);
						$outBuffer .= $msgnbr . $str;
						$write_set->add( $sock );
					}
					elsif( ord(substr($packet, 1, 1)) == 0x2a )
					{
						# do NetAuth20 0x2ah command
						logit ("Received a 02ah packet - '" . $packet . "'\n");
						$str = testNetAuth20($packet);
						$outBuffer .= $msgnbr . $str;
						$write_set->add( $sock );
					}
					elsif( ord(substr($packet, 1, 1)) == 0x2b )
					{
						# do ForceAuth20 Response 0x2bh command
						logit ("Received a 02bh packet - '" . $packet . "'\n");
						$str = testForceAuth20($packet);
						$outBuffer .= $msgnbr . $str;
						$write_set->add( $sock );
					}
					else
					{
						logit ("Unknown Packet\n");
					}

					($packet, $inBuffer) = getPacket($inBuffer);
				} # while ( defined $packet )
			}
			$next_command_check = 0;
		}

		foreach $wh (@$wh_set)
		{
			my $sent = $wh->send(uuencode($outBuffer));

			$watchdog = time;

			logit ("Sent: " . substr($outBuffer, 0, $sent) . "\x0a");
			$outBuffer = substr $outBuffer, $sent;

			# if there's no more data to send,
			# remove it from the select()
			if( length( $outBuffer ) == 0 )
			{
				$write_set->remove( $wh );
			}
		}

		logit "outBuffer = '" . $outBuffer . "', last_commmand_resend = '" . $last_command_resend;
		if( (length($outBuffer) > 0) and ($last_command_resend <= time()) )
		{
			logit ("Sending reply to client (ASCII): '" . $outBuffer . "'\n");
			logit ("Sending reply to client (hex): '" . unpack("H*", $outBuffer) . "'\n");

			# send the command again, it timed out

			$outBuffer .= $outCommand;

			$write_set->add( $sock );

		}
		elsif( $next_command_check <= time() )
		{
			$next_command_check = time() + 5;
		}
	}
}

sub testSettings($)		#0x33h
{
	return "got an 0x33 command\n";
}

sub testSettingsAck($)	#0x3ch
{
	return "got an 0x3c command\n";
}

sub testReqSettings($)	#0x33h
{
	return "got an 0x33 command\n";
}

sub testInitialize($)	#0x49h
{
	return "got an 0x49 command\n";
}

sub testAuth20($)		#0x5eh
{
	if ($bool == 0)
	{
		$bool = 1;
		return '`00011';
	}
	else
	{
		$bool = 0;
		return '`00010';
	}
}

sub testAuthResp20($)	#0x60h
{
	return "got an 0x60 command\n";
}

sub testNetAuth20($)	#0x2ah
{
	return "got an 0x2a command\n";
}

sub testForceAuth20($)	#0x2bh
{
	return "got an 0x2b command\n";
}


sub getPacket ($)
{
	my $inBuffer = shift;
	logit "getPacket() inBuffer = '" . $inBuffer . "'";

	my $packet;
	my $byte;

	while( length($inBuffer) )
	{
		$byte = substr $inBuffer, 0, 1;
		$inBuffer = substr $inBuffer, 1;

		$packet = $byte;

		while( length($inBuffer) )
		{
			$byte = substr $inBuffer, 0, 1;
			$inBuffer = substr $inBuffer, 1;

			if( $byte eq "\x0a" )
			{
				$packet .= $byte;

				return (uudecode($packet), $inBuffer);
			}
			else
			{
				$packet .= $byte;
			}
		}

		# the packet is incomplete
		return (undef, $packet);
	}

	return( undef, '');
}

sub check ($)
{
	my $packet = shift;

	return(undef) if !defined $packet;

	my $crc = ord(substr($packet,1,1))<<8 | ord(substr($packet,2,1));

	my $datablock = substr(substr($packet, 3), 0, -1);

	my $calcrc = getcrc16i($datablock);

	logit ( sprintf('Got Checksum: 0x%4.4X Computed: 0x%4.4X' ."\n", $crc, $calcrc));

	if( ($crc == $calcrc) or ($crc == 0) )
	{
		return($packet);
	}
	else
	{
		logit "Bad CRC\n";
		return(undef);
	}
}

sub uuencode
{
   my @byte = split('', shift);

   my $len = scalar @byte;

   for(my $i = 0; $i < $len; $i++)
   {
      $byte[$i] = ord($byte[$i]);
   }

   my ($final, $ptr);
   for(my $i = 0; $i < $len; $i += 3)
   {
      $final .= sprintf("%c", 32 + (($byte[$i] >> 2) & 0x3f));
      if( defined $byte[$i+1] )
      {
         $final .= sprintf("%c", 32 + ((($byte[$i]<<4) |
										((($byte[$i+1]>>4)&0xf))) & 0x3f));
         if( defined $byte[$i+2] )
         {
            $final .= sprintf("%c", 32 + (((($byte[$i+1]<<2)) |
											((($byte[$i+2]>>6)&0x3))) &0x3f));
            $final .= sprintf("%c", 32 + (($byte[$i+2]) & 0x3f));
         }
         else
         {
            $final .= sprintf("%c", 32 + (($byte[$i+1]<<2) & 0x3f));
         }
      }
      else
      {
         $final .= sprintf("%c", 32 + (($byte[$i]<<4) & 0x3f));
      }
   }

   return $final;
}

sub uudecode
{
   my @byte = split //, $_[0];

   #my $header = shift @byte;
   my $footer = pop @byte;

   my $val;
   my $final;
   my $mod;

   for(my $i = 0; $i < @byte; $i++)
   {
      $mod = $i % 4;
      my $info = ord($byte[$i]) - 32;

      if ($mod == 0) {
         $val = ($info & 0x3f) << 2;
      }
      elsif ($mod == 1) {
         $val |= ($info >> 4) & 0x03;
         $final .= sprintf("%c", $val);
         $val  = ($info & 0x0f) << 4;
      }
      elsif ($mod == 2) {
         $val |= ($info >> 2) & 0x0f;
         $final .= sprintf("%c", $val);
         $val  = ($info & 0x03) << 6;
      }
      elsif ($mod == 3) {
         $val |= ($info & 0x3f);
         $final .= sprintf("%c", $val);
      }
   }

   return $final . $footer;
}


sub logit($)
{
	my $line = shift;

	print "[" . localtime() . "] $line\n";
}
1;
