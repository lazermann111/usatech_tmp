package USANet2DB;

use strict;
use warnings;

use IO::Select;
use Time::HiRes qw(sleep);
use USAT::NetLayer::Database;	# Access to the Database
use USAT::NetLayer::Session;
use Evend::ReRix::Shared qw(MakeHex MakeString getcrc16s getcrc16i);
use USAT::NetLayer::Filter::UU qw(uuencode uudecode);
use USAT::NetLayer::Utils qw(getPacket SecureLogIt);
use USAT::Thread::Queue::Any;

use constant {
	MAINLOOP_SLEEP_SEC							=> 0.2,
	INCOMPLETE_PACKET_RETRY_MAX_SEC				=> 8,
};

sub Run ($$$$$$$$$$)
{
	my $sock = shift;
	$| = 1;

	my $queue_db_config = shift;
	my $net_layer_id = shift;
	my $inbound_queue_ref = shift;
	my $outbound_queue_ref = shift;
	my $outbound_queue_pool_ref = shift;
	my $initBuffer = shift;
	my $session_id_ref = shift;
	my $logLevel = shift;
	my $debugFile = shift;
	
	$$session_id_ref = 0;
	
	my $DATABASE = USAT::NetLayer::Database->new
	(
		config 				=> $queue_db_config,
		log_level 			=> $logLevel,
		debug_file_handle	=> $debugFile
	);
	$DATABASE->disconnect;	#disconnect immediately, since superclass auto-connects on object creation
	
	my $add_init_buffer = 1;

	my $remote_addr = $sock->peerhost;
	my $port = $sock->peerport;
	my $call_id = "$remote_addr:$port";

	my $started_log = 'N';
	my $in_msg_count = 0;
	my $out_msg_count = 0;
	my $in_byte_count = 0;
	my $out_byte_count = 0;
	my $session_outbound_queue_ref;

	my $TIMEOUT = 45;
	my $machine_id;
	my $ssn;
	my $device_type;
	
	my ($out_command_id, $out_command);
	my @outbound_commands;

	my $rh_set;
	my $wh_set;
	my $rh;
	my $wh;
	my $rerix;
	my $msg_no;

	my $read_set = new IO::Select();
	my $write_set = new IO::Select();
	$read_set->add($sock);

	my $next_command_check = time() + 2;
	my $last_command_resend;
	my $outBuffer;
	my $inBuffer;
	my $watchdog = time;	
	my $watchdog_valid_inbound_msg = time();
	my $read_incomplete_count = 0;
	my $read_incomplete_timeout_ts = 0;

	SecureLogIt("$call_id\t" . "Connected from $remote_addr:$port, logLevel: $logLevel", $logLevel, $debugFile);
	MAINLOOP: while(1)
	{
		($rh_set, $wh_set) = IO::Select->select($read_set, $write_set, undef, 0.001);

		if ($watchdog + $TIMEOUT < time)
		{
			SecureLogIt("$call_id\t" . "Timeout " . localtime(), $logLevel, $debugFile);
			if ($started_log eq 'Y')
			{
				$DATABASE->connect();
				USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
				$DATABASE->disconnect();
			}
			last MAINLOOP;
		}

		foreach $rh (@$rh_set)
		{
			my ($tempBuffer, $tempBufferLen);
			$watchdog = time;
			$rh->recv($tempBuffer, 512);
			
			if ($add_init_buffer)
			{
				$tempBuffer = $initBuffer . $tempBuffer;
				$add_init_buffer = 0;
			}			

			$tempBufferLen = length($tempBuffer);
			
			SecureLogIt("$call_id\t" . "Read $tempBufferLen bytes.", $logLevel, $debugFile);

			if($tempBufferLen == 0)
			{
				SecureLogIt("$call_id\t" . "Connection Lost " . localtime(), $logLevel, $debugFile);
				if ($started_log eq 'Y')
				{
					$DATABASE->connect();
					USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
					$DATABASE->disconnect();
				}
				last MAINLOOP;
			}
			else
			{
				$inBuffer .= $tempBuffer;
			}
		}
		
		my $data_valid = 1;
		PACKETLOOP: while(defined $inBuffer && (length($inBuffer) > 0) && $data_valid)
		{
			SecureLogIt("$call_id\t" . ">---------------------------------------->", $logLevel, $debugFile);

			my $packet;
			my $result;
			my $message_size = 0;
			my $loopcnt = 0;

			SecureLogIt("$call_id\t" . "inBuffer hex: ", $logLevel, $debugFile, unpack('H*', $inBuffer));
			$watchdog_valid_inbound_msg = time();
			
			my $hangup_received = &check_hangup($inBuffer);
			if($hangup_received)
			{
				SecureLogIt("$call_id\t" . "Caught hangup string ($hangup_received)... closing the connection.", $logLevel, $debugFile);
				if ($started_log eq 'Y')
				{
					$DATABASE->connect();
					USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
					$DATABASE->disconnect();
				}
				last MAINLOOP;
			}			

			# gets data until x0A
			($packet, $inBuffer) = getPacket($inBuffer);
			
			if(not defined $packet)
			{
				$read_incomplete_count++;
				if ($read_incomplete_timeout_ts == 0)
				{
					$read_incomplete_timeout_ts = time() + INCOMPLETE_PACKET_RETRY_MAX_SEC;
				}

#				if($read_incomplete_count > 8)
				if(time() > $read_incomplete_timeout_ts)
				{
					SecureLogIt("$call_id\t" . "Failed to receive complete packet in getPacket, timeout reached... aborting!", $logLevel, $debugFile);
					if ($started_log eq 'Y')
					{
						$DATABASE->connect();
						USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
						$DATABASE->disconnect();
					}
					last MAINLOOP;
				}
				else
				{
					SecureLogIt("$call_id\t" . "Failed to receive complete packet in getPacket; attempt $read_incomplete_count (retry expires ".localtime($read_incomplete_timeout_ts).")", $logLevel, $debugFile);
					$data_valid = 0;
					next PACKETLOOP;
				}
			}
			
			SecureLogIt("$call_id\t" . "received packet: ", $logLevel, $debugFile, unpack("H*", $packet));
			SecureLogIt("$call_id\t" . length($inBuffer) . " bytes remaining in inBuffer", $logLevel, $debugFile);
			
			$message_size = length($packet);
			
			# do some simple validation in an effort to filter out junk data and line noise
			if($message_size < 16)
			{
				SecureLogIt("$call_id\t" . "Packet failed validation, message size = $message_size... aborting!", $logLevel, $debugFile);
				if($started_log eq 'Y')
				{
					$DATABASE->connect();
					USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
					$DATABASE->disconnect();
				}

				last MAINLOOP;
			}
			
			$read_incomplete_count = 0;
			$read_incomplete_timeout_ts = 0;
			$packet = uudecode(substr($packet, 0, -1)) . substr($packet, -1);
			
			if(not defined $packet)
			{
				SecureLogIt("$call_id\t" . "Aborting... uudecode failed for: ", $logLevel, $debugFile, unpack("H*", $packet));
				if($started_log eq 'Y')
				{
					$DATABASE->connect();
					USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
					$DATABASE->disconnect();
				}
				
				last MAINLOOP;
			}
			
			SecureLogIt("$call_id\t" . "decoded packet hex: ", $logLevel, $debugFile, unpack('H*', $packet));

			# the first 8 bytes of the uudecoded packet contains the SSN; use that to decrypt the packet
			$ssn = uc(unpack("H*",substr($packet, 0, 8)));
			SecureLogIt("$call_id\t" . "Serial number: $ssn", $logLevel, $debugFile);

			if ($ssn =~ /^[0-9A-F]{8}(63){4}$/)
			{
				$device_type = 4;
			}
			else
			{
				# USANet2 is used only by Sony PictureStations with the old DLL
				SecureLogIt("$call_id\t" . "Received invalid serial number... ending connection", $logLevel, $debugFile);
				if ($started_log eq 'Y')
				{
					$DATABASE->connect();
					USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
					$DATABASE->disconnect();
				}
				last MAINLOOP;
			}

			if($started_log eq 'N')
			{
				# get machine id from database
				$DATABASE->connect(); 
				my $array_ref = $DATABASE->select(
					table			=> 'device',
					select_columns	=> 'device_name',
					order			=> 'device_active_yn_flag desc',
					where_columns	=> ['device_type_id=?', 'device_serial_cd=?'],
					where_values	=> [$device_type, $ssn]
				);
				$DATABASE->disconnect();

				if (defined $array_ref->[0])
				{
					$machine_id = $array_ref->[0][0];
					SecureLogIt("$call_id\t" . "DB machine_id for $ssn is $machine_id", $logLevel, $debugFile);
				}
				else
				{
					$machine_id = "EV000000";
					SecureLogIt("$call_id\t" . "New machine, set machine_id to EV000000", $logLevel, $debugFile);						
				}
			
				$DATABASE->connect();
				USAT::NetLayer::Session::start($DATABASE, $session_id_ref, $net_layer_id, $machine_id, $remote_addr, $port);
				$DATABASE->disconnect();
				
				$started_log = 'Y';
				SecureLogIt("$call_id\t" . "Started new device session, session_id: $$session_id_ref", $logLevel, $debugFile);

				{
					{
						lock($outbound_queue_pool_ref);
						$session_outbound_queue_ref = shift @$outbound_queue_pool_ref;
					}

					if (not defined $session_outbound_queue_ref)
					{
						$session_outbound_queue_ref = new USAT::Thread::Queue::Any;
					}	

					lock($outbound_queue_ref);
					$outbound_queue_ref->{$$session_id_ref} = $session_outbound_queue_ref;
				}
			}

			$in_msg_count++;
			$in_byte_count += $message_size;

			# decrypt the packet
			SecureLogIt("$call_id\t" . "Decrypting packet...", $logLevel, $debugFile);
			($result, $packet) = @{UnpackEncrypted(substr($packet,8), $ssn)};

			if ($result == 0)
			{
				SecureLogIt("$call_id\t" . "Decryption failure for SSN " . $ssn, $logLevel, $debugFile);
			}
			else
			{
				$msg_no = substr($packet, 0, 1);

				SecureLogIt("$call_id\t" . "*** New Command Received ***", $logLevel, $debugFile);
				SecureLogIt("$call_id\t" . "The packet hex is       : ", $logLevel, $debugFile, unpack("H*", $packet));
				SecureLogIt("$call_id\t" . "The msg_no byte hex is  : " . unpack("H*", $msg_no), $logLevel, $debugFile);
				SecureLogIt("$call_id\t" . "The command type hex is : " . unpack("H*", substr($packet, 1, 1)), $logLevel, $debugFile);
				SecureLogIt("$call_id\t" . "The inBuffer hex is     : ", $logLevel, $debugFile, unpack("H*", $inBuffer));
				SecureLogIt("$call_id\t" . "The inBuffer length is  : " . length($inBuffer), $logLevel, $debugFile);

				$rerix = unpack("H*", substr($packet,1));
				$msg_no = ord($msg_no);

				SecureLogIt("$call_id\t" . "Inserting $machine_id, msg_no $msg_no, session_id $$session_id_ref, ", $logLevel, $debugFile, $rerix, " into inbound queue...");
				$inbound_queue_ref->enqueue("$machine_id,$rerix,$msg_no,$$session_id_ref");

				$next_command_check = 0;
			}
		}

		foreach $wh (@$wh_set)
		{
			SecureLogIt("$call_id\t" . "Sending reply to client (hex): " . unpack("H*", $outBuffer), $logLevel, $debugFile);
			my $sent = $wh->send($outBuffer);

			$out_msg_count++;
			$out_byte_count += $sent;

			$watchdog = time;

			$outBuffer = substr($outBuffer, $sent);

			# remove it from the select()
			if (length( $outBuffer ) == 0)
			{
				$write_set->remove( $wh );
			}
		}

		if ($next_command_check <= time() && $started_log eq 'Y')
		{
			$next_command_check = time() + 0.25;
			
			SecureLogIt("$call_id\t" . "Polling session outbound queue...", $logLevel, $debugFile);
			@outbound_commands = $session_outbound_queue_ref->dequeue_all_dontwait;

			foreach my $outbound_data (@outbound_commands)
			{
				foreach my $outbound_row (@$outbound_data)
				{
					SecureLogIt("$call_id\t" . "<----------------------------------------<", $logLevel, $debugFile);
					($out_command_id, $out_command, undef) = @$outbound_row;
					
					if (substr($out_command, 2, 2) eq "69")
					{
						# if we get response to Init V2 command (Set Serial Number, Data Type [69h]), use EV number that was sent in init message
						SecureLogIt("$call_id\t" . "About to send response to Init V2 command. Retrieving new machine_id...", $logLevel, $debugFile);
						
						$DATABASE->connect();
						my $array_ref = $DATABASE->select(
							table			=> 'device',
							select_columns	=> 'device_name',
							order			=> 'device_active_yn_flag desc',
							where_columns	=> ['device_type_id=?', 'device_serial_cd=?'],
							where_values	=> [$device_type, $ssn]
						);
						$DATABASE->disconnect();

						if (defined $array_ref->[0])
						{
							$machine_id = $array_ref->[0][0];
							SecureLogIt("$call_id\t" . "New machine_id is $machine_id", $logLevel, $debugFile);
						}
						else
						{
							SecureLogIt("$call_id\t" . "No EV# for serial number $ssn, closing", $logLevel, $debugFile);
							if ($started_log eq 'Y')
							{
								$DATABASE->connect();
								USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
								$DATABASE->disconnect();
							}
							last MAINLOOP;
						}
					}															

					SecureLogIt("$call_id\t" . "Sending client:$machine_id session_id:$$session_id_ref id:$out_command_id cmd:$out_command", $logLevel, $debugFile);

					$out_command = pack('H*' ,$out_command);
					SecureLogIt("$call_id\t" . "Encrypting packet...", $logLevel, $debugFile);
					$out_command = PackEncrypted($out_command, $ssn);
					
					SecureLogIt("$call_id\t" . "Sending encrypted client:$machine_id id:$out_command_id cmd:" . unpack("H*", $out_command), $logLevel, $debugFile);
					
					$out_command = uuencode($out_command) . "\x0a";
					SecureLogIt("$call_id\t" . "Sending encoded client:$machine_id id:$out_command_id cmd:" . unpack("H*", $out_command), $logLevel, $debugFile);

					$outBuffer .= $out_command;
					$write_set->add($sock);
					$last_command_resend = time() + 5;
				}
			}
		}
		elsif ($next_command_check <= time())
		{
			$next_command_check = time() + 0.25;
		}
		
		if ($watchdog_valid_inbound_msg + $TIMEOUT < time())
		{
			SecureLogIt("$call_id\t" . "Exceeded inactivity timeout... closing the connection.", $logLevel, $debugFile);
			if ($started_log eq 'Y')
			{
				$DATABASE->connect();
				USAT::NetLayer::Session::finish($DATABASE, $$session_id_ref, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count);
				$DATABASE->disconnect();
			}
			last MAINLOOP;
		}
		
		sleep MAINLOOP_SLEEP_SEC;
	}
	
	$sock->close();
	return 1;
}

sub check_hangup ($)
{
	my $inBuffer = shift;

	if(not defined $inBuffer)
	{
		return undef;
	}

	if($inBuffer =~ /^\+\+\+/)
	{
		# standard +++
		return '+++';
	}
	elsif($inBuffer =~ /^AT\x0d/)
	{
		# AnyDATA CDMA modem
		return "41540d";
	}
	elsif($inBuffer =~ /\x9e\x86\x9e\x86\x9e\x86/)
	{
		# AS5300 
		return '9e869e869e86';
	}
	elsif($inBuffer =~ /^AT#CONNECTIONSTOP/)
	{
		# Multitech GSM/GPRS modem
		return 'AT#CONNECTIONSTOP';
	}
	
	return undef;
}

sub PackEncrypted ($$)
{
	my ($message, $ssn) = @_;
	my ($key, $len, $crc, @list_key, $tea);

	# Got the number now build a key in the right order
	$key = &MakeString($ssn);
	@list_key =( 
		(ord(substr($key,3,1))<<24) | 
		(ord(substr($key,2,1))<<16) | 
		(ord(substr($key,1,1))<<8) | 
		ord(substr($key,0,1))
		,
		(ord(substr($key,7,1))<<24) | 
		(ord(substr($key,6,1))<<16) | 
		(ord(substr($key,5,1))<<8) | 
		ord(substr($key,4,1))
	);
	# The Key has the duplicated part
	push @list_key, @list_key;

	# set the key
	$tea = Evend::Crypt::TEA->new(Key=>\@list_key);
	# get the length of the message as a character
	$len = chr(length $message);
	# Compute the CRC16 of the message
	$crc = getcrc16s($message);
	# build the message with the length and the crc
	$message = $len . $message . $crc;
	# Do magic
	return $tea->EncodeStream($message);
}

sub UnpackEncrypted ($$)
{
	my ($message, $ssn) = @_;
	my ($key, $len, $crc, $ccrc, @list_key, $tea, $out);

	# Got the number now build a key in the right order
	$key = &MakeString($ssn);
	@list_key =( 
		(ord(substr($key,3,1))<<24) | 
		(ord(substr($key,2,1))<<16) | 
		(ord(substr($key,1,1))<<8) | 
		ord(substr($key,0,1))
		,
		(ord(substr($key,7,1))<<24) | 
		(ord(substr($key,6,1))<<16) | 
		(ord(substr($key,5,1))<<8) | 
		ord(substr($key,4,1))
	);
	# The Key has the duplicated part
	push @list_key, @list_key;

	# set the key
	$tea = Evend::Crypt::TEA->new(Key=>\@list_key);
	$out = $tea->DecodeStream($message);

	$len = ord($out);
	$out = substr($out,1);
	$crc = substr($out,$len,2);
	$out = substr($out,0,$len);
	$ccrc = getcrc16s($out);

	return [($crc eq $ccrc)?1:0, $out];
}


1;
