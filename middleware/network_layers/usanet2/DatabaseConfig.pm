package DatabaseConfig;

use strict;
use USAT::Database::Config;
use vars qw(@ISA);
@ISA = qw(USAT::Database::Config);

sub new {
	my $type = shift;
	my $self = USAT::Database::Config->new(
		username		=> 'net_user',
		password		=> 'passwd',
		timeout_connect	=> 10,	#seconds
		retries			=> 1,
		@_
	);
	return $self;
}

1;
