#!/usr/local/bin/perl
use strict;
use warnings;
use forks;
use forks::shared;
use sigtrap qw(die untrapped normal-signals stack-trace error-signals);

use USAT::NetLayer::Daemon;
use USAT::NetLayer::Utils qw(LogIt);

use lib '/opt/USANet2';
use DatabaseConfig;
use HandlerDBConfig;
use USANet2DB;

LogIt("USANet2 Network Layer - Copyright (c) 2003-2006 USA Technologies.  All rights reserved.");
my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, lines may be out of order
my $server = USAT::NetLayer::Daemon->new(
	{
		'pidfile'			=> '/opt/USANet2/USANet2.pid',
		'localport'			=> 14105, 
		'logfile'			=> 'STDERR',
		'mode'				=> 'ithreads',
		'mydesc'			=> 'USANet2 on port 14105',
		'mydbconfig'		=> DatabaseConfig->new(),
		'mynetlayeridfile'	=> '/opt/USANet2/USANet2.id',
		'myrunlib'			=> 'USANet2DB',
		'myrunlibpath'		=> '/opt/USANet2',
		'mysocketlimit'		=> 30,
		'mysocketlimitIPExclRegexp'	=> undef,
		'myconfigfile'		=> '/opt/USANet2/layer.config',
		'mydebugfile'		=> '/opt/USANet2/layer.debug',
		'myhandlerdbconfig'	=> HandlerDBConfig->new()
	}, 
	""
);

$server->Bind();

END
{
	eval { $server->shutdown_net_layer_id() };
}
