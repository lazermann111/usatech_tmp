package HandlerDBConfig;

use strict;
use USAT::Database::Config;
use vars qw(@ISA);
@ISA = qw(USAT::Database::Config);

sub new {
	my $type = shift;
	my $self = USAT::Database::Config->new(
		primary			=> 'host=localhost;port=1521',
		backup			=> 'host=localhost;port=1521',
		driver			=> 'SQLRelay::Oracle',
		username		=> 'net_user',
		password		=> 'passwd',
		retries			=> 1,
		@_
	);
	return $self;
}

1;
