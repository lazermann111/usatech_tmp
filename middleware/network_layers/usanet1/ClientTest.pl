#!perl
package ClientTest;
#
# ClientTest.pl
#
use strict;

use IO::Socket;
use IO::Select;
use SendReceive qw(receiveData sendData waitForData LogIt);
use USAT::Database;
use USAT::Database::Config;

#CONSTANTS
my $STX = chr(2);
my $ETX = chr(3);
my $EOT = chr(4);
my $ENQ = chr(5);
my $ACK = chr(6);
my $EOC = chr(7);
my $SO  = chr(15);
my $NAK = chr(21);
my $SI  = chr(33);

$| = 1; # auto-flush output

my $socket = IO::Socket::INET->new(PeerAddr => '10.0.0.63:14111');
my $read_set = new IO::Select();
#my $read_set = $socket;
my $write_set = new IO::Select();

if(!defined($socket)) {
	LogIt("Failed to connect to server\n");
	exit 200;
} else {
	LogIt("Connected to server\n");
}

$read_set->add($socket);
$write_set->add($socket);

my $DATABASE = USAT::Database->new(config => USAT::Database::Config->new());
	
my $ret = run($read_set, $write_set);
LogIt("Result = $ret");
exit $ret;

    sub run($$;) {
    	my ($read_set, $write_set) = @_;
    	my ($read_byte_count, $write_byte_count) = (0,0);
        #wait for enq
        my $serial = 'E4070003';
        LogIt("Sending initial enqueue");
        waitForData($read_set, $write_set, \$read_byte_count, \$write_byte_count, $ENQ, undef, 60, 10) || return 202;

        #send logon
        LogIt("Sending serial $serial");
        sendData($read_set, $write_set, \$read_byte_count, \$write_byte_count, $serial, $serial, 3) || return 204;

        #send config
        my ($sec,$min,$hour,$mday,$mon,$year) = localtime();
    	my $dt = sprintf('%02d%02d%02d%02d%02d%02d', $mon+1, $mday, $year % 100, $hour, $min, $sec);
        my $old_config = "USA-E41BVr.1.8.4071095000000Coke Atlanta    E.179001089996      NONE                NONE                34AAAAAA37AAAAAA4AAAAAAA51AAAAAA52AAAAAA53AAAAAA54AAAAAA55AAAAAABBBBBBBB6011AAAA0010000A1111111111918884044192None        918009501292918005453334^         TN213904151000501000000000010000100025001000501001NI84850800C10070225YSNHY720000000000000000000101001-888-561-4748\x1C$dt\x0D";
        LogIt("Sending config; length=".length($old_config));
        sendData($read_set, $write_set, \$read_byte_count, \$write_byte_count, $serial, $old_config, 5) || return 206;

        #get new config data (x2)
        my $config = receiveData($read_set, $write_set, \$read_byte_count, \$write_byte_count, 5, $ACK, 2, undef, undef, $serial . ($NAK x 3));
        ($config && $config !~ m/$EOC|$SO/so) || return 208;
		LogIt("Successfully Received new Config!!");
        #get new coupon data
        my $coupon = receiveData($read_set, $write_set, \$read_byte_count, \$write_byte_count, 5, $serial . $ACK, 2);
        (defined($coupon) && $coupon !~ m/$EOC|$SO/so) || return 210;
		LogIt("Successfully Received new Coupon!!");
        
        #send trans
        my @transactions = (
        	#'373274616272183=0703030370934        100100AP5889931122908470000122909050001000000000000000000000000',
        	'Currency Vend                        F00250        3123017390000123039120000017800000000000000000004');
        #wait for ACK - to ensure that server is now ready to receive trans
        LogIt("Waiting for ENQ from server");
        waitForData($read_set, $write_set, \$read_byte_count, \$write_byte_count, $ENQ, $serial . $ACK, 10, 10) || return 216;
        foreach my $x (@transactions) {
            LogIt("Sending transaction");
        	sendData($read_set, $write_set, \$read_byte_count, \$write_byte_count, $serial, $x, 5) || return 217;
        }
        waitForData($read_set, $write_set, \$read_byte_count, \$write_byte_count, $ENQ, $EOC x 3, 10, 10) || return 218;

        waitForData($read_set, $write_set, \$read_byte_count, \$write_byte_count, qr/($ACK)|($NAK)|($EOT)/so, $SO, 10, 10) || return 219;
        
        #sendData($serial, 'Some Dex Data for you!', 5) || return 220;

		my @wh_set = $write_set->can_write(0);
        my $wh_fh = $wh_set[0];
        if($wh_fh) {
		    for(my $i = 0; $i < 5; $i++) {
	            LogIt("Sending EOC\n");
	            $wh_fh->print($EOC x 3);
	            $wh_fh->flush();
		        sleep(1);
	        }
	        #ALL DONE
	        $wh_fh->print($EOT x 3); # send end of transmission
	        $wh_fh->flush();
        }	        
        return 0;
    }

sub blob_select_config
{
	my ($db, $serial) = @_;
	my ($blob, $buffer);
	
	$db->{LongReadLen}=500000;  # Make sure buffer is big enough for BLOB
	my $stmt = $db->prepare(q{
		select file_transfer_content 
		  from file_transfer ft, device d
		 where ft.file_transfer_name = d.device_name || '-CFG' 
		   and d.device_serial_cd = :serial
		   and device_active_yn_flag = 'Y'});
	$stmt->bind_param(":serial", "$serial");

	$stmt->execute();

	while ($blob = $stmt->fetchrow)
	{
		$buffer = $buffer . $blob;
	}
	$stmt->finish();

	return pack("H*", $buffer);
}

