#---------------------------------------------------------------------------------------------
# Change History
#
# Version       Date            Programmer      Description
# -------       ----------      ----------      ----------------------------------------------
# 1.00          06/20/2005      B Krug          Copied from USANet3 but completely re-wrote the
#												workings of this, since Legacy G4's and BEX devices
#                                               send an ordered set of data instead of commands, per se
#

package Logic;

use strict;

use IO::Select;
use IO::Handle;
use IO::Socket;
use Time::HiRes qw(sleep time);
use Time::Local qw(timelocal);
use USAT::Database;        # Access to the Database
use USAT::NetLayer::Session;
use USAT::NetLayer::Utils qw(LogIt);
use USAT::Common::CallInRecord;
use SendReceive qw(receiveData sendData waitForData sendAck);

#CONSTANTS
my $STX = chr(2);
my $ETX = chr(3);
my $EOT = chr(4);
my $ENQ = chr(5);
my $ACK = chr(6);
my $EOC = chr(7);
my $SO  = chr(15);
my $NAK = chr(21);
my $SI  = chr(33);

my $DEBUG = 1;    
my $RERIX_TIMEOUT = 20.0;

# Using completely different logic for order-based legacy device 
sub Run ($$$) {
	my $sock = shift;
	$| = 1; # auto-flush output

	my $dbconfig = shift;
	my %call_info = ();
	
	$call_info{net_layer_id} = shift;	
	$call_info{DATABASE} = USAT::Database->new(config => $dbconfig);
	
	$call_info{remote_addr} = $sock->peerhost;
	$call_info{port} = $sock->peerport;
	$call_info{call_id} = "$call_info{remote_addr}:$call_info{port}";

	my $in_msg_count = 0;
	my $out_msg_count = 0;
	$call_info{read_byte_count} = 0;
	$call_info{write_byte_count} = 0;
	
	$call_info{read_set} = new IO::Select();
	$call_info{write_set} = new IO::Select();
	$call_info{read_set}->add($sock);
	$call_info{write_set}->add($sock);
	
	my $success = 0;
	
	LogIt("$call_info{call_id}\t" . "Connected from $call_info{remote_addr}:$call_info{port}");
	
	if(logon(\%call_info)) {
		flushOutboundQueue($call_info{DATABASE}, $call_info{machine_id});
	   	$call_info{msg_no} = 1;
		if(exchange_config(\%call_info)) {
			if(get_transactions(\%call_info)) {
				if(get_dex(\%call_info)) {
					LogIt("$call_info{call_id}\t" . "Call completed!");
					$success = 1;
				}
			}
		}
		my @wh_set = $call_info{write_set}->can_write(0);
		if($wh_set[0]) {
			$wh_set[0]->print($EOT x 3);
			$call_info{write_byte_count} += 3;
		}
		
	    # finish call record
		&USAT::Common::CallInRecord::set_status($call_info{DATABASE}, $call_info{machine_id}, 'S') if($success);
		USAT::NetLayer::Session::finish($call_info{DATABASE}, $call_info{session_id}, $call_info{msg_no}, $call_info{msg_no}, $call_info{read_byte_count}, $call_info{write_byte_count}) if($call_info{session_id});
		&USAT::Common::CallInRecord::finish($call_info{DATABASE}, $call_info{machine_id});		    		
	}
	$sock->close(); 
    return;	
}

sub logon(%;) {
	my ($call_info) = @_;
    # get logon
	LogIt("$call_info->{call_id}\t" . "Getting Serial");
	$call_info->{serial} = receiveData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, 3);
	if(!$call_info->{serial}) {
		LogIt("$call_info->{call_id}\t" . "Failure obtaining Serial Number");
		return undef;
	}

	add_device_info($call_info);
	if(!$call_info->{machine_id}) {
		LogIt("$call_info->{call_id}\t" . "Invalid Serial Number '$call_info->{serial}'");	
		return undef;
	}			
	my $session_id;
	#record communication
	&USAT::Common::CallInRecord::start($call_info->{DATABASE}, $call_info->{machine_id}, 'USANet1', $call_info->{remote_addr});
	USAT::NetLayer::Session::start($call_info->{DATABASE}, \$session_id, $call_info->{net_layer_id}, $call_info->{machine_id}, $call_info->{remote_addr}, $call_info->{port});
	$call_info->{session_id} = $session_id;    	
	return 1;
}

sub exchange_config(%;) {
    my ($call_info) = @_;
    #get config data (x2)
    LogIt("$call_info->{call_id}\t" . "Getting Config") if($DEBUG);
    sleep(0.5); #wait a short time
    my $config = receiveData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, 5, $call_info->{serial} . ($ACK x 3), 2, qr/(.{374})\x1C\d{12}\x0D(.*)/s, 45, $call_info->{serial} . ($NAK x 3)); # two copies to be safe
    #my $config = receiveData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, 5, $call_info->{serial} . ($ACK x 3), 2, qr/(.*)\x1C\d{12}\x0D(.*)/s, 45); # two copies to be safe
    if(!defined($config) || $config =~ m/$EOC|$SO/so) {	
    	LogIt("$call_info->{call_id}\t" . "Failed to receive client config data");
    	return undef; 
    } 

	#pull receipt value from config
	$call_info->{receipt} = substr($config, 261, 1);
	LogIt("Receipt = `$call_info->{receipt}'");
	# I belive we can get the canada flag from  the software version
	$call_info->{canada} = undef; #TODO: implement this
    
	#store config and get new config
    LogIt("$call_info->{call_id}\t" . "Storing Config") if($DEBUG);
    if(!storeConfig($config, $call_info)) {
    	LogIt("$call_info->{call_id}\t" . "Failed to store the client config data");
    	return undef;
	}

	LogIt("$call_info->{call_id}\t" . "Generating new Config") if($DEBUG);
	$config = getNewConfig($config, $call_info);
	if(!$config) {
    	LogIt("$call_info->{call_id}\t" . "Failed to retrieve the new config data");
    	return undef;
	}

	#wait for ACK - to ensure that client is now ready to receive config
    LogIt("$call_info->{call_id}\t" . "Waiting for ACK") if($DEBUG);
    if(!waitForData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, $ACK, $call_info->{serial}.$ACK x 3, 10, 10)) {
        LogIt("Failed to receive an acknowledgement that client is ready for data");
        return undef;
    } 

	#send config
    LogIt("Sending Config") if($DEBUG);
    if(!sendData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, $call_info->{serial}, $config, 5)) {
        LogIt("$call_info->{call_id}\t" . "Failed to send the new config data"); 
        return undef;
    } 

	#send coupon data
    LogIt("$call_info->{call_id}\t" . "Generating Coupon") if($DEBUG);
    my $coupon = getNewCoupon($call_info);#PARAMS
    if(!defined($coupon)) {
    	LogIt("$call_info->{call_id}\t" . "Failed to retrieve the new coupon data");
    	return undef;
	}
	
    LogIt("Sending Coupon") if($DEBUG);
    if($coupon and !sendData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, $call_info->{serial}, $coupon, 5)) {
        LogIt("$call_info->{call_id}\t" . "Failed to send the new coupon data"); 
		return undef;
	}
	
	return 1;
}
	
sub get_transactions(%;) {
	my ($call_info) = @_;
    my $cnt = receiveTransactions($call_info);
	if(!defined($cnt)) { # could be zero
		LogIt("$call_info->{call_id}\t" . "Failed to receive the transaction data");
		return undef; 
	} 
	return 1;
}

sub get_dex(%;) {
	my ($call_info) = @_;
    #get DEX data
    LogIt("$call_info->{call_id}\t" . "Receving DEX Data") if($DEBUG);
    my $dex = receiveData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, 5, $ENQ, 1, undef, 60);
    if(!$dex) {
        LogIt("$call_info->{call_id}\t" . "Failed to receive the DEX data"); 
        return undef;
    } elsif($dex !~ m/^($SO|$EOT)/so) {
    	if(!saveDex($dex, $call_info)) {
	        LogIt("$call_info->{call_id}\t" . "Failed to save the DEX data");
	        return undef; 
    	}
    	&USAT::Common::CallInRecord::dex_received($call_info->{DATABASE}, $call_info->{machine_id}, length($dex));
    } 

	if($dex !~ m/^($EOT)/so) {
		LogIt("$call_info->{call_id}\t" . "Sending Acknowledgement") if($DEBUG);
	    if(!waitForData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, $EOC, $ACK x 3, 3, 6)) {
	    	LogIt("$call_info->{call_id}\t" . "Did not receive EOC from client"); #XXX: do we need to check success of this?
	    	return 1;
	    }
    } else {
    	LogIt("$call_info->{call_id}\t" . "Sending Acknowledgement") if($DEBUG);
	    sendAck($call_info->{write_set}, \$call_info->{write_byte_count}, $ACK x 3);
	}
	LogIt("$call_info->{call_id}\t" . "Call completed!");
    return 1;
}

sub receiveTransactions(%;) {
    my ($call_info) = @_;
    my $x = receiveData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, 5, $ENQ);
    my $cnt = 0;
    my @oldXs = ();
    LogIt("$call_info->{call_id}\t" . "Receiving Transactions") if($DEBUG);						    
    while(1) {
    	LogIt("Transaction Count = $cnt");
        if(!defined($x)) {
            LogIt("Timeout waiting for next Transaction!") if($DEBUG);
            return undef;
        } elsif($x =~ m/$EOC/so) {
            LogIt("Received $cnt transactions!") if($DEBUG);
            return $cnt;
        } else {
            my $result = saveTransaction($x, $call_info);
            if(!$result) {
                LogIt("Couldn't save transaction: $x") if($DEBUG);
                return;
            } elsif($result == -2) { #transaction did not match pattern
                if(scalar(@oldXs) and eval {
                    foreach my $oldX (@oldXs) {
                        if($oldX eq $x) { return 1; }
                    }
                }) {
                    #return saveInvalidTransaction($x); #PARAMS
                    LogIt("Received a transaction from $call_info->{machine_id} that is invalid: $x");
                } else {
                    push(@oldXs, $x);
                    $x = receiveData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, 5, $NAK x 3);
                    next;
                }
             } else {
             	$cnt++;
             }
        } 
        @oldXs = (); #reset copies of transaction
        $x = receiveData($call_info->{read_set}, $call_info->{write_set}, \$call_info->{read_byte_count}, \$call_info->{write_byte_count}, 5, $ACK x 3);
    }
}

# returns the machine id (device_name) of the device with the given serial number or undef if not found in
# the database; NOTE: We should consider avoiding use of the database and instead storing the EV number in
# the location spot of the EEROM
sub add_device_info(%;) {
	my ($call_info) = @_;
	my $array_ref = $call_info->{DATABASE}->select(
							table          => 'device d',
							select_columns => 'd.device_name, d.device_type_id',
							where_columns  => [ 'device_serial_cd = ?', 
												'device_active_yn_flag = ?'],
							where_values   => [ $call_info->{serial}, 'Y'] );
							
	if($array_ref->[0])	{
		LogIt("Found device with machine_id=".$array_ref->[0][0]);
		$call_info->{machine_id} = $array_ref->[0][0];
		$call_info->{device_type} = $array_ref->[0][1];
		return 1;
	} else {
		return undef;
	}
}

sub storeConfig($%;) {
	my ($config, $call_info) = @_;
	my @h_sections = ((82,161),(172,194),(198,201),(203,207),(213,225),(240,359),(362,369),(511,511));
	my $new_config = "";
	my $pos = 0;
	my $add = 0;			
	for(my $i = 0; $i < $#h_sections; $i += 2) {
		my ($start, $end) = @h_sections[$i..$i+1];
        $start += $add;
		last if($start >= length($config) || $pos >= length($config));
		$new_config .= substr($config, $pos, $start - $pos);
		$end += $add;
		$add += (($end + 1) - $start);
		$pos = $end + $add + 1;
		$new_config .= pack("H*", substr($config, $start, $pos - $start));
		LogIt("Start = $start; End = $end; Pos = $pos; Len = ".length($new_config)) if($DEBUG);
	}
	$new_config .= substr($config, $pos) if($pos < length($config));	
	#LogIt("Config file:\n$new_config") if($DEBUG);		
	return storeFile($call_info->{machine_id} . "-CFG", $new_config, 1, $call_info);
}

sub getNewConfig($%;) {
	my ($old_config, $call_info) = @_;
	# look for file transfer of config in outbound
	my $expect = "[0-9A-F]{2}7C([0-9A-F]{2})([0-9A-F]{8})([0-9A-F]{2})".unpack("H*", pack("Ca*",
		1, #file type
		$call_info->{machine_id} . "-CFG"));
	my @match = matchOutbound(qr/^$expect$/i, 0, $call_info);
	if(@match) {
		# send ack
		my $cmd = "7D$match[2]";
		queueInbound($call_info->{DATABASE}, $call_info->{machine_id}, $call_info->{session_id}, $call_info->{msg_no}++, $cmd);
		my $config = "";
		my $num = unpack("C", pack("H2", $match[0]));
		for(my $i = 1; $i <= $num; $i++) {
			my $i_packed = unpack("H*", pack("C", $i));
			$expect = "[0-9A-F]{2}7E$match[2]$i_packed([0-9A-F]*)";
			my @file = matchOutbound(qr/^$expect$/i, $RERIX_TIMEOUT, $call_info);
			if(!@file) {
				#TODO: put outbound config file transfer back in queue somehow
				LogIt("$call_info->{call_id}\t" . "ERROR - Did not find config file packet $i in the outbound queue");   
				return fix_old_config($old_config, $call_info); #let's not blow the whole call if we fail to get a config file
			}
			$config .= $file[0];
			# send ack
			$cmd = "7F$match[2]$i_packed";
			queueInbound($call_info->{DATABASE}, $call_info->{machine_id}, $call_info->{session_id}, $call_info->{msg_no}++, $cmd);		
		}
		# check config length
		my $length = unpack("N", pack("H8", $match[1]));
		if(length($config) != $length) {
			#TODO: put outbound config file transfer back in queue somehow
			LogIt("$call_info->{call_id}\t" . "ERROR - Config File is wrong length (".length($config)." instead of $length");   
			return fix_old_config($old_config, $call_info); #let's not blow the whole call if we fail to get a config file
		} else {
			LogIt("$call_info->{call_id}\t" . "Successfully retrieved new config file from server");
			my @h_sections = ((82,161),(172,194),(198,201),(203,207),(213,225),(240,359),(362,369),(511,511));
			my $new_config = "";
			my $pos = 0;			
			foreach my $s_e (@h_sections) {
				my ($start, $end) = $s_e;
				last if($start > length($config));
				$new_config .= substr($config, $pos, $start - $pos);
				$pos = $end + 1;
				$new_config .= unpack("H*", substr($config, $start, $pos - $start));
			}
			$new_config .= substr($config, $pos);
			return $new_config;
		}			
	} else {
		return fix_old_config($old_config, $call_info);
	}
}

sub fix_old_config($;) {
	my ($old_config, $call_info) = @_;
	# parse config
	my $config = substr($old_config, 22, 352)."\x1C".get_device_time_str($call_info);	
	return $config;
}

sub get_device_time_str(%;) {
	my ($call_info) = @_;
    # Call rerix with message 82h to get unix time
    my ($cmd, $expect) = ("8200", "([0-9A-F]{2})84([0-9A-F]{8})");       
	queueInbound($call_info->{DATABASE}, $call_info->{machine_id}, $call_info->{session_id}, $call_info->{msg_no}++, $cmd);
    my @match = matchOutbound(qr/^$expect$/i, $RERIX_TIMEOUT, $call_info);
    if(!@match || !$match[1]) {
    	return undef;
    }
    my $time = unpack("N", pack("H*", $match[1]));
	my ($sec,$min,$hour,$mday,$mon,$year) = localtime($time);
	# ACK the server command
	queueInbound($call_info->{DATABASE}, $call_info->{machine_id}, $call_info->{session_id}, $call_info->{msg_no}++, "2F".$match[0]);	
    return sprintf('%02d%02d%02d%02d%02d%02d', $mon+1, $mday, $year % 100, $hour, $min, $sec); 
}

sub getNewCoupon(%;) {
	my ($call_info) = @_;
	if($call_info->{serial} =~ /^(E|G)/) {
		#TODO: get coupon from database
		return chr(26) x 3;
	} else {
    	return "";
	}
}

sub saveTransaction($%;) {
    my ($x, $call_info) = @_;
    LogIt("$call_info->{call_id}\t" . "Saving transaction for $call_info->{machine_id}: $x") if($DEBUG);
    #parse the data
    my ($track_data, $merch_ndx, $amt, $apcode, $type, $date1, $prtcnt1, $date2, $prtcnt2, $specific) = 
        $x =~ /^([\x20-\x7E]{37})([\x20-\x7E]{1})(\d{5})([\x20-\x7E]{8})([1-7]{1})(\d{8})([\x20-\x7E]{4})(?:(\d{8})([\x20-\x7E]{4})(\d{24}))?$/o;
    unless($type) {
        LogIt("$call_info->{call_id}\t" . "Incorrect transaction format: '$x'");
        return -2; #signify no match
    }
    
    #determine settle state , card type - C=Credit card; S=Special card; I=Inventory card, inventory code, or fill button pressed; E=Error message
    my ($card_type, $state, $tran_result);
    if($type == 1) { $card_type = "C"; $state = "A"; } # credit already pre-authed, needs settlement
    elsif($type == 2) { LogIt("$call_info->{call_id}\t" . "Received settlement record for '$call_info->{machine_id}'"); return 0;} # settlement record - record where? 
    elsif($type == 4) { $card_type = "C"; $state = "S"; } #credit card - settled already
    elsif($type == 7) { LogIt("$call_info->{call_id}\t" . "Received email record for '$call_info->{machine_id}'"); return 0; } # email record - record where? 
    elsif($type == 3 and $apcode =~ /^APG4/o) { $card_type = "C"; $state = "L"; } #local auth
    elsif($type == 3 and ($apcode =~ /^\d{2}G4/o && ($call_info->{canada} || $call_info->{serial} =~ m/^D[CPF]/))) { $card_type = "C"; $state = "L"; } #local auth
    elsif($track_data =~ /^Currency Vend/io) { $card_type = "H"; $state = "L"; } # cash
    elsif($track_data =~ /^Err\.DEX Fill/o) { $card_type = "I"; $state = "L"; }# FILL record
    elsif($track_data =~ /^Err/) { $card_type = "E"; $state = "L";  
    	#if($call_info->{canada} and substr($track_data, 0, 11) eq "Error   -->") {
        #    $track_data = substr($track_data, 11, 24);
        #} else {
        #    $err_serial = substr($track_data, 16, 8);
        #    $track_data = substr($track_data, 0, 13);
        #}
    } #error record
    else { $card_type = "S"; $state = "L"; } #passcard	
    
    #build detail and calc seconds for time based on device type
    my $detail;
    my $time;
    if($call_info->{device_type} =~ m/^(9|0|1)$/) { #Legacy G4
    	if($card_type eq "H") { # cash - we only need mdb of first column vended
    		$detail = pack("C", (defined $specific ? substr($specific, 2, 2) : 0));
    	} else { # card
    		my ($vends, @cols) = (0);
    		if(defined $specific) {
    			($vends, @cols) = $specific =~ /^(\d{2}){12}/;
    		}
    		$detail = pack("C", $vends);
    		my $amt_str = unpack("xH6", pack("N", $amt));
		    for(my $i = 0; $i < $vends; $i++) {
		    	$detail .= pack("CH6", $cols[$i] || 0, ($vends == 1 ? $amt_str : "\0\0\0"));
		    }
    	}
		#calculate the transaction date
    	$time = calc_tran_time($date1, substr($date1,6,2) eq substr($date2, 4, 2) ? substr($date2, 6, 2) : "00");
    } elsif($call_info->{device_type} =~ m/^(10)$/) { #Transacts
    	#calculate the transaction date
    	$time = calc_tran_time($date1, "00");
    	my $end_time = calc_tran_time(substr($date1, 0, 4).substr($date2, 4, 4), "00");
    	my @cnts = $specific =~ /^(\d{4}){6}/;   
    	my $duration = (($end_time - $time) / 60) % (24 * 60); 	
    	if($call_info->{serial} =~ m/^D[CPF]/) {
    		if($call_info->{canada}) {
    			$prtcnt1 .= substr($date2, 0, 2);
    		} else {
    			$prtcnt1 .= substr($apcode, 0, 2);
    		}
    	}
    	$detail = "A1|$duration|$prtcnt1|$prtcnt2";
    	for(my $i = 0; $i < 6; $i++) {
    		$detail .= "|".(ord($cnts[$i]) || 0);
    	}	
    }

    # NOTE: We assume that if the receipt value is set then a receipt really was printed
    if($card_type eq "I" || $card_type eq "E") {
    	$tran_result = " ";
    } elsif($amt == 0) {
    	$tran_result = "C";
    } elsif($call_info->{receipt} eq "Y") {
    	$tran_result = "S";
    } else {
    	$tran_result = "Q";
    }
	
	#format command
	my $tran_id = $time;
    my $cmd = buildTransCommand($call_info, $tran_id, $card_type, $state, $tran_result, $track_data, $amt, $time, $detail);
    
    #send transaction to inbound queue
    queueInbound($call_info->{DATABASE}, $call_info->{machine_id}, $call_info->{session_id}, $call_info->{msg_no}++, $cmd);
    
    #wait for response
    my ($expect, $success_value);
    if($card_type eq "H") {
    	$expect = "[0-9A-F]{2}95".unpack("H*", pack("N", $tran_id));
	} else {
		$expect = "[0-9A-F]{2}71".unpack("H*", pack("N", $tran_id))."(00|01|02)";
		$success_value = qr/^(01|00)$/; #XXX: should 0 (Fail) also be accepted???
	} 
    return waitForOutbound(qr/^$expect$/i, $success_value, $RERIX_TIMEOUT, $call_info);    
}

#formats a transaction into a rerix command string
sub buildTransCommand(%$$$$$$$$@) {
    my ($call_info, $tran_id, $card_type, $state, $tran_result, $track_data, $amt, $time, $detail) = @_;
    LogIt("Building Trans Command with id=$tran_id; Card Type=$card_type; State=$state; Result=$tran_result; Amount=$amt; Time=$time; Track Data Length=".length($track_data));
	LogIt("Device Type=".$call_info->{device_type}."; Tran Detail=$detail;");
    # $cmd_type = A2h (Net) or A3h (local) or 96h (Cash)
    
    if($card_type eq "C" && $state eq "N") { # Net batch
	    return unpack("H*", pack("H2NNaa*", 
	    	"A2", # command
	    	$tran_id,
	    	$amt, #amount
	    	$tran_result, #tran_result
	   	    $detail));
	} elsif($card_type eq "H") { # cash
	    my $amt_str = unpack("xH6", pack("N", $amt));
    	return unpack("H*", pack("H2NNH6a*", 
	    	"96", # command
	    	$tran_id,
	    	$time,
	    	$amt_str, #amount
	    	$detail));	
    } else {
    	if($card_type eq "C" && $state eq "S") { # Settled batch - very bad
	    	LogIt("$call_info->{call_id}\t" . "Transaction $tran_id for \$$amt on $call_info->{machine_id} is SETTLED - adding as local batch with card_type of 'A'");
	    	$card_type = "A";
	    }
		return unpack("H*", pack("H2NNaNC/a*aa*", 
	   		"A3", # command
	    	$tran_id, 
	    	$time, #date & time
	    	$card_type, #cardType - C=Credit card; S=Special card; I=Inventory card, inventory code, or fill button pressed; E=Error message
	    	$amt, #amount
	    	$track_data, #Credit Card Magstripe
	    	$tran_result, #tran_result
	   	    $detail));
    }
}

sub buildTransCommand_OLD(%$$$$$$$$@) {
    my ($call_info, $tran_id, $card_type, $state, $tran_result, $track_data, $amt, $time, $vends, @cols) = @_;
    if($call_info->{device_type} eq '9') { #Legacy G4
    
    } elsif($call_info->{device_type} eq '10') { #BEX
    
    }
    # $cmd_type = 2Ah (Net) or 2Bh (local) or 96h (Cash)
    my $receipt = 0; #REMIND: Get this from the config
 	my $tax = 0;
    my $amt_str = unpack("xH6", pack("N", $amt));
    my $detail = pack("C", $vends);
    for(my $i = 0; $i < $vends; $i++) {
    	$detail .= pack("CH6", $cols[$i] || 0, ($vends == 1 ? $amt_str : "\0\0\0"));
    }
    if($card_type eq "C" && $state eq "N") { # Net batch
	    return unpack("H*", pack("H2NH6vaa*", 
	    	"2A", # command
	    	$tran_id,
	    	$amt_str, #amount
	    	$tax,
	    	$tran_result, #tran_result
	   	    $detail));
	} elsif($card_type eq "H") { # cash
	    return unpack("H*", pack("H2NNH6C", 
	    	"96", # command
	    	$tran_id,
	    	$time,
	    	$amt_str, #amount
	    	$cols[0]));	
    } else {
    	if($card_type eq "C" && $state eq "S") { # Settled batch - very bad
	    	LogIt("$call_info->{call_id}\t" . "Transaction $tran_id for \$$amt on $call_info->{machine_id} is SETTLED - adding as local batch with card_type of 'A'");
	    	$card_type = "A";
	    }
		return unpack("H*", pack("H2NNaH6vC/a*aa*", 
	   		"2B", # command
	    	$tran_id, 
	    	$time, #date & time
	    	$card_type, #cardType - C=Credit card; S=Special card; I=Inventory card, inventory code, or fill button pressed; E=Error message
	    	$amt_str, #amount
	    	$tax, 
	    	$track_data, #Credit Card Magstripe
	    	$tran_result, #tran_result
	   	    $detail));
    }
}

#takes a date string formated at MMddhhmm and a seconds value and turns it into seconds since the epoch 
sub calc_tran_time($$;) {
	my ($date_str, $sec) = @_;
	my ($min, $hour, $mday, $mon) = (substr($date_str,6,2), substr($date_str,4,2), substr($date_str,2,2), substr($date_str,0,2) - 1);
    my $year = (localtime())[5];
    my $time = timelocal($sec, $min, $hour, $mday, $mon, $year + 1);
    #make sure time is in the past
    if($time > time() + (48 * 60 * 60)) { $time = timelocal($sec, $min, $hour, $mday, $mon, $year); }
	return $time;
}

sub saveDex($%;) {
    my ($dex, $call_info) = @_;
    LogIt("$call_info->{call_id}\t" . "Saving DEX Data...") if($DEBUG);
    
    if($dex eq "DEX-") {
        LogIt("$call_info->{call_id}\t" . "No DEX Data");
        return 2;
    }
    my ($dex_serial, $dex_time, $dex_date, $dex_type, $data) = 
        $dex =~ /^DEX-(\w*)-(\d{6})-(\d{6})-(\w*)\x0D\x0A/o;
    unless($dex_serial) {
        LogIt("$call_info->{call_id}\t" . "Incorrect DEX format: '$dex'");
        return;
    }
    $data = $'; #'substr($dex, $+[0]);
    
    unless($dex_serial eq $call_info->{serial}) {
        LogIt("$call_info->{call_id}\t" . "DEX serial number, '$dex_serial' does not match logon serial, '$call_info->{serial}'");
        return;
    }

    # determine type and save filename in database
    #my $type = 0;
    #for($dex_type) {
    #    $type = /SCHEDULED/ ? 1 :
    #            /INTERVAL/  ? 2 :
    #            /FILL/      ? 3 :
    #            /ALARM/     ? 4 :
    #            0;
    #}

    my $filename = "DEX-$dex_serial-$dex_time-$dex_date-$dex_type.log";
    
    return storeFile($filename, $data, 0, $call_info);
}

#ReRix Interaction Commands
sub flushOutboundQueue($$;) {
	my ($DATABASE, $machine_id) = @_;
	# remove any existing responses for this ID
	LogIt("Flushing outbound command queue for $machine_id...");
	$DATABASE->update(
		table			=> 'machine_command',
		update_columns	=> 'execute_cd',
		update_values	=> ['E'],
		where_columns  => ['modem_id = ?'],
		where_values   => [$machine_id]);
}

sub queueInbound($$$$$;) {
	my ($DATABASE, $machine_id, $session_id, $msg_no, $msg) = @_;
	LogIt("Inserting $machine_id $msg_no $msg");
	$DATABASE->insert(
		table			=> 'machine_command_inbound',
		insert_columns	=> 'machine_id, inbound_command, inbound_msg_no, session_id',
		insert_values	=> [$machine_id, $msg, $msg_no, $session_id]);
	&USAT::Common::CallInRecord::add_message($DATABASE, $machine_id, "I", length($msg));	
}

sub storeFile($$%;) {
	my ($filename, $contents, $filetype, $call_info) = @_;
	my $MAX_PACKET_SIZE = 4000;
	my $max_content_length = ($MAX_PACKET_SIZE - 6) / 2;
	# send file transfer start
	my $transfer_grp_num = $call_info->{session_id} % 256; #XXX: Will this work?
	use POSIX; # so we can use the ceil function
	my $num_packets = ceil(length($contents) / $max_content_length);
	my $cmd = unpack("H*", pack("H2CNCCa*",
		"7C", # command
		$num_packets, # num of packets
		length($contents),
		$transfer_grp_num, #File Transfer Group Number - should be unique
		$filetype, #file type
		$filename));
	queueInbound($call_info->{DATABASE}, $call_info->{machine_id}, $call_info->{session_id}, $call_info->{msg_no}++, $cmd);
	my $expect = "[0-9A-F]{2}7D".unpack("H*", pack("C", $transfer_grp_num));
	if(!waitForOutbound(qr/^$expect$/i, undef(), $RERIX_TIMEOUT, $call_info)) {
		return undef;
	}
	
	#now send file
	for(my $pkt = 0; $pkt < $num_packets; $pkt++) {
		my ($offset, $len) = ($max_content_length * $pkt, $max_content_length);
		$len = length($contents) - $offset unless($max_content_length * $pkt > length($contents));
		$cmd = unpack("H*", pack("H2CCa*",
			"7E", # command
			$transfer_grp_num, #File Transfer Group Number - should be unique
			$pkt, # packet number
			substr($contents, $offset, $len)));
		queueInbound($call_info->{DATABASE}, $call_info->{machine_id}, $call_info->{session_id}, $call_info->{msg_no}++, $cmd);
		$expect = "[0-9A-F]{2}7F".unpack("H*", pack("CC",
			$transfer_grp_num, #File Transfer Group Number - should be unique
			$pkt)); # packet number
		return undef unless(waitForOutbound(qr/^$expect$/i, undef, $RERIX_TIMEOUT, $call_info));
	}
	return $num_packets;
}

sub matchOutbound($$%;) {
	my ($expect, $timeout, $call_info) = @_;
	my $done = time() + $timeout;
	while(1) {
		my $outbound_command_ref = $call_info->{DATABASE}->select(
						table			=> 'Machine_Command',
						select_columns	=> 'COMMAND_ID, COMMAND',
						order			=> 'Command_ID',
						where_columns	=> ['Execute_cd = ?', 'modem_id = ?'],
						where_values	=> ['N', $call_info->{machine_id}]
					);
		my @match;
		if($outbound_command_ref->[0]) { 
			foreach my $outbound_row (@$outbound_command_ref) {
				my ($out_command_id, $out_command) = @{$outbound_row};
				if(@match = ($out_command =~ $expect)) {
					LogIt("Found outbound match on command $out_command_id of $expect: $out_command");			
					$call_info->{DATABASE}->update(
									table			=> 'Machine_Command',
									update_columns	=> 'Execute_Cd',
									update_values	=> ['Y'],
									where_columns	=> ['Command_ID = ?'],
									where_values	=> [$out_command_id]);
					#LogIt("Match: (".join(";", @match).")");
					&USAT::Common::CallInRecord::add_message($call_info->{DATABASE}, $call_info->{machine_id}, "O", length($out_command));	# This is not really the true byte count & num of messages sent to client				
					return @match;
				}
			}
		}
		if($done <= time()) {
			LogIt("Timeout while waiting $timeout seconds for $expect in outbound queue");
			return ();
		}
		sleep(0.5);
	}
}

sub waitForOutbound($$$%) {
	my ($expect, $success_value, $timeout, $call_info) = @_;
	my @match = matchOutbound($expect, $timeout, $call_info);
	return (@match && (!defined($success_value) || $match[0] =~ $success_value));
}

sub clearOutbound($%;) {
	my ($expect, $call_info) = @_;
	my $cnt = 0;
	my $outbound_command_ref = $call_info->{DATABASE}->select(
					table			=> 'Machine_Command',
					select_columns	=> 'COMMAND_ID, COMMAND',
					order			=> 'Command_ID',
					where_columns	=> ['Execute_cd = ?', 'modem_id = ?'],
					where_values	=> ['N', $call_info->{machine_id}]
				);
	my @match;
	if($outbound_command_ref->[0]) { 
		foreach my $outbound_row (@$outbound_command_ref) {
			my ($out_command_id, $out_command) = @{$outbound_row};
			if(@match = ($out_command =~ $expect)) {
				LogIt("Clearing outbound command $out_command_id: $out_command");			
				$call_info->{DATABASE}->update(
								table			=> 'Machine_Command',
								update_columns	=> 'Execute_Cd',
								update_values	=> ['E'],
								where_columns	=> ['Command_ID = ?'],
								where_values	=> [$out_command_id]);
				$cnt++;
			}
		}
	}
	return $cnt;
}

1;
