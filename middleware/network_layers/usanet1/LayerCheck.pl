#!/usr/bin/perl
use strict;
use Utils;

use IO::Socket;
use IO::Select;
use Net::SMTP;
use USAT::Database;        # Access to the Database

my $DATABASE = USAT::Database->new(
					print_query			=> 1, 
					execute_flatfile	=> 1, 
					debug				=> 1, 
					debug_die			=> 0);

my $machine = 'EV017289';
my $msg = '135C';

my $packed = pack("H*", $msg);
my $key = &getKey($machine, $DATABASE);
my $encrypted = $machine . PackEncrypted($packed, $key);
my $encoded = uuencode($encrypted) . "\x0a";

my $socket = new IO::Socket::INET (PeerAddr => 'localhost:14107');
my $select = new IO::Select();

if(!defined $socket )
{
	&send_alert("Failed to connect");
	exit;
}

$select->add($socket);
	
if($socket->send($encoded) != length($encoded))
{
	&send_alert("Failed to send request");
	$socket->close();
	exit;
}

my $responseBuffer;
$socket->recv($responseBuffer, 500);
my $response .= $responseBuffer;

if(length($response) == 0)
{
	&send_alert("No response!");
	$socket->close();
	exit;
}

my $decoded = uudecode($response);
my ($result, $decrypted) = @{UnpackEncrypted($decoded, $key)};

if($result ne '1')
{
	&send_alert("Failed to decrypt response: $response");
	$socket->close();
	exit;
}

$response = unpack("H*", $decrypted);

LogIt($response);

my $expected_response = "2f13";
my $received_response = substr($response, 2);

if($expected_response eq $received_response)
{
	LogIt("OK");
}
else
{
	LogIt("FAILED");
	&send_alert("Did not receive expected response: $response");
}

sub send_alert
{
	my ($info) = @_;
	&send_email('rerix_engine@usatech.com','pcowan@usatech.com,4844671281@mobile.att.net,mheilman@usatech.com,4844674506@mobile.att.net','ReRix USANet1 Layer Failure!', localtime() . " - ALERT! $info\n");
	LogIt("ALERT! $info");
}

sub getKey
{
	my ($machineID, $MYDATABASE) = @_;
	
	my $array_ref = $MYDATABASE->select(
							table          => 'device',
							select_columns => 'encryption_key',
							where_columns  => [ 'device_name = ?' ],
							where_values   => [ $machineID ]
					);
							
	if(not $array_ref->[0])
	{
		return 0;
	}
	else
	{
		return $array_ref->[0][0];
	}
}

sub send_email
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = 'owa.usatech.com';
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}
