#---------------------------------------------------------------------------------------------
# Change History
#
# Version       Date            Programmer      Description
# -------       ----------      ----------      ----------------------------------------------
# 1.00          08/26/2005      B Krug          Broke this module out from Logic												workings of this, since Legacy G4's and BEX devices
#

package SendReceive;

use strict;

use IO::Select;
use IO::Handle;
use IO::Socket;
use Time::HiRes qw(sleep time gettimeofday);
#use USAT::NetLayer::Utils qw(LogIt);
use vars qw (@ISA @EXPORT_OK);
require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = qw(receiveData sendData waitForData sendAck LogIt);

#CONSTANTS
my $STX = chr(2);
my $ETX = chr(3);
my $EOT = chr(4);
my $ENQ = chr(5);
my $ACK = chr(6);
my $EOC = chr(7);
my $SO  = chr(15);
my $NAK = chr(21);
my $SI  = chr(33);

my $READ_SIZE = 120;
my $DEAD_AIR_TIMEOUT = 6 + ($READ_SIZE / 100); 
my $DEBUG = 0;    

sub sendAck($$;$) {
    my ($write_set, $write_byte_count, $ack) = @_;
    $ack ||= $ACK x 3;
    my @wh_set = $write_set->can_write(0);
    my $wh_fh = $wh_set[0];
    if($wh_fh) {
        printData($wh_fh, $ack);
        $write_byte_count += length($ack);
        return 1;
    }   
}

#send STX + $data + ETX + LCR until ACK, EOT or tries is up
sub sendData($$$$$$;$) {
    my ($read_set, $write_set, $read_byte_count, $write_byte_count, $serial, $data, $tries) = @_;
    $tries ||= 1;
    my $lcr = lcr($data);
    $serial = substr($serial, length($serial) - 6); # only use last 6 numbers of serial
	        
    for(my $i = 0; $i < $tries; $i++) {
        flushInput($read_set);
        my @wh_set = $write_set->can_write(0);
        my $wh_fh = $wh_set[0];
        if($wh_fh) {
	        printData($wh_fh, $STX.$data.$ETX.chr($lcr));
	        $write_byte_count += length($data) + 3;
	        LogIt("Sent data; waiting for response...") if($DEBUG);
	        my ($retACK, $retNAK, $retEOT);
	        #($retACK, $retNAK, $retEOT) = waitForData("(${serial}${ACK})|(${serial}${NAK})|($EOT)", undef,10,10);
	        #($retACK, $retNAK, $retEOT) = waitForData($read_set, $write_set, \$read_byte_count, \$write_byte_count, qr/($serial$ACK)|($serial$NAK)|($EOT{3})/so, undef,10,10);
	        ($retACK, $retNAK, $retEOT) = waitForData($read_set, $write_set, \$read_byte_count, \$write_byte_count, qr/($ACK)|($NAK)|($EOT{3})/so, undef,10,10);
	        if($retACK) { #success - we are done
	            LogIt("Transmission Acknowledged!") if($DEBUG);
	            return $retACK;
	        } elsif($retEOT) { #failure - we are done
	            LogIt("End-of-Transmission received") if($DEBUG);
	            return undef;
	        }
        } else {
        	LogIt("No write handle is available");
        }
    }
    LogIt("Failed to receive ACK for sent data after $tries tries.") if($DEBUG);
    return undef;    
}

# gets data by sending the enq character and waiting for STX + data + ETX + LCR,
# then checks LCR and returns data if all is ok
{
    my $data_re = qr/(?:$STX([\x0A\x0D\x1A\x1C\x20-\x7E]*)$ETX(.))|($EOT{3})|($EOC{3}|$SO{3})/s;
    #my $data_re = qr/(?:$STX([\x0A-\x7E]*)$ETX(.))|($EOT)|($EOC|$SO)/so;
    # Arguments: read IO::Select object, write IO::Select Object, number of tries, the enqueue string, 
    #            number of copies required, regex for how copies must match, and final timeout
    sub receiveData($$$$;$$$$$$) {
        my ($read_set, $write_set, $read_byte_count, $write_byte_count, $tries, $enq, $copies, $copyMatch, $finalTimeout, $nak) =  @_;
        $tries ||= 1;
    	$enq or ($enq = $ENQ x 3);
        $copies ||= 1;
    	$copyMatch ||= "(.*)";
    	$finalTimeout ||= 10;
    	$nak ||= $NAK x 3;
    	$copyMatch = qr/$copyMatch/s;
        my (@data, @cp);
        for(my $i = 0; $i < $tries; $i++) {
            #flushInput($read_set);
            @data = waitForData($read_set, $write_set, \$read_byte_count, \$write_byte_count, $data_re, $enq, 10, $finalTimeout);
            if(scalar(@data) == 0) {
            	LogIt("Timeout on try $i");
                #return undef; #timeout
            } elsif($data[2]) { #EOT
                LogIt("End-of-Transmission received");
                return $data[2];
            } elsif($data[3]) { #EOC
                LogIt("End-of-Collection received");
                return $data[3];
            } elsif(checkLCR($data[0], $data[1])) {
                if($copies == 1) {
                    return $data[0];
                } else {
                    my ($same, $dn, $d0, @cm) = (1);
                    @cm = $data[0] =~ qr/$copyMatch/;
                    if(scalar(@cm) > 0) {
                        $dn = join("|||", @cm); 
                        LogIt("Verifying against previous copies") if($DEBUG);
                        foreach $d0 (@cp) {
                            LogIt("Comparing $d0 and $dn") if($DEBUG);
                            if($d0 eq $dn) { 
                                LogIt("Copy matched. Increment copy variable to ". ($same + 1)) if($DEBUG);
                                ++$same < $copies or return $data[0]; 
                            }
                        }
                        push(@cp, $dn);
                    } else {
                        LogIt("Data received `$data[0]'does not match Copy Match pattern `$copyMatch'");
                    }
                }
            } else { #invalid LCR
                LogIt("LCR did not match; try again?");
            }
            $enq = $nak; #more copies or invalid LCR - send NAK
        }
        return undef; #failed too many times
    }
}
#args: data string, LCR
sub checkLCR($$) {
    my ($data, $lcr) = @_;
    my $n = lcr($data);
    if($n == ord($lcr)) {
        LogIt('LCR matches!') if($DEBUG);
        return 1;
    } else {
        LogIt("LCR " . ord($lcr) . " ($lcr) does not match " . ord($n) ." ($n)") if($DEBUG);
        return 0;
    }
}

sub lcr($) {
    my ($data) = @_;
    my $n = ord($ETX);# because the end character is included in the calculation
    foreach my $ch (split(//,$data)) {
        $n ^= ord($ch);
    }
    return $n & 127;
}

# args: read IO::Select object, write IO::Select Object, RegEx to search for, ENQ to send, timeout b4 first char (seconds), 
# timeout for data (seconds)
my $buf = "";
sub waitForData($$$$;$$$$) {
    my ($read_set, $write_set, $read_byte_count, $write_byte_count, $searchFor, $enq, $firstTimeout, $finalTimeout) = @_;
    $searchFor ||= '.*';
    $firstTimeout ||= 10;
    $finalTimeout ||= 20;
    my ($deadAirTimeout, $readSize, $data, @match) = ($DEAD_AIR_TIMEOUT, $READ_SIZE, $buf);
    LogIt("Test for match to $searchFor") if($DEBUG);
    $searchFor = qr/$searchFor/s; # pre-compile for optimization
        
    # try to do this without alarm & signals
    my $start = time();               
    my $next_enq_time = $start;               
    my $end = $next_enq_time + $firstTimeout;
    my $len = 0;
    while($end > time()) {
    	# try to read
	    my $old_len = $len;
	    #my $sz = readData($read_set, \$data, $readSize);
    	#if(!defined($sz)) { 
        # 	LogIt("Failed to read from input stream");
        #    return ();
        #}
        #$len += $sz;
        my $tmp = readData($read_set, $readSize);
        if(!defined($tmp)) { 
         	LogIt("Failed to read from input stream");
            return ();
        }
        $data .= $tmp;
        $len = length($data);
        
    	if(@match = ($data =~ $searchFor)) {
        	LogIt("Match: " . join(";", @match)) if($DEBUG);   
        	LogIt("Received: $data", 1); # if($DEBUG);
        	$buf = $';   
        	return @match; # found match - return
        }
        if($old_len < $len) {
        	LogIt("`$data' did not match `$searchFor'", 1); # if($DEBUG);
        	if($old_len == 0) { #first byte was received add to end time
        		$end = $start + $finalTimeout;
        	}
        }
        # First send the enqueue string if provided	    
	    if(defined($enq) && $old_len == 0 && length($tmp) == 0) {
	    	my @wh_set = $write_set->can_write(0);
	    	my $wh_fh = $wh_set[0];
	    	if(defined($wh_fh)) {
	    		if($next_enq_time <= time()) {
			    	LogIt("Sending ENQ: $enq") if($DEBUG);
			        printData($wh_fh, $enq);
			        $write_byte_count += length($enq);
		    		$next_enq_time = time() + 5;
	    		}
	    	} elsif($DEBUG) {
	    		LogIt("Could not get a ready write handle");
	    	}
	    }
	    
        sleep 0.1; # rest for 100 milleseconds		
    }
    LogIt("Timeout after ".($len == 0 ? $firstTimeout : $finalTimeout)." seconds while waiting for " . ($len == 0 ? "data" : "a match") . "; Received data: $data");
    $read_byte_count += $len;
    return ();
}

# flushes the INPUT. Argument is the read IO::Select object
sub flushInput($;) {
    my ($read_set) = @_;
    foreach my $rh ($read_set->can_read(0)) {
   		$rh->flush();
    }
    $buf = "";
}

sub LogIt($;$) {
	my ($line, $escape_all) = @_;
	if($escape_all) {
		$line =~ s/([\x00-\x1F\x80-\xFF])/"[".ord($1)."]"/eg;
	} else {
		$line =~ s/([\x00-\x08\x0B\x0C\x0E-\x1F\x80-\xFF])/"[".ord($1)."]"/eg;
	}
	print "[" . get_date_stamp() . "] $line\n";	
}

sub get_date_stamp {
	my @epoch_sec = gettimeofday();
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($epoch_sec[0]);
	return sprintf("%04d-%02d-%02d %02d:%02d:%02d.%06d", ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec, $epoch_sec[1]);
}

sub printData($$;) {
	my ($wh, $data) = @_;
	LogIt("Sending: `$data'", 1);
	$wh->print($data);
	$wh->flush();	
}

sub readData($$;) {
	my ($read_set, $max) = @_;
	my ($cnt, $data) = (0, "");
	while(my @rhs = $read_set->can_read(0)) {
	foreach my $rh (@rhs) {
		my $avail = get_available($rh);			
		LogIt("Available=$avail") if(defined($avail));
		if(UNIVERSAL::can($rh, 'recv')) {
			my $tmp = "";
			defined($rh->recv($tmp, 1)) or return undef;
			$data .= decode($tmp);
			$cnt += length($tmp);
		} else {	
			my $sz = $rh->read($data, 1, $cnt);
			if(!defined($sz)) {
				LogIt("Read returned undef");
				return undef;
			}
			$cnt += $sz;		
		}
		if($cnt >= $max) {
			LogIt("Returning with data: `$data'") if($DEBUG);
			return $data;
		}  	
	}
	}
	LogIt("Returning with data: `$data'") if($DEBUG);		
    return $data;	
}

sub get_available($;) {
	my ($rh) = @_;
	#my $avail =  pack("L", 0);
	#$rh->ioctl(FIONREAD(), $avail) or LogIt( "Couldn't call ioctl: $!\n");
	#return unpack("L", $avail);	
	return eval{ return $rh->Get_cnt(); };					
}

sub decode($;) {
	my ($tmp) = @_;
	$tmp =~ s/([\x80-\xFF])/chr(ord($1) & 0x7F)/eg;
	return $tmp;
}

sub encode($;) {
	my ($tmp) = @_;
	$tmp =~ s/([\x00-\x7F])/chr(ord($1) | 0x80)/eg;
	return $tmp;
}
#not sure why this wasn't working (reference passed in as $data did not have newly read stuff)
sub readDataInto($$;$) {
	my ($read_set, $data, $max) = @_;
	my @rhs = $read_set->can_read(1);
	my $cnt = 0;
	foreach my $rh (@rhs) {
		my $tmp = "";
		if(!defined($rh->recv($tmp, $max-$cnt))) {
			LogIt("Recv returned undef");
			return undef;
		}
		$data .= $tmp;
		$cnt += length($tmp);
		if($cnt >= $max) {
			LogIt("Returning with data: `$data'");
			return $cnt;
		} else {
			LogIt("Data so far (1): `$data'");
		}   	
	}
    return $cnt;	
}
1;
