use strict;

use IO::Socket;
use Time::HiRes qw(sleep time gettimeofday);
use SendReceive qw(LogIt);

$| = 1; # auto-flush output

my $socket = IO::Socket::INET->new(PeerAddr => '10.0.0.63:14111');
if(!defined($socket)) {
	LogIt("Failed to connect");
}
LogIt("Reading from socket");
my $buf;
$socket->recv($buf, 1);
LogIt("Received Data: `$buf'");
$socket->recv($buf, 1);
LogIt("Received Data: `$buf'");
$socket->recv($buf, 1);
LogIt("Received Data: `$buf'");
$socket->recv($buf, 1);
LogIt("Received Data: `$buf'; Sending serial...");
$socket->printflush(chr(2)."E4070022".chr(3).chr(lcr("")));
LogIt("Sent Serial");


sub lcr($) {
    my ($data) = @_;
    my $n = ord(chr(3));# because the end character is included in the calculation
    foreach my $ch (split(//,$data)) {
        $n ^= ord($ch);
    }
    return $n & 127;
}