use strict;

use IO::Socket;
use Time::HiRes qw(sleep time gettimeofday);

$| = 1; # auto-flush output

my $socket = IO::Socket::INET->new(Listen => '14111');
if(!defined($socket)) {
	LogIt("Failed to listen");
}
LogIt("Waiting for connection");
my $client = $socket->accept();
LogIt("Connection received; Sending ENQ");
for(my $i = 0; $i < 10; $i++) {
	$client->printflush(chr(5));
	sleep(1);
}
LogIt("Sent ten ENQs; exitting");

sub LogIt($) {
	my $line = shift;
	$line =~ s/([\x00-\x08\x0B\x0C\x0E-\x1F])/"[".ord($1)."]"/eg;
	print "[" . get_date_stamp() . "] $line\n";	
}

sub get_date_stamp {
	my @epoch_sec = gettimeofday();
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($epoch_sec[0]);
	return sprintf("%04d-%02d-%02d %02d:%02d:%02d.%06d", ($year + 1900), ($mon + 1), $mday, $hour, $min, $sec, $epoch_sec[1]);
}
