#!/usr/local/bin/perl -w
use strict;
use sigtrap qw(die untrapped normal-signals stack-trace error-signals);

use USAT::NetLayer::Daemon;
use USAT::NetLayer::Utils qw(LogIt);

use lib '/opt/USANet1';
use DatabaseConfig;
use Logic;

LogIt("USANet1 Network Layer - Copyright (c) 2003-2005 USA Technologies.  All rights reserved.");
my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, lines may be out of order
my $server = USAT::NetLayer::Daemon->new(
	{
		'pidfile'			=> '/opt/USANet1/USANet1.pid',
		'localport'			=> 14111, 
		'logfile'			=> 'STDERR',
		'mode'				=> 'fork',
		'mydesc'			=> 'USANet1 on port 14111',
		'mydbconfig'		=> DatabaseConfig->new(),
		'mynetlayeridfile'	=> '/opt/USANet1/USANet1.id',
		'myrunlib'			=> 'Logic',
		'myrunlibpath'		=> '/opt/USANet1',
		'mysocketlimit'		=> 30
	}, 
	""
);

$server->Bind();

END
{
	eval { $server->shutdown_net_layer_id() };
}
