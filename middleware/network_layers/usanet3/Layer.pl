#!/usr/local/bin/perl
use strict;
use warnings;
#use forks;
#use forks::shared;
use forks::BerkeleyDB 0.02;
use forks::BerkeleyDB::shared;
use sigtrap qw(die untrapped normal-signals stack-trace error-signals);

use USAT::NetLayer::Daemon;
use USAT::NetLayer::Utils qw(LogIt);

use lib '/opt/USANet3';
use DatabaseConfig;
use HandlerDBConfig;
use Logic;

LogIt("USANet3 Network Layer - Copyright (c) 2003-2006 USA Technologies.  All rights reserved.");
my $oldfh = select(STDOUT); $| = 1; select($oldfh);	#force auto-flush of STDOUT; otherwise, lines may be out of order
my $server = USAT::NetLayer::Daemon->new(
	{
		'pidfile'			=> '/opt/USANet3/USANet3.pid',
		'localport'			=> 14107, 
		'logfile'			=> 'STDERR',
		'mode'				=> 'ithreads',
		'mydesc'			=> 'USANet3 on port 14107',
		'mydbconfig'		=> DatabaseConfig->new(),
		'mynetlayeridfile'	=> '/opt/USANet3/USANet3.id',
		'myrunlib'			=> 'Logic',
		'myrunlibpath'		=> '/opt/USANet3',
		'mysocketlimit'		=> 100,
		'mysocketlimitIPExclRegexp'	=> undef,
		'myconfigfile'		=> '/opt/USANet3/layer.config',
		'mydebugfile'		=> '/opt/USANet3/layer.debug',
		'myhandlerdbconfig'	=> HandlerDBConfig->new()
	}, 
	""
);

$server->Bind();

END
{
	eval { $server->shutdown_net_layer_id() };
}
