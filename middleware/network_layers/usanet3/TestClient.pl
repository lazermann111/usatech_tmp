#!/usr/bin/perl
use strict;

use forks;
use forks::shared;
use IO::Socket;
use IO::Select;
use USAT::Database;        # Access to the Database
use Evend::ReRix::Shared qw(uuencode uudecode);
use USAT::NetLayer::Utils qw(LogIt);

use lib '/opt/USANet3';
use Logic;

my $DATABASE = USAT::Database->new(	print_query => 1, 
												execute_flatfile=>1, 
												debug => 1, 
												debug_die => 0,
												print_query => 0);

my $DEFAULT_MACHINE_ID = "EV000000";
my $DEFAULT_ENCRYPTION_KEY = "9265027739274401";

my $LOG_LEVEL = 'INFO';
my $DEBUG_FILE = '/opt/USANet3/TestClient.debug';

my $machine = shift(@ARGV);
my $in 		= shift(@ARGV);

my $packed = pack("H*", $in);
my $key = &getKey($machine, $DATABASE);
my $encrypted = $machine . Logic::PackEncrypted($packed, $key, $LOG_LEVEL, $DEBUG_FILE);
my $encoded = uuencode($encrypted) . "\x0a";

print "\n";

print "Machine ID : $machine\n";
print "Input      : $in\n";
print "Key        : $key\n";
print "Output     : $encoded\n";

my $socket = new IO::Socket::INET (PeerAddr => 'localhost:14107');
my $select = new IO::Select();

if(!defined $socket )
{
	print "Failed to connect to server\n";
	exit;
}

$select->add($socket);
	
if($socket->send($encoded) != length($encoded))
{
	print "Failed to send request\n";
	$socket->close();
	exit;
}

my $responseBuffer;
$socket->recv($responseBuffer, 500);
my $response .= $responseBuffer;

my $decoded = uudecode($response);
my ($result, $decrypted) = @{Logic::UnpackEncrypted($decoded, $key, $LOG_LEVEL, $DEBUG_FILE)};

print "Response   : " . unpack("H*",$response) . "\n";
print "Decrypted  : " . unpack("H*",$decrypted) . "\n";
print "Result     : $result\n";

print "\n";

sub getKey
{
	my ($machineID, $MYDATABASE) = @_;
	LogIt("Looking up encryption key for $machineID");
	
	if($machineID eq $DEFAULT_MACHINE_ID)
	{
		LogIt("Returning DEFAULT encryption key");
		return $DEFAULT_ENCRYPTION_KEY;
	}
	
	my $array_ref = $MYDATABASE->select(
							table          => 'device',
							select_columns => 'encryption_key',
							where_columns  => [ 'device_name = ?' ],
							where_values   => [ $machineID ]
					);
							
	if(not $array_ref->[0])
	{
		return 0;
	}
	else
	{
		return $array_ref->[0][0];
	}
}

