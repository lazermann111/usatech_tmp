#!/usr/bin/perl
#usage: LoadTest.pl <num iterations before session complete> EV#0 [EV#1 EV#2 ... EV#N]
use strict;
use warnings;
use sigtrap qw(die untrapped normal-signals stack-trace error-signals);

use Time::HiRes qw(time);
use IO::Poll;
use POE::strict qw(
	Component::Client::TCP
	Filter::Line
);

use USAT::NetLayer::Database;
use USAT::NetLayer::Filter::UU qw(uuencode uudecode);
use USAT::NetLayer::Utils qw(LogIt);

use lib '/opt/USANet3';
use DatabaseConfig;
use Logic;

use constant DEBUG => 0;
use constant SIMULATE_CONNECT_FIRST_MESSAGE_DELAY => 0;
use constant DISCONNECT_SESSION_AFTER_LAST_MESSAGE => 0;
use constant MESSAGES => qw(
	015c
);
#	025e24074d455300043131323334

my $LOG_LEVEL = 'INFO';
our $active_sessions = 0;
our $total_sessions_started = 0;
our $msg_process_time_total = 0;
our $msg_process_time_total_num = 0;
our $msg_process_time_max = undef;
our $msg_process_time_min = undef;

sub main () {
	my $host  = "localhost";
	my $port = 14107;
	my $num_loops = shift @ARGV;
	die "Number of loops param '$num_loops' isn't a valid positive integer" unless $num_loops =~ m/^\d+$/o;
	my @devices = @ARGV;
	die "No devices specified" unless @devices;
	my @messages = (MESSAGES) x $num_loops;
	my @keys;
	my $DATABASE = USAT::NetLayer::Database->new(
		config 				=> new DatabaseConfig,
		log_level 			=> $LOG_LEVEL,
		debug_file_handle	=> *STDOUT,
	);
	push @keys, (Logic::getKey($_, $DATABASE, "$host:$port", $LOG_LEVEL, *STDOUT))[0] foreach @devices;

	# Spawn a new client for each EV number.
	for (my $idx = 0; $idx < scalar @devices; $idx++) {

		POE::Component::Client::TCP->new(
			Args			=> [$devices[$idx], $keys[$idx]],
		    RemoteAddress	=> $host,
			RemotePort		=> $port,
			Filter			=> ['POE::Filter::Line', Literal => "\x0A"],
			
			# The session started.
			Started => sub {
				$_[HEAP]->{device_name} = $_[ARG0] || die 'Unknown device name';
				$_[HEAP]->{device_key} = $_[ARG1] || die "Unknown device key for device $_[HEAP]->{device_name}";
				$_[HEAP]->{buffer} = [];
				$_[HEAP]->{msg_idx} = 0;
				$_[HEAP]->{msg_process_start_ts} = 0;
				$_[HEAP]->{msg_process_time} = [];
				$_[HEAP]->{input_timeout_alarm_id} = undef;
				$_[HEAP]->{msg_process_time_total} = 0;
				$_[HEAP]->{msg_process_time_total_num} = 0;
			},

			# The connection was successful. Send a message.
			Connected => sub {
				$_[KERNEL]->yield( log => "connected to $host:$port ..." );
				SIMULATE_CONNECT_FIRST_MESSAGE_DELAY
					? $_[KERNEL]->delay( 'send_message', int(rand()*10) )
					: $_[KERNEL]->yield( 'send_message' );
				$active_sessions++;
				$total_sessions_started++;
			},

			# The connection failed.
			ConnectError => sub {
				$_[KERNEL]->yield( log => "could not connect to $host:$port ..." );
			},

			# The output buffer flushed.
			ServerFlushed => sub {
				$_[HEAP]->{msg_process_start_ts} = time();
			},
			
			# The server has sent us something.
			ServerInput => sub {
				my $input = $_[ARG0];
				$_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1] = time() - $_[HEAP]->{msg_process_start_ts};
				$_[HEAP]->{msg_process_time_total} += $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1];
				$_[HEAP]->{msg_process_time_total_num}++;
				$msg_process_time_total += $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1];
				$msg_process_time_total_num++;
				$msg_process_time_max = $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1]
					if !defined $msg_process_time_max ||  $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1] > $msg_process_time_max;
				$msg_process_time_min = $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1]
					if !defined $msg_process_time_min || $_[HEAP]->{msg_process_time}->[$_[HEAP]->{msg_idx} - 1] < $msg_process_time_min;
				
				$_[KERNEL]->yield( log => "got input from $host:$port ..." );
				push @{ $_[HEAP]->{buffer} }, $input;
				if ($_[HEAP]->{msg_idx} < scalar @messages) {
					$_[KERNEL]->yield( 'send_message' );
					$_[HEAP]->{input_timeout_alarm_id} = $_[KERNEL]->alarm( input_timeout => time() + 30 );
				}
				else {
					$_[KERNEL]->yield( 'disconnect' );
				}
			},
			
			InlineStates => {

				# Send a message.
				send_message => sub {
					$_[KERNEL]->yield( log => "sending message \#$_[HEAP]->{msg_idx} on $host:$port ..." );

					my $packed = pack("H*", $messages[$_[HEAP]->{msg_idx}]);
					my $encrypted = $_[HEAP]->{device_name} . Logic::PackEncrypted($packed, $_[HEAP]->{device_key}, $LOG_LEVEL, *STDOUT, "$host:$port");
					my $encoded = uuencode($encrypted);
					if (DEBUG) {
						$_[KERNEL]->yield( log => "Machine ID : ".$_[HEAP]->{device_name} );
						$_[KERNEL]->yield( log => "Input      : ".$messages[$_[HEAP]->{msg_idx}] );
						$_[KERNEL]->yield( log => "Key        : ".$_[HEAP]->{device_key} );
						$_[KERNEL]->yield( log => "Output     : $encoded" );
					}

					$_[HEAP]->{msg_idx}++;
					$_[HEAP]->{server}->put($encoded);
					$_[HEAP]->{input_timeout_alarm_id} = $_[KERNEL]->alarm( input_timeout => time() + 30 );
				},

				# The server sent us something already, but it has become idle.
				# Display what the server sent us so far, and shut down.
				input_timeout => sub {
					$_[KERNEL]->yield( log => "got input timeout from $host:$port ..." );

					if (DEBUG) {
						$_[KERNEL]->yield( log => ",----- Responses from $host:$port" );
						my $idx = 0;
						foreach ( @{ $_[HEAP]->{buffer} } ) {
							my $decoded = uudecode($_);
							my ($result, $decrypted) = @{Logic::UnpackEncrypted($decoded, $_[HEAP]->{device_key}, $LOG_LEVEL, *STDOUT, "$host:$port")};
							$_[KERNEL]->yield( log => "Message \#$idx:" );
							$_[KERNEL]->yield( log => "\tResponse   : " . unpack("H*",$_) );
							$_[KERNEL]->yield( log => "\tDecrypted  : " . unpack("H*",$decrypted) );
							$_[KERNEL]->yield( log => "\tResult     : $result" );
							$idx++;
						}
						$_[KERNEL]->yield( log => "`-----" );
					}

					$_[KERNEL]->yield( 'disconnect' );
				},
				
				disconnect => sub {
					$_[KERNEL]->alarm_remove_all;

					$_[KERNEL]->yield( log => "Overall Execution statistics (so far): " );
					$_[KERNEL]->yield( log => "\tOverall Avg Msg Time for all sessions: "
						.($msg_process_time_total_num ? $msg_process_time_total / $msg_process_time_total_num : 0) );
					$_[KERNEL]->yield( log => "\tOverall Max Msg Time: ". $msg_process_time_max);
					$_[KERNEL]->yield( log => "\tOverall Min Msg Time: ". $msg_process_time_min);
					$_[KERNEL]->yield( log => "Session Execution statistics: " );
					$_[KERNEL]->yield( log => "\tAvg Msg Time for this session: "
						.($_[HEAP]->{msg_process_time_total_num} ? $_[HEAP]->{msg_process_time_total} / $_[HEAP]->{msg_process_time_total_num} : 0) );

					if (DEBUG) {
						my $detail = '';
						$detail .= "\n\t\#$_: $_[HEAP]->{msg_process_time}->[$_] seconds" for (0..scalar @{$_[HEAP]->{msg_process_time}} - 1);
						$_[KERNEL]->yield( log => $detail ) if $detail;
					}
					$_[KERNEL]->yield( log => "session shutting down..." );
					$_[KERNEL]->yield( log => "active sessions remaining: ".(--$active_sessions)." of $total_sessions_started total)");

					$_[KERNEL]->alarm_remove($_[HEAP]->{input_timeout_alarm_id}) if defined $_[HEAP]->{input_timeout_alarm_id};
					$_[KERNEL]->yield( 'shutdown' ) if DISCONNECT_SESSION_AFTER_LAST_MESSAGE;
				},
				
				log => sub {
					LogIt("Session #".($_[SESSION]->ID).": ".(@_[ARG0..$#_]));
				},
			  },
		  );
	}

	# Run the clients until the last one has shut down.

	$poe_kernel->run();
	exit 0;
}

main();
