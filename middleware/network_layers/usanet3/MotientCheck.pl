#!/usr/bin/perl
use strict;
use Net::SMTP;
use USAT::NetLayer::Utils;

my $count = `netstat -an | grep 63.84.140.7 | grep 8780 | grep ESTABLISHED | wc -l`;
$count =~ s/\s//g;

if($count eq '1')
{
	LogIt("OK");
}
else
{
	LogIt("Motient connection to 63.84.140.7 port 8780 appears to be down!");
	&send_alert("Motient connection to 63.84.140.7 port 8780 appears to be down!");
}

sub send_alert
{
	my ($info) = @_;
	&send_email('rerix_engine@usatech.com','pcowan@usatech.com,4844671281@mobile.att.net,mheilman@usatech.com,4844674506@mobile.att.net','Motient Connection Failure!', localtime() . " - ALERT! $info\n");
	LogIt("ALERT! $info");
}

sub getKey
{
	my ($machineID, $MYDATABASE) = @_;
	
	my $array_ref = $MYDATABASE->select(
							table          => 'device',
							select_columns => 'encryption_key',
							where_columns  => [ 'device_name = ?' ],
							where_values   => [ $machineID ]
					);
							
	if(not $array_ref->[0])
	{
		return 0;
	}
	else
	{
		return $array_ref->[0][0];
	}
}

sub send_email
{
	my ($from_addr, $to_addrs, $subject, $content) = @_;
	my @to_array = split(/ |,/, $to_addrs);

	my $smtp_server = '10.0.0.23';
	#my $smtp = Net::SMTP->new($smtp_server, Debug => 1);
	my $smtp = Net::SMTP->new($smtp_server);
	
	if(not defined $smtp)
	{
		warn "send_email_detailed: Failed to connect to SMTP server $smtp_server!\n";
		return 0;
	}
	
	$smtp->mail($from_addr);

	foreach my $to_addr (@to_array)
	{
		$smtp->to($to_addr);
	}
	
	$smtp->data(); 
	$smtp->datasend("Subject: $subject\n");  
	$smtp->datasend("\n"); 
	$smtp->datasend("$content\n\n"); 
	$smtp->dataend(); 
	
	$smtp->quit();   
	
	return 1;
}
