#!/usr/bin/perl -w

use strict;
use lib '/opt/ReRixEngine2/';
use ReRix::Utils;

my $timestamp = substr(localtime(), 0, 10);

my $data = `tail -1000000 /opt/USANet3/current | egrep "$timestamp(.*)10.24.77.150(.*)NOT on END bug list" | cut -c 72-79`;
chomp($data);

my %bag;
my @lines = split /\n/, $data;
foreach my $ev (@lines)
{
  my $count = $bag{$ev};
  if(defined $count)
  {
    $bag{$ev} = ($count+1);
  }
  else
  {
    $bag{$ev} = 1;
  }
}

my @email_list;
my $output;
while(my($ev,$count) = each(%bag))
{
  if($count > 10)
  {
    $output .= "$ev\n";
  }
}

my @sorted_email_list = sort @email_list;

my $email_txt;
foreach my $email_list (@sorted_email_list)
{
  $email_txt .= "$email_list\n";
}

if($email_txt)
{
  my $msg_subject = "Detected Decryption Failures ($timestamp)";
  my $msg_txt = "On $timestamp the following devices transmitted transactions that failed to decrypt.  This usually indicated an END bug problem.\n\nCount EV Number\n$email_txt\n\n";

  if($output)
  {
    $msg_txt .= "The following EV Numbers were automatically added to the FIX list:\n\n$output\n\n";
  }

  &ReRix::Utils::send_email('rerixengine@usatech.com', 'pcowan@usatech.com', $msg_subject, $msg_txt);
  #&ReRix::Utils::send_email('rerixengine@usatech.com', 'pcowan@usatech.com,gharrum@usatech.com,jlouvet@usatech.com,bpatrizzi@usatech.com,ddemedio@usatech.com,bkolls@usatech.com', $msg_subject, $msg_txt);
}

if($output)
{
  print $output;
}

