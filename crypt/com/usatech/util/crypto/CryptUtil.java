package com.usatech.util.crypto;

import java.util.*;
import java.lang.reflect.*;
import com.usatech.util.Conversions; 
import com.usatech.util.crypto.algorithms.*;
import com.usatech.util.crypto.crc.*;

public class CryptUtil
{
	// main method for testing only
	public static void main(String args[])
	{
		if(args.length != 4)
		{
			System.out.println("Usage: com.usatech.util.crypto.CryptUtil <(e)ncrypt|(d)ecrypt> <crypt name> <hex key> <hex data>");
			System.exit(1);
		}
		
		String operation = args[0];
		String cryptName = args[1];
		String key = args[2];
		String inputHex = args[3];
		
		Crypt crypt = CryptUtil.getCrypt(cryptName);
		
		if(crypt == null)
		{
			System.out.println("Failed to find crypt for: " + cryptName);
			System.exit(1);
		}
		
		byte[] inputBytes = null;
		
		try
		{
			inputBytes = Conversions.hexToByteArray(inputHex);
		}
		catch(Exception e)
		{
			System.out.println("Failed to convert hex data to bytes: " + e);
			System.exit(1);
		}
		
		if(key.length() != 32)
		{
			System.out.println("Invalid key length: " + key + " != 32");
			System.exit(1);
		}
		
		System.out.println();
		System.out.println("CRYPT		: " + crypt.toString());
		System.out.println("KEY (HEX)	: " + key);
		
		System.out.println("INPUT (HEX)	: " + Conversions.bytesToHex(inputBytes));
		
		byte[] outputBytes = null;

		try
		{
			if(operation.toUpperCase().startsWith("E"))
			{
				System.out.println("OPERATION 	: ENCRYPT");
				outputBytes = crypt.encrypt(inputBytes, Conversions.hexToByteArray(key));
			}
			else if(operation.toUpperCase().startsWith("D"))
			{
				System.out.println("OPERATION 	: DECRYPT");
				outputBytes = crypt.decrypt(inputBytes, Conversions.hexToByteArray(key));
			}
			else
			{
				System.out.println("Unrecognized operation: " + operation);
				System.exit(1);
			}
		}
		catch(Exception e) 
		{
			System.out.println("Crypt operation failed: " + e);
			System.exit(1);
		}
		
		System.out.println("OUTPUT (HEX)	: " + Conversions.bytesToHex(outputBytes));
		System.out.println("BYTE LENGTH 	: " + outputBytes.length);
	}
	
	// a cache of singleton crypt objects
	private static Hashtable crypts = new Hashtable();	
	
	static 
	{
		// original ESuds crypt
		RijndaelCrypt rijndaelWithUnicodeCRC = new RijndaelCrypt(new BE_FFFF_UnicodeOnly());
		// legacy name
		crypts.put("RIJNDAEL", rijndaelWithUnicodeCRC);
		// newer descriptive name
		crypts.put("RIJNDAEL_UNICODE_CRC", rijndaelWithUnicodeCRC);

		// hopefully the future MEI and ESuds crypt
		AES_CBC_Crypt aesCbcWithCRC16 = new AES_CBC_Crypt(new BE_FFFF());
		// legacy name
		crypts.put("AES_CBC", aesCbcWithCRC16);
		// newer descriptive name
		crypts.put("AES_CBC_CRC16", aesCbcWithCRC16);
		
		// original ESuds crypt with fixed CRC16 
		RijndaelCrypt rijndaelWithCRC16 = new RijndaelCrypt(new BE_FFFF());
		crypts.put("RIJNDAEL_CRC16", rijndaelWithCRC16);
		
		// TEA with little endian CRC16 for G4s
		TEACrypt teaWithCRC16LE = new TEACrypt(new LE_0000());
		crypts.put("TEA_CRC16", teaWithCRC16LE);
	
		// add any others here
	}
	
	public static Crypt getCrypt(String name)
	{
		return (Crypt) crypts.get(name.toUpperCase());
	}
}
