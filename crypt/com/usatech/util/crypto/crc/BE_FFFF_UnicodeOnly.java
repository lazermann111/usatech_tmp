package com.usatech.util.crypto.crc;

import com.usatech.util.Conversions;
import com.usatech.util.SignUtils;

/**
 * 
 * @author pcowan
 *
 * Endianness		: Big Endian
 * Initial value	: 0xffff
 * 
 * The initial release of ESuds used a CRC that was not quite calculated correctly
 * because of Java's treatment of characters as Unicode.  As a result it works 
 * consitently in Java but not when translated to other languages.
 * 
 * This class was moved here from com.usatech.util.CRCUtil
 * 
 */

public class BE_FFFF_UnicodeOnly implements CRC
{
	/** 
	* generator polynomial 
	*/ 
	private static final int poly = 0x1021; /* x16 + x12 + x5 + 1 generator polynomial */ 
	/* 0x8408 used in European X.25 */ 
	
	/** 
	* scrambler lookup table for fast computation. 
	*/ 
	private static int[] crcTable = new int [256];

	static 
	{ 
		// initialise scrambler table 
		for(int i=0; i<256; i++) 
		{ 
			int fcs = 0; 
			int d = i<<8 ; 
			
			for(int k=0; k<8; k++) 
			{
				if(((fcs ^ d) & 0x8000) != 0) 
				{
					fcs = (fcs << 1) ^ poly ; 
				}
				else 
				{
					fcs = (fcs << 1); 
				}
				
				d <<= 1 ; 
				fcs &= 0xffff ; 
			} 
			
			crcTable[i] = fcs; 
		} 
	} 
	
	public static void main(String args[])
	{
		BE_FFFF_UnicodeOnly alg = new BE_FFFF_UnicodeOnly();
		short crc = (short) alg.calcCRC(Conversions.hexToByteArray(args[0]));
		System.out.println("crc		: " + crc + " (" + Conversions.bytesToHex(Conversions.shortToByteArray(crc)) + ")");
	}

	public BE_FFFF_UnicodeOnly()
	{
		
	}
	
	public int calcCRC (byte[] b) 
	{ 
		// loop, calculating CRC for each byte of the string 
		int work = 0xffff; 
		for(int i=0; i < b.length; i++) 
		{ 
			work = (crcTable[((work >> 8)) & 0xff] ^ (work << 8) ^ b[i]) & 0xffff;
		} 
		
		return work; 
	}
}
