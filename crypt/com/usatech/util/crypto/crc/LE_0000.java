package com.usatech.util.crypto.crc;

import com.usatech.util.Conversions;

/**
 * 
 * @author pcowan
 * 
 * This is the CRC the G4 uses as of 07/20/2004
 *
 * Endianness		: Little Endian    
 * Initial value	: 0x0000
 */

public class LE_0000 implements CRC
{
	public static void main(String args[])
	{
		LE_0000 alg = new LE_0000();
		short crc = (short) alg.calcCRC(Conversions.hexToByteArray(args[0]));
		System.out.println("crc		: " + crc + " (" + Conversions.bytesToHex(Conversions.shortToByteArray(crc)) + ")");
	}
	
	public LE_0000()
	{
		
	}
	
	public int calcCRC(byte[] bytes)
	{
		CRC16 crc16 = new CRC16(bytes, 0x0000);
		return crc16.getCrcLittleEndian();
	}
}
