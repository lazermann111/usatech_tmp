/*
 * TEA
 * 
 * Implementation of the ANSI C CRC16-ccitt encryption/decryption algorithm.
 * 
 * Version History:
 * 1.00		06/28/2004	erybski		Initial release.
 * 2.00		07/20/2004	pcowan		Changed to accept byte array instead of strings
 * 
 */
package com.usatech.util.crypto.crc;

import com.usatech.util.Conversions;

/**
 * @author erybski
 */
public class CRC16
{
	private int _crc = 0;
	
	public CRC16(byte[] b)
	{
		_crc = updateCrc(b, 0);
	}
	
	public CRC16(byte[] b, int crc)
	{
		_crc = updateCrc(b, crc);
	}
	
	private int updateCrc(byte[] b, int crc)
	{
		for (int i = 0; i < b.length; i++)
		{
			crc  = ((crc & 0xff00) >>> 8) | ((crc & 0xff) << 8);
			crc ^= b[i] & 0xff;
			crc ^= (crc & 0xff) >>> 4;
			crc ^= (crc << 8) << 4;
			crc ^= ((crc & 0xff) << 4) << 1;
		}

		return (crc & 0xffff);
	}

	
	public int getCrc()
	{
		return _crc;
	}
	
	public int getCrcLittleEndian()
	{
		int crcOut = 0;

		// rotate bytes to generate little endian pseudo-integer value
		crcOut <<= 8;
		crcOut |= (_crc & 0xff);	//lower byte
		crcOut <<= 8;
		crcOut |= ((_crc & 0xff00) >>> 8);	//upper byte

		return crcOut;
	}
	
	public String getCrcStr()
	{
		StringBuffer r = new StringBuffer();

		// rotate bytes to generate big endian string value
		r.append((char)((_crc & 0xff00) >>> 8));	//upper byte
		r.append((char)(_crc & 0xff));	//lower byte

		return r.toString();
	}
	
	public String getCrcStrLittleEndian()
	{
		StringBuffer r = new StringBuffer();

		// rotate bytes to generate little endian string value
		r.append((char)(_crc & 0xff));	//lower byte
		r.append((char)((_crc & 0xff00) >>> 8));	//upper byte

		return r.toString();
	}
}
