package com.usatech.util.crypto.crc;

public interface CRC
{
	public int calcCRC(byte[] data);
}
