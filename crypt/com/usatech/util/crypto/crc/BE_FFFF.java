package com.usatech.util.crypto.crc;

import com.usatech.util.Conversions;

/**
 * 
 * @author pcowan
 * 
 * This is the CRC newer versions of ESuds and MEI devices use as of 07/20/2004
 *
 * Endianness		: Big Endian    
 * Initial value	: 0xFFFF
 */

public class BE_FFFF implements CRC
{
	public static void main(String args[])
	{
		BE_FFFF alg = new BE_FFFF();
		short crc = (short) alg.calcCRC(Conversions.hexToByteArray(args[0]));
		System.out.println("crc		: " + crc + " (" + Conversions.bytesToHex(Conversions.shortToByteArray(crc)) + ")");
	}
	
	public BE_FFFF()
	{
		
	}
	
	public int calcCRC(byte[] bytes)
	{
		CRC16 crc16 = new CRC16(bytes, 0xffff);
		return crc16.getCrc();
	}
}
