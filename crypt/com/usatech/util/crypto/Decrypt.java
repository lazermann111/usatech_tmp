package com.usatech.util.crypto;

import com.usatech.util.*;

public class Decrypt
{
	public static void main(String args[]) throws Exception
	{
		Crypt crypt = CryptUtil.getCrypt("RIJNDAEL");
		System.out.println(Conversions.bytesToHex(crypt.decrypt(Conversions.hexToByteArray(args[1]), Conversions.hexToByteArray(args[0]))));
	}
}
