package com.usatech.util.crypto;

import com.usatech.util.*;

public class Encrypt
{
	public static void main(String args[]) throws Exception
	{
		Crypt crypt = CryptUtil.getCrypt("RIJNDAEL");
		System.out.println(Conversions.bytesToHex(crypt.encrypt(Conversions.hexToByteArray(args[1]), Conversions.hexToByteArray(args[0]))));
	}
}
