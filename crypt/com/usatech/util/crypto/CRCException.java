package com.usatech.util.crypto;

public class CRCException extends Exception
{
	public CRCException()
	{
		super();
	}
	
	public CRCException(String msg)
	{
		super(msg);
	}
}
