package com.usatech.util.crypto;

import java.security.InvalidKeyException;

public interface Crypt
{
	public String toString();

    public byte[] encrypt(byte[] unencrypted, byte[] key) throws InvalidKeyException, CRCException, CryptException;
    public byte[] encrypt(byte[] unencrypted, byte[] key, int blockSize) throws InvalidKeyException, CRCException, CryptException;	
	public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding) throws InvalidKeyException, CryptException;
	public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding, int blockSize) throws InvalidKeyException, CryptException;

	public byte[] decrypt(byte[] encrypted, byte[] key) throws InvalidKeyException, CRCException, CryptException;
	public byte[] decrypt(byte[] encrypted, byte[] key, int blockSize) throws InvalidKeyException, CRCException, CryptException;
	public byte[] decrypt(byte[] encrypted, byte[] key, int unencryptedLength, int blockSize) throws InvalidKeyException, CryptException;
}
