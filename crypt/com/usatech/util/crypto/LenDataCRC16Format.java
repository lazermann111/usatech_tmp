package com.usatech.util.crypto;

import java.security.InvalidKeyException;
import java.security.SecureRandom;

/*import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;*/

import com.usatech.util.crypto.crc.*;
import com.usatech.util.Conversions;
import com.usatech.util.SignUtils;

public abstract class LenDataCRC16Format implements Crypt
{
	public static final int DEFAULT_BLOCK_SIZE = 16;
    
	//private static Log log = LogFactory.getLog(LenDataCRC16Format.class);
	
    private static final int        DEFAULT_MSG_LEN_BYTE_NUM    = 2;
    private static final int        DEFAULT_NONCE_SIZE          = 0;
    private static final String     RANDOM_NAME                 = "SHA1PRNG";
	
	private CRC crcAlg;
    private int msgLenByteNum;
    private int nonceSize;
	protected int blockSize;
	
	private SecureRandom random = null;
	private String randomError;
	
	public LenDataCRC16Format(CRC crcAlg)
	{
		this(crcAlg, DEFAULT_BLOCK_SIZE, DEFAULT_MSG_LEN_BYTE_NUM, DEFAULT_NONCE_SIZE);
	}
	
	public LenDataCRC16Format(CRC crcAlg, int blockSize, int msgLenByteNum, int nonceSize)
	{
		this.crcAlg = crcAlg;
        this.msgLenByteNum = msgLenByteNum;
		this.blockSize = blockSize;
		this.nonceSize = nonceSize;
		if (nonceSize > 0)
		{
	        try
	        {
	            random = SecureRandom.getInstance(RANDOM_NAME);
	        }
	        catch (Exception e)
	        {
	            randomError = e.getMessage();
	        }
		}
	}
	
	public String toString() 
	{ 
		return "[" + nonceSize + " byte nonce][" + msgLenByteNum + " byte len][len bytes][crc][padding (0x00)] (Block Size = " + blockSize + ")"; 
	}
	
	public int getBlockSize() { return blockSize; }
	
	public byte[] encrypt(byte[] unencrypted, byte[] key) throws InvalidKeyException, CRCException, CryptException
	{
		return encrypt(unencrypted, key, getBlockSize());
	}

	public byte[] encrypt(byte[] unencrypted, byte[] key, int blockSize) throws InvalidKeyException, CRCException, CryptException
	{
		// calculate the crc, returns an unsigned short in int form
		int crc = -1;
		
		try
		{
			crc = crcAlg.calcCRC(unencrypted);
		}
		catch(Exception e)
		{
			throw new CRCException("CRC calculation error: " + e);
		}
		
		byte[] tempBytes = new byte[unencrypted.length+msgLenByteNum+2+nonceSize];
		
		if (nonceSize > 0)
		{
		    if (random == null)
		        throw new CryptException("Failed to get instance of SecureRandom " + RANDOM_NAME + ": " + randomError);
		    
		    byte[] nonce = new byte[nonceSize];
            synchronized (random)
            {
                random.nextBytes(nonce);
            }
		    System.arraycopy(nonce, 0, tempBytes, 0, nonceSize);
		}
        
        if (msgLenByteNum == 1)
            tempBytes[nonceSize] = (byte) unencrypted.length;
        else
            SignUtils.writeShort((short)unencrypted.length, tempBytes, nonceSize);
		
        System.arraycopy(unencrypted, 0, tempBytes, msgLenByteNum+nonceSize, unencrypted.length);
		SignUtils.writeShort((short)crc, tempBytes, (unencrypted.length+msgLenByteNum+nonceSize));
		
		/*if (log.isDebugEnabled())
        	log.debug("unencrypted: " + Conversions.bytesToHex(tempBytes));*/
		
		return encrypt(tempBytes, key, (byte)0x00, blockSize);
	}
	
	public byte[] decrypt(byte[] encrypted, byte[] key) throws InvalidKeyException, CRCException, CryptException
	{
		return decrypt(encrypted, key, getBlockSize());
	}

	public byte[] decrypt(byte[] encrypted, byte[] key, int blockSize) throws InvalidKeyException, CRCException, CryptException
	{
		byte[] decrypted = decrypt(encrypted, key, encrypted.length, blockSize);
		
		// pull out len, bytes, and crc; the rest is padding
        int len; 
        
        /*if (log.isDebugEnabled())
        	log.debug("decrypted: " + Conversions.bytesToHex(decrypted));*/
        
        // discard nonce
        if (msgLenByteNum == 1)
            len = SignUtils.toUnsigned(decrypted[nonceSize]);
        else            
            len = SignUtils.readUnsignedShort(decrypted, nonceSize);  
		
		if(len > decrypted.length)
		{
			// this is a sanity check - if the decryption failed for some reason but didn't produce
			// any exception, len will usually be screwed up, which can cause an ArrayIndexOutOfBoundException
			// this is usually caused by decrypting with the wrong key
			
			throw new CryptException("Decryption appears to have failed; len > array.length.");
		}
		
		byte[] bytes = new byte[len];
		System.arraycopy(decrypted, msgLenByteNum+nonceSize, bytes, 0, len);
		int crc = SignUtils.readUnsignedShort(decrypted, (len+msgLenByteNum+nonceSize));
		
		// calculate a crc on the bytes to compare to the stored one
		int calculatedCRC = -1;
		try
		{
			calculatedCRC = crcAlg.calcCRC(bytes);
		}
		catch(Exception e)
		{
			throw new CRCException("CRC calculation error: " + e);
		}
		
		// compare to the stored CRC to see if anything got screwed up		
		if(crc != calculatedCRC)
		{
			throw new CRCException("Calculated CRC(" + calculatedCRC + ") does not match stored CRC(" + crc + ")");
		}
		
		return bytes;
	}
}
