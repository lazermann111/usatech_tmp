package com.usatech.util.crypto;

public class CryptException extends Exception
{
	public CryptException()
	{
		super();
	}
	
	public CryptException(String msg)
	{
		super(msg);
	}
}
