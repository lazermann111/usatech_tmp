package com.usatech.util.crypto.algorithms;

import com.usatech.util.crypto.*;
import com.usatech.util.crypto.algorithms.rijndael.*;
import com.usatech.util.crypto.crc.*;

import java.security.InvalidKeyException;

public class RijndaelCrypt extends LenDataCRC16Format implements Crypt
{
	public RijndaelCrypt(CRC crc) 
	{
		super(crc);
	} 

	public String toString() { return "Rijndael(" + super.toString() + ")"; }
	
	public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding) throws InvalidKeyException, CryptException
	{
		return encrypt(unencrypted, key, padding, getBlockSize());
	}
	
	public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding, int blockSize) throws InvalidKeyException, CryptException
	{
		byte[] paddedBytes = null; 

		// figure out the size we need
		for(int paddedSize=blockSize; ; paddedSize+=blockSize)
		{
			if(unencrypted.length <= paddedSize && unencrypted.length > (paddedSize-blockSize))
			{
				paddedBytes = new byte[paddedSize];
				System.arraycopy(unencrypted, 0, paddedBytes, 0, unencrypted.length);
				break;
			}
		}
		
		// fill the padding up with the requested byte
		for (int i = unencrypted.length; i < paddedBytes.length; i++) 
		{
			paddedBytes[i] = padding;
		}
		 
		// build the object key
		Object oKey = Rijndael_Algorithm.makeKey(key, blockSize);
		
		try
		{
			// step through the array, encrypting blocks of bytes
			for(int i=0; i<paddedBytes.length; i+=blockSize)
			{
				byte[] encryptedBlock = Rijndael_Algorithm.blockEncrypt(paddedBytes, i, oKey, blockSize);
				System.arraycopy(encryptedBlock, 0, paddedBytes, i, blockSize);
			}
		}
		catch(Exception e)
		{
			throw new CryptException("Rijndael encryption failed: " + e);
		}
		
		// padded array now contains the encrypted padded bytes
		return paddedBytes;
	}
	
	public byte[] decrypt(byte[] encrypted, byte[] key, int unencryptedLength, int blockSize) throws InvalidKeyException, CryptException
	{
		// build the object key
		Object oKey = Rijndael_Algorithm.makeKey(key, blockSize);
		
		// copy the array contents to a new array so we don't change the original
		byte[] decrypted =  new byte[encrypted.length];
		System.arraycopy(encrypted, 0, decrypted, 0, decrypted.length);
		
		try
		{
			// step through the array, decrypting blocks of bytes
			for(int i=0; i<decrypted.length; i+=blockSize)
			{
				byte[] decryptedBlock = Rijndael_Algorithm.blockDecrypt(encrypted, i, oKey, blockSize);
				System.arraycopy(decryptedBlock, 0, decrypted, i, blockSize);
			}
		}
		catch(Exception e)
		{
			throw new CryptException("Rijndael decryption failed: " + e);
		}
		
		// if they're asking for the whole thing, don't bother to copy to a new array
		if(unencryptedLength == decrypted.length)
			return decrypted;
		
		byte[] trimmed = new byte[unencryptedLength];
		System.arraycopy(decrypted, 0, trimmed, 0, trimmed.length);
		return trimmed;
	}
}
