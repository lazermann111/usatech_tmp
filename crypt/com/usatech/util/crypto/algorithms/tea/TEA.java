/*
 * TEA
 * 
 * Implementation of the ANSI C TEA encryption/decryption algorithm.
 * 
 * Version History:
 * 1.00		06/28/2004	erybski		Initial release.
 * 
 */
package com.usatech.util.crypto.algorithms.tea;

import com.usatech.util.Conversions;

/**
 * @author erybski
 */

public class TEA extends java.lang.Object
{
	private int _key[] = new int[4];
	private byte _keyBytes[];

	public static void main(String args[])
	{
		String key = args[1];
		TEA t = new TEA(key);

		byte[] src = Conversions.hexToByteArray(args[2]);
		
		if(args[0].equalsIgnoreCase("E"))
		{
			System.out.println(Conversions.bytesToHex(t.encodeByte(src, (char) 0x00)));
		}
		else if(args[0].equalsIgnoreCase("D"))
		{
			System.out.println(Conversions.bytesToHex(t.decodeByte(src)));
		}
	}

	public TEA(String key)
	{
		_keyBytes = key.getBytes();
		int kLen = _keyBytes.length;

		if (kLen == 16)
		{
			for (int i = 0, j = 0; j < kLen; i++, j += 4)
			{
				_key[i] = ((_keyBytes[j+3] & 0xff) << 24) | ((_keyBytes[j+2] & 0xff) << 16) | ((_keyBytes[j+1] & 0xff) << 8) | (_keyBytes[j] & 0xff);	//note: non-standard key ordering
			}
		}
		else
		{
			throw new ArrayIndexOutOfBoundsException(this.getClass().getName() + ": Key is not 16 bytes long (length = " + kLen);
		}
	}
	
	public String getKey()
	{
		return new String(_keyBytes);
	}

	public String getKeyHex()
	{
		return byteToHex(_keyBytes);
	}
	
	public String encode(String inStr)
	{
		return new String(encodeByte(inStr.getBytes()));
	}
	public String decode(String inStr)
	{
		return new String(decodeByte(inStr.getBytes()));
	}
	
	public byte[] encodeByte(byte[] inBytes)
	{
		return encodeByte(inBytes, (char)(0x20));
	}
	public byte[] encodeByte(byte[] inBytes, char padChar)
	{
		int bLen = inBytes.length;
		byte bp[];

		if ((bLen % 8) != 0)
		{
			int padding = 8 - (bLen % 8);
			bp = new byte[bLen + padding];
			System.arraycopy(inBytes, 0, bp, 0, bLen);
			for (int i = bLen; i < (bLen + padding); i++)
			{
				bp[i] = (byte)(padChar);
			}
			bLen = bp.length;
		}
		else
		{
			bp = inBytes;
		}

		int intCount = bLen / 4, r[] = new int[2], w[] = new int[2], outInts[] = new int[intCount];
		for (int i = 0, j = 0; j < bLen; i += 2, j += 8)
		{
			r[0] = ((bp[j] & 0xff) << 24) | ((bp[j+1] & 0xff) << 16) | ((bp[j+2] & 0xff) << 8) | (bp[j+3] & 0xff);
			r[1] = ((bp[j+4] & 0xff) << 24) | ((bp[j+5] & 0xff) << 16) | ((bp[j+6] & 0xff) << 8) | (bp[j+7] & 0xff);
			w = encipher(r);
			outInts[i] = w[0];
			outInts[i+1] = w[1];
		}

		byte outBytes[] = new byte[bLen];
		for (int i = 0, j = 0; i < outInts.length; i++, j += 4)
		{
			outBytes[j] = (byte)((outInts[i] & 0xff000000) >>> 24);
			outBytes[j+1] = (byte)((outInts[i] & 0xff0000) >>> 16);
			outBytes[j+2] = (byte)((outInts[i] & 0xff00) >>> 8);
			outBytes[j+3] = (byte)(outInts[i] & 0xff);
		}

		return outBytes;
	}
	public byte[] decodeByte(byte[] bp)	//identical to encode, but with decode() call
	{
		int bLen = bp.length;

		if ((bLen % 8) != 0)
		{
			throw new ArrayIndexOutOfBoundsException(this.getClass().getName() + ": Some encoded data appears to be missing (" + bLen + "found, not a multiple of 8");
		}

		int intCount = bLen / 4, r[] = new int[2], w[] = new int[2], outInts[] = new int[intCount];
		for (int i = 0, j = 0; j < bLen; i += 2, j += 8)
		{
			r[0] = ((bp[j] & 0xff) << 24) | ((bp[j+1] & 0xff) << 16) | ((bp[j+2] & 0xff) << 8) | (bp[j+3] & 0xff);
			r[1] = ((bp[j+4] & 0xff) << 24) | ((bp[j+5] & 0xff) << 16) | ((bp[j+6] & 0xff) << 8) | (bp[j+7] & 0xff);
			w = decipher(r);
			outInts[i] = w[0];
			outInts[i+1] = w[1];
		}

		byte outBytes[] = new byte[bLen];
		for (int i = 0, j = 0; i < outInts.length; i++, j += 4)
		{
			outBytes[j] = (byte)((outInts[i] & 0xff000000) >>> 24);
			outBytes[j+1] = (byte)((outInts[i] & 0xff0000) >>> 16);
			outBytes[j+2] = (byte)((outInts[i] & 0xff00) >>> 8);
			outBytes[j+3] = (byte)(outInts[i] & 0xff);
		}

		return outBytes;
	}

	public final int[] encipher(int[] v)
	{
		int y=v[0], z=v[1], w[] = new int[2], sum = 0, delta = 0x9E3779B9, n = 32;

		while(n-->0)
		{
			y += (z << 4 ^ z >>> 5) + z ^ sum + _key[sum&3];
			sum += delta;
			z += (y << 4 ^ y >>> 5) + y ^ sum + _key[sum>>>11 & 3];
		}

		w[0] = y;
		w[1] = z;
		return w;
	}
	public final int[] decipher(int[] v)
	{
		int y=v[0], z=v[1], w[] = new int[2], sum = 0xC6EF3720, delta = 0x9E3779B9, n = 32;

		while(n-- > 0)
		{
			z -= (y << 4 ^ y >>> 5) + y ^ sum + _key[sum>>>11 & 3];
			sum -= delta;
			y -= (z << 4 ^ z >>> 5) + z ^ sum + _key[sum&3];
		}

		w[0] = y;
		w[1] = z;
		return w;
	}

	public String byteToHex(byte b[])
	{
		StringBuffer r = new StringBuffer();
		for (int i = 0; i < b.length; i++)
		{
			r.append(Integer.toHexString((b[i] >>> 4) & 0xf));
			r.append(Integer.toHexString(b[i] & 0xf));
		}
		return r.toString();
	}
}
