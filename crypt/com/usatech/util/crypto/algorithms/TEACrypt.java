package com.usatech.util.crypto.algorithms;

import java.security.InvalidKeyException;
import com.usatech.util.crypto.*;
import com.usatech.util.crypto.algorithms.tea.*;
import com.usatech.util.crypto.crc.*;

public class TEACrypt extends LenDataCRC16Format implements Crypt
{
	public TEACrypt(CRC crc)
	{
		super(crc, 8, 1, 0);
	}

	public String toString() { return "Crypt: TEA(" + super.toString() + ")"; }

	public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding) throws InvalidKeyException, CryptException
	{
		return encrypt(unencrypted, key, padding, getBlockSize());
	}

	public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding, int blockSize) throws InvalidKeyException, CryptException
	{
		TEA tea = new TEA(new String(key));
		return tea.encodeByte(unencrypted, (char) padding);
	}

	public byte[] decrypt(byte[] encrypted, byte[] key, int unencryptedLength, int blockSize) throws InvalidKeyException, CryptException
	{
		// some incoming data checks

        boolean modifiedEncryptedLength = false;
        int encryptedLength = encrypted.length;
        int remainder = encryptedLength % blockSize;

        if (remainder > 0)
        {
            encryptedLength -= remainder;
            modifiedEncryptedLength = true;
        }

        if (encryptedLength < blockSize)
        {
            throw new CryptException("encryptedLength < blockSize!");
        }
		else if (unencryptedLength > encryptedLength)
		{
            unencryptedLength = encryptedLength;
		}

		TEA tea = new TEA(new String(key));
		byte[] decrypted;

        if (modifiedEncryptedLength == true)
        {
            byte[] modifiedEncrypted = new byte[encryptedLength];
            System.arraycopy(encrypted, 0, modifiedEncrypted, 0, encryptedLength);
            decrypted = tea.decodeByte(modifiedEncrypted);
        }
        else
            decrypted = tea.decodeByte(encrypted);

		// trim to the requested size
		byte[] trimmed = new byte[unencryptedLength];
		System.arraycopy(decrypted, 0, trimmed, 0, unencryptedLength);
        return trimmed;
	}
}
