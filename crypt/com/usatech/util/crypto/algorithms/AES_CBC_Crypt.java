package com.usatech.util.crypto.algorithms;

import java.security.InvalidKeyException;
import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.*;

import com.usatech.util.crypto.*;
import com.usatech.util.crypto.crc.*;

public class AES_CBC_Crypt extends LenDataCRC16Format implements Crypt
{
    private static final String     ALGORITHM_NAME          = "AES";
    private static final String     PROVIDER_NAME           = "SunJCE";
    private static final String     TRANSFORMATION_NAME     = "AES/CBC/NOPADDING";
    
	public AES_CBC_Crypt(CRC crc) 
	{
		super(crc, 16, 2, 4);
	} 

	public String toString() { return "Crypt: AES_CBC, " + super.toString(); }
	
	public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding) throws InvalidKeyException, CryptException
	{
		return encrypt(unencrypted, key, padding, getBlockSize());
	}
	
    public byte[] encrypt(byte[] unencrypted, byte[] key, byte padding, int blockSize) throws InvalidKeyException, CryptException 
    {
        Provider provider = Security.getProvider(PROVIDER_NAME);
        if (provider == null)
            throw new CryptException("Provider not found: " + PROVIDER_NAME);
        
        Cipher cipher = null;
        try
        {
            cipher = Cipher.getInstance(TRANSFORMATION_NAME, provider);
        }
        catch (Exception e)
        {
            throw new CryptException("Failed to get instance of cipher " + TRANSFORMATION_NAME + ": " + e.getMessage());
        }        
        if (cipher == null)
            throw new CryptException("Cipher not found: " + TRANSFORMATION_NAME);
        
        // calculate the minimum number of blocks required to do the encryption
        int blockCount = (unencrypted.length / blockSize);
        if ((unencrypted.length % blockSize) != 0)
            blockCount++;
        
        // create an array of the padded size and pad it
        byte[] paddedBytes = new byte[(blockCount*blockSize)];
        System.arraycopy(unencrypted, 0, paddedBytes, 0, unencrypted.length);
        for (int i = unencrypted.length; i < paddedBytes.length; i++) 
        {
            paddedBytes[i] = padding;
        }
               
        SecretKeySpec keySpec = new SecretKeySpec(key, ALGORITHM_NAME);
        IvParameterSpec ivSpec = new IvParameterSpec(new byte[blockSize]);        
        
        try
        {
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        }
        catch (Exception e)
        {
            throw new CryptException("Failed to init cipher: " + e.getMessage());
        }

        try
        {
            return cipher.doFinal(paddedBytes);
        }
        catch (Exception e)
        {
            throw new CryptException("Failed to encrypt data: " + e.getMessage());
        }
    }	
	
    public byte[] decrypt(byte[] encrypted, byte[] key, int unencryptedLength, int blockSize) throws InvalidKeyException, CryptException
    {
        // some incoming data checks
        
        if (encrypted.length % blockSize != 0) 
        {
            throw new CryptException("Encrypted length not a multiple of blockSize!");
        } 
        else if (encrypted.length < blockSize) 
        {
            throw new CryptException("Encrypted length < blockSize!");
        }
        else if (unencryptedLength > encrypted.length)
        {
            throw new CryptException("unencryptedLength > encrypted.length!");
        }
        
        Provider provider = Security.getProvider(PROVIDER_NAME);
        if (provider == null)
            throw new CryptException("Provider not found: " + PROVIDER_NAME);
        
        Cipher cipher = null;
        try
        {
            cipher = Cipher.getInstance(TRANSFORMATION_NAME, provider);
        }
        catch (Exception e)
        {
            throw new CryptException("Failed to get instance of cipher " + TRANSFORMATION_NAME + ": " + e.getMessage());
        }        
        if (cipher == null)
            throw new CryptException("Cipher not found: " + TRANSFORMATION_NAME);        
        
        SecretKeySpec keySpec = new SecretKeySpec(key, ALGORITHM_NAME);
        IvParameterSpec ivSpec = new IvParameterSpec(new byte[blockSize]);
        
        try
        {
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
        }
        catch (Exception e)
        {
            throw new CryptException("Failed to init cipher: " + e.getMessage());
        }
                    
        // create a new array to hold the decryption results
        byte decrypted[] = new byte[encrypted.length];
        
        try
        {
            decrypted = cipher.doFinal(encrypted);
        }
        catch (Exception e)
        {
            throw new CryptException("Failed to decrypt data: " + e.getMessage());
        }
        
        // if they're asking for the whole thing, don't bother to copy to a new array
        if(unencryptedLength == decrypted.length)
            return decrypted;
        
        // trim to the requested size
        byte[] trimmed = new byte[unencryptedLength];
        System.arraycopy(decrypted, 0, trimmed, 0, trimmed.length);
        return trimmed;
    }
}
