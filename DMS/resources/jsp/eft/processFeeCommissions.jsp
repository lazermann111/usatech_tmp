<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results,java.math.BigDecimal"%>
<%@page import="java.util.Vector"%>

<% Results results = RequestUtils.getAttribute(request, "results", Results.class, false);
   Results eft = RequestUtils.getAttribute(request, "eft", Results.class, false); %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Process Fees</span></div>
</div>

<div class="tableContent">
<% eft.next(); %>

<div class="gridHeader">EFT ID <%=eft.getFormattedValue("EFT_ID", "HTML") %>, Batch # <%=eft.getFormattedValue("BATCH_REF_NBR", "HTML") %></div>

<table class="tabDataDisplayBorder">
		<tr class="gridHeader">
			<td>Terminal #</td>
			<td>Tran Type</td>
			<td>Sales Amount</td>
			<td>Transaction Count</td>
			<td>Buy Rate</td>
			<td>Sell Rate</td>
			<td>Net Commission Amount</td>
		</tr>
<%	int rowCount = 0;
	while(results.next()) { %>
		<tr class="<%=(rowCount%2 == 0) ? "row1" : "row0"%>">
			<td><%=results.getFormattedValue("TERMINAL_NBR", "HTML")%></td>
			<td><%=results.getFormattedValue("TRANS_TYPE_NAME", "HTML")%></td>
			<td><%=results.getFormattedValue("GROSS_AMOUNT", "HTML")%></td>
			<td><%=results.getFormattedValue("TRAN_COUNT", "HTML")%></td>
			<td><%=results.getFormattedValue("BUY_RATE_DESC", "HTML")%></td>
			<td><%=results.getFormattedValue("SELL_RATE_DESC", "HTML")%></td>
			<td><%=results.getFormattedValue("NET_AMOUNT", "HTML")%></td>
			
		</tr>
<%		rowCount++;
	} %>
</table>

<div class="spacer10"></div>
<div align="center">
<input name="close" id="close" type="button" value="Close" onclick="window.location = '/manageEFT.i?eftId=<%=eft.getFormattedValue("EFT_ID", "HTML") %>';" class="cssButton"/>
</div>
<div class="spacer10"></div>
</div>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
