<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.BasicServletUser"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results,java.math.BigDecimal"%>
<%@page import="simple.text.StringUtils"%>
<% 
BasicServletUser loggedUser = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
BigDecimal adjId = RequestUtils.getAttribute(request, "adjId", BigDecimal.class, false); 
BigDecimal eftId = RequestUtils.getAttribute(request, "eftId", BigDecimal.class, false); 
Results adj = RequestUtils.getAttribute(request, "adj", Results.class, false);
Results term = RequestUtils.getAttribute(request, "term", Results.class, false);
String action = RequestUtils.getAttribute(request, "action", String.class, false); 

String amount="";
String explanation="";
String termId = null;
BigDecimal terminalId = new BigDecimal(-1);

if(adj != null && adj.next()) {
     amount = adj.getFormattedValue("AMOUNT");
     explanation = adj.getFormattedValue("REASON");
     termId=adj.getFormattedValue("TERMINAL_ID");
     if(termId==null)
    	 termId="";
     terminalId = adj.getValue("TERMINAL_ID", BigDecimal.class);
     if (terminalId == null)
    	terminalId = new BigDecimal(-1);
}
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="smallFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Adjustment</span></div>
</div>

<div class="tableContent">

<div class="spacer10"></div>

<form name="adjForm" id="adjForm" method="post" action="manageEFT.i">
<input type="hidden" name="userName" value="<%=loggedUser.getUserName()%>" />
<input type="hidden" name="adjId" id="adjId" value="<%=adjId %>" />
<input type="hidden" name="eftId" id="eftId" value="<%=eftId %>" />
<input type="hidden" name="eftAction" id="eftAction" value="<%=StringUtils.encodeForHTMLAttribute(action) %>" />

<table>
	<tbody>
	<tr>
		<td class="label">EFT ID</td>
		<td class="label"><%=eftId %></td>
	</tr>

	<tr>
	
		<td class="label">Amount<font color="red">*</font></td>
		<td class="control">
			<input type="text" name="amount" id="amount" value="<%=amount %>"/>
		</td>
	</tr>
	<tr>
		<td class="label">Terminal</td>		
		<td>
			<select name="termId" id="termId" style="width: 240px;"  
			<%if(termId!=null) out.write("disabled=\"disabled\""); %>
			
			>
<option></option>
<%if (term!=null){ while(term.next()) {
%><option value="<%if(term!=null) out.write(term.getValue("TERMINAL_ID")+"");%>"
<% if(terminalId.compareTo(term.getValue("TERMINAL_ID", BigDecimal.class)) == 0){ %> selected="selected"<%} %>
><%=term.getFormattedValue("TERMINAL_NBR")%></option>	    	
<%}}%>			
			</select>
		</td>
	</tr>
		
	<tr>
		<td class="label">Explanation<font color="red">*</font></td>
		<td class="control">
			<textarea name="reason" id="reason" rows="5" cols="40" tabindex="3"><%=explanation %></textarea>	
		</td>
	</tr>
	<%if ("addAdjustment".equalsIgnoreCase(action)) {%>
	<tr>
		<td class="label">Split into Multiple Payments</td>
		<td><input type="checkbox" name="split_payment_flag" id="split_payment_flag" value="Y" /></td>
	</tr>
	
	<tr>
		<td class="label">Payment Interval</td>
		<td class="control">
			<select name="split_payment_interval" id="split_payment_interval">
				<option value="D">Daily</option>
				<option value="W">Weekly</option>
				<option value="M">Monthly</option>
			</select>
		</td>
	</tr>
	
	<tr>	
		<td class="label">Number of Payments</td>
		<td class="control">
			<input type="text" name="split_number_of_payments" id="split_number_of_payments" maxlength="3" value="5" />
		</td>
	</tr>
	
	<tr>	
		<td class="label">First Split Payment Date</td>
		<td class="control">
			<input type="text" name="first_split_payment_date" id="first_split_payment_date" maxlength="10" value="<%=Helper.getCurrentDate()%>" />
			<img src="/images/calendar.gif" id="first_split_payment_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
	</tr>
	<%}%>
	<tr>
		<td colspan="2" align="center">
			<input name="okBtn" id="okBtn" type="button" class="cssButton"
			value="OK" onclick="submitOK();"/>
			
			<input name="cancelBtn" id="cancelBtn" type="button" class="cssButton"
			value="Cancel" onclick="window.location = '/manageEFT.i?eftId=<%=eftId%>';" />
		</td>
	</tr>
	
</tbody>
</table>		
</form>

<div class="spacer10"></div>
</div>
</div>

<script type="text/javascript" defer="defer">
<%if ("addAdjustment".equalsIgnoreCase(action)) {%>
Calendar.setup({
    inputField     :    "first_split_payment_date",                  	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "first_split_payment_date_trigger",  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
<%}%>

function submitOK(){
	var adjForm = document.getElementById("adjForm");
	var amount = document.getElementById("amount");
	var reason = document.getElementById("reason");
	var termId = document.getElementById("termId");
	
	if( amount.value == null || amount.value=='' ){
		alert('Enter amount please');
		return;
	}
	if( reason.value == null || reason.value=='' ){
		alert('Enter explanation please');
		return;
	}
	if (termId.value == null || termId.value == '') {
		if (!confirm("Are you sure you don't want to apply this adjustment to a terminal?"))
			return;
	}
	<%if ("addAdjustment".equalsIgnoreCase(action)) {%>
	if (document.getElementById("split_payment_flag").checked) {
		var split_number_of_payments = document.getElementById("split_number_of_payments");
		split_number_of_payments.value = split_number_of_payments.value.trim();
		if (!split_number_of_payments.value.match("^[0-9]+$")) {
			alert("Please enter a valid Number of Payments");
			split_number_of_payments.focus();
			return;
		}
		var first_split_payment_date = document.getElementById("first_split_payment_date");
		first_split_payment_date.value = first_split_payment_date.value.trim();
		if(!isDate(first_split_payment_date.value)) {
			first_split_payment_date.focus();
			return;
		}
	}
	<%}%>
	adjForm.submit();
}

</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
