<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "ach");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("achFileId"), ""));

long achFileId = RequestUtils.getAttribute(request, "achFileId", Long.class, true);
Results achFile = DataLayerMgr.executeQuery("GET_ACH_FILE_INFO", new Object[] {achFileId});

String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false); %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="fullPageFormContainer">

	<div class="tableHead">
		<div class="tabHeadTxt"><span class="txtWhiteBold">Generated ACH File</span></div>
	</div>
	<table>
	<% if (!StringUtils.isBlank(msg)) { %>
		<tr><td class="status-info"><%=msg%></td></tr>
	<% } %>
	<% if (!StringUtils.isBlank(err)) { %>
		<tr><td class="status-error"><%=err%></td></tr>
	<% } %>
	</table>
	<% if (achFile.next()) {
			String statusCd = achFile.getFormattedValue("STATUS_CD");
			long fileTransferId = ConvertUtils.getLong(achFile.getValue("FILE_TRANSFER_ID")); %>
	<div class="tableContent">
		<form name="achForm" id="achForm" method="post" action="manageACHFile.i">
			<input type="hidden" name="achId"  id="achId" value="<%=achFile.getFormattedValue("ACH_ID") %>"/>
			<input type="hidden" name="achFileId"  id="achFileId" value="<%=achFileId %>"/>
			<input type="hidden" name="fileId"  id="achFileId" value="<%=fileTransferId %>"/>
			<input type="hidden" name="fileName"  id="achFileId" value="<%=StringUtils.prepareCDATA(achFile.getFormattedValue("FILE_TRANSFER_NAME")) %>"/>
			<table class="grid">
				<tr>
					<td class="label">ACH ID</td>
					<td class="label">Created</td>
					<td class="label">Status</td>
					<td class="label">Uploaded</td>
					<td class="label">Cancelled</td>
					<td class="label">File ID</td>
					<td class="label">File Name</td>
				</tr>
				<tr>
					<td class="control"><a href="manageACH.i?achId=<%=achFile.getFormattedValue("ACH_ID") %>"><%=achFile.getFormattedValue("ACH_ID") %></a></td>
					<td class="control"><%=achFile.getFormattedValue("CREATE_TSF") %> by <%=achFile.getFormattedValue("CREATE_USER_NAME") %></td>
					<td class="control"><%=achFile.getFormattedValue("STATUS") %> <%=statusCd.equals("N") ? "(" + achFile.getFormattedValue("TIME_REMAINING") + ")" : ""%></td>
					<td class="control"><% if (achFile.getValue("UPLOAD_TSF") != null) { %><%=achFile.getFormattedValue("UPLOAD_TSF") %> by <%=achFile.getFormattedValue("UPDATE_USER_NAME") %><% } %></td>
					<td class="control"><% if (achFile.getValue("CANCEL_TSF") != null) { %><%=achFile.getFormattedValue("CANCEL_TSF") %> by <%=achFile.getFormattedValue("UPDATE_USER_NAME") %><% } %></td>
					<td class="control"><a href="fileDetails.i?file_transfer_id=<%=fileTransferId %>"><%=fileTransferId %></a></td>
					<td class="control"><a href="fileDetails.i?file_transfer_id=<%=fileTransferId %>"><%=achFile.getFormattedValue("FILE_TRANSFER_NAME") %></a></td>
				</tr>
			</table>
			<table class="grid">
				<tr>
					<td>
						<input type="submit" id="action" name="action" value="Download File"/>
						<input type="submit" id="action" name="action" value="Mark As Uploaded"/>
					</td>
				</tr>
			</table>
			<% String fileContent = FileActions.getFileTransfer(fileTransferId, null, Integer.MAX_VALUE, null).getFileContent(); %>
			<table class="grid">
				<tr>
					<td>
						<div class="control" style="font-size:16px;color:red;font-weight:bold;padding:1em;">Be sure to click 'Mark As Uploaded' after uploading the ACH file!</div>
						<textarea readonly="readonly" disabled="disabled" style="width:60em; height:30em; font-family:monospace; font-size:14px; white-space: pre; word-wrap: normal; overflow-x: scroll;" wrap="soft"><%=StringUtils.encodeForHTML(fileContent)%></textarea>
					</td>
				</tr>
			</table>
			<div class="spacer10"></div>
		</form>
	</div>
	<% } else { %>
	<div align="center">Not Found</div>
	<% } %>
	<div class="spacer10"></div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
