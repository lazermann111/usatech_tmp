<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results,java.math.BigDecimal"%>
<%@page import="java.util.Vector"%>


<% 
Results adj = RequestUtils.getAttribute(request, "adj", Results.class, false);
Results eft = RequestUtils.getAttribute(request, "eft", Results.class, false);

Vector<String> arr = new Vector<String>(0);
arr.add("TERMINAL_NBR");
arr.add("FEE_NAME");
arr.add("FEE_AMOUNT");
arr.add("FEE_DATE");

long eftId = 0;
String num = "";
String status = "";
if(eft !=null && eft.next()) {
	eftId = eft.getValue("EFT_ID", long.class);
	num = eft.getFormattedValue("BATCH_REF_NBR");
	status = eft.getFormattedValue("STATUS");
}

%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Service Fees</span></div>
</div>

<div class="tableContent">

<div class="gridHeader">EFT ID <%=eftId %>, Batch # <%=num %></div>

<form name="sfForm" id="sfForm" method="post" action="manageEFT.i">
<input type="hidden" name="eftAction" id="eftAction" value="refund" />
<input type="hidden" name="eftId" id="eftId" value="<%=eftId %>" />

<table class="tabDataDisplayBorder">
		<tr class="gridHeader">
			<td width="20">&nbsp;</td>
			<td>Terminal</td>
			<td>Fee Name</td>
			<td>Amount</td>
			<td>Fee Date</td>
		</tr>
		
		<%
		
		if(adj == null || !adj.next()) {
		     out.write("<tr><td align=\"center\" colspan="+(arr.size()+1)+">No records</td></tr>");
		}else{
			int i = 0; 
			String rowClass = "row0";
			do {
				rowClass = (i%2 == 0) ? "row1" : "row0";
				i++;
				out.write("<tr class="+rowClass+">");
				out.write("<td><input type='radio' name='ledgerId' id='ledgerId' value='"+adj.getFormattedValue("PAYMENT_SERVICE_FEE_ID")+"'></td>");
				for(int j=0;j<arr.size();j++){
					out.write("<td>"+adj.getValue(arr.get(j))+"</td>");
				}
				out.write("</tr>");
			}while(adj.next());
		}

		%>
		
</table>
</form>

<%if (eftId > 0) {%>
<div class="spacer10"></div>
<div align="center">
<input <% out.write( (!status.equalsIgnoreCase("O") && !status.equalsIgnoreCase("L")) ? " disabled='disabled' class='cssButtonDisabled' ":"class='cssButton'"); %> name="close" id="close" type="button" value="Delete Fee Forever" onclick="deleteFee()" />
<input <% out.write( (!status.equalsIgnoreCase("L"))?" disabled='disabled' class='cssButtonDisabled' ":"class='cssButton'"); %> name="close" id="close" type="button" value="Do Not Include Fee In this Payment" onclick="dontInclude()" />
<input name="close" id="close" type="button" value="Close" onclick="window.location = '/manageEFT.i?eftId=<%=eftId%>';" class="cssButton" />
</div>
<%}%>
<div class="spacer10"></div>
</div>
</div>

<script type="text/javascript" defer="defer">

var sfForm = document.getElementById("sfForm");
var eftAction = document.getElementById("eftAction");
var eftId = document.getElementById("eftId");

function deleteFee(){	
	if( !ifChecked() ){
		alert('Choose a document please');
		return;
	}
	eftAction.value="deletesf";
	sfForm.submit();
}

function dontInclude(){
	if( !ifChecked() ){
		alert('Choose a document please');
		return;
	}
	eftAction.value="delaysf";
	sfForm.submit();
}

function ifChecked(){
	var ledgerId = document.getElementById("ledgerId");
	if(sfForm.ledgerId.length>0){
		for (i=0; i<sfForm.ledgerId.length; i++){
			if(sfForm.ledgerId[i].checked)
				return true;
		}
	}else{
		return ledgerId.checked;
	}	
	return false;
}

</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
