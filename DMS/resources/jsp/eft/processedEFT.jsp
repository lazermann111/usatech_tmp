<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String sortField = PaginationUtil.getSortField(null);
	String sortIndex = inputForm.getString(sortField, false);
	sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "3" : sortIndex;
	boolean norec = false;
	
	String totalCount = (String) request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length() > 0 && Integer.parseInt(totalCount) == 0)
		norec = true;
	
	String action = inputForm.getString("action", false);
	String eft_from_date = inputForm.getString("eft_from_date", false);
	if (StringHelper.isBlank(eft_from_date))
		eft_from_date = Helper.getDefaultStartDate();
	String eft_from_time = inputForm.getString("eft_from_time", false);
	if (StringHelper.isBlank(eft_from_time))
		eft_from_time = "00:00:00";
	String eft_to_date = inputForm.getString("eft_to_date", false);
	if (StringHelper.isBlank(eft_to_date))
		eft_to_date = Helper.getDefaultEndDate();
	String eft_to_time = inputForm.getString("eft_to_time", false);
	if (StringHelper.isBlank(eft_to_time))
		eft_to_time = Helper.getDefaultEndTime();
	String currencyCode = inputForm.getStringSafely("currency_code", "");
	String autoProcessed = inputForm.getStringSafely("auto_processed", "");
	String dataFormat = inputForm.getStringSafely("data_format", "");
	
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Processed EFTs</div>
</div>

<table>
<% if (!StringUtils.isBlank(msg)) { %>
	<tr><td class="status-info"><%=msg%></td></tr>
<% } %>
<% if (!StringUtils.isBlank(err)) { %>
	<tr><td class="status-error"><%=err%></td></tr>
<% } %>
</table>

<div class="spacer5"></div>
<form name="pForm" id="pForm" method="post" action="/processedEFT.i" onsubmit="return validateDate();">
	&nbsp;&nbsp;
	<b>
	From <input type="text" name="eft_from_date" id="eft_from_date" value="<%=eft_from_date%>" size="8" maxlength="10" >
	<img src="/images/calendar.gif" id="eft_from_date_trigger" class="calendarIcon" title="Date selector" /> 
	<input type="text" size="6" maxlength="8" id="eft_from_time" name="eft_from_time" value="<%=eft_from_time%>" />
	To <input type="text" name="eft_to_date" id="eft_to_date" value="<%=eft_to_date%>" size="8" maxlength="10">
	<img src="/images/calendar.gif" id="eft_to_date_trigger" class="calendarIcon" title="Date selector" />
	<input type="text" size="6" maxlength="8" id="eft_to_time" name="eft_to_time" value="<%=eft_to_time%>" />
	Currency <select name="currency_code">
		<option value="">All</option>
		<%Results currencies = DataLayerMgr.executeQuery("GET_CURRENCIES", null);%>
		<%while (currencies.next()) {%>
		<option value="<%=currencies.getFormattedValue("currency_code")%>"<%if (currencyCode.equalsIgnoreCase(currencies.getFormattedValue("currency_code"))) out.write (" selected=\"selected\"");%>><%=currencies.getFormattedValue("currency_name")%></option>
		<%}%>
	</select>
	Auto Processed 
	<select name="auto_processed">
		<option value="">All</option>
		<option value="Y"<%if ("Y".equalsIgnoreCase(autoProcessed)) out.write (" selected=\"selected\""); %>>Y</option>
		<option value="N"<%if ("N".equalsIgnoreCase(autoProcessed)) out.write (" selected=\"selected\""); %>>N</option>
	</select>
	Format 
	<select name="data_format">
		<option value="HTML"<%if ("HTML".equalsIgnoreCase(dataFormat)) out.write (" selected=\"selected\""); %>>HTML</option>
		<option value="CSV"<%if ("CSV".equalsIgnoreCase(dataFormat)) out.write (" selected=\"selected\""); %>>CSV</option>
	</select>
	<input type="submit" name="action" class="cssButton" value="List EFTs" />
	</b>

<div class="spacer10"></div>
<%if ("List EFTs".equalsIgnoreCase(action) || "Generate ACH Debit".equalsIgnoreCase(action)) {%>
<%if (!norec) { %>
<table>
<tr>
	<td>
		<input class='cssButton' name="action" type="submit" value="Generate ACH Debit"/>
	</td>
</tr>
</table>
<%} %>

<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td width="10"><%if (norec) {%>&nbsp; <%} else {%> <input type="checkbox" onclick="onClick()"/> <%}%></td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">EFT&nbsp;ID</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Customer</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Batch #</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Description</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Bank Account #</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Bank Routing #</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">EFT Amount</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Currency</a>
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">Auto Processed</a>
				<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Processed Date</a>
				<%=PaginationUtil.getSortingIconHtml("10", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "11", sortIndex)%>">Customer Status</a>
				<%=PaginationUtil.getSortingIconHtml("11", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "12", sortIndex)%>">Address Name</a>
				<%=PaginationUtil.getSortingIconHtml("12", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "13", sortIndex)%>">Address1</a>
				<%=PaginationUtil.getSortingIconHtml("13", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "14", sortIndex)%>">Address2</a>
				<%=PaginationUtil.getSortingIconHtml("14", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "15", sortIndex)%>">City</a>
				<%=PaginationUtil.getSortingIconHtml("15", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "16", sortIndex)%>">State</a>
				<%=PaginationUtil.getSortingIconHtml("16", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "17", sortIndex)%>">Postal</a>
				<%=PaginationUtil.getSortingIconHtml("17", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "18", sortIndex)%>">First Name</a>
				<%=PaginationUtil.getSortingIconHtml("18", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "19", sortIndex)%>">Last Name</a>
				<%=PaginationUtil.getSortingIconHtml("19", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "20", sortIndex)%>">Email</a>
				<%=PaginationUtil.getSortingIconHtml("20", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "21", sortIndex)%>">Customer ID</a>
				<%=PaginationUtil.getSortingIconHtml("21", sortIndex)%>
			</td>
		</tr>
	</thead>
	<tbody>
	<% if (norec) { %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<% } else {
	Results list = (Results) request.getAttribute("resultlist");
	int i = 0; 
	String rowClass = "row0";
	while(list.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++; %>
		<tr class="<%=rowClass%>">
		<td><input type="checkbox" name="pid" id="pid" value="<%=list.getFormattedValue(1) %>"></td>
		<td><a href="/manageEFT.i?eftId=<%=list.getFormattedValue("DOC_ID")%>"><%=list.getFormattedValue("DOC_ID")%></a></td>
		<td><%=list.getFormattedValue("CUSTOMER_NAME")%></td>
		<td><%=list.getFormattedValue("REF_NBR")%></td>
		<td><%=list.getFormattedValue("DESCRIPTION")%></td>
		<td><%=list.getFormattedValue("BANK_ACCT_NBR")%></td>
		<td><%=list.getFormattedValue("BANK_ROUTING_NBR")%></td>
		<td><%=list.getFormattedValue("TOTAL_AMOUNT_FMT")%></td>
		<td><%=list.getFormattedValue("CURRENCY_CODE")%></td>
		<td><%=list.getFormattedValue("AUTO_PROCESSED")%></td>
		<td><%=list.getFormattedValue("SENT_DATE_FORMATTED")%></td>
		<td><%=list.getFormattedValue("CUSTOMER_STATUS")%></td>
		<td><%=list.getFormattedValue("ADDRESS_NAME")%></td>
		<td><%=list.getFormattedValue("ADDRESS1")%></td>
		<td><%=list.getFormattedValue("ADDRESS2")%></td>
		<td><%=list.getFormattedValue("CITY")%></td>
		<td><%=list.getFormattedValue("STATE")%></td>
		<td><%=list.getFormattedValue("ZIP")%></td>
		<td><%=list.getFormattedValue("FIRST_NAME")%></td>
		<td><%=list.getFormattedValue("LAST_NAME")%></td>
		<td><%=list.getFormattedValue("EMAIL")%></td>
		<td><%=list.getFormattedValue("CUSTOMER_ID")%></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%
	String storedNames = PaginationUtil.encodeStoredNames(new String[] {
		"action", "eft_from_date", "eft_from_time", "eft_to_date", "eft_to_time", "currency_code", "auto_processed", "data_format"}); %>

<jsp:include page="/jsp/include/pagination.jsp" flush="true">
	<jsp:param name="_param_request_url" value="processedEFT.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

<% } %>

</form>
<% } %>

<div class="spacer15"></div>

</div>

<script type="text/javascript" defer="defer">
	Calendar.setup({
		inputField	: "eft_from_date",// id of the input field
		ifFormat		: "%m/%d/%Y", // format of the input field
		button			: "eft_from_date_trigger", // trigger for the calendar (button ID)
		align		  	: "B2", // alignment (defaults to "Bl")
		singleClick	: true,
		onUpdate		: "swap_dates"
	});
	Calendar.setup({
		inputField	: "eft_to_date", // id of the input field
		ifFormat		: "%m/%d/%Y", // format of the input field
		button			: "eft_to_date_trigger", // trigger for the calendar (button ID)
		align				: "B2", // alignment (defaults to "Bl")
		singleClick	: true,
		onUpdate		: "swap_dates"
	});

	var fromDate = document.getElementById("eft_from_date");
	var fromTime = document.getElementById("eft_from_time");
	var toDate = document.getElementById("eft_to_date");
	var toTime = document.getElementById("eft_to_time");
	
	function validateDate() {
		if(!isDate(fromDate.value)) {
			fromDate.focus();
			return false;
		}
		if(!isTime(fromTime.value)) {
			fromTime.focus();
			return false;
		}
		if(!isDate(toDate.value)) {
			toDate.focus();
			return false;
		}
		if(!isTime(toTime.value)) {
			toTime.focus();
			return false;
		}
		return true;
	}
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">
var checked = false;

function onClick(){
	if(checked){
		uncheckIt();
		checked = false;
	}else{
		checkIt();
		checked = true;
	}
}

function checkIt(){
	if(document.pForm.pid.length>0){
		for (i=0; i<document.pForm.pid.length; i++){
			document.pForm.pid[i].checked=1;
		}
	}else{
		document.pForm.pid.checked=1;
	}
}

function uncheckIt(){
	if(document.pForm.pid.length>0){
		for (i=0; i<document.pForm.pid.length; i++){
			document.pForm.pid[i].checked=0;
		}
	}else{
		document.pForm.pid.checked=0;
	}
}

</script>
