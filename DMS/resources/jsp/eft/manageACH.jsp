<%@page import="com.usatech.dms.model.DmsPrivilege"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.BasicServletUser"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "ach");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("achId"), ""));

BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);

long achId = RequestUtils.getAttribute(request, "achId", Long.class, true);
Results ach = DataLayerMgr.executeQuery("GET_ACH_INFO", new Object[] {achId});
Results files = DataLayerMgr.executeQuery("GET_ACH_FILES", new Object[] {achId});
Results docs = DataLayerMgr.executeQuery("GET_DOCS_BY_ACH", new Object[] {achId});

String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false); %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="fullPageFormContainer">

	<div class="tableHead">
		<div class="tabHeadTxt"><span class="txtWhiteBold">ACH Manager</span></div>
	</div>
	<table>
	<% if (!StringUtils.isBlank(msg)) { %>
		<tr><td class="status-info"><%=msg%></td></tr>
	<% } %>
	<% if (!StringUtils.isBlank(err)) { %>
		<tr><td class="status-error"><%=StringUtils.encodeForHTML(err)%></td></tr>
	<% } %>
	</table>
	<% if (ach.next()) { %>
	<div class="tableContent">
		<form name="achForm" id="achForm" method="post" action="manageACH.i">
			<input type="hidden" name="achId"  id="achId" value="<%=achId %>"/>
			<div class="gridHeader">ACH ID <%=achId %></div>
			<table class="grid">
				<tr>
					<td class="label">Created By</td>
					<td class="control"><%=ach.getFormattedValue("CREATE_USER_NAME") %></td>
					<td class="label">Created Time</td>
					<td class="control"><%=ach.getFormattedValue("CREATE_TSF") %></td>
				</tr>
				<tr>
					<td class="label">Credits</td>
					<td class="control"><%=ach.getFormattedValue("CREDIT_COUNT") %> / <%=ach.getFormattedValue("CREDIT_TOTAL") %> <%=ach.getFormattedValue("CURRENCY_CD") %></td>
					<td class="label">USAT Routing #</td>
					<td class="control"><%=ach.getFormattedValue("USAT_ROUTING_NUM") %></td>
				</tr>
				<tr>
					<td class="label">Debits</td>
					<td class="control"><%=ach.getFormattedValue("DEBIT_COUNT") %> / <%=ach.getFormattedValue("DEBIT_TOTAL") %> <%=ach.getFormattedValue("CURRENCY_CD") %></td>
					<td class="label">USAT Bank Account #</td>
					<td class="control"><%=ach.getFormattedValue("USAT_BANK_ACCT_NUM") %></td>
				</tr>
			</table>
			<div class="spacer10"></div>
			<div class="gridHeader">Generated ACH Files</div>
			<table class="grid">
				<tr>
					<td class="label">ACH File Id</td>
					<td class="label">Created</td>
					<td class="label">Status</td>
					<td class="label">Uploaded</td>
					<td class="label">Cancelled</td>
					<td class="label">File Name</td>
					<td class="label"></td>
				</tr>
				<% 	boolean isUploaded = false;
						boolean canRegenerate = true;
						while (files.next()) {
							String expired = files.getFormattedValue("IS_EXPIRED");
							String statusCd = files.getFormattedValue("STATUS_CD");
							boolean canUpload = false;
							if (statusCd.equals("M") || statusCd.equals("A")) {
								isUploaded = true;
								canUpload = false;
								canRegenerate = false;
							} else if (!isUploaded && expired.equals("N") && statusCd.equals("N")) {
								canUpload = true; 
								canRegenerate = false;
							} else if (statusCd.equals("C")) {
								canRegenerate = false;
							} %>
				<tr>
					<td class="control"><%=files.getFormattedValue("ACH_FILE_ID") %></td>
					<td class="control"><%=files.getFormattedValue("CREATE_TSF") %> by <%=files.getFormattedValue("CREATE_USER_NAME") %></td>
					<td class="control"><%=files.getFormattedValue("STATUS") %> <%=statusCd.equals("N") ? "(" + files.getFormattedValue("TIME_REMAINING") + ")" : ""%></td>
					<td class="control"><% if (files.getValue("UPLOAD_TSF") != null) { %><%=files.getFormattedValue("UPLOAD_TSF") %> by <%=files.getFormattedValue("UPDATE_USER_NAME") %><% } %></td>
					<td class="control"><% if (files.getValue("CANCEL_TSF") != null) { %><%=files.getFormattedValue("CANCEL_TSF") %> by <%=files.getFormattedValue("UPDATE_USER_NAME") %><% } %></td>
					<td class="control"><%=files.getFormattedValue("FILE_TRANSFER_NAME") %></td>
					<td class="control">
						<% if (canUpload && user.hasPrivilege(DmsPrivilege.DMS_ACH_ADMINS.getValue())) { %>
						<input type="button" value="View" onclick="javascript:window.location='manageACHFile.i?achFileId=<%=files.getFormattedValue("ACH_FILE_ID")%>'" />
						<% } %>
						<% if (canUpload) { %>
						<input type="button" value="Cancel" onclick="javascript:window.location='manageACH.i?action=Cancel&achId=<%=achId%>&achFileId=<%=files.getFormattedValue("ACH_FILE_ID")%>'" />
						<% } %>
					</td>
				</tr>
				<% } %>
			</table>
			<% if (canRegenerate) { %>
			<div class="spacer10"></div>
			<table class="grid">
				<tr>
					<td>
						<input type="submit" id="action" name="action" value="Regenerate ACH File"/>
					</td>
				</tr>
			</table>
			<% } %>
			<div class="spacer10"></div>
			<div class="gridHeader">Included EFTs</div>
			<table class="grid">
				<tr>
					<td class="label">EFT ID</td>
					<td class="label">Customer</td>
					<td class="label">Status</td>
					<td class="label">Amount</td>
					<td class="label">Batch Reference #</td>
					<td class="label">Routing #</td>
					<td class="label">Account	 #</td>
				</tr>
				<% while (docs.next()) { %>
				<tr>
					<td class="control"><a href="manageEFT.i?eftId=<%=docs.getFormattedValue("EFT_ID") %>"><%=docs.getFormattedValue("EFT_ID") %></td>
					<td class="control"><%=docs.getFormattedValue("CUSTOMER_NAME") %></td>
					<td class="control"><%=docs.getFormattedValue("STATUS") %></td>
					<td class="control"><%=docs.getFormattedValue("EFT_FMT_AMOUNT") %></td>
					<td class="control"><%=docs.getFormattedValue("BATCH_REF_NBR") %></td>
					<td class="control"><%=docs.getFormattedValue("BANK_ROUTING_NBR") %></td>
					<td class="control"><%=docs.getFormattedValue("BANK_ACCT_NBR") %></td>
				</tr>
				<% } %>
			</table>
		</form>
	</div>
	<% } else { %>
	<div align="center">Not Found</div>
	<% } %>
	<div class="spacer10"></div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
