<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.BasicServletUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>

<%
BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
String emailAddress = user.getEmailAddress();
String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false);
if(StringUtils.isBlank(msg) && StringUtils.isBlank(err)) {
	for(MessagesInputSource.Message message : RequestUtils.getMessages(request)) {
		if("error".equals(message.getType()) || "warn".equals(message.getType())) {
			if(StringUtils.isBlank(err))
				err = message.toHtml();
            else
            	err += "<br/>" + message.toHtml();
		} else {
			if(StringUtils.isBlank(msg))
				msg = message.toHtml();
			else
				msg += "<br/>" + message.toHtml();
		}
	}
}
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="formContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Add EFT Adjustments</span></div>
</div>

<div class="tableContent">

<form method="post" action="addEFTAdjustments.i" onsubmit="return doSubmit()" enctype="multipart/form-data">

<table class="padding3">
	<%if(msg != null && msg.length() > 0) {%>
	<tr><td colspan="2" class="status-info"><%=msg%></td></tr>
	<%} %>
	<%if(err != null && err.length() > 0) {%>
	<tr><td colspan="2" class="status-error"><%=err%></td></tr>
	<%} %>
		
	<tr>
		<td class="label" valign="top">Serial Numbers<font color="red">*</font><br />(1 per line)</td>
		<td>
			<textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"></textarea>
		</td>
	</tr>
	<tr>
		<td class="label">Amount<font color="red">*</font></td>
		<td>
			<input type="text" name="amount" id="amount" />
		</td>
	</tr>
	<tr>
		<td class="label">Explanation<font color="red">*</font></td>
		<td>
			<textarea name="reason" id="reason" rows="5" cols="40"></textarea>	
		</td>
	</tr>
	<tr>
		<td colspan="2" class="label">or Upload a CSV File with these values in each line: {Serial #},{Amount},{Explanation}</td>
	</tr>
	<tr>
		<td colspan="2">
	  		<input type="file" name="file_data" id="file_data" size="80"/>
	  	</td>
	<tr>
		<td colspan="2" class="label">Enter an email that will receive notification that the processing is complete (optional):</td>	  	
	<tr>
		<td>
			<input type="email" name="notifyEmail" value="<%=emailAddress%>"/>
		</td>
	<tr>
		<td colspan="2" align="center">
			<input type="submit" value="Submit" class="cssButton" />
		</td>
	</tr>
</table>

</form>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
function doSubmit(){
	if (document.getElementById("file_data").value.trim() == '') {
		if(document.getElementById("dev_list").value.trim() == ''){
			alert('Please enter serial numbers');
			return false;
		}
		
		if(document.getElementById("amount").value.trim() == ''){
			alert('Please enter amount');
			return false;
		}
		
		if(document.getElementById("reason").value.trim() == ''){
			alert('Please enter explanation');
			return false;
		}
	}

	return true;
}
</script>
