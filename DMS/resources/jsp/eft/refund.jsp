<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results,java.math.BigDecimal"%>
<%@page import="java.util.Vector"%>

<% 
Results adj = RequestUtils.getAttribute(request, "adj", Results.class, false);
Results eft = RequestUtils.getAttribute(request, "eft", Results.class, false);

Vector<String> arr = new Vector<String>(0);
arr.add("DESCRIPTION");
arr.add("TERMINAL_NBR");
arr.add("CARD_NUMBER");
arr.add("REFUND_AMOUNT");
arr.add("ORIG_DATE");
arr.add("REFUND_DATE");
arr.add("CC_APPR_CODE");

long eftId = 0;
String num = "";
String status = "";
if(eft !=null && eft.next()){
	eftId = eft.getValue("EFT_ID", long.class);
	num = eft.getFormattedValue("BATCH_REF_NBR");
	status = eft.getFormattedValue("STATUS");
}

%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Refunds &amp; Chargebacks</span></div>
</div>

<div class="tableContent">

<div class="gridHeader">EFT ID <%=eftId %>, Batch # <%=num %></div>

<form name="refForm" id="refForm" method="post" action="manageEFT.i">
<input type="hidden" name="eftAction" id="eftAction" value="refund" />
<input type="hidden" name="eftId" id="eftId" value="<%=eftId %>" />

<table class="tabDataDisplayBorder">
		<tr class="gridHeader">
			<td>&nbsp;</td>
			<td>Description</td>
			<td>Terminal</td>
			<td>Card Number</td>
			<td>Refund Amount</td>
			<td>Transaction Date</td>
			<td>Refund Date</td>
			<td>AP Code</td>
		</tr>
		
		<%
		
		if(adj == null || !adj.next()) {
		     out.write("<tr><td align=\"center\" colspan="+(arr.size()+1)+">No records</td></tr>");
		}else{
			int i = 0; 
			String rowClass = "row0";
			do {
				rowClass = (i%2 == 0) ? "row1" : "row0";
				i++;
				out.write("<tr  class="+rowClass+">");
				if("L".equalsIgnoreCase(status)){
					out.write("<td><input type='radio' name='ledgerId' id='ledgerId' value='"+adj.getFormattedValue("LEDGER_ID")+"'></td>");
				}else{
					out.write("<td><input disabled=\"disabled\" type=\"radio\" name='ledgerId' id='ledgerId' value=\""+adj.getFormattedValue("LEDGER_ID")+"\"></td>");
				}
				
				for(int j=0;j<arr.size();j++){
					out.write("<td>"+adj.getFormattedValue(arr.get(j))+"</td>");
				}
				out.write("</tr>");
			}while(adj.next());
		}

		%>
		
</table>
</form>

<%if (eftId > 0) {%>
<div class="spacer10"></div>
<div align="center">
<input <% out.write( (!"L".equalsIgnoreCase(status))?" disabled='disabled' class='cssButtonDisabled' ":"class='cssButton'"); %> name="btn1" id="btn1" type="button" value="Do Not Include In this Payment" onclick="donotInclude()"/>
<input name="close" id="close" type="button" value="Close" onclick="window.location = '/manageEFT.i?eftId=<%=eftId%>';" class="cssButton"/>
</div>
<%}%>
<div class="spacer10"></div>
</div>
</div>

<script type="text/javascript" defer="defer">

var refForm = document.getElementById("refForm");
var ledgerId = document.getElementById("ledgerId");
var eftAction = document.getElementById("eftAction");
var eftId = document.getElementById("eftId");

function donotInclude(){
	if( !ifChecked() ){
		alert('Choose a document please');
		return;
	}
	refForm.submit();
}

function ifChecked(){
	ledgerId = document.getElementById("ledgerId");
	if(refForm.ledgerId.length>0){
		for (i=0; i<refForm.ledgerId.length; i++){
			if(refForm.ledgerId[i].checked)
				return true;
		}
	}else{
		return ledgerId.checked;
	}	
	return false;

}

</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
