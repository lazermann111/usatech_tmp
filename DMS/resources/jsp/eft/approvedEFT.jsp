<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String sortField = PaginationUtil.getSortField(null);
	String sortIndex = inputForm.getString(sortField, false);
	sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "3" : sortIndex;
	boolean norec=false;

	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
	
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Approved EFTs</div>
</div>
<form name="pForm" id="pForm" method="post">
<table>
<% if (!StringUtils.isBlank(msg)) { %>
	<tr><td class="status-info"><%=msg%></td></tr>
<% } %>
<% if (!StringUtils.isBlank(err)) { %>
	<tr><td class="status-error"><%=err%></td></tr>
<% } %>
	<tr>
		<td>
			<input <% out.write( (norec)?" disabled='disabled' class='cssButtonDisabled' ":"class='cssButton'"); %> name="action" type="submit" value="Mark as Paid"/>
			<input <% out.write( (norec)?" disabled='disabled' class='cssButtonDisabled' ":"class='cssButton'"); %> name="action" type="submit" value="Generate ACH Debit"/>
		</td>
	</tr>
</table>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td width="10"><%if (norec) {%>&nbsp; <%} else {%> <input type="checkbox" onclick="onClick()"/> <%}%></td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">EFT ID</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Customer</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Batch #</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Description</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Bank Account #</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Bank Routing #</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">EFT Amount</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
		</tr>
	</thead>
	<tbody>
	<% if (norec) { %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<% } else {
	Results list = (Results) request.getAttribute("resultlist");
	int i = 0; 
	String rowClass = "row0";
	while(list.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++;
	%>
		<tr class="<%=rowClass%>">
			<td><input type="checkbox" name="pid" id="pid" value="<%=list.getFormattedValue(1) %>"></td>
			<td><a href="/manageEFT.i?eftId=<%=list.getFormattedValue(1)%>"><%=list.getFormattedValue(1)%></a></td>
			<td><%=list.getFormattedValue(6)%></td>
			<td><%=list.getFormattedValue(3)%></td>
			<td><%=list.getFormattedValue(4)%></td>
			<td><%=list.getFormattedValue(7)%></td>
			<td><%=list.getFormattedValue(8)%></td>
			<td><%=list.getFormattedValue(9)%></td>
		</tr>
	<% } %>
	</tbody>
</table>

	<jsp:include page="/jsp/include/pagination.jsp" flush="true">
		<jsp:param name="_param_request_url" value="approvedEFT.i" />
	</jsp:include>

<% } %>

</form>

</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">
var checked = false;

function onClick(){
	if(checked){
		uncheckIt();
		checked = false;
	}else{
		checkIt();
		checked = true;
	}
}

function checkIt(){
	if(document.pForm.pid.length>0){
		for (i=0; i<document.pForm.pid.length; i++){
			document.pForm.pid[i].checked=1;
		}
	}else{
		document.pForm.pid.checked=1;
	}
}

function uncheckIt(){
	if(document.pForm.pid.length>0){
		for (i=0; i<document.pForm.pid.length; i++){
			document.pForm.pid[i].checked=0;
		}
	}else{
		document.pForm.pid.checked=0;
	}
}

</script>
