<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String sortField = PaginationUtil.getSortField(null);
	String sortIndex = inputForm.getString(sortField, false);
	sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "-2" : sortIndex;
	boolean norec=false;

	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
	
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
	
	String searchType = inputForm.getStringSafely("search_type", "");
	String searchParam = inputForm.getStringSafely("search_param", "");
	String status = RequestUtils.getAttribute(request, "status", String.class, false);
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">ACH Search</div>
</div>
<div class="spacer10"></div>
<form method="get" action="searchACH.i">
<table>
	<tr>
		<td class="label">Search Type</td>
		<td class="label">Search Value</td>
		<td class="label">ACH Status</td>
		<td class="label"></td>
	</tr>
	<tr>
		<td>
			<select name="search_type">
				<option value="ach_id"<%if ("ach_id".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>ACH ID</option>
				<option value="eft_id"<%if ("eft_id".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>EFT ID</option>
				<option value="customer_name"<%if ("customer_name".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>Customer Name</option>
				<option value="routing_num"<%if ("routing_num".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>Customer Bank Routing Number</option>
				<option value="account_num"<%if ("account_num".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>Customer Bank Account Number</option>
				<option value="account_title"<%if ("account_title".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>Customer Bank Account Title</option>
			</select>
		</td>
		<td>
			<input type="text" name="search_param" maxlength="200" value="<%=searchParam%>" />
		</td>
		<td>
			<select name="status">
				<option value=""<%if (status == null) out.write (" selected=\"selected\"");%>>All</option>
				<option value="N"<%if ("N".equalsIgnoreCase(status)) out.write (" selected=\"selected\"");%>>Not Uploaded</option>
				<option value="U"<%if ("U".equalsIgnoreCase(status)) out.write (" selected=\"selected\"");%>>Uploaded</option>
				<option value="F"<%if ("F".equalsIgnoreCase(status)) out.write (" selected=\"selected\"");%>>Failed</option>
				<option value="C"<%if ("C".equalsIgnoreCase(status)) out.write (" selected=\"selected\"");%>>Cancelled</option>
			</select>
		</td>
		<td>
			<input name="ui_action" type="submit" class="cssButton" value="List ACHs" />
		</td>
	</tr>
</table>
</form>
<div class="spacer10"></div>
<form name="pForm" id="pForm" method="post">
<table>
<% if (!StringUtils.isBlank(msg)) { %>
	<tr><td class="status-info"><%=StringUtils.prepareHTML(msg)%></td></tr>
<% } %>
<% if (!StringUtils.isBlank(err)) { %>
	<tr><td class="status-error"><%=err%></td></tr>
<% } %>
</table>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">ACH ID</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Created</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Credits</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Debits</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Status</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
		</tr>
	</thead>
	<tbody>
	<% if (norec) { %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<% } else {
	Results list = (Results) request.getAttribute("resultlist");
	int i = 0; 
	String rowClass = "row0";
	while(list.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++;
		String afStatus = list.getValue(15, String.class);
	%>
		<tr class="<%=rowClass%>">
			<td><a href="/manageACH.i?achId=<%=list.getFormattedValue(1)%>"><%=list.getFormattedValue(1)%></a></td>
			<td><%=list.getFormattedValue(3)%></td>
			<td><%=list.getFormattedValue(4)%> / <%=list.getFormattedValue(5)%></td>
			<td><%=list.getFormattedValue(6)%> / <%=list.getFormattedValue(7)%></td>
			<td>
				<%=list.getFormattedValue(16)%> <%=afStatus.equals("N") ? "(" + list.getFormattedValue(28) + ")" : ""%>
			</td>
		</tr>
	<% } %>
	</tbody>
</table>

	<jsp:include page="/jsp/include/pagination.jsp" flush="true">
		<jsp:param name="_param_request_url" value="searchACH.i" />
	</jsp:include>

<% } %>

</form>

</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

