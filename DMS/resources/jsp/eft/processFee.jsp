<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results,java.math.BigDecimal"%>
<%@page import="java.util.Vector"%>

<% 
Results adj = RequestUtils.getAttribute(request, "adj", Results.class, false);
Results eft = RequestUtils.getAttribute(request, "eft", Results.class, false);

Vector<String> arr = new Vector<String>(0);
arr.add("TERMINAL_NBR");
arr.add("TRANS_TYPE_NAME");
arr.add("GROSS_AMOUNT");
arr.add("FEE_PERCENT");
arr.add("FIXED_FEE_AMOUNT");
arr.add("MIN_AMOUNT");
arr.add("PROCESS_FEE_AMOUNT");
arr.add("NET_AMOUNT");

long eftId = 0;
String num = "";
if(eft !=null && eft.next()) {
	eftId = eft.getValue("EFT_ID", long.class);
	num = eft.getFormattedValue("BATCH_REF_NBR");
}
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Process Fees</span></div>
</div>

<div class="tableContent">

<div class="gridHeader">EFT ID <%=eftId %>, Batch # <%=num %></div>

<table class="tabDataDisplayBorder">
		<tr class="gridHeader">
			<td>Terminal #</td>
			<td>Tran Type</td>
			<td>Gross Amount</td>
			<td>Fee Percent</td>
			<td>Fixed Fee</td>
			<td>Minimum Fee</td>
			<td>Total Fee Amount</td>
			<td>Net Amount</td>
		</tr>
		
		<%
		
		if(adj == null || !adj.next()) {
		     out.write("<tr><td align=\"center\" colspan="+arr.size()+">No records</td></tr>");
		}else{
			int i = 0; 
			String rowClass = "row0";
			do {
				rowClass = (i%2 == 0) ? "row1" : "row0";
				i++;
				out.write("<tr class="+rowClass+">");
				for(int j=0;j<arr.size();j++){
					out.write("<td>"+adj.getValue(arr.get(j))+"</td>");
				}
				out.write("</tr>");
			}while(adj.next());
		}

		%>
		
</table>

<div class="spacer10"></div>
<div align="center">
<input name="close" id="close" type="button" value="Close" onclick="window.location = '/manageEFT.i?eftId=<%=eftId%>';" class="cssButton"/>
</div>
<div class="spacer10"></div>
</div>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
