<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<% 
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "eft");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("eftId"), ""));

Results eft = RequestUtils.getAttribute(request, "eft", Results.class, false);
Results adj = RequestUtils.getAttribute(request, "adj", Results.class, false);
Results ach = RequestUtils.getAttribute(request, "ach", Results.class, false);
String eftAction = RequestUtils.getAttribute(request, "eftAction", String.class, false);

String eftId="";
String customerId="";
String customer="";
String accountNbr="";
String settledAmount="";
String status="";
String statusName="";
String processFee="";
String processFeeCommission="";
String currency="";
String refundAmount="";
String chargebackAmount="";
String accountTitle="";
String serviceFee="";
String serviceFeeCommission="";
String batchDate="";
String totalAdj="";
String batchNbr="";
String netAmount="";
String eftMemo="";
String decAmount="";
String method="";
String bankId ="";
String businessUnitName = "";
String totalAmount = "";
String autoProcessed = "";
String autoProcessStartTs = "";
String autoProcessEndTs = "";
String sentDate = "";
String processedBy = "";
boolean found = false;

if(eft != null && eft.next()) {
	 found = true;
	 eftId = eft.getFormattedValue("EFT_ID");
	 customerId = eft.getFormattedValue("CUSTOMER_ID");
     customer = eft.getFormattedValue("CUSTOMER_NAME");
     accountNbr = eft.getFormattedValue("BANK_ACCT_NBR");
     settledAmount = eft.getFormattedValue("GROSS_AMOUNT");
     status = eft.getFormattedValue("STATUS");
     statusName = eft.getFormattedValue("STATUS_NAME");
     processFee = eft.getFormattedValue("PROCESS_FEE_AMOUNT");
     processFeeCommission = eft.getFormattedValue("PROCESS_FEE_COMMISSION_AMOUNT");
     currency = eft.getFormattedValue("CURRENCY_CODE");
     refundAmount = eft.getFormattedValue("REFUND_AMOUNT");
     chargebackAmount = eft.getFormattedValue("CHARGEBACK_AMOUNT");
     accountTitle = eft.getFormattedValue("ACCOUNT_TITLE");
     serviceFee = eft.getFormattedValue("SERVICE_FEE_AMOUNT");
     serviceFeeCommission = eft.getFormattedValue("SERVICE_FEE_COMMISSION_AMOUNT");
     batchDate = eft.getFormattedValue("BATCH_DATE");
     totalAdj = eft.getFormattedValue("ADJUST_AMOUNT");
     batchNbr = eft.getFormattedValue("BATCH_REF_NBR");
     netAmount = eft.getFormattedValue("NET_AMOUNT");
     eftMemo = eft.getFormattedValue("DESCRIPTION");
     decAmount = eft.getFormattedValue("FAILED_AMOUNT");
     method = eft.getFormattedValue("PAYMENT_METHOD");
     bankId = eft.getFormattedValue("CUSTOMER_BANK_ID");
     businessUnitName = eft.getFormattedValue("BUSINESS_UNIT_NAME");
     totalAmount = eft.getFormattedValue("TOTAL_AMOUNT");
     autoProcessed = eft.getFormattedValue("AUTO_PROCESSED");
     autoProcessStartTs = eft.getFormattedValue("AUTO_PROCESS_START_TS");
     autoProcessEndTs = eft.getFormattedValue("AUTO_PROCESS_END_TS");
     sentDate = eft.getFormattedValue("SENT_DATE");
     processedBy = eft.getFormattedValue("PROCESSED_BY");
}

%>


<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="fullPageFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">EFT Manager</span></div>
</div>

<div class="tableContent">

<%if (found) {%>

<form name="eftForm" id="eftForm" method="post" action="manageEFT.i">
<input type="hidden" name="eftId"  id="eftId" value="<%=eftId %>"></input>
<input type="hidden" name="eftAction" id="eftAction" />
<input type="hidden" name="bankId" id="bankId" value="<%=bankId %>"/>

<div class="gridHeader">EFT ID <%=eftId %></div>
<table class="grid">
		<tr>
			<td class="label">Customer</td>
			<td class="control"><a href="/customer.i?customerId=<%=customerId%>"><%=customer %></a></td>

			<td class="label">Account #</td>
			<td class="control"><a href="/account.i?accountId=<%=bankId%>&customerId=<%=customerId%>"><%=accountNbr %></a></td>
		</tr>
		
		<tr>		

			<td class="label">Status</td>
			<td class="control">
				<%=statusName%>
			</td>
			
			<td class="label">Currency</td>
			<td class="control">
				<%=currency %>
			</td>			
		</tr>
		
		<tr>
			<td class="label">Account Title</td>
			<td class="control">
				<%=accountTitle %>
			</td>
			
			<td class="label">Batch Date</td>
			<td class="control">
				<%=batchDate %>
			</td>			
		</tr>
		
		<tr>
			<td class="label">Business Unit</td>
			<td class="control">
				<%=businessUnitName%>
			</td>
			
			<td class="label">Method</td>
			<td class="control">
				<%=method %>
			</td>
		</tr>
		
		<tr>
			<td class="label">EFT Batch #</td>
			<td class="control">
				<%=batchNbr %>
			</td>
			
			<td class="label">EFT Memo</td>
			<td class="control">
				<%=eftMemo %>
			</td>
		</tr>
		
		<tr>
			<td class="label">Customer Status</td>
			<td><%=eft.getFormattedValue("CUSTOMER_STATUS")%></td>
			<td class="label">Customer ID</td>
			<td><%=eft.getFormattedValue("CUSTOMER_ID")%></td>
		</tr>
		
		<tr>
			<td class="label">Address Name</td>
			<td><%=eft.getFormattedValue("ADDRESS_NAME")%></td>
			<td class="label">Address 1</td>
			<td><%=eft.getFormattedValue("ADDRESS1")%></td>
		</tr>
		
		<tr>
			<td class="label">Address 2</td>
			<td><%=eft.getFormattedValue("ADDRESS2")%></td>
			<td class="label">City</td>
			<td><%=eft.getFormattedValue("CITY")%></td>
		</tr>
		
		<tr>
			<td class="label">State</td>
			<td><%=eft.getFormattedValue("STATE")%></td>
			<td class="label">Postal</td>
			<td><%=eft.getFormattedValue("ZIP")%></td>
		</tr>
		
		<tr>
			<td class="label">Country</td>
			<td><%=eft.getFormattedValue("COUNTRY_NAME")%></td>
			<td class="label">Email</td>
			<td><%=eft.getFormattedValue("EMAIL")%></td>
		</tr>
		
		<tr>
			<td class="label">First Name</td>
			<td><%=eft.getFormattedValue("FIRST_NAME")%></td>
			<td class="label">Last Name</td>
			<td><%=eft.getFormattedValue("LAST_NAME")%></td>
		</tr>
		
		<%if ("L".equalsIgnoreCase(status) || "O".equalsIgnoreCase(status)) {%>
		<tr>		
			<td class="label">Settled Amount</td>
			<td class="control">
				<%=settledAmount %>
			</td>	
		
  			<td class="label">Process Fees</td>
			<td class="control">
				<%=processFee %>
			</td>							
		</tr>
		
		<tr>
			<td class="label">Refund Amount</td>
			<td class="control">
				<%=refundAmount %>
			</td>
      
            <td class="label">Process Fee Commissions</td>
            <td class="control">
              <%=processFeeCommission %>
            </td>             
			
		</tr>
			
		<tr>
			<td class="label">Chargeback Amount</td>
			<td class="control">
				<%=chargebackAmount %>
			</td>
		
            <td class="label">Service Fees</td>
            <td class="control">
              <%=serviceFee %>
            </td>
		</tr>	

		<tr>
			<td class="label">Net Amount</td>
			<td class="control">
				<%=netAmount %>
			</td>	
		
            <td class="label">Service Fee Commissions</td>
            <td class="control">
              <%=serviceFeeCommission %>
            </td>
  		</tr>

        <tr>
            <td class="label">Total Adjustments</td>
            <td class="control">
              <%=totalAdj %>
            </td>
            <td class="label">Declined Amount</td>
            <td class="control">
              <%=decAmount %>
            </td> 
        </tr>



		<%} else {%>
		<tr>
			<td class="label">EFT Amount</td>
			<td class="control"><%=totalAmount %></td>
			<td class="label">Auto Processed</td>
			<td class="control"><%=autoProcessed %></td>
		</tr>
		<tr>
			<td class="label">Auto Processing Started</td>
			<td class="control"><%=autoProcessStartTs %></td>
			<td class="label">Auto Processing Ended</td>
			<td class="control"><%=autoProcessEndTs %></td>
		</tr>
		<tr>
			<td class="label">Processed Date</td>
			<td class="control"><%=sentDate%></td>
			<td class="label">Processed By</td>
			<td class="control"><%=processedBy%></td>
		</tr>
		<tr>
			<td class="label">ACHs</td>
			<td class="control" colspan="3">
			<% while(ach.next()) { %>
				<a href="/manageACH.i?achId=<%=ach.getFormattedValue("ACH_ID")%>"><%=ach.getFormattedValue("ACH_ID")%></a>
			<% } %>
			</td>
		</tr>
		<%}%>
	</table>
	
	<table style="width: 100%;">
		<tr>
			<td class="control">
				&nbsp;&nbsp;
				<input name="lockBtn" id="lockBtn" type="button" value="Lock"  <%if(!"O".equalsIgnoreCase(status)) out.write("disabled='disabled'");%> onclick="lock()"/>
				<input name="unlockBtn" id="unlockBtn" type="button" value="Unlock" <%if(!"L".equalsIgnoreCase(status)) out.write("disabled='disabled'"); %> onclick="unlock()"/>
				<input name="approveBtn" id="approveBtn" type="button" value="Approve" <%if(!"L".equalsIgnoreCase(status)) out.write("disabled='disabled'"); %> onclick="approve()" />
				<input name="serviceBtn" id="serviceBtn" type="button" value="Service Fees" onclick="showServiceFee();"/>
				<input name="serviceComBtn" id="serviceComBtn" type="button" value="Service Fee Commissions" onclick="window.location = '/serviceFeeCommissions.i?eftId='+eftId.value;"/>
				<input name="processBtn" id="processBtn" type="button" value="Process Fees" onclick="showProcessFee();"/>
				<input name="processComBtn" id="processComBtn" type="button" value="Process Fee Commissions" onclick="window.location = '/processFeeCommissions.i?eftId='+eftId.value;"/>
				<input name="refundBtn" id="refundBtn" type="button" value="Refunds &amp; Chargebacks" onclick="showRefund();"/>
			</td>
		</tr>
		
		<tr>
			<td class="control">
			<div class="spacer5"></div>
			<span class="label">Adjustment Notes</span>
			<div class="spacer5"></div>
			<select name="adjId" id="adjId" size="10" style="width: 100%;" >
<%while(adj.next()) {%>
<option value="<%=adj.getFormattedValue("ADJUST_ID")%>">
<%=adj.getFormattedValue("AMOUNT")%> on <%=adj.getFormattedValue("CREATE_DATE")%> <%=adj.getFormattedValue("REASON")%> <%=adj.getFormattedValue("TERMINAL_NBR")%></option>	    	
<%}%>
			</select>
			</td>

		</tr>
		
		<tr>
			<td>
				&nbsp;&nbsp;
				<input name="addAdj" id="addAdj" type="button" value="Add" onclick="addNote()"<%if(!"O".equalsIgnoreCase(status)) out.write(" disabled='disabled'");%>/>
				<input name="editAdj" id="editAdj" type="button" value="Edit" onclick="editNote()"<%if(!"O".equalsIgnoreCase(status)) out.write(" disabled='disabled'");%>/>
				<input name="deleteAdj" id="deleteAdj" type="button" value="Delete" onclick="deleteAdjustment()"<%if(!"O".equalsIgnoreCase(status)) out.write(" disabled='disabled'");%>/>
			</td>
		</tr>
</table>

</form>

<script type="text/javascript" defer="defer">

var eftId = document.getElementById("eftId");
var adjId = document.getElementById("adjId");
var eftAction =  document.getElementById("eftAction");
var bankId = document.getElementById("bankId");

var customerName = document.getElementById("customerName");
var accountNbr = document.getElementById("accountNbr");
var selectedAmount = document.getElementById("selectedAmount");
var status = document.getElementById("status");
var processFee = document.getElementById("processFee");
var currency = document.getElementById("currency");
var rfnd = document.getElementById("rfnd");
var accountTitle = document.getElementById("accountTitle");
var serviceFee = document.getElementById("serviceFee");
var bathcDate = document.getElementById("bathcDate");
var totalAdj =document.getElementById("totalAdj");
var batchNbr = document.getElementById("batchNbr");
var netAmount =document.getElementById("netAmount");
var eftMemo = document.getElementById("eftMemo");
var declinedAmount = document.getElementById("declinedAmount");
var method =document.getElementById("method");
var eftForm =document.getElementById("eftForm");


function editNote(){
	if( adjId.value == null ||adjId.value=='' ){
		alert('Choose a note please');
		return;
	}
	window.location = "adjustmentNote.i?action=updateAdjustment&adjId="+adjId.value+"&bankId="+bankId.value+"&eftId="+eftId.value;
}

function addNote(){
	window.location = "adjustmentNote.i?action=addAdjustment&bankId="+bankId.value+"&eftId="+eftId.value;
}

function showServiceFee(){
	window.location = 'serviceFee.i?eftId='+eftId.value;
}

function showProcessFee(){
	window.location = "processFee.i?eftId="+eftId.value;
}

function showRefund(){
	window.location = "refundEFT.i?eftId="+eftId.value;
}

function deleteAdjustment(){
	if( adjId.value == null || adjId.value=='' ){
		alert('Choose a note please');
		return;
	}
	if(!confirm('Are you sure you want to delete this adjustment?')){
		return;
	}

	eftAction.value="deleteAdjustment";
	eftForm.submit();
}

function lock(){
	eftAction.value="lock";
	eftForm.submit();
}

function unlock(){
	eftAction.value="unlock";
	eftForm.submit();
}

function approve(){
	eftAction.value="approve";
	eftForm.submit();
}

</script>

<% } else { %>

<div align="center">Not Found</div>
<div class="spacer10"></div>

<%}%>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
