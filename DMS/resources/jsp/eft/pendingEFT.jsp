<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.ArrayList, java.util.Iterator, java.util.List" %>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:useBean id="dms_values_list_payCyclesList" class="com.usatech.dms.util.GenericList" scope="application" />
<jsp:useBean id="dms_values_list_businessUnitsList" class="com.usatech.dms.util.GenericList" scope="application" />
<jsp:useBean id="dms_values_list_currenciesList" class="com.usatech.dms.util.GenericList" scope="application" />

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String sortField = PaginationUtil.getSortField(null);
	String sortIndex = inputForm.getString(sortField, false);
	sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
	boolean norec=false;
  
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
	
	List<NameValuePair> nvp_list_pay_cycles = dms_values_list_payCyclesList.getList();
	List<NameValuePair> nvp_list_business_units = dms_values_list_businessUnitsList.getList();
	List<NameValuePair> nvp_list_currencies = dms_values_list_currenciesList.getList();
	
	String uiAction = inputForm.getStringSafely("ui_action", "");
	String searchType = inputForm.getStringSafely("search_type", "");
	String searchParam = inputForm.getStringSafely("search_param", "");
	String payCycle = inputForm.getStringSafely("pay_cycle", "");
	String businessUnit = inputForm.getStringSafely("business_unit", "");
	String currency = inputForm.getStringSafely("currency", "");
	String dataFormat = inputForm.getStringSafely("data_format", "");
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Pending EFTs</div>
</div>

<div class="spacer5"></div>

<form method="get" action="pendingEFT.i">
<table>
	<tr>
		<td class="label">Search Type</td>
		<td class="label">Pay Cycle</td>
		<td class="label">Business Unit</td>
		<td class="label">Currency</td>
		<td class="label">Format</td>
		<td class="label">Status</td>
	</tr>
	<tr>
		<td>
		<select name="search_type">
			<option value="customer_name"<%if ("customer_name".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>Customer Name</option>
			<option value="account_title"<%if ("account_title".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>Account Title</option>
			<option value="eft_id"<%if ("eft_id".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>EFT ID</option>
			<option value="net_amount"<%if ("net_amount".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>Net Amount &gt;=</option>
			<option value="net_amt_less_eq"<%if ("net_amt_less_eq".equalsIgnoreCase(searchType)) out.write (" selected=\"selected\"");%>>Net Amount &lt;=</option>
		</select>
		<input type="text" name="search_param" maxlength="200" value="<%=searchParam%>" />
		</td>
		<td>
		<select name="pay_cycle">
		<option value="">All Pay Cycles</option>
		<% 
		Iterator<NameValuePair> payCycleIt = nvp_list_pay_cycles.iterator();
		while(payCycleIt.hasNext()){
			NameValuePair payCycleNVP = payCycleIt.next();
		%>
			<option value="<%=payCycleNVP.getValue()%>"<%if (payCycle.equalsIgnoreCase(payCycleNVP.getValue())) out.write (" selected=\"selected\"");%>><%=payCycleNVP.getName()%></option>
		<% } %>
		</select>
		</td>
		<td>
		<select name="business_unit">
		<option value="">All Business Units</option>
		<% 
		Iterator<NameValuePair> businessUnitIt = nvp_list_business_units.iterator();
		while(businessUnitIt.hasNext()){
		    NameValuePair businessUnitNVP = businessUnitIt.next();
		%>
			<option value="<%=businessUnitNVP.getValue()%>"<%if (businessUnit.equalsIgnoreCase(businessUnitNVP.getValue())) out.write (" selected=\"selected\"");%>><%=businessUnitNVP.getName()%></option>
		<% } %>
		</select>
		</td>
		<td>
		<select name="currency">
		<option value="">All Currencies</option>
		<% 
		Iterator<NameValuePair> currencyIt = nvp_list_currencies.iterator();
		while(currencyIt.hasNext()){
		    NameValuePair currencyNVP = currencyIt.next();
		%>
			<option value="<%=currencyNVP.getValue()%>"<%if (currency.equalsIgnoreCase(currencyNVP.getValue())) out.write (" selected=\"selected\"");%>><%=currencyNVP.getName()%></option>
		<% } %>
		</select>
		</td>
		<td>
		<select name="data_format">
			<option value="HTML"<%if ("HTML".equalsIgnoreCase(dataFormat)) out.write (" selected=\"selected\""); %>>HTML</option>
			<option value="CSV"<%if ("CSV".equalsIgnoreCase(dataFormat)) out.write (" selected=\"selected\""); %>>CSV</option>
		</select>
		</td>
		<td>
		<select name="status">
			<option value="">All Statuses</option>
			<option value="Locked">Locked</option>
		</select>
		</td>
		<td rowspan="2">
		<input name="ui_action" type="submit" class="cssButton" value="List EFTs" />
		</td>
</table>
</form>

<div class="spacer10"></div>

<form name="pForm" id="pForm" method="post">
<%if ("List EFTs".equalsIgnoreCase(uiAction)) {%>
<%if (!norec) { %>
<table>
<tr>
	<td>
	<input name="action" type="submit" value="Lock" class="cssButton" />
	<input name="action" type="submit" value="Unlock" class="cssButton" />
	<input name="action" type="submit" value="Approve" class="cssButton" />
	</td>
</tr>
</table>
<div class="spacer5"></div>
<%} %>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td width="10"><%if (norec) {%>&nbsp; <%} else {%> <input type="checkbox" onclick="onClick()"/> <%}%></td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "18", sortIndex)%>">EFT ID</a>
				<%=PaginationUtil.getSortingIconHtml("18", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Customer</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Account Title</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Account #</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Currency</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
		    <td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Business<br/>Unit</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Method</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Settled</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Refunds &amp;<br/>Chargebacks</a>
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">Process Fees</a>
				<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
			</td>
            <td>
              <a href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Process Fee<br/>Commissions</a>
              <%=PaginationUtil.getSortingIconHtml("10", sortIndex)%>
            </td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "11", sortIndex)%>">Service Fees</a>
				<%=PaginationUtil.getSortingIconHtml("11", sortIndex)%>
			</td>
            <td>
              <a href="<%=PaginationUtil.getSortingScript(null, "12", sortIndex)%>">Service Fee<br/>Commissions</a>
              <%=PaginationUtil.getSortingIconHtml("12", sortIndex)%>
            </td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "13", sortIndex)%>">Adjustments</a>
				<%=PaginationUtil.getSortingIconHtml("13", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "14", sortIndex)%>">Net Amount</a>
				<%=PaginationUtil.getSortingIconHtml("14", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "15", sortIndex)%>">Declined</a>
				<%=PaginationUtil.getSortingIconHtml("15", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "16", sortIndex)%>">Batch Date</a>
				<%=PaginationUtil.getSortingIconHtml("16", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "17", sortIndex)%>">Status</a>
				<%=PaginationUtil.getSortingIconHtml("17", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "19", sortIndex)%>">Customer<br/>Status</a>
				<%=PaginationUtil.getSortingIconHtml("19", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "20", sortIndex)%>">Address Name</a>
				<%=PaginationUtil.getSortingIconHtml("20", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "21", sortIndex)%>">Address 1</a>
				<%=PaginationUtil.getSortingIconHtml("21", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Address 2</a>
				<%=PaginationUtil.getSortingIconHtml("22", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "23", sortIndex)%>">City</a>
				<%=PaginationUtil.getSortingIconHtml("23", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "24", sortIndex)%>">State</a>
				<%=PaginationUtil.getSortingIconHtml("24", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "25", sortIndex)%>">Postal</a>
				<%=PaginationUtil.getSortingIconHtml("25", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "26", sortIndex)%>">First Name</a>
				<%=PaginationUtil.getSortingIconHtml("26", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "27", sortIndex)%>">Last Name</a>
				<%=PaginationUtil.getSortingIconHtml("27", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "28", sortIndex)%>">Email</a>
				<%=PaginationUtil.getSortingIconHtml("28", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "29", sortIndex)%>">Customer ID</a>
				<%=PaginationUtil.getSortingIconHtml("29", sortIndex)%>
			</td>

		</tr>
	</thead>
	<tbody>
	<%
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0){
	    %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<%} else {
	Results list = (Results) request.getAttribute("resultlist");
	int i = 0; 
	String rowClass = "row0";
	while(list.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++;
	%>
		<tr class="<%=rowClass%>">
		<td>
			<input type="checkbox" name="pid" id="pid" value="<%=list.getFormattedValue("EFT_ID")%>" />
			<input type="hidden" name="pid_status_<%=list.getFormattedValue("EFT_ID")%>" value="<%=list.getFormattedValue("STATUS")%>" />
		</td>
		<td><a href="/manageEFT.i?eftId=<%=list.getFormattedValue("EFT_ID")%>"><%=list.getFormattedValue("EFT_ID")%></a></td>
		<td><%=list.getFormattedValue("CUSTOMER_NAME")%></td>
		<td><%=list.getFormattedValue("ACCOUNT_TITLE")%></td>
  		<td><%=list.getFormattedValue("BANK_ACCT_NBR")%></td>
	    <td><%=list.getFormattedValue("CURRENCY_CODE")%></td>
		<td><%=list.getFormattedValue("BUSINESS_UNIT_NAME")%></td>
    	<td><%=list.getFormattedValue("PAYMENT_METHOD")%></td>
   		<td class="nowrap"><%=list.getFormattedValue("GROSS_AMOUNT_FORMATTED")%></td>
	    <td class="nowrap"><%=list.getFormattedValue("RF_CB_AMOUNT_FORMATTED")%></td>
	    <td class="nowrap"><%=list.getFormattedValue("PROCESS_FEE_AMOUNT_FORMATTED")%></td>
        <td class="nowrap"><%=list.getFormattedValue("PFC_AMOUNT_FORMATTED")%></td>
		<td class="nowrap"><%=list.getFormattedValue("SERVICE_FEE_AMOUNT_FORMATTED")%></td>
        <td class="nowrap"><%=list.getFormattedValue("SFC_AMOUNT_FORMATTED")%></td>
   		<td class="nowrap"><%=list.getFormattedValue("ADJUST_AMOUNT_FORMATTED")%></td>
  		<td class="nowrap"><font color="<%=list.getFormattedValue("NET_AMOUNT_COLOR")%>"><b><%=list.getFormattedValue("NET_AMOUNT_FORMATTED")%></b></font></td>
  		<td class="nowrap"><%=list.getFormattedValue("FAILED_AMOUNT_FORMATTED")%></td>
   		<td><%=list.getFormattedValue("BATCH_DATE")%></td>
	    <td><%=list.getFormattedValue("STATUS")%></td>
	    <td><%=list.getFormattedValue("CUSTOMER_STATUS")%></td>
	    <td><%=list.getFormattedValue("ADDRESS_NAME")%></td>
	    <td><%=list.getFormattedValue("ADDRESS1")%></td>
	    <td><%=list.getFormattedValue("ADDRESS2")%></td>
	    <td><%=list.getFormattedValue("CITY")%></td>
	    <td><%=list.getFormattedValue("STATE")%></td>
	    <td><%=list.getFormattedValue("ZIP")%></td>
	    <td><%=list.getFormattedValue("FIRST_NAME")%></td>
	    <td><%=list.getFormattedValue("LAST_NAME")%></td>
	    <td><%=list.getFormattedValue("EMAIL")%></td>
	    <td><%=list.getFormattedValue("CUSTOMER_ID")%></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%
String storedNames = PaginationUtil.encodeStoredNames(new String[] {
    	DevicesConstants.PARAM_SEARCH_PARAM, DevicesConstants.PARAM_SEARCH_TYPE, "pay_cycle", "business_unit", "currency", "data_format", "ui_action"});
%>

<jsp:include page="/jsp/include/pagination.jsp" flush="true">
    <jsp:param name="_param_request_url" value="pendingEFT.i" />
    <jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

<% }} %>

</form>

</div>
				
<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">
var checked = false;

function onClick(){
	if(checked){
		uncheckIt();
		checked = false;
	}else{
		checkIt();
		checked = true;
	}
}

function checkIt(){
	if(document.pForm.pid.length>0){
		for (i=0; i<document.pForm.pid.length; i++){
			document.pForm.pid[i].checked=1;
		}
	}else{
		document.pForm.pid.checked=1;
	}
}

function uncheckIt(){
	if(document.pForm.pid.length>0){
		for (i=0; i<document.pForm.pid.length; i++){
			document.pForm.pid[i].checked=0;
		}
	}else{
		document.pForm.pid.checked=0;
	}
}

</script>
