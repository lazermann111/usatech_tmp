<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.text.StringUtils"%>
<%
Results postal = RequestUtils.getAttribute(request, "postal", Results.class, true);
if (postal != null && postal.next()) {
String countryId = ConvertUtils.getStringSafely(request.getParameter("countryId"), "country");
String stateId = ConvertUtils.getStringSafely(request.getParameter("stateId"), "state");
String cityId = ConvertUtils.getStringSafely(request.getParameter("cityId"), "city");
String tzId = ConvertUtils.getStringSafely(request.getParameter("timezoneId"), "time_zone_id");
%>
<script type="text/javascript">	
	try {
		var e;
		if(e = document.getElementById("<%=StringUtils.prepareScript(countryId)%>"))
			e.value = "<%=postal.getFormattedValue("COUNTRY_CD")%>";
		if(e = document.getElementById("<%=StringUtils.prepareScript(stateId)%>"))
            e.value = "<%=postal.getFormattedValue("STATE_CD")%>";
		if(e = document.getElementById("<%=StringUtils.encodeForJavaScript(cityId)%>"))
            e.value = "<%=postal.getFormattedValue("CITY")%>";
		if(e = document.getElementById("<%=StringUtils.prepareScript(tzId)%>"))
            e.value = "<%=postal.getFormattedValue("TIME_ZONE_ID")%>";
	} catch (e) {}
</script>
<%}%>