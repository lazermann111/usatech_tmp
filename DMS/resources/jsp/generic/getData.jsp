<%@page import="simple.text.StringUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
String type = RequestUtils.getAttribute(request, "type", String.class, true);
String subType = ConvertUtils.getStringSafely(request.getParameter("subType"), "");
String defaultValue = ConvertUtils.getStringSafely(request.getParameter("defaultValue"), "");
String defaultName = ConvertUtils.getStringSafely(request.getParameter("defaultName"), "");
String noDataValue = ConvertUtils.getStringSafely(request.getParameter("noDataValue"), "");
String noDataName = ConvertUtils.getStringSafely(request.getParameter("noDataName"), "No data found");
String selected = ConvertUtils.getStringSafely(request.getParameter("selected"), "");
String onChange = ConvertUtils.getStringSafely(request.getParameter("onChange"), "");
String search = ConvertUtils.getStringSafely(request.getParameter("search"), "");
String deviceTypeId = ConvertUtils.getStringSafely(request.getParameter("deviceTypeId"), "");
boolean selectedPassed = !StringHelper.isBlank(selected);
boolean selectedInd = false;
StringBuilder sb = new StringBuilder("<select");
String usatRequired =  ConvertUtils.getStringSafely(request.getParameter("usatRequired"), "");
sb.append(" usatRequired='"+usatRequired+"'");
String label =  ConvertUtils.getStringSafely(request.getParameter("label"), "");
sb.append(" label='"+label+"'");
String query = null;
String id = ConvertUtils.getStringSafely(request.getParameter("id"), null);
String name = null;
String valueName = "aValue";
String labelName = "aLabel";
String fieldName = null;
String postScript = ConvertUtils.getStringSafely(request.getParameter("postScript"), "");
Map<String, Object> params = new HashMap<String, Object>();
boolean singleValue = false;
String innerHTML = "&nbsp;";
if ("app_req_params".equalsIgnoreCase(type)) {
	singleValue = true;
	query = "GET_APP_REQUEST";
	fieldName = "REQUEST_PARAMETERS";
	params.put("appRequestId", request.getParameter("item_id"));
} else if ("bank_account".equalsIgnoreCase(type)) {
	query = "GET_ALL_BANK_ACCOUNTS";
	id = "customer_bank_id";
	params.put("search", search);
	sb.append(" name='customer_bank_id' id='customer_bank_id' onchange='getData(&quot;type=customer_dealers&customerBankId=&quot; + this.value)'>");
} else if ("commission_bank_account".equalsIgnoreCase(type)) {
	long customerBankId = ConvertUtils.getLongSafely(request.getParameter("customer_bank_id"), -1);
	if (customerBankId > 0)
		query = "GET_PARENT_BANK_ACCOUNTS";
	else
		query = "GET_ALL_PARENT_BANK_ACCOUNTS";
	id = "commission_bank_id";
	params.put("accountId", request.getParameter("customer_bank_id"));
	sb.append(" name='commission_bank_id' id='commission_bank_id'>");
} else if ("config_template".equalsIgnoreCase(type)) {
	query = "GET_CUSTOM_CONFIG_TEMPLATE_EDIT_LIST";
	id = ConvertUtils.getStringSafely(request.getParameter("id"), "template_info");
	name = ConvertUtils.getStringSafely(request.getParameter("name"), "template_info");
	if (!"editor".equalsIgnoreCase(subType))
		valueName = "configTemplateId";
	params.put("search", search);
	sb.append(" name='").append(name).append("' id='").append(id).append("'");
	if (!StringUtils.isBlank(onChange))
		sb.append(" onChange='").append(onChange).append("'");
	sb.append(">");
} else if ("config_template_by_device_type".equalsIgnoreCase(type)) {
	query = "GET_CONFIG_TEMPLATES";
	id = ConvertUtils.getStringSafely(request.getParameter("id"), "template_info");
	name = ConvertUtils.getStringSafely(request.getParameter("name"), "template_info");
	params.put("device_type_id", deviceTypeId);
	params.put("search", search);
	sb.append(" name='").append(name).append("' id='").append(id).append("'");
	if (!StringUtils.isBlank(onChange))
		sb.append(" onChange='").append(onChange).append("'");
	sb.append(">");
} else if ("consumer_merchant".equalsIgnoreCase(type)) {
	query = "GET_ALL_CONSUMER_MERCHANT_FORMATTED";
	id = "consumer_merchant_id";
	params.put("search", search);
	sb.append(" name='consumer_merchant_id' id='consumer_merchant_id'");
	if ("oper".equalsIgnoreCase(subType))
		sb.append(" style='font-family: courier; font-size: 12px;'");
	sb.append(">");
} else if ("corp_customer".equalsIgnoreCase(type)) {
	query = "GET_ALL_CUSTOMERS_WITH_BANK_ACCOUNTS";
	id = ConvertUtils.getStringSafely(request.getParameter("id"), "customerId");
	name = ConvertUtils.getStringSafely(request.getParameter("name"), "customerId");
	sb.append(" name='").append(name).append("' id='").append(id).append("'");
	if ("oper".equalsIgnoreCase(subType)) {
		sb.append(" style='font-family: courier; font-size: 12px;'");
		if ("corp_customer_name".equalsIgnoreCase(onChange))
			sb.append(" onchange='selectChange(this);'");
		sb.append(">");
	} else
		sb.append(" onchange='customerChange();'>");
	valueName = "CUSTOMER_ID";
	labelName = "CUSTOMER_NAME";
	params.put("search", search);
} else if ("credential".equalsIgnoreCase(type)) {
	query = "GET_CREDENTIALS";
	valueName = "credentialId";
	labelName = "credentialName";
	id = "credential_id";
	params.put("search", search);
	sb.append(" name='credential_id' id='credential_id'");
	if (!StringUtils.isBlank(onChange))
		sb.append(" onChange='").append(onChange).append("'");
	sb.append(">");
} else if ("customer".equalsIgnoreCase(type)) {
	query = "GET_FORMATTED_CUSTOMER_NAME_LIST";
	id = ConvertUtils.getStringSafely(request.getParameter("id"), "customer_id");
	name = ConvertUtils.getStringSafely(request.getParameter("name"), "customer_id");
	params.put("search", search);
	sb.append(" name='").append(name).append("' id='").append(id).append("' style='font-family: courier; font-size: 12px;'>");
} else if ("customer_dealers".equalsIgnoreCase(type)) {
	query = "GET_CUSTOMER_DEALERS_WITH_LICENSE_BY_BANK";
	params.put("customerBankId", request.getParameter("customerBankId"));
	valueName = "dealerId";
	labelName = "dealerName";
	id = "dealerId";
	sb.append(" name='dealerId' id='dealerId' onchange='updateResellerRequired()'>");
	selectedInd = true;
} else if ("diag_app_version".equalsIgnoreCase(type)) {
	query = "GET_ALL_DIAG_APP_VERSIONS";
	id = "diag_app_version";
	sb.append(" name='diag_app_version' id='diag_app_version' style='font-family: courier; font-size: 12px;'>");
} else if ("firmware_version".equalsIgnoreCase(type)) {
	query = "GET_ALL_FIRMWARE_VERSIONS";
	id = "firmware_version";
	params.put("search", search);
	sb.append(" name='firmware_version' id='firmware_version' style='font-family: courier; font-size: 12px;'>");
} else if ("ptest_version".equalsIgnoreCase(type)) {
	query = "GET_ALL_PTEST_VERSIONS";
	id = "ptest_version";
	sb.append(" name='ptest_version' id='ptest_version' style='font-family: courier; font-size: 12px;'>");
} else if ("license".equalsIgnoreCase(type)) {
	query = "GET_LICENSES";
	id = "license1";
	sb.append(" name='license1' id='license1' size='10' style='width: 300px;''>");
	valueName = "LICENSE_ID";
	labelName = "LICENSE_NAME";
} else if ("location".equalsIgnoreCase(type)) {
	query = "GET_ALL_LOCATIONS_FORMATTED";
	id = ConvertUtils.getStringSafely(request.getParameter("id"), "location_id");
	name = ConvertUtils.getStringSafely(request.getParameter("name"), "location_id");
	params.put("search", search);
	sb.append(" name='").append(name).append("' id='").append(id).append("' style='font-family: courier; font-size: 12px;'");
	if ("location_name".equalsIgnoreCase(onChange))
		sb.append(" onchange='selectChange(this);'");
	sb.append(">");
} else if ("mapped_terminals".equalsIgnoreCase(type)) {
	long customerId = ConvertUtils.getLongSafely(request.getParameter("customerId"), -1);
	if (customerId > 0) {
		query = "MAPPED_TERMINALS";
		params.put("customerId", customerId);
		valueName = "TERMINAL_ID";
		labelName = "TERMINAL_NBR";
	}
	id = "map2";
	sb.append(" name='map2' id='map2' size='5' style='width: 330px;' onclick='map2Change()'>");
	postScript = "init(); map2Change();";
} else if ("master_customer".equalsIgnoreCase(type)) {
	query = "GET_ALL_CUSTOMERS";
	id = "mainSelect";
	sb.append(" id='mainSelect' onchange='handleChange(this);'>");
	valueName = "CUSTOMER_ID";
	labelName = "CUSTOMER_NAME";
} else if ("master_dealer".equalsIgnoreCase(type)) {
	query = "GET_DEALERS";
	id = "mainSelect";
	sb.append(" id='mainSelect' onchange='handleChange(this);'>");
	valueName = "DEALER_ID";
	labelName = "DEALER_NAME";
} else if ("master_salesRep".equalsIgnoreCase(type)) {
	query = "GET_ALL_SALES_REPS";
	id = "mainSelect";
	sb.append(" id='mainSelect' onchange='handleChange(this);'>");
	valueName = "SALES_REP_ID";
	labelName = "SALES_REP_NAME";
} else if ("master_distributor".equalsIgnoreCase(type)) {
	query = "GET_ALL_DISTRIBUTORS";
	id = "mainSelect";
	sb.append(" id='mainSelect' onchange='handleChange(this);'>");
	valueName = "DISTRIBUTOR_ID";
	labelName = "DISTRIBUTOR_NAME";
} else if ("master_affiliate".equalsIgnoreCase(type)) {
	query = "GET_ALL_AFFILIATES";
	id = "mainSelect";
	sb.append(" id='mainSelect' onchange='handleChange(this);'>");
	valueName = "AFFILIATE_ID";
	labelName = "AFFILIATE_NAME";
} else if ("master_pricingTier".equalsIgnoreCase(type)) {
	query = "GET_ALL_PRICING_TIERS";
	id = "mainSelect";
	sb.append(" id='mainSelect' onchange='handleChange(this);'>");
	valueName = "PRICING_TIER_ID";
	labelName = "PRICING_TIER_NAME";
} else if ("master_category".equalsIgnoreCase(type)) {
	query = "GET_ALL_CATEGORIES";
	id = "mainSelect";
	sb.append(" id='mainSelect' onchange='handleChange(this);'>");
	valueName = "CATEGORY_ID";
	labelName = "CATEGORY_NAME";
} else if ("master_license".equalsIgnoreCase(type)) {
	query = "GET_LICENSES";
	id = "mainSelect";
	sb.append(" id='mainSelect' onchange='handleChange(this);'>");
	valueName = "LICENSE_ID";
	labelName = "LICENSE_NAME";
} else if ("payment_template".equalsIgnoreCase(type)) {
	query = "GET_POS_PTA_TEMPLATE_SELECTIONS";
	id = ConvertUtils.getStringSafely(request.getParameter("id"), "pos_pta_tmpl_id");
	name = ConvertUtils.getStringSafely(request.getParameter("name"), "pos_pta_tmpl_id");
	params.put("search", search);
	sb.append(" name='").append(name).append("' id='").append(id).append("'>");	
} else if ("unmapped_terminals".equalsIgnoreCase(type)) {
	long customerId = ConvertUtils.getLongSafely(request.getParameter("customerId"), -1);
	if (customerId > 0) {
		query = "UNMAPPED_TERMINALS";
		params.put("customerId", customerId);
		valueName = "TERMINAL_ID";
		labelName = "TERMINAL_NBR";
	}
	id = "map1";
	sb.append(" name='map1' id='map1' size='5' style='width: 330px;' onclick='map1Change()'>");
	postScript = "init(); map1Change();";
} else
	throw new ServletException("Data type " + type + " has not been implemented");
if (singleValue) {
	if (query != null) {
		Results results = DataLayerMgr.executeQuery(query, params);
		if (results.next())
			innerHTML = results.getFormattedValue(fieldName);
	}
} else {
	if (query != null) {
		Results results = DataLayerMgr.executeQuery(query, params);
		if (!StringHelper.isBlank(defaultName))
			sb.append("<option value='").append(defaultValue).append("'>").append(defaultName).append("</option>");
		int resultCount = 0;
		while (results.next()) {
			sb.append("<option value='").append(StringUtils.prepareCDATA(results.getFormattedValue(valueName))).append("'");
			if (selectedPassed && selected.equals(results.getFormattedValue(valueName))
					|| selectedInd && "Y".equalsIgnoreCase(results.getFormattedValue("selectedInd")))
				sb.append(" selected='selected'");
			sb.append(">");
			sb.append(StringUtils.prepareCDATA(results.getFormattedValue(labelName))).append("</option>");
			resultCount++;
		}
		if (resultCount == 0) {
			if ("commission_bank_account".equalsIgnoreCase(type))
				postScript = "alert('WARNING: Selected bank account customer should be a child customer to select a reseller bank account!')";
			else if (StringHelper.isBlank(defaultName))
				sb.append("<option value='").append(noDataValue).append("'>").append(noDataName).append("</option>");
		}
	} else
		sb.append(">");
	sb.append("</select>");
}
%>

<script type="text/javascript">
	<%if (singleValue) {%>
	document.getElementById("<%=StringUtils.encodeForJavaScript(id)%>").innerHTML = "<%=innerHTML == null ? "" : innerHTML.replaceAll("[\r\n]", " ").replaceAll("\"", "'")%>";
	<%} else {%>
	document.getElementById("<%=StringUtils.encodeForJavaScript(id)%>").outerHTML = "<%=StringUtils.encodeForJavaScript(sb.toString())%>";
	<%}%>
	document.getElementById("msg").innerHTML = "";
	<%if (type.startsWith("master_")) {%>
	mainSelect = $("mainSelect");
	<%}%>
	<%=StringUtils.encodeForJavaScript(postScript)%>
</script>
