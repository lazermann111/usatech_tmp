<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%><%
String name = RequestUtils.getAttribute(request, "itemName", String.class, true); 
String codeName = simple.text.StringUtils.toJavaName(name);
String sqlName = simple.text.StringUtils.toSqlName(name);
String idField = ConvertUtils.getStringSafely(request.getAttribute("idField"), sqlName + "_ID");
String nameField = ConvertUtils.getStringSafely(request.getAttribute("nameField"), sqlName + "_NAME");
BigDecimal selectedId = RequestUtils.getAttribute(request, codeName + "Id", BigDecimal.class, false); 
Results results = RequestUtils.getAttribute(request, codeName + "s", Results.class, false);
%>
<option value="-9">Choose <%=name %></option>
<%
if (results != null) {
while(results.next()) {
BigDecimal itemId = results.getValue(idField, BigDecimal.class);
%><option value="<%=itemId%>" <% if(selectedId != null && selectedId.compareTo(itemId) == 0){ %> selected="selected"<%} %>><%=results.getFormattedValue(nameField)%></option>	    	
<%}}%>
