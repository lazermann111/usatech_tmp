<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils,simple.text.StringUtils"%>
<%
String name = RequestUtils.getAttribute(request, "itemName", String.class, true); 
String title = RequestUtils.getAttribute(request, "itemTitle", String.class, false);
if(title == null || title.trim().length() == 0)
	title = name;
String codeName = simple.text.StringUtils.toJavaName(name);
String itemDetailJsp = RequestUtils.getAttribute(request, "itemDetailJsp", String.class, true);
String statusMessage = RequestUtils.getAttribute(request, "statusMessage", String.class, false);
String statusType = RequestUtils.getAttribute(request, "statusType", String.class, false);
if(statusMessage == null || (statusMessage=statusMessage.trim()).length() == 0)
	statusMessage = "&nbsp;";
else
	statusMessage = StringUtils.prepareCDATA(statusMessage);
if(statusType == null || (statusType=statusType.trim()).length() == 0)
	statusType = "info";

Boolean noCreateBtn = RequestUtils.getAttribute(request, "noCreateBtn", Boolean.class, false);
if(noCreateBtn != null )
	noCreateBtn = true;
else
	noCreateBtn = false;

Boolean noDeleteBtn = RequestUtils.getAttribute(request, "noDeleteBtn", Boolean.class, false);
if(noDeleteBtn != null )
	noDeleteBtn = true;
else
	noDeleteBtn = false;

String noLicense =  RequestUtils.getAttribute(request, "noLicense", String.class, false);
String servletPath = request.getServletPath();
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold"><%=title %></span></div>
</div>
<div class="tableContent">
<form name="mainForm" id="mainForm" method="post">
<div id="status" class="status-<%=statusType %>"><%=statusMessage %></div>
<table>
	<tr>
		<td class="label">
			<%=name%>&nbsp;
		    <input type="hidden" name="<%=codeName%>Id"<%Object itemId = simple.servlet.RequestUtils.getAttribute(request, codeName + "Id", false); if(itemId != null) {%> value="<%=StringUtils.encodeForHTMLAttribute(itemId)%>"<%} %>/>
			<select id="mainSelect" onchange="handleChange(this);">
	     		<jsp:include page="itemOptions.jsp" flush="true" />	
			</select>
			<%if (!"/account.i".equalsIgnoreCase(servletPath)) {%>
			<a href="javascript:getData('type=master_<%=codeName%>&defaultValue=-9&defaultName=Choose <%=name%>&selected=' + itemIdInput.value);"><img src="/images/list.png" title="List" class="list" /></a>
			<%}%>
			&nbsp;
			<input name="saveBtn" id="saveBtn" type="button" value="Save" onclick="saveItem()" />
			<input name="cancelBtn" id="cancelBtn" type="button" value="Cancel" onclick="cancelItem()" />
		     <% if (!noCreateBtn){ %>
		    <input name="createBtn" id="createBtn" type="button" value="Create" onclick="createItem()"/>
		     <%} %>
		     <% if (!noDeleteBtn){ %>
			<input name="deleteBtn" id="deleteBtn" type="button" value="Delete" onclick="deleteItem()" />
		     <%} %>
       </td>
	</tr>
</table>
<div id="detail"><jsp:include page="<%=itemDetailJsp%>" flush="true" /></div>
</form>
</div>
<div class="spacer10"></div>
</div>
<script type="text/javascript">
var createBtn = $("createBtn");
var deleteBtn = $("deleteBtn");
var saveBtn = $("saveBtn");
var cancelBtn = $("cancelBtn");
var mainForm = $("mainForm");
var mainSelect = $("mainSelect");
var detail = $("detail");
var statusBox = $("status");
var itemIdInput = $(mainForm.<%=codeName %>Id);
var mainBtns = [createBtn, deleteBtn, saveBtn, cancelBtn];

function deleteItem() {
	if(itemIdInput.value == -9) {
		handleChange(mainSelect);	
    } else {
    	if(!confirm("Do you really want to delete the <%=name%> '" +  mainSelect.options[mainSelect.selectedIndex].text + "'?"))
            return;
    	submit(mainForm, "<%=codeName %>Delete.i");
    }
}

function saveItem() {
    //validate form
    if(!validateForm(mainForm))
        return;
    
    var pageCode = document.getElementById("page_code");
    if (pageCode != null && pageCode.value == "LICENSE_DETAILS") {
	    for(var i = 0; i < mainForm.elements.length; i++) {
			var elem = mainForm.elements[i];
			var elemName = elem.name;
			if (elemName == null)
				elemName = elem.id;
			if(elemName != null && (elemName.indexOf("serviceFee") > -1 || elemName.indexOf("processFee") > -1)
				&& (document.getElementById("lbn_" + elemName) != null && elem.value.trim() > 0 && elem.value.trim() <= parseFloat(document.getElementById("lbn_" + elemName).value)
					|| document.getElementById("ubn_" + elemName) != null && elem.value.trim() >= parseFloat(document.getElementById("ubn_" + elemName).value))
					&& !confirm("Please confirm " + elem.value.trim() + " " + elem.getAttribute("label")))
				return false;
		}
    }
    if(pageCode != null && pageCode.value == "EDIT_BANK_ACCOUNT"){
    	if($("useTaxIdForAll").checked){
    		if(!confirm("Are you sure you want to use this tax id for all other existing banks?")){
    		   $("useTaxIdForAll").removeAttribute("checked");
    		}else{
    			submit(mainForm, "<%=codeName %>" + (itemIdInput.value == -9 ? "Insert" : "Update") + ".i");
    		}
    	}else{
    		submit(mainForm, "<%=codeName %>" + (itemIdInput.value == -9 ? "Insert" : "Update") + ".i");
    	}
    }else{
    	submit(mainForm, "<%=codeName %>" + (itemIdInput.value == -9 ? "Insert" : "Update") + ".i");
    }
}

function createItem() {
	mainSelect.value = -9;
	handleChange(mainSelect);
	disable(createBtn);
	//enable all form controls
	enableForm(mainForm, mainBtns);
    disableSelect();
}

function disableSelect() {
	disable(mainSelect);
	updateStatus("Editing in process. Click 'Cancel' to select a different <%=name%>.", "info");
}
function cancelItem() {
    handleChange(mainSelect);
}

function handleChange(select) {
	updateStatus("<img src='/images/spinner.gif' style='vertical-align:middle;' /> Please wait, loading data...", "info");
	itemIdInput.value = select.value;
	updateButtonState(select);
    new Ajax("<%=codeName %>.i?getDetail=1<%if(noLicense!=null) out.write("&noLicense=1");%>", {method: mainForm.method, async: false, data: "<%=codeName %>Id=" + select.value, update: detail, evalScripts: true }).request();
    var formLogon = document.getElementById("formLogon");
    if (formLogon != null)
    	window.location = "/login.i";
    updateDetailState(select);
    updateStatus("", "info");
}

function updateButtonState(select) {
	if(select.value == -9) {
        disable(saveBtn);
        disable(cancelBtn);
        if(createBtn!=null) enable(createBtn);
        disable(deleteBtn);
    } else {
        disable(saveBtn);
        disable(cancelBtn);
        if(createBtn!=null) enable(createBtn);
        enable(deleteBtn);
    }
}
function updateDetailState(select) {
	enable(select);
    if(select.value == -9) {
    	disableForm(mainForm, mainBtns);
    } else {
    	enableForm(mainForm, mainBtns);
    }
}
function onDetailChange() {
    enable(saveBtn);
    enable(cancelBtn);
    if(createBtn!=null) disable(createBtn);
    disable(deleteBtn);
    disableSelect();
}
function updateStatus(msg, type) {
	if(!msg || trim(msg).length == 0)
		statusBox.setHTML("&nbsp;");
	else 
		statusBox.setHTML(msg);
	statusBox.className = "status-" + type;
}
updateButtonState(mainSelect);
updateDetailState(mainSelect);
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />