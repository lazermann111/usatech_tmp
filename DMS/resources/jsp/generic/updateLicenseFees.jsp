<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<script type="text/javascript">
<%	Results processFees = RequestUtils.getAttribute(request, "processFees", Results.class, true);
	while(processFees.next()) {
		Number ttId = processFees.getValue("TRANS_TYPE_ID", Number.class);
		double percent = ConvertUtils.getDouble(processFees.get("PERCENT"), 0);
		double parentPercent = ConvertUtils.getDouble(processFees.get("PARENT_LICENSE_PERCENT"), 0);
		double amount = ConvertUtils.getDouble(processFees.get("AMOUNT"), 0);
		double parentAmount = ConvertUtils.getDouble(processFees.get("PARENT_LICENSE_AMOUNT"), 0); %>
  $('processFee_percent_'<%=ttId%>).setProperty('value', <%=percent%>);
  $('processFee_parent_percent_'<%=ttId%>).setProperty('value', <%=parentPercent%>);
  $('processFee_amount_'<%=ttId%>).setProperty('value', <%=amount%>);
  $('processFee_parent_amount_'<%=ttId%>).setProperty('value', <%=parentAmount%>);
<% 	}
	Results serviceFees = RequestUtils.getAttribute(request, "serviceFees", Results.class, true);
	while(serviceFees.next()) {
	int feeId = serviceFees.getValue("FEE_ID", Integer.class);
	if(feeId == 7)
		continue;
	double amount = ConvertUtils.getDouble(serviceFees.get("AMOUNT"), 0);
	double parentAmount = ConvertUtils.getDouble(serviceFees.get("PARENT_LICENSE_AMOUNT"), 0); %>
  $('serviceFee_amount_'<%=feeId%>).setProperty('value', <%=amount%>);
  $('serviceFee_parent_amount_'<%=feeId%>).setProperty('value', <%=parentAmount%>);
<% } %>
calcCommissions();
</script>
