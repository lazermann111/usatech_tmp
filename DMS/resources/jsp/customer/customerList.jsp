<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />



<%	

    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
%>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">DMS Customer List</div>
</div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td width="40%">
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Name</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td width="20%">
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">City</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td width="20%">
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Country</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td width="20%">
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Type</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
		</tr>
	</thead>
	<tbody>
	<% 
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0){
	    %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<%} else {
	Results customerList = (Results) request.getAttribute("customerList");
	int i = 0; 
	String rowClass = "row0";
	while(customerList.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++;
	%>
		<tr class="<%=rowClass%>">
		    <td><a href="editCustomer.i?customer_id=<%=customerList.getFormattedValue(1)%>"><%=customerList.getFormattedValue(2)%></a>
		    </td>
		    <td><%=customerList.getFormattedValue(3)%></td>
		    <td><%=customerList.getFormattedValue(4)%></td>
		    <td><%=customerList.getFormattedValue(5)%></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{"customer_name","customer_type"});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="customerList.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

<% } %>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />