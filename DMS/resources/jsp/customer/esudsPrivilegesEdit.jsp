<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:useBean id="user_count" class="java.lang.String" scope="request" />
<jsp:useBean id="userAdminReportsSection" class="java.lang.String" scope="request" />

<jsp:useBean id="userMaintPermissions" class="java.lang.String" scope="request" />

<jsp:useBean id="appUserInfo" type="simple.results.Results" scope="request" />
<jsp:useBean id="userOpAccessSchoolPermissions" class="java.lang.String" scope="request" />
<jsp:useBean id="userOpAccessSchoolLocationsList" type="simple.results.Results" scope="request" />
<jsp:useBean id="userOpAccessReportsCustList" type="simple.results.Results" scope="request" />

<jsp:useBean id="userOpAccessCampusPermissions" class="java.lang.String" scope="request" />

<jsp:useBean id="userOpAccessCampusCustList" type="simple.results.Results" scope="request" />
<jsp:useBean id="userOpAccessReportsPermissions" class="java.lang.String" scope="request" />
<jsp:useBean id="userCustList" type="simple.results.Results" scope="request" />
<jsp:useBean id="app_user_id" class="java.lang.Object" scope="request" />
<jsp:useBean id="loc_bg" class="java.lang.String" scope="request" />
<jsp:useBean id="myaction" class="java.lang.String" scope="request" />

<jsp:useBean id="editEsudsPrivs" class="java.lang.String" scope="request" />

<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/ajax.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/esudsPrivileges.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
		
		<!-- App User Info Table for edit screen -->
		
		<div align="CENTER" >
			<table class="tabDataDisplayBorderNoFixedLayout" width="60%" align=center >
				<tr>
				<td  class=leftHeader>App User First Name</td>
				<td  >${appUserInfo[0].app_user_fname}</td>
				
				</tr>
				<tr>
				<td  class=leftHeader>App User Last Name</td>
				<td  >${appUserInfo[0].app_user_lname}</td>
				</tr>
				<tr>
				<td  class=leftHeader>App User Primary Email</td>
				<td  >${appUserInfo[0].app_user_email_addr}</td>
				</tr>
				<tr>
				<td  class=leftHeader>Is App User Active</td>
				<c:choose>
					<c:when test="${appUserInfo[0].app_user_active_yn_flag eq 'Y' }"><td  >Yes</td></c:when>
					<c:otherwise><td  >No</td></c:otherwise>
				</c:choose>				
				</tr>
				</table>
		</div>
		<br/><br/>
		
		 	<input type="hidden" name="app_user_id" value="${app_user_id}" />
		 	<table class="tabDataDisplayBorderNoFixedLayout" width="100%" align=center >
				<tr>
				<td  align=center colspan=6><input type="submit" class="cssButton" tabindex="27" name="myaction" value="Submit" onclick="return confirmSubmit2()" /></td>
				</tr>
				
				<tr>
					<td  colspan=6 align=center>&nbsp</td>
				</tr>

				<tr>
				<td  colspan=6 align=center class=header0><B><FONT size=3>USER ADMIN REPORTS</FONT></B></td>
				</tr>
				<tr>
				<td  class=leftHeader>Customer</td>
				
				<td  class=leftHeader>Location ( School )</td>
				<td  class=leftHeader>Location ( Campus )</td>
				<td  class=leftHeader>Read</td>
				<td  class=leftHeader>Write</td>
				<td  class=leftHeader>Delete?</td>
				</tr>
		 		
		 		<c:choose>
		 			<c:when test="${not empty userAdminReportsSection and userAdminReportsSection ne null and userAdminReportsSection ne ''}">
		 				
		 				<jsp:useBean id="userAdminPrivsList" class="java.lang.String" scope="request" />
						
						<jsp:useBean id="userAdminOperatorCheckBoxes" type="java.lang.String[]" scope="request" />						
						
						<!-- display privs section -->
						
						<c:out value="${userAdminReportsSection}" escapeXml="false" />
					</c:when>
		 		</c:choose>
		 				<!-- Insert drop-down code for customer and location here -->
		 				
		 				<tr>
							<td  bgcolor="${loc_bg }" align=center colspan=3><B>Operator: </B>
								
								<select name="user_admin_reports__customer_id" tabindex="28" onchange="display_message1(); get_location1('${userAdminPrivsList}'); confirmCustomer('user_admin_reports__');" align="yes" 
								style="font-family: courier; font-size: 12px;" 
								id="user_admin_reports__customer_id_div" delimiter=".">
									<option selected="selected" value="1">Undefined</option>
									<c:forEach var="item" items="${userCustList}">

										<option value="${item.aValue}">${item.aLabel}</option>
											    	
								    </c:forEach>	   
							    </select>
							
							
								<div id="location_div1">
									<B>
										<FONT color="green" size=2><br/>Add a school: Select an operator and new location</FONT>
									</B>
								</div>
								
							</td>
							<c:choose>
								<c:when test="${userAdminOperatorCheckBoxes[0] ne ''}">
									<td  bgcolor="${loc_bg }"><label><input type="checkbox" name="${userAdminOperatorCheckBoxes[0]}" value="Y" onclick="return confirmSubmit1('user_admin_reports');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${userAdminOperatorCheckBoxes[1] ne ''}">
									<td  bgcolor="${loc_bg }"><label><input type="checkbox" name="${userAdminOperatorCheckBoxes[1]}" value="Y"  onclick="return confirmSubmit1('user_admin_reports');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${userAdminOperatorCheckBoxes[2] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${userAdminOperatorCheckBoxes[2]}" value="Y"  onclick="return confirmSubmit1('user_admin_reports');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
						</tr>
		 				
		 			
		 	
		 		<tr>
					<td  colspan=6 align=center>&nbsp</td>
				</tr>
				
				<tr>
				<td  colspan=6 align=center class=header0><B><FONT size=3>USER MAINTENANCE</FONT></B></td>
				</tr>
				<tr>
				<td  class=leftHeader>Customer</td>
				<td  class=leftHeader>Location ( School )</td>
				<td  class=leftHeader>Location ( Campus )</td>
				<td  class=leftHeader>Read</td>
				<td  class=leftHeader>Write</td>
				<td  class=leftHeader>Delete?</td>
				
				</tr>
				
				<c:choose>
		 			<c:when test="${not empty userMaintReportsSection and userMaintReportsSection ne null and userMaintReportsSection ne ''}">
		 				<jsp:useBean id="userMaintPrivsList" class="java.lang.String" scope="request" />
						
						<jsp:useBean id="userMaintOperatorCheckBoxes" type="java.lang.String[]" scope="request" />
		 				
		 				<!-- display privs section -->
						
						<c:out value="${userMaintReportsSection}" escapeXml="false" />
					</c:when>
		 		</c:choose>
		 				<!-- Insert drop-down code for customer and location here -->	
		 				
		 				<tr>
							<td  bgcolor="${loc_bg }" align=center colspan=3><B>Operator: </B>
								
								<select name="user_maintenance__customer_id" tabindex="29" 
								onchange="display_message2(); get_location2('${userMaintPrivsList }'); confirmCustomer('user_maintenance__');" 
								align="yes" style="font-family: courier; font-size: 12px;" id="user_maintenance__customer_id_div" 
								delimiter=".">
									<option selected="selected" value="1">Undefined</option>
									<c:forEach var="item" items="${userCustList}">

										<option value="${item.aValue}">${item.aLabel}</option>
											    	
								    </c:forEach>	   
							    </select>
							
							
								<div id=location_div2><B><FONT color="green" size=2><br/>Add a school or campus: Select an operator and new location</FONT></B></div>
							</td>

							<c:choose>
								<c:when test="${userMaintOperatorCheckBoxes[0] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${userMaintOperatorCheckBoxes[0]}" value="Y" onclick="return confirmSubmit1('user_maintenance');" /> </label></td>
								</c:when>
								<c:otherwise>
									<tdbgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${userMaintOperatorCheckBoxes[1] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${userMaintOperatorCheckBoxes[1]}" value="Y"  onclick="return confirmSubmit1('user_maintenance');" /> </label></td>
								</c:when>
								<c:otherwise>
									<tdbgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${userMaintOperatorCheckBoxes[2] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${userMaintOperatorCheckBoxes[2]}" value="Y"  onclick="return confirmSubmit1('user_maintenance');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
						</tr>

		 				
		 			
		 		
		 		<tr>
					<td  colspan=6 align=center>&nbsp</td>
				</tr>
				
				<tr>
				<td  COlspan=6 align=center class=header0><B><FONT size=3>OPERATOR ACCESS SCHOOL</FONT></B></td>
				</tr>
				<tr>
				<td  class=leftHeader>Customer</td>
				<td  class=leftHeader>Location ( School )</td>
				
				<td  class=leftHeader>Location ( Campus )</td>
				<td  class=leftHeader>Read</td>
				<td  class=leftHeader>Write</td>
				<td  class=leftHeader>Delete?</td>
				</tr>
		 		
		 		<c:choose>
		 			<c:when test="${not empty userOpAccessSchoolSection and userOpAccessSchoolSection ne null and userOpAccessSchoolSection ne ''}">
		 				<jsp:useBean id="userOpAccessSchoolPrivsList" class="java.lang.String" scope="request" />
						
						<jsp:useBean id="opAccessSchoolOperatorCheckBoxes" type="java.lang.String[]" scope="request" />
		 				
		 				<!-- display privs section -->
						
						<c:out value="${userOpAccessSchoolSection}" escapeXml="false" />
						
		 			</c:when>
		 		</c:choose>
		 				<!-- Insert drop-down code for customer/location here -->
		 				
		 				<tr>
							<td  bgcolor="${loc_bg }" align=center colspan=3>
								
								<select name="operator_access_school__location_id" 
								
								align="yes" style="font-family: courier; font-size: 12px;" id="operator_access_school__location_id_div" 
								delimiter=".">
									<option selected="selected" value="1">Undefined</option>
									
								    <c:forEach var="item" items="${userOpAccessSchoolLocationsList}">

										<option value="${item.aValue}">${item.aLabel}</option>
											    	
								    </c:forEach>	   
							    </select>
							
							</td>

							<c:choose>
								<c:when test="${opAccessSchoolOperatorCheckBoxes[0] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${opAccessSchoolOperatorCheckBoxes[0]}" value="Y" onclick="return confirmSubmit1('operator_access_school');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${opAccessSchoolOperatorCheckBoxes[1] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${opAccessSchoolOperatorCheckBoxes[1]}" value="Y"  onclick="return confirmSubmit1('operator_access_school');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${opAccessSchoolOperatorCheckBoxes[2] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${opAccessSchoolOperatorCheckBoxes[2]}" value="Y"  onclick="return confirmSubmit1('operator_access_school');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
						</tr>
		 				
		 			
		 		
		 		<tr>
					<td  colspan=6 align=center>&nbsp</td>
				</tr>
				
				<tr>
				<td  COlspan=6 align=center class=header0><B><FONT size=3>OPERATOR ACCESS CAMPUS</FONT></B></td>
				</tr>
				
				<tr>
				<td  class=leftHeader>Customer</td>
				<td  class=leftHeader>Location ( School )</td>
				<td  class=leftHeader>Location ( Campus )</td>
				<td  class=leftHeader>Read</td>
				<td  class=leftHeader>Write</td>
				<td  class=leftHeader>Delete?</td>
				</tr>
		 		
		 		<c:choose>
		 			<c:when test="${not empty userOpAccessCampusSection and userOpAccessCampusSection ne null and userOpAccessCampusSection ne ''}">
		 				<jsp:useBean id="userOpAccessCampusPrivsList" class="java.lang.String" scope="request" />
						
						<jsp:useBean id="opAccessCampusOperatorCheckBoxes" type="java.lang.String[]" scope="request" />
		 				
		 				<!-- display privs section -->
						
						<c:out value="${userOpAccessCampusSection}" escapeXml="false" />
		 				
		 			</c:when>
		 		</c:choose>
		 				<!-- Insert drop-down code for customer/location here -->
		 				
		 				<tr>
							<td  bgcolor="${loc_bg }" align=center colspan=3><B>Operator: </B>
								
								<select name="operator_access_campus__customer_id" 
								onchange="display_message3(); get_location3('${userOpAccessCampusPrivsList }'); confirmCustomer('operator_access_campus__');" 
								align="yes" style="font-family: courier; font-size: 12px;" id="operator_access_campus__customer_id_div" 
								delimiter=".">
									<option selected="selected" value="1">Undefined</option>
									<c:forEach var="item" items="${userOpAccessCampusCustList}">

										<option value="${item.aValue}">${item.aLabel}</option>
											    	
								    </c:forEach>	   
							    </select>
							
							
								<div id=location_div3><B><FONT color="green" size=2><br/>Add a campus: Select an operator and new location</FONT></B></div>
							</td>

							<c:choose>
								<c:when test="${opAccessCampusOperatorCheckBoxes[0] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${opAccessCampusOperatorCheckBoxes[0]}" value="Y" onclick="return confirmSubmit1('operator_access_campus');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${opAccessCampusOperatorCheckBoxes[1] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${opAccessCampusOperatorCheckBoxes[1]}" value="Y"  onclick="return confirmSubmit1('operator_access_campus');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${opAccessCampusOperatorCheckBoxes[2] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${opAccessCampusOperatorCheckBoxes[2]}" value="Y"  onclick="return confirmSubmit1('operator_access_campus');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
						</tr>
		 				
		 				
		 				
		 			
		 		
		 		<tr>
					<td  colspan=6 align=center>&nbsp</td>
				</tr>
				
				<tr>
				<td  colspan=6 align=center class=header0><B><FONT size=3>OPERATOR REPORTS</FONT></B></td>
				</tr>
				<tr>
				<td  class=leftHeader>Customer</td>
				<td  class=leftHeader>Location ( School )</td>
				<td  class=leftHeader>Location ( Campus )</td>
				<td  class=leftHeader>Read</td>
				
				<td  class=leftHeader>Write</td>
				<td  class=leftHeader>Delete?</td>
				</tr>
		 		
		 		<c:choose>
		 			<c:when test="${not empty userOpAccessReportsSection and userOpAccessReportsSection ne null and userOpAccessReportsSection ne ''}">
		 				<jsp:useBean id="userOpAccessReportsPrivsList" class="java.lang.String" scope="request" />
						 				
		 				<jsp:useBean id="opAccessReportsOperatorCheckBoxes" type="java.lang.String[]" scope="request" />
		 				<!-- display privs section -->
						
						<c:out value="${userOpAccessReportsSection}" escapeXml="false" />
						
		 			</c:when>
		 		</c:choose>
		 				<!-- Insert drop-down code for customer/location here -->
		 				
		 				<tr>
							<td  bgcolor="${loc_bg }" align=center colspan=3><B>Operator: </B>
								
								<select name="operator_reports__customer_id" 
								onchange="display_message4(); get_location4('${userOpAccessReportsPrivsList }'); confirmCustomer('operator_reports__');" 
								align="yes" style="font-family: courier; font-size: 12px;" id="operator_reports__customer_id_div" 
								delimiter=".">
									<option selected="selected" value="1">Undefined</option>
									<c:forEach var="item" items="${userOpAccessReportsCustList}">

										<option value="${item.aValue}">${item.aLabel}</option>
											    	
								    </c:forEach>	   
							    </select>
							
								<div id=location_div4><B><FONT color="green" size=2><br/>Add a school: Select an operator and new location</FONT></B></div>
							</td>
							

							<c:choose>
								<c:when test="${opAccessReportsOperatorCheckBoxes[0] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${opAccessReportsOperatorCheckBoxes[0]}" value="Y" onclick="return confirmSubmit1('operator_reports');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${opAccessReportsOperatorCheckBoxes[1] ne ''}">
									<td bgcolor="${loc_bg }"  ><label><input type="checkbox" name="${opAccessReportsOperatorCheckBoxes[1]}" value="Y"  onclick="return confirmSubmit1('operator_reports');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${opAccessReportsOperatorCheckBoxes[2] ne ''}">
									<td bgcolor="${loc_bg }" ><label><input type="checkbox" name="${opAccessReportsOperatorCheckBoxes[2]}" value="Y"  onclick="return confirmSubmit1('operator_reports');" /> </label></td>
								</c:when>
								<c:otherwise>
									<td bgcolor="${loc_bg }">&nbsp;</td>
								</c:otherwise>
							</c:choose>
						</tr>
		 				
		 					 		
		 		<tr>
					<td  align=center colspan=6>&nbsp;</td>
				</tr>
		 		
		 		<tr>
					<td  align=center colspan=6>
						<input type="submit" class="cssButton" tabindex="27" name="myaction" value="Submit" onclick="return confirmSubmit2()" />
					</td>
				</tr>
		 		
		 		
		 	</table>
		<!-- /form-->
	</c:otherwise>

</c:choose>