<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:useBean id="user_count" class="java.lang.Object" scope="request" />

<jsp:useBean id="myaction" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_fname_search" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_lname_search" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_email_addr_search" class="java.lang.String" scope="request" />

<jsp:useBean id="popup_fname" class="java.lang.String" scope="request" />
<jsp:useBean id="popup_lname" class="java.lang.String" scope="request" />
<jsp:useBean id="popup_email" class="java.lang.String" scope="request" />

<jsp:useBean id="chkbox6" class="java.lang.String" scope="request" />
<jsp:useBean id="chkbox1" class="java.lang.String" scope="request" />
<jsp:useBean id="chkbox2" class="java.lang.String" scope="request" />
<jsp:useBean id="chkbox3" class="java.lang.String" scope="request" />
<jsp:useBean id="chkbox5" class="java.lang.String" scope="request" />

<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/ajax.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	
	<c:when test="${not empty(errorMessage)}">
		<div class="tableContainer">				    
			<span class="error">${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>
	
	<SCRIPT language="JavaScript">
	function onSubmitEdit(app_user_id)
	{
  		document.myForm.app_user_id_edit.value = app_user_id;
  		
  		return true;
	}
	</SCRIPT>
			<input type="hidden" name="app_user_id_edit" value="" />
			<c:set var="sortIndex" value="${sortIndex}"/>
			<c:set var="rowClass" value="row0"/>
			<c:set var="i" value="0"/>
			<div align="center">
			
					<!--  form name="mySearchForm" method="post" action="esudsPrivilegesEdit.i" /-->								
					<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
					
					<thead>
							<tr class="sortHeader">
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "1", (String)pageContext.getAttribute("sortIndex"))%>">App&nbsp;User&nbsp;ID</a>
									<%=PaginationUtil.getSortingIconHtml("1", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "2", (String)pageContext.getAttribute("sortIndex"))%>">First&nbsp;Name</a>
									<%=PaginationUtil.getSortingIconHtml("2", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "3", (String)pageContext.getAttribute("sortIndex"))%>">Last&nbsp;Name</a>
									<%=PaginationUtil.getSortingIconHtml("3", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "4", (String)pageContext.getAttribute("sortIndex"))%>">Email</a>
									<%=PaginationUtil.getSortingIconHtml("4", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>Edit&nbsp;Privileges</td>
								
								
							</tr>
						</thead>
						
						<c:choose>
							<c:when test="${user_count != '0'}" >
								<jsp:useBean id="appUserInfo" type="simple.results.Results" scope="request" />
								<tbody>
								<c:forEach var="user_info" items="${appUserInfo}">
									<c:choose>
										<c:when test="${i%2 == 0}">
											<c:set var="rowClass" value="row1"/>
										</c:when>
										<c:otherwise>
											<c:set var="rowClass" value="row0"/>
										</c:otherwise>
									</c:choose>
									<c:set var="i" value="${i+1}"/>
									<tr class="${rowClass}">
									    <td nowrap align="right"><a href="/editAppUser.i?app_user_id=${user_info.app_user_id }">${user_info.app_user_id }</a></td>
									    <td nowrap align="left">${user_info.app_user_fname }</td>
									    <td nowrap align="left">${user_info.app_user_lname }</td>
									    <td nowrap align="left">${user_info.app_user_email_addr }</td>
									    <td nowrap align="left"><input type="submit" tabindex="30" name="myaction" class="cssButton" value="Edit" onclick="return onSubmitEdit('${user_info.app_user_id }');" /></td>														    
									</tr>
								</c:forEach>
			
								</tbody>
								<c:set var="storedNames" value="app_user_fname_search,app_user_lname_search,app_user_email_addr_search,popup_fname,popup_lname,popup_email,chkbox6,chkbox1,chkbox3,chkbox2,chkbox5,user_count,myaction" />	
							
							    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
							        <jsp:param name="_param_request_url" value="esudsPrivileges.i" />
							    	<jsp:param name="_param_stored_names" value="${storedNames}" />
							    </jsp:include>
							   
							  
							</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="9" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>
			</table>
			
		 <!-- /form-->
		</div>
		
	</c:otherwise>

</c:choose>