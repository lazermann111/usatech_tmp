<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long customerId = inputForm.getLong("customerId", false, 0); 
Results dealers = RequestUtils.getAttribute(request, "dealers", Results.class, true);
if (customerId <= 0)
	throw new ServletException("Required parameter customerId not found");
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="smallFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Add Secondary Program</span></div>
</div>

<div class="tableContent">

<div class="spacer10"></div>

<form method="post" action="addCustomerDealer.i" onsubmit="return addDealer();">
<input type="hidden" name="customerId" id="customerId" value="<%=customerId %>" />
<table>
	<tr>
		<td>
		<span class="label">Programs</span><br/>
		<select name="dealerId" id="dealerId" size="10">	
			<%
			while(dealers.next()) {
			%><option value="<%=dealers.getFormattedValue("DEALER_ID")%>"><%=dealers.getFormattedValue("DEALER_NAME")%></option>	    	
			<%}%>
		</select>
		</td>
	</tr>
	
	<tr>
		<td>
			<input type="submit" class="cssButton" value="Add Program" />			
			<input type="button" class="cssButton" value="Cancel" onclick="window.location = '/customer.i?customerId=<%=customerId%>';" />
		</td>
	</tr>

</table>		
</form>

<div class="spacer10"></div>
</div>
</div>

<script type="text/javascript" defer="defer">
function addDealer() {
	if (document.getElementById("dealerId").selectedIndex < 0) { 
		alert("Please select a program");
		return false;
	} else 
		return true;
}
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />