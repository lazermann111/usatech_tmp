<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	int i = 0; 
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    boolean norec=false;
    
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Customer List</div>
</div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">					
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Customer Name</a>
			<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Created Date</a>
			<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Address Name</a>
			<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Address 1</a>
			<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Address 2</a>
				 <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">City</a>
			<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">State</a> 
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Zip</a> 
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">License #</a> 
				<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
			</td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Status</a>
			<%=PaginationUtil.getSortingIconHtml("10", sortIndex)%></td>
	
		</tr>
	</thead>
	<tbody>
	<% 
	if (norec){
	    %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<%} else {
	Results list = (Results) request.getAttribute("resultlist");
	while(list.next()) {
	%>
		<tr class="<%=(i++%2==0)?"row1":"row0"%>">
			<td><a href="/customer.i?customerId=<%=list.getFormattedValue("CUSTOMER_ID")%>"><%=list.getFormattedValue("CUSTOMER_NAME")%></a></td>
			<td><%=list.getFormattedValue("CREATE_DATE")%></td>
			<td><%=list.getFormattedValue("ADDRESS_NAME")%></td>
			<td><%=list.getFormattedValue("ADDRESS1")%></td>
			<td><%=list.getFormattedValue("ADDRESS2")%></td>
			<td><%=list.getFormattedValue("CITY")%></td>
			<td><%=list.getFormattedValue("STATE")%></td>
			<td><%=list.getFormattedValue("ZIP")%></td>
			<td><%=list.getFormattedValue("LICENSE_NBR")%></td>
			<td><%=list.getFormattedValue("STATUS")%></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%
    String storedNames = PaginationUtil.encodeStoredNames(new String[] {"customer_name"});
%>

    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="customersList.i" />
        <jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

<% } %>


</div>

<div class="spacer10"></div>
				
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
