<%@page import="com.usatech.layers.common.model.PayrollSchedule.PayrollScheduleType"%>
<%@page import="com.usatech.layers.common.model.PayrollSchedule"%>
<%@page import="com.usatech.dms.sales.Category"%>
<%@page import="com.usatech.dms.sales.PricingTier"%>
<%@page import="com.usatech.dms.sales.Affiliate"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>

<%
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "reporting_customer");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("customerId"), ""));

BigDecimal customerId = RequestUtils.getAttribute(request, "customerId", BigDecimal.class, false);
Results customer = RequestUtils.getAttribute(request, "customer", Results.class, false);
Results tcount = RequestUtils.getAttribute(request, "tcount", Results.class, false);
Results accounts = RequestUtils.getAttribute(request, "accounts", Results.class, false);
Results states = RequestUtils.getAttribute(request, "states", Results.class, true);
Results countries = RequestUtils.getAttribute(request, "countries", Results.class, true);
Results dealers = RequestUtils.getAttribute(request, "dealers", Results.class, false);
Results secondaryDealers = RequestUtils.getAttribute(request, "secondaryDealers", Results.class, false);
Results users = RequestUtils.getAttribute(request, "users", Results.class, false);
String nolic = RequestUtils.getAttribute(request, "noLicense", String.class, false);
Results parents = RequestUtils.getAttribute(request, "parents", Results.class, false);
Results corpFeeScheduleResult = RequestUtils.getAttribute(request, "corpFeeSchedule", Results.class, false);

if (customer!=null && !customer.next())
	customer=null;
if (tcount!=null) tcount.next();

BigDecimal dealerId;
String state, countryCd;
if(customer!=null && customer.getValue("dealerId")!=null) {
	dealerId = customer.getValue("dealerId", BigDecimal.class);
	state = customer.getFormattedValue("state");
	countryCd = customer.getFormattedValue("countryCd");
} else {
	state = "";
	countryCd = "";
	dealerId = new BigDecimal(-1);
}

long accountId = -1;
int accountCount = 0;
if (accounts!=null) {
	accounts.trackAggregate(1);
	if (accounts.next()) {
		accountId = ConvertUtils.getLong(accounts.get("ACCOUNT_ID"), -1);
		accountCount = ConvertUtils.getInt(accounts.getAggregate(1, 0, Results.Aggregate.COUNT));
	}
}

if (users!=null && !users.next())
	users = null;

BigDecimal parentCustomerId = null;
if (customer != null) {
	parentCustomerId = customer.getValue("parentCustomerId", BigDecimal.class);
}

%>

				
<table>
	<tbody>
		<tr>
			<td class="label">Created Date</td>
			<td class="control"><%if (customer != null) {%><%=customer.getFormattedValue("createDateFormatted")%><%} else {%>&nbsp;<%}%></td>
		</tr>
		<tr>
			<td class="label">Monthly Fee Schedule</td>
			<td class="control">
				<select name="feeScheduleId" id="feeScheduleId" style="width: 340px;" onchange="onDetailChange()" >
				<%if (corpFeeScheduleResult != null){ while(corpFeeScheduleResult.next()) {%>						
					<option value="<%=corpFeeScheduleResult.getFormattedValue("feeScheduleId")%>" <%if (customer != null && corpFeeScheduleResult.getFormattedValue("feeScheduleId").equals(customer.getFormattedValue("feeScheduleId"))) {%> selected="selected"<%}%>>
					<%=corpFeeScheduleResult.getFormattedValue("feeScheduleDescription")%>
					</option>
				<%}}%>
				</select>
			</td>
		</tr>
		<tr>
			<td class="label">No License Agreement</td>
			<td class="control">
				<input type="checkbox" name="nolic" id="nolic" onclick="clickNolic()"
					<%if (nolic != null || customer != null && customer.getValue("licenseNbr") == null) {%> checked="checked" <%}%>
				/>
			</td>
		</tr>
	
		<tr>
			<td class="label">Alternate Name</td>
			<td class="control">
				<input type="text" name="altName" id="altName" size="50" maxlength="50" onchange="onDetailChange()"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("altName")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Name</td>
			<td class="control">
				<input type="text" name="name" id="name" onchange="onDetailChange()" size="50" maxlength="50" usatRequired="true"  label="Name"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("name")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Address To</td>
			<td class="control">
				<input type="text" name="addressName" id="addressName" size="50" maxlength="50" onchange="onDetailChange()"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("addressName")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Address 1</td>
			<td class="control">
				<input type="text" name="address1" id="address1" size="50" maxlength="255" onchange="onDetailChange()"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("address1")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Address 2</td>
			<td class="control">
				<input type="text" name="address2" id="address2" size="50" maxlength="255" onchange="onDetailChange()"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("address2")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Postal Code</td>
			<td class="control">
				<input type="text" name="zip" id="zip" size="20" maxlength="20" onchange="onDetailChange()" onblur="getPostalInfo('postalCd=' + this.value);"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("zip")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">City</td>
			<td class="control">
				<input type="text" name="city" id="city" size="50" maxlength="50" onchange="onDetailChange()"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("city")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">State</td>
			<td class="control">
				<select name="state" id="state" style="width: 340px;" onchange="onStateChange(this)" >
					<option value="">Please select:</option>
				<%if (states != null){ while(states.next()) {%>						
					<option value="<%=states.getFormattedValue("STATE_CD")%>" data-countryCd="<%=states.getFormattedValue("COUNTRY_CD")%>"<%if (customer != null && state.equals(states.getFormattedValue("STATE_CD"))) {%> selected="selected"<%}%>>
					<%=states.getFormattedValue("STATE_NAME")%>: <%=states.getFormattedValue("STATE_CD")%>, <%=states.getFormattedValue("COUNTRY_CD")%>
					</option>
				<%}}%>
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">Country</td>
			<td class="control">
				<select name="country" id="country" style="width: 340px;" onchange="onDetailChange()" >
					<option value="US" >Please select:</option>
				<%if (countries != null){ while(countries.next()) {%>						
					<option value="<%=countries.getFormattedValue("COUNTRY_CD")%>"<%if (customer != null && countryCd.equals(countries.getFormattedValue("COUNTRY_CD"))) {%> selected="selected"<%}%>>
					<%=countries.getFormattedValue("COUNTRY_NAME")%>: <%=countries.getFormattedValue("COUNTRY_CD")%>
					</option>
				<%}}%>
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">Phone</td>
			<td class="control"> 
					<%if (users!=null ){ %>
						<%=users.getFormattedValue("telephone")%>
					<%}%>
			</td>
		</tr>	
		
		<tr>
			<td class="label">Fax</td>
			<td class="control"> 
					<%if (users!=null){ %>
						<%=users.getFormattedValue("fax")%>
					<%}%>
			</td>
		</tr>
		
		<tr>
			<td class="label">Best Vendors Customer</td>
			<td class="control">
				<input type="checkbox" name="bestVendorsCustomer" id="bestVendorsCustomer" onchange="onDetailChange()"<%if (customer != null && customer.getValue("isBestVendorsCustomer", boolean.class)) {%> checked="checked" <%}%> />
			</td>
		</tr>	
		
		<tr>
			<td class="label">Primary Program</td>
			<td class="control">
				<select name="dealerId" id="dealerId" style="width: 340px;" onchange="onDetailChange()" >
				<%if (dealers!=null){ while(dealers.next()) {
				%><option value="<%=dealers.getValue("DEALER_ID")%>"
				<% if(dealerId.compareTo(dealers.getValue("DEALER_ID", BigDecimal.class)) == 0){ %> selected="selected"<%} %>
				><%=dealers.getFormattedValue("DEALER_NAME")%></option>
				<%}}%>		
				</select>
				<input type="button" value="Show" id="btnShowPrimaryDealer" onclick="window.location = '/dealer.i?dealerId=' + mainForm.dealerId.value;"/>
			</td>
		</tr>
		
		<tr>
			<td class="label" valign="top">Secondary Programs</td>
			<td class="control">
			<select name="secondaryDealerId" id="secondaryDealerId" size="5" style="width: 340px;">
			<%
			if (secondaryDealers != null) {
			while(secondaryDealers.next()) {%>
				<option value="<%=secondaryDealers.getFormattedValue("DEALER_ID")%>"><%=secondaryDealers.getFormattedValue("DEALER_NAME")%></option>
			<%}}%>
			</select>
			<br/><input type="button" value="Add Program" name="btnAddDealer" onclick="window.location = '/showDealer.i?customerId=' + mainForm.customerId.value;"/>
			<input type="button" value="Remove Program" name="btnRemoveDealer" onclick="removeDealer();"/>
			<input type="button" value="Show" id="btnShowSecondaryDealer" onclick="window.location = '/dealer.i?dealerId=' + mainForm.secondaryDealerId.value;"/>
			</td>
		</tr>
		
		<%if (customer!=null && customer.getValue("licenseNbr")==null ){ %>
		<tr>
			<td>&nbsp;</td>
			<td>We have not received a signed License Agreement</td>
		</tr>
		<%}%>
		
		<tr>
			<td class="label">License Number</td>
			<td class="control"> 
				<%if (customer!=null && customer.getValue("licenseNbr")!=null ){ %>
					<%=customer.getFormattedValue("licenseNbr")%>
                <input name="showLicense" id="showLicense" type="button" value="Show" onclick="window.location = '/license.i?licenseId=<%=customer.getFormattedValue("licenseId")%>';"/>
				<%}%>
                <input name="chooseLicense" id="chooseLicense" type="button" value="Choose License" onclick="window.location = '/showLicense.i?customerId=' + mainForm.customerId.value;"/> 
			</td>
		</tr>
		
		<tr>
			<td class="label" valign="top">Users</td>
			<td class="control">
<select name="users" id="users" size="5" style="width: 340px;">
<%
if (users!=null){ 
	do{
	BigDecimal ldid = users.getValue("id", BigDecimal.class);
	%>
	<option value="<%=ldid%>">
	<%=users.getFormattedValue("firstName")%>&nbsp;
	<%=users.getFormattedValue("lastName")%>&nbsp;
	(<%=users.getFormattedValue("name")%>)&nbsp;
	<%if ("Y".equals(users.getFormattedValue("custuserFlag"))) out.write("Primary");%>
	</option>	    	
	<%
	}while(users.next());
}
%>
</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">Doing Business As</td>
			<td class="control">
				<input type="text" name="doingBusinessAs" id="doingBusinessAs" size="50" maxlength="21" onchange="onDetailChange()"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("doingBusinessAs")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label" valign="top">Parent Customer</td>
			<td class="control">
				<select name="parentCustomerId" id="parentCustomerId" style="width: 340px;" onchange="onDetailChange()">
				<option value="0">Please select:</option>
				<% if (parents != null) {
				while(parents.next()) {%>
					<option value="<%=parents.getFormattedValue("CUSTOMER_ID")%>" <%if (parentCustomerId != null && parentCustomerId.compareTo(parents.getValue("CUSTOMER_ID", BigDecimal.class)) == 0) {%> selected="selected"<%}%>><%=parents.getFormattedValue("CUSTOMER_NAME")%></option>
				<%}}%>
			</select>
			<input type="button" value="Show" id="btnShowParentCustomer" onclick="window.location = '/customer.i?customerId=' + mainForm.parentCustomerId.value;"/>
			</td>
		</tr>
		<tr>
			<td class="label">Crane Customer Id</td>
			<td class="control">
				<input type="text" label="Crane Customer Id" name="craneCustomerId" id="craneCustomerId" size="20" maxlength="100" onchange="this.value=this.value.trim();onDetailChange()"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("craneCustomerId")%>"
					<%}%>
				/>
			</td>
		</tr>
		<tr>
			<td class="label">Sprout Vendor Id</td>
			<td class="control">
				<input type="text" label="Sprout Vendor Id" name="sproutVendorId" id="sproutVendorId" size="20" maxlength="20" valid="^[0-9]*$" onchange="onDetailChange()"
					<%if (customer!=null){ %>
						value="<%=customer.getFormattedValue("sproutVendorId")%>"
					<%}%>
				/>
			</td>
		</tr>
		<tr>
			<td class="label" valign="top">Risk Monitoring<br/>Factors</td>
			<td class="control">
				<table>
					<tr>
						<td align="left">Avg Tran Amt</td>
						<td rowspan="2">&nbsp;</td>
						<td align="left">Max Tran Amt</td>
						<td rowspan="2">&nbsp;</td>
						<td align="left">Avg Daily Trans</td>
					</tr>
					<tr>
						<td align="left"><input type="text" name="avgTransAmt" id="avgTransAmt" size="5" maxlength="15" onchange="onDetailChange()" <%if (customer!=null) { %>value="<%=customer.getFormattedValue("avgTransAmt")%>"<%}%>/></td>
						<td align="left"><input type="text" name="maxTransAmt" id="maxTransAmt" size="5" maxlength="15" onchange="onDetailChange()" <%if (customer!=null) { %>value="<%=customer.getFormattedValue("maxTransAmt")%>"<%}%>/></td>
						<td align="left"><input type="text" name="avgTransCnt" id="avgTransCnt" size="5" maxlength="15" onchange="onDetailChange()" <%if (customer!=null) { %>value="<%=customer.getFormattedValue("avgTransCnt")%>"<%}%>/></td>
				</table>
			</td>
		</tr>
<% List<Affiliate> affiliates = Affiliate.findAllActive();
	Long affiliateId = null;
	if (customer != null)
		affiliateId = customer.getValue("affiliateId", Long.class); %>
		<tr>
			<td class="label">Sales Affiliate</td>
			<td class="control">
				<select name="affiliateId" id="affiliateId" style="width: 340px;" onchange="onDetailChange()" >
					<option value="">Please select:</option>
				<% for(Affiliate affiliate : affiliates) {%>
					<option value="<%=affiliate.getAffiliateId()%>" <% if (affiliateId != null && affiliateId.equals(affiliate.getAffiliateId())) {%> selected="selected"<%}%>><%=affiliate.getName()%></option>
				<% } %>
				</select>
			</td>
		</tr>
<% List<PricingTier> pricingTiers = PricingTier.findAllActive();
	Long pricingTierId = null;
	if (customer != null)
		pricingTierId = customer.getValue("pricingTierId", Long.class); %>
		<tr>
			<td class="label">Pricing Tier</td>
			<td class="control">
				<select name="pricingTierId" id="pricingTierId" style="width: 340px;" onchange="onDetailChange()" >
					<option value="">Please select:</option>
				<% for(PricingTier pricingTier : pricingTiers) {%>
					<option value="<%=pricingTier.getPricingTierId()%>" <% if (pricingTierId != null && pricingTierId.equals(pricingTier.getPricingTierId())) {%> selected="selected"<%}%>><%=pricingTier.getName()%></option>
				<% } %>
				</select>
			</td>
		</tr>
<% List<Category> categories = Category.findAllActive();
	Long categoryId = null;
	if (customer != null)
		categoryId = customer.getValue("categoryId", Long.class); %>
		<tr>
			<td class="label">Sales Category</td>
			<td class="control">
				<select name="categoryId" id="categoryId" style="width: 340px;" onchange="onDetailChange()" >
					<option value="">Please select:</option>
				<% for(Category category : categories) {%>
					<option value="<%=category.getCategoryId()%>" <% if (categoryId != null && categoryId.equals(category.getCategoryId())) {%> selected="selected"<%}%>><%=category.getName()%></option>
				<% } %>
				</select>
			</td>
		</tr>
<% PayrollSchedule payrollSchedule = PayrollSchedule.loadFrom(customer);
	if (payrollSchedule != null && payrollSchedule.getPayrollScheduleType() != PayrollScheduleType.NONE) { %>
		<tr>
			<td class="label">Payroll Deduct Schedule</td>
			<td class="control"><%=payrollSchedule.getPayrollScheduleType().getDesc() %>: <%=payrollSchedule.getPayrollScheduleData()%></td>
		</tr>
		<tr>
			<td class="label">Last Top Up</td>
			<td class="control"><%=customer.getFormattedValue("lastTopUp") %></td>
		</tr>
		<tr>
			<td class="label">Next Top Up</td>
			<td class="control"><%=payrollSchedule.getNextPayrollDate() %></td>
		</tr>
<% } %>
		<tr>
			<td class="label">Allow MORE Card Negative Balance<br/>only for Offline Transactions</td>
			<td class="control">
				<input type="checkbox" name="allowNegMoreBalOfflineOnly" id="allowNegMoreBalOfflineOnly" onchange="onDetailChange()"<%if (customer != null && customer.getValue("allowNegBalOfflineOnlyInd")!=null&& customer.getValue("allowNegBalOfflineOnlyInd",char.class)=='Y') {%> checked="checked" <%}%> />
			</td>
		</tr>
				
		<tr>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<% if (customerId != null) { %>
			Customer ID(<%=customerId%>)
			<% } %> &nbsp;
			<% if (customerId != null && accountId > 0) { %>
				<a href="account.i?accountId=<%=accountId %>&amp;customerId=<%=customerId %>">Bank Accounts(<%=accountCount%>)</a>
			<%}else{ %>
				Bank Accounts(0)
			<%} %> &nbsp;
			<% if (customerId != null && ConvertUtils.getInt(tcount.get("tcount"), -1) > 0) { %>
				<a href="terminalsList.i?customerId=<%= customerId %>">Terminals(<%=tcount.getFormattedValue("tcount")%>)</a>
			<%}else{ %>
				Terminals(0)
			<%} %> &nbsp;
			</td>
		</tr>
	
</tbody>
</table>		
	
<script type="text/javascript">

function clickNolic(){
	<%if (nolic==null || nolic.equals("")) {%>
		window.location="customer.i?noLicense=1"; 
	<%}else{%>
		window.location="customer.i"; 
	<%}%>

}

function removeDealer() {
	var selectedIndex = document.getElementById("secondaryDealerId").selectedIndex;
	if (selectedIndex < 0)
		alert("Please select a secondary program");
	else if (confirm("Are you sure you want to remove dealer " + document.getElementById("secondaryDealerId").options[selectedIndex].text + "?"))
		window.location = "customer.i?customerId=<%=customerId%>&action=removeDealer&dealerId=" + document.getElementById("secondaryDealerId").options[selectedIndex].value;
}

function onStateChange(select) {
	onDetailChange();
	var countryCd;
	if(select.selectedIndex < 0)
		countryCd = null;
	else
		countryCd = select.options[select.selectedIndex].getAttribute("data-countryCd");
	if(countryCd) {
		var country = select.form.country;
		country.disabled = true;
		for(var i = 0; i < country.options.length; i++)
			country.options[i].selected = (country.options[i].value == countryCd);
	} else
		select.form.country.disabled = false;
}
</script>

		

