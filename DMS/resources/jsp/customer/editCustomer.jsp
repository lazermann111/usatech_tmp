<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm,simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.layers.common.model.Customer"%>
<%@page import="java.util.LinkedList"%>
<%@page import="simple.text.StringUtils"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String errorMessage = inputForm.getString("errorMessage", false);
    if (errorMessage != null && errorMessage.length() > 0)
    {
%>
<div class="tableContainer">
<div align="center"><span class="error"><%=errorMessage%></span></div>
</div>
<%
    }
    else
    {
    	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "customer");
    	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.PARAM_CUSTOMER_ID), ""));
        Customer customer = (Customer)inputForm.getAttribute("customer");
        String action_type = inputForm.getString("action_type",false);
        int count = inputForm.getInt("device_count",false,0);
        Results customer_types = (Results)inputForm.getAttribute("customer_types");
%>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt"><%=action_type %> DMS Customer</div>
</div>
<div class="innerTable">
<div class="tabHead" align="center"><%=customer.getName() %> ( <%=count %> Devices )</div>
<form method="post">
<input type="hidden" name="old_name" value="<%=customer.getName() %>">
<table class="tabDataDisplayBorder">
	<tbody>
		<tr>
			<td class="label" width="18%">Name</td>
			<td class="data" width="40%"><input name="customer_name"
				value="<%=StringUtils.prepareCDATA(customer.getName()) %>" size="30"
				<%=(action_type.equalsIgnoreCase("New")?"readonly='readonly'":"") %>>
			</td>
			<td class="label" width="12%">Type</td>
			<td class="data" width="30%"><select name="customer_type_id" id="customer_type_id">
				<option value="">Undefined</option>
				<%while (customer_types.next()) {%>
				<option value="<%=customer_types.getFormattedValue(1) %>"
					<%=(customer_types.getValue("customer_type_id",Integer.class).intValue() == customer.getTypeId().intValue())?" selected":"" %>><%=customer_types.getFormattedValue(2) %></option>
				<%} %>
			</select></td>
		</tr>
		<tr>
			<td class="label">Address Line 1</td>
			<td class="data"><input name="customer_addr1"
				value='<%=StringUtils.prepareCDATA(customer.getAddress1()) %>'
				size="30"></td>
			<td class="label">Address Line 2</td>
			<td class="data"><input name="customer_addr2"
				value='<%=StringUtils.prepareCDATA(customer.getAddress2()) %>'
				size="30">
			</td>
		</tr>
		<tr>
			<td class="label">Postal Code</td>
			<td class="data"><input name="customer_postal_cd"
				value='<%=customer.getPostalCode()!=null?customer.getPostalCode():"" %>'
				size="30" onblur="getPostalInfo('postalCd=' + this.value);" /></td>
			<td class="label">City</td>
			<td class="data"><input name="customer_city" id="city"
				value='<%=StringUtils.prepareCDATA(customer.getCity()) %>'
				size="30"></td>
		</tr>
		<tr>
			<td class="label">State</td>
			<td class="data">
			<div id="statecd" style="display: inline"><select
				name="customer_state_cd" tabindex="1" id="state">
				<option value="">Undefined</option>
				<% Results state_cds = (Results)inputForm.getAttribute("state_cds");
			      while(state_cds.next()){
			    %>
				<option value="<%=state_cds.getFormattedValue(1) %>"
					<%=(customer.getStateCode()!=null && customer.getStateCode().equalsIgnoreCase(state_cds.getFormattedValue(1)))?" selected":"" %>><%=state_cds.getFormattedValue(2) %></option>
				<%} %>
			</select></div>
			<input type="button" class="cssButton" value="New State"
				onClick="javascript:window.location = 'newState.i?action=New'"></td>
			<td class="label">County</td>
			<td class="data"><input name="customer_county"
				value='<%=StringUtils.prepareCDATA(customer.getCountry()) %>'
				size="30"></td>			
		</tr>
		<tr>
			<td class="label">Country</td>
			<td class="data" <%if (action_type.equalsIgnoreCase("New")) {%>
				colspan="3" <%} %>><select id="country"
				name="customer_country_cd">
				<option value="">Undefined</option>
				<%LinkedList<String[]> country_cds = (LinkedList<String[]>)inputForm.getAttribute("country_list");
		      for (String[] country_cd : country_cds){ %>
				<option value="<%=country_cd[0] %>"
					<%=(customer.getCountryCode()!=null && customer.getCountryCode().equalsIgnoreCase(country_cd[0]))?" selected":"" %>><%=country_cd[1] %></option>
				<%} %>
			</select></td>
			<%if (!action_type.equalsIgnoreCase("New")){ %>
			<td class="label">Deleted</td>
			<td class="data"><%=customer.getActiveFlag().equalsIgnoreCase("Y") ? "No" : "Yes" %>
			</td>
			<%} %>
		</tr>
		<%if (action_type.equalsIgnoreCase("New")){ %>
	</tbody>
</table>
<div class="spacer5"></div>
<div class="tabHead" align="center">eSuds Report Website Options
(all fields optional)</div>
<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td class="label" width="24%">User Name (i.e. e-mail)</td>
			<td class="data" width="35%"><input name="app_user_name"
				size="30">
			</td>
			<td class="label" width="14%">E-mail Address</td>
			<td class="data" width="27%"><input name="email_addr"
				size="30"></td>
		</tr>
		<tr>
			<td class="label">New Sub-domain for user</td>
			<td class="data"><input name="subdomain" size="30"></td>
			<td class="label">&nbsp;</td>
			<td class="data">&nbsp;</td>
		</tr>
		<%} %>
		<tr class="gridHeader"">
			<td class="data" colspan="4" align="center">
			<% 
			if (customer.getActiveFlag()!=null && customer.getActiveFlag().equalsIgnoreCase("Y")){
               long cid = customer.getId();
               if(cid == 1) {
       %> <input type="button" class="cssButton"
				value="List Devices"
				onClick="javascript:window.location = 'devicelist.i?customer_id=<%=cid %>';">
			<% 
                } else {
                    if(count > 0) {
          %> <input type="submit" class="cssButton"
				name="myaction" value="Save" onClick="return validateSubmit1()"> <input
				type="submit" class="cssButton"
				name="myaction" value="Delete"
				onClick="return confirmSubmit2('Delete')"> <input
				type="button" class="cssButton"
				value="List Devices"
				onClick="javascript:window.location = 'devicelist.i?customer_id=<%=cid %>';">

			<% 
                    } else {%> <input type="submit" class="cssButton"
				name="myaction" value="Save" onClick="return validateSubmit1()"> <input
				type="submit" class="cssButton"
				name="myaction" value="Delete"
				onClick="return confirmSubmit2('Delete')"> <input
				type="button" class="cssButtonDisabled"
				value="List Devices" disabled="disabled"> <%              }
                 }
            } else {
                if(action_type.equalsIgnoreCase("New")) {
       %> <input type="submit" class="cssButton"
				name="myaction" value="Save New" onClick="return validateSubmit1()">
			<%           } else {
      
                     long cid = customer.getId();
                     if(cid != 1) {
          %> <input type="submit" class="cssButton"
				name="myaction" value="Undelete"
				onClick="return confirmSubmit2('Undelete')"> <% 
                     }
                 }
             }
%>
			</td>
		</tr>
	</tbody>
</table>
</form>
</div>
</div>
<script type="text/javascript">
var flag = 1;
function confirmSubmit2(myaction)
{
    var agree;
    switch(myaction)
    {
        case 'Delete':
        {
            agree=confirm("To delete a customer record you must first reassign all its devices to a different customer. If you have not done this already, please click Cancel and do so now.\n\nContinue to delete <%=StringUtils.prepareScript(customer.getName())%>?");
            break;
        }
        case 'Undelete':
        {
            agree=confirm('OK to undelete <%=StringUtils.prepareScript(customer.getName())%>?');
            break;
        }
    }
    if (agree)
        return true ;
    else
        return false ;
}

function validateSubmit1()
{
    var flag = document.getElementById('customer_type_id').value;
    if (flag > 0) {
        return true;
    } else {
        alert('Please select a Customer type');
        return false;
    }
}
</script>
<%}%>

<div class="spacer5"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
