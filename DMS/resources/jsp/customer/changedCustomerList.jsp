<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    Results customerList = (Results)inputForm.getAttribute("customerList");
    Long custBankId = inputForm.getLong("custBankId", false, 0);
    Long custId = inputForm.getLong("custId", false, 0);
    String terminalNumber = inputForm.getString("terminalNumber", false);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    
    boolean norec=false;    
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
	
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Newly Activated Customers</div>
</div>
<form name="pForm" id="pForm" method="post" action="changedCustomerList.i">
<table>
<tr><td><input <% out.write( (norec)?" disabled='disabled' class='cssButtonDisabled' ":"class='cssButton'"); %> name="activateBtn" id="activateBtn" type="submit" value="Accept" /></td></tr>
</table>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td width="10"><%if (norec) {%>&nbsp; <%} else {%> <input type="checkbox" onclick="onClick()"/> <%}%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Customer Name</a>
			<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Created Date</a>
			<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Address Name</a>
			<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Address 1</a>
			<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Address 2</a>
				 <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">City</a>
			<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">State</a> 
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Zip</a> 
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">License #</a> 
				<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
			</td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Status</a>
				<%=PaginationUtil.getSortingIconHtml("10", sortIndex)%>
			</td>
		</tr>
	</thead>

	<tbody>
		<%
		    int i = 0;
		    String rowClass = "row0";
		    while (customerList.next())
		    {
		        rowClass = (i % 2 == 0) ? "row1" : "row0";
		        i++;
		%>
		<tr class="<%=rowClass%>">
		 	<td><input type="checkbox" name="cid" id="cid" value="<%= customerList.getFormattedValue("CUSTOMER_ID") %>"></td>
			<td><a href="/customer.i?customerId=<%=customerList.getFormattedValue("CUSTOMER_ID")%>"><%=customerList.getFormattedValue("CUSTOMER_NAME")%></a></td>
			<td><%=customerList.getFormattedValue("CREATE_DATE")%></td>
			<td><%=customerList.getFormattedValue("ADDRESS_NAME")%></td>
			<td><%=customerList.getFormattedValue("ADDRESS1")%></td>
			<td><%=customerList.getFormattedValue("ADDRESS2")%></td>
			<td><%=customerList.getFormattedValue("CITY")%></td>
			<td><%=customerList.getFormattedValue("STATE")%></td>
			<td><%=customerList.getFormattedValue("ZIP")%></td>
			<td><%=customerList.getFormattedValue("LICENSE_NBR")%></td>
			<td><%=customerList.getFormattedValue("STATUS")%></td>
		</tr>
		<%
		    }
		%>
	</tbody>

</table>

<%
    String storedNames = PaginationUtil.encodeStoredNames(null);
%> <jsp:include page="/jsp/include/pagination.jsp" flush="true">
	<jsp:param name="_param_request_url" value="changedCustomerList.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

</form>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">
var checked = false;

function onClick(){
	if(checked){
		uncheckIt();
		checked = false;
	}else{
		checkIt();
		checked = true;
	}	
}

function checkIt(){
	if(document.pForm.cid.length>0){
		for (i=0; i<document.pForm.cid.length; i++){
			document.pForm.cid[i].checked=1;
		}
	}else{
		document.pForm.cid.checked=1;
	}
}

function uncheckIt(){
	if(document.pForm.cid.length>0){
		for (i=0; i<document.pForm.cid.length; i++){
			document.pForm.cid[i].checked=0;
		}
	}else{
		document.pForm.cid.checked=0;
	}
}

</script>


