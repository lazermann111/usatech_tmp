<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>


<%
BigDecimal customerId = RequestUtils.getAttribute(request, "customerId", BigDecimal.class, false); 
Results list_lic = RequestUtils.getAttribute(request, "list_licenses", Results.class, false);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="smallFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Choose License</span></div>
</div>

<div class="tableContent">

<div class="spacer10"></div>

<form name="lForm" id="lForm" method="post" action="addRLicense.i">
<input type="hidden" name="customerId" id="customerId" value="<%=customerId %>" />
<table>
	<tbody>
	
		<tr>
			<td class="control"><input type="radio" name="ltype" id="ltype" value="N" onclick="click1()" checked="checked"></td>
			<td class="label">Enter License Number</td>
		</tr>
		<tr>
			<td class="control">&nbsp;</td>
			<td class="control"><input type="text" name="licNumber" id="licNumber" class="txtBox" /></td>
		</tr>
		
		<tr>
					<td class="control"><input type="radio" name="ltype" id="ltype" value="C" onclick="click2()"></td>
			<td class="label">Select Custom License</td>
		</tr>
		<tr>
			<td class="control">&nbsp;</td>
			<td class="control">
			<select name="licenseId" id="licenseId" size="10" disabled="disabled">	
				<%
				if (list_lic != null) {
				while(list_lic.next()) {
				BigDecimal lid = list_lic.getValue("LICENSE_ID", BigDecimal.class);
				%><option value="<%=lid%>"><%=list_lic.getFormattedValue("LICENSE_NAME")%></option>	    	
				<%}}%>
			</select>
			</td>
		</tr>
		
		<tr>
			<td class="control">&nbsp;</td>
			<td class="control">
				<input name="selectBtn" id="selectBtn" type="button" class="cssButton"
				value="Select" onclick="selectLicense();"/>
				
				<input name="cancelBtn" id="cancelBtn" type="button" class="cssButton"
				value="Cancel" onclick="window.location = '/customer.i?customerId=<%=customerId%>';" />
				<input type="button" value="Show" id="btnLicense" onclick="window.location = '/license.i?licenseId=' + lForm.licenseId.value;"/>
			</td>
		</tr>

</tbody>
</table>		
</form>

<div class="spacer10"></div>
</div>
</div>

<script type="text/javascript" defer="defer">
var show=false;

var ltype = document.getElementById("ltype");
var licenseId = document.getElementById("licenseId");
var licNumber = document.getElementById("licNumber");
var lForm = document.getElementById("lForm");

function selectLicense(){
	
	if( getValue1()=='C' && (licenseId.value == null || licenseId.value=='') ){
		alert('Choose license please');
		return;
	}

	if( getValue1()=='N' && (licNumber.value == null || licNumber.value=='') ){
		alert('Enter number please');
		return;
	}
	
	lForm.submit();
}

function getValue1(){
	for (i=0; i<document.lForm.ltype.length; i++){
		if(document.lForm.ltype[i].checked){
			return document.lForm.ltype[i].value;
		}
	}
}

function click1(){
	disable(licenseId);
	enable(licNumber);
	show = !show;
}

function click2(){
	enable(licenseId);
	disable(licNumber);
	show = !show;
}

</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />