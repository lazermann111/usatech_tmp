<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="app_user_name" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_fname" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_lname" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_email_addr" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_password" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_active_yn_flag" class="java.lang.String" scope="request" />
<jsp:useBean id="force_pw_change_yn_flag" class="java.lang.String" scope="request" />
<jsp:useBean id="myaction" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
	
	<script LANGUAGE="JavaScript">
	   function confirmPasswordChange()
	   {
	      var agree=confirm("Are you sure you want to reset password?");
	      if (agree)
	          return true ;
	      else
	          return false ;
	    }
	</script>
	
	<%
	long app_user_id = ConvertUtils.getLongSafely(request.getAttribute("app_user_id"), -1);
	%>
		
		<div class="formContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">App User Edit Page</div>
		</div>
		<form method="post" action="editAppUserSave.i" autocomplete="off">
			<table class="tabDataDisplayBorder" align=center >
			
			<tr>
			<td class="leftHeader">User Name</td>
			<td><input type="text" name="app_user_name" tabindex="4"  size="30" maxlength="50" value="${app_user_name }"/></td>
			</tr>
			<tr>
			<td class="leftHeader">User First Name</td>
			<td><input type="text" name="app_user_fname" tabindex="6"  size="30" maxlength="50" value="${app_user_fname }"/></td>
			
			</tr>
			<tr>
			<td class="leftHeader">User Last Name</td>
			<td><input type="text" name="app_user_lname" tabindex="8"  size="30" maxlength="50" value="${app_user_lname }"/></td>
			</tr>
			<tr>
			<td class="leftHeader">User Email Addr</td>
			<td><input type="text" name="app_user_email_addr" tabindex="10"  size="30" maxlength="50" value="${app_user_email_addr }"/></td>
			</tr>
			<tr>
			<td class="leftHeader">User Password</td>
			<td>
			<%if (app_user_id > 0) {%>
				<input type="hidden" name="app_user_id" value="<%=app_user_id%>" />
				<input type="hidden" name="myaction" value="Update" />
				****************<input class="cssButton" type="submit" name="userOP" value='Change And Notify' onClick="return confirmPasswordChange()" />
				<input class="cssButton" type="submit" name="userOP" value="Notify" />
			<%} else {%>
				<input type="hidden" name="myaction" value="Insert" />
				<input type="text" name="app_user_password" tabindex="10" size="30" maxlength="50" value="${app_user_password }"/>
			<%}%>
			</td>
			</tr>
			<tr>			
			<td class="leftHeader">User Active</td>
			<td><select name="app_user_active_yn_flag" tabindex="14">
			<c:set var="active_selected_yes" value="" />
			<c:set var="active_selected_no" value="" />
			<c:choose>
				<c:when test="${(app_user_active_yn_flag!='') and (app_user_active_yn_flag=='Y')}"><c:set var="active_selected_yes" value="selected" /></c:when>
				<c:otherwise><c:choose><c:when test="${(app_user_active_yn_flag!='') and (app_user_active_yn_flag=='N')}"><c:set var="active_selected_no" value="selected" /></c:when></c:choose> </c:otherwise>
			</c:choose>
			<option value="Y" ${active_selected_yes}>Yes</option>
			<option value="N" ${active_selected_no}>No</option>
			</select></td>
			</tr>
			<tr>
			<td class="leftHeader">Force Password Change</td>
			<td>
			<c:set var="force_pw_change_selected_yes" value="" />
			<c:set var="force_pw_change_selected_no" value="" />
			<c:choose>
				<c:when test="${(force_pw_change_yn_flag!='') and (force_pw_change_yn_flag=='Y')}"><c:set var="force_pw_change_selected_yes" value="selected" /></c:when>
				<c:otherwise><c:choose><c:when test="${(force_pw_change_yn_flag!='') and (force_pw_change_yn_flag=='N')}"><c:set var="force_pw_change_selected_no" value="selected" /></c:when></c:choose> </c:otherwise>
			</c:choose>
				<select name="force_pw_change_yn_flag" tabindex="16">
				<option value="Y" ${force_pw_change_selected_yes }>Yes</option>
				<option value="N" ${force_pw_change_selected_no }>No</option>
			
				</select>
			</td>
			</tr>
			<tr>
			<td colspan=2 align=center><br/><input class="cssButton" type="submit" tabindex="1" name="userOP" value="Save" /> 
			<input class="cssButton"  type="button" tabindex="2" name="userOP" value="&lt; Back" onclick="javascript:history.go(-1);" /><br/><br/></td>
			</tr>
			</table>
		</form>
		</div>
		
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />