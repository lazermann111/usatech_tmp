<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="simple.util.NameValuePair"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="user_count" class="java.lang.Object" scope="request" />
<jsp:useBean id="app_user_id" class="java.lang.Object" scope="request" />
<jsp:useBean id="myaction" class="java.lang.String" scope="request" />
<jsp:useBean id="userOP" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_fname_search" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_lname_search" class="java.lang.String" scope="request" />
<jsp:useBean id="app_user_email_addr_search" class="java.lang.String" scope="request" />

<jsp:useBean id="popup_fname" class="java.lang.String" scope="request" />
<jsp:useBean id="popup_lname" class="java.lang.String" scope="request" />
<jsp:useBean id="popup_email" class="java.lang.String" scope="request" />

<jsp:useBean id="chkbox6" class="java.lang.String" scope="request" />
<jsp:useBean id="chkbox1" class="java.lang.String" scope="request" />
<jsp:useBean id="chkbox2" class="java.lang.String" scope="request" />
<jsp:useBean id="chkbox3" class="java.lang.String" scope="request" />
<jsp:useBean id="chkbox5" class="java.lang.String" scope="request" />

<jsp:useBean id="editEsudsPrivs" class="java.lang.String" scope="request" />

<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/esudsPrivileges.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
		
		<%List searchOptions = new ArrayList();
		searchOptions.add(new NameValuePair(new String("Contains"), new String("C")));
		searchOptions.add(new NameValuePair(new String("Begins"), new String("B")));
		searchOptions.add(new NameValuePair(new String("Ends"), new String("E")));%>
		
		<div class="formContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">eSuds Privileges Screen</div>
		</div>
		
		<form name=myForm method="post" action="esudsPrivileges.i"> 
			<table class="tabDataDisplayBorderNoFixedLayout" width="100%" align=center >
			<tr>
			<td class="leftHeader">App User First Name</td>
			
			<td  >
				<select name="popup_fname" tabindex="2">
					<c:forEach var="nvp_item_search_option" items="<%=searchOptions%>">
						<c:choose>
							<c:when test="${nvp_item_search_option.value == popup_fname || nvp_item_search_option.value == 'C'}">
								<option value="${nvp_item_search_option.value}" selected="selected">${nvp_item_search_option.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${nvp_item_search_option.value}">${nvp_item_search_option.name}</option>
							</c:otherwise>
						</c:choose>	    	
				    </c:forEach>
				</select>
			<input type="text" name="app_user_fname_search" tabindex="1"  size="40" id="id_app_user_fname" value="${app_user_fname_search}" /></td>
			</tr>
			<tr>
			<td class="leftHeader">App User Last Name</td>
			<td  >
				<select name="popup_lname" tabindex="4">
				
				<c:forEach var="nvp_item_search_option" items="<%=searchOptions%>">
					<c:choose>
						<c:when test="${nvp_item_search_option.value == popup_lname || nvp_item_search_option.value == 'C'}">
							<option value="${nvp_item_search_option.value}" selected="selected">${nvp_item_search_option.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_search_option.value}">${nvp_item_search_option.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
			</select>
			<input type="text" name="app_user_lname_search" tabindex="3"  size="40" id="id_app_user_lname" value="${app_user_lname_search}" /></td>
			
			</tr>
			<tr>
			<td class="leftHeader">App User Primary Email</td>
			<td  >
				<select name="popup_email" tabindex="6">
				
					<c:forEach var="nvp_item_search_option" items="<%=searchOptions%>">
						<c:choose>
							<c:when test="${nvp_item_search_option.value == popup_email || nvp_item_search_option.value == 'C'}">
								<option value="${nvp_item_search_option.value}" selected="selected">${nvp_item_search_option.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${nvp_item_search_option.value}">${nvp_item_search_option.name}</option>
							</c:otherwise>
						</c:choose>	    	
				    </c:forEach>
			
			</select>
			<input type="text" name="app_user_email_addr_search" tabindex="5"  size="40" id="id_app_user_email_addr" value="${app_user_email_addr_search}" /></td>
			</tr>
			<tr>
			<td  class="leftHeader">Restrict Search by<br/>Privilege Type</td>
			
			<td  align="center">
			<c:set var="chkbox6_selected" value="checked=\"checked\"" />
			<c:set var="chkbox1_selected" value="" />
			<c:set var="chkbox2_selected" value="checked=\"checked\"" />
			<c:set var="chkbox3_selected" value="checked=\"checked\"" />
			<c:set var="chkbox5_selected" value="checked=\"checked\"" />
			
			
			<c:choose><c:when test="${((myaction != '') and (userOP != 'Save')) and (chkbox6 == '')}"><c:set var="chkbox6_selected" value="" /></c:when></c:choose>
			<c:choose><c:when test="${((myaction != '') and (userOP != 'Save')) and (chkbox1 == '')}"><c:set var="chkbox1_selected" value="" /></c:when></c:choose>
			<c:choose><c:when test="${((myaction != '') and (userOP != 'Save')) and (chkbox2 == '')}"><c:set var="chkbox2_selected" value="" /></c:when></c:choose>
			<c:choose><c:when test="${((myaction != '') and (userOP != 'Save')) and (chkbox3 == '')}"><c:set var="chkbox3_selected" value="" /></c:when></c:choose>
			<c:choose><c:when test="${((myaction != '') and (userOP != 'Save')) and (chkbox5 == '')}"><c:set var="chkbox5_selected" value="" /></c:when></c:choose>
			
			<table class="tabDataDisplayNoBorder" width=30% align=center >
			<tr>
			<td  align="left"><label><input type="checkbox" name="chkbox6" value="6" tabindex="18" ${chkbox6_selected} id="checkbox6_id" />Admin Reports</label></td>
			<td  align="left"><label><input type="checkbox" name="chkbox1" value="1" tabindex="20" <%if(chkbox1.equals("1")) out.write(" checked='checked' ");%> id="checkbox1_id" />Student Access</label></td>
			</tr>
			<tr>
			<td  align="left"><label><input type="checkbox" name="chkbox3" value="3" tabindex="22" ${chkbox3_selected} id="checkbox3_id" />User Maintanence</label></td>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td  align="left"><label><input type="checkbox" name="chkbox2" value="2" tabindex="24" ${chkbox2_selected} id="checkbox2_id" />Operator Access</label></td>
			<td>&nbsp;</td>
			
			</tr>
			<tr>
			<td  align="left"><label><input type="checkbox" name="chkbox5" value="5" tabindex="26" ${chkbox5_selected} id="checkbox5_id" />Operator Reports</label></td>
			<td>&nbsp;</td>
			</tr>
			</table>
			</td>
			</tr>
			</table>
			<c:choose>
				<c:when test="${not empty editEsudsPrivs and editEsudsPrivs eq 'true'}">
					<div align="center">
					<input type="hidden" name="tab_index_edit_start" value="29"  />
					<br/><input class="cssButton" type="button" tabindex="27" value="&lt; Back" onClick="javascript:history.go(-1);">
					<input class="cssButton" type="submit" tabindex="28" name="myaction" value="Search" onClick="return confirmSubmit()">
					</div>
					<br/><br/>
					
					<jsp:include page="esudsPrivilegesEdit.jsp" flush="true" />
					
					
				</c:when>
				<c:otherwise>
					<div align=center>
						<BR/><input class="cssButton" type="submit" tabindex="27" name="myaction" value="Search" onclick="return confirmSubmit()" />
						<input class="cssButton"  type="button" tabindex="28" value="New App User" onclick="javascript:window.location='/newAppUser.i';" />
					</div>
					<BR/><input type="hidden" name="app_user_id" value="${app_user_id }"  />					
				</c:otherwise>			
			</c:choose>
			
			<c:choose>
				<c:when test="${not empty myaction and myaction eq 'Search'}">
					<jsp:include page="esudsPrivilegesSearchResults.jsp" flush="true" />
				</c:when>
			
			</c:choose>
			
		</form>
		</div>
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />