<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />


<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    long device_id = inputForm.getLong("device_id",false,-1);
    long customer_id = inputForm.getLong("customer_id", false, -1);
    if (device_id <= 0 || customer_id <= 0){
%>
<div class="tableContainer">
<div align="center"><span class="error">Required Parameter Not Found: device_id, customer_id</span></div>
</div>
<%} else {%>
<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">Change DMS Customer</div>
</div>

<div align="center">
<form method="post" action="changeCustomer.i"><BR/><BR/>	
<input type="hidden" name="device_id" value="<%=device_id %>"/>
	<select id="new_customer_id" name="new_customer_id" style="font-family: courier; font-size: 12px;">
	<%Results customerNameInfo = (Results)inputForm.getAttribute("customerNameInfo");
	String label = "";
	while (customerNameInfo.next()) {
		if(customerNameInfo.getFormattedValue(2) != null && customerNameInfo.getFormattedValue(2).startsWith("Unknown")){
			label = "Undefined";
		}else{
			label = customerNameInfo.getFormattedValue(2);
		}
	
	%>
	   <option value="<%=customerNameInfo.getFormattedValue(1) %>" <%=(customerNameInfo.getValue(1, Integer.class).intValue() == customer_id)?" selected":"" %>><%=label %></option>
	<%} %>
		</select>
		<input type="text" name="item_search" id="item_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("item_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'item_search_link')" />
		<a id="item_search_link" href="javascript:getData('type=customer&id=new_customer_id&name=new_customer_id&noDataValue=1&noDataName=Undefined&selected=<%=customer_id%>&search=' + document.getElementById('item_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
		<BR/><BR/><BR/>				
	<input type="submit" class="cssButton" name="userOP" value="Change Customer"><BR/><BR/><BR/>
</form>
</div>
</div>
<%}%>

<div class="spacer5"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
