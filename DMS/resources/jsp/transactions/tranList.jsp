<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.*"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.util.*"%>
<%@page import="com.usatech.dms.transaction.TranConstants"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.*"%>
<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
if(missingParam != null && missingParam.booleanValue() == true) {
%>
<div class="tableContainer">
<span class="error">Required parameter not found: tran_id, start_date, end_date</span>
</div>
<%	
} else {
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    
    int tranId = inputForm.getInt(TranConstants.PARAM_TRAN_ID, false, -1);
    String beforeDate = inputForm.getString("tran_before_date", false);
    String afterDate = inputForm.getString("tran_after_date", false);
    String serialNumber = inputForm.getString("serial_number", false);
    
%>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Tran List</div>
</div>
<form method="POST" action="tranList.i">
<table class="tabDataDisplayBorderNoFixedLayout" cellspacing="0" cellpadding="5" border="1" width="100%">
	<thead>
		<tr class="sortHeader">
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Tran ID</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</th>
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Time</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</th>
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Tran State</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</th>
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Sale Amount</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</th>
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Account Data</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</th>
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Global Trans No</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</th>
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Payment Type</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</th>
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Device Name</a>
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
			</th>
			<th>
				<a href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">Device Serial</a>
				<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
			</th>
		</tr>
	</thead>
	<tbody>
	<% 
	Results fileList = (Results) request.getAttribute("tranList");
	int i = 0; 
	String rowClass = "row0";
	while(fileList.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++;
	%>
		<tr class="<%=rowClass%>">
		    <td><a href="tran.i?tran_id=<%=fileList.getFormattedValue("s_tran_id")%>"><%=fileList.getFormattedValue("s_tran_id")%></a></td>
		    <td><%=fileList.getFormattedValue("tran_start_ts")%></td>
		    <td><%=fileList.getFormattedValue("tran_state_desc")%></td>
		    <td><%=fileList.getFormattedValue("sale_amount")%></td>
		    <td><%=fileList.getFormattedValue("tran_received_raw_acct_data")%></td>
		    <td><%=fileList.getFormattedValue("tran_global_trans_cd")%></td>
		    <td><%=fileList.getFormattedValue("payment_subtype_class")%></td>
		    <td><%=fileList.getFormattedValue("device_name")%></td>
		    <td><%=fileList.getFormattedValue("device_serial_cd")%></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{TranConstants.PARAM_TRAN_SERIAL, TranConstants.PARAM_TRAN_ID,
		TranConstants.PARAM_TRAN_START_TS_NAME, TranConstants.PARAM_TRAN_BEFORE_DATE, TranConstants.PARAM_TRAN_AFTER_DATE});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="tranlist.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>
    
    
</form>
</div>


<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />