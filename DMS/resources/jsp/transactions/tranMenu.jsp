<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.model.Location"%>
<%@page import="com.usatech.dms.transaction.TranConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMessage");
if((missingParam != null && missingParam.booleanValue() == true) || errorMessage != null && errorMessage.trim().length() > 0) {
	if(missingParam != null && missingParam.booleanValue() == true){
		%>
		<div class="tableContainer">
		<span class="error">Required parameter not found: location_id, location_name</span>
		</div>
		<%	
	}else{
		%>
		<div class="tableContainer">
		<span class="error">Error Message: <%= errorMessage%></span>
		</div>
		<%	
	}
} else {
	simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	Helper.getSelectList(inputForm, TranConstants.NVP_TERMINAL_LIST_FORMATTED, "GET_TERMINALS_BY_MERCHANT_FORMATTED", true, new Object[] {}, "", null, null);
	Helper.getSelectList(inputForm, TranConstants.NVP_AUTHORITY_LIST_FORMATTED, "GET_AUTHORITY_FORMATTED", true, new Object[] {}, "", null, null);
	Helper.getSelectList(inputForm, TranConstants.NVP_MERCHANT_LIST_FORMATTED, "GET_MERCHANT_FORMATTED", true, new Object[] {}, "", null, null);
    List<NameValuePair> nvp_list_terminals = (List<NameValuePair>)inputForm.getAttribute(TranConstants.NVP_TERMINAL_LIST_FORMATTED);
    List<NameValuePair> nvp_list_authority = (List<NameValuePair>)inputForm.getAttribute(TranConstants.NVP_AUTHORITY_LIST_FORMATTED);
    List<NameValuePair> nvp_list_merchant = (List<NameValuePair>)inputForm.getAttribute(TranConstants.NVP_MERCHANT_LIST_FORMATTED);
    String currentDate = Helper.getCurrentDate();
    %>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Transactions</div>
</div>
 <form name="batchList" method="get" action="batchList.i">
 <table class="tabDataDisplayBorderNoFixedLayout" cellspacing="0" cellpadding="5" border="1" width="100%">
	 <tr><th colspan="2" class="gridHeader">Batches</th></tr>
	 <tr>
	  <td width="15%">Terminal</td>
	  <td>
		  <select name="terminal_id" id="terminal_id" class="smallText">
			<c:forEach var="nvp_list_terminal_item" items="<%=nvp_list_terminals%>">
				<option value="${nvp_list_terminal_item.value}">${nvp_list_terminal_item.name}</option> 	
		    </c:forEach>
		   </select>
		   <input type="button" class="cssButton" value="View Terminal" onclick="window.location = 'terminalInfo.i?terminal_id=' + document.getElementById('terminal_id').value;" />
	  </td>
	 </tr>
	 <tr>
	  <td>Batch Number</td>
	  <td><input type="text" name="terminal_batch_num"></td>
	 </tr>
	 <tr>
	  <td>State</td>
	  <td nowrap>
	   <input type="radio" name="state" value="open" checked="checked" />Open
	   <input type="radio" name="state" value="closed" />Closed
	   <input type="radio" name="state" value="" />Either
	   &nbsp;|&nbsp;
	   <input type="radio" name="with_tran" value="Y" checked="checked" />With Transactions
	   <input type="radio" name="with_tran" value="N" />Without Transactions
	   <input type="radio" name="with_tran" value="" />Either
	   <div class="spacer5"></div>
	   <table class="noBorder">
	   <tr>
	   <td bgcolor="lightgreen" bgcolor="lightgreen">Green</td>
	   <td><input type="text" name="green_top_limit" tabindex="3" value="840" size="6" maxlength="8" /> minutes</td>
	   <td bgcolor="gold">Orange</td>
	   <td><input type="text" name="gold_top_limit" tabindex="5" value="1080" size="6" maxlength="8" /> minutes</td>
	   <td bgcolor="tomato">Red</td>
	   </tr>
	   </table>
	  </td>
	 </tr>
	 <tr>
	  <td>Authority</td>
	  <td>
	   <select name="authority_id" id="authority_id">
			<c:forEach var="nvp_list_authority_item" items="<%=nvp_list_authority%>">
				<option value="${nvp_list_authority_item.value}">${nvp_list_authority_item.name}</option> 	
		    </c:forEach>
	   </select>
	  </td>
	 </tr>
	 <tr>
	  <td class="nowrap">Batch Open Date</td>
	  <td>
	   <table cellspacing="0" cellpadding="0" class="noBorder">
	    <tr><td>From</td><td>
	    
	    <input type="text" name="open_after" id=11 value="" size="10" maxlength="10">
	    <img src="/images/calendar.gif" id="open_after_trigger" class="calendarIcon" title="Date selector" />	      
	      
	    </td></tr>
	    <tr><td>To</td><td><input id="12" type="text" name="open_before" value="" size="10" maxlength="10" >
	    <img src="/images/calendar.gif" id="open_before_trigger" class="calendarIcon" title="Date selector" />
	    </td></tr>
	   </table>
	  </td>
	 </tr>
	 <tr>
	  <td class="nowrap">Batch Close Date</td>
	  <td>
	   <table cellspacing="0" cellpadding="0" class="noBorder">
	    <tr><td>From</td><td><input type="text" id="21" name="close_after" value="" size="10" maxlength="10" >	    
	    <img src="/images/calendar.gif" id="close_after_trigger" class="calendarIcon" title="Date selector" />
	    </td></tr>
	    <tr><td>To</td><td><input type="text" id="22" name="close_before" value="" size="10" maxlength="10" >
	    <img src="/images/calendar.gif" id="close_before_trigger" class="calendarIcon" title="Date selector" />
	    </td></tr>
	   </table>
	  </td>
	 </tr>
	 <tr>
	  <td colspan="2" align="center">
	   <input type="submit" name="action" class="cssButton" value="List Batches" onclick="return validateListBatchesBtn();return false;" />
	  </td>
	 </tr>
 </table>
 </form>
 <div class="spacer5"></div>
 <form method="get" action="terminalList.i">
 <table class="tabDataDisplayBorderNoFixedLayout" cellspacing="0" cellpadding="5" border="1" width="100%" >
	 <tr><th colspan="2" class="gridHeader">Terminals</th></tr>
	 <tr>
	  <td width="15%">Authority</td>
	  <td>
	   <select name="authority_id" id="terminal_authority_id">
			<c:forEach var="nvp_list_authority_item" items="<%=nvp_list_authority%>">
				<option value="${nvp_list_authority_item.value}">${nvp_list_authority_item.name}</option> 	
		    </c:forEach>
	   </select>
	   <input type="button" class="cssButton" value="View Authority" onclick="window.location = 'authorityInfo.i?authority_id=' + document.getElementById('terminal_authority_id').value;" />
	  </td>
	 </tr>
	 <tr>
	  <td>Merchant</td>
	  <td>
	   <select name="merchant_id" id="merchant_id">
			<c:forEach var="nvp_list_merchant_item" items="<%=nvp_list_merchant%>">
				<option value="${nvp_list_merchant_item.value}">${nvp_list_merchant_item.name}</option> 	
		    </c:forEach>
	   </select>
	   <input type="button" class="cssButton" value="View Merchant" onclick="window.location = 'merchantInfo.i?merchant_id=' + document.getElementById('merchant_id').value;" />
	  </td>
	 </tr>
	 <tr>
	  <td colspan="2" align="center">
	   <input type="submit" name="action" class="cssButton" value="List Terminals" />
	   <input type="button" class="cssButton" value="Add Merchant" onclick="window.location = 'addMerchant.i?authority_id=' + document.getElementById('terminal_authority_id').value;" />
	   <input type="button" class="cssButton" value="Add Terminal" onclick="window.location = 'addTerminal.i?merchant_id=' + document.getElementById('merchant_id').value;" />
	  </td>
	 </tr>
 </table>
 </form>
 <div class="spacer5"></div>
 <table class="tabDataDisplayBorderNoFixedLayout">
	 <tr><th colspan="2" class="gridHeader">Transactions</th></tr>
	 <tr>
	  <td width="20%">Tran ID</td>
	  <td>
	   <form method="get" action="tran.i"> 
	   <input type="text" name="tran_id" value="">
	   <input type="submit" class="cssButton" value="View Transaction"/> 
	   </form>
	  </td>
	 </tr>
	 <tr>
	  <td>Global Trans No or Merchant Order</td>
	  <td>
	   <form method="get" action="tran.i"> 
	   <input type="text" name="tran_global_trans_cd" value="">
	   <input type="submit" class="cssButton" value="View Transaction"/> 
	   </form>
	  </td>
	 </tr>
 </table>
 <div class="spacer2"></div>
 <form method="get" name="tran" action="tranList.i">
 <table class="tabDataDisplayBorderNoFixedLayout">	 
     <tr>
      <td>Device Serial Number</td>
      <td>
        <input type="text" name="serial_number" value="">
      </td>
     </tr>
	 <tr>
	  <td>Date</td>
	  <td>
	   <table cellspacing="0" cellpadding="0" class="noBorder">
	    <tr><td>From</td><td><input type="text" id="31" name="tran_after_date" value="<%=currentDate %>" size="10" maxlength="10">
	    <img src="/images/calendar.gif" id="tran_after_trigger" class="calendarIcon" title="Date selector" /></td></tr>
	    <tr><td>To</td><td><input type="text" id="32" name="tran_before_date" value="<%=currentDate %>" size="10" maxlength="10">
	    <img src="/images/calendar.gif" id="tran_before_trigger" class="calendarIcon" title="Date selector" /></td></tr>
	   </table>
	  </td>
	 </tr>
	 <tr>
	  <td colspan="2" align="center">
	   <input type="submit" name="myaction" class="cssButton" value="List Transactions" onclick="return validateListTransBtn();return false;"/>
	  </td>
	 </tr>
 </table>
 </form>
 
 </div>

<script type="text/javascript">
Calendar.setup({
    inputField     :    "11",                  // id of the input field
    ifFormat       :    "%m/%d/%Y",            // format of the input field
    button         :    "open_after_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                  // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
Calendar.setup({
    inputField     :    "12",                   // id of the input field
    ifFormat       :    "%m/%d/%Y",             // format of the input field
    button         :    "open_before_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                   // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
Calendar.setup({
    inputField     :    "21",                   // id of the input field
    ifFormat       :    "%m/%d/%Y",             // format of the input field
    button         :    "close_after_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                   // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
Calendar.setup({
    inputField     :    "22",                    // id of the input field
    ifFormat       :    "%m/%d/%Y",              // format of the input field
    button         :    "close_before_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                    // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
Calendar.setup({
    inputField     :    "31",                   // id of the input field
    ifFormat       :    "%m/%d/%Y",             // format of the input field
    button         :    "tran_after_trigger",   // trigger for the calendar (button ID)
    align          :    "B2",                   // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
Calendar.setup({
    inputField     :    "32",                    // id of the input field
    ifFormat       :    "%m/%d/%Y",              // format of the input field
    button         :    "tran_before_trigger",   // trigger for the calendar (button ID)
    align          :    "B2",                    // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

function validateDate(inDate) {
	if(inDate && inDate.value != ''){
		if(!isDate(inDate.value)) {
			inDate.focus();
			inDate.style.backgroundColor='red';
			inDate.select();
	    	return false;
		}
	}
	return true;
}

function validateListBatchesBtn() {
	document.getElementById("11").style.backgroundColor='';
	document.getElementById("12").style.backgroundColor='';
	document.getElementById("21").style.backgroundColor='';
	document.getElementById("22").style.backgroundColor='';
	var validateDate11 = validateDate(document.getElementById("11"));
	var validateDate12 = validateDate(document.getElementById("12"));
	var validateDate21 = validateDate(document.getElementById("21"));
	var validateDate22 = validateDate(document.getElementById("22"));

    if(!(validateDate11) || !(validateDate12) 
    		|| !(validateDate21) || !(validateDate22)){
		
    	return false;
    }else{
    	
        return true;
    }
}

function validateListTransBtn() {
	document.getElementById("31").style.backgroundColor='';
	document.getElementById("32").style.backgroundColor='';
	var validateDate31 = validateDate(document.getElementById("31"));
	var validateDate32 = validateDate(document.getElementById("32"));
    if(!(validateDate31) || !(validateDate32)){
        return false;
    }else{
        return true;
    }
}

</script>
<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />