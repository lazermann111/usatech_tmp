<%@page import="com.usatech.dms.transaction.DFRRejectionListStep"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String sortField = PaginationUtil.getSortField(null);
	String sortIndex = inputForm.getString(sortField, false);
	sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? DFRRejectionListStep.DEFAULT_SORT_INDEX : sortIndex;
	boolean norec = false;
	
	String totalCount = (String) request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length() > 0 && Integer.parseInt(totalCount) == 0)
		norec = true;
	
	String action = inputForm.getString("action", false);
	String dfr_from_date = inputForm.getString("dfr_from_date", false);
	if (StringHelper.isBlank(dfr_from_date))
		dfr_from_date = Helper.getDefaultStartDate();
	String dfr_from_time = inputForm.getString("dfr_from_time", false);
	if (StringHelper.isBlank(dfr_from_time))
		dfr_from_time = "00:00:00";
	String dfr_to_date = inputForm.getString("dfr_to_date", false);
	if (StringHelper.isBlank(dfr_to_date))
		dfr_to_date = Helper.getDefaultEndDate();
	String dfr_to_time = inputForm.getString("dfr_to_time", false);
	if (StringHelper.isBlank(dfr_to_time))
		dfr_to_time = Helper.getDefaultEndTime();
	
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">DFR Rejections</div>
</div>

<table>
<% if (!StringUtils.isBlank(msg)) { %>
	<tr><td class="status-info"><%=msg%></td></tr>
<% } %>
<% if (!StringUtils.isBlank(err)) { %>
	<tr><td class="status-error"><%=err%></td></tr>
<% } %>
</table>

<div class="spacer5"></div>
<form name="pForm" id="pForm" method="post" action="dfrRejectionList.i" onsubmit="return validateDate();">
	&nbsp;&nbsp;
	<b>
	DFR File From <input type="text" name="dfr_from_date" id="dfr_from_date" value="<%=dfr_from_date%>" size="8" maxlength="10" >
	<img src="/images/calendar.gif" id="dfr_from_date_trigger" class="calendarIcon" title="Date selector" /> 
	<input type="text" size="6" maxlength="8" id="dfr_from_time" name="dfr_from_time" value="<%=dfr_from_time%>" />
	To <input type="text" name="dfr_to_date" id="dfr_to_date" value="<%=dfr_to_date%>" size="8" maxlength="10">
	<img src="/images/calendar.gif" id="dfr_to_date_trigger" class="calendarIcon" title="Date selector" />
	<input type="text" size="6" maxlength="8" id="dfr_to_time" name="dfr_to_time" value="<%=dfr_to_time%>" />
	<input type="submit" name="action" class="cssButton" value="List Rejections" />
	</b>

<div class="spacer10"></div>
<%if (!StringUtils.isBlank(totalCount)) {%>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Tran&nbsp;ID</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Device&nbsp;Serial&nbsp;#</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Upload Time</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Amount</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Card</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Merchant Order</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">DFR File ID</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
		</tr>
	</thead>
	<tbody>
	<% if (norec) { %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<% } else {
	Results list = (Results) request.getAttribute("resultlist");
	int i = 0; 
	String rowClass = "row0";
	while(list.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++; %>
		<tr class="<%=rowClass%>">
		<td><a href="/tran.i?tran_id=<%=list.getFormattedValue("TRAN_ID")%>"><%=list.getFormattedValue("TRAN_ID")%></a></td>
		<td><%=list.getFormattedValue("DEVICE_SERIAL_CD")%></td>
		<td><%=list.getFormattedValue("TRAN_UPLOAD_TS")%></td>
		<td><%=list.getFormattedValue("AMOUNT")%></td>
		<td><%=list.getFormattedValue("CARD")%></td>
		<td><%=list.getFormattedValue("MERCHANT_ORDER")%></td>
		<td><a href="/DFRFile.i?dfrFileId=<%=list.getFormattedValue("FILE_CACHE_ID")%>"><%=list.getFormattedValue("FILE_CACHE_ID")%></a></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%
	String storedNames = PaginationUtil.encodeStoredNames(new String[] {
		"action", "dfr_from_date", "dfr_from_time", "dfr_to_date", "dfr_to_time", "fileCacheId"}); %>

<jsp:include page="/jsp/include/pagination.jsp" flush="true">
	<jsp:param name="_param_request_url" value="dfrRejectionList.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

<% } %>

</form>
<% } %>

<div class="spacer15"></div>

</div>

<script type="text/javascript" defer="defer">
	Calendar.setup({
		inputField	: "dfr_from_date",// id of the input field
		ifFormat		: "%m/%d/%Y", // format of the input field
		button			: "dfr_from_date_trigger", // trigger for the calendar (button ID)
		align		  	: "B2", // alignment (defaults to "Bl")
		singleClick	: true,
		onUpdate		: "swap_dates"
	});
	Calendar.setup({
		inputField	: "dfr_to_date", // id of the input field
		ifFormat		: "%m/%d/%Y", // format of the input field
		button			: "dfr_to_date_trigger", // trigger for the calendar (button ID)
		align				: "B2", // alignment (defaults to "Bl")
		singleClick	: true,
		onUpdate		: "swap_dates"
	});

	var fromDate = document.getElementById("dfr_from_date");
	var fromTime = document.getElementById("dfr_from_time");
	var toDate = document.getElementById("dfr_to_date");
	var toTime = document.getElementById("dfr_to_time");
	
	function validateDate() {
		if(!isDate(fromDate.value)) {
			fromDate.focus();
			return false;
		}
		if(!isTime(fromTime.value)) {
			fromTime.focus();
			return false;
		}
		if(!isDate(toDate.value)) {
			toDate.focus();
			return false;
		}
		if(!isTime(toTime.value)) {
			toTime.focus();
			return false;
		}
		return true;
	}
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
