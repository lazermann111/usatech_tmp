<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.*"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.util.*"%>
<%@page import="com.usatech.dms.transaction.TranConstants"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.*"%>
<%@page import="java.math.*"%>
<%@page import="java.text.*"%>
<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
if(missingParam != null && missingParam.booleanValue() == true) {
%>
<div class="tableContainer">
<span class="error">Required parameter not found: tran_id, start_date, end_date</span>
</div>
<%	
} else {
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    int greenTopLimit = inputForm.getInt("green_top_limit", false, -1);
    int goldTopLimit = inputForm.getInt("gold_top_limit", false, -1);
    String color = "white";
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"); 
    Calendar tempDate = null;
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "5" : sortIndex;
    
%>
<c:set var="grnTopLimit" value="<%=greenTopLimit %>"/>
<c:set var="gldTopLimit" value="<%=goldTopLimit %>"/>
<c:set var="dateColor" value="<%=color %>"/>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Terminal Batch List</div>
</div>
<form>
<table class="tabDataDisplayBorderNoFixedLayout" cellspacing="0" cellpadding="5" border="1" width="100%">
		<tr class="sortHeader">
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Merchant</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Terminal</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Batch Num</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">State</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Time Opened</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Time Closed</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Authority</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
		</tr>
	<% 
	Results fileList = (Results) request.getAttribute("batchList");
	int i = 0;
	String rowClass = "row1";
	while(fileList.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row1";
		i++;
	%>
		<tr class="<%=rowClass %>">
			<c:set var="dateColor" value="white"/>
			<c:set var="closed" value="<%=fileList.getFormattedValue(\"terminal_batch_close_ts\") %>"/>
		    <td><a href="/merchantInfo.i?merchant_id=<%=fileList.getFormattedValue("merchant_id")%>"><%=fileList.getFormattedValue("merchant_name")%></a></td>
		    <td><a href="/terminalInfo.i?terminal_id=<%=fileList.getFormattedValue("terminal_id")%>"><%=fileList.getFormattedValue("terminal_cd")%></a></td>
		    <td><a href="/terminalBatch.i?terminal_batch_id=<%=fileList.getFormattedValue("terminal_batch_id")%>"><%=fileList.getFormattedValue("terminal_batch_num")%></a></td>
		    <td><c:choose><c:when test="${(closed eq null) or (closed eq '')}">Open</c:when><c:otherwise>Closed</c:otherwise></c:choose></td>
		    <c:set var="now" value="<%=(Calendar.getInstance()).getTimeInMillis()%>"/>
		    <% tempDate = Calendar.getInstance();tempDate.setTime((Date)dateFormat.parse(fileList.getFormattedValue("terminal_batch_open_ts")));%>
		    <c:set var="date" value="<%=tempDate.getTimeInMillis()%>"/>
		    <c:set var="diff" value="${(now - date)/(1000)}"/>
		    <c:choose>
		    <c:when test="${(closed eq null) or (closed eq '')}">
			    <c:choose>
					<c:when test="${diff lt (60 * grnTopLimit)}">
						<c:set var="dateColor" value="lightgreen"/>
					</c:when>
					<c:otherwise>
					 	<c:choose>
							<c:when test="${diff lt (60 * gldTopLimit)}">
								<c:set var="dateColor" value="gold"/>
							</c:when>
							<c:otherwise>
								<c:set var="dateColor" value="tomato"/>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</c:when>	
			</c:choose>	  
		    <td bgcolor="${dateColor}"><%=fileList.getFormattedValue("terminal_batch_open_ts")%></td>
		    <td><%=fileList.getFormattedValue("terminal_batch_close_ts")%></td>
		    <td><a href="/authorityInfo.i?authority_id=<%=fileList.getFormattedValue("authority_id")%>"><%=fileList.getFormattedValue("authority_name")%></a></td>
		</tr>
	<% } %>
</table>

<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{TranConstants.PARAM_BATCH_MERCHANT_ID,TranConstants.PARAM_BATCH_TERMINAL_BATCH_NUM,
		TranConstants.PARAM_BATCH_TERMINAL_ID,TranConstants.PARAM_BATCH_TERMINAL_STATE_NAME, TranConstants.PARAM_BATCH_GOLD_TOP_LIMIT, TranConstants.PARAM_BATCH_GREEN_TOP_LIMIT,
		TranConstants.PARAM_BATCH_TERMINAL_BATCH_CLOSED_AFTER, TranConstants.PARAM_BATCH_TERMINAL_BATCH_CLOSED_BEFORE, 
		TranConstants.PARAM_BATCH_TERMINAL_BATCH_OPENED_AFTER, TranConstants.PARAM_BATCH_TERMINAL_BATCH_OPEN_BEFORE, TranConstants.PARAM_BATCH_AUTHORITY_ID,
		TranConstants.PARAM_BATCH_WITH_TRAN});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="batchlist.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>
</form>
</div>


<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />