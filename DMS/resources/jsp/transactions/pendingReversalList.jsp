<%@page import="com.usatech.dms.transaction.PendingReversalListStep"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String sortField = PaginationUtil.getSortField(null);
	String sortIndex = inputForm.getString(sortField, false);
	sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? PendingReversalListStep.DEFAULT_SORT_INDEX : sortIndex;
	boolean norec = false;
	
	String totalCount = (String) request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length() > 0 && Integer.parseInt(totalCount) == 0)
		norec = true;
	
	String action = inputForm.getString("action", false);	
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Pending Reversals</div>
</div>

<table>
<% if (!StringUtils.isBlank(msg)) { %>
	<tr><td class="status-info"><%=msg%></td></tr>
<% } %>
<% if (!StringUtils.isBlank(err)) { %>
	<tr><td class="status-error"><%=err%></td></tr>
<% } %>
</table>

<div class="spacer5"></div>
<form name="pForm" id="pForm" method="post" action="pendingReversalList.i" onsubmit="return validateDate();">
	&nbsp;&nbsp;
	<input type="submit" name="action" class="cssButton" value="View Reversals" />
	<input type="submit" name="action" class="cssButton" value="Approve All" onclick="return confirm('Are you sure you want to approve all pending reversals?');" />
	<input type="submit" name="action" class="cssButton" value="Reject All"  onclick="return confirm('Are you sure you want to reject all pending reversals?');" />	
	<input type="submit" name="action" class="cssButton" value="Approve Selected" onclick="return confirm('Are you sure you want to approve selected pending reversals?');" />	
	<input type="submit" name="action" class="cssButton" value="Reject Selected"  onclick="return confirm('Are you sure you want to reject all pending reversals?');" />
	

<div class="spacer10"></div>
<%if (!StringUtils.isBlank(totalCount)) {%>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td width="10"><%if (norec) {%>&nbsp; <%} else {%> <input type="checkbox" onclick="onClick()"/> <%}%></td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Tran&nbsp;ID</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Device&nbsp;Serial&nbsp;#</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Upload Time</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Amount</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Card</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Global Trans No</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Tran Type</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Reversal Type</a>
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
			</td>
		</tr>
	</thead>
	<tbody>
	<% if (norec) { %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<% } else {
	Results list = (Results) request.getAttribute("resultlist");
	int i = 0; 
	String rowClass = "row0";
	while(list.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++; %>
		<tr class="<%=rowClass%>">
		<td><input type="checkbox" name="tranId" value="<%=list.getFormattedValue("TRAN_ID")%>" /></td>
		<td><a href="/tran.i?tran_id=<%=list.getFormattedValue("TRAN_ID")%>"><%=list.getFormattedValue("TRAN_ID")%></a></td>
		<td><%=list.getFormattedValue("DEVICE_SERIAL_CD")%></td>
		<td><%=list.getFormattedValue("TRAN_UPLOAD_TS")%></td>
		<td><%=list.getFormattedValue("AMOUNT")%></td>
		<td><%=list.getFormattedValue("CARD")%></td>
		<td><%=list.getFormattedValue("TRAN_GLOBAL_TRANS_CD")%></td>
		<td><%=list.getFormattedValue("TRAN_TYPE")%></td>
		<td><%=list.getFormattedValue("REVERSAL_TYPE")%></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%
	String storedNames = PaginationUtil.encodeStoredNames(new String[] {"action"}); %>

<jsp:include page="/jsp/include/pagination.jsp" flush="true">
	<jsp:param name="_param_request_url" value="pendingReversalList.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

<% } %>

</form>
<% } %>

<div class="spacer15"></div>

</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">
var checked = false;

function onClick(){
	if(checked){
		uncheckIt();
		checked = false;
	}else{
		checkIt();
		checked = true;
	}
}

function checkIt(){
	if(document.pForm.tranId.length>0){
		for (i=0; i<document.pForm.tranId.length; i++){
			document.pForm.tranId[i].checked=1;
		}
	}else{
		document.pForm.tranId.checked=1;
	}
}

function uncheckIt(){
	if(document.pForm.tranId.length>0){
		for (i=0; i<document.pForm.tranId.length; i++){
			document.pForm.tranId[i].checked=0;
		}
	}else{
		document.pForm.tranId.checked=0;
	}
}

</script>

