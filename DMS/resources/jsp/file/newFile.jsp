<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.*"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.util.*"%>
<%@page import="com.usatech.dms.file.FileConstants"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.*"%>
<%@page import="java.math.*"%>
<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMsg = (String) request.getAttribute("errorMsg"); 

if((missingParam != null && missingParam.booleanValue() == true) || !(StringHelper.isBlank(errorMsg))) {
%>
<div class="tableContainer">
<span class="error"><%=errorMsg %></span>
</div>
<%	
} else {
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    
    String evNumber = inputForm.getString("ev_number", false);
    
    List<NameValuePair> nvp_list_fileTransferTypes = (List<NameValuePair>)dms_values_list_fileTransferTypesList.getList();
    
	    
%>
<form method="post" action="newFileFunc.i" enctype="multipart/form-data">
<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">Upload File to Server <%=(evNumber!= null && evNumber.length() > 0) ? " for " + StringUtils.encodeForHTML(evNumber) : ""  %></div>
</div>
<input type="hidden" name="ev_number" value="<%=StringUtils.encodeForHTMLAttribute(evNumber)%>"/>
<input type="hidden" name="device_type_id" value="<%=inputForm.getStringSafely("device_type_id", "")%>"/>
<table class="tabDataDisplayBorder" cellspacing="0" cellpadding="5" border="1" width="100%"> 
	 <tr>
	  <td>File Name on Server (as Sent to Client)</td>
	  <td><input type="text" name="file_name" size="40" maxlength="255" /></td>
	 </tr>
	 <tr>
	  <td>File Type</td>
	  <td>
		  <select name="file_type" id="file_type">
		    <c:forEach var="nvp_item_transferTypes" items="<%=nvp_list_fileTransferTypes %>">
					<option value="${nvp_item_transferTypes.value}">${nvp_item_transferTypes.name}</option>   	
			    </c:forEach>
		  </select>	    
	  </td>
	 </tr>
	 <tr>
	  <td>Descriptive Comment</td>
	  <td><input type="text" name="file_comment" size="60" maxlength="200" /></td>
	 </tr>
	 <tr>
	  <td>File to Upload to Server</td>
	  <td><input type="file" name="file_data" size="80"/></td>
	 </tr>
	 <tr>
	  <td colspan="2" align="center">
	   <input type="submit" name="action" class="cssButton" value="Upload to Server" />
	  </td>
 	</tr>
 
</table>
</div>
</form>

<% } %>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />