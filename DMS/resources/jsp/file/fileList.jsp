<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.sql.Connection"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.ProcessingUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.file.FileConstants"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.*"%>
<%@page import="com.usatech.dms.util.Helper"%>
<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
if(missingParam != null && missingParam.booleanValue() == true) {
%>
<div class="tableContainer">
<span class="error">Required parameter not found: file_name, file_id, file_type</span>
</div>
<%	
} else {
	final simple.io.Log log = simple.io.Log.getLog();
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String call_from_date = inputForm.getString("call_from_date", false);
    if (StringHelper.isBlank(call_from_date)) {
        call_from_date = Helper.getFirstOfMonth();
    }
    String call_to_date = inputForm.getString("call_to_date", false);
    if (StringHelper.isBlank(call_to_date)) {
        call_to_date = Helper.getDefaultEndDate();
    }
    boolean bulk = inputForm.getBoolean("bulk", false, false);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    String fileTransferTypeCd = inputForm.getString(FileConstants.PARAM_FILE_TYPE_CD, false);
    String fileName = inputForm.getStringSafely(FileConstants.PARAM_FILE_NAME, "");
    String evNumber = inputForm.getStringSafely("ev_number", "");
    String serialNumber = inputForm.getStringSafely("serial_number", "");
    int deviceTypeId = inputForm.getInt("device_type_id", false, -1);
    
    List<NameValuePair> nvp_list_fileTransferTypes = (List<NameValuePair>)dms_values_list_fileTransferTypesList.getList();
    String fileTransferTypeName = FileActions.getFileTransferTypeName(nvp_list_fileTransferTypes, fileTransferTypeCd);
    if(StringHelper.isBlank(fileTransferTypeName)){
    	fileTransferTypeName = "";
    }
    List<NameValuePair> searchOptions = new ArrayList<NameValuePair>();
	searchOptions.add(new NameValuePair(new String("Begins"), new String("B")));
	searchOptions.add(new NameValuePair(new String("Equals"), new String("E")));
    
%>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">File List</div>
</div>
<form id="fileListForm" method="post"<%if (!bulk) {%> action="fileList.i"<%}%>>
<%if (bulk) {%>
	<input type="hidden" name="bulk" value="<%=inputForm.getStringSafely("bulk", "")%>">
	<input type="hidden" name="include_device_ids" value="<%=inputForm.getStringSafely("include_device_ids", "")%>" />
	<input type="hidden" name="params_to_change" value="<%=inputForm.getStringSafely("params_to_change", "")%>" />
	<input type="hidden" name="params_to_change_request" value="<%=inputForm.getStringSafely("params_to_change_request", "")%>" />
	<input type="hidden" name="customer_id" value="<%=inputForm.getStringSafely("customer_id", "")%>" />
	<input type="hidden" name="location_id" value="<%=inputForm.getStringSafely("location_id", "")%>" />
	<input type="hidden" name="parent_location_id" value="<%=inputForm.getStringSafely("parent_location_id", "")%>" />
	<input type="hidden" name="device_type_id" value="<%=deviceTypeId%>" />
	<input type="hidden" name="zero_counters" value="<%=inputForm.getStringSafely("zero_counters", "")%>" />
	<input type="hidden" name="debug" value="<%=inputForm.getStringSafely("debug", "")%>" />
	<input type="hidden" name="pos_pta_tmpl_id" value="<%=inputForm.getStringSafely("pos_pta_tmpl_id", "")%>" />
	<input type="hidden" name="mode_cd" value="<%=inputForm.getStringSafely("mode_cd", "")%>" />
	<input type="hidden" name="order_cd" value="<%=inputForm.getStringSafely("order_cd", "")%>" />
	<input type="hidden" name="set_terminal_cd_to_serial" value="<%=inputForm.getStringSafely("set_terminal_cd_to_serial", "N")%>" />
	<input type="hidden" name="only_no_two_tier_pricing" value="<%=inputForm.getStringSafely("only_no_two_tier_pricing", "N")%>" />
	<input type="hidden" name="file_uploads" value="<%=inputForm.getStringSafely("file_uploads", "")%>" />
	<input type="hidden" name="file_upload_fields" value="<%=inputForm.getStringSafely("file_upload_fields", "")%>" />
	<input type="hidden" name="eft" value="<%=inputForm.getStringSafely("eft", "")%>" />
	<input type="hidden" name="file_downloads" value="<%=inputForm.getStringSafely("file_downloads", "")%>" />
	<input type="hidden" name="file_download_fields" value="<%=inputForm.getStringSafely("file_download_fields", "")%>" />
	<input type="hidden" name="s2c_requests" value="<%=inputForm.getStringSafely("s2c_requests", "")%>" />
	<input type="hidden" name="s2c_request_fields" value="<%=inputForm.getStringSafely("s2c_request_fields", "")%>" />
	<input type="hidden" name="forward" value="<%=inputForm.getStringSafely("forward", "")%>" />
	<input type="hidden" name="firmware_upgrades" value="<%=inputForm.getStringSafely("firmware_upgrades", "")%>" />
	<input type="hidden" name="myaction" value="Upload to Device" /> 
	<input type="hidden" name="file_transfer_id" id="file_transfer_id" />
<%} else {%>
<input type="hidden" name="file_transfer_type_cd" id="file_transfer_type_cd" value="<%=(fileTransferTypeCd==null?"":fileTransferTypeCd) %>"/>
<input type="hidden" name="ev_number" value="<%=StringUtils.prepareCDATA(evNumber)%>" />
<input type="hidden" name="device_type_id" value="<%=deviceTypeId%>" />
<div class="tableBorder" align="center">
	<br/><table><tr><td align="left"><b>File Type:</b></td><td colspan="3" align="left"><%=fileTransferTypeName%></td></tr>
	<tr>
	<td align="left">
	    <b>From</b>
	</td>
	<td colspan="3" align="left">
	    <input type="text" name="call_from_date" id="call_from_date" value="<%=call_from_date%>" size="8" maxlength="10" >
        <img src="/images/calendar.gif" id="from_date_trigger" class="calendarIcon" title="Date selector" style="position: relative; top: 3px;"/>
	</td>
	</tr>
	<tr>
	<td align="left">
        <b>To</b>
    </td>
    <td colspan="3" align="left">
        <input type="text" name="call_to_date" id="call_to_date" value="<%=call_to_date%>" size="8" maxlength="10">
        <img src="/images/calendar.gif" id="to_date_trigger" class="calendarIcon" title="Date selector" style="position: relative; top: 3px;"/>
    </td>
    </tr>
		<tr><td align="left"><b>File Name:</b></td><td>
		<select name="file_name_search_char" id="file_name_search_char">
			<c:forEach var="nvp_item_search_option" items="<%=searchOptions%>">
				<c:choose>
					<c:when test="${nvp_item_search_option.value == file_name_search_char}">
						<option value="${nvp_item_search_option.value}" selected="selected">${nvp_item_search_option.name}</option>
					</c:when>
					<c:otherwise>
						<option value="${nvp_item_search_option.value}">${nvp_item_search_option.name}</option>
					</c:otherwise>
				</c:choose>	    	
		    </c:forEach>
	    </select>
		</td><td><input type="text" name="file_transfer_name" id="file_transfer_name" size="30" maxlength="60" value="<%=fileName %>" /></td>
		<td><input type="submit" name="userOP" class="cssButton" value="Submit" /></td></tr></table><br/>
</div>
<div class="spacer5"></div>
<%}%>
<div align="center" class="gridHeader"><%=bulk ? "Choose File to Upload" : !StringHelper.isBlank(evNumber) ? "Choose File to Upload to " + StringUtils.encodeForHTML(evNumber) : "File Search Results" %></div>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout" cellspacing="0" cellpadding="5" border="1" width="100%">
	<thead>
		<tr class="sortHeader">
			<%if (bulk) {%>
			<td>ID</td>
			<td>Name</td>
			<td>Type</td>
			<td>Comment</td>
			<td>Created Date</td>
			<td>Last Updated</td>
			<td>Size Bytes</td>
			<%} else {%>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">ID</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Name</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Type</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Comment</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Created Date</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Last Updated</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Size Bytes</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
			<%}%>
		</tr>
	</thead>
	<tbody>
	<% 
	Results fileList = (Results) request.getAttribute("fileList");
	int i = 0; 
	String rowClass = "row0";
	Connection conn = null;
	try {
		conn = DataLayerMgr.getConnection("OPER");
		while(fileList.next()) {
			rowClass = (i%2 == 0) ? "row1" : "row0";
			i++;
		%>
			<tr class="<%=rowClass%>">
				<td>
				<%if (!StringHelper.isBlank(evNumber)) {%>
					<a href="fileDetailsFunc.i?file_transfer_id=<%=fileList.getFormattedValue(1)%>&ev_number=<%=evNumber%>&serial_number=<%=serialNumber%>&device_type_id=<%=deviceTypeId%>&myaction=Upload to Device"><%=fileList.getFormattedValue(1)%></a>
				<%} else if (bulk) {%>
					<a href="javascript:selectFile('<%=fileList.getFormattedValue(1)%>');"><%=fileList.getFormattedValue(1)%></a>
				<%} else {%>
					<a href="fileDetails.i?file_transfer_id=<%=fileList.getFormattedValue(1)%>"><%=fileList.getFormattedValue(1)%></a>
				<%}%>
				</td>	    	
			    <td><%=fileList.getFormattedValue(2)%></td>
				<td><%=FileActions.getFileTransferTypeName(nvp_list_fileTransferTypes, fileList.getFormattedValue(3))%></td>		    
			    <td><%=fileList.getFormattedValue(4)%></td>
			    <td><%=fileList.getFormattedValue(5)%></td>
			    <td><%=fileList.getFormattedValue(6)%></td>
			    <td align="right"><%=FileActions.getFileSizeFormatted(conn, fileList.getValue(1, long.class))%></td>
			</tr>
	<%	}
	} finally { 
		ProcessingUtils.closeDbConnection(log, conn);
	} %>
	</tbody>
</table>

<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{FileConstants.PARAM_FILE_NAME,
		FileConstants.PARAM_FILE_ID, FileConstants.PARAM_FILE_TYPE_CD,
		FileConstants.PARAM_FILE_COMMENT, FileConstants.PARAM_FILE_CREATED,
		FileConstants.PARAM_FILE_UPDATED, FileConstants.PARAM_FILE_SIZE,
		"ev_number", "file_name_search_char",
		"bulk", "include_device_ids", "params_to_change", "params_to_change_request", "customer_id", "location_id", "parent_location_id", "device_type_id", "zero_counters",
		"debug", "pos_pta_tmpl_id", "mode_cd", "file_uploads", "file_upload_fields", "eft", "file_downloads", "file_download_fields",
		"s2c_requests", "s2c_request_fields", "firmware_upgrades", "forward", "call_from_date", "call_to_date"});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="filelist.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>
</form>
</div>

<% if (bulk) {%>
<script type="text/javascript">
function selectFile(fileId) {
	document.getElementById('file_transfer_id').value = fileId;
	document.getElementById('fileListForm').submit();
}
</script>
<%}%>

<% if (!bulk) {%>
<script type="text/javascript" defer="defer">
    Calendar.setup({
        inputField     :    "call_from_date",      // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "call_to_date",         // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });

    var fromDate = document.getElementById("call_from_date");
    var fromTime = document.getElementById("call_from_time");
    var toDate = document.getElementById("call_to_date");
    var toTime = document.getElementById("call_to_time");

    function validateDate() {
    	if(!isDate(fromDate.value)) {
        	fromDate.focus();
        	return false;
    	}
    	if(!isTime(fromTime.value)) {
        	fromTime.focus();
        	return false;
    	}
    	if(!isDate(toDate.value)) {
        	toDate.focus();
        	return false;
    	}
    	if(!isTime(toTime.value)) {
        	toTime.focus();
        	return false;
    	}
    	return true;
    }

</script>
<%}%>

<%}%>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
