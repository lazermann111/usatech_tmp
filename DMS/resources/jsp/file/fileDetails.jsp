<%@page import="com.usatech.dms.model.DmsPrivilege"%>
<%@page import="simple.servlet.BasicServletUser"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.util.*"%>
<%@page import="com.usatech.dms.file.FileConstants"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.*"%>
<%@page import="java.math.*"%>
<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<script type="text/javascript">
function confirmFileDelete(fileId, fileName) {
	var pendingCount = new XHR({
		method: 'get',
		async: false,
	}).send('fileDetailsFunc.i', 'file_transfer_id=' + fileId + '&myaction=count_pending')
	.response.text;
    if (pendingCount == -1) {
    	alert('There was an error while trying to count pending commands.');
    	return false;
    }
    var message = 'Are you sure you want to delete the file \''+ fileName + '\' (id:' + fileId + ')?';
    if (pendingCount > 0) {
    	message += '\nThis file is in ' + pendingCount + ' pending commands now. Proceeding will cancel these pending commands.';
    }
	return confirm(message);
}
</script>

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 

if(missingParam != null && missingParam.booleanValue() == true) {
%>
<div class="tableContainer">
<span class="error">Required parameter not found: file_id, file_type</span>
</div>
<%	
} else {
	BasicServletUser loggedUser = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);

	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "file");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.PARAM_FILE_ID), ""));
	
	Results fileInfoResults = (Results) request.getAttribute("fileInfoResults");	
	String fileContent = (String) request.getAttribute("file_content");
	String dataMode = (String) request.getAttribute("dataMode");
	
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    
    String fileId = inputForm.getString("file_transfer_id", false);
    String lineBreaks = inputForm.getStringSafely("lineBreaks", "");
    int filePreviewSizeKB = inputForm.getInt("filePreviewSizeKB", false, 100);
    String userOP = request.getParameter("userOP");
%>
<c:set var="fileTransferTypeCd" value=""/>
<c:set var="configFileType"><%=FileType.CONFIGURATION_FILE.getValue()%></c:set>
<c:set var="propertyListFileType"><%=FileType.PROPERTY_LIST.getValue()%></c:set>
<c:set var="edgeDefaultConfigFileType"><%=FileType.EDGE_DEFAULT_CONFIGURATION_TEMPLATE.getValue()%></c:set>
<c:set var="edgeCustomConfigFileType"><%=FileType.EDGE_CUSTOM_CONFIGURATION_TEMPLATE.getValue()%></c:set>
<c:set var="nachaFileType"><%=FileType.NACHA_ACH_CCD_FILE.getValue()%></c:set>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">File Details</div>
</div>

<% if(fileInfoResults != null && fileInfoResults.next()) { 
	int fileType = ConvertUtils.getIntSafely(fileInfoResults.getValue("fileTransferTypeCd"), -1);
	StringBuilder statusString = new StringBuilder(fileInfoResults.getFormattedValue("statusDescription"));
	String statusChangedBy = fileInfoResults.getFormattedValue("statusChangedBy");
	if (!"".equals(statusChangedBy)) {
		statusString.append(" (by " + statusChangedBy + ")");
	}
%>
<table class="tabDataDisplayBorder">	
	<c:set var="fileTransferTypeCd"><%=fileInfoResults.getFormattedValue("fileTransferTypeCd")%></c:set>
		
	<tr><td>File ID</td><td><%=StringUtils.prepareCDATA(fileId)%></td><td>Comment</td><td><%=fileInfoResults.getFormattedValue("fileTransferComment")%></td></tr>
	<tr><td>File Name</td><td><%=fileInfoResults.getFormattedValue("fileTransferName")%></td><td>Created</td><td><%=fileInfoResults.getFormattedValue("createdTs")%></td></tr>
	<tr><td>File Type</td><td><%=fileInfoResults.getFormattedValue("fileTransferTypeName")%></td><td>Last Updated</td><td><%=fileInfoResults.getFormattedValue("lastUpdatedTs")%></td></tr>
	<tr><td>File Size</td><td><%=fileInfoResults.getFormattedValue("fileSize")%></td><td>Status</td><td><%=statusString.toString()%></td></tr>
</table>
 
<div class="gridHeader">File Content Preview (First <%=filePreviewSizeKB%> KB, <%=dataMode%>)</div>
	<textarea rows="10" readonly="readonly" style="width: 99.7%;"><%=StringUtils.prepareCDATA(fileContent)%></textarea>
<%if (!"Hex".equalsIgnoreCase(dataMode)) { %>
<div class="gridHeader"><%=lineBreaks %></div>
<%} %>

<% if(fileType != FileType.NACHA_ACH_CCD_FILE.getValue() || loggedUser.hasPrivilege(DmsPrivilege.DMS_ACH_ADMINS.getValue())) { %>
<div class="gridHeader" align="center">
<form method="get" action="fileDetailsFunc.i">
	<input type="hidden" name="file_transfer_id" value="<%=StringUtils.prepareCDATA(fileId) %>">
	<c:choose>
		<c:when test="${(fileTransferTypeCd eq configFileType) or (fileTransferTypeCd eq propertyListFileType) or (fileTransferTypeCd eq edgeDefaultConfigFileType) or (fileTransferTypeCd eq edgeCustomConfigFileType) or (fileTransferTypeCd eq edgeCustomConfigFileType) or (fileTransferTypeCd eq nachaFileType)}">
			<input type="submit" name="myaction" class="cssButton" value="Download File"/>&nbsp;
		</c:when>
		<c:otherwise>
			<input type="submit" name="myaction" class="cssButton" value="Edit File"/>&nbsp;
			<input type="submit" name="myaction" class="cssButton" value="Download File"/>&nbsp;
			<input type="submit" name="myaction" class="cssButton" value="Upload to Device"/>&nbsp;
<%if(!"D".equals(fileInfoResults.get("statusCode"))) {%>
			<input type="submit" name="myaction" class="cssButton" value="Delete File" onclick="return confirmFileDelete(<%=StringUtils.encodeForJavaScript(fileId) %>, '<%=fileInfoResults.getFormattedValue("fileTransferName")%>');"/>
<%} %>
		</c:otherwise>
	</c:choose>
</form>
</div>
<% } %>
<% } %>

<div class="spacer5"></div>

<div class="gridHeader" align="center">
	<b>Transfer Events</b>
	<input type="button" class="cssButton" value="List" onclick="window.location = '/fileDetails.i?file_transfer_id=<%=StringUtils.prepareCDATA(fileId)%>&userOP=transfers';">
</div>
<%if ("transfers".equalsIgnoreCase(userOP)) {%>
<table class="tabDataDisplayBorder" cellspacing="0" cellpadding="5" border="1" width="100%">
		<tr class="gridHeader">
			<td>Transfer ID</td>
			<td>Device</td>
			<td>Transfer Time</td>
			<td>Direction</td>
			<td>Status</td>
			<td>Group Number</td>
			<td>Packet Size</td>
			<td>Call ID</td>
		</tr>
	<tbody>
	<%
	Results deviceFileTransfers = (Results) request.getAttribute("deviceFileTransfers");
	while(deviceFileTransfers.next()) {
		%>
		<tr>
			<td><%=deviceFileTransfers.getFormattedValue("deviceFileTransferId")%></td>
			<td><a href="profile.i?device_id=<%=deviceFileTransfers.getFormattedValue("deviceId")%>"><%=deviceFileTransfers.getFormattedValue("deviceName")%></a></td>
			<td><%=deviceFileTransfers.getFormattedValue("deviceFileTransferTs")%></td>
			<td><%=deviceFileTransfers.getFormattedValue("deviceFileTransferDirect")%></td>
			<td><%=deviceFileTransfers.getFormattedValue("deviceFileTransferStatus")%></td>
			<td><%=deviceFileTransfers.getFormattedValue("deviceFileTransferGroupNum")%></td>
			<td><%=deviceFileTransfers.getFormattedValue("deviceFileTransferPktSize")%></td>
			<td><%if (!StringHelper.isBlank(deviceFileTransfers.getFormattedValue("globalSessionCd"))) {%><a href="/deviceCallInLog.i?session_cd=<%=deviceFileTransfers.getFormattedValue("globalSessionCd")%>"><%=deviceFileTransfers.getFormattedValue("sessionId")%></a><%} %>&nbsp;</td>
		</tr>
	<% } %>

	</tbody>
</table>
<%}%>

<div class="spacer5"></div>

</div>

<% } %>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />