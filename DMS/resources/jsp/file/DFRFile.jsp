<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long dfrFileId = inputForm.getLong("dfrFileId", false, -1); 

if(dfrFileId < 1) {
%>
<div class="tableContainer">
<span class="error">Required parameter not found: dfrFileId</span>
</div>
<%	
} else {
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "dfr_file");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("dfrFileId"), ""));
%>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">DFR File Details</div>
</div>

<% 
Results fileInfoResults = DataLayerMgr.executeQuery("GET_DFR_FILE_INFO", new Object[] {dfrFileId});
if(fileInfoResults.next()) {%>
<table class="tabDataDisplayBorder">
	<tr><td>File ID</td><td><%=dfrFileId%></td><td>File Name</td><td><%=fileInfoResults.getFormattedValue("fileName")%></td></tr>
	<tr><td>File State</td><td><%=fileInfoResults.getFormattedValue("fileStateDesc")%></td><td><%if (fileInfoResults.getValue("fileTypeId", int.class) > 1) {%>Compressed <%}%>File Size</td><td><%=fileInfoResults.getFormattedValue("fileSize")%></td></tr>
	<tr><td>Created</td><td><%=fileInfoResults.getFormattedValue("createdTs")%></td><td>Last Updated</td><td><%=fileInfoResults.getFormattedValue("lastUpdatedTs")%></td></tr>	
</table>

<%if (fileInfoResults.getValue("fileStateId", int.class) > 0) {%>
<div class="gridHeader" align="center">
<form method="post" action="DFRFileFunc.i">
	<input type="hidden" name="dfrFileId" value="<%=dfrFileId %>"/>
	<input type="hidden" name="fileName" value="<%=fileInfoResults.getFormattedValue("fileName")%>"/>
	<input type="submit" name="action" class="cssButton" value="Download File" />
</form>
</div>
<%
}
} else {
%>
<div align="center">
Not found
</div>
<%}%>

<div class="spacer5"></div>

</div>

<% } %>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
