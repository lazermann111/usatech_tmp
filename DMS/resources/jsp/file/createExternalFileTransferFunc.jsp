<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.dms.device.*"%>
<%@page import="com.usatech.dms.util.*"%>
<%@page import="com.usatech.dms.model.*"%>
<%@page import="com.usatech.dms.file.FileConstants"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.*"%>
<%@page import="java.math.*"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Map errorMap = (Map)inputForm.getAttribute("errorMap");

if((errorMap != null && errorMap.size() > 0)) {
	Set keys = errorMap.keySet();
	Iterator it = keys.iterator();
	while(it.hasNext()){
		String key = (String)it.next();
		String errorMsg = (String)errorMap.get(key);
		%>
		<div class="tableContainer">
		<span class="error"><%=StringUtils.encodeForHTML(errorMsg) %></span>
		</div>
		<%	
	}
} else
	out.write(inputForm.getString("resultsTable", false));
%>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />