<%@page import="com.usatech.layers.common.model.Device"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.util.*"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.dms.device.*"%>
<%@page import="com.usatech.dms.util.*"%>
<%@page import="com.usatech.dms.model.*"%>
<%@page import="com.usatech.dms.file.FileConstants"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.*"%>
<%@page import="java.math.*"%>
<%@page import="java.util.*"%>
<%@page import="com.usatech.dms.exception.InternalException"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
boolean bulk = inputForm.getBoolean("bulk", false, false);
Boolean missingParam = (Boolean) request.getAttribute("missingParam");
long deviceId = inputForm.getLong("device_id", false, -1);
String deviceName = inputForm.getStringSafely("device_name", "");
String errorMsg = null;
int deviceTypeId = -1;
if (bulk) {
	deviceTypeId = inputForm.getInt("device_type_id", true, -1);
} else {	
	Device device = null; 
	
	if(deviceId<0 && StringHelper.isBlank(deviceName))
		errorMsg = "Required Parameter Not Found: device_name, device_id : " + (StringHelper.isBlank(deviceName) ? deviceId : deviceName); 
	else
		errorMsg = (String) request.getAttribute("errorMsg");
	
	try{
		if(StringHelper.isBlank(errorMsg) && deviceId<0){
			device = DeviceActions.loadDevice(deviceName, request);
		}else{
			device = DeviceActions.loadDevice(deviceId, request);
		}
	}catch(InternalException e){
		errorMsg=e.getMessage();
	}
	
	if(device!= null) {
		if (device.getId()<0)
			errorMsg = "No device found for device_id=" + device.getId() + ", device_name=" + (device.getDeviceName()==null?deviceName:device.getDeviceName());	
		else if(DeviceType.EDGE.getValue() == device.getTypeId())
			errorMsg = "Edge device " + device.getDeviceName() + " does not support this operation";
		else {
			deviceId = device.getId();
			deviceName = device.getDeviceName();
			deviceTypeId = device.getTypeId();
		}
	}
}

if(missingParam != null && missingParam.booleanValue() == true || !StringHelper.isBlank(errorMsg)) {
%>
<div class="tableContainer">
<span class="error"><%=errorMsg %></span>
</div>
<%	
} else {
   
	Calendar cal = Calendar.getInstance();
	Date d = new Date(cal.getTimeInMillis());
	String start = (String)FileActions.dateTimeformatter.format(d);
	cal.add(Calendar.DAY_OF_MONTH, 7);
	d = new Date(cal.getTimeInMillis());
	String end = (String)FileActions.dateTimeformatter.format(d);
%>

<form method="post" action="createexternalfiletransferfunc.i">
<input type="hidden" name="device_name" value="<%=deviceName %>">
<input type="hidden" name="device_id" value="<%=deviceId %>">
<%if (bulk) { %>
<input type="hidden" name="bulk" value="<%=inputForm.getStringSafely("bulk", "")%>">
<input type="hidden" name="include_device_ids" value="<%=inputForm.getStringSafely("include_device_ids", "")%>" />
<input type="hidden" name="params_to_change" value="<%=inputForm.getStringSafely("params_to_change", "")%>" />
<input type="hidden" name="params_to_change_request" value="<%=inputForm.getStringSafely("params_to_change_request", "")%>" />
<input type="hidden" name="customer_id" value="<%=inputForm.getStringSafely("customer_id", "")%>" />
<input type="hidden" name="location_id" value="<%=inputForm.getStringSafely("location_id", "")%>" />
<input type="hidden" name="parent_location_id" value="<%=inputForm.getStringSafely("parent_location_id", "")%>" />
<input type="hidden" name="device_type_id" value="<%=inputForm.getStringSafely("device_type_id", "")%>" />
<input type="hidden" name="zero_counters" value="<%=inputForm.getStringSafely("zero_counters", "")%>" />
<input type="hidden" name="debug" value="<%=inputForm.getStringSafely("debug", "")%>" />
<input type="hidden" name="pos_pta_tmpl_id" value="<%=inputForm.getStringSafely("pos_pta_tmpl_id", "")%>" />
<input type="hidden" name="mode_cd" value="<%=inputForm.getStringSafely("mode_cd", "")%>" />
<input type="hidden" name="order_cd" value="<%=inputForm.getStringSafely("order_cd", "")%>" />
<input type="hidden" name="set_terminal_cd_to_serial" value="<%=inputForm.getStringSafely("set_terminal_cd_to_serial", "N")%>" />
<input type="hidden" name="only_no_two_tier_pricing" value="<%=inputForm.getStringSafely("only_no_two_tier_pricing", "N")%>" />
<input type="hidden" name="file_uploads" value="<%=inputForm.getStringSafely("file_uploads", "")%>" />
<input type="hidden" name="file_upload_fields" value="<%=inputForm.getStringSafely("file_upload_fields", "")%>" />
<input type="hidden" name="eft" value="<%=inputForm.getStringSafely("eft", "")%>" />
<input type="hidden" name="file_downloads" value="<%=inputForm.getStringSafely("file_downloads", "")%>" />
<input type="hidden" name="file_download_fields" value="<%=inputForm.getStringSafely("file_download_fields", "")%>" />
<input type="hidden" name="s2c_requests" value="<%=inputForm.getStringSafely("s2c_requests", "")%>" />
<input type="hidden" name="s2c_request_fields" value="<%=inputForm.getStringSafely("s2c_request_fields", "")%>" />
<input type="hidden" name="forward" value="<%=inputForm.getStringSafely("forward", "")%>" />
<input type="hidden" name="firmware_upgrades" value="<%=inputForm.getStringSafely("firmware_upgrades", "")%>" />
<%} %>
<table class="tabDataDisplayBorder" cellspacing="0" cellpadding="5" border="1" width="100%">
<tr class="gridHeader">
<td colspan="4">Create External Client to Server File Transfer<%if (!bulk) out.write(" - " + StringUtils.encodeForHTML(deviceName)); %></td>
</tr>
<tr>
<td>Transfer Protocol</td>
<td><select name="protocol" tabindex="2">
		<c:forEach var="nvp_item_protocol" items="<%=FileActions.protocols%>">
		<c:choose>
			<c:when test="${nvp_item_protocol.value == inProtocol}">
				<option value="${nvp_item_protocol.value}" selected="selected">${nvp_item_protocol.name}</option>
			</c:when>
			<c:otherwise>
				<option value="${nvp_item_protocol.value}">${nvp_item_protocol.name}</option>
			</c:otherwise>
		</c:choose>	    	
	   </c:forEach>
  </select>
</td>
<td>File Format</td>
<td><select name="file_format" tabindex="4">
		<c:forEach var="nvp_item_file_format" items="<%=FileActions.fileFormats%>">
		<c:choose>
			<c:when test="${nvp_item_file_format.value == inFileFormat}">
				<option value="${nvp_item_file_format.value}" selected="selected">${nvp_item_file_format.name}</option>
			</c:when>
			<c:otherwise>
				<option value="${nvp_item_file_format.value}">${nvp_item_file_format.name}</option>
			</c:otherwise>
		</c:choose>	    	
	   </c:forEach>
  </select>
</td>
</tr>
<tr>
<td>Server IP Address</td>
<td><input type="text" name="ip_address" tabindex="6" value="216.127.247.19" /></td>
<td>Server Port</td>
<td><input type="text" name="port" tabindex="8" value="21" /></td>
</tr>
<tr>
<td>Login Name</td>
<td><input type="text" name="login_name" tabindex="10" autocomplete="off" /></td>
<td>Login Password</td>
<td><input type="text" name="login_password" tabindex="12" autocomplete="off" /></td>

</tr>
<tr>
<td>File Path on Server</td>
<td colspan=3><input type="text" name="file_path" tabindex="14"  size="70" /></td>
</tr>
<tr>
<td>File Name on Server</td>
<td colspan=3><input type="text" name="file_name" tabindex="16"  size="70" /></td>
</tr>
<tr>
<td>File Encapsulation</td>
<td colspan=3>
<select name="file_encapsulation" tabindex="18">
		<c:forEach var="nvp_item_file_encaps" items="<%=FileActions.fileEncapsulations%>">
		<c:choose>
			<c:when test="${nvp_item_file_encaps.value == inFileEncapsulation}">
				<option value="${nvp_item_file_encaps.value}" selected="selected">${nvp_item_file_encaps.name}</option>
			</c:when>
			<c:otherwise>
				<option value="${nvp_item_file_encaps.value}">${nvp_item_file_encaps.name}</option>
			</c:otherwise>
		</c:choose>	    	
	   </c:forEach>
  </select>
</td>
</tr>
<tr>
<td>CRC Type</td>
<td>
<select name="crc_type" tabindex="20">
		<c:forEach var="nvp_item_crc_type" items="<%=FileActions.crcTypes%>">
		<c:choose>
			<c:when test="${nvp_item_crc_type.value == inCrcType}">
				<option value="${nvp_item_crc_type.value}" selected="selected">${nvp_item_crc_type.name}</option>
			</c:when>
			<c:otherwise>
				<option value="${nvp_item_crc_type.value}">${nvp_item_crc_type.name}</option>
			</c:otherwise>
		</c:choose>	    	
	   </c:forEach>
  </select>

</td>
<td>CRC (In HEX)</td>
<td><input type="text" name="crc" tabindex="22"  size="40" /></td>
</tr>
<tr>
<td>Encryption Key (In HEX)</td>
<td colspan=3><input type="text" name="encryption_key" tabindex="24"  size="70" /></td>
</tr>
<tr>
<td>Transfer Time Window Start</td>
<td><input type="text" name="transfer_start" tabindex="26" value="<%=start %>" size="30" /></td>
<td>Transfer Time Window End</td>

<td><input type="text" name="transfer_end" tabindex="28" value="<%=end %>" size="30" /></td>
</tr>
<tr>
<td>Number of Retries</td>
<td><input type="text" name="retries" tabindex="30" value="5" /></td>
<td>Minimum Retry Interval (Minutes)</td>
<td><input type="text" name="retry_interval" tabindex="32" value="15" /></td>
</tr>
<tr>
<%if(deviceTypeId == DeviceType.KIOSK.getValue()){ %>
<td>Client File Path/Name</td>
<td colspan="3"><input type="text" name="client_file_path" tabindex="34"  size="70" /></td>
</tr>
<%
}
if (!bulk) {
%>
<tr>
<td>Confirmation Email</td>
<td colspan="3"><input type="text" name="confirm_email" tabindex="34"  size="70" /></td>
</tr>
<%} %>
<tr class="gridHeader">
<td align="center" colspan="4">
	<input type="submit" class="cssButton" name="userOP" value="Save" />
	<input type="button" class="cssButton" value="Cancel" onClick="javascript:history.go(-1);" />
</td>
</tr>
</table>
</form>

<% } %>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />