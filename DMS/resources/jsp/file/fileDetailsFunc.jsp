<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.*"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.util.*"%>
<%@page import="com.usatech.dms.file.FileConstants"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.*"%>
<%@page import="java.math.*"%>
<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%!
    public boolean showFirmwareMoveDeviceWarning(int fileType, String fileEnvironmentLetter) {
        boolean appUpgrade = fileType == FileType.APPLICATION_SOFTWARE_UPGRADE.getValue();
        if (!appUpgrade) {
            return false;
        }
        boolean hasEnvironment = !StringHelper.isBlank(fileEnvironmentLetter);
        if (!hasEnvironment) {
            return false;
        }
        String envLetter = WebHelper.getEnvironmentLetter();
        boolean anotherEnvironment = !(envLetter.equalsIgnoreCase(fileEnvironmentLetter) || sameECCEnvironment(envLetter, fileEnvironmentLetter));
        if (!anotherEnvironment) {
            return false;
        }
        return true;
    }

	public static final List<String> ECCEnvironment = Arrays.asList("C", "E");
	
	public boolean sameECCEnvironment(String envLetter, String fileEnvLetter) {
		return ECCEnvironment.contains(envLetter.toUpperCase()) && ECCEnvironment.contains(fileEnvLetter.toUpperCase());
	}
%>

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMsg = (String) request.getAttribute("errorMsg"); 
String action = (String) request.getAttribute("myaction");
if(StringHelper.isBlank(action) || action.equalsIgnoreCase("null")){action = (String) request.getAttribute("myaction");}

if((missingParam != null && missingParam.booleanValue() == true) || !(StringHelper.isBlank(errorMsg))) {
%>
<div class="tableContainer">
<span class="error"><%=errorMsg %></span>
</div>
<%	
} else {
	String fileContent = (String) request.getAttribute("file_content");
	String dataMode = (String) request.getAttribute("dataMode");
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    boolean bulk = inputForm.getBoolean("bulk", false, false);
    long fileId = inputForm.getLong("file_transfer_id", false, -1);
    String evNumber = inputForm.getString("ev_number", false);
    String serialNumber = inputForm.getStringSafely("serial_number", "");
    int deviceTypeId = inputForm.getInt("device_type_id", false, -1);
    if(evNumber==null)evNumber="";
    String lineBreaks = inputForm.getStringSafely("lineBreaks", "");
    String fileName = inputForm.getString("file_name", false);
    int fileType = inputForm.getInt("file_type", false, -1);
    String file_type_name = inputForm.getString("file_type_name", false);
    String fileSize = inputForm.getString("file_size", false);
    String createDate = inputForm.getString("create_date", false);
    String lastUpdate = inputForm.getString("last_update", false);
    String comment = inputForm.getString("comment", false);
    int fileContentSize = inputForm.getInt("fileContentSize", false, -1);
    int packetSize;
    if (deviceTypeId == DeviceType.GX.getValue())
    	packetSize = DeviceUtils.GX_PACKET_SIZE;
    else if (deviceTypeId == DeviceType.KIOSK.getValue() && serialNumber.startsWith("K3"))
    	packetSize = DeviceUtils.EPORT_CONNECT_PACKET_SIZE;
    else
    	packetSize = DeviceUtils.DEFAULT_PACKET_SIZE;
    String fileEnvironmentLetter = "";
    if (deviceTypeId == DeviceType.EDGE.getValue())
    	fileEnvironmentLetter = inputForm.getStringSafely("G9EnvironmentLetter", "");    
    else if (deviceTypeId == DeviceType.GX.getValue())
    	fileEnvironmentLetter = inputForm.getStringSafely("GxEnvironmentLetter", "");
	
	if("Edit File".equalsIgnoreCase(action)){
		
		if(fileType != FileType.EDGE_DEFAULT_CONFIGURATION_TEMPLATE.getValue() && fileType != FileType.NACHA_ACH_CCD_FILE.getValue()) {
			
			%>
			<form method="post" action="fileDetailsFunc.i" enctype="multipart/form-data">
			<input type="hidden" name="dataMode" value="<%=dataMode%>" />
			<table class="tabDataDisplayBorder" cellspacing="0" cellpadding="5" border="1" width="100%">
	 			<tr>
	  				<th class="gridHeader" colspan="1">Edit File - <%=StringUtils.encodeForHTML(fileName) %></th>
	 			</tr>
	 			<tr>
	  				<td>
	   					&nbsp;Comment: <input type="text" name="file_comment" value="<%=comment %>" maxlength="1000" style="width:60%;" />
	   				<%if (!dataMode.equalsIgnoreCase("Hex")) {%>	
	   				Line Breaks:
	   				<select name="line_break_format">
	   					<%
	   					String unixSelected = (lineBreaks.startsWith("Unix") ? "selected" : "");
	   					String windowsSelected = (lineBreaks.startsWith("Win") ? "selected" : "");
	   					%>
	    				<option value="Unix Line Breaks" <%=unixSelected%>>Unix Line Breaks</option>  
	    				<option value="Windows Line Breaks" <%=windowsSelected%>>Windows Line Breaks</option>
	   				</select>
	   				<%}%>
	  				</td>
	 			</tr>
	 			<tr>
	  				<td align="center">
	  				<%if (fileContentSize <= DMSConstants.MAX_EDITABLE_FILE_SIZE) { %>
	  				<br /><b>Edit File Content (<%=dataMode%>):</b>
	  				<br />
	  				<div class="spacer5"></div>
	  				<textarea name="file_content" rows="20" style="width: 99%;"><%=StringUtils.prepareCDATA(fileContent)%></textarea><br />
	  				<br /><b>or Upload File to Server:</b><br />
	  				<div class="spacer5"></div>
	  				<input type="file" name="file_data" size="80"/>
	  				<%} else { %>
	  				<b>File is too large to be edited online, please Upload File to Server:</b><br />
	  				<div class="spacer5"></div>
	  				<input type="file" name="file_data" size="80"/>
	  				<%} %>
	  				</td>
	 			</tr>
	 			<tr>
	  				<td align="center" class="gridHeader">
					   <input type="hidden" name="file_transfer_id" value="<%=fileId %>" />
					   <input type="submit" name="myaction" class="cssButton" value="Save Changes" />
					   <input type="button" value="Cancel" class="cssButton" onClick="window.location = '/fileDetails.i?file_transfer_id=<%=fileId%>';" />
	  				</td>
	 			</tr>
			</table>
			</form>
			<%
			
		} else {
			%>
			<div class="tableContainer"><br/><br/>
				<span class="error"><b><font color="red">'${file_type_name}' files can not be edited!</font></b><br/><br/><br/>
				<input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
				<br/><br/></span>
			</div>
			<%	
		}
		
		
	}else if("Upload to Device".equalsIgnoreCase(action)){
		
		%>
		<form method="post" action="<%=bulk ? "" : "fileDetailsFunc.i"%>">
		<%if (bulk) { %>
			<input type="hidden" name="bulk" value="<%=inputForm.getStringSafely("bulk", "")%>">
			<input type="hidden" name="include_device_ids" value="<%=inputForm.getStringSafely("include_device_ids", "")%>" />
			<input type="hidden" name="params_to_change" value="<%=inputForm.getStringSafely("params_to_change", "")%>" />
			<input type="hidden" name="params_to_change_request" value="<%=inputForm.getStringSafely("params_to_change_request", "")%>" />
			<input type="hidden" name="customer_id" value="<%=inputForm.getStringSafely("customer_id", "")%>" />
			<input type="hidden" name="location_id" value="<%=inputForm.getStringSafely("location_id", "")%>" />
			<input type="hidden" name="parent_location_id" value="<%=inputForm.getStringSafely("parent_location_id", "")%>" />
			<input type="hidden" name="device_type_id" value="<%=deviceTypeId%>" />
			<input type="hidden" name="zero_counters" value="<%=inputForm.getStringSafely("zero_counters", "")%>" />
			<input type="hidden" name="debug" value="<%=inputForm.getStringSafely("debug", "")%>" />
			<input type="hidden" name="pos_pta_tmpl_id" value="<%=inputForm.getStringSafely("pos_pta_tmpl_id", "")%>" />
			<input type="hidden" name="mode_cd" value="<%=inputForm.getStringSafely("mode_cd", "")%>" />
			<input type="hidden" name="order_cd" value="<%=inputForm.getStringSafely("order_cd", "")%>" />
			<input type="hidden" name="set_terminal_cd_to_serial" value="<%=inputForm.getStringSafely("set_terminal_cd_to_serial", "N")%>" />
			<input type="hidden" name="only_no_two_tier_pricing" value="<%=inputForm.getStringSafely("only_no_two_tier_pricing", "N")%>" />
			<input type="hidden" name="file_uploads" value="<%=inputForm.getStringSafely("file_uploads", "")%>" />
			<input type="hidden" name="file_upload_fields" value="<%=inputForm.getStringSafely("file_upload_fields", "")%>" />
			<input type="hidden" name="eft" value="<%=inputForm.getStringSafely("eft", "")%>" />
			<input type="hidden" name="file_downloads" value="<%=inputForm.getStringSafely("file_downloads", "")%>" />
			<input type="hidden" name="file_download_fields" value="<%=inputForm.getStringSafely("file_download_fields", "")%>" />
			<input type="hidden" name="s2c_requests" value="<%=inputForm.getStringSafely("s2c_requests", "")%>" />
			<input type="hidden" name="s2c_request_fields" value="<%=inputForm.getStringSafely("s2c_request_fields", "")%>" />
			<input type="hidden" name="forward" value="<%=inputForm.getStringSafely("forward", "")%>" />
			<input type="hidden" name="firmware_upgrades" value="<%=inputForm.getStringSafely("firmware_upgrades", "")%>" />
		<%} %>
		<table class="tabDataDisplayBorder" cellspacing="0" cellpadding="5" border="1">
		 <tr>
		  <th class="gridHeader" colspan="2">Upload File to Device</th>
		 </tr>
		 <tr>
		  <td width="50%">File ID</td><td width="50%"><%= fileId%></td>
		 </tr>
		 <tr>
		  <td>File Name</td><td><%= StringUtils.prepareCDATA(fileName)%></td>
		 </tr>
		 <tr>
		  <td>File Type</td><td><%=inputForm.getString("file_type_name", false) %></td> 
		 </tr>
		 <tr>
		  <td>Comment</td><td><%= StringUtils.prepareCDATA(comment)%>&nbsp;</td>
		 </tr>
		 <tr>
		  <td>File Size</td><td><%=fileSize%></td>
		 </tr>
		 <%if (!bulk) {%>
		 <tr>		
		<%if(!StringHelper.isBlank(evNumber)){%>			
			<td>Device Name</td><td><input type="hidden" name="ev_number" value="<%=StringUtils.prepareCDATA(evNumber) %>" /><%=StringUtils.prepareCDATA(evNumber) %></td>			
		<%}else{%>			
			<td>Device Name</td><td><input type="text" name="ev_number" value="<%=StringUtils.encodeForHTMLAttribute(evNumber) %>" /></td>			
		<%}%>
		</tr>
		<%if(!StringHelper.isBlank(serialNumber)){%>
		<tr>
		 <td>Device Serial Number</td><td><%=serialNumber%></td>
		</tr>
		<%}%>
		<%}%>
	 	<tr>
	  		<td>Execute Order (0 to determine automatically)</td><td><input type="text" name="execute_order" value="0"></td>
	 	</tr>
	 	<tr>
	  		<td>Packet Size (recommended values: <%=DeviceUtils.GX_PACKET_SIZE%> for Gx, <%=DeviceUtils.EPORT_CONNECT_PACKET_SIZE%> for K3 Kiosks, <%=DeviceUtils.DEFAULT_PACKET_SIZE%> for others)</td><td><input type="text" name="packet_size" value="<%=packetSize%>"></td>
	 	</tr>
	 	<%if (showFirmwareMoveDeviceWarning(fileType, fileEnvironmentLetter)) {%>
	 	<tr>
	 		<td colspan="2" class="warn-solid">WARNING! This firmware will move the device from <%=WebHelper.getCurrentEnvironment()%> to <%=WebHelper.getEnvironment(fileEnvironmentLetter)%> environment.</td>
	 	</tr>
	 	<%}%>
	 	<tr>
	  		<td align="center" colspan="2" class="gridHeader">
	   			<input type="hidden" name="file_transfer_id" value="<%=fileId %>" />
	   			<input type="submit" name="myaction" class="cssButton" value="Schedule Device Upload" />
	   			<input type="button" value="Cancel" class="cssButton" onClick="javascript:history.go(-1);" />
	  		</td>
	 	</tr>	 
	</table>
	</form>
		
		<%
		
	}
}
	
    
		%>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />