<%@page import="com.usatech.dms.sales.CommissionEventType"%>
<%@page import="com.usatech.dms.sales.SalesRep"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<% InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
	String currentDate = Helper.getCurrentDate();
	String currentTime = Helper.getCurrentTime(); 
	List<SalesRep> salesReps = SalesRep.findAllActive();
	List<CommissionEventType> eventTypes = CommissionEventType.findAll(); %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">
	<div class="tableHead">
		<div class="tabHeadTxt"><span class="txtWhiteBold">Update Sales Reps</span></div>
	</div>
	<div class="tableContent">
	<div class="spacer5"></div>
		<form method="post" action="bulkSalesReps.i" onsubmit="return doSubmit()">
			<table class="padding3">
			<% if(msg != null && msg.length() > 0) { %>
				<tr><td colspan="4" class="status-info"><%=msg%></td></tr>
			<% } %>
			<% if(err != null && err.length() > 0) { %>
				<tr><td colspan="4" class="status-error"><%=err%></td></tr>
			<% } %>
				<tr>
					<td class="label" valign="top">
						Serial Numbers
						<font color="red">*</font><br />(1 per line)
					</td>
					<td>
						<textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("dev_list", "")%></textarea>
					</td>
				</tr>
				<tr>
					<td class="label">Sales Rep<font color="red">*</font></td>
					<td class="data">
						<% long salesRepId = inputForm.getLong("salesRepId", false, 0);
							 long fallbackSalesRepId = inputForm.getLong("fallback_salesRepId", false, 0); %>
						<select name="salesRepId" id="salesRepId" onchange="salesRepChanged();">
							<option value="0"></option>
							<option value="-999" <%=salesRepId == -999 ? "selected=\"selected\"" : "" %>>- Current Sales Rep -</option>
							<% for(SalesRep salesRep : salesReps) { %>
						<option value="<%=salesRep.getSalesRepId()%>" <%=salesRep.getSalesRepId() == salesRepId ? "selected=\"selected\"" : ""%>><%=salesRep.getName() %></option>
						<% } %>
						</select>
						<div id="fallback" name="fallback" style="display:<%=salesRepId == -999 ? "inline-block;" : "none"%>;">
							or if none 
							<select name="fallback_salesRepId" id="fallback_salesRepId">
								<option value="0"></option>
								<% for(SalesRep salesRep : salesReps) { %>
								<option value="<%=salesRep.getSalesRepId()%>" <%=salesRep.getSalesRepId() == fallbackSalesRepId ? "selected=\"selected\"" : ""%>><%=salesRep.getName() %></option>
								<% } %>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td class="label">Reason<font color="red">*</font></td>
					<td class="data">
						<select name="eventTypeCode" id="eventTypeCode" style="font-family: 'Courier New', Courier, monospace">
						<% String eventTypeCode = inputForm.getString("eventTypeCode", false);
						%>
						<option value=""></option>
						<% for(CommissionEventType eventType : eventTypes) { %>
						<option style="font-family: 'Courier New', Courier, monospace" <%=eventType.getCode().toString().equals(eventTypeCode) ? "selected=\"selected\"" : ""%> value="<%=eventType.getCode()%>"><%=eventType.getFixedWidthDesc()%></option>
						<% } %>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Effective Date<font color="red">*</font></td>
					<td class="data">
						<input type="text" name="date" id="date" value="<%=Helper.getCurrentDate()%>" size="8" maxlength="10">
						<img src="/images/calendar.gif" id="date_trigger" class="calendarIcon" title="Date selector" />
						&nbsp;<input type="text" size="6" maxlength="8" id="time" name="time" value="<%=Helper.getCurrentTime()%>" />
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<input type="submit" id="action" name="action" value="Change Sales Reps" class="cssButton" />
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>

<div class="spacer10"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
Calendar.setup({
    inputField     :    "date",                  	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "date_trigger",  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

function doSubmit(){
	if(document.getElementById("dev_list").value.trim() == ''){
		alert('Please enter serial numbers');
		return false;
	}
	
	var date = document.getElementById("date");
	var time = document.getElementById("time");

	if (!isDate(date.value)) {
		date.focus();
		return false;
	}
	
	if(!isTime(time.value)) {
		time.focus();
		return false;
	}
	
	var dateTimeStr = date.value + " " + time.value;
	var dateTime = getDateFromFormat(dateTimeStr, "M/d/y H:m:s");
	var midnight = new Date();
	midnight.setHours(0,0,0,0);
	midnight = midnight.getTime();
	if (dateTime < midnight) {
		date.focus();
		alert("Effective date may not be in the past.");
		return false;
	}
	
	var salesRepSelect = document.getElementById("salesRepId");
	if(salesRepSelect.options[salesRepSelect.selectedIndex].text == ''){
		alert('Please select a sales rep');
		return false;
	}

	var eventTypeCodeSelect = document.getElementById("eventTypeCode");
	if(eventTypeCodeSelect.options[eventTypeCodeSelect.selectedIndex].text == ''){
		alert('Please select a reason');
		return false;
	}

	return true;
}

function salesRepChanged() {
	var salesRepId = $('salesRepId').value;
	if (salesRepId == -999) {
		$('fallback').setStyle('display', 'inline-block');
	} else {
		$('fallback').setStyle('display', 'none');
	}
}

</script>
