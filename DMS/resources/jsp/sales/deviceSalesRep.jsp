<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.sales.CommissionEventType"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.dms.sales.SalesRep"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>
<%@page import="simple.text.StringUtils"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String deviceSerialCd = inputForm.getString("deviceSerialCd", true);
	String deviceId = inputForm.getString("deviceId", true);
	SalesRep currentSalesRep = SalesRep.findSalesRepResponsibleForDevice(deviceSerialCd);
	List<SalesRep> salesReps = SalesRep.findAllActive();
	List<CommissionEventType> eventTypes = CommissionEventType.findAll(); %>
<div class="largeFormContainer">
	<div class="tableHead">
		<div class="tabHeadTxt"><span class="txtWhiteBold">Sales Rep for Device <%=StringUtils.prepareCDATA(deviceSerialCd)%></span></div>
	</div>
	<div class="tableContent">
		<div class="spacer5"></div>
		<form name="deviceSalesRepForm" id="deviceSalesRepForm" method="post" action="createCommissionEvent.i" onsubmit="return validateForm();">
			<input type="hidden" name="deviceId" value="<%=StringUtils.encodeForHTMLAttribute(deviceId)%>">
			<input type="hidden" name="deviceSerialCd" value="<%=StringUtils.encodeForHTMLAttribute(deviceSerialCd)%>">
			<table class="padding3">
				<tr>
					<td class="label">Sales Rep<font color="red">*</font></td>
					<td class="data">
						<select name="salesRepId" id="salesRepId">
							<% for(SalesRep salesRep : salesReps) { %>
							<option value="<%=salesRep.getSalesRepId()%>" <% if(currentSalesRep != null && currentSalesRep.getSalesRepId() == salesRep.getSalesRepId()) { %>selected="selected"<% } %>><%=salesRep.getName() %></option>
							<% } %>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Reason<font color="red">*</font></td>
					<td class="data">
						<select name="eventTypeCode" id="eventTypeCode" style="font-family: 'Courier New', Courier, monospace">
							<% for(CommissionEventType eventType : eventTypes) { %>
							<option style="font-family: 'Courier New', Courier, monospace" value="<%=eventType.getCode()%>"><%=eventType.getFixedWidthDesc()%></option>
							<% } %>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label">Effective Date<font color="red">*</font></td>
					<td class="data">
						<input type="text" name="date" id="date" value="<%=Helper.getCurrentDate()%>" size="8" maxlength="10">
						<img src="/images/calendar.gif" id="date_trigger" class="calendarIcon" title="Date selector" />
						&nbsp;<input type="text" size="6" maxlength="8" id="time" name="time" value="<%=Helper.getCurrentTime()%>" />
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<input type="submit" class="cssButton" value="Change Sales Rep"/>
						<input type="button" class="cssButton" value="Cancel" onclick="window.location='profile.i?device_id=<%=deviceId%>&userOP=Sales';"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<div class="spacer5"></div>

<script type="text/javascript" defer="defer">
	Calendar.setup({
		inputField : "date", // id of the input field
		ifFormat : "%m/%d/%Y", // format of the input field
		button : "date_trigger", // trigger for the calendar (button ID)
		align : "B2", // alignment (defaults to "Bl")
		singleClick : true,
		onUpdate : "swap_dates"
	});

	var date = document.getElementById("date");
	var time = document.getElementById("time");

	function validateForm() {
		if (!isDate(date.value)) {
			date.focus();
			return false;
		}
		if(!isTime(time.value)) {
			time.focus();
			return false;
		}
		
		var dateTimeStr = date.value + " " + time.value;
		var dateTime = getDateFromFormat(dateTimeStr, "M/d/y H:m:s");
		var midnight = new Date();
		midnight.setHours(0,0,0,0);
		midnight = midnight.getTime();
		if (dateTime < midnight) {
			date.focus();
			alert("Effective date may not be in the past.");
			return false;
		}
	}
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
