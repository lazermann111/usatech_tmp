<%@page import="com.usatech.dms.sales.Affiliate"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>

<% request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "affiliate");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("affiliateId"), ""));
	
	Long affiliateId = RequestUtils.getAttribute(request, "affiliateId", Long.class, false);
	Affiliate affiliate = null;
	if (affiliateId !=null) {
		affiliate = Affiliate.findByPk(affiliateId);
	} %>
<table>
	<tbody>
		<tr>
			<td class="label">Name</td>
			<td class="control"> 
				<input type="text" name="name" onchange="onDetailChange()" usatRequired="true" label="Name" size="50" maxlength="50" value="<%=affiliate == null ? "" : affiliate.getName()%>"/>
			</td>
		</tr>
		<tr>
			<td class="label">Status</td>
			<td class="control">
				<select name="statusCode" id="status" onchange="onDetailChange()" >
				<%
					for(Affiliate.StatusCode status : Affiliate.StatusCode.values()) {
				%>
					<option value="<%=status.name()%>" <%if (affiliate != null && affiliate.getStatusCode() == status) {%> selected="selected"<% } %>>
						<%=status.getDesc()%>
					</option>
				<%}%>
				</select>
			</td>
		</tr>
	</tbody>
</table>
<div class="spacer10"></div>
