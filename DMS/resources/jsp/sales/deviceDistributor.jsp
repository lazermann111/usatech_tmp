<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.dms.sales.Distributor"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>
<%@page import="simple.text.StringUtils"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String deviceSerialCd = inputForm.getString("deviceSerialCd", true);
	String deviceId = inputForm.getString("deviceId", true);
	Distributor currentDistributor = Distributor.findDistributorForDevice(deviceSerialCd);
	List<Distributor> distributors = Distributor.findAll();
%>
<div class="largeFormContainer">
	<div class="tableHead">
		<div class="tabHeadTxt"><span class="txtWhiteBold">Distributor for Device <%=StringUtils.prepareCDATA(deviceSerialCd)%></span></div>
	</div>
	<div class="tableContent">
		<div class="spacer5"></div>
		<form name="deviceDistributorForm" id="deviceDistributorForm" method="post" action="deviceDistributor.i">
			<input type="hidden" name="deviceId" value="<%=deviceId %>"/>
			<input type="hidden" name="deviceSerialCd" value="<%=StringUtils.encodeForHTMLAttribute(deviceSerialCd)%>">
			<table class="padding3">
				<tr>
					<td class="label">Distributor Name<font color="red">*</font></td>
					<td class="data">
						<select name="distributorId" id="distributorId">
							<% for(Distributor distributor : distributors) { %>
							<option value="<%=distributor.getDistributorId()%>" <% if(currentDistributor != null && currentDistributor.getDistributorId() == distributor.getDistributorId()) { %>selected="selected"<% } %>><%=distributor.getDistributorName() %></option>
							<% } %>
						</select>
					</td>
				</tr>
				
				<tr>
					<td align="center" colspan="2">
						<input type="submit" class="cssButton" name="userOP" value="Change Distributor" onclick="submitForm();" />
						<input type="button" class="cssButton" value="Cancel" onclick="window.location='profile.i?device_id=<%=deviceId%>&userOP=Sales';"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<div class="spacer5"></div>

<script type="text/javascript" defer="defer">
	
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
