<%@page import="com.usatech.dms.sales.PricingTier"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>

<% request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "pricingTier");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("pricingTierId"), ""));
	
	Long pricingTierId = RequestUtils.getAttribute(request, "pricingTierId", Long.class, false);
	PricingTier pricingTier = null;
	if (pricingTierId !=null) {
		pricingTier = PricingTier.findByPk(pricingTierId);
	} %>
<table>
	<tbody>
		<tr>
			<td class="label">Name</td>
			<td class="control"> 
				<input type="text" name="name" onchange="onDetailChange()" usatRequired="true" label="Name" size="50" maxlength="50" value="<%=pricingTier == null ? "" : pricingTier.getName()%>"/>
			</td>
		</tr>
		<tr>
			<td class="label">Status</td>
			<td class="control">
				<select name="statusCode" id="status" onchange="onDetailChange()" >
				<%
					for(PricingTier.StatusCode status : PricingTier.StatusCode.values()) {
				%>
					<option value="<%=status.name()%>" <%if (pricingTier != null && pricingTier.getStatusCode() == status) {%> selected="selected"<% } %>>
						<%=status.getDesc()%>
					</option>
				<%}%>
				</select>
			</td>
		</tr>
	</tbody>
</table>
<div class="spacer10"></div>
