<%@page import="java.util.Arrays"%>
<%@page import="com.usatech.dms.sales.Category"%>
<%@page import="com.usatech.dms.sales.PricingTier"%>
<%@page import="com.usatech.dms.sales.Affiliate"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="org.eclipse.birt.report.model.api.util.StringUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.model.CorpCustomer"%>
<%@page import="java.util.Map"%>
<%@page import="com.usatech.dms.sales.CommissionEventType"%>
<%@page import="com.usatech.dms.sales.SalesRep"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<% InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
	Map<String, List<CorpCustomer>> results = (Map<String, List<CorpCustomer>>) RequestUtils.getAttribute(request, "results", false); %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">
	<div class="tableHead">
		<div class="tabHeadTxt"><span class="txtWhiteBold">Update Customer Sales Attributes</span></div>
	</div>
	<div class="tableContent">
		<div class="spacer5"></div>
		<form method="post" action="customerSales.i">
			<input type="hidden" name="old_lines" id="old_lines" value="<%=inputForm.getStringSafely("lines", inputForm.getStringSafely("old_lines", "")) %>"/>
			<table class="padding3 fullPage">
			<% if(msg != null && msg.length() > 0) { %>
				<tr><td class="status-info"><%=StringUtils.prepareHTML(msg)%></td></tr>
			<% } %>
			<% if(err != null && err.length() > 0) { %>
				<tr><td class="status-error"><%=err%></td></tr>
			<% } %>
			<% if (results == null) { %>
				<tr>
					<td class="label" valign="top">
						Customer Names or Customer IDs<br/>(1 per line)
					</td>
					<td class="data">
						<textarea name="lines" id="lines" rows="5" style="width: 400px;"><%=StringUtils.prepareCDATA(inputForm.getStringSafely("lines", ""))%></textarea>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
					<input type="submit" id="action" name="action" value="Find Customers >" class="cssButton" />
					</td>
				</tr>
			<% } else {
				long[] customerIds = inputForm.getLongArray("customerIds", false);
				if (customerIds == null)
					customerIds = new long[0];
				Arrays.sort(customerIds); %>
				<tr>
					<td class="padding3">
						<table class="padding3 fullPage border" >
							<tr>
								<th class="label">Input</th>
								<th class="label">Update?
									<input type="checkbox" name="checkAll" id="checkAll" onClick="$$('.checkbox').each(function(el) {el.checked = $('checkAll').checked;} )" />
								</th>
								<th class="label">Customer Id</th>
								<th class="label">Name</th>
								<th class="label">Affiliate</th>
								<th class="label">Pricing Tier</th>
								<th class="label">Category</th>
								<th class="label">Address</th>
							</tr>
						<% for (String nameInput : results.keySet()) {
									List<CorpCustomer> customers = results.get(nameInput); 
									boolean first = true; %>
							<tr>
								<td class="label" valign="top" rowspan="<%=customers.isEmpty() ? 1 : customers.size()%>">
									<%=StringUtils.prepareCDATA(nameInput)%>
								</td>
								<% if (customers == null || customers.isEmpty()) { %>
								<td class="data"></td>
								<td class="data"></td>
								<td class="data" colspan="5">
									Customer not found
								</td>
								<% } else { 
								for (CorpCustomer customer : customers) {
									if (!first) { %>
							<tr>
							<%  } 
									first = false; %>
								<td class="data " align="right">
									<input type="checkbox" class="checkbox" id="customerIds" name="customerIds" <%=Arrays.binarySearch(customerIds, customer.getId()) >= 0 ? "checked=\"checked\"" : ""  %> value="<%=customer.getId() %>">
								</td>
								<td class="data ">
									<%=customer.getId() %>
								</td>
								<td class="data ">
									<a href="customer.i?customerId=<%=customer.getId()%>" target="_blank"><%=StringUtils.prepareCDATA(customer.getName()) %></a>
								</td>
								<td class="data ">
									<%=StringUtils.prepareCDATA(customer.getAffiliateName()) %>
								</td>
								<td class="data ">
									<%=StringUtils.prepareCDATA(customer.getPricingTierName()) %>
								</td>
								<td class="data ">
									<%=StringUtils.prepareCDATA(customer.getCategoryName()) %>
								</td>
								<td class="data ">
									<%=StringUtils.prepareCDATA(customer.getFormattedAddress(false))%>
								</td>
							</tr>
							<% } } } %>
						</table>
					</td>
				</tr>
				<tr>
					<td class="padding3">
						<table class="padding3 border" style="width:100%; border:1px solid blue;">
						<% List<Affiliate> affiliates = Affiliate.findAllActive();
								long affiliateId = inputForm.getLong("affiliateId", false, 0); %>
							<tr>
								<td class="label">Sales Affiliate</td>
								<td class="control">
									<select name="affiliateId" id="affiliateId" style="width: 340px;" onchange="onDetailChange()" >
										<option value="0"></option>
									<% for(Affiliate affiliate : affiliates) {%>
										<option <%=affiliateId == affiliate.getAffiliateId() ? "selected=\"selected\"" : "" %> value="<%=affiliate.getAffiliateId()%>"><%=affiliate.getName()%></option>
									<% } %>
									</select>
								</td>
							</tr>
						<% List<PricingTier> pricingTiers = PricingTier.findAllActive();
								long pricingTierId = inputForm.getLong("pricingTierId", false, 0); %>
							<tr>
								<td class="label">Pricing Tier</td>
								<td class="control">
									<select name="pricingTierId" id="pricingTierId" style="width: 340px;" onchange="onDetailChange()" >
										<option value="0"></option>
									<% for(PricingTier pricingTier : pricingTiers) {%>
										<option <%=pricingTierId == pricingTier.getPricingTierId() ? "selected=\"selected\"" : "" %> value="<%=pricingTier.getPricingTierId()%>"><%=pricingTier.getName()%></option>
									<% } %>
									</select>
								</td>
							</tr>
						<% List<Category> categories = Category.findAllActive();
								long categoryId = inputForm.getLong("categoryId", false, 0); %>
							<tr>
								<td class="label">Sales Category</td>
								<td class="control">
									<select name="categoryId" id="categoryId" style="width: 340px;" onchange="onDetailChange()" >
										<option value="0"></option>
									<% for(Category category : categories) {%>
										<option <%=categoryId == category.getCategoryId() ? "selected=\"selected\"" : "" %>  value="<%=category.getCategoryId()%>"><%=category.getName()%></option>
									<% } %>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="padding3">
						<table class="padding3 fullPage border">
							<tr>
								<td align="center">
									<div class="spacer10"></div>
									<input type="submit" id="action" name="action" value="< Back" class="cssButton" />
									&nbsp;&nbsp;
									<input type="submit" id="action" name="action" value="Update Customers >" class="cssButton" />
									<div class="spacer10"></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<% } %>
			</table>
		</form>
	</div>
</div>

<div class="spacer10"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
