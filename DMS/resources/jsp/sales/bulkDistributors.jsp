<%@page import="com.usatech.dms.sales.Distributor"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<% InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
	List<Distributor> distributors = Distributor.findAll();
%>

<jsp:include page="/jsp/include/header.jsp" />

<div class="largeFormContainer">
	<div class="tableHead">
		<div class="tabHeadTxt"><span class="txtWhiteBold">Update Distributors</span></div>
	</div>
	<div class="tableContent">
	<div class="spacer5"></div>
		<form method="post" action="bulkDistributors.i" onsubmit="return doSubmit()">
			<table class="padding3">
			<% if(msg != null && msg.length() > 0) { %>
				<tr><td colspan="4" class="status-info"><%=msg%></td></tr>
			<% } %>
			<% if(err != null && err.length() > 0) { %>
				<tr><td colspan="4" class="status-error"><%=err%></td></tr>
			<% } %>
				<tr>
					<td class="label" valign="top">
						Serial Numbers
						<font color="red">*</font><br />(1 per line)
					</td>
					<td>
						<textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("dev_list", "")%></textarea>
					</td>
				</tr>
				<tr>
					<td class="label">Distributor<font color="red">*</font></td>
					<td class="data">
						<% int distributorId = inputForm.getInt("distributorId", false, 0);%>
						<select name="distributorId" id="distributorId" onchange="distributorChanged();">
							<option value="0"></option>
							<% for(Distributor distributor : distributors) { %>
						<option value="<%=distributor.getDistributorId()%>" <%=distributor.getDistributorId() == distributorId ? "selected=\"selected\"" : ""%>><%=distributor.getDistributorName() %></option>
						<% } %>
						</select>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" align="center">
					<input type="submit" id="action" name="action" value="Change Distributors" class="cssButton" />
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>

<div class="spacer10"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">

function doSubmit(){
	if(document.getElementById("dev_list").value.trim() == ''){
		alert('Please enter serial numbers');
		return false;
	}
	
	var distributorSelect = document.getElementById("distributorId");
	if(distributorSelect.options[distributorSelect.selectedIndex].text == ''){
		alert('Please select a distributor');
		return false;
	}
	
	return true;
}

</script>
