<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<% int i = 0;
	InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String sortField = PaginationUtil.getSortField(null);
	String sortIndex = inputForm.getString(sortField, false);
	sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "3" : sortIndex;
	boolean norec = false;
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if(totalCount != null && totalCount.length() > 0 && Integer.parseInt(totalCount) == 0) {
		norec = true;
	} %>

<div class="largeFormContainer">
	<div class="tableHead">
		<div class="tabHeadTxt">Sales Rep List</div>
	</div>
<% if(norec) { %>
	<div class="tabHead" align="center">NO RECORDS</div>
	<% } else { %>
	<table class="tabDataDisplayBorderNoFixedLayout">
		<thead>
			<tr class="sortHeader">
				<td><a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">ID</a> <%=PaginationUtil.getSortingIconHtml("1", sortIndex)%></td>
				<td><a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">First Name</a> <%=PaginationUtil.getSortingIconHtml("2", sortIndex)%></td>
				<td><a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Last Name</a> <%=PaginationUtil.getSortingIconHtml("3", sortIndex)%></td>
				<td><a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Status</a> <%=PaginationUtil.getSortingIconHtml("4", sortIndex)%></td>
			</tr>
		</thead>
		<tbody>
<% Results list = (Results)request.getAttribute("resultlist");
	while(list.next()) { %>
			<tr class="<%=(i++ % 2 == 0) ? "row1" : "row0"%>">
				<td><a href="/salesRep.i?salesRepId=<%=list.getFormattedValue("SALES_REP_ID")%>"><%=list.getFormattedValue("SALES_REP_ID")%></a></td>
				<td><%=list.getFormattedValue("FIRST_NAME")%></td>
				<td><%=list.getFormattedValue("LAST_NAME")%></td>
				<td><%=list.getFormattedValue("STATUS")%></td>
			</tr>
<% } %>
		</tbody>
	</table>
<% String storedNames = PaginationUtil.encodeStoredNames(new String[] {"SALES_REP_ID", "FIRST_NAME", "LAST_NAME", "STATUS"}); %>
	<jsp:include page="/jsp/include/pagination.jsp" flush="true">
		<jsp:param name="_param_request_url" value="salesRepList.i" />
		<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
	</jsp:include>
<% } %>
</div>
<div class="spacer10"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
