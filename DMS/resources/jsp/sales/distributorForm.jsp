<%@page import="com.usatech.dms.sales.Distributor"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>

<% request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "distributor");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("distributorId"), ""));
	
	Integer distributorId = RequestUtils.getAttribute(request, "distributorId", Integer.class, false);
	Distributor distributor = null;
	if (distributorId !=null) {
		distributor = Distributor.findByPk(distributorId);
	} %>
<table>
	<tbody>
		<tr>
			<td class="label">Name</td>
			<td class="control"> 
				<input type="text" name="distributorName" onchange="onDetailChange()" usatRequired="true" label="Name" size="50" maxlength="100" value="<%=distributor == null ? "" : distributor.getDistributorName()%>"/>
			</td>
		</tr>
		<tr>
			<td class="label">Description</td>
			<td class="control"> 
				<input type="text" name="description" onchange="onDetailChange()" usatRequired="true" label="Description" size="50" maxlength="300" value="<%=distributor == null ? "" : distributor.getDescription()%>"/>
			</td>
		</tr>
	</tbody>
</table>
<div class="spacer10"></div>
