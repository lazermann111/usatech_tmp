<%@page import="com.usatech.dms.sales.SalesRep"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>

<% request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "salesRep");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("salesRepId"), ""));
	
	Long salesRepId = RequestUtils.getAttribute(request, "salesRepId", Long.class, false);
	SalesRep salesRep = null;
	if (salesRepId !=null) {
		salesRep = SalesRep.findByPk(salesRepId);
	} %>
<table>
	<tbody>
		<tr>
			<td class="label">First Name</td>
			<td class="control"> 
				<input type="text" name="firstName" onchange="onDetailChange()" usatRequired="true" label="First Name" size="50" maxlength="50" value="<%=salesRep == null ? "" : salesRep.getFirstName()%>"/>
			</td>
		</tr>
		<tr>
			<td class="label">Last Name</td>
			<td class="control"> 
				<input type="text" name="lastName" onchange="onDetailChange()" usatRequired="true" label="Last Name" size="50" maxlength="50" value="<%=salesRep == null ? "" : salesRep.getLastName()%>"/>
			</td>
		</tr>
		<tr>
			<td class="label">Status</td>
			<td class="control">
				<select name="statusCode" id="status" onchange="onDetailChange()" >
				<%
					for(SalesRep.StatusCode status : SalesRep.StatusCode.values()) {
				%>
					<option value="<%=status.name()%>" <%if (salesRep != null && salesRep.getStatusCode() == status) {%> selected="selected"<% } %>>
						<%=status.getDesc()%>
					</option>
				<%}%>
				</select>
			</td>
		</tr>
	</tbody>
</table>
<div class="spacer10"></div>
