<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="action" class="java.lang.String" scope="request" />
<jsp:useBean id="start_date" class="java.lang.String" scope="request" />
<jsp:useBean id="end_date" class="java.lang.String" scope="request" />
<jsp:useBean id="storedNames" class="java.lang.String" scope="request" />

<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	<c:otherwise>
	
		<jsp:include page="gprsweb_header.jsp" flush="true" />
		<c:choose>
			<c:when test="${(action eq 'Show All')}">
				<span><b>Daily SIM Usage between ${start_date } and ${end_date }</b></span>
				
				<jsp:useBean id="gprsweb_usage_show_all_results" type="simple.results.Results" scope="request" />
				<c:set var="sortIndex" value="${sortIndex}"/>
				<c:set var="rowClass" value="row0"/>
				<c:set var="i" value="0"/>
				
				<table width="100%">						
					<table class="tabDataDisplayBorder" width="100%">			
					<thead>
							<tr class="sortHeader">
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "1", (String)pageContext.getAttribute("sortIndex"))%>">Date</a>
									<%=PaginationUtil.getSortingIconHtml("1", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "2", (String)pageContext.getAttribute("sortIndex"))%>">ICCID</a>
									<%=PaginationUtil.getSortingIconHtml("2", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "4", (String)pageContext.getAttribute("sortIndex"))%>">Device Serial<br/>Number</a>
									<%=PaginationUtil.getSortingIconHtml("4", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "5", (String)pageContext.getAttribute("sortIndex"))%>">Monthly Plan<br/>Limit (Kb)</a>
									<%=PaginationUtil.getSortingIconHtml("5", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "6", (String)pageContext.getAttribute("sortIndex"))%>">Total Usage<br/>(Kb)</a>
									<%=PaginationUtil.getSortingIconHtml("6", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								
							</tr>
						</thead>
				
					<c:choose>
						<c:when test="${fn:length(gprsweb_usage_show_all_results) > 0}" >
							<tbody>
							<c:forEach var="iccid_item" items="${gprsweb_usage_show_all_results}">
								<c:choose>
									<c:when test="${i%2 == 0}">
										<c:set var="rowClass" value="row1"/>
									</c:when>
									<c:otherwise>
										<c:set var="rowClass" value="row0"/>
									</c:otherwise>
								</c:choose>
								<c:set var="i" value="${i+1}"/>
								<tr class="${rowClass}">
								    <td nowrap align="left">${iccid_item.report_date }&nbsp;</td>
								    <td align="left"><a href="gprswebUsageDetail.i?iccid=${iccid_item.iccid}">${iccid_item.iccid_formatted}</a></td>
								    <td nowrap align="left"><a href="profile.i?device_id=${iccid_item.device_id }" >${iccid_item.device_serial_cd }</a>&nbsp;</td>	
								    <td nowrap align="right">${iccid_item.included_usage }&nbsp;</td>
									<td nowrap align="right">${iccid_item.total_kb_usage }&nbsp;</td>														    
								</tr>
							</c:forEach>
							
							</tbody>
							</table>
							<input type="hidden" name="storedNames"	value="${storedNames}" />							
									
							    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
							        <jsp:param name="_param_request_url" value="gprswebUsageResults.i?action=Show All" />
							    	<jsp:param name="_param_stored_names" value="${storedNames}" />
							    </jsp:include>
						    
												
						</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="6" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>	    	


				</table>
				
			</c:when>
		</c:choose>
		<c:choose>
			<c:when test="${(action eq 'Show Overage')}">
				<span><b>SIM cards which exceed the usage limit <br/> From ${start_date } to ${end_date }</b></span>
				
				<jsp:useBean id="gprsweb_usage_show_overage_results" type="simple.results.Results" scope="request" />
				<c:set var="sortIndex" value="${sortIndex}"/>
				<c:set var="rowClass" value="row0"/>
				<c:set var="i" value="0"/>
				
				<table width="100%">						
					<table class="tabDataDisplayBorder" width="100%">			
					<thead>
							<tr class="sortHeader">
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "1", (String)pageContext.getAttribute("sortIndex"))%>">ICCID</a>
									<%=PaginationUtil.getSortingIconHtml("1", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "3", (String)pageContext.getAttribute("sortIndex"))%>">Device Serial Number</a>
									<%=PaginationUtil.getSortingIconHtml("3", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "4", (String)pageContext.getAttribute("sortIndex"))%>">Monthly Plan<br/>Limit (Kb)</a>
									<%=PaginationUtil.getSortingIconHtml("4", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "5", (String)pageContext.getAttribute("sortIndex"))%>">Total Usage<br/>(Kb)</a>
									<%=PaginationUtil.getSortingIconHtml("5", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "6", (String)pageContext.getAttribute("sortIndex"))%>">Remains<br/>(Kb)</a>
									<%=PaginationUtil.getSortingIconHtml("6", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "8", (String)pageContext.getAttribute("sortIndex"))%>">Average Daily <br/>Usage (Kb)</a>
									<%=PaginationUtil.getSortingIconHtml("8", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								
							</tr>
						</thead>
				
					<c:choose>
						<c:when test="${fn:length(gprsweb_usage_show_overage_results) > 0}" >
							<tbody>
							<c:forEach var="iccid_item" items="${gprsweb_usage_show_overage_results}">
								<c:choose>
									<c:when test="${i%2 == 0}">
										<c:set var="rowClass" value="row1"/>
									</c:when>
									<c:otherwise>
										<c:set var="rowClass" value="row0"/>
									</c:otherwise>
								</c:choose>
								<c:set var="i" value="${i+1}"/>
								<tr class="${rowClass}">
									<td align="left"><a href="gprswebUsageDetail.i?iccid=${iccid_item.iccid}">${iccid_item.iccid_formatted}</a></td>
									<td nowrap align="left"><a href="profile.i?device_id=${iccid_item.device_id }" >${iccid_item.device_serial_cd }</a>&nbsp;</td>								    	
								    <td nowrap align="right">${iccid_item.included_usage }&nbsp;</td>
									<td nowrap align="right">${iccid_item.total_usage }&nbsp;</td>
									<c:set var="colorCode" value="" />
									<c:forEach var="color_code_item" items="${gprswebUsageCodes}">
										<c:choose>
											<c:when test="${color_code_item.statusCode == iccid_item.usage_state_id}">
												<c:set var="colorCode" value="${color_code_item.colorCode}" />
											</c:when>
										</c:choose>
									</c:forEach>
									<td nowrap align="right" bgcolor="${colorCode }" >${iccid_item.diff }&nbsp;</td>
									<td nowrap align="right">${iccid_item.avg_daily_usage_formatted }&nbsp;</td>																    
								</tr>
							</c:forEach>
							
							</tbody>
							</table>
							<input type="hidden" name="storedNames"	value="${storedNames}" />							
									
							    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
							        <jsp:param name="_param_request_url" value="gprswebUsageResults.i?action=Show Overage" />
							    	<jsp:param name="_param_stored_names" value="${storedNames}" />
							    </jsp:include>
						    
												
						</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="6" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>	    	


				</table>
				
			</c:when>
		</c:choose>
	</c:otherwise>

</c:choose>
<jsp:include page="gprsweb_footer.html" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />