<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.dms.gprsweb.GPRSWEBConstants"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="action" class="java.lang.String" scope="request" />

<jsp:useBean id="gprswebStatusDescColorCodes" type="simple.results.Results" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	<c:otherwise>
	
		<jsp:include page="gprsweb_header.jsp" flush="true" />
		
		<c:choose>
			<c:when  test="${(action eq 'List SIMs')}">
				<c:set var="rowClass" value="row0"/>
				<c:set var="i" value="0"/>
				<span>--SIM Search Results</span>
				<jsp:useBean id="dms_values_list_gprswebICCIDList" type="simple.results.Results" scope="request" />
				<jsp:useBean id="count" class="java.lang.Integer" scope="request" />
					<table class="tabDataDisplayBorder" width="100%">
					<thead>
						<tr><td class="header1" >ICCID</td><td class="header1" >Status</td><td class="header1" >Allocated<br/>To</td><td class="header1" >Billable<br/>To</td><td class="header1" >Rate<br/>Plan</td><td class="header1" >Assigned<br/>Device</td></tr>
					</thead>
					<c:forEach var="iccid_item" items="${dms_values_list_gprswebICCIDList}">
							<c:choose>
								<c:when test="${i%2 == 0}">
									<c:set var="rowClass" value="row1"/>
								</c:when>
								<c:otherwise>
									<c:set var="rowClass" value="row0"/>
								</c:otherwise>
							</c:choose>
							<c:set var="i" value="${i+1}"/>
							<tr class="${rowClass}">
							<td align="right" nowrap><a href="iccidDetail.i?iccid=${iccid_item.iccid}">${iccid_item.iccid_formatted}</a></td>
							<c:set var="colorCode" value="" />
							<c:set var="statusDesc" value="" />
							<c:forEach var="color_code_item" items="${gprswebStatusDescColorCodes}">
								<c:choose>
									<c:when test="${color_code_item.statusCode == iccid_item.gprs_device_state_id}">
										<c:set var="colorCode" value="${color_code_item.colorCode}" />
										<c:set var="statusDesc" value="${color_code_item.statusDesc}" />
									</c:when>
								</c:choose>
							</c:forEach>
							<td nowrap bgcolor="${colorCode }">${statusDesc }&nbsp;
								<c:choose>
									<c:when  test="${(iccid_item.gprs_device_state_id eq '1')}">
										${iccid_item.ordered_ts }
									</c:when>
									<c:when  test="${(iccid_item.gprs_device_state_id eq '2')}">
										${iccid_item.allocated_ts }
									</c:when>
									<c:when  test="${(iccid_item.gprs_device_state_id eq '3')}">
										${iccid_item.activated_ts }
									</c:when>
									<c:when  test="${(iccid_item.gprs_device_state_id eq '4')}">
										${iccid_item.provider_activation_ts }
									</c:when>
									<c:when  test="${(iccid_item.gprs_device_state_id eq '5')}">
										${iccid_item.assigned_ts }
									</c:when>
								</c:choose>
							</td>
							<td nowrap>${iccid_item.allocated_to }&nbsp;</td>
							
							<td nowrap>${iccid_item.billable_to_name }&nbsp;</td>
							<td nowrap>${iccid_item.rate_plan_name }&nbsp;</td>
							<td nowrap><a href="/profile.i?device_id=${iccid_item.device_id }" ></a>&nbsp;</td>
						</tr>
					</c:forEach>	
					
					</table><br/>
					<span class="subhead">${count} rows</span>
							
			</c:when>
		</c:choose>
		<c:choose>
			<c:when test="${(action eq 'Search SIMs')}">
				<span><b>SIM Search Results<%if ("Y".equalsIgnoreCase(request.getParameter("disabled"))) out.write(" (Disabled)");%></b></span>
				<jsp:useBean id="gprsweb_search_results" type="simple.results.Results" scope="request" />
				<c:set var="sortIndex" value="${sortIndex}"/>
				<c:set var="rowClass" value="row0"/>
				<c:set var="i" value="0"/>
				
				<table width="100%">						
					<table class="tabDataDisplayBorder" width="100%">			
					<thead>
							<tr class="sortHeader">
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "1", (String)pageContext.getAttribute("sortIndex"))%>">ICCID</a>
									<%=PaginationUtil.getSortingIconHtml("1", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "2", (String)pageContext.getAttribute("sortIndex"))%>">Status</a>
									<%=PaginationUtil.getSortingIconHtml("2", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "3", (String)pageContext.getAttribute("sortIndex"))%>">Allocated To</a>
									<%=PaginationUtil.getSortingIconHtml("3", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "4", (String)pageContext.getAttribute("sortIndex"))%>">Billable To</a>
									<%=PaginationUtil.getSortingIconHtml("4", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "5", (String)pageContext.getAttribute("sortIndex"))%>">Rate Plan</a>
									<%=PaginationUtil.getSortingIconHtml("5", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "6", (String)pageContext.getAttribute("sortIndex"))%>">Assigned Device</a>
									<%=PaginationUtil.getSortingIconHtml("6", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								
							</tr>
						</thead>
				
					<c:choose>
						<c:when test="${fn:length(gprsweb_search_results) > 0}" >
							<tbody>
							<c:forEach var="iccid_item" items="${gprsweb_search_results}">
								<c:choose>
									<c:when test="${i%2 == 0}">
										<c:set var="rowClass" value="row1"/>
									</c:when>
									<c:otherwise>
										<c:set var="rowClass" value="row0"/>
									</c:otherwise>
								</c:choose>
								<c:set var="i" value="${i+1}"/>
								<tr class="${rowClass}">
								    <td align="right"><a href="iccidDetail.i?iccid=${iccid_item.iccid}">${iccid_item.iccid_formatted}</a></td>
								    <c:set var="colorCode" value="" />
									<c:set var="statusDesc" value="" />
									<c:forEach var="color_code_item" items="${gprswebStatusDescColorCodes}">
										<c:choose>
											<c:when test="${color_code_item.statusCode == iccid_item.gprs_device_state_id}">
												<c:set var="colorCode" value="${color_code_item.colorCode}" />
												<c:set var="statusDesc" value="${color_code_item.statusDesc}" />
											</c:when>
										</c:choose>
									</c:forEach>
								    <td nowrap bgcolor="${colorCode}">${statusDesc }&nbsp;
										<c:choose>
											<c:when  test="${(iccid_item.gprs_device_state_id eq '1')}">
												${iccid_item.ordered_ts }
											</c:when>
											<c:when  test="${(iccid_item.gprs_device_state_id eq '2')}">
												${iccid_item.allocated_ts }
											</c:when>
											<c:when  test="${(iccid_item.gprs_device_state_id eq '3')}">
												${iccid_item.activated_ts }
											</c:when>
											<c:when  test="${(iccid_item.gprs_device_state_id eq '4')}">
												${iccid_item.provider_activation_ts }
											</c:when>
											<c:when  test="${(iccid_item.gprs_device_state_id eq '5')}">
												${iccid_item.assigned_ts }
											</c:when>
										</c:choose>
									</td>
								    
								    <td nowrap>${iccid_item.allocated_to }&nbsp;</td>
									<td nowrap>${iccid_item.billable_to_name }&nbsp;</td>
									<td nowrap>${iccid_item.rate_plan_name }&nbsp;</td>
									<td nowrap><a href="/profile.i?device_id=${iccid_item.device_id }" ></a>&nbsp;</td>						    
								</tr>
							</c:forEach>
							
							</tbody>
							</table>
							
							<%
							String storedNames = PaginationUtil.encodeStoredNames(new String[]{
									GPRSWEBConstants.PARAM_ACTION,
									GPRSWEBConstants.PARAM_ICCID,
									GPRSWEBConstants.PARAM_ICCID_FORMATTED,
									GPRSWEBConstants.PARAM_STATE_ID,
									GPRSWEBConstants.PARAM_BILLABLE_TO,
									GPRSWEBConstants.PARAM_ALLOCATED_TO,
									GPRSWEBConstants.PARAM_RATE_PLAN_NAME,
									GPRSWEBConstants.PARAM_DEVICE_ID,
									GPRSWEBConstants.PARAM_STATUS});
							%>							
									
							    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
							        <jsp:param name="_param_request_url" value="gprswebSearchResults.i?action=Search SIMs" />
							    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
							    </jsp:include>
						    
												
						</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="6" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>	    	


				</table>
				
			</c:when>
		</c:choose>
	</c:otherwise>

</c:choose>
<jsp:include page="gprsweb_footer.html" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />