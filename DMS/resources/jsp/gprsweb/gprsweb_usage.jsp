<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="dms_values_list_gprswebFirstICCID" type="simple.results.Results" scope="request" />

<jsp:useBean id="iccid_label" class="java.lang.String" scope="request" />
<jsp:useBean id="iccid_button_label" class="java.lang.String" scope="request" />
<jsp:useBean id="show_sim_count" class="java.lang.String" scope="request" />
<jsp:useBean id="search_date" class="java.lang.String" scope="request" />
<jsp:useBean id="exceed_month" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
		<jsp:include page="gprsweb_header.jsp" flush="true" />
		
		<span class="label">SIM Usage</span>
		
		<form method="post" action="gprswebUsageResults.i">
			<jsp:include page="gprsweb_search_by_iccid.jsp" flush="true" />
		&nbsp;<br/>
		</form>
		
		&nbsp;<br/>

		<span>Show All Sim Card Usage</span><br/>
		<table align="center" class="tabDataDisplayBorder" width="50%">
		 <form method="post" action="gprswebUsageResults.i">
		  <td>Start Date:</td>
		  <td><input type="text" name="start_date" size="20" value="${search_date }" maxlength="20">
		 </tr>
		 <tr>
		  <td>End Date:</td>
		
		  <td><input type="text" name="end_date" size="20" value="${search_date }" maxlength="20">
		 </tr>
		 <tr>
		  <td>&nbsp;</td>
		  <td><input type="submit" class="cssButton" name="action" value="Show All"></td>
		 </tr>
		 </form>
		</table>
		&nbsp;<br/>
		
		<span>Show Sim Cards Exceeding the Monthly Usage Limit</span><br/>
		<table align="center" class="tabDataDisplayBorder" width="50%">
		 <form method="post" action="gprswebUsageResults.i">
		  <td>Month:</td>
		  <td><input type="text" name="month" size="20" value="${exceed_month }" maxlength="20">
		 </tr>
		 <tr>
		  <td>&nbsp;</td>
		  <td><input type="submit" class="cssButton" name="action" value="Show Overage"></td>
		
		 </tr>
		 </form>
		</table>
		&nbsp;<br/>
		
		
	</c:otherwise>

</c:choose>
<jsp:include page="gprsweb_footer.html" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />