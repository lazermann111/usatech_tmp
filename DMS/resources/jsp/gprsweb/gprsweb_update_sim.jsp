<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String error = inputForm.getStringSafely("error", "");
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:include page="gprsweb_header.jsp" flush="true" />

<%if (!StringHelper.isBlank(error)) {%>
<span class="status-error"><%=error%></span>
<%}%>

<form method="post"  onsubmit="return doSubmit();">
<span class="label">Update SIMs</span>

<table class="padding3">
	<tr>
		<td class="label" valign="top">ICCIDs<font color="red">*</font><br />(1 per line)</td>
		<td><textarea name="item_list" id="item_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("item_list", "")%></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="submit" name="action" value="Enable" class="cssButton" />
			<input type="submit" name="action" value="Disable" class="cssButton" />
		</td>
	</tr>
</table>
</form>
		
<jsp:include page="gprsweb_footer.html" flush="true" />
<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
function doSubmit(){	
	if(document.getElementById("item_list").value.trim() == ''){
		alert('Please enter ICCIDs');
		return false;
	}
}
</script>