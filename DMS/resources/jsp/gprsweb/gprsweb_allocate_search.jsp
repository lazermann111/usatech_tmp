<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="dms_values_list_gprswebFirstICCID" type="simple.results.Results" scope="request" />

<jsp:useBean id="iccid_label" class="java.lang.String" scope="request" />
<jsp:useBean id="iccid_button_label" class="java.lang.String" scope="request" />
<jsp:useBean id="show_sim_count" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
		<jsp:include page="gprsweb_header.jsp" flush="true" />
		
		<span class="label">Allocate SIMs</span>
		
		<form method="post" action="gprswebAllocateSearchResults.i">
			<jsp:include page="gprsweb_search_by_iccid.jsp" flush="true" />
		&nbsp;<br/>
		</form>
	</c:otherwise>

</c:choose>
<jsp:include page="gprsweb_footer.html" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />