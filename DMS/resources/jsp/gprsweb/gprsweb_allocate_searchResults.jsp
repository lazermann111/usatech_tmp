<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 


<jsp:useBean id="iccid_first" class="java.lang.String" scope="request" />
<jsp:useBean id="iccid_last" class="java.lang.String" scope="request" />
<jsp:useBean id="count" class="java.lang.Integer" scope="request" />

<jsp:useBean id="gprswebStatusDescColorCodes" type="simple.results.Results" scope="request" />
<jsp:useBean id="dms_values_list_gprswebAllocateICCIDList" type="simple.results.Results" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	<c:otherwise>
	
		<jsp:include page="gprsweb_header.jsp" flush="true" />
		
			<c:choose>
				<c:when test="${fn:length(dms_values_list_gprswebAllocateICCIDList) > 0}">
					<c:set var="rowClass" value="row0"/>
					<c:set var="i" value="0"/>
					<span>${iccid_first} a ${iccid_last} a ${count} Allocate Search Results</span>
						<form method="post" action="gprswebAllocateFunc.i">
							<table class="tabDataDisplayBorder" width="100%">
							
							<thead>
								<tr><td class="header1" >Include?</td><td class="header1" >ICCID</td><td class="header1" >Status</td><td class="header1" >Ordered<br/>Date</td><td class="header1" >Ordered<br/>By</td><td class="header1" >Order<br/>ID</td></tr>
							</thead>
							<c:forEach var="iccid_item" items="${dms_values_list_gprswebAllocateICCIDList}">
								<c:choose>
								<c:when test="${i%2 == 0}">
									<c:set var="rowClass" value="row1"/>
								</c:when>
								<c:otherwise>
									<c:set var="rowClass" value="row0"/>
								</c:otherwise>
								</c:choose>
								<c:set var="i" value="${i+1}"/>
								<tr class="${rowClass}">
									<td align="center">
										<c:choose>
											<c:when test="${iccid_item.gprs_device_state_id == 1}">
												<input type="checkbox" name="iccid" value="${iccid_item.iccid}" checked>
											</c:when>
											<c:otherwise>&nbsp;</c:otherwise>
										</c:choose>
									</td>
									<td align="right" nowrap><a href="iccidDetail.i?iccid=${iccid_item.iccid}">${iccid_item.iccid_formatted}</a></td>
									<c:set var="colorCode" value="" />
									<c:set var="statusDesc" value="" />
									<c:forEach var="color_code_item" items="${gprswebStatusDescColorCodes}">
										<c:choose>
											<c:when test="${color_code_item.statusCode == iccid_item.gprs_device_state_id}">
												<c:set var="colorCode" value="${color_code_item.colorCode}" />
												<c:set var="statusDesc" value="${color_code_item.statusDesc}" />
											</c:when>
										</c:choose>
									</c:forEach>
									<td nowrap bgcolor="${colorCode }">${statusDesc }&nbsp;</td>								
									<td nowrap>${iccid_item.ordered_by }&nbsp;</td>
									<td nowrap>${iccid_item.ordered_ts }&nbsp;</td>
									<td><a href="https://eod.wireless.att.com/USA/view_sim_order.asp?order_id=${iccid_item.provider_order_id }">${iccid_item.provider_order_id }</a>&nbsp;</td>
								</tr>
							</c:forEach>	
							
							</table><br/>
							<div align="center">								
							<table class="tabDataDisplayBorderNoFixedLayout" width="790">
								<tr><td>Allocated By: </td><td><input type="text" name="allocated_by" size="40"></td></tr>
								
								<tr><td>Allocated To: </td><td><input type="text" name="allocated_to" size="40"></td></tr>
								<tr><td>Allocated Notes: </td><td><textarea cols="60" rows="5" name="allocated_notes"></textarea></td></tr>
								<tr><td>Billable To: </td><td><select name="billable_to"><option value="USAT">USAT</option><option value="MEI">MEI</option></select></td></tr>
								<tr><td>Billable Notes: </td><td><textarea cols="60" rows="5" name="billable_notes"></textarea></td></tr>
								<tr><td>&nbsp;</td><td><input type="submit" class="cssButton" value="Allocate"></td></tr>
								
								</table>
							</div>
						</form>
						
						
			</c:when>
			<c:otherwise>
				<span>No Records Found!</span>
			</c:otherwise>
			
		</c:choose>
</c:otherwise>

</c:choose>
<jsp:include page="gprsweb_footer.html" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />