<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/gprsweb/gprsweb_menu.js") %>"></script>
<jsp:useBean id="dms_values_list_gprswebRatePlan" type="simple.results.Results" scope="request" />
<jsp:useBean id="dms_values_list_gprswebStatus" type="simple.results.Results" scope="request" />
<jsp:useBean id="dms_values_list_gprswebBillableToName" type="simple.results.Results" scope="request" />
<jsp:useBean id="dms_values_list_gprswebAllocatedToName" type="simple.results.Results" scope="request" />
<jsp:useBean id="dms_values_list_gprswebFirstICCID" type="simple.results.Results" scope="request" />

<jsp:useBean id="iccid_label" class="java.lang.String" scope="request" />
<jsp:useBean id="iccid_button_label" class="java.lang.String" scope="request" />
<jsp:useBean id="show_sim_count" class="java.lang.String" scope="request" />

<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>	
	
		<jsp:include page="gprsweb_header.jsp" flush="true" />
		
		<span class="label">SIM Search</span>
		
		<form method="post" action="gprswebSearchResults.i">
			<jsp:include page="gprsweb_search_by_iccid.jsp" flush="true" />
		&nbsp;<br/>
		</form>
		<form method="post" action="gprswebSearchResults.i">
			<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
			 <tr>
			  <td>Status</td>
			  <td>
			  
			  	<select name="status" tabindex="2" style="font-family: courier; font-size: 12px;">
		     		<option value="">Any</option>
					<c:forEach var="nvp_item_gprswebStatus" items="${dms_values_list_gprswebStatus}">
							<option value="${nvp_item_gprswebStatus.aValue}">${nvp_item_gprswebStatus.aLabel}</option>	    	
					</c:forEach>	
				</select>
				
				</td>
		 	</tr>
			 <tr>
			
			  <td>Billable To</td>
			  <td>
			  
			  	<select name="billable_to_name" tabindex="3" style="font-family: courier; font-size: 12px;">
		     		<option value="">Any</option>
					<c:forEach var="nvp_item_gprswebBillableToName" items="${dms_values_list_gprswebBillableToName}">
							<option value="${nvp_item_gprswebBillableToName.aValue}">${nvp_item_gprswebBillableToName.aLabel}</option>	    	
					</c:forEach>	
				</select>
			  
			  </td>
			
			 </tr>
			 <tr>
			  <td>Allocated To</td>
			  <td>
			  
			  	<select name="allocated_to" tabindex="4" style="font-family: courier; font-size: 12px;">
		     		<option value="">Any</option>
					<c:forEach var="nvp_item_gprswebAllocatedToName" items="${dms_values_list_gprswebAllocatedToName}">
							<option value="${nvp_item_gprswebAllocatedToName.aValue}">${nvp_item_gprswebAllocatedToName.aLabel}</option>	    	
					</c:forEach>	
				</select>
			  
			  </td>
			 </tr>
			 <tr>
			  <td>Rate Plan</td>
			  <td>
			  
			  <select name="rate_plan_name" tabindex="5" style="font-family: courier; font-size: 12px;">
		     		<option value="">Any</option>
					<c:forEach var="nvp_item_gprswebRatePlan" items="${dms_values_list_gprswebRatePlan}">
							<option value="${nvp_item_gprswebRatePlan.aValue}">${nvp_item_gprswebRatePlan.aLabel}</option>	    	
					</c:forEach>	
				</select>			
				
			  </td>
			 </tr>
			 <tr>
			
			  <td>&nbsp;</td>
			  <td><input type="submit" class="cssButton" name="action" value="Search SIMs"></td>
			 </tr>
		</table>
		</form>
		&nbsp;<br/>
		
			<span class="subhead"><b>Go To SIM</b></span><br/>
			<form method="get" action="iccidDetail.i" name="iccidDetailForm">
			<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
			 
			 <tr>
			
			  <td>Full ICCID: </td>
			  <td>
			   <input type="text" name="iccid" size="20" maxlength="20">
			   <input type="button" class="cssButton" name="action" value="Go To"
			   		onclick="if(!iccidDetailForm.iccid.value.match('\\d{20}')){ alert('Full ICCID must contain 20 digits!');}else{iccidDetailForm.submit();}">
			  </td>
			 </tr>
			
			</table>
			</form>
	</c:otherwise>

</c:choose>
<jsp:include page="gprsweb_footer.html" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />