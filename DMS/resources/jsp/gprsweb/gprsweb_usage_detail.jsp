<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="iccid" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<jsp:include page="gprsweb_header.jsp" flush="true" />

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
		
		<jsp:useBean id="gprswebUsageCodes" type="simple.results.Results" scope="request" />
		<jsp:useBean id="gprswebUsageDeviceLink" type="simple.results.Results" scope="request" />
		<jsp:useBean id="gprswebUsageMonthlySummary" type="simple.results.Results" scope="request" />
		<jsp:useBean id="gprswebUsageDailyReport" type="simple.results.Results" scope="request" />
		<jsp:useBean id="gprswebUsageDailyReportLastMonth" type="simple.results.Results" scope="request" />
		<c:set var="iccid_formatted" value="" />
		<c:set var="device_serial_cd" value="Device Not Assigned" />
		<c:set var="device_id" value="" />
		
		<c:choose>
			<c:when test="${fn:length(gprswebUsageDeviceLink) > 0}">
				<c:set var="iccid_formatted" value="${gprswebUsageDeviceLink[0].iccid_formatted}" />
				<c:choose>
					<c:when test="${not empty(gprswebUsageDeviceLink[0].device_serial_cd)}">
						<c:set var="device_serial_cd" value="${gprswebUsageDeviceLink[0].device_serial_cd}" />
					</c:when>
				</c:choose>
				<c:set var="device_id" value="${gprswebUsageDeviceLink[0].device_id}" />
				
			</c:when>
		</c:choose>
		<span><a href="iccidDetail.i?iccid=${iccid}">Iccid:${iccid_formatted }</a></span><br/>
		<span><a href="profile.i?device_id=${device_id }">Device Serial Number: ${device_serial_cd }</a>&nbsp;</span><br/>
		
		<span>&nbsp;</span><br/>
		<span>Recent Monthly Usage Summary</span><br/>
		<table width="100%" class="tabDataDisplayBorder">
			<thead>
				<tr><td class="header1">Date</td><td class="header1">Monthly Plan <br/> Limit (Kb)</td><td class="header1">Total Usage <br/> (Kb)</td><td class="header1">Remains <br/> (Kb)</td><td class="header1">Average Daily <br/> Usage (Kb)</td></tr>
			</thead>
			<c:choose>
				<c:when test="${fn:length(gprswebUsageMonthlySummary) > 0}">
					<c:forEach var="line_item" items="${gprswebUsageMonthlySummary}">
					   <tr>
						<td nowrap>${line_item.report_date}&nbsp;</td>
				        <td nowrap>${line_item.included_usage}&nbsp;</td>
				        <td nowrap>${line_item.total_usage}&nbsp;</td>
				        <c:set var="usage_desc" value="" />
						<c:set var="color_code" value="" />
				        <c:forEach var="usage_code_item" items="${gprswebUsageCodes}">
				        	<c:choose>
				        		<c:when test="${line_item.state_id == usage_code_item.usageCode}">
				        			<c:set var="usage_desc" value="${usage_code_item.usageDesc}" />
									<c:set var="color_code" value="${usage_code_item.colorCode}" />
				        		</c:when>
				        	</c:choose>
				        </c:forEach>
				        <td nowrap bgcolor="${color_code }">${line_item.diff}</td>
				       <td nowrap>${line_item.avg_daily_usage }&nbsp;</td>
				      </tr>
					
					</c:forEach>
				</c:when>
			</c:choose>
			
		</table>
		<span>&nbsp;</span><br/>
		<span>Daily Usage For This Month</span><br/>
		<table width="100%" class="tabDataDisplayBorder">
			<thead>
				<tr><td class="header1">Date</td><td class="header1">Monthly Plan Limit (Kb)</td><td class="header1">Total Usage (Kb)</td><td class="header1">Remains (Kb)</td></tr>
			</thead>
			<c:choose>
				<c:when test="${fn:length(gprswebUsageDailyReport) > 0}">
					
					<c:forEach var="line_item" items="${gprswebUsageDailyReport}">
						<c:set var="remains" value="${line_item.included_usage}" />
						<c:set var="color_code" value="" />
						<c:set var="color_code_normal" value="" />
						<c:set var="color_code_overused" value="" />
						<c:forEach var="usage_code_item" items="${gprswebUsageCodes}">
				        	<c:choose>
				        		<c:when test="${1 == usage_code_item.usageCode}">
									<c:set var="color_code_normal" value="${usage_code_item.colorCode}" />
				        		</c:when>
				        		<c:when test="${2 == usage_code_item.usageCode}">
									<c:set var="color_code_overused" value="${usage_code_item.colorCode}" />
				        		</c:when>
				        	</c:choose>
		        		</c:forEach>
						<c:choose>
							<c:when test="${not empty(line_item.included_usage)}">
								<c:set var="remains" value="${line_item.included_usage - line_item.total_kb_usage}" />
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${remains < 0}">
								<c:set var="color_code" value="${color_code_normal}" />
							</c:when>
							<c:otherwise>
								<c:set var="color_code" value="${color_code_overused}" />
							</c:otherwise>
						</c:choose>
					   <tr>
						<td nowrap>${line_item.report_date}&nbsp;</td>
				        <td nowrap>${line_item.included_usage}&nbsp;</td>
				        <td nowrap>${line_item.total_kb_usage}&nbsp;</td>
				        <td nowrap bgcolor="${color_code }">${remains}</td>
				      </tr>
					
					</c:forEach>
				</c:when>
			</c:choose>
		
		</table>
		<span>&nbsp;</span><br/>
		<span>Daily Usage For Last Month</span><br/>
		<table width="100%" class="tabDataDisplayBorder">
			<thead>
				<tr><td class="header1">Date</td><td class="header1">Monthly Plan Limit (Kb)</td><td class="header1">Total Usage (Kb)</td><td class="header1">Remains (Kb)</td></tr>
			</thead>
			<c:choose>
				<c:when test="${fn:length(gprswebUsageDailyReportLastMonth) > 0}">
					
					<c:forEach var="line_item" items="${gprswebUsageDailyReportLastMonth}">
						<c:set var="remains" value="${line_item.included_usage}" />
						<c:set var="color_code" value="" />
						<c:set var="color_code_normal" value="" />
						<c:set var="color_code_overused" value="" />
						<c:forEach var="usage_code_item" items="${gprswebUsageCodes}">
				        	<c:choose>
				        		<c:when test="${1 == usage_code_item.usageCode}">
									<c:set var="color_code_normal" value="${usage_code_item.colorCode}" />
				        		</c:when>
				        		<c:when test="${2 == usage_code_item.usageCode}">
									<c:set var="color_code_overused" value="${usage_code_item.colorCode}" />
				        		</c:when>
				        	</c:choose>
		        		</c:forEach>
						<c:choose>
							<c:when test="${not empty(line_item.included_usage)}">
								<c:set var="remains" value="${line_item.included_usage - line_item.total_kb_usage}" />
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${remains < 0}">
								<c:set var="color_code" value="${color_code_normal}" />
							</c:when>
							<c:otherwise>
								<c:set var="color_code" value="${color_code_overused}" />
							</c:otherwise>
						</c:choose>
					   <tr>
						<td nowrap>${line_item.report_date}&nbsp;</td>
				        <td nowrap>${line_item.included_usage}&nbsp;</td>
				        <td nowrap>${line_item.total_kb_usage}&nbsp;</td>
				        <td nowrap bgcolor="${color_code }">${remains}</td>
				      </tr>
					
					</c:forEach>
				</c:when>
			</c:choose>
		</table>

		
		
	</c:otherwise>

</c:choose>
<jsp:include page="gprsweb_footer.html" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />