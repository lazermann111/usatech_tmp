<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.dms.gprsweb.GPRSWEBConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="java.util.Map"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="gprswebIccidHistDetail" type="simple.results.Results" scope="request" />
<jsp:useBean id="gprswebStatusDescColorCodes" type="simple.results.Results" scope="request" />

<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
	
	<script>
		function setInput(val) {
		   if(val == 'allocated_by_id') {
		      document.getElementById(val).innerHTML = '<input type="text" name="allocated_by" size="40">';
		   }
		   if(val == 'allocated_to_id') {
		      document.getElementById(val).innerHTML = '<input type="text" name="allocated_to" size="40">';
		   }
		   if(val == 'activated_by_id') {
		      document.getElementById(val).innerHTML = '<input type="text" name="activated_by" size="40">';
		   }
		}
	</script>
	
		<jsp:include page="gprsweb_header.jsp" flush="true" />		
		
		<div class="tabHead"><span >SIM Card Information:${gprswebIccidHistDetail[0].iccid }</span><br/></div>
		
		<table class="tabDataDisplayBorderNoFixedLayout" width="100%" >
		
		<tr><td>GPRS Device ID</td><td>${gprswebIccidHistDetail[0].GPRS_DEVICE_ID }</td><td>ICCID</td><td><a href="https://eod.wireless.att.com/USA/history.asp?open=11&cat=Service&page=Sim%20History%20/%20Status&iccid=${gprswebIccidHistDetail[0].iccid }" 
			target="_cingular">${gprswebIccidHistDetail[0].iccid_formatted }</a></td></tr>
			
			<c:set var="colorCode" value="" />
			<c:set var="statusDesc" value="" />
			<c:forEach var="color_code_item" items="${gprswebStatusDescColorCodes}">
				<c:choose>
					<c:when test="${color_code_item.statusCode == gprswebIccidHistDetail[0].gprs_device_state_id}">
						<c:set var="colorCode" value="${color_code_item.colorCode}" />
						<c:set var="statusDesc" value="${color_code_item.statusDesc}" />
					</c:when>
				</c:choose>
			</c:forEach>
		<tr><td>Status</td><td bgcolor="${colorCode }">${statusDesc }</td><td>IMSI</td><td>${gprswebIccidHistDetail[0].IMSI }&nbsp;</td></tr>
		</table>
		&nbsp;
		<br/><div class="tabHead"><span >PIN/PUK Information</span><br/></div>
		<table class="tabDataDisplayBorderNoFixedLayout" width="100%" >
		<tr><td>Pin1</td><td>Puk1</td><td>Pin2</td><td>Puk2</td></tr>
		
		<tr><td>${gprswebIccidHistDetail[0].PIN1 }&nbsp;</td><td>${gprswebIccidHistDetail[0].PUK1 }&nbsp;</td><td>${gprswebIccidHistDetail[0].PIN2 }&nbsp;</td><td>${gprswebIccidHistDetail[0].PUK2 }&nbsp;</td></tr>
		</table>
		&nbsp;
		<br/><div class="tabHead"><span >Order Information</span><br/></div>
		<table class="tabDataDisplayBorderNoFixedLayout"  width="100%">
		<tr><td>Ordered By</td><td>Date Ordered</td><td>Cingular Order ID</td></tr>
		<tr><td>${gprswebIccidHistDetail[0].ORDERED_BY }&nbsp;</td><td>${gprswebIccidHistDetail[0].ORDERED_TS }&nbsp;</td><td><a href="https://eod.wireless.att.com/USA/view_sim_order.asp?order_id=${gprswebIccidHistDetail[0].PROVIDER_ORDER_ID }" target="_cingular">${gprswebIccidHistDetail[0].PROVIDER_ORDER_ID }</a>&nbsp;</td></tr>
		<tr><td colspan="3">Order Notes: </td></tr>
		</table>
		
		&nbsp;
		<br/><div class="tabHead"><span >Allocation Information</span><br/></div>
		<table class="tabDataDisplayBorderNoFixedLayout"  width="100%" >
		<tr><td>Allocated By</td><td>Date Allocated</td><td>Allocated To</td></tr>
		<tr><td>${gprswebIccidHistDetail[0].ALLOCATED_BY }&nbsp;</td><td>${gprswebIccidHistDetail[0].ALLOCATED_TS }&nbsp;</td><td>${gprswebIccidHistDetail[0].ALLOCATED_TO }&nbsp;</td></tr>
		<tr><td colspan="3">Allocation Notes: ${gprswebIccidHistDetail[0].ALLOCATED_NOTES }&nbsp;</td></tr>
		<tr><td>Billable to</td><td colspan="2">${gprswebIccidHistDetail[0].BILLABLE_TO_NAME }&nbsp;</td></tr>
		
		<tr><td colspan="3">Billable Notes: ${gprswebIccidHistDetail[0].BILLABLE_TO_NOTES }&nbsp;</td></tr>
		</table>
		&nbsp;
		<br/><div class="tabHead"><span >Activation Information</span><br/></div>
		<table class="tabDataDisplayBorderNoFixedLayout"   width="100%" >
		<tr><td>Activated By</td><td>Date Activated</td></tr>
		<tr><td>${gprswebIccidHistDetail[0].ACTIVATED_BY }&nbsp;</td><td>${gprswebIccidHistDetail[0].ACTIVATED_TS }&nbsp;</td></tr>
		<tr><td colspan="2">Activation Notes: ${gprswebIccidHistDetail[0].ACTIVATED_NOTES }&nbsp;</td></tr>
		<tr><td>Cingular Activation ID</td><td><a href="https://eod.wireless.att.com/USA/batch_results.asp?batch=${gprswebIccidHistDetail[0].PROVIDER_ACTIVATION_ID }" target="_cingular"></a>${gprswebIccidHistDetail[0].PROVIDER_ACTIVATION_ID }&nbsp;</td></tr>
		
		<tr><td>Cingular Activation Date</td><td>${gprswebIccidHistDetail[0].PROVIDER_ACTIVATION_TS }&nbsp;</td></tr>
		<tr><td>MSISDN</td><td>${gprswebIccidHistDetail[0].MSISDN }&nbsp;</td></tr>
		<tr><td>Phone Number</td><td>${gprswebIccidHistDetail[0].PHONE_NUMBER }&nbsp;</td></tr>
		<tr><td>Rate Plan</td><td><a href="https://eod.wireless.att.com/USA/view_rate_plans.asp?open=11&cat=Service&page=View%20Rate%20Plans" target="_cingular">${gprswebIccidHistDetail[0].RATE_PLAN_NAME }</a>&nbsp;</td></tr>
		</table>
		&nbsp;
		<br/><div class="tabHead"><span >Assignment Information</span><br/></div>
		<table  class="tabDataDisplayBorderNoFixedLayout"   width="100%" >
		<tr><td>Assigned By</td><td>Date Assigned</td><td>Assigned To Device</td></tr>
		
		<tr><td>${gprswebIccidHistDetail[0].ASSIGNED_BY }&nbsp;</td><td>${gprswebIccidHistDetail[0].ASSIGNED_TS }&nbsp;</td><td><a href="profile.i?device_id=${gprswebIccidHistDetail[0].DEVICE_ID }">${gprswebIccidHistDetail[0].DEVICE_ID }</a>&nbsp;</td></tr>
		<tr><td colspan="3">Assignment Notes: ${gprswebIccidHistDetail[0].ASSIGNED_NOTES }&nbsp;</td></tr>
		</table>
		&nbsp;
		<br/><div class="tabHead"><span >Modem Information</span><br/></div>
		<table  class="tabDataDisplayBorderNoFixedLayout"  width="100%" >
		<tr><td>IMEI</td><td>${gprswebIccidHistDetail[0].IMEI }&nbsp;</td></tr>
		<tr><td>Modem Type</td><td>${gprswebIccidHistDetail[0].DEVICE_TYPE_NAME }&nbsp;</td></tr>
		
		<tr><td>Modem Firmware</td><td>${gprswebIccidHistDetail[0].DEVICE_FIRMWARE_NAME }&nbsp;</td></tr>
		<tr><td>Last RSSI</td><td>${gprswebIccidHistDetail[0].RSSI }&nbsp;</td></tr>
		
		<tr><td>Last RSSI Timestamp</td><td>${gprswebIccidHistDetail[0].RSSI_TS }&nbsp;</td></tr>
		<tr><td>Raw Modem Data</td><td><pre>${gprswebIccidHistDetail[0].MODEM_INFO }</pre></td></tr>		
		
		</table>
		
	</c:otherwise>

</c:choose>
<jsp:include page="gprsweb_footer.html" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />