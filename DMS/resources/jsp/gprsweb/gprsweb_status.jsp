<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:include page="gprsweb_header.jsp" flush="true" />

<% 
Results gprswebStatusTotals = RequestUtils.getAttribute(request, "gprswebStatusTotals", Results.class, true);
%>

	<table class="tabDataDisplayBorder" width="100%">
	<thead>
		<tr>
		<td class="header1">Status</td>
		<td class="header1">Blue</td>
		<td class="header1">Orange</td>
		<td class="header1">Total</td>
		</tr>
	</thead>
	<%while(gprswebStatusTotals.next()){ %>	
		<tr>
			<td bgcolor='<%=gprswebStatusTotals.getValue("color") %>'><%=gprswebStatusTotals.getValue("desc") %></td>
			<td  bgcolor='lightblue'><%=gprswebStatusTotals.getValue("blue") %></td>
			<td  bgcolor='orange'><%=gprswebStatusTotals.getValue("orange") %></td>
			<td><%=gprswebStatusTotals.getValue("total") %></td>
		</tr>
	<%} %>
	</table>

<jsp:include page="gprsweb_footer.html" flush="true" />
<jsp:include page="/jsp/include/footer.jsp" flush="true" />