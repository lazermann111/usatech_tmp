<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:useBean id="iccid_label" class="java.lang.String" scope="request" />
<jsp:useBean id="iccid_button_label" class="java.lang.String" scope="request" />
<jsp:useBean id="show_sim_count" class="java.lang.String" scope="request" />

<jsp:useBean id="dms_values_list_gprswebFirstICCID" type="simple.results.Results" scope="request" />

		<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
		 <tr>
		  <td><b>${iccid_label }:</b></td>
		  <td>
		   <table width="100%">
		    <tr>
		     <td><b>First 16 digits</b></td>
		
		     <td><b>Last 4 digits</b></td>
		     <c:choose>
			 	<c:when test="${show_sim_count eq 'Y'}">
					 <td><b>From :</b></td>
		     		 <td><input type="text" name="frICCID" size="25" maxlength="20" id="33" ></td>
				 </c:when>
				 <c:otherwise>
				 	 <td><b>Full ICCID</b></td>
				 </c:otherwise>
		 	</c:choose>
		     
		    </tr>
		    <tr>
		     <td>
		     
		     	<select name="iccid_first" tabindex="1" style="font-family: courier; font-size: 12px;" delimiter=" ">
					<c:forEach var="nvp_item_gprswebFirstICCID" items="${dms_values_list_gprswebFirstICCID}">
							<option value="${nvp_item_gprswebFirstICCID.aValue}">${nvp_item_gprswebFirstICCID.aLabel}</option>	    	
					</c:forEach>	
				</select>
				
		     </td>
		     <td>
		      	<input type="text" name="iccid_last" size="7" maxlength="4" id="iccid_last_id">
		     </td>
		     <c:choose>
			 	<c:when test="${show_sim_count eq 'Y'}">
					  <td><b>To :</b></td>
		    		  <td><input type="text" name="toICCID" size="25" maxlength="20" id="34"></td>
				 </c:when>
				 <c:otherwise>
				 	 <td><input type="text" name="iccid_full" size="25" maxlength="20"></td>
				 </c:otherwise>
		 	</c:choose>
		    
		    </tr>
		   </table>
		
		  </td>
		 </tr>
		 <c:choose>
		 	<c:when test="${show_sim_count eq 'Y'}">
				 <tr>
				  <td><b>SIM Count: </b></td>
				  <td><input type="text" name="count" size="4" maxlength="4" id="iccid_count_id"></td>
				 </tr>
				 <tr>
			 </c:when>
		 </c:choose>
		  <td>&nbsp;</td>
		
		  <td><input type="submit" class="cssButton" name="action" value="${iccid_button_label }"></td>
		 </tr>
		</table>