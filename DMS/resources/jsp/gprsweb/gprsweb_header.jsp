<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/gprsweb/gprsweb_menu.js") %>"></script>

  <div class="tableContainer">
  	<div class="tableHead">
    <div class="tabHeadTxt">SIM Card Administration</div>
    </div>

	<div align="center">
  
	<div id="gprsweb_menu">
	<table class="gprsweb_menu">
			<tr>
				<td class="TipsGPRS" id="gprsweb_homeElem" ></td> 
				<td class="TipsGPRS" id="gprsweb_registerElem"></td> 
				<td class="TipsGPRS" id="gprsweb_searchElem"><a href="gprswebSearch.i">Search</a></td> 
				<td class="TipsGPRS" id="gprsweb_allocateElem"><a href="gprswebAllocateSearch.i">Allocate</a></td>
				<td class="TipsGPRS" id="gprsweb_updateElem"><a href="gprswebUpdateSIM.i">Update</a></td> 
				<td class="TipsGPRS" id="gprsweb_usageElem"><a href="gprswebUsage.i">Usage</a></td>
				<td class="TipsGPRS" id="gprsweb_usageElem"></td>
				<td class="TipsGPRS" id="gprsweb_activateElem"></td> 
			</tr>
			<tr>
				<td class="TipsGPRS" id="gprsweb_statusElem"><a href="gprswebStatus.i">Status</a></td> 
				<td class="TipsGPRS" id="gprsweb_allElem"><a href="gprswebSearchResults.i?status=&action=Search+SIMs">All</a></td> 
				<td class="TipsGPRS" id="gprsweb_registeredElem" bgcolor="#9999FF"><a href="gprswebSearchResults.i?status=1&action=Search+SIMs">Registered</a></td> 
				<td class="TipsGPRS" id="gprsweb_allocatedElem" bgcolor="#99CCCC"><a href="gprswebSearchResults.i?status=2&action=Search+SIMs">Allocated</a></td> 
				<td class="TipsGPRS" id="gprsweb_activatedElem" bgcolor="#99FF99"><a href="gprswebSearchResults.i?status=4&action=Search+SIMs">Activated</a></td> 
				<td class="TipsGPRS" id="gprsweb_pendingElem" bgcolor="#FFFF99"><a href="gprswebSearchResults.i?status=3&action=Search+SIMs">Pending</a></td> 
				<td class="TipsGPRS" id="gprsweb_assignedElem" bgcolor="orange"><a href="gprswebSearchResults.i?status=5&action=Search+SIMs">Assigned</a></td>
				<td class="TipsGPRS" id="gprsweb_assignedElem" bgcolor="#FF6666"><a href="gprswebSearchResults.i?status=&action=Search+SIMs&disabled=Y">Disabled</a></td>
			</tr>
	</table>
	</div>
	<br />  
  
  <table width="100%">

   <tr>
   <td align="center" colspan="3">