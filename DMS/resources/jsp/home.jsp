<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="include/header.jsp" flush="true" />

<div class="formContainer">
<div class="tableHead">
<div class="tabHeadTxt">USA Technologies Device Management System</div>
</div>
<div class="tableContent">
<table class="tabDataDisplayBorder">
	<tr class="gridHeader">
		<td colspan="2" align="center">Quick Links</td>
	</tr>
	<tr>
		<td width="50%">&raquo; <a href="http://www.usatech.com/" target="_blank">USA Technologies Public Website</a></td>
		<td width="50%">&raquo; <a href="https://usalive.usatech.com/" target="_blank">USALive Customer Reporting</a></td>
	</tr>
	<tr>
		<td width="50%">&raquo; <a href="https://getmore.usatech.com/" target="_blank">USAT MORE Website</a></td>
		<td width="50%">&raquo; <a href="https://knowledgebase.usatech.com/" target="_blank">USAT Knowledgebase</a></td>
	</tr>
</table>

<div class="spacer10"></div>
</div>
</div>

<jsp:include page="include/footer.jsp" flush="true" />