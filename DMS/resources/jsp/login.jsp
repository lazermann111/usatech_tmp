<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>

<jsp:include page="include/header.jsp" flush="true" />

<div class="smallFormContainer">

    <div class="tableHead">
        <div class="tabHeadTxt">DMS Login</div>
	</div>

	<div class="tableContent">
<%
Throwable exception = (Throwable)request.getAttribute("simple.servlet.SimpleAction.exception");
if (exception != null && exception.getMessage() != null) {
%>
<br /><center><span class="message error"><%=exception.getMessage()%></span></center><br />
<%
}

String servletPath = request.getServletPath();
if ("/getData.i".equalsIgnoreCase(servletPath)) {
%>
<script type="text/javascript">
window.location = "/login.i";
</script>
<%} else {

String forward = null;
InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
if (form != null) {
	forward = (String) form.getAttribute(SimpleServlet.ATTRIBUTE_REQUEST_URI_QUERY);
	if (forward == null || forward.length() == 0 || AuthenticationStep.LOGIN_FORWARD_REGEX.matcher(forward).matches()) {
		forward = (String) form.getAttribute(AuthenticationStep.ATTRIBUTE_FORWARD);
		// forward is blank or a login/logout value; use search path
		if (forward == null || forward.length() == 0 || AuthenticationStep.LOGIN_FORWARD_REGEX.matcher(forward).matches())
	    	forward = (String) form.getAttribute(SimpleServlet.ATTRIBUTE_SEARCH_PATH);
	}
}
if (forward == null)
	forward = "";
else {
	int pos = forward.indexOf("&msg=");
	if (pos > -1)
		 forward = forward.substring(0, pos);
}
%>

    <form name="formLogon" id="formLogon" method="post" action="logon.i" autocomplete="off">
    	<input type="hidden" name="<%=AuthenticationStep.ATTRIBUTE_FORWARD%>" value="<%=StringUtils.encodeForHTMLAttribute(forward)%>" />
    	<input type="hidden" id="clientHeight" name="clientHeight" />
        <div class="spacer10"></div>
        <table align="center">
        <tr>
            <td>Username</td>
            <td><input type="text" id="userName" name="<%=AuthenticationStep.PARAM_USERNAME%>" class="credential" title="Username" tabindex="1" /></td>
		</tr>
		<tr>
			<td>Password</td> 
			<td><input type="password" name="<%=AuthenticationStep.PARAM_CREDENTIAL%>" class="credential" title="Password" tabindex="2" /></td>
		</tr>
		<tr>
			<td colspan="2" class="buttonContainer" align="center">
				<div class="spacer5"></div>
				<input type="submit" class="cssButton" value="Login" tabindex="3" />
				&nbsp;<input type="reset" class="cssButton" value="Clear" tabindex="4" />
			</td>
		</tr>
		</table>
	</form>

	<div class="spacer10"></div>
	</div>
</div>

<div class="spacer10"></div>

<jsp:include page="include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
<!--
try {
	document.getElementById("clientHeight").value = document.body.clientHeight;
} catch (e) { }

try {
	document.getElementById("userName").focus();
} catch (e) { }
//-->
</script>
<%}%>
