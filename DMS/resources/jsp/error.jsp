<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="include/header.jsp" flush="true" />

<div class="largeFormContainer">
<p class="error" style="font-weight: bold;">
&nbsp;&nbsp;Application Error
</p>
<pre>
<%
final simple.io.Log log = simple.io.Log.getLog();

StringBuilder error = new StringBuilder();
Throwable exception = (Throwable)request.getAttribute("simple.servlet.SimpleAction.exception");
if (exception == null)
	exception = (Throwable)request.getAttribute("exception");
if (exception != null) {
	error.append(exception.getClass().getName());
	if (!StringHelper.isBlank(exception.getMessage()))
		error.append(": ").append(exception.getMessage());

	Throwable ourCause = exception.getCause();
	if (ourCause != null && !StringHelper.isBlank(ourCause.toString()))
	    error.append("\n\n").append("Caused By: ").append(ourCause.toString());
}

String exceptionMessage = (String)request.getAttribute("simple.servlet.SimpleAction.exceptionMessage");
if (!StringHelper.isBlank(exceptionMessage) && error.indexOf(exceptionMessage) < 0)
	error.append("\n\n").append(exceptionMessage);

String errorMessage = (String)request.getAttribute("errorMessage");
if (!StringHelper.isBlank(errorMessage) && error.indexOf(errorMessage) < 0)
	error.append("\n\n").append(errorMessage);

if (error.length() == 0)
	error.append("Generic error occurred");
else
	error.insert(0, "Error: ");

error.append("\n\nURL: ").append(request.getRequestURL().toString());
if (!StringHelper.isBlank(request.getQueryString()))
	error.append("?").append(request.getQueryString());

out.write(StringUtils.encodeForHTML(error.toString()));

String logError = error.toString().replaceAll("\n\n", ", ");
if (exception == null)
	log.error(logError);
else
	log.error(logError, exception);
%>
</pre>
</div>

<jsp:include page="include/footer.jsp" flush="true" />