<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>	
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%
String statusMessage = RequestUtils.getAttribute(request, "statusMessage", String.class, false);
String statusType = RequestUtils.getAttribute(request, "statusType", String.class, false);
if(StringUtils.isBlank(statusType))
	statusType = "info";
Results firmwareUpgrades = RequestUtils.getAttribute(request, "firmwareUpgrades", Results.class, true);
Results deviceTypes = RequestUtils.getAttribute(request, "deviceTypes", Results.class, true);
Results steps = RequestUtils.getAttribute(request, "firmwareUpgradeSteps", Results.class, true);
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long firmwareUpgradeId = inputForm.getLong("firmwareUpgradeId", false, 0);
Results firmwareUpgrade = DataLayerMgr.executeQuery("GET_FIRMWARE_UPGRADE_INFO", new Object[] {firmwareUpgradeId});
boolean isGet = "GET".equalsIgnoreCase(request.getMethod());
boolean isFirmwareUpgrade = firmwareUpgrade.next();
int deviceTypeId = -1;
if (isFirmwareUpgrade)
	deviceTypeId = firmwareUpgrade.getValue("deviceTypeId", int.class);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<div class="fullPageFormContainer">
<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Firmware Upgrades</span></div>
</div>
<div class="tableContent">
<div class="spacer10"></div>
<form name="firmwareUpgrade" method="post" onsubmit="return validateForm(this);">
<input type="hidden" name="current_step_id" />
<table cellpadding="3" class="fullPage">
	<%if (!StringUtils.isBlank(statusMessage)) {%>
	<tr><td colspan="2"><span id="status" class="status-<%=statusType %>"><%=statusMessage %></span></td></tr>
	<%}%>
	<tr>
		<td class="label" width="10%">Firmware Upgrade</td>
		<td width="90%">
			<select onchange="window.location = '/firmwareUpgrade.i?firmwareUpgradeId=' + this.value">
	     		<option value="0">Choose Firmware Upgrade</option>
	     		<%
	     		while(firmwareUpgrades.next()) {
	     			long currentfirmwareUpgradeId = firmwareUpgrades.getValue("firmwareUpgradeId", long.class);
	     		%>
	     		<option value="<%=currentfirmwareUpgradeId%>"<%if (firmwareUpgradeId == currentfirmwareUpgradeId) {%> selected="selected"<%}%>><%=firmwareUpgrades.getFormattedValue("firmwareUpgradeName")%></option>
	     		<%}%>
			</select>
       </td>
	</tr>
	<tr>
		<td class="label">Upgrade Name<font color="red">*</font></td>
		<td><input id="firmware_upgrade_name" name="firmware_upgrade_name" value="<%if (isFirmwareUpgrade) out.write(firmwareUpgrade.getFormattedValue("firmwareUpgradeName"));%>" size="40" maxlength="100" usatRequired="true" label="Firmware Upgrade Name"/></td>
	</tr>
	<tr>
		<td class="label nowrap">Upgrade Description</td>
		<td><input id="firmware_upgrade_desc" name="firmware_upgrade_desc" value="<%if (isFirmwareUpgrade) out.write(firmwareUpgrade.getFormattedValue("firmwareUpgradeDesc"));%>" size="70" maxlength="200"/></td>
	</tr>
	<tr>
		<td class="label">Device Type</td>
		<td>
			<select id="device_type_id" name="device_type_id">
	     		<%
	     		while(deviceTypes.next()) {
	     			int currentDeviceTypeId = deviceTypes.getValue("deviceTypeId", int.class);
	     		%>
	     		<option value="<%=currentDeviceTypeId%>"<%if (deviceTypeId == currentDeviceTypeId) {%> selected="selected"<%}%>><%=deviceTypes.getFormattedValue("deviceTypeDesc")%></option>
	     		<%}%>
			</select>
		</td>
	</tr>
	<tr>
		<td class="label">Created</td>
		<td><%if (isFirmwareUpgrade) out.write(firmwareUpgrade.getFormattedValue("createdTs"));%></td>
	</tr>
	<tr>
		<td class="label">Last Updated</td>
		<td><%if (isFirmwareUpgrade) out.write(firmwareUpgrade.getFormattedValue("lastUpdatedTs"));%></td>
	</tr>
	<tr>
		<td colspan="2">
		<table class="tabDataDisplayBorder">
		<tr><td colspan="10" class="gridHeader">Upgrade Steps</td></tr>
		<tr class="gridHeader nowrap">
			<td>Step #<font color="red">*</font></td>
			<td>Step Name<font color="red">*</font></td>
			<td>Branch</td>
			<td>Branch Regex</td>
			<td>Target</td>
			<td>Target Version</td>
			<td>Target Regex</td>
			<td>File ID<font color="red">*</font></td>
			<td>File Name</td>
			<td>Action</td></tr>
		<%
		int stepCount = 0;
		while(steps.next()) {
			stepCount++;
		%>
		<tr>
			<td><input type="text" name="step_number_<%=steps.getFormattedValue("stepId")%>" value="<%=isGet ? steps.getFormattedValue("stepNumber") : inputForm.getStringSafely("step_number_" + steps.getFormattedValue("stepId"), "")%>" size="2" maxlength="5" usatRequired="true" label="Step #" /></td>
			<td><input type="text" name="step_name_<%=steps.getFormattedValue("stepId")%>" value="<%=isGet ? steps.getFormattedValue("stepName") : inputForm.getStringSafely("step_name_" + steps.getFormattedValue("stepId"), "")%>" size="15" maxlength="100" usatRequired="true" label="Step Name" /></td>
			<td>
				<%String branchParamCd = isGet ? steps.getFormattedValue("branchParamCd") : inputForm.getStringSafely("branch_param_cd_" + steps.getFormattedValue("stepId"), "");%>
				<select name="branch_param_cd_<%=steps.getFormattedValue("stepId")%>">
					<option value="">None</option>
					<option value="Bezel Mfgr"<%if ("Bezel Mfgr".equalsIgnoreCase(branchParamCd)) {%> selected="selected"<%}%>>Bezel Mfgr</option>
				</select>
			</td>
			<td><input type="text" name="branch_value_regex_<%=steps.getFormattedValue("stepId")%>" value="<%=isGet ? steps.getFormattedValue("branchValueRegex") : inputForm.getStringSafely("branch_value_regex_" + steps.getFormattedValue("stepId"), "")%>" size="15" maxlength="500" /></td>
			<td>
				<%String targetParamCd = isGet ? steps.getFormattedValue("targetParamCd") : inputForm.getStringSafely("target_param_cd_" + steps.getFormattedValue("stepId"), "");%>
				<select name="target_param_cd_<%=steps.getFormattedValue("stepId")%>">
					<option value="">None</option>
					<option value="Bezel App Rev"<%if ("Bezel App Rev".equalsIgnoreCase(targetParamCd)) {%> selected="selected"<%}%>>Bezel App</option>
					<option value="Bezel Mfgr"<%if ("Bezel Mfgr".equalsIgnoreCase(targetParamCd)) {%> selected="selected"<%}%>>Bezel Mfgr</option>
					<option value="Bootloader Rev"<%if ("Bootloader Rev".equalsIgnoreCase(targetParamCd)) {%> selected="selected"<%}%>>Bootloader</option>
					<option value="Diagnostic App Rev"<%if ("Diagnostic App Rev".equalsIgnoreCase(targetParamCd)) {%> selected="selected"<%}%>>Diagnostic</option>
					<option value="Firmware Version"<%if ("Firmware Version".equalsIgnoreCase(targetParamCd)) {%> selected="selected"<%}%>>Firmware</option>
					<option value="PTest Rev"<%if ("PTest Rev".equalsIgnoreCase(targetParamCd)) {%> selected="selected"<%}%>>PTest</option>
				</select>
			</td>
			<td><input type="text" name="target_version_<%=steps.getFormattedValue("stepId")%>" value="<%=isGet ? steps.getFormattedValue("targetVersion") : inputForm.getStringSafely("target_version_" + steps.getFormattedValue("stepId"), "")%>" size="10" maxlength="200" valid="[0-9]" label="Target Version" /></td>
			<td><input type="text" name="target_value_regex_<%=steps.getFormattedValue("stepId")%>" value="<%=isGet ? steps.getFormattedValue("targetValueRegex") : inputForm.getStringSafely("target_value_regex_" + steps.getFormattedValue("stepId"), "")%>" size="15" maxlength="500" /></td>
			<td><input type="text" name="file_transfer_id_<%=steps.getFormattedValue("stepId")%>" value="<%=isGet ? steps.getFormattedValue("fileTransferId") : inputForm.getStringSafely("file_transfer_id_" + steps.getFormattedValue("stepId"), "")%>" size="10" maxlength="20" usatRequired="true" label="File ID" /></td>
			<td>
				<%if (steps.getValue("fileTransferId", long.class) > 0) {%>
				<a href="/fileDetails.i?file_transfer_id=<%=steps.getFormattedValue("fileTransferId")%>"><%=steps.getFormattedValue("fileTransferName")%></a>
				<%} else {%>
				<%=steps.getFormattedValue("fileTransferName")%>
				<%}%>
			</td>
			<td>
				<%if (steps.getValue("stepId", int.class) == 0) {%>
				<input type="submit" name="action" value="Add Step"<%if (!isFirmwareUpgrade) out.write(" disabled=\"disabled\"");%> />
				<%} else {%>
				<input type="submit" name="action" value="Update Step" onclick="document.forms['firmwareUpgrade'].elements['current_step_id'].value = '<%=steps.getFormattedValue("stepId")%>';" />
				<input type="submit" name="action" value="Delete Step" onclick="document.forms['firmwareUpgrade'].elements['current_step_id'].value = '<%=steps.getFormattedValue("stepId")%>'; return confirm('Are you sure you want to delete this Firmware Upgrade Step?');" />
				<%}%>
			</td>
		</tr>
		<%}%>
		</table>
		<input type="hidden" name="step_count" value="<%=stepCount%>" />
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div class="spacer10"></div>
		<input type="submit" name="action" value="Save"<%if (!isFirmwareUpgrade) out.write(" disabled=\"disabled\"");%> />
		<input type="submit" name="action" value="Create" />
		<input type="submit" name="action" value="Delete"<%if (!isFirmwareUpgrade) out.write(" disabled=\"disabled\"");%> onclick="return confirm('Are you sure you want to delete this Firmware Upgrade?');" />
		</td>
	</tr>
</table>
</form>
</div>
<div class="spacer10"></div>
</div>
<script type="text/javascript">
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />