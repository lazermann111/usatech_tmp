<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
		<div class="tableContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">Device Configuration Wizard - Page 2: Device Selection Confirmation</div>
		</div>
		<form method="post" action="bulkConfigWizard3.i">
<table class="tabDataDisplayBorder">   
		<%
Results results = RequestUtils.getAttribute(request, "bulk_config_wizard_2_search_results", Results.class, true);
if(results.isGroupEnding(0)) {
	%><tbody><tr><td align="center" colspan="<%=results.getColumnCount()%>" width="100%"><br/><font color='red'><b>Sorry, no devices were found that match your search criteria!</b></font><br/><br/></td></tr></tbody><% 
} else { %>					
    <thead>
		<tr class="gridHeader">
			<td>Include?</td>
			<%for(int i = 2; i <= results.getColumnCount(); i++)  {%>
			<td><%=StringUtils.prepareHTML(results.getColumnName(i)) %></td><%
			} %>
		</tr>
	</thead>
	<tbody><%
while(results.next()) {
	Long deviceId = results.getValue("device_id", Long.class);
	%>
<tr>
				<td align="center"><input type="checkbox" name="include_device_ids" onClick="count_check(this.form.include_device_ids)" value="<%=deviceId %>" checked="checked"></td>
				<td><a href="/profile.i?device_id=<%=deviceId %>"><%=StringUtils.prepareHTML(results.getFormattedValue(2)) %></a></td>
				<%for(int i = 3; i <= results.getColumnCount(); i++)  {%>
	            <td><%=StringUtils.prepareHTML(results.getFormattedValue(i)) %></td><%
	            } %>
			</tr><%
}
%></tbody> <%
}%>
		<tr class="gridHeader">
			<td colspan="<%=results.getColumnCount()%>" align="center">
			<%if(results.getRowCount() > 0) { %>
		       <input type="button" id="check_count_button" class="cssButton" value="Uncheck All" onClick="this.value=check(this.form.include_device_ids)" />
		       <input type="text" id="check_count" value="<%=results.getRowCount() %>" style="width:30px;" /><%} %>
		       <input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
		       <input type="button" class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
		       <%if(results.getRowCount() > 0) { %>
		       <input type="submit" class="cssButton" name="action" value="Next &gt;" onClick="return confirmSubmitBulk3()" /> <%} %>
			</td>
		</tr>
</table>
		<input type="hidden" name="start_serial_head" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "start_serial_head", String.class, false)) %>"  />
		<input type="hidden" name="device_type_id" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "device_type_id", String.class, false)) %>"  />
		<input type="hidden" name="comm_method" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "comm_method", String.class, false)) %>" />	
		</form>
		</div>
<script type="text/javascript">
	var checkflag = "true";
	function confirmSubmitBulk3()
	{
	   var obj = document.getElementById("check_count");
	   if (obj.value > 0) {
	     return true ;
	   } else {
	     alert("Please make a checkbox selection!");
	     return false ;
	   }
	}
	function count_check(field) {
	   var count = 0;
	   var obj = document.getElementById("check_count");
	   if(field.length)
		   for (i = 0; i < field.length; i++) {
		      if(field[i].checked == true) {
		         count++;
		      }
		   }
	   else if(field.checked == true)
		   count++;
	   obj.value = count;
	   var obj = document.getElementById("check_count_button");
	   if(count == 0){		   
		   obj.value = "Check All";
		   checkflag = "false";
	   }else{
		   obj.value = "Uncheck All";
		   var checkflag = "true";
	   }
	   //alert("count_check checkflag="+checkflag);
	}
	
	//following is checking all checkboxes
	
	function check(field) {
	   var obj = document.getElementById("check_count");
	   var count = 0;
	   //Lets get true picture of what the checkflag should be
	   //Go through the checkboxes and make sure checkflag is correct
	   if(field.length)
	       for (i = 0; i < field.length; i++) {
	         if(field[i].checked == true){
	         	count++;
	         }
	       }
	   else if(field.checked == true)
           count++;
       if(count>0){
		   checkflag = "true";
	   }else{
		   checkflag = "false";
	   }
	   //alert("check checkflag1="+checkflag);
	   if (checkflag == "false") {
		   if(field.length)
		       for (i = 0; i < field.length; i++) {
		         field[i].checked = true;
		         count++;
		      }
		   else {
			   field.checked = true; 
			   count++;
		   }
	      checkflag = "true";
	      obj.value = count;
	      //alert("check checkflag was false now="+checkflag);
	      return "Uncheck All";
	      
	   } else {
		   if(field.length)
	           for (i = 0; i < field.length; i++) {
		         field[i].checked = false;
		      }
		   else
               field.checked = false; 
          checkflag = "false";
	      obj.value = 0;
	      //alert("check checkflag was true now="+checkflag);
	      return "Check All";
	      
	   }
	}
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
