<%@page import="simple.servlet.BasicServletUser"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.layers.common.util.DeviceUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.sql.Connection"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>

<%@page import="simple.bean.ConvertUtils" %>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.dms.action.ConfigFileActions"%>
<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.action.PendingCmdActions"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.dms.model.FileTransfer"%>
<%@page import="com.usatech.layers.common.ProcessingUtils"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.device.DeviceConfigurationUtils"%>
<%@page import="com.usatech.layers.common.model.ConfigTemplateSetting"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.util.DMSSecurityUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@ page import="simple.db.DataLayerException" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="simple.bean.ConvertException" %>
<%@ page import="simple.service.modem.service.dto.USATDevice" %>
<%@ page import="com.usatech.dms.device.modem.AsyncDmsModemService" %>
<%@ page import="simple.modem.USATDeviceDao" %>
<%@ page import="simple.modem.USATModemService" %>
<%@ page import="simple.modem.USATDeviceDaoImpl" %>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div id="__div_edit_configuration_func" class="tableDataContainer">

<div class="spacer5"></div>

<%!
	public boolean isDeviceModemStatusField(String changedKey) {
		return "DEVICE_MODEM_STATUS".equals(changedKey);
	}

	public void enqueueModemStatusChange(long deviceId, String newStatusKey, String oldStatusKey) throws DataLayerException, SQLException, ConvertException, ServletException {
		if (newStatusKey == null || newStatusKey.length() != 1) {
			return;
		}
		if (oldStatusKey != null && oldStatusKey.equals(newStatusKey)) {
			return;
		}
		USATDeviceDao modemDeviceDao = new USATDeviceDaoImpl();
		USATDevice dev = modemDeviceDao.populateByDeviceId(deviceId);
		if (dev.hasSerialNumberAndProvider()) {
			USATModemService dmsModemService = new AsyncDmsModemService();
            if ("N".equals(newStatusKey)) {
                dmsModemService.refreshActivationStatus(new USATDevice[] {dev});
            } else if ("A".equals(newStatusKey)) {
                if (oldStatusKey == null || !"S".equals(oldStatusKey)) {
                    dmsModemService.activate(new USATDevice[]{dev});
                } else {
                    dmsModemService.resume(new USATDevice[]{dev});
                }
            } else if ("D".equals(newStatusKey)) {
                dmsModemService.deactivate(new USATDevice[] {dev});
            } else if ("S".equals(newStatusKey)) {
                dmsModemService.suspend(new USATDevice[] {dev});
            } else {
                dmsModemService.refreshActivationStatus(new USATDevice[] {dev});
            }
		}
	}
%>

<%
final simple.io.Log log = simple.io.Log.getLog();

BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
String userName = user.getUserName();

InputForm inputForm    = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long deviceId          = inputForm.getLong("device_id", false, -1);
String evNumber        = inputForm.getStringSafely("ev_number", "");
String serialNumber    = inputForm.getStringSafely("ssn", "");
String newName         = inputForm.getStringSafely("new_name", "");
int deviceType         = inputForm.getInt("device_type_id", false, -1);
String action          = inputForm.getStringSafely("action", "");
String sendAction      = inputForm.getStringSafely("send", "");
String editMode 	   = inputForm.getStringSafely("edit_mode", "A");
boolean publicPc       = inputForm.getBoolean("publicPc", false, false);

if ("Copy To".equals(action) && StringHelper.isBlank(newName)) {
%>
<span class="error">Required Parameter Not Found: new_name</span>
<%
}else if (deviceId < 1) {
%>
<span class="error">Required Parameter Not Found: device_id</span>
<%	
} else {
%>
<table class="tabDataDisplayNoBorder">
<tr>
<td>
   <center>
   <form>
<%
    if (StringHelper.isBlank(evNumber)) {
%>
    <input type=button class="cssButton" value="Return to Template Menu" onClick="javascript:window.location = '/templateMenu.i';">
<%
    } else {
        String profileLink = deviceId < 1 ? "/deviceSearch.i?ev_number=" + evNumber : "/deviceConfig.i?device_id=" + deviceId;
%>
    <input type="button" class="cssButton" value="Return to Device Profile" onClick="javascript:window.location = '<%=profileLink%>';">
<%
    }
%>
   </form>
   </center>
   <pre>
<%

if (StringHelper.isBlank(evNumber))
    out.print("\n<b>Processing configuration template...</b>\n");
else
    out.println("\n<b>Processing device configuration for " + evNumber + "...</b>");

Connection conn = null;
boolean success = false;
int changeCount = 0;
int serverOnlyChangeCount = 0;
boolean rawEditMode = "R".equalsIgnoreCase(editMode);
try {
	conn = DataLayerMgr.getConnection("OPER");
	
	LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceType, -1, null, conn);
	LinkedHashMap<String, String> oldDeviceSettingData = ConfigFileActions.getDeviceSettingData(defaultSettingData, deviceId, deviceType, true, conn);
	LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String> ();
	Map<String, String> errorMap = new HashMap<String, String> ();
	
	if(rawEditMode)
		deviceSettingData = ConfigFileActions.parseIniFileData(inputForm.getStringSafely("file_data", ""));
	else {		
		for (String configParam : oldDeviceSettingData.keySet()) {
			ConfigTemplateSetting defaultSetting = defaultSettingData.get(configParam);
			if (defaultSetting != null && defaultSetting.isServerOnly())
				continue;
			boolean storedInPennies = defaultSetting != null && "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
		    String oldParamValue = oldDeviceSettingData.get(configParam);
		    if (oldParamValue == null)
		    	oldParamValue = "";
		    
			String paramValue = inputForm.getStringSafely(new StringBuilder("cfg_field_").append(configParam).toString(), "");
		    
		    String regex = inputForm.getString("regex_" + configParam, false);
		    boolean isDailyExtendedSchedule=WebHelper.isDailyExtendedSchedule(defaultSetting);
			if(!StringHelper.isBlank(regex)) {
				if(isDailyExtendedSchedule){
					if(!StringUtils.isBlank(paramValue)){
						String[] paramValueList=paramValue.split(",");
						for(String paramValueListValue:paramValueList){
							if (!Helper.validateRegexWithMessage(errorMap, paramValueListValue, configParam, regex, "Value validation failed for field: " + configParam))
								continue;
						}
					}
				}else{
					if (!Helper.validateRegexWithMessage(errorMap, paramValue, configParam, regex, "Value validation failed for field: " + configParam))
						continue;
				}
			}
			
			if (storedInPennies && StringHelper.isNumeric(paramValue))
				paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
			
			if(publicPc && (configParam.indexOf("OpenAPIPassword") >-1 || configParam.indexOf("MFPAdminPassword") >-1 || configParam.indexOf("SNMPCommunity") >-1)) {				
				String encryptedOutput = DMSSecurityUtil.rijndaelEncrypt(paramValue);
				paramValue = StringHelper.encodeHexString(encryptedOutput);
			}
			
			if(isDailyExtendedSchedule){
				paramValue=WebHelper.buildScheduleDailyExtended(paramValue);
			}
			
			if(!deviceSettingData.containsKey(configParam)) {
				deviceSettingData.put(configParam, paramValue);
				if (!StringHelper.equalConfigValues(oldParamValue, paramValue, storedInPennies)) {					
					if (defaultSetting == null || !defaultSetting.isServerOnly())
						changeCount++;
				}
			}
		}
		
		for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
			ConfigTemplateSetting defaultSetting = entry.getValue();
			if (!defaultSetting.isServerOnly())
				continue;
			String key = defaultSetting.getKey();
			boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
			String paramValue = inputForm.getStringSafely(new StringBuilder("cfg_field_").append(key).toString(), "");
			if (storedInPennies && StringHelper.isNumeric(paramValue))
				paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
			String oldParamValue = oldDeviceSettingData.get(key);
		    if (oldParamValue == null)
		    	oldParamValue = "";
		    if (!StringHelper.equalConfigValues(oldParamValue, paramValue, storedInPennies)) {
				DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, paramValue, conn, userName);
				if (isDeviceModemStatusField(key)) {
					enqueueModemStatusChange(deviceId, paramValue, oldParamValue);
				}
				changeCount++;
				serverOnlyChangeCount++;
			}
		}
	}

	if(errorMap.size() > 0){
		Collection<String> errors = (Collection<String>)errorMap.keySet();
		for(String error : errors)
			out.println("<font color=\"red\"><b>" + error + "</b></font>");
	} else if(changeCount > 0 || rawEditMode) {
   		String newIniFile = ConfigFileActions.getIniFileFromDeviceSettingData(deviceSettingData);
   		
   		if (changeCount > serverOnlyChangeCount || rawEditMode) {
	    	out.print("\n<b>Saving device configuration...</b>\n\n");	    	
	    	DeviceConfigurationUtils.saveDeviceSettingData(deviceId, deviceSettingData, true, false, conn, userName);
	    	
	    	out.print("<b>Configuration Data:</b>\n");
	    	out.print("<textarea rows=\"10\" readonly=\"readonly\" style=\"width: 99%;\">" + newIniFile + "</textarea>\n\n");				
	    	out.println("<b>Successfully saved device configuration</b>");
   		}
   		
   		if (serverOnlyChangeCount > 0)
   			out.print("\n<b>Changed " + serverOnlyChangeCount + " server-only settings</b>\n");
   		
    	if (!StringUtils.isBlank(newIniFile) && "Save and Send".equals(action)&&!WebHelper.isCraneDevice(serialNumber)) {
        	out.println("\n<b>Creating command to send config file...</b>");
	    	long fileId = ConfigFileActions.saveFile(new StringBuilder(evNumber).append("-CFG").toString(), newIniFile, -1, FileType.CONFIGURATION_FILE.getValue(), conn);
	    	DeviceUtils.sendCommand(deviceType, deviceId, evNumber, fileId, newIniFile.length(), DeviceUtils.DEFAULT_PACKET_SIZE, DeviceUtils.DEFAULT_EXECUTE_ORDER, conn, DeviceActions.KIOSK_CONFIG_FILE_NAME_HEX, true);
       		out.println("\n<b>Successfully created File Transfer Start command</b>");
        }    	
    	conn.commit();
	} else
		out.print("\n<b>No User Changes!</b>\n");
    	
   	success = true;
} catch(Exception e){
 	out.print("\n\n<font color='red'><b>Exception occurred! " + e.getMessage() + "</b></font>\n\n");
 	log.error(StringUtils.exceptionToString(e));
}finally{
	if (!success)
		ProcessingUtils.rollbackDbConnection(log, conn);    		
	ProcessingUtils.closeDbConnection(log, conn);
}

%>
   </pre>
   <center>
   <form>
<%
    if (StringHelper.isBlank(evNumber)) {
%>
    <input type=button class="cssButton" value="Return to Template Menu" onClick="javascript:window.location = '/templateMenu.i';">
<%
    } else {
        String profileLink = deviceId < 1 ? "/deviceSearch.i?ev_number=" + evNumber : "/deviceConfig.i?device_id=" + deviceId;
%>
    <input type="button" class="cssButton" value="Return to Device Profile" onClick="javascript:window.location = '<%=profileLink%>';">
<%
    }
%>
   </form>
   </center>
   <div class="spacer5"></div>
</td>
</tr>
</table>
</div>
<%
}
%>

<div class="spacer5"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
