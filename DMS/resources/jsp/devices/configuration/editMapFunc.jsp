<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.Collection"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>

<%@page import="simple.bean.ConvertUtils" %>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.dms.action.ConfigFileActions"%>
<%@page import="com.usatech.dms.action.PendingCmdActions"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.device.DeviceConfigurationUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.results.Results"%>

<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="spacer5"></div>

<%
final simple.io.Log log = simple.io.Log.getLog();

InputForm inputForm    = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String templateName    = inputForm.getString("config_template_name", false);
String templateType    = inputForm.getString("file_type", false);
int deviceType         = inputForm.getInt("device_type_id", false, -1);
int locationsNumber    = inputForm.getInt("number_of_settings", false, 0);
String action          = inputForm.getString("action", false);
%>
   <div align="center">  
   <form>
    	<input type=button class="cssButton" value="Return to Template Menu" onClick="javascript:window.location='/templateMenu.i';">    
   </form>
   </div>

   <pre>
<%
StringBuilder buffer;
StringBuilder newLocationFileBuffer = new StringBuilder();
int changeCount = 0;
int memCounter = 0;
int location_counter_sum = 0;
int new_order_sum = 0;
int orderNum = -1;
int counter = 0;
boolean fatalError = false;

Map<String, Object[]> dataHash = new HashMap<String, Object[]>();
Map<String, String> locationsMap = new HashMap<String, String>();
Map<String, String> locationsOffsetSortedMap = new TreeMap<String, String>(new ConfigFileActions.KeySorter());
for (int locationCounter = 1; locationCounter <= locationsNumber; locationCounter++) {
	orderNum = ConvertUtils.getIntSafely(request.getParameter("cfg_order_" + locationCounter), -1);
	if (orderNum < 1)
		continue;
	location_counter_sum = location_counter_sum + locationCounter;
	new_order_sum = new_order_sum + orderNum;	
	locationsMap.put(String.valueOf(locationCounter), String.valueOf(orderNum));	
}

if(location_counter_sum != new_order_sum){
	out.println("<font color=red><b>Order Validation Failed!<br/>");
	out.println("Please check your order numbers and make sure there are no duplicates.<br/></b></font>");
	out.println("<script language=\"javascript\">window.alert(\"Error Occured! Please click OK, then print this screen and give it to an administrator.\");</script>\n");
	
}else{
	
	out.println("Looks good!<br/>");
	locationsOffsetSortedMap.putAll(locationsMap);
	Collection<String> locationsList = (Collection<String>)locationsOffsetSortedMap.keySet();
	try{
		for(String locationCounter : locationsList){
		    buffer = new StringBuilder();
		    String order          = request.getParameter("cfg_order_" + locationCounter);
		    int startAddr = memCounter;
		    String strStartAddr  = String.valueOf(startAddr);
			
		    String name          = request.getParameter("cfg_name_" + locationCounter);
		    int size             = ConvertUtils.getIntSafely(request.getParameter("cfg_size_" + locationCounter), -1);
			String description   = request.getParameter("cfg_description_" + locationCounter);
			String align         = request.getParameter("cfg_align_" + locationCounter);
			String padWith       = request.getParameter("cfg_pad_with_" + locationCounter);
			String dataMode      = request.getParameter("cfg_data_mode_" + locationCounter);
			String address = request.getParameter("cfg_address_" + locationCounter);
			String altName = request.getParameter("cfg_alt_name_" + locationCounter);
			String regexId = request.getParameter("cfg_regex_id_" + locationCounter);
			String display = request.getParameter("cfg_display_" + locationCounter);
			String allowChange = request.getParameter("cfg_allow_change_" + locationCounter);
			String origKey = request.getParameter("cfg_original_key_" + locationCounter);
			int categoryId = ConvertUtils.getIntSafely(request.getParameter("cfg_category_id_" + locationCounter), -1);
			
			// EDITOR fields
			// incoming user submitted editor type chosen from drop-down
			String editorType = request.getParameter("cfg_editor_type_" + locationCounter);
			String editorDetails = request.getParameter("cfg_editor_details_" + locationCounter);
			
			address = StringHelper.isBlank(address) ? "" : address.trim();
			name = StringHelper.isBlank(name) ? "" : name.trim();
			description = StringHelper.isBlank(description) ? "" : description.trim();
			editorDetails = StringHelper.isBlank(editorDetails) ? "" : editorDetails.trim();
			allowChange = StringHelper.isBlank(allowChange) ? "" : allowChange.trim();
			
			out.println("Location Counter     : " + locationCounter + "-----------------------");
			out.println("New Order            : " + StringUtils.encodeForHTML(order));
			out.println("Memory Counter       : " + memCounter);
			out.println("Name                 : " + StringUtils.encodeForHTML(name));
			out.println("Address              : " + StringUtils.encodeForHTML(address));
			out.println("Size                 : " + size);
			out.println("Align                : " + StringUtils.encodeForHTML(align));
			out.println("Pad With             : " + StringUtils.encodeForHTML(padWith));
			out.println("Data Mode            : " + StringUtils.encodeForHTML(dataMode));
			out.println("Display              : " + StringUtils.encodeForHTML(display));
			out.println("Description          : " + StringUtils.encodeForHTML(description));
			out.println("Editor Details       : " + StringUtils.encodeForHTML(editorDetails));
			out.println("Allow Change         : " + StringUtils.encodeForHTML(allowChange) + "\n");
						
			String editor;
			if (StringHelper.isBlank(editorDetails))
			{
				out.println("<font color=red><b>Editor Details are required!\n");
				out.println("Please enter an editor details value for: " + StringUtils.encodeForHTML(name) + "\n</b></font>");
				out.println("<script language=\"javascript\">window.alert(\"Error Occured! Please click OK, then print this screen and give it to an administrator.\");</script>\n");
				throw new ServletException();
			}
				
			if(!"TEXT".equalsIgnoreCase(editorType)) {
				if(!editorDetails.contains("="))
				{
					out.println("<font color=red><b>Invalid editor details for a " + StringUtils.encodeForHTML(editorType) + " field!\n");
					out.println("Please enter valid editor details for: " + StringUtils.encodeForHTML(name) + "\n</b></font>");
					out.println("<script language=\"javascript\">window.alert(\"Error Occured! Please click OK, then print this screen and give it to an administrator.\");</script>\n");
					throw new ServletException();
				}
			    	
			    editor = new StringBuilder(editorType).append(":").append(editorDetails).toString();
		    } else {	    	
				if(!editorDetails.contains(" to "))
				{
					out.println("<font color=red><b>Invalid editor details for a " + StringUtils.encodeForHTML(editorType) + " field!\n");
					out.println("Please enter valid editor details for: " + StringUtils.encodeForHTML(name) + "\n</b></font>");
					out.println("<script language=\"javascript\">window.alert(\"Error Occured! Please click OK, then print this screen and give it to an administrator.\");</script>\n");
					throw new ServletException();
				}
		    		
	    		int min = 0;
	    		int max = "H".equalsIgnoreCase(dataMode) ? size*2 : size;
	    		
	    		String minMax[] = editorDetails.split(" to ");
	    		if (minMax.length > 1) {
	    			int editorMax = ConvertUtils.getIntSafely(minMax[1], -1);
	    			if (editorMax > 0 && editorMax < max)
	    				max = editorMax;
	    			int editorMin = ConvertUtils.getIntSafely(minMax[0], -1);
	    			if (editorMin > 0 && editorMin < max)
	    				min = editorMin;
	    		}

	    		editor = new StringBuilder(editorType).append(":").append(min).append(" to ").append(max).toString();
		    	  
			}
		    
		    dataHash.put(locationCounter, new Object[]{address, address, size, align, padWith, dataMode, display, description, editor, regexId, null,
		    		allowChange, order, name, altName, categoryId, origKey, deviceType});
		    
		    memCounter = memCounter + size;
		}
		
		out.println("Checking memory size: " + memCounter + "...");
		if(deviceType == DeviceType.G4.getValue() && memCounter != 512)
		{
			out.println( "<font color=red><b>Size Validation Failed!\n");
			out.println( "Please check your memory addresses and sizes and make sure there are no overlaps or gaps.\n\n</b></font>");
			out.println( "<script language=\"javascript\">window.alert(\"Error Occured! Please click OK, then print this screen and give it to an administrator.\");</script>\n");
		    throw new ServletException();
		}
		else
		{
			out.println ("Looks good!\n\n");
		}
		
	}catch(ServletException e){
		fatalError = true;
	}catch(Exception e){
		log.error(StringUtils.exceptionToString(e));
		out.println("<font color=\"red\"><b>Exception occurred:" + e.getMessage() + "</b></font>");
		fatalError = true;
	}
	
	if (!fatalError) {
		
		out.println("Saving Default Template Config...\n\n");
		
		boolean fail = false;
		try{
			for(String locationCounter : locationsList){
				
				Object[] params = dataHash.get(locationCounter);
				int returnCode = ConfigFileActions.updateDefaultTemplateSetting(params);
				if(returnCode==0){
					out.println("Save Error--Unable to save changes for field name="+ StringUtils.encodeForHTML(params[0]));
					fail=true;
					throw new ServletException("Save Error--Unable to save changes for field name="+ params[0]);
				}else{
					out.print("Line " + locationCounter + "\t\t: "); 
					int ctr = 0;
					for(Object param : params){
						
						ctr++;
						if(ctr==1)
							out.print("\""+StringUtils.encodeForJavaScript(param)+"\""); 
						else
							out.print(",\""+StringUtils.encodeForJavaScript(param)+"\""); 
					}
					out.println("");
				}
					
				
			}
		}catch(Exception e){
			throw new ServletException(e);
		}
		
		if(!fail){
			out.println( "\n<b>Save Success: Updated configuration metadata for " + templateName + "</b>");
		}
		
	}else{
		out.println( "<b>Save Failed!: Configuration metadata update failed for " + StringUtils.encodeForHTML(templateName) + "</b>\n");	
		
	}
	
}

%>
   </pre>
   
   <div align="center">
   <form>
   		<input type=button class="cssButton" value="Return to Template Menu" onClick="javascript:window.location='/templateMenu.i';" />
   </form>
	</div>

<div class="spacer5"></div>


<jsp:include page="/jsp/include/footer.jsp" flush="true" />

