<%@page import="java.lang.StringBuilder"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.List"%>

<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.util.NameValuePair"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@page import="com.usatech.dms.action.ConfigFileActions"%>
<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
long deviceId = device.getId();
int deviceTypeId = device.getTypeId();
boolean ePortConnect = device.getSerialNumber().startsWith("K3");

%>
<div class="innerTable">
<%
if (device.isG4() || device.isGX() || device.isMEI()) {
%>
<div class="tabHead">Device Configuration</div>
<table class="tabDataDisplayBorderNoFixedLayout">
<tbody>
<tr>
	<td style="text-align: center;">
    <select name="edit_mode">           
    	<option value="A">Advanced</option>
        <option value="B">Basic</option>
    </select>
    <%if (device.isShowDisabled()) {%>
    <input type="button" class="cssButtonDisabled" value="Edit" disabled="disabled" />
    <%} else {%>
    <input type="button" class="cssButton" value="Edit"
    onclick="return redirectWithParams('editDeviceConfig.i', 'device_id=<%=deviceId%>&device_type_id=<%=deviceTypeId%>&ev_number=<%=device.getDeviceName()%>&ssn=<%=device.getSerialNumber()%>&file_name=<%=device.getConfigFileName()%>', new Array(edit_mode));" />
    <%}%>
	</td>

	<td style="text-align: center;">
<%
List<NameValuePair> templates = ConfigFileActions.getConfigTemplates(deviceTypeId, deviceId);
if (templates != null) {
%>
	<select name="template_id" id="template_id">
<%
String defaultConfigFile = DeviceActions.getDeviceDefaultConfigFile(device, null);
for (NameValuePair item : templates) {%>
        <option value="<%=item.getValue()%>"<%if (item.getName().equals(defaultConfigFile)) {%> selected="selected"<%}%>><%=item.getName()%></option>
<%}%>
	</select>	
    <%if (device.isShowDisabled()) {%>
    <input type="button" class="cssButtonDisabled" value="Import" disabled="disabled" />
    <%} else {%>
	<input type="button" class="cssButton" name="myaction" value="Import"
	onclick="window.location = '/editDeviceConfig.i?myaction=Import&device_id=<%=deviceId%>&edit_mode=A&device_type_id=<%=deviceTypeId%>&ev_number=<%=device.getDeviceName()%>&ssn=<%=device.getSerialNumber()%>&file_name=<%=device.getConfigFileName()%>&template_id=' + document.getElementById('template_id').value + '&template_name=' + document.getElementById('template_id').options[document.getElementById('template_id').selectedIndex].text;" />
<%} }%>
   &nbsp;
	</td>	
	<td align="center">
		<%if (device.isShowDisabled()) {%>
	    <input type="button" class="cssButtonDisabled" value="Backup" disabled="disabled" />
	    <%} else {%>
    	<input type="button" name="userOP" class="cssButton" value="Backup" onclick="return redirectWithParams(null, 'device_id=<%=deviceId%>', new Array(this));" />
    	<%} %>
    </td>
    <td style="text-align: center;">
    <input type="text" class="cssText" id="ssn_new" name="ssn_new" value="" onkeypress="return ignoreEnterKey(event)">
    <%if (device.isShowDisabled()) {%>
    <input type="button" class="cssButtonDisabled" value="Clone Serial Number" disabled="disabled" />
    <%} else {%>
    <input type="button" class="cssButton" value="Clone Serial Number"
    onclick="return redirectWithParams('deviceCloning.i', 'cloningType=S&userOP=Compare&targetNumber=<%=device.getSerialNumber()%>&sourceNumber=' + document.getElementById('ssn_new').value, null);" />
    <%}%>
    </td>    
</tr>
</tbody>
</table>

<%}
 else if (device.isKIOSK() || device.isEDGE() || device.isLegacyKIOSK() || device.isT2()) {
	 LinkedHashMap<String, String> deviceSettingData = ConfigFileActions.getDeviceSettingData(null, deviceId, deviceTypeId, false, null);
%>
<div class="tabHead">Device Configuration</div>
<div class="spacer5"></div>
<table class="tabDataDisplay">            	
<tbody>
<tr>
    <td align="center">
    <select id="edit_mode" name="edit_mode">
    	<option value="A">Advanced</option>
    	<option value="B">Basic</option>
    	<%if (!device.isEDGE()) {%>
        <option value="R">Raw</option>
    	<%}%>
    </select>
    <%if (device.isShowDisabled()) {%>
    <input type="button" class="cssButtonDisabled" value="Edit" disabled="disabled" />
    <%} else {%>
    <input type="button" name="myaction" class="cssButton" value="Edit"
    onclick="return redirectWithParams('editDeviceConfig.i','device_id=<%=deviceId%>&device_type_id=<%=deviceTypeId%>&file_name=<%=device.getConfigFileName()%>&ev_number=<%=device.getDeviceName()%>&ssn=<%=device.getSerialNumber()%>&plv=<%=device.getPropertyListVersion()%>', new Array(this, edit_mode));" />
    <%} %>
    </td>
<%if (!ePortConnect || device.getSerialNumber().startsWith("K3VS")) { %>
    <td align="center">
	<select name="template_id" id="template_id">
<%
List<NameValuePair> templates = ConfigFileActions.getConfigTemplates(deviceTypeId, deviceId);
if (templates != null) {
	String defaultConfigFile = DeviceActions.getDeviceDefaultConfigFile(device, deviceSettingData);
	for (NameValuePair item : templates) {%>
		<option value="<%=item.getValue()%>"<%if (item.getName().equals(defaultConfigFile)) {%> selected="selected"<%}%>><%=item.getName()%></option>
<%} }%>
	</select>
	<%if (device.isShowDisabled()) {%>
    <input type="button" class="cssButtonDisabled" value="Import" disabled="disabled" />
    <%} else {%>
	<input type="button" name="myaction" class="cssButton" value="Import"
	onclick="window.location = '/editDeviceConfig.i?myaction=Import&device_id=<%=deviceId%>&device_type_id=<%=deviceTypeId%>&ev_number=<%=device.getDeviceName()%>&ssn=<%=device.getSerialNumber()%>&plv=<%=device.getPropertyListVersion()%>&edit_mode=<%=device.isEDGE() ? "A" : "R"%>&file_name=<%=device.getConfigFileName()%>&template_id=' + document.getElementById('template_id').value + '&template_name=' + document.getElementById('template_id').options[document.getElementById('template_id').selectedIndex].text;" />
	<%} %>
    </td>

    <td align="center">
    	<%if (device.isShowDisabled()) {%>
	    <input type="button" class="cssButtonDisabled" value="Backup" disabled="disabled" />
	    <%} else {%>
    	<input type="button" name="userOP" class="cssButton" value="Backup" onclick="return redirectWithParams(null, 'device_id=<%=deviceId%>', new Array(this));" />
    	<%} %>
    </td>
<%}%>    
    <td style="text-align: center;">
    <input type="text" class="cssText" id="sourceNumber" name="sourceNumber" value="" onkeypress="return ignoreEnterKey(event)">
    <%if (device.isShowDisabled()) {%>
    <input type="button" class="cssButtonDisabled" value="Clone Serial Number" disabled="disabled" />
    <%} else {%>
    <input type="button" class="cssButton" value="Clone Serial Number"
    onclick="return redirectWithParams('deviceCloning.i', 'cloningType=S&userOP=Compare&targetNumber=<%=device.getSerialNumber()%>', new Array(sourceNumber));" />
    <%}%>
    </td>
</tr>
<%
String iniFile = ConfigFileActions.getIniFileFromDeviceSettingData(deviceSettingData);
if (!StringHelper.isBlank(iniFile)) {
%>
<tr><td colspan="4" class="gridHeader"><b>Configuration Data</b></td></tr>
<tr>
<td colspan="4">
<textarea rows="5" readonly="readonly" style="width: 99%;"><%=ConfigFileActions.getIniFileFromDeviceSettingData(deviceSettingData)%></textarea>
</td>
</tr>
<%}%>
</tbody>
</table>

<%if (device.isKIOSK() && device.getSerialNumber().startsWith("K3")) {%>
<div class="spacer10"></div>
<div class="tabHead">Quick Connect Configuration</div>
<div class="spacer5"></div>
<table class="tabDataDisplay">            	
<tr>
	<td align="center"><b>Credential</b>&nbsp;
		<select id="credential_id" name="credential_id">
     		<option value="0">No Credential</option>
     		<%
     		Results credentials = DataLayerMgr.executeQuery("GET_CREDENTIALS", null);
     		while(credentials.next()) {
     			long currentCredentialId = credentials.getValue("credentialId", long.class);
     		%>
     		<option value="<%=currentCredentialId%>"<%if (device.getCredentialId() == currentCredentialId) {%> selected="selected"<%}%>><%=credentials.getFormattedValue("credentialName")%></option>
     		<%}%>
		</select>
		<input type="text" name="credential_search" id="credential_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("credential_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'credential_search_link')" />
		<a id="credential_search_link" href="javascript:getData('type=credential&id=credential_id&name=credential_id&noDataValue=0&noDataName=No+Credential&search=' + document.getElementById('credential_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
		<input type="submit" class="cssButton" name="myaction" value="Change Credential" <%if (device.getCredentialId() > 0) {%>onclick="if (document.getElementById('credential_id').value != <%=device.getCredentialId()%>) return confirm('Are you sure you want to change the credential?'); else return true;"<%}%> <%if (device.isShowDisabled()) out.write("disabled=\"disabled\"");%>/>
	</td>
</tr>
</table>
<%}%>

<%}
else if (device.isESUDS()) {%>
<div class="tabHead">Client Status Information</div>
<table class="tabDataDisplayBorderNoFixedLayout">            	
<tbody>
<tr>
<td align="center">
<input type="button" class="cssButton" value="View Room Diagnostics" onClick="return redirectWithParams('esudsDiagnostics.i','device_id=<%=deviceId%>');" />
</td>
<td align="center">
<input type="button" class="cssButton" value="View Client Activity Logs" onClick="return redirectWithParams('genericLogs.i','ev_number=<%=device.getDeviceName()%>');" />
</td>
</tr>
</tbody>
</table>

</div>

<div class="innerTable">
<div class="tabHead">Client Remote Control</div>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">            	
<tbody>
<tr>
<td align="center" style="background-color:#C0C0C0; font-weight:bold;">Startup</td>
<td align="center" colspan="3" style="background-color:#C0C0C0; font-weight:bold;">Shutdown</td>
</tr>
<tr>
<td align="center">
<%if (device.isShowDisabled()) {%>
<input type="button" class="cssButtonDisabled" disabled value="Device Reactive" />
<%} else {%>
<input type="button" class="cssButton" name="myaction" value="Device Reactive" onclick="return redirectWithParams(null,'device_id=<%=deviceId%>', new Array(this))" />
<%}%>
</td>
<td align="left">
<input type="radio" id="_shutdown_op_1" name="shutdown_option" value="01" checked>
<label for="_shutdown_op_1">Send Transactions Before Shutdown</label>
<br/>
<input type="radio" id="_shutdown_op_2" name="shutdown_option" value="00">
<label for="_shutdown_op_2">Shutdown Immediately</label>
</td>
<td align="left">
<input type="radio" id="_shutdown_st_1" name="shutdown_state" value="01" checked>
<label for="_shutdown_st_1">Listen For Device Reactivate</label>
<br/>
<input type="radio" id="_shutdown_st_2" name="shutdown_state" value="00">
<label for="_shutdown_st_2">Cease All Device Functions</label>
</td>
<td align="center">
<%if (device.isShowDisabled()) {%>
<input type="button" class="cssButtonDisabled" disabled value="Shutdown" />
<%} else {%>
<input type="button" class="cssButton" name="myaction" value="Shutdown" onClick="if (confirmSubmit('Are you sure you want to shutdown this device?')) return redirectWithParams(null,'device_id=<%=deviceId%>', new Array(this, shutdown_option, shutdown_state)); else return false;" />
<%}%>
</td>
</tr>
</tbody>
</table>

<%}
%>
</div>