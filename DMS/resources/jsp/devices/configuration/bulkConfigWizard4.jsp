<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="errorMessage" scope="request" class="java.lang.String"/>
<jsp:useBean id="form_action" scope="request" class="java.lang.String"/>
<jsp:useBean id="change_configuration" scope="request" class="java.lang.String"/>
<jsp:useBean id="start_serial_head" scope="request" class="java.lang.String"/>
<jsp:useBean id="device_type_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="comm_method" scope="request" class="java.lang.String"/>
<jsp:useBean id="include_device_ids" scope="request" class="java.lang.String"/>
<jsp:useBean id="customer_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="parent_location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="config_template_selections" scope="request" class="java.util.ArrayList"/>
<c:choose>
		<c:when test="${not empty errorMessage}" >
			
			
			<div class="tabDataContent" style="width: 980px;">
			<table width="100%">
			<tr>
			<td align="center">
				${errorMessage}
			</td>
			</tr>
			 <tr>
			  <td align="center" class="gridHeader">
			   <input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
			   <input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = 'home.i';" />			   
			  </td>
			 </tr>
			</table>
			</div>
</c:when>
<c:otherwise>
	<c:choose>
			<c:when test="${not empty change_configuration and change_configuration == '0'}" >
				
				
				<div class="tableContainer">
				<div class="tableHead">
				<div class="tabHeadTxt">Device Configuration Wizard - Page 4</div>
				</div>
				<form method="post" action="${form_action}">
				<input type="hidden" name="start_serial_head" value="${start_serial_head}"  />
				<input type="hidden" name="include_device_ids" value="${include_device_ids}"  />
				<input type="hidden" name="customer_id" value="${customer_id}"  />
				<input type="hidden" name="location_id" value="${location_id}"  />
				<input type="hidden" name="parent_location_id" value="${parent_location_id}"  />
				<input type="hidden" name="device_type_id" value="${device_type_id}"  />
				<input type="hidden" name="comm_method" value="${comm_method}" />
				<input type="hidden" name="edit_mode" value="A"  />
				<input type="hidden" name="change_configuration" value="${change_configuration}"  />
				
				<table class="tabDataDisplayBorder">
					<tr>
					<td align="center" class="status-info">
						<br/>
						Please note that you are not changing device configuration data for selected devices!
						<br/><br/>
					</td>
					</tr>
					<tr class="gridHeader">
						<td align="center">
			    			<input type="reset" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
			    			<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
			    			<input type="submit" class="cssButton" name="action" value="Next &gt;" />
				 		</td>
					</tr>
				</table>
				</form>
				</div>
			</c:when>
			<c:otherwise>
				<div class="tableContainer">
				<div class="tableHead">
				<div class="tabHeadTxt">Device Configuration Wizard - Page 4: Template Selection</div>
				</div>
				<form method="post" action="${form_action}">
				<input type="hidden" name="start_serial_head" value="${start_serial_head}"  />
				<input type="hidden" name="include_device_ids" value="${include_device_ids}"  />
				<input type="hidden" name="customer_id" value="${customer_id}"  />
				<input type="hidden" name="location_id" value="${location_id}"  />
				<input type="hidden" name="parent_location_id" value="${parent_location_id}"  />
				<input type="hidden" name="device_type_id" value="${device_type_id}"  />
				<input type="hidden" name="comm_method" value="${comm_method}" />
				<input type="hidden" name="change_configuration" value="${change_configuration}"  />
				<jsp:useBean id="edit_config_form_action" scope="request" class="java.lang.String"/>
				<input type="hidden" name="edit_config_form_action" value="${edit_config_form_action}"  />
				
				<table class="tabDataDisplayBorder">
				<tbody>
				<tr>
					<td  align=center>
						<br/>Template:
						<select name="templateId" tabindex="1" id="templateId">
							
						<c:forEach var="nvp_item_template" items="${config_template_selections}">
							<option value="${nvp_item_template.value}" label="${nvp_item_template.name}">${nvp_item_template.name}</option>
						</c:forEach>
						</select>&nbsp;
						<input type="text" name="templateId_search" id="templateId_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("templateId_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'templateId_search_link')" />
						<a id="templateId_search_link" href="javascript:getData('type=config_template_by_device_type&id=templateId&name=templateId&deviceTypeId=${device_type_id}&search=' + document.getElementById('templateId_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
						<select name="edit_mode" tabindex="2">
							<option value="A">Advanced</option>
							<option value="B">Basic</option>
						</select><br/><br/>	
			    	</td>
			</tr>
			<tr class="gridHeader">
				<td align="center">
			   		<input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
			   		<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
			   		<input type="submit" class="cssButton" name="action" value="Next &gt;" />
				</td>
			</tr>
		</tbody>
	</table>
			
			</form>
			</div>
	</c:otherwise>
	</c:choose>
</c:otherwise>
</c:choose>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
