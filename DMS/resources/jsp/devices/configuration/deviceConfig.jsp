<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:include page="/jsp/devices/deviceHeaderTabs.jsp" flush="true" />

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
String alertMsg = inputForm.getString("_conf_alert_msg", false);
%>

<div class="tabDataContent" >

<%if (!device.isLegacyKIOSK() && !device.isT2()) {%>
<jsp:include page="tableServerToClient.jsp" flush="true" />
<div class="spacer10"></div>
<%}%>

<form method="post">
<%if (device.isEDGE()){%>
	<input type="hidden" name="plv" value="<%=device.getPropertyListVersion()%>">
<%}%>

<jsp:include page="tableDeviceConfig.jsp" flush="true" />
<div class="spacer10"></div>
<jsp:include page="tablePendingCommand.jsp" flush="true" />
<div class="spacer10"></div>
<jsp:include page="tableCommandHistory.jsp" flush="true" />
<div class="spacer10"></div>

</form>
</div>

<jsp:include page="/jsp/devices/deviceFooter.jsp" flush="true" />
<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<%if (!StringHelper.isBlank(alertMsg)) {%>
<script type="text/javascript">
alert("<%=StringUtils.prepareScript(alertMsg)%>");
</script>
<%}%>