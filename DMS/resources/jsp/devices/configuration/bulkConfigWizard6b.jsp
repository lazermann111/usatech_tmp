<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="errorMessage" scope="request" class="java.lang.String"/>
<jsp:useBean id="form_action" scope="request" class="java.lang.String"/>
<jsp:useBean id="device_type_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="comm_method" scope="request" class="java.lang.String"/>
<jsp:useBean id="include_device_ids" scope="request" class="java.lang.String"/>
<jsp:useBean id="customer_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="parent_location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="zero_counters" scope="request" class="java.lang.String"/>
<jsp:useBean id="pos_pta_templates" scope="request" class="java.util.ArrayList"/>
<jsp:useBean id="params_to_change" scope="request" class="java.lang.String"/>
<jsp:useBean id="params_to_change_request" scope="request" type="java.lang.String"/>
<jsp:useBean id="debug" scope="request" class="java.lang.String"/>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
%>

<c:choose>
		<c:when test="${not empty errorMessage}" >
			
			
			<div class="tabDataContent"  style="width: 980px;">
			<table width="100%">
			<tr>
			<td align="center">
				${errorMessage}
			</td>
			</tr>
			 <tr>
			  <td align="center" class="gridHeader">
			   <input type="button" value="&lt; Back" onClick="javascript:history.go(-1);">
			   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
			   <input type="submit" name="action" value="Next &gt;">
			  </td>
			 </tr>
			</table>
			</div>
</c:when>
<c:otherwise>
			<div class="tableContainer">
			<div class="tableHead">
			<div class="tabHeadTxt">Device Configuration Wizard - Page 7: Configure Payment Types</div>
			</div>
			<form method="post" action="${form_action}">
			<input type="hidden" name="include_device_ids" value="${include_device_ids}"  />
			<input type="hidden" name="params_to_change" value="${StringUtils.encodeForHTMLAttribute(params_to_change)}"  />
			<input type="hidden" name="params_to_change_request" value="${params_to_change_request}"  />
			<input type="hidden" name="customer_id" value="${customer_id}"  />
			<input type="hidden" name="location_id" value="${location_id}"  />
			<input type="hidden" name="parent_location_id" value="${parent_location_id}"  />
			<input type="hidden" name="device_type_id" value="${device_type_id}"  />
			<input type="hidden" name="comm_method" value="${comm_method}" />
			<input type="hidden" name="zero_counters" value="${zero_counters}"  />
			<input type="hidden" name="debug" value="${debug}"  />
			<input type="hidden" name="bulk" value="true" />
			<input type="hidden" name="plv" value="<%=inputForm.getStringSafely("plv", "") %>" />
			<table class="tabDataDisplayBorder">
			<tr>
			  <td width="25%">Payment Type Template</td>
			<td>
				<c:choose>
					<c:when test=" ${empty pos_pta_templates}">
					
						<br/><h3>No templates exist! ${errorMessage}</h3><br/><br/>
					
					</c:when>
						
					<c:otherwise>
						<select name="pos_pta_tmpl_id" id="pos_pta_tmpl_id" >
							<option value="" label="-- Choose a template --" selected="selected">-- Choose a template --</option>
						<c:forEach var="nvp_item_tmpl_id" items="${pos_pta_templates}">
							<option value="${nvp_item_tmpl_id.value}" label="${nvp_item_tmpl_id.name}">${nvp_item_tmpl_id.name}</option>
						</c:forEach>
						</select>
						<input type="text" name="payment_template_search" id="payment_template_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("payment_template_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'payment_template_search_link')" />
						<a id="payment_template_search_link" href="javascript:getData('type=payment_template&id=pos_pta_tmpl_id&name=pos_pta_tmpl_id&search=' + document.getElementById('payment_template_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
					
					</c:otherwise>
				
				</c:choose>
			</td>
			</tr>
			<tr>
				<td>Import Mode</td>
				<td>
				   <input type="radio" name="mode_cd" value="S" /> 1. Safe<br/>
				   <input type="radio" name="mode_cd" value="MS" /> 2. Merge Safe<br/>
				   <input type="radio" name="mode_cd" value="MO" /> 3. Merge Overwrite<br/>
				   <input type="radio" name="mode_cd" value="O" /> 4. Overwrite<br/>
				   <input type="radio" name="mode_cd" value="CO" /> 5. Complete Overwrite<br/>
				   <input type="radio" name="mode_cd" value="" checked="checked" /> X. Do Not Import A Payment Template<br/>
				</td>
			</tr>
			<tr>
				<td>Order Mode</td>
				<td>
					<select name="order_cd">
						<option value="AE">Above Error Bin</option>
						<option value="T">Top</option>
						<option value="B">Bottom</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="nowrap">Set Terminal Code to Device Serial #</td>
				<td><input type="checkbox" name="set_terminal_cd_to_serial" value="Y" /></td>
			</tr>
			<tr>
				<td class="nowrap">Only Import "No Two-Tier Pricing" Setting</td>
				<td><input type="checkbox" name="only_no_two_tier_pricing" value="Y" /></td>
			</tr>
			<tr>
			  <td colspan="2">
			  <b>Import Mode</b>
			   <ul>
			    <li><b>Safe Mode</b>
			        <ul><li>
					Will not deactivate or alter any existing payment types.
					</li><li>
					May create new payment types if none exist for each <b>Payment Action Type / Payment Entry Method category</b> based on the selected template.
					</li></ul>
				</li>
				<li><b>Merge Safe Mode</b>
			        <ul><li>
					Will not deactivate or alter any existing payment types.
					</li><li>
					May create new payment types if none exist for each <b>Payment Type category</b> based on the selected template.
					</li></ul>
				</li>
				<li><b>Merge Overwrite Mode</b><br/>
			        <ul><li>
					May deactivate an existing payment type if one exists for each <b>Payment Type category</b> being imported.
					</li><li>
					Will create new payment types based on the selected template.
					</li></ul>
				</li>
				<li><b>Overwrite Mode</b><br/>
			        <ul><li>
					Will deactivate any existing payment types for each <b>Payment Action Type / Payment Entry Method category</b> being imported.
					</li><li>
					Will create new payment types based on the selected template.
					</li></ul>
				</li>
				<li><b>Complete Overwrite Mode</b><br/>
			        <ul><li>
					Will deactivate <b>any</b> existing payment types.
					</li><li>
					Will create new payment types based on the selected template.
					</li></ul>
				</li>
			   </ul>
			   <b>Order Mode</b>
				<table class="noBorder">
					<tr><td>&nbsp;&nbsp;</td><td><b>Above Error Bin:</b></td><td>Inserts the new payment types above the first Error Bin and below all the other payment types.</td></tr>
					<tr><td>&nbsp;&nbsp;</td><td><b>Top:</b></td><td>Inserts the new payment types above all the existing payment types.</td></tr>
					<tr><td>&nbsp;&nbsp;</td><td><b>Bottom:</b></td><td>Adds the new payment types below all the existing payment types.</td></tr>
				</table>
			  </td>
			 </tr>
			<tr class="gridHeader">
				<td colspan="2" align="center">
		   			<input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
		   			<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
		   			<input type="submit" class="cssButton" name="action" value="Next &gt;" />		
		  		</td>
		 	</tr>
		</table>
		</form>
		</div>
</c:otherwise>
</c:choose>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
