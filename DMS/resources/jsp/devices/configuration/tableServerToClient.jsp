<%@page import="java.util.List"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.model.ActionPayloadTriple"%>
<%@page import="com.usatech.layers.common.model.Device"%>

<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
boolean bulk = inputForm.getBoolean("bulk", false, false);
if (!bulk) {
%>
<form method="post">
<%
}
int deviceTypeId, plv, protocolRevision;
boolean deviceDisabled;
String commMethodCd;
if (bulk) {
	deviceTypeId = inputForm.getInt("device_type_id", true, -1);
	deviceDisabled = false;
	plv = inputForm.getInt("plv", false, -1);
	if (plv < 0)
		plv = 9999999;
	protocolRevision = 9999999;
	commMethodCd = inputForm.getStringSafely("comm_method", "");
} else {
	Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
	deviceTypeId = device.getTypeId();
	deviceDisabled = device.isShowDisabled();
	plv = device.getPropertyListVersion();
	protocolRevision = device.getProtocolRevision();
	commMethodCd = device.getCommMethodCd();
}

if (deviceTypeId == DeviceType.EDGE.getValue()){
	List<ActionPayloadTriple> actionPayloads = DeviceActions.getGrsaActionPayloads(deviceTypeId, plv, protocolRevision);
	
    StringBuilder grsaOption = new StringBuilder();
    StringBuilder grsaScript = new StringBuilder();

    for (ActionPayloadTriple actionPayload : actionPayloads) {
        grsaOption.append("<option value=\"").append(actionPayload.getActionValue()).append("\">").append(actionPayload.getActionDesc()).append("</option>\n");
        grsaScript.append("if (action.value == \"").append(actionPayload.getActionValue()).append("\") {document.getElementById(\"payload\").value = \"").append(actionPayload.getPayloadValue()).append("\"; return;}\n");
    }

%>
<div class="innerTable" align="center">
<div class="tabHead">Server To Client Requests</div>
<div class="spacer5"></div>
<script type="text/javascript" defer="defer">
function populatePayload(action) {
	<%=grsaScript.toString()%>
}
</script>

<select name="generic_response_s2c" onchange="populatePayload(this);">
<%=grsaOption.toString()%>
</select>
<input type="text" class="cssText" id="payload" name="payload" size="50" maxlength="1024" />
<input type="submit" class="cssButton" name="myaction" value="Queue Command" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/>
</div>
<%} else if (deviceTypeId == DeviceType.ESUDS.getValue()) {%>
<div class="innerTable">
<div class="tabHead">Server To Client Requests</div>
<div class="spacer5"></div>
<table class="tabDataDisplay">
<tr>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Room Layout" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Software Reboot" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Enable Logins" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Dump Transactions" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Synch Time" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
</tr>
<tr>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Client Version" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Hardware Reboot" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Send Logs" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Machine Diagnostics" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
<td align="center"><input type="submit" class="cssButton" name="myaction" value="Clear Card Cache" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
</tr>
</table>
</div>
<%} else if(deviceTypeId == DeviceType.KIOSK.getValue()) {%>
<div class="innerTable" align="center">
<div class="tabHead">Server To Client Requests</div>
<div class="spacer5"></div>
<table class="tabDataDisplay">
<tr>
	<td align="center"><input type="submit" class="cssButton" name="myaction" value="External File Transfer" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>
	<% if(bulk || "G".equalsIgnoreCase(commMethodCd)){%>
	<td align="center"><input type="submit" class="cssButton" name="myaction" value="Download GPRS Info" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/></td>	
	<%} %>
</tr>
</table>
</div>
<%} else if (deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.G4.getValue() || deviceTypeId == DeviceType.MEI.getValue()) {%>
<div class="innerTable">
<div class="tabHead">Server To Client Requests</div>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
<tr>
<td align="center">
<%
if (deviceTypeId == DeviceType.MEI.getValue()) {%>
<input type="submit" class="cssButton" name="myaction" value="Upload Config" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/>
<%} else {%>
<input type="submit" class="cssButton" name="myaction" value="Firmware Version" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/>
</td>
<td align="center">
<input type="submit" class="cssButton" name="myaction" value="Upload Config" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/>
</td>
<td align="center">
<input type="submit" class="cssButton" name="myaction" value="Wavecom Modem Info" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/>
</td>
</tr>
<tr>
<td align="center">
<input type="submit" class="cssButton" name="myaction" value="Boot Load/App Rev" <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/>
</td>
<td align="center">
<input type="submit" class="cssButton" name="myaction" value="Bezel Info"  <%if (deviceDisabled) out.write("disabled=\"disabled\"");%>/>
</td>
<td align="center">
<input type="submit" class="cssButton" name="myaction" value="External File Transfer" <%if (deviceDisabled || "C".equalsIgnoreCase(commMethodCd)) out.write("disabled=\"disabled\"");%>/>
<%}%>
</td>
</tr>
</table>
</div>
<%
}
if (!bulk) {
%>
</form>
<%}%>