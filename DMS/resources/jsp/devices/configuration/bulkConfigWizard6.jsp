<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="errorMessage" scope="request" class="java.lang.String"/>
<jsp:useBean id="device_type_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="comm_method" scope="request" class="java.lang.String"/>
<jsp:useBean id="include_device_ids" scope="request" class="java.lang.String"/>
<jsp:useBean id="customer_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="parent_location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="params_to_change" scope="request" type="java.lang.String"/>
<jsp:useBean id="params_to_change_request" scope="request" type="java.lang.String"/>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
%>

<c:choose>
		<c:when test="${not empty errorMessage}" >
			
			<div class="tabDataContent"  style="width: 980px;">
			<table width="100%">
			<tr>
			<td align="center">
				${errorMessage}
			</td>
			</tr>
			 <tr>
			  <td align="center" class="gridHeader">
			   <input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);">
			   <input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/';">
			   <input type="submit" class="cssButton" name="action" value="Next &gt;">
			  </td>
			 </tr>
			</table>
			</div>
</c:when>
<c:otherwise>
			<div class="tableContainer">
			<div class="tableHead">
			<div class="tabHeadTxt">Device Configuration Wizard - Page 6: Other Settings</div>
			</div>
			<form method="post" action="bulkConfigWizard6b.i">
			<input type="hidden" name="include_device_ids" value="${include_device_ids}"  />
			<input type="hidden" name="params_to_change" value="<%=StringUtils.prepareCDATA(params_to_change) %>"  />
			<input type="hidden" name="params_to_change_request" value="${params_to_change_request}"  />
			<input type="hidden" name="customer_id" value="${customer_id}"  />
			<input type="hidden" name="location_id" value="${location_id}"  />
			<input type="hidden" name="parent_location_id" value="${parent_location_id}"  />
			<input type="hidden" name="device_type_id" value="${device_type_id}"  />
			<input type="hidden" name="comm_method" value="${comm_method}" />
			<input type="hidden" name="plv" value="<%=inputForm.getStringSafely("plv", "") %>" />
			<table class="tabDataDisplayBorder">			
			<tr>
  				<td style="font-size: 12pt;">Do you want to Zero Out Device Counters?</td><td style="font-size: 12pt;"><input type="radio" name="zero_counters" value="1">Yes <input type="radio" name="zero_counters" value="0" checked>No</td>
 			</tr>
 
 			<tr>
  				<td style="font-size: 12pt;">Turn Debugging ON?</td><td style="font-size: 12pt"><input type="radio" name="debug" value="1">Yes <input type="radio" name="debug" value="0" checked>No</td>
 			</tr>
  			
			<tr class="gridHeader">
				<td colspan="2" align="center">
		   			<input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
		   			<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
		   			<input type="submit" class="cssButton" name="action" value="Next &gt;" />
		  		</td>
		 	</tr>
		</table>
		</form>
		</div>
</c:otherwise>
</c:choose>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
