<%@page import="java.util.Calendar"%>
<%@page import="com.usatech.layers.common.util.DeviceUtils"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.lang.Holder"%>
<%@page import="java.math.BigDecimal"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.model.ConfigTemplateSetting"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%@page import="com.usatech.dms.action.ConfigFileActions"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<script type="text/javascript">
function enableOptions(options, values, fn) {
	for(var i = 0; i < options.length; i++) {
		var valid = (values && values.contains && values.contains(fn ? fn(options[i].value) : options[i].value));
		options[i].disabled = !valid;
		if(!valid && options[i].selected)
			$(options[i].parentElement).addClass("unsupported");
	}
}
function checkSupport(select) {
	select = $(select);
	if(select.hasClass("unsupported") && select.selectedIndex >= 0 && !select.options[select.selectedIndex].disabled)
		select.removeClass("unsupported");
}
</script>
<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Results pendingCfgCommandsCountResult = (Results) inputForm.getAttribute("pendingCfgCommandsCount");
boolean hasPendingCfgCommands = pendingCfgCommandsCountResult != null && pendingCfgCommandsCountResult.next();
if (hasPendingCfgCommands) {
	hasPendingCfgCommands = ((BigDecimal)pendingCfgCommandsCountResult.getValue("PENDING_CFG_COMMANDS_COUNT")).longValue() > 0L;
}
String configType = inputForm.getStringSafely("configType", "");
String edit_mode = inputForm.getStringSafely("edit_mode", "A");
int plv = inputForm.getInt("plv", false, -1);
boolean isRawEditMode = "R".equalsIgnoreCase(edit_mode);
boolean isBasicEditor = edit_mode != null && edit_mode.toUpperCase().startsWith(("B"));
String ssn = inputForm.getStringSafely("ssn", "");
long device_id = inputForm.getLong("device_id", false, -1);
int device_type_id = inputForm.getInt("device_type_id", false, -1);
boolean isEdge = device_type_id == DeviceType.EDGE.getValue();
boolean isGx = device_type_id == DeviceType.GX.getValue() || device_type_id == DeviceType.G4.getValue();
boolean isMEI = device_type_id == DeviceType.MEI.getValue();
boolean isKiosk = device_type_id == DeviceType.KIOSK.getValue();
String action = inputForm.getStringSafely("myaction", "Edit");
LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = (LinkedHashMap<String, ConfigTemplateSetting>) inputForm.getAttribute("defaultSettingData");
LinkedHashMap<String, String> targetSettingData = (LinkedHashMap<String, String>) inputForm.getAttribute("targetSettingData");
LinkedHashMap<String, String> currentSettingData = (LinkedHashMap<String, String>) inputForm.getAttribute("currentSettingData");
if (currentSettingData == null)
	currentSettingData = targetSettingData;
boolean isPublicPC = isKiosk && !isRawEditMode && currentSettingData.get("Public_PC_Version") != null;
String file_name = inputForm.getStringSafely("file_name", "");
String template_name = inputForm.getStringSafely("template_name", "");
String templateType = inputForm.getStringSafely("template_type", "");
int templateTypeId = inputForm.getInt("template_type_id", false, -1);
int deviceUtcOffsetMin;
if (device_id > 0 && isEdge) {
	Device device = DeviceUtils.generateDevice(device_id);
	deviceUtcOffsetMin = device.getDeviceUtcOffsetMin();
} else {
	deviceUtcOffsetMin = -Calendar.getInstance().get(Calendar.ZONE_OFFSET) / 1000 / 60;
}

StringBuilder pageTitle;
boolean isBulk = false;
boolean isTemplate = false;
if ("bulk".equalsIgnoreCase(configType)) {
	isBulk = true;
	pageTitle = new StringBuilder("Device Configuration Wizard - Page 5: Parameter Selection");
	if (!StringHelper.isBlank(file_name))
		pageTitle.append(" - ").append(file_name);
} else if ("template".equalsIgnoreCase(configType)) {
	isTemplate = true;
	pageTitle = new StringBuilder("Edit Template - ").append(template_name);
	file_name = template_name;
} else {
	pageTitle = new StringBuilder(action).append(" Configuration - ").append(ssn);
	if (!StringHelper.isBlank(template_name))
		pageTitle.append(" - ").append(template_name);
	else if (!StringHelper.isBlank(file_name))
		pageTitle.append(" - ").append(file_name);
}
pageTitle.append(isRawEditMode ? " (raw)" : isBasicEditor ? " (basic)" : " (advanced)");
if (!isRawEditMode)
	pageTitle.append(" <font color=\"red\">*</font>denotes required parameter");

String formAction;
if (isBulk)
	formAction = inputForm.getString("edit_config_form_action", true);
else if (isTemplate)
	formAction = new StringBuilder("editTmplConfigFunc").append(inputForm.getString("templateCategory", true)).append(".i").toString();
else if (isGx || isMEI)
	formAction = "editMapConfigFunc.i";
else if (isKiosk)
	formAction = "editIniConfigFunc.i";
else
	formAction = "editIniConfigFuncEdge.i";

if (isPublicPC) {
	ConfigFileActions.decryptDeviceSettingValue(defaultSettingData, targetSettingData, "OpenAPIPassword");
	ConfigFileActions.decryptDeviceSettingValue(defaultSettingData, targetSettingData, "MFPAdminPassword");
	ConfigFileActions.decryptDeviceSettingValue(defaultSettingData, targetSettingData, "SNMPCommunity");
}
%>

<div class="tableDataContainer">
<form name="editConfig" action="<%=formAction%>" method="post" onsubmit="return validateForm(this) && validateDeviceConfiguration(this);"<%if (isBulk) out.write(" bulk=\"true\"");%>>
<input name="device_type_id" value="<%=device_type_id%>" type="hidden" />
<input name="file_name" value="<%=StringUtils.prepareCDATA(file_name)%>" type="hidden" />
<input name="edit_mode" value="<%=edit_mode%>" type="hidden" />
<input name="plv" id="plv" value="<%=plv%>" type="hidden" />

<% if (isBulk) { %>
	<input name="parent_location_id" value="<%=inputForm.getStringSafely("parent_location_id", "")%>" type="hidden"/>
	<input name="change_configuration" value="<%=inputForm.getStringSafely("change_configuration", "")%>" type="hidden"/>
	<input name="include_device_ids" value="<%=inputForm.getStringSafely("include_device_ids", "")%>" type="hidden"/>
	<input name="customer_id" value="<%=inputForm.getStringSafely("customer_id", "")%>" type="hidden"/>
	<input name="start_serial_head" value="<%=inputForm.getStringSafely("start_serial_head", "")%>" type="hidden"/>
	<input name="location_id" value="<%=inputForm.getStringSafely("location_id", "")%>" type="hidden"/>
	<input name="comm_method" value="<%=inputForm.getStringSafely("comm_method", "")%>" type="hidden"/>
<%} else { %>
	<input name="ssn" value="<%=ssn%>" type="hidden" />
	<input name="ev_number" value="<%=inputForm.getStringSafely("ev_number", "")%>" type="hidden" />
	<input name="myaction" value="<%=action%>" type="hidden" />
	<input name="device_id" value="<%=device_id%>" type="hidden" />
	<input name="template" value="<%=isTemplate%>" type="hidden" />
<%} %>

<%if (isTemplate) { %>
<input name="template_id" value="<%=inputForm.getLong("template_id", false, -1)%>" type="hidden" />
<input name="template_name" value="<%=StringUtils.encodeForHTMLAttribute(template_name)%>" type="hidden" />
<%} %>

<%if (isKiosk) { %>
	<input name="publicPc" value="<%=isPublicPC%>" type="hidden"/>
<%} %>
	
<div class="innerTable">

<div class="tableDataHead">
	<div class="txtWhiteBold" align="left">&nbsp;&nbsp;<%=pageTitle.toString()%>&nbsp;&nbsp;</div>
</div>

<%
if ((isEdge || isGx || isMEI || isKiosk) && !isRawEditMode) {
%>
	<table class="tabDataDisplayNoBorder">
			<tr>
				<td colspan="2">
				<div id="section_index"></div>
				<div class="gridHeader">
				<table class="tabDataDisplayNoBorder">
					<tr>
					<td align="left" valign="top" style="width: 110px;">
						<b>Section Index:</b>
					</td>
					<td align="left">
					<%
					int currentCategoryId = -1;
					for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
						ConfigTemplateSetting defaultSetting = entry.getValue();
						String key = defaultSetting.getKey();
						boolean isServerOnly = defaultSetting.isServerOnly();
						if (!targetSettingData.containsKey(key) && !isServerOnly)
							continue;
						if (isBasicEditor && "N".equalsIgnoreCase(defaultSetting.getCategoryDisplay()))
							continue;
						if (!isPublicPC && "PublicPC".equalsIgnoreCase(defaultSetting.getSubtypeCategory()))
							continue;
						int categoryId = defaultSetting.getCategoryId();
						String categoryName = defaultSetting.getCategoryName();
						if (StringHelper.isBlank(categoryName) || categoryId == currentCategoryId)
							continue;
						currentCategoryId = categoryId; %>
						<a href="#category_<%=categoryId%>"><%=categoryName%></a><br />	
					<%} %>
					</td>
					</tr>
				</table>
				</div>
				</td>
			</tr>
	</table>

<%} %>

<%if (isBulk) { %>
<div class="gridHeader">
	<input type="checkbox" onclick="check_all(this);" /> <b>Check All Properties</b>
	<br /><br />
	<font color="green"><b>* Please note: in order for the changes to take effect you must check the checkboxes of the properties you want to change</b></font>
</div>
<%} else {
	if ((isEdge || isKiosk) && !isTemplate) { %>
	<table cellspacing="0" cellpadding="5" align="center">
	<tr>
	<td valign="top" align="center">
		<input type="submit" class="cssButton" name="action" value="Save and Send" />
	</td>
	<td valign="top" align="center" rowspan="2">
		<input type="button" class="cssButton" value="Cancel" onClick="javascript:window.location='deviceConfig.i?device_id=<%=device_id %>'" />
	</td>
	</tr>
	</table>
	<%}%>

 <%if ((isEdge || isGx || isMEI) && !isTemplate) {%>
	<div align="center">
	<input type="checkbox" onClick="toggle_all_defaults(this);"/>Set All To Defaults<br />
	<font color="green">* Checkboxes are used to set default values</font>
	</div>
 <%} %>

	<%if (hasPendingCfgCommands) {%>
	<table cellspacing="0" cellpadding="5" align="center" width="100%" style="text-align: center;">
	<tr>
		<td class="warn-solid">
			Note: configuration updates are pending
		</td>
	</tr>
	</table>
	<%} %>

<div class="spacer5"></div>
<%}%>

<%StringBuilder hiddenInputs = new StringBuilder(); %>
<table class="tabDataDisplayBorder">
	<col width="50%" />
	<col width="50%" />

<%
int counter = 0;
if (isRawEditMode) {
%>
<tr>
	<td align="center" colspan="2">
	<textarea name="file_data" rows="20" style="width:99%;"><%=inputForm.getStringSafely("file_data", "")%></textarea>
	</td>
</tr>

<%
} else {
int categoryId = 0;
String currentCategoryName = "";
Holder<Boolean> optionalHolder = new Holder<Boolean>();
for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
	ConfigTemplateSetting defaultSetting = entry.getValue();
	String key = defaultSetting.getKey();
	boolean isServerOnly = defaultSetting.isServerOnly();
	
	if (!defaultSetting.isSupported() && !isTemplate && !isBulk)
		continue;
	if (isTemplate && !targetSettingData.containsKey(key) && !isServerOnly)
		continue;
	if (!isPublicPC && "PublicPC".equalsIgnoreCase(defaultSetting.getSubtypeCategory()))
		continue;
	
	if (isGx) {
		int offSet = ConvertUtils.getIntSafely(key, -1);
		if (isGx && DevicesConstants.GX_MAP_PROTECTED_FIELDS.contains(offSet))
			continue;
	}
		
	String label = defaultSetting.getLabel();
	String description = defaultSetting.getDescription();
	String lineDesc = defaultSetting.getLineDesc();
	String editorType = defaultSetting.getEditorType();
	boolean required = defaultSetting.isRequired();
	counter = defaultSetting.getCounter();
	boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
	String defaultValue = defaultSetting.getConfigTemplateSettingValue();
	if (defaultValue == null)
		defaultValue = "";
	else if (storedInPennies && StringHelper.isNumeric(defaultValue))
		defaultValue = String.format("%.2f", new BigDecimal(defaultValue).divide(BigDecimal.valueOf(100)));
	
	String value = targetSettingData.get(key);
	if (value == null)
		value = "";
	else if (storedInPennies && StringHelper.isNumeric(value))
		value = String.format("%.2f", new BigDecimal(value).divide(BigDecimal.valueOf(100)));
	
	if ("SCHEDULE".equalsIgnoreCase(editorType) && value.startsWith("I^"))
		value = WebHelper.buildIntervalScheduleValue(value, deviceUtcOffsetMin);
	
	String originalValue = currentSettingData.get(key);
	if (originalValue == null)
		originalValue = "";
	else if (storedInPennies && StringHelper.isNumeric(originalValue))
		originalValue = String.format("%.2f", new BigDecimal(originalValue).divide(BigDecimal.valueOf(100)));
	
	if (!isBulk)
		hiddenInputs.append("<input name=\"cfg_original_").append(key).append("\" id=\"cfg_original_").append(key).append("\" value=\"").append(originalValue).append("\" type=\"hidden\" />\n");
	
	if (isKiosk) {
		if (!isPublicPC && "PublicPC".equalsIgnoreCase(defaultSetting.getSubtypeCategory()))
			continue;
				
		if (required && StringHelper.isBlank(value))
			value = defaultValue;
		String regex = defaultSetting.getRegex();
		hiddenInputs.append("<input name=\"regex_").append(key).append("\" id=\"regex_").append(key).append("\" value=\"").append(regex).append("\" type=\"hidden\" />\n");
	}
	
	if ((isEdge || isGx || isMEI || isKiosk) && (!isBasicEditor || !"N".equalsIgnoreCase(defaultSetting.getCategoryDisplay())) && (isServerOnly || targetSettingData.containsKey(key))) {
		String categoryName = defaultSetting.getCategoryName();
		if (!StringHelper.isBlank(categoryName) && !categoryName.equals(currentCategoryName)) {
			currentCategoryName = categoryName;
			categoryId = defaultSetting.getCategoryId();
%>		
			<tr>
				<td class="tabHead" colspan="2">
					<div id="category_<%=categoryId%>">
					<%if ((isEdge || isGx || isMEI) && !isTemplate || isBulk && isKiosk) { %>
					<input type="checkbox" name="category" value="<%=categoryId%>" onClick="<%=isBulk ? "check_category(this);" : "toggle_category_defaults(this);"%>" />
					<%} %>
					<b><%=categoryName%></b>
					<a href="#section_index"><img src="/images/arrow-up-icon.png" style="vertical-align:bottom; border:0;"/></a>
					</div>
				</td>
			</tr>
<%
		}
	}
	
	if (isBasicEditor && "N".equalsIgnoreCase(defaultSetting.getDisplay()) || !isServerOnly && !targetSettingData.containsKey(key))
		hiddenInputs.append("<input name=\"cfg_field_").append(key).append("\" id=\"cfg_field_").append(counter).append("\" value=\"").append(originalValue).append("\" type=\"hidden\" />\n");
	else {
		hiddenInputs.append("<input name=\"cfg_editor_").append(key).append("\" id=\"cfg_editor_").append(key).append("\" value=\"").append(editorType).append("\" type=\"hidden\" />\n");
		if (!isBulk) {
			hiddenInputs.append("<input name=\"cfg_default_").append(key).append("\" value=\"").append(defaultValue).append("\" type=\"hidden\" />\n");
			hiddenInputs.append("<input name=\"cfg_holder_").append(key).append("\" value=\"").append(value).append("\" type=\"hidden\" />\n");
		}
%>
		<tr bgcolor="#FEFEFE">
			<td valign="top">
			<%if (isBulk) { %>
				<input type="checkbox" id="cb_<%=counter %>_cat_<%=categoryId %>" value="<%=key %>" name="param_to_change" />
			<%} else { %>
				<%if ((isEdge || isGx || isMEI) && !isTemplate) { %>
				<input type="checkbox" name="cat_<%=categoryId%>" id="cb_<%=counter%>" value="<%=key%>" onClick="toggle_default(this);"/>
				<%} %>
			<%} %>
				<b><%=label%><%if ((isEdge || isServerOnly) && !key.equals(label)) {%> [<%=key%>]<%}%><%
				optionalHolder.setValue(null);
				String inputHtml = WebHelper.generateInput(defaultSetting, value, device_type_id, plv, true, defaultValue, isBulk, optionalHolder, "");
				if (required || optionalHolder.getValue() == Boolean.FALSE) out.write("<font color=\"red\">*</font>");%></b>
			</td>
			<td valign="top" style="vertical-align:top;"><%=inputHtml%></td>
		</tr>
		<tr>
			<td colspan="2" bgcolor="#EEEEEE">
				<%=description%><%if ((isEdge || isGx || isMEI) && !isTemplate && !isServerOnly) out.write(WebHelper.generateDefaultValueString(defaultSetting, defaultValue, description));%>
				<%if (!StringHelper.isBlank(lineDesc)) { %>
				<br /><%=lineDesc %>
				<%} %>
			</td>
		</tr>
	<%} %>
<%}}%>

</table>

<%if (hiddenInputs.length() > 0) out.write(hiddenInputs.toString());%>
<input type="hidden" id="elem_count" value="<%=counter%>"/>

<div class="spacer5"></div>
<% if (isGx) { %>
<div class="spacer10"></div>
<div align="left" class="note">
* Please note: to turn ON DEX Read and Send, set the following properties together: DEX Read and Send Time, DEX Read and Send Interval Minutes, Server Override: Call-in Time Window Start - DEX, Server Override: Call-in Time Window Hours - DEX
</div>
<div class="spacer10"></div>
<% } %>
<div align="center">
<% if (isBulk) { %>
<table cellspacing="0" cellpadding="2" border="0" width="100%">
	<tr>
		<td align="center" class="gridHeader">
			<input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
			<input type="button" class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';"/>
			<input type="submit" class="cssButton" name="action" value="Next &gt;"/>
		</td>
	</tr>
</table>
<%} else if (isTemplate) { %>
	<table cellspacing="0" cellpadding="5">
	<tr>
	<td align="center" colspan="2">
	<%if (isEdge) { %>
		<% if ("custom".equalsIgnoreCase(templateType)) { %>
		<input type="submit" name="action" class="cssButton" value="Save" />
		<%} %>
		<input type="submit" name="action" class="cssButton" value="Copy To" />
		<select name="new_file_type" tabindex="1">
			<option selected="selected" value="23">Edge Custom Configuration Template</option>
			<option value="22">Edge Default Configuration Template</option>
		</select>
		<input type="text" name="new_name" size="30" maxlength="50"/>
	<%} else { %>
		<input type="submit" name="action" class="cssButton" value="Save" />
		<input type="submit" name="action" class="cssButton" value="Copy To" />
		<input type="text" name="new_name" />
 	<%} %>
 		<input type="button" class="cssButton" value="Cancel" onClick="window.location = '/templateMenu.i';"/>
 		<%if (templateTypeId == 2) {%>
 		<input type="submit" class="cssButton" name="action" value="Delete Template" onClick="return confirmSubmit('Are you sure you want to delete this template?');" />
 		<%}%>
 	</td>
 	</tr>
 	</table>
<% } else { %>
<table cellspacing="0" cellpadding="5" align="center"<%if (isGx) out.write(" class=\"border\"");%>>
	<tr>
		<td valign="top" align="center">
		<input type="submit" class="cssButton" name="action" value="Save and Send"/>
		</td>
	 	<td valign="top" align="center" rowspan="2">
		<input type="button" class="cssButton" value="Cancel" onClick="javascript:window.location='deviceConfig.i?device_id=<%=device_id %>'" />
		</td>
	</tr>
	<%if (isGx) {%>
	<tr>
		<td>
		<input type="radio" name="send" value="changes" checked="checked"/>Changes
		<input type="radio" name="send" value="complete"/>Complete<br/>
		<input type="checkbox" name="comms" value="Y"/>Communication Settings<br/>
		<input type="checkbox" name="counters" value="Y"/>Zero Out Counters<br/>
		</td>
	</tr>
	<%} %>
</table>
<%}%>
</div>

<div class="spacer5"></div>
</div>

</form>
</div>

<script defer="defer" type="text/javascript">

<%if (isBulk) { %>

function check_category(cb) {
	var theForm = document.forms["editConfig"];
	if (theForm.param_to_change) {
		for (i = 0; i < theForm.param_to_change.length; i++){
			if (theForm.param_to_change[i].id.indexOf('_cat_' + cb.value) > -1)
				theForm.param_to_change[i].checked = cb.checked;
		}
	}
}

function check_all(cb) {
	var theForm = document.forms["editConfig"];
	if (theForm.category) {
		for (i = 0; i < theForm.category.length; i++){
			theForm.category[i].checked = cb.checked;
		}
	}
	if (theForm.param_to_change) {
		for (i = 0; i < theForm.param_to_change.length; i++){
			theForm.param_to_change[i].checked = cb.checked;
		}
	}
}

<%} else {%>

function toggle_default(cb) {
	var newValue;
	var field_elem = document.getElementsByName('cfg_field_' + cb.value);
	var default_elem = document.getElementsByName('cfg_default_' + cb.value)[0];
	var holder_elem = document.getElementsByName('cfg_holder_' + cb.value)[0];
	var editorType = document.getElementById('cfg_editor_' + cb.value).value;
	if (editorType == "BITMAP") {
		if (cb.checked) {
			var currentValue = 0;
			for (var i=0; i<field_elem.length; i++) {
				if (field_elem[i].checked)
					currentValue += parseInt(field_elem[i].value);
			}
			if (currentValue != parseInt(default_elem.value))
				holder_elem.value = currentValue;
			newValue = parseInt(default_elem.value);
		} else
			newValue = parseInt(holder_elem.value);
		for (var i=0; i<field_elem.length; i++) {
			field_elem[i].checked = (parseInt(field_elem[i].value) & newValue) == parseInt(field_elem[i].value);
		}
	} else if (editorType == "SCHEDULE") {
		var scheduleType = field_elem[0].value;
		if (cb.checked)
			newValue = default_elem.value;
		else
			newValue = document.getElementById('cfg_original_' + cb.value).value;
		var valueParts = newValue.split("^");
		scheduleType = valueParts[0];
		field_elem[0].value = scheduleType;
		change_schedule(field_elem[0]);
		if (scheduleType != "" && valueParts.length > 1) {
			var time = "0300";
			if (scheduleType == "I") {
				if (valueParts.length > 2 && !isNaN(valueParts[1]) && !isNaN(valueParts[2])) {
					var utcOffsetSec = Number(valueParts[1]);
					var utcLocalOffsetMin = Math.floor((utcOffsetSec - <%=deviceUtcOffsetMin%> * 60) / 60) % 1440;
					if (utcLocalOffsetMin < 0)
						utcLocalOffsetMin += 1440;
					var hours = Math.floor(utcLocalOffsetMin / 60);
					time = (hours < 10 ? "0" : "") + hours;
					var minutes = utcLocalOffsetMin % 60;
					time += (minutes < 10 ? "0" : "") + minutes;
					var intervalMin = Math.floor(Number(valueParts[2]) / 60);
					document.getElementsByName("sch_min_" + cb.value)[0].value = intervalMin;
				}
			}
			if (scheduleType == "D" || scheduleType == "W") {
				time = valueParts[1];
				if (scheduleType == "W" && valueParts.length > 2) {
					var selectedDays = valueParts[2].split("|");
					var days = document.getElementsByName("sch_day_" + cb.value);
					for (var i = 0; i < days.length; i++) {
						days[i].checked = false;
						for (var j = 0; j < selectedDays.length; j++) {
							if (days[i].value == selectedDays[j]) {
								days[i].checked = true;
								break;
							}
						}
					}
				}
			}
			document.getElementsByName("sch_time_" + cb.value)[0].value = time;
		}
	} else {
		if (cb.checked) {
			if (field_elem[0].value != default_elem.value)
				holder_elem.value = field_elem[0].value;
			field_elem[0].value = default_elem.value;
		} else
			field_elem[0].value = holder_elem.value;
	}
}

function toggle_category_defaults(cb) {
	var theForm = document.forms["editConfig"];
	var category = eval("theForm.cat_" + cb.value);
	if (category) {
		if(category.length) {
			for (var i = 0; i < category.length; i++) {
				category[i].checked = cb.checked;
				toggle_default(category[i]);
			}
		} else {
			category.checked = cb.checked;
			toggle_default(category);
		}
	}
}

function toggle_all_defaults(cb) {
	var theForm = document.forms["editConfig"];
	if (theForm.category) {
		for (var i = 0; i < theForm.category.length; i++){
			theForm.category[i].checked = cb.checked;
			toggle_category_defaults(theForm.category[i]);
		}
	} else {
		var elem_count = document.getElementById("elem_count");
		for (var i = 1; i <= elem_count.value; i++){
			var check = document.getElementById("cb_" + i);
			if (check != null) {
				check.checked = cb.checked;
				toggle_default(check);
			}
		}
	}
}

<%} %>

function change_schedule(item) {
	var plv = document.getElementById('plv').value;
	document.getElementById(item.name.replace("cfg_field_", "span_time_")).style.display = item.value == "" ? "none" : "inline-block";
	var schedules = (plv && plv > 0 && plv < 20) ? ["I", "W"] : ["I", "W", "E"];
	for (var i = 0; i < schedules.length; i++) {
		if (document.getElementById(item.name.replace("cfg_field_", "span_" + schedules[i] + "_"))) {
			document.getElementById(item.name.replace("cfg_field_", "span_" + schedules[i] + "_")).style.display = item.value == schedules[i] ? "inline-block" : "none";
		}
	}
	if (item.value == 'E' && document.getElementById(item.name.replace("cfg_field_", "span_time_"))) {
		document.getElementById(item.name.replace("cfg_field_", "span_time_")).style.display = "none";
	}
	var timelist_elems = document.getElementsByName(item.name.replace("cfg_field_", "sch_timelist_"));
	for (var i=timelist_elems.length-1; i>=0; i--) {
		if (!timelist_elems[i].value) {
			var tr_elem = timelist_elems[i].parentNode;
			tr_elem.parentNode.remove(tr_elem);
			this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] = this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] - 1;
		}
	}
	if (document.getElementById(item.name.replace("cfg_field_", "add_time_button_"))) {
		document.getElementById(item.name.replace("cfg_field_", "add_time_button_")).disabled=(this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] >= 6);
	}
}

function validateDeviceConfiguration(theForm) {
	var elem = theForm.elements["cfg_field_CALL_IN_TIME_WINDOW_HOURS"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 23)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 23. Please correct.");
		return false;
	}
	var elem = theForm.elements["cfg_field_CALL_IN_TIME_WINDOW_HOURS_ACTIVATED"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 23)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 23. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 23)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 23. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 23)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 23. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_CALL_IN_TIME_WINDOW_HOURS_DEX"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 23)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 23. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_1501"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 255)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 255. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_1502"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 1000)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 1000. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_1503"];
	var elemValue;
	if (elem != null) {
		elemValue = parseFloat(elem.value);
	}
	if (elemValue !=null && (elemValue < 0.01 || elemValue > 655.35)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 0.01 and greather than 655.35. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_1504"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 255)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 255. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_1523"];
	var elemValue;
	if (elem != null) {
		elemValue = parseFloat(elem.value);
	}
	if (elemValue !=null && (elemValue < 0.01 || elemValue > 655.35)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 0.01 and greather than 655.35. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_1525"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 1 || elemValue > 255)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 1 and greather than 255. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_1526"];
	var elemValue;
	if (elem != null) {
		elemValue = parseFloat(elem.value);
	}
	if (elemValue !=null && (elemValue < 0.01 || elemValue > 655.35)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 0.01 and greather than 655.35. Please correct.");
		return false;
	}
	elem = theForm.elements["cfg_field_1415"];
	var elemValue;
	if (elem != null) {
		elemValue = parseInt(elem.value);
	}
	if (elemValue !=null && (elemValue < 0 || elemValue > 255)){
		invalidInput(elem, elem.getAttribute("label") + " cannot be less than 0 and greather than 255. Please correct.");
		return false;
	}
	
	<%if (isEdge) {%>
		var interfaceType = theForm.elements["cfg_field_1500"].value;
		var elem = theForm.elements["cfg_field_1200"];
		var elemValue;
		if (elem != null) {
			elemValue = parseFloat(elem.value);
		}
		if (interfaceType == '3' || interfaceType == '4' || interfaceType == '5') {
			if (elemValue < parseFloat(theForm.elements["cfg_field_1503"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1506"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_1508"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1510"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_1512"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1514"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_1516"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1518"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_1520"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1523"].value)) {
				invalidInput(elem, elem.getAttribute("label") + " cannot be less than a Coin Pulse monetary value. Please correct.");
				return false;
			}
		}
		if ((theForm.elements["cfg_field_MAX_AUTH_AMOUNT"].value != null) && (elemValue > parseFloat(theForm.elements["cfg_field_MAX_AUTH_AMOUNT"].value))) {
			invalidInput(elem, elem.getAttribute("label") + " cannot be more than a Maximum Authorization Amount[MAX_AUTH_AMOUNT]. Please correct.");
			return false;
		}
	<%}else if (isGx) {%>
	   var elem = theForm.elements["cfg_field_195"];
	   var elemValue;
		if (elem != null) {
			elemValue = parseFloat(elem.value);
		}	
	   if ((theForm.elements["cfg_field_MAX_AUTH_AMOUNT"].value != null) && (elemValue > parseFloat(theForm.elements["cfg_field_MAX_AUTH_AMOUNT"].value))) {
		   invalidInput(elem, elem.getAttribute("label") + " cannot be more than a Maximum Authorization Amount[MAX_AUTH_AMOUNT]. Please correct.");
		   return false;
	   }
	   <%}%>
	return true;
}

</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
