<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="errorMessage" scope="request" class="java.lang.String"/>
<jsp:useBean id="device_type_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="comm_method" scope="request" class="java.lang.String"/>
<jsp:useBean id="include_device_ids" scope="request" class="java.lang.String"/>
<jsp:useBean id="customer_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="parent_location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="zero_counters" scope="request" class="java.lang.String"/>
<jsp:useBean id="pos_pta_tmpl_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="params_to_change" scope="request" class="java.lang.String"/>
<jsp:useBean id="params_to_change_request" scope="request" type="java.lang.String"/>
<jsp:useBean id="debug" scope="request" class="java.lang.String"/>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String file_uploads = inputForm.getStringSafely("file_uploads", "");
String file_downloads = inputForm.getStringSafely("file_downloads", "");
String s2c_requests = inputForm.getStringSafely("s2c_requests", "");
String firmware_upgrades = inputForm.getStringSafely("firmware_upgrades", "");
%>

<c:choose>
		<c:when test="${not empty errorMessage}" >
			
			
			<div class="tabDataContent" style="width: 980px;">
			<table width="100%">
			<tr>
			<td align="center">
				${errorMessage}
			</td>
			</tr>
			 <tr>
			  <td align="center" class="gridHeader">
			   <input type="button" value="&lt; Back" onClick="javascript:history.go(-1);">
			   <input type=button value="Cancel" onClick="javascript:window.location = '/';">
			   <input type="submit" name="action" value="Next &gt;">
			  </td>
			 </tr>
			</table>
			</div>
</c:when>
<c:otherwise>
			<div class="tableContainer">
			<div class="tableHead">
			<div class="tabHeadTxt">Device Configuration Wizard - Page 8: File Transfers</div>
			</div>
			<form action="bulkConfigWizard6c.i" method="post">
			<input type="hidden" name="bulk" value="<%=inputForm.getStringSafely("bulk", "")%>">
			<input type="hidden" name="include_device_ids" value="<%=inputForm.getStringSafely("include_device_ids", "")%>" />
			<input type="hidden" name="params_to_change" value="<%=inputForm.getStringSafely("params_to_change", "")%>" />
			<input type="hidden" name="params_to_change_request" value="<%=inputForm.getStringSafely("params_to_change_request", "")%>" />
			<input type="hidden" name="customer_id" value="<%=inputForm.getStringSafely("customer_id", "")%>" />
			<input type="hidden" name="location_id" value="<%=inputForm.getStringSafely("location_id", "")%>" />
			<input type="hidden" name="parent_location_id" value="<%=inputForm.getStringSafely("parent_location_id", "")%>" />
			<input type="hidden" name="device_type_id" value="<%=inputForm.getStringSafely("device_type_id", "")%>" />
			<input type="hidden" name="comm_method" value="<%=inputForm.getStringSafely("comm_method", "")%>" />
			<input type="hidden" name="zero_counters" value="<%=inputForm.getStringSafely("zero_counters", "")%>" />
			<input type="hidden" name="debug" value="<%=inputForm.getStringSafely("debug", "")%>" />
			<input type="hidden" name="pos_pta_tmpl_id" value="<%=inputForm.getStringSafely("pos_pta_tmpl_id", "")%>" />
			<input type="hidden" name="mode_cd" value="<%=inputForm.getStringSafely("mode_cd", "")%>" />
			<input type="hidden" name="order_cd" value="<%=inputForm.getStringSafely("order_cd", "")%>" />
			<input type="hidden" name="set_terminal_cd_to_serial" value="<%=inputForm.getStringSafely("set_terminal_cd_to_serial", "N")%>" />
			<input type="hidden" name="only_no_two_tier_pricing" value="<%=inputForm.getStringSafely("only_no_two_tier_pricing", "N")%>" />
			<input type="hidden" name="file_uploads" value="<%=inputForm.getStringSafely("file_uploads", "")%>" />
			<input type="hidden" name="file_upload_fields" value="<%=inputForm.getStringSafely("file_upload_fields", "")%>" />
			<input type="hidden" name="eft" value="<%=inputForm.getStringSafely("eft", "")%>" />
			<input type="hidden" name="file_downloads" value="<%=inputForm.getStringSafely("file_downloads", "")%>" />
			<input type="hidden" name="file_download_fields" value="<%=inputForm.getStringSafely("file_download_fields", "")%>" />
			<input type="hidden" name="s2c_requests" value="<%=inputForm.getStringSafely("s2c_requests", "")%>" />
			<input type="hidden" name="s2c_request_fields" value="<%=inputForm.getStringSafely("s2c_request_fields", "")%>" />
			<input type="hidden" name="forward" value="<%=inputForm.getStringSafely("forward", "")%>" />
			<input type="hidden" name="firmware_upgrades" value="<%=inputForm.getStringSafely("firmware_upgrades", "")%>" />
					
			<%if (!StringHelper.isBlank(file_uploads)) {%>
			<div class="gridHeader">Pending File Uploads: <%=file_uploads %></div>
			<%} %>
			
			<%if (!StringHelper.isBlank(file_downloads)) {%>
			<div class="gridHeader">Pending File Downloads: <%=file_downloads %></div>
			<%} %>
			
			<%if (!StringHelper.isBlank(s2c_requests)) {%>
			<div class="gridHeader">Pending Server to Client Requests: <%=s2c_requests %></div>
			<%} %>
			
			<%if (!StringHelper.isBlank(firmware_upgrades)) {%>
			<div class="gridHeader">Pending Firmware Upgrades: <%=firmware_upgrades %></div>
			<%} %>
			
			<jsp:include page="/jsp/devices/profile/tableFileTransfer.jsp" flush="true" />
			<div class="gridHeader">
			   <input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
			   <input type="button" class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
			   <input type="submit" class="cssButton" name="step6cAction" value="Next &gt;" />			
			</div>
			</form>
			</div>
</c:otherwise>
</c:choose>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
