<%@page import="java.util.Calendar"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.SQLException"%>
<%@page import="simple.db.DataLayerMgr"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.bean.ConvertUtils,simple.servlet.RequestUtils" %>

<%@page import="com.usatech.dms.action.ConfigFileActions"%>
<%@page import="com.usatech.dms.action.PendingCmdActions"%>
<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.layers.common.ProcessingUtils"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.device.DeviceConfigurationUtils"%>
<%@page import="com.usatech.layers.common.model.ConfigTemplateSetting"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.util.DeviceUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>


<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div id="__div_edit_configuration_func" class="tableDataContainer">

<div class="spacer5"></div>
<div class="innerTable">

<%
final simple.io.Log log = simple.io.Log.getLog();

InputForm inputForm    = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String evNumber        = inputForm.getString("ev_number", false);
long templateId 	   = inputForm.getLong("template_id", false, -1);
String templateName    = inputForm.getString("template_name", false);
int templateTypeId     = inputForm.getInt("template_type_id", false, -1);
String newName         = inputForm.getString("new_name", false);
if(newName!=null)
	newName = newName.trim();
int newTemplateTypeId  = inputForm.getInt("new_template_type_id", false, -1);
int deviceType         = inputForm.getInt("device_type_id", false, -1);
String action 		   = inputForm.getString("action", false);
String editMode 	   = inputForm.getString("edit_mode", false);
int propertyListVersion = DeviceUtils.getMaxPropertyListVersion(null);
int changeCount = 0;
if (action == null)
	action = "";

if ("Copy To".equals(action) && StringHelper.isBlank(newName)) {
%>
<span class="error">Required Parameter Not Found: new_name</span>
<%	
} else {
%>
<table class="tabDataDisplayNoBorder">
<tr>
<td>
   <center>
   <form>
<%
    if (StringHelper.isBlank(evNumber)) {
%>
    <input type=button class="cssButton" value="Return to Configuration Templates" onClick="javascript:window.location = '/templateMenu.i';">
<%
    } 
%>
   </form>
   </center>
   <pre>
<%
int utcOffsetMin = -Calendar.getInstance().get(Calendar.ZONE_OFFSET) / 1000 / 60;
StringBuilder updates = new StringBuilder();

Connection conn = null;
boolean success = false;
try{
	conn = DataLayerMgr.getConnection("OPER");
	
	LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceType, propertyListVersion, "SETTING_NUMBER", conn);
	LinkedHashMap<String, String> oldDeviceSettingData = ConfigFileActions.getConfigTemplateSettingData(defaultSettingData, templateId, deviceType, true, conn);
	LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String> ();
	Map<String, String> errorMap = new HashMap<String, String>();
	
	for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
		ConfigTemplateSetting defaultSetting = entry.getValue();
		String key = defaultSetting.getKey();
		String editorType = defaultSetting.getEditorType();
		boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
		String paramValue = ConvertUtils.getString(request.getAttribute("cfg_field_" + key), false);
		if (paramValue == null)
			continue;
		String oldParamValue = oldDeviceSettingData.get(key);
		if (oldParamValue == null)
			oldParamValue = "";
		
		if (editorType.equalsIgnoreCase("BITMAP")) {
			int intParamValue = 0;
			if (!StringHelper.isBlank(paramValue)) {
				String[] options = paramValue.split(",", -1);
				for (String option: options)
					intParamValue += Integer.valueOf(option);
			}
			paramValue = String.valueOf(intParamValue);
		} else if ("SCHEDULE".equalsIgnoreCase(editorType))
			paramValue = WebHelper.buildSchedule(request, defaultSetting, key, paramValue, utcOffsetMin);
		
		String regex = defaultSetting.getRegex();
		if(!StringHelper.isBlank(regex) && !Helper.validateRegexWithMessage(errorMap, paramValue, key, regex, "Value validation failed for field: " + defaultSetting.getLabel()))
			continue;
		
		if (DeviceUtils.EDGE_SCHEDULE_EXPRESSION.matcher(key).matches() && !Helper.validateEdgeSchedule(key, paramValue, errorMap))			
			continue;
		
		String enteredParamValue = paramValue;
		if (storedInPennies && StringHelper.isNumeric(paramValue))
			paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
		
		deviceSettingData.put(key, paramValue);
		if(!StringHelper.equalConfigValues(oldParamValue, paramValue, storedInPennies)){
			changeCount++;
			updates.append(defaultSetting.getLabel()).append(" [").append(key).append("]=").append(enteredParamValue).append("\n");
		}	
	}	
	
	out.println("\n<b>Processing Configuration Template " + templateName + "...</b>");
	
	if(errorMap.size() > 0){
		for(String error : errorMap.keySet())
			out.println("<font color=\"red\"><b>" + error + ", " + errorMap.get(error) + "</b></font>");
	} else if(changeCount > 0 && !StringHelper.isBlank(action)) {
    	out.println("\n<b>Changes you made are:</b>\n" + StringUtils.prepareCDATA(updates.toString()));
   		
   		if (action.equals("Copy To")) {
   			if (ConfigFileActions.configTemplateNameExists(newName))
	    		throw new ServletException("\nTemplate \"" + newName + "\" already exists!\n");
    		templateId = ConfigFileActions.getNextFileTransferSequenceNum(conn);		
    		ConfigFileActions.insertConfigTemplateWithId(templateId, newName, newTemplateTypeId == 22 ? 1 : 2, deviceType, conn);	    		
    		DeviceConfigurationUtils.saveConfigTemplateSettingData(templateId, deviceSettingData, true, conn);
    		out.println("\n<b>Created new template \"" + newName + "\"</b>\n");
		} else if (action.equals("Save")) {
			DeviceConfigurationUtils.saveConfigTemplateSettingData(templateId, deviceSettingData, true, conn);
   			out.println("\n<b>Saved template \"" + StringUtils.encodeForHTML(templateName) + "\"</b>\n");
		} else
			throw new ServletException("Unknown action: " + action);
       	
       	conn.commit();
    } else {
        out.print("\n<b>No User Changes!</b>\n");
    }
    
   	success = true;
}catch(Exception e){
	out.print("\n\n<font color='red'><b>Exception occurred! " + e.getMessage() + "</b></font>\n\n");
	log.error(StringUtils.exceptionToString(e));
}finally{
	if (!success)
		ProcessingUtils.rollbackDbConnection(log, conn);    		
	ProcessingUtils.closeDbConnection(log, conn);
}

%>
   </pre>
   <center>
   <form>

    <input type=button class="cssButton" value="Return to Configuration Templates" onClick="javascript:window.location = '/templateMenu.i';">

   </form>
   </center>
</td>
</tr>
</table>
</div>
<%
}
%>

</div>

<div class="spacer5"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
