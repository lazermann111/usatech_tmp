<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.action.PendingCmdActions"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.dms.model.PendingCommand"%>
<%@ page import="simple.results.Results" %>
<%@ page import="simple.db.DataLayerMgr" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@ page import="java.sql.SQLException" %>
<%@ page import="simple.db.DataLayerException" %>

<%
Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);

List<PendingCommand> pendingList = PendingCmdActions.loadPendingData(request.getSession().getServletContext(), device);
boolean hasPendingNonePVL = false;
if (pendingList != null && !pendingList.isEmpty()) {
    for (PendingCommand data : pendingList) {
        if (!data.isPendingPvl()) {
            hasPendingNonePVL = true;
            break;
        }
    }
}
Boolean fwUpdateRestrictedInSettings = null;
%>

<%!
	boolean readFwUpdateRestrictedState(long deviceId) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("device_id", deviceId);
		Results res = DataLayerMgr.executeQuery("GET_DEVICE_PARAM_RESTRICT_FIRMWARE_UPDATE", params);
		return res != null && res.next() && "Y".equalsIgnoreCase(res.getFormattedValue("upadate_resticted"));
	}
%>

<div class="spacer10"></div>

<div class="innerTable">
<div class="tabHead">Pending Commands</div>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
<thead>
<tr class="gridHeader">
<td>ID</td>
<td>Type</td>
<td>Data</td>
<td>Status</td>
<td>Order</td>
<td>Execute Date</td>
<td>Created Date</td>
<td>Attempts</td>
<td>Call ID</td>
<td align="center">
<%

if (!hasPendingNonePVL) {%>
Action
<%} else {%>
<input type="button" class="cssButton" name="myaction" value="Cancel All" onclick="return redirectWithParams(null,'device_id=<%=device.getId()%>', new Array(this));" />
<%}%>
</td>
</tr>
</thead>

<%
if (pendingList != null) {%>
<tbody>
<%for (PendingCommand data : pendingList) {%>
<tr>
<td><%=data.getId()%></td>
<td>
<%
long firmwareUpgradeId = data.getFirmwareUpgradeIdNumber();
if (firmwareUpgradeId > 0) {
	out.write("<a href=\"/firmwareUpgrade.i?firmwareUpgradeId="); out.write(data.getFirmwareUpgradeId()); out.write("\">"); 
	out.write(data.getFirmwareUpgradeName()); out.write("</a>");
	if (!"FW_UPG".equalsIgnoreCase(data.getType())) {
		out.write(": ");
		out.write(data.getTypeDesc());
	}
} else {
	out.write(data.getTypeDesc());
}
%>
</td>
<td>
<%
long fileTransId = data.getFileTransIdNumber();
String actionDesc = data.getActionDesc();
if (!StringHelper.isBlank(actionDesc)) {
%>
<%=actionDesc %>;
<%
}
if (fileTransId > 0) {
%>
Transfer ID <%=data.getDeviceFileTransferId()%>, <a href="fileDetails.i?file_transfer_id=<%=fileTransId%>">File ID <%=fileTransId%>: <%=data.getFileTransName()%></a>
<%} else if (data.getCommandDesc().length() > 50 && !data.getCommandDesc().contains(" ")) {%>
<input type="text" class="cssText" value="<%=data.getCommandDesc()%>" style="width:98%" readonly="readonly"/>
<%} else if (firmwareUpgradeId > 0) {
	out.write("State: "); out.write(data.getDeviceFirmwareUpgradeStatusName());
	if (data.getFirmwareUpgradeFileIdNumber() > 0) {
		out.write(", Step: "); out.write("<a href=\"fileDetails.i?file_transfer_id="); out.write(data.getFirmwareUpgradeFileId()); 
		out.write("\">File ID "); out.write(data.getFirmwareUpgradeFileId()); out.write(": "); out.write(data.getFirmwareUpgradeFileName()); out.write("</a>");
	}
	if (!StringHelper.isBlank(data.getErrorMessage())) {
		out.write(", Error: "); 
		out.write(data.getErrorMessage());
	}
%>
<%} else {%>
<%=data.getCommandDesc()%>
<%}%>
</td>
	<%
		boolean fwUpdateRestrictedForThisCommand = false;
		if (data.isFirmwareUpdate()) {
			// This is firmware upgrade command
			if (fwUpdateRestrictedInSettings == null) {
				fwUpdateRestrictedInSettings = readFwUpdateRestrictedState(device.getId());
			}
			fwUpdateRestrictedForThisCommand = fwUpdateRestrictedInSettings;
		}
	%>
<td><%=fwUpdateRestrictedForThisCommand ? "<b><font color='red'>Restricted</font></b>" : data.getStatusDesc()%></td>
<td><%=data.getExecuteOrder()%></td>
<td><%=(data.getExecuteDate() == null ? "&nbsp;" : data.getExecuteDate())%></td>
<td><%=data.getCreatedTs()%></td>
<td><%=data.getAttemptCount()%></td>
<td><%if (!StringHelper.isBlank(data.getGlobalSessionCd())) {%><a href="/deviceCallInLog.i?session_cd=<%=data.getGlobalSessionCd()%>"><%=data.getSessionId()%></a><%} %>&nbsp;</td>
<td align="center">
<%if (data.isPendingPvl()) {%>
&nbsp;
<%} else {%>
<input type="button" class="cssButton" value="Cancel" onclick="return redirectWithParams(null,'device_id=<%=device.getId()%>&cancel_pending_command_id=<%=data.getId()%>')" />
<%}%>
</td>

</tr>
<%}%>
</tbody>
<%}%>
</table>
</div>