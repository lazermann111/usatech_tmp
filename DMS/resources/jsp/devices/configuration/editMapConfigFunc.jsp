<%@page import="simple.servlet.BasicServletUser"%>
<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.sql.Connection"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>

<%@page import="simple.bean.ConvertUtils" %>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.dms.action.ConfigFileActions"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.action.PendingCmdActions"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.model.ConfigTemplateSetting"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.layers.common.ProcessingUtils"%>

<%@page import="com.usatech.layers.common.device.DeviceConfigurationUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%><jsp:include page="/jsp/include/header.jsp" flush="true" />

<%@ page import="simple.db.DataLayerException" %>
<%@ page import="simple.bean.ConvertException" %>
<%@ page import="simple.service.modem.service.dto.USATDevice" %>
<%@ page import="com.usatech.dms.device.modem.AsyncDmsModemService" %>
<%@ page import="simple.modem.USATDeviceDao" %>
<%@ page import="simple.modem.USATModemService" %>
<%@ page import="simple.modem.USATDeviceDaoImpl" %>
<%@ page import="java.sql.SQLException" %>

<div id="__div_edit_configuration_func" class="tableContainer">

<div class="spacer5"></div>

<%!
	public boolean isDeviceModemStatusField(String changedKey) {
		return "DEVICE_MODEM_STATUS".equals(changedKey);
	}

	public void enqueueModemStatusChange(long deviceId, String newStatusKey, String oldStatusKey) throws DataLayerException, SQLException, ConvertException, ServletException {
		if (newStatusKey == null || newStatusKey.length() != 1) {
			return;
		}
		if (oldStatusKey != null && oldStatusKey.equals(newStatusKey)) {
			return;
		}
		USATDeviceDao modemDeviceDao = new USATDeviceDaoImpl();
		USATDevice dev = modemDeviceDao.populateByDeviceId(deviceId);
		if (dev.hasIccIdAndProvider()) {
			USATModemService dmsModemService = new AsyncDmsModemService();
            if ("N".equals(newStatusKey)) {
                dmsModemService.refreshActivationStatus(new USATDevice[] {dev});
            } else if ("A".equals(newStatusKey)) {
                if (oldStatusKey == null || !"S".equals(oldStatusKey)) {
                    dmsModemService.activate(new USATDevice[]{dev});
                } else {
                    dmsModemService.resume(new USATDevice[]{dev});
                }
            } else if ("D".equals(newStatusKey)) {
                dmsModemService.deactivate(new USATDevice[] {dev});
            } else if ("S".equals(newStatusKey)) {
                dmsModemService.suspend(new USATDevice[] {dev});
            } else {
                dmsModemService.refreshActivationStatus(new USATDevice[] {dev});
            }
		}
	}
%>

<%
final simple.io.Log log = simple.io.Log.getLog();

BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
String userName = user.getUserName();

InputForm inputForm    = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String evNumber        = inputForm.getString("ev_number", false);
long templateId		   = inputForm.getLong("template_id", false, -1);
String fileName        = inputForm.getString("file_name", false);
String newName         = inputForm.getString("new_name", false);
long deviceId          = inputForm.getLong("device_id", false, -1);
int deviceType         = inputForm.getInt("device_type_id", false, -1);
boolean isTemplate     = inputForm.getBoolean("template", false, false); 
boolean isGx 		   = DeviceType.GX.getValue() == deviceType || DeviceType.G4.getValue() == deviceType;
boolean isMEI		   = DeviceType.MEI.getValue() == deviceType;

String sendComms       = inputForm.getString("comms", false);
String sendCounters    = inputForm.getString("counters", false);

String sendAction      = inputForm.getString("send", false);
String action 		   = inputForm.getStringSafely("action", "");
boolean newTemplate = "Copy To".equals(action);

if (newTemplate && StringHelper.isBlank(newName)) {
%>
<span class="error">Required Parameter Not Found: new_name</span>
<%
} else {
%>
<table class="tabDataDisplayNoBorder">
<tr>
<td>
   <center>
   <form>
<%
    if (StringHelper.isBlank(evNumber) || ((!(StringHelper.isBlank(action))) && "Import".equalsIgnoreCase(action))) {
%>
    <input type=button class="cssButton" value="Return to Template Menu" onClick="javascript:window.location = '/templateMenu.i';">
<%
    } else {
        String profileLink = deviceId == -1 ? "/deviceSearch.i?ev_number=" + evNumber : "/deviceConfig.i?device_id=" + deviceId;
%>
    <input type="button" class="cssButton" value="Return to Device Profile" onClick="javascript:window.location = '<%=profileLink%>';">
<%
    }
%>
   </form>
   </center>
   <pre>
<%
if (StringHelper.isBlank(evNumber) || "Import".equalsIgnoreCase(action))
    out.print("<b>Processing configuration template " + fileName + "...</b>\n\n");
else
    out.println("<b>Processing device configuration for " + evNumber + "...</b>\n\n");

StringBuilder buffer = new StringBuilder();
int changeCount = 0;
int serverOnlyChangeCount = 0;
int callInTimeChangeCount = 0;
int callInTimeWindowChangeCount = 0;
int memCounter = 0;
Map<String, String> errorMap = new HashMap<String, String>();

Connection conn = null;
boolean success = false;
try {
	conn = DataLayerMgr.getConnection("OPER");
	
	LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceType, -1, "FIELD_OFFSET", conn);
	LinkedHashMap<String, String> oldDeviceSettingData;
	if (isTemplate)
		oldDeviceSettingData = ConfigFileActions.getConfigTemplateSettingData(defaultSettingData, templateId, deviceType, true, conn);
	else
		oldDeviceSettingData = ConfigFileActions.getDeviceSettingData(defaultSettingData, deviceId, deviceType, true, conn);
	LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String> ();
	
	if (newTemplate) {
    	if (ConfigFileActions.configTemplateNameExists(newName))
    		throw new ServletException("\nTemplate \"" + newName + "\" already exists!\n");
    	fileName = newName;
 		templateId = ConfigFileActions.getNextFileTransferSequenceNum(conn);		
 		ConfigFileActions.insertConfigTemplateWithId(templateId, newName, 2, deviceType, conn);
	}
	
	int counter = 0;
	for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
		ConfigTemplateSetting defaultSetting = entry.getValue();
		int offSet = defaultSetting.getFieldOffset();
		int size = defaultSetting.getFieldSize();
		boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
		counter++;
		buffer.setLength(0);
		
		String key           = defaultSetting.getKey();
		String name          = defaultSetting.getName();
	    String label         = defaultSetting.getLabel();
	    String align         = defaultSetting.getAlign();
		String padWith       = defaultSetting.getPadChar();
		String dataMode      = defaultSetting.getDataMode();
		String eeromLocation = defaultSetting.getEeromLocation();		
		int displayOrder     = defaultSetting.getDisplayOrder();		
		String regex 		 = defaultSetting.getRegex();
		
		if (DevicesConstants.GX_MAP_PROTECTED_FIELDS.contains(offSet) && isGx) {
			deviceSettingData.put(key, StringHelper.rightPad("", size * 2, '0'));
			memCounter += size;
			buffer.append(counter).append(") ").append(displayOrder).append(' ').append(offSet)
				.append(' ').append(size).append(' ').append(align).append(' ').append(padWith).append(' ')
				.append(dataMode).append(" [ ").append(label).append(" ]\n")
				.append("Protected field\n");
			out.println(buffer.toString());
			continue;
		}
	    
		String orig = oldDeviceSettingData.get(key);
		if (orig == null) 
			orig = "";
	    String data = inputForm.getString(new StringBuilder("cfg_field_").append(key).toString(), false);
		if (data == null)
			data = orig;
				
		if(!StringHelper.isBlank(data) && !StringHelper.isBlank(regex)) {
			if (!Helper.validateRegexWithMessage(errorMap, data, name, regex, "Value validation failed for field: " + label))
				continue;
		}
		
		if (storedInPennies && StringHelper.isNumeric(data))
			data = String.valueOf((new BigDecimal(data).multiply(BigDecimal.valueOf(100)).intValue()));
			
		if (defaultSetting.isServerOnly()) {			
			buffer.append(counter).append(") ").append(" [ ").append(label).append(" ]\n");
			deviceSettingData.put(key, data);
			if (!StringHelper.equalConfigValues(data, orig, storedInPennies)) {
				buffer.append("<font color=\"red\"><b>CHANGED!</b></font> \"").append(data).append("\"  <>  \"").append(orig).append("\"\n");
				if (key.startsWith("CALL_IN_TIME_WINDOW_"))
					callInTimeWindowChangeCount++;
				changeCount++;
				serverOnlyChangeCount++;
			}
			buffer.append("OUT: \"").append(data).append("\" len=").append(data.length()).append("\n");
			out.println(buffer.toString());
			if (isDeviceModemStatusField(key)) {
                enqueueModemStatusChange(deviceId, data, orig);
            }
			continue;
		}
	
		buffer.append(counter).append(") ").append(displayOrder).append(' ').append(offSet)
			.append(' ').append(size).append(' ').append(align).append(' ').append(padWith).append(' ')
			.append(dataMode).append(" [ ").append(label).append(" ]\n")
			.append("IN: \"").append(data).append("\" len=").append(data.length()).append("\n");
	
		out.print(buffer.toString());
	
		int zerod = 0;
		if (isGx && "Y".equals(sendCounters) && memCounter >= 320 && memCounter < 356) {
		    data = "00000000";
		    out.println("Zeroing out counter " + name);
		    zerod = 1;
		}
	
		// data changed
		if (!StringHelper.equalConfigValues(data, orig, storedInPennies)) {
		    out.println(new StringBuilder("<font color=\"red\"><b>CHANGED!</b></font> \"").append(data).append("\"  <>  \"").append(orig).append("\"").toString());
	
			changeCount++;
			
			if (offSet == DevicesConstants.GX_MAP_CALL_IN_TIME && isGx || offSet == DevicesConstants.MEI_MAP_CALL_IN_TIME && isMEI)
				callInTimeChangeCount++;
	
			if(zerod > 0) {
				out.println("Skipping Poke for individial zero'd counter");
			} else if ("Save and Send".equals(action) && "changes".equals(sendAction)) {
				// insert a Poke for this memory location
				int pokeLoc, pokeSize;

				if(isGx) {
					// Gx uses a 2 byte per address scheme, ugh...
					// $eerom_location should already be adjusted
					pokeLoc = Integer.parseInt(eeromLocation, 16);
					pokeSize = size;

					if(pokeSize == 1) {
						// you must send at least 2 bytes
						pokeSize = 2;
					}
				} else {
					// this can happen for MEI
				    pokeLoc = Integer.parseInt(eeromLocation, 16);
					pokeSize = size;
				}

				PendingCmdActions.createPoke(conn, evNumber, pokeLoc, pokeSize, 0);
			}
		}
	
		if (("H".equals(dataMode) && data.length() < (size * 2)) || (!"H".equals(dataMode)) && data.length() < size) {
		    String[] paddedResult = PendingCmdActions.pad(data, padWith, size, align, dataMode);
		    data = paddedResult[1];
		    out.println(paddedResult[0]);
		    out.println(new StringBuilder("Padded: \"").append(data).append("\" len=").append(data.length()).toString());
		} else
		    out.println("No Padding Needed");
			
		out.println(new StringBuilder("OUT: \"").append(data).append("\" len=").append(data.length()).append("\n").toString());
		
		String outStr;
		int outLength;
		if ("H".equalsIgnoreCase(dataMode)) {
			outStr = data.toUpperCase();
			outLength = data.length() / 2;
		} else { 
			outStr = StringHelper.encodeHexString(data);
			outLength = data.length();
		}
		if (outLength != size)
		    throw new Exception("Fatal Error: the length of OUT value is invalid! expecting " + size + " but got " + outLength);
		
		deviceSettingData.put(key, outStr);
		memCounter += size;
	}
	
	if(errorMap.size() > 0){
		Collection<String> errors = (Collection<String>)errorMap.keySet();
		for(String error : errors)
			out.println("<font color=\"red\"><b>" + error + "</b></font>");
	} else if(isGx && memCounter != 512)
	    out.println("<font color=\"red\"><b>Fatal Error: size of new Gx config file " + memCounter + " is not 512!</b></font>");
	else {
		String mapFile = ConfigFileActions.getMapFileFromDeviceSettingData(defaultSettingData, deviceSettingData, true);
		
	    if(changeCount > 0 || newTemplate) {
	    	out.print("<font color=\"red\"><b>\n" + changeCount + " User Changes! </b></font>\n");
	    	out.print("\n<b>Saving configuration " + fileName + "</b>\n\n");
	    	if (changeCount > serverOnlyChangeCount) {
	    		out.print("HEX Data:\n");
	    		out.print("<textarea rows=\"10\" readonly=\"readonly\" style=\"width: 99%;\">" + mapFile +  "</textarea>\n\n");
	    	}
			
	    	if (isTemplate) {
	    		DeviceConfigurationUtils.saveConfigTemplateSettingData(templateId, deviceSettingData, true, conn);
	    		if (newTemplate)
	    		    out.println("\n<b>Created new template " + newName + "</b>\n");
	    	} else
	    		DeviceConfigurationUtils.saveDeviceSettingData(deviceId, deviceSettingData, true, false, conn, userName);
	    	out.println("<b>Save Success: " + fileName + "</b>");
	    } else {
	        out.print("\n<b>No User Changes!</b>\n");
	    }
	
	    if ("Save and Send".equals(action)) {
		    if ("complete".equals(sendAction) || isMEI && changeCount > serverOnlyChangeCount) {
		    	out.print("\n<b>Creating Pokes to Send Complete...</b>\n");
		    	String pokeData = StringHelper.decodeHexString(mapFile);
		
		    	if(isGx)
		    	{
		    		out.print("\nThis is a Gx device! Sending portions of complete config...\n");
		    		// we are sending very specific sections of the config, not the whole thing for Gx
		    		//	0 	- 135	/ 2 = 0		length = 136
		    		PendingCmdActions.createPoke(conn, evNumber, 0, 136, 0);
		    		//	142 - 283	/ 2 = 71	length = 142
		    		PendingCmdActions.createPoke(conn, evNumber, 71, 142, 0);
		    		//	356 - 511	/ 2 = 178	length = 156
		    		PendingCmdActions.createPoke(conn, evNumber, 178, 156, 0);
		    	}
		    	else
		    	{
		    		int pokeSize = 200;
		
		    		// figure out how many Pokes it will take to transmit the entire file
		    		int totalBytes = pokeData.length();
		    		int numParts = totalBytes / pokeSize + (totalBytes % pokeSize > 0 ? 1 : 0);
		
		    		out.println("Poke Size         : " + pokeSize);
		    		out.println("Total Bytes       : " + totalBytes);
		    		out.println("Num Parts         : " + numParts + "\n");
		
		    		// create each Poke and put it in the machine_cmd_pending table
		    		for (int pokeCounter = 0; pokeCounter < numParts; pokeCounter++) {
						int pos = pokeCounter * pokeSize;
		
		    			String chunk;
		    			if((pos + pokeSize) > totalBytes)
		    			{
		    				// this should be the last Poke
		    				chunk = pokeData.substring(pos);
		    			}
		    			else
		    			{
		    			    chunk = pokeData.substring(pos, pos + pokeSize);
		    			}
		
		    			// my $memory_location = ($pos/2);
		    			int memoryLocation = pos;
		    			out.println("Poke Number       : " + pokeCounter);
		    			out.println("File Location     : " + pos);
		    			out.println("Memory Location   : " + memoryLocation);
		    			out.println("Size              : " + chunk.length());
		
		        		PendingCmdActions.createPoke(conn, evNumber, memoryLocation, chunk.length(), 0);
		    		}
		    	}
		    }
		
		    if ("Y".equals(sendComms)) {
		    	// queue up 2 pokes for comm settings
		    	// comms are:
		    	//	136	- 141	/ 2 = 68	length = 6
		    	//	284 - 289	/ 2 = 142	length = 6
		
		    	out.println("\nSending Communication Parameters Pokes...");
				PendingCmdActions.createPoke(conn, evNumber, 68, 6, 0);
		
				PendingCmdActions.createPoke(conn, evNumber, 142, 6, 0);
		    }
		
		    if ("Y".equals(sendCounters)) {
		    	// queue up 1 poke to overwrite counters
		    	// counters are:
		    	//	320	- 355	/ 2 = 160	length = 36
		
		    	out.println("\nSending Counters Poke...");
				PendingCmdActions.createPoke(conn, evNumber, 160, 36, 0);
		    }
	    }
	}
	
	if (callInTimeWindowChangeCount > 0)
		DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] {1, deviceId});
	else if (callInTimeChangeCount > 0)
		DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] {0, deviceId});
	
	conn.commit();
	success = true;
}catch(Exception e){
	out.print("\n\n<font color='red'><b>Exception occurred! " + e.getMessage() + "</b></font>\n\n");
	log.error(StringUtils.exceptionToString(e));
}finally{
	if (!success)
		ProcessingUtils.rollbackDbConnection(log, conn);    		
	ProcessingUtils.closeDbConnection(log, conn);
}

%>
   </pre>
   <center>
   <form>
<%
    if (StringHelper.isBlank(evNumber)) {
%>
    <input type=button class="cssButton" value="Return to Template Menu" onClick="javascript:window.location = '/templateMenu.i';">
<%
    } else {
        String profileLink = deviceId == -1 ? "/deviceSearch.i?ev_number=" + evNumber : "/deviceConfig.i?device_id=" + deviceId;
%>
    <input type="button" class="cssButton" value="Return to Device Profile" onClick="javascript:window.location = '<%=profileLink%>';">
<%
    }
%>
   </form>
   </center>
   <div class="spacer5"></div>
</td>
</tr>
</table>
<%
}
%>

</div>

<div class="spacer5"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
