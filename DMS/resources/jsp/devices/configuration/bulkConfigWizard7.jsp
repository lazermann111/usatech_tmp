<%@page import="simple.text.StringUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Arrays"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="errorMessage" scope="request" class="java.lang.String"/>
<jsp:useBean id="device_type_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="customer_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="parent_location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="params_to_change" scope="request" class="java.lang.String"/>
<jsp:useBean id="params_to_change_request" scope="request" type="java.lang.String"/>
<jsp:useBean id="pos_pta_tmpl_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="zero_counters" scope="request" class="java.lang.String"/>
<jsp:useBean id="mode_cd" scope="request" class="java.lang.String"/>
<jsp:useBean id="debug" scope="request" class="java.lang.String"/>
<jsp:useBean id="form_action" scope="request" class="java.lang.String"/>
<jsp:useBean id="include_device_ids" scope="request" class="java.lang.String"/>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String file_uploads = inputForm.getStringSafely("file_uploads", "");
String eft = inputForm.getStringSafely("eft", "");
String file_downloads = inputForm.getStringSafely("file_downloads", "");
String s2c_requests = inputForm.getStringSafely("s2c_requests", "");
String ec_credential_name = inputForm.getStringSafely("ec_credential_name", "");
String firmware_upgrades = inputForm.getStringSafely("firmware_upgrades", "");
%>

<c:choose>
		<c:when test="${not empty errorMessage}" >
			
			
			<div class="tabDataContent"  style="width: 980px;">
			<table class="tabDataDisplayBorder" style="width: 980px;">
			<tr>
			<td align="center">
				${errorMessage}
			</td>
			</tr>
			<tr class="gridHeader">
				<td align="center">
	    			<input type="reset" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
	    			<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
	    			<input type="submit" class="cssButton" name="action" value="Next &gt;" />
	   			</td>
			</tr>
			</table>
			</div>
</c:when>
<c:otherwise>
			<jsp:useBean id="pos_pta_tmpl_name" scope="request" class="java.lang.String"/>
			<jsp:useBean id="include_device_ids_results" scope="request" type="simple.results.Results"/>
			<jsp:useBean id="customer_name" scope="request" class="java.lang.String"/>
			<jsp:useBean id="location" scope="request" class="com.usatech.layers.common.model.Location"/>
			
			<div class="tableContainer">
			<div class="tableHead">
			<div class="tabHeadTxt">Device Configuration Wizard - Page 10: Confirm Changes</div>
			</div>
			<form method="post" action="bulkConfigWizard8.i" onSubmit="return confirmChanges();">
			<input type="hidden" name="bulk" value="<%=inputForm.getStringSafely("bulk", "")%>">
			<input type="hidden" name="include_device_ids" value="<%=inputForm.getStringSafely("include_device_ids", "")%>" />
			<input type="hidden" name="params_to_change" value="<%=inputForm.getStringSafely("params_to_change", "")%>" />
			<input type="hidden" name="params_to_change_request" value="<%=inputForm.getStringSafely("params_to_change_request", "")%>" />
			<input type="hidden" name="customer_id" value="<%=inputForm.getStringSafely("customer_id", "")%>" />
			<input type="hidden" name="location_id" value="<%=inputForm.getStringSafely("location_id", "")%>" />
			<input type="hidden" name="parent_location_id" value="<%=inputForm.getStringSafely("parent_location_id", "")%>" />
			<input type="hidden" name="device_type_id" value="<%=inputForm.getStringSafely("device_type_id", "")%>" />
			<input type="hidden" name="zero_counters" value="<%=inputForm.getStringSafely("zero_counters", "")%>" />
			<input type="hidden" name="debug" value="<%=inputForm.getStringSafely("debug", "")%>" />
			<input type="hidden" name="pos_pta_tmpl_id" value="<%=inputForm.getStringSafely("pos_pta_tmpl_id", "")%>" />
			<input type="hidden" name="mode_cd" value="<%=inputForm.getStringSafely("mode_cd", "")%>" />
			<input type="hidden" name="order_cd" value="<%=inputForm.getStringSafely("order_cd", "")%>" />
			<input type="hidden" name="set_terminal_cd_to_serial" value="<%=inputForm.getStringSafely("set_terminal_cd_to_serial", "N")%>" />
			<input type="hidden" name="only_no_two_tier_pricing" value="<%=inputForm.getStringSafely("only_no_two_tier_pricing", "N")%>" />
			<input type="hidden" name="file_uploads" value="<%=inputForm.getStringSafely("file_uploads", "")%>" />
			<input type="hidden" name="file_upload_fields" value="<%=inputForm.getStringSafely("file_upload_fields", "")%>" />
			<input type="hidden" name="eft" value="<%=inputForm.getStringSafely("eft", "")%>" />
			<input type="hidden" name="file_downloads" value="<%=inputForm.getStringSafely("file_downloads", "")%>" />
			<input type="hidden" name="file_download_fields" value="<%=inputForm.getStringSafely("file_download_fields", "")%>" />
			<input type="hidden" name="s2c_requests" value="<%=inputForm.getStringSafely("s2c_requests", "")%>" />
			<input type="hidden" name="s2c_request_fields" value="<%=inputForm.getStringSafely("s2c_request_fields", "")%>" />
			<input type="hidden" name="ec_credential_id" value="<%=inputForm.getStringSafely("ec_credential_id", "")%>" />
			<input type="hidden" name="ec_credential_name" value="<%=StringUtils.prepareCDATA(ec_credential_name)%>" />
			<input type="hidden" name="forward" value="<%=inputForm.getStringSafely("forward", "")%>" />
			<input type="hidden" name="firmware_upgrades" value="<%=inputForm.getStringSafely("firmware_upgrades", "")%>" />
			<textarea id="changes" name="changes" style="height: 0px; visibility: hidden;"></textarea>
			
			<div align="left"><b>&nbsp;Please confirm that you want to make the following changes!</b></div>
			<div class="spacer10"></div>
			
			<div id="changeSummary" style="margin: 5px;">
				<c:choose>
					<c:when test="${debug == 1}">
						<center><font color='red'><b>----- DEBUG IS ON!  CHANGES WILL NOT BE SAVED! -----</b></font></center><br/><br/>  					
					</c:when>
				</c:choose>
					<b>Change Customer and Location:</b>
				  	<br/><br/>
				  	<table class="tabNormal">
				   		<tr>
				   			<th>Customer</th><th>Location</th><th>Parent Location</th>
				   		</tr>
				   		<tr>
				   			<c:choose>
  								<c:when test="${not empty customer_id}">
  									<td>${customer_name}</td>
  					
  								</c:when>
  								<c:otherwise>
  									<td>Do not change</td>
  								</c:otherwise>
  							</c:choose>
  							<c:choose>
  								<c:when test="${not empty location_id}">
  									<input type="hidden" name="location_name" value="${location.name}"  />
  									<td>${location.name} - ${location.city}, ${location.stateCode} ${location.countryCode} </td>
  					
  								</c:when>
  								<c:otherwise>
  									<td>Do not change</td>
  								</c:otherwise>
  							</c:choose>
  							<c:choose>
  								<c:when test="${not empty parent_location_id}">
  									<input type="hidden" name="location_name" value="${location.name}"  />
  									<td>${location.name} - ${location.city}, ${location.stateCode} ${location.countryCode} </td>
  								</c:when>
  								<c:otherwise>
  									<td>Do not change</td>
  								</c:otherwise>
  							</c:choose>
  						</tr>
  					</table>
  					<br/><br/>
				   	<b>Import Payment Template:</b>
				   	<br/><br/>
				    <table class="tabNormal">
				    	<tr>
				    		<th>Template Name</th><th>Import Mode</th><th>Order Mode</th><th>Set Terminal Code to Device Serial #</th><th>Only Import "No Two-Tier Pricing" Setting</th>
				    	</tr>
				    	<c:choose>
							<c:when test="${not empty mode_cd and not empty pos_pta_tmpl_id}">
								<tr>
									<td>${pos_pta_tmpl_name}</td><td>${mode_cd}</td><td>${order_cd}</td><td><%=inputForm.getStringSafely("set_terminal_cd_to_serial", "N")%></td><td><%=inputForm.getStringSafely("only_no_two_tier_pricing", "N")%></td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="5" align="center">Do Not Import</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</table>
					<br/><br/>
					<b>Update Configuration Parameters:</b>
					<br/><br/>
					<table class="tabNormal">
						<tr>
							<th>Parameter</th><th>Value</th>
						</tr>
					   <c:choose>
						<c:when test="${empty params_to_change and empty zero_counters}">
							<tr><td colspan="2">No Configuration Changes</td></tr>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${not empty params_to_change}">
									<jsp:useBean id="params_to_change_list" scope="request" class="java.util.ArrayList"/>
									<c:forEach var="nvp_item" items="${params_to_change_list}">
										<tr>
											<td>${nvp_item.name}</td><td>${StringUtils.prepareCDATA(nvp_item.value)}</td>
										</tr>
									</c:forEach>
								</c:when>
							</c:choose>
						</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${device_type_id eq '1' or device_type_id eq '0'}">
								<tr class="row0">
									<c:choose>
									<c:when test="${zero_counters > 0}">
										<td>Zero Out Counters</td><td>Yes</td>
									</c:when>
									<c:otherwise>
										<td>Zero Out Counters</td><td>No</td>
									</c:otherwise>
									</c:choose>
								</tr>
								</c:when>
						</c:choose>
  					</table>
  					<br/><br/>
  					
  					<%if (!StringHelper.isBlank(file_uploads)) {%>
					<b>File Uploads:</b> <%=file_uploads %>
					<br/><br/>
					<%} %>
					
					<%if (!StringHelper.isBlank(eft)) {%>
					<b>External File Transfer:</b> <%=eft.length() / 2 %> bytes
					<br/><br/>
					<%} %>
					
					<%if (!StringHelper.isBlank(file_downloads)) {%>
					<b>File Downloads:</b> <%=file_downloads %>
					<br/><br/>
					<%} %>
  					
  					<%if (!StringHelper.isBlank(s2c_requests)) {%>
					<b>Server to Client Requests:</b> <%=s2c_requests %>
					<br/><br/>
					<%} %>
					
					<%if (!StringHelper.isBlank(firmware_upgrades)) {%>
					<b>Firmware Upgrades:</b> <%=firmware_upgrades %>
					<br/><br/>
					<%} %>
					
					<%if (!StringHelper.isBlank(ec_credential_name)) {%>
					<b>Quick Connect Credential Change:</b> <%=StringUtils.prepareHTML(ec_credential_name) %>
					<br/><br/>
					<%} %>
  					
  					<b>Changes will be done for all of the following devices:</b><br/><br/>
  					<c:choose>
						<c:when test="${device_type_id eq '1' or device_type_id eq '0'}">
 						Pokes will be created and queued for the devices where needed.<br/><br/>
 						</c:when>
					</c:choose>
									
  					<table class="tabNormal">
   						<tr class="gridHeader">
   							<td>Device Name</td>
   							<td>Serial Number</td>
   							<td>Device Type</td>
   							<td>Reporting Customer</td>
   							<td>Customer</td>
   							<td>Location</td>
   							<td>Firmware</td>
   							<td>Diagnostic</td>
   							<td>PTest</td>
							<td>Bezel Mfgr</td>
							<td>Bezel App Version</td>
							<td>Comm Method</td>
							<td>Modem Mfgr</td>
							<td>Last Activity</td>
   						</tr>
						<c:forEach var="row" items="${include_device_ids_results}">
							<tr>
								<td><a href="<%=RequestUtils.getBaseUrl(request, false)%>/profile.i?device_id=${row.device_id}">${row.device_name}</a></td>
								<td>${row.device_serial_cd}</td>
								<td>${row.device_type_desc}</td>
								<td>${row.corp_customer_name}</td>
								<td>${row.customer_name}</td>
								<td>${row.location_name}</td>
								<td>${row.firmware_version}</td>
								<td>${row.diag_app_version}</td>
								<td>${row.ptest_version}</td>
								<td>${row.bezel_mfgr}</td>
								<td>${row.bezel_app_version}</td>
								<td>${row.comm_method_name}</td>
								<td>${row.modem_mfgr}</td>
								<td>${row.last_activity_ts}</td>
							</tr>
						</c:forEach>
					 </table> 
				 	<c:choose>
						<c:when test="${debug == 1}">
							<center><font color="red"><b>----- DEBUG IS ON!  CHANGES WILL NOT BE SAVED! -----</b></font></center><br/><br/>
						</c:when>
					</c:choose>
		<br/><br/>	
		</div>	
  		<div class="gridHeader">
   			<input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
   			<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
   			<input type="submit" class="cssButton" name="action" value="Finish >" />
   		</div>
		</form>
		</div>
</c:otherwise>
</c:choose>
<script type="text/javascript">
function confirmChanges() {
	if (confirm('Please click OK to continue and wait for your changes to be processed')) {
		document.getElementById('changes').value = document.getElementById('changeSummary').innerHTML;
		return true;
	} else
		return false;
}
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
