<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="action" class="java.lang.String" scope="request" />
<jsp:useBean id="file_type" class="java.lang.String" scope="request" />
<jsp:useBean id="device_type_id" class="java.lang.String" scope="request" />
<jsp:useBean id="config_template_name" class="java.lang.String" scope="request" />
<jsp:useBean id="config_template_type_name" class="java.lang.String" scope="request" />
<jsp:useBean id="number_of_settings" class="java.math.BigDecimal" scope="request" />
<jsp:useBean id="defaultConfigTemplateSettings" type="simple.results.ListArrayResults" scope="request" />
<jsp:useBean id="defaultConfigTemplateDataModes" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="defaultConfigTemplateRegex" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="defaultConfigTemplateCategories" type="simple.results.ListArrayResults" scope="request" />
<jsp:useBean id="defaultConfigTemplatePadChar" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="defaultConfigTemplateAlign" class="java.util.ArrayList" scope="request" />

<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>


<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	<c:otherwise>
	<c:set var="y_option" value="" /> 
	<c:set var="n_option" value="" /> 
	<c:set var="setting_cntr" value="0" />
	
	<div class="tableContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">Default Template Editor - ${file_type} - ${config_template_name}</div>
		</div>	
		<form method="post" action="editDefaultTemplateFunc.i" >
			<input type="hidden" name="device_type_id" value="${device_type_id}" />
			<input type="hidden" name="config_template_name" value="${config_template_name}" />	
			<input type="hidden" name="file_type" value="${file_type}" />
			<input type="hidden" name="number_of_settings" value="${number_of_settings}" />							
		<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
	
			<c:forEach var="item" items="${defaultConfigTemplateSettings}">
			<c:if test="${item.serverOnly == 0}">
				<c:set var="setting_cntr" value="${setting_cntr+1}" />
				<tr>
					 <td class="gridHeader">Order<br /><input type="text" maxlength="3" size="3" name="cfg_order_${setting_cntr}" value="${item.displayOrder }" /></td>
					 <td class="gridHeader">Name<br /><input type="text" maxlength="200" size="50" name="cfg_name_${setting_cntr}" value="${item.name }" />
					  <input type="hidden" name="cfg_original_key_${setting_cntr}" value="${item.key }" />
					 </td>
					 	
				 	 <td class="gridHeader">
				 	 	<table align="center" class="noBorder noSpacing">
				 	 		<tr>
				 	 			<td>
				 	 			Category<br />
								<select name="cfg_category_id_${setting_cntr}">
								 	
							 		<c:forEach var="category_item" items="${defaultConfigTemplateCategories}">
										<c:choose>
											<c:when test="${category_item.config_template_category_id == item.categoryId}">
												<option value="${category_item.config_template_category_id}" selected="selected">${category_item.config_template_category_name}</option>
											</c:when>
											<c:otherwise>
												<option value="${category_item.config_template_category_id}">${category_item.config_template_category_name}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
							 		
							 	</select>
							 	</td>
							 	<td>Size<br /><input type="text" maxlength="3" size="3" name="cfg_size_${setting_cntr}" value="${item.fieldSize }" /></td>
							 </tr>
						</table>
				 	 </td>
					 <td class="gridHeader">Memory Address<br /><input type="text" maxlength="3" size="3" name="cfg_address_${setting_cntr}" value="${item.fieldOffset }" /></td>
					 
					 <td class="gridHeader">Data Mode<br />
					 	
					 	<select name="cfg_data_mode_${setting_cntr}">
					 				 		
					 		<c:forEach var="data_mode_item" items="${defaultConfigTemplateDataModes}">
								<c:choose>
									<c:when test="${data_mode_item.value == item.dataMode}">
										<option value="${data_mode_item.value}" selected="selected">${data_mode_item.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${data_mode_item.value}">${data_mode_item.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					 		
					 	</select>
					 	
					 </td>
					 
					 <td class="gridHeader">Align<br />
					 	
					 	<select name="cfg_align_${setting_cntr}">
					 				 		
					 		<c:forEach var="align_item" items="${defaultConfigTemplateAlign}">
								<c:choose>
									<c:when test="${align_item.value == item.align}">
										<option value="${align_item.value}" selected="selected">${align_item.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${align_item.value}">${align_item.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					 		
					 	</select>
					 	
					 </td>
				</tr>
				<tr>
					<c:set var="thisEditor" value="${fn:split(item.editor, ':')}"  />
					<c:set var="thisTextEditorSelected" value=""  />
					<c:set var="thisSelectEditorSelected" value=""  />
					<c:choose>
						<c:when test="${thisEditor[0] == 'TEXT'}">
							<c:set var="thisTextEditorSelected" value="selected=\"selected\""  />
						</c:when>
						<c:otherwise>			
							<c:set var="thisSelectEditorSelected" value="selected=\"selected\""  />				
						</c:otherwise>
					</c:choose>
									
					<td class="gridHeader">Editor<br />
						<select name="cfg_editor_type_${setting_cntr}" title="The Editor field indicates the type of field displayed for ${item.name} on the screen.">							
						 	<option value="TEXT" ${thisTextEditorSelected}>TEXT</option>
						 	<option value="SELECT" ${thisSelectEditorSelected}>SELECT</option>
						</select>
					</td>
					<td class="gridHeader">Alternate Name<br /><input type="text" maxlength="200" size="50" name="cfg_alt_name_${setting_cntr}" value="${item.altName }" /></td>
					<td class="gridHeader">Validation<br />						 
					 	<select name="cfg_regex_id_${setting_cntr}" title="This field indicates the type of validation needed for the ${item.name} field.">						 	
					 		
					 		<c:forEach var="regex_item" items="${defaultConfigTemplateRegex}">
								<c:choose>
									<c:when test="${regex_item.value == item.REGEX_ID}">
										<option value="${regex_item.value}" selected="selected">${regex_item.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${regex_item.value}">${regex_item.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					 		
					 	</select>
					</td>
					
					 <td class="gridHeader">Padding<br />
					 
					 	<select name="cfg_pad_with_${setting_cntr}">
					 	
					 		<c:forEach var="pad_item" items="${defaultConfigTemplatePadChar}">
								<c:choose>
									<c:when test="${pad_item.value == item.padChar}">
										<option value="${pad_item.value}" selected="selected">${pad_item.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${pad_item.value}">${pad_item.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					 	
					 	</select>
					 	
					 </td>					
					
					<td class="gridHeader">Display<br />
					 	
						 <select name="cfg_display_${setting_cntr}">
						 	<c:choose>
								<c:when test="${item.display == 'Y'}">
									<option value="Y" selected="selected">Yes</option>
									<option value="N" >No</option>
								</c:when>
								<c:otherwise>
									<option value="Y" >Yes</option>
									<option value="N" selected="selected">No</option>
								</c:otherwise>
							</c:choose>
						</select>
					 	
					</td>
					<td class="gridHeader">Send Changes<br />
					 
						 <select name="cfg_allow_change_${setting_cntr}">
						 	
						 	<c:choose>
								<c:when test="${item.clientSend == 'Y'}">
									<option value="Y" selected="selected">Yes</option>
									<option value="N" >No</option>
								</c:when>
								<c:otherwise>
									<option value="Y" >Yes</option>
									<option value="N" selected="selected">No</option>
								</c:otherwise>
							</c:choose>
						 	
						 </select>
					 
					</td>
				</tr>
				
				<tr>
					 <td class="gridHeader" colspan="6">Description<br /><textarea rows="4" name="cfg_description_${setting_cntr}" style="width:99%;">${item.description}</textarea></td>
				</tr>
				
				<tr>
					 <td class="gridHeader" colspan="6">Editor Details<br /><input type="text" maxlength="200" name="cfg_editor_details_${setting_cntr}" value="${thisEditor[1]}" style="width:99%;" /></td>
				</tr>
				<tr>
					 <td colspan="6"><div class="spacer1"></div></td>
				</tr>
			
			</c:if>
			</c:forEach>
			<tr>
			  <td class="gridHeader" colspan="6" align="center">
			   <br/>			
			   <input type="submit" name="action" class="cssButton" value="Save" />
			   <input type="button" class="cssButton" value="Cancel" onclick="window.location = '/templateMenu.i';" />
			   <br/>
			  </td>
			 </tr>
			
		</table>
		</form>
		</div>
			
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />