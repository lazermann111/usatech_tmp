<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="errorMessage" scope="request" class="java.lang.String"/>
<jsp:useBean id="device_type_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="customer_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="bulk_out" scope="request" class="java.lang.String"/>
<jsp:useBean id="parent_location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="pos_pta_tmpl_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="mode_cd" scope="request" class="java.lang.String"/>


<jsp:useBean id="include_device_ids" scope="request" class="java.lang.String"/>
<c:choose>
		<c:when test="${not empty errorMessage}" >
			
			
			<div class="tabDataContent"  style="width: 980px;">
			<table width="100%">
			<tr>
			<td align="center">
				${errorMessage}
			</td>
			</tr>
			 <tr>
			  <td align="center" class="gridHeader">
			   <input type="button" value="&lt; Back" onClick="javascript:history.go(-1);" />
			   <input type=button value="Cancel" onClick="javascript:window.location = '/home.i';" />
			   <input type="submit" name="action" value="Next &gt;" />
			  </td>
			 </tr>
			</table>
			</div>
</c:when>
<c:otherwise>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Device Configuration Wizard - Page 11: Processing Log</div>
</div>
<br />
<div align="center" >
	<input type=button class="cssButton" value="Start Over" onClick="javascript:window.location = 'bulkConfigWizard1.i';"> &nbsp;
</div>

<pre>
<%out.print(bulk_out); %>
</pre>

</div>
</c:otherwise>
</c:choose>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
