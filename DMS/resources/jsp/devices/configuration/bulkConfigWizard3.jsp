<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="errorMessage" scope="request" class="java.lang.String"/>
<jsp:useBean id="form_action" scope="request" class="java.lang.String"/>
<jsp:useBean id="start_serial_head" scope="request" class="java.lang.String"/>
<jsp:useBean id="device_type_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="comm_method" scope="request" class="java.lang.String"/>
<jsp:useBean id="include_device_ids" scope="request" class="java.lang.String"/>

		<div class="tableContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">Device Configuration Wizard - Page 3: Customer &amp; Location Selection</div>
		</div>
		<form method="post" action="${form_action}">
		<table class="tabDataDisplayBorder">
		<tbody>
		<tr>
			<td>Change Customer:</td>
			<td>
				<select name="customer_id" id="customer_id" style="font-family: courier; font-size: 12px;">
					<option value="">Do not change</option>
				</select>
				<input type="text" name="customer_search" id="customer_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'customer_search_link')" />
				<a id="customer_search_link" href="javascript:getData('type=customer&id=customer_id&name=customer_id&defaultValue=&defaultName=Do+not+change&noDataValue=&noDataName=Do+not+change&search=' + document.getElementById('customer_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Change Location:</td>
			<td>
				<select name="location_id" id="location_id" style="font-family: courier; font-size: 12px;">
					<option value="">Do not change</option>
				</select>
				<input type="text" name="location_search" id="location_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'location_search_link')" />
				<a id="location_search_link" href="javascript:getData('type=location&id=location_id&name=location_id&defaultValue=&defaultName=Do+not+change&noDataValue=&noDataName=Do+not+change&search=' + document.getElementById('location_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Change Parent Location:</td>
			<td>
				<select name="parent_location_id" id="parent_location_id" style="font-family: courier; font-size: 12px;">	
					<option value="">Do not change</option>
				</select>
				<input type="text" name="parent_location_search" id="parent_location_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'parent_location_search_link')" />
				<a id="parent_location_search_link" href="javascript:getData('type=location&id=parent_location_id&name=parent_location_id&defaultValue=&defaultName=Do+not+change&noDataValue=&noDataName=Do+not+change&search=' + document.getElementById('parent_location_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Change Configuration:</td>
			<td>
				<c:choose>
					<c:when test="${device_type_id eq '13' or device_type_id eq '1' or device_type_id eq '0' or device_type_id eq '11' or device_type_id eq '6'}">
					<input type="radio" name="change_configuration" value="1" checked="checked" />Yes
					&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="change_configuration" value="0" />No					
					</c:when>
					<c:otherwise>
					<input type="radio" name="change_configuration" value="0" checked="checked" />No: <i>Change Configuration is enabled only when Edge, Gx, G4, Kiosk or MEI device type is selected on Page 1</i>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		
		<tr class="gridHeader">
			<td colspan="3" align="center">
			    <input type="reset" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
			    <input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
			    <input type="submit" class="cssButton" name="action" value="Next &gt;" />
			</td>
		</tr>
	</tbody>
</table>
		
		<input type="hidden" name="start_serial_head" value="${start_serial_head}"  />
		<input type="hidden" name="device_type_id" value="${device_type_id}"  />
		<input type="hidden" name="comm_method" value="${comm_method}" />
		<input type="hidden" name="include_device_ids" value="${include_device_ids}"  />
		
		</form>
		</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
