<%@page import="simple.servlet.BasicServletUser"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.Collection"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.SQLException"%>
<%@page import="simple.bean.ConvertUtils" %>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.dms.action.ConfigFileActions"%>
<%@page import="com.usatech.dms.action.PendingCmdActions"%>
<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.constants.DeviceProperty"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.model.Device"%>

<%@page import="com.usatech.layers.common.device.DeviceConfigurationUtils"%>
<%@page import="com.usatech.layers.common.ProcessingUtils"%>
<%@page import="com.usatech.layers.common.constants.FileType"%>
<%@page import="com.usatech.layers.common.model.ConfigTemplateSetting"%>
<%@page import="com.usatech.layers.common.util.DeviceUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>


<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@ page import="simple.db.DataLayerException" %>
<%@ page import="simple.bean.ConvertException" %>
<%@ page import="simple.service.modem.service.dto.USATDevice" %>
<%@ page import="com.usatech.dms.device.modem.AsyncDmsModemService" %>
<%@ page import="simple.modem.USATDeviceDao" %>
<%@ page import="simple.modem.USATModemService" %>
<%@ page import="simple.modem.USATDeviceDaoImpl" %>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div id="__div_edit_configuration_func" class="tableDataContainer">

<div class="spacer5"></div>

<%
final simple.io.Log log = simple.io.Log.getLog();

BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
String userName = user.getUserName();

InputForm inputForm    = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long deviceId          = ConvertUtils.getLongSafely(inputForm.getString("device_id", false), -1);
String evNumber        = inputForm.getString("ev_number", false);
String newName         = inputForm.getString("new_name", false);
int deviceType         = inputForm.getInt("device_type_id", false, -1);
String action          = inputForm.getString("action", false);
String sendAction      = inputForm.getString("send", false);
String editMode 	   = inputForm.getString("edit_mode", false);
boolean isBasicEditor  = editMode != null && editMode.toUpperCase().startsWith("B");
int propertyListVersion = inputForm.getInt("plv", false, -1);

if (deviceId < 1) {
%>
<span class="error">Required Parameter Not Found: device_id</span>
<%	
} else {
%>
<table class="tabDataDisplayNoBorder">
<tr>
<td>
   <center>
   <form>
<%
    if (StringHelper.isBlank(evNumber)) {
%>
    <input type=button class="cssButton" value="Return to Template Menu" onClick="javascript:window.location = '/templateMenu.i';">
<%
    } else {
        String profileLink = deviceId < 1 ? "/deviceSearch.i?ev_number=" + evNumber : "/deviceConfig.i?device_id=" + deviceId;
%>
    <input type="button" class="cssButton" value="Return to Device Profile" onClick="javascript:window.location = '<%=profileLink%>';">
<%
    }
%>
   </form>
   </center>
   <pre>
<%!
	public boolean isDeviceModemStatusField(String changedKey) {
		return "DEVICE_MODEM_STATUS".equals(changedKey);
	}

	public void enqueueModemStatusChange(long deviceId, String newStatusKey, String oldStatusKey) throws DataLayerException, SQLException, ConvertException, ServletException {
		if (newStatusKey == null || newStatusKey.length() != 1) {
			return;
		}
		if (oldStatusKey != null && oldStatusKey.equals(newStatusKey)) {
			return;
		}
		USATDeviceDao modemDeviceDao = new USATDeviceDaoImpl();
		USATDevice dev = modemDeviceDao.populateByDeviceId(deviceId);
		if (dev.hasSerialNumberAndProvider()) {
			USATModemService dmsModemService = new AsyncDmsModemService();
            if ("N".equals(newStatusKey)) {
                dmsModemService.refreshActivationStatus(new USATDevice[] {dev});
            } else if ("A".equals(newStatusKey)) {
                if (oldStatusKey == null || !"S".equals(oldStatusKey)) {
                    dmsModemService.activate(new USATDevice[]{dev});
                } else {
                    dmsModemService.resume(new USATDevice[]{dev});
                }
            } else if ("D".equals(newStatusKey)) {
                dmsModemService.deactivate(new USATDevice[] {dev});
            } else if ("S".equals(newStatusKey)) {
                dmsModemService.suspend(new USATDevice[] {dev});
            } else {
                dmsModemService.refreshActivationStatus(new USATDevice[] {dev});
            }
		}
	}
%>
<%
if (StringHelper.isBlank(evNumber))
    out.print("<b>Processing a configuration template...</b>\n\n");

Device device = null;
if(propertyListVersion < 0){
	device = DeviceUtils.generateDevice(deviceId);
	propertyListVersion = device.getPropertyListVersion();
}

StringBuilder buffer = new StringBuilder();
StringBuilder newIniFileShort = new StringBuilder();
StringBuilder updates = new StringBuilder();
String newIniFileHex = null;
int changeCount = 0;
int serverOnlyChangeCount = 0;
int callInTimeChangeCount = 0;
int callInTimeWindowChangeCount = 0;
Map<String, String> errorMap = new HashMap<String, String>();

Connection conn = null;
boolean success = false;
try {
	conn = DataLayerMgr.getConnection("OPER");
	
	LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceType, propertyListVersion, "SETTING_NUMBER", conn);
	LinkedHashMap<String, String> oldDeviceSettingData = ConfigFileActions.getDeviceSettingData(defaultSettingData, deviceId, deviceType, true, conn);
	LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String> ();
	
	for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
		ConfigTemplateSetting defaultSetting = entry.getValue();
		if (!defaultSetting.isSupported())
			continue;		
		buffer.setLength(0);
	
		String key = defaultSetting.getKey();
		String label = defaultSetting.getLabel();
		String editorType = defaultSetting.getEditorType();
		boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
		String paramValue = ConvertUtils.getString(request.getAttribute("cfg_field_" + key), false);
		if (paramValue == null)
			paramValue = "";
		String defaultConfigValue = defaultSetting.getConfigTemplateSettingValue();
		if (defaultConfigValue == null)
			defaultConfigValue = "";
		
		if (editorType.equalsIgnoreCase("BITMAP")) {
			int intParamValue = 0;
			if (!StringHelper.isBlank(paramValue)) {
				String[] options = paramValue.split(",", -1);
				for (String option: options)
					intParamValue += Integer.valueOf(option);
			}
			paramValue = String.valueOf(intParamValue);
		} else if ("SCHEDULE".equalsIgnoreCase(editorType)) {
			if (!isBasicEditor || "Y".equalsIgnoreCase(defaultSetting.getDisplay())) {
				if (device == null)
					device = DeviceUtils.generateDevice(deviceId);
				paramValue = WebHelper.buildSchedule(request, defaultSetting, key, paramValue, device.getDeviceUtcOffsetMin());
			}
		}
		
		// if a DNS host name or IP address doesn't have two dots, use the default value
		if (DeviceUtils.EDGE_COMM_SETTING_EXPRESSION.matcher(key).matches() && !DeviceUtils.TWO_DOT_EXPRESSION.matcher(paramValue).find()) {
			out.println(new StringBuilder("Detected invalid value '").append(paramValue).append("' in property ").append(key)
					.append(", resetting to default value '").append(defaultConfigValue).append("'").toString()); 
			paramValue = defaultConfigValue;
		}		
		
		String regex = defaultSetting.getRegex();
		if(!StringHelper.isBlank(regex) && !Helper.validateRegexWithMessage(errorMap, paramValue, key, regex, "Value validation failed for field: " + defaultSetting.getLabel()))
			continue;
		
		String enteredParamValue = paramValue;
		if (storedInPennies && StringHelper.isNumeric(paramValue))
			paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
		
		if (DeviceUtils.EDGE_SCHEDULE_EXPRESSION.matcher(key).matches() && !Helper.validateEdgeSchedule(key, paramValue, errorMap))			
			continue;
		
		String oldParamValue = oldDeviceSettingData.get(key);
		if (oldParamValue == null)
			oldParamValue = "";
		
		if (defaultSetting.isServerOnly()) {
			if (!StringHelper.equalConfigValues(oldParamValue, paramValue, storedInPennies)) {
				DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, paramValue, conn, userName);
				updates.append(label).append(" [").append(key).append("]=").append(enteredParamValue);
				if (isDeviceModemStatusField(key)) {
					enqueueModemStatusChange(deviceId, enteredParamValue, oldParamValue);
				}
				if (defaultConfigValue.equals(paramValue))
					updates.append(" (default)");
				updates.append("\n");
				changeCount++;
				serverOnlyChangeCount++;
				if (key.startsWith("CALL_IN_TIME_WINDOW_"))
					callInTimeWindowChangeCount++;
			}
			continue;
		}
		
		deviceSettingData.put(key, paramValue);
		if(!oldDeviceSettingData.containsKey(key) || !StringHelper.equalConfigValues(oldParamValue, paramValue, storedInPennies)){
			updates.append(label).append(" [").append(key).append("]=").append(enteredParamValue);
			if (isDeviceModemStatusField(key)) {
				enqueueModemStatusChange(deviceId, enteredParamValue, oldParamValue);
			}
			if(StringHelper.equalConfigValues(defaultConfigValue, paramValue, storedInPennies)) {
				newIniFileShort.append(key).append("!\n");
				updates.append(" (default)");
			} else
				newIniFileShort.append(key).append("=").append(paramValue).append("\n");
			updates.append("\n");
			changeCount++;
			
			if ("SCHEDULE".equalsIgnoreCase(defaultSetting.getEditorType()))
				callInTimeChangeCount++;
		}
	}

	if(errorMap.size() > 0){
		for(String error : errorMap.keySet())
			out.println("<font color=\"red\"><b>" + error + ", " + errorMap.get(error) + "</b></font>");
	} else if(changeCount > 0) {
   		//get min file transfer id
   		out.println("\n<b>Processing device configuration...</b>");   		
   		out.print("\n<b>Changes you made are:</b>\n" + StringUtils.prepareCDATA(updates.toString()));

   		if (changeCount > serverOnlyChangeCount) {
   			String newIniFile = ConfigFileActions.getIniFileFromDeviceSettingData(deviceSettingData);
	    	out.print("\n<b>Saving device configuration</b>\n\n");
	    	
	    	out.print("<b>Configuration Data:</b>\n");
	    	out.print("<textarea rows=\"10\" readonly=\"readonly\" style=\"width: 99%;\">" + newIniFile + "</textarea>\n\n");
		    		  
	    	DeviceConfigurationUtils.saveDeviceSettingData(deviceId, deviceSettingData, true, false, conn, userName);
	    	out.print("\n<b>Successfully saved device configuration</b>\n");		    	
	    	
	    	long newFileId = ConfigFileActions.getNextFileTransferSequenceNum(conn);
	    	ConfigFileActions.saveFile(new StringBuilder(evNumber).append("-CFG-").append(newFileId).toString(), newIniFileShort.toString(), newFileId, FileType.PROPERTY_LIST.getValue(), conn);
	    	
	       	out.print("\n<b>Creating command to send config file...</b>\n");
	       	DeviceUtils.sendCommand(deviceType, deviceId, evNumber, newFileId, newIniFileShort.length(), DeviceUtils.DEFAULT_PACKET_SIZE, DeviceUtils.DEFAULT_EXECUTE_ORDER, conn, null, false);
       		out.print("\n<b>Successfully created File Transfer Start command for file transfer id " + newFileId + "<b>\n");
   		}
    } else
        out.print("\n<b>No User Changes!</b>\n");
	
	if (callInTimeWindowChangeCount > 0)
		DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] {1, deviceId});
	else if (callInTimeChangeCount > 0)
		DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] {0, deviceId});	
	
	conn.commit();
    success = true;
}catch(Exception e){
	out.print("\n\n<font color='red'><b>Exception occurred! " + e.getMessage() + "</b></font>\n\n");
	log.error(StringUtils.exceptionToString(e));
}finally{
	if (!success)
		ProcessingUtils.rollbackDbConnection(log, conn);    		
	ProcessingUtils.closeDbConnection(log, conn);
}

%>
   </pre>
   <center>
   <form>
<%
    if (StringHelper.isBlank(evNumber)) {
%>
    <input type=button class="cssButton" value="Return to Template Menu" onClick="javascript:window.location = '/templateMenu.i';">
<%
    } else {
        String profileLink = deviceId < 1 ? "/deviceSearch.i?ev_number=" + evNumber : "/deviceConfig.i?device_id=" + deviceId;
%>
    <input type="button" class="cssButton" value="Return to Device Profile" onClick="javascript:window.location = '<%=profileLink%>';">
<%
    }
%>
   </form>
   </center>
   <div class="spacer5"></div>
</td>
</tr>
</table>
</div>
<%
}
%>

<div class="spacer5"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
