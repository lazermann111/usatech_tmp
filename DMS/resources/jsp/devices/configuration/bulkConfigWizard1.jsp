<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.model.Location"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="bulk_config_wizard_1_device_type_list" scope="request" class="java.util.ArrayList"/>
<jsp:useBean id="dms_values_list_commMethodsList" class="com.usatech.dms.util.GenericList" scope="application" />

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Device Configuration Wizard - Page 1: Search Criteria</div>
</div>
<form method="post" action="bulkConfigWizard2.i">
<table class="tabDataDisplayBorder">
		<tr>
			<td>Reporting Customer</td>			
			<td>				
				<select name="corp_customer_id" id="corp_customer_id" style="font-family: courier; font-size: 12px;">
				<option value="">Any Customer</option>
				</select>
				<input type="text" name="corp_customer_search" id="corp_customer_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'corp_customer_search_link')" />
				<a id="corp_customer_search_link" href="javascript:getData('type=corp_customer&subType=oper&id=corp_customer_id&name=corp_customer_id&defaultValue=&defaultName=Any+Customer&search=' + document.getElementById('corp_customer_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Customer</td>			
			<td>				
				<select name="customer_id" id="customer_id" style="font-family: courier; font-size: 12px;">
				<option value="">Any Customer</option>
				</select>
				<input type="text" name="customer_search" id="customer_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'customer_search_link')" />
				<a id="customer_search_link" href="javascript:getData('type=customer&id=customer_id&name=customer_id&defaultValue=&defaultName=Any+Customer&noDataValue=&noDataName=Any+Customer&search=' + document.getElementById('customer_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Location</td>
			<td>						
				<select name="location_id" id="location_id" style="font-family: courier; font-size: 12px;">
				<option value="">Any Location</option>
				</select>
				<input type="text" name="location_search" id="location_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'location_search_link')" />
				<a id="location_search_link" href="javascript:getData('type=location&id=location_id&name=location_id&defaultValue=&defaultName=Any+Location&noDataValue=&noDataName=Any+Location&search=' + document.getElementById('location_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Device Type</td>
			<td>						
				<select name="device_type_id" style="font-family: courier; font-size: 12px;" id="device_type_id">
					<option value="">Any Device Type</option>
				<c:forEach var="nvp_item_device_type" items="${bulk_config_wizard_1_device_type_list}">
					<option value="${nvp_item_device_type.value}" label="${nvp_item_device_type.name}">${nvp_item_device_type.name}</option>
				</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>Comm Method</td>
			<td>
				<select name="comm_method" style="font-family: courier; font-size: 12px;" id="comm_method">
					<option value="">Any Comm Method</option>
					<%
					List<NameValuePair> nvp_list_comm_methods = dms_values_list_commMethodsList.getList();
			    	Iterator<NameValuePair> commMethodIt = nvp_list_comm_methods.iterator();
			    	while(commMethodIt.hasNext()) {
			    		NameValuePair nvp = commMethodIt.next();
			    	%>
			    	<option value="<%=nvp.getValue()%>" label="<%=nvp.getName()%>"><%=nvp.getName()%></option>
			    	<% } %>
				</select>
			</td>
		</tr>
		<tr>
			<td>Firmware Version</td>
			<td>
				<select name="firmware_version" id="firmware_version" style="font-family: courier; font-size: 12px;">
				<option value="">Any Firmware Version</option>
				</select>
				<input type="text" name="firmware_version_search" id="firmware_version_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'firmware_version_search_link')" />
				<a id="firmware_version_search_link" href="javascript:getData('type=firmware_version&id=firmware_version&name=firmware_version&defaultValue=&defaultName=Any+Firmware+Version&noDataValue=&noDataName=Any+Firmware+Version&search=' + document.getElementById('firmware_version_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Firmware Version Prefix</td>
			<td><input type="text" name="firmware_version_prefix" maxlength="200" /></td>
		</tr>
		<tr>
			<td>Diagnostic Version</td>
			<td>
				<select name="diag_app_version" id="diag_app_version" style="font-family: courier; font-size: 12px;">
				<option value="">Any Diagnostic Version</option>
				</select>
				<a href="javascript:getData('type=diag_app_version&defaultValue=&defaultName=Any Diagnostic Version');"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Diagnostic Version Prefix</td>
			<td><input type="text" name="diag_app_version_prefix" maxlength="200" /></td>
		</tr>
		<tr>
			<td>PTest Version</td>
			<td>
				<select name="ptest_version" id="ptest_version" style="font-family: courier; font-size: 12px;">
				<option value="">Any PTest Version</option>
				</select>
				<a href="javascript:getData('type=ptest_version&defaultValue=&defaultName=Any PTest Version');"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>PTest Version Prefix</td>
			<td><input type="text" name="ptest_version_prefix" maxlength="200" /></td>
		</tr>
		<tr>
			<td>Bezel Manufacturer Prefix</td>
			<td><input type="text" name="bezel_mfgr_prefix" maxlength="255" /></td>
		</tr>
		<tr>
			<td>Bezel App Version Prefix</td>
			<td><input type="text" name="bezel_app_ver_prefix" maxlength="255" /></td>
		</tr>
		<tr>
			<td>Property List Version</td>
			<td>
				<select name="plv" style="font-family: courier; font-size: 12px;">
					<option value="">Any Property List Version</option>
				<c:forEach var="nvp_item_plv" items="${bulk_config_wizard_1_plv_list}">
					<option value="${nvp_item_plv.value}" label="${nvp_item_plv.name}">${nvp_item_plv.name}</option>
				</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>Product Type</td>
			<td>
				<select name="productType" style="font-family: courier; font-size: 12px;">
					<option value="">Any Product Type</option>
					<c:forEach var="nvp_prod_type" items="${PRODUCT_TYPES}">
						<option value="${nvp_prod_type.value}" label="${nvp_prod_type.name}">${nvp_prod_type.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>Gx Vendor Interface Type</td>
			<td>
				<select name="vipType" style="font-family: courier; font-size: 12px;">
					<option value="">Any Gx Vendor Interface Type</option>
					<c:forEach var="nvp_vip_type" items="${VENDOR_INTERFACE_TYPE_PARAMS}">
						<option value="${nvp_vip_type.value}" label="${nvp_vip_type.name}">${nvp_vip_type.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>G9/Edge VMC Interface Type</td>
			<td>
				<select name="vmcType" style="font-family: courier; font-size: 12px;">
					<option value="">Any G9/Edge VMC Interface Type</option>
					<c:forEach var="nvp_vmc_type" items="${VMC_INTERFACE_TYPE_PARAMS}">
						<option value="${nvp_vmc_type.value}" label="${nvp_vmc_type.name}">${nvp_vmc_type.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
            <td>Parameter Setting 1</td>
            <td>
                Parameter Code: <input type="text" name="ps1" maxlength="255"/>
                Parameter Value: <input type="text" name="pv1" maxlength="255"/>
            </td>
        </tr>
        <tr>
            <td>Parameter Setting 2</td>
            <td>
                Parameter Code: <input type="text" name="ps2" maxlength="255"/>
                Parameter Value: <input type="text" name="pv2" maxlength="255"/>
            </td>
        </tr>
		<tr>
			<td>Serial Number Prefix</td>
			<td><input type="text" name="serial_number_prefix" maxlength="20" /></td>
		</tr>
		<tr>
			<td>First serial number in sequence</td>
			<td>
				Serial Prefix: <input type="text" name="start_serial_head" size="4" maxlength="10" />
				Serial Number: <input type="text" onChange="clearList();" id="start_serial_number_id"  name="start_serial_number" size="11"> 
				Number of devices in sequence: <input type="text" name="num_devices_seq" id="num_devices_seq_id" size="7" maxlength="6">
			</td>
		</tr>
		<tr>
			<td>Max number of devices</td>
			<td>
				<input type="text" name="num_devices" size="7" maxlength="6" value="20">
			</td>
		</tr>
		<tr>
			<td valign="top">Serial Number List<br/>(1 per line)</td>
			<td>
				<textarea name="serial_number_list" rows="5" style="width: 240px;" onchange="clearSelect();" id="serial_number_list_id"></textarea>
			</td>			
		</tr>
		<tr>
			<td colspan="2" align="center" class="gridHeader">
		    	<input type="submit" class="cssButton" name="action" value="Download CSV" />
		    	<input type="reset" class="cssButton" value="Reset" />
		    	<input type="submit" class="cssButton" name="action" value="Next &gt;" />
		   </td>
		</tr>
</table>
</form>
</div>
<script type="text/javascript">
<!--
	function clearSelect() {
	   var obj1 = document.getElementById('start_serial_number_id');
	   var obj2 = document.getElementById('num_devices_seq_id');
	   obj1.value = '';
	   obj2.value = '';
	}
	function clearList() {
	   var obj1 = document.getElementById('serial_number_list_id');
	   obj1.value = '';
	}
// -->
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />