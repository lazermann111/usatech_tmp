<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>	
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%
String statusMessage = RequestUtils.getAttribute(request, "statusMessage", String.class, false);
String statusType = RequestUtils.getAttribute(request, "statusType", String.class, false);
if(StringUtils.isBlank(statusType))
	statusType = "info";
Results credentials = RequestUtils.getAttribute(request, "credentials", Results.class, true);
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long credentialId = inputForm.getLong("credential_id", false, 0);
Results credential = DataLayerMgr.executeQuery("GET_CREDENTIAL_INFO", new Object[] {credentialId});
boolean isCredential = credential.next();
String username = "";
if (isCredential)
	username = credential.getFormattedValue("username");
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<div class="formContainer">
<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Credentials</span></div>
</div>
<div class="tableContent">
<div class="spacer10"></div>
<form action="credential.i" method="post" onsubmit="return validateForm(this);" autocomplete="off">
<table cellpadding="3">
	<%if (!StringUtils.isBlank(statusMessage)) {%>
	<tr><td colspan="2"><span id="status" class="status-<%=statusType %>"><%=statusMessage %></span></td></tr>
	<%}%>
	<tr>
		<td class="label">Credential</td>
		<td>
			<select id="credential_id" name="credential_id" onchange="credentialChange()">
	     		<option value="0">Choose Credential</option>
	     		<%
	     		while(credentials.next()) {
	     			long currentCredentialId = credentials.getValue("credentialId", long.class);
	     		%>
	     		<option value="<%=currentCredentialId%>"<%if (credentialId == currentCredentialId) {%> selected="selected"<%}%>><%=credentials.getFormattedValue("credentialName")%></option>
	     		<%}%>
			</select>
			<input type="text" name="credential_search" id="credential_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("search", "")%>" onkeypress="return clickLinkOnEnter(event, 'credential_search_link')" />
			<a id="credential_search_link" href="javascript:getData('type=credential&id=credential_id&name=credential_id&defaultValue=0&defaultName=Choose+Credential&onChange=credentialChange()&search=' + document.getElementById('credential_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
       </td>
	</tr>
	<tr>
		<td class="label">Credential Name<font color="red">*</font></td>
		<td><input id="credential_name" name="credential_name" value="<%if (isCredential) out.write(credential.getFormattedValue("credentialName"));%>" size="70" maxlength="200" usatRequired="true" label="Credential Name"/></td>
	</tr>
	<tr>
		<td class="label">Credential Description</td>
		<td><input id="credential_desc" name="credential_desc" value="<%if (isCredential) out.write(credential.getFormattedValue("credentialDesc"));%>" size="70" maxlength="200"/></td>
	</tr>
	<tr>
		<td class="label">Username<font color="red">*</font></td>
		<td><input id="username" name="username" value="<%if (isCredential) out.write(username);%>" size="70" maxlength="60" usatRequired="true" label="Username"/></td>
	</tr>
	<tr>
		<td class="label">Created</td>
		<td><%if (isCredential) out.write(credential.getFormattedValue("createdTs"));%></td>
	</tr>
	<tr>
		<td class="label">Last Updated</td>
		<td><%if (isCredential) out.write(credential.getFormattedValue("lastUpdatedTs"));%></td>
	</tr>
	<tr>
		<td class="label">Credential Timestamp</td>
		<td><%if (isCredential) out.write(credential.getFormattedValue("credentialTs"));%></td>
	</tr>
	<%
	if (isCredential) {
	String attributeName = "credentialPwd" + username;
	String password = ConvertUtils.getString(request.getSession().getAttribute(attributeName), false);
	if (!StringUtils.isBlank(password)) {
	%>
	<tr>
		<td class="label">Passcode</td>
		<td style="color: blue;"><b><%=password%></b></td>
	</tr>
	<tr><td colspan="2" style="color: green;"><b>Please configure this temporary passcode on new devices that use this credential. This passcode will never be displayed again.</b></td></tr>
	<%
	request.getSession().removeAttribute(attributeName);
	}
	}
	%>
	<tr>
		<td colspan="2" align="center">
		<div class="spacer10"></div>
		<input type="submit" name="action" value="Save"<%if (!isCredential) out.write(" disabled=\"disabled\"");%> onclick="if (document.getElementById('username').value != '<%=username %>') return confirm('Are you sure you want to change the username?'); else return true;" />
		<input type="submit" name="action" value="Create" />
		<input type="submit" name="action" value="Change Passcode"<%if (!isCredential) out.write(" disabled=\"disabled\"");%> onclick="return confirm('Are you sure you want to change the passcode?');" />
		<input type="button" value="List Devices"<%if (!isCredential) out.write(" disabled=\"disabled\"");%> onclick="window.location = '/deviceList.i?credential_id=<%=credentialId%>';" />
		</td>
	</tr>
</table>
</form>
</div>
<div class="spacer10"></div>
</div>
<script type="text/javascript">
function credentialChange() {
	window.location = '/credential.i?credential_id=' + document.getElementById('credential_id').value + '&search=' + document.getElementById('credential_search').value;
}
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />