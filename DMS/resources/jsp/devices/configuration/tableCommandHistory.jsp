<%@page import="java.util.List"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.dms.model.PendingCommand"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);

int cmdInt = inputForm.getInt("cmd_cnt", false, 5);
%>

<div class="innerTable">
<div class="tabHead">
Command History ( <input type="text" class="cssText" name="cmd_cnt" size="2" value="<%=cmdInt%>"
  onkeypress="return numbersonly(event, false, false);" /> Most Recent )
<input type="button" class="cssButton" name="userOP" value="List" onclick="return redirectWithParams(null,'device_id=<%=device.getId()%>', new Array(this.form.cmd_cnt, this))" />
</div>
<%
List<PendingCommand> histories = (List<PendingCommand>)inputForm.get("com.usatech.dms.device.CommandHistory");
if (histories != null) {
%>
<div class="spacer5"></div>
<table class="tabDataDisplay">
<thead>
<tr class="gridHeader">
<td>ID</td>
<td>Type</td>
<td>Data</td>
<td>Status</td>
<td>Order</td>
<td>Execute Date</td>
<td>Created Date</td>
<td>Attempts</td>
<td>Call ID</td>
</tr>
</thead>

<tbody>
<%for (PendingCommand data : histories) {%>
<tr>
<td><%=data.getId()%></td>
<td>
<%
long firmwareUpgradeId = data.getFirmwareUpgradeIdNumber();
if (firmwareUpgradeId > 0) {
	out.write("<a href=\"/firmwareUpgrade.i?firmwareUpgradeId="); out.write(data.getFirmwareUpgradeId()); out.write("\">"); 
	out.write(data.getFirmwareUpgradeName()); out.write("</a>");
	if (!"FW_UPG".equalsIgnoreCase(data.getType())) {
		out.write(": ");
		out.write(data.getTypeDesc());
	}
} else {
	out.write(data.getTypeDesc());
}
%>
</td>
<td>
<%
long fileTransId = data.getFileTransIdNumber();
String actionDesc = data.getActionDesc();
if (!StringHelper.isBlank(actionDesc)) {
%>
<%=actionDesc %>;
<%
}
if (fileTransId > 0) {
%>
Transfer ID <%=data.getDeviceFileTransferId()%>, <a href="fileDetails.i?file_transfer_id=<%=fileTransId%>">File ID <%=fileTransId%>: <%=data.getFileTransName()%></a>
<%} else if (data.getCommandDesc().length() > 50 && !data.getCommandDesc().contains(" ")) {%>
<input type="text" class="cssText" value="<%=data.getCommandDesc()%>" style="width:98%" readonly="readonly"/>
<%} else if (firmwareUpgradeId > 0) {
	out.write("State: "); out.write(data.getDeviceFirmwareUpgradeStatusName());
	if (data.getFirmwareUpgradeFileIdNumber() > 0) {
		out.write(", Step: "); out.write("<a href=\"fileDetails.i?file_transfer_id="); out.write(data.getFirmwareUpgradeFileId()); 
		out.write("\">File ID "); out.write(data.getFirmwareUpgradeFileId()); out.write(": "); out.write(data.getFirmwareUpgradeFileName()); out.write("</a>");
	}
	if (!StringHelper.isBlank(data.getErrorMessage())) {
		out.write(", Error: "); 
		out.write(data.getErrorMessage());
	}
} else {%>
<%=data.getCommandDesc()%>
<%}%>
</td>
<td><%=data.getStatusDesc()%></td>
<td><%=data.getExecuteOrder()%></td>
<td><%=(data.getExecuteDate() == null ? "&nbsp;" : data.getExecuteDate())%></td>
<td><%=data.getCreatedTs()%></td>
<td><%=data.getAttemptCount()%></td>
<td><%if (!StringHelper.isBlank(data.getGlobalSessionCd())) {%><a href="/deviceCallInLog.i?session_cd=<%=data.getGlobalSessionCd()%>"><%=data.getSessionId()%></a><%} %>&nbsp;</td>
</tr>
<%}%>
</tbody>

</table>
<%}%>
</div>