<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>	
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%
String statusMessage = RequestUtils.getAttribute(request, "statusMessage", String.class, false);
String statusType = RequestUtils.getAttribute(request, "statusType", String.class, false);
if(StringUtils.isBlank(statusType))
	statusType = "info";
Results publicKeys = RequestUtils.getAttribute(request, "publicKeys", Results.class, true);
Results publicKeyTypes = RequestUtils.getAttribute(request, "publicKeyTypes", Results.class, true);
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long publicKeyId = inputForm.getLong("publicKeyId", false, 0);
String publicKeyTypeCd = "";
String publicKeyHex = "";
String publicKeyIdentifier = "";
String publicKeyCreatedTs = "";
if (publicKeyId > 0) {
	Results publicKey = DataLayerMgr.executeQuery("GET_PUBLIC_KEY_INFO", new Object[] {publicKeyId});
	if (publicKey.next()) {
		publicKeyTypeCd = publicKey.getFormattedValue("publicKeyTypeCd");
		publicKeyHex = publicKey.getFormattedValue("publicKeySize") + " bytes";
		publicKeyIdentifier = publicKey.getFormattedValue("publicKeyIdentifier");
		publicKeyCreatedTs = publicKey.getFormattedValue("createdTs");
	}
}
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Public Keys</span></div>
</div>
<div class="tableContent">
<div class="spacer10"></div>
<form method="post" onsubmit="return validateForm(this);" autocomplete="off">
<table cellpadding="3">
	<%if (!StringUtils.isBlank(statusMessage)) {%>
	<tr><td colspan="2"><span id="status" class="status-<%=statusType %>"><%=statusMessage %></span></td></tr>
	<%}%>
	<tr>
		<td class="label">Public Key</td>
		<td>
			<select onchange="window.location = '/publicKey.i?publicKeyId=' + this.value">
	     		<option value="0">Select Public Key</option>
	     		<%while(publicKeys.next()) {%>
	     		<option value="<%=publicKeys.getValue("publicKeyId", long.class)%>"<%if (publicKeyId == publicKeys.getValue("publicKeyId", long.class)) {%> selected="selected"<%}%>><%=publicKeys.getFormattedValue("publicKeyInfo")%></option>
	     		<%}%>
			</select>
       </td>
	</tr>
	<tr>
		<td class="label">Public Key Type</td>
		<td>
			<select id="publicKeyTypeCd" name="publicKeyTypeCd">
	     		<%while(publicKeyTypes.next()) {%>
	     		<option value="<%=publicKeyTypes.getFormattedValue("publicKeyTypeCd")%>"<%if (publicKeyTypes.getFormattedValue("publicKeyTypeCd").equalsIgnoreCase(publicKeyTypeCd)) {%> selected="selected"<%}%>><%=publicKeyTypes.getFormattedValue("publicKeyTypeName")%></option>
	     		<%}%>
			</select>
		</td>
	</tr>
	<tr>
		<td class="label">Public Key Hex<font color="red">*</font></td>
		<td><input id="publicKeyHex" name="publicKeyHex" value="<%=publicKeyHex%>" size="120" maxlength="3000" usatRequired="true" label="publicKeyHex"/></td>
	</tr>
	<tr>
		<td class="label">Public Key Identifier<font color="red">*</font></td>
		<td><input id="publicKeyIdentifier" name="publicKeyIdentifier" value="<%=publicKeyIdentifier%>" size="30" maxlength="50" usatRequired="true" label="publicKeyHex"/></td>
	</tr>
	<tr>
		<td class="label">Created</td>
		<td><%=publicKeyCreatedTs%></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div class="spacer10"></div>
		<input type="submit" name="action" value="Create" />
		</td>
	</tr>
</table>
</form>
</div>
<div class="spacer10"></div>
</div>
<script type="text/javascript">
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />