<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.util.NameValuePair"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:useBean id="defaultConfigTemplateList" type="simple.results.Results" scope="request" />
<jsp:useBean id="templateList" type="simple.results.Results" scope="request" />
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
List<NameValuePair> posptaTmplSelections = (List<NameValuePair>)inputForm.get("com.usatech.dms.tmpl_menu.pos_pta_tmpl_selections");
%>

<div class="spacer15"></div>

<div class="tableContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Configuration Templates</span></div>
</div>

<div class="tableContent">

<form method="get" action="editTmplConfig.i">
<table class="tabDataDisplayBorderNoBottom">
 <tr>
  	<td width="20%">Configuration Template</td>
  	<td>
	  	<select name="template_info" id="template_info" onchange="selectTemplate(this);">
	  		<option value="">-- Select Template --</option>
	  	<c:forEach var="custom_template_item" items="${templateList}">
			<option value="${custom_template_item.aValue}" >${custom_template_item.aLabel}</option>
		</c:forEach>
	  	</select>
	  	<input type="text" name="config_template_search" id="config_template_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("config_template_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'config_template_search_link')" />
		<a id="config_template_search_link" href="javascript:getData('type=config_template&subType=editor&id=template_info&name=template_info&defaultValue=&defaultName=-- Select Template --&onChange=selectTemplate(this)&search=' + document.getElementById('config_template_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
	    <select name="edit_mode">
	        <option value="A">Advanced</option>
	        <option value="B">Basic</option>	        
	    </select>
   </td>
   <td>
   <div id="memory_map" class="nowrap">
   Memory Map: 
   <select name="map" id="map">
   <c:forEach var="default_template_item" items="${defaultConfigTemplateList}">
		<option value="${default_template_item.aValue}" >${default_template_item.aLabel}</option>
   </c:forEach>
   </select>
   </div>
  </td>
  <td width="20%"><input type="submit" class="cssButton" value="Edit Template"></td>
 </tr>
</table>
</form>

<form method="get" action="editDefaultTemplate.i">
<table class="tabDataDisplayBorderNoBottom">
 <tr>
  <td width="20%">Memory Map Editor</td>
  <td colspan="2">
  <select name="default_config_device_type_id">
  <c:forEach var="default_template_item" items="${defaultConfigTemplateList}">
	<option value="${default_template_item.aValue}" >${default_template_item.aLabel}</option>
  </c:forEach>
  </select>
  </td>
  <td width="20%"><input type="submit" class="cssButton" value="Edit Map"></td>
 </tr>
 </table>
 </form>
 
 <form method="get" action="editPosptaTmpl.i">
 <table class="tabDataDisplayBorder">
 <tr>
  <td width="20%">Payment Template</td>
  <td colspan="2">
    <select name="pos_pta_tmpl_id" id="pos_pta_tmpl_id">
<%for (NameValuePair option : posptaTmplSelections) {%>
      <option value="<%=option.getValue()%>"><%=option.getName()%></option>
<%}%>
    </select>
    <input type="text" name="payment_template_search" id="payment_template_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("payment_template_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'payment_template_search_link')" />
	<a id="payment_template_search_link" href="javascript:getData('type=payment_template&id=pos_pta_tmpl_id&name=pos_pta_tmpl_id&search=' + document.getElementById('payment_template_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
  </td>
  <td width="20%">
   <input type="submit" class="cssButton" name="action" value="Edit Template" />
   <input type="submit" class="cssButton" name="action" value="Add" />
   <input type="submit" class="cssButton" name="action" value="Clone" />
  </td>
 </tr>
</table>
</form>

</div>

<div class="spacer5"></div>

</div>

<script type="text/javascript">
<!--
function selectTemplate(template) {
	var deviceTypeId = template.value.substring(0, template.value.indexOf('_'));
	if (deviceTypeId == 0 || deviceTypeId == 1 || deviceTypeId == 6) {
		if (deviceTypeId == 6)
			document.getElementById('map').selectedIndex = 1;
		else
			document.getElementById('map').selectedIndex = 0;
		document.getElementById('memory_map').style.visibility = 'visible';		
	} else
		document.getElementById('memory_map').style.visibility = 'hidden';
}
-->
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
