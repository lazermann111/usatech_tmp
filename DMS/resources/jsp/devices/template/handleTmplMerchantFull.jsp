<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.NotEnoughRowsException"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%
Map<String,Object> params = new HashMap<String,Object>();
Long posPtaTmplId = RequestUtils.getAttribute(request, "pos_pta_tmpl_id", Long.class, true);
params.put("pos_pta_tmpl_id", posPtaTmplId);
String deviceIdsParam = RequestUtils.getAttribute(request, "device_ids_param", String.class, true);
String submitTo = RequestUtils.getAttribute(request, "submit_to", String.class, true);
long[] deviceIds = RequestUtils.getAttribute(request, deviceIdsParam, long[].class, true);
params.put("device_ids", deviceIds);
Results tmplResults = DataLayerMgr.executeQuery("GET_POS_PTA_TMPL_BY_POS_PTA_TMPL_ID", params);
if(!tmplResults.next())
	throw new NotEnoughRowsException("Template " + posPtaTmplId + " not found");
Results results = DataLayerMgr.executeQuery("GET_FULL_MERCHANTS_IN_TMPL", params);
%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Alternate Merchant/Terminal</span></div>
</div>
<div class="innerTable">
<div class="label">Template: <%=StringUtils.prepareHTML(tmplResults.getValue("POS_PTA_TMPL_NAME", String.class))%></div>
<div>The following merchant/terminals do not have enough slots available. Please select an alternate merchant/terminal for each:</div>
<div class="spacer5"></div>
<form name="alternateTerminalForm" action="<%=submitTo %>">
<%for(Map.Entry<String,Object> param : RequestUtils.getInputForm(request).getParameters().entrySet()) {
	if(!StringUtils.isBlank(param.getKey()) && !param.getKey().equals("replaceTerminalIds") && !param.getKey().startsWith("alternateTerminalId_") && !param.getKey().equals("full_action")) {
	%><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(ConvertUtils.getString(param.getValue(), false)) %>"/><%
	}
}%>
<table class="tabDataDisplayBorder">
    <tr><th>Authority Name</th><th>Merchant Desc</th><th>Merchant Code</th><th>Terminal Desc</th><th>Terminal Code</th><th>Total Slots</th><th>Used Slots</th><th>Alternate</th></tr>
<%while(results.next())  {
	Long minPosPtaNum = results.getValue("minPosPtaNum", Long.class);
	Long maxPosPtaNum = results.getValue("maxPosPtaNum", Long.class);
    Long usedPosPtaNum = results.getValue("usedPosPtaNum", Long.class);  
    Long terminalId = results.getValue("terminalId", Long.class);  
    params.put("terminal_id", terminalId);
    Results altResults = DataLayerMgr.executeQuery("GET_ALTERNATE_TERMINALS", params);
%>
    <tr class="row<%=results.getRow()%2%>"><td><%=StringUtils.prepareHTML(results.getValue("authorityName", String.class)) %></td>
    <td><%=StringUtils.prepareHTML(results.getValue("merchantDesc", String.class)) %></td>
    <td><%=StringUtils.prepareHTML(results.getValue("merchantCode", String.class)) %></td>
    <td><%=StringUtils.prepareHTML(results.getValue("terminalDesc", String.class)) %></td>
    <td><%=StringUtils.prepareHTML(results.getValue("terminalCode", String.class)) %></td>
    <td><%=1L + maxPosPtaNum - minPosPtaNum%></td>
    <td><%=usedPosPtaNum%></td>
    <td class="center">
        <input type="hidden" name="replaceTerminalIds" value="<%=terminalId %>"/>
    <% if(!altResults.isGroupEnding(0)) { %><select name="alternateTerminalId_<%=terminalId %>">
        <option value="">-- Please select --</option><%
        while(altResults.next()) {
        	String merchantDesc = altResults.getValue("merchantDesc", String.class);
        	String merchantCode = altResults.getValue("merchantCode", String.class);
        	String terminalDesc = altResults.getValue("terminalDesc", String.class);
        	String terminalCode = altResults.getValue("terminalCode", String.class);           
        	minPosPtaNum = altResults.getValue("minPosPtaNum", Long.class);
		    maxPosPtaNum = altResults.getValue("maxPosPtaNum", Long.class);
		    usedPosPtaNum = altResults.getValue("usedPosPtaNum", Long.class);  
		    %><option value="<%=altResults.getValue("terminalId", Long.class) %>"><%=StringUtils.prepareHTML(results.getValue("authorityName", String.class)) %><%
        	if(!StringUtils.isBlank(merchantDesc)) { %>: <%=StringUtils.prepareHTML(merchantDesc) %><%}
            if(!StringUtils.isBlank(merchantCode)) { %>: <%=StringUtils.prepareHTML(merchantCode) %><%}
            if(!StringUtils.isBlank(terminalDesc)) { %>: <%=StringUtils.prepareHTML(terminalDesc) %><%}
            if(!StringUtils.isBlank(terminalCode)) { %>: <%=StringUtils.prepareHTML(terminalCode) %><%}
            if(maxPosPtaNum != null && minPosPtaNum != null && usedPosPtaNum != null) { %> [<%=1L + maxPosPtaNum - minPosPtaNum - usedPosPtaNum%> slots available]<%}
        %></option><%
        }
        %>        
    </select><%} %>
    <div><a href="addMerchant.i?authority_id=<%=results.getValue("authorityId", Long.class)%>&amp;pos_pta_cd_exp=<%=StringUtils.prepareURLPart(results.getValue("posPtaCdExp", String.class))%>&amp;min_pos_pta_num=<%=minPosPtaNum%>&amp;max_pos_pta_num=<%=maxPosPtaNum%>&amp;merchant_bus_name=<%=StringUtils.prepareURLPart(results.getValue("merchantBusName", String.class))%>&amp;doing_business_as=<%=StringUtils.prepareURLPart(results.getValue("doingBusinessAs", String.class))%>">Create New Merchant</a></div>
    </td>
    </tr>
<%} %>
<tr class="gridHeader"><th colspan="8">
<button name="full_action" class="cssButton" type="submit" value="updateTmpl">Import and Update Template</button>
<button name="full_action" class="cssButton" type="submit" value="importOnly">Import Only</button>
<button class="cssButton" type="button" onclick="history.go(-1);">Cancel</button>
</th></tr>
</table>
</form>
</div>
</div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />