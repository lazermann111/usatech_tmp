<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="tabDataContent">
<%boolean paramFlag = true;
				String errorMessage =request.getParameter("errorMessage");
				if(errorMessage != null && errorMessage.length() > 0){	
					%>
<div align="center"><span class="error"><%=StringUtils.encodeForHTML(errorMessage) %></span></div>
<% 
				    paramFlag = false;
		          }
				  String device_id= request.getParameter("device_id");
				  String action=request.getParameter("action");
				  if(paramFlag&&(action == null || action.length() == 0)){
				      action = request.getParameter("myaction");
				      if(action == null || action.length() == 0){
						%>
<div align="center"><span class="error">Required Parameter
Not Found:action</span></div>
<% 
					    paramFlag = false;
				      }
			      }
				  
						if(paramFlag && (device_id == null || device_id.length() == 0)){	
							%>
<div align="center"><span class="error">Required Parameter
Not Found:device_id</span></div>
<% 
						    paramFlag = false;
				      }
					
					if (paramFlag){	
					    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
				        Results templateList = (Results) inputForm.getAttribute("templateList");
						if (templateList == null || !templateList.next()){
						    %>
<div align="center"><span class="error">No templates exist!
</span></div>
<% 
						    
				      }else {
					  %>

<div class="innerTable">
<div class="tabHead" align="center">Point-of-Sale Payment Type - <%=StringUtils.encodeForHTML(action)%></div>
<form method="post" action="importPosptaTmplFunc.i"><input
	type="hidden" name="device_id" value="<%=StringUtils.encodeForHTMLAttribute(device_id) %>" />
<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td class="label" width="25%">Device ID</td>
			<td class="data"><%=StringUtils.encodeForHTML(device_id) %></td>
		</tr>
		<tr>
			<td class="label">Payment Type Template</td>
			<td class="data">
			<select id="pos_pta_tmpl_id" name="pos_pta_tmpl_id">
				<option value="">-- Choose a template --</option>
				<%
                templateList.setRow(0);
                while (templateList.next()) {%>
				<option value="<%= templateList.getFormattedValue(1) %>"><%= templateList.getFormattedValue(2)%></option>
				<%} %>
			</select>
			<input type="text" name="payment_template_search" id="payment_template_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("payment_template_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'payment_template_search_link')" />
			<a id="payment_template_search_link" href="javascript:getData('type=payment_template&id=pos_pta_tmpl_id&name=pos_pta_tmpl_id&search=' + document.getElementById('payment_template_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td class="label">Import Mode</td>
			<td class="data">
				<input type="radio" name="mode_cd" value="S" checked="checked" /> 1. Safe<br/>
				<input type="radio" name="mode_cd" value="MS" /> 2. Merge Safe<br/>
				<input type="radio" name="mode_cd" value="MO" /> 3. Merge Overwrite<br/>
				<input type="radio" name="mode_cd" value="O" /> 4. Overwrite<br/>
				<input type="radio" name="mode_cd" value="CO" /> 5. Complete Overwrite<br/>
			</td>
		</tr>
		<tr>
			<td class="label">Order Mode</td>
			<td class="data">
				<select name="order_cd">
					<option value="AE">Above Error Bin</option>
					<option value="T">Top</option>
					<option value="B">Bottom</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="label nowrap">Set Terminal Code to Device Serial #</td>
			<td><input type="checkbox" name="set_terminal_cd_to_serial" value="Y" /></td>
		</tr>
		<tr>
			<td class="label nowrap">Only Import "No Two-Tier Pricing" Setting</td>
			<td><input type="checkbox" name="only_no_two_tier_pricing" value="Y" /></td>
		</tr>
		<tr>
			<td class="data" colspan="2" align="center"><input type="submit"
				class="cssButton" style="width: 100px;" name="action" value="Import">&nbsp;
			<input type="button" class="cssButton" style="width: 120px;"
				name="action" value="View Template"
				onClick="javascript:window.location='editPosptaTmpl.i?pos_pta_tmpl_id='+this.form.pos_pta_tmpl_id.options[this.form.pos_pta_tmpl_id.selectedIndex].value;">
			<input type="reset" class="cssButton" style="width: 120px;"
				name="action" value="Reset Form">&nbsp; <input type="button"
				class="cssButton" style="width: 220px;" name="1action"
				value="Go Back to Device Profile"
				onClick="javascript:window.location='paymentConfig.i?device_id=<%=StringUtils.encodeForURL(device_id)%>&userOP=payment_types';">
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<b>Import Mode</b>
			<ul>
			    <li><b>Safe Mode</b>
			        <ul><li>
					Will not deactivate or alter any existing payment types.
					</li><li>
					May create new payment types if none exist for each <b>Payment Action Type / Payment Entry Method category</b> based on the selected template.
					</li></ul>
				</li>
				<li><b>Merge Safe Mode</b>
			        <ul><li>
					Will not deactivate or alter any existing payment types.
					</li><li>
					May create new payment types if none exist for each <b>Payment Type category</b> based on the selected template.
					</li></ul>
				</li>
				<li><b>Merge Overwrite Mode</b><br/>
			        <ul><li>
					May deactivate an existing payment type if one exists for each <b>Payment Type category</b> being imported.
					</li><li>
					Will create new payment types based on the selected template.
					</li></ul>
				</li>
				<li><b>Overwrite Mode</b><br/>
			        <ul><li>
					Will deactivate any existing payment types for each <b>Payment Action Type / Payment Entry Method category</b> being imported.
					</li><li>
					Will create new payment types based on the selected template.
					</li></ul>
				</li>
				<li><b>Complete Overwrite Mode</b><br/>
			        <ul><li>
					Will deactivate <b>any</b> existing payment types.
					</li><li>
					Will create new payment types based on the selected template.
					</li></ul>
				</li>
			   </ul>
			<b>Order Mode</b>
			<table class="noBorder">
				<tr><td>&nbsp;&nbsp;</td><td><b>Above Error Bin:</b></td><td>Inserts the new payment types above the first Error Bin and below all the other payment types.</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td><b>Top:</b></td><td>Inserts the new payment types above all the existing payment types.</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td><b>Bottom:</b></td><td>Adds the new payment types below all the existing payment types.</td></tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</form>
<%} }%>
</div>
<div class="spacer5"></div>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
