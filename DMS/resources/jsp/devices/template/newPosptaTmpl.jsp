<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="header_name" class="java.lang.String" scope="request" />
<jsp:useBean id="button_text" class="java.lang.String" scope="request" />
<jsp:useBean id="template_name" class="java.lang.String" scope="request" />
<jsp:useBean id="pos_pta_tmpl_name" class="java.lang.String" scope="request" />
<jsp:useBean id="id" class="java.lang.Object" scope="request" />
<jsp:useBean id="pos_pta_tmpl_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="template_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
		
		<c:choose>
			<c:when test="${not empty pos_pta_tmpl_name}">
				<c:set var="pos_pta_tmpl_name" value="Copy of ${pos_pta_tmpl_name}" />
			</c:when>
		</c:choose>
		
			<div class="tabDataContent">
			<div class="innerTable" >
					<div class="tableDataHead">
						<span class="txtWhiteBold">
						Payment Type Template Admin</span>
					    </div>
			<form method="post" action="editPosptaTmpl.i">								
			<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
			 <tr>
			  <th colspan="8" class="gridHeader"><c:out value="${header_name }" /></th>
			 </tr>			  
			  	<c:choose>
					<c:when test="${not empty pos_pta_tmpl_name}">
						<input type="hidden" name="pos_pta_tmpl_id" value="${id }"/>
					</c:when>
				</c:choose>
				 <tr>
				  <td>${template_name }</td>
				  <td><input type="text" name="name" size="30" value="${pos_pta_tmpl_name }"></td>
				 </tr>
				 <tr>
				  <td>${template_desc }</td>
		
				  <td><input type="text" name="desc" size="60" value="${pos_pta_tmpl_desc }"></td>
				 </tr>
				 <tr>
				  <td colspan="2" align="center"><input type="submit" class="cssButton" name="action" value="${button_text }">
				 </tr>		 	
			</table>
			</form>
		</div>
		</div>

	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />