
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.dms.action.HostAction"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />



<div class="tableContainer">
<%
    boolean paramFlag = true;
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String errorMessage = inputForm.getString("errorMessage", false);
    if (errorMessage != null && errorMessage.length() > 0)
    {
%>
<div align="center"><span class="error"><%=errorMessage%></span></div>
<%
    paramFlag = false;
    }
    String pos_pta_tmpl_id = request.getParameter("pos_pta_tmpl_id");
    if (paramFlag && (pos_pta_tmpl_id == null || pos_pta_tmpl_id.length() == 0))
    {
%>
<div align="center"><span class="error">Required Parameter
Not Found:pos_pta_tmpl_id</span></div>
<%
    paramFlag = false;
    }
    if (paramFlag)
    {
        Results templateData = (Results)inputForm.getAttribute("templateData");
        if (templateData == null)
        {
			%>
			<div align="center"><span class="error">Template not found:
			<%=StringUtils.encodeForHTML(pos_pta_tmpl_id)%></span></div>
			<%
    	}
        else
        {
            try
            {
            	templateData.next();
%>

<div class="tableHead">
<div class="tabHeadTxt">Payment Type Template Administration</div>
</div>
<div class="gridHeader"><%=templateData.getFormattedValue(2)%></div>
<form method="post" action="editPosptaTmpl.i">
<input type="hidden" name="pos_pta_tmpl_id" value="<%=StringUtils.encodeForHTMLAttribute(pos_pta_tmpl_id)%>" />
<input type="hidden" name="payment_action_type_cd" />
<table class="tabDataDisplayBorder">
	<thead>
		<tr class="gridHeader">
			<td>Payment Action Type</td>
			<td>Payment Entry Method</td>
			<td>Edit</td>
			<td>Priority</td>
			<td>Payment Type</td>
			<td>Merchant</td>
			<td>Activation Time</td>
			<td>Deactivation Time</td>
			<td>Currency</td>
			<td>Auth Pass-thru</td>
			<td>Auth Override</td>
			<td>Disable Debit Denial</td>
			<td>No Two-Tier Pricing</td>			
		</tr>
	</thead>
	<tbody>
		<%
		    			String last_payment_entry_method_cd = "";
						String last_payment_action_type_cd = "";
		                String pos_pta_tmpl_entry_name = null;
		                String merchant_desc = null;
		                Results paymentMethods = (Results)inputForm.getAttribute("paymentMethods");
		                int payment_entry_method_count = 0;
		                int loop_id = 0;
		                while (paymentMethods.next())
		                {
		                    if (!last_payment_entry_method_cd.equalsIgnoreCase(paymentMethods.getFormattedValue(1))
		                    		|| !last_payment_action_type_cd.equalsIgnoreCase(paymentMethods.getFormattedValue("payment_action_type_cd")))
		                    {
		%>
		<tr>
			<td colspan="12" class="noPadding"><div class="divider"></div></td>
		</tr>
		<%
		    }
		                    Results posPta = DataLayerMgr.executeQuery("GET_POS_PTA_TEMPLATE", new Object[] {pos_pta_tmpl_id, paymentMethods.getFormattedValue(1), paymentMethods.getFormattedValue("payment_action_type_cd")});
		                    payment_entry_method_count = 0;
		                    while (posPta.next())
		                        payment_entry_method_count++;
		                    if (payment_entry_method_count == 0)
		                    {
		%>
		<tr>
			<td class="accent"><%=paymentMethods.getFormattedValue("payment_action_type_desc")%></td>
			<td align="left" valign="middle">
				    <input type="radio" name="payment_entry_method_cd" value="<%=paymentMethods.getFormattedValue(1)%>"
					onclick="this.form.payment_action_type_cd.value = '<%=paymentMethods.getFormattedValue("payment_action_type_cd")%>';"/><%=paymentMethods.getFormattedValue(2)%></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center" colspan="9">---</td>
		</tr>
		<%
		    }
		                    else
		                    {
		                        posPta.setRow(0);
		                        while (posPta.next())
		                        {
		                            String temp = posPta.getFormattedValue(10);
		                            Integer key_id = (temp == null || temp.length() == 0) ? null : (new Integer(temp));
		                            //lookup merchant description
		                            merchant_desc = HostAction.getMerchantDesc(posPta.getFormattedValue(8), posPta.getFormattedValue(9), key_id);
		                            pos_pta_tmpl_entry_name = posPta.getFormattedValue(17).toUpperCase() + ": " + posPta.getFormattedValue(2);
		%>
		<tr>
			<td class="accent"><%=paymentMethods.getFormattedValue("payment_action_type_desc")%></td>
			<%
			    if (!last_payment_entry_method_cd.equalsIgnoreCase(paymentMethods.getFormattedValue(1)))
			                            {
			%>
			<td align="left" valign="middle" rowspan="<%=payment_entry_method_count%>">
				    <input type="radio" name="payment_entry_method_cd" value="<%=paymentMethods.getFormattedValue(1)%>" 
				        onclick="this.form.payment_action_type_cd.value = '<%=paymentMethods.getFormattedValue("payment_action_type_cd")%>';"/><%=paymentMethods.getFormattedValue(2)%></td>
			<%
			    }
			%>
			<td align="center"><input type="radio" id="radio_<%=loop_id%>" name="pos_pta_tmpl_entry_id" value="<%=posPta.getFormattedValue(1)%>"/></td>
			<td><input type="text" name="new_priority_<%=posPta.getFormattedValue(1)%>" value="<%=posPta.getFormattedValue(7)%>" size="3"/>
			    <input type="hidden" name="old_priority_<%=posPta.getFormattedValue(1)%>" value="<%=posPta.getFormattedValue(7)%>"/>
			    <input type="hidden" name="payment_entry_method_cd_<%=posPta.getFormattedValue(1)%>" value="<%=paymentMethods.getFormattedValue(1)%>"/>
				<input type="hidden" name="payment_action_type_cd_<%=posPta.getFormattedValue(1)%>" value="<%=paymentMethods.getFormattedValue("payment_action_type_cd")%>"/></td>
			<td><%=pos_pta_tmpl_entry_name%>&nbsp;</td>
			<td align="left"><%=merchant_desc%>&nbsp;</td>
			<td>
			<%
			    if (posPta.getFormattedValue(3) == null || posPta.getFormattedValue(3).length() == 0)
			                            {
			%><font color="orange">Inactive</font> <%
     }
                             else if (posPta.getFormattedValue(3).equals("0"))
                             {
 %>Now<%
     }
                             else
                             {
 %>Now plus <%=posPta.getFormattedValue(3)%> hours<%
     }
 %>&nbsp;</td>
			<td>
			<%
			    if (posPta.getFormattedValue(4) == null || posPta.getFormattedValue(4).length() == 0 )
			                            {
			%>Never<%
			    }
			                            else
			                            {
			%>Now plus <%=posPta.getFormattedValue(4)%> hours<%
			    }
			%>&nbsp;</td>
			<td align="center"><%=posPta.getFormattedValue("currency_cd")%> - <%=posPta.getFormattedValue("currency_name")%></td>
			<td align="center">
			<%
			    if (posPta.getFormattedValue(14).equalsIgnoreCase("Y"))
			                            {
			%><img src="/images/descending.gif"> <%
		     }
		 	%> &nbsp;</td>
			<td align="center"><%=(posPta.getFormattedValue(15) != null && posPta.getFormattedValue(15).length() > 0) ? "$" + posPta.getFormattedValue(15) + "/$"
                                            + posPta.getFormattedValue(16) : ""%>
			&nbsp;</td>
		 	<td align="center"><%="Y".equalsIgnoreCase(posPta.getFormattedValue("pos_pta_disable_debit_denial")) ? "Yes" : "&nbsp;"%></td>
			<td align="center"><%="Y".equalsIgnoreCase(posPta.getFormattedValue("no_convenience_fee")) ? "Yes" : "&nbsp;"%></td>
		</tr>
		<%
		    						last_payment_entry_method_cd = paymentMethods.getFormattedValue(1);
									last_payment_action_type_cd = paymentMethods.getFormattedValue("payment_action_type_cd");
		                            loop_id++;
		                        }
		                    }
		                }
		%>
	</tbody>
</table>
<div class="spacer10"></div>

<table class="tabDataDisplayBorder">
	<thead>
		<tr>
			<td class="header1">
			    <input type="submit" class="cssButton" name="action" value="Edit" onClick="javascript:return validateForm();" />
			    <input type="submit" class="cssButton" name="action" value="Reorder" onClick="javascript:return confirmSubmit1();" />
			    <input type="submit" class="cssButton" name="action" value="Add New" onclick="javascript:return validateAddNew();" />
			    <input type="text" name="name" value="<%=templateData.getFormattedValue(2)%>" size="30" />
			    <input type="text" name="desc" value="<%=templateData.getFormattedValue(3)%>" size="30" />
			    <input type="submit" class="cssButton" name="action" value="Update" onClick="return confirmSubmit1();" />
			    <input type="submit" class="cssButton" name="action" value="Delete Template" onClick="return confirmSubmit1();" />
			</td>
		</tr>
	</thead>
</table>

</form>
<%
    }
            catch (Exception e)
            {
%> Couldn't execute statement: <%=e.getMessage()%> <%
     }
         }
     }
 %>
<div class="spacer5"></div>
</div>
<script type="text/javascript">
       function confirmSubmit1() {
			var agree=window.confirm("Are you sure you want to continue with this operation?");
			if (agree)
				return true;
			else
				return false;
		}
        function validateForm() {
        	 var tlength;
             if (!(document.getElementsByName('pos_pta_tmpl_entry_id') === undefined)) {
                 tlength = document.getElementsByName('pos_pta_tmpl_entry_id').length;
                 if (tlength === undefined) {
                     tlength = 1;
                 }
             }
             for(var i = 0; i < tlength; i++) {
                if(document.getElementById('radio_'+i).checked) {
                   return true;
                }
             }
             alert("Please select a Payment Type in the Edit column");
             return false;
        }
        function validateAddNew() {
        	for(var i = 0; i < document.getElementsByName('payment_entry_method_cd').length; i++) {
                if(document.getElementsByName('payment_entry_method_cd')[i].checked)
                   return true;
            }
        	alert("Please select a Payment Entry Method");
        	return false;
        }
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
