<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.dms.action.HostAction"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String pos_pta_tmpl_id = request.getParameter("pos_pta_tmpl_id");
    String pos_pta_priority = inputForm.getString("pos_pta_priority", true);
    String payment_action_type_desc = inputForm.getString("payment_action_type_desc", true);
    String payment_entry_method_desc = inputForm.getString("payment_entry_method_desc", true);
    Results templateData = (Results)inputForm.getAttribute("templateData");
    templateData.next();
    Results payment_subtypes = (Results)inputForm.getAttribute("payment_subtypes");
    
    String errorMessage = inputForm.getString("errorMessage", false);
    if (errorMessage != null && errorMessage.length() > 0)
    {
%>
<div class="error"><%=StringUtils.encodeForHTML(errorMessage)%></div>
<%} else {%>
<div class="tableContainer">
<div class="tableContent">

<div class="gridHeader" align="center"><b>New
Payment Type Template Entry<br /><%=templateData.getFormattedValue(2)%></b></div>
<form method="post" action="editPosptaTmplEntry.i">
<input type="hidden" name="pos_pta_tmpl_id" value="<%=StringUtils.encodeForHTMLAttribute(pos_pta_tmpl_id)%>" />
<input type="hidden" name="pos_pta_priority" value="<%=StringUtils.encodeForHTMLAttribute(pos_pta_priority)%>" />
<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td width="25%" nowrap>Template ID</td>
			<td width="25%"><%=StringUtils.encodeForHTML(pos_pta_tmpl_id)%></td>
			<td width="25%" nowrap>Payment Action Type</td>
			<td width="25%"><%=StringUtils.prepareHTML(payment_action_type_desc)%></td>
		</tr>
		<tr>
			<td>Payment Entry Method</td>
			<td><%=payment_entry_method_desc%></td>
		</tr>
		<tr>
			<td nowrap>Payment Type</td>
			<td colspan="3"><select name="payment_subtype_id">
				<option value="">-- Choose a type --</option>
				<%
				    while (payment_subtypes.next())
				    {
				%>
				<option value="<%=payment_subtypes.getFormattedValue(1)%>"><%=payment_subtypes.getFormattedValue(2)%></option>
				<%
				    }
				%>
			</select></td>
		</tr>
		<tr>
			<td colspan="4" align="center"><input type="button"
				class="cssButton" style="width: 100px;" value="&lt; Back"
				onClick="javascript:window.location='editPosptaTmpl.i?pos_pta_tmpl_id=<%=StringUtils.encodeForURL(pos_pta_tmpl_id)%>';">
			<input type="submit" class="cssButton" style="width: 100px;"
				name="action" value="Next &gt;">&nbsp;</td>
		</tr>
	</tbody>
</table>
</form>
</div>
<div class="spacer5"></div>
</div>
<%} %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
