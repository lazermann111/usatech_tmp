<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.LinkedList"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.dms.action.HostAction"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />


<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String errorMessage = inputForm.getString("errorMessage", false);
    if (errorMessage != null && errorMessage.length() > 0)
    {
%>
<div class="tableContainer">
<div align="center"><span class="error"><%=errorMessage%></span>
</div>
<div class="spacer10"></div>
</div>
<%
    }
    else
    {

        HashMap<String, String> param = (HashMap)inputForm.getAttribute("params");
        int pos_pta_tmpl_id = Integer.parseInt(param.get("pos_pta_tmpl_id"));
        int pos_pta_tmpl_entry_id = Integer.parseInt(param.get("pos_pta_tmpl_entry_id"));
        String payment_action_type_cd = param.get("payment_action_type_cd");
        String payment_entry_method_cd = param.get("payment_entry_method_cd");
        String pp_payment_subtype_id = param.get("pp_payment_subtype_id");
        String pp_payment_subtype_key_id = param.get("pp_payment_subtype_key_id");
        String pp_authority_payment_mask_id = param.get("pp_authority_payment_mask_id");
        String regex_final = param.get("regex_final");
        String regex_final_bref = param.get("regex_final_bref");
        String pp_pos_pta_device_serial_cd = param.get("pp_pos_pta_device_serial_cd");
        String pp_pos_pta_encrypt_key = param.get("pp_pos_pta_encrypt_key");
        String pp_pos_pta_encrypt_key2 = param.get("pp_pos_pta_encrypt_key2");
        String pp_pos_pta_pin_req_yn_flag = param.get("pp_pos_pta_pin_req_yn_flag");
        String pp_pos_pta_passthru_allow_yn_flag = param.get("pp_pos_pta_passthru_allow_yn_flag");
        String pp_pos_pta_disable_debit_denial = param.get("pp_pos_pta_disable_debit_denial");
        String pp_no_convenience_fee = param.get("pp_no_convenience_fee");
        String pos_pta_pref_auth_amt = param.get("pos_pta_pref_auth_amt");
        String pos_pta_pref_auth_amt_max = param.get("pos_pta_pref_auth_amt_max");
        String payment_subtype_key_name_formatted = param.get("payment_subtype_key_name_formatted");
        String payment_action_type_desc = param.get("payment_action_type_desc");
        String payment_entry_method_desc = param.get("payment_entry_method_desc");
        String pp_pos_pta_activation_oset_hr = param.get("pp_pos_pta_activation_oset_hr");
        if(pos_pta_tmpl_entry_id == 0){
        	pp_pos_pta_activation_oset_hr = "0";
        }
        String pp_pos_pta_deactivation_oset_hr = param.get("pp_pos_pta_deactivation_oset_hr");
        String pos_pta_encrypt_key_hex = param.get("pos_pta_encrypt_key_hex");
        String pos_pta_encrypt_key2_hex = param.get("pos_pta_encrypt_key2_hex");
        String pp_currency_cd = param.get("pp_currency_cd");
        String authority_payment_mask_id = param.get("authority_payment_mask_id");
        String pp_pos_pta_priority = param.get("pp_pos_pta_priority");
        if (StringHelper.isBlank(pp_pos_pta_priority))
        	pp_pos_pta_priority = inputForm.getString("pos_pta_priority", false);

        String regex_ids_str = param.get("regex_ids_str");
        String regex_methods_str = param.get("regex_methods_str");
        String regex_bref_methods_str = param.get("regex_bref_methods_str");
        String regex_bref_codes_str = param.get("regex_bref_codes_str");
        String regex_bref_names_str = param.get("regex_bref_names_str");
        String regex_bref_descs_str = param.get("regex_bref_descs_str");
%>
<div class="tabDataContent">
<div class="innerTable">
<div class="tabHead" align="center">Point-of-Sale Payment Type
Template Entry</div>
<form method="post" action="editPosptaTmplEntryFunc.i" name="mainForm"
	onSubmit="javascript:return confirmSubmit2();"><input
	type="hidden" name="pos_pta_tmpl_id" value="<%=pos_pta_tmpl_id%>">
<input type="hidden" name="pos_pta_tmpl_entry_id"
	value="<%=pos_pta_tmpl_entry_id%>"> 
<input type="hidden" name="payment_action_type_cd" value="<%=payment_action_type_cd%>">
<input type="hidden" name="payment_entry_method_cd" value="<%=payment_entry_method_cd%>">
<input type="hidden" name="original_payment_subtype_id"
	value="<%=pp_payment_subtype_id%>"> <input type="hidden"
	name="original_payment_subtype_key_id"
	value="<%=pp_payment_subtype_key_id%>"> <input type="hidden"
	name="original_authority_payment_mask_id"
	value="<%=pp_authority_payment_mask_id%>"> <input type="hidden"
	name="original_pos_pta_regex" value="<%=regex_final%>"> <input
	type="hidden" name="original_pos_pta_regex_bref"
	value="<%=regex_final_bref%>"> <input type="hidden"
	name="original_pos_pta_device_serial_cd"
	value="<%=pp_pos_pta_device_serial_cd%>"> <input type="hidden"
	name="original_pos_pta_encrypt_key" value="<%=pp_pos_pta_encrypt_key%>">
<input type="hidden" name="original_pos_pta_encrypt_key2"
	value="<%=pp_pos_pta_encrypt_key2%>"> <input type="hidden"
	name="original_pos_pta_pin_req_yn_flag"
	value="<%=pp_pos_pta_pin_req_yn_flag%>"> <input type="hidden"
	name="original_pos_pta_passthru_allow_yn_flag"
	value="<%=pp_pos_pta_passthru_allow_yn_flag%>"> <input
	type="hidden" name="original_pos_pta_pref_auth_amt"
	value="<%=pos_pta_pref_auth_amt%>"> <input type="hidden"
	name="original_pos_pta_pref_auth_amt_max"
	value="<%=pos_pta_pref_auth_amt_max%>">
	<input type="hidden" name="original_pos_pta_disable_debit_denial" value="<%=pp_pos_pta_disable_debit_denial%>">
	<input type="hidden" name="original_no_convenience_fee" value="<%=pp_no_convenience_fee%>">
<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td class="label" width="18%" nowrap>Template Entry ID</td>
			<td class="data" width="82%"><%=pos_pta_tmpl_entry_id <= 0 ? "New Template Entry" : pos_pta_tmpl_entry_id%>
			</td>
		</tr>
		<tr>
			<td class="label" nowrap>Template ID</td>
			<td class="data"><%=pos_pta_tmpl_id%></td>
		</tr>
		<tr>
			<td class="label" nowrap>Payment Action Type</td>
			<td class="data"><%=payment_action_type_desc%></td>
		</tr>
		<tr>
			<td class="label" nowrap>Payment Entry Method</td>
			<td class="data"><%=payment_entry_method_desc%></td>
		</tr>
		<tr>
			<%
			    Results template_data = (Results)inputForm.getAttribute("template_data");
			%>
			<td class="label" nowrap>Template Name</td>
			<td class="data"><%=template_data.getFormattedValue(2)%></td>
		</tr>
		<tr>
			<td class="label" nowrap>Payment Type</td>
			<td class="data"><%
			    Results payment_subtypes = (Results)inputForm.getAttribute("payment_subtypes");
			        if (payment_subtypes.next())
			        {
			            
			%> <select name="payment_subtype_id"
				onchange="document.mainForm.action = 'editPosptaTmplEntry.i'; document.mainForm.submit();">
				<%
				    payment_subtypes.setRow(0);
				            while (payment_subtypes.next())
				            {
				%>
					<option value="<%=payment_subtypes.getFormattedValue(1)%>"
						<%=(payment_subtypes.getFormattedValue(1).equals(pp_payment_subtype_id) ? " selected" : "")%>><%=payment_subtypes.getFormattedValue(2)%></option>
					<%
				    }
				%>
				</select>
				<%
     }
         else
         {
 %>
				<input type="hidden" name="payment_subtype_id" value="N/A">
				N/A
				<%
     }
 %>
			</td>
		</tr>
		<tr>
			<td class="label" nowrap><%=payment_subtype_key_name_formatted%></td>
			<td class="data"><%
			    Results payment_subtype_table_key_ids = (Results)inputForm.getAttribute("payment_subtype_table_key_ids");
			        if (payment_subtype_table_key_ids != null && payment_subtype_table_key_ids.next())
			        {
			%> <select name="payment_subtype_key_id" class="smallText">
				<%
				    payment_subtype_table_key_ids.setRow(0);
				            while (payment_subtype_table_key_ids.next())
				            {
				%>
				<option
					value="<%=payment_subtype_table_key_ids.getFormattedValue(1)%>"
					<%=(payment_subtype_table_key_ids.getFormattedValue(1).equals(pp_payment_subtype_key_id) ? " selected" : "")%>><%=payment_subtype_table_key_ids.getFormattedValue(2)%></option>
				<%
				    }
				%>
			</select> <%
     }
         else
         {
 %> <input type="hidden" name="payment_subtype_key_id" value="N/A">N/A
			<%
     }
 %>
			</td>
		</tr>
		<tr>
			<td class="label" nowrap>Activation Offset Hours</td>
			<td class="data"><input type="text"
				name="pos_pta_activation_oset_hr"
				value="<%=pp_pos_pta_activation_oset_hr%>" size="60">
			(0=Now, Empty=Inactive)</td>
		</tr>
		<tr>
			<td class="label" nowrap>Deactivation Offset Hours</td>
			<td class="data"><input type="text"
				name="pos_pta_deactivation_oset_hr"
				value="<%=pp_pos_pta_deactivation_oset_hr%>" size="60">
			(0=Now, Empty=Never)</td>
		</tr>
		<tr>
			<td class="label" nowrap>Authority Terminal Code</td>
			<td class="data"><input type="text"
				name="pos_pta_device_serial_cd"
				value="<%=pp_pos_pta_device_serial_cd%>" size="60"></td>
		</tr>
		<tr>
			<td class="label" nowrap>Authority Encryption Key</td>
			<td class="data"><input type="text" name="pos_pta_encrypt_key"
				value="<%=pp_pos_pta_encrypt_key%>" size="60"> <input
				type="checkbox" name="pos_pta_encrypt_key_hex" value="1"
				<%=pos_pta_encrypt_key_hex%>> Hex Encoded</td>
		</tr>
		<tr>
			<td class="label" nowrap>Authority Encryption Key 2</td>
			<td class="data"><input type="text" name="pos_pta_encrypt_key2"
				value="<%=pp_pos_pta_encrypt_key2%>" size="60"> <input
				type="checkbox" name="pos_pta_encrypt_key2_hex" value="1"
				<%=pos_pta_encrypt_key2_hex%>> Hex Encoded</td>
		</tr>
		<tr>
			<td class="label" nowrap>PIN Required?</td>
			<td class="data"><select name="pos_pta_pin_req_yn_flag">
				<option value="N"
					<%="N".equals(pp_pos_pta_pin_req_yn_flag.toUpperCase()) ? " selected" : ""%>>No</option>
				<option value="Y"
					<%="Y".equals(pp_pos_pta_pin_req_yn_flag.toUpperCase()) ? " selected" : ""%>>Yes</option>
			</select></td>
		</tr>
		<tr>
			<td class="label" nowrap>Allow Auth Pass-through<br/>
			(on Decline/Failure)?</td>
			<td class="data"><select name="pos_pta_passthru_allow_yn_flag">
				<option value="N"
					<%="N".equals(pp_pos_pta_passthru_allow_yn_flag.toUpperCase()) ? " selected" : ""%>>No</option>
				<option value="Y"
					<%="Y".equals(pp_pos_pta_passthru_allow_yn_flag.toUpperCase()) ? " selected" : ""%>>Yes</option>
			</select></td>
		</tr>
		<tr>
			<td class="label" nowrap>Server Auth Override Amount</td>
			<td class="data"><input type="text"
				name="pos_pta_pref_auth_amt" id="override_amount_id"
				value="<%=pos_pta_pref_auth_amt%>" size="10"></td>
		</tr>
		<tr>
			<td class="label" nowrap>Server Auth Override Limit</td>
			<td class="data"><input type="text"
				name="pos_pta_pref_auth_amt_max" id="override_limit_id"
				value="<%=pos_pta_pref_auth_amt_max%>" size="10"></td>
		</tr>
		<tr>
			<td class="label" nowrap>Disable Debit Denial</td>
			<td class="data"><select name="pos_pta_disable_debit_denial">
				<option value="N"
					<%="N".equals(pp_pos_pta_disable_debit_denial) ? " selected" : ""%>>No</option>
				<option value="Y"
					<%="Y".equals(pp_pos_pta_disable_debit_denial) ? " selected" : ""%>>Yes</option>
			</select></td>
		</tr>
		<tr>
			<td class="label" nowrap>No Two-Tier Pricing</td>
			<td class="data">
				<select name="no_convenience_fee">
				<option value="N"
					<%="N".equals(pp_no_convenience_fee) ? " selected" : ""%>>No</option>
				<option value="Y"
					<%="Y".equals(pp_no_convenience_fee) ? " selected" : ""%>>Yes</option>
				</select>
				Requires 6.0.8 firmware for Gx, property list version 12 for G9, N/A for others
			</td>
		</tr>
		<tr>
			<td class="label" nowrap>Currency</td>
			<td class="data"><%
			    if (pp_currency_cd == null || pp_currency_cd.length() == 0)
			            pp_currency_cd = "USD";
			%> <select name="currency_cd">
				<%
				    Results currency_list = (Results)inputForm.getAttribute("currency_list");
				        while (currency_list.next())
				        {
				%>
				<option value="<%=currency_list.getFormattedValue(1)%>"
					<%=currency_list.getFormattedValue(1).equals(pp_currency_cd) ? " selected" : ""%>><%=currency_list.getFormattedValue(1)%>
				- <%=currency_list.getFormattedValue(2)%></option>
				<%
				    }
				%>
			</select></td>
		</tr>
		<tr>
			<td class="label" nowrap>Priority in Category</td>
			<%
			    if (StringHelper.isBlank(pp_pos_pta_priority))
			    	pp_pos_pta_priority = "1";
			%>
			<td class="data"><input type="text" name="pos_pta_priority"
				value="<%=pp_pos_pta_priority%>" size="3"></td>
		</tr>
		<tr>
			<td class="label" valign="top">Card Data Validation
			and Decoding Method</td>
			<td class="data">
			<table class="tabDataDisplayNoBorder">
				<tbody>
					<tr>
						<td class="label" width="13%" nowrap>Method&nbsp;</td>
						<td class="data" width="87%"><select name="pos_pta_regex"
							onChange="javascript:regexSelectedMethod(this.form);" class="smallText">
							<%
							    LinkedList<HashMap> toggle_regex_methods = (LinkedList<HashMap>)inputForm.getAttribute("toggle_regex_methods");
							        for (HashMap<String, String> map : toggle_regex_methods)
							        {
							%>
							<option value="<%=map.get("value")%>" <%=map.get("default")%>><%=map.get("text")%></option>
							<%
							    }
							%>
						</select> <input type="hidden" name="authority_payment_mask_id"
							value="<%=authority_payment_mask_id%>"> <input
							type="hidden" name="pos_pta_regex_bref"
							value="<%=regex_final_bref%>"></td>
					</tr>
					<tr>
						<td class="label" nowrap>Card Data Regex&nbsp;</td>
						<td class="data"><input type="text" name="selected_regex"
							value="<%=regex_final%>" size="80" disabled="disabled"></td>
					</tr>
					<tr>
						<td class="label" nowrap>Backreference&nbsp;</td>
						<td class="data"><input type="text"
							name="selected_regex_bref" value="<%=regex_final_bref%>"
							size="80" disabled="disabled"></td>
					</tr>
					<tr>
						<td class="label" nowrap>Sample Card Data&nbsp;</td>
						<td class="data"><input type="text"
							name="test_regex_card_num" value="" size="80"> <input
							type="button" class="cssButton" style="width: 50px;"
							name="test_regex" value="Test"
							onClick="javascript:testRegex(this.form);"></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr class="gridHeader">
			<td class="data" colspan="4" align="center">
				<input type="submit" class="cssButton" name="userAction" value="Save" />
 <%
     if (pos_pta_tmpl_entry_id > 0) {
 %> 
 				<input type="submit" class="cssButton" name="userAction" value="Delete" /> 
 <%
     }
 %>
 				<input type="button" class="cssButton" value="Cancel" onClick="javascript:window.location='editPosptaTmpl.i?pos_pta_tmpl_id=<%=pos_pta_tmpl_id%>';" />
			</td>
		</tr>
	</tbody>
</table>
</form>
</div>
<script language="JavaScript" type="text/javascript">
function testRegex(myform) {
	var regexBrefArr = new Array(<%=regex_bref_methods_str%>);
	var regexBrefCodes = new Array(<%=regex_bref_codes_str%>);
	var regexBrefNames = new Array(<%=regex_bref_names_str%>);
	var regexBrefDescs = new Array(<%=regex_bref_descs_str%>);
	var regexBrefTypes = new Array();
	for (var i = 0; i < regexBrefCodes.length; i++) {
		regexBrefTypes[regexBrefCodes[i]] = regexBrefNames[i];
	}
	var myRegex = new RegExp(myform.pos_pta_regex[myform.pos_pta_regex.selectedIndex].value, 'i');
	var myResArr = myRegex.exec(myform.test_regex_card_num.value);
	var myRegexBref = regexBrefArr[myform.pos_pta_regex.selectedIndex];
	if (myResArr == null) {
		alert("Invalid Match.\nPlease check that you have selected the correct method\nand have entered a correct sample string.");
	}	
	else {
		var brefResStr = "";
		if (myRegexBref != '') {
			brefResStr = "\nMatched substring results are as follows:\n";
			var myBrefCodeArr = myRegexBref.split('|');
			for (var i = 0; i < myBrefCodeArr.length; i++) {
				var myBrefPartsArr = myBrefCodeArr[i].split(':');	//0=num, 1=db id
				brefResStr = brefResStr + " - " + regexBrefTypes[myBrefPartsArr[1]] + ": " + myResArr[myBrefPartsArr[0]] + "\n";
			}
		}
		alert('Successful Match!' + brefResStr);
	}
}

function regexSelectedMethod(myForm) {
	var regexIdArr = new Array(<%=regex_ids_str%>);
	var regexArr = new Array(<%=regex_methods_str%>);
	var regexBrefArr = new Array(<%=regex_bref_methods_str%>);
	if(myForm.pos_pta_regex.value!=''){
		myForm.selected_regex.value = regexArr[myForm.pos_pta_regex.selectedIndex];
		myForm.selected_regex_bref.value = regexBrefArr[myForm.pos_pta_regex.selectedIndex];
	}else{
		myForm.selected_regex.value = '';
		myForm.selected_regex_bref.value = '';
	}		
	myForm.authority_payment_mask_id.value = regexIdArr[myForm.pos_pta_regex.selectedIndex];
	myForm.pos_pta_regex_bref.value = myForm.selected_regex_bref.value;
}
</script> 
</div>
<%
     }
 %>
<div class="spacer5"></div>
<script language="JavaScript" type="text/javascript">
function confirmSubmit2() {
	var agree=window.confirm("Are you sure you want to continue with this operation?");
	if (agree) {
        if(document.getElementById('override_amount_id').value == '' && document.getElementById('override_limit_id').value == '') {
            return true;
        }
        if(document.getElementById('override_amount_id').value > 0 && document.getElementById('override_limit_id').value == '') {
          alert('Please enter Override Limit');
          return false; 
        }
        if(document.getElementById('override_amount_id').value.match('^[0-9]+(\\.[0-9]{0,2})?\$') == null) {
            alert("Invalid Override Amount");
            return false;
        }
        if(document.getElementById('override_limit_id').value.match('^[0-9]+(\\.[0-9]{0,2})?\$') == null) {
            alert("Invalid Override Limit");
            return false;
        }
	    return true;
     } else {
	    return false;
     }
}
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
