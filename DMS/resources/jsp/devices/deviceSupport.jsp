<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="simple.servlet.BasicServletUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>

<%
	BasicServletUser user = (BasicServletUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
	String emailAddress = user.getEmailAddress();
	String msg = RequestUtils.getAttribute(request, "message", String.class, false);
	String err = RequestUtils.getAttribute(request, "error", String.class, false);
	if (StringUtils.isBlank(msg) && StringUtils.isBlank(err)) {
		for (MessagesInputSource.Message message : RequestUtils.getMessages(request)) {
			if ("error".equals(message.getType()) || "warn".equals(message.getType())) {
				if (StringUtils.isBlank(err))
					err = message.toHtml();
				else
					err += "<br/>" + message.toHtml();
			} else {
				if (StringUtils.isBlank(msg))
					msg = message.toHtml();
				else
					msg += "<br/>" + message.toHtml();
			}
		}
	}
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="fullPageFormContainer">

	<div class="tableHead">
		<div class="tabHeadTxt">
			<span class="txtWhiteBold">Device Support</span>
		</div>
	</div>

	<form method="post" action="deviceSupport.i"
		onsubmit="return doSubmit()" enctype="multipart/form-data">

		<table class="padding3">
			<%
				if (msg != null && msg.length() > 0) {
			%>
			<tr>
				<td colspan="2" class="status-info"><%=msg%></td>
			</tr>
			<%
				}
			%>
			<%
				if (err != null && err.length() > 0) {
			%>
			<tr>
				<td colspan="2" class="status-error"><%=err%></td>
			</tr>
			<%
				}
			%>

			<tr>
				<td class="label">Cellular Device Support</td>
			</tr>

			<tr>
				<td colspan="2"><hr /></td>
			</tr>

			<tr>
				<td colspan="2" class="label">Upload a CSV File with these
					values in each line: {device serial #},{MEID or SIM #}[,{Phone #}]</td>
				<td colspan="2" class="label">Host type<font color="red">*</font></td>
			</tr>
			<tr>
				<td colspan="2"><input type="file" name="file_data"
					id="file_data" size="80" /></td>
				<td><select name="host_type_id" id="host_type_id">
						<%
							Results results = DataLayerMgr.executeQuery("GET_HOST_TYPE_ID_LIST", null);
							while (results.next()) {
						%>
						<option value="<%=results.getValue("hostTypeId")%>"><%=results.getFormattedValue("hostTypeDesc")%></option>
						<%
							}
						%>
				</select></td>
			<tr>
				<td colspan="2" class="label">Enter an email that will receive
					notification that the processing is complete (optional):</td>
			<tr>
				<td><input type="email" id="notifyEmail" name="notifyEmail"
					value="<%=emailAddress%>" /></td>
			<tr>
				<td colspan="2" align="center"><input type="submit"
					value="Submit" class="cssButton" /></td>
			</tr>
		</table>

	</form>

</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
	function doSubmit() {
		var file_data = document.getElementById("file_data").value.trim();
		if (file_data == '') {
			alert('Please choose file to upload');
			return false;
		}
		if(document.getElementById("notifyEmail").value.trim() == ''){
			alert('Please enter an email for notification');
			return false;
		}
		var notify_email = document.getElementById("notifyEmail").value.trim();
		if (notify_email == ''){
			alert('Please enter an email for notification');
			return false;
		}
		return true;
	}
</script>