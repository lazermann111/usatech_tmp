<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.action.SerialAllocatAction"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<%
    simple.io.Log log = simple.io.Log.getLog();
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String txtSerialNumber = inputForm.getString("txtSerialNumber", false);
    String userOP = inputForm.getString("userOP", false);
    String insertType = inputForm.getString("insertType", false);
    String email = inputForm.getString("email", false);
    String desc = inputForm.getString("desc", false);
    int deviceTypeId = inputForm.getInt("deviceTypeId", false, -1);
    int serialFormatId = inputForm.getInt("serialFormatId", false, -1);
    long sequenceKey = inputForm.getLong("sequenceKey", false, -1);
    long numToAllocate = inputForm.getLong("numToAllocate", false, 0);
    long numToStart = inputForm.getLong("numToStart", false, 0);
%>

<script type="text/javascript">

var formatData = new Array();
var regexData = "";

<%
	Results deviceTypes = null;
	Map<Integer, List<NameValuePair>> serialFormats = null;
     //if (StringHelper.isBlank(userOP))
     {
         int i = 0;
         List<String> regexList = new ArrayList<String>();
         deviceTypes = (Results)inputForm.getAttribute("deviceTypes");
         
         serialFormats = (Map<Integer, List<NameValuePair>>)inputForm.getAttribute("serialFormats");
         if(deviceTypes==null && serialFormats==null){
         	deviceTypes = SerialAllocatAction.getDeviceTypes();
         	serialFormats = SerialAllocatAction.getSerialFormats(deviceTypes);
         }
         while (deviceTypes.next())
         {
             int device_type_id = deviceTypes.getValue("device_type_id", int.class);
             for (NameValuePair serialFormat : serialFormats.get(device_type_id))
             {%>
				formatData[<%=i++%>] = new Array("<%=String.valueOf(device_type_id)%>", "<%=serialFormat.getName()%>", "<%=serialFormat.getValue()%>");
			 <%}
           	 regexList.add(deviceTypes.getValue("device_type_serial_cd_regex", String.class));
       	}
       	deviceTypes.setRow(0);
       	i = 0;
       	for (String regex : regexList)
       	{
           	i++;%>
			regexData = regexData + "<%=regex%>";
			<%if (i < regexList.size())
           	{%>
				regexData = regexData + "|";
			<%}


		}
        
     }	
            
   %>

function changeFormat() {
	var deviceType = document.getElementById("deviceTypeId").value;
	var options = "<select name='serialFormatId'><option selected='selected' value='-1'>Please Select</option>";
	

	for(var i = 0; i < formatData.length; i++) {
		if(deviceType == formatData[i][0]) {
			options += "<option value='";
			options += formatData[i][1];
			options += "'>";
			options += formatData[i][2];
			options += "</option>";
		}
	}
	options += "</select>";
	document.getElementById("serialFormatId").innerHTML = options;
}

function validate() {
	with(document.serialNumberAllocationForm) {
		if("Assign Numbers" == userOP.value) {
			if(serialFormatId.value == -1) {
				alert("You must select a device type and a serial number format!");
				return false;
			}
			return true;
		} else if("List Assigned Logs" == userOP.value) {
			var reg = new RegExp(regexData);
			var str = trim(txtSerialNumber.value);
			if(!reg.test(str.toUpperCase())) {
				alert('You must enter correct format for serial number!');
				return false;
			} 
		    return true;
		} else if("Submit" == userOP.value) {
			var reg = new RegExp("^([A-Za-z0-9_\\\-\\\.])+\@([A-Za-z0-9_\\\-\\\.])+\.([A-Za-z]{2,4})$");
			var array = email.value.split(/ |,/);
			for(var i=0; i<array.length; i++) {
				if(!reg.test(array[i])) {
					alert("You must enter valid email address!");
					return false;
				}
			}
			if(desc.value == "") {
				alert("You must enter description of your action!");
		        return false;
			}
			if(numToAllocate.value == "") {
				alert("You must enter allocated amount!");
		        return false;
			}
			return true;
		}
		return false;
	}
}

function populateStart(val) {
    var obj = document.getElementById("numToStart");
    obj.value = val;
}
</script>

<div class="formContainer">
<%
    if (("Assign Numbers".equals(userOP)) && (deviceTypeId >= 0) && (serialFormatId > 0))
    {
        String regex = (String)inputForm.getAttribute("regex");
        String prefix = (String)inputForm.getAttribute("prefix");
        String suffix = (String)inputForm.getAttribute("suffix");
        List<Long> sequences = (List<Long>)inputForm.getAttribute("sequences");
%>
<div class="tableHead">
<div class="tabHeadTxt">USAT Device Serial Number Assignment Utility</div>
</div>
<div class="innerTable">
<form name="serialNumberAllocationForm"
	action="serialNumberAllocation.i" method="post"
	onsubmit="return validate();">
<table class="tabDataDisplayBorder">
	<tbody>
		<tr>
			<td>Prefix</td>
			<td>Value</td>
			<td>Suffix</td>
			<td rowspan="2"><%=(regex == null) ? "G5[0-9]{6}" : StringUtils.encodeForHTML(regex)%></td>
		</tr>
		<tr>
			<td>
			<%
			    if (prefix == null)
			        {
			%> <label style="color: red">G5</label> <%
     }
         else
         {
 %>
			<input style="" type="text" name="prefix" value="<%=StringUtils.encodeForHTMLAttribute(prefix)%>"
				readonly="readonly" /> <%
     }
 %>
			</td>
			<td><label style="color: green">123456</label></td>
			<td>
			<%
			    if (suffix == null)
			        {
			%> <label style="color: red">NA</label> <%
     }
         else
         {
 %>
			<label style="color: red"><%=StringUtils.encodeForHTML(suffix)%></label> <%
     }
 %>
			</td>
		</tr>
	</tbody>
</table>
<div class="spacer10"></div>
<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td class="label" width="35%">Enter a valid Email Address:</td>
			<td class="data" width="65%"><input style="" type="text"
				name="email" size="40" /></td>
		</tr>
		<tr>
			<td class="label">Please Enter Description of the Job:</td>
			<td class="data"><textarea rows="5" cols="50" name="desc"></textarea></td>
		</tr>
		<tr>
			<td class="label">Number of Serial Num to Allocate:</td>
			<td class="data"><input type="text" name="numToAllocate"
				id="numToAllocate" class="cssText"
				onkeypress="return numbersonly(event, false, false);" /></td>
		</tr>
		<tr>
			<td class="label">Serial Number to Start At:</td>
			<td class="data"><input type="text" name="numToStart"
				id="numToStart" class="cssText"
				onkeypress="return numbersonly(event, false, false);" /></td>
		</tr>
		<tr>
			<td class="label"><input type="radio" name="insertType"
				value="N" />Next Available</td>
			<td class="data"><input type="radio" name="insertType" value="S"
				checked="checked" />Sequential Guaranteed</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit"
				value="Submit" class="cssButton"
				onclick="document.serialNumberAllocationForm.userOP.value='Submit'" />
			</td>
		</tr>
	</tbody>
</table>
<input type="hidden" name="userOP" /> <input type="hidden"
	name="deviceTypeId" value="<%=deviceTypeId%>" /> <input type="hidden"
	name="serialFormatId" value="<%=serialFormatId%>" /> 
	<input type="hidden"
	name="sequenceKey" value="<%=sequenceKey%>" /><br />
<br />
<table class="tabDataDisplayBorder">
	<thead>
		<tr class="gridHeader">
			<td>Range Start</td>
			<td>Range End</td>
			<td>Range Count</td>
		</tr>
	</thead>
	<tbody>
		<%
		    long sum = 0;
		        long rowNum = 0;
		        for (int index = 1; index < sequences.size(); index++)
		        {
		            long diff = sequences.get(index) - sequences.get(index - 1) - 1;
		            if (diff < 1)
		            {
		                continue;
		            }
		            sum += diff;
		            long start = sequences.get(index - 1) + 1;
		            long end = sequences.get(index) - 1;
		%>
		<tr class="<%=(rowNum % 2 == 0) ? "row0" : "row1"%>">
			<td><a id="startValue"
				href="javascript:populateStart(<%=String.valueOf(start)%>);"><%=start%></a></td>
			<td><%=end%></td>
			<td><%=diff%></td>
		</tr>
		<%
		    rowNum++;
		        }
		%>
		<tr class="<%=(rowNum % 2 == 0) ? "row0" : "row1"%>">
			<td colspan="2">Total Available Numbers ==&gt;&nbsp;</td>
			<td><%=sum%></td>
		</tr>
	</tbody>
</table>
</form>
<%
    }
    else if ("List Assigned Logs".equals(userOP))
    {
        StringBuilder serials = new StringBuilder();
        List<NameValuePair> items = new ArrayList<NameValuePair>();
        Results serialInfoBySerialNumber = (Results)inputForm.getAttribute("serialInfoBySerialNumber");
        Results serialInfoByInfoId = (Results)inputForm.getAttribute("serialInfoByInfoId");
        while (serialInfoBySerialNumber.next())
        {
            serials.append(serialInfoBySerialNumber.getFormattedValue("device_serial_cd"));
            serials.append("\n");
            items.add(new NameValuePair(serialInfoBySerialNumber.getFormattedValue("device_serial_id"), serialInfoBySerialNumber.getFormattedValue("device_serial_cd")));
        }
        serialInfoBySerialNumber.setRow(0);
        boolean hasInfo = (serialInfoByInfoId != null && serialInfoByInfoId.next());
%>
<form name="serialNumberAllocationForm"
	action="serialNumberAllocation.i" method="post">
	<input type="hidden" name="userOP" />
<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td class="label">Email:</td>
			<td class="data"><%=hasInfo ? serialInfoByInfoId.getFormattedValue("device_serial_info_email") : ""%></td>
		</tr>
		<tr>
			<td class="label">Description:</td>
			<td class="data"><%=hasInfo ? serialInfoByInfoId.getFormattedValue("device_serial_info_desc") : ""%></td>
		</tr>
		<tr>
			<td class="label">You can select All<br />
			Device Serial CD from right</td>
			<td class="data"><textarea rows="10" cols="15"><%=serials.toString()%></textarea>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2"><input type="submit"
				value="Start Over" class="cssButton"
				onclick="document.serialNumberAllocationForm.userOP.value='Start Over'" />
			</td>
		</tr>
		<tr>
			<td colspan="2" class="tabHead">Assigned Device List</td>
		</tr>
		<tr class="gridHeader">
			<td>Device Serial ID</td>
			<td>Device Serial CD</td>
		</tr>
		<%
		    long rowNum = 0;
		        for (NameValuePair item : items)
		        {
		%>
		<tr class="<%=(rowNum % 2 == 0) ? "row0" : "row1"%>">
			<td><%=item.getName()%></td>
			<td><%=item.getValue()%></td>
		</tr>
		<%
		    rowNum++;
		        }
		%>
	</tbody>
</table>
</form>
<%
    }
    else if ("Submit".equals(userOP) && serialFormatId > 0 && numToAllocate >= 0)
    {
%>
<form name="serialNumberAllocationForm"
	action="serialNumberAllocation.i" method="post">
	<input type="hidden" name="userOP" />
<div style="text-align: center;">
<%
    List<String> assigned = (List<String>)inputForm.getAttribute("assigned");
        if (assigned!=null && !assigned.isEmpty())
        {
            StringBuilder serialNumbers = new StringBuilder();
            for (String serial : assigned)
            {
                serialNumbers.append(" ");
                serialNumbers.append(serial);
                serialNumbers.append("\n");
            }
%> <br />
<h3 class="error"><%=assigned.size()%>&nbsp;Generated Serial
Numbers:</h3>
<h3 class="error">A copy of this list has been emailed to you for
your records.</h3>
<textarea rows="10" cols="15"><%=serialNumbers.toString()%></textarea>
<h3 class="error">Select all and Copy Paste.</h3>
<%
		try{SerialAllocatAction.clearSequenceMap(sequenceKey);}catch(Exception e){}
        }
        else
        {
%> <br />
<input type="hidden"
	name="sequenceKey" value="<%=sequenceKey%>" />
<h2 class="error">There are existing records in the range!</h2>
<br />
<h3 class="error">Unable to assign serial numbers in the range you
requested! <br />
There are not enough available serial numbers to complete your request!</h3>
<%
    }
%> <input type="submit" value="Start Over" class="cssButton"
	onclick="document.serialNumberAllocationForm.userOP.value='Start Over'" />
</div>
<br />
</form>
<%
    }
    else
    {
%>
<div class="tableHead">
<div class="tabHeadTxt">USAT Device Serial Number Assignment Utility</div>
</div>
<form name="serialNumberAllocationForm"
	action="serialNumberAllocation.i" method="post"
	onsubmit="return validate();">
<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td width="%65">Device Type:</td>
			<td width="35%"><select name="deviceTypeId" id="deviceTypeId"
				onchange="changeFormat();">
				<option selected="selected" value="-1">Please Select</option>
				<%
				    if (deviceTypes != null)
				        {
				            while (deviceTypes.next())
				            {
				%>
				<option value="<%=deviceTypes.getFormattedValue("device_type_id")%>"><%=deviceTypes.getFormattedValue("device_type_desc")%></option>
				<%
				    }
				            deviceTypes.setRow(0);
				        }
				%>
			</select></td>
		</tr>
		<tr>
			<td>Serial Number Format:</td>
			<td>
			<div id="serialFormatId"><select name="serialFormatId">
				<option selected="selected" value="-1">Please Select</option>
			</select></div>
			</td>
		</tr>
		<tr>
			<td>To assign serial numbers, please
			select Device Type and Serial Number Format and press Assign Numbers button.</td>
			<td><input type="submit" value="Assign Numbers"
				class="cssButton"
				onclick="document.serialNumberAllocationForm.userOP.value='Assign Numbers'" />
			</td>
		</tr>
		<tr>
			<td>Enter Serial Number:</td>
			<td><input style="" type="text" name="txtSerialNumber"
				value="<%=StringHelper.isBlank(txtSerialNumber) ? "" : txtSerialNumber%>" />
			</td>
		</tr>
		<tr>
			<td>To display serial numbers entered previously,
			please enter a valid Serial Number and press List Assigned Logs button.</td>
			<td><input type="submit" value="List Assigned Logs"
				class="cssButton"
				onclick="document.serialNumberAllocationForm.userOP.value='List Assigned Logs'" />
			</td>
		</tr>
	</tbody>
</table>
<input type="hidden" name="userOP" /></form>
<%
    }
%>
</div>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />