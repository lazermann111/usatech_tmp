<%@page import="java.util.List"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="com.usatech.dms.action.FileActions"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
 
<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
String userOp = inputForm.getString("userOP", false);
int cmdInt = inputForm.getInt("cmd_cnt", false, 5);
int searchFileType = inputForm.getInt("file_transfer_type", false, -1);
List<NameValuePair> nvp_list_fileTransferTypes = (List<NameValuePair>)dms_values_list_fileTransferTypesList.getList();

%>
<c:set var="searchFileType"><%=searchFileType%></c:set>

<form method="get" action="fileTransfer.i">
<div class="innerTable">
<div class="gridHeader">
	<span>File Type&nbsp;</span> <span><select name="file_transfer_type" id="file_transfer_type"> 
	<option value="-1"<%if(searchFileType == -1) {%> selected="selected"<%} %>>-- All --</option>
	<option value="-2"<%if(searchFileType == -2) {%> selected="selected"<%} %>>-- All DEX Files --</option>
    <c:forEach var="nvp_item_file_transfer_type" items="<%=nvp_list_fileTransferTypes%>">
		<c:choose>
			<c:when test="${nvp_item_file_transfer_type.value == searchFileType}">
				<option value="${nvp_item_file_transfer_type.value}" selected="selected">${nvp_item_file_transfer_type.name}</option>
			</c:when>
			<c:otherwise>
				<option value="${nvp_item_file_transfer_type.value}">${nvp_item_file_transfer_type.name}</option>
			</c:otherwise>
		</c:choose>	    	
	   </c:forEach>
	  </select></span>
	<span>&nbsp;File Transfer History (&nbsp;</span> <input type="text" class="cssText" name="cmd_cnt" size="2" value="<%=cmdInt%>"
	  onkeypress="return numbersonly(event, false, false);"><span>&nbsp;Most Recent )&nbsp;</span>
	<input type="submit" class="cssButton" name="userOP" value="List" />
	<input type="hidden" name="device_id" value="<%=device.getId()%>">
</div>
</div>
<%		if("List".equals(userOp)){
		Results histories = FileActions.getFileTransferHistory(device.getDeviceName(), searchFileType, cmdInt);
			if (histories != null) {
	%>
<table class="tabDataDisplay">
		<tr class="gridHeader">
			<td>Transfer ID</td>
			<td>File Name</td>
			<td>Direction</td>
			<td>File Type</td>
			<td>Bytes</td>
			<td>Time</td>
			<td>Status</td>
			<td>Call ID</td>
		</tr>
		<%
			while (histories.next()) {
		%>
		<tr>
			<td><%=histories.getFormattedValue("device_file_transfer_id")%></td>
			<td><a href="fileDetails.i?file_transfer_id=<%=histories.getFormattedValue("file_transfer_id")%>"><%=histories.getFormattedValue("file_transfer_name")%></a></td>
			<td><%=histories.getFormattedValue("device_file_transfer_direction")%></td>
			<td><%=histories.getFormattedValue("file_transfer_type_name")%></td>
			<td><%=histories.getFormattedValue("file_content_size")%></td>       
            <td><%=histories.getFormattedValue("device_file_transfer_ts")%></td>
			<td><%=histories.getFormattedValue("device_file_transfer_status")%></td>
		    <td><%if (!StringHelper.isBlank(histories.getFormattedValue("global_session_cd"))) {%><a href="/deviceCallInLog.i?session_cd=<%=histories.getFormattedValue("global_session_cd")%>"><%=histories.getFormattedValue("session_id")%></a><%} %>&nbsp;</td>
		</tr>
		<%
			}
		%>
</table>
<%
	}
	}
%>
</form>