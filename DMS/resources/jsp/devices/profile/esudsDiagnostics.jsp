<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="device_id" class="java.lang.Object" scope="request" />
<jsp:useBean id="esudsDiagnostics" type="simple.results.Results" scope="request" />

<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	
	<c:when test="${not empty(errorMessage)}">
		<div class="tableContainer">				    
			<span class="error">${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>
	
			
			<div align="center">
			<div class="tabDataContent">
			<div class="innerTable" >
					<div class="tableDataHead" align="center">
						<span class="txtWhiteBold">
						Active Room Diagnostic Codes</span>
					    </div>								
					<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
						
					<thead>
							<tr class="sortHeader">
								  <td>Host ID</td>
								  <td>Port</td>
								  <td>Type</td>
								  <td>Position</td>
								  <td>Label</td>
								  <td>Code</td>
								  <td>Value</td>
								  <td>Started </td>
								  <td>Last Reported</td>							
							</tr>
						</thead>
						
						<c:choose>
							<c:when test="${fn:length(esudsDiagnostics)>0}" >
							
								<tbody>
								<c:forEach var="esuds_diag_item" items="${esudsDiagnostics}">
									<tr>
									    <td nowrap align="left"><a href="hostProfile.i?host_id=${esuds_diag_item.host_id }">${esuds_diag_item.host_id }</a>&nbsp;</td>
									    <td nowrap align="left">${esuds_diag_item.host_port_num }&nbsp;</td>
									    <td nowrap align="left">${esuds_diag_item.host_type_desc }&nbsp;</td>
									    <td nowrap align="left">
									    	<c:choose>
									    		<c:when test="${esuds_diag_item.host_position_num eq '0'}" >
									    			<c:out value="Bottom" />
									    		</c:when>
									    		<c:otherwise>
									    			<c:out value="Top" />
									    		</c:otherwise>
									    	</c:choose>&nbsp;
									    </td>
									    <td nowrap align="left">${esuds_diag_item.host_label_cd }&nbsp;</td>
									    <td nowrap align="left">${esuds_diag_item.host_diag_cd }&nbsp;</td>
									    <td nowrap align="left">${esuds_diag_item.host_diag_value }&nbsp;</td>
									    <td nowrap align="left">${esuds_diag_item.host_diag_start_ts }&nbsp;</td>
									    <td nowrap align="left">${esuds_diag_item.host_diag_last_reported_ts }&nbsp;</td>
									    
									</tr>
								</c:forEach>
			
								</tbody>
						</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="9" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>
		</table>
		</div>
		</div>
		</div>
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />