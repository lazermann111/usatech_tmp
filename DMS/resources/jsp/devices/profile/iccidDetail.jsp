<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.dms.device.MessageMap"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<script>
function setInput(val) {
   if(val == 'allocated_by_id') {
      document.getElementById(val).innerHTML = '<input style="cssText" type="text" name="allocated_by" size="40">';
   }
   if(val == 'allocated_to_id') {
      document.getElementById(val).innerHTML = '<input style="cssText" type="text" name="allocated_to" size="40">';
   }
   if(val == 'activated_by_id') {
      document.getElementById(val).innerHTML = '<input style="cssText" type="text" name="activated_by" size="40">';
   }
}
</script>

<div>&nbsp;</div>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Results iccidDetail = (Results) inputForm.getAttribute("iccidDetail");
boolean isError = false;
String errorMessage = "";
String iccid_param = request.getParameter("iccid");
if(iccid_param == null) {
	isError = true;
	errorMessage = "Invalid or Missing ICCID!";
} else if(!iccidDetail.next()) {
	isError = true;
	errorMessage = "ICCID " + iccid_param + " not found!";
	
}
if(isError) {
%>
<span class="error"><%=StringUtils.encodeForHTML(errorMessage)%></span>
<%	
} else {
	StringBuilder formattedIccid = new StringBuilder();
	for(int i = 0; ; i += 4) {
		if(i + 4 < iccid_param.length()) {
			formattedIccid.append(iccid_param.substring(i, i + 4));
		    formattedIccid.append(" ");
		} else {
			formattedIccid.append(iccid_param.substring(i));
            break;
		}
	}
	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	String date = format.format(new Date());
%>

<jsp:useBean id="gprswebStatusDescColorCodes" type="simple.results.Results" scope="request" />
<jsp:useBean id="gprswebRatePlan" type="simple.results.Results" scope="request" />

<jsp:include page="/jsp/gprsweb/gprsweb_header.jsp" flush="true" />

<div class="tabHead">SIM Card Information: <%=iccidDetail.getFormattedValue("iccid")%></div>
<form name="updateForm" method="post">
<table class="tabDataDisplayBorderNoFixedLayout">
	<tbody>
		<tr>
			<td class="label">GPRS Device ID</td>
			<td class="data"> <%=iccidDetail.getFormattedValue("gprs_device_id")%></td>
			<td class="label">ICCID</td>
			<td class="data"> <a href="https://eod.wireless.att.com/USA/history.asp?open=11&cat=Service&page=Sim%20History%20/%20Status&iccid=<%=StringUtils.encodeForURL(iccidDetail.getFormattedValue("iccid"))%>" target="_cingular"><%=StringUtils.encodeForHTML(formattedIccid.toString())%></a></td>
		</tr>
		<tr>
			<td class="label">Status</td>
				<c:set var="colorCode" value="" />
				<c:set var="statusDesc" value="" />
				<c:set var="iccidDetailDeviceStateId" value="<%=ConvertUtils.getIntSafely(iccidDetail.getValue("gprs_device_state_id"), -1) %>" />
				<c:forEach var="color_code_item" items="${gprswebStatusDescColorCodes}">
					<c:choose>
						<c:when test="${color_code_item.statusCode == iccidDetailDeviceStateId}">
							<c:set var="colorCode" value="${color_code_item.colorCode}" />
							<c:set var="statusDesc" value="${color_code_item.statusDesc}" />
						</c:when>
					</c:choose>
				</c:forEach>
			
			<td class="data" bgcolor="${colorCode }">${statusDesc }</td>
			<td class="label">IMSI</td>
			<td class="data"><%=iccidDetail.getFormattedValue("imsi")%></td>
		</tr>
		<tr>
			<td class="label">Enabled/Disabled</td>
			<td colspan="4" align="left">
				<b><%if ("D".equalsIgnoreCase(iccidDetail.getFormattedValue("status_cd"))) {%><font color="red">Disabled</font><%} else {%><font color="green">Enabled</font><%}%></b>
				<input type="submit" name="action" value="Toggle" class="cssButton" />	
			</td>
		</tr>
	</tbody>
</table>
</form>

<div class="innerTable">
<div class="tabHead">PIN/PUK Information</div>
<table class="tabDataDisplayBorderNoFixedLayout">

	<tbody>
		<tr>
			<td class="label">Pin1</td>
			<td class="data"><%=iccidDetail.getFormattedValue("pin1")%></td>
			<td class="label">Puk1</td>
			<td class="data"><%=iccidDetail.getFormattedValue("puk1")%></td>
		</tr>
		<tr>
			<td class="label">Pin2</td>
			<td class="data"><%=iccidDetail.getFormattedValue("pin2")%></td>
			<td class="label">Puk2</td>
			<td class="data"><%=iccidDetail.getFormattedValue("puk2")%></td>
		</tr>
	</tbody>
</table>
</div>

<div class="innerTable">
<div class="tabHead">Order Information</div>
<table class="tabDataDisplayBorderNoFixedLayout">

	<tbody>
		<tr>
			<td class="label">Ordered By</td>
			<td class="data"><%=iccidDetail.getFormattedValue("ordered_by")%></td>
			<td class="label">Cingular OrderID</td>
			<td class="data"><a href="https://eod.wireless.att.com/USA/view_sim_order.asp?order_id=<%=iccidDetail.getFormattedValue("provider_order_id")%>" target="_cingular"><%=iccidDetail.getFormattedValue("provider_order_id")%></a></td>
		</tr>
		<tr>
			<td class="label">Date Ordered</td>
			<td class="data"><%=iccidDetail.getFormattedValue("ordered_ts")%></td>
			<td class="label"></td>
			<td class="data"></td>
		</tr>
		<tr>
			<td class="label">Order Notes</td>
			<td class="data"><%=iccidDetail.getFormattedValue("ordered_notes")%></td>
			<td class="label"></td>
			<td class="data"></td>
		</tr>
	</tbody>
</table>
</div>

<div class="innerTable">
<div class="tabHead">Allocation Information</div>
<table class="tabDataDisplayBorderNoFixedLayout">

	<tbody>
		<tr>
			<td class="label">Allocated By</td>
			<td class="data"><%=iccidDetail.getFormattedValue("allocated_by")%></td>
			<td class="label">Allocated To</td>
			<td class="data"><%=iccidDetail.getFormattedValue("allocated_to")%></td>
		</tr>
		<tr>
			<td class="label">Date allocated</td>
			<td class="data"><%=iccidDetail.getFormattedValue("allocated_ts")%></td>
			<td class="label">Billable To</td>
			<td class="data"><%=iccidDetail.getFormattedValue("billable_to_name")%></td>
		</tr>
		<tr>
			<td class="label">Allocation Notes</td>
			<td class="data"><%=iccidDetail.getFormattedValue("allocated_notes")%></td>
			<td class="label"></td>
			<td class="data"></td>
		</tr>
		<tr>
			<td class="label">Billable Notes</td>
			<td class="data"><%=iccidDetail.getFormattedValue("billable_to_notes")%></td>
			<td class="label"></td>
			<td class="data"></td>
		</tr>
	</tbody>
</table>
</div>

<div class="innerTable">
<div class="tabHead">Activation Information</div>
<table class="tabDataDisplayBorderNoFixedLayout">

	<tbody>
		<tr>
			<td class="label">Activated By</td>
			<td class="data"><%=iccidDetail.getFormattedValue("activated_by")%></td>
			<td class="label">Cingular Activation ID</td>
			<td class="data"><a href="https://eod.wireless.att.com/USA/batch_results.asp?batch=<%=iccidDetail.getFormattedValue("provider_activation_id")%>" target="_cingular"><%=iccidDetail.getFormattedValue("provider_activation_id")%></a></td>
		</tr>
		<tr>
			<td class="label">Date Activated</td>
			<td class="data"><%=iccidDetail.getFormattedValue("activated_ts")%></td>
			<td class="label">Cingular Activation Date</td>
			<td class="data"><%=iccidDetail.getFormattedValue("provider_activation_ts")%></td>
		</tr>
		<tr>
			<td class="label">Activation Notes</td>
			<td class="data"><%=iccidDetail.getFormattedValue("activated_notes")%></td>
			<td class="label"></td>
			<td class="data"></td>
		</tr>
		<tr>
			<td class="label">MSISDN</td>
			<td class="data"><%=iccidDetail.getFormattedValue("msisdn")%></td>
			<td class="label">Phone Number</td>
			<td class="data"><%=iccidDetail.getFormattedValue("phone_number")%></td>
		</tr>
		<tr>
			<td class="label">Rate Plan</td>
			<td class="data"><a href="https://eod.wireless.att.com/USA/view_rate_plans.asp?open=11&cat=Service&page=View%20Rate%20Plans" target="_cingular"><%=iccidDetail.getFormattedValue("rate_plan_name")%></a></td>
			<td class="label"></td>
			<td class="data"></td>
		</tr>
	</tbody>
</table>
</div>

<div class="innerTable">
<div class="tabHead">Assignment Information</div>
<table class="tabDataDisplayBorderNoFixedLayout">

	<tbody>
		<tr>
			<td class="label">Assigned By</td>
			<td class="data"><%=iccidDetail.getFormattedValue("assigned_by")%></td>
			<td class="label">Assigned to device</td>
			<td class="data"><%if (iccidDetail.get("device_id") != null) { %><a href="profile.i?device_id=<%=iccidDetail.getFormattedValue("device_id")%>"><%=iccidDetail.getFormattedValue("device_serial_cd")%></a><%}%>&nbsp;</td>
		</tr>
		<tr>
			<td class="label">Date Assigned</td>
			<td class="data"><%=iccidDetail.getFormattedValue("assigned_ts")%></td>
			<td class="label"></td>
			<td class="data"></td>
		</tr>
	</tbody>
</table>
</div>

<div class="innerTable">
<div class="tabHead">Modem Information</div>
<table class="tabDataDisplayBorderNoFixedLayout">

	<tbody>
		<tr>
			<td class="label" width="30%">IMEI</td>
			<td class="data" width="70%"><%=iccidDetail.getFormattedValue("imei")%></td>
		</tr>
		<tr>
			<td class="label">Modem Type</td>
			<td class="data"><%=iccidDetail.getFormattedValue("device_type_name")%></td>
		</tr>
		<tr>
			<td class="label">Modem Firmware</td>
			<td class="data"><%=iccidDetail.getFormattedValue("device_firmware_name")%></td>
		</tr>
		<tr>
			<td class="label">Last RSSI</td>
			<td class="data"><%=iccidDetail.getFormattedValue("rssi")%>&nbsp;<%= (iccidDetail.getFormattedValue("rssi_ts") != null) ? "(" + iccidDetail.getFormattedValue("rssi_ts") + ")" : "" %></td>
		</tr>
		<% if(iccidDetail.getFormattedValue("modem_info") != null) {%>
		<tr>
		    <td class="label">Raw Modem Data</td>
		    <td><pre><%=iccidDetail.getFormattedValue("modem_info")%></pre></td>
		</tr>
        <% } %>
	</tbody>
</table>
</div>

<div class="innerTable">
<div class="tabHead">Major SIM Change History</div>
<div class="spacer10"></div>
<div class="tabData">
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="gridHeader">
			<td width="30%">&nbsp;</td>
			<td width="35%">Status at Time of Change</td>
			<td width="35%">Change Timestamp</td>
		</tr>
	</thead>

	<tbody>
	<% 
	Results simChangeHistory = (Results) inputForm.getAttribute("simChangeHistory");
	while(simChangeHistory.next()) {
	%>
		<tr>
			<td>&nbsp;<a href="gprswebIccidHistDetail.i?gprs_device_hist_id=<%=simChangeHistory.getFormattedValue("gprs_device_hist_id")%>">View</a>&nbsp;</td>
			<c:set var="colorCode" value="" />
				<c:set var="statusDesc" value="" />
				<c:set var="simChangeHistoryDeviceStateId" value="<%=ConvertUtils.getIntSafely(simChangeHistory.getFormattedValue("gprs_device_state_id"), -1) %>" />
				<c:forEach var="color_code_item" items="${gprswebStatusDescColorCodes}">
					<c:choose>
						<c:when test="${color_code_item.statusCode == simChangeHistoryDeviceStateId}">
							<c:set var="colorCode" value="${color_code_item.colorCode}" />
							<c:set var="statusDesc" value="${color_code_item.statusDesc}" />
						</c:when>
					</c:choose>
				</c:forEach>
			<td bgcolor="${colorCode }">${statusDesc }</td>
			<td><%=simChangeHistory.getFormattedValue("created_ts")%></td>
		</tr>
	<% } %>
	</tbody>
</table>
</div>
</div>

<div class="spacer10"></div>
<div class="innerTable">
<div class="tabHead">Daily Usage For This Month</div>
<div class="spacer10"></div>
<div class="tabData">
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="gridHeader">
			<td width="25%">Date</td>
			<td width="25%">Monthly Plan Limit (Kb)</td>
			<td width="25%">Total Usage (Kb)</td>
			<td width="25%">Remains (Kb)</td>
		</tr>
	</thead>

	<tbody>
	<% 
	Results dailyUsage = (Results) inputForm.getAttribute("dailyUsage");
	while(dailyUsage.next()) {
		long includeUsage;
		long totalUsage;
		if(dailyUsage.getFormattedValue("included_usage") == null || dailyUsage.getFormattedValue("included_usage").length() == 0)
		{
			includeUsage = 0;
		} else {
			includeUsage = Long.parseLong(dailyUsage.getFormattedValue("included_usage"));
		}
		if(dailyUsage.getFormattedValue("total_kb_usage") == null || dailyUsage.getFormattedValue("total_kb_usage").length() == 0)
		{
			totalUsage = 0;
		} else {
			totalUsage = Long.parseLong(dailyUsage.getFormattedValue("total_kb_usage"));
		}
		String remainsColor = (includeUsage - totalUsage < 0 ? "#FF9999" : "#99FF99");
	%>
		<tr>
		    <td><%=dailyUsage.getFormattedValue("report_date")%></td>
			<td><%=dailyUsage.getFormattedValue("included_usage")%></td>
			<td><%=dailyUsage.getFormattedValue("total_kb_usage")%></td>
			<td><span style="color: <%=remainsColor%>">includeUsage - totalUsage</span></td>
		</tr>
	<% } %>
	</tbody>
</table>
</div>
</div>
<div class="spacer5"></div>
<b><a href="gprswebUsageResults.i?action=Show%20Specific&iccid_full=<%=StringUtils.encodeForURL(iccid_param)%>">  See more usage report about this iccid.  </a></b>
<div class="spacer5"></div>

<div class="innerTable">

<%
if("1".equals(iccidDetail.getFormattedValue("gprs_device_state_id"))) {

	Results allocatedByName = (Results) inputForm.getAttribute("allocatedByName");
	Results allocatedToName = (Results) inputForm.getAttribute("allocatedToName");
	Results billableToName = (Results) inputForm.getAttribute("billableToName");
	
%>
<div class="tabHead">Allocate</div>
<form name="allocateForm" method="post" action="#">
<table class="tabDataDisplayBorderNoFixedLayout">
	<tbody>
		<tr>
		   <td class="label" width="15%">Allocated By:</td>
		   <td class="data" width="50%"><div id="allocated_by_id"><select name="allocated_by"><% while(allocatedByName.next()) { %><option value="<%=allocatedByName.getFormattedValue("allocated_by")%>"><%=allocatedByName.getFormattedValue("allocated_by")%></option><% } %></select></div></td>
		   <td><input type="button" class="cssButton" value="New" style="width:80px;" onClick="setInput('allocated_by_id');"></td>
		</tr>
		<tr>
		   <td class="label">Allocated To:</td>
		   <td class="data"><div id="allocated_to_id"><select name="allocated_to"><% while(allocatedToName.next()) { %><option value="<%=allocatedToName.getFormattedValue("allocated_to")%>"><%=allocatedToName.getFormattedValue("allocated_to")%></option><% } %></select></div></td>
		   <td><input type="button" class="cssButton" value="New" style="width:80px;" onClick="setInput('allocated_to_id');"></td>
		</tr>
		<tr>
		   <td class="label">Allocated Notes:</td>
		   <td class="data" colspan="2">
		   <textarea cols="60" rows="5" name="allocated_notes"></textarea>
		   </td>
		</tr>
		<tr>
		    <td class="label">Billable To:</td>
		    <td class="data" colspan="2">
		    <select name="billable_to_name"><% while(billableToName.next()) { %><option value="<%=billableToName.getFormattedValue("billable_to_name")%>"><%=billableToName.getFormattedValue("billable_to_name")%></option><% } %></select>
		   </td>
		</tr>
		<tr>
		   <td class="label">Billable Notes:</td>
		   <td class="data" colspan="2">
		   <textarea cols="60" rows="5" name="billable_notes"></textarea>
		   </td>
		</tr>
		<tr>
		    <td class="label"><input type="button" class="cssButton" value="Allocate" onClick="document.allocateForm.submit();"></td>
		    <td class="data" colspan="2"><input type="hidden" name="iccid" value="<%=iccidDetail.getFormattedValue("iccid")%>"></td>
		</tr>
	</tbody>
</table>
</form>
<% } else if("2".equals(iccidDetail.getFormattedValue("gprs_device_state_id"))) {
	Results activatedByName = (Results) inputForm.getAttribute("activatedByName");
%>
<form name="deallocateForm" method="post" action="#">
<table class="tabDataDisplayBorderNoFixedLayout">
	<tbody>
		<tr>
		    <td><input type="button" class="cssButton" value="Deallocate" onClick="javascript:alert('FUNCTION UNAVAILABLE AT THE PRESENT TIME!');"></td>
		    <td><input type="hidden" name="iccid" value="<%=iccidDetail.getFormattedValue("iccid")%>"></td>
		</tr>
	</tbody>
</table>
</form>

<div class="tabHead">Activate</div>
<form name="activateForm" method="post" action="#">
<table class="tabDataDisplayBorderNoFixedLayout">
	<tbody>
		<tr>
		    <td class="label" width="15%">Rate Plan:</td>
		    <td class="data" width="50%">
		    <select name="rate_plan_name" style="font-family: courier; font-size: 12px;">
				<c:forEach var="nvp_item_gprswebRatePlan" items="${gprswebRatePlan}">
						<option value="${nvp_item_gprswebRatePlan.aValue}">${nvp_item_gprswebRatePlan.aLabel}</option>	    	
				</c:forEach>	
			</select>	
		    </td>
		    <td width="35%">&nbsp;</td>
		</tr>
		<tr>
		    <td class="label">Effective Date:</td>
		    <td class="data" colspan="2"><input style="cssText"  type="text" name="effective_ts" size="11" value="<%=date%>" maxlength="10"></td>
		</tr>
		<tr>
		    <td class="label">Activated By:</td>
		    <td class="data"><div id="activated_by_id"><select name="activated_by"><% while(activatedByName.next()) { %><option value="<%=activatedByName.getFormattedValue("activated_by")%>"><%=activatedByName.getFormattedValue("activated_by")%></option><% } %></select></div></td>
		    <td><input type="button" class="cssButton" value="New" style="width:80px;" onClick="setInput('activated_by_id');"></td>
		</tr>
		<tr>
		    <td class="label">Activated Notes:</td>
		    <td class="data" colspan="2"><textarea cols="60" rows="5" name="activated_notes"></textarea></td>
		</tr>
		<tr>
		    <td><input type="button" class="cssButton" value="Activate" onClick="javascript:alert('AT&T FUNCTION NOT AVAILABLE!');"></td>
		    <td colspan="2">
		    <input type="hidden" name="iccid" value="<%=iccidDetail.getFormattedValue("iccid") + "," + iccidDetail.getFormattedValue("allocated_to") + "," + iccidDetail.getFormattedValue("billable_to_name") +"," + iccidDetail.getFormattedValue("rate_plan_name")%>" />
		    </td>
		</tr>
	</tbody>
</table>
</form>
<% } else if(Integer.parseInt(iccidDetail.getFormattedValue("gprs_device_state_id")) >= 4) {
	Results activatedByName = (Results) inputForm.getAttribute("activatedByName");
%>
<div class="tabHead">Change Rate Plan</div>
<form name="rateForm" method="post" action="#">
<table class="tabDataDisplayBorderNoFixedLayout">
	<tbody>
		<tr>
		    <td class="label" width="20%">New Rate Plan:</td>
		    <td class="data" width="50%">
		    <select name="rate_plan_name" style="font-family: courier; font-size: 12px;">
				<c:forEach var="nvp_item_gprswebRatePlan" items="${gprswebRatePlan}">
						<option value="${nvp_item_gprswebRatePlan.aValue}">${nvp_item_gprswebRatePlan.aLabel}</option>	    	
				</c:forEach>
				 <option value="KILL">KILL: Select to Deactivate SIM</option>	
			</select>
		    </td>
		    <td>&nbsp;</td>
		</tr>
		<tr>
		    <td class="label">Effective Date:</td>
		    <td class="data" colspan="2"><input style="cssText"  type="text" name="effective_ts" size="11" value="<%=date%>" maxlength="10"></td>
		</tr>
		<tr>
		    <td class="label">Activated By:</td>
		    <td class="data"><div id="activated_by_id"><select name="activated_by"><% while(activatedByName.next()) { %><option value="<%=activatedByName.getFormattedValue("activated_by")%>"><%=activatedByName.getFormattedValue("activated_by")%></option><% } %></select></div></td>
		    <td><input type="button" class="cssButton" value="New" style="width:80px;" onClick="setInput('activated_by_id');"></td>
		</tr>
		<tr>
		    <td class="label">Rate Change Notes:</td>
		    <td class="data" colspan="2"><textarea cols="60" rows="5" name="activated_notes"></textarea></td>
		</tr>
		<tr>
		    <td><input type="button" class="cssButton" value="Change Rate" onClick="javascript:alert('AT&T FUNCTION NOT AVAILABLE!');"></td>
		    <td colspan="2">
		    <input type="hidden" name="iccid" value="<%=iccidDetail.getFormattedValue("iccid")%>" />
		    </td>
		</tr>
	</tbody>
</table>
</form>
<br /><div class="tabHead"><%=iccidDetail.getFormattedValue("iccid")%></div><br />
<% } %>

</div>
<% } %>
<jsp:include page="/jsp/gprsweb/gprsweb_footer.html" flush="true" />
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
