<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.dms.sales.SalesRep"%>
<%@page import="com.usatech.dms.sales.Distributor"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="simple.bean.ConvertUtils" %>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.device.EsudsCycleCode"%>
<%@page import="com.usatech.layers.common.model.Device" %>
<%@ page import="simple.service.modem.service.DeviceModemStatusDao" %>
<%@ page import="simple.service.modem.service.DeviceModemStatusDaoImpl" %>
<%@ page import="simple.service.modem.service.dto.DeviceModemState" %>
<%@ page import="simple.service.modem.service.dto.ModemState" %>
<%@ page import="simple.db.DataLayerException" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="simple.bean.ConvertException" %>
<%@ page import="simple.service.modem.service.dto.USATDevice" %>
<%@ page import="com.usatech.dms.device.modem.AsyncDmsModemService" %>
<%@ page import="simple.modem.USATModemService" %>
<%@ page import="simple.modem.USATDeviceDao" %>
<%@ page import="simple.modem.USATDeviceDaoImpl" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<jsp:include page="/jsp/devices/deviceHeaderTabs.jsp" flush="true" />

<%
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
	long deviceId = device.getId();
	String serialNumber = device.getSerialNumber();
	String userOP = request.getParameter("userOP");
%>

<%!
	public void enqueueModemStatusRead(long deviceId) throws DataLayerException, SQLException, ConvertException, ServletException {
		USATDeviceDao modemDeviceDao = new USATDeviceDaoImpl();
		USATDevice dev = modemDeviceDao.populateByDeviceId(deviceId);
		if (dev.hasSerialNumberAndProvider()) {
			USATModemService dmsModemService = new AsyncDmsModemService();
            dmsModemService.refreshActivationStatus(new USATDevice[] {dev});
		}
	}
%>

<div class="tabDataContent">
<div class="innerTable">
<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td class="tabHead" colspan="4"><%=device.getTypeDesc()%> Device Profile</td>
		</tr>
		<tr>
			<td class="label">Device Name</td>
			<td class="data"><%=device.getDeviceName()%></td>
			<td class="label">Device ID</td>
			<td class="data"><%=deviceId%>&nbsp;</td>
		</tr>
		<tr>
			<td class="label">Serial Number</td>
			<td class="data"><a href="serialNumberAllocation.i?userOP=List%20Assigned%20Logs&txtSerialNumber=<%=serialNumber%>"><%=serialNumber%></a></td>
			<td class="label">Created</td>
			<td class="data"><%=device.getCreatedTs()%></td>
		</tr>
		<tr>
			<td class="label">Device Type</td>
			<td class="data"><%=device.getTypeDesc()%></td>
			<td class="label">Last Updated</td>
			<td class="data"><%=device.getLastUpdatedTs()%></td>
		</tr>
		<tr>
			<td class="label">Device Subtype</td>
			<td class="data"><%=device.getSubTypeDesc()%></td>
			<td class="label">Last Activity</td>
			<td class="label" style="color: <%=device.getLastActivityColor() %>;"><%=device.getLastActivityTs()%></td>
		</tr>
		<tr>
			<td class="label">Status</td>
			<td class="data">
			<%
				if (device.isEnabled()) {
			%> <span style="color: green;"><b>Enabled</b></span>
			<%
				} else {
			%> <span style="color: red;"><b>Disabled</b></span>
			<%
				}
			%>
			<%if (device.getTypeId() != 14) {%>
			<input type="button" class="cssButton" value="Toggle"
				onClick="if (confirm('Are you sure you want to toggle the activity status?')) return redirectWithParams('profile.i', 'device_id=<%=deviceId%>&last_active_flag=<%=device.isEnabled() ? "Y" : "N"%>&userOP=Toggle', null); else return false;">
			<%}%>
			<%if (device.getDeviceRecordCount() > 1) {%>
				<a href="deviceList.i?enabled=A&serial_number=<%=serialNumber%>">List All</a>
			<%}%>
				&nbsp;&nbsp;<% if(!device.isEnabled()) { %><a
				href="deviceSearch.i?ev_number=<%=device.getDeviceName()%>">Active Device</a> <% } %>
			</td>
			<td class="label">Last Rejected Until</td>
			<td class="label" style="color: <%=device.getRejectUntilColor() %>;"><%=device.getRejectUntilTs()%></td>
		</tr>
		<%
			if(device.isG4() || device.isGX()) {
			Results firmwareVersionInfo = (Results) inputForm.getAttribute("firmwareVersionInfo");
			if (firmwareVersionInfo != null) {
				String firmwareVersion = "";
				String firmwareVersionTs = "";
				String bootloaderVersion = "";
				String bootloaderVersionTs = "";
				String diagnosticVersion = "";
				String diagnosticVersionTs = "";
				String ptestVersion = "";
				String ptestVersionTs = "";
				while (firmwareVersionInfo.next()) {
					String firmware = ConvertUtils.getStringSafely(firmwareVersionInfo.get("firmware"), "");
					if (firmware.equalsIgnoreCase("Firmware Version")) {
						firmwareVersion	= firmwareVersionInfo.getFormattedValue("firmware_version");
						firmwareVersionTs = firmwareVersionInfo.getFormattedValue("firmware_version_ts");
					} else if (firmware.equalsIgnoreCase("Bootloader Rev")) {
						bootloaderVersion = firmwareVersionInfo.getFormattedValue("firmware_version");
						bootloaderVersionTs	= firmwareVersionInfo.getFormattedValue("firmware_version_ts");
					} else if (firmware.equalsIgnoreCase("Diagnostic App Rev")) {
						diagnosticVersion = firmwareVersionInfo.getFormattedValue("firmware_version");
						diagnosticVersionTs	= firmwareVersionInfo.getFormattedValue("firmware_version_ts");
					} else if (firmware.equalsIgnoreCase("PTest Rev")) {
						ptestVersion = firmwareVersionInfo.getFormattedValue("firmware_version");
						ptestVersionTs = firmwareVersionInfo.getFormattedValue("firmware_version_ts");
					}
				}
			%>
		<tr>
			<td class="label">Firmware Version</td>
			<td class="data"><%=firmwareVersion%></td>
			<td class="label">Firmware Reported Time</td>
			<td class="data"><%=firmwareVersionTs%></td>
		</tr>
		<tr>
			<td class="label">Bootloader Version</td>
			<td class="data"><%=bootloaderVersion%></td>
			<td class="label">Bootloader Reported Time</td>
			<td class="data"><%=bootloaderVersionTs%></td>
		</tr>
		<tr>
			<td class="label">Diagnostic Version</td>
			<td class="data"><%=diagnosticVersion%></td>
			<td class="label">Diagnostic Reported Time</td>
			<td class="data"><%=diagnosticVersionTs%></td>
		</tr>
		<tr>
			<td class="label">PTest Version</td>
			<td class="data"><%=ptestVersion%></td>
			<td class="label">PTest Reported Time</td>
			<td class="data"><%=ptestVersionTs%></td>
		</tr>
		<% } }
		Results iccidInfo = (Results) inputForm.getAttribute("iccidInfo");
		if(iccidInfo != null && iccidInfo.next()) {
			String iccid = iccidInfo.getFormattedValue("iccid");
			String statusCd = iccidInfo.getFormattedValue("status_cd");
		%>
		<tr>
			<td class="label">SIM Card ICCID</td>
			<td class="data"><a href="iccidDetail.i?iccid=<%=iccid%>"<%if ("D".equalsIgnoreCase(statusCd)) {out.write(" class=\"err\"");}%>><%=iccid%></a></td>
			<td class="label">SIM Assigned Date</td>
			<td class="data"><%=iccidInfo.getFormattedValue("assigned_ts")%></td>
		</tr>		
		<% }
			if(device.isESUDS()) {
				Results firmwareVersionInfo = (Results) inputForm.getAttribute("firmwareVersionInfo");
				String firmwareVersionTs = "";
				if (firmwareVersionInfo != null) {					
					if (firmwareVersionInfo.next())
						firmwareVersionTs = firmwareVersionInfo.getFormattedValue("firmware_version_ts");
				}
				
				Results firmwareData = (Results) inputForm.getAttribute("firmwareData");
				StringBuilder appender = new StringBuilder();
				if (firmwareData != null) {
					List<String> layouts = new ArrayList<String>();
					layouts.add("Display Firmware Version");
					layouts.add("eSuds Application Version");
					layouts.add("eSuds File System Version");
					layouts.add("Init File System Version");
					layouts.add("MAC Address");
					layouts.add("Multiplexor Application Version");
					layouts.add("Multiplexor Bootloader Version");
					layouts.add("Root File System Version");				
					while(firmwareData.next()) {
						if(layouts.contains(firmwareData.getFormattedValue("device_setting_name"))) {
							appender.append(firmwareData.getFormattedValue("device_setting_name"));
							appender.append(" = ");
							appender.append(firmwareData.getFormattedValue("device_setting_value"));
					        appender.append("\n");
						}
					}
				}
			%>
		<tr>
			<td class="label">Client Version</td>
			<td class="data"><textarea rows="1" readonly="readonly" style="width:99%"><%=appender%></textarea></td>
			<td class="label">Version Reported Time</td>
			<td class="data"><%=firmwareVersionTs%></td>
		</tr>
		<% } 
			if(device.isEDGE()) {
				Results firmwareVersionInfo = (Results) inputForm.getAttribute("firmwareVersionInfo");
				if (firmwareVersionInfo != null) {
					String firmwareVersion = "";
					String firmwareVersionTs = "";
					String protocolRevision = "";
					String propertyListVersion = "";
					while (firmwareVersionInfo.next()) {
						String firmware = ConvertUtils.getStringSafely(firmwareVersionInfo.get("firmware"), "");
						if (firmware.equalsIgnoreCase("Firmware Version")) {
							firmwareVersion	= firmwareVersionInfo.getFormattedValue("firmware_version");
							firmwareVersionTs = firmwareVersionInfo.getFormattedValue("firmware_version_ts");
						} else if (firmware.equalsIgnoreCase("Protocol Revision")) {
							protocolRevision = firmwareVersionInfo.getFormattedValue("firmware_version");
						} else if (firmware.equalsIgnoreCase("Property List Version")) {
							propertyListVersion	= firmwareVersionInfo.getFormattedValue("firmware_version");
						}				
					}
			%>
		<tr>
			<td class="label">Firmware Version</td>
			<td class="data"><%=firmwareVersion%></td>
			<td class="label">Version Reported Time</td>
			<td class="data"><%=firmwareVersionTs%></td>
		</tr>
		<tr>
			<td class="label">Protocol Revision</td>
			<td class="data"><%=protocolRevision%></td>
			<td class="label">Property List Version</td>
			<td class="data"><%=propertyListVersion%></td>
		</tr>
		<% } } %>
		<%if (device.isKIOSKWebService()) {
			Results appVersionInfo = (Results) inputForm.getAttribute("appVersionInfo");
            if (appVersionInfo != null) {
            	String appType = "";
                String appTypeTs = "";
                String appVersion = "";
                String appVersionTs = "";
                String propertyListVersion = "";
                String propertyListVersionTs = "";
                while(appVersionInfo.next()) {
                    String name = ConvertUtils.getStringSafely(appVersionInfo.get("name"), "");
                    if (name.equalsIgnoreCase("App Type")) {
                    	appType = appVersionInfo.getFormattedValue("value");
                    	appTypeTs = appVersionInfo.getFormattedValue("change_ts");
                    } else if(name.equalsIgnoreCase("App Version")) {
                    	appVersion = appVersionInfo.getFormattedValue("value");
                    	appVersionTs = appVersionInfo.getFormattedValue("change_ts");
                    } else if(name.equalsIgnoreCase("Property List Version")) {
                    	propertyListVersion = appVersionInfo.getFormattedValue("value");
                    	propertyListVersionTs = appVersionInfo.getFormattedValue("change_ts");
                    }              
                }
		%>
		<tr>
            <td class="label">App Type</td>
            <td class="data"><%=appType%></td>
            <td class="label">App Type Changed Time</td>
            <td class="data"><%=appTypeTs%></td>
        </tr>
        <tr>
            <td class="label">App Version</td>
            <td class="data"><%=appVersion%></td>
            <td class="label">App Version Changed Time</td>
            <td class="data"><%=appVersionTs%></td>
        </tr>
        <tr>
            <td class="label">Protocol Revision</td>
            <td class="data"><%=propertyListVersion%></td>
            <td class="label">Protocol Revision Changed Time</td>
            <td class="data"><%=propertyListVersionTs%></td>
        </tr><%} %>
		<tr>
			<td class="label">Password Generated</td>
			<td class="data"><%=device.getLastPasswordGeneratedTs()%></td>
			<td class="label">Password Updated</td>
			<td class="data"><%=device.getLastPasswordUpdatedTs()%></td>
		</tr>
		<%} %>
		<tr>
			<td class="label">IP Address</td>
			<td class="data"><%=device.getLastClientIPAddress()%></td>
			<td class="label">Last Sale</td>
			<td class="data"><%=device.getLastSaleTs()%></td>
		</tr>
		<%
		    DeviceModemStatusDao deviceModemStatusDao = new DeviceModemStatusDaoImpl();
		    String modemSerialCode = deviceModemStatusDao.findModemSerialCodeByDeviceId(deviceId);
		    DeviceModemState modemState = null;
		    if (modemSerialCode != null) {
            	modemState = deviceModemStatusDao.getDeviceModemStatus(modemSerialCode);
		    }
		%>
		<%if (modemState != null) {%>
		<tr>
			<td class="label">Last Known Modem Status</td>
			<%
				StringBuilder additionalStatus = new StringBuilder("");
				if (modemState != null) {
				    ModemState nowState = ModemState.fromStringStatus(modemState.getLastModemStatusCd());
				    ModemState targetState = ModemState.fromStringStatus(modemState.getRequestedStatusCd());
                    boolean srcStatusSameAsTarget = modemState.getLastModemStatusCd().equals(modemState.getRequestedStatusCd());
                    String srcStatus = nowState == null ? modemState.getLastModemStatusCd() : nowState.name();
                    if (targetState == ModemState.UNKNOWN) {
                        additionalStatus.append(srcStatus)
                                        .append(" (updating current state...)");
                        if (modemState.getErrorMessage() != null) {
              				additionalStatus.append(srcStatus)
								.append(" (error occurred while status change from ")
                                .append(srcStatus).append(" to ")
                                .append(targetState == null ? modemState.getRequestedStatusCd() : targetState.name())
                                .append(": ")
              					.append(modemState.getErrorCode())
              					.append(" -> ")
              					.append(modemState.getErrorMessage())
              					.append(")");
              			}
					} else if (modemState.getRequestId() != null) {
						additionalStatus.append(srcStatus)
						                .append(" (status change in progress: ")
										.append(srcStatus)
										.append(" -> ")
										.append(targetState == null ? modemState.getRequestedStatusCd() : targetState.name())
										.append(")");
					} else if (modemState.getErrorMessage() != null) {
						additionalStatus.append(srcStatus)
						                .append(" (error occurred while status change from ")
                                        .append(srcStatus).append(" to ")
                                        .append(targetState == null ? modemState.getRequestedStatusCd() : targetState.name())
                                        .append(": ")
										.append(modemState.getErrorCode())
										.append(" -> ")
										.append(modemState.getErrorMessage())
										.append(")");
					} else {
						additionalStatus.append(srcStatus);
					}
					if (modemState.getStatusUpdatedTs().getTime() + 1000L * 60L * 60L < System.currentTimeMillis()) {
						enqueueModemStatusRead(deviceId);
					}
				}
			%>
			<td class="data" style="width: 25%"><%=additionalStatus.length() > 1 ? additionalStatus.toString() : modemState.getLastModemStatusCd()%></td>
			<td class="label">Modem Status Time</td>
			<td class="data"><%=device.getModemStatusTs()%></td>
		</tr>
		<%}%>
		<tr>
			<td class="label">Call Logs</td>
			<td class="data" colspan="3">
				<form method="get" action="/deviceCallInLog.i" onsubmit="return validateDate();">
				From <input type="hidden" name="device_id" value="<%=deviceId%>" />
				<input type="text" name="call_from_date" id="call_from_date" value="<%=Helper.getDefaultStartDate()%>" size="8" maxlength="10" >
			    <img src="/images/calendar.gif" id="from_date_trigger" class="calendarIcon" title="Date selector" /> 
			    <input type="text" size="6" maxlength="8" id="call_from_time" name="call_from_time" value="<%=Helper.getDefaultStartTime()%>" />
			    To <input type="text" name="call_to_date" id="call_to_date" value="<%=Helper.getDefaultEndDate()%>" size="8" maxlength="10">
			    <img src="/images/calendar.gif" id="to_date_trigger" class="calendarIcon" title="Date selector" />
			    <input type="text" size="6" maxlength="8" id="call_to_time" name="call_to_time" value="<%=Helper.getDefaultEndTime()%>" />
			    <input type="submit" class="cssButton" value="List Calls" />
			    <input type="button" class="cssButton" onclick="showLogsByDevice();" value="List Logs" />
			    </form>
    		</td>
		</tr>		
		<%if ("Y".equalsIgnoreCase(device.getRestrictFirmwareUpdate()) || "Y".equalsIgnoreCase(device.getRestrictSettingsUpdate())) {%>
		<tr>
			<td class="warn-solid" colspan="4">
				<%if ("Y".equalsIgnoreCase(device.getRestrictFirmwareUpdate())) {%>Firmware update restricted: <%=StringHelper.isBlank(device.getFirmwareRestrictionReason()) ? "No reason specified." : device.getFirmwareRestrictionReason()%>.<%}%>
				<%if ("Y".equalsIgnoreCase(device.getRestrictSettingsUpdate())) {%>&nbsp;&nbsp;&nbsp; Settings update restricted: <%=StringHelper.isBlank(device.getSettingsRestrictionReason()) ? "No reason specified." : device.getSettingsRestrictionReason()%>.<%}%>
			</td>
		</tr>
		<%}%>
	</tbody>
</table>
</div>

<div class="innerTable">
<div class="gridHeader">USALive Information</div>
	<table class="tabDataDisplay">
	<% 
		Results usaliveLocationInfo = (Results) inputForm.getAttribute("usaliveLocationInfo");	    
	    if (usaliveLocationInfo != null && usaliveLocationInfo.next()) {
	    	if (StringHelper.isBlank(usaliveLocationInfo.getFormattedValue("customer_bank_id"))) {
	%>
		<tr><td colspan="4" class="warn-solid">No active bank account. Payable authorizations will be declined!</td></tr>
		<%} %>
		<tr>
			<td class="label">Terminal #</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("terminal_nbr")%></td>
			<td class="label">Bank Account ID</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("customer_bank_id")%>
			</td>
		</tr>
		<tr>
			<td class="label">Customer</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("customer_name")%>
			</td>
			<td class="label">Bank Account Title</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("account_title")%></td>
		</tr>
		<tr>
			<td class="label">Asset #</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("asset_nbr")%></td>
			<td class="label">Region</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("region_name")%></td>
		</tr>
		<tr>
			<td class="label">Location Name</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("location_name")%></td>
			<td class="label">Location Type</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("location_type_name")%></td>
		</tr>
		<tr>
			<td class="label">Address</td>
			<td class="data">
			<%=usaliveLocationInfo.getFormattedValue("address1")%> <%=usaliveLocationInfo.getFormattedValue("address2")%>
			<%=usaliveLocationInfo.getFormattedValue("city")%> <%=usaliveLocationInfo.getFormattedValue("state")%>
			<%=usaliveLocationInfo.getFormattedValue("zip")%> <%=usaliveLocationInfo.getFormattedValue("country_name")%>
			</td>
			<td class="label">Product Type</td>
			<td class="data"><%=usaliveLocationInfo.getFormattedValue("product_type_name")%></td>
		</tr>
<% } else { %>
	<tr><td id="noActiveTerminal" class="warn-solid">No active terminal. Payable authorizations will be declined!</td></tr>
<%} %>
	</table>
</div>

<div class="gridHeader">
<input type="button" class="cssButton" value="List All" onclick="return redirectWithParams('profile.i', 'device_id=<%=deviceId%>&userOP=list_all', null);">
</div>

<div class="innerTable">
<div class="gridHeader">
	Last Call Dates
	<input type="button" class="cssButton" value="List" onclick="return redirectWithParams('profile.i', 'device_id=<%=deviceId%>&userOP=CallDates', null);">
</div>
	<% 
	if(DevicesConstants.USER_OP_LIST_ALL.equals(userOP) || "CallDates".equals(userOP)) {
	%>
<table class="tabDataDisplay">
	<tr class="gridHeader">
		<td>Last Call Date</td>
		<td>Last Call Success Date</td>
		<td>Last Batch Success Date</td>
	</tr>
	<tr>
		<td>
		<% 
		Results lastCallDates = (Results) inputForm.getAttribute("lastCallDates");
		if (lastCallDates != null && lastCallDates.next()) {
			float round = ConvertUtils.getFloat(lastCallDates.get("last_call_date_round"));
			if(round < 0) { %>
			&nbsp;
			<%
			} else if(round >5) {
			%> <span style="color: red; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%>
			(<%=lastCallDates.getFormattedValue("last_call_date_round")%> days)</span> <%
			} else if(round > 2) {
			%> <span style="color: orange; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%>
			(<%=lastCallDates.getFormattedValue("last_call_date_round")%> days)</span> <%
			} else {
			%> <span style="color: green; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%></span>
		<%}} %>
		&nbsp;
		</td>
		<td>
		<% 
		lastCallDates = (Results) inputForm.getAttribute("lastCallSuccessDates");
		if (lastCallDates != null && lastCallDates.next()) {
			float round = ConvertUtils.getFloat(lastCallDates.get("last_call_date_round"));
			if(round < 0) { %>
			&nbsp;
			<%
			} else if(round >5) {
			%> <span style="color: red; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%>
			(<%=lastCallDates.getFormattedValue("last_call_date_round")%> days)</span> <%
			} else if(round > 2) {
			%> <span style="color: orange; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%>
			(<%=lastCallDates.getFormattedValue("last_call_date_round")%> days)</span> <%
			} else {
			%> <span style="color: green; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%></span>
		<%}} %>
		&nbsp;
		</td>
		<td>
		<% 
		lastCallDates = (Results) inputForm.getAttribute("lastCallBatchDates");
		if (lastCallDates != null && lastCallDates.next()) {
			float round = ConvertUtils.getFloat(lastCallDates.get("last_call_date_round"));
			if(round < 0) { %>
			&nbsp;
			<%
			} else if(round >5) {
			%> <span style="color: red; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%>
			(<%=lastCallDates.getFormattedValue("last_call_date_round")%> days)</span> <%
			} else if(round > 2) {
			%> <span style="color: orange; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%>
			(<%=lastCallDates.getFormattedValue("last_call_date_round")%> days)</span> <%
			} else {
			%> <span style="color: green; font-weight: bold;"><%=lastCallDates.getFormattedValue("last_call_date")%></span>
		<%}} %>
		&nbsp;
		</td>
	</tr>
</table>
	<% } %>
</div>

<div class="innerTable">
<div class="gridHeader">
ePort Program and Terminals
<input type="button" class="cssButton" value="List" onclick="return redirectWithParams('profile.i', 'device_id=<%=deviceId%>&userOP=Dealer', null);">
</div>
<% 
if(DevicesConstants.USER_OP_LIST_ALL.equals(userOP) || "Dealer".equals(userOP)) {
%>
<table class="tabDataDisplay ">
	<tr>
		<td class="label">Program</td>
		<td class="data">
		<%
		Results dealer = (Results) inputForm.getAttribute("dealer");
		if(dealer != null && dealer.next())
			out.write(dealer.getFormattedValue("DEALER_NAME"));
		else
			out.write("&nbsp;");
		%>
		</td>
		<td class="label">
		<input type="button" class="cssButton" value="Change Program" onclick="return redirectWithParams('changeDealer.i','device_serial_cd=<%=serialNumber%>')" />
		</td>
	</tr>
	<tr>
		<td class="label">Terminals</td>
		<td class="data">
			<select name="termList" id="termList" style="width: 100%;" size="10" >
			<%
			Results terms = (Results) inputForm.getAttribute("terms");
			if (terms != null) {
				while(terms.next()) {
				%><option value="<%=terms.getFormattedValue("TERMINAL_ID") %>"><%=new StringBuilder(terms.getFormattedValue("TERMINAL_NBR")).append(": ").append(terms.getFormattedValue("START_DATE")).append(" - ").append(terms.getFormattedValue("END_DATE")) %></option>   
			<%} }%>
			</select>
		</td>
		<td class="label">
		<input type="button" class="cssButton" value="Edit Terminal" onclick="seeTerminals()" />
		</td>
	</tr>		
</table>
<script type="text/javascript">
function seeTerminals(){
	var termList = document.getElementById("termList");
	if(termList.value==null||termList.selectedIndex==-1){
		alert('Please select a terminal');
		return;
	}
	window.location='editTerminals.i?terminalId='+termList.value;
}
</script>
<% } %>
</div>

<div class="innerTable">
<div class="gridHeader">
Authorization Location and Hosts
<input type="button" class="cssButton" value="List" onclick="return redirectWithParams('profile.i', 'device_id=<%=deviceId%>&userOP=List', null);">
</div>
<% if(DevicesConstants.USER_OP_LIST_ALL.equals(userOP) || "List".equals(userOP)) { 
   Results locationInfo = (Results) inputForm.getAttribute("locationInfo");
   Results customerInfo= (Results) inputForm.getAttribute("customerInfo");
   Results locationHierarchyInfo = (Results) inputForm.getAttribute("locationHierarchyInfo");
   boolean hasCustomerInfo = customerInfo.next();
   boolean hasLocationInfo = locationInfo.next();
   %>
<table class="tabDataDisplay">
		<tr>
			<td class="label">DMS Customer</td>
			<td class="data">
			<% if(hasCustomerInfo) { %><a href="editCustomer.i?customer_id=<%=customerInfo.getFormattedValue("customer_id") %>"><%=customerInfo.getFormattedValue("customer_name")%>&nbsp;<%=customerInfo.getFormattedValue("customer_city")%>&nbsp;<%=customerInfo.getFormattedValue("customer_state_cd")%></a>
			<% } %>
			</td>
			<td class="data">
			<% if(hasCustomerInfo) { 
			       if(device.isShowDisabled()) { %>
			<input disabled="disabled" type="button" class="cssButtonDisabled" value="Change Customer"><% } else { %>
			<input type="button" class="cssButton" value="Change Customer" onclick="return redirectWithParams('changeCustomer.i','device_id=<%=deviceId%>&customer_id=<%=customerInfo.getFormattedValue("customer_id")%>')"><% } } %>
			</td>
		</tr>
		<tr>
			<td class="label">DMS Location</td>
			<td class="data"> <% 
			int locationCount = 0;
			while(locationHierarchyInfo.next()) {
				locationCount ++;
			}
			locationHierarchyInfo.setRow(0);
			StringBuilder prefix = new StringBuilder();
			int j = 1;
			while(locationHierarchyInfo.next()) {
				prefix.setLength(0);
				for(int i = 0; i < locationCount - j; i++) {
					prefix.append("...");
				}
				if(j == 1) {
				%> <a href="editLocation.i?location_id=<%=locationInfo.getFormattedValue("location_id")%>"><%=prefix.append(locationHierarchyInfo.getFormattedValue("location_name")).toString()%></a><br />
			<% } else {
			%><%=prefix.append(locationHierarchyInfo.getFormattedValue("location_name")).toString()%><br />
			<% }
				j++;
			} %>
			</td>
			<td class="data">
			<% if(hasLocationInfo) { 
			       if(device.isShowDisabled()) { %>
			<input disabled="disabled" type="button" class="cssButtonDisabled" value="Change Location" disabled="disabled"><% } else { %>
			<input type="button" class="cssButton" value="Change Location" onclick="return redirectWithParams('changeLocation.i','device_id=<%=deviceId%>&location_id=<%=locationInfo.getFormattedValue("location_id")%>', null)"><% } } %>
			<div class="spacer5"></div>
			<%if(hasLocationInfo && Integer.parseInt(locationInfo.getFormattedValue("location_id")) > 1) { %>
			<input type="button" class="cssButton" value="View Hierarchy" onclick="return redirectWithParams('locationTree.i','source_id=<%=locationInfo.getFormattedValue("location_id")%>')"><% } %>
			</td>
		</tr>		
</table>
</div>
<div class="innerTable">
<% 
Results hostInfo = (Results) inputForm.getAttribute("hostInfo");
if(device.isESUDS()) {
%>
<div class="innerTable">
<div class="tabHead">Current Room Configuration</div>
<div class="spacer2"></div>
<table class="tabDataDisplay">
	<thead>
		<tr class="gridHeader">
			<td>Host ID</td>
			<td>Port</td>
			<td>Type</td>
			<td>Position</td>
			<td>Label</td>
			<td>Status</td>
			<td>Last Start Time</td>
		</tr>
	</thead>

	<tbody>
		<%
	String hostStatusCd;
	String hostStatus;
	String hostCompMin;
	while(hostInfo.next()) {
		hostStatusCd =  hostInfo.getFormattedValue("host_status_cd");
		hostCompMin = hostInfo.getFormattedValue("host_est_complete_minut");
		hostStatus = EsudsCycleCode.forValue(hostStatusCd).getLabel().concat((hostStatusCd.equals("2") || hostStatusCd.equals("7")) ? " (".concat(hostCompMin).concat(" Min)") : "");
	%>
		<tr>
			<td><a
				href="hostProfile.i?host_id=<%=hostInfo.getFormattedValue("host_id")%>"><%=hostInfo.getFormattedValue("host_id")%></a></td>
			<td><%=hostInfo.getFormattedValue("host_port_num")%></td>
			<td><%=hostInfo.getFormattedValue("host_equipment_mfgr")%>&nbsp;<%=hostInfo.getFormattedValue("host_type_desc")%></td>
			<td><%=hostInfo.getFormattedValue("host_position_num").equals("0") ? "Bottom" : "Top"%></td>
			<td><%=hostInfo.getFormattedValue("host_label_cd")%></td>
			<td><%=hostStatus%></td>
			<td><%=hostInfo.getFormattedValue("host_last_start_ts")%></td>
		</tr>
		<% } %>
	</tbody>
</table>
</div>
<% } else {%>
<div class="innerTable">
<div class="tabHead">Component Information</div>
<div class="spacer2"></div>
<table class="tabDataDisplay">
	<thead>
		<tr class="gridHeader">
			<td>Host ID</td>
			<td>Type</td>
			<td>Manufacturer</td>
			<td>Model</td>
			<td>Serial Number</td>
		</tr>
	</thead>

	<tbody>
		<%
	while(hostInfo.next()) {
	%>
		<tr>
			<td><a
				href="hostProfile.i?host_id=<%=hostInfo.getFormattedValue("host_id")%>"><%=hostInfo.getFormattedValue("host_id")%></a></td>
			<td><%=hostInfo.getFormattedValue("host_type_desc")%></td>
			<td><%=hostInfo.getFormattedValue("host_equipment_mfgr")%></td>
			<td><%=hostInfo.getFormattedValue("host_equipment_model")%></td>
			<td><%=hostInfo.getFormattedValue("host_serial_cd")%></td>
		</tr>
<% } %>
	</tbody>
</table>
</div>
<% 
}
} 
%>
</div>


<div class="innerTable">
<div class="gridHeader">
Sale Reps and Affiliations
<input type="button" class="cssButton" value="List" onclick="return redirectWithParams('profile.i', 'device_id=<%=deviceId%>&userOP=Sales', null);">
</div>
<%  if(DevicesConstants.USER_OP_LIST_ALL.equals(userOP) || "Sales".equals(userOP)) { %>
<table class="tabDataDisplay ">
	<tr>
		<td class="label">Sales rep currently responsible for this device</td>
		<td class="data accent">
		<% SalesRep responsibleSalesRep = SalesRep.findSalesRepResponsibleForDevice(serialNumber);
			 if (responsibleSalesRep == null) { %>NONE<% } else { %>
			<a href="/salesRep.i?salesRepId=<%=responsibleSalesRep.getSalesRepId()%>"><%=responsibleSalesRep.getName() %></a>
		<% } %>
		</td>
		<td class="data">
			<input type="button" class="cssButton" value="Change Sales Rep" onclick="document.location='deviceSalesRep.i?deviceId=<%=deviceId%>&amp;deviceSerialCd=<%=serialNumber%>';">
		</td>
	</tr>
	<tr>
		<td class="label">Distributor Name</td>
		<td class="data accent">
			<% Distributor distributor = Distributor.findDistributorForDevice(serialNumber);
			 if (distributor == null) { %>NONE<% } else { %>
			<a href="/distributor.i?distributorId=<%=distributor.getDistributorId()%>"><%=distributor.getDistributorName() %></a>
			<% } %>
		</td>
		<td class="data">
			<input type="button" id="changeDistributor" class="cssButton" value="Change Distributor" title="Available with active terminal exists" onclick="document.location='deviceDistributor.i?deviceId=<%=deviceId%>&amp;deviceSerialCd=<%=serialNumber%>';">
		</td>
	</tr>
	<tr>
		<td class="label">Sales commission events</td>
		<td class="data" colspan="2">
			<table class="tabDataDisplay">
				<tr class="gridHeader">
					<td>Event Timestamp</td>
					<td>Sales Rep Name</td>
					<td>Event Type</td>
					<td>Event Desc</td>
					<td>Commission Effect</td>
					<td></td>
				</tr>
<% 	Results salesEvents = (Results) inputForm.getAttribute("salesEvents");
		while(salesEvents.next()) {
				int commissionValue = salesEvents.getValue("commissionValue", Integer.class); %>
				<tr>
					<td><%=salesEvents.getFormattedValue("commissionEventId")%> <%=salesEvents.getFormattedValue("eventTimestamp")%></td>
					<td><a href="/salesRep.i?salesRepId=<%=salesEvents.getFormattedValue("salesRepId")%>"><%=salesEvents.getFormattedValue("salesRepName")%></a></td>
					<td><%=salesEvents.getFormattedValue("eventTypeName")%></td>
					<td><%=salesEvents.getFormattedValue("eventTypeDesc")%></td>
					<td style="color:<%=commissionValue == 0 ? "black" : commissionValue > 0 ? "green;" : "red;"%>"><%=salesEvents.getFormattedValue("commisionDesc")%></td>
					<td><input type="button" id="deleteCommissionEvent" name="deleteCommissionEvent" value="Delete" onclick="if(confirm('Delete commission event?')) {window.location = 'deleteCommissionEvent.i?commissionEventId=<%=salesEvents.getFormattedValue("commissionEventId")%>&amp;deviceId=<%=deviceId%>'}"></td>
				</tr>
<%	} %>
			</table>
		</td>
	</tr>
</table>
<% } %>
</div>
</div>

<script type="text/javascript" defer="defer">
    Calendar.setup({
        inputField     :    "call_from_date",      // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "call_to_date",         // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });

    var fromDate = document.getElementById("call_from_date");
    var fromTime = document.getElementById("call_from_time");
    var toDate = document.getElementById("call_to_date");
    var toTime = document.getElementById("call_to_time");
    
    function validateDate() {
    	if(!isDate(fromDate.value)) {
        	fromDate.focus();
        	return false;
    	}
    	if(!isTime(fromTime.value)) {
        	fromTime.focus();
        	return false;
    	}
    	if(!isDate(toDate.value)) {
        	toDate.focus();
        	return false;
    	}
    	if(!isTime(toTime.value)) {
        	toTime.focus();
        	return false;
    	}
    	return true;
    }
    
    function showLogsByDevice() {
    	if (!validateDate()) {
    		return;
    	}
    	var logsHistoriUrl = "/logsHistory.i?maxCount=100&action=Submit";
    	logsHistoriUrl += "&StartDate=" + fromDate.value + "&StartTime=" + fromTime.value;
    	logsHistoriUrl += "&EndDate=" + toDate.value + "&EndTime=" + toTime.value;
    	logsHistoriUrl += "&deviceIdentifier=<%=device.getDeviceName()%>,<%=device.getSerialNumber()%>"
    	location.href = logsHistoriUrl;
    	
    } 
   
   if ($('noActiveTerminal')) {
   		$('changeDistributor').disabled=true;
   }
</script>

<jsp:include page="/jsp/devices/deviceFooter.jsp" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
