<%@page import="com.usatech.layers.common.constants.CardType"%>
<%@page import="com.usatech.layers.common.util.DeviceUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Map"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.dms.device.MessageMap"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.constants.SessionCloseReason"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="tabDataContent">
<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long deviceId = ConvertUtils.getLongSafely(inputForm.getString("device_id", false), -1);
String sessionCd = ConvertUtils.getStringSafely(inputForm.getString("session_cd", false), "");
boolean sessionCdPresent = !StringHelper.isBlank(sessionCd);
if(deviceId < 1 && !sessionCdPresent) { %>
<span class="error">Required parameters not found: device_id or session_cd</span>
<%
} else {
String call_from_date = inputForm.getString("call_from_date", false);
if (StringHelper.isBlank(call_from_date))
    call_from_date = Helper.getDefaultStartDate();
String call_from_time = inputForm.getString("call_from_time", false);
if (StringHelper.isBlank(call_from_time))
    call_from_time = Helper.getDefaultStartTime();
String call_to_date = inputForm.getString("call_to_date", false);
if (StringHelper.isBlank(call_to_date))
    call_to_date = Helper.getDefaultEndDate();
String call_to_time = inputForm.getString("call_to_time", false);
if (StringHelper.isBlank(call_to_time))
    call_to_time = Helper.getDefaultEndTime();
String serialNumber = "";
Results deviceCallInLog = null;
if (sessionCdPresent) {
	deviceCallInLog = DataLayerMgr.executeQuery("GET_DEVICE_CALL_IN_INFO", new Object[] {sessionCd});
	if (deviceCallInLog.next()) {
		serialNumber = deviceCallInLog.getFormattedValue("serial_number");
		Timestamp callInStartTs = ConvertUtils.convert(Timestamp.class, deviceCallInLog.getValue("db_call_in_start_ts"));
		deviceId = DeviceActions.getDeviceIdBySerial(serialNumber, callInStartTs);
		String[] callInStartTsArray = ConvertUtils.getStringSafely(deviceCallInLog.getValue("call_in_start_ts"), "").split(" ");
		if (callInStartTsArray.length > 1) {
			call_from_date = callInStartTsArray[0];
			call_from_time = callInStartTsArray[1];
			call_to_date = callInStartTsArray[0];
			call_to_time = callInStartTsArray[1];
		}
	} else
		sessionCdPresent = false;
} else if (deviceId > 0) {
	Device device = DeviceUtils.generateDevice(deviceId);
	serialNumber = device.getSerialNumber();
	Map<String, Object> params = new HashMap<String, Object>();
	params.put("call_from_date", call_from_date);
	params.put("call_from_time", call_from_time);
	params.put("call_to_date", call_to_date);
	params.put("call_to_time", call_to_time);
	params.put("device_name", device.getDeviceName());
	deviceCallInLog = DataLayerMgr.executeQuery("GET_DEVICE_CALL_IN_LOG", params);
}

StringBuilder js = new StringBuilder();
%>
<div class="innerTable">
<table class="tabDataDisplayBorderNoFixedLayout">
	<tr class="tabHead">
		<td colspan="41" align="left" class="nowrap">
		<form method="get" action="/deviceCallInLog.i" onsubmit="return validateDate();">
			<a href="/profile.i?device_id=<%=deviceId%>"><%=serialNumber%></a>&nbsp;&nbsp;Device Call Log
			From <input type="hidden" name="device_id" value="<%=deviceId%>" />
			<input type="text" name="call_from_date" id="call_from_date" value="<%=StringUtils.encodeForHTMLAttribute(call_from_date)%>" size="8" maxlength="10" >
		    <img src="/images/calendar.gif" id="from_date_trigger" class="calendarIcon" title="Date selector" /> 
		    <input type="text" size="6" maxlength="8" id="call_from_time" name="call_from_time" value="<%=StringUtils.encodeForHTMLAttribute(call_from_time)%>" />
		    To <input type="text" name="call_to_date" id="call_to_date" value="<%=StringUtils.prepareCDATA(call_to_date)%>" size="8" maxlength="10">
		    <img src="/images/calendar.gif" id="to_date_trigger" class="calendarIcon" title="Date selector" />
		    <input type="text" size="6" maxlength="8" id="call_to_time" name="call_to_time" value="<%=StringUtils.prepareCDATA(call_to_time)%>" />
		    <input type="submit" class="cssButton" value="List Calls" />
		</form>
		</td>
	</tr>
	
	<tr class="gridHeader">
		<th colspan="7">&nbsp;</th>
		<th colspan="3">Credit</th>
		<th colspan="3">Cash</th>
		<th colspan="3">Special</th>
		<th colspan="25">&nbsp;</th>
	</tr>

    <tr class="gridHeader">
    <th>Call ID</th>
    <th>Call Status</th>
    <th>Start Time</th>
    <th>End Time</th>
    <th>Auth&nbsp;Request Type/Amt/App</th>
    <th>Inbound DEX&nbsp;Size</th>
    <th>DEX State</th>
    <th>Trans</th>
    <th>Vends</th>
    <th>Amt</th>
    <th>Trans</th>
    <th>Vends</th>
    <th>Amt</th>
    <th>Trans</th>
    <th>Vends</th>
    <th>Amt</th>
    <th>Duration</th>
    <th>Inbound Msgs/Bytes</th>
    <th>Outbound Msgs/Bytes</th>    
    <th>Session State</th>
    <th>Reason</th>
    <th>Update Requests</th>
    <th>Inbound Files</th>
    <th>Outbound Files</th>
    <th>Init Msgs</th>
    <th>Component Msgs</th>
    <th>Events</th>
    <th>Settlements</th>
    <th>Inbound Config</th>
    <th>Outbound Config</th>
    <th>Local Auths</th>
    <th>Invalid Msgs</th>
    <th>Customer</th>
    <th>Location</th>
    <th>Net Layer</th>
    <th>IP&nbsp;Address</th>
    <th>Port</th>
    <th>Comm Method</th>
    <th>Firmware</th>
    <th>Comments</th>
    <th>Session Code</th>
    </tr>
    
	<%
	if (deviceCallInLog != null) {
		int sessionStateId = 1;
		String commMethodName = "";
		String firmwareVersion = "";
		String alert;
		int i = 0;
	while(sessionCdPresent || deviceCallInLog.next()) {
		i++;
		alert = "";
		if (sessionStateId != 1 && deviceCallInLog.getValue("session_state_id", int.class) != 1 && i > 1) {
			if (!deviceCallInLog.getFormattedValue("comm_method_name").equals(commMethodName)) {
				alert += "Comm Method changed";
				js.append("document.getElementById('comm_method_name_").append(i - 1).append("').style.backgroundColor = 'orange';\n");
			}
			if (!deviceCallInLog.getFormattedValue("firmware_version").equals(firmwareVersion)) {
				if (alert.length() > 0)
					alert += ", ";
				alert += "Firmware changed";
				js.append("document.getElementById('firmware_version_").append(i - 1).append("').style.backgroundColor = 'orange';\n");
			}
			if (alert.length() > 0) {
				js.append("document.getElementById('call_id_").append(i - 1).append("').style.backgroundColor = 'orange';\n");
				js.append("document.getElementById('call_id_").append(i - 1).append("').title = '").append(alert).append("';\n");
			}
		}
		SessionCloseReason callStatus = SessionCloseReason.getByValue(deviceCallInLog.getValue("call_in_status", char.class));
		String startDate = ConvertUtils.formatObject(deviceCallInLog.getValue("call_in_start_ts"), "DATE:MM/dd/yyyy");
		String startTime = ConvertUtils.formatObject(deviceCallInLog.getValue("call_in_start_ts"), "DATE:HH:mm:ss");
		String endDate = ConvertUtils.formatObject(deviceCallInLog.getValue("call_in_finish_ts"), "DATE:MM/dd/yyyy");
		String endTime = ConvertUtils.formatObject(deviceCallInLog.getValue("call_in_finish_ts"), "DATE:HH:mm:ss");
	%>
	<tr bgcolor="<%=MessageMap.getDeviceCallTypeColor().get(deviceCallInLog.getFormattedValue("call_in_type"))%>" class="nowrap">
		<td id="call_id_<%=i%>"><a href="/deviceMessages.i?session_cd=<%=deviceCallInLog.getFormattedValue("global_session_cd")%>"><%=deviceCallInLog.getFormattedValue("session_id")%></a></td>
		<td><font color="<%=callStatus.getColor()%>"><%=callStatus.getName()%></font></td>
        <td><%=deviceCallInLog.getFormattedValue("call_in_start_ts")%></td>
        <td><%=deviceCallInLog.getFormattedValue("call_in_finish_ts")%></td>
        <td><%if (StringHelper.isBlank(deviceCallInLog.getFormattedValue("auth_card_type"))) {out.write("&nbsp;");} else {%><%=CardType.getCardTypeNameByValue(deviceCallInLog.getValue("auth_card_type", char.class))%>/<%=deviceCallInLog.getFormattedValue("auth_amount")%>/<%="Y".equalsIgnoreCase(deviceCallInLog.getFormattedValue("auth_approved_flag")) ? "Yes" : "No"%><%}%></td>
        <td><%if ("Y".equalsIgnoreCase(deviceCallInLog.getFormattedValue("dex_received_flag"))){%><%=deviceCallInLog.getFormattedValue("dex_file_size")%><%}%></td>
        <td><%=deviceCallInLog.getFormattedValue("dex_status")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("credit_trans_count")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("credit_vend_count")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("credit_trans_total")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("cash_trans_count")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("cash_vend_count")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("cash_trans_total")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("passcard_trans_count")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("passcard_vend_count")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("passcard_trans_total")%>&nbsp;</td>
        <td><%=deviceCallInLog.getFormattedValue("call_in_finish_ts-call_in_start_ts")%></td>
		<td><%=deviceCallInLog.getFormattedValue("inbound_message_count")%>/<%=deviceCallInLog.getFormattedValue("inbound_byte_count")%></td>
        <td><%=deviceCallInLog.getFormattedValue("outbound_message_count")%>/<%=deviceCallInLog.getFormattedValue("outbound_byte_count")%></td>
        <td><%=deviceCallInLog.getFormattedValue("session_state_desc")%></td>
        <td><%=MessageMap.getDeviceCallType().get(deviceCallInLog.getFormattedValue("call_in_type"))%></td>
        <td><%=deviceCallInLog.getValue("usr_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("usr_count") : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getValue("inbound_file_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("inbound_file_count") : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getValue("outbound_file_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("outbound_file_count") : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getValue("init_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("init_count") : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getValue("component_info_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("component_info_count") : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getValue("event_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("event_count") : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getValue("settlement_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("settlement_count") : "&nbsp;"%></td>
        <td><%="Y".equalsIgnoreCase(deviceCallInLog.getFormattedValue("device_sent_config_flag")) ? "Yes" : "&nbsp;"%></td>
        <td><%="Y".equalsIgnoreCase(deviceCallInLog.getFormattedValue("server_sent_config_flag")) ? "Yes" : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getValue("local_auth_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("local_auth_count") : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getValue("invalid_count", int.class) > 0 ? deviceCallInLog.getFormattedValue("invalid_count") : "&nbsp;"%></td>
        <td><%=deviceCallInLog.getFormattedValue("customer_name")%></td>
        <td><%=deviceCallInLog.getFormattedValue("location_name")%></td>
        <td><%=deviceCallInLog.getFormattedValue("network_layer")%></td>
        <td><%=deviceCallInLog.getFormattedValue("client_ip_address")%></td>
        <td><%=deviceCallInLog.getFormattedValue("client_port")%></td>
        <td id="comm_method_name_<%=i%>"><%=deviceCallInLog.getFormattedValue("comm_method_name")%>&nbsp;</td>
        <td id="firmware_version_<%=i%>"><%=deviceCallInLog.getFormattedValue("firmware_version")%>&nbsp;</td>
        <td><%=StringUtils.prepareHTML(deviceCallInLog.getFormattedValue("comments"))%></td>
        <td><a href="/logsHistory.i?StartDate=<%=startDate %>&StartTime=<%=startTime %>&EndDate=<%=endDate %>&EndTime=<%=endTime %>&globalSession=<%=deviceCallInLog.getFormattedValue("global_session_cd")%>&maxCount=100&action=Submit"><%=deviceCallInLog.getFormattedValue("global_session_cd")%></a></td>
	</tr>
	<%
		if (sessionCdPresent)
			break;
	
		sessionStateId = deviceCallInLog.getValue("session_state_id", int.class);
		commMethodName = deviceCallInLog.getFormattedValue("comm_method_name");
    	firmwareVersion = deviceCallInLog.getFormattedValue("firmware_version");
	} } 
	%>
</table>
</div>
<% if (js.length() > 0) {%>
<script type="text/javascript" defer="defer">
<%=js%>
</script>
<%}%>
<%}%>
</div>

<div class="spacer10"></div>

<script type="text/javascript" defer="defer">
    Calendar.setup({
        inputField     :    "call_from_date",      // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "call_to_date",         // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });

    var fromDate = document.getElementById("call_from_date");
    var fromTime = document.getElementById("call_from_time");
    var toDate = document.getElementById("call_to_date");
    var toTime = document.getElementById("call_to_time");
    
    function validateDate() {
    	if(!isDate(fromDate.value)) {
        	fromDate.focus();
        	return false;
    	}
    	if(!isTime(fromTime.value)) {
        	fromTime.focus();
        	return false;
    	}
    	if(!isDate(toDate.value)) {
        	toDate.focus();
        	return false;
    	}
    	if(!isTime(toTime.value)) {
        	toTime.focus();
        	return false;
    	}
    	return true;
    }
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
