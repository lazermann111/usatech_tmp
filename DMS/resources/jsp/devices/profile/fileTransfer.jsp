<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:include page="/jsp/devices/deviceHeaderTabs.jsp" flush="true" />
<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMsg");
if((missingParam != null && missingParam.booleanValue() == true) || errorMessage != null && errorMessage.trim().length() > 0) {
				%>
				<div class="tableContainer">
				<span class="error"><%=errorMessage %></span>
				</div>
				<%	
		} else {
			simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
			Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
			
		    
		%>
		
		<div class="tabDataContent">
		<%if(DeviceType.GX.getValue() == device.getTypeId() || DeviceType.EDGE.getValue() == device.getTypeId() || DeviceType.KIOSK.getValue() == device.getTypeId() 
			|| DeviceType.ESUDS.getValue() == device.getTypeId()){%>
			<jsp:include page="tableFileTransfer.jsp" flush="true" /> 	
		<%} %>
		<div class="spacer10"></div>
		<jsp:include page="tableFileTransferHistory.jsp" flush="true" />
		</div>
		
	
<% } %>
<jsp:include page="/jsp/devices/deviceFooter.jsp" flush="true" />
<jsp:include page="/jsp/include/footer.jsp" flush="true" />