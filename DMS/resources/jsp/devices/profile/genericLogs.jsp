<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="ev_number" class="java.lang.Object" scope="request" />
<jsp:useBean id="call_count" class="java.lang.Object" scope="request" />

<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	
	<c:when test="${not empty(errorMessage)}">
		<div class="tableContainer">				    
			<span class="error">${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>
	
			
			<div align="center">
			<div class="tabDataContent">
					<div class="tableDataHead" align="center">
						<span class="txtWhiteBold">
						Generic Logs - ${ev_number }</span>
					    </div>								
					<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
						
					<thead>
							<tr class="sortHeader">
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "1", (String)pageContext.getAttribute("sortIndex"))%>">Timestamp</a>
									<%=PaginationUtil.getSortingIconHtml("1", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "2", (String)pageContext.getAttribute("sortIndex"))%>">Log Data</a>
									<%=PaginationUtil.getSortingIconHtml("2", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "3", (String)pageContext.getAttribute("sortIndex"))%>">Additional Info</a>
									<%=PaginationUtil.getSortingIconHtml("3", (String)pageContext.getAttribute("sortIndex"))%>
								</td>							
							</tr>
						</thead>
						
						<c:choose>
							<c:when test="${call_count ne '' and call_count ne '0'}" >
							<jsp:useBean id="reportResults" type="simple.results.Results" scope="request" />
								<tbody>
								<c:forEach var="report_item" items="${reportResults}">
									<tr>
									    <td nowrap align="left">${report_item.timestamp }&nbsp;</td>
									    <td nowrap align="left">${report_item.object_name }&nbsp;</td>
									    <td nowrap align="left">${report_item.additional_information }&nbsp;</td>
									</tr>
								</c:forEach>
			
								</tbody>
								<c:set var="storedNames" value="device_type,ev_number,call_count" />	
							
							    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
							        <jsp:param name="_param_request_url" value="genericLogs.i" />
							    	<jsp:param name="_param_stored_names" value="${storedNames}" />
							    </jsp:include>
						</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="8" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>
		</table>
		</div>
		</div>
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />