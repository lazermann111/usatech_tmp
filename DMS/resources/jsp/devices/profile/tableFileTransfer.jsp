<%@page import="simple.db.DataLayerMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMessage");
if((missingParam != null && missingParam.booleanValue() == true) || errorMessage != null && errorMessage.trim().length() > 0) {
	if(missingParam != null && missingParam.booleanValue() == true){
		%>
		<div class="tableContainer">
		<span class="error">Required parameter not found: location_id, location_name</span>
		</div>
		<%	
	}else{
		%>
		<div class="tableContainer">
		<span class="error">Error Message: <%= errorMessage%></span>
		</div>
		<%	
	}
} else {
	simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String action = inputForm.getString(DMSConstants.PARAM_ACTION_TYPE, false);
    String searchFileType = inputForm.getString("file_transfer_type", false);
    List<NameValuePair> nvp_list_fileTransferTypes = (List<NameValuePair>)dms_values_list_fileTransferTypesList.getList();
    boolean bulk = inputForm.getBoolean("bulk", false, false);
    long deviceId;
    int deviceTypeId;
    String deviceName;
    String serialNumber;
    if (bulk) {
    	deviceId = -1;
    	deviceTypeId = inputForm.getInt("device_type_id", true, -1);
    	deviceName = "";
    	serialNumber = "";
    } else {
    	Device device = (Device) request.getAttribute(DevicesConstants.STORED_DEVICE);
    	deviceId = device.getId();
    	deviceTypeId = device.getTypeId();
    	deviceName = device.getDeviceName();
    	serialNumber = device.getSerialNumber();
    }
%>

<div class="innerTable">
<div class="gridHeader">File Transfers</div>
<div class="x-scrollable-auto">
<table class="tabDataDisplay">
<tr>
<%if (!bulk){%>
<td align="center">	
	<form method="post" action="fileList.i">
		<table class="noBorder"><tr><td align="left">
		  &nbsp;<input type="text" name="file_transfer_name" size="20" maxlength="255" value="file name" onfocus="this.value=''" onkeypress="return ignoreEnterKey(event)" />
		  <div class="spacer5"></div>
		  <input type="submit" name="myaction" class="cssButton" value="Upload to Device" />
		  <%if (!bulk) {%>
		  <input type="hidden" name="ev_number" value="<%=deviceName %>" />
		  <input type="hidden" name="serial_number" value="<%=serialNumber %>" />
		  <input type="hidden" name="device_type_id" value="<%=deviceTypeId %>" />
		  <%}%>
		</td></tr></table>
	</form>
</td>
<%}%>
<td align="center">
	<%if (!bulk){%>
	<form method="get" action="fileDetailsFunc.i">
	<%}%>
	<table class="noBorder"><tr><td align="left">
	  &nbsp;<input type="text" name="file_transfer_id" value="file id" size="8" onfocus="this.value=''" onkeypress="return ignoreEnterKey(event)" />
	  <div class="spacer5"></div>
	  <input type="submit" name="myaction" class="cssButton" value="Upload to Device" />
	  <%if (!bulk) {%>
	  <input type="hidden" name="ev_number" value="<%=deviceName %>" />
	  <input type="hidden" name="serial_number" value="<%=serialNumber %>" />
	  <input type="hidden" name="device_type_id" value="<%=deviceTypeId %>" />
	  <%}%>
	</td></tr></table>
	<%if (!bulk){%>
	</form>
	<%}%>
</td>
<%if (!bulk) {%>
<td align="center">
	<form method="get" action="newFile.i">
		<table class="noBorder"><tr><td align="left">	
		  <input type="submit" class="cssButton" value="Upload New File to Device" />
		  <input type="hidden" name="ev_number" value="<%=deviceName %>" />
		  <input type="hidden" name="serial_number" value="<%=serialNumber %>" />
		  <input type="hidden" name="device_type_id" value="<%=deviceTypeId %>" />
		</td></tr></table>
	</form>
</td>
<%}%>
<td align="center">
	<%if (!bulk){%>
	<form method="get" action="fileTransfer.i">
	<%}%>
	<table class="noBorder"><tr><td align="left">
	  <% if(DeviceType.EDGE.getValue() == deviceTypeId){%>
	  	<div class="spacer3"></div>
	  	&nbsp;<select name="download_file_type" id="download_file_type">
			<c:forEach var="nvp_item_file_transfer_type" items="<%=nvp_list_fileTransferTypes%>">
				<c:choose>
					<c:when test="${nvp_item_file_transfer_type.value == searchFileType}">
						<option value="${nvp_item_file_transfer_type.value}" selected="selected">${nvp_item_file_transfer_type.name}</option>
					</c:when>
					<c:otherwise>
						<option value="${nvp_item_file_transfer_type.value}">${nvp_item_file_transfer_type.name}</option>
					</c:otherwise>
				</c:choose>	    	
			   </c:forEach>
		</select>
	  <% }%>
	  <div class="spacer5"></div>
	  &nbsp;<input type="text" name="download_file_name" id="download_file_name" value="name or path on client" size="30" maxlength="255" onfocus="this.value=''" onkeypress="return ignoreEnterKey(event)" />
	  <div class="spacer5"></div>
	  <input type="submit" class="cssButton" name="myaction" value="Download File" />
	  <input type="hidden" name="device_id" value="<%=deviceId%>" />
	</td></tr></table>
	<%if (!bulk){%>
	</form>
	<%}%>
</td>
<% if(deviceTypeId != DeviceType.GX.getValue()){%>
<td align="center">
	<%if (!bulk){%>
	<form method="get" action="fileTransfer.i">
	<%}%>
		<table class="noBorder"><tr><td align="left">
		  <input type="submit" class="cssButton" name="myaction" value="Download Config" />
		  <input type="hidden" name="device_id" value="<%=deviceId%>" />
		</td></tr></table>
	<%if (!bulk){%>
	</form>
	<%}%>
</td>
<% }%>
<%
Results results = DataLayerMgr.executeQuery("GET_FIRMWARE_UPGRADES_BY_DEVICE_TYPE", new Object[]{deviceTypeId}, true);
if (results.next()) {
%>
<td align="center">
	<%if (!bulk){%>
	<form method="post" action="deviceConfig.i">
	  	<input type="hidden" name="device_id" value="<%=deviceId %>" />
	<%}%>
		<table class="noBorder"><tr><td align="left">
		   	&nbsp;<select id="firmware_upgrade_id" name="firmware_upgrade_id">
		   		<option value="">Select Upgrade</option>
		    	<%do {%>
				<option value="<%=results.getFormattedValue("firmwareUpgradeId")%>"><%=results.getFormattedValue("firmwareUpgradeName")%></option>
				<%} while (results.next());%>
			</select>
			<div class="spacer5"></div>
			<input type="submit" class="cssButton" name="myaction" value="Upgrade Device" onclick="if (document.getElementById('firmware_upgrade_id').value == '') { alert('Please select Firmware Upgrade'); return false; } else return true;" />
		</td></tr></table>
	<%if (!bulk){%>
	</form>
	<%}%>
</td>
<%}%>
</tr>
</table>
</div>
</div>

<% } %>