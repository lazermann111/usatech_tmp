<%@page import="simple.lang.InvalidIntValueException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.constants.ComponentType"%>

<%@page import="com.usatech.dms.device.MessageMap"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<%
	InputForm inputForm = (InputForm) request
			.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	Results hostProfileInfo = (Results) inputForm
			.getAttribute("hostProfileInfo");
	Results deviceProfileInfo = (Results) inputForm
			.getAttribute("deviceProfileInfo");
	Results hostSettingInfo = (Results) inputForm
			.getAttribute("hostSettingInfo");
	Results hostEstTimeInfo = (Results) inputForm
			.getAttribute("hostEstTimeInfo");
	Results hostDiagInfo = (Results) inputForm
			.getAttribute("hostDiagInfo");
	boolean hasHostProfileInfo = true;
	boolean hasDeviceProfileInfo = true;
	String host_id_param = request.getParameter("host_id");	
%>
<div class="tableContainer">
<% if (host_id_param == null || host_id_param.length() == 0) { %>
<span class="error">Required Parameter Not Found: host_id</span>
<%
   } else if(!(hasDeviceProfileInfo = deviceProfileInfo.next())) {
%>
<span class="error">Device not found!</span>
<% } else { 
	hasHostProfileInfo = hostProfileInfo.next();
	int hostTypeId = hasHostProfileInfo ? ConvertUtils.getInt(hostProfileInfo.get("host_type_id"), -1) : -1;
%>
<div class="gridHeader"><b>Host Profile</b></div>
<table class="tabDataDisplayBorder">
	<tbody>
		<tr>
			<td class="label">Host ID</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("host_id") : ""%></td>
			<td class="label">Created</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("created_ts") : ""%></td>
		</tr>
		<tr>
			<td class="label">Last Start Time</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("host_last_start_ts") : ""%></td>
			<td class="label">Last Updated</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("last_updated_ts") : ""%></td>
		</tr>
		<tr>
			<td class="label">
			<%
			ComponentType componentType;
			try {
				componentType = ComponentType.getByValue(hostTypeId);
			} catch(InvalidIntValueException e) {
				componentType = null;
			}
			if(componentType != null && componentType.toString().endsWith("_MODEM")) {
			    out.write("Product Serial Number (IMEI)");
			} else {
				out.write("Label");
			}
			%>
			</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("host_label_cd") : ""%></td>
			<td class="label">Manufacturer</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("host_equipment_mfgr") : ""%></td>
		</tr>
		<tr>
			<td class="label">Host Name</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("host_type_desc") : ""%></td>
			<td class="label">Model Code</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("host_equipment_model") : ""%></td>
		</tr>
		<tr>
			<td class="label">Port/Position</td>
			<td class="data"><%=hasHostProfileInfo ? ("0"
								.equals(hostProfileInfo
										.getFormattedValue("host_port_num")) ? "Bottom"
								: "Top")
								: ""%></td>
			<td class="label">
				<%				
				if(componentType != null && componentType.toString().endsWith("_MODEM")) {
					if(componentType.toString().startsWith("CDMA_"))
						out.write("MEID");
					else
						out.write("ICCID");
				} else {
                    out.write("Serial Number");
				}
				%>				
			</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("host_serial_cd") : ""%></td>
		</tr>

		<tr>
			<td class="label">Status</td>
			<td class="data"><%=hasHostProfileInfo ? MessageMap.getStatusCode()
						.get(
								hostProfileInfo
										.getFormattedValue("host_status_cd"))
						: ""%></td>
			<td class="label">Completion Minutes</td>
			<td class="data"><%=hasHostProfileInfo ? hostProfileInfo
						.getFormattedValue("host_est_complete_minut") : ""%></td>
		</tr>
	</tbody>

</table>
<div class="spacer10"></div>
<div class="gridHeader"><b>Device Information</b></div>
<div class="spacer10"></div>
<table class="tabDataDisplayBorder">
	<thead>
		<tr class="gridHeader">
			<td>Device ID</td>
			<td>Device Name</td>
			<td>Serial Number</td>
			<td>Device Type</td>
			<td>Customer</td>
			<td>Location</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><a href="profile.i?device_id=<%=deviceProfileInfo.getFormattedValue("device_id")%>"><%=deviceProfileInfo.getFormattedValue("device_id")%></a></td>
			<td><%=deviceProfileInfo.getFormattedValue("device_name")%></td>
			<td><%=deviceProfileInfo.getFormattedValue("device_serial_cd")%></td>
			<td><%=deviceProfileInfo.getFormattedValue("device_type_desc")%></td>
			<td><%=deviceProfileInfo.getFormattedValue("customer_name")%></td>
			<td><%=deviceProfileInfo.getFormattedValue("location_name")%></td>
		</tr>
	</tbody>
</table>
<div class="spacer10"></div>
<div class="gridHeader"><b>Settings</b></div>
<div class="spacer10"></div>
<table class="tabDataDisplayBorder">
	<thead>
		<tr class="gridHeader">
			<td>Code</td>
			<td>Value</td>
			<td>Last Updated</td>
		</tr>
	</thead>
	<tbody>
	<% while(hostSettingInfo.next()) { %>
	<tr>
	    <td><%=hostSettingInfo.getFormattedValue("host_setting_parameter")%></td>
	    <td><%=hostSettingInfo.getFormattedValue("host_setting_value")%></td>
	    <td><%=hostSettingInfo.getFormattedValue("last_updated_ts")%></td>
	</tr>
    <% } %>
	</tbody>
</table>

<div class="spacer10"></div>
<div class="gridHeader"><b>Cycle Time Estimate</b></div>
<div class="spacer10"></div>
<table class="tabDataDisplayBorder">
	<thead>
		<tr class="gridHeader">
			<td>Cycle Type</td>
			<td>Average Time</td>
			<td>Cycles in Average</td>
			<td>Last Cycle Time</td>
			<td>Last Updated</td>
		</tr>
	</thead>
	<tbody>
    <% while(hostEstTimeInfo.next()) { %>
    <tr>
        <td><%=hostEstTimeInfo.getFormattedValue("tran_line_item_type_desc")%></td>
        <td><%=hostEstTimeInfo.getFormattedValue("avg_tran_complete_minut")%></td>
        <td><%=hostEstTimeInfo.getFormattedValue("num_cycle_in_avg")%></td>
        <td><%=hostEstTimeInfo.getFormattedValue("last_tran_complete_minut")%></td>
        <td><%=hostEstTimeInfo.getFormattedValue("last_updated_ts")%></td>
    </tr>
    <% } %>
	</tbody>
</table>



<div class="spacer10"></div>
<div class="gridHeader"><b>Diagnostics</b></div>
<div class="spacer10"></div>
<table class="tabDataDisplayBorder">
	<thead>
		<tr class="gridHeader">
			<td>Code</td>
			<td>Value</td>
			<td>Start Time</td>
			<td>Last Reported Time</td>
			<td>Cleared Time</td>
		</tr>
	</thead>
	<tbody>
    <% while(hostDiagInfo.next()) { %>
    <tr>
        <td><%=hostDiagInfo.getFormattedValue("host_diag_cd")%></td>
        <td><%=hostDiagInfo.getFormattedValue("host_diag_value")%></td>
        <td><%=hostDiagInfo.getFormattedValue("host_diag_start_ts")%></td>
        <td><%=hostDiagInfo.getFormattedValue("host_diag_last_reported_ts")%></td>
        <td><%=hostDiagInfo.getFormattedValue("host_diag_clear_ts")%></td>
    </tr>
    <% } %>
	</tbody>
</table>
<div class="spacer5"></div>
<%
	}
%>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
