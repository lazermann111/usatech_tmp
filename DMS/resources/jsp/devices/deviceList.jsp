<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />


<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
if(missingParam != null && missingParam.booleanValue() == true) {
%>
<div class="tableContainer">
<span class="error">Required parameter not found: ev_number, serial_number, device_type, location_id,
               customer_id, firmware_version, enabled, authority_id, merchant_id, terminal_id</span>
</div>
<%	
} else {
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "-12,-13" : sortIndex;
%>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Device List</div>
</div>
<table class="tabDataDisplayBorder">
	<thead>
		<tr class="sortHeader">
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Device Type</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Device Name</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Serial Number</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "14", sortIndex)%>">Reporting Customer</a>
				<%=PaginationUtil.getSortingIconHtml("14", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">DMS Customer</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">DMS Location</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Firmware</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Diagnostic</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">PTest</a>
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
			</td>
			<td class="nowrap">
				<a href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">Bezel Mfgr</a>
				<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Bezel App Version</a>
				<%=PaginationUtil.getSortingIconHtml("10", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "11", sortIndex)%>">Comm Method</a>
				<%=PaginationUtil.getSortingIconHtml("11", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "-12,-13", sortIndex)%>">Last Activity</a>
				<%=PaginationUtil.getSortingIconHtml("-12,-13", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "-13,-12", sortIndex)%>">Status</a>
				<%=PaginationUtil.getSortingIconHtml("-13,-12", sortIndex)%>
			</td>
		</tr>
	</thead>
	<tbody>
	<% 
	Results deviceList = (Results) request.getAttribute("deviceList");
	int i = 0; 
	String rowClass = "row0";
	while(deviceList.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++;
	%>
		<tr class="<%=rowClass%>">
		    <td><%=deviceList.getFormattedValue("device_type_desc")%></td>
		    <td nowrap>
		    <a href="profile.i?device_id=<%=deviceList.getFormattedValue("device_id")%>"><%=deviceList.getFormattedValue("device_name")%></a>
		    <a href="deviceList.i?enabled=A&ev_number=<%=deviceList.getFormattedValue("device_name")%>"><img src="/images/disabled.gif" border="0" title="Display all devices for this Device Name" /></a>
		    </td>
		    <td><%=deviceList.getFormattedValue("device_serial_cd")%></td>
		    <td><%=deviceList.getFormattedValue("corp_customer_name")%></td>
		    <td><%=deviceList.getFormattedValue("customer_name")%></td>
		    <td><%=deviceList.getFormattedValue("location_name")%></td>
		    <td><%=deviceList.getFormattedValue("firmware_version")%></td>
		    <td><%=deviceList.getFormattedValue("diag_app_version")%></td>
		    <td><%=deviceList.getFormattedValue("ptest_version")%></td>
		    <td><%=deviceList.getFormattedValue("bezel_mfgr")%></td>
		    <td><%=deviceList.getFormattedValue("bezel_app_version")%></td>
		    <td><%=deviceList.getFormattedValue("comm_method_name")%></td>
		    <td>
		    <% 
		    double round = 0;
		    if(deviceList.getFormattedValue("round_last_activity_ts") != null && deviceList.getFormattedValue("round_last_activity_ts").trim().length() > 0) {
		    	round = Double.valueOf(deviceList.getFormattedValue("round_last_activity_ts"));
		    }
		    if(round > 5) {
		    %>
		    <span style="color: red"><%=deviceList.getFormattedValue("char_last_activity_ts")%><br />(<%=deviceList.getFormattedValue("round_last_activity_ts")%> days)</span>
		    <% } else if(round > 2) { %>
		    <span style="color: orange"><%=deviceList.getFormattedValue("char_last_activity_ts")%><br />(<%=deviceList.getFormattedValue("round_last_activity_ts")%> days)</span>
		    <% } else { %>
		    <span style="color: green"><%=deviceList.getFormattedValue("char_last_activity_ts")%></span>
		    <% } %>
		    </td>
		    <td>
		    <% 
		    String device_active_yn_flag = deviceList.getFormattedValue("device_active_yn_flag");
		    if("Y".equals(device_active_yn_flag)) {
		    %>
		    <span style="color: green">Enabled</span>
		    <% } else { %>
		    <span style="color: red">Disabled</span>
		    <% } %>
		    </td>
		</tr>
	<% } %>
	</tbody>
</table>

<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{
			DevicesConstants.PARAM_SERIAL_NUMBER, DevicesConstants.PARAM_FIRMWARE_VERSION,
            DevicesConstants.PARAM_CUSTOMER_ID, DevicesConstants.PARAM_LOCATION_ID,
            DevicesConstants.PARAM_DEVICE_TYPE_ID, DevicesConstants.PARAM_EV_NUMBER,
            DevicesConstants.PARAM_ENABLED, DevicesConstants.PARAM_TERMINAL_ID,
            DevicesConstants.PARAM_MERCHANT_ID, DevicesConstants.PARAM_AUTHORITY_ID,
            DevicesConstants.PARAM_CUSTOMER_NAME, DevicesConstants.PARAM_LOCATION_NAME,
            DevicesConstants.PARAM_SEARCH_PARAM, DevicesConstants.PARAM_SEARCH_TYPE,
            DevicesConstants.PARAM_COMM_METHOD, DevicesConstants.PARAM_CREDENTIAL_ID});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="devicelist.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

</div>

<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />