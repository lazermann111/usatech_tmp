<%@page import="simple.io.ByteArrayUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.device.EsudsCycleCode"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:include page="deviceHeaderTabs.jsp" flush="true" />

<%
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);	
	Device device = (Device) request.getAttribute(DevicesConstants.STORED_DEVICE);
	int deviceTypeId = device.getTypeId();
	String userOP = request.getParameter("userOP");
	boolean isGx = deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.G4.getValue();
	boolean isMEI = deviceTypeId == DeviceType.MEI.getValue();
%>

<div class="tabDataContent" >

<%if (device.hasCounters() || device.hasEvents()) { %>
<div class="gridHeader">
<input type="button" class="cssButton" value="List All" onClick="return redirectWithParams('deviceSettings.i', 'device_id=<%=device.getId()%>&userOP=list_all', new Array(document.getElementById('counter_count'), document.getElementById('event_from_date'), document.getElementById('event_from_time'), document.getElementById('event_to_date'), document.getElementById('event_to_time')));">
</div>
<% } %>

<div class="gridHeader">
	Device Settings
	<% if (device.hasCounters() || device.hasEvents()) { %>
	<input type="button" class="cssButton" value="List" onClick="return redirectWithParams('deviceSettings.i', 'device_id=<%=device.getId()%>&userOP=list_settings', null);">
	<% } %>
</div>

<%
Results deviceSettings = (Results) inputForm.getAttribute("deviceSettings");
if (deviceSettings != null) {
	String categoryName;
	String oldCategoryName = "";
%>
<table class="tabDataDisplay">
	<tr class="gridHeader">
		<td>Name</td>
		<td>Value</td>
		<td>Created</td>
		<td>Last Updated</td>		
		<td>Changed By</td>
		<td>Changed</td>
		<td>Old Value</td>
		<td>Configurable</td>
	</tr>
	<%
	while (deviceSettings.next()) { 
		String deviceSettingParameterCd = deviceSettings.getFormattedValue("device_setting_parameter_cd");
		if (isGx && Helper.isInteger(deviceSettingParameterCd) && DevicesConstants.GX_MAP_PROTECTED_FIELDS.contains(Integer.valueOf(deviceSettingParameterCd)))				
			continue;
		String editor = deviceSettings.getFormattedValue("editor");
		categoryName = deviceSettings.getFormattedValue("config_template_category_name");
		if (!StringHelper.isBlank(categoryName) && !categoryName.equalsIgnoreCase(oldCategoryName)) {
			oldCategoryName = categoryName;
	%>
	<tr>
		<td colspan="8"><b><%=categoryName %></b></td>
	</tr>
	<%}%>
	<tr>
		<td>
			<%
			boolean serverOnly = deviceSettings.getValue("server_only", boolean.class); 
			if (serverOnly)
				out.write("Server Setting: ");
			%>
			<%=deviceSettings.getFormattedValue("device_setting_name")%>
			<%if ((deviceTypeId == DeviceType.EDGE.getValue() || deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.G4.getValue() || deviceTypeId == DeviceType.MEI.getValue()) 
				&& !serverOnly && !deviceSettings.getFormattedValue("device_setting_name").equals(deviceSettingParameterCd)) {%>
				[<%=deviceSettingParameterCd %>]
			<%} %>
		</td>
		<td>
		<%
		int fieldOffset = deviceSettings.getValue("field_offset", int.class);
		if ((isGx || isMEI) && fieldOffset >=0 && fieldOffset < 512 && StringHelper.isHex(deviceSettings.getFormattedValue("raw_device_setting_value"))) {
			String hexValue = deviceSettings.getFormattedValue("raw_device_setting_value");
			String asciiValue;
			if (("A".equalsIgnoreCase(deviceSettings.getFormattedValue("data_mode")) || editor != null && editor.startsWith("SELECT")) && StringHelper.isHex(hexValue))
				asciiValue = new String(ByteArrayUtils.fromHex(hexValue));
			else	
				asciiValue = deviceSettings.getFormattedValue("device_setting_value");				
				
			if (asciiValue != null && !asciiValue.equals(hexValue) && StringHelper.isDataPrintable(asciiValue)) {
				out.write("\"");
				out.write(asciiValue);
				out.write("\", ");
				out.write("Hex: ");
			}			
			out.write(hexValue);
		} else 
			out.write(StringUtils.prepareCDATA(deviceSettings.getFormattedValue("device_setting_value")));
		%>
		</td>
		<td><%=deviceSettings.getFormattedValue("created_ts")%></td>
		<td><%=deviceSettings.getFormattedValue("last_updated_ts")%></td>
		<td><%=deviceSettings.getFormattedValue("changed_by")%></td>
		<td><%=deviceSettings.getFormattedValue("changed_ts")%></td>		
		<td>
		<%
		if ((isGx || isMEI) && fieldOffset >=0 && fieldOffset < 512 && StringHelper.isHex(deviceSettings.getFormattedValue("raw_old_device_setting_value"))) {
			String hexValue = deviceSettings.getFormattedValue("raw_old_device_setting_value");
			String asciiValue;
			if (("A".equalsIgnoreCase(deviceSettings.getFormattedValue("data_mode")) || editor != null && editor.startsWith("SELECT")) && StringHelper.isHex(hexValue))
				asciiValue = new String(ByteArrayUtils.fromHex(hexValue));
			else	
				asciiValue = deviceSettings.getFormattedValue("old_device_setting_value");				
				
			if (asciiValue != null && !asciiValue.equals(hexValue) && StringHelper.isDataPrintable(asciiValue)) {
				out.write("\"");
				out.write(asciiValue);
				out.write("\", ");
				out.write("Hex: ");
			}			
			out.write(hexValue);
		} else 
			out.write(StringUtils.prepareCDATA(deviceSettings.getFormattedValue("old_device_setting_value")));
		%>
		</td>
		<td><%=deviceSettings.getFormattedValue("configurable")%></td>
	</tr>
	<% } %>
</table>
<% } %>

<% if (device.hasCounters()) { %>
<div class="innerTable">
<div class="gridHeader">
Counters History ( <input type="text" class="csstest" onkeypress="return numbersonly(event, false, false);" id="counter_count" name="counter_count" size="5" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute("counter_count"))%>" />
 Most Recent ) <input type="button" class="cssButton" value="List" onClick="return redirectWithParams('deviceSettings.i', 'device_id=<%=device.getId()%>&userOP=list_counters', new Array(document.getElementById('counter_count')));">
</div>
<%
   String[] counter_names;
if (device.isEDGE()) {
	counter_names = DevicesConstants.counter_names_for_edge;
} else {
	counter_names = DevicesConstants.counter_names_for_other;
}

if(DevicesConstants.USER_OP_LIST_ALL.equals(userOP) || DevicesConstants.USER_OP_LIST_COUNTERS.equals(userOP)){

for (int i = 0; i < counter_names.length-3; i += 4) {
	Results counter1 = (Results) inputForm.getAttribute("counters_" + counter_names[i]);
	Results counter2 = (Results) inputForm.getAttribute("counters_" + counter_names[i+1]);
	Results counter3 = (Results) inputForm.getAttribute("counters_" + counter_names[i+2]);
	Results counter4 = (Results) inputForm.getAttribute("counters_" + counter_names[i+3]);

%>
<table class="tabDataDisplayNoBorder">
	<tr>
		<td valign=top>
		<table class="tabDataDisplayBorder">
			<thead>
				<tr class="gridHeader">
					<td colspan="2"><%=counter_names[i]%></td>
				</tr>
			</thead>
			<tbody>
				<%if (counter1 != null && counter1.next()) {
				do{%>
				<tr>
					<td><%=counter1.getFormattedValue("created_ts")%></td>
					<td><%=counter1.getFormattedValue("host_counter_value_fmt")%></td>
				</tr>
				<%
				}while( counter1.next());
				}%>
			</tbody>
		</table>
		</td>
		
		<td valign=top>
		<table class="tabDataDisplayBorder">
			<thead>
				<tr class="gridHeader">
					<td colspan="2"><%=counter_names[i+1]%></td>
				</tr>
			</thead>
			<tbody>
				<%if (counter2 != null && counter2.next()) {
				do{%>
				<tr>
					<td><%=counter2.getFormattedValue("created_ts")%></td>
					<td ><%=counter2.getFormattedValue("host_counter_value_fmt")%></td>
				</tr>
				<%
				}while( counter2.next());
				}%>
			</tbody>
		</table>
		</td>
		
		<td valign=top>
		<table class="tabDataDisplayBorder">
			<thead>
				<tr class="gridHeader">
					<td colspan="2"><%=counter_names[i+2]%></td>
				</tr>
			</thead>
			<tbody>
				<%if (counter3 != null && counter3.next()) {
				do{%>
				<tr>
					<td><%=counter3.getFormattedValue("created_ts")%></td>
					<td><%=counter3.getFormattedValue("host_counter_value_fmt")%></td>
				</tr>
				<%
				}while( counter3.next());
				}%>
			</tbody>
		</table>
		</td>
		
		<td valign=top>
		<table class="tabDataDisplayBorder">
			<thead>
				<tr class="gridHeader">
					<td colspan="2"><%=counter_names[i+3]%></td>
				</tr>
			</thead>
			<tbody>
				<%if (counter4 != null && counter4.next()) {
				do{%>
				<tr>
					<td><%=counter4.getFormattedValue("created_ts")%></td>
					<td ><%=counter4.getFormattedValue("host_counter_value_fmt")%></td>
				</tr>
				<%
				}while( counter4.next());
				}%>
			</tbody>
		</table>
		</td>
	</tr>
</table>
<%
}
}
%>
</div>
<% } %>
<% if (device.hasEvents()) { %>
<div class="gridHeader">
	Event History From <input type="text" name="event_from_date" id="event_from_date" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute("event_from_date"))%>" size="8" maxlength="10" /> 
		<img src="/images/calendar.gif" id="from_date_trigger" class="calendarIcon" title="Date selector" />
		<input type="text" size="6" maxlength="8" id="event_from_time" name="event_from_time" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute("event_from_time"))%>" /> 
		To <input type="text" name="event_to_date" id="event_to_date" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute("event_to_date"))%>" size="8" maxlength="10" />
		<img src="/images/calendar.gif" id="to_date_trigger" class="calendarIcon" title="Date selector" />
		<input type="text" size="6" maxlength="8" id="event_to_time" name="event_to_time" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute("event_to_time"))%>" />
	<input type="button" class="cssButton" value="List" onClick="handleListBtn();">
</div>
<div class="spacer2"></div>	
	<%
		Results events = (Results) inputForm.getAttribute("events");
			if (events != null) {
				String sortField = PaginationUtil.getSortField(null);
				String sortIndex = inputForm.getString(sortField, false);
				sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "-2" : sortIndex;
	%>
<table class="tabDataDisplay">
	<tbody>
		<tr class="sortHeader">
			<td><a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Event ID</a> <%=PaginationUtil.getSortingIconHtml("1", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Time</a> <%=PaginationUtil.getSortingIconHtml("2", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">State</a> <%=PaginationUtil.getSortingIconHtml("3", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Type</a> <%=PaginationUtil.getSortingIconHtml("4", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Host Type</a> <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Call ID</a> <%=PaginationUtil.getSortingIconHtml("6", sortIndex)%></td>
		</tr>
		<%
			while (events.next()) {
		%>
		<tr>
			<td><a href="event.i?event_id=<%=events.getFormattedValue("event_id")%>"><%=events.getFormattedValue("event_id")%></a></td>
			<td><%=events.getFormattedValue("event_start_ts")%></td>
			<td><%=events.getFormattedValue("event_state_name")%></td>
			<td><%=events.getFormattedValue("event_type_name")%></td>
			<td><%=events.getFormattedValue("host_type_desc")%></td>
			<td><%if (!StringHelper.isBlank(events.getFormattedValue("global_session_cd"))) {%><a href="/deviceCallInLog.i?session_cd=<%=events.getFormattedValue("global_session_cd")%>"><%=events.getFormattedValue("session_id")%></a><%} %>&nbsp;</td>
		</tr>
		<%
			}
		%>
	</tbody>
</table>

<%
}
	String storedNames = PaginationUtil.encodeStoredNames(new String[]{
					DevicesConstants.PARAM_DEVICE_ID,
					DevicesConstants.PARAM_USER_OP,
					DevicesConstants.PARAM_EVENT_FROM_DATE,
					DevicesConstants.PARAM_EVENT_TO_DATE,
					DevicesConstants.PARAM_EVENT_FROM_TIME,
					DevicesConstants.PARAM_EVENT_TO_TIME});
%> 

<jsp:include page="/jsp/include/pagination.jsp" flush="true">
	<jsp:param name="_param_request_url" value="deviceSettings.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

<script type="text/javascript" defer="defer">
    Calendar.setup({
        inputField     :    "event_from_date",                  // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "event_to_date",                   // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });


    var fromDate = document.getElementById("event_from_date");
    var fromTime = document.getElementById("event_from_time");
    var toDate = document.getElementById("event_to_date");
    var toTime = document.getElementById("event_to_time");
    
    function validateDate() {
    	if(!isDate(fromDate.value)) {
        	fromDate.focus();
        	return false;
    	}
    	if(!isTime(fromTime.value)) {
        	fromTime.focus();
        	return false;
    	}
    	if(!isDate(toDate.value)) {
        	toDate.focus();
        	return false;
    	}
    	if(!isTime(toTime.value)) {
        	toTime.focus();
        	return false;
    	}
    	return true;
    }

    function handleListBtn() {
        if(validateDate()) {
        	return redirectWithParams('deviceSettings.i', 'device_id=<%=device.getId()%>&userOP=list_events', new Array(fromDate, fromTime, toDate, toTime));
        }
    }
</script>
<% } %>
</div>

<jsp:include page="deviceFooter.jsp" flush="true" />
<jsp:include page="/jsp/include/footer.jsp" flush="true" />