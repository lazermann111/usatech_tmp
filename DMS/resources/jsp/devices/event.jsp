<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="eventInfo" type="simple.results.Results" scope="request" />
<jsp:useBean id="event_id" class="java.lang.String" scope="request" />
<jsp:useBean id="eventDetailsExist" class="java.lang.String" scope="request" />
<jsp:useBean id="eventCountersExist" class="java.lang.String" scope="request" />
<jsp:useBean id="eventFilesExist" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (not empty(errorMessage))}">
		<div class="tableContainer">
			<span class="error">Error Message: ${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>

		<div class="tabDataContent">
		<div class="innerTable" >
		<div class="tabHead" align="center" >Event Information</div>
		
		<c:set var="event_info_item" value="${eventInfo[0]}" />
		
		<table class="tabDataDisplayBorder" >
			 
			 <tr>
			 
			  <td nowrap>Event ID</td>
			  <td width="25%">${event_info_item.event_id }&nbsp;</td>
			
			  <td nowrap>Event Type</td>
			  <td >${event_info_item.event_type_name }&nbsp;</td>
			  
			 </tr>
			 <tr>
			 
			  <td nowrap>Event State</td>
			  <td>${event_info_item.event_state_name }&nbsp;</td>
			  
			  <td nowrap>Start Time</td>
			  <td>${event_info_item.event_start_ts }&nbsp;</td>
			  
			 </tr>
			 <tr>
			 
			  <td>Created</td>
			  <td>${event_info_item.created_ts }&nbsp;</td>
			  
			  <td nowrap>End Time</td>
			  <td>${event_info_item.event_end_ts }&nbsp;</td>
			  
			
			 </tr> 
			 <tr>
			  <td nowrap>Last Updated</td>
			  <td>${event_info_item.last_updated_ts }&nbsp;</td>
			  <td nowrap>Upload Time</td>
			  <td>${event_info_item.event_upload_complete_ts }&nbsp;</td>
			 </tr> 
			 <tr>
			
			  <td nowrap>Device Event ID</td>
			  <td>${event_info_item.event_device_tran_cd}&nbsp;</td>
			  
			  <td nowrap>Global Event Code</td>
			  <td>${event_info_item.event_global_trans_cd}&nbsp;</td>
			  
			 </tr> 
			 <tr>
			 
			  <td nowrap>Device Serial #</td>
			  <td><a href="profile.i?device_id=${event_info_item.device_id}">${ event_info_item.device_serial_cd}</a>&nbsp;</td>
			  
			  <td nowrap>Device Type</td>
			  <td>${event_info_item.device_type_desc }&nbsp;</td>
			  
			 </tr> 
			 <tr>
			 
			  <td nowrap>Host ID</td>
			  <td><a href="hostProfile.i?host_id=${event_info_item.host_id}">${event_info_item.host_id}</a>&nbsp;</td>
			
			  <td nowrap>Host Type</td>
			  <td>${event_info_item.host_type_desc}&nbsp;</td>
			  
			 </tr>
			
			<c:choose>
				<c:when test="${not empty(event_info_item.tran_id)}">
				
					<tr>
					  <td nowrap>Reference Tran ID</td>
					  <td><a href="tran.i?tran_id=${event_info_item.tran_id}">${event_info_item.tran_id}</a>&nbsp;</td>
					  <td nowrap>Tran Start Time</td>
					  <td>${event_info_item.tran_start_ts}&nbsp;</td>
					</tr>
				
				</c:when>
			</c:choose>
			
			<c:choose>
				<c:when test="${not empty(event_info_item.global_session_cd)}">				
					<tr>
					  <td nowrap>Call ID</td>
					  <td colspan="3"><a href="/deviceCallInLog.i?session_cd=${event_info_item.global_session_cd}">${event_info_item.session_id}</a>&nbsp;</td>
					</tr>				
				</c:when>
			</c:choose>			
			
			</table>
			
			<c:choose>
				<c:when test="${not empty(event_info_item.device_batch_id)}">
				
					<div class="spacer5"></div>
					<div class="tabHead" align="center" >Settlement Event</div>
					<table class="tabDataDisplayBorder" >
					  <tr>
					   <td nowrap>Device Batch ID</td>
					   <td width="25%">${event_info_item.device_batch_id}&nbsp;</td>
					   <td nowrap>Device New Batch ID</td>
					   <td >${event_info_item.device_new_batch_id}&nbsp;</td>
					  </tr> 
					  <tr>
					   <td nowrap>Timestamp</td>
					   <td colspan="3">${event_info_item.device_event_utc_ts}&nbsp;</td>
					  </tr>
					</table>
				
				</c:when>
			</c:choose>
			
			<div class="spacer5"></div>
			<div class="tabHead" align="center" >Event Details</div>
			<table class="tabDataDisplayBorder">
			  <tr>
			   <td nowrap align="center"><B>Detail ID</B></td>
			   <td nowrap align="center"><B>Detail Type</B></td>
			   <td align="center"><B>Value</B></td>
			   <td align="center"><B>Timestamp</B></td>
			  </tr>
			
			<c:choose>
				<c:when test="${not empty(eventDetailsExist) and eventDetailsExist == 'true'}">
					<jsp:useBean id="eventDetails" type="simple.results.Results" scope="request" />
					<c:forEach var="event_detail_item" items="${eventDetails}">
							
							<tr>    	 
							     <td>${event_detail_item.event_detail_id }&nbsp;</td>
								 <td>${event_detail_item.event_detail_type_name }&nbsp;</td>
								 <td>${event_detail_item.event_detail_value }&nbsp;</td>
							     <td>${event_detail_item.event_detail_value_ts }&nbsp;</td>
					    	</tr>
					    		    	
					</c:forEach>	
				
				</c:when>
			</c:choose>
			
			</table>
			
			<c:choose>
				<c:when test="${not empty(eventCountersExist) and eventCountersExist == 'true'}">
					<jsp:useBean id="eventCounters" type="simple.results.Results" scope="request" />
					
					<div class="spacer5"></div>
					<div class="tabHead" align="center" >Event Counters</div>
					<table class="tabDataDisplayBorder">
						  	<tr>
						   	<th nowrap>Counter ID</th>
						   	<th nowrap>Host ID</th>
						   	<th nowrap>Host Type</th>
						   	<th>Counter</th>
						   	<th>Value</th>
						   	<th>Timestamp</th>
					  	</tr>
					
					<c:forEach var="event_counter_item" items="${eventCounters}">
							
							<tr>    	 
							     <td>${event_counter_item.host_counter_id }&nbsp;</td>
							     <td><a href="hostProfile.i?host_id=${event_counter_item.host_id }">${event_counter_item.host_id }</a>&nbsp;</td>
								 <td>${event_counter_item.host_type_desc }&nbsp;</td>
							     <td>${event_counter_item.host_counter_parameter }&nbsp;</td>
							     <td>${event_counter_item.host_counter_value }&nbsp;</td>
							     <td>${event_counter_item.created_ts }&nbsp;</td>
					    	</tr>
					    		    	
					</c:forEach>	
					
					</table>
				</c:when>
			</c:choose>
			
			<c:choose>
				<c:when test="${not empty(eventFilesExist) and eventFilesExist == 'true'}">
					<jsp:useBean id="eventFiles" type="simple.results.Results" scope="request" />
					
					<div class="spacer5"></div>
					<div class="tabHead" align="center" >Event Counters</div>
					<table class="tabDataDisplayBorder">
					  	<tr class="gridHeader">
						   	<th>Transfer ID</th>
						   	<th>File Name</th>
						   	<th>Direction</th>
						   	<th>File Type</th>
						   	<th>Transfer Time</th>
						   	<th>Status</th>
					  	</tr>
					
					<c:forEach var="event_file_item" items="${eventFiles}">
							
							<tr>    	 
							     <td>${event_file_item.device_file_transfer_id }&nbsp;</td>
							     <td><a href="fileDetails.i?file_transfer_id=${ event_file_item.file_transfer_id}">${ event_file_item.file_transfer_name}</a>&nbsp;</td>
								 <td>${event_file_item.device_file_transfer_direct }&nbsp;</td>
							     <td>${event_file_item.file_transfer_type_name }&nbsp;</td>
							     <td>${event_file_item.device_file_transfer_ts }&nbsp;</td>
							     <td>${event_file_item.device_file_transfer_status }&nbsp;</td>
					    	</tr>
					    		    	
					</c:forEach>	
					
					</table>
				</c:when>
			</c:choose>
			
		</div>
		</div>
	</c:otherwise>

</c:choose>


<jsp:include page="/jsp/include/footer.jsp" flush="true" />