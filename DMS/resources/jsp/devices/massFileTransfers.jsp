<%@page import="simple.text.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="formContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Mass File Transfers</span></div>
</div>

<div class="tableContent">

<div class="spacer5"></div>

<form method="post" action="massFileTransfers.i" onsubmit="return doSubmit()">

<table class="padding3">		
	<%if(msg != null && msg.length() > 0) {%>
	<tr><td colspan="4" class="status-info"><%=StringUtils.prepareHTML(msg)%></td></tr>
	<%} %>
	<%if(err != null && err.length() > 0) {%>
	<tr><td colspan="4" class="status-error"><%=StringUtils.prepareHTML(err)%></td></tr>
	<%} %>
		
	<tr>
		<td class="label" valign="top">Serial Number List<br/>(1 per line)</td>
		<td>
			<textarea name="serial_number_list" id="serial_number_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("serial_number_list", "")%></textarea>
		</td>
		<td class="label" valign="top">File ID List<br/>(1 per line)</td>
		<td>
			<textarea name="file_id_list" id="file_id_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("file_id_list", "")%></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center">
		<div class="spacer5"></div>
		<input type="submit" name="action" value="Upload to Devices" class="cssButton" />
		<div class="spacer5"></div>
		</td>
	</tr>
</table>

</form>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
function doSubmit(){	
	if(document.getElementById("serial_number_list").value.trim() == ''){
		alert('Please enter serial numbers');
		return false;
	}
	
	if(document.getElementById("file_id_list").value.trim() == ''){
		alert('Please enter file IDs');
		return false;
	}

	return true;
}
</script>
