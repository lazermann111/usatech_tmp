<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.model.PaymentConfig"%>

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String cloningType = inputForm.getString("cloningType", false);
    Device sourceDevice = (Device)inputForm.getAttribute("sourceDevice");
    Device targetDevice = (Device)inputForm.getAttribute("targetDevice");
    Device device = null;
    long deviceId = Long.parseLong(request.getParameter("deviceId"));
    if (deviceId == sourceDevice.getId())
    {
        device = sourceDevice;
    }
    else
    {
        device = targetDevice;
    }

    List<PaymentConfig> sourceConfigList = device.getPaymentConfigList();
    if (!sourceConfigList.isEmpty())
    {
%>

<div class="innerTable">
<div class="tabHead">Payment Configuration&nbsp;(<%="S".equals(cloningType) ? device.getSerialNumber() : device.getDeviceName()%>)</div>
<table class="tabDataDisplay">

	<col width="50%"></col>
	<col width="25%"></col>
	<col width="25%"></col>

	<tr class="gridHeader">
		<td style="border: 0px;">Type</td>
		<td style="border: 0px;">Merchant</td>
		<td style="border: 0px;">Status</td>
	</tr>

	<%
	    String lastMethodCd = "";
	        PaymentConfig currentConfig = null;
	        for (int i = 0; i < sourceConfigList.size(); i++)
	        {
	            currentConfig = sourceConfigList.get(i);
	%>

	<%
	    if (!lastMethodCd.equals(currentConfig.getPaymentEntryMethodCd()))
	            {
	%>
	<tr align="center" class="gridHeader">
		<td colspan="3"><%=currentConfig.getPaymentEntryMethodDesc()%></td>
	</tr>
	<%
	    }
	            lastMethodCd = currentConfig.getPaymentEntryMethodCd();
	%>

	<tr>
		<td><%=currentConfig.getPaymentSubtypeName()%></td>
		<td><%=currentConfig.getMerchantDesc() == null ? "" : currentConfig.getMerchantDesc()%></td>
		<td>
		<%
		    if (currentConfig.getDeactivationTs() != null)
		            {
		%> <%=currentConfig.getStatusTxtDisabled()%> <%
     }
             else if (currentConfig.getActivationTs() != null)
             {
 %> <%=currentConfig.getStatusTxtEnabled()%> <%
     }
             else
             {
 %> <b><font color="orange">Inactive</font></b> <%
     }
 %> <%=currentConfig.getPrefAuthAmount() != null ? " ($" + currentConfig.getPrefAuthAmount() + " Override)" : ""%>
		</td>
	</tr>

	<%
	    }
	%>

</table>
<div class="spacer5"></div>
</div>
<%
    }
%>