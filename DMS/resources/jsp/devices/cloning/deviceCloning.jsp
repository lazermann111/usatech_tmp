<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.dms.device.EsudsCycleCode"%>
<%@page import="com.usatech.layers.common.model.Customer"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.model.Location"%>
<%@page import="com.usatech.layers.common.model.PaymentConfig"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />



<%
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
			String sourceNumber = inputForm.getString("sourceNumber", false);
			String targetNumber = inputForm.getString("targetNumber", false);
			String userOP = inputForm.getString("userOP", false);
			String cloningType = inputForm.getString("cloningType", false);
			Device sourceDevice = (Device) inputForm.getAttribute("sourceDevice");
			Device targetDevice = (Device) inputForm.getAttribute("targetDevice");
%>

<div class="tableContainer">
<%
    if (StringHelper.isBlank(userOP)) {
%>
<div class="tableHead">
<div class="tabHeadTxt">USAT Device Setting Clone Tool</div>
</div>

<div class="tabDataContent">
<form name="deviceCloningForm" id="deviceCloningForm" method="get"
	action="deviceCloning.i" onsubmit="return confirmSubmit()">
<div class="innerTable">
<table class="tabDataDisplay">

	<col width="65%"></col>
	<col width="35%"></col>

	<tr>
		<td colspan="2"><input type="radio" name="cloningType"
			checked="checked" value="S" />Serial Number <input type="radio"
			name="cloningType" value="E" />Device Name</td>
	</tr>

	<tr>
		<td>Source Number:</td>
		<td><input class="cssText" type="text" name="sourceNumber"
			id="sourceNumber" /></td>
	</tr>

	<tr>
		<td>Target Number:</td>
		<td><input class="cssText" type="text" name="targetNumber"
			id="targetNumber" /></td>
	</tr>

	<tr>
		<td>This application clones device server settings from one
		device to another. Cloned settings include:<br />
		<ul>
			<li>Customer</li>
			<li>Location</li>
			<li>Payment Configuration</li>
			<li>Configuration Parameters</li>
		</ul>
		You may optionally assign the clone source device to the "Unknown"
		location after the process is complete, if cloning is being used to
		replace a device at a location.
		<p>To do this, enter either the&nbsp;<b class="txtGreen">Device
		Name</b>&nbsp;or&nbsp;<b class="txtGreen">Serial Number</b>&nbsp;for the
		clone source device&nbsp;<b>(Source Number)</b>&nbsp;and the the clone
		target device&nbsp;<b>(Target Number)</b></p>
		</td>
		<td><input name="userOP" class="cssButton" type="submit"
			value="Compare" /></td>
	</tr>

</table>
</div>
</form>
</div>
<%
    } else if ("Compare".equals(userOP)) {
%>

<div class="tableHead">
<div class="tabHeadTxt">USAT Device Setting Clone Tool</div>
</div>

<div class="tabDataContent">
<form name="deviceCloningForm" id="deviceCloningForm" method="post"
	action="deviceCloning.i" onsubmit="return confirmCheckbox();"><input
	type="hidden" name="cloningType" value="<%=cloningType%>" /> <input
	type="hidden" name="sourceNumber" value="<%=sourceNumber%>" /> <input
	type="hidden" name="targetNumber" value="<%=StringUtils.prepareCDATA(targetNumber)%>" />
<div class="innerTable">
<div class="tabHead"><%="E".equals(cloningType)
						? "Device Name"
						: "Serial Number"%></div>
<table class="tabDataDisplay">

	<col width="35%"></col>
	<col width="65%"></col>

	<tr>
		<td>Source Number:</td>
		<td><%=StringUtils.prepareCDATA(sourceNumber)%></td>
	</tr>

	<tr>
		<td>Target Number:</td>
		<td><%=targetNumber%></td>
	</tr>

	<%
	    if (sourceDevice == null) {
	%>
	<tr>
		<td colspan="2"><input type="button" value="<-- Go Back"
			onClick="javascript:history.back(1)" /></td>
	</tr>
	<tr>
		<td colspan="2"><span class="error">Are you sure your
		Selection Type (EV/Serial) matches your entry?</span></td>
	</tr>
	<%
	    } else if (targetDevice == null) {
	%>
	<tr>
		<td colspan="2"><input type="button" value="<-- Go Back"
			onClick="javascript:history.back(1)" /></td>
	</tr>
	<tr>
		<td colspan="2"><span class="error">Destination Device ID
		Can't be found?</span></td>
	</tr>
	<%
	    } else {
	%>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="userOP" value="Clone" class="cssButton"/></td>
	</tr>
	<%
	    }
	%>

</table>
<div class="spacer5"></div>
</div>

<%
    if (sourceDevice != null && targetDevice != null) {
%>  <jsp:include page="tableProperties.jsp" flush="true" />
    <jsp:include page="tablePos.jsp" flush="true" />
    <jsp:include page="tableLocation.jsp" flush="true" />
    <jsp:include page="tableCustomer.jsp" flush="true" />
    <jsp:include page="tablePaymentConfig.jsp" flush="true">
        <jsp:param name="deviceId" value="<%=sourceDevice.getId()%>" />
    </jsp:include>
    <jsp:include page="tablePaymentConfig.jsp" flush="true">
	    <jsp:param name="deviceId" value="<%=targetDevice.getId()%>" />
    </jsp:include> <%
     }
 %>
</form>
</div>
<%
    } else if ("Clone".equals(userOP)) {
				String errorMsg = inputForm.getString("errorMsg", true);
%>
<div align="center"><%=errorMsg%> <%
     if (errorMsg.contains("succeeded")) {
 %>
<br />
<br />
<br />
<%
DeviceType targetDeviceType = DeviceType.getByValue((byte) targetDevice.getTypeId());
if (sourceDevice.getTypeId() == targetDevice.getTypeId() && (targetDeviceType == DeviceType.GX || targetDeviceType == DeviceType.G4 || targetDeviceType == DeviceType.EDGE || targetDeviceType == DeviceType.KIOSK || targetDeviceType == DeviceType.MEI)) {
%>
<a href="editDeviceConfig.i?device_id=<%=targetDevice.getId()%>&device_type_id=<%=targetDevice.getTypeId()%>&ev_number=<%=targetDevice.getDeviceName()%>&ssn=<%=targetDevice.getSerialNumber()%>&plv=<%=targetDevice.getPropertyListVersion()%>&edit_mode=<%=targetDevice.isKIOSK() ? "R" : "A"%>&file_name=<%=targetDevice.getConfigFileName()%>&myaction=Import&template_name=<%=sourceDevice.getConfigFileName()%>&template_device_id=<%=sourceDevice.getId()%>">Clone Configuration Parameters</a>
<br />
<br />
<%}%>
<a href="profile.i?device_id=<%=targetDevice.getId()%>">Go To Cloned Device</a> <br />
<br />
<br />
<%
    }
%>
</div>

<%
    }
%>
</div>
<div class="spacer5"></div>
<script type="text/javascript">
<!--
function confirmCheckbox()
{
    var agree=confirm("Please verify all settings before proceeding");
    if (agree)
        return true ;
    else
        return false ;
}

function confirmSubmit()
{
	with(document.deviceCloningForm) {
		if (sourceNumber.value == '') {
		    alert('Source number is empty !');
		    return false;
		}
		if (targetNumber.value == '') {
		    alert('target number is empty !');
		    return false;
		}
		if (sourceNumber.value == targetNumber.value)
		{
		     alert('Source and Target number should be different !!!');
		     return false ;
		} else {
		     return true ;
		}
	}
}
// -->
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />