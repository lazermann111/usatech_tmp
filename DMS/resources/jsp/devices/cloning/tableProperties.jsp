<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.model.Device"%>

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String cloningType = inputForm.getString("cloningType", false);
    Device sourceDevice = (Device)inputForm.getAttribute("sourceDevice");
    Device targetDevice = (Device)inputForm.getAttribute("targetDevice");
%>

<div class="innerTable">
<div class="tabHead">Properties To Be Cloned</div>
<table class="tabDataDisplay">

	<col width="24%"></col>
	<col width="38%"></col>
	<col width="38%"></col>

	<tr class="gridHeader">
		<td>&nbsp;</td>
		<td>Source<br />
		(<%="S".equals(cloningType) ? sourceDevice.getSerialNumber() : sourceDevice.getDeviceName()%>)</td>
		<td>Target<br />
		(<%="S".equals(cloningType) ? targetDevice.getSerialNumber() : targetDevice.getDeviceName()%>)</td>
	</tr>

	<tr>
		<td>Device ID</td>
		<td><%=sourceDevice.getId()%></td>
		<%
		    if (sourceDevice.getId() != targetDevice.getId())
		    {
		%>
		<td style="background-color: orange;"><%=targetDevice.getId()%></td>
		<%
		    }
		    else
		    {
		%>
		<td><%=targetDevice.getId()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Device Name</td>
		<td><%=sourceDevice.getDeviceName()%></td>
		<%
		    if (!sourceDevice.getDeviceName().equals(targetDevice.getDeviceName()))
		    {
		%>
		<td style="background-color: orange;"><%=targetDevice.getDeviceName()%></td>
		<%
		    }
		    else
		    {
		%>
		<td><%=targetDevice.getDeviceName()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Device Serial Code</td>
		<td><%=sourceDevice.getSerialNumber()%></td>
		<%
		    if (!sourceDevice.getSerialNumber().equals(targetDevice.getSerialNumber()))
		    {
		%>
		<td style="background-color: orange;"><%=targetDevice.getSerialNumber()%></td>
		<%
		    }
		    else
		    {
		%>
		<td><%=targetDevice.getSerialNumber()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Device Type Id</td>
		<td><%=sourceDevice.getTypeId()%></td>
		<%
		    if (sourceDevice.getTypeId() != targetDevice.getTypeId())
		    {
		%>
		<td style="background-color: orange;"><%=targetDevice.getTypeId()%></td>
		<%
		    }
		    else
		    {
		%>
		<td><%=targetDevice.getTypeId()%></td>
		<%
		    }
		%>
	</tr>

</table>
<div class="spacer5"></div>
</div>