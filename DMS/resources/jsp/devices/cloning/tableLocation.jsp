<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.model.Location"%>

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String cloningType = inputForm.getString("cloningType", false);
    Device sourceDevice = (Device)inputForm.getAttribute("sourceDevice");
    Device targetDevice = (Device)inputForm.getAttribute("targetDevice");
    Location sourceLocation = sourceDevice.getLocation();
    Location targetLocation = targetDevice.getLocation();
%>

<div class="innerTable">
<div class="tabHead">Location</div>
<table class="tabDataDisplay">

	<col width="24%"></col>
	<col width="38%"></col>
	<col width="38%"></col>

	<tr>
		<td>Location ID</td>
		<td><%=sourceLocation.getId() == null ? "" : sourceLocation.getId()%></td>
		<%
		    if (targetLocation.getId() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (sourceLocation.getId() != null && targetLocation.getId().longValue() == sourceLocation.getId().longValue())
		    {
		%>
		<td><%=targetLocation.getId()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getId()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Name</td>
		<td><%=sourceLocation.getName() == null ? "" : sourceLocation.getName()%>
		<%
		    if (sourceLocation.getId() != null && sourceLocation.getId().longValue() > 1)
		    {
		%> <br />
			<input type="checkbox" name="locationCheckbox"
					id="locationCheckbox" value="1"
					<%=sourceDevice.isESUDS() ? "checked='checked'" : ""%> />
				<span style="color: green;">Set to
				'Unknown' after Clone (e.g. use for device replacements)</span>
		<%
		    }
		%>
		</td>
		<%
		    if (targetLocation.getName() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getName().equals(sourceLocation.getName()))
		    {
		%>
		<td><%=targetLocation.getName()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getName()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Address 1</td>
		<td><%=sourceLocation.getAddress1() == null ? "" : sourceLocation.getAddress1()%></td>
		<%
		    if (targetLocation.getAddress1() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getAddress1().equals(sourceLocation.getAddress1()))
		    {
		%>
		<td><%=targetLocation.getAddress1()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getAddress1()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Address 2</td>
		<td><%=sourceLocation.getAddress2() == null ? "" : sourceLocation.getAddress2()%></td>
		<%
		    if (targetLocation.getAddress2() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getAddress2().equals(sourceLocation.getAddress2()))
		    {
		%>
		<td><%=targetLocation.getAddress2()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getAddress2()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location City</td>
		<td><%=sourceLocation.getCity() == null ? "" : sourceLocation.getCity()%></td>
		<%
		    if (targetLocation.getCity() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getCity().equals(sourceLocation.getCity()))
		    {
		%>
		<td><%=targetLocation.getCity()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getCity()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Country</td>
		<td><%=sourceLocation.getCountry() == null ? "" : sourceLocation.getCountry()%></td>
		<%
		    if (targetLocation.getCountry() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getCountry().equals(sourceLocation.getCountry()))
		    {
		%>
		<td><%=targetLocation.getCountry()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getCountry()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Postal Code</td>
		<td><%=sourceLocation.getPostalCode() == null ? "" : sourceLocation.getPostalCode()%></td>
		<%
		    if (targetLocation.getPostalCode() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getPostalCode().equals(sourceLocation.getPostalCode()))
		    {
		%>
		<td><%=targetLocation.getPostalCode()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getPostalCode()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Parent Location ID</td>
		<td><%=sourceLocation.getParentId() == null ? "" : sourceLocation.getParentId()%></td>
		<%
		    if (targetLocation.getParentId() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (sourceLocation.getParentId() != null && targetLocation.getParentId().longValue() == sourceLocation.getParentId().longValue())
		    {
		%>
		<td><%=targetLocation.getParentId()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getParentId()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Country Code</td>
		<td><%=sourceLocation.getCountryCode() == null ? "" : sourceLocation.getCountryCode()%></td>
		<%
		    if (targetLocation.getCountryCode() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getCountryCode().equals(sourceLocation.getCountryCode()))
		    {
		%>
		<td><%=targetLocation.getCountryCode()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getCountryCode()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Type ID</td>
		<td><%=sourceLocation.getTypeId() == null ? "" : sourceLocation.getTypeId()%></td>
		<%
		    if (targetLocation.getTypeId() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (sourceLocation.getTypeId() != null && targetLocation.getTypeId().intValue() == sourceLocation.getTypeId().intValue())
		    {
		%>
		<td><%=targetLocation.getTypeId()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getTypeId()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location State Code</td>
		<td><%=sourceLocation.getStateCode() == null ? "" : sourceLocation.getStateCode()%></td>
		<%
		    if (targetLocation.getStateCode() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getStateCode().equals(sourceLocation.getStateCode()))
		    {
		%>
		<td><%=targetLocation.getStateCode()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getStateCode()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Time Zone Code</td>
		<td><%=sourceLocation.getTimeZoneCode() == null ? "" : sourceLocation.getTimeZoneCode()%></td>
		<%
		    if (targetLocation.getTimeZoneCode() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getTimeZoneCode().equals(sourceLocation.getTimeZoneCode()))
		    {
		%>
		<td><%=targetLocation.getTimeZoneCode()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getTimeZoneCode()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Location Active YN Flag</td>
		<td><%=sourceLocation.getActiveFlag() == null ? "" : sourceLocation.getActiveFlag()%></td>
		<%
		    if (targetLocation.getActiveFlag() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetLocation.getActiveFlag().equals(sourceLocation.getActiveFlag()))
		    {
		%>
		<td><%=targetLocation.getActiveFlag()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetLocation.getActiveFlag()%></td>
		<%
		    }
		%>
	</tr>

</table>
<div class="spacer5"></div>
</div>