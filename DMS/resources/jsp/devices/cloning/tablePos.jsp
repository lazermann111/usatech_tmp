<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.model.Device"%>

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String cloningType = inputForm.getString("cloningType", false);
    Device sourceDevice = (Device)inputForm.getAttribute("sourceDevice");
    Device targetDevice = (Device)inputForm.getAttribute("targetDevice");
%>

<div class="innerTable">
<div class="tabHead">POS</div>
<table class="tabDataDisplay">

	<col width="24%"></col>
	<col width="38%"></col>
	<col width="38%"></col>

	<tr>
		<td>POS ID</td>
		<td><%=sourceDevice.getPosId()%></td>
		<% if (targetDevice.getPosId() == sourceDevice.getPosId()) { %>
		<td><%=targetDevice.getPosId()%></td>
		<% } else { %>
		<td style="background-color: orange;"><%=targetDevice.getPosId()%></td>
		<% } %>
	</tr>

</table>
<div class="spacer5"></div>
</div>