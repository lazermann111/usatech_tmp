<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.model.Customer"%>
<%@page import="com.usatech.layers.common.model.Device"%>

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String cloningType = inputForm.getString("cloningType", false);
    Device sourceDevice = (Device)inputForm.getAttribute("sourceDevice");
    Device targetDevice = (Device)inputForm.getAttribute("targetDevice");
    Customer sourceCustomer = sourceDevice.getCustomer();
    Customer targetCustomer = targetDevice.getCustomer();
%>

<div class="innerTable">
<div class="tabHead">Customer</div>
<table class="tabDataDisplay">

	<col width="24%"></col>
	<col width="38%"></col>
	<col width="38%"></col>

	<tr>
		<td>Customer ID</td>
		<td><%=sourceCustomer.getId() == null ? "" : sourceCustomer.getId()%></td>
		<%
		    if (targetCustomer.getId() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (sourceCustomer.getId() != null && targetCustomer.getId().longValue() == sourceCustomer.getId().longValue())
		    {
		%>
		<td><%=targetCustomer.getId()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getId()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer Name</td>
		<td><%=sourceCustomer.getName() == null ? "" : sourceCustomer.getName()%></td>
		<%
		    if (targetCustomer.getName() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getName().equals(sourceCustomer.getName()))
		    {
		%>
		<td><%=targetCustomer.getName()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getName()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer Address1</td>
		<td><%=sourceCustomer.getAddress1() == null ? "" : sourceCustomer.getAddress1()%></td>
		<%
		    if (targetCustomer.getAddress1() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getAddress1().equals(sourceCustomer.getAddress1()))
		    {
		%>
		<td><%=targetCustomer.getAddress1()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getAddress1()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer Address2</td>
		<td><%=sourceCustomer.getAddress2() == null ? "" : sourceCustomer.getAddress2()%></td>
		<%
		    if (targetCustomer.getAddress2() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getAddress2().equals(sourceCustomer.getAddress2()))
		    {
		%>
		<td><%=targetCustomer.getAddress2()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getAddress2()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer City</td>
		<td><%=sourceCustomer.getCity() == null ? "" : sourceCustomer.getCity()%></td>
		<%
		    if (targetCustomer.getCity() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getCity().equals(sourceCustomer.getCity()))
		    {
		%>
		<td><%=targetCustomer.getCity()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getCity()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer Country</td>
		<td><%=sourceCustomer.getCountry() == null ? "" : sourceCustomer.getCountry()%></td>
		<%
		    if (targetCustomer.getCountry() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getCountry().equals(sourceCustomer.getCountry()))
		    {
		%>
		<td><%=targetCustomer.getCountry()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getCountry()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer Postal Code</td>
		<td><%=sourceCustomer.getPostalCode() == null ? "" : sourceCustomer.getPostalCode()%></td>
		<%
		    if (targetCustomer.getPostalCode() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getPostalCode().equals(sourceCustomer.getPostalCode()))
		    {
		%>
		<td><%=targetCustomer.getPostalCode()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getPostalCode()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer Country Code</td>
		<td><%=sourceCustomer.getCountryCode() == null ? "" : sourceCustomer.getCountryCode()%></td>
		<%
		    if (targetCustomer.getCountryCode() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getCountryCode().equals(sourceCustomer.getCountryCode()))
		    {
		%>
		<td><%=targetCustomer.getCountryCode()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getCountryCode()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer Type ID</td>
		<td><%=sourceCustomer.getTypeId() == null ? "" : sourceCustomer.getTypeId()%></td>
		<%
		    if (targetCustomer.getTypeId() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (sourceCustomer.getTypeId() != null && targetCustomer.getTypeId().intValue() == sourceCustomer.getTypeId().intValue())
		    {
		%>
		<td><%=targetCustomer.getTypeId()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getTypeId()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer State Code</td>
		<td><%=sourceCustomer.getStateCode() == null ? "" : sourceCustomer.getStateCode()%></td>
		<%
		    if (targetCustomer.getStateCode() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getStateCode().equals(sourceCustomer.getStateCode()))
		    {
		%>
		<td><%=targetCustomer.getStateCode()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getStateCode()%></td>
		<%
		    }
		%>
	</tr>

	<tr>
		<td>Customer Active YN Flag</td>
		<td><%=sourceCustomer.getActiveFlag() == null ? "" : sourceCustomer.getActiveFlag()%></td>
		<%
		    if (targetCustomer.getActiveFlag() == null)
		    {
		%>
		<td>&nbsp;</td>
		<%
		    }
		    else if (targetCustomer.getActiveFlag().equals(sourceCustomer.getActiveFlag()))
		    {
		%>
		<td><%=targetCustomer.getActiveFlag()%></td>
		<%
		    }
		    else
		    {
		%>
		<td style="background-color: orange;"><%=targetCustomer.getActiveFlag()%></td>
		<%
		    }
		%>
	</tr>

</table>
<div class="spacer5"></div>
</div>