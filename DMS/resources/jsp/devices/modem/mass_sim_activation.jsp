<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="spacer5"></div>

<ul class="shadeTabs">
<li>&nbsp;</li>
<li><a href="massSimActivation.i" class="selected">Mass SIM Activation (EsEye)</a></li>
<li><a href="massSimActivationVerizon.i">Mass SIM Activation (Verizon)</a></li>
<li><a href="simActivationStatus.i">Activation Status</a></li>
</ul>

<div class="tableDataContainer tabContent" style="width: 80%">
<form method="post" action="massSimActivation.i" onsubmit="return doSubmit()">
<table class="padding3">
	<tr>
		<td class="label" valign="top">SIM ICCIDs or MEIDs<br/><small>one item per line or<br/>comma separated</small></td>
		<td>
			<textarea name="icc_ids" id="icc_ids" rows="20" style="width: 500px;"></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div class="spacer5"></div>
		<input type="submit" name="action" value="Activate SIMs" class="cssButton" />
		<div class="spacer5"></div>
		</td>
	</tr>
	<%if(err != null && err.length() > 0) {%>
    	<tr><td colspan="2" class="status-error"><%=err%></td></tr>
    <%} %>
</table>
</form>

</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
function doSubmit(){
	if(document.getElementById("icc_ids").value.trim() == '') {
		alert('Please enter SIMs ICCIDs');
		return false;
	}
	return true;
}
</script>
