<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.service.modem.service.dto.DeviceModemState"%>
<%@page import="simple.service.modem.service.DeviceModemStatusDao"%>
<%@page import="simple.service.modem.service.DeviceModemStatusDaoImpl"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="java.util.List"%>
<%@page import="simple.service.modem.service.dto.ModemState"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
DeviceModemStatusDao deviceModemStatusDao = new DeviceModemStatusDaoImpl();
List<List<DeviceModemState>> groupedStatuses = (List) RequestUtils.getAttribute(request, "groupedStatuses", Object.class, true);
Integer statusPending = (Integer) RequestUtils.getAttribute(request, "status_pending", Integer.class, false);
Integer statusActivated = (Integer) RequestUtils.getAttribute(request, "status_activated", Integer.class, false);
Integer statusFailed = (Integer) RequestUtils.getAttribute(request, "status_failed", Integer.class, false);
if ((statusPending == null || statusPending == 0) &&
    (statusActivated == null || statusActivated == 0) &&
    (statusFailed == null || statusFailed == 0)) {
    // When no params then show all checkboxes checked
    statusPending = 1;
    statusActivated = 1;
    statusFailed = 1;
}

String pendingChecked = statusPending != null && statusPending != 0 ? "checked" : "";
String activatedChecked = statusActivated != null && statusActivated != 0 ? "checked" : "";
String failedChecked = statusFailed != null && statusFailed != 0 ? "checked" : "";
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<script language="JavaScript">
<!--

function showHide(HID,IMG) {
        if (document.getElementById(IMG).src.indexOf('expand') != -1) {
                document.getElementById(IMG).src='/images/collapse.gif';
                document.getElementById(HID).className='visibleRow';
        } else {
                document.getElementById(IMG).src='/images/expand.gif';
                document.getElementById(HID).className='hiddenRow';
        }
}

// -->
</script>

<div class="spacer5"></div>
<ul class="shadeTabs">
<li>&nbsp;</li>
<li><a href="massSimActivation.i">Mass SIM Activation (EsEye)</a></li>
<li><a href="massSimActivationVerizon.i">Mass SIM Activation (Verizon)</a></li>
<li><a href="simActivationStatus.i" class="selected">Activation Status</a></li>
</ul>
<div class="tableDataContainer tabContent" style="width: 80%">
<form method="post" action="simActivationStatus.i">
<span style="background-color: #EEEEEE; padding: 3px"><input style="position: relative; top: 2px" type="checkbox" name="status_pending" value="1" <%= pendingChecked %>><b>PENDING</b></input></span>&nbsp;&nbsp;&nbsp;<span style="background-color: #99FF99; padding: 3px"><input style="position: relative; top: 2px" type="checkbox" name="status_activated" value="1" <%= activatedChecked %>><b>ACTIVATED</b></input></span>&nbsp;&nbsp;&nbsp;<span style="background-color: #FF9999; padding: 3px"><input style="position: relative; top: 2px" type="checkbox" name="status_failed" value="1" <%= failedChecked %>><b>FAILED</b></input></span>&nbsp;&nbsp;&nbsp;<input type="submit" name="action" value="FILTER" class="cssButton"/>
</form>
<table class="padding3">
    <% if (groupedStatuses.isEmpty() || groupedStatuses.get(0).isEmpty()) { %>
        <tr>
            <td class="label">
                <br/>
                <br/>
                NO SIM ACTIVATION REQUESTS WERE MADE RECENTLY
                <br/>
                <br/>
            </td>
        </tr>
    <% } else { %>
    <%  for(int g = 0; g < groupedStatuses.size(); g++) {
        List<DeviceModemState> statuses = groupedStatuses.get(g);
        int groupId = groupedStatuses.size() - g;
        %>
        <tr>
            <td>
                <img id="image<%=groupId%>" border="0" src="<%= g == 0 ? "/images/collapse.gif" : "/images/expand.gif" %>" valign="ABSBOTTOM" onClick="showHide('trHidden<%=groupId%>','image<%=groupId%>');" style="cursor:pointer;">&nbsp;
                <span style="font-weight:bold;cursor:pointer;color:#0099FF;" onClick="showHide('trHidden<%=groupId%>','image<%=groupId%>');">
                    Activation request status <%= g == 0 ? "(LATEST)" : ("("+ statuses.get(0).getStatusUpdatedTs().toString() +")")%>
                </span>
            </td>
        </tr>
        <tr><td>
            <table id="trHidden<%=groupId%>" class="<%= g == 0 ? "visibleRow" : "hiddenRow" %>">
            <tr>
                <td class="label" valign="top" align="center">
                    DEVICE ID
                </td>
                <td class="label" valign="top" align="center">
                    TIME
                </td>
                <td class="label" valign="top" align="center">
                    STATUS
                </td>
            </tr>
        <%
        for(int i = 0; i < statuses.size(); i++) {
            ModemState modemStatus = ModemState.fromStringStatus(statuses.get(i).getLastModemStatusCd());
            String statusText = "<b>" + modemStatus.name() + "</b>";
            boolean isOk;
            boolean isInProgress;
            String devColor;
            if (modemStatus == ModemState.PENDING_ACTIVATION) {
                isInProgress = true;
                isOk = true;
                devColor = "#FFFFFF";
            } else if (modemStatus == ModemState.ACTIVE) {
               isInProgress = false;
               isOk = true;
               devColor = "#99FF99";
            } else {
               isInProgress = false;
               isOk = false;
               devColor = "#FF9999";
            }
            Long deviceId = deviceModemStatusDao.findDeviceIdBySerialCode(statuses.get(i).getModemSerialCode());
            if (deviceId == null) {
            	deviceId = 0L;
            }
            String errMsg = (!isInProgress && !isOk) ?
                            (" :: ERROR " + (statuses.get(i).getErrorCode() == null ? "" : statuses.get(i).getErrorCode()) +
                            (statuses.get(i).getErrorMessage() == null ? "" : (" - " + statuses.get(i).getErrorMessage()))) : "";
        %>
    <tr>
        <td style="background-color: <%=devColor%>">
            <a href=<%="/profile.i?device_id=" + StringUtils.prepareHTML(deviceId.toString())%> > <%=StringUtils.prepareHTML(statuses.get(i).getModemSerialCode())%></a>
        </td>
        <td style="background-color: <%=devColor%>">
            <%=statuses.get(i).getStatusUpdatedTs().toString() %>
        </td>
        <td style="background-color: <%=devColor%>">
            <%=statusText + StringUtils.prepareHTML(errMsg) %>
        </td>
    </tr>
    <% } %>
    </table></td>
    </tr>
    <% } %>
	<% } %>
</table>

</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
