<%@page import="java.sql.Timestamp"%>
<%@page import="com.usatech.layers.common.messagedata.MessageDataFactory"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
String sessionCd = RequestUtils.getAttribute(request, "session_cd", String.class, false);
String searchDevice = "";
String startDate = "";
String startTime = "";
String endDate = "";
String endTime = "";
Results deviceMessageResults = null;
if (StringHelper.isBlank(sessionCd)) {
	InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);	
	searchDevice = inputForm.getString("searchDevice", false);
	if (searchDevice == null)
		searchDevice = "";
	
	startTime = inputForm.getString("StartTime", false);
	if (StringHelper.isBlank(startTime))
		startTime = Helper.getDefaultStartTime();
	
	startDate = inputForm.getString("StartDate", false);
	if (StringHelper.isBlank(startDate))
		startDate = Helper.getDefaultStartDate(); 
	
	endDate = inputForm.getString("EndDate", false);
	if (StringHelper.isBlank(endDate))
		endDate = Helper.getDefaultEndDate();
	
	endTime = inputForm.getString("EndTime", false);
	if (StringHelper.isBlank(endTime))
		endTime = Helper.getDefaultEndTime();
	
	if (!StringHelper.isBlank(searchDevice)) {
		String deviceName = DeviceActions.getDeviceName(searchDevice);
		if (deviceName != null)
			deviceMessageResults = DataLayerMgr.executeQuery("GET_DEVICE_MESSAGES", new Object[] {deviceName, startDate, startTime, endDate, endTime});
	}
} else {
	Results sessionResults = DataLayerMgr.executeQuery("GET_SESSION_INFO", new Object[] {sessionCd});
	if (sessionResults != null && sessionResults.next()) {
		searchDevice = sessionResults.getFormattedValue("deviceName");
		startDate = sessionResults.getFormattedValue("sessionStartDate");
		startTime = sessionResults.getFormattedValue("sessionStartTime");
		endDate = sessionResults.getFormattedValue("sessionEndDate");
		endTime = sessionResults.getFormattedValue("sessionEndTime");
	}
	deviceMessageResults = DataLayerMgr.executeQuery("GET_SESSION_MESSAGES", new Object[] {sessionCd});
}
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Device Messages</div>
</div>
<form name="deviceMessagesForm" method="get" action="deviceMessages.i" onsubmit="return validateForm(this) && validateDate();">
<table align="center" width="100%" class="tabDataDisplay">
	<tr class="gridHeader">
		<td align="center">
		Date/Time Range
		<table class="noBorder">
			<tr>
				<td align="center">
					<b>Start</b> 
					<input type="text" id="StartDate" size="8" maxlength="10" name="StartDate" value="<%=startDate %>" usatRequired="true" label="Start Date" /> 
					<img src="/images/calendar.gif" id="StartDate_trigger" class="calendarIcon" title="Date selector" />
					<input type="text" size="6" maxlength="8" id="StartTime" name="StartTime" value="<%=startTime %>" usatRequired="true" label="Start Time" /> 
					&nbsp;&nbsp;<b>End</b> 
					<input type="text" id="EndDate" size="8" maxlength="10" name="EndDate" value="<%=endDate %>" usatRequired="true" label="End Date" /> 
					<img src="/images/calendar.gif" id="EndDate_trigger" class="calendarIcon" title="Date selector" />
					<input type="text" size="6" maxlength="8" id="EndTime" name="EndTime" value="<%=endTime %>" usatRequired="true" label="End Time" />
				</td>
			</tr>
		</table>
		</td>
		<td>
		<b>Device Name or Serial #</b> 
		<input type="text" name="searchDevice" value="<%=searchDevice %>" usatRequired="true" label="Device Name or Serial #" />
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div class="spacer5"></div>
		<input class="cssButton" type="submit" value="Submit" />
		<div class="spacer5"></div>
		</td>
	</tr>
</table>
</form>

<div class="spacer5"></div>

<script type="text/javascript" defer="defer">
Calendar.setup({
    inputField     :    "StartDate",                  // id of the input field
    ifFormat       :    "%m/%d/%Y",            // format of the input field
    button         :    "StartDate_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                  // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
Calendar.setup({
    inputField     :    "EndDate",                   // id of the input field
    ifFormat       :    "%m/%d/%Y",             // format of the input field
    button         :    "EndDate_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                   // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

var fromDate = document.getElementById("StartDate");
var fromTime = document.getElementById("StartTime");
var toDate = document.getElementById("EndDate");
var toTime = document.getElementById("EndTime");

function validateDate() {
	if(!isDate(fromDate.value)) {
    	fromDate.focus();
    	return false;
	}
	if(!isTime(fromTime.value)) {
    	fromTime.focus();
    	return false;
	}
	if(!isDate(toDate.value)) {
    	toDate.focus();
    	return false;
	}
	if(!isTime(toTime.value)) {
    	toTime.focus();
    	return false;
	}
	return true;
}

</script>

<%if (deviceMessageResults!=null){ %>
	<table class="tabDataDisplay">
		<tr class="gridHeader">
			<td>Source</td>
			<td>Timestamp</td>
			<td>Message Type</td>
			<td>Bytes</td>
			<td>Message Content</td>
			<td>Details</td>
		</tr>
	<% 
	String sessionName ="";
	int i = 0;
	Device device = null;
	DeviceType deviceType = null;
	while(deviceMessageResults.next()){
		if (device == null) {
			device = DeviceActions.loadDevice(DeviceActions.getDeviceIdBySerial(deviceMessageResults.getFormattedValue("deviceSerialCd"), new Timestamp(System.currentTimeMillis())), request);
			deviceType = DeviceType.getByValue((byte) device.getTypeId());
		}
		if(!sessionName.equals(deviceMessageResults.getValue("globalSessionCd"))){
	%>
	<tr class="gridHeader">
		<td colspan="6" align="left">
			Call ID: <a href="/deviceCallInLog.i?session_cd=<%=deviceMessageResults.getValue("globalSessionCd") %>"><%=deviceMessageResults.getValue("sessionId") %></a>,
			Session Code: <%=deviceMessageResults.getValue("globalSessionCd") %>,
			Session State: <%=deviceMessageResults.getValue("sessionStateDesc")%>, 
			Client: <%=deviceMessageResults.getFormattedValue("sessionClientIPAddr")%>:<%=deviceMessageResults.getFormattedValue("sessionClientPort")%>,
			Net Layer: <%=deviceMessageResults.getFormattedValue("netLayerDesc")%>,
			Session Start: <%=deviceMessageResults.getValue("sessionStartTs")%>,
			Session End: <%=deviceMessageResults.getValue("sessionEndTs")%>,
			Inbound Messages: <%=deviceMessageResults.getFormattedValue("sessionInboundMsgCnt")%>,
			Outbound Messages: <%=deviceMessageResults.getFormattedValue("sessionOutboundMsgCnt")%>,
			Encrypted Inbound Bytes: <%=deviceMessageResults.getFormattedValue("sessionInboundByteCnt")%>,
			Encrypted Outbound Bytes: <%=deviceMessageResults.getFormattedValue("sessionOutboundByteCnt")%>	
		</td>
	</tr>
	<%
		sessionName=deviceMessageResults.getFormattedValue("globalSessionCd");
	}
	%>	

	<%
	if (!StringHelper.isBlank(deviceMessageResults.getFormattedValue("inboundType"))) {
		i++;
	%>				
	<tr class="row<%=i % 2%>">
		<td>Client</td>
		<td class="nowrap"><%=deviceMessageResults.getValue("inboundTs") %></td>
		<td class="nowrap"><%=deviceMessageResults.getFormattedValue("inboundType")%></td>
		<td><%=deviceMessageResults.getFormattedValue("inboundLength")%></td>
		<td>
		<%String inboundContent = ConvertUtils.getStringSafely(deviceMessageResults.getValue("inboundContent"));%>
		<%=StringHelper.isBlank(inboundContent) ? "&nbsp;" : MessageDataFactory.readInboundMessageSafely(inboundContent, deviceType)%> 
		</td>
		<td>Duration:&nbsp;<%=deviceMessageResults.getFormattedValue("messageDurationMs")%>&nbsp;ms</td>
	</tr>
	<%
	}
	if (!StringHelper.isBlank(deviceMessageResults.getFormattedValue("outboundType"))) {
		i++;
	%>
	<tr class="row<%=i % 2%>">
		<td>Server</td>
		<td class="nowrap"><%=deviceMessageResults.getValue("outboundTs") %></td>
		<td class="nowrap"><%=deviceMessageResults.getFormattedValue("outboundType")%></td>
		<td><%=deviceMessageResults.getFormattedValue("outboundLength")%></td>
		<td>
		<%String outboundContent = ConvertUtils.getStringSafely(deviceMessageResults.getValue("outboundContent"));%>
		<%=StringHelper.isBlank(outboundContent) ? "&nbsp;" : MessageDataFactory.readOutboundMessageSafely(outboundContent, deviceType)%>
		</td>
		<td>Transmitted:&nbsp;<%=deviceMessageResults.getFormattedValue("messageTransmitted")%></td>
	</tr>
	<%
	}
	}
	%>
	</table>
	
	<div class="spacer5"></div>

<%} %>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />