<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.text.Normalizer.Form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.stream.Collectors"%>
<%@page import="com.usatech.dms.sales.CommissionEventType"%>
<%@page import="com.usatech.dms.sales.SalesRep"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="com.usatech.dms.action.ConfigFileActions"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Set"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Results types = RequestUtils.getAttribute(request, "types", Results.class, true);
Results states = RequestUtils.getAttribute(request, "states", Results.class, true);
Results countries = RequestUtils.getAttribute(request, "countries", Results.class, true);
Results locationTypes = RequestUtils.getAttribute(request, "locationTypes", Results.class, true);
Results timeZones = RequestUtils.getAttribute(request, "timeZones", Results.class, true);
Results productTypes = RequestUtils.getAttribute(request, "productTypes", Results.class, true);
Results paymentSchedules = RequestUtils.getAttribute(request, "paymentSchedules", Results.class, true);
Results businessTypes = RequestUtils.getAttribute(request, "businessTypes", Results.class, true);
String customerBankId = inputForm.getStringSafely("customer_bank_id", "").trim();
Results activateDetails=DataLayerMgr.executeQuery("GET_ACTIVATE_DETAIL", null);
Results deactivateDetails=DataLayerMgr.executeQuery("GET_DEACTIVATE_DETAIL", null);
Results distributors = DataLayerMgr.executeQuery("GET_DISTRIBUTORS", null);
List<SalesRep> salesReps = SalesRep.findAllActive();
List<CommissionEventType> eventTypes = CommissionEventType.findAll();
Map<String, Object> params = new HashMap<String, Object>();

String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="fullPageFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Device Activations</span></div>
</div>

<div class="tableContent">

<form id="main_form" method="post" action="deviceInit.i" onsubmit="return validateForm(this) && doSubmit();">

<table class="padding3">
	<%if(msg != null && msg.length() > 0) {%>
	<tr><td colspan="4" class="status-info"><%=StringUtils.prepareHTML(msg)%></td></tr>
	<%} %>
	<%if(err != null && err.length() > 0) {%>
	<tr><td colspan="4" class="status-error"><%=StringUtils.prepareHTML(err)%></td></tr>
	<%} %>
		
	<tr>
		<td class="label" valign="top">Serial Numbers<font color="red">*</font><br />(1 per line)</td>
		<td>
			<textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("dev_list", "")%></textarea>
		</td>
		<td class="label" valign="top">Source Serial Numbers for Swapping<br />(1 per line)</td>
		<td>
			<textarea name="dev_list_to_swap" id="dev_list_to_swap" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("dev_list_to_swap", "")%></textarea>
		</td>
	</tr>
			
	<tr>
		<td class="label">Initialize Devices</td>
		<td><input type="checkbox" name="initialize" id="initialize" value="Y"<%if (inputForm.getStringSafely("initialize", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
		<td class="label">Device Type</td>
		<td>
			<select name="typeId" id="typeId">
				<option value="-1">Auto Detection</option>
			<%if (types!=null){
			String deviceTypeId = inputForm.getStringSafely("typeId", "").trim();
			while(types.next()) {
			%><option value="<%=types.getValue("DEVICE_TYPE_ID")%>"<%if (deviceTypeId.length() > 0 && deviceTypeId.equals(types.getFormattedValue("DEVICE_TYPE_ID"))) out.write(" selected=\"selected\"");%>><%=types.getFormattedValue("DEVICE_TYPE_DESC")%></option>
			<%}}%>
			</select>
		</td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
			
	<tr>
		<td class="label">Register Devices</td>
		<td><input type="checkbox" name="register" id="register" value="Y"<%if (inputForm.getStringSafely("register", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
		<td class="label">Program</td>
		<td>
			<select name="dealerId" id="dealerId" onchange="updateResellerRequired()">
			<%
			Results dealers;
			if (customerBankId.length() > 0 && !customerBankId.equals("0")) {
				params.clear();
				params.put("customerBankId", customerBankId);
				dealers = DataLayerMgr.executeQuery("GET_CUSTOMER_DEALERS_WITH_LICENSE_BY_BANK", params);
			} else
				dealers = DataLayerMgr.executeQuery("GET_DEALERS_WITH_LICENSE", null);
			String dealerId = inputForm.getStringSafely("dealerId", "").trim();
			while(dealers.next()) {
				String type = dealers.getFormattedValue("licenseType"); %>
				<option value="<%=dealers.getValue("dealerId")%>"<%if (dealerId.length() > 0 && dealerId.equals(dealers.getFormattedValue("dealerId"))) out.write(" selected=\"selected\"");%>>
				<%=dealers.getFormattedValue("dealerName")%> <%=type.equals("C") ? "(SELL RATE)" : type.equals("R") ? "(BUY RATE)" : "" %>
				</option>
			<%}%>
			</select>
		</td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
	
	<tr>
		<td class="label">Swap Devices</td>
		<td><input type="checkbox" name="swap_devices" id="swap_devices" value="Y"<%if (inputForm.getStringSafely("swap_devices", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> onchange="checkSwapping()"/></td>
		<td class="label">Reuse old terminals</td>
		<td><input type="checkbox" name="reuse_terminal" id="reuse_terminal" value="Y"<%if (inputForm.getStringSafely("reuse_terminal", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	
	<tr>
		<td class="label">Effective Date<font color="red">*</font></td>
		<td>
			<input type="text" name="swap_effective_date" id="swap_effective_date" maxlength="10" value="<%=inputForm.getStringSafely("swap_effective_date", Helper.getCurrentDate())%>" />
			<img src="/images/calendar.gif" id="swap_effective_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Effective Time</td>
		<td><input type="text" name="swap_effective_time" id="swap_effective_time" maxlength="8" value="<%=inputForm.getStringSafely("swap_effective_time", Helper.getCurrentTime())%>" /></td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>	
	
	<tr>
		<td class="label">Deactivate Devices</td>
		<td><input type="checkbox" name="deactivate" id="deactivate" value="Y"<%if (inputForm.getStringSafely("deactivate", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
		<td class="label">Override</td>
		<td><input type="checkbox" name="terminal_override" id="terminal_override" value="Y"<%if (inputForm.getStringSafely("terminal_override", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	<tr>
		<td class="label">Deactivation Reason/Details<font color="red">*</font></td>
		<td colspan="4">
		<select name=deactivateDetailId id="deactivateDetailId">
			<option value="0">Please select:</option>
			<%if (deactivateDetails != null){
				int deactivateDetailId = inputForm.getInt("deactivateDetailId", false, -1);
			while(deactivateDetails.next()) {
			%><option value="<%=deactivateDetails.getValue("deactivateDetailId")%>"<%if (deactivateDetailId> 0 && deactivateDetails.getValue("deactivateDetailId",int.class)==deactivateDetailId) out.write(" selected=\"selected\"");%>><%=deactivateDetails.getFormattedValue("deactivateDetail")%></option>	    	
			<%}}%>	
		</select>
		</td>
	</tr>
	<tr>
		<td class="label">Deactivation Reason/Details Additional Info</td>
		<td colspan="4"><input id="deactivateDetail" class="cssText" type="text" size="100" value="<%=inputForm.getStringSafely("deactivateDetail", "")%>" name="deactivateDetail"/></td>
	</tr>
	
	<tr>
		<td class="label">Terminal End Date<font color="red">*</font></td>
		<td>
			<input type="text" name="terminal_end_date" id="terminal_end_date" maxlength="10" value="<%=inputForm.getStringSafely("terminal_end_date", Helper.getCurrentDate())%>" />
			<img src="/images/calendar.gif" id="terminal_end_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Terminal End Time</td>
		<td><input type="text" name="terminal_end_time" id="terminal_end_time" maxlength="8" value="<%=inputForm.getStringSafely("terminal_end_time", Helper.getCurrentTime())%>" /></td>
	</tr>
	<tr>
		<td class="label">Include Rentals</td>
		<td>
			<input type="checkbox" name="deactivate_rentals" id="deactivate_rentals" value="Y"<%if (inputForm.getStringSafely("deactivate_rentals", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%>/>
		</td>
		<td class="label">Include QuickStarts</td>
		<td>
			<input type="checkbox" name="deactivate_quick_starts" id="deactivate_quick_starts" value="Y"<%if (inputForm.getStringSafely("deactivate_quick_starts", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%>/>
		</td>
	</tr>
	<tr>
		<td class="label">Sales Rep</td>
		<td class="data" colspan="3">
			<% long deactSalesRepId = inputForm.getLong("deact_salesRepId", false, 0);
				 long deactFallbackSalesRepId = inputForm.getLong("deact_fallback_salesRepId", false, 0); %>
			<select name="deact_salesRepId" id="deact_salesRepId" onchange="deactSalesRepChanged();">
				<option value="0"></option>
				<option value="-999" <%=deactSalesRepId == -999 ? "selected=\"selected\"" : "" %>>- Current Sales Rep -</option>
				<% for(SalesRep salesRep : salesReps) { %>
				<option value="<%=salesRep.getSalesRepId()%>" <%=salesRep.getSalesRepId() == deactSalesRepId ? "selected=\"selected\"" : ""%>><%=salesRep.getName() %></option>
				<% } %>
			</select>
			<div id="deactFallback" name="deactFallback" style="display:<%=deactSalesRepId == -999 ? "inline-block;" : "none"%>;">
				or if none 
				<select name="deact_fallback_salesRepId" id="deact_fallback_salesRepId">
					<option value="0"></option>
					<% for(SalesRep salesRep : salesReps) { %>
					<option value="<%=salesRep.getSalesRepId()%>" <%=salesRep.getSalesRepId() == deactFallbackSalesRepId ? "selected=\"selected\"" : ""%>><%=salesRep.getName() %></option>
					<% } %>
				</select>
			</div>
		</td>
	</tr>
	<tr>
		<td class="label">Commission Effect</td>
		<td class="data" colspan="3">
			<% String deactEventTypeCode = inputForm.getStringSafely("deact_eventTypeCode", ""); %>
			<select name="deact_eventTypeCode" id="deact_eventTypeCode" style="font-family: 'Courier New', Courier, monospace">
				<option value="0"></option>
				<% for(CommissionEventType eventType : eventTypes) { if (eventType.isDeact()) {%>
				<option style="font-family: 'Courier New', Courier, monospace" <%=deactEventTypeCode.equals(eventType.getCode().toString()) ? "selected=\"selected\"" : "" %> value="<%=eventType.getCode()%>"><%=eventType.getFixedWidthDesc()%></option>
				<% } } %>
			</select>
		</td>
	</tr>
	
	<tr>
		<td class="label">Error on Multiple Devices per Terminal</td>
		<td><input type="checkbox" name="error_on_multiple_device_per_terminal" id="error_on_multiple_device_per_terminal" value="Y"<%if (inputForm.getStringSafely("error_on_multiple_device_per_terminal", "Y").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	
	<tr>
		<td class="label">Error on Campus Card Fee</td>
		<td><input type="checkbox" name="error_on_campus_card_fee" id="error_on_campus_card_fee" value="Y"<%if (inputForm.getStringSafely("error_on_campus_card_fee", "Y").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
	
	<tr>
		<td class="label">Clear Service Fees</td>
		<td><input type="checkbox" name="clear_service_fees" id="clear_service_fees" value="Y"<%if (inputForm.getStringSafely("clear_service_fees", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
		<td class="label">Override</td>
		<td><input type="checkbox" name="service_fee_override" id="service_fee_override" value="Y"<%if (inputForm.getStringSafely("service_fee_override", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	
	<tr>
		<td class="label">Service Fee End Date<font color="red">*</font></td>
		<td>
			<input type="text" name="service_fee_end_date" id="service_fee_end_date" maxlength="10" value="<%=inputForm.getStringSafely("service_fee_end_date", Helper.getCurrentDate())%>" />
			<img src="/images/calendar.gif" id="service_fee_end_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label nowrap">Service Fee End Time</td>
		<td><input type="text" name="service_fee_end_time" id="service_fee_end_time" maxlength="8" value="<%=inputForm.getStringSafely("service_fee_end_time", Helper.getCurrentTime())%>" /></td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
	
	<tr>
		<td class="label nowrap">Deactivate Card Authorizations</td>
		<td colspan="3"><input type="checkbox" name="deactivate_cashless" id="deactivate_cashless" value="Y"<%if (inputForm.getStringSafely("deactivate_cashless", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	
	<tr>
		<td class="label">Deactivation Date<font color="red">*</font></td>
		<td>
			<input type="text" name="cashless_end_date" id="cashless_end_date" maxlength="10" value="<%=inputForm.getStringSafely("cashless_end_date", Helper.getCurrentDate())%>" />
			<img src="/images/calendar.gif" id="cashless_end_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Deactivation Time</td>
		<td><input type="text" name="cashless_end_time" id="cashless_end_time" maxlength="8" value="<%=inputForm.getStringSafely("cashless_end_time", Helper.getCurrentTime())%>" /></td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
	
	<tr>
		<td class="label">Activate Devices</td>
		<td colspan="4"><input type="checkbox" name="activate" id="activate" value="Y"<%if (inputForm.getStringSafely("activate", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	<tr>
		<td class="label">Activation Reason/Details<font color="red">*</font></td>
		<td>
		<select name=activateDetailId id="activateDetailId" onchange="onActivationReasonChange();">
			<option value="0">Please select:</option>
			<%if (activateDetails != null){
				int activateDetailId = inputForm.getInt("activateDetailId", false, -1);
			while(activateDetails.next()) {
			%><option value="<%=activateDetails.getValue("activateDetailId")%>"<%if (activateDetailId> 0 && activateDetails.getValue("activateDetailId",int.class)==activateDetailId) out.write(" selected=\"selected\"");%>><%=activateDetails.getFormattedValue("activateDetail")%></option>	    	
			<%}}%>	
		</select>
		</td>
		<td class="label">Distributor Name<span style="color:red; display:none" id="distributorRequired">*</span></td>
		<td><select id="distributorName" name="distributorName" type="text"/>
				<option value="0">Please select:</option>
				<%if (distributors != null){
				int distributorId = inputForm.getInt("distributorName", false, -1);
				while(distributors.next()) {
				%><option value="<%=distributors.getFormattedValue("DISTRIBUTOR_ID")%>"<%if (distributorId > 0 && distributorId == distributors.getValue("DISTRIBUTOR_ID",int.class)) out.write(" selected=\"selected\"");%>><%=distributors.getFormattedValue("DISTRIBUTOR_NAME")%></option>		
				<%}}%> 
			</select>
		</td>
	
	</tr>
	<tr>
		<td class="label">Activation Reason/Details Additional Info</td>
		<td colspan="4"><input id="activateDetail" class="cssText" type="text" size="100" value="<%=inputForm.getStringSafely("activateDetail", "")%>" name="activateDetail"/></td>
	</tr>
	
	<tr>
		<td class="label">Bank Account<font color="red">*</font></td>
		<td colspan="4">
			<select name="customer_bank_id" id="customer_bank_id" onchange="getData('type=customer_dealers&customerBankId=' + this.value)">
			<option value="0">Please select:</option>
			<%
			if (customerBankId.length() > 0) {
				params.clear();
				params.put("search", inputForm.getString("bank_search", false));
			Results bankAccounts = DataLayerMgr.executeQuery("GET_ALL_BANK_ACCOUNTS", params);
			while(bankAccounts.next()) {
			%>
			<option value="<%=bankAccounts.getValue("aValue")%>"<%if (customerBankId.equals(bankAccounts.getFormattedValue("aValue"))) out.write(" selected=\"selected\"");%>><%=bankAccounts.getFormattedValue("aLabel")%></option>
			<%}}%>
			</select>
			<input type="text" name="bank_search" id="bank_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("bank_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'bank_search_link')" />
			<a id="bank_search_link" href="javascript:getData('type=bank_account&defaultValue=0&defaultName=Please select:&selected=<%=customerBankId%>&search=' + document.getElementById('bank_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>
	<tr>
		<td class="label">Reseller Bank Account<div id="resellerRequired" style="display:none"><font color="red">*</font></div></td>
		<td colspan="4">
			<select name="commission_bank_id" id="commission_bank_id">
				<option value="0">Please select:</option>
				<%
				String commissionBankId = inputForm.getStringSafely("commission_bank_id", "").trim();
				if (commissionBankId.length() > 0) {
					Results bankAccounts = DataLayerMgr.executeQuery("GET_PARENT_BANK_ACCOUNTS", new Object[] {customerBankId});
					while(bankAccounts.next()) {
					%>
					<option value="<%=bankAccounts.getValue("aValue")%>"<%if (commissionBankId.equals(bankAccounts.getFormattedValue("aValue"))) out.write(" selected=\"selected\"");%>><%=bankAccounts.getFormattedValue("aLabel")%></option>
				<%}}%>
			</select>
			<a href="javascript:getData('type=commission_bank_account&defaultValue=0&defaultName=Please select:&selected=<%=commissionBankId%>&customer_bank_id='+main_form.customer_bank_id[main_form.customer_bank_id.selectedIndex].value);"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><hr /></td>
	</tr>
	
	<tr>
		<td class="label">Keep Existing Data</td>
		<td><input type="checkbox" name="keep_existing_data" id="keep_existing_data" value="Y"<%if (inputForm.getStringSafely("keep_existing_data", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
		<td class="label">Override Payment Schedule</td>
		<td><input type="checkbox" name="override_payment_schedule" id="override_payment_schedule" value="Y"<%if (inputForm.getStringSafely("override_payment_schedule", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>

	<tr>
		<td class="label">Asset Number</td>
		<td><input type="text" name="asset_number" id="asset_number" maxlength="50" style="width: 240px;" value="<%=inputForm.getStringSafely("asset_number", "TBD")%>" /></td>
		<td class="label">Telephone</td>
		<td><input type="text" name="telephone" id="telephone" maxlength="15" style="width: 240px;" value="<%=inputForm.getStringSafely("telephone", "")%>" /></td>
	</tr>
	
	<tr>
		<td class="label">Location Name<font color="red">*</font></td>
		<td><input type="text" name="location_name" id="location_name" maxlength="50" style="width: 240px;" value="<%=inputForm.getStringSafely("location_name", "TBD")%>" /></td>
		<td class="label">Postal<font color="red">*</font></td>
		<td><input type="text" name="postal" id="postal" maxlength="20" style="width: 240px;" value="<%=inputForm.getStringSafely("postal", "19355")%>" onblur="getPostalInfo('postalCd=' + this.value);" /></td>
	</tr>
	
	<tr>
		<td class="label">Address</td>
		<td><input type="text" name="address" id="address" maxlength="255" style="width: 240px;" value="<%=inputForm.getStringSafely("address", "")%>" /></td>
		<td class="label">City<font color="red">*</font></td>
		<td><input type="text" name="city" id="city" maxlength="50" style="width: 240px;" value="<%=inputForm.getStringSafely("city", "Malvern")%>" /></td>
	</tr>
	
	<tr>
		<td class="label">State</td>
		<td>
			<select name="state" id="state">
			<%if (states != null){
			String stateCd = inputForm.getStringSafely("state", "PA").trim();
			while(states.next()) {
			%><option value="<%=states.getFormattedValue("STATE_CD")%>"<%if (stateCd.length() > 0 && stateCd.equals(states.getFormattedValue("STATE_CD"))) out.write(" selected=\"selected\"");%>><%=states.getFormattedValue("STATE_NAME")%>: <%=states.getFormattedValue("STATE_CD")%>, <%=states.getFormattedValue("COUNTRY_CD")%></option>
			<%}}%>
			</select>
		</td>
		<td class="label">Country</td>
		<td>
			<select name="country" id="country">
			<%if (countries != null){
			String countryCd = inputForm.getStringSafely("country", "").trim();
			while(countries.next()) {
			%><option value="<%=countries.getFormattedValue("COUNTRY_CD")%>"<%if (countryCd.length() > 0 && countryCd.equals(countries.getFormattedValue("COUNTRY_CD"))) out.write(" selected=\"selected\"");%>><%=countries.getFormattedValue("COUNTRY_NAME")%>: <%=countries.getFormattedValue("COUNTRY_CD")%></option>
			<%}}%>
			</select>
		</td>
	</tr>
	
	<tr>
		<td class="label">Location Details</td>
		<td><input type="text" name="location_details" id="location_details" maxlength="255" style="width: 240px;" value="<%=inputForm.getStringSafely("location_details", "")%>" /></td>
		<td class="label">Location Type</td>
		<td>
			<select name="location_type" id="location_type">
			<%if (locationTypes != null){
			String locationType = inputForm.getStringSafely("location_type", "").trim();
			while(locationTypes.next()) {
			%><option value="<%=locationTypes.getFormattedValue("LOCATION_TYPE_NAME")%>"<%if (locationType.length() > 0 && locationType.equals(locationTypes.getFormattedValue("LOCATION_TYPE_NAME"))) out.write(" selected=\"selected\"");%>><%=locationTypes.getFormattedValue("LOCATION_TYPE_NAME")%><%if ("Y".equalsIgnoreCase(locationTypes.getFormattedValue("SPECIFY"))) out.write(" (specify)");%></option>
			<%}}%>
			</select>
		</td>
	</tr>
	
	<tr>
		<td class="label">Location Type Specific</td>
		<td><input type="text" name="location_type_specific" id="location_type_specific" maxlength="50" style="width: 240px;" value="<%=inputForm.getStringSafely("location_type_specific", "")%>" /></td>
		<td class="label">Time Zone</td>
		<td>
			<select name="time_zone_id" id="time_zone_id">
			<%if (timeZones != null){
			String timeZoneId = inputForm.getStringSafely("time_zone_id", "").trim();
			while(timeZones.next()) {
			%><option value="<%=timeZones.getValue("TIME_ZONE_ID")%>"<%if (timeZoneId.length() > 0 && timeZoneId.equals(timeZones.getFormattedValue("TIME_ZONE_ID"))) out.write(" selected=\"selected\"");%>><%=timeZones.getFormattedValue("ABBREV")%>: <%=timeZones.getFormattedValue("NAME")%></option>
			<%}}%>
			</select>
		</td>
	</tr>
	
	<tr>
		<td class="label">Machine Make<font color="red">*</font></td>
		<td><input type="text" name="machine_make" id="machine_make" maxlength="20" style="width: 240px;" value="<%=inputForm.getStringSafely("machine_make", "TBD")%>" /></td>
		<td class="label">Machine Model<font color="red">*</font></td>
		<td><input type="text" name="machine_model" id="machine_model" maxlength="20" style="width: 240px;" value="<%=inputForm.getStringSafely("machine_model", "To Be Determined")%>" /></td>
	</tr>
	
	<tr>
		<td class="label">Product Type</td>
		<td>
			<select name="product_type" id="product_type">
			<%if (productTypes != null){
			String productType = inputForm.getStringSafely("product_type", "").trim();
			while(productTypes.next()) {
			%><option value="<%=productTypes.getFormattedValue("PRODUCT_TYPE_NAME")%>"<%if (productType.length() > 0 && productType.equals(productTypes.getFormattedValue("PRODUCT_TYPE_NAME"))) out.write(" selected=\"selected\"");%>><%=productTypes.getFormattedValue("PRODUCT_TYPE_NAME")%><%if ("Y".equalsIgnoreCase(productTypes.getFormattedValue("SPECIFY"))) out.write(" (specify)");%></option>
			<%}}%>
			</select>
		</td>
		<td class="label">Product Type Specific</td>
		<td><input type="text" name="product_type_specific" id="product_type_specific" maxlength="50" style="width: 240px;" value="<%=inputForm.getStringSafely("product_type_specific", "")%>" /></td>
	</tr><%
		Character terminalActivator;
		Character mobileIndicator;
		terminalActivator = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"terminal_activator", false), '?');
		mobileIndicator = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"mobile_indicator", false), '?');
		Set<Character> entryList = ConvertUtils.convert(Set.class,  Character.class, RequestUtils.getAttribute(request,"entry_list", false));
		if(entryList == null)
			entryList = Collections.emptySet();
		%>
	<tr><td class="label">Machine is Tablet / Phone</td><td><fieldset class="border">
		<label><input type="radio" name="mobile_indicator" value="Y"<%if(mobileIndicator == 'Y') {%> checked="checked"<%} %>/>Yes</label>
		<label><input type="radio" name="mobile_indicator" value="N"<%if(mobileIndicator == 'N') {%> checked="checked"<%} %>/>No</label>
		</fieldset></td>
		<td class="label">Card Data Entered By</td><td><fieldset class="border">
		<label><input type="radio" name="terminal_activator" value="C"<%if(terminalActivator == 'C') {%> checked="checked"<%} %>/>Card Holder</label>
		<label><input type="radio" name="terminal_activator" value="A"<%if(terminalActivator == 'A') {%> checked="checked"<%} %>/>Attendant (Cashier)</label>
		</fieldset></td></tr>
	<tr><td class="label">Card Entry Types</td><td colspan="3"><fieldset class="border">
		<label><input type="checkbox" name="entry_list" value="M"<%if(entryList.contains('M')) {%> checked="checked"<%} %>/>Keypad or Keyboard</label>
		<label><input type="checkbox" name="entry_list" value="S"<%if(entryList.contains('S')) {%> checked="checked"<%} %>/>Card Swipe Reader</label>
		<label><input type="checkbox" name="entry_list" value="P"<%if(entryList.contains('P')) {%> checked="checked"<%} %>/>Proximity Reader (Contactless)</label>
		<label><input type="checkbox" name="entry_list" value="E"<%if(entryList.contains('E')) {%> checked="checked"<%} %>/>EMV</label>
		</fieldset></td></tr>
	<tr>
		<td class="label">Signature Capture</td><td><fieldset class="border">
		<label><input type="radio" name="entry_list" value="X"<%if(entryList.contains('X')) {%> checked="checked"<%} %>/>Electronic</label>
		<label><input type="radio" name="entry_list" value=""<%if(!entryList.contains('X')) {%> checked="checked"<%} %>/>Manual</label>
		</fieldset></td>
		<td class="label">Business Type<font color="red">*</font></td>
		<td>
			<select name="business_type" id="business_type">
				<option value="">Please select:</option>
			<%if (businessTypes != null){
			String businessType = inputForm.getStringSafely("business_type", "").trim();
			while(businessTypes.next()) {
			%><option value="<%=businessTypes.getFormattedValue("BUSINESS_TYPE_NAME")%>"<%if (businessType.length() > 0 && businessType.equals(businessTypes.getFormattedValue("BUSINESS_TYPE_NAME"))) out.write(" selected=\"selected\"");%>><%=businessTypes.getFormattedValue("BUSINESS_TYPE_NAME")%></option>		
			<%}}%>  
			</select>
		</td>
	</tr>
	<tr>
		<td class="label">Region</td>
		<td><input type="text" name="region" id="region" maxlength="50" style="width: 240px;" value="<%=inputForm.getStringSafely("region", "")%>" /></td>
		<td class="label">Payment Schedule</td>
		<td>
			<select name="payment_schedule_id" id="payment_schedule_id">
			<option value=""></option>
			<%if (paymentSchedules != null){ 
			String paymentScheduleId = inputForm.getStringSafely("payment_schedule_id", "").trim();
			while(paymentSchedules.next()) {
			%><option value="<%=paymentSchedules.getValue("PAYMENT_SCHEDULE_ID")%>"<%if (paymentScheduleId.length() > 0 && paymentScheduleId.equals(paymentSchedules.getFormattedValue("PAYMENT_SCHEDULE_ID"))) out.write(" selected=\"selected\"");%>><%=paymentSchedules.getFormattedValue("DESCRIPTION")%></option>
			<%}}%>
			</select>
		</td>
	</tr>
	
	<tr>
		<td class="label">Custom Field 1</td>
		<td><input type="text" name="custom_1" id="custom_1" maxlength="4000" style="width: 240px;" value="<%=inputForm.getStringSafely("custom_1", "")%>" /></td>
		<td class="label">Custom Field 2</td>
		<td><input type="text" name="custom_2" id="custom_2" maxlength="4000" style="width: 240px;" value="<%=inputForm.getStringSafely("custom_2", "")%>" /></td>
	</tr>
	
	<tr>
		<td class="label">Custom Field 3</td>
		<td><input type="text" name="custom_3" id="custom_3" maxlength="4000" style="width: 240px;" value="<%=inputForm.getStringSafely("custom_3", "")%>" /></td>
		<td class="label">Custom Field 4</td>
		<td><input type="text" name="custom_4" id="custom_4" maxlength="4000" style="width: 240px;" value="<%=inputForm.getStringSafely("custom_4", "")%>" /></td>
	</tr>
	
	<tr>
		<td class="label">Custom Field 5</td>
		<td><input type="text" name="custom_5" id="custom_5" maxlength="4000" style="width: 240px;" value="<%=inputForm.getStringSafely("custom_5", "")%>" /></td>
		<td class="label">Custom Field 6</td>
		<td><input type="text" name="custom_6" id="custom_6" maxlength="4000" style="width: 240px;" value="<%=inputForm.getStringSafely("custom_6", "")%>" /></td>
	</tr>
	
	<tr>
		<td class="label">Custom Field 7</td>
		<td><input type="text" name="custom_7" id="custom_7" maxlength="4000" style="width: 240px;" value="<%=inputForm.getStringSafely("custom_7", "")%>" /></td>
		<td class="label">Custom Field 8</td>
		<td><input type="text" name="custom_8" id="custom_8" maxlength="4000" style="width: 240px;" value="<%=inputForm.getStringSafely("custom_8", "")%>" /></td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td colspan="3"><hr /></td>
	</tr>
	
	<tr>
		<td class="label">Effective Date<font color="red">*</font></td>
		<td>
			<input type="text" name="effective_date" id="effective_date" maxlength="10" value="<%=inputForm.getStringSafely("effective_date", Helper.getCurrentDate())%>" />
			<img src="/images/calendar.gif" id="effective_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Effective Time</td>
		<td><input type="text" name="effective_time" id="effective_time" maxlength="8" value="<%=inputForm.getStringSafely("effective_time", Helper.getCurrentTime())%>" /></td>
	</tr>
	
	<tr>
		<td class="label">Grace Days</td>
		<td><input type="text" name="fee_grace_days" id="fee_grace_days" maxlength="3" value="<%=inputForm.getStringSafely("fee_grace_days", "0")%>" /></td>
		<td class="label">No Rental Fee Trigger</td>
		<td><input type="checkbox" name="fee_no_trigger_event_flag" id="fee_no_trigger_event_flag" value="Y"<%if (inputForm.getStringSafely("fee_no_trigger_event_flag", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> title="Check to disable charging the rental fee on the first transaction or DEX file" /></td>
	</tr>
	
	<tr>
		<td class="label">Sales Order Number</td>
		<td><input type="text" name="sales_order_number" id="sales_order_number" maxlength="50" style="width: 240px;" value="<%=inputForm.getStringSafely("sales_order_number", "")%>" /></td>
		<td class="label">Purchase Order Number</td>
		<td><input type="text" name="purchase_order_number" id="purchase_order_number" maxlength="50" style="width: 240px;" value="<%=inputForm.getStringSafely("purchase_order_number", "")%>" /></td>
	</tr>
	
	<tr>
		<td class="label">Override Doing Business As</td>
		<td><input type="text" name="doing_business_as" id="doing_business_as" maxlength="21" style="width: 240px;" value="<%=inputForm.getStringSafely("doing_business_as", "")%>" /></td>
		<td class="label">Override Customer Service Phone</td>
		<td><input type="tel" name="customer_service_phone" id="customer_service_phone" maxlength="20" style="width: 240px;" value="<%=inputForm.getStringSafely("customer_service_phone", "")%>" /></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td class="label">Override Customer Service Email</td>
		<td><input type="email" name="customer_service_email" id="customer_service_email" maxlength="70" style="width: 240px;" value="<%=inputForm.getStringSafely("customer_service_email", "")%>" /></td>
	</tr>

	<tr>
		<td class="label">Sales Rep</td>
		<td class="data">
			<% long actSalesRepId = inputForm.getLong("act_salesRepId", false, 0); %>
			<select name="act_salesRepId" id="act_salesRepId" onchange="actSalesRepChanged();">
				<option value="0"></option>
				<% for(SalesRep salesRep : salesReps) { %>
				<option value="<%=salesRep.getSalesRepId()%>" <%=salesRep.getSalesRepId() == actSalesRepId ? "selected=\"selected\"" : ""%>><%=salesRep.getName() %></option>
				<% } %>
			</select>
		</td>
	</tr>
	<tr>
		<td class="label">Commission Effect</td>
		<td class="data" colspan="3">
			<select name="act_eventTypeCode" id="act_eventTypeCode" style="font-family: 'Courier New', Courier, monospace">
				<% String actEventTypeCode = inputForm.getStringSafely("act_eventTypeCode", ""); %>
				<option value="0"></option>
				<% for(CommissionEventType eventType : eventTypes) { if (eventType.isAct()) {%>
				<option style="font-family: 'Courier New', Courier, monospace" value="<%=eventType.getCode()%>" <%=actEventTypeCode.equals(eventType.getCode().toString()) ? "selected=\"selected\"" : "" %>><%=eventType.getFixedWidthDesc()%></option>
				<% } } %>
			</select>
		</td>
	</tr>

	<tr>
		<td colspan="4"><hr /></td>
	</tr>

	<tr>
		<td class="label">Change Credential (K3 Kiosks)</td>
		<td colspan="4"><input type="checkbox" name="change_credential" id="change_credential" value="Y"<%if (inputForm.getStringSafely("change_credential", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	
	<tr>
		<td class="label">Quick Connect Credential</td>
		<td>
			<select id="credential_id" name="credential_id">
					<option value="0">No Credential</option>
					<%
					String credentialId = inputForm.getStringSafely("credential_id", "").trim();
					Results credentials = DataLayerMgr.executeQuery("GET_CREDENTIALS", null);
					while(credentials.next()) {
					%>
					<option value="<%=credentials.getFormattedValue("credentialId")%>"<%if (credentialId.length() > 0 && credentialId.equals(credentials.getFormattedValue("credentialId"))) out.write(" selected=\"selected\"");%>><%=credentials.getFormattedValue("credentialName")%></option>
					<%}%>
			</select>
			<input type="text" name="credential_search" id="credential_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("credential_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'credential_search_link')" />
			<a id="credential_search_link" href="javascript:getData('type=credential&id=credential_id&name=credential_id&noDataValue=0&noDataName=No+Credential&search=' + document.getElementById('credential_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
			
	<tr>
		<td class="label">Clone Serial Number</td>
		<td colspan="3">
			<input type="checkbox" name="clone" id="clone" value="Y"<%if (inputForm.getStringSafely("clone", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> />
			<input type="text" name="clone_serial_number" id="clone_serial_number" value="<%=inputForm.getStringSafely("clone_serial_number", "")%>" />
		</td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
	
	<tr>
		<td class="label" valign="top">Import Configuration Template</td>
		<td colspan="3">
			<input type="checkbox" name="import_config_template" id="import_config_template" value="Y"<%if (inputForm.getStringSafely("import_config_template", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> />
			<select name="config_template_id" id="config_template_id">
				<option value="">Please select:</option>
				<%
				String configTemplateId = inputForm.getStringSafely("config_template_id", "");
				Results configTemplates = DataLayerMgr.executeQuery("GET_CUSTOM_CONFIG_TEMPLATE_EDIT_LIST", null);				
				while(configTemplates.next()) {
				%><option value="<%=configTemplates.getFormattedValue(1)%>"<%if (configTemplateId.length() > 0 && configTemplateId.equals(configTemplates.getFormattedValue(1))) out.write(" selected=\"selected\"");%>><%=configTemplates.getFormattedValue(2)%></option>
				<%}%>
			</select>
			<input type="text" name="config_template_search" id="config_template_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("config_template_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'config_template_search_link')" />
			<a id="config_template_search_link" href="javascript:getData('type=config_template&subType=editor&id=config_template_id&name=config_template_id&search=' + document.getElementById('config_template_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>
		
	<tr>
		<td colspan="4"><hr /></td>
	</tr>
	
	<tr>
		<td class="label">Change Two-Tier Pricing</td>
		<td colspan="3">
			<input type="checkbox" name="change_two_tier_pricing" id="change_two_tier_pricing" value="Y"<%if (inputForm.getStringSafely("change_two_tier_pricing", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> />
			<input type="text" name="two_tier_pricing_value" id="two_tier_pricing_value" value="<%=inputForm.getStringSafely("two_tier_pricing_value", "")%>" label="Two-Tier Pricing" valid="^(?:|[0-9]*\.[0-9]{1,2})$" maxlength="5" size="10" />
		</td>
	</tr>
	
	<tr>
		<td colspan="4"><hr /></td>
	</tr>	
	
	<tr>
		<td class="label" valign="top">Import Payment Template</td>
		<td colspan="3">
			<input type="checkbox" name="import_payment_template" id="import_payment_template" value="Y"<%if (inputForm.getStringSafely("import_payment_template", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> />
			<select id="pos_pta_tmpl_id" name="pos_pta_tmpl_id">
				<option value="">Please select:</option>
				<%
				String paymentTemplateId = inputForm.getStringSafely("pos_pta_tmpl_id", "");
				Results paymentTemplates = DataLayerMgr.executeQuery("GET_PTA_TEMPLATE_LIST", null);
				while (paymentTemplates.next()) {%>
				<option value="<%= paymentTemplates.getFormattedValue(1) %>"<%if (paymentTemplateId.length() > 0 && paymentTemplateId.equals(paymentTemplates.getFormattedValue(1))) out.write(" selected=\"selected\"");%>><%= paymentTemplates.getFormattedValue(2)%></option>
				<%} %>
			</select>
			<input type="text" name="payment_template_search" id="payment_template_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("payment_template_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'payment_template_search_link')" />
			<a id="payment_template_search_link" href="javascript:getData('type=payment_template&id=pos_pta_tmpl_id&name=pos_pta_tmpl_id&search=' + document.getElementById('payment_template_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			<br /><br />
			<table class="tabDataDisplay">
				<tr>
					<td class="label" width="30%">Import Mode</td>
					<td class="data">
						<%String modeCd = inputForm.getStringSafely("mode_cd", "S");%>
						<input type="radio" name="mode_cd" value="S"<%if (modeCd.equalsIgnoreCase("S")) out.write(" checked=\"checked\"");%> /> 1. Safe<br/>
						<input type="radio" name="mode_cd" value="MS"<%if (modeCd.equalsIgnoreCase("MS")) out.write(" checked=\"checked\"");%> /> 2. Merge Safe<br/>
						<input type="radio" name="mode_cd" value="MO"<%if (modeCd.equalsIgnoreCase("MO")) out.write(" checked=\"checked\"");%> /> 3. Merge Overwrite<br/>
						<input type="radio" name="mode_cd" value="O"<%if (modeCd.equalsIgnoreCase("O")) out.write(" checked=\"checked\"");%> /> 4. Overwrite<br/>
						<input type="radio" name="mode_cd" value="CO"<%if (modeCd.equalsIgnoreCase("CO")) out.write(" checked=\"checked\"");%> /> 5. Complete Overwrite<br/>
					</td>
				</tr>
				<tr>
					<td class="label">Order Mode</td>
					<td class="data">
						<%String orderCd = inputForm.getStringSafely("order_cd", "");%>
						<select name="order_cd">
							<option value="AE">Above Error Bin</option>
							<option value="T"<%if (orderCd.equalsIgnoreCase("T")) out.write(" selected=\"selected\"");%>>Top</option>
							<option value="B"<%if (orderCd.equalsIgnoreCase("B")) out.write(" selected=\"selected\"");%>>Bottom</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="label nowrap">Set Terminal Code to Device Serial #</td>
					<td><input type="checkbox" name="set_terminal_cd_to_serial" value="Y"<%if (inputForm.getStringSafely("set_terminal_cd_to_serial", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
				</tr>
				<tr>
					<td class="label nowrap">Only Import "No Two-Tier Pricing" Setting</td>
					<td><input type="checkbox" name="only_no_two_tier_pricing" value="Y"<%if (inputForm.getStringSafely("only_no_two_tier_pricing", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
				</tr>
				<tr>
					<td colspan="2">
					<b>Import Mode</b>
					<ul>
						<li><b>Safe Mode</b>
						<ul><li>
							Will not deactivate or alter any existing payment types.
							</li><li>
							May create new payment types if none exist for each <b>Payment Action Type / Payment Entry Method category</b> based on the selected template.
							</li></ul>
						</li>
						<li><b>Merge Safe Mode</b>
						<ul><li>
							Will not deactivate or alter any existing payment types.
							</li><li>
							May create new payment types if none exist for each <b>Payment Type category</b> based on the selected template.
							</li></ul>
						</li>
						<li><b>Merge Overwrite Mode</b><br/>
							<ul><li>
							May deactivate an existing payment type if one exists for each <b>Payment Type category</b> being imported.
							</li><li>
							Will create new payment types based on the selected template.
							</li></ul>
						</li>
						<li><b>Overwrite Mode</b><br/>
							<ul><li>
							Will deactivate any existing payment types for each <b>Payment Action Type / Payment Entry Method category</b> being imported.
							</li><li>
							Will create new payment types based on the selected template.
							</li></ul>
						</li>
						<li><b>Complete Overwrite Mode</b><br/>
							<ul><li>
							Will deactivate <b>any</b> existing payment types.
							</li><li>
							Will create new payment types based on the selected template.
							</li></ul>
						</li>
					</ul>
					<b>Order Mode</b>
					<table class="noBorder">
						<tr><td>&nbsp;&nbsp;</td><td><b>Above Error Bin:</b></td><td>Inserts the new payment types above the first Error Bin and below all the other payment types.</td></tr>
						<tr><td>&nbsp;&nbsp;</td><td><b>Top:</b></td><td>Inserts the new payment types above all the existing payment types.</td></tr>
						<tr><td>&nbsp;&nbsp;</td><td><b>Bottom:</b></td><td>Adds the new payment types below all the existing payment types.</td></tr>
					</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
		
	<tr>
		<td colspan="4" align="center">
		<input type="submit" value="Submit" class="cssButton" />
		</td>
	</tr>
</table>

</form>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
Calendar.setup({
	inputField	:	"terminal_end_date",					// id of the input field
	ifFormat		:	"%m/%d/%Y",										// format of the input field
	button			:	"terminal_end_date_trigger",	// trigger for the calendar (button ID)
	align				:	"B2",													// alignment (defaults to "Bl")
	singleClick	:	true,
	onUpdate		:	"swap_dates"
});

Calendar.setup({
	inputField	:	"service_fee_end_date",					// id of the input field
	ifFormat		:	"%m/%d/%Y",											// format of the input field
	button			:	"service_fee_end_date_trigger",	// trigger for the calendar (button ID)
	align				:	"B2",														// alignment (defaults to "Bl")
	singleClick	:	true,
	onUpdate		:	"swap_dates"
});

Calendar.setup({
	inputField	:	"cashless_end_date",					// id of the input field
	ifFormat		:	"%m/%d/%Y",										// format of the input field
	button			:	"cashless_end_date_trigger",	// trigger for the calendar (button ID)
	align				:	"B2",													// alignment (defaults to "Bl")
	singleClick	:	true,
	onUpdate		:	"swap_dates"
});

Calendar.setup({
	inputField	:	"effective_date",					// id of the input field
	ifFormat		:	"%m/%d/%Y",								// format of the input field
	button			:	"effective_date_trigger",	// trigger for the calendar (button ID)
	align				:	"B2",											// alignment (defaults to "Bl")
	singleClick	:	true,
	onUpdate		:	"swap_dates"
});

Calendar.setup({
	inputField	:	"swap_effective_date",					// id of the input field
	ifFormat		:	"%m/%d/%Y",										// format of the input field
	button			:	"swap_effective_date_trigger",	// trigger for the calendar (button ID)
	align				:	"B2",													// alignment (defaults to "Bl")
	singleClick	:	true,
	onUpdate		:	"swap_dates"
});

function checkSwapping(){
	if(document.getElementById("swap_devices").checked){
		document.getElementById("activate").checked = false;
		document.getElementById("activate").disabled = true;
		document.getElementById("clone").checked = false;
		document.getElementById("clone").disabled = true;
	} else{
		document.getElementById("activate").disabled = false;
		document.getElementById("clone").disabled = false;
	}
}

function doSubmit() {
	var dev_list = document.getElementById("dev_list").value.trim();
	if(dev_list == ''){
		alert('Please enter Serial Numbers');
		return false;
	}
	var dev_list_to_swap = document.getElementById("dev_list_to_swap").value.trim();
	if(dev_list_to_swap == '' && document.getElementById("swap_devices").checked){
		alert('Please enter Source Serial Numbers for Swapping');
		return false;
	}
	if (document.getElementById("deactivate").checked) {
		if (document.getElementById("deactivateDetailId").value == '0') {
			alert('Please select Deactivation Reason/Details');
			document.getElementById("deactivateDetailId").focus();
			return false;
		}
		var terminal_end_date = document.getElementById("terminal_end_date");
		if(!isDate(terminal_end_date.value)){
			terminal_end_date.focus();
			return false;
		}
		if($('deact_salesRepId').value != 0 && $('deact_eventTypeCode').value == 0) {
			alert('Commission Effect is required with Sales Rep');
			return false;
		}
		if($('deact_salesRepId').value == 0 && $('deact_eventTypeCode').value != 0) {
			alert('Sales Rep is required with Commission Effect');
			return false;
		}
	}
	if (document.getElementById("clear_service_fees").checked) {
		var service_fee_end_date = document.getElementById("service_fee_end_date");
		if(!isDate(service_fee_end_date.value)){
			service_fee_end_date.focus();
			return false;
		}
	}
	if (document.getElementById("activate").checked) {
		if (document.getElementById("activateDetailId").value == '0') {
			alert('Please select Activation Reason/Details');
			document.getElementById("activateDetailId").focus();
			return false;
		}
		if(document.getElementById("activateDetailId").value == '3' && document.getElementById("distributorName").value == '0'){
			alert('Please select Distributor Name');
			document.getElementById("distributorName").focus();
			return false;
		}		
		if(document.getElementById("customer_bank_id").value == '0'){
			alert('Please select Bank Account');
			return false;
		}
		if(document.getElementById("location_name").value.trim() == ''){
			alert('Please enter Location Name');
			return false;
		}
		if(document.getElementById("city").value.trim() == ''){
			alert('Please enter City');
			return false;
		}
		if(document.getElementById("postal").value.trim() == ''){
			alert('Please enter Postal');
			return false;
		}
		if(document.getElementById("machine_make").value.trim() == ''){
			alert('Please enter Machine Make');
			return false;
		}
		if(document.getElementById("machine_model").value.trim() == ''){
			alert('Please enter Machine Model');
			return false;
		}
		if(document.getElementById("business_type").value.trim() == ''){
			alert('Please select Business Type');
			return false;
		}
		var effective_date = document.getElementById("effective_date");
		if(!isDate(effective_date.value)){
			effective_date.focus();
			return false;
		}
		var fee_grace_days = document.getElementById("fee_grace_days").value.trim();
		if(fee_grace_days == '' || isNaN(fee_grace_days) || fee_grace_days < 0 || fee_grace_days > 180){
			alert('Grace Days must be a number between 0 and 180');
			return false;
		}
		var re = /^K3/m;
		if(re.test(dev_list)) {
			var form = document.getElementById("main_form");
			if(!isOneChecked(form.mobile_indicator)) {
				alert('Must select whether machines are tablets/phones or not');
				form.mobile_indicator[0].focus();
				return false;
			}
			if(!isOneChecked(form.terminal_activator)) {
				alert('Must select who enters the Card Data');
				form.terminal_activator[0].focus();
				return false;
			}
			if(!isOneChecked(form.entry_list)) {
				alert('Must select at least one Card Entry Type');
				form.entry_list[0].focus();
				return false;
			}
		}
		if($('act_salesRepId').value != 0 && $('act_eventTypeCode').value == 0) {
			alert('Commission Effect is required with Sales Rep');
			return false;
		}
		if($('act_salesRepId').value == 0 && $('act_eventTypeCode').value != 0) {
			alert('Sales Rep is required with Commission Effect');
			return false;
		}
	}

	if (document.getElementById("clone").checked) {
		if(document.getElementById("clone_serial_number").value.trim() == ''){
			alert('Please enter Clone Serial Number');
			return false;
		}
	}
	
	if (document.getElementById("import_config_template").checked) {
		if(document.getElementById("config_template_id").value == ''){
			alert('Please select a Configuration Template');
			return false;
		}
	}
	
	if (document.getElementById("change_two_tier_pricing").checked) {
		if(document.getElementById("two_tier_pricing_value").value == ''){
			alert('Please enter Two-Tier Pricing');
			document.getElementById("two_tier_pricing_value").focus();
			return false;
		}
	}
	
	if (document.getElementById("import_payment_template").checked) {
		if(document.getElementById("pos_pta_tmpl_id").value == ''){
			alert('Please select a Payment Template');
			return false;
		}
	}
	
	return true;
}

function isOneChecked(elements) {
	if(!elements)
		return false;
	for(var i = 0; i < elements.length; i++)
		if(elements[i].checked)
			return true;
	return false;
}

function deactSalesRepChanged() {
	var salesRepId = $('deact_salesRepId').value;
	if (salesRepId == -999) {
		$('deactFallback').setStyle('display', 'inline-block');
	} else {
		$('deactFallback').setStyle('display', 'none');
	}
	if (salesRepId == 0) {
		$('deact_eventTypeCode').value = 0;
	} else if (!$('deact_eventTypeCode').value == 0) {
		$('deact_eventTypeCode').value = 'DEACT';
	}
}

function actSalesRepChanged() {
	var salesRepId = $('act_salesRepId').value;
	if (salesRepId == 0) {
		$('act_eventTypeCode').value = 0;
	} else if ($('act_eventTypeCode').value == 0) {
		$('act_eventTypeCode').value = 'ACT';
	}
}

function updateResellerRequired() {
  var selectBox = document.getElementById("dealerId");
  var selectedText = selectBox.options[selectBox.selectedIndex].text;
  if (/\(SELL RATE\)$/.test(selectedText)) {
    $('resellerRequired').setStyle('display', 'inline-block');
  } else {
    $('resellerRequired').setStyle('display', 'none');
  }
}

function onActivationReasonChange() {
	if ($('activateDetailId').value == 3) {
		$('distributorRequired').setStyle('display','inline');
	} else {
		$('distributorRequired').setStyle('display','none');
	}
}

</script>
