<%@page import="simple.lang.InvalidValueException"%>
<%@page import="com.usatech.layers.common.constants.EntryCapability"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.dms.action.DeviceActions"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%
Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
String servletPath = request.getServletPath();
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "device");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, device.getDeviceName());

String statusMessage = RequestUtils.getAttribute(request, "statusMessage", String.class, false);
if(!StringUtils.isBlank(statusMessage)) {
	statusMessage = StringUtils.prepareCDATA(statusMessage);
	String statusType = RequestUtils.getAttribute(request, "statusType", String.class, false);
	if(StringUtils.isBlank(statusType))
		statusType = "info"; %>
<div id="status" class="status-<%=statusType %>"><%=statusMessage %></div>
<br/>
<% } %>
<ul class="shadeTabs">
<li>&nbsp;</li>
<li><a href="profile.i?device_id=<%=device.getId()%>"<%if("/profile.i".equalsIgnoreCase(servletPath)) {%> class="selected"<%}%>>Device Profile</a></li>
<li><a href="paymentConfig.i?device_id=<%=device.getId()%>"<%if("/paymentConfig.i".equalsIgnoreCase(servletPath)) {%> class="selected"<%}%>>Payment Configuration</a></li>
<li><a href="deviceConfig.i?device_id=<%=device.getId()%>"<%if("/deviceConfig.i".equalsIgnoreCase(servletPath)) {%> class="selected"<%}%>>Device Configuration</a></li>
<li><a href="deviceSettings.i?device_id=<%=device.getId()%>"<%if("/deviceSettings.i".equalsIgnoreCase(servletPath)) {%> class="selected"<%}%>>Device Settings</a></li>
<li><a href="fileTransfer.i?device_id=<%=device.getId()%>"<%if("/fileTransfer.i".equalsIgnoreCase(servletPath)) {%> class="selected"<%}%>>File Transfer</a></li>
</ul>

<div class="tableDataContainer tabContent" style="width: 99%">
	<div class="headerTab">
		<table width="100%">
			<tr>
				<td class="txtWhiteBold" align="left">&nbsp;<%=StringUtils.prepareHTML(device.getTypeDesc())%> -- <%=StringUtils.prepareHTML(device.getDeviceName())%> -- <%=StringUtils.prepareHTML(device.getSerialNumber())%><%if (!StringHelper.isBlank(device.getCommMethodName())) { %> -- <%=device.getCommMethodName() %><%} %>
				-- <%=StringUtils.prepareHTML(device.getPosEnvironment().getDescription())%> [<%
				String ecc = device.getEntryCapability();
				if(ecc != null)
					for(int i = 0; i < ecc.length(); i++) {
						if(i > 0) {%>, <%}
						char ch = ecc.charAt(i);
						try { %><%=StringUtils.prepareHTML(EntryCapability.getByValue(ch).getDescription()) %><%
						} catch(InvalidValueException e) {
							%>?<%
						}
					}
				%>]</td>
				<td class="txtWhiteBold" align="right"><%=StringUtils.prepareHTML(device.getCustomerName())%> -- <%=StringUtils.prepareHTML(device.getLocationName())%> -- <%=StringUtils.prepareHTML(device.getLastActivityTs())%>&nbsp;</td>
			</tr>
		</table>
	</div>
