<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/moment.min.js") %>"></script>

<div class="tableContainer">
  <div class="tableContent">
<%  Long posId = RequestUtils.getAttribute(request, "pos_id", Long.class, true);
    Long deviceId = RequestUtils.getAttribute(request, "device_id", Long.class, true);
    String serial = RequestUtils.getAttribute(request, "serial", String.class, true);
    String handlerClassParam = RequestUtils.getAttributeDefault(request, "handler_class", String.class, "Tandem");
    int declineUntilPeriod = RequestUtils.getAttributeDefault(request, "decline_until_period", Integer.class, -1);
    String declineUntilDate = RequestUtils.getAttributeDefault(request, "decline_until_date", String.class, "");
    String declineUntilTime = RequestUtils.getAttributeDefault(request, "decline_until_time", String.class, "");
    String msg = RequestUtils.getAttribute(request, "message", String.class, false);
    String err = RequestUtils.getAttribute(request, "error", String.class, false); %>
    <form method="post" action="blacklist.i" name="form" id="form">
      <%if(!StringUtils.isBlank(msg)) {%>
      <div class="status-info"><%=msg%></div>
      <%} %>
      <%if(!StringUtils.isBlank(err)) {%>
      <div class="status-error"><%=err%></div>
      <%} %>
      <input type="hidden" name="pos_id" value="<%=posId%>" />
      <input type="hidden" name="device_id" value="<%=deviceId%>" />
      <input type="hidden" name="serial" value="<%=StringUtils.prepareCDATA(serial)%>" />
      <div class="gridHeader" class="gridHeader" align="center">Temporary Card Authorization Whitelist/Blacklist for <%=StringUtils.prepareCDATA(serial)%></div>
      <table class="tabDataDisplay">
        <tr>
          <td width="25%" class="label">Action <font color="red">*</font></td>
          <td>
            <input type="radio" name="operation" id="operationY" value="Y" onclick="showHideBlacklist()">Set
            <input type="radio" name="operation" id="operationN" value="N" onclick="showHideBlacklist()">Clear
          </td>
        </tr>
        <tr>
          <td width="25%" class="label">Type <font color="red">*</font></td>
          <td>
            <input type="radio" name="type" id="typeB" value="B" onclick="showHideBlacklist()">Blacklist
            <input type="radio" name="type" id="typeW" value="W" onclick="showHideBlacklist()">Whitelist
          </td>
        </tr>
        <tr>
          <td width="25%" class="label">Processor <font color="red">*</font></td>
          <td>
            <select name="handler_class">
            <% Results handlerResults = DataLayerMgr.executeQuery("GET_HANDLERS", new Object[]{});
               while(handlerResults.next()) {
                 String handlerClass = handlerResults.getFormattedValue("HANDLER_CLASS"); %>
               <option value="<%=handlerClass%>" <%=handlerClass.equalsIgnoreCase(handlerClassParam) ? "selected=\"selected\"" : "" %>"><%=handlerResults.getFormattedValue("HANDLER_NAME")%></option>
            <% } %>
            </select>
          </td>
        </tr>
        <tr id="blacklist_for" style="display:none">
          <td width="25%" class="label">Blacklist For</td>
          <td>
            <select name="decline_until_period" onchange="selectDeclinePeriod(this)">
              <option value=""></option>
            <% Map<Integer, String> periods = (Map<Integer, String>) RequestUtils.getAttribute(request, "periods", true);
              for(Map.Entry<Integer, String> entry : periods.entrySet()) { %>
              <option value="<%=entry.getKey()%>" <%=entry.getKey() == declineUntilPeriod ? "selected" : ""%>><%=entry.getValue()%></option>
            <% } %>
            </select>
          </td>
        </tr>
        <tr id="blacklist_until" style="display: none">
          <td width="25%" class="label">Blacklist Until <font color="red">*</font></td>
          <td>
            <input type="text" name="decline_until_date" id="decline_until_date" value="<%=declineUntilDate%>" size="8" maxlength="10"> 
            <img src="/images/calendar.gif" id="decline_until_calendar" class="calendarIcon" title="Date selector" /> 
            <input type="text" size="6" maxlength="8" id="decline_until_time" name="decline_until_time" value="<%=declineUntilTime%>" /> 
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center" width="100%">
              <input type="submit" id="action" name="action" class="cssButton" value="Submit" onclick="return validate();">
              <input type="button" class="cssButton" name="action" value="Go Back to Device Profile" onClick="javascript:window.location='paymentConfig.i?device_id=<%=deviceId%>&userOP=payment_types';">
          </td>
        </tr>
      </table>
    </form>
  </div>
  <div class="spacer5"></div>
</div>

<script type="text/javascript" defer="defer">
  Calendar.setup({
    inputField : "decline_until_date", // id of the input field
    ifFormat : "%m/%d/%Y", // format of the input field
    button : "decline_until_calendar", // trigger for the calendar (button ID)
    align : "B2", // alignment (defaults to "Bl")
    singleClick : true,
    onUpdate : "swap_dates"
  });

  var declineUntilDate = document.getElementById("decline_until_date");
  var declineUntilTime = document.getElementById("decline_until_time");

  function selectDeclinePeriod(select) {
    var selectValue = select.value;
    if (selectValue) {
      var then = moment().add(selectValue, 'minutes');
      $('decline_until_date').value = then.format('MM/DD/YYYY');
      $('decline_until_time').value = then.format('HH:mm:ss')
    } else {
      $('decline_until_date').value = '';
      $('decline_until_time').value = '';
    }
  }

  function validate() {
    
    if (!document.getElementById('operationY').checked && !document.getElementById('operationN').checked) {
      alert("Select Set/Clear!")
      return false;
    }
    
    if (!document.getElementById('typeB').checked && !document.getElementById('typeW').checked) {
      alert("Select Whitelist/Blacklist!")
      return false;
    }
    
    if (document.getElementById('typeB').checked && document.getElementById('operationY').checked) {
      if (!isDate(declineUntilDate.value)) {
        declineUntilDate.focus();
        return false;
      }
      if (!isTime(declineUntilTime.value)) {
        declineUntilTime.focus();
        return false;
      }
    }
    
    return true;
  }
  
  function showHideBlacklist() {
    if (document.getElementById('operationY').checked) {
      if (document.getElementById('typeB').checked) {
        $('blacklist_for').setStyle('display', 'table-row')
        $('blacklist_until').setStyle('display', 'table-row')
      } else {
        $('blacklist_for').setStyle('display', 'none')
        $('blacklist_until').setStyle('display', 'none')
      }
    } else {
      $('blacklist_for').setStyle('display', 'none')
      $('blacklist_until').setStyle('display', 'none')
    }
  }
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
