<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

 		<div class="tableContainer">
 		<div class="tableHead">
		<div class="tabHeadTxt">Add Merchant</div>
		</div>
		<div class="gridHeader">New Merchant</div>
		<form method="post" action="<%=StringUtils.encodeForHTMLAttribute(request.getRequestURI()) %>" onsubmit="return validateForm(this);">
		<table class="tabDataDisplay">
			<tr>
				<td class="label">Name</td>
				<td class="data"><input type="text" name="merchant_name" maxlength="255" style="width: 240px;" usatRequired="true" label="Name" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "merchant_name", String.class, false))%>"/></td>
				<td class="label">Description</td>
				<td class="data"><input type="text" name="merchant_desc" maxlength="255" style="width: 240px;" usatRequired="true" label="Description" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "merchant_desc", String.class, false))%>"/></td>
			</tr>			
			<tr>
				<td class="label">Authority</td>
				<td class="data">
					<select name="merchant_authority_id">
					<%Results authorities = DataLayerMgr.executeQuery("GET_AUTHORITY_FORMATTED", null);
					long selectedAuthorityId = inputForm.getLong("merchant_authority_id", false, inputForm.getLong("authority_id", false, 0L));
					while (authorities.next()) {
					   Long authorityId = authorities.getValue("aValue", Long.class);%>
						<option value="<%=authorityId%>"<%if (selectedAuthorityId > 0 && authorityId == selectedAuthorityId) { %> selected="selected"<%}%>><%=authorities.getFormattedValue("aLabel")%></option>
					<%}%>
					</select>
				</td>
				<td class="label">Business Name</td>
				<td class="data"><input type="text" name="merchant_bus_name" maxlength="255" style="width: 240px;" usatRequired="true" label="Business Name" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "merchant_bus_name", String.class, false))%>"/></td>				
			</tr>			
			<tr>
				<td class="label">Code</td>
				<td class="data"><input type="text" name="merchant_cd" maxlength="255" style="width: 240px;" usatRequired="true" label="Code" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "merchant_cd", String.class, false))%>"/></td>
				<td class="label">Doing Business As</td>
				<td class="data"><input type="text" name="doing_business_as" maxlength="21" style="width: 240px;" usatRequired="true" label="Doing Business As" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "doing_business_as", String.class, false))%>"/></td>	
			</tr>
			<tr>
                <td class="label">Merchant Category Code</td>
                <td class="data"><input type="text" name="mcc" maxlength="4" style="width: 240px;" label="Merchant Category Code" value="<% Integer mcc = RequestUtils.getAttribute(request, "mcc", Integer.class, false); if(mcc != null) {%><%=mcc%><%} %>" /></td>
                <td class="label">Authority Assigned Number</td>
                <td class="data"><input type="text" name="authority_assigned_num" maxlength="30" style="width: 240px;" label="Authority Assigned Number" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "authority_assigned_num", String.class, false))%>" /></td>
            </tr>
            <tr>
                <td class="label">Merchant Group</td>
                <td class="data"><input type="text" name="merchant_group_cd" maxlength="30" style="width: 240px;" label="Merchant Group" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "merchant_group_cd", String.class, false))%>" /></td>
            </tr>
            <tr>
				<td colspan="4" align="center">
					<input type="submit" name="action" value="Add Merchant" class="cssButton" />
				</td>				
			</tr>
	    </table>
	    </form>
	  </div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
