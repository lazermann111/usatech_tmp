<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
			<div class="tableContent">
				<% String settlement_batch_id = request.getParameter("settlement_batch_id");
				   if(settlement_batch_id == null || settlement_batch_id.length() == 0){	
				    %>
					<div align="center">			
					<span class="error">Required Parameter Not Found:settlement_batch_id</span></div> 
				    <% 
		            }else{
		                InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
		    	        Results settlementinfo = (Results) inputForm.getAttribute("settlementinfo");
		    	        Results transuminfo = (Results) inputForm.getAttribute("transuminfo");
		    	        Results refundsuminfo = (Results) inputForm.getAttribute("refundsuminfo");
		    	        Results salesinfo = (Results) inputForm.getAttribute("salesinfo");
		    	        Results refundinfo = (Results) inputForm.getAttribute("refundinfo");
				   String settleinfo[] = new String[15]; 
                   for(int i=0 ;i< settleinfo.length; i++){
                       settleinfo[i] = " ";
                    }		
                   if (settlementinfo.next()){ 
                       for(int i=0 ;i<settlementinfo.getColumnCount(); i++){
	                       settleinfo[i] = settlementinfo.getFormattedValue(i+1);
                       }
                   }
                   
                   %>
					<div class="gridHeader">Settlement</div>
				<table class="tabDataDisplayBorder">
				<tbody>
					<tr>
						<td class="label" width="15%">ID</td>
						<td class="data" width="25%"><%=settleinfo[0]%></td>
						<td class="label" width="15%">State</td>
						<td class="data" width="45%"><%=settleinfo[3]%></td>
					</tr>
					<tr>
						<td class="label">Start Time</td>
						<td class="data"><%=settleinfo[1]%></td>
						<td class="label">Last Updated</td>
						<td class="data"><%=settleinfo[2]%></td>
					</tr>
					<tr>
						<td class="label">Created</td>
						<td class="data"><%=settleinfo[7]%></td>
						<td class="label">Last Updated</td>
						<td class="data"><%=settleinfo[8]%></td>
					</tr>
					<tr>
						<td class="label">Response Code</td>
						<td class="data"><%=settleinfo[4]%></td>
						<td class="label">Response Message</td>
						<td class="data"><%=settleinfo[5]%></td>
					</tr>
					<tr>
						<td class="label">Reference Code</td>
						<td class="data"><%=settleinfo[6]%></td>
						<td class="label">Terminal Batch</td>
						<td class="data"><a href="terminalBatch.i?terminal_batch_id=<%=settleinfo[9]%>"><%=settleinfo[10]%></a></td>
					</tr>
					<tr>
						<td class="label">Sale Totals</td>
						<td class="data"><%if (transuminfo.next()){ %><%=transuminfo.getFormattedValue(1)%>/$<%=transuminfo.getFormattedValue(2)%><%} %></td>
						<td class="label">Refund Totals</td>
						<td class="data"><%if (refundsuminfo.next()){%><%=refundsuminfo.getFormattedValue(1)%>/$<%=refundsuminfo.getFormattedValue(2)%><%} %></td>
					</tr>
					<tr>
						<td class="label">Merchant</td>
						<td class="data"><a href="merchantinfo.i?merchant_id=<%=settleinfo[11]%>"><%=settleinfo[12]%></td>
						<td class="label">Terminal</td>
						<td class="data"><a href="terminalinfo.i?terminal_id=<%=settleinfo[13]%>"><%=settleinfo[14]%></td>
					</tr>
					</tbody>
                 </table>
             	<div class="spacer10"></div>
				<div class="gridHeader" align="center"><b>Sales</b></div>
                    <table class="tabDataDisplayBorderNoFixedLayout">
                    <thead>
						<tr class="gridHeader">
							<td align="center" width="10%">Detail ID</td>
							<td align="center" width="10%">Tran ID</td>
							<td align="center" width="15%">Response Code</td>
							<td align="center" width="15%">Response Desc</td>
							<td align="center" width="20%">Settlement Amount</td>
							<td align="center" width="30%">Timestamp</td>
						</tr>
					</thead>
					<tbody>
					<% while(salesinfo.next()){%>
						<tr>
							<td><a href="auth.i?auth_id=<%=salesinfo.getFormattedValue(1)%>"><%=salesinfo.getFormattedValue(1)%></a></td>
							<td><a href="tran.i?tran_id=<%=salesinfo.getFormattedValue(2)%>"><%=salesinfo.getFormattedValue(2)%></a></td>
							<td><%=salesinfo.getFormattedValue(21)%></td>
							<td><%=salesinfo.getFormattedValue(22)%></td>
							<td>$<%=salesinfo.getFormattedValue(23)%></td>
							<td><%=salesinfo.getFormattedValue(24)%></td>
						</tr>
						<%} %>
					</tbody>
					</table>
				<div class="spacer10"></div>
				<div class="gridHeader" align="center"><b>Refunds</b></div>
                    <table class="tabDataDisplayBorderNoFixedLayout">
                    <thead>
						<tr class="gridHeader">
							<td align="center" width="10%">Detail ID</td>
							<td align="center" width="10%">Tran ID</td>
							<td align="center" width="15%">Response Code</td>
							<td align="center" width="15%">Response Desc</td>
							<td align="center" width="20%">Settlement Amount</td>
							<td align="center" width="30%">Timestamp</td>
						</tr>
					</thead>
					<tbody>
					<% while(refundinfo.next()){%>
						 <tr>
						     <td><a href="refund.i?refund_id=<%=refundinfo.getFormattedValue(1)%>"><%=refundinfo.getFormattedValue(1)%></a>&nbsp;</td>
					    	 <td><a href="tran.i?tran_id=<%=refundinfo.getFormattedValue(2)%>"><%=refundinfo.getFormattedValue(2)%></a></td>
						     <td><%=refundinfo.getFormattedValue(21)%>&nbsp;</td>
						     <td><%=refundinfo.getFormattedValue(22)%>&nbsp;</td>
						     <td>$<%=refundinfo.getFormattedValue(23)%>&nbsp;</td>
						     <td><%=refundinfo.getFormattedValue(24)%>&nbsp;</td>
				    	</tr>
				    	<%} %>
				    </tbody>
					</table>
					<%} %>
				</div>
				  
                <div class="spacer5"></div>
              
             </div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
