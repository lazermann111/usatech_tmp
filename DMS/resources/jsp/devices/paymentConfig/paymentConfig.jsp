<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.dms.action.CardAction"%>
<%@page import="com.usatech.dms.action.HostAction"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<jsp:include page="/jsp/devices/deviceHeaderTabs.jsp" flush="true" />
	
<div class="tabDataContent">
<%
   SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"); 
   InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
   String reorderError = inputForm.getString("reorderError",false);
   if (reorderError != null && reorderError.length() > 0){
       if (reorderError.equals("MissingParam")){
%>
<div class="innerTable">
<div align="center">
<span class="error">
Required Parameter Not Found:pos_pta_id, old_priority, new_priority, pos_pta_payment_entry_method_cd
</span></div> 
<%
       }else {
           String[] reorderValue = (String[])inputForm.getAttribute("reorderValue");
           if (reorderError.equals("MissingPriority")){
%>

<div align="center">			
<span class="error">Missing Category Priority Value! (Action Type: <%=StringUtils.prepareCDATA(reorderValue[0]) %>, Entry Method: <%=StringUtils.prepareCDATA(reorderValue[1]) %>, POS PTA: <%=StringUtils.prepareCDATA(reorderValue[2]) %>)</span></div>
<%
           }else {
%>
<div align="center">			
<span class="error">Duplicate Category Priority Values! (Action Type: <%=StringUtils.prepareCDATA(reorderValue[0]) %>, Entry Method: <%=StringUtils.prepareCDATA(reorderValue[1]) %>, Priority: <%=StringUtils.prepareCDATA(reorderValue[2]) %>)</span></div>
<%
           }
       }
   }else{
       String errorMessage = inputForm.getString("errorMessage",false);
       if (errorMessage != null && errorMessage.length() > 0){
 %>  
 
<div align="center">			
<span class="error"><%=errorMessage %></span></div>    
<%     
       }else {
   	   Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
   	   
%>
<div class="gridHeader">
	Payment Types
	<input type="button" class="cssButton" value="List" onclick="window.location = '/paymentConfig.i?device_id=<%=device.getId()%>&userOP=payment_types';">
</div>
<%
String userOP = inputForm.getStringSafely("userOP", "");
if ("payment_types".equalsIgnoreCase(userOP) || "list_all".equalsIgnoreCase(userOP)) {
Results posPtaInfos = DataLayerMgr.executeQuery("GET_POS_PTA", new Object[]{device.getId()});
if (posPtaInfos.next()){
      posPtaInfos.setRow(0);
		    %>
	<form method="post" action="reorder.i">
	<input type="hidden" name="device_id" value="<%=device.getId() %>">
	<table class="tabDataDisplay">	    
	<thead>
		<tr class="gridHeader">
			<td>&nbsp;</td>
			<td>Payment Type</td>
			<td>Merchant</td>
			<td>Status</td>
			<td>Priority</td>
			<td>Currency</td>			
			<td>Auth Pass-thru</td>
			<td>Disable Debit Denial</td>
			<td>No Two-Tier Pricing</td>
			<td>Blacklist Until</td>
		</tr>
	</thead>	
	<tbody>
		<tr><td colspan="10" class="noPadding"><div class="spacer2"></div></td></tr>
		<% 
		    String last_payment_entry_method_cd = "";
			String last_payment_action_type_cd = "";
	        int consecutive_cnt = 0;
	        String merchant_desc;
		    while(posPtaInfos.next()) { 
		        String temp = posPtaInfos.getFormattedValue(11);
		        Integer key_id = (temp==null || temp.length() == 0)?null:(new Integer(temp));
		        merchant_desc = posPtaInfos.getFormattedValue(16);
		        //merchant_desc = HostAction.getMerchantDesc(posPtaInfos.getFormattedValue(9),posPtaInfos.getFormattedValue(10),key_id);
		        if (!last_payment_entry_method_cd.equalsIgnoreCase(posPtaInfos.getFormattedValue(7)) || !last_payment_action_type_cd.equalsIgnoreCase(posPtaInfos.getFormattedValue("payment_action_type_cd"))){
		%>
        <tr class="gridHeader"><td colspan="10"><%=posPtaInfos.getFormattedValue("payment_action_type_desc").toUpperCase()%>: <%=posPtaInfos.getFormattedValue(13)%></td></tr>
         <%} %>
         <tr>
             <td align="center"><input type="radio" name="pos_pta_id" id="pos_pta_id" value="<%= posPtaInfos.getFormattedValue(1)%>"<%= posPtaInfos.getRow() == 1? " checked=\"checked\"" : ""%>></td>
             <td><%= posPtaInfos.getFormattedValue(2)%>&nbsp;</td>
             <td><%=((merchant_desc==null) ? "" : merchant_desc) %>&nbsp;</td>
             <td><% if (posPtaInfos.getFormattedValue(3) != null && posPtaInfos.getFormattedValue(3).length() > 0){ %>
                   <%=posPtaInfos.getFormattedValue(4) %>
                 <%} else if (posPtaInfos.getFormattedValue(5) != null && posPtaInfos.getFormattedValue(5).length() > 0){ %> 
                   <%=posPtaInfos.getFormattedValue(6) %>
                 <%} else {%>
                   <b><font color="orange">Inactive</font></b>
                 <% }
                    if (posPtaInfos.getFormattedValue(14) != null && posPtaInfos.getFormattedValue(14).length() > 0)  { %>
                       &nbsp;($<%=posPtaInfos.getFormattedValue(14) %> Override)
                 <%} %>
             </td>
             <td align="center">
                <input type="text" name="new_priority_<%=posPtaInfos.getFormattedValue(1)%>" value="<%= posPtaInfos.getFormattedValue(8)%>" size="3">
                <input type="hidden" name="old_priority_<%=posPtaInfos.getFormattedValue(1)%>" value="<%= posPtaInfos.getFormattedValue(8)%>">
                <input type="hidden" name="pos_pta_payment_action_type_cd_<%=posPtaInfos.getFormattedValue(1)%>" value="<%= posPtaInfos.getFormattedValue("payment_action_type_cd")%>">
                <input type="hidden" name="pos_pta_payment_entry_method_cd_<%=posPtaInfos.getFormattedValue(1)%>" value="<%= posPtaInfos.getFormattedValue(7)%>">
             </td>
             <td align="center"><%=posPtaInfos.getFormattedValue("currency_cd")%> - <%=posPtaInfos.getFormattedValue("currency_name")%>&nbsp;</td>
             <td align="center"><% if (posPtaInfos.getFormattedValue(12).equalsIgnoreCase("Y")) { %>
                 <img src="/images/descending.gif">
                 <%}else { %>&nbsp;
                 <%} %>
              </td>
              <td align="center"><%="Y".equalsIgnoreCase(posPtaInfos.getFormattedValue("pos_pta_disable_debit_denial")) ? "Yes" : "&nbsp;"%></td>
              <td align="center"><%="Y".equalsIgnoreCase(posPtaInfos.getFormattedValue("no_convenience_fee")) ? "Yes" : "&nbsp;"%></td>
              <td align="center">
                <%  String whitelist = posPtaInfos.getFormattedValue("whitelist");
                    Date declineUntil = posPtaInfos.getValue("decline_until", Date.class); %>
                <%="Y".equalsIgnoreCase(whitelist) ? "Whitelisted" : "&nbsp;"%>
                <% if (declineUntil != null && !"Y".equalsIgnoreCase(whitelist)) { %>
                  <%=dateFormat.format(declineUntil) %>
                <% } %>
              </td>
           </tr>
		<% 
		     last_payment_entry_method_cd = posPtaInfos.getFormattedValue(7);
			 last_payment_action_type_cd = posPtaInfos.getFormattedValue("payment_action_type_cd");
		    } 
		%>
	</tbody>
</table>
<div class="spacer5"></div>
<div class="gridHeader">
  <input type="button" class="cssButton" value="Edit Settings" name="action"
    onClick="return redirectWithParams('editPosPtaSettings.i', 'device_id=<%=device.getId()%>', new Array(this, pos_pta_id));">
  <input type="button" class="cssButton" value="Add New / Reactivate Disabled Type" name="action"
    onClick="return redirectWithParams('addPtaSetting.i', 'device_id=<%=device.getId()%>', new Array(this,document.getElementById('pos_pta_id')));">
  <input type="button" class="cssButton" value="Import Template" name="action" 
    onClick="return redirectWithParams('importPosptaTmpl.i', 'device_id=<%=device.getId()%>', new Array(this));">
  <input type="submit" class="cssButton" value="Reorder">
  <input type="button" class="cssButton" value="Whitelist/Blacklist" name="action" 
     onClick="return redirectWithParams('blacklist.i', 'pos_id=<%=device.getPosId()%>&serial=<%=device.getSerialNumber()%>&device_id=<%=device.getId()%>', new Array(this));">
</div>
</form>
	<%}else{ %>
		<div align="center">
	   <br/>No payment types currently enabled<br/><br/>
	   <input type="button" class="cssButton" value="Add a New Type" name="action"
		   onClick="return redirectWithParams('addPtaSetting.i', 'device_id=<%=device.getId()%>', new Array(this));">
	   <input type="button" class="cssButton" value="Import Template" name="action"
		   onClick="return redirectWithParams('importPosptaTmpl.i', 'device_id=<%=device.getId()%>', new Array(this));">
		</div>
	<%} %>
</div>
<div class="spacer10"></div>
<%}%>
<div class="gridHeader">
	Transaction History From <input type="text" name="transaction_from_date" id="transaction_from_date" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute(DevicesConstants.PARAM_TRAN_FROM_DATE))%>" size="8" maxlength="10" >
    <img src="/images/calendar.gif" id="from_date_trigger" class="calendarIcon" title="Date selector" /> 
    <input type="text" size="6" maxlength="8" id="transaction_from_time" name="transaction_from_time" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute(DevicesConstants.PARAM_TRAN_FROM_TIME))%>" />
    To <input type="text" name="transaction_to_date" id="transaction_to_date" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute(DevicesConstants.PARAM_TRAN_TO_DATE))%>" size="8" maxlength="10">
    <img src="/images/calendar.gif" id="to_date_trigger" class="calendarIcon" title="Date selector" />
    <input type="text" size="6" maxlength="8" id="transaction_to_time" name="transaction_to_time" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute(DevicesConstants.PARAM_TRAN_TO_TIME))%>" />
    <input type="button" class="cssButton" value="List" onclick="handleListBtn();"> 
</div>
<div class="spacer2"></div>
	<% 
	Results tranList = (Results) inputForm.getAttribute("tranList");
	if(tranList != null) {
		String sortField = PaginationUtil.getSortField(null);
	    String sortIndex = inputForm.getString(sortField, false);
	    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "-2" : sortIndex;
	%>
<table class="tabDataDisplay">
	<tbody>
		<tr class="sortHeader">
			<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Tran ID</a> <%=PaginationUtil.getSortingIconHtml("1", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Time</a> <%=PaginationUtil.getSortingIconHtml("2", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">State</a> <%=PaginationUtil.getSortingIconHtml("3", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Payment Type</a> <%=PaginationUtil.getSortingIconHtml("4", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Entry Method</a> <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Card Data</a> <%=PaginationUtil.getSortingIconHtml("6", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Amount</a> <%=PaginationUtil.getSortingIconHtml("7", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Consumer Email</a> <%=PaginationUtil.getSortingIconHtml("8", sortIndex)%></td>
		</tr>
		<% 
		int i = 0; 
		while (tranList.next()) {
			i++;
		%>
		<tr class="<%=(i%2 == 0) ? "row1" : "row0"%>">
			<td><a href="tran.i?tran_id=<%=tranList.getFormattedValue(1)%>"><%=tranList.getFormattedValue(1)%></a></td>
			<td><%=tranList.getFormattedValue(2) %></td>
			<td><%=tranList.getFormattedValue(3) %></td>
			<td><%=tranList.getFormattedValue(4) %></td>
			<td><%=tranList.getFormattedValue(5) %></td>
			<td><%=CardAction.maskCreditCard(tranList.getFormattedValue(6))%>&nbsp;</td>
			<td><%=tranList.getFormattedValue(7) %>&nbsp;</td>
			<td><%=tranList.getFormattedValue(8) %>&nbsp;</td>
		</tr>
		<%} %>
	</tbody>
	<%} %>
</table>
<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{
		DevicesConstants.PARAM_DEVICE_ID,DevicesConstants.PARAM_USER_OP,
		DevicesConstants.PARAM_TRAN_FROM_DATE,DevicesConstants.PARAM_TRAN_TO_DATE,
		DevicesConstants.PARAM_TRAN_FROM_TIME,DevicesConstants.PARAM_TRAN_TO_TIME});

%>
</div>

<jsp:include page="/jsp/include/pagination.jsp" flush="true">
   	<jsp:param name="_param_request_url" value="paymentConfig.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

<script type="text/javascript" defer="defer">
    Calendar.setup({
        inputField     :    "transaction_from_date",                  // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "transaction_to_date",                   // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });

    var fromDate = document.getElementById("transaction_from_date");
    var fromTime = document.getElementById("transaction_from_time");
    var toDate = document.getElementById("transaction_to_date");
    var toTime = document.getElementById("transaction_to_time");
    
    function validateDate() {
      if(!isDate(fromDate.value)) {
        fromDate.focus();
        return false;
      }
      if(!isTime(fromTime.value)) {
        fromTime.focus();
        return false;
      }
      if(!isDate(toDate.value)) {
        toDate.focus();
        return false;
      }
      if(!isTime(toTime.value)) {
        toTime.focus();
        return false;
      }
      return true;
    }
    
    function handleListBtn() {
      if(validateDate()) {
        return redirectWithParams('paymentConfig.i','device_id=<%=device.getId()%>&userOP=<%="payment_types".equalsIgnoreCase(userOP) || "list_all".equalsIgnoreCase(userOP) ? "list_all" : "list_tran"%>', new Array(fromDate, fromTime, toDate, toTime));
      }
    }
    
</script>
<%}} %>

<jsp:include page="/jsp/devices/deviceFooter.jsp" flush="true" />

<jsp:include page="/jsp/include/footer.jsp" flush="true" />