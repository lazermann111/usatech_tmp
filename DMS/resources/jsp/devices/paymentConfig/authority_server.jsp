<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="tableContainer">
	<div class="tableContent">
		<% 
		String authority_server_id= request.getParameter("authority_server_id");
		if(authority_server_id == null || (authority_server_id.length()==0)){	
		    %>
			<div align="center">			
			<span class="error">Required Parameter Not Found:authority_server_id</span></div> 
		    <% 		     
        }else{
            InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
	        Results authserverinfo = (Results) inputForm.getAttribute("authserverinfo");
	        
		    String authserver[] = new String[10]; 
            for(int i=0 ;i< authserver.length; i++){
                authserver[i] = " ";
            }		
            if (authserverinfo.next()){ 
                for (int i=0 ;i<authserverinfo.getColumnCount(); i++){
	                authserver[i] = authserverinfo.getFormattedValue(i+1);
                }
            }%>
		<div class="gridHeader" align="center">
			<b>Authority Server</b>
		</div>
		<table class="tabDataDisplayBorder">
			<tbody>
				<tr>
					<td class="label">ID</td>
					<td class="data"><%=authserver[0]%></td>
					<td class="label">Name</td>
					<td class="data"><%=authserver[1]%></td>
				</tr>
				<tr>
					<td class="label">Address</td>
					<td class="data"><%=authserver[2]%></td>
					<td class="label">Port</td>
					<td class="data"><%=authserver[3]%></td>
				</tr>
				<tr>
					<td class="label">Created</td>
					<td class="data"><%=authserver[5]%></td>
					<td class="label">Last Updated</td>
					<td class="data"><%=authserver[6]%></td>
				</tr>
				<tr>
					<td>Authority Gateway</td>
					<td><a href="authorityGateway.i?authority_gateway_id=<%=authserver[7]%>"><%=authserver[8]%></a></td>
					<td colspan="2">&nbsp;</td>
				</tr>				
			</tbody>
		</table>
		<%} %>
	</div>
	<div class="spacer5">&nbsp;	</div>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
