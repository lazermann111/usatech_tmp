<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="com.usatech.dms.action.CardAction" %>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="tableContainer">
	<div class="tableContent">
		<%String refund_id= request.getParameter("refund_id");
			if((refund_id == null) || (refund_id.length()==0)){%>
			    <div align="center">			
				<span class="error">Required Parameter Not Found:refund_id </span>	</div> 
				<% 		     
	      }else{
	          InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
		      Results refundinfo = (Results) inputForm.getAttribute("refundinfo");
		      Results refundsettleInfo = (Results) inputForm.getAttribute("refundsettleInfo");  
	      	String refund[] = new String[32]; 
			for(int i=0 ;i< refund.length; i++){
			refund[i] = "";
			}		
			if(refundinfo.next()){ 
			for(int i=0 ;i<refundinfo.getColumnCount(); i++){
				refund[i] = refundinfo.getFormattedValue(i+1);
			}		}	           
		  %>
		<div class="gridHeader" align="center">Refund Details</div>
		<table class="tabDataDisplay">
			<col width="15%" />
			<col width="35%" />
			<col width="15%" />
			<col width="35%" />
			<tbody>
				<tr>
					<td class="label">Detail ID</td>
					<td class="data"><%=refund[0]%></td>
					<td class="label">Description</td>
					<td class="data"><%=refund[2]%>&nbsp;</td>
				</tr>
				<tr>
					<td class="label">State</td>
					<td class="data"><%=refund[3]%></td>
					<td class="label">Account Data	</td>
					<td class="data"><%=CardAction.maskCreditCard(refund[4])%>&nbsp;</td>
				</tr>
				<tr>
					<td class="label">Amount</td>
					<td class="data">$<%=refund[6]%>&nbsp;</td>
					<td class="label">Entry Method</td>
					<td class="data"><%=refund[5]%></td>
				</tr>
				<tr>
					<td class="label">Issued By</td>
					<td class="data"><%=refund[18]%>&nbsp;</td>
					<td class="label">Result</td>
					<td class="data"><%=refund[19]%></td>
				</tr>
				<tr>
					<td class="label">Response Code</td>
					<td class="data"><%=refund[9]%>&nbsp;</td>
					<td class="label">Response Message</td>
					<td class="data"><%=refund[10]%>&nbsp;</td>
				</tr>
				<tr>
					<td class="label">Authorization Code</td>
					<td class="data"><%=refund[11]%>&nbsp;</td>
					<td class="label">Reference Number</td>
					<td class="data"><%=refund[12]%>&nbsp;</td>
				</tr>
				<tr>
					<td class="label">Detail Timestamp</td>
					<td class="data"><%=refund[7]%>&nbsp;</td>
					<td class="label">Authority Timestamp</td>
					<td class="data"><%=refund[13]%>&nbsp;</td>
				</tr>
				<tr><td class="label">Created</td>
					<td class="data"><%=refund[14]%></td>
					<td class="label">Last Updated</td>
					<td class="data"><%=refund[15]%></td>
				</tr>
				<tr>
					<td class="label">Terminal Batch ID</td>
					<td class="data"><%if (!StringHelper.isBlank(refund[16])) { %><a href="terminalBatch.i?terminal_batch_id=<%=refund[16]%>"><%=refund[16]%></a><%} %>&nbsp;</td>
					<td class="label">Transaction ID</td>
					<td class="data"><a href="tran.i?tran_id=<%=refund[1]%>"><%=refund[1]%></a>&nbsp;</td>

				</tr>
				<tr>
					<td class="label">Misc Data</td>
					<td class="data" colspan="3"><%=refund[17]%>&nbsp;</td>
				</tr>
			</tbody>
		</table>
		
		<%if (!StringUtils.isBlank(refund[25])) {%>
			<div class="gridHeader" align="center"><b>Chase Processing Details</b></div>
			<table class="tabDataDisplayBorder">
				<col width="15%" />
				<col width="35%" />
				<col width="15%" />
				<col width="35%" />
				<tbody>					
				<tr>
					<td class="label">DFR File ID</td>
					<td class="data"><a href="/DFRFile.i?dfrFileId=<%=refund[25]%>"><%=refund[25]%></a></td>
					<td class="label">Entity #</td>
					<td class="data"><%=refund[27]%></td>
				</tr>
				<tr>
					<td class="label">Submission #</td>
					<td class="data"><%=refund[28]%></td>
					<td class="label">Record #</td>
					<td class="data"><%=refund[29]%></td>
				</tr>
				<tr>
					<td class="label">Submission Date</td>
					<td class="data"><%=refund[26]%></td>
					<td class="label">Interchange Code</td>
					<td class="data"><%=refund[21]%></td>
				</tr>
				<tr>
					<td class="label">Fee Description</td>
					<td class="data"><%=refund[23]%></td>
					<td class="label">Fee Amount</td>
					<td class="data"><%=refund[24]%></td>
				</tr>
				<tr>
					<td class="label">Method of Payment</td>
					<td class="data"><%=refund[20]%></td>
					<td class="label">Downgrade Reason Code</td>
					<td class="data"><%=refund[22]%></td>
				</tr>
				<%if (!StringUtils.isBlank(refund[30])) {%>
				<tr>
					<td class="label">Downgrade Reason Description</td>
					<td class="data" colspan="3"><%=refund[30]%></td>
				</tr>
				<%}%>
				</tbody>			    
			</table>
			<div class="spacer10"></div>	
	    <%}%>
       
		<div class="spacer10"></div>
		<div class="gridHeader" align="center">Settlements</div>
		<div class="spacer10"></div>
		<table class="tabDataDisplayBorder">
			<thead>
				<tr class="gridHeader">
					<td align="center">ID</td>
					<td align="center">Settlement Date</td>
					<td align="center">Result</td>
				</tr>
			</thead>
			<tbody>
			<% while(refundsettleInfo.next()){  %>
				<tr>
					<td><a href="settlementBatch.i?settlement_batch_id=<%=refundsettleInfo.getFormattedValue(1)%>"><%=refundsettleInfo.getFormattedValue(1)%></a></td>
					<td><%=refundsettleInfo.getFormattedValue(2)%></td>
					<td><%=refundsettleInfo.getFormattedValue(4)%></td>
				</tr>
				<%} %>
			</tbody>
		</table>
		 <%} %>
	</div>
	<div class="spacer10"></div>

</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
