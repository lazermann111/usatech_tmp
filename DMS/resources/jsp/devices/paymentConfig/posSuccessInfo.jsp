<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
			<div class="tableContent">				
					<%
					InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
					String device_id= request.getParameter("device_id");					
					String errorMessage = (String)inputForm.getAttribute("errorMessage");
					if (errorMessage!=null && errorMessage.equalsIgnoreCase("Y")){
					    %>
					    <br/>
					    <div align="center">			
						<span class="error">
					    <h3>No unique payment types available to be added at this time.</h3>
					    If you are seeing this error, then you may have clicked back in your browser or accidently refreshed the Add New / Reactivate Payment Type completion page.
					    </span>
					    <br/>
					    <br/>
					    <input type="button"  class="cssButton"  style="width:220px;" value="Go Back to Device Profile" onClick="javascript:window.location='profile.i?device_id=<%=StringUtils.encodeForURL(device_id) %>';">
					    </div>
					    <%
					}else{
					    boolean errorParam = inputForm.getBoolean("errorParam", false, true);
					    if (!errorParam){
					        %>
					        <div align="center">	 		
						    <span class="error">Required Parameter Not Found:device_id,payment_subtype_id,pos_pta_duplicate_settings</span></div> 
					    
					    <%} else { 
					String action = request.getParameter("action");
					if(action == null || action.length() ==0) {%>
					<span class="error">Required parameter not found: action</span>
					<%}else {
					    Long pos_pta_id = (Long)inputForm.getAttribute("pos_pta_id");
					    String pos_pta_duplicate_settings = (String)inputForm.getAttribute("pos_pta_duplicate_settings");
					    boolean pos_old_data = (Boolean)inputForm.getAttribute("pos_old_data");
					%>
					<div class="gridHeader" align="center">Point-of-Sale Payment Type - <%=StringUtils.encodeForHTML(action) %></div>
				<form method="post" action="editPosPtaSettings.i">
				<table class="tabDataDisplay">
				<tbody>
                <input type="hidden" name="device_id" value="<%=StringUtils.encodeForHTMLAttribute(device_id) %>">
                <input type="hidden" name="pos_pta_id" value="<%=StringUtils.encodeForHTMLAttribute(pos_pta_id)%>">
                <input type="hidden" name="action" value="edit">
               <tr>
                  <td align="center">
                      Device payment type successfully 
		<% 
		if (pos_old_data)
		{
		    %>reactivated
		    <%if (pos_pta_duplicate_settings.equalsIgnoreCase("Y")){ %>
			 (using old settings)<%} %>
			!
			<% 
		}
		else
		{
		    %>added or reactivated
       <% }%>
       <p>
                <input type="submit"  class="cssButton"  style="width:100px;" name="myaction" value="Edit Settings" >&nbsp;
                <input type="button"  class="cssButton"  style="width:220px;" value="Go Back to Device Profile" onClick="javascript:window.location='paymentConfig.i?device_id=<%=StringUtils.encodeForURL(device_id) %>&userOP=payment_types';">
                  </td>
                </tr>
                
				</tbody>
			</table></form>	<%} } }%>
						</div>
			<div class="spacer10"></div>
			<div class="spacer10"></div>
        </div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />