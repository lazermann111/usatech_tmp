<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="device_id" class="java.lang.String" scope="request" />
<jsp:useBean id="pos_pta_id" class="java.lang.String" scope="request" />
<jsp:useBean id="tooltip" class="java.lang.String" scope="request" />
<jsp:useBean id="authority_payment_mask_id" class="java.lang.String" scope="request" />
<jsp:useBean id="params" class="java.util.HashMap" scope="request" />

<jsp:useBean id="payment_subtype_key_name_formatted" class="java.lang.String" scope="request" />
<% Results payment_subtype_table_key_ids = RequestUtils.getAttribute(request, "payment_subtype_table_key_ids", Results.class, true);%>
<jsp:useBean id="toggle_hour_loop" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="toggle_ampm_loop" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="toggle_tzone_loop" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="toggle_type_loop" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="toggle_currency" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="toggle_cd" class="java.lang.String" scope="request" />
<jsp:useBean id="toggle_custom" class="java.lang.String" scope="request" />
<jsp:useBean id="toggle_ampm" class="java.lang.String" scope="request" />
<jsp:useBean id="toggle_hour" class="java.lang.String" scope="request" />
<jsp:useBean id="toggle_regex_methods" class="java.util.LinkedList" scope="request" />
<jsp:useBean id="currentDate" class="java.lang.String" scope="request" />
<jsp:useBean id="currentTz" class="java.lang.String" scope="request" />
<jsp:useBean id="currentHour" class="java.lang.String" scope="request" />
<jsp:useBean id="currentAmPm" class="java.lang.String" scope="request" />

<c:set var="pp_pos_pta_device_serial_cd" value="${params['pp_pos_pta_device_serial_cd']}" />
<c:set var="device_serial_cd" value="${params['device_serial_cd']}" />
<c:set var="device_type_id" value="${params['device_type_id']}" />
<c:set var="pp_pos_pta_encrypt_key" value="${params['pp_pos_pta_encrypt_key']}" />
<c:set var="pp_pos_pta_encrypt_key2" value="${params['pp_pos_pta_encrypt_key2']}" />
<c:set var="pp_payment_subtype_key_id" value="${params['pp_payment_subtype_key_id']}" />
<c:set var="pp_pos_pta_pin_req_yn_flag" value="${params['pp_pos_pta_pin_req_yn_flag']}" />
<c:set var="ps_authority_payment_mask_id" value="${params['ps_authority_payment_mask_id']}" />
<c:set var="pp_authority_payment_mask_id" value="${params['pp_authority_payment_mask_id']}" />
<c:set var="pos_pta_regex" value="${params['pos_pta_regex']}" />
<c:set var="pos_pta_regex_bref" value="${params['pos_pta_regex_bref']}" />
<c:set var="pp_payment_subtype_id" value="${params['pp_payment_subtype_id']}" />
<c:set var="ps_payment_subtype_table_name" value="${params['ps_payment_subtype_table_name']}" />
<c:set var="ps_payment_subtype_key_name" value="${params['ps_payment_subtype_key_name']}" />
<c:set var="ps_payment_subtype_key_desc_name" value="${params['ps_payment_subtype_key_desc_name']}" />
<c:set var="ps_payment_subtype_name" value="${params['ps_payment_subtype_name']}" />
<c:set var="pp_pos_pta_activation_ts" value="${params['pp_pos_pta_activation_ts']}" />
<c:set var="pp_pos_pta_deactivation_ts" value="${params['pp_pos_pta_deactivation_ts']}" />
<c:set var="status_disabled" value="${params['status_disabled']}" />
<c:set var="status_enabled" value="${params['status_enabled']}" />
<c:set var="pp_pos_id" value="${params['pp_pos_id']}" />
<c:set var="pp_pos_pta_passthru_allow_yn_flag" value="${params['pp_pos_pta_passthru_allow_yn_flag']}" />
<c:set var="pp_pos_pta_pref_auth_amt" value="${params['pp_pos_pta_pref_auth_amt']}" />
<c:set var="pp_pos_pta_pref_auth_amt_max" value="${params['pp_pos_pta_pref_auth_amt_max']}" />
<c:set var="pp_currency_cd" value="${params['pp_currency_cd']}" />
<c:set var="pp_pos_pta_disable_debit_denial" value="${params['pp_pos_pta_disable_debit_denial']}" />
<c:set var="pp_no_convenience_fee" value="${params['pp_no_convenience_fee']}" />
<c:set var="pp_payment_action_type_desc" value="${params['pp_payment_action_type_desc']}" />
<c:set var="pp_payment_entry_method_desc" value="${params['pp_payment_entry_method_desc']}" />
<c:set var="pp_decline_until_date" value="${params['pp_decline_until_date']}" />
<c:set var="pp_decline_until_time" value="${params['pp_decline_until_time']}" />
<c:set var="pp_whitelist" value="${params['pp_whitelist']}" />

<c:set var="pos_pta_encrypt_key_hex" value="${params['pos_pta_encrypt_key_hex']}" />
<c:set var="pos_pta_encrypt_key2_hex" value="${params['pos_pta_encrypt_key2_hex']}" />

<c:set var="regex_final" value="${params['regex_final']}" />
<c:set var="regex_final_bref" value="${params['regex_final_bref']}" />
<c:set var="regex_final_name" value="${params['regex_final_name']}" />
<c:set var="regex_final_desc" value="${params['regex_final_desc']}" />

<c:set var="regex_ids_str" value="${params['regex_ids_str']}" />
<c:set var="regex_methods_str" value="${params['regex_methods_str']}" />
<c:set var="regex_bref_methods_str" value="${params['regex_bref_methods_str']}" />
<c:set var="regex_bref_codes_str" value="${params['regex_bref_codes_str']}" />
<c:set var="regex_bref_names_str" value="${params['regex_bref_names_str']}" />
<c:set var="regex_bref_descs_str" value="${params['regex_bref_descs_str']}" />

<c:set var="form_toggle_rowspan" value="8" />

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/location.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>

		<div class="tableContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">Point-of-Sale Payment Type</div>
		</div>
		
<form method="post" action="editPosPtaSettingsFunc.i" onSubmit="javascript:return confirmSubmit();">
<input type="hidden" name="device_id" value="${device_id }">
<input type="hidden" name="pos_pta_id" value="${pos_pta_id }">
<input type="hidden" name="pos_id" value="${pp_pos_id }">
<input type="hidden" name="toggle_cd" value="${toggle_cd }">
<input type="hidden" name="original_payment_subtype_id" value="${pp_payment_subtype_id }">
<input type="hidden" name="original_payment_subtype_key_id" value="${pp_payment_subtype_key_id }">
<input type="hidden" name="original_authority_payment_mask_id" value="${authority_payment_mask_id }">

<input type="hidden" name="original_pos_pta_regex" value="${regex_final }">
<input type="hidden" name="original_pos_pta_regex_bref" value="${regex_final_bref }">
<input type="hidden" name="original_pos_pta_device_serial_cd" value="${pp_pos_pta_device_serial_cd }">
<input type="hidden" name="original_pos_pta_encrypt_key" value="${pp_pos_pta_encrypt_key }">
<input type="hidden" name="original_pos_pta_encrypt_key2" value="${pp_pos_pta_encrypt_key2 }">
<input type="hidden" name="original_pos_pta_pin_req_yn_flag" value="${pp_pos_pta_pin_req_yn_flag }">
<input type="hidden" name="original_pos_pta_passthru_allow_yn_flag" value="${pp_pos_pta_passthru_allow_yn_flag }">
<input type="hidden" name="original_pos_pta_disable_debit_denial" value="${pp_pos_pta_disable_debit_denial }">
<input type="hidden" name="original_no_convenience_fee" value="${pp_no_convenience_fee }">
<input type="hidden" name="original_decline_until_date" value="${pp_decline_until_date }">
<input type="hidden" name="original_decline_until_time" value="${pp_decline_until_time }">
<input type="hidden" name="original_whitelist" value="${pp_whitelist }">
<table class="tabDataDisplay">
 <tr><td colspan="4" class="gridHeader">${ps_payment_subtype_name }</td></tr>
 <tr>
  <td>Device ID</td>
  <td>${device_id }</td>
  <td>Serial Number</td>
  <td>${device_serial_cd }</td>
 </tr>
 <tr>
  <td>Payment Action Type</td>
  <td>${pp_payment_action_type_desc}</td>
  <td>Payment Entry Method</td>
  <td>${pp_payment_entry_method_desc}</td>
 </tr>
 <tr>
  <td nowrap>${payment_subtype_key_name_formatted}</td>

  <td colspan="3">
  	<% boolean systemManagedTerminalCd = false;
  	if(!payment_subtype_table_key_ids.isGroupEnding(0)) {%>
  	 <select id="terminal" name="payment_subtype_key_id" class="smallText">
			<% 
			long pp_payment_subtype_key_id = ConvertUtils.getLong(params.get("pp_payment_subtype_key_id"), 0L);
            while(payment_subtype_table_key_ids.next()) {
			     long terminal_id = payment_subtype_table_key_ids.getValue("terminal_id", Long.class);
			     Long available_slots = payment_subtype_table_key_ids.getValue("available_slots", Long.class);
                 boolean smtc = !StringUtils.isBlank(payment_subtype_table_key_ids.getValue("pos_pta_cd_expression", String.class));
			     %><option value="<%=terminal_id%>"<%
			    if(pp_payment_subtype_key_id == terminal_id) {
			    	 systemManagedTerminalCd = smtc;
			     %> selected="selected"<%
			     } else if(available_slots != null && available_slots < 1) {
			     %> disabled="disabled"<%
			     }
			     %> onselect="systemManageTerminalCode(this.form, <%=smtc%>);"><%=StringUtils.prepareHTML(payment_subtype_table_key_ids.getValue("merchant_name", String.class))%><%
			     if(available_slots != null) {
                 %> [<%=available_slots%> slots available]<%
                 }
                 %></option>
			 <%} %>
	  </select>
  	<%} else { %>
  	<input type="hidden" name="payment_subtype_key_id" value="N/A">N/A
  	<%} %>
  </td>
 </tr>	 
 <tr>
  <td nowrap>Activation Date</td>
		<c:choose>
			<c:when test="${toggle_cd > 0 or toggle_cd == -1}">
				<c:set var="form_toggle_rowspan" value="7" />
				
					<td>${pp_pos_pta_activation_ts }&nbsp;</td>
					<td>POS PTA ID</td>
				
					<td>${pos_pta_id }</td>  
				 </tr>
				 <tr>
						<td nowrap>Deactivation Date</td>
		
  			</c:when>
		</c:choose>
		<c:choose>
			<c:when test="${toggle_cd == -1}">
				<td>${pp_pos_pta_deactivation_ts }&nbsp;</td>
			</c:when>
			<c:otherwise>
				<td>
   					<table class="tabDataDisplayNoBorder">
    
					    <c:forEach var="nvp_item_type" items="${toggle_type_loop}">
							
								<c:choose>
									<c:when test="${toggle_custom eq 'false'}">
										<c:choose>
										<c:when test="${nvp_item_type.name eq 'None'}">
											<tr><td><input type="radio" name="toggle_date" value="${nvp_item_type.value}" checked></td><td width="90%">${nvp_item_type.name}</td></tr>
										</c:when>
										<c:otherwise>
											<tr><td><input type="radio" name="toggle_date" value="${nvp_item_type.value}"></td><td>${nvp_item_type.name}</td></tr>
										</c:otherwise>
										</c:choose>
									
									</c:when>
									<c:otherwise>
										<c:choose>
										<c:when test="${nvp_item_type.name eq 'Custom:'}">
											<tr><td><input type="radio" name="toggle_date" value="${nvp_item_type.value}" checked></td><td>${nvp_item_type.name}</td></tr>
										</c:when>
										<c:otherwise>
											<tr><td><input type="radio" name="toggle_date" value="${nvp_item_type.value}"></td><td>${nvp_item_type.name}</td></tr>
										</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							    	
						</c:forEach>
 				 
			    <tr>
			     <td >&nbsp;</td>
			     <td nowrap >
			      
			      <img src="/images/calendar.gif" id="toggle_date_trigger" name="toggle_date_trigger" class="calendarIcon" title="Date selector" />
				  <span>&nbsp;</span> 
				  <input type="text" style="" name="toggle_full_date"
							id="dateTxt"
							value="${currentDate}" size="10"
							maxlength="10" READONLY
							onchange="javascript:this.form.toggle_date[2].checked=true;">
				  <div class="spacer5"></div>
				  <select name="toggle_time" onClick="javascript:this.form.toggle_date[2].checked=true;">
						<c:forEach var="nvp_item_hour" items="${toggle_hour_loop}">
							<c:choose>
							<c:when test="${nvp_item_hour.value eq currentHour}">
								<option value="${nvp_item_hour.value}" selected="selected">${nvp_item_hour.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${nvp_item_hour.value}">${nvp_item_hour.name}</option>
							</c:otherwise>
							</c:choose>
						</c:forEach>
				  </select>
				  <select name="toggle_ampm" onClick="javascript:this.form.toggle_date[2].checked=true;">
						<c:forEach var="nvp_item_ampm" items="${toggle_ampm_loop}">
							<c:choose>
							<c:when test="${nvp_item_ampm.value eq currentAmPm}">
								<option value="${nvp_item_ampm.value}" selected="selected">${nvp_item_ampm.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${nvp_item_ampm.value}">${nvp_item_ampm.name}</option>
							</c:otherwise>
							</c:choose>
						</c:forEach>
				  </select>
			      &nbsp;<select name="toggle_tzone" onClick="javascript:this.form.toggle_date[2].checked=true;">
						<c:forEach var="nvp_item_tzone" items="${toggle_tzone_loop}">
							<c:choose>
							<c:when test="${nvp_item_tzone.value eq currentTz}">
								<option value="${nvp_item_tzone.value}" selected="selected">${nvp_item_tzone.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${nvp_item_tzone.value}">${nvp_item_tzone.name}</option>
							</c:otherwise>
							</c:choose>
						</c:forEach>
				  </select>
			     </td>
			    </tr>
			    
			   </table>
			  </td>
			</c:otherwise>
		</c:choose>
		
  <c:set var="ID" value="${authority_payment_mask_id}" />
  <td valign="top" colspan="2" rowspan="${form_toggle_rowspan}">
   <script language="JavaScript" type="text/javascript">
<!--

function regexSelectedMethod(myForm) {
	var regexIdArr = new Array(${regex_ids_str});
	var regexArr = new Array(${regex_methods_str});
	var regexBrefArr = new Array(${regex_bref_methods_str});
	myForm.selected_regex.value = regexArr[myForm.pos_pta_regex.selectedIndex];
	myForm.selected_regex_bref.value = regexBrefArr[myForm.pos_pta_regex.selectedIndex];
	myForm.authority_payment_mask_id.value = regexIdArr[myForm.pos_pta_regex.selectedIndex];
	myForm.pos_pta_regex_bref.value = myForm.selected_regex_bref.value;
}
// -->
   </script>
   <input type="hidden" name="payment_subtype_id" value="${pp_payment_subtype_id }">
   <table class="tabDataDisplayNoBorder">
    <tr>

     <td nowrap colspan="2" class="label">Card Data Validation and Decoding Method</td>
    </tr>
    <tr>
     <td nowrap colspan="2"><hr style="width:100%;height:1px;color:#CCCCCC"/></td>
    </tr>
    <tr>
     <td nowrap>Method&nbsp;</td>

     <td>
     <select name="pos_pta_regex" onClick="javascript:regexSelectedMethod(this.form);" class="smallText">
			<c:forEach var="nvp_item_regex_method" items="${toggle_regex_methods}">
				<option value="${nvp_item_regex_method['value']}" ${nvp_item_regex_method['default']}>${nvp_item_regex_method['text']}</option>		    	
			</c:forEach>
	  </select>
      <input type="hidden" name="authority_payment_mask_id" value="${authority_payment_mask_id }">
      <input type="hidden" name="pos_pta_regex_bref" value="${regex_final_bref }">
     </td>
    </tr>

    <tr>
     <td valign="bottom" align="center"><input type="button" class="cssButton" name="test_regex" value="Test" onClick="javascript:testRegex(${ID});"></td>
     <td>
      <table>
       <tr>
        <td title="${regex_final}" nowrap>Card Data Regex ${tooltip}</td>
        <td><input type="text" name="selected_regex" title="This is Read Only Field" value="${regex_final}" id="regex_${ID }" size="70" disabled="disabled" /></td>
       </tr>

       <tr>
        <td nowrap>Backreference&nbsp;</td>
        <td><input type="text" name="selected_regex_bref" value="${regex_final_bref}" id="backr_${ID}" size="70" disabled="disabled" /></td>
       </tr>
       <tr>
        <td nowrap>Sample Card Data&nbsp;</td><td><input type="text" name="test_regex_card_num" value="" id="testv_${ID }" size="70" /></td>
       </tr>
      </table>

     </td>
    </tr>
   </table>
   <table class="tabDataDisplayNoBorder">
    <tr>
     <td nowrap colspan="2" class="label">&nbsp;<br/>Server Authorization Amount Override</td>
    </tr>
    <tr>
     <td nowrap colspan="2"><hr style="width:100%;height:1px;color:#CCCCCC"/></td>
    </tr>

    <tr>
     <td nowrap style="width:120px">Override Amount&nbsp;</td>
     <td><input type="text" name="pos_pta_pref_auth_amt" id="override_amount_id" value="${pp_pos_pta_pref_auth_amt }" size="15"></td>
    </tr>  
    <tr>
     <td nowrap>Override Limit</td>
     <td><input type="text" name="pos_pta_pref_auth_amt_max" id="override_limit_id" value="${pp_pos_pta_pref_auth_amt_max }" size="15"></td>
    </tr>

    <tr>
     <td colspan="2"><hr style="width:100%;height:1px;color:#CCCCCC"/></td>
    </tr>
    <tr>
     <td nowrap>Disable Debit Denial&nbsp;</td>
     <td>
   		<select name="pos_pta_disable_debit_denial">
   			<c:choose>
	   			<c:when test="${'Y' eq pp_pos_pta_disable_debit_denial}">
					<option value="N">No</option>
					<option value="Y" selected="selected">Yes</option>
				</c:when>
				<c:otherwise>
					<option value="N" selected="selected">No</option>
					<option value="Y">Yes</option>
				</c:otherwise>
			</c:choose>
   		</select>     
     </td>
    </tr>
    <tr>
     <td nowrap>No Two-Tier Pricing&nbsp;</td>
     <td>
   		<select name="no_convenience_fee">
   			<c:choose>
	   			<c:when test="${'Y' eq pp_no_convenience_fee}">
					<option value="N">No</option>
					<option value="Y" selected="selected">Yes</option>
				</c:when>
				<c:otherwise>
					<option value="N" selected="selected">No</option>
					<option value="Y">Yes</option>
				</c:otherwise>
			</c:choose>
   		</select>
   		Requires 6.0.8 FW for Gx, PLV 12 for G9, N/A for others
     </td>
    </tr>
  </table>
  <table class="tabDataDisplayNoBorder">
    <tr>
     <td nowrap colspan="2" class="label">&nbsp;<br/>Temporary Blacklisting</td>
    </tr>
    <tr>
     <td nowrap colspan="2"><hr style="width:100%;height:1px;color:#CCCCCC"/></td>
    </tr>
    <tr>
      <td nowrap>Whitelist</td>
      <td>
        <select name="whitelist" id="whitelist" onchange="clearDeclineUntil()">
          <c:choose>
            <c:when test="${'Y' eq pp_whitelist}">
            <option value="N">No</option>
            <option value="Y" selected="selected">Yes</option>
            </c:when>
            <c:otherwise>
            <option value="N" selected="selected">No</option>
            <option value="Y">Yes</option>
            </c:otherwise>
          </c:choose>
        </select>
      </td>
    </tr>
    <tr>
      <td nowrap>Decline Until</td>
      <td>
        <input type="text" name="declineUntilDate" id="declineUntilDate" value="${pp_decline_until_date}" size="10" maxlength="10" />
        <img src="/images/calendar.gif" id="declineUntilCalendar" class="calendarIcon" title="Date Selector" />
        <input type="text" name="declineUntilTime" id="declineUntilTime" size="8" maxlength="8" value="${pp_decline_until_time}" />
      </td>
    </tr>
   </table>
  </td>
 </tr>

	<c:set var="default_currency_cd" value="USD" />
   <c:choose>
	<c:when test="${not empty(pp_currency_cd)}">
		<c:set var="default_currency_cd" value="${pp_currency_cd}" />
	</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${toggle_cd < -1 || toggle_cd == 0}">
			<tr>
			  <td nowrap>Deactivation Date</td>
			  <td>${pp_pos_pta_deactivation_ts}&nbsp;</td>
			</tr>
		</c:when>
	</c:choose>
 <tr>
  <td nowrap>Authority Terminal Code</td>
  <td><input type="text" name="pos_pta_device_serial_cd" value="${pp_pos_pta_device_serial_cd}" size="29"/> <span id="terminalCodeMessage" class="note<%
  if(!systemManagedTerminalCd) {%> hide<%
  } %>">System&nbsp;Managed</span></td>
 </tr>	 
 <tr>

  <td nowrap>
   <table class="tabDataDisplayNoBorder">
    <tr>
     <td align="left">Authority Encryption Key&nbsp;&nbsp;&nbsp;</td>
     <td align="right"><input type="checkbox" name="pos_pta_encrypt_key_hex" value="1" ${pos_pta_encrypt_key_hex}>&nbsp;hex</td>
    </tr>
   </table>
  </td>

  <td><input type="text" name="pos_pta_encrypt_key" value="${pp_pos_pta_encrypt_key }" size="35"></td>
 </tr>	 
 <tr>
  <td nowrap>
   <table class="tabDataDisplayNoBorder">
    <tr>
     <td nowrap align="left">Authority Encryption Key 2</td>
     <td nowrap align="right"><input type="checkbox" name="pos_pta_encrypt_key2_hex" value="1" ${pos_pta_encrypt_key2_hex}>&nbsp;hex</td>

    </tr>
   </table>
  </td>
  <td><input type="text" name="pos_pta_encrypt_key2" value="${pp_pos_pta_encrypt_key2 }" size="35"></td>
 </tr>	 
 <tr>
   <td>Currency</td>	
   <td><select name="currency_cd" tabindex="1" id="currency_cd_select" class="smallText">
			<c:forEach var="nvp_item_currency" items="${toggle_currency}">
				<c:choose>
				<c:when test="${nvp_item_currency.value eq default_currency_cd}">
					<option value="${nvp_item_currency.value}" selected="selected">${nvp_item_currency.name}</option>
				</c:when>
				<c:otherwise>
					<option value="${nvp_item_currency.value}">${nvp_item_currency.name}</option>
				</c:otherwise>
				</c:choose>
			</c:forEach>
	  </select>
 </td>
 </tr>
 <tr>

  <td nowrap>PIN Required?</td>
  <td>
   <select name="pos_pta_pin_req_yn_flag">
   		<c:choose>
   		<c:when test="${not empty(pp_pos_pta_pin_req_yn_flag)}">
   			<c:choose>
	   			<c:when test="${'N' eq pp_pos_pta_pin_req_yn_flag}">
					<option value="N" selected="selected">No</option>
					<option value="Y">Yes</option>
				</c:when>
				<c:otherwise>
					<option value="N">No</option>
					<option value="Y" selected="selected">Yes</option>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<option value="N" selected="selected">No</option>
			<option value="Y">Yes</option>
		</c:otherwise>
		</c:choose>
   </select>
  </td>

 </tr>
 <tr>
  <td nowrap>Allow Authority Pass-through<br/>(on Auth Decline/Failure)?</td>
  <td>
  <select name="pos_pta_passthru_allow_yn_flag">
	  <c:choose>
   		<c:when test="${not empty(pp_pos_pta_passthru_allow_yn_flag)}">
   			<c:choose>
	   			<c:when test="${'N' eq pp_pos_pta_passthru_allow_yn_flag}">
					<option value="N" selected="selected">No</option>
					<option value="Y">Yes</option>
				</c:when>
				<c:otherwise>
					<option value="N">No</option>
					<option value="Y" selected="selected">Yes</option>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<option value="N" selected="selected">No</option>
			<option value="Y">Yes</option>
		</c:otherwise>
		</c:choose>
   </select>
  </td>
 </tr>
 <tr class="gridHeader">
  <td colspan="4" align="center">
   <input type="submit" class="cssButton" name="myaction" value="Save">&nbsp;
   <input type="reset" class="cssButton" value="Reset Form">&nbsp;
   <input type="button" class="cssButton" value="Go Back to Device Profile" onClick="javascript:window.location='paymentConfig.i?device_id=${device_id}&userOP=payment_types';">

  </td>
 </tr>
 </table>
</form>

 </div>
 </c:otherwise>
 </c:choose>
                  
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "dateTxt",             // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "toggle_date_trigger", // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });

    Calendar.setup({
        inputField     :    "declineUntilDate",    // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "declineUntilCalendar",// trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });

    var d = document.getElementById("dateTxt");
    
    function validateDate() {
    	if(!isDate(d.value)) {
    		d();
        	return false;
    	}
    	return true;
    }
    <%if(payment_subtype_table_key_ids.getRowCount() > 0) {%>
    function systemManageTerminalCode(frm, on) {
    	if(on) {
    		frm.pos_pta_device_serial_cd.disabled = true;
    		$("terminalCodeMessage").removeClass("hide");
    	} else {
    		frm.pos_pta_device_serial_cd.disabled = false;
    		$("terminalCodeMessage").addClass("hide");
    	}
    }
    window.addEvent("domready", function() {
    	window.attachOnSelect($("terminal"));
    });
    <%}%>
	function confirmSubmit() {
	   var terminalElem = document.getElementById('terminal');
	   if(terminalElem) {
           var terminal = terminalElem.value;
           if(terminal == '') {
              alert('Please Select A Terminal!');
              return false;
           }
		}
	   
   var declineUntilDate = document.getElementById("declineUntilDate");
   var declineUntilTime = document.getElementById("declineUntilTime");
   var whitelist = document.getElementById("whitelist");

   if (declineUntilDate.value || declineUntilTime.value) {
     if (whitelist.value == 'Y') {
       alert('You can not set Decline Until for a Whitelisted device!');
       return false;
     }
     
     if (!isDate(declineUntilDate.value)) {
       declineUntilDate.focus();
       return false;
     }
     
     if(!isTime(declineUntilTime.value)) {
       declineUntilTime.focus();
       return false;
     }
   }
       
   var agree=confirm("Are you sure you want to continue with this operation?");
   if (agree) {
               if(document.getElementById('override_amount_id').value == '' && document.getElementById('override_limit_id').value == '') {
                   return true;
               }
               if(document.getElementById('override_amount_id').value > 0 && document.getElementById('override_limit_id').value == '') {
                 alert('Please enter Override Limit');
                 return false;
               }
            if(document.getElementById('override_amount_id').value.match('^[0-9]+(\\.[0-9]{0,2})?\$') == null) {
           		alert("Invalid Override Amount");
                   return false;
               }
            if(document.getElementById('override_limit_id').value.match('^[0-9]+(\\.[0-9]{0,2})?\$') == null) {
           		alert("Invalid Override Limit");
                   return false;
               }
            
			return true;
		} else {
			return false;
           }
	}
	function adjust_days(myform) {
		// sets day field based on selected month and year
		var y = myform.toggle_year.options[myform.toggle_year.selectedIndex].value;
		var m = myform.toggle_month.selectedIndex;
		var oldDayIdx = myform.toggle_day.selectedIndex;
		var days;

		// find number of days in current month
		if ((m == 3) || (m == 5) || (m == 8) || (m == 10)) { days = 30; }
		else if (m == 1) {
			// check for leapyear - Any year divisible by 4, except those divisible by 100 (but NOT 400)
			if ((Math.floor(y/4) == (y/4)) && ((Math.floor(y/100) != (y/100)) || (Math.floor(y/400) == (y/400)))) { days = 29; }
			else { days = 28; }
		}
		else { days = 31; }

		// if (days in new month > current days) then we must add the extra days
		var curEndDateDayLength = myform.toggle_day.length;
		if (days > curEndDateDayLength) {
			myform.toggle_day.length = days;
			for (var i = curEndDateDayLength; i < days; i++) {
				myform.toggle_day.options[i].text = i + 1;
				myform.toggle_day.options[i].value = i + 1;
			}
		}

		// if (days in new month < current days) then we must delete the extra days
		if (days < myform.toggle_day.length) {
			if (oldDayIdx >= days) { myform.toggle_day.selectedIndex = days - 1; }
			myform.toggle_day.length = days;
		}
		return true;
	}
	function setCurrentDate(myform) {
		// changes the date selector menus to the current date
		var currentDate = new Date();
		myform.toggle_year.selectedIndex = 0;
		myform.toggle_month.selectedIndex = currentDate.getMonth();
		adjust_days(myform);
		return true;
	}
	
	function clearDeclineUntil() {
		var whitelist = document.getElementById("whitelist");
		if (whitelist.value == 'Y') {
			document.getElementById("declineUntilDate").value = "";
			document.getElementById("declineUntilTime").value = "";
		}
	}
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
