<%@page import="java.util.Collections"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

 		<div class="tableContainer">
 		<div class="tableHead">
		<div class="tabHeadTxt">Merchant Details</div>
		</div>
 		<% 
String merchant_id= request.getParameter("merchant_id");
if(merchant_id == null || (merchant_id.length()==0)){	
    %>
	<div align="center">			
	<span class="error">Required Parameter Not Found:merchant_id</span></div> 
    <% 		     
} else {
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
Results merchantList = (Results) inputForm.getAttribute("merchantList");
if(!merchantList.next()) {
	%>
    <div align="center">            
    <span class="error">Merchant <%=StringUtils.encodeForHTML(merchant_id) %> Not Found</span></div> 
    <%  
} else {
	     	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "authority_merchant");
	     	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.PARAM_MERCHANT_ID), ""));
	     	
	      Results terminalInfo = (Results) inputForm.getAttribute("terminalInfo");
	String merchantInfo[] = new String[11]; 
for(int i=0 ;i<merchantInfo.length; i++){
	merchantInfo[i] = merchantList.getFormattedValue(i+1);
}
%>
		<div class="gridHeader">Merchant</div>
		<form method="post" onsubmit="return validateForm(this);">
		<table class="tabDataDisplay">
			<tbody>
			<tr>
				<td class="label">Name</td>
				<td class="data"><input type="text" name="merchant_name" maxlength="255" style="width: 240px;" usatRequired="true" label="Name" value="<%=merchantInfo[2]%>" /></td>
				<td class="label">Description</td>
				<td class="data"><input type="text" name="merchant_desc" maxlength="255" style="width: 240px;" usatRequired="true" label="Description" value="<%=merchantInfo[3]%>" /></td>
			</tr>			
			<tr>
				<td class="label">Authority</td>
				<td class="data"><a href="authorityInfo.i?authority_id=<%=merchantInfo[8]%>"><%=merchantInfo[9]%></a></td>
				<td class="label">Business Name</td>
				<td class="data"><input type="text" name="merchant_bus_name" maxlength="255" style="width: 240px;" usatRequired="true" label="Business Name" value="<%=merchantInfo[4]%>" /></td>
			</tr>
			<tr>
                <td class="label">Doing Business As</td>
                <td class="data"><input type="text" name="doing_business_as" maxlength="21" style="width: 240px;" usatRequired="true" label="Doing Business As" value="<%=merchantInfo[10]%>" /></td>
                <td class="label">Code</td>
                <td class="data"><%=merchantInfo[1]%></td>
            </tr>
            <tr>
                <td class="label">Merchant Category Code</td>
                <td class="data"><input type="text" name="mcc" maxlength="4" style="width: 240px;" label="Merchant Category Code" value="<%Integer mcc = merchantList.getValue("mcc", Integer.class); if(mcc != null) {%><%=mcc %><%} %>" /></td>
                <td class="label">Authority Assigned Number</td>
                <td class="data"><input type="text" name="authority_assigned_num" maxlength="30" style="width: 240px;" label="Authority Assigned Number" value="<%=StringUtils.prepareCDATA(merchantList.getValue("authority_assigned_num", String.class))%>" /></td>
            </tr>
            <tr>
                <td class="label">Merchant Group</td>
                <td class="data"><%
                        long authorityId = merchantList.getValue("m_authority_id", Long.class);
                        String merchantGroupCd = merchantList.getValue("merchant_group_cd", String.class); 
                  boolean found = false;
                  %><select name="merchant_group_cd" onchange="this.form.new_merchant_group_cd.disabled=(this.selectedIndex != 1)">
                    <option value=" "<%if(StringUtils.isBlank(merchantGroupCd)) { found = true; %> selected="selected"<%} %>></option>
                    <option value="">-- New Value --</option><%
                Results merchantGroups = DataLayerMgr.executeQuery("GET_MERCHANT_GROUPS", Collections.singletonMap("authority_id", authorityId));
                while(merchantGroups.next()) {
                	String optionMerchantGroupCd = merchantGroups.getValue("merchant_group_cd", String.class);
                	%><option value="<%=StringUtils.prepareCDATA(optionMerchantGroupCd) %>"<%if(ConvertUtils.areEqual(merchantGroupCd, optionMerchantGroupCd)) { found = true; %> selected="selected"<%} %>><%=StringUtils.prepareHTML(optionMerchantGroupCd) %></option><%
                }
                %></select>
                <input type="text" name="new_merchant_group_cd" maxlength="30" style="width: 240px;" label="Merchant Group" value="<%=StringUtils.prepareCDATA(merchantGroupCd)%>"<%if(found) {%> disabled="disabled"<%} %>/></td>
            </tr><%
			Long minPosPtaNum = merchantList.getValue("min_pos_pta_num", Long.class);
			Long maxPosPtaNum = merchantList.getValue("max_pos_pta_num", Long.class);
			Long usedPosPtaNum = merchantList.getValue("used_pos_pta_num", Long.class);
			String posPtaCdExp = merchantList.getValue("pos_pta_cd_exp", String.class);
			if(!StringUtils.isBlank(posPtaCdExp) && minPosPtaNum != null && maxPosPtaNum != null) { %>
			<tr>
                <td class="label">Slots Used</td>
                <td class="data" colspan="3"><%=usedPosPtaNum%> of <%=(1L + maxPosPtaNum - minPosPtaNum) %> [<%=(1L + maxPosPtaNum - minPosPtaNum - usedPosPtaNum)%> available]</td>
            </tr><%} %>          
            <tr>
				<td class="label">Created</td>
				<td class="data"><%=merchantInfo[6]%></td>
				<td class="label">Last Updated</td>
				<td class="data"><%=merchantInfo[7]%></td>
			</tr>
			<tr>
				<td colspan="4" align="center">
					<input type="submit" class="cssButton" name="action" value="Save" style="width: 100px;" />
					<input type="submit" class="cssButton" name="action" value="Delete" style="width: 100px;" onclick="return window.confirm('Are you sure you want to continue with this operation?');" />
					<input type="button" class="cssButton" value="Add Terminal" onclick="window.location = 'addTerminal.i?merchant_id=<%=merchantInfo[0]%>';" />
					<input type="button" class="cssButton" value="List Devices" onClick="javascript:window.location = 'deviceList.i?merchant_id=<%=merchantInfo[0]%>';" />
				</td>						
			</tr>
			</tbody>
	    </table>
	    </form>
		<div class="gridHeader">Terminals</div>
	       <table class="tabDataDisplay">
				<tr class="gridHeader">
					<td align="center" width="10%">ID</td>
					<td align="center" width="20%">Code</td>
					<td align="center" width="30%">Name</td>
					<td align="center" width="15%">Next Batch Num</td>
					<td align="center" width="25%">Current State</td>
				</tr>
			<tbody>
			<%
			int i = 0;
	         		while(terminalInfo.next()) { 
	         		%>
			<tr class="<%=(i++%2==0)?"row1":"row0"%>">
				<td><%=terminalInfo.getFormattedValue(1)%></td>
				<td><a href="terminalInfo.i?terminal_id=<%=terminalInfo.getFormattedValue(1)%>"><%=terminalInfo.getFormattedValue(2)%></a></td>
				<td><%=terminalInfo.getFormattedValue(7)%></td>
				<td><%=terminalInfo.getFormattedValue(5)%></td>
				<td><%=terminalInfo.getFormattedValue(8)%></td>
			</tr>
			<% } %>
			</tbody>
			</table>
			<%}
}%>
			
	  </div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
