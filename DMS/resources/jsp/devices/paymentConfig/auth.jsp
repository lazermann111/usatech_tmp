<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.action.CardAction" %>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
			<div class="tableContent">
				<%String auth_id= request.getParameter("auth_id");
						if(auth_id == null || (auth_id.length()==0)){	
						    %>
							<div align="center">			
							<span class="error">Required Parameter Not Found:auth_id</span></div> 
						    <% 		     
				      }else{
				        InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
				        Results authinfo = (Results) inputForm.getAttribute("authinfo");
				        Results settlementdetailinfo = (Results) inputForm.getAttribute("settlementdetailinfo");				        
				      	String auth[] = new String[37]; 
						for(int i=0 ;i< auth.length; i++){
						auth[i] = "";
						}		
						while(authinfo.next()){ 
						for(int i=0 ;i<authinfo.getColumnCount(); i++){
							auth[i] = authinfo.getFormattedValue(i+1);
						}			           
					  %>
					<div class="gridHeader" align="center"><b>Transaction Details</b></div>
				<table class="tabDataDisplayBorder">
					<col width="15%" />
					<col width="35%" />
					<col width="15%" />
					<col width="35%" />
					<tbody>
					<tr>
						<td class="label">Detail ID</td>
						<td class="data"><%=auth[0]%></td>
						<td class="label">Type</td>
						<td class="data"><%=auth[2]%></td>
					</tr>
					<tr>
						<td class="label">State</td>
						<td class="data"><%=auth[3]%></td>
						<td class="label">Account Data</td>
						<td class="data"><%=CardAction.maskCreditCard(auth[4])%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Amount Requested</td>
						<td class="data"><%=auth[6]%>&nbsp;<%if (auth[19] != null && auth[19].length() > 0 && auth[6] != null && !auth[6].equalsIgnoreCase(auth[19])) {%>(<%=auth[19]%> Override)<%}%></td>
						<td class="label">Entry Method</td>
						<td class="data"><%=auth[5]%></td>
					</tr>
					<tr>
						<td class="label">Amount Approved</td>
						<td class="data"><%=auth[17]%>&nbsp;<%if (auth[20] != null && auth[20].length() > 0 && auth[17] != null && !auth[17].equalsIgnoreCase(auth[20])) {%>(<%=auth[20]%> Override)<%}%></td>
						<td class="label">Result</td>
						<td class="data"><%=auth[8]%></td>
					</tr>
					<tr>
						<td class="label">Detail Timestamp</td>
						<td class="data"><%=auth[7]%>&nbsp;</td>
						<td class="label">Response Code</td>
						<td class="data"><%=auth[9]%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Authority Timestamp</td>
						<td class="data"><%=auth[13]%>&nbsp;</td>
						<td class="label">Response Message</td>
						<td class="data"><%=StringUtils.prepareCDATA(auth[10])%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Created</td>
						<td class="data"><%=auth[14]%></td>
						<td class="label">Authorization Code</td>
						<td class="data"><%=auth[11]%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Last Updated</td>
						<td class="data"><%=auth[15]%>&nbsp;<%=auth[16]%></td>
						<td class="label">Reference Number</td>
						<td class="data"><%=auth[12]%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Terminal Batch ID</td>
						<td class="data"><%if (!StringHelper.isBlank(auth[16])) { %><a href="terminalBatch.i?terminal_batch_id=<%=auth[16]%>"><%=auth[16]%></a><%} %>&nbsp;</td>
						<td class="label">Transaction ID</td>
						<td class="data"><a href="tran.i?tran_id=<%=auth[1]%>"><%=auth[1]%></a></td>
					</tr>
					<tr>
						 <td class="label">Trace Number</td>
						 <td class="data"><%=auth[21]%>&nbsp;</td>
						 <td class="label">Balance Amount</td>
						 <td class="data"><%=auth[22]%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Misc Data</td>
						<td class="data" colspan="3"><%=auth[18] == null ? "" : auth[18].replace(StringUtils.FS, ' ')%>&nbsp;</td>
					</tr>					
					<%if(!StringHelper.isBlank(auth[23])){ %>
						<tr>
						  <td class="label">Action</td>
						  <td class="data" colspan="3"><%=auth[24]%>&nbsp;</td>
						</tr>
					<% }%>					
					</tbody>
                 </table>
				 <div class="spacer10"></div>
				 <% if ((auth[23]!=null && auth[23].length()>0) && (auth[25]!=null && auth[25].length()>0) ) {	 
				       Results deviceAction = (Results) inputForm.getAttribute("deviceAction");%>
				 <div class="gridHeader" align="center">Action Parameters</div>   
                <table class="tabDataDisplayBorderNoFixedLayout">
                   <thead>
						<tr class="gridHeader">
							<td align="center" width="50%">Action Parameter</td>
							<td align="center" width="50%">Bit Index</td>							
						</tr>
				   </thead>
				   <tbody>
					<% while(deviceAction.next()){%>
					<tr>
						<td><%=deviceAction.getFormattedValue(1)%></td>
						<td><%=deviceAction.getFormattedValue(2)%></td>						
					</tr>
					<%} %>
					</tbody>
				</table>
			    <div class="spacer10"></div>
			    <%} %>
			    <%} %>
			    
			    <%if (!StringUtils.isBlank(auth[31])) {%>
					<div class="gridHeader" align="center"><b>Chase Processing Details</b></div>
					<table class="tabDataDisplayBorder">
						<col width="15%" />
						<col width="35%" />
						<col width="15%" />
						<col width="35%" />
						<tbody>					
						<tr>
							<td class="label">DFR File ID</td>
							<td class="data"><a href="/DFRFile.i?dfrFileId=<%=auth[31]%>"><%=auth[31]%></a></td>
							<td class="label">Entity #</td>
							<td class="data"><%=auth[33]%></td>
						</tr>
						<tr>
							<td class="label">Submission #</td>
							<td class="data"><%=auth[34]%></td>
							<td class="label">Record #</td>
							<td class="data"><%=auth[35]%></td>
						</tr>
						<tr>
							<td class="label">Submission Date</td>
							<td class="data"><%=auth[32]%></td>
							<td class="label">Interchange Code</td>
							<td class="data"><%=auth[27]%></td>
						</tr>
						<tr>
							<td class="label">Fee Description</td>
							<td class="data"><%=auth[29]%></td>
							<td class="label">Fee Amount</td>
							<td class="data"><%=auth[30]%></td>
						</tr>
						<tr>
							<td class="label">Method of Payment</td>
							<td class="data"><%=auth[26]%></td>
							<td class="label">Downgrade Reason Code</td>
							<td class="data"><%=auth[28]%></td>
						</tr>
						<%if (!StringUtils.isBlank(auth[36])) {%>
						<tr>
							<td class="label">Downgrade Reason Description</td>
							<td class="data" colspan="3"><%=auth[36]%></td>
						</tr>
						<%}%>
						</tbody>			    
					</table>
					<div class="spacer10"></div>	
			    <%}%>
			    
           		<div class="gridHeader" align="center"><b>Settlements</b></div>
                    <table class="tabDataDisplayBorderNoFixedLayout">
                    <thead>
						<tr class="gridHeader">
							<td>ID</td>
							<td>Settlement Date</td>
							<td>Result</td>
						</tr>
					</thead>
					<tbody>
					<% while(settlementdetailinfo.next()){%>
					<tr>
						<td><a href="settlementBatch.i?settlement_batch_id=<%=settlementdetailinfo.getFormattedValue(1)%>"><%=settlementdetailinfo.getFormattedValue(1)%></a></td>
						<td><%=settlementdetailinfo.getFormattedValue(2)%></td>
						<td><%=settlementdetailinfo.getFormattedValue(4)%></td>
					</tr>
					<%} %>
					</tbody>
				
					</table>
					<%} %>
				</div>
				  
                <div class="spacer5"></div>
              
             </div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
