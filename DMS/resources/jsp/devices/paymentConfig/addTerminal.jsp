<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.transaction.TranConstants"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

 		<div class="tableContainer">
 		<div class="tableHead">
		<div class="tabHeadTxt">Add Terminal</div>
		</div>
		<div class="gridHeader">New Terminal</div>
		<form method="post" onsubmit="return validateForm(this);">
		<table class="tabDataDisplay">
			<tr>
				<td class="label">Name</td>
				<td class="data"><input type="text" name="terminal_desc" maxlength="255" style="width: 240px;" usatRequired="true" label="Name" /></td>
				<td class="label">Code</td>
				<td class="data"><input type="text" name="terminal_cd" maxlength="255" style="width: 240px;" usatRequired="true" label="Code" /></td>
			</tr>			
			<tr>
				<td class="label">Merchant</td>
				<td class="data" colspan="3">
					<select name="terminal_merchant_id">
					<%Results merchants = DataLayerMgr.executeQuery("GET_MERCHANT_FORMATTED", null);
					String merchantId = inputForm.getStringSafely("merchant_id", "").trim();
					while (merchants.next()) {%>
						<option value="<%=merchants.getFormattedValue("aValue")%>"<%if (merchantId.length() > 0 && merchantId.equalsIgnoreCase(merchants.getFormattedValue("aValue"))) out.write(" selected=\"selected\"");%>><%=merchants.getFormattedValue("aLabel")%></option>
					<%}%>
					</select>
				</td>
			</tr>
			<tr>
				<td class="label">Batch Cycle Number</td>
				<td class="data"><input type="text" name="terminal_batch_cycle_num" maxlength="10" style="width: 240px;" value="1" usatRequired="true" label="Batch Cycle Number" /></td>
				<td class="label">Next Batch Number</td>
				<td class="data"><input type="text" name="terminal_next_batch_num" maxlength="20" style="width: 240px;" value="1" usatRequired="true" label="Next Batch Number" /></td>				
			</tr>			
			<tr>				
				<td class="label">Min Batch Number</td>
				<td class="data"><input type="text" name="terminal_min_batch_num" maxlength="20" style="width: 240px;" value="1" usatRequired="true" label="Min Batch Number" /></td>
				<td class="label">Max Batch Number</td>
				<td class="data"><input type="text" name="terminal_max_batch_num" maxlength="20" style="width: 240px;" value="999" usatRequired="true" label="Max Batch Number" /></td>
			</tr>
			<tr>				
				<td class="label">Min Transaction Count</td>
				<td class="data"><input type="text" name="terminal_batch_min_tran" maxlength="20" style="width: 240px;" value="4999" usatRequired="true" label="Min Transaction Count" /></td>
				<td class="label">Max Transaction Count</td>
				<td class="data"><input type="text" name="terminal_batch_max_tran" maxlength="20" style="width: 240px;" value="4999" usatRequired="true" label="Max Transaction Count" /></td>
			</tr>
			<tr>				
				<td class="label">Min Hours Open</td>
				<td class="data"><input type="text" name="terminal_min_batch_close_hr" maxlength="5" style="width: 240px;" value="12" usatRequired="true" label="Min Hours Open" /></td>
				<td class="label">Max Hours Open</td>
				<td class="data"><input type="text" name="terminal_max_batch_close_hr" maxlength="5" style="width: 240px;" value="12" usatRequired="true" label="Max Hours Open" /></td>
			</tr>
			<tr>
				<td colspan="4" align="center">
					<input type="submit" name="action" value="Add Terminal" class="cssButton" />
				</td>				
			</tr>
	    </table>
	    </form>
	  </div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
