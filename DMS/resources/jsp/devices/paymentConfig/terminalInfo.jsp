<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
if(missingParam != null && missingParam.booleanValue() == true) {
%>
<div class="tableContainer"><div align="center">	
<span class="error">Required parameter not found: terminal_id</span></div>
</div>
<%
} else {
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "authority_terminal");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.PARAM_TERMINAL_ID), ""));
	
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
    Results terminaldetailInfo = (Results) inputForm.getAttribute("terminaldetailInfo");

String terminaldetail[] = new String[21]; 
for(int i=0 ;i< terminaldetail.length; i++){
terminaldetail[i] = " ";
}	

int colCount = terminaldetailInfo.getColumnCount();
if(terminaldetailInfo.next()){ 
	for(int i=0 ;i<terminaldetailInfo.getColumnCount(); i++){
		terminaldetail[i] = terminaldetailInfo.getFormattedValue(i+1);
	}
}
terminaldetailInfo.setRow(0);
%>

		<div class="tableContainer">
			<div class="tableHead">
			<div class="tabHeadTxt">Terminal Details</div>
			</div>
				<% if(terminaldetailInfo.next()) { %>
				<div class="gridHeader">Terminal</div>
				<form method="post" onsubmit="return validateForm(this);">
				<table class="tabDataDisplay">
				<tbody>
					<tr>
						<td class="label">Name</td>
						<td class="data"><input type="text" name="terminal_desc" maxlength="255" style="width: 240px;" usatRequired="true" label="Name" value="<%=terminaldetail[6]%>" /></td>
						<td class="label">Code</td>
						<td class="data"><%=terminaldetail[1]%></td>						
					</tr>
					<tr>
						<td class="label">Authority</td>
						<td class="data"><a href="authorityInfo.i?authority_id=<%=terminaldetail[20]%>"><%=terminaldetail[19]%></a></td>
						<td class="label">Merchant</td>
						<td class="data"><a href="merchantInfo.i?merchant_id=<%=terminaldetail[5]%>"><%=terminaldetail[13]%></a></td>
					</tr>
					<tr>
						<td class="label">Batch Cycle Number</td>
						<td class="data"><%=terminaldetail[10]%></td>
						<td class="label">Next Batch Number</td>
						<td class="data"><%=terminaldetail[4]%></td>
					</tr>
					<tr>
						<td class="label">Min Batch Number</td>
						<td class="data"><%=terminaldetail[18]%></td>
						<td class="label">Max Batch Number</td>
						<td class="data"><%=terminaldetail[8]%></td>
					</tr>
					<tr>				
						<td class="label">Min Transaction Count</td>
						<td class="data"><input type="text" name="terminal_batch_min_tran" maxlength="20" style="width: 240px;" usatRequired="true" label="Min Transaction Count" value="<%=terminaldetail[17]%>" /></td>
						<td class="label">Max Transaction Count</td>
						<td class="data"><input type="text" name="terminal_batch_max_tran" maxlength="20" style="width: 240px;" usatRequired="true" label="Max Transaction Count" value="<%=terminaldetail[9]%>" /></td>
					</tr>
					<tr>				
						<td class="label">Min Hours Open</td>
						<td class="data"><input type="text" name="terminal_min_batch_close_hr" maxlength="5" style="width: 240px;" usatRequired="true" label="Min Hours Open" value="<%=terminaldetail[15]%>" /></td>
						<td class="label">Max Hours Open</td>
						<td class="data"><input type="text" name="terminal_max_batch_close_hr" maxlength="5" style="width: 240px;" usatRequired="true" label="Max Hours Open" value="<%=terminaldetail[16]%>" /></td>
					</tr>
					<tr>
						<td class="label">ID</td>
						<td class="data"><%=terminaldetail[0]%></td>
						<td class="label">State</td>
						<td class="data"><%=terminaldetail[7]%></td>
					</tr>					
					<tr>
						<td class="label">Created</td>
						<td class="data"><%=terminaldetail[11]%></td>
						<td class="label">Last Updated</td>
						<td class="data"><%=terminaldetail[12]%></td>						
					</tr>
					<tr>
						<td class="label">Batch Close Settings</td>
						<td colspan="3">
							<i>
								Close batches if they contain at least <%=terminaldetail[17]%>
								transactions and have been open for at
								least <%=terminaldetail[15]%> hours, or <%=terminaldetail[9]%> transactions, or <%=terminaldetail[16]%>
								hours
							</i>
						</td>
					</tr>
					<tr>
						<td colspan="4" align="center">
							<input type="submit" class="cssButton" name="action" value="Save" style="width: 100px;" />
							<input type="submit" class="cssButton" name="action" value="Delete" style="width: 100px;" onclick="return window.confirm('Are you sure you want to continue with this operation?');" />
							<input type="button" class="cssButton" value="List Devices" onClick="javascript:window.location = 'deviceList.i?terminal_id=<%=terminaldetail[0]%>';">
						</td>						
					</tr>					
				</tbody>
			</table>
			</form>
			<% } 
				String sortField = PaginationUtil.getSortField(null);
			    String sortIndex = inputForm.getString(sortField, false);
			    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
			    Results terminalbatchInfo = (Results) request.getAttribute("terminalbatchInfo");
				int i = 0; 
				String rowClass = "row0";
				
			%>
			
			<div class="gridHeader">Terminal Batches</div>
			<table class="tabDataDisplayBorderNoFixedLayout">
				<thead>
					<tr class="sortHeader">
					    <td>
				            <a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">ID</a>
				            <%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			            </td>
			            <td>
				            <a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Batch Num</a>
				            <%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			            </td>
			            <td>
				            <a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Time Opened</a>
				            <%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			            </td>
			            <td>
				            <a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Time Closed</a>
				            <%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			            </td>
					</tr>
				</thead>
				<tbody>
				<%
				while(terminalbatchInfo.next()) {
					rowClass = (i%2 == 0) ? "row1" : "row0";
					i++; %>
					<tr class="<%=rowClass%>">
						<td><%=terminalbatchInfo.getFormattedValue(1)%></td>
						<td>
							<a href="terminalBatch.i?terminal_batch_id=<%=terminalbatchInfo.getFormattedValue(1)%>"><%=terminalbatchInfo.getFormattedValue(3)%></a>
						</td>
						<td><%=terminalbatchInfo.getFormattedValue(4)%></td>
						<td><%=terminalbatchInfo.getFormattedValue(5)%></td>
					</tr>
				<% } %>
				</tbody>
			</table>

<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{DevicesConstants.PARAM_TERMINAL_ID});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="terminalInfo.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

</div>
<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
