<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
			<div class="tableContent">
				<%boolean paramFlag = true;
				  String device_id= request.getParameter("device_id");
				  String action=request.getParameter("action");
				  if(action == null || action.length() == 0){
				      action = request.getParameter("myaction");
				      if(action == null || action.length() == 0){
						%>
						<div align="center">			
						<span class="error">Required Parameter Not Found:action</span></div> 
					    <% 
					    paramFlag = false;
				      }
			      }
				  
						if(device_id == null || device_id.length() == 0){	
							%>
							<div align="center">	 		
						    <span class="error">Required Parameter Not Found:device_id</span></div> 
						    <% 
						    paramFlag = false;
				      }
					if (paramFlag){	
					    boolean subtypesFlag = true;
					    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
				        Results ptsubtypes = (Results) inputForm.getAttribute("ptsubtypes");
						if (ptsubtypes == null || !ptsubtypes.next()){
						    subtypesFlag = false;
						    %>
							<div align="center">			
							<span class="error">No unique payment types available to be added at this time. </span></div> 
						    <% 
						    
				      }else {
					  %>
					<div class="gridHeader" class="gridHeader" align="center">Point-of-Sale Payment Type - <%=StringUtils.encodeForHTML(action) %></div>
					<form method="post" action="addPtaSuccess.i">
					<input type="hidden" name="device_id" value="<%=StringUtils.encodeForHTMLAttribute(device_id) %>"/>				
					<table class="tabDataDisplay">
					<tbody>	
					<tr>
                      <td nowrap>Payment Type</td>
                      <td colspan="3">
                      <%if (subtypesFlag) {%>
		                  <select name="payment_subtype_id">
			                   <option value="">-- Choose a type --</option>
 		                       <%
 		                      ptsubtypes.setRow(0);
 		                       while (ptsubtypes.next()) {%>
		                           <option value="<%= ptsubtypes.getFormattedValue(1) %>"><%= ptsubtypes.getFormattedValue(2)%></option>
			                   <%} %>
			              </select>
			             <% 
		}
		else
		{
		    %>   <input type="hidden" name="payment_subtype_id" value="N/A"/>N/A
		    <% 
		}
                      %>
		
  </td>
 </tr> 
 <tr>
  <td width="25%" nowrap>Device ID</td>
  <td width="25%"><%=StringUtils.encodeForHTML(device_id)%></td>
  <td width="25%">Duplicate settings from most recently disabled payment type of the same type and code (if&nbsp;any&nbsp;exist)</td>
  <td width="25%">
   <select name="pos_pta_duplicate_settings">
		<option value="Y" selected>Yes</option>
		<option value="N">No</option>		
   </select>
  </td>
 </tr>
 <tr>
  <td colspan="4" align="center">
   <input type="submit"  class="cssButton" name="myaction" value="Add / Reactivate"/>&nbsp;
   <input type="button"  class="cssButton" value="Go Back to Device Profile" onClick="javascript:window.location='paymentConfig.i?device_id=<%=StringUtils.encodeForURL(device_id)%>&userOP=payment_types';"/>
  </td>
 </tr>
					</tbody>
					
                 </table>
                 </form>
                 <%} }%> 
                </div>
                <div class="spacer5"></div>
              
             </div>
            

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
