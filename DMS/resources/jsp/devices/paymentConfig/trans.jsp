<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.Iterator" %>
<%@page import="com.usatech.dms.action.CardAction" %>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%
String tran_id = request.getParameter("tran_id");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "transaction");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, tran_id);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
			<div class="tableContent">
						<%
						String tran_global_trans_cd = request.getParameter("tran_global_trans_cd");
						String processingItemA[] = new String[21];
		         		String processingItemB[] = new String[21];
		         		String processingItemC[] = new String[21];
						if(StringHelper.isBlank(tran_id) && StringHelper.isBlank(tran_global_trans_cd)){	
						    %>
							<div align="center">			
							<span class="error">Required Parameter Not Found: tran_id or tran_global_trans_cd</span></div> 
						    <% 
				      }else{
				          InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
			    	        Results traninfo = (Results) inputForm.getAttribute("traninfo");
			    	        Results transuminfo = (Results) inputForm.getAttribute("transuminfo");
			    	        Results lineIteminfo = (Results) inputForm.getAttribute("lineIteminfo");
			    	        Results processinginfo = (Results) inputForm.getAttribute("processinginfo");
			    	        Results refundprocessinginfo = (Results) inputForm.getAttribute("refundprocessinginfo"); 
			    	        Results tranrefundinfo = (Results) inputForm.getAttribute("tranrefundinfo");
                            Results ledgerInfo = (Results) inputForm.getAttribute("ledgerInfo");
			    	     String payment_subtype_table_name = inputForm.getString("payment_subtype_table_name", false);
				         String tran[] = new String[59]; 
				         String terminal[] = new String[8];
				         String lineItem[] = new String[16];
				         
						 for(int i=0 ;i< tran.length; i++){
						 	tran[i] = " ";
						 }
						 for(int i=0 ;i< terminal.length; i++){
						 	terminal[i] = " ";
						 }	
						 for(int i=0 ;i< lineItem.length; i++){
						 	lineItem[i] = " ";
						 }	
						 for(int i=0 ;i< processingItemA.length; i++){
						 	processingItemA[i] = " ";
						 }	
						 for(int i=0 ;i< processingItemB.length; i++){
						 	processingItemB[i] = " ";
						 }	
						 for(int i=0 ;i< processingItemC.length; i++){
						 	processingItemC[i] = " ";
						 }	
						 if (traninfo.next()){ 
						 	for(int i=0 ;i<traninfo.getColumnCount(); i++){
								tran[i] = traninfo.getFormattedValue(i+1);
							}
						 }
						 String actualCount = "0";
						 String actualAmount = "$0";
						 String intendedCount = "0";
						 String intendedAmount = "$0";
						 while (transuminfo.next()){
							if ("A".equalsIgnoreCase(transuminfo.getFormattedValue("tran_line_item_batch_type_cd"))) {
								actualCount = transuminfo.getFormattedValue("count");
								actualAmount = ("$" + transuminfo.getFormattedValue("tran_line_item_amount")).replace("$-", "-$");
							} else if ("I".equalsIgnoreCase(transuminfo.getValue("tran_line_item_batch_type_cd", String.class))) {
								intendedCount = transuminfo.getFormattedValue("count");
								intendedAmount = ("$" + transuminfo.getFormattedValue("tran_line_item_amount")).replace("$-", "-$");
							}
						 }
					  %>
					<div class="gridHeader" align="center"><b>Transaction</b></div>
					<form action="tranUpdate.i" method="post">
					<input type="hidden" name="tran_id" value="<%=StringUtils.encodeForHTMLAttribute(tran_id)%>" />
					<table class="tabDataDisplayBorder">
					<tbody>					
					<tr>
						<td class="label">Tran ID</td>
						<td class="data"><%=tran[0]%></td>
						<td class="label">Start Time</td>
						<td class="data"><%=tran[1]%></td>
					</tr>
					<tr>
						<td class="label">Reference Tran ID</td>
						<td class="data"><a href="tran.i?tran_id=<%=tran[35]%>"><%=tran[35]%></a></td>
						<td class="label">End Time</td>
						<td class="data"><%=tran[3]%></td>
					</tr>
					<tr>
						<td class="label">Created</td>
						<td class="data"><%=tran[8]%></td>
						<td class="label">Upload Time</td>
						<td class="data"><%=tran[2]%></td>
					</tr>
					<tr>
						<td class="label">Last Updated</td>
						<td class="data"><%=tran[9]%></td>
						<td class="label">Consumer Account ID</td>
						<td class="data"><%if (tran[6]!=null && tran[6].length() > 0){ %><a href="editConsumerAcct.i?consumer_acct_id=<%=tran[6]%>"><%=tran[6]%></a><%} %>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">State</td>
						<td class="data"><%=tran[4]%></td>
						<td class="label">Result</td>
						<td class="data"><%=tran[16]%><%= tran[17] != null && tran[17].length() > 0 ? ":" : "" %><%=tran[17]%>&nbsp;</td>
					</tr>
					<%if ("Y".equals(tran[57])) {%>
					<tr>
						<td class="label">Change State To</td>
						<td colspan="3">
							<select name="new_tran_state_cd">
								<%
								Results results = DataLayerMgr.executeQuery("GET_TRAN_STATES_FOR_RESET", new Object[] {tran[56]});
								while (results.next()) {
								%>
								<option value="<%=results.getFormattedValue("TRAN_STATE_CD")%>"><%=results.getFormattedValue("TRAN_STATE_DESC")%></option>
								<%}%>
							</select>
							<input type="submit" name="action" value="Change" class="cssButton" onclick="return confirm('Please confirm your action')" />
						</td>
					</tr>
					<%}%>
					<tr>
						<td class="label">Device Tran ID</td>
						<td class="data"><%=tran[7]%></td>
						<td class="label">Global Trans No</td>
						<td class="data"><%=tran[18]%></td>
					</tr>
					<tr>
						<td class="label">Auth Call ID</td>
						<td class="data"><%if (tran[48] != null && tran[48].length() > 0) {%><a href="/deviceCallInLog.i?session_cd=<%=tran[48] %>"><%=tran[53]%></a><%} %>&nbsp;</td>
						<td class="label">Client Payment Type</td>
						<td class="data"><%=tran[29]%>:<%=tran[30]%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Sale Call ID</td>
						<td class="data"><%if (tran[50] != null && tran[50].length() > 0) {%><a href="/deviceCallInLog.i?session_cd=<%=tran[50] %>"><%=tran[54]%></a><%} %>&nbsp;</td>
						<td class="label">Payment Entry Method</td>
						<td class="data"><%=tran[39]%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Account Data</td>
						<td class="data"><%=CardAction.maskCreditCard(tran[11])%></td>
						<td class="label">Payment Type</td>
						<td class="data"><%=tran[38]%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Auth Hold Used</td>
						<td class="data"><%=tran[49]%>&nbsp;</td>
						<td class="label">Device Serial #</td>
						<td class="data"><a href="profile.i?device_id=<%=tran[31]%>"><%=tran[33]%></a></td>
					</tr>
					<tr>
						<td class="label">Imported</td>
						<td class="data"><%=tran[51]%>&nbsp;</td>
						<td class="label">Device Type</td>
						<td class="data"><%=tran[34]%>&nbsp;</td>
					</tr>
					<tr>
						<td class="label">Intended Purchase</td>
						<td class="data"><%=intendedCount%>/<%=intendedAmount%></td>
						<td class="label">Actual Purchase</td>
						<td class="data"><%=actualCount%>/<%=actualAmount%></td>
					</tr>
					<tr>
						<td class="label">Payment Type</td>
						<td class="data"><%=tran[27]%></td>
						<%if(payment_subtype_table_name!=null && payment_subtype_table_name.equals("TERMINAL")) {
						    Results terminaldetail = (Results) inputForm.getAttribute("terminaldetail");
						    if (terminaldetail !=null && terminaldetail.next()){ 
							 	for(int i=0 ;i<terminaldetail.getColumnCount(); i++){
							 	   terminal[i] = terminaldetail.getFormattedValue(i+1);
								}
						    }
						%>
							<td class="label">Terminal</td>
							<td class="data"><a href="terminalInfo.i?terminal_id=<%=terminal[0]%>"><%=terminal[1]%></a></td>
							</tr>
							<tr>
								<td class="label">Authority</td>
								<td class="data"><a href="authorityInfo.i?authority_id=<%=terminal[6]%>"><%=terminal[7]%></a></td>
								<td class="label">Merchant</td>
								<td class="data"><a href="merchantInfo.i?merchant_id=<%=terminal[2]%>"><%=terminal[4]%></a></td>
							</tr>
						<%} else {%>
							<td class="label">Payment Category</td>
							<td class="data"><%=tran[36]%></td>
						
						<%} %>
						<%if(tran[40]!=null && tran[40].length() > 0){ %>
						<tr>
							<td class="label">Sale Type</td>
							<td class="data"><%=tran[40]%></td>
							<td class="label">Sale Result</td>
							<td class="data"><%=tran[44]%></td>
						</tr>
						<tr>
							<td class="label">Sale Start Time</td>
							<td class="data"><%=tran[42]%></td>
							<td class="label">Sale End Time</td>
							<td class="data"><%=tran[43]%></td>
						</tr>
						<tr>
							<td class="label">Sale Amount</td>
							<td class="data">$<%=tran[45]%></td>
							<td class="label">Device Batch ID</td>
							<td class="data"><%=tran[41]%></td>
						</tr>
						<tr>
							<td class="label">Receipt Result</td>
							<td class="data"><%=tran[46]%></td>
							<td class="label">Duplicate Count</td>
							<td class="data"><%=tran[47]%></td>
						</tr>
						<%}%>
						<tr>
							<td class="label">Payment Action Type</td>
							<td class="data"><%=tran[55]%></td>
							<td class="label">Consumer Pass ID</td>
							<td class="data"><%=tran[58]%></td>
						</tr>
					<%if (!StringHelper.isBlank(tran[52])) {%>
						<tr>
							<td class="label">Tran Info</td>
							<td class="data" colspan="3"><%=tran[52]%>&nbsp;</td>
						</tr>
					<%}%>	
					</tbody>
                 </table>
				 </form>

                <div class="spacer10"></div>
				<div class="gridHeader" align="center"><b>Line Items</b></div>
                    <table class="tabDataDisplayBorderNoFixedLayout">
                    <thead>
					<tr class="gridHeader">
						<td>Line Item ID</td>
						<td>Host ID</td>
						<td>Batch Type</td>
						<td>Item Type</td>
						<td>Purchase</td>
						<td>Description</td>
						<td>Timestamp</td>
						<td>Sale Result</td>
					</tr>
					</thead>
					<tbody>
					<%
					HashMap hash = new HashMap();
					while(lineIteminfo.next()){
						for(int i=0 ;i<lineIteminfo.getColumnCount(); i++){
							lineItem[i] = lineIteminfo.getFormattedValue(i+1);
						}
						String key = lineItem[6] + " " + lineItem[4] + " " + lineItem[12] ;
						if (lineItem[11]!=null && lineItem[11].equals("I")){
						    hash.put(key,new String[]{lineItem[0],lineItem[1],lineItem[2],lineItem[3],lineItem[4],lineItem[5],lineItem[6],lineItem[7],lineItem[8],lineItem[9],lineItem[10],lineItem[11],lineItem[12],lineItem[13],lineItem[14]});
						}else{
						  String[] data = (String[])hash.get(key);
						%>
					
					<tr>
					<td><%=lineItem[0]%></td>
					<td><a href="hostProfile.i?host_id=<%=lineItem[6]%>"><%=lineItem[6]%></a></td>
					<td><%=lineItem[9]%></td>
					<td><%=lineItem[8]%></td>
					<td>
					<%
					if(data!=null)
		            {
					%>
					    Intended: <%=data[5]%>/$<%=data[1]%>, Actual: <%=lineItem[5]%>/<%if ("N".equalsIgnoreCase(lineItem[15])) out.write("-");%>$<%=lineItem[1]%>
		  	        <% 
		  	            hash.remove(key);
		            }
		            else
		            {
		             %>
		             <%=lineItem[5]%>/<%if ("N".equalsIgnoreCase(lineItem[15])) out.write("-");%>$<%=lineItem[1]%>&nbsp;
		            <% }%>
		            </td>
					<td><%=lineItem[3]%></td>
					<td><%=lineItem[13]%></td>
					<td><%=lineItem[14]%></td>
					</tr>
					<%} }
					Iterator keys = hash.keySet().iterator();
					
					while (keys.hasNext()){
					    String[] data = (String[])hash.get(keys.next());
	               %>
	               <tr>
	               <td align="left"><%=data[0]%>&nbsp;</td>
	               <td align="left"><a href="hostProfile.i?host_id=<%=data[6]%>"><%=data[6]%></a>&nbsp;</td>
	               <td align="left"><%=data[9]%>&nbsp;</td>
	               <td align="left"><%=data[8]%>&nbsp;</td>
	               <td align="left" nowrap><%=data[5]%>/$<%=data[1]%>&nbsp;</td>
	               <td align="left"><%=data[3]%>&nbsp;</td>
	               <td align="left"><%=data[13]%>&nbsp;</td>
	               <td align="left"><%=data[14]%>&nbsp;</td>
	               </tr>
                   <%} %>
					</tbody>
					</table>
				
			    <div class="spacer10"></div>
				<div class="gridHeader" align="center"><b>Processing Details</b></div>
                <table class="tabDataDisplayBorderNoFixedLayout">
                    <thead>
					<tr class="gridHeader">
						<td>Detail ID</td>
						<td>Type</td>
						<td>State</td>
						<td>Amount</td>
						<td>Timestamp</td>
						<td>Result</td>
					</tr>
					</thead>					
					<tbody>
					<%while(processinginfo.next()){
						for(int i=0 ;i<processinginfo.getColumnCount(); i++){
							processingItemA[i] = processinginfo.getFormattedValue(i+1);
						}
							 %>
					<tr>
						<td><a href="auth.i?auth_id=<%=processingItemA[0]%>"><%=processingItemA[0]%></a></td>
						<td><%=processingItemA[2]%></td>
						<td><%=processingItemA[3]%></td>
						<td>$<%=processingItemA[6]%>&nbsp;<%if (processingItemA[19]!=null && processingItemA[19].length()>0 && processingItemA[6] != null && !processingItemA[6].equalsIgnoreCase(processingItemA[19])){%>($<%=processingItemA[19] %> Override)<%} %></td>
						<td><%=processingItemA[7]%></td>
						<td><%=processingItemA[8]%></td>
					</tr>
					<%}
					while(refundprocessinginfo.next()){
						for(int i=0 ;i<refundprocessinginfo.getColumnCount(); i++){
							processingItemB[i] = refundprocessinginfo.getFormattedValue(i+1);
						}
					%>
					<tr>
						<td><a href="refund.i?refund_id=<%=processingItemB[0]%>"><%=processingItemB[0]%></a></td>
						<td><%=processingItemB[2]%></td>
						<td><%=processingItemB[3]%></td>
						<td>$<%=processingItemB[6]%></td>
						<td><%=processingItemB[7]%></td>
						<td><%=processingItemB[18]%></td>
					</tr>
					<%} 
					while(tranrefundinfo.next()){
						for(int i=0 ;i<tranrefundinfo.getColumnCount(); i++){
							processingItemC[i] = tranrefundinfo.getFormattedValue(i+1);
						}
					%>
					<tr>
						<td><a href="refund.i?refund_id=<%=processingItemC[0]%>"><%=processingItemC[0]%></a></td>
						<td><%=processingItemC[2]%></td>
						<td><%=processingItemC[3]%></td>
						<td>$<%=processingItemC[6]%></td>
						<td><%=processingItemC[7]%></td>
						<td><%=processingItemC[18]%></td>
					</tr>
					<%} %>
					</tbody>
			    </table>
			    
          <div class="spacer10"></div>
          <div class="gridHeader" align="center"><b>Ledger Details</b></div>
          <table class="tabDataDisplayBorderNoFixedLayout">
            <thead>
          <tr class="gridHeader">
            <td>Ledger ID</td>
            <td>EFT ID</td>
            <td>Customer</td>
            <td>Timestamp</td>
            <td>Entry Type</td>
            <td>Ledger Status</td>
            <td>EFT Status</td>
            <td>Fee</td>
            <td>Ledger Amount</td>
          </tr>
          </thead>
          <tbody>
          <%
          while (ledgerInfo.next()){
            String entryType = ledgerInfo.getFormattedValue("entry_type");
                 %>
             <tr>
               <td align="left"><%=ledgerInfo.getFormattedValue("ledger_id")%>&nbsp;</td>
               <td align="left"><a href="/manageEFT.i?eftId=<%=ledgerInfo.getFormattedValue("doc_id")%>"><%=ledgerInfo.getFormattedValue("doc_id")%></a>&nbsp;</td>
               <td align="left"><a href="/customer.i?customerId=<%=ledgerInfo.getFormattedValue("customer_id")%>"><%=ledgerInfo.getFormattedValue("customer_name")%></a>&nbsp;</td>
               <td align="left"><%=ledgerInfo.getFormattedValue("ledger_date")%>&nbsp;</td>
               <td align="left"><%=ledgerInfo.getFormattedValue("entry_type_name")%>&nbsp;</td>
               <td align="left"><%=ledgerInfo.getFormattedValue("settle_state_name")%>&nbsp;</td>
               <td align="left"><%=ledgerInfo.getFormattedValue("doc_status_name")%>&nbsp;</td>
               <% if (entryType.equals("PF")) { %> 
                     <td align="left"><%=ledgerInfo.getFormattedValue("fee_desc")%>&nbsp;</td>
               <% } else if (entryType.equals("PC")) { %>
                     <td align="left"><%=ledgerInfo.getFormattedValue("commission_desc")%>&nbsp;</td>
               <% } else { %>
                     <td align="left">&nbsp;</td>
               <% } %>
               <td align="left"><%=ledgerInfo.getFormattedValue("ledger_amount")%>&nbsp;</td>
             </tr>
          <%} %>
          </tbody>
          </table>
          
			    <%} %>
				</div>
               <div class="spacer5"></div>
              
             </div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
