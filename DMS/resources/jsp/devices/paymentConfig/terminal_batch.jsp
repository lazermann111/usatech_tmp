<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
			<div class="tableContent">
					<% Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
                      if(missingParam != null && missingParam.booleanValue() == true) {
                    %>
                      <div align="center">		
                  <span class="error">Required parameter not found: terminal_batch_id</span>
                </div>
                 <%
                  } else {
                      InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
		    	        Results terminalinfo = (Results) inputForm.getAttribute("terminalinfo");
		    	        Results settlementinfo = (Results) inputForm.getAttribute("settlementinfo");
		    	        Results refundsuminfo = (Results) inputForm.getAttribute("refundsuminfo");
		    	        Results refundsinfo = (Results) inputForm.getAttribute("refundsinfo");
		    	        Results authinfo = (Results) inputForm.getAttribute("authinfo");
		    	        Results reversalTotals = (Results) inputForm.getAttribute("reversalTotals");
                      	String terminal_batch_id = (String)request.getParameter("terminal_batch_id");
					  %>
					<div class="gridHeader" align="center"><b>Terminal Batch</b></div>
				<table class="tabDataDisplayBorder">
				<tbody>
				<%
            				if(terminalinfo.next()  ) { 
            				   refundsuminfo.next();
            				   authinfo.next();
            				   reversalTotals.next();
            	%>
					<tr>
						<td class="label">Terminal Batch ID</td>
						<td class="data"><%=StringUtils.encodeForHTML(terminal_batch_id)%></td>
						<td class="label">Terminal Batch Number</td>
						<td class="data"><%=terminalinfo.getFormattedValue(1)%></td>
					</tr>
					<tr>
						<td class="label">Created</td>
						<td class="data"><%=terminalinfo.getFormattedValue(7)%></td>
						<td class="label">Last Updated</td>
						<td class="data"><%=terminalinfo.getFormattedValue(8)%></td>
					</tr>
					<tr>
						<td class="label">Batch Open Time</td>
						<td class="data"><%=terminalinfo.getFormattedValue(4)%></td>
						<td class="label">Terminal Batch Cycle Count</td>
						<td class="data"><%=terminalinfo.getFormattedValue(3)%></td>
					</tr>
					<tr>
						<td class="label">Batch Close Time</td>
						<td class="data"><%=terminalinfo.getFormattedValue(5)%></td>
						<td class="label">Sale Totals</td>
						<td class="data"><%=authinfo.getFormattedValue(2)%>/$<%=authinfo.getFormattedValue(1)%></td>
					</tr>
					<tr>
						<td class="label">Terminal Name</td>
						<td class="data"><%=terminalinfo.getFormattedValue(2)%></td>
						<td class="label">Auth Reversal Totals</td>
						<td class="data"><%=reversalTotals.getFormattedValue(2)%>/$<%=reversalTotals.getFormattedValue(1)%></td>
					</tr>
					<tr>
						<td class="label">Terminal Code</td>
						<td class="data"><a href="terminalInfo.i?terminal_id=<%=terminalinfo.getFormattedValue(6)%>"><%=terminalinfo.getFormattedValue(9)%></a></td>
						<td class="label">Refund Totals</td>
						<td class="data"><%=refundsuminfo.getFormattedValue(2)%>/$<%=refundsuminfo.getFormattedValue(1)%></td>
					</tr>
					<tr>
						<td class="label">Merchant</td>
						<td class="data"><a href="merchantinfo.i?merchant_id=<%=terminalinfo.getFormattedValue(11)%>"><%=terminalinfo.getFormattedValue(12)%></a></td>
						<td class="label">Terminal State</td>
						<td class="data"><%=terminalinfo.getFormattedValue(10)%></td>
					</tr>
					<%} %>
					</tbody>
                 </table>
             	<div class="spacer10"></div>
				<div class="gridHeader" align="center"><b>Settlements</b></div>
                    <table class="tabDataDisplayBorderNoFixedLayout">
                   		 <thead>
							<tr class="gridHeader">
								<td align="center" width="20%">ID</td>
								<td align="center" width="40%">Settlement Date</td>
								<td align="center" width="40%">Result</td>
							</tr>
						</thead>
						<tbody>
						<%
            				while(settlementinfo.next()) { %>
							<tr>
								<td><a href="settlementBatch.i?settlement_batch_id=<%=settlementinfo.getFormattedValue(1)%>"><%=settlementinfo.getFormattedValue(1)%></a></td>
								<td><%=settlementinfo.getFormattedValue(2)%></td>
								<td><%=settlementinfo.getFormattedValue(4)%></td>
							</tr>
							<%} %>
						</tbody>
					</table>
			<% 
			String sortField = PaginationUtil.getSortField(null);
		    String sortIndex = inputForm.getString(sortField, false);
		    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "-2" : sortIndex;
		    Results salesinfo = (Results) request.getAttribute("salesinfo");
			%>
				<div class="spacer10"></div>
				<div class="gridHeader" align="center"><b>Sales</b></div>
                    <table class="tabDataDisplayBorderNoFixedLayout">
                    	<thead>
							<tr class="sortHeader">
							    <td>
				                    <a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Detail ID</a>
				                    <%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			                    </td>
						        <td>
				                    <a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Tran ID</a>
				                    <%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			                    </td>
			                    <td>
				                    <a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Type</a>
				                    <%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			                    </td>
			                    <td>
				                    <a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Detail State</a>
				                    <%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			                    </td>
						        <td>
				                    <a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Transaction State</a>
				                    <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			                    </td>
			                    <td>
				                    <a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Amount</a>
				                    <%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			                    </td>	
			                    <td>
				                    <a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Timestamp</a>
				                    <%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			                    </td>	
							</tr>
						</thead>
						<tbody>
							<%
							int i = 0; 
							String rowClass = "row0";
            				while(salesinfo.next()) { 
            				    rowClass = (i%2 == 0) ? "row1" : "row0";
            					i++;
            				%>
							<tr class="<%=rowClass%>">
								<td><a href="auth.i?auth_id=<%=salesinfo.getFormattedValue(1)%>"><%=salesinfo.getFormattedValue(1)%></a></td>
								<td><a href="tran.i?tran_id=<%=salesinfo.getFormattedValue(2)%>"><%=salesinfo.getFormattedValue(2)%></a></td>
								<td><%=salesinfo.getFormattedValue(3)%></td>
								<td><%=salesinfo.getFormattedValue(4)%></td>
								<td><%=salesinfo.getFormattedValue(20)%></td>
								<td>$<%=salesinfo.getFormattedValue(7)%></td>
								<td><%=salesinfo.getFormattedValue(8)%></td>
							</tr>
							<%} %>
						</tbody>
					</table>
					<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{DevicesConstants.PARAM_TERMINAL_BATCH_ID});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="terminalBatch.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

				<div class="gridHeader" align="center"><b>Refunds</b></div>
                <table class="tabDataDisplayBorderNoFixedLayout">
                    	<thead>
                    	   <tr class="gridHeader">
								<td align="center" width="8%">Refund ID</td>
								<td align="center" width="8%">Tran ID</td>
								<td align="center" width="12%">Type</td>
								<td align="center" width="23%">Refund State</td>
								<td align="center" width="21%">Transaction State</td>
								<td align="center" width="8%">Amount</td>
								<td align="center" width="20%">Timestamp</td>
							</tr>							
						</thead>
						<tbody>
						<%
            				while(refundsinfo.next()) { %>
							<tr>
								<td><a href="refund.i?refund_id=<%=refundsinfo.getFormattedValue(1)%>"><%=refundsinfo.getFormattedValue(1)%></a></td>
								<td><a href="tran.i?tran_id=<%=refundsinfo.getFormattedValue(2)%>"><%=refundsinfo.getFormattedValue(2)%></a></td>
								<td><%=refundsinfo.getFormattedValue(7)%></td>
								<td><%=refundsinfo.getFormattedValue(10)%></td>
								<td><%=refundsinfo.getFormattedValue(20)%></td>
								<td>$<%=refundsinfo.getFormattedValue(3)%></td>
								<td><%=refundsinfo.getFormattedValue(5)%></td>
							</tr>
							<%} %>
						</tbody>
						
					</table>
					<%} %>
				</div>
				  
                <div class="spacer5"></div>
              
             </div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
