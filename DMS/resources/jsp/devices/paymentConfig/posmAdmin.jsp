<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.List"%>

<jsp:useBean id="dms_values_list_adminCmdTypeList" class="com.usatech.dms.util.GenericList" scope="application" />

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
		
<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">POSM Administration</div>
</div>
<form method="post">
<table class="tabDataDisplay">
	<tr>
		<td width="30%">Command</td>
		<td>
			<select name="admin_cmd" id="admin_cmd" onchange="selectCommand(this);">
				<option value="">Please select command</option>
				<%
				List<NameValuePair> nvp_list_comm_methods = dms_values_list_adminCmdTypeList.getList();
		    	Iterator<NameValuePair> commMethodIt = nvp_list_comm_methods.iterator();
		    	while(commMethodIt.hasNext()) {
		    		NameValuePair nvp = commMethodIt.next();
		    	%>
		    	<option value="<%=nvp.getValue()%>"><%=nvp.getName()%></option>
		    	<% } %>
			</select>
		</td>
	</tr>
	<tr id="paramRow1" style="display: none;">
		<td id="paramName1"></td>
		<td><input type="text" name="param1" maxlength="4000" class="largeInput" /></td>
	</tr>
	<tr id="paramRow2" style="display: none;">
		<td id="paramName2"></td>
		<td><input type="text" name="param2" maxlength="4000" class="largeInput" /></td>
	</tr>
	<tr class="gridHeader">
		<td colspan="2" align="center">
			<input type="submit" name="action" class="cssButton" value="Submit" />
		</td>	
	</tr>
</table>
</form>

</div>

<script type="text/javascript">
function selectCommand(command) {
	var params = command.value.split("_");
	for (var i = 1; i < params.length; i++) {
		if (params[i].length == 0)
			document.getElementById("paramRow" + i).style.display = "none";
		else {
			document.getElementById("paramName" + i).innerHTML = params[i];
			document.getElementById("paramRow" + i).style.display = "";
		}
	}
}
</script>
	
<jsp:include page="/jsp/include/footer.jsp" flush="true" />