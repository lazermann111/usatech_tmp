<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">Authority Details</div>
		</div>
				<% Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
if(missingParam != null && missingParam.booleanValue() == true) {
    %>
    <div align="center">
    <span class="error">Required parameter not found: authority_id</span></div>
    <%
    } else {
        InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
        Results authinfo = (Results) inputForm.getAttribute("authinfo");
        Results authgatewayinfo = (Results) inputForm.getAttribute("authgatewayinfo");  
        if(authinfo.next()) {
        Long authorityId = authinfo.getValue("authority_id", Long.class);
%>
			<div class="gridHeader">Authority</div>
			<table class="tabDataDisplayBorder">
				<tbody>
				<tr>
					<td class="label">Name</td>
					<td class="data"><%=StringUtils.prepareHTML(authinfo.getValue("authority_name", String.class))%></td>
					<td class="label">ID</td>
					<td class="data"><%=authorityId%></td>
				</tr>
				<tr>
					<td class="label">Type</td>
					<td class="data"><%=StringUtils.prepareHTML(authinfo.getValue("authority_type_name", String.class))%></td>
					<td class="label">Description</td>
					<td class="data"><%=StringUtils.prepareHTML(authinfo.getValue("authority_type_desc", String.class))%></td>
				</tr><%
				String posPtaCdExp =  authinfo.getValue("pos_pta_cd_exp", String.class);              
				Long minPosPtaNum =  authinfo.getValue("min_pos_pta_num", Long.class);
				Long maxPosPtaNum =  authinfo.getValue("max_pos_pta_num", Long.class);              
				if(!StringUtils.isBlank(posPtaCdExp) && minPosPtaNum != null && maxPosPtaNum != null) { %>
	            <tr>
                    <td class="label" rowspan="2">Authority Terminal<br/>Code Expression</td>
                    <td class="data" rowspan="2"><%=StringUtils.prepareHTML(posPtaCdExp) %></td>               
                    <td class="label">Minimum Authority Terminal Num</td>
                    <td class="data"><%=minPosPtaNum%></td> 
                </tr>
                <tr>
	                <td class="label">Maximum Authority Terminal Num</td>
	                <td class="data"><%=maxPosPtaNum%></td> 
	            </tr>
                <%} %>
                <tr>
					<td class="label">Created</td>
					<td class="data"><%=StringUtils.prepareHTML(authinfo.getValue("created_ts", String.class))%></td>
					<td class="label">Last Updated</td>
					<td class="data"><%=StringUtils.prepareHTML(authinfo.getValue("last_updated_ts", String.class))%></td>
				</tr>
				<tr>
					<td colspan="4" align="center">
						<input type="button" class="cssButton" value="List Devices" onClick="javascript:window.location = 'deviceList.i?authority_id=<%=authorityId%>';">
						<input type="button" class="cssButton" value="Add Merchant" onclick="window.location = 'addMerchant.i?authority_id=<%=authorityId%>';" />
					</td>						
				</tr>
			</tbody>
            </table>
           	
			<div class="gridHeader">Authority Gateways</div>
			<table class="tabDataDisplayBorder">
				<thead>
					<tr class="gridHeader">
						<td align="center" width="10%">ID</td>
						<td align="center" width="40%">Name</td>
						<td align="center" width="40%">Address</td>
						<td align="center" width="10%">Priority</td>
					</tr>
				</thead>
				<tbody>
				<%
            		while(authgatewayinfo.next()) { %>
					<tr>
						<td><%=authgatewayinfo.getFormattedValue("authority_gateway_id")%></td>
						<td>
							<a href="authorityGateway.i?authority_gateway_id=<%=authgatewayinfo.getFormattedValue("authority_gateway_id")%>"><%=authgatewayinfo.getFormattedValue("authority_gateway_name")%></a>
						</td>
						<td><%=authgatewayinfo.getFormattedValue("authority_gateway_addr")%></td>
						<td><%=authgatewayinfo.getFormattedValue("authority_gateway_priority")%></td>
					</tr>					
				<% } %>
				</tbody>
			</table>
            <% 
			String sortField = PaginationUtil.getSortField(null);
		    String sortIndex = inputForm.getString(sortField, false);
		    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "3" : sortIndex;
		    Results merchantinfo = (Results) request.getAttribute("merchantinfo");
			%>
			
			<div class="gridHeader">Merchants</div>
			<table class="tabDataDisplayBorder">
				<thead>
					<tr class="sortHeader">
					   <td>
				            <a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">ID</a>
				            <%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			            </td>
						<td>
				            <a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Code</a>
				            <%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			            </td>
			            <td>
				            <a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Name</a>
				            <%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			            </td>						
                        <td>
                            <a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">MCC</a>
                            <%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
                        </td>                       
                        <td>
                            <a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Group</a>
                            <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
                        </td>                       
					</tr>
				</thead>
				<tbody>
				<% 
				
				int i = 0; 
				String rowClass = "row0";
				while(merchantinfo.next()) {
					rowClass = (i%2 == 0) ? "row1" : "row0";
					i++;
			%>
					<tr class="<%=rowClass%>">
						<td><%=merchantinfo.getFormattedValue("merchant_id")%></td>
						<td>
							<a href="merchantinfo.i?merchant_id=<%=merchantinfo.getFormattedValue("merchant_id")%>"><%=merchantinfo.getFormattedValue("merchant_cd")%></a>
						</td>
						<td><%=merchantinfo.getFormattedValue("merchant_name")%></td>
						<td><%=merchantinfo.getFormattedValue("mcc")%></td>
						<td><%=merchantinfo.getFormattedValue("merchant_group_cd")%></td>
					</tr>
				<% } %>
				</tbody>
			</table>
			<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{DevicesConstants.PARAM_AUTHORITY_ID});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="authorityInfo.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>
<% } else {%>
    <div align="center">
    <span class="error">Authority <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "authority_id", String.class, false)) %> not found</span></div>
<% }
}%>
</div>


<jsp:include page="/jsp/include/footer.jsp" flush="true" />
