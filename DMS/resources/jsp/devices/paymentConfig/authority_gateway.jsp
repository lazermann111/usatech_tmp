<%@page import="com.usatech.dms.user.AuthenticationStep"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="tableContainer">
	<div class="tableContent">
		<% 
		String authority_gateway_id= request.getParameter("authority_gateway_id");
		if(authority_gateway_id == null || (authority_gateway_id.length()==0)){	
		    %>
			<div align="center">			
			<span class="error">Required Parameter Not Found:authority_gateway_id</span></div> 
		    <% 		     
        }else{
            InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);  
	        Results authgatewayinfo = (Results) inputForm.getAttribute("authgatewayinfo");
	        Results authoritiesinfo = (Results) inputForm.getAttribute("authoritiesinfo");
	        Results authserverinfo = (Results) inputForm.getAttribute("authserverinfo");
            String authgateway[] = new String[7]; 
            for(int i=0 ;i< authgateway.length; i++){
                authgateway[i] = " ";
            }		
            if (authgatewayinfo.next()){ 
               for(int i=0 ;i<authgatewayinfo.getColumnCount(); i++){
	               authgateway[i] = authgatewayinfo.getFormattedValue(i+1);
               }
            }

        %>
		
		<div class="gridHeader" align="center"> 
			<b>Authority Gateway</b>
		</div>
		<table class="tabDataDisplayBorder">
			<tbody>
				<tr>
					<td class="label">ID</td>
					<td class="data"><%=authgateway[0]%></td>
					<td class="label">Name</td>
					<td class="data"><%=authgateway[1]%>	</td>
				</tr>
				<tr>
					<td class="label">Address</td>
					<td class="data"><%=authgateway[2]%></td>
					<td class="label">Port</td>
					<td class="data"><%=authgateway[3]%></td>
				</tr>
				<tr>
					<td class="label">Created</td>
					<td class="data"><%=authgateway[5]%></td>
					<td class="label">Last Updated</td>
					<td class="data"><%=authgateway[6]%></td>
				</tr>
				
			</tbody>
		</table>
		<div class="spacer10"></div>
		<div class="gridHeader" align="center">
			<b>Authorities</b>
		</div>
		<div class="spacer10"></div>
		<table class="tabDataDisplayBorderNoFixedLayout">
			<thead>
				<tr class="gridHeader">
					<td align="center" width="20%">ID</td>
					<td align="center" width="50%">Name</td>
					<td align="center" width="30%">Type</td>
				</tr>
			</thead>
			<tbody>
			<%
            		while(authoritiesinfo.next()) { %>
				<tr>
					<td><%=authoritiesinfo.getFormattedValue(1)%></td>
					<td><a href="authorityInfo.i?authority_id=<%=authoritiesinfo.getFormattedValue(1)%>"><%=authoritiesinfo.getFormattedValue(2)%></a></td>
					<td><%=authoritiesinfo.getFormattedValue(3)%></td>
				</tr>
				<% } %>
			</tbody>
		</table>
		<div class="spacer10"></div>
		<div class="gridHeader" align="center">
			<b>Authority Servers</b>
		</div>
		<div class="spacer10"></div>
		<table class="tabDataDisplayBorderNoFixedLayout">
			<thead>
				<tr class="gridHeader">
					<td align="center" width="15%">ID</td>
					<td align="center" width="50%">Name</td>
					<td align="center" width="20%">Address</td>
					<td align="center" width="15%">Priority</td>
				</tr>
			</thead>
			<tbody>
			<%
            		while(authserverinfo.next()) { %>
				<tr>
					<td><%=authserverinfo.getFormattedValue(1)%></td>
					<td><a href="authorityServerInfo.i?authority_server_id=<%=authserverinfo.getFormattedValue(1)%>"><%=authserverinfo.getFormattedValue(2)%></a></td>
					<td><%=authserverinfo.getFormattedValue(3)%>:<%=authserverinfo.getFormattedValue(4)%></td>
					<td><%=authserverinfo.getFormattedValue(5)%></td>
				</tr>
				<%} %>
			</tbody>
		</table>
		<%} %>
	</div>
	<div class="spacer5">&nbsp;	</div>
</div>



<jsp:include page="/jsp/include/footer.jsp" flush="true" />
