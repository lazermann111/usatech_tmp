<%@page import="simple.text.StringUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>

<%
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "bank_account");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("accountId"), ""));

BigDecimal customerId = RequestUtils.getAttribute(request, "customerId", BigDecimal.class, false);
BigDecimal accountId = RequestUtils.getAttribute(request, "accountId", BigDecimal.class, false); 
Results accounts = RequestUtils.getAttribute(request, "accounts", Results.class, false);
Results account = RequestUtils.getAttribute(request, "account", Results.class, false);
Results cycles = RequestUtils.getAttribute(request, "cycles", Results.class, true);
Results tcount = RequestUtils.getAttribute(request, "tcount", Results.class, false);
Results states = RequestUtils.getAttribute(request, "states", Results.class, true);
Results countries = RequestUtils.getAttribute(request, "countries", Results.class, true);

if (account!=null && !account.next())
	account = null;

String state, countryCd;
if(account!=null) {
	state = account.getFormattedValue("state");
	countryCd = account.getFormattedValue("countryCd");
} else {
	state = "";
	countryCd = "";
}

BigDecimal payCycleId;
if(account!=null)
	payCycleId = account.getValue("PAY_CYCLE_ID", BigDecimal.class);
else
	payCycleId = new BigDecimal(-1);

int terminalCount = 0;
if (tcount!=null && tcount.next())
	terminalCount = tcount.getValue("tcount", Integer.class);
%>

<table>
	<tbody>
	
		<tr>
			<td class="label">Customer Name</td>
			<td>
					<%if (account!=null){ %>
						<a href="/customer.i?customerId=<%=account.getFormattedValue("CUSTOMER_ID")%>"><%=StringUtils.prepareHTML(account.getFormattedValue("CUSTOMER_NAME"))%></a>
					<%}%>
				
				<input type="hidden" name="customerId"
					<%if (account!=null){ %>
						value="<%=account.getFormattedValue("CUSTOMER_ID")%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Bank Account ID</td>
			<td>
					<%if (account!=null){ %>
						<%=account.getFormattedValue("CUSTOMER_BANK_ID")%>
					<%}%>
			</td>
		</tr>
		
		<tr>
			<td class="label">Customer Tax ID</td>
			<td class="control">
				<input type="text" name="taxIdNbr" id=""taxIdNbr"" size="12" maxlength="12" onchange="onDetailChange()" usatRequired="true" label="Bank Tax Id Number"
					<%if (account!=null){%>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("taxIdNbr"))%>"
					<%}%>
			 	/><input type="hidden" name="page_code" id="page_code" value="EDIT_BANK_ACCOUNT"><input id="useTaxIdForAll" name="useTaxIdForAll" type="checkbox" value="Y" /> Use This Tax Id For All Other Existing Banks For This Customer</td>
			 </td>
		</tr>
		<tr>
			<td class="label">Billing Address 1</td>
			<td class="control">
				<input type="text" name="address1" id="address1" size="50" maxlength="255" onchange="onDetailChange()"
					<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("address1"))%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Billing Address 2</td>
			<td class="control">
				<input type="text" name="address2" id="address2" size="50" maxlength="255" onchange="onDetailChange()"
					<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("address2"))%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Billing Postal Code</td>
			<td class="control">
				<input type="text" name="zip" id="zip" size="20" maxlength="20" onchange="onDetailChange()" onblur="getPostalInfo('postalCd=' + this.value);"
					<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("zip"))%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Billing City</td>
			<td class="control">
				<input type="text" name="city" id="city" size="50" maxlength="50" onchange="onDetailChange()"
					<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("city"))%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Billing State</td>
			<td class="control">
				<select name="state" id="state" style="width: 340px;" onchange="onStateChange(this)" >
					<option value="">Please select:</option>
				<%if (states != null){ while(states.next()) {%>						
					<option value="<%=StringUtils.prepareCDATA(states.getFormattedValue("STATE_CD"))%>" data-countryCd="<%=StringUtils.prepareCDATA(states.getFormattedValue("COUNTRY_CD"))%>"<%if (account != null && state.equals(states.getFormattedValue("STATE_CD"))) {%> selected="selected"<%}%>>
					<%=StringUtils.prepareHTML(states.getFormattedValue("STATE_NAME"))%>: <%=StringUtils.prepareHTML(states.getFormattedValue("STATE_CD"))%>, <%=StringUtils.prepareHTML(states.getFormattedValue("COUNTRY_CD"))%>
					</option>
				<%}}%>
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">Billing Country</td>
			<td class="control">
				<select name="country" id="country" style="width: 340px;" onchange="onDetailChange()" >
					<option value="US" >Please select:</option>
				<%if (countries != null){ while(countries.next()) {%>						
					<option value="<%=StringUtils.prepareCDATA(countries.getFormattedValue("COUNTRY_CD"))%>"<%if (account != null && countryCd.equals(countries.getFormattedValue("COUNTRY_CD"))) {%> selected="selected"<%}%>>
					<%=StringUtils.prepareHTML(countries.getFormattedValue("COUNTRY_NAME"))%>: <%=StringUtils.prepareHTML(countries.getFormattedValue("COUNTRY_CD"))%>
					</option>
				<%}}%>
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">Bank Account #</td>
			<td class="control">
				<input type="text" name="accountNbr" id="accountNbr" size="20" maxlength="20" onchange="onDetailChange()" usatRequired="true" label="Bank Account #"
					<%if (account!=null){%>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("BANK_ACCT_NBR"))%>"
					<%}%>
			 	/>
			 </td>
		</tr>
		
		<tr>
			<td class="label">Bank Routing #</td>
			<td class="control">
				<input type="text" name="routeNbr" id="routeNbr" size="20" maxlength="20" onchange="onDetailChange()" usatRequired="true" label="Bank Routing #"
								<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("BANK_ROUTING_NBR"))%>"
					<%}%>
				/>			
			</td>
		</tr>
		
		<tr>
			<td class="label">Account Title</td>
			<td class="control">
				<input type="text" name="bankTitle" id="bankTitle" size="50" maxlength="50" onchange="onDetailChange()"
					<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("ACCOUNT_TITLE"))%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Bank Name</td>
			<td class="control">
				<input type="text" name="bankName" id="bankName" size="50" maxlength="50" onchange="onDetailChange()"
					<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("BANK_NAME"))%>"
					<%}%>
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Account Type</td>
			<td class="control">
			
				<input type="radio" name="accountType" id="accountType" value="C" onchange="onDetailChange()"
					<%if (account!=null && account.getFormattedValue("ACCOUNT_TYPE").equals("C")){ %>
						" checked "
					<%}%>
				> Checking&nbsp;
				<input type="radio" name="accountType" id="accountType" value="S" onchange="onDetailChange()"
					<%if (account!=null && account.getFormattedValue("ACCOUNT_TYPE").equals("S")){ %>
						" checked "
					<%}%>
				> Saving
			</td>
		</tr>
		
		<tr>
			<td class="label" valign="top">Description</td>
			<td class="control">
				<textarea name="desc" id="desc" rows="5" cols="50" onchange="onDetailChange()"><%if (account!=null){ %><%=StringUtils.prepareHTML(account.getFormattedValue("DESCRIPTION"))%><%}%></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="label">EFT Memo Prefix</td>
			<td class="control">
				<input type="text" name="memo" id="memo" size="25" maxlength="25" onchange="onDetailChange()"
					<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("EFT_PREFIX"))%>"
					<%}%>
				/>				
			</td>
		</tr>
		
		<tr>
			<td class="label">Minimum Amount</td>
			<td class="control">
				<input type="text" name="minAmount" id="minAmount" size="20" maxlength="20" onchange="onDetailChange()" usatRequired="true"  label="Minimum Amount"
					<%if (account!=null){ %>
						value="<%=StringUtils.prepareCDATA(account.getFormattedValue("PAY_MIN_AMOUNT"))%>"
					<%}%>	
				/>
			</td>
		</tr>
		
		<tr>
			<td class="label">Payment Type</td>
			<td class="control">
				<input type="radio" name="paymentType" id="paymentType" value="Y" onchange="onDetailChange()"
					<%if (account!=null && account.getFormattedValue("IS_EFT").equals("Y")){ %>
						" checked "
					<%}%>
				> EFT&nbsp;
				<input type="radio" name="paymentType" id="paymentType" value="N" onchange="onDetailChange()"
					<%if (account!=null && !account.getFormattedValue("IS_EFT").equals("Y")){ %>
						" checked "
					<%}%>
				> Check
			</td>
		</tr>
		
		<tr>
			<td class="label">Pay Cycle</td>
			<td class="control">
				<select name="payCycle" id="payCycle" style="width: 240px;" onchange="onDetailChange()" usatRequired="true"  label="Pay Cycle">
				<%if (cycles!=null){ while(cycles.next()) {
				%><option value="<%=cycles.getValue("PAY_CYCLE_ID")%>"
				<% if(payCycleId.compareTo(cycles.getValue("PAY_CYCLE_ID", BigDecimal.class)) == 0){ %> selected="selected"<%} %>
				><%=StringUtils.prepareHTML(cycles.getFormattedValue("PAY_CYCLE_NAME"))%></option>	    	
				<%}}%>
				</select>
			</td>
		</tr>
	
		<tr>
			<td colspan="2">
				<div class="spacer5"></div>
				<%if (terminalCount > 0) { %>
				<a href="terminalsList.i?accountId=<%=accountId%>">Terminals(<%=terminalCount%>)</a>
				<%} else {%>
				Terminals(0)
				<%}%>
			</td>
		</tr>		
</tbody>
</table>		
		

