<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	int i = 0; 
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    boolean norec=false;
    
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Bank Accounts</div>
</div>
<table class="tabDataDisplayBorderNoFixedLayout">
		<tr class="sortHeader">
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "17", sortIndex)%>">Account ID</a>
				<%=PaginationUtil.getSortingIconHtml("17", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Bank Name</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Account Number</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Routing #</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Account Title</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
		    <td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Created Date</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Status</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Account Type</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Bank Address</a>
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
			</td>

			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">Bank City</a>
				<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Bank State</a>
				<%=PaginationUtil.getSortingIconHtml("10", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "11", sortIndex)%>">Bank Zip</a>
				<%=PaginationUtil.getSortingIconHtml("11", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "12", sortIndex)%>">Contact</a>
				<%=PaginationUtil.getSortingIconHtml("12", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "13", sortIndex)%>">Contact Title</a>
				<%=PaginationUtil.getSortingIconHtml("13", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "14", sortIndex)%>">Contact Telephone</a>
				<%=PaginationUtil.getSortingIconHtml("14", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "15", sortIndex)%>">Contact Fax</a>
				<%=PaginationUtil.getSortingIconHtml("15", sortIndex)%>
			</td>
				
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "16", sortIndex)%>">Customer</a>
				<%=PaginationUtil.getSortingIconHtml("16", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "18", sortIndex)%>">Pay Cycle</a>
				<%=PaginationUtil.getSortingIconHtml("18", sortIndex)%>
			</td>
		</tr>
	<% 
	if (norec){
	    %>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<%} else {
	Results list = (Results) request.getAttribute("resultlist");
	while(list.next()) {
	%>
		<tr class="<%=(i++%2==0)?"row1":"row0"%>">
		 	<td><a href="/account.i?accountId=<%=list.getFormattedValue("CUSTOMER_BANK_ID")%>&customerId=<%=list.getFormattedValue("CUSTOMER_ID")%>"><%=list.getFormattedValue("CUSTOMER_BANK_ID")%></a></td>
		    <td><%=list.getFormattedValue("BANK_NAME")%></td>
		    <td><%=list.getFormattedValue("BANK_ACCT_NBR")%></td>
		    <td><%=list.getFormattedValue("BANK_ROUTING_NBR")%></td>
		    <td><%=list.getFormattedValue("ACCOUNT_TITLE")%></td>
		    <td><%=list.getFormattedValue("CREATE_DATE")%></td>
		    <td><%=list.getFormattedValue("STATUS_TEXT")%></td>
		    <td><%=list.getFormattedValue("ACCOUNT_TYPE_TEXT")%></td>
		    <td><%=list.getFormattedValue("BANK_ADDRESS")%></td>
		    <td><%=list.getFormattedValue("BANK_CITY")%></td>
			<td><%=list.getFormattedValue("BANK_STATE")%></td>
		    <td><%=list.getFormattedValue("BANK_ZIP")%></td>
		    <td><%=list.getFormattedValue("CONTACT_NAME")%></td>
		    <td><%=list.getFormattedValue("CONTACT_TITLE")%></td>
		    <td><%=list.getFormattedValue("CONTACT_TELEPHONE")%></td>
		    <td><%=list.getFormattedValue("CONTACT_FAX")%></td>	    
		    <td><%=list.getFormattedValue("CUSTOMER_NAME")%></td>
			<td><%=list.getFormattedValue("PAY_CYCLE_NAME")%></td>
		</tr>
	<% } %>
</table>

<%
    String storedNames = PaginationUtil.encodeStoredNames(new String[] {"customer_name"});
%>

    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="bankList.i" />
        <jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

<% } %>


</div>

<div class="spacer10"></div>
				
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
