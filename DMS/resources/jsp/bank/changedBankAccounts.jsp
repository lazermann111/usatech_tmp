<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    boolean norec=false;
    
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">New or Updated Bank Accounts</div>
</div>
<form name="accountForm" id="accountForm" method="post" action="changedBankAccounts.i">
<table>
<tr>
	<td>
	<input <% out.write( (norec)?" disabled='disabled' class='cssButtonDisabled' ":"class='cssButton'"); %> name="activateBtn" id="activateBtn" type="submit" value="Activate" />
	Please activate accounts ONLY after receiving a signed EFT Authorization Form and verifying all the account information.
	</td>
</tr>
</table>
<div class="spacer5"></div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td width="10"><%if (norec) {%>&nbsp; <%} else {%> <input type="checkbox" onclick="onClick()"/> <%}%></td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "17", sortIndex)%>">Account ID</a>
				<%=PaginationUtil.getSortingIconHtml("17", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Bank Name</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Account Number</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Routing #</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td >
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Account Title</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
		    <td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Created Date</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Status</a>
				<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Account Type</a>
				<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Bank Address</a>
				<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
			</td>

			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">Bank City</a>
				<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Bank State</a>
				<%=PaginationUtil.getSortingIconHtml("10", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "11", sortIndex)%>">Bank Zip</a>
				<%=PaginationUtil.getSortingIconHtml("11", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "12", sortIndex)%>">Contact</a>
				<%=PaginationUtil.getSortingIconHtml("12", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "13", sortIndex)%>">Contact Title</a>
				<%=PaginationUtil.getSortingIconHtml("13", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "14", sortIndex)%>">Contact Telephone</a>
				<%=PaginationUtil.getSortingIconHtml("14", sortIndex)%>
			</td>
			
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "15", sortIndex)%>">Contact Fax</a>
				<%=PaginationUtil.getSortingIconHtml("15", sortIndex)%>
			</td>
				
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "16", sortIndex)%>">Customer</a>
				<%=PaginationUtil.getSortingIconHtml("16", sortIndex)%>
			</td>
					
		</tr>
	</thead>
	<tbody>
	<% 
	if (norec){
	    %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<%} else {
	Results list = (Results) request.getAttribute("resultlist");
	int i = 0; 
	String rowClass = "row0";
	while(list.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++;
	%>
		<tr class="<%=rowClass%>">
		 	<td><input type="checkbox" name="bankid" id="bankid" value="<%= list.getFormattedValue(6) %>">  </td>
		 	<td><a href="/account.i?accountId=<%=list.getFormattedValue("CUSTOMER_BANK_ID")%>&customerId=<%=list.getFormattedValue("CUSTOMER_ID")%>"><%=list.getFormattedValue("CUSTOMER_BANK_ID")%></a></td>
		    <td><%=list.getFormattedValue("BANK_NAME")%></td>
		    <td><%=list.getFormattedValue("BANK_ACCT_NBR")%></td>
		    <td><%=list.getFormattedValue("BANK_ROUTING_NBR")%></td>
		    <td><%=list.getFormattedValue("ACCOUNT_TITLE")%></td>
		    <td><%=list.getFormattedValue("CREATE_DATE")%></td>
		    <td><%=list.getFormattedValue("STATUS_TEXT")%></td>
		    <td><%=list.getFormattedValue("ACCOUNT_TYPE_TEXT")%></td>
		    <td><%=list.getFormattedValue("BANK_ADDRESS")%></td>
		    <td><%=list.getFormattedValue("BANK_CITY")%></td>
			<td><%=list.getFormattedValue("BANK_STATE")%></td>
		    <td><%=list.getFormattedValue("BANK_ZIP")%></td>
		    <td><%=list.getFormattedValue("CONTACT_NAME")%></td>
		    <td><%=list.getFormattedValue("CONTACT_TITLE")%></td>
		    <td><%=list.getFormattedValue("CONTACT_TELEPHONE")%></td>
		    <td><%=list.getFormattedValue("CONTACT_FAX")%></td>	    
		    <td><%=list.getFormattedValue("CUSTOMER_NAME")%></td>	 		    

		</tr>
	<% } %>
	</tbody>
</table>

    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="changedBankAccounts.i" />
    </jsp:include>

<% } %>

</form>

</div>

<div class="spacer5"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">
var checked = false;

function onClick(){
	if(checked){
		uncheckIt();
		checked = false;
	}else{
		checkIt();
		checked = true;
	}
}

function checkIt(){
	if(document.accountForm.bankid.length>0){
		for (i=0; i<document.accountForm.bankid.length; i++){
			document.accountForm.bankid[i].checked=1;
		}
	}else{
		document.accountForm.bankid.checked=1;
	}
}

function uncheckIt(){
	if(document.accountForm.bankid.length>0){
		for (i=0; i<document.accountForm.bankid.length; i++){
			document.accountForm.bankid[i].checked=0;
		}
	}else{
		document.accountForm.bankid.checked=0;
	}
}

</script>
