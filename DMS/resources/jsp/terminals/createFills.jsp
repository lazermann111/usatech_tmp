<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>

<%
String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false);

String currentDate = Helper.getCurrentDate();
String currentTime = Helper.getCurrentTime();
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="smallFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Create Fills</span></div>
</div>

<div class="tableContent">

<form method="post" action="createFills.i" onsubmit="return doSubmit()">

<table class="padding3">		
	<%if(msg != null && msg.length() > 0) {%>
	<tr><td colspan="4" class="status-info"><%=msg%></td></tr>
	<%} %>
	<%if(err != null && err.length() > 0) {%>
	<tr><td colspan="4" class="status-error"><%=err%></td></tr>
	<%} %>
		
	<tr>
		<td class="label" valign="top">Serial Numbers<font color="red">*</font><br />(1 per line)</td>
		<td colspan="3">
			<textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"></textarea>
		</td>
	</tr>
	<tr>
		<td class="label nowrap">Local Fill Date<font color="red">*</font></td>
		<td>
			<input type="text" name="fill_date" id="fill_date" maxlength="10" value="<%=currentDate%>" />
			<img src="/images/calendar.gif" id="fill_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label nowrap">Local Fill Time</td>
		<td><input type="text" name="fill_time" id="fill_time" maxlength="8" value="<%=currentTime%>" /></td>
	</tr>
	<tr>
		<td colspan="4" align="center">
		<input type="submit" name="myaction" value="Submit" class="cssButton" />
		</td>
	</tr>
</table>

</form>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
Calendar.setup({
    inputField     :    "fill_date",                  	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "fill_date_trigger",  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

function doSubmit(){	
	if(document.getElementById("dev_list").value.trim() == ''){
		alert('Please enter serial numbers');
		return false;
	}
	
	var fill_date = document.getElementById("fill_date");
	if(!isDate(fill_date.value)){
		fill_date.focus();
		return false;
	}

	return true;
}
</script>
