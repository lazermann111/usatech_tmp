<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%
String customerId = ConvertUtils.getStringSafely(request.getParameter("customerId"), "");
String terminalId = ConvertUtils.getStringSafely(request.getParameter("terminalId"), "");
String terminalNbr = ConvertUtils.getStringSafely(request.getParameter("terminalNbr"), "");
String statusMessage = ConvertUtils.getStringSafely(request.getAttribute("statusMessage"), "&nbsp;");
String statusClass = ConvertUtils.getStringSafely(request.getAttribute("statusClass"), "status-info");
%>

<input type="hidden" name="terminalId" id="terminalId" value="<%=StringUtils.encodeForHTMLAttribute(terminalId)%>" />
<input type="hidden" name="modelId" id="modelId" />
<input type="hidden" name="act" id="act" />

<table align="center">	
		
	<tr>
		<td colspan="3"><b>Current Terminal</b>
		<input readonly="readonly" type="text" name="terminalNbr" id="terminalNbr" class="txtBox" style="width:180px;" value="<%if(terminalNbr != null) out.write(StringUtils.prepareCDATA(terminalNbr));%>" />
		<input name="closeBtn" id="closeBtn" type="button" value="Edit Terminal" onclick="if (terminalId.value) window.location = '/editTerminals.i?terminalId=' + terminalId.value; else alert('Please select a terminal');" class="cssButton"/>
		</td>
	</tr>		

	<tr >
		<td colspan="3">
		<input <% if (StringHelper.isBlank(terminalId)) out.write(" disabled='disabled' "); %> type="checkbox" name="useas" id="useas" onclick="clickIt()"/>&nbsp;Use selected mapped terminal as template
		</td>
	</tr>	
			
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	
	<tr>
		<td>
			<b>Unmapped Terminals</b>
			<a href="javascript:getData('type=unmapped_terminals&customerId=<%=StringUtils.encodeForURL(customerId)%>&selected=' + terminalId.value);"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
		<td>&nbsp;</td>
		<td>
			<b>Mapped Terminals</b>
			<a href="javascript:getData('type=mapped_terminals&customerId=<%=StringUtils.encodeForURL(customerId)%>&selected=' + terminalId.value);"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>	
	
			
	<tr>
		<td valign="top">
			<select name="map1" id="map1" size="2" style="width: 330px;"></select>
		</td>
		<td align="center">
			<input name="addBtn" id="addBtn" type="button" class="cssButtonDisabled"
				value="Add" onclick="clickAdd()" disabled="disabled" />	
				<div class="spacer3"></div>
			<input name="removeBtn" id="removeBtn" type="button"  class="cssButton"
				value="Delete" onclick="clickRemove();"/>
		</td>
		<td valign="top">
			<select name="map2" id="map2" size="2" style="width: 330px;"></select>
		</td>
	</tr>
</table>

<div id="divColumnMappingTerminal" >
<jsp:include page="columnMappingTerminal.jsp" flush="true" />
</div>

<script type="text/javascript">
document.getElementById("statusMessage").className = "<%=statusClass%>";
document.getElementById("statusMessage").innerHTML = "<%=statusMessage%>";
</script>
