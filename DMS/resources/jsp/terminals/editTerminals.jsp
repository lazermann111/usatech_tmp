<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.model.RiskAlert"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="com.usatech.dms.action.TerminalActions"%>
<%@page import="com.usatech.dms.model.BusinessUnit"%>
<%@page import="com.usatech.dms.model.Currency"%>
<%@page import="com.usatech.layers.common.model.Customer"%>
<%@page import="com.usatech.layers.common.model.CustomerBankTerminal"%>
<%@page import="com.usatech.dms.model.PaymentSchedule"%>
<%@page import="com.usatech.dms.model.Terminal"%>
<%@page import="com.usatech.dms.model.TerminalEport"%>
<%@page import="com.usatech.dms.util.Helper"%>

<%@page import="java.util.List"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "terminal");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("terminalId"), ""));

InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
List<PaymentSchedule> paymentSchedules = (List<PaymentSchedule>)inputForm.getAttribute("paymentSchedules");
List<Currency> currencies = (List<Currency>)inputForm.getAttribute("currencies");
List<BusinessUnit> businessUnits = (List<BusinessUnit>)inputForm.getAttribute("businessUnits");
List<RiskAlert> riskAlerts = (List<RiskAlert>)inputForm.getAttribute("riskAlerts");
Terminal terminal = (Terminal)inputForm.getAttribute("terminal");
long customerId = terminal.getCustomerId() == null ? -1 : terminal.getCustomerId().longValue();
long terminalId = terminal.getId() == null ? -1 : terminal.getId().longValue();
String terminalNbr = terminal.getTerminalNumber() == null ? "" : terminal.getTerminalNumber();
%>

<div class="formContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Edit Terminal</span></div>
</div>

<div class="tableContent">
<form name="editForm" id="editForm" method="post">
<input type="hidden" name="action" value="" />

<div id="statusMessage" class="status-info"></div>

<table class="tabFormControls">
	<tbody>
		<tr>
			<td width="15%" class="formLabel">Terminal Number:</td>
			<td class="control"><span id="terminalNumber"><%=terminalNbr%></span></td>
		</tr>		
		<tr>
			<td class="formLabel">Customer Name:</td>
			<td class="control">
				<span id="customerName"><%=terminal.getCustomerName() == null ? "" : terminal.getCustomerName()%>
					<input type="button" value="Show" id="btnShowCustomer" onclick="window.location = '/customer.i?customerId=<%=customerId%>';"/>
				</span>
			</td>
		</tr>
		<tr>
			<td class="formLabel">Location Name:</td>
			<td class="control"><span id="locationName"><%=terminal.getLocationName() == null ? "" : terminal.getLocationName()%></span></td>
		</tr>
		<tr>
			<td class="formLabel">Asset #:</td>
			<td class="control"><input type="text"
				name="assetNumber" id="assetNumber"
				class="formTxt txtBox" disabled="disabled"
				onchange="enableButton(saveChangesBtn);"
				value="<%=terminal.getAssetNumber() == null ? "" : terminal.getAssetNumber()%>" /></td>
		</tr>
		<tr>
			<td class="formLabel">Customer Asset #:</td>
			<td class="control"><input type="text"
				name="customerTerminalNumber" id="customerTerminalNumber"
				class="formTxt txtBox" disabled="disabled"
				onchange="handleCustomerTerminalNbrChange();"
				value="<%=terminal.getCustomerTerminalNumber() == null ? "" : terminal.getCustomerTerminalNumber()%>" /></td>
		</tr>
		<tr>
			<td class="formLabel">Payment
			Schedule:</td>
			<td class="control"><select name="paymentScheduleSelect"
				onchange="disableSaveChangeBtn(this);" id="paymentScheduleSelect"
				style="width: 240px;">
				<option value="-1">Please select:</option>
				<%
				    for (PaymentSchedule schedule : paymentSchedules)
				    {
				%>
				<option value="<%=schedule.getId()%>"><%=schedule.getDescription()%></option>
				<%
				    }
				%>
			</select></td>
		</tr>
		<tr>
			<td class="formLabel">Currency:</td>
			<td class="control"><select name="currencySelect"
				onchange="disableSaveChangeBtn(this);" id="currencySelect"
				style="width: 240px;">
				<option value="-1">Please select:</option>
				<%
				    for (Currency currency : currencies)
				    {
				%>
				<option value="<%=currency.getId()%>"><%=currency.getName()%></option>
				<%
				    }
				%>
			</select></td>
		</tr>
		<tr>
			<td class="formLabel">Business
			Unit:</td>
			<td class="control"><select name="businessUnitSelect"
				onchange="disableSaveChangeBtn(this);" id="businessUnitSelect"
				style="width: 240px;">
				<option value="-1">Please select:</option>
				<%
				    for (BusinessUnit unit : businessUnits)
				    {
				%>
				<option value="<%=unit.getId()%>"><%=unit.getName()%></option>
				<%
				    }
				%>
			</select></td>
		</tr>
		<tr>
			<td class="formLabel" valign="top">ePort History:</td>
			<td class="control">
				<select name="eportSelect" id="eportSelect"	onchange="handleEportChange()" ; size="5" style="width: 400px">
				<%
				StringBuilder eportString = new StringBuilder();
				    for (TerminalEport eport : terminal.getTerminalEports())
				    {
				        eportString.append(eport.getEportSerialNumber());
				        eportString.append(" ");
				        eportString.append(eport.getStartDate() == null ? "?" : Helper.getDateTime(eport.getStartDate()));
				        eportString.append(" - ");
				        eportString.append(eport.getEndDate() == null ? "?" : Helper.getDateTime(eport.getEndDate()));
				%>
				<option><%=eportString.toString()%></option>
				<%
				eportString.setLength(0);
				    }
				%>
			</select></td>
		</tr>
		<tr>
			<td colspan="2" class="control" style="text-align: center">
				<input name="assignEportBtn" id="assignEportBtn" type="button" 
						value="Assign ePort" class="cssButtonDisabled" disabled="disabled" onclick="assignEport();"/>
				<input name="deactivateEportBtn" id="deactivateEportBtn"
						type="button" value="Deactivate ePort" class="cssButtonDisabled"
							disabled="disabled" onclick="deactivateEport();" />
				<input name="viewDeviceBtn" id="viewDeviceBtn"
						type="button" value="View Device" class="cssButtonDisabled"
							disabled="disabled" onclick="viewDevice();" />
			</td>
		</tr>
		<tr>
			<td colspan="2"  class="control" style="text-align: center">
				<input name="changeStartDateBtn" id="changeStartDateBtn" type="button"
					value="Change Start Date" class="cssButtonDisabled" disabled="disabled" onclick="changeStartDate();" /> 
				<input name="changeEndDateBtn" id="changeEndDateBtn" type="button"
					value="Change End Date" class="cssButtonDisabled" disabled="disabled" onclick="changeEndDate();" />
				<input name="deleteTerminalBtn" id="deleteTerminalBtn" type="button" 
						value="Delete Terminal" onclick="deleteTerminal();"/>
			</td>
		</tr>
		<tr>
			<td class="formLabel" valign="top">Bank Account:</td>
			<td class="control"><select name="accountSelect"
				onchange="handleAccountChange()" id="accountSelect" size="5"
				style="width: 400px">
				<%
				StringBuilder bankString = new StringBuilder();
				    for (CustomerBankTerminal bank : terminal.getCustomerBankTerminals())
				    {
				        bankString.append(bank.getBankAccountNumber());
				        bankString.append(" ");
				        bankString.append(bank.getStartDate() == null ? "?" : Helper.getDateTime(bank.getStartDate()));
				        bankString.append(" - ");
				        bankString.append(bank.getEndDate() == null ? "?" : Helper.getDateTime(bank.getEndDate()));
				%>
				<option value="<%=bank.getCustomerBankId()%>"><%=bankString.toString()%></option>
				<%
				bankString.setLength(0);
				    }
				%>
			</select>
			<input type="button" value="Show" id="btnShowBank" onclick="window.location = '/account.i?customerId=<%=customerId%>&accountId=' + editForm.accountSelect[editForm.accountSelect.selectedIndex].value;"/>
			</td>
		</tr>
		<tr>
            <td colspan="2"  class="control" style="text-align: center">
            <input  name="bankAcctDateBtn" id="bankAcctDateBtn" type="button" value="Change Bank Acct Date" class="cssButtonDisabled" disabled="disabled" onclick="changeBankAcctDate();" />
            <input name="viewFeesBtn" id="viewFeesBtn" type="button" value="View Fees" class="cssButtonDisabled" disabled="disabled" onclick="viewFees()"/> 
            </td>
		</tr>
		<tr>
			<td class="formLabel">Sales Order Number:</td>
			<td><%=terminal.getSalesOrderNumber() == null ? "" : terminal.getSalesOrderNumber()%></td>
		</tr>
		<tr>
			<td class="formLabel">Purchase Order Number:</td>
			<td><%=terminal.getPurchaseOrderNumber() == null ? "" : terminal.getPurchaseOrderNumber()%></td>
		</tr>
		<tr>
			<td class="formLabel">Risk Monitoring Factors:</td>
			<td class="control">
				<table>
					<tr>
						<td align="left">Avg Tran Amt</td>
						<td rowspan="2">&nbsp;</td>
						<td align="left">Max Tran Amt</td>
						<td rowspan="2">&nbsp;</td>
						<td align="left">Avg Daily Trans</td>
					</tr>
					<tr>
						<td align="left"><input type="text" name="avgTransAmt" id="avgTransAmt" size="5" maxlength="15" onchange="enableButton(saveChangesBtn);" <%if (terminal!=null) { %>value="<%=terminal.getAvgTransAmt() == null ? "" : terminal.getAvgTransAmt()%>"<%}%>/></td>
						<td align="left"><input type="text" name="maxTransAmt" id="maxTransAmt" size="5" maxlength="15" onchange="enableButton(saveChangesBtn);" <%if (terminal!=null) { %>value="<%=terminal.getMaxTransAmt() == null ? "" : terminal.getMaxTransAmt()%>"<%}%>/></td>
						<td align="left"><input type="text" name="avgTransCnt" id="avgTransCnt" size="5" maxlength="15" onchange="enableButton(saveChangesBtn);" <%if (terminal!=null) { %>value="<%=terminal.getAvgTransCnt() == null ? "" : terminal.getAvgTransCnt()%>"<%}%>/></td>
				</table>
			</td>
		</tr>
    <tr>
      <td class="formLabel" valign="top">Risk Alerts:</td>
      <td class="control">
        <select name="riskAlertSelect" id="riskAlertSelect" size="5" style="width: 400px">
        <% for (RiskAlert riskAlert : riskAlerts) {
          String desc = String.format("%s\t%s\t%s\t%s",
              Helper.getDate(riskAlert.getAlertTime()),
              riskAlert.getScore(),
              riskAlert.getStatus().getName(),
              riskAlert.getMessage()); %>
        <option value="<%=riskAlert.getId()%>"><%=StringUtils.prepareHTML(desc)%></option>
        <% } %>
      </select>
      <input type="button" value="Show" id="btnShowRiskAlert" onclick="window.location = '/riskAlert.i?riskAlertId=' + editForm.riskAlertSelect[editForm.riskAlertSelect.selectedIndex].value;"/>
      </td>
    </tr>
		<tr>
			<td class="formLabel">Last Risk Check Date:</td>
			<td><%=terminal.getLastRiskCheck() == null ? "" : Helper.getDateTime(terminal.getLastRiskCheck())%></td>
		</tr><%
		Results mappingResults = DataLayerMgr.executeQuery("GET_DATA_EXCHANGE_MAPPING", terminal);
		if(!mappingResults.isGroupEnding(0)) {
			%><tr>
            <td class="formLabel" valign="top">Data Exchange Mappings:</td>
            <td><table class="tabDataDisplayBorder" style="width: 400px"><tr><th>Type</th><th>Value</th><th>Level</th></tr><%
            while(mappingResults.next()) {
            	int level = mappingResults.getValue("level", int.class);
            	%><tr><td><%=StringUtils.prepareHTML(mappingResults.getValue("typeName", String.class))%></td><td><%=StringUtils.prepareHTML(mappingResults.getValue("mappingValue", String.class))%></td><td><%
            			switch(level) {
            			     case 1:%>Customer<% break; case 2: %>Terminal<% break; default: %>Unknown<%
           			     }%></td></tr><%
            } %>
            </table></td>
        </tr><%
		}
		%>
		<tr>
			<td colspan="2" class="control" style="text-align: center">
				<input name="saveChangesBtn" id="saveChangesBtn" type="button" value="Save Changes" class="cssButtonDisabled" 
					disabled="disabled" onclick="handleChange()" />
			</td>
		</tr>
	</tbody>
</table>
</form>
</div>

<div class="spacer10"></div>

</div>

<script type="text/javascript" defer="defer">

var xmlHttp;
var accountSelect = document.getElementById("accountSelect");
var eportSelect = document.getElementById("eportSelect");
var paymentScheduleSelect = document.getElementById("paymentScheduleSelect");
var currencySelect = document.getElementById("currencySelect");
var businessUnitSelect = document.getElementById("businessUnitSelect");
var customerName = document.getElementById("customerName");
var locationName = document.getElementById("locationName");
var assetNumber = document.getElementById("assetNumber");
var avgTransAmt = document.getElementById("avgTransAmt");
var maxTransAmt = document.getElementById("maxTransAmt");
var avgTransCnt = document.getElementById("avgTransCnt");
var customerTerminalNumber = document.getElementById("customerTerminalNumber");
var assignEportBtn = document.getElementById("assignEportBtn");
var changeStartDateBtn = document.getElementById("changeStartDateBtn");
var changeEndDateBtn = document.getElementById("changeEndDateBtn");
var deactivateEportBtn = document.getElementById("deactivateEportBtn");
var viewDeviceBtn = document.getElementById("viewDeviceBtn");
var viewFeesBtn = document.getElementById("viewFeesBtn");
var saveChangesBtn = document.getElementById("saveChangesBtn");
var bankAcctDateBtn = document.getElementById("bankAcctDateBtn");
var colEports = new Array();
var customerBankIds = new Array();

<% if(terminal.getPaymentScheduleId() != null) {%>
paymentScheduleSelect.value = "<%=terminal.getPaymentScheduleId()%>";
<% } else {%>
paymentScheduleSelect.value = -1;
<% } if(terminal.getFeeCurrencyId() != null) {%>
currencySelect.value = "<%=terminal.getFeeCurrencyId()%>";
<% } else {%>
currencySelect.value = -1;
<% } if(terminal.getBusinessUnitId() != null) {%>
businessUnitSelect.value = "<%=terminal.getBusinessUnitId()%>";
<% } else {%>
businessUnitSelect.value = -1;
<% } %>

<% if(terminal != null) { %>
	enableButtons();
<% } %>

<% for(int i=0; i<terminal.getTerminalEports().size(); i++) {
    TerminalEport eport = terminal.getTerminalEports().get(i);
%>
   
    colEports[<%=i%>] = new Array('<%=eport.getEportId()%>', '<%=eport.getStartDate() == null ? "" : Helper.getDateTime(eport.getStartDate())%>', '<%=eport.getEndDate() == null ? "" : Helper.getDateTime(eport.getEndDate())%>', '<%=eport.getId()%>', '<%=eport.getEportSerialNumber()%>');
<% } %> 

<% for(int i=0; i<terminal.getCustomerBankTerminals().size(); i++) {
    CustomerBankTerminal bankTerminal = terminal.getCustomerBankTerminals().get(i);
    %>
    customerBankIds[<%=i%>] = "<%=bankTerminal.getCustomerBankId()%>";
<% } %>

function handleChange() {
	if(saveChangesBtn.className == "cssButton" && confirm('You have made changes to this terminal. Do you want to save them?')) {
		var paramString = "&paymentScheduleId=" + paymentScheduleSelect.value;
		paramString += "&feeCurrencyId=" + currencySelect.value;
		paramString += "&businessUnitId=" + businessUnitSelect.value;
		paramString += "&userOP=SaveChanges";
		paramString += "&assetNumber=" + assetNumber.value;
		paramString += "&avgTransAmt=" + avgTransAmt.value;
		paramString += "&maxTransAmt=" + maxTransAmt.value;
		paramString += "&avgTransCnt=" + avgTransCnt.value;
		paramString += "&customerTerminalNumber=" + customerTerminalNumber.value;
		document.getElementById("statusMessage").innerHTML = "<img src='/images/spinner.gif' style='vertical-align:middle;' /> Please wait, loading data...";
		xmlHttp = getNewHttpObject();
		xmlHttp.onreadystatechange = handleStateChange;
		xmlHttp.open("POST", "editTerminalsChange.i?terminalId=<%=terminalId%>" + paramString, true);
		xmlHttp.send(null);
	}
}

function handleStateChange() {
	if(xmlHttp.readyState == 4) {
		if(xmlHttp.status == 200) {
			processResponse(xmlHttp.responseXML);
			document.getElementById("statusMessage").innerHTML = "Terminal updated";
		}
	}
}

function processResponse(response) {
	var terminals = response.getElementsByTagName("Terminal");
	setTerminalDetails(response);
}

function disableButtons() {
	disableButton(assignEportBtn);
	disableButton(changeStartDateBtn);
	disableButton(changeEndDateBtn);
	disableButton(deactivateEportBtn);
	disableButton(viewDeviceBtn);
	disableButton(viewFeesBtn);
	disableButton(saveChangesBtn);
	disableButton(bankAcctDateBtn);
}

function enableButtons() {
	enableButton(assignEportBtn);
	enableButton(viewFeesBtn);
	disableButton(saveChangesBtn);
	
	assetNumber.removeAttribute("disabled");
	customerTerminalNumber.removeAttribute("disabled");
	paymentScheduleSelect.removeAttribute("disabled");
	if(accountSelect.value == "") {
		disableButton(bankAcctDateBtn);
	}
	if(eportSelect.selectedIndex == -1) {
		disableButton(changeStartDateBtn);
		disableButton(changeEndDateBtn);
		disableButton(deactivateEportBtn);
		disableButton(viewDeviceBtn);
	}
}

function setTerminalDetails(response) {
	var terminalNode = response.getElementsByTagName("Terminal")[0];

	with(terminalNode.getElementsByTagName("CustomerName")[0]) { customerName.innerHTML = (hasChildNodes() ? firstChild.nodeValue : ""); }
	with(terminalNode.getElementsByTagName("LocationName")[0]) { locationName.innerHTML = (hasChildNodes() ? firstChild.nodeValue : ""); }
	with(terminalNode.getElementsByTagName("AssetNumber")[0]) { assetNumber.value = (hasChildNodes() ? firstChild.nodeValue : ""); }
	with(terminalNode.getElementsByTagName("CustomerTerminalNumber")[0]) { customerTerminalNumber.value = (hasChildNodes() ? firstChild.nodeValue : ""); }

	paymentScheduleSelect.value = terminalNode.getElementsByTagName("PaymentScheduleId")[0].firstChild.nodeValue;
	currencySelect.value = terminalNode.getElementsByTagName("CurrencyId")[0].firstChild.nodeValue;
	businessUnitSelect.value = terminalNode.getElementsByTagName("BusinessUnitId")[0].firstChild.nodeValue;

	clearSelect(accountSelect);
	clearSelect(eportSelect);

	var optionContent = "";
	var eportSerialNumber;
	var eports = terminalNode.getElementsByTagName("TerminalEport");
	for(var i=0; i<eports.length; i++) {
		colEports[i] = new Array(4);
		optionContent += eports[i].getElementsByTagName("EportSerialNumber")[0].firstChild.nodeValue;
		optionContent += " ";
	    with(eports[i].getElementsByTagName("StartDate")[0]) { optionContent += (hasChildNodes() ? firstChild.nodeValue : "?"); colEports[i][1] = (hasChildNodes() ? firstChild.nodeValue : "");}
	    optionContent += " - ";
	    with(eports[i].getElementsByTagName("EndDate")[0]) { optionContent += (hasChildNodes() ? firstChild.nodeValue : "?"); colEports[i][2] = (hasChildNodes() ? firstChild.nodeValue : "");}
		eportSelect.options[i] = new Option(optionContent);
		colEports[i][0] = eports[i].getElementsByTagName("EportId")[0].firstChild.nodeValue;
		colEports[i][3] = eports[i].getElementsByTagName("TerminalEportId")[0].firstChild.nodeValue;
		optionContent = "";
	}

	var bankAccounts = terminalNode.getElementsByTagName("CustomerBankTerminal");
	for(var i=0; i<bankAccounts.length; i++) {
		customerBankIds[i] = bankAccounts[i].getElementsByTagName("CustomerBankId")[0].firstChild.nodeValue;
		optionContent += bankAccounts[i].getElementsByTagName("BankAccountNumber")[0].firstChild.nodeValue;
		optionContent += " ";
		with(bankAccounts[i].getElementsByTagName("StartDate")[0]) { optionContent += (hasChildNodes() ? firstChild.nodeValue : "?"); }
	    optionContent += " - ";
	    with(bankAccounts[i].getElementsByTagName("EndDate")[0]) { optionContent += (hasChildNodes() ? firstChild.nodeValue : "?"); }
		accountSelect.options[i] = new Option(optionContent);
		optionContent = "";
	}

	enableButtons();
}

function clearSelect(tempSelect) {
	var length = tempSelect.childNodes.length;
	for(var i=length-1; i>=0; i--) {
		tempSelect.removeChild(tempSelect.childNodes[i]);
	}	
}

function handleAccountChange() {
	enableButton(bankAcctDateBtn);
}

function handleEportChange() {
	enableButton(changeStartDateBtn);
	var selectedEport = eportSelect[eportSelect.selectedIndex];
	if(selectedEport.text.indexOf("- ?") != -1) {
		enableButton(deactivateEportBtn);
		disableButton(changeEndDateBtn);
	} else {
		enableButton(changeEndDateBtn);
		disableButton(deactivateEportBtn);
	}
	enableButton(viewDeviceBtn);
}

function disableSaveChangeBtn(selectList) {
	if(selectList.value == -1) {
		disableButton(saveChangesBtn);
	} else {
		enableButton(saveChangesBtn);
	}
}

function handleCustomerTerminalNbrChange() {
	enableButton(saveChangesBtn);
}

function changeStartDate() {
	selectDate("dateAction=<%=TerminalActions.ACT_CHANGE_START_DATE%>");
}

function changeEndDate() {
	selectDate("dateAction=<%=TerminalActions.ACT_CHANGE_END_DATE%>");
}

function changeBankAcctDate() {	
	selectDate("dateAction=<%=TerminalActions.ACT_CHANGE_BANK_ACCT_DATE%>");
}

function deactivateEport() {
	selectDate("dateAction=<%=TerminalActions.ACT_DEACTIVATE_EPORT%>");
}

function viewDevice() {
	window.location = "/deviceSearch.i?serial_number=" + colEports[eportSelect.selectedIndex][4];
}

function selectDate(params) {
	with(document.editForm) {
		if (params.length > 0)
			params += "&";
		params += "terminalId=<%=terminalId%>";
		params += "&terminalNbr=<%=terminalNbr%>";
		if (colEports[eportSelect.selectedIndex]) {
			params += "&eportId=" + colEports[eportSelect.selectedIndex][0];
			params += "&startDateStr=" + colEports[eportSelect.selectedIndex][1];
			params += "&endDateStr=" + colEports[eportSelect.selectedIndex][2];
			params += "&terminalEportId=" + colEports[eportSelect.selectedIndex][3];
			params += "&serialNbr=" + colEports[eportSelect.selectedIndex][4];
		}
		if (customerBankIds[accountSelect.selectedIndex])
			params += "&customerBankId=" + customerBankIds[accountSelect.selectedIndex];
	}
	window.location = 'dateSelection.i?' + params;
}

function disableButton(item) {
	item.className="cssButtonDisabled";
	item.disabled = "disabled";
}

function enableButton(item) {
	item.className = "cssButton";
	item.removeAttribute("disabled");
	if (item.id == "saveChangesBtn") {
		document.getElementById("statusMessage").innerHTML = "&nbsp;";
	}
}

function editMapping(){
	window.location='columnMapping.i?customerId=<%=customerId%>&terminalId=<%=terminalId%>&terminalNbr=<%=terminalNbr%>';
}

function viewFees(){
	window.location='viewFees.i?terminalId=<%=terminalId%>&terminalNbr=<%=terminalNbr%>';
	
}

function assignEport(){
	window.location='assignEport.i?terminalId=<%=terminalId%>&terminalNbr=<%=terminalNbr%>';
}

function deleteTerminal() {
	<%if (terminalId > 0) {%>
	if (confirm("Are you sure you want to delete this terminal?")) {
		document.getElementById("editForm").action.value = "deleteTerminal";
		document.getElementById("editForm").submit();
	}
	<%}%>
}

</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />