<%@page import="simple.db.DataLayerMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>
<%@page import="com.usatech.dms.util.Helper"%>

<%
BigDecimal terminalId = RequestUtils.getAttribute(request, "terminalId", BigDecimal.class, false);
String terminalNbr = RequestUtils.getAttribute(request, "terminalNbr", String.class, false);
Results activateDetails=DataLayerMgr.executeQuery("GET_ACTIVATE_DETAIL", null);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="smallFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Assign ePort</span></div>
</div>

<div class="tableContent">

<div class="spacer10"></div>

<form name=mainForm id="mainForm" method="post">
<input type="hidden" name="myaction" id="myaction" value="assigneport" />
<input type="hidden" name="date1" id="date1" value="" />
		
<div id="divMainForm" >
<table align="center">
	<tr>
		<td class="formLabel">Terminal:</td><td><a href="/editTerminals.i?terminalId=<%=terminalId %>"><%=terminalNbr %></a></td>
		<td>&nbsp;&nbsp;</td>
		<td class="formLabel">Serial Number:</td><td><input type="text" class="cssText" name="eportSerialNum" id="eportSerialNum" maxlength="50" /></td>
	</tr>
	<tr>
		<td class="formLabel">Date:</td>
		<td>
			<input type="text" class="cssText" name="dateTxt" onchange="" id="dateTxt" value="<%=Helper.getCurrentDate() %>" size="10" maxlength="10" /> 
			<img src="/images/calendar.gif" id="from_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td>&nbsp;&nbsp;</td>
		<td class="formLabel">Time:</td>
		<td><input type="text" class="cssText" name="timeTxt" id="timeTxt" size="8" maxlength="8" value="<%=Helper.getCurrentTime() %>" /></td>
	</tr>
	<tr>
		<td colspan="5">
		Activation Reason/Details:
		<select name=activateDetailId id="activateDetailId">
			<%if (activateDetails != null){
			while(activateDetails.next()) {
			%><option value="<%=activateDetails.getValue("activateDetailId")%>"><%=activateDetails.getFormattedValue("activateDetail")%></option>	    	
			<%}}%>	
	</select>
		</td>
	</tr>	
	<tr>
		<td colspan="5">
		Activation Reason/Details Additional Info:
		<input id="activateDetail" class="cssText" type="text" size="100" value="" name="activateDetail"/>
		</td>
	</tr>
	<tr>
		<td colspan="5" align="center">
			<div class="spacer10"></div>
			<input name="saveBtn" id="saveBtn" type="submit" value="Save" onclick="return saveAll()" class="cssButton" />
			&nbsp;
			<input type="button" value="Cancel" onclick="window.location = '/editTerminals.i?terminalId=<%=terminalId %>';" class="cssButton" />
		</td>
	</tr>
</table>

</div>
</form>

<div class="spacer10"></div>
</div>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">

Calendar.setup({
    inputField     :    "dateTxt",                  // id of the input field
    ifFormat       :    "%m/%d/%Y",            // format of the input field
    button         :    "from_date_trigger",   // trigger for the calendar (button ID)
    align          :    "B2",                  // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

var divMainForm = document.getElementById("divMainForm");
var eportSerialNum = document.getElementById("eportSerialNum");
var dateTxt = document.getElementById("dateTxt");
var timeTxt = document.getElementById("timeTxt");
var date1 = document.getElementById("date1");

function saveAll(){
	if (eportSerialNum.value.length <= 0){
		alert('Enter ePort Serial Number please');
		return false;
	}
	if(dateTxt.value.length <= 0){
		alert('Enter date please');
		return false;
	}
	if(timeTxt.value.length <= 0){
		alert('Enter time please');
		return false;
	}
	date1.value=dateTxt.value+' '+timeTxt.value;
}

</script>
