<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results,java.math.BigDecimal"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.Vector"%>

<%@page import="com.usatech.dms.action.TerminalActions"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<% 
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
int action = inputForm.getInt("dateAction", false, 0);
BigDecimal terminalId = RequestUtils.getAttribute(request, "terminalId", BigDecimal.class, false);
String terminalNbr = RequestUtils.getAttribute(request, "terminalNbr", String.class, false);
String date = RequestUtils.getAttribute(request, "date", String.class, false);
if (StringHelper.isBlank(date) || request.getMethod().equalsIgnoreCase("POST"))
	date = "";
boolean override = "Y".equals(request.getAttribute("override")) ? true : false;
Results pfee = RequestUtils.getAttribute(request, "pfee", Results.class, true);
Results sfee = RequestUtils.getAttribute(request, "sfee", Results.class, true);

Results qsInactiveDays = RequestUtils.getAttribute(request, "quickStartInactiveDays", Results.class, false);
qsInactiveDays.next();


String statusMessage = RequestUtils.getAttribute(request, "statusMessage", String.class, false);

Results hasPfcResults = RequestUtils.getAttribute(request, "hasPfc", Results.class, true);
hasPfcResults.next();
boolean hasPfc = ConvertUtils.getBooleanSafely(hasPfcResults.getValue("TRUE_FALSE"), false);

Results hasSfcResults = RequestUtils.getAttribute(request, "hasSfc", Results.class, true);
hasSfcResults.next();
boolean hasSfc = ConvertUtils.getBooleanSafely(hasSfcResults.getValue("TRUE_FALSE"), false);

boolean hasCommissions = hasPfc || hasSfc; 

int colSpan = hasCommissions ? 14 : 10;

Results commissionBankResults = RequestUtils.getAttribute(request, "commissionBanks", Results.class, true);
Map<Integer, String> commissionBanks = new LinkedHashMap<Integer, String>();
while(commissionBankResults.next()) {
	int bankId = ConvertUtils.getInt(commissionBankResults.getValue("aValue"));
	String bankName = commissionBankResults.getFormattedValue("aLabel");
	commissionBanks.put(bankId, bankName);
}

%>

<input type="hidden" name="terminalId" id="terminalId" value="<%=terminalId %>" />
<input type="hidden" name="terminalNbr" id="terminalNbr" value="<%=terminalNbr %>" />

<div class="spacer5"></div>

<%if (statusMessage!=null) {
	if (statusMessage.startsWith("Error:")){%>
	<div id="status" class="status-info" style="color:RED"><%=StringUtils.encodeForHTML(statusMessage) %></div>
	<%}else{ %>	
	<div id="status" class="status-info"><%=statusMessage %></div>
	<%} %>
	<div class="spacer5"></div>	
<%} %>

<table class="tabDataDisplayBorder">		

	<tr class="gridHeader"><td colspan="<%=colSpan%>">&nbsp;<b>Fees for <%if (terminalNbr!=null) {%> <a href="/editTerminals.i?terminalId=<%=terminalId %>"><%=StringUtils.encodeForHTML(terminalNbr)%></a> <%}%></b></td></tr>		
		<tr class="gridHeader"><td colspan="<%=colSpan%>">Process Fees</td></tr>
        <tr class="gridHeader">
            <td>Fee Type</td>
            <td>Percent(%)</td>
            <% if (hasCommissions) { %><td>Commission<br/>Percent(%)</td><% } %>
            <td>Fixed Amount($)</td>
            <% if (hasCommissions) { %><td>Commission<br/>Amount($)</td><% } %>
            <td>Minimum Amount($)</td>
            <% if (hasCommissions) { %><td>Commission<br/>Minimum($)</td><% } %>
            <td>Effective Date</td>
            <td align="center"><input type="checkbox" id="overrideAll" onclick="markAllOverride(this, 'P')"<%if (override) out.write(" checked=\"checked\"");%> />&nbsp;Override</td>
            <td align="center"><input type="checkbox" id="updateAllP" onclick="markAllUpdate(this, 'P')"<%if (date.length() > 0) out.write(" checked=\"checked\"");%> />&nbsp;Update</td>
            <% if (hasCommissions) { %><td>Commission Bank</td><% } %>
        </tr>
		<% while( pfee.next()) {
			String itemId = pfee.getFormattedValue("TRANS_TYPE_ID");
			int bankId = ConvertUtils.getIntSafely(pfee.getFormattedValue("COMMISSION_BANK_ID"), -1); %>
		<tr>
			<td nowrap="nowrap">
				<b><%=pfee.getFormattedValue("TRANS_TYPE_NAME")%> Fee</b>
				<input type="hidden" id="old_feepercent_P<%=itemId%>" name="old_feepercent_P<%=itemId%>" value="<%=pfee.getFormattedValue("FEE_PERCENT")%>" />
				<input type="hidden" id="old_feeamount_P<%=itemId%>" name="old_feeamount_P<%=itemId%>" value="<%=pfee.getFormattedValue("FEE_AMOUNT")%>" />
				<input type="hidden" id="old_feemin_P<%=itemId%>" name="old_feemin_P<%=itemId%>" value="<%=pfee.getFormattedValue("MIN_AMOUNT")%>" />
				<% if (hasCommissions) { %>
					<input type="hidden" id="old_commissionpercent_P<%=itemId%>" name="old_commissionpercent_P<%=itemId%>" value="<%=pfee.getFormattedValue("COMMISSION_PERCENT")%>" />
					<input type="hidden" id="old_commissionamount_P<%=itemId%>" name="old_commissionamount_P<%=itemId%>" value="<%=pfee.getFormattedValue("COMMISSION_AMOUNT")%>" />
					<input type="hidden" id="old_commissionmin_P<%=itemId%>" name="old_commissionmin_P<%=itemId%>" value="<%=pfee.getFormattedValue("COMMISSION_MIN")%>" />
					<input type="hidden" id="old_commissionbank_P<%=itemId%>" name="old_commissionbank_P<%=itemId%>" value="<%=bankId%>" />
				<% } %>
			</td>
			<td><input type="text" name="feepercent_P<%=itemId%>" id="feepercent_P<%=itemId%>" size="9" maxlength="9" value="<%=pfee.getFormattedValue("FEE_PERCENT")%>" onchange="markIt(this)" /></td>
			<% if (hasCommissions && itemId.matches("13|16")) { %>
				<td><input type="text" name="commissionpercent_P<%=itemId%>" id="commissionpercent_P<%=itemId%>" size="9" maxlength="9" value="<%=pfee.getFormattedValue("COMMISSION_PERCENT")%>" onchange="markIt(this)" /></td>
			<% } else if (hasCommissions) { %>
				<td class="inactive">&nbsp;</td>
			<% } %>
			<td><input type="text" name="feeamount_P<%=itemId%>" id="feeamount_P<%=itemId%>" size="10" maxlength="16" value="<%=pfee.getFormattedValue("FEE_AMOUNT")%>" onchange="markIt(this)" /></td>
			<% if (hasCommissions && itemId.matches("13|16")) { %>
				<td><input type="text" name="commissionamount_P<%=itemId%>" id="commissionamount_P<%=itemId%>" size="10" maxlength="16" value="<%=pfee.getFormattedValue("COMMISSION_AMOUNT")%>" onchange="markIt(this)" /></td>
			<% } else if (hasCommissions) { %>
				<td class="inactive">&nbsp;</td>
			<% } %>
			<td><input type="text" name="feemin_P<%=itemId%>" id="feemin_P<%=itemId%>" size="10" maxlength="16" value="<%=pfee.getFormattedValue("MIN_AMOUNT")%>" onchange="markIt(this)" /></td>
			<% if (hasCommissions && itemId.matches("13|16")) { %>
				<td><input type="text" name="commissionmin_P<%=itemId%>" id="commissionmin_P<%=itemId%>" size="10" maxlength="16" value="<%=pfee.getFormattedValue("COMMISSION_MIN")%>" onchange="markIt(this)" /></td>
			<% } else if (hasCommissions) { %>
				<td class="inactive">&nbsp;</td>
			<% } %>
			<td><input type="text" maxlength="19" name="date1_P<%=itemId%>" id="date1_P<%=itemId%>" size="17" value="<%=action == TerminalActions.ACT_CHANGE_FEE_EFFECTIVE_DATE ? StringUtils.encodeForHTMLAttribute(date) : pfee.getFormattedValue("START_DATE")%>" onchange="markIt(this)" /></td>
			<td align="center"><input type="checkbox" name="override_P<%=itemId%>" id="override_P<%=itemId%>" onclick="markIt(this)"<%if (override) out.write(" checked=\"checked\"");%> /></td>
			<td align="center"><input type="checkbox" name="update" id="update_P<%=itemId%>" value="P<%=itemId%>"<%if (date.length() > 0 && action == TerminalActions.ACT_CHANGE_FEE_EFFECTIVE_DATE) out.write(" checked=\"checked\"");%> /></td>
			<% if (hasCommissions && itemId.matches("13|16")) { %>
				<td align="center">
					<select id="commissionbank_P<%=itemId%>" name="commissionbank_P<%=itemId%>">
						<option value="0">Please select:</option>
						<% for(Map.Entry<Integer, String> entry : commissionBanks.entrySet()) { %>
						<option value="<%=entry.getKey()%>" <%=(entry.getKey() == bankId ? "selected=\"selected\"" : "") %>><%=entry.getValue() %></option>
						<% } %>
					</select>
				</td>
			<% } else if (hasCommissions) { %>
				<td class="inactive">&nbsp;</td>
			<% } %>
		</tr>
		<%}%>
</table><table  class="tabDataDisplayBorder">
		<tr class="gridHeader"><td colspan="<%=colSpan%>">Service Fees</td></tr>
        <tr class="gridHeader">
            <td>Fee Type</td>
            <td>Frequency</td>
            <td>Fixed Amount($)</td>
            <% if (hasCommissions) { %><td>Commission<br/>Amount($)</td><% } %>
            <td>Inactive Amount($)</td>
            <td>Effective Date</td>
            <td>Grace Days</td>
            <td>No Rental Fee Trigger</td>
            <td>End Date</td>
            <td align="center"><input type="checkbox" id="overrideAll" onclick="markAllOverride(this, 'S')"<%if (override) out.write(" checked=\"checked\"");%> />&nbsp;Override</td>
            <td align="center"><input type="checkbox" id="updateAllS" onclick="markAllUpdate(this, 'S')"<%if (date.length() > 0) out.write(" checked=\"checked\"");%> />&nbsp;Update</td>
            <% if (hasCommissions) { %><td>Commission Bank</td><% } %>
        </tr>
		<% while( sfee.next()) {
			String itemId = sfee.getFormattedValue("FEE_ID");
			int bankId = ConvertUtils.getIntSafely(sfee.getFormattedValue("COMMISSION_BANK_ID"), -1); 
			String frequencyId=sfee.getFormattedValue("FREQUENCY_ID");
			Character initiationType = sfee.getValue("INITIATION_TYPE_CD", Character.class);
            %>
		<tr>
			<td nowrap="nowrap">
				<b><%=sfee.getFormattedValue("FEE_NAME")%></b>
				<input type="hidden" id="old_feeamount_S<%=itemId%>" name="old_feeamount_S<%=itemId%>" value="<%=sfee.getFormattedValue("FEE_AMOUNT")%>" />
				<% if (hasCommissions) { %>
					<input type="hidden" id="old_commissionamount_S<%=itemId%>" name="old_commissionamount_S<%=itemId%>" value="<%=sfee.getFormattedValue("COMMISSION_AMOUNT")%>" />
					<input type="hidden" id="old_commissionbank_S<%=itemId%>" name="old_commissionbank_S<%=itemId%>" value="<%=bankId%>" />
				<% } %>
			</td>
			<%
			double feeAmount = ConvertUtils.getDouble(sfee.getFormattedValue("FEE_AMOUNT"), 0);
			double dynamicMin = ConvertUtils.getDouble(sfee.getFormattedValue("DYNAMIC_MIN_AMOUNT"), 0);
			double dynamicMax = ConvertUtils.getDouble(sfee.getFormattedValue("DYNAMIC_MAX_AMOUNT"), 0);
			double dynamicAmount = ConvertUtils.getDouble(sfee.getFormattedValue("DYNAMIC_AMOUNT_PER_TRANS"), 0);
			boolean isDynamic = dynamicMin != 0 || dynamicMax != 0 || dynamicAmount != 0;
			char dynamicFeeInd = ConvertUtils.getCharSafely(sfee.getFormattedValue("DYNAMIC_FEE_IND"), 'N');
			%>
			
			<% switch(initiationType) {
			     case 'A': case 'P': %>
	                <td class="inactive">&nbsp;</td><%
	                break;
			     default: %>
			<td> 
			<select id="serviceFeeFrequencyId_S<%=itemId%>" name="serviceFeeFrequencyId_S<%=itemId%>" onchange="markIt(this)">
				<option value="2" <%if(frequencyId.equals("2")){ %>selected<%} %> >Monthly</option>
				<option value="4" <%if (isDynamic) {%> style="display:none" <%}%> <%if(frequencyId.equals("4")){ %>selected<%} %> >Daily</option>
			</select>
			</td><%
			} %>
			
			<td>
				<div style="display:block">
					<div>
						<input type="text" name="feeamount_S<%=itemId%>" id="feeamount_S<%=itemId%>" size="6" maxlength="16" <%if(isDynamic) {%>value="" disabled="true"<%}else{ %>value="<%=sfee.getFormattedValue("FEE_AMOUNT")%>"<%} %> onchange="markIt(this)" />
						<%if (dynamicFeeInd=='Y') {%>
							<input type="checkbox" name="feeDynamic_S<%=itemId%>" id="feeDynamic_S<%=itemId%>" onchange="onDynamicFeeChange(<%=itemId%>, <%=feeAmount%>, <%=dynamicMin%>, <%=dynamicMax%>, <%=dynamicAmount%>)" <%if (isDynamic) {%>checked="checked"<%} %> onclick="markIt(this)">Dynamic</input>
						<%} %>
					</div>
					<div name="dynamicValues_S<%=itemId%>" id="dynamicValues_S<%=itemId%>" <%if (isDynamic) {%>style="display:"<%} else{%> style="display:none"<%} %>>
					<%if (dynamicFeeInd=='Y') {%>
						<div>
							<input type="text" name="dynamicMin_S<%=itemId%>" id="dynamicMin_S<%=itemId%>" size="6" maxlength="16" value="<%=sfee.getFormattedValue("DYNAMIC_MIN_AMOUNT")%>" onchange="markIt(this)"/>
					  		Min
						</div>
						<div>
							<input type="text" name="dynamicMax_S<%=itemId%>" id="dynamicMax_S<%=itemId%>" size="6" maxlength="16" value="<%=sfee.getFormattedValue("DYNAMIC_MAX_AMOUNT")%>" onchange="markIt(this)"/>
					  		Max
						</div>
						<div>
						<input type="text" name="dynamicAmount_S<%=itemId%>" id="dynamicAmount_S<%=itemId%>" size="6" maxlength="16" value="<%=sfee.getFormattedValue("DYNAMIC_AMOUNT_PER_TRANS")%>" onchange="markIt(this)"/>
							Per trans
						</div>
					<%} %>
					</div>	
				</div>
			</td>
			<% if (hasCommissions && itemId.equals("1")) { %>
				<td><input type="text" name="commissionamount_S<%=itemId%>" id="commissionamount_S<%=itemId%>" size="10" maxlength="16" value="<%=sfee.getFormattedValue("COMMISSION_AMOUNT")%>" onchange="markIt(this)" /></td>
			<% } else if (hasCommissions) { %>
				<td class="inactive">&nbsp;</td>
			<% } %>
			<td class="inactive">
			<%if(initiationType == 'R') { %>
				<input type="text" name="inactivefeeamount_S<%=itemId%>" id="inactivefeeamount_S<%=itemId%>" size="10" maxlength="16" value="<%=sfee.getFormattedValue("INACTIVE_FEE_AMOUNT")%>" onchange="markIt(this)" />
				, <input type="text" name="inactivefeeremaining_S<%=itemId%>" id="inactivefeeremaining_S<%=itemId%>" size="4" maxlength="4" value="<%=sfee.getFormattedValue("INACTIVE_FEES_REMAINING")%>" onchange="markIt(this)" />
				times
			<%} else { %>
				&nbsp;<%} %>
			</td>
			<td><input type="text" maxlength="19" name="date1_S<%=itemId%>" id="date1_S<%=itemId%>" size="17" value="<%=action == TerminalActions.ACT_CHANGE_FEE_EFFECTIVE_DATE ? StringUtils.encodeForHTMLAttribute(date) : sfee.getFormattedValue("START_DATE")%>" onchange="markIt(this)" /></td>
			<%if ("Y".equalsIgnoreCase(sfee.getFormattedValue("GRACE_PERIOD_IND"))) {%>
				<td><input type="text" maxlength="3" name="gracedays_S<%=itemId%>" id="gracedays_S<%=itemId%>" size="5" value="<%=sfee.getFormattedValue("GRACE_DAYS")%>" onchange="markIt(this)" /></td>
				<td align="center"><input type="checkbox" name="noTriggerEventFlag_S<%=itemId %>" id="noTriggerEventFlag_S<%=itemId %>" value="Y"<%if ("Y".equalsIgnoreCase(sfee.getFormattedValue("NO_TRIGGER_EVENT_FLAG"))) out.write(" checked=\"checked\"");%> onclick="markIt(this)" /></td>
			<%} else {%>
				<td class="inactive">&nbsp;</td>
				<td class="inactive">&nbsp;</td>
			<%}%>
			<td><input type="text" maxlength="19" name="date2_S<%=itemId%>" id="date2_S<%=itemId%>" size="17" value="<%=action == TerminalActions.ACT_CHANGE_FEE_END_DATE ? date : sfee.getFormattedValue("END_DATE")%>" onchange="markIt(this)" /></td>
			<td align="center"><input type="checkbox" name="override_S<%=itemId%>" id="override_S<%=itemId%>" onclick="markIt(this)"<%if (override) out.write(" checked=\"checked\"");%> /></td>
			<td align="center"><input type="checkbox" name="update" id="update_S<%=itemId%>" value="S<%=itemId%>"<%if (date.length() > 0) out.write(" checked=\"checked\"");%> /></td>
			<% if (hasCommissions && itemId.equals("1")) { %>
				<td align="center">
					<select id="commissionbank_S<%=itemId%>" name="commissionbank_S<%=itemId%>">
						<option value="0">Please select:</option>
						<% for(Map.Entry<Integer, String> entry : commissionBanks.entrySet()) { %>
						<option value="<%=entry.getKey()%>" <%=(entry.getKey() == bankId ? "selected=\"selected\"" : "") %>><%=entry.getValue() %></option>
						<% } %>
					</select>
				</td>
			<% } else if (hasCommissions) { %>
				<td class="inactive">&nbsp;</td>
			<% } %>
		</tr>
		<%}%>		
		<tr>
		<td align="left" colspan="<%=colSpan-1%>"><b>Terminate ePort Quick Start Fees when device is inactive for </b><input type="text" name="eportQSInactivityDays" id="eportQSInactivityDays" size="4" maxlength="3" value="<%=qsInactiveDays.getFormattedValue("quickStartInactiveDays")%>" onchange="markIt(this)" /><b> days</b></td>
		<td align="center"><input type="checkbox" name="update" id="update_S900" value="S900"<%if (date.length() > 0) out.write(" checked=\"checked\"");%> /></td>
		</tr>
		<tr class="gridHeader">
			<td colspan="<%=colSpan%>" align="center">
				<input type="button" value="Effective Date" onclick="setdate(<%=TerminalActions.ACT_CHANGE_FEE_EFFECTIVE_DATE%>)" class="cssButton"/>
				<input type="button" value="End Date" onclick="setdate(<%=TerminalActions.ACT_CHANGE_FEE_END_DATE%>)" class="cssButton"/>
				<input name="saveBtn" id="saveBtn" type="button" value="Save" onclick="saveAll()" class="cssButton"/>
				<input type="button" value="Cancel" onclick="window.location = '/editTerminals.i?terminalId=<%=terminalId %>';" class="cssButton" />
			</td>
		</tr>
		
</table>
<script type="text/javascript">
function onDynamicFeeChange(itemId, feeAmount, dynamicMin, dynamicMax, dynamicAmount){
	var frequency = document.getElementById('serviceFeeFrequencyId_S'+itemId);
	if ($('feeDynamic_S'+itemId).checked){
		$('dynamicValues_S'+itemId).setStyle('display', '');
		$('feeamount_S'+itemId).value='';
		$('feeamount_S'+itemId).disabled="true";
		$('dynamicMin_S'+itemId).value=dynamicMin;
		$('dynamicMax_S'+itemId).value=dynamicMax;
		$('dynamicAmount_S'+itemId).value=dynamicAmount;
		frequency[1].style.display="none";
		frequency[0].selected=true;
	}else{
		$('dynamicValues_S'+itemId).setStyle('display', 'none');
		$('feeamount_S'+itemId).value=feeAmount;
		$('feeamount_S'+itemId).disabled="";
		$('dynamicMin_S'+itemId).value='';
		$('dynamicMax_S'+itemId).value='';
		$('dynamicAmount_S'+itemId).value='';
		frequency[1].style.display="inline";
	}	
}
</script>
<div class="spacer5"></div>
