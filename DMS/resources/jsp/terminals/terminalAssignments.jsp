<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Results masterAccountUsers = RequestUtils.getAttribute(request, "masterAccountUsers", Results.class, true);
Results terminals = RequestUtils.getAttribute(request, "terminals", Results.class, true);
int userId = inputForm.getInt("user_id", false, -1);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="formContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Terminal Assignments</span></div>
</div>

<div class="tableContent">

<form method="post" action="terminalAssignments.i" onsubmit="return doSubmit()">

<table class="padding3">
	<tr>
		<td class="label">Master Account User</td>
		<td>
			<select name="user_id" id="user_id" onchange="window.location = '/terminalAssignments.i?user_id=' + this.value">
			<option value="">Please select:</option>
			<%if (masterAccountUsers != null){ while(masterAccountUsers.next()) {
			%><option value="<%=masterAccountUsers.getValue("USER_ID")%>"<%if (userId == masterAccountUsers.getValue("USER_ID", int.class)) { %> selected="selected"<%}%>>
				<%=masterAccountUsers.getFormattedValue("USER_NAME")%>: <%=masterAccountUsers.getFormattedValue("FIRST_NAME")%> <%=masterAccountUsers.getFormattedValue("LAST_NAME")%>
			</option>	    	
			<%}}%>	
			</select>
		</td>
	</tr>		
	<tr>
		<td class="label" valign="top">Serial Numbers<font color="red">*</font><br />(1 per line)</td>
		<td><textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"><%if (terminals != null){ while(terminals.next()) {out.println(terminals.getFormattedValue("EPORT_SERIAL_NUM"));}}%></textarea></td>
	</tr>
	<tr>
		<td class="label">Allow Edit</td>
		<td><input type="checkbox" name="allow_edit" id="allow_edit" value="Y" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<input type="submit" name="action" value="Submit" class="cssButton" />
		</td>
	</tr>
</table>

</form>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
function doSubmit(){	
	if(document.getElementById("user_id").value == ''){
		alert('Please select user');
		return false;
	}

	return true;
}
</script>
