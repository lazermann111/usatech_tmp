<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.Date"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.dms.action.TerminalActions"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	int action = inputForm.getInt("dateAction", false, 0);
	String title;
	if (action == TerminalActions.ACT_CHANGE_START_DATE)
		title = "Activation Start Date";
	else if (action == TerminalActions.ACT_CHANGE_END_DATE)
		title = "Activation End Date";
	else if(action == TerminalActions.ACT_CHANGE_BANK_ACCT_DATE)
	    title = "Bank Account Assignment Date";
	else if(action == TerminalActions.ACT_DEACTIVATE_EPORT)
	    title = "Deactivation Date";
	else if(action == TerminalActions.ACT_CHANGE_FEE_EFFECTIVE_DATE)
		title = "Fee Effective Date";
	else if(action == TerminalActions.ACT_CHANGE_FEE_END_DATE)
		title = "Fee End Date";
	else
		title = "Date Selection";
	String dateTxtStr = (String) inputForm.getAttribute("dateTxtStr");
	String timeTxtStr = (String) inputForm.getAttribute("timeTxtStr");
	String startDateStr = inputForm.getString("startDateStr", false);
    String endDateStr = inputForm.getString("endDateStr", false);
    Date startDate = (Date) inputForm.getAttribute("startDate");
	Date endDate = (Date) inputForm.getAttribute("endDate");
	Date date;
    switch(action) {
        case TerminalActions.ACT_CHANGE_START_DATE:
        case TerminalActions.ACT_ASSIGN_EPORT:
        case TerminalActions.ACT_CHANGE_BANK_ACCT_DATE:
            date = startDate;
            break;
        case TerminalActions.ACT_CHANGE_FEE_EFFECTIVE_DATE:
        case TerminalActions.ACT_CHANGE_END_DATE:
        case TerminalActions.ACT_CHANGE_FEE_END_DATE:
        case TerminalActions.ACT_DEACTIVATE_EPORT:
        	date = endDate;
            break;
        default:
        	date = startDate;
            break;
    }
    String dateStr = Helper.getDateTime(date);
    Date preEndDate = (Date) inputForm.getAttribute("preEndDate");
	String preEndDateStr = Helper.getDateTime(preEndDate);
	Date postStartDate = (Date) inputForm.getAttribute("postStartDate");
	String postStartDateStr = Helper.getDateTime(postStartDate);
	String allowEarlier = inputForm.getString("allowEarlier", false);
	long eportId = inputForm.getLong("eportId", false, -1);
	long terminalId = inputForm.getLong("terminalId", false, -1);
	String terminalNbr = inputForm.getString("terminalNbr", false);
	String serialNbr = inputForm.getString("serialNbr", false);
	long terminalEportId = inputForm.getLong("terminalEportId", false, -1);
	long customerBankId = inputForm.getLong("customerBankId", false, -1);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold"><%=title%></span></div>
</div>

<div class="spacer10"></div>

<div class="tableContent">
<form name="dateForm" id="dateForm" method="post" action="editTerminals.i">
<input type="hidden" name="dateAction" value="<%=action %>" />
<input type="hidden" name="terminalId" value="<%=terminalId %>" />
<input type="hidden" name="eportId" value="<%=eportId %>" />
<input type="hidden" name="startDateStr" value="<%=StringUtils.prepareCDATA(startDateStr) %>" />
<input type="hidden" name="endDateStr" value="<%=StringUtils.prepareCDATA(endDateStr) %>" />
<input type="hidden" name="terminalEportId" value="<%=terminalEportId %>" />
<input type="hidden" name="customerBankId" value="<%=customerBankId %>" /> 

<table align="center">
	<tr>
		<td class="formLabel">Terminal:</td><td><a href="/editTerminals.i?terminalId=<%=terminalId %>"><%=StringUtils.encodeForHTML(terminalNbr) %></a></td>
		<%if (StringHelper.isBlank(serialNbr)) { %>
		<td colspan="3">&nbsp;</td>
		<%} else {%>
		<td>&nbsp;&nbsp;</td>
		<td class="formLabel">Serial Number:</td><td><%=serialNbr %></td>
		<%}%>
	</tr>
	<tr><td colspan="5"><div class="spacer1"></div></td></tr>
	<tr>
		<td class="formLabel">Date:</td>
		<td><input type="text" class="cssText" name="dateTxt"
			onchange="handleDateChange();" id="dateTxt" value="<%=StringUtils.encodeForHTMLAttribute(dateTxtStr)%>"
			size="10" maxlength="10" />&nbsp;
			<img src="/images/calendar.gif" id="from_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td>&nbsp;&nbsp;</td>
		<td class="formLabel">Time:</td>
		<td><input type="text" class="cssText" name="timeTxt"
			onchange="handleDateChange();" id="timeTxt" size="10" maxlength="8"
			value="<%=StringUtils.encodeForHTMLAttribute(timeTxtStr)%>" /></td>
	</tr>
	<tr><td colspan="5"><div class="spacer1"></div></td></tr>
	<tr>
		<td colspan="5" align="center">
			<input type="checkbox" name="overrideChk" id="overrideChk" value="Y" disabled="disabled" />
			Override Already Paid Check
		</td>
	</tr>
	<tr><td colspan="5"><div class="spacer1"></div></td></tr>
<%if(action == TerminalActions.ACT_DEACTIVATE_EPORT){ 
	Results deactivateDetails=DataLayerMgr.executeQuery("GET_DEACTIVATE_DETAIL", null);
%>
	<tr>
	<td colspan="5">
	Deactivation Reason/Details:
	<select name=deactivateDetailId id="deactivateDetailId">
			<%if (deactivateDetails != null){
				int deactivateDetailId = inputForm.getInt("deactivateDetailId", false, -1);
			while(deactivateDetails.next()) {
			%><option value="<%=deactivateDetails.getValue("deactivateDetailId")%>"<%if (deactivateDetailId> 0 && deactivateDetails.getValue("deactivateDetailId",int.class)==deactivateDetailId) out.write(" selected=\"selected\"");%>><%=deactivateDetails.getFormattedValue("deactivateDetail")%></option>	    	
			<%}}%>	
	</select>
	</td>
	</tr>
	<tr>
	<td colspan="5">
	Deactivation Reason/Details Additional Info:
	<input id="deactivateDetail" class="cssText" type="text" size="100" value="" name="deactivateDetail" />
	</td>
	</tr>
<%} %>
	<tr>
		<td colspan="5" align="center">
			<input type="button" value="Submit" class="cssButton" onclick="processChanges();" />
			&nbsp;
			<input type="button" value="Cancel" class="cssButton" onclick="window.location = '/editTerminals.i?terminalId=<%=terminalId %>';" />
		</td>
	</tr>
</table>

<div class="spacer5"></div>

<% if(action >= TerminalActions.ACT_CHANGE_START_DATE && action <= TerminalActions.ACT_CHANGE_FEE_END_DATE) { %>
<div class="innerTable">
<table class="tabDataDisplayBorder" id="transTabl">	
	<tr class="gridHeader">
		<td colspan="5">Loading... </td>
	</tr>
</table>
</div>
<%}%>

<div class="spacer5"></div>

</form>
</div>

</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">

    Calendar.setup({
        inputField     :    "dateTxt",             // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });

    var dateTxt = document.getElementById("dateTxt");
    var timeTxt = document.getElementById("timeTxt");
    var overrideChk = document.getElementById("overrideChk");
    var allowEarlier = <%="N".equalsIgnoreCase(allowEarlier) ? "false" : "true"%>;
    var action = <%=action%>;
    var startDate = <%=StringHelper.isBlank(startDateStr) ? "null" : "Date.parse('" + startDateStr + "')"%>;
    var endDate = <%=StringHelper.isBlank(endDateStr) ? "null" : "Date.parse('" + endDateStr + "')"%>;
    var preEndDate = <%=StringHelper.isBlank(preEndDateStr) ? "null" : "Date.parse('" + preEndDateStr + "')"%>;
    var postStartDate = <%=StringHelper.isBlank(postStartDateStr) ? "null" : "Date.parse('" + postStartDateStr + "')"%>;

    function handleDateSelect(date, time) {
        dateTxt.value = date;
        timeTxt.value = time;
        setAffectedTrans();
    }
    
    function handleDateChange() {
        if(validateDate())
        	setAffectedTrans();
    }
    
    function validateDate() {
        if(!isDate(dateTxt.value)) {
            dateTxt.focus();
            return false;
        }
        if(!isTime(timeTxt.value)) {
            timeTxt.focus();
            return false;
        }
        var today = new Date();
        var parsedDate = Date.parse(dateTxt.value);
        var days = Math.round((parsedDate-today)/1000*60*60*24);
        if(!allowEarlier && days < 0) {
            alert("Please select a date that is later than the current date");
            return false;
        }
        var parsedDateTime = Date.parse(dateTxt.value + " " + timeTxt.value);
        <% if(action == TerminalActions.ACT_CHANGE_START_DATE) { %>
        if(endDate != null) {
            if((endDate - parsedDateTime)/1000 <= 0) {
                alert("New start date cannot be >= end date <%=StringUtils.encodeForJavaScript(endDateStr)%>");
                return false;
            }
        }
        if(preEndDate != null) {
            if((parsedDateTime - preEndDate) / 1000 <= 0) {
                alert("New start date cannot be <= previous end date <%=preEndDateStr%>");
                return false;
            }
        }
        <% } else if(action == TerminalActions.ACT_CHANGE_END_DATE || action == TerminalActions.ACT_DEACTIVATE_EPORT) {%>
        if(startDate != null) {
            if((parsedDateTime - startDate) / 1000 <= 0) {
                alert("<%=action == TerminalActions.ACT_DEACTIVATE_EPORT ? "Deactivation" : "New end"%>" + " date cannot be <= start date <%=StringUtils.encodeForJavaScript(startDateStr)%>");
                return false;
            }
        }
        if(postStartDate != null) {
            if((postStartDate - parsedDateTime) / 1000 <= 0) {
                alert("<%=action == TerminalActions.ACT_DEACTIVATE_EPORT ? "Deactivation" : "New end"%>" + " date cannot be >= next start date <%=postStartDateStr%>");
                return false;
            }
        }
        <% } %>    
        return true;
    }

    function processChanges() {
    	if (validateDate()) {
        <% if(action == TerminalActions.ACT_CHANGE_FEE_EFFECTIVE_DATE || action == TerminalActions.ACT_CHANGE_FEE_END_DATE) { %>
        	var url = "/viewFees.i?dateAction=<%=action%>&terminalId=<%=terminalId%>&terminalNbr=<%=terminalNbr%>&date=" + dateTxt.value + " " + timeTxt.value;
        	if (overrideChk.checked)
            	url += "&override=Y";
        	window.location = url;
        <%} else {%>
			dateForm.submit();
        <%}%>
    	}
    }
    
    var affectedTransRequest = new Ajax("affectedTransList.i", { method: "get", update: $("transTabl"), evalScripts: true });
    
    function setAffectedTrans() {
        <% if(action >= TerminalActions.ACT_CHANGE_START_DATE && action <= TerminalActions.ACT_CHANGE_FEE_END_DATE) { %>
            affectedTransRequest.send("affectedTransList.i", Object.toQueryString({
        	dateStr: "<%=dateStr%>",
        	postStartDateStr: "<%=postStartDateStr%>",
        	dateTimeTxt: dateTxt.value + " " + timeTxt.value,
        	eportId: <%=eportId%>,
        	terminalId: <%=terminalId%>,
        	dateAction: <%=action%>
        }));
        <% } %>
    }
   
    window.addEvent('domready', setAffectedTrans);
</script>
