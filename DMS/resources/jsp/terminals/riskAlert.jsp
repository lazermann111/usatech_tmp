<%@page import="simple.text.StringUtils"%>
<%@page import="simple.bean.converters.DateConverter"%>
<%@page import="com.usatech.dms.model.RiskAlertDetails"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.dms.model.RiskAlert"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.List"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<% 
Long riskAlertId = RequestUtils.getAttribute(request, "riskAlertId", Long.class, true);
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "riskAlertId");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, riskAlertId);

InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
RiskAlertDetails riskAlert = (RiskAlertDetails)inputForm.getAttribute("riskAlert"); 
%>

<div class="formContainer">
	<div class="tableHead">
		<div class="tabHeadTxt">
			<span class="txtWhiteBold">Risk Alert</span>
		</div>
	</div>

	<div class="tableContent">
		<form name="editForm" id="editForm" method="post">
			<div id="statusMessage" class="status-info"><%=RequestUtils.getAttributeDefault(request, "statusMessage", String.class, "")%></div>
			<table class="tabFormControls">
				<tbody>
					<tr>
						<td width="15%" class="formLabel">Type:</td>
						<td class="control">
							<span id="riskAlertId"><%=riskAlert.hasTerminal() ? "Terminal Activity Alert" : "Customer Activity Alert"%></span>
						</td>
					</tr>
					<% if (riskAlert.hasCustomer()) { %>
					<tr>
						<td class="formLabel">Customer Name:</td>
						<td class="control">
							<span id="customerName"><%=riskAlert.getCustomerName() == null ? "" : StringUtils.prepareHTML(riskAlert.getCustomerName())%>
								<input type="button" value="Show" id="btnShowCustomer" onclick="window.location = '/customer.i?customerId=<%=riskAlert.getCustomerId()%>';" />
							</span>
						</td>
					</tr>
					<% } %>
					<% if (riskAlert.hasTerminal()) { %>
					<tr>
						<td class="formLabel">Terminal:</td>
						<td class="control">
							<span id="terminalId"><%=StringUtils.prepareHTML(riskAlert.getTerminalNbr())%>
								<input type="button" value="Show" id="btnShowTerminal" onclick="window.location = '/editTerminals.i?terminalId=<%=riskAlert.getTerminalId()%>';" />
							</span>
						</td>
					</tr>
					<tr>
						<td class="formLabel">ePort Serial Number:</td>
						<td class="control">
							<span id="deviceSerialCd"><%=riskAlert.getDeviceSerialCd() == null ? "" : StringUtils.prepareHTML(riskAlert.getDeviceSerialCd())%></span>
						</td>
					</tr>
					<tr>
						<td class="formLabel">Device Type:</td>
						<td class="control">
							<span id="deviceTypeDesc"><%=riskAlert.getDeviceTypeDesc() == null ? "" : StringUtils.prepareHTML(riskAlert.getDeviceTypeDesc())%></span>
						</td>
					</tr>
					<tr>
						<td class="formLabel">Location:</td>
						<td class="control">
							<span id="locationName"><%=riskAlert.getLocationName() == null ? "" : StringUtils.prepareHTML(riskAlert.getLocationName())%></span>
						</td>
					</tr>
					<% } %>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="15%" class="formLabel">Risk Alert Id:</td>
						<td class="control">
							<span id="riskAlertId"><%=riskAlert.getId()%></span>
						</td>
					</tr>
					<tr>
						<td width="15%" class="formLabel">Alert Timestamp:</td>
						<td class="control">
							<span id="alertTime"><%=DateConverter.dateFormat.format(riskAlert.getAlertTime())%></span>
						</td>
					</tr>
					<tr>
						<td width="15%" class="formLabel">Score:</td>
						<td class="control">
							<span id="score"><%=riskAlert.getScore()%></span>
						</td>
					</tr>
					<tr>
						<td width="15%" class="formLabel">Message:</td>
						<td class="control">
							<textarea id="message" name="message" disabled="disabled" style="overflow:scroll;width:600px;height:100px;" wrap="off"><%=StringUtils.prepareCDATA(riskAlert.getMessage())%></textarea>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="15%" class="formLabel">Status</td>
						<td class="control">
							<select id="statusCd" name="statusCd">
								<% for(RiskAlert.Status status : RiskAlert.Status.values()) { %>
								<option value="<%=status.toString()%>" <%=(status.equals(riskAlert.getStatus()) ? "selected=\"selected\"" : "")%>><%=StringUtils.prepareHTML(status.getName())%></option>
								<% } %>
							</select>
						</td>
					</tr>
					<tr>
						<td width="15%" class="formLabel">Investigator Notes:</td>
						<td class="control">
							<textarea id="notes" name="notes" style="overflow:scroll;width:600px;height:100px;" wrap="off"><%=riskAlert.getNotes() == null ? "" : StringUtils.prepareCDATA(riskAlert.getNotes())%></textarea>
						</td>
					</tr>
					<tr>
						<td width="15%" class="formLabel">Last Updated:</td>
						<td class="control">
							<span id="alertTime"><%=DateConverter.dateFormat.format(riskAlert.getLastUpdated())%></span>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="control" style="text-align: center">
							<input name="action" id="action" type="submit" value="Save Changes" class="cssButtonDisabled"/>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>

	<div class="spacer10"></div>

</div>

<script type="text/javascript" defer="defer">

function disableButtons() {
	disableButton(saveChangesBtn);
}

function enableButtons() {
	disableButton(saveChangesBtn);
}

</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />