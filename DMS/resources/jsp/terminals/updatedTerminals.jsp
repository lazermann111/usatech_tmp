<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="com.usatech.dms.model.BusinessUnit"%>
<%@page import="com.usatech.dms.model.Currency"%>
<%@page import="com.usatech.layers.common.model.Customer"%>
<%@page import="com.usatech.layers.common.model.CustomerBankTerminal"%>
<%@page import="com.usatech.dms.model.PaymentSchedule"%>
<%@page import="com.usatech.dms.model.Terminal"%>
<%@page import="com.usatech.dms.model.TerminalEport"%>

<%@page import="java.util.List"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    List<Terminal> terminals = (List<Terminal>)inputForm.getAttribute("terminals");
%>

<div class="largeFormContainer">

<div class="tableHead">
<div class="tabHeadTxt">Updated Terminals</div>
</div>
<form method="post">
<table class="tabDataDisplayNoBorder">
	<tbody>
		<tr>
			<td rowspan="2" valign="top" style="width: 180px;"><b>Modified Terminals</b><br />
			<select name="terminalSelect" id="terminalSelect" size="20"
				onchange="handleChange(this.value)" style="width: 99%;" multiple="multiple">
				<%
				    for (Terminal terminalItem : terminals)
				    {
				%>
				<option value="<%=terminalItem.getId()%>"><%=terminalItem.getTerminalNumber()%></option>
				<%
				    }
				%>
			</select></td>

			<td valign="top">
			<div class="gridHeader">Modification Details</div>
			<table id="terminalDetails" class="tabDataDisplayAutoBorder">

				<tr class="gridHeader">
					<td>ePorts</td>
					<td>Attribute</td>
					<td>Old Value</td>
					<td>New Value</td>
					<td>Update Date</td>
				</tr>

			</table>
			</td>

		</tr>

		<tr>
			<td align="center" valign="bottom"><input name="action"
				id="acceptBtn" type="submit" value="Accept"
				class="cssButtonDisabled" disabled="disabled" />
			</td>
		</tr>
	</tbody>
</table>
</form>
<div class="spacer5"></div>
</div>

<script type="text/javascript">
var xmlHttp;
var terminalDetails = document.getElementById("terminalDetails");
var terminalDetailsString = "";

function handleChange(terminalId) {
	xmlHttp = getNewHttpObject();
	xmlHttp.onreadystatechange = handleStateChange;
	xmlHttp.open("GET", "updatedTerminalsChange.i?terminalId=" + terminalId, true);
	xmlHttp.send(null);
	
}

function handleStateChange() {
	if(xmlHttp.readyState == 4) {
		if(xmlHttp.status == 200) {
			processResponse(xmlHttp.responseXML);
			
		}
	}
}

function processResponse(response) {
	var terminals = response.getElementsByTagName("Terminal");

	document.getElementById('acceptBtn').className = "cssButton";
	document.getElementById('acceptBtn').removeAttribute("disabled");
	deleteTerminalDetails();
	for(var i=0; i<terminals.length; i++) {
		createRow(terminals[i], i+1);
	}
}

function deleteTerminalDetails() {
	for(var i=terminalDetails.rows.length-1; i>=1; i--) {
		terminalDetails.deleteRow(i);
	}
}

function createRow(terminal, rowNumber) {
	var rowItem = terminalDetails.insertRow(rowNumber);
	for(var i=0; i<5; i++) {
		rowItem.insertCell(i);
	}
	rowItem.cells[0].innerHTML = terminal.getElementsByTagName("EportSerialNumber")[0].hasChildNodes() ? terminal.getElementsByTagName("EportSerialNumber")[0].firstChild.nodeValue : "";
	rowItem.cells[1].innerHTML = terminal.getElementsByTagName("Attribute")[0].hasChildNodes() ? terminal.getElementsByTagName("Attribute")[0].firstChild.nodeValue : "";
	rowItem.cells[2].innerHTML = terminal.getElementsByTagName("OldValue")[0].hasChildNodes() ? terminal.getElementsByTagName("OldValue")[0].firstChild.nodeValue : "";
	rowItem.cells[3].innerHTML = terminal.getElementsByTagName("NewValue")[0].hasChildNodes() ? terminal.getElementsByTagName("NewValue")[0].firstChild.nodeValue : "";
	rowItem.cells[4].innerHTML = terminal.getElementsByTagName("UpdateDate")[0].hasChildNodes() ? terminal.getElementsByTagName("UpdateDate")[0].firstChild.nodeValue : "";
}

</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />