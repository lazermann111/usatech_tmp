<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
  int i = 0; 
  InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
  String sortField = PaginationUtil.getSortField(null);
  String sortIndex = inputForm.getString(sortField, false);
  sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "-7" : sortIndex;
  boolean norec=false;
    
  String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
  if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
    norec = true;
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Risk Alerts</div>
</div>
<table class="tabDataDisplayBorderNoFixedLayout">
  <thead>
    <tr class="sortHeader">
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Alert Id</a>
        <%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
      </td>
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Serial Number</a>
        <%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
      </td>
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Device Type</a>
        <%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
      </td>
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Customer Name</a>
        <%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
      </td>
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Location Name</a>
        <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
      </td>
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "-7", sortIndex)%>">Alert Date</a>
        <%=PaginationUtil.getSortingIconHtml("-7", sortIndex)%>
      </td>
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "-9", sortIndex)%>">Risk Score</a>
        <%=PaginationUtil.getSortingIconHtml("-9", sortIndex)%>
      </td>
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Alert Status</a>
        <%=PaginationUtil.getSortingIconHtml("10", sortIndex)%>
      </td>
      <td>
        <a href="<%=PaginationUtil.getSortingScript(null, "11", sortIndex)%>">Alert Message</a>
        <%=PaginationUtil.getSortingIconHtml("11", sortIndex)%>
      </td>
    </tr>
  </thead>
  <tbody>
  <% 
  if (norec){
      %>
  </tbody>
  </table>
  <div class="tabHead" align="center">NO RECORDS</div>
  <%} else {
  Results list = (Results) request.getAttribute("resultlist");
  while(list.next()) {
  %>
    <tr class="<%=(i++%2==0)?"row1":"row0"%>">
      <td><a href="/riskAlert.i?riskAlertId=<%=list.getFormattedValue("risk_alert_id")%>"><%=list.getFormattedValue("risk_alert_id")%></a></td>
      <td><%=list.getFormattedValue("device_serial_cd", "HTML")%></td>
      <td><%=list.getFormattedValue("device_type_desc", "HTML")%></td>
      <td><%=list.getFormattedValue("customer_name", "HTML")%></td>
      <td><%=list.getFormattedValue("location_name", "HTML")%></td>
      <td><%=list.getFormattedValue("alert_time", "HTML")%></td>
      <td><%=list.getFormattedValue("score", "HTML")%></td>
      <td><%=list.getFormattedValue("status_name", "HTML")%></td>
      <td><%=list.getFormattedValue("alert_message", "HTML")%></td>
    </tr>
  <% } %>
  </tbody>
</table>

<%
    String storedNames = PaginationUtil.encodeStoredNames(new String[] {
        DevicesConstants.PARAM_SEARCH_PARAM, DevicesConstants.PARAM_SEARCH_TYPE,
        DevicesConstants.PARAM_SERIAL_NUMBER, DevicesConstants.PARAM_TERMINAL_NUMBER,
        DevicesConstants.PARAM_CUSTOMER_NAME, DevicesConstants.PARAM_LOCATION_NAME,
        "customerId", "terminalId", DevicesConstants.PARAM_DEVICE_TYPE_ID});
%>

    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="riskAlerts.i" />
        <jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

<% } %>


</div>

<div class="spacer10"></div>
        
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
