<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results,java.math.BigDecimal"%>
<%@page import="java.util.Vector"%>
<%@page import="com.usatech.dms.action.TerminalActions"%>

<%
Results pfee2 = RequestUtils.getAttribute(request, "pfee", Results.class, true);
Results pfee = pfee2.clone();
Results sfee2 = RequestUtils.getAttribute(request, "sfee", Results.class, true);
Results sfee = sfee2.clone();
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="tableContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Terminal Fees</span></div>
</div>

<div class="tableContent">

<form name="mainForm" id="mainForm" method="post" action="updateFees.i">
<div id="divMainForm" >
<jsp:include page="viewFeesDetails.jsp" flush="true" />
</div>
</form>

</div>
</div>

<div class="spacer10"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">

var mainForm = document.getElementById("mainForm");
var terminalId = document.getElementById("terminalId");
var terminalNbr = document.getElementById("terminalNbr");
var divMainForm = document.getElementById("divMainForm");

function setdate(dateAction){
	var params = "dateAction=" + dateAction;
	params += "&terminalId=" + terminalId.value;
	params += "&terminalNbr=" + terminalNbr.value;
	window.location = 'dateSelection.i?' + params;
}

function saveAll(){
	var updateCount = 0
	for (i=0; i<mainForm.update.length; i++){
		if (mainForm.update[i].checked)
			updateCount++;
	}
	if (updateCount < 1) {
		alert('Please select fees for update');
		return;	
	}
	
	var qsInactivityDays = document.getElementById("eportQSInactivityDays").value.trim();
    if (qsInactivityDays != '' && (isNaN(qsInactivityDays) || qsInactivityDays < 0 )) {
        alert("ePort Quick Start fees device inactive days should be a number greater than or equal to zero");
        return false;
    }	
	<%
	while(pfee.next()) {
	String itemId = pfee.getFormattedValue("TRANS_TYPE_ID");
	%>	
	if (document.getElementById("old_feepercent_P<%=itemId%>").value.trim() != document.getElementById("feepercent_P<%=itemId%>").value.trim()) {
		if (document.getElementById("feepercent_P<%=itemId%>").value.trim() > 0 && document.getElementById("feepercent_P<%=itemId%>").value.trim() <= <%=pfee.getFormattedValue("FEE_PERCENT_LBN")%> && !confirm("Please confirm " + document.getElementById("feepercent_P<%=itemId%>").value.trim() + " Percent <%=pfee.getFormattedValue("TRANS_TYPE_NAME")%> Process Fee!"))
			return false;
		if (document.getElementById("feepercent_P<%=itemId%>").value.trim() >= <%=pfee.getFormattedValue("FEE_PERCENT_UBN")%> && !confirm("Please confirm " + document.getElementById("feepercent_P<%=itemId%>").value.trim() + " Percent <%=pfee.getFormattedValue("TRANS_TYPE_NAME")%> Process Fee!"))
			return false;
	}
	if (document.getElementById("old_feeamount_P<%=itemId%>").value.trim() != document.getElementById("feeamount_P<%=itemId%>").value.trim() && document.getElementById("feeamount_P<%=itemId%>").value.trim() >= <%=pfee.getFormattedValue("FEE_AMOUNT_UBN")%> && !confirm("Please confirm $" + document.getElementById("feeamount_P<%=itemId%>").value.trim() + " Fixed Amount <%=pfee.getFormattedValue("TRANS_TYPE_NAME")%> Process Fee!"))
		return false;
	if (document.getElementById("old_feemin_P<%=itemId%>").value.trim() != document.getElementById("feemin_P<%=itemId%>").value.trim() && document.getElementById("feemin_P<%=itemId%>").value.trim() >= <%=pfee.getFormattedValue("FEE_MIN_UBN")%> && !confirm("Please confirm $" + document.getElementById("feemin_P<%=itemId%>").value.trim() + " Minimum Amount <%=pfee.getFormattedValue("TRANS_TYPE_NAME")%> Process Fee!"))
		return false;
	<%}%>
		
	<%
	while(sfee.next()) {
	String itemId = sfee.getFormattedValue("FEE_ID");
	Character initiationType = sfee.getValue("INITIATION_TYPE_CD", Character.class);
	%>
	
	if (document.getElementById("old_feeamount_S<%=itemId%>").value.trim() != document.getElementById("feeamount_S<%=itemId%>").value.trim() && document.getElementById("feeamount_S<%=itemId%>").value.trim() >= <%=sfee.getFormattedValue("FEE_AMOUNT_UBN")%> && !confirm("Please confirm $" + document.getElementById("feeamount_S<%=itemId%>").value.trim() + " Fixed Amount <%=sfee.getFormattedValue("FEE_NAME")%>!"))
		return false;
	
	<%if ("Y".equalsIgnoreCase(sfee.getFormattedValue("GRACE_PERIOD_IND"))) {%>
	var gracedays_S<%=itemId%> = document.getElementById("gracedays_S<%=itemId%>").value.trim();
	if (gracedays_S<%=itemId%> == '' || isNaN(gracedays_S<%=itemId%>) || gracedays_S<%=itemId%> < <%=sfee.getFormattedValue("GRACE_DAYS_LBN")%> || gracedays_S<%=itemId%> > <%=sfee.getFormattedValue("GRACE_DAYS_UBN")%>) {
		alert("Grace Days should be a number between <%=sfee.getFormattedValue("GRACE_DAYS_LBN")%> and <%=sfee.getFormattedValue("GRACE_DAYS_UBN")%>");
		return false;
	}
	<%}
	if(initiationType == 'R') {%>
	var inactivefeeremaining_S<%=itemId%> = document.getElementById("inactivefeeremaining_S<%=itemId%>").value.trim();
    if (inactivefeeremaining_S<%=itemId%> != '' && (isNaN(gracedays_S<%=itemId%>) || inactivefeeremaining_S<%=itemId%> < 0 )) {
        alert("Inactive Fee Remaining should be a number greater than or equal to zero");
        return false;
    }
	<%
	}
	}%>
	
	new Ajax("updateFees.i", {method: mainForm.method, async: false, data: mainForm, update: divMainForm, evalScripts: true }).request();
}

function markIt(o){
	var update = document.getElementById("update" + o.name.substring(o.name.indexOf('_')));
	if (update)
		update.checked = true;
}

function markAllOverride(cb, prefix) {
	if (cb.checked)
		document.getElementById("updateAll"+prefix).checked = true;
	for (i=0; i<mainForm.update.length; i++){
		if(mainForm.update[i].id.indexOf("update_"+prefix) > -1){

			if(cb.checked)
				mainForm.update[i].checked = true;
			
			document.getElementById("override_" + mainForm.update[i].value).checked = cb.checked;
	    }
	}
}

function markAllUpdate(cb, prefix) {
	for (i=0; i<mainForm.update.length; i++){
		if(mainForm.update[i].id.indexOf("update_"+prefix) > -1)
			mainForm.update[i].checked = cb.checked;
	}
}

</script>
