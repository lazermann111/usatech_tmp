<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%
String customerId = ConvertUtils.getStringSafely(request.getParameter("customerId"), "");
if (StringHelper.isBlank(customerId))
	request.setAttribute("customers", DataLayerMgr.executeQuery("GET_ALL_CUSTOMERS", null));
else {
	request.setAttribute("idField", "id");
	request.setAttribute("nameField", "name");
	request.setAttribute("customers", DataLayerMgr.executeQuery("GET_CORP_CUSTOMER", new Object[]{customerId}));					
}
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="formContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Column Mapping</span></div>
</div>

<form name="mainForm" id="mainForm" method="post" action="columnMapping.i">

<div id="statusMessage" class="status-info" style="margin-left: 8px;"></div>

<table class="noPadding noSpacing">
	<tr>
		<td>&nbsp;
		<b>Customer</b>
		<select name="customerId" id="customerId" onchange="customerChange();">
			<jsp:include page="/jsp/generic/itemOptions.jsp" flush="true" />
		</select>
		<a href="javascript:getData('type=corp_customer&defaultValue=-9&defaultName=Choose Customer&selected=' + customerId.value + '&postScript=init();');"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>
</table>

<div class="spacer15"></div>

<div id="divMainForm" >
<jsp:include page="columnMappingDetails.jsp" flush="true" />
</div>

</form>
</div>
<script type="text/javascript">

var checked = false;
var mainForm = document.getElementById("mainForm");
var divMainForm = document.getElementById("divMainForm");
var divColumnMappingTerminal = document.getElementById("divColumnMappingTerminal");
var customerId = document.getElementById("customerId");
var terminalId = document.getElementById("terminalId");
var modelId = document.getElementById("modelId");
var terminalNbr = document.getElementById("terminalNbr");
var useas = document.getElementById("useas");
var map1 = document.getElementById("map1");
var map2 = document.getElementById("map2");
var pid = document.getElementById("pid");
var addBtn = document.getElementById("addBtn");
var mdb = document.getElementById("mdb");
var vend = document.getElementById("vend");
var act = document.getElementById("act");
var status = document.getElementById("status");
var mappings = document.getElementById("mappings");

function clickRemove(){
	if(terminalId.value == null || terminalId.value == ''){
		alert('Choose terminal please');
		return;
	}
	if(!confirm('Do you want to delete all mappings?'))
		return;
	makeCall('delete');
}
function addNew(){
	if(terminalId.value == null || terminalId.value == ''){
		alert('Choose terminal please');
		return;
	}
	if(mdb.value == null || mdb.value == '' || vend.value == null || vend.value == '' ){
		alert('Enter values please');
		return;
	}
	makeCall('addnew');
}

function updateMapppings(){
	if(terminalId.value == null || terminalId.value == ''){
		alert('Choose terminal please');
		return;
	}
	if(mappings.value == null || mappings.value == ''){
		if (!confirm('Do you want to delete all mappings?'))
			return;
	}
	else if (!confirm('Do you want to update all mappings?'))
		return;
	makeCall('updateMappings');
}

function clickAdd(){
	if(modelId.value == null || modelId.value == ''){
		alert('Choose a mapped terminal please');
		return;
	}
	if(terminalId.value == null || terminalId.value == '' || terminalId.value == modelId.value){
		alert('Choose an unmapped terminal please');
		return;
	}
	makeCall('copy');
}


function makeCall(act){
	var statusMessage = document.getElementById("statusMessage");
	statusMessage.innerHTML = "<img src='/images/spinner.gif' style='vertical-align:middle;' /> Please wait, loading data...";
	mainForm.act.value=act;
	new Ajax("columnMappingDetails.i", {method: mainForm.method, async: false,
		data: mainForm, 
		update: divMainForm, evalScripts: true }).request();
	statusMessage.innerHTML = "&nbsp;";
	init();
}

function makeTerminalCall(){
	var statusMessage = document.getElementById("statusMessage");
	statusMessage.innerHTML = "<img src='/images/spinner.gif' style='vertical-align:middle;' /> Please wait, loading data...";
	new Ajax("columnMappingTerminal.i", {method: mainForm.method, async: false,
		data: mainForm, 
		update: divColumnMappingTerminal, evalScripts: true }).request();
	statusMessage.innerHTML = "&nbsp;";
	init();
}

function init(){
	checked = false;
	mainForm = document.getElementById("mainForm");
	divMainForm = document.getElementById("divMainForm");
	divColumnMappingTerminal = document.getElementById("divColumnMappingTerminal");
	customerId = document.getElementById("customerId");
	terminalId = document.getElementById("terminalId");
	modelId = document.getElementById("modelId");
	terminalNbr = document.getElementById("terminalNbr");
	useas = document.getElementById("useas");
	map1 = document.getElementById("map1");
	map2 = document.getElementById("map2");
	pid = document.getElementById("pid");
	addBtn = document.getElementById("addBtn");
	mdb = document.getElementById("mdb");
	vend = document.getElementById("vend");
	act = document.getElementById("act");
	status = document.getElementById("status");
	mappings = document.getElementById("mappings");
}
function doIt(){
	checked=!checked;
	if(mainForm.pid!=null){
		if(mainForm.pid.length>0){
			for (i=0; i<mainForm.pid.length; i++){
				mainForm.pid[i].checked=checked;
			}
		}else{
			mainForm.pid.checked=checked;
		}
	}
}

function customerChange(){
	terminalId.value = '';
	terminalNbr.value = '';
	makeCall();
}

function map2Change(){
	if(map2.selectedIndex==null||map2.selectedIndex==-1)
		return;
	terminalId.value=map2.value;
	terminalNbr.value=map2.options[map2.selectedIndex].text;
	makeTerminalCall();
	useas.removeAttribute("disabled");	
}

function map1Change(){
	if(map1.selectedIndex==null||map1.selectedIndex==-1)
		return;
	terminalId.value=map1.value;
	terminalNbr.value=map1.options[map1.selectedIndex].text;
	makeTerminalCall();
}

function enableButton(item) {
	item.className = "cssButton";
	item.removeAttribute("disabled");
}

function clickIt(){
	modelId.value=map2.value;
	enableButton(addBtn);
}

function deleteAll(){
	if( !ifChecked() ){
		alert('Choose mapping please');
		return;
	}
	makeCall('delcol');
}

function ifChecked(){
	if(mainForm.pid.length>0){
		for (i=0; i<mainForm.pid.length; i++){
			if(mainForm.pid[i].checked)
				return true;
		}
	}else{
		return mainForm.pid.checked;
	}	
	return false;

}
</script>

<div class="spacer10"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
