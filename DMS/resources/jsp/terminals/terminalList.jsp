<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    Results terminalList = (Results)inputForm.getAttribute("terminalList");
    int authorityId = inputForm.getInt("authority_id", false, -1);
    int merchantId = inputForm.getInt("merchant_id", false, -1);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Terminal List</div>
</div>

<form name="searchForm" id="searchForm" method="post"
	action="TerminalList.i"><input type="hidden" name="authority_id"
	value="<%=authorityId%>" /> <input type="hidden" name="merchant_id"
	value="<%=merchantId%>" />

<table class="tabDataDisplayBorder">
	<thead>
		<tr class="sortHeader">
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Authority</a>
			<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Merchant</a>
			<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Merchant Num</a>
			<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%></td>
			
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Terminal</a>
			<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Terminal State</a>
			<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Next Batch Num</a>
			<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%></td>
			
		</tr>
	</thead>

	<tbody>
		<%
		    int i = 0;
		    String rowClass = "row0";
		    while (terminalList.next())
		    {
		        rowClass = (i % 2 == 0) ? "row1" : "row0";
		        i++;
		%>
		<tr class="<%=rowClass%>">
			<td><a href="/authorityInfo.i?authority_id=<%=terminalList.getFormattedValue("authority_id")%>"><%=terminalList.getFormattedValue("authority_name")%></a></td>
		    <td><a href="/merchantInfo.i?merchant_id=<%=terminalList.getFormattedValue("merchant_id")%>"><%=terminalList.getFormattedValue("merchant_name")%></a></td>
		    <td><%=terminalList.getFormattedValue("merchant_cd")%></td>
		    <td><a href="/terminalInfo.i?terminal_id=<%=terminalList.getFormattedValue("terminal_id")%>"><%=terminalList.getFormattedValue("terminal_cd")%></a></td>
			<td><%=terminalList.getFormattedValue("terminal_state_name")%></td>
			<td><%=terminalList.getFormattedValue("terminal_next_batch_num")%></td>
		</tr>
		<%
		    }
		%>
	</tbody>

</table>

<%
    String storedNames = PaginationUtil.encodeStoredNames(new String[] {"authority_id", "merchant_id"});
%> <jsp:include page="/jsp/include/pagination.jsp" flush="true">
	<jsp:param name="_param_request_url" value="terminalList.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

</form>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />