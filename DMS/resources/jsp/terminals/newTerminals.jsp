<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    Results terminalList = (Results)inputForm.getAttribute("terminalList");
    String terminalNumber = inputForm.getString("terminalNumber", false);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
	boolean norec=false;
    
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Newly activated terminals</div>
</div>
<form name="pForm" id="pForm" method="post">
<div class="innerTable">
<div class="tabData">
<table class="tabDataDisplayNoBorder">
	<tr>
		<td align="left"><b>Terminal Number</b>
			<input type="text" name="terminalNumber" id="terminalNumber"
				value="<%=StringHelper.isBlank(terminalNumber) ? "" : terminalNumber%>" />
			<input type="submit" value="Search" class="cssButton" />
			<%if (!norec) { %>
			<input name="action" type="submit" value="Accept" class="cssButton" />
			<div class="spacer5"></div>
			<%} %>
		</td>
	</tr>
</table>
</div>
</div>
<div class="spacer5"></div>

<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td width="10"><%if (norec) {%>&nbsp; <%} else {%> <input type="checkbox" onclick="onClick()"/> <%}%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Terminal</a>
			<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Customer</a>
			<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Location</a>
			<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Created Date</a>
			<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">ePort
			Serial Number</a> <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Primary
			Contact</a> <%=PaginationUtil.getSortingIconHtml("6", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Program</a>
			<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Created By</a>
			<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">Activation Reason</a>
			<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%></td>
			<td><a
				href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Sales Person</a>
			<%=PaginationUtil.getSortingIconHtml("10", sortIndex)%></td>
		</tr>
	</thead>

	<tbody>
		<%
		int i = 0;
		String rowClass = "row0";
		while (terminalList.next())
		{
			rowClass = (i % 2 == 0) ? "row1" : "row0";
			i++;
		%>
		<tr class="<%=rowClass%>">
			<td><input type="checkbox" name="pid" id="pid" value="<%=terminalList.getFormattedValue("terminalId")%>" /></td>
			<td><a href="/editTerminals.i?terminalId=<%=terminalList.getFormattedValue("terminalId")%>"><%=terminalList.getFormattedValue("terminalNumber")%></a></td>
			<td><%=terminalList.getFormattedValue("customerName")%></td>
			<td><%=terminalList.getFormattedValue("locationName")%></td>
			<td><%=terminalList.getFormattedValue("createdDate")%></td>
			<td><%=terminalList.getFormattedValue("eportSerialNumber")%></td>
			<td><%=terminalList.getFormattedValue("primaryContact")%></td>
			<td><%=terminalList.getFormattedValue("program")%></td>
			<td><%=terminalList.getFormattedValue("createdBy")%></td>
			<td><%=terminalList.getFormattedValue("activationReason")%></td>
			<td><%=terminalList.getFormattedValue("salesperson")%></td>
		</tr>
		<%
		    }
		%>
	</tbody>

</table>

<%
    String storedNames = PaginationUtil.encodeStoredNames(new String[] {"terminalNumber"});
%> <jsp:include page="/jsp/include/pagination.jsp" flush="true">
	<jsp:param name="_param_request_url" value="newTerminals.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

</form>

</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">
var checked = false;

function onClick(){
	if(checked){
		uncheckIt();
		checked = false;
	}else{
		checkIt();
		checked = true;
	}
}

function checkIt(){
	if(document.pForm.pid.length>0){
		for (i=0; i<document.pForm.pid.length; i++){
			document.pForm.pid[i].checked=1;
		}
	}else{
		document.pForm.pid.checked=1;
	}
}

function uncheckIt(){
	if(document.pForm.pid.length>0){
		for (i=0; i<document.pForm.pid.length; i++){
			document.pForm.pid[i].checked=0;
		}
	}else{
		document.pForm.pid.checked=0;
	}
}

</script>
