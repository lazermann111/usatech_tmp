<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Results fees = RequestUtils.getAttribute(request, "fees", Results.class, true);
String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false);
String currentDate = Helper.getCurrentDate();
String currentTime = Helper.getCurrentTime();
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Update Service Fees</span></div>
</div>

<div class="tableContent">

<form method="post" action="updateServiceFees.i" onsubmit="return doSubmit()">

<table class="padding3">
	<%if(msg != null && msg.length() > 0) {%>
	<tr><td colspan="4" class="status-info"><%=msg%></td></tr>
	<%} %>
	<%if(err != null && err.length() > 0) {%>
	<tr><td colspan="4" class="status-error"><%=err%></td></tr>
	<%} %>
		
	<tr>
		<td class="label" valign="top">
			<%String deviceNumberType = inputForm.getStringSafely("device_number_type", "").trim(); %>
			<select name="device_number_type" id="device_number_type" style="font-weight: bold" onchange="deviceNumberTypeChange();">
				<option value="serial">Serial Numbers</option>
				<option value="terminal"<%if (deviceNumberType.equals("terminal")) out.write(" selected=\"selected\"");%>>Terminal Numbers</option>
			</select>
			<font color="red">*</font><br />(1 per line)
		</td>
		<td colspan="3">
			<textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("dev_list", "")%></textarea>
		</td>
	</tr>
	<tr>
		<td class="label">Fee</td>
		<td>
			Daily <input type="checkbox" name="dailyFrequency" id="dailyFrequency" value="Y"/>
			<%StringBuilder sb = new StringBuilder();%>
			<select name="fee_id" id="fee_id" onchange="onFeeChange()">
			<%if (fees != null){ 
			String feeId = inputForm.getStringSafely("fee_id", "").trim();
			while(fees.next()) {
			%><option value="<%=fees.getValue("FEE_ID")%>"<%if (feeId.length() > 0 && feeId.equals(fees.getFormattedValue("FEE_ID"))) out.write(" selected=\"selected\"");%>><%=fees.getFormattedValue("FEE_NAME")%></option>	    	
			<%
			sb.append("<input type=\"hidden\" id=\"fee_amount_ubn_").append(fees.getFormattedValue("FEE_ID")).append("\" value=\"").append(fees.getFormattedValue("FEE_AMOUNT_UBN")).append("\" />\n");
			sb.append("<input type=\"hidden\" id=\"grace_days_lbn_").append(fees.getFormattedValue("FEE_ID")).append("\" value=\"").append(fees.getFormattedValue("GRACE_DAYS_LBN")).append("\" />\n");
			sb.append("<input type=\"hidden\" id=\"grace_days_ubn_").append(fees.getFormattedValue("FEE_ID")).append("\" value=\"").append(fees.getFormattedValue("GRACE_DAYS_UBN")).append("\" />\n");
			sb.append("<input type=\"hidden\" id=\"dynamic_fee_ind_").append(fees.getFormattedValue("FEE_ID")).append("\" value=\"").append(fees.getFormattedValue("DYNAMIC_FEE_IND")).append("\" />\n");
			}
			}
			%>	
			</select>
			<%=sb.toString()%>
		</td>
		<td class="label">Override</td>
		<td><input type="checkbox" name="override" id="override" value="Y"<%if (inputForm.getStringSafely("override", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	<tr>
		<td class="label">Fixed Amount ($)</td>
		<td>
			<input type="text" name="fee_amount" id="fee_amount" maxlength="18" value="<%=inputForm.getStringSafely("fee_amount", "")%>" />
			<div class="dynamic" id="dynamic" style="display:inline">
				<input type="checkbox" name="dynamic_fee" id="dynamic_fee" onchange="onDynamicChange()"/>
				Dynamic
			</div>
		</td>
		<td class="label">Inactive Amount ($)</td>
		<td><input type="text" name="inactive_fee_amount" id="inactive_fee_amount" maxlength="18" value="<%=StringUtils.prepareCDATA(inputForm.getStringSafely("inactive_fee_amount", ""))%>" /></td>        
	</tr>
	<tr class="dynamicAmounts" id="dynamicAmounts" style="display:none">
		<td/>
		<td>
			Min
			<input type="text" name="dynamic_min" id="dynamic_min" size="3" maxlength="8" value="<%=inputForm.getStringSafely("dynamic_min", "")%>" />
			Max
			<input type="text" name="dynamic_max" id="dynamic_max" size="3" maxlength="8" value="<%=inputForm.getStringSafely("dynamic_max", "")%>" />
			Per trans
			<input type="text" name="dynamic_amount" id="dynamic_amount" size="3" maxlength="8" value="<%=inputForm.getStringSafely("dynamic_amount", "")%>" />
		</td>
	</tr>
	<tr class="tr_commissions" style="display:none;">
		<td class="label">Commission Amount ($)</td>
		<td><input type="text" name="commission_amount" id="commission_amount" maxlength="18" value="<%=inputForm.getStringSafely("commission_amount", "")%>" /></td>
		<td class="label">Reseller Bank</td>
		<td style="white-space: nowrap;">
			<select name="commission_bank_id" id="commission_bank_id">
				<option value="0">Please select:</option>
				<%
				String commissionBankId = inputForm.getStringSafely("commission_bank_id", "").trim();
				if (commissionBankId.length() > 0) {
				Results bankAccounts = DataLayerMgr.executeQuery("GET_ALL_PARENT_BANK_ACCOUNTS", new Object[] {});
				while(bankAccounts.next()) {
				%>
				<option value="<%=bankAccounts.getValue("aValue")%>"<%if (commissionBankId.equals(bankAccounts.getFormattedValue("aValue"))) out.write(" selected=\"selected\"");%>><%=bankAccounts.getFormattedValue("aLabel")%></option>
				<%}}%>
			</select>
			<a href="javascript:getData('type=commission_bank_account&defaultValue=0&defaultName=Please select:&selected=<%=commissionBankId%>');"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>
	<tr id="tr_terminal_date">
		<td class="label">Terminal Date<font color="red">*</font></td>
		<td>
			<input type="text" name="terminal_date" id="terminal_date" maxlength="10" value="<%=inputForm.getStringSafely("terminal_date", currentDate)%>" />
			<img src="/images/calendar.gif" id="terminal_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Terminal Time</td>
		<td><input type="text" name="terminal_time" id="terminal_time" maxlength="8" value="<%=inputForm.getStringSafely("terminal_time", currentTime)%>" /></td>
	</tr>
	<tr>
		<td class="label">Effective Date<font color="red">*</font></td>
		<td>
			<input type="text" name="effective_date" id="effective_date" maxlength="10" value="<%=inputForm.getStringSafely("effective_date", currentDate)%>" />
			<img src="/images/calendar.gif" id="effective_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Effective Time</td>
		<td><input type="text" name="effective_time" id="effective_time" maxlength="8" value="<%=inputForm.getStringSafely("effective_time", currentTime)%>" /></td>
	</tr>
	<tr>
		<td class="label">Fee Grace Days</td>
		<td><input type="text" name="fee_grace_days" id="fee_grace_days" maxlength="3" value="<%=inputForm.getStringSafely("fee_grace_days", "60")%>" /></td>
		<td class="label">No Rental Fee Trigger</td>
		<td><input type="checkbox" name="no_trigger_event_flag" id="no_trigger_event_flag" value="Y"<%if (inputForm.getStringSafely("no_trigger_event_flag", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	<tr>	
		<td class="label">Only Update Grace Days and No Trigger</td>
		<td colspan="3"><input type="checkbox" name="grace_days_only" id="grace_days_only" value="Y"<%if (inputForm.getStringSafely("grace_days_only", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	<tr>
		<td class="label">End Date</td>
		<td>
			<input type="text" name="end_date" id="end_date" maxlength="10" value="<%=inputForm.getStringSafely("end_date", "")%>" />
			<img src="/images/calendar.gif" id="end_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">End Time</td>
		<td><input type="text" name="end_time" id="end_time" maxlength="8" value="<%=inputForm.getStringSafely("end_time", "")%>" /></td>
	</tr>
	<tr>
		<td colspan="4" align="center">
		<input type="button" id="button_toggle_commissions" value="Show Commissions" class="cssButton" onclick="toggleCommissions()"/>
		<input type="submit" value="Submit" class="cssButton" />
		</td>
	</tr>
</table>

</form>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
Calendar.setup({
    inputField     :    "terminal_date",                  	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "terminal_date_trigger",  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

Calendar.setup({
    inputField     :    "effective_date",                  	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "effective_date_trigger",  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

Calendar.setup({
    inputField     :    "end_date",                  		// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "end_date_trigger",  				// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

function onFeeChange(){
	var feeId = document.getElementById("fee_id").value;
	var isDynamic = document.getElementById("dynamic_fee_ind_"+feeId).value;
	var frequency = document.getElementById('serviceFeeFrequencyId'+feeId);
	if (isDynamic == 'Y'){
		$('dynamic').setStyle('display', 'inline');
	}else{
		$('dynamic').setStyle('display' ,'none');
		$('dynamic_fee').checked=false;
		$('dynamicAmounts').setStyle('display' ,'none');
		onDynamicChange();
	}
}

function onDynamicChange(){
	if ($('dynamic_fee').checked){
		$('dynamicAmounts').setStyle('display' ,'');
		$('fee_amount').disabled="true";
		$('fee_amount').value='';
		$('dailyFrequency').checked="";
		$('dailyFrequency').disabled="true";
	}else{
		$('dynamicAmounts').setStyle('display' ,'none');
		$('fee_amount').disabled="";
		$('dynamic_min').value='';
		$('dynamic_max').value='';
		$('dynamic_amount').value='';
		$('dailyFrequency').disabled="";
	}
}

onFeeChange();

function doSubmit(){
	if(document.getElementById("dev_list").value.trim() == ''){
		alert('Please enter serial numbers');
		return false;
	}
	
	var fee_id = document.getElementById("fee_id").value;
	
	if (document.getElementById("grace_days_ubn_" + fee_id).value > 0) {
		var fee_grace_days = document.getElementById("fee_grace_days").value.trim();
		if(fee_grace_days == '' || isNaN(fee_grace_days) 
				|| fee_grace_days < parseFloat(document.getElementById("grace_days_lbn_" + fee_id).value) 
				|| fee_grace_days > parseFloat(document.getElementById("grace_days_ubn_" + fee_id).value)){
			alert("Fee Grace Days must be a number between " + document.getElementById("grace_days_lbn_" + fee_id).value + " and " + document.getElementById("grace_days_ubn_" + fee_id).value);
			return false;
		}
		if (document.getElementById("grace_days_only").checked)
			return true;
	}
	
	var fee_percent = ''; //var fee_percent = document.getElementById("fee_percent").value.trim(); //uncomment if fee_percent will be in use.
	var fee_amount = document.getElementById("fee_amount").value.trim();
	
	var is_dynamic = document.getElementById("dynamic_fee").checked;  
	
	if(!is_dynamic && (fee_percent == '') && (fee_amount == '' )){
		alert('Please enter percent or fixed amount');
		return false;
	}
	
	var dynamic_min = document.getElementById("dynamic_min").value.trim();
	var dynamic_max = document.getElementById("dynamic_max").value.trim();
	var dynamic_amount = document.getElementById("dynamic_amount").value.trim();
	if (is_dynamic && (dynamic_min == '' || dynamic_max == '' || dynamic_amount == '')){
		alert('Please enter dynamic fee values');
		return false;
	}else if (dynamic_min > dynamic_max){
		alert('Dynamic minimum amount value must be less(or equal) than maximum amount value. Please enter correct values');
		return false;
	}
	
	if (document.getElementById("fee_amount_ubn_" + fee_id).value > 0 && fee_amount >= parseFloat(document.getElementById("fee_amount_ubn_" + fee_id).value) && !confirm("Please confirm $" + fee_amount + " Fixed Amount fee!"))
		return false;
	
	if (document.getElementById("device_number_type").value == "serial") {
		var terminal_date = document.getElementById("terminal_date");
		if(!isDate(terminal_date.value)){
			terminal_date.focus();
			return false;
		}	
	}
	
	var effective_date = document.getElementById("effective_date");
	if(!isDate(effective_date.value)){
		effective_date.focus();
		return false;
	}
	
	var end_date = document.getElementById("end_date");
	if(end_date.value != '' && !isDate(end_date.value)){
		end_date.focus();
		return false;
	}

	return true;
}
</script>
