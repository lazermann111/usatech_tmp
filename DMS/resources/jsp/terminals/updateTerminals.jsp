<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);

Results currencies = RequestUtils.getAttribute(request, "currencies", Results.class, true);
Results businessUnits = RequestUtils.getAttribute(request, "businessUnits", Results.class, true);
Results deactivateDetails = RequestUtils.getAttribute(request, "deactivateDetails", Results.class, true);
Results activateDetails = RequestUtils.getAttribute(request, "activateDetails", Results.class, true);

String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false);

String currentDate = Helper.getCurrentDate();
String currentTime = Helper.getCurrentTime();
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="largeFormContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Update Terminals</span></div>
</div>

<div class="tableContent">

<form method="post" action="updateTerminals.i" onsubmit="return doSubmit()">

<table class="padding3">		
	<%if(msg != null && msg.length() > 0) {%>
	<tr><td colspan="4" class="status-info"><%=msg%></td></tr>
	<%} %>
	<%if(err != null && err.length() > 0) {%>
	<tr><td colspan="4" class="status-error"><%=err%></td></tr>
	<%} %>
		
	<tr>
		<td class="label" valign="top">
			<%String deviceNumberType = inputForm.getStringSafely("device_number_type", "").trim(); %>
			<select name="device_number_type" id="device_number_type" style="font-weight: bold" onchange="deviceNumberTypeChange();">
				<option value="serial">Serial Numbers</option>
				<option value="terminal"<%if (deviceNumberType.equals("terminal")) out.write(" selected=\"selected\"");%>>Terminal Numbers</option>
			</select>
			<font color="red">*</font><br />(1 per line)
		</td>
		<td colspan="3">
			<textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("dev_list", "")%></textarea>
		</td>
	</tr>
	<tr id="tr_terminal_date">
		<td class="label">Terminal Date<font color="red">*</font></td>
		<td>
			<input type="text" name="terminal_date" id="terminal_date" maxlength="10" value="<%=inputForm.getStringSafely("terminal_date", currentDate)%>" />
			<img src="/images/calendar.gif" id="terminal_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Terminal Time</td>
		<td><input type="text" name="terminal_time" id="terminal_time" maxlength="8" value="<%=inputForm.getStringSafely("terminal_time", currentTime)%>" /></td>
	</tr>
	<tr>
		<td class="label">New Date<font color="red">*</font></td>
		<td>
			<input type="text" name="new_date" id="new_date" maxlength="10" value="<%=inputForm.getStringSafely("new_date", currentDate)%>" />
			<img src="/images/calendar.gif" id="new_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">New Time</td>
		<td><input type="text" name="new_time" id="new_time" maxlength="8" value="<%=inputForm.getStringSafely("new_time", currentTime)%>" /></td>
	</tr>
	<tr>
		<td class="label">Override</td>
		<td colspan="3"><input type="checkbox" name="override" id="override" value="Y"<%if (inputForm.getStringSafely("override", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	<tr>
		<td class="label">Currency</td>
		<td>
			<select name="currency_id" id="currency_id">
			<%if (currencies != null){
			String currencyId = inputForm.getStringSafely("currency_id", "").trim();
			while(currencies.next()) {
			%><option value="<%=currencies.getValue("currency_id")%>"<%if (currencyId.length() > 0 && currencyId.equals(currencies.getFormattedValue("currency_id"))) out.write(" selected=\"selected\"");%>><%=currencies.getFormattedValue("currency_name")%></option>	    	
			<%}}%>	
			</select>
		</td>
		<td class="label">Business Unit</td>
		<td>
			<select name="business_unit_id" id="business_unit_id">
			<%if (businessUnits != null){
			while(businessUnits.next()) {
			String businessUnitId = inputForm.getStringSafely("business_unit_id", "").trim();
			%><option value="<%=businessUnits.getValue("BUSINESS_UNIT_ID")%>"<%if (businessUnitId.length() > 0 && businessUnitId.equals(businessUnits.getFormattedValue("BUSINESS_UNIT_ID"))) out.write(" selected=\"selected\"");%>><%=businessUnits.getFormattedValue("BUSINESS_UNIT_NAME")%></option>	    	
			<%}}%>	
			</select>
		</td>
	</tr>
	<tr>
		<td class="label">Doing Business As</td>
		<td colspan="3"><input type="text" name="doing_business_as" id="doing_business_as" maxlength="21" style="width: 250px;" value="<%=inputForm.getStringSafely("doing_business_as", "")%>" /></td>
	</tr>
	<tr>
		<td class="label">Crane Customer Id</td>
		<td colspan="3"><input type="text" name="craneCustomerId" id="craneCustomerId" maxlength="100" style="width: 250px;" value="<%=inputForm.getStringSafely("craneCustomerId", "")%>" /></td>
	</tr>
	<tr>
		<td class="label">Activation Reason/Details</td>
		<td colspan="3">
		<select name=activateDetailId id="activateDetailId">
			<%if (activateDetails != null){
				int activateDetailId = inputForm.getInt("activateDetailId", false, -1);
			while(activateDetails.next()) {
			
			%><option value="<%=activateDetails.getValue("activateDetailId")%>"<%if (activateDetailId> 0 && activateDetails.getValue("activateDetailId",int.class)==activateDetailId) out.write(" selected=\"selected\"");%>><%=activateDetails.getFormattedValue("activateDetail")%></option>	    	
			<%}}%>		
		</select>
		</td>
	</tr>
	<tr>
		<td class="label">Activation Reason/Details Addtional Info:</td>
		<td colspan="3">
		<input id="activateDetail" class="cssText" type="text" size="100" value="" name="activateDetail" />
		</td>
	</tr>
	<tr>
		<td class="label">Deactivation Reason/Details</td>
		<td colspan="3">
		<select name=deactivateDetailId id="deactivateDetailId">
			<%if (deactivateDetails != null){
				int deactivateDetailId = inputForm.getInt("deactivateDetailId", false, -1);
			while(deactivateDetails.next()) {
			
			%><option value="<%=deactivateDetails.getValue("deactivateDetailId")%>"<%if (deactivateDetailId> 0 && deactivateDetails.getValue("deactivateDetailId",int.class)==deactivateDetailId) out.write(" selected=\"selected\"");%>><%=deactivateDetails.getFormattedValue("deactivateDetail")%></option>	    	
			<%}}%>	
		</select>
		</td>
	</tr>
	<tr>
		<td class="label">Deactivation Reason/Details Addtional Info:</td>
		<td colspan="3">
		<input id="deactivateDetail" class="cssText" type="text" size="100" value="<%=inputForm.getStringSafely("deactivateDetail", "")%>" name="deactivateDetail" />
		</td>
	</tr>
	<tr>
		<td class="label">Terminate ePort Quick Start Fees when device is inactive for # days:</td>
		<td colspan="3">
		<input id="eportQSInactivityDays" class="cssText" type="text" size="4" maxlength="3" value="<%=inputForm.getStringSafely("eportQSInactivityDays", "")%>" name="eportQSInactivityDays" />
		</td>
	</tr>
	<tr>
		<td class="label" colspan="4" align="center">Setting to Change</td>
	</tr>	
	<tr>
		<td colspan="4" align="center">
		<input type="submit" name="myaction" value="Start Date" class="cssButton" />
		<input type="submit" name="myaction" value="End Date" class="cssButton" />
		<input type="submit" name="myaction" value="Bank Acct Date" class="cssButton" />
		<input type="submit" name="myaction" value="Currency" class="cssButton" />
		<input type="submit" name="myaction" value="Business Unit" class="cssButton" />
		<input type="submit" name="myaction" value="Deactivate ePort" class="cssButton" />
		<input type="submit" name="myaction" value="Doing Business As" class="cssButton" />
		<input type="submit" name="myaction" value="Crane Customer" class="cssButton" />
		<input type="submit" name="myaction" value="ePort Inactive Days" class="cssButton" />
		<input type="submit" name="myaction" value="Activation Reason/Details" class="cssButton"/>
		<input type="submit" name="myaction" value="Deactivation Reason/Details" class="cssButton"/>
		</td>
	</tr>
</table>

</form>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
Calendar.setup({
    inputField     :    "terminal_date",                  	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "terminal_date_trigger",  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

Calendar.setup({
    inputField     :    "new_date", 	                 	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "new_date_trigger",		  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

function doSubmit(){	
	if(document.getElementById("dev_list").value.trim() == ''){
		alert('Please enter serial numbers');
		return false;
	}
	
	if (document.getElementById("device_number_type").value == "serial") {
		var terminal_date = document.getElementById("terminal_date");
		if(!isDate(terminal_date.value)){
			terminal_date.focus();
			return false;
		}
	}
	
	var new_date = document.getElementById("new_date");
	if(!isDate(new_date.value)){
		new_date.focus();
		return false;
	}

	return true;
}
</script>
