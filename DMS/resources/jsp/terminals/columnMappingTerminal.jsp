<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%
String terminalId = ConvertUtils.getStringSafely(request.getParameter("terminalId"), "");
Results map = null;
if (!StringHelper.isBlank(terminalId))
	map = DataLayerMgr.executeQuery("COLUMN_MAPPING", new Object[]{terminalId});
Results columnMap = null;
if (map != null)
	columnMap = map.clone();

boolean empty = map == null || !map.next();

StringBuilder mappings = new StringBuilder();
if (columnMap != null) {
	while (columnMap.next())
		mappings.append(columnMap.getValue("MDB_NUMBER")).append("=").append(columnMap.getValue("VEND_COLUMN")).append("\n");
	if (mappings.length() > 0)
		mappings.deleteCharAt(mappings.length() - 1);
}
%>	

<div class="spacer5"></div>

<table width="100%">
	<tr><td>&nbsp;<b>Column Mappings</b> (MDB #=Vend Column)</td></tr>
	<tr><td align="center"><textarea id="mappings" name="mappings" rows="5" style="width:98%;"><%=mappings%></textarea></td></tr>
	<tr><td align="center"><input type="button" value="Update" onclick="updateMapppings()" class="cssButton"/></td></tr>
</table>

<table>
	<tr ><td colspan="2">&nbsp;</td></tr>	
	<tr ><td colspan="2"><b>Add new mapping to current terminal</b></td></tr>
	<tr>
		<td class="label" >MDB #</td>
		<td class="control" ><input type="text" name="mdb" id="mdb" size="50" maxlength="255"/></td>
	</tr>
	<tr>
		<td class="label">Vend Column</td>
		<td class="control" ><input type="text" name="vend" id="vend" size="50" maxlength="50"/></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<input name="addNewBtn" id="addNewBtn" type="button" value="Add New" onclick="addNew()" class="cssButton"/>
		</td>
	</tr>
	<tr ><td colspan="2">&nbsp;</td></tr>	
</table>
	
<table width="100%">
		<tr class="gridHeader">
			<td width="10"><input type="checkbox" onclick="doIt()"/></td>
			<td>MDB #</td>
			<td>Vend Column</td>
		</tr>
		
		<%		
		if(empty) {
		%>
		<tr><td align="center" colspan="3">No records</td></tr>
		<%
		} else {
			int i = 0; 
			String rowClass = "row0";
			do {
				rowClass = (i%2 == 0) ? "row1" : "row0";
				i++;
			%>
			<tr class="<%=rowClass%>">
				<td><input type='checkbox' name='pid' id='pid' value='<%=map.getValue("MDB_NUMBER")%>'></td>
				<td><%=map.getValue("MDB_NUMBER")%></td>
				<td><%=map.getValue("VEND_COLUMN")%></td>
			</tr>
			<%
			} while(map.next());
		}
		%>
		<tr>
			<td colspan="3" align="center">
			<input <% if(empty){ out.write(" disabled='disabled' ");} %> name="close" id="close" type="button" value="Delete" onclick="deleteAll()" class="cssButton"/>
			</td>
		</tr>
</table>

<div class="spacer10"></div>
