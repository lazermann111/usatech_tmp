<%@page import="simple.db.DataLayerMgr"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Results transTypes = RequestUtils.getAttribute(request, "transTypes", Results.class, true);
String msg = RequestUtils.getAttribute(request, "message", String.class, false);
String err = RequestUtils.getAttribute(request, "error", String.class, false);
String currentDate = Helper.getCurrentDate();
String currentTime = Helper.getCurrentTime();
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="formContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Update Process Fees</span></div>
</div>

<div class="tableContent">

<div class="spacer5"></div>

<form method="post" action="updateProcessFees.i" onsubmit="return doSubmit()">

<table class="padding3">		
	<%if(msg != null && msg.length() > 0) {%>
	<tr><td colspan="4" class="status-info"><%=msg%></td></tr>
	<%} %>
	<%if(err != null && err.length() > 0) {%>
	<tr><td colspan="4" class="status-error"><%=err%></td></tr>
	<%} %>
		
	<tr>
		<td class="label" valign="top">
			<%String deviceNumberType = inputForm.getStringSafely("device_number_type", "").trim(); %>
			<select name="device_number_type" id="device_number_type" style="font-weight: bold" onchange="deviceNumberTypeChange();">
				<option value="serial">Serial Numbers</option>
				<option value="terminal"<%if (deviceNumberType.equals("terminal")) out.write(" selected=\"selected\"");%>>Terminal Numbers</option>
			</select>
			<font color="red">*</font><br />(1 per line)
		</td>
		<td colspan="3">
			<textarea name="dev_list" id="dev_list" rows="5" style="width: 240px;"><%=inputForm.getStringSafely("dev_list", "")%></textarea>
		</td>
	</tr>
	<tr>
		<td class="label">Transaction Type</td>
		<td>
			<%StringBuilder sb = new StringBuilder();%>
			<select name="trans_type_id" id="trans_type_id">
			<%if (transTypes != null){
			String transTypeId = inputForm.getStringSafely("trans_type_id", "").trim();
			while(transTypes.next()) {
			%><option value="<%=transTypes.getValue("TRANS_TYPE_ID")%>"<%if (transTypeId.length() > 0 && transTypeId.equals(transTypes.getFormattedValue("TRANS_TYPE_ID"))) out.write(" selected=\"selected\"");%>><%=transTypes.getFormattedValue("TRANS_TYPE_NAME")%></option>
			<%
			sb.append("<input type=\"hidden\" id=\"fee_percent_lbn_").append(transTypes.getFormattedValue("TRANS_TYPE_ID")).append("\" value=\"").append(transTypes.getFormattedValue("FEE_PERCENT_LBN")).append("\" />\n");
			sb.append("<input type=\"hidden\" id=\"fee_percent_ubn_").append(transTypes.getFormattedValue("TRANS_TYPE_ID")).append("\" value=\"").append(transTypes.getFormattedValue("FEE_PERCENT_UBN")).append("\" />\n");
			sb.append("<input type=\"hidden\" id=\"fee_amount_ubn_").append(transTypes.getFormattedValue("TRANS_TYPE_ID")).append("\" value=\"").append(transTypes.getFormattedValue("FEE_AMOUNT_UBN")).append("\" />\n");
			sb.append("<input type=\"hidden\" id=\"fee_min_ubn_").append(transTypes.getFormattedValue("TRANS_TYPE_ID")).append("\" value=\"").append(transTypes.getFormattedValue("FEE_MIN_UBN")).append("\" />\n");
			}
			}
			%>	
			</select>
			<%=sb.toString()%>
		</td>
		<td class="label">Override</td>
		<td><input type="checkbox" name="override" id="override" value="Y"<%if (inputForm.getStringSafely("override", "").equalsIgnoreCase("Y")) out.write(" checked=\"checked\"");%> /></td>
	</tr>
	<tr>
		<td class="label">Percent (%)</td>
		<td><input type="text" name="fee_percent" id="fee_percent" maxlength="13" value="<%=inputForm.getStringSafely("fee_percent", "")%>" /></td>
		<td class="label">Fixed Amount ($)</td>
		<td><input type="text" name="fee_amount" id="fee_amount" maxlength="18" value="<%=inputForm.getStringSafely("fee_amount", "")%>" /></td>
	</tr>
	<tr>
		<td class="label">Minimum Amount ($)</td>
		<td colspan="3"><input type="text" name="fee_minimum" id="fee_minimum" maxlength="18" value="<%=inputForm.getStringSafely("fee_minimum", "")%>" /></td>
	</tr>
	<% boolean hasCommissions = inputForm.getStringSafely("commission_bank_id", null) != null; %>
	<tr class="tr_commissions" style="display:<%=hasCommissions ? "table-row" : "none"%>">
		<td class="label">Commission Percent (%)</td>
		<td><input type="text" name="commission_percent" id="commission_percent" maxlength="13" value="<%=inputForm.getStringSafely("commission_percent", "")%>" /></td>
		<td class="label">Commission Fixed Amount ($)</td>
		<td><input type="text" name="commission_amount" id="commission_amount" maxlength="18" value="<%=inputForm.getStringSafely("commission_amount", "")%>" /></td>
	</tr>
	<tr class="tr_commissions" style="display:<%=hasCommissions ? "table-row" : "none"%>">
		<td class="label">Commission Minimum Amount ($)</td>
		<td><input type="text" name="commission_minimum" id="commission_minimum" maxlength="18" value="<%=inputForm.getStringSafely("commission_minimum", "")%>" /></td>
		<td class="label">Reseller Bank</td>
		<td style="white-space: nowrap;">
			<select name="commission_bank_id" id="commission_bank_id">
				<option value="0">Please select:</option>
				<%
				String commissionBankId = inputForm.getStringSafely("commission_bank_id", "").trim();
				if (commissionBankId.length() > 0) {
				Results bankAccounts = DataLayerMgr.executeQuery("GET_ALL_PARENT_BANK_ACCOUNTS", new Object[] {});
				while(bankAccounts.next()) {
				%>
				<option value="<%=bankAccounts.getValue("aValue")%>"<%if (commissionBankId.equals(bankAccounts.getFormattedValue("aValue"))) out.write(" selected=\"selected\"");%>><%=bankAccounts.getFormattedValue("aLabel")%></option>
				<%}}%>
			</select>
			<a href="javascript:getData('type=commission_bank_account&defaultValue=0&defaultName=Please select:&selected=<%=commissionBankId%>');"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
		
	</tr>
	<tr id="tr_terminal_date">
		<td class="label">Terminal Date<font color="red">*</font></td>
		<td>
			<input type="text" name="terminal_date" id="terminal_date" maxlength="10" value="<%=inputForm.getStringSafely("terminal_date", currentDate)%>" />
			<img src="/images/calendar.gif" id="terminal_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Terminal Time</td>
		<td><input type="text" name="terminal_time" id="terminal_time" maxlength="8" value="<%=inputForm.getStringSafely("terminal_time", currentTime)%>" /></td>
	</tr>
	<tr>
		<td class="label">Effective Date<font color="red">*</font></td>
		<td>
			<input type="text" name="effective_date" id="effective_date" maxlength="10" value="<%=inputForm.getStringSafely("effective_date", currentDate)%>" />
			<img src="/images/calendar.gif" id="effective_date_trigger" class="calendarIcon" title="Date selector" />
		</td>
		<td class="label">Effective Time</td>
		<td><input type="text" name="effective_time" id="effective_time" maxlength="8" value="<%=inputForm.getStringSafely("effective_time", currentTime)%>" /></td>
	</tr>
	<tr>
		<td colspan="4" align="center">
		<input type="button" id="button_toggle_commissions" value="Show Commissions" class="cssButton" onclick="toggleCommissions()"/>
		<input type="submit" value="Submit" class="cssButton" />
		</td>
	</tr>
</table>

</form>

</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript" defer="defer">
Calendar.setup({
    inputField     :    "terminal_date",                  	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "terminal_date_trigger",  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

Calendar.setup({
    inputField     :    "effective_date",                  	// id of the input field
    ifFormat       :    "%m/%d/%Y",            				// format of the input field
    button         :    "effective_date_trigger",  			// trigger for the calendar (button ID)
    align          :    "B2",                  				// alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

function doSubmit(){	
	if(document.getElementById("dev_list").value.trim() == ''){
		alert('Please enter serial numbers');
		return false;
	}
	
	var trans_type_id = document.getElementById("trans_type_id").value;
	var fee_percent = document.getElementById("fee_percent").value.trim();
	var fee_amount = document.getElementById("fee_amount").value.trim();
	var fee_minimum = document.getElementById("fee_minimum").value.trim();
	
	if(fee_percent == '' && fee_amount == '' && fee_minimum == ''){
		alert('Please enter percent, fixed or minimum amount');
		return false;
	}
	
	if (fee_percent > 0 && fee_percent <= parseFloat(document.getElementById("fee_percent_lbn_" + trans_type_id).value) && !confirm("Please confirm " + fee_percent + " % fee!"))
		return false;
	
	if (fee_percent >= parseFloat(document.getElementById("fee_percent_ubn_" + trans_type_id).value) && !confirm("Please confirm " + fee_percent + " % fee!"))
		return false;
	
	if (fee_amount >= parseFloat(document.getElementById("fee_amount_ubn_" + trans_type_id).value) && !confirm("Please confirm $" + fee_amount + " Fixed Amount fee!"))
		return false;
	
	if (fee_minimum >= parseFloat(document.getElementById("fee_min_ubn_" + trans_type_id).value) && !confirm("Please confirm $" + fee_minimum + " Minimum Amount fee!"))
		return false;
	
	if (document.getElementById("device_number_type").value == "serial") {
		var terminal_date = document.getElementById("terminal_date");
		if(!isDate(terminal_date.value)){
			terminal_date.focus();
			return false;
		}	
	}
	
	var effective_date = document.getElementById("effective_date");
	if(!isDate(effective_date.value)){
		effective_date.focus();
		return false;
	}

	return true;
}
</script>
