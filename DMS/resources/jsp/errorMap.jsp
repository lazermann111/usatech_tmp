<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />

<jsp:include page="include/header.jsp" flush="true" />

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span><br/>
		
		</c:forEach>
		</div>
	</c:when> 
</c:choose>

<jsp:include page="include/footer.jsp" flush="true" />
