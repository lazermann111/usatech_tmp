<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="dms_vl_list" class="java.util.TreeMap" scope="application" />

<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	
	<c:when test="${not empty(errorMessage)}">
		<div class="tableContainer">				    
			<span class="error">${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>
			
			<c:set var="success" value="Cache Refreshed" />
			
			<div class="tableContainer">
					<div class="tableDataHead" align="center">
						<span class="txtWhiteBold">
						Refreshed Cache List</span>
					    </div>								
					<table class="tabDataDisplayBorder">
						
						<c:choose>
							<c:when test="${fn:length(dms_vl_list) > 0}" >
								
								
								<c:forEach var="list_item" items="${dms_vl_list}">
									<tr>
									    <td bgcolor="lightblue">${list_item }&nbsp;</td>
									    <td align="center" bgcolor="lightgreen">${success}</td>
									</tr>
								</c:forEach>
			
								
						</c:when>
					</c:choose>
		</table>
		</div>
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />