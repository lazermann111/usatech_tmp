<%@page import="com.usatech.dms.model.RiskAlert"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="simple.servlet.BasicServletUser,simple.servlet.SimpleServlet,simple.bean.ConvertUtils,simple.servlet.RequestUtils" %>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.user.DmsMenuManager"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="java.util.ArrayList, java.util.Iterator, java.util.List" %>
<%@page import="simple.util.NameValuePair" %>
<%@page import="simple.servlet.InputForm" %>
<%@page import="simple.results.Results" %>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants" %>
 
<jsp:useBean id="dms_values_list_deviceTypesList" class="com.usatech.dms.util.DeviceTypeList" scope="application" />
<jsp:useBean id="dms_values_list_commMethodsList" class="com.usatech.dms.util.GenericList" scope="application" /> 
<jsp:useBean id="dms_values_list_deviceTypesFirmwareList" class="com.usatech.dms.util.DeviceTypeFirmwareList" scope="application" /> 
<jsp:useBean id="dms_values_list_fileTransferTypesList" class="com.usatech.dms.util.FileTransferTypeList" scope="application" />
<jsp:useBean id="dms_values_list_locationTypesList" class="com.usatech.dms.util.LocationTypeList" scope="application" />
<jsp:useBean id="dms_values_list_esudsAlphaCharsList" class="com.usatech.dms.util.EsudsAlphaCharList" scope="application" />
<div id="leftMenu">
<div id="menu">
<input type="hidden" id="<%=DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD%>" value=""/>
<input type="hidden" id="<%=DevicesConstants.CHANGE_HISTORY__OBJECT_CD%>" value=""/>
<ul id="verticalmenu">
	<%
	    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	    Results location_custom_menu2 = null;
	    List<NameValuePair> nvp_list_esuds_alpha_chars_2 = new ArrayList<NameValuePair>();
		String menu = inputForm.getString("menu", false);
	    BasicServletUser loggedUser = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
	    if (loggedUser == null)
	    {
	%>
	<li><a href="/login.i">Login</a></li> <%
     }
     else
     {
 		List<NameValuePair> nvp_list_device_types = dms_values_list_deviceTypesList.getList();
 		List<NameValuePair> nvp_list_comm_methods = dms_values_list_commMethodsList.getList();
		List<NameValuePair> nvp_list_file_transfer_types = dms_values_list_fileTransferTypesList.getList();
		List<NameValuePair> nvp_list_location_types = dms_values_list_locationTypesList.getList();
		if("firmware".equalsIgnoreCase(menu) && !dms_values_list_deviceTypesFirmwareList.isLoaded()) 
			dms_values_list_deviceTypesFirmwareList.getList();
 %> 
	<li><a href="home.i" class="TipsMainMenu" id="tooltip_mm_home">Home</a></li>
	<%
	 DmsMenuManager menuManager = (DmsMenuManager)request.getSession().getAttribute(DmsMenuManager.ATTRIBUTE_MENU_MANAGER);
		
     if (menuManager.showTopMenuDevices())
     {
	%>
	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_devices">Devices</a>
		
		<ul style="top:-25px;">
			<li class="section_header">Device Search</li>
			<li class="section_text" ><form method="get" action="/deviceSearch.i" name="search"><table><tr><td><select name="search_type" id="tooltip_sm_device_search_type" class="TipsMainMenu menuSelect"><option value="serial_number">Serial Number</option><option value="ev_number">Device Name</option><option value="corp_customer_name">Reporting Customer</option><option value="customer_name">DMS Customer Name</option><option value="location_name">DMS Location Name</option><option value="firmware_version">Firmware Version</option><option value="diag_app_version">Diagnostic Version</option><option value="ptest_version">PTest Version</option><option value="plv">Property List Version</option><option value="host_serial_number">Host Serial Number</option><option value="bezel_mfgr">Bezel Manufacturer</option><option value="bezel_app_ver">Bezel App Version</option></select></td><td rowspan="2"><input type="submit" class="cssButton" value="Search" /></td></tr><tr><td><input type="checkbox" name="enabled" value="A" class="TipsMainMenu" id="tooltip_sm_device_search_checkbox" /><input type="text" name="search_param" maxlength="200" class="TipsMainMenu menuTextbox" style="width:107px;" id="tooltip_sm_device_search" /></td></tr></table></form></li>
			<li class="section_header">View Devices</li>
			<li class="section_text" ><a href="#">by Type</a>
			<ul>
				<li class="section_header">Device Type</li>
				<%
			    	Iterator<NameValuePair> deviceTypeIt = nvp_list_device_types.iterator();
			    	while(deviceTypeIt.hasNext()){
			    		NameValuePair deviceIdNVP = deviceTypeIt.next();
			    %>
			    	<li class="section_text"><a href="deviceList.i?device_type_id=<%=deviceIdNVP.getValue()%>"><%=deviceIdNVP.getName()%></a></li>
			    <% } %>
			</ul>	
			</li>
			<li class="section_text" ><a href="#">by Comm Method</a>
			<ul>
				<li class="section_header">Comm Method</li>
				<%
			    	Iterator<NameValuePair> commMethodIt = nvp_list_comm_methods.iterator();
			    	while(commMethodIt.hasNext()){
			    		NameValuePair nvp = commMethodIt.next();
			    %>
			    	<li class="section_text"><a href="deviceList.i?comm_method=<%=nvp.getValue()%>"><%=nvp.getName()%></a></li>
			    <% } %>
			</ul>	
			</li>			
			
			<%if("customer".equalsIgnoreCase(menu)) {%>
				
				<li class="section_text" ><a href="#">by DMS Customer</a>
					<ul id="menu_devices_by_customer">
						<li class="section_header">DMS Customer</li>
						<li class="section_text"><a href="#" >#0-9</a>							
							<%if(request.getAttribute("customersByDigitName") != null) {
								%>
								<ul>
								<li class="section_header">#0-9</li>
								<%
						    	for(String menuItem : ((List<String>) inputForm.getAttribute("customersByDigitName"))) {
						    		String[] menuItemData = Helper.getMenuTextUrlPair(menuItem);
						    		%><li class="section_text"><a href="<%=menuItemData[1]%>" ><%=menuItemData[0]%></a></li>
						    	<%}
								%>
								</ul>
								<%
						    }%>							
						</li>
						
						<%
						for(String l : DevicesConstants.letter) {
							%>
							<li class="section_text"><a href="#" ><%=l.toUpperCase()%></a>
							<%
							if(((List<String>) inputForm.getAttribute("customersByLetterName" + l)) != null) {
								%>
								<ul>
								<li class="section_header"><%=l.toUpperCase()%></li>
								<%
								for(String menuItem : ((List<String>) inputForm.getAttribute("customersByLetterName" + l))) {
									String[] menuItemData = Helper.getMenuTextUrlPair(menuItem);
									%><li class="section_text"><a href="<%=menuItemData[1] %>" ><%=menuItemData[0]%></a></li>
								<%}
								%>
								</ul>
							<%}%>
							</li><%
						}%>
					</ul>
				</li>
			<%} else {%>
				<li class="section_text" ><a href="home.i?menu=customer" class="TipsMainMenu" id="tooltip_sm_devices_load_customer">by DMS Customer</a></li>
			<%}				
			if("location".equalsIgnoreCase(menu)) {%>
				<li class="section_text" ><a href="#" >by DMS Location</a>					
					<ul id="menu_devices_by_location">
						<li class="section_header">DMS Location</li>
						<li class="section_text"><a href="#" >#0-9</a>
							<%if(request.getAttribute("locationsByDigitName") != null) {
								%> 
								<ul>
								<li class="section_header">#0-9</li>
								<%
						    	for(String menuItem : ((List<String>) inputForm.getAttribute("locationsByDigitName"))) {
						    		String[] menuItemData = Helper.getMenuTextUrlPair(menuItem);
									%><li class="section_text"><a href="<%=menuItemData[1]%>" ><%=menuItemData[0]%></a></li>
						    	<%}
								%>
								</ul>
								<%
							}%>
						</li>
						<%
						for(String l : DevicesConstants.letter) {%>
							<li class="section_text"><a href="#" ><%=l.toUpperCase() %></a>
							<%
							if(request.getAttribute("locationsByLetterName" + l) != null) {
								%> 
								<ul>
								<li class="section_header"><%=l.toUpperCase()%></li>
								<%
								for(String menuItem : ((List<String>) inputForm.getAttribute("locationsByLetterName" + l))) {
									String[] menuItemData = Helper.getMenuTextUrlPair(menuItem);
									%><li class="section_text"><a href="<%=menuItemData[1]%>" ><%=menuItemData[0]%></a></li><%
								}
								%>
								</ul>
								<%
							}
							%>
							</li><%
						}%>
					</ul>
				</li>
			<%} else {%>				
				<li class="section_text" ><a href="home.i?menu=location" class="TipsMainMenu" id="tooltip_sm_devices_load_location">by DMS Location</a></li>
			<%}%>			
			<%if ("firmware".equalsIgnoreCase(menu) && dms_values_list_deviceTypesFirmwareList.isLoaded()) {%>
				<li class="section_text" ><a href="#" >by Firmware Version</a>
				<ul>
					<li class="section_header">Device Type</li>
				<%
		    	deviceTypeIt = nvp_list_device_types.iterator();
		    	while(deviceTypeIt.hasNext()){
		    		NameValuePair deviceIdNVP = deviceTypeIt.next();
		    		if (dms_values_list_deviceTypesFirmwareList.hasFirmwareList(Integer.parseInt(deviceIdNVP.getValue()))) {				    		
		    			%><li class="section_text"><a href="deviceList.i?device_type_id=<%=deviceIdNVP.getValue()%>"><%=deviceIdNVP.getName()%></a>
		    			<%		    				
			    			List firmWareList = dms_values_list_deviceTypesFirmwareList.getFirmwareListPerDeviceId(deviceIdNVP.getValue());
			    			Iterator it = firmWareList.iterator();
			    			%>
			    			<ul class="scrollable">
		    			    <li class="section_header" >Firmware Version</li>
		    				<%
	    					while(it.hasNext()){
	    						String firmwareName = (String)it.next();
	    						String firmwareVersion = firmwareName.substring(0, firmwareName.indexOf(Helper.FIELD_DELIMITER));
	    						%><li class="section_text"><a href="deviceList.i?device_type_id=<%=deviceIdNVP.getValue()%>&amp;firmware_version=<%=Helper.encodeParamValue(firmwareVersion)%>"><%=StringUtils.prepareCDATA(firmwareName.replace(Helper.FIELD_DELIMITER, ""))%></a></li><%
	    					}
		    				%>
		    				</ul>
		    				</li>
		    				<%
		    		}		    			
		    	}
		    	%>
		    	</ul>
		    	</li>
			<% } else { %>
	    		<li class="section_text" ><a href="home.i?menu=firmware" class="TipsMainMenu" id="tooltip_sm_devices_load_firmware_version">by Firmware Version</a>
	    	<% } %>
			<li class="section_header">Advanced Tools</li>
			<li class="section_text"><a href="bulkConfigWizard1.i" class="TipsMainMenu" id="tooltip_sm_devices_device_configuration_wizard">Device Configuration Wizard</a></li>
			<li class="section_text"><a href="massFileTransfers.i" class="TipsMainMenu" id="tooltip_sm_devices_mass_file_transfers">Mass File Transfers</a></li>
			<%
                if (menuManager.showTopDeviceConfWizard()) {%>
                    <li class="section_text"><a href="massSimActivation.i" class="TipsMainMenu" id="tooltip_sm_mass_sim_activation">Mass SIM Activation</a></li>
                <%}
             %>
			<li class="section_text"><a href="serialNumberAllocation.i" class="TipsMainMenu" id="tooltip_sm_devices_serial_number_allocations">Serial Number Allocations</a></li>
			<li class="section_text"><a href="deviceCloning.i" class="TipsMainMenu" id="tooltip_sm_devices_device_cloning">Device Cloning</a></li>
			<li class="section_text"><a href="deviceInit.i" class="TipsMainMenu" id="tooltip_sm_devices_device_init">Device Activations</a></li>
			<li class="section_text"><a href="deviceMessages.i" class="TipsMainMenu" id="tooltip_sm_devices_device_messages">Device Messages</a></li>
			<li class="section_text"><a href="credential.i" class="TipsMainMenu" id="tooltip_sm_devices_credentials">Credentials</a></li>
			<li class="section_text"><a href="firmwareUpgrade.i" class="TipsMainMenu" id="tooltip_sm_devices_firmware_upgrades">Firmware Upgrades</a></li>
			<li class="section_text"><a href="deviceSupport.i" class="TipsMainMenu" id="tooltip_sm_devices_support">Device Support</a></li>
		</ul>
	</li>
	<%}
     if (menuManager.showTopMenuLocations())
     {
     %>
	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_locations">DMS Locations</a>
		<ul id="ul_locations">
			<li class="section_header">DMS Location Name</li>
			<li class="section_text"><form method="get" action="/locationList.i" name="search"><table><tr><td><input type="text" name="name" maxlength="60" class="TipsMainMenu menuTextbox" id="tooltip_sm_locations_search_location_name" /><input type='hidden' name='action' value='Search' /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
			<li class="section_header">DMS Location City</li>
			<li class="section_text"><form method="get" action="/locationList.i" name="search"><table><tr><td><input type="text" name="city" maxlength="60" class="TipsMainMenu menuTextbox" id="tooltip_sm_locations_search_location_city" /><input type='hidden' name='action' value='Search' /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
			<li class="section_header">New DMS Location Name</li>
			<li class="section_text"><form method="post" action="/editLocation.i" name="search"><table><tr><td><input type="text" name="name" maxlength="60" class="TipsMainMenu menuTextbox" id="tooltip_sm_locations_new_location" /><input type='hidden' name='action' value='New' /><input type="submit" class="cssButton" value="Next" /></td></tr></table></form></li>
			<li class="section_header">View DMS Locations</li>
			<li class="section_text"><a href="#">by Type</a>
			<ul class="scrollable">
			<li class="section_header">DMS Location Type</li>
			<%
			Iterator<NameValuePair> locationTypesIt = nvp_list_location_types.iterator();
			while (locationTypesIt.hasNext()) {   
				NameValuePair locationTypeNVP = locationTypesIt.next();		
				%>
				<li class="section_text"><a href="locationList.i?location_type_id=<%=locationTypeNVP.getValue()%>" ><%=locationTypeNVP.getName()%></a></li>	
				<%
				
			}%>
			</ul>
			</li>
			<%
			if ("esuds".equalsIgnoreCase(menu)) {
			List<NameValuePair> nvp_list_esuds_alpha_chars = dms_values_list_esudsAlphaCharsList.getList(getServletContext());
			%>
			<li class="section_text"><a href="#">by eSuds School</a>
			<ul id="menu_esuds_schools">
			<li class="section_header">School</li>			
			<%Iterator<NameValuePair> esudsAlphsIt = nvp_list_esuds_alpha_chars.iterator();			
			while  ( esudsAlphsIt.hasNext() )   { 
				NameValuePair esudsAlphaNVP = esudsAlphsIt.next();
				String alphaChar = (String)esudsAlphaNVP.getValue();				
				%><li class="section_text"><a href="#"><%=alphaChar%></a><%
				List<NameValuePair> esudsAlpaSubList = null;
				esudsAlpaSubList = (List<NameValuePair>)application.getAttribute("dms_values_list_esudsAlphaCharsList_sub_"+alphaChar);
				Iterator<NameValuePair> esudsAlphaSubIt = esudsAlpaSubList.iterator();
				%>
				<ul>
				<li class="section_header"><%=alphaChar%></li>
				<%
				while(esudsAlphaSubIt.hasNext()){
					NameValuePair esudsAlphaSubNVP = esudsAlphaSubIt.next();
					%><li class="section_text" ><a href="locationTree.i?source_id=<%=esudsAlphaSubNVP.getValue()%>"><%=esudsAlphaSubNVP.getName()%></a></li><%
				}				
				%>
				</ul>
				</li>
				<%
			}
			%>					
			</ul>
			</li>
			<% } else { %>
				<li class="section_text" ><a href="home.i?menu=esuds" class="TipsMainMenu" id="tooltip_sm_locations_load_esuds_school">by eSuds School</a>
			<%} %>
		</ul>
	</li>
	
	<%}
     if (menuManager.showTopMenuCustomers()) 
     {
    %>
    	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_customers">DMS Customers</a>
			<ul>
				<li class="section_header">DMS Customer Name</li>
				<li class="section_text"><form method="get" action="/customerList.i" name="search"><table><tr><td><input type="text" name="customer_name" class="TipsMainMenu menuTextbox" id="tooltip_sm_customers_search_customer_name" /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
				<li class="section_header">New DMS Customer Name</li>
				<li class="section_text"><form method="post" action="/editCustomer.i" name="createNew"><table><tr><td><input type="text" name="customer_name" class="TipsMainMenu menuTextbox" id="tooltip_sm_customers_new_customer" /><input type="submit" class="cssButton" value="Next" /></td></tr></table></form></li>
				<li class="section_header">View DMS Customers</li>
				<li class="section_text"><a href="#">by Type</a>
				<ul>
				<li class="section_header">DMS Customer Type</li>
				<li class="section_text"><a href="/customerList.i?customer_type=2" >Distributor</a></li>			
				<li class="section_text"><a href="/customerList.i?customer_type=3" >End User</a></li>
				<li class="section_text"><a href="/customerList.i?customer_type=1" >eSuds Operator</a></li>
				<li class="section_text"><a href="/customerList.i?customer_type=4" >Sony PictureStation Operator</a></li>				
				</ul>
				</li>
				<li class="section_header">Advanced Tools</li>
				<li class="section_text"><a href="/esudsPrivileges.i" class="TipsMainMenu" id="tooltip_sm_customers_esuds_privileges">eSuds Web Privilege Manager</a></li>
			</ul>
    	</li>
    <%}
     if (menuManager.showTopMenuConsumers()) {
     %>
	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_consumers">Consumers</a>
		<ul>
			<li class="section_header">Consumers</li>
			<li class="section_text" ><a href="/consumerSearch.i?action=Start%20Search" class="TipsMainMenu" id="tooltip_mm_consumers2">Consumer Search</a></li>
			<li class="section_text" ><a href="/consumerCardSearch.i?action=Start%20Search" class="TipsMainMenu" id="tooltip_mm_cards">Consumer Card Search</a></li>
			<li class="section_text"><a href="/newConsumerAcct1.i" >Card Creation Wizard</a></li>
			<li class="section_text"><a href="/cardConfigWizard1.i" >Card Configuration Wizard</a></li>
			<li class="section_text"><a href="/newSproutCardAcct.i" >Sprout Card Wizard</a></li>
		</ul>
	</li>
	<%
	}
    if (menuManager.showTopMenuFiles()) {
	%>
	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_file_type">Files</a>
		<ul>
			<li class="section_header">File Name</li>
			<li class="section_text" ><form method="get" action="/fileList.i" name="search"><table><tr><td><input type="text" name="file_transfer_name" class="TipsMainMenu menuTextbox" id="tooltip_sm_files_search_file_name" /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
			<li class="section_header">File ID</li>
			<li class="section_text" ><form method="get" action="/fileList.i" name="search"><table><tr><td><input type="text" name="file_transfer_id" class="TipsMainMenu menuTextbox" id="tooltip_sm_files_search_file_id" /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
			<li class="section_header">Upload File to Server</li>
			<li class="section_text" ><form method="get" action="/newFile.i" name="search"><table><tr><td><input type="submit" class="cssButton" value="Next" /></td></tr></table></form></li>
			<li class="section_header">Create External File Transfer</li>
			<li class="section_text" ><form method="get" action="/createExternalFileTransfer.i" name="search"><table><tr><td><input type="text" name="device_name" class="TipsMainMenu menuTextbox" id="tooltip_sm_files_create_ext_file_transfer" /><input type="submit" class="cssButton" value="Next" /></td></tr></table></form></li>
			<li class="section_header">View Files</li>
			<li class="section_text"><a href="#">by Type</a>
			<ul id="menu_file_types" class="scrollable">
			<li class="section_header">File Type</li>
			<%
				Iterator<NameValuePair> fileTypesIt = nvp_list_file_transfer_types.iterator();
				while(fileTypesIt.hasNext()){
					NameValuePair fileTypeNVP = fileTypesIt.next();
					%><li class="section_text"><a href="fileList.i?file_transfer_type_cd=<%=fileTypeNVP.getValue()%>" ><%=fileTypeNVP.getName()%></a></li><%
				}	
			%>
			</ul>
			</li>
			<li class="section_text"><a href="/DFRFileList.i" class="TipsMainMenu" id="tooltip_mm_dfr_files">DFR Files</a></li>
		</ul>
	</li>	
	<% 
    }
	if (menuManager.showLeftMenus4CustService()) { 
	%>
   <li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_risk_alerts">Risk Alerts</a>
   <ul style="top:-151px;">
     <li class="section_header">Risk Alert Search</li>
     <li class="section_text" >
       <form method="get" action="/riskAlerts.i" name="search">
       <table>
         <tr>
           <td>
            <select name="search_type" id="tooltip_sm_terminal_search_type" class="TipsMainMenu menuSelect">
              <option value="serial_number">Serial Number</option>
              <option value="terminal_number">Terminal Number</option>
              <option value="customer_name">Customer Name</option>
              <option value="location_name">Location Name</option>
            </select>
           </td>
         </tr>
         <tr><td><input type="text" name="search_param" maxlength="200" class="TipsMainMenu menuTextbox" id="tooltip_sm_terminal_search" /></td></tr>
         <tr>
         <td>
         <select name="device_type_id" class="TipsMainMenu menuSelect">
         <option value="">All Device Types</option>
         <%
             Iterator<NameValuePair> deviceTypeIt = nvp_list_device_types.iterator();
             while(deviceTypeIt.hasNext()){
               NameValuePair deviceIdNVP = deviceTypeIt.next();
           %>
           <option value="<%=StringUtils.prepareCDATA(deviceIdNVP.getValue())%>"><%=StringUtils.prepareHTML(deviceIdNVP.getName())%></option>
         <% } %>
         </select>
         </td>
         </tr>
         <tr>
         <td>
         <select name="statusCd" class="TipsMainMenu menuSelect">
         <option value="">Any Status</option>
         <% for(RiskAlert.Status riskAlertStatus : RiskAlert.Status.values()) { %>
           <option value="<%=StringUtils.prepareCDATA(riskAlertStatus.toString())%>"><%=StringUtils.prepareHTML(riskAlertStatus.getName())%></option>
         <% } %>
         </select>
         </td>
         </tr>
         <tr>
         <td class="menu_text">
         Minimum Score:<br/><input type="text" name="minScore" class="TipsMainMenu menuTextbox" id="minScore" />
         </td>
         </tr>
         <tr>
         <td class="menu_text">
         Maximum Days Old:<input type="text" name="daysOld" class="TipsMainMenu menuTextbox" id="daysOld" />
         </td>
         <td rowspan="6">
           <input type="submit" class="cssButton" value="Search" />
         </td>
         </tr>
       </table>
       </form>
     </li>
     <li class="section_header">Risk Alerts</li>
     <li class="section_text"><a href="riskAlerts.i?minScore=100">High Priority</a></li>
     <li class="section_text"><a href="riskAlerts.i?daysOld=1">All Recent</a></li>
     <li class="section_text"><a href="riskAlerts.i?statusCd=N">All New</a></li>
   </ul>
   </li>
 <% } %>

	<% if (menuManager.showLeftMenuSales()) { %>
	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_sales">Sales</a>
		<ul>
			<li class="section_header">Last Name</li>
			<li class="section_text" ><form method="get" action="/salesRepList.i" name="search"><table><tr><td><input type="text" name="lastName" class="TipsMainMenu menuTextbox" id="tooltip_sm_sales_rep_search_lastName" /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
			<li class="section_header">Sales Reps</li>
			<li class="section_text"><a href="/salesRep.i">Edit</a></li>
			<li class="section_text"><a href="/bulkSalesReps.i">Update Device Sales Reps</a></li>
			<li class="section_header">Distributors</li>
			<li class="section_text"><a href="/distributor.i">Edit</a></li>
			<li class="section_text"><a href="/bulkDistributors.i">Update Device Distributors</a></li>
			<li class="section_header">Customer Sales Data</li>
			<li class="section_text"><a href="/affiliate.i">Edit Affiliates</a></li>
			<li class="section_text"><a href="/pricingTier.i">Edit Pricing Tiers</a></li>
			<li class="section_text"><a href="/category.i">Edit Categories</a></li>
			<li class="section_text"><a href="/customerSales.i">Update Customer Sales Data</a></li>
		</ul>
	</li>
	<%  } %>
	
	<%
	if (menuManager.showLeftMenuTransactions())
	{
	%>
	
	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_menu_transactions">Transactions</a>
		<ul>
			<li class="section_header">Transaction Tools</li>
			<li class="section_text"><a href="tranMenu.i" class="TipsMainMenu" id="tooltip_mm_transactions" >Transactions</a></li>
			<li class="section_text"><a href="dfrRejectionList.i" class="TipsMainMenu" id="tooltip_mm_dfr_rejections" >DFR Rejections</a></li>
			<%if (menuManager.isPaymentAdmin()) {%>
			<li class="section_text"><a href="pendingReversalList.i" class="TipsMainMenu" id="tooltip_mm_pending_reversals" >Pending Reversals</a></li>
			<%}%>
		</ul>
	<%
	    }

	        if (menuManager.showLeftMenuReports())
	        {
	%><li class="parent_nav"><a href="reportsUtilityMenu.i" class="TipsMainMenu" id="tooltip_mm_reports">Reports</a></li>
	<%
	    }
	        if (menuManager.showLeftMenuAdvTools())
	        {
	%>
	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_advanced_tools">Advanced Tools</a>
		<ul>
			<li class="section_header">Advanced Tools</li> 
			<%
			if (menuManager.showLeftAdvToolConfTemplate())
            {
				%><li class="section_text"><a href="templateMenu.i" class="TipsMainMenu" id="tooltip_mm_config_templates">Configuration&nbsp;Templates</a></li><%
		    }
			if (menuManager.isPOSMAdmin())
            {
				%><li class="section_text"><a href="posmAdmin.i" class="TipsMainMenu" id="tooltip_mm_posm_admin">POSM&nbsp;Administration</a></li><%
		    }
		    if (menuManager.showLeftAdvToolGprs())
		    {
		    	%><li class="section_text"><a href="gprswebIndex.i">SIM Card Administration</a></li><%
		    }
		    %>
			<li class="section_text"><a href="javascript:window.location = '/changeHistory.i?object_type_cd=' + document.getElementById('<%=DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD%>').value + '&object_cd=' + document.getElementById('<%=DevicesConstants.CHANGE_HISTORY__OBJECT_CD%>').value">Change History</a></li>			
			<%
			if (menuManager.showLeftAdvToolRefreshMenu())
            {
				%><li class="section_text"><a href="reloadValuesLists.i" >Refresh&nbsp;Cache</a></li><%
		    }			
		   %>
		   <%
			if (menuManager.showTopDeviceNumber())
            {
				%><li class="section_text"><a href="logsHistory.i" >Log History</a></li><%
		    }
		   %>
		   <%if (menuManager.showTopDeviceConfWizard()) {%>
		   		<li class="section_text"><a href="publicKey.i" class="TipsMainMenu" id="tooltip_sm_devices_public_keys">Public Keys</a></li>
		   <%}%>
		</ul>
	</li>
	<%
	    } %>
	    
	<li class="parent_nav"><a href="#" class="TipsMainMenu" id="tooltip_mm_eport_manager">ePort Manager</a>
		<ul>
			<li class="section_header">ePort Manager</li>
			<% if (menuManager.showLeftMenuBankAccounts())  {  %>
				<li class="section_text"><a href="#">Bank Accounts</a>
				<ul>
					<%  if (menuManager.showLeftBANew()) { %>
					<li class="section_header">Customer Name</li>
					<li class="section_text" ><form method="get" action="/bankList.i" name="search"><table><tr><td><input type="text" name="customer_name" class="TipsMainMenu menuTextbox" id="tooltip_sm_bank_search_customer_name" /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
					<li class="section_header">Bank Accounts</li>
					<li class="section_text"><a href="/changedBankAccounts.i">New</a></li>
					<%  } %>
				</ul>
				</li>
			<% } %>	
    	
			<% if (menuManager.showLeftMenus4CustService()) { %>
				<li class="section_text"><a href="#">Customers</a>
				<ul>
					<li class="section_header">Customer Name</li>
					<li class="section_text" ><form method="get" action="/customersList.i" name="search"><table><tr><td><input type="text" name="customer_name" class="TipsMainMenu menuTextbox" id="tooltip_sm_customers2_search_customer_name" /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
					<li class="section_header">Customers</li>
					<li class="section_text"><a href="customer.i">Edit</a></li>
				    <li class="section_text"><a href="customer.i?noLicense=1">No License Agreement</a></li>
					<li class="section_text"><a href="changedCustomerList.i">New</a></li>
				</ul>
				</li>
			<% } %>
	    	
	    	<li class="section_text"><a href="#">Programs (Dealers)</a>
				<ul>
					<%  if (menuManager.showLeftMenus4CustService()) { %>
					<li class="section_header">Program Name</li>
					<li class="section_text" ><form method="get" action="/dealerList.i" name="search"><table><tr><td><input type="text" name="dealer_name" class="TipsMainMenu menuTextbox" id="tooltip_sm_dealer_search_customer_name" /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
					<li class="section_header">Programs</li>
					<li class="section_text"><a href="/dealer.i">Edit</a></li>
					<%  } %>
				</ul>
			</li>
		    
		    <% if (menuManager.showLeftMenuEft()) {  %>
				<li class="section_text"><a href="#">EFT/ACH</a>
				<ul style="top:-86px;">
					<li class="section_header">EFTs</li>
					<% if (menuManager.showLeftEftPending()) { %>
					<li class="section_text" ><a href="pendingEFT.i">Pending EFTs</a></li>
					<% } %>
					<% if (menuManager.showLeftEftApproved()){ %>
					<li class="section_text"><a href="approvedEFT.i">Approved EFTs</a></li>
					<li class="section_text"><a href="processedEFT.i">Processed EFTs</a></li>
					<% } %>
					<% if (menuManager.showLeftEftPending()) { %>
					<li class="section_text"><a href="addEFTAdjustments.i" class="TipsMainMenu" id="tooltip_sm_efts_add_adjustments">Add Adjustments</a></li>
					<% } %>
					<li class="section_header">ACHs</li>
					<li class="section_text"><a href="searchACH.i">ACH Search</a></li>
				</ul>
				</li>
			<% } %>
			
			<% if (menuManager.showLeftMenuFees()) {  %>
			<li class="section_text"><a href="#">Fees</a>
				<ul>
					<li class="section_header">Fees</li>
					<li class="section_text"><a href="updateProcessFees.i" class="TipsMainMenu" id="tooltip_sm_fees_update_process_fees">Update Process Fees</a></li>
					<li class="section_text"><a href="updateServiceFees.i" class="TipsMainMenu" id="tooltip_sm_fees_update_service_fees">Update Service Fees</a></li>
				</ul>
			</li>
			<% } %>
	
			<% if (menuManager.showLeftMenus4CustService()) { %>
		    <li class="section_text"><a href="#">License Agreements</a>
				<ul>					
					<li class="section_header">License Agreement Name</li>
					<li class="section_text" ><form method="get" action="/licenseList.i" name="search"><table><tr><td><input type="text" name="license_name" class="TipsMainMenu menuTextbox" id="tooltip_sm_license_search_license_name" /><input type="submit" class="cssButton" value="Search" /></td></tr></table></form></li>
					<li class="section_header">License Agreements</li>
					<li class="section_text"><a href="/license.i">Edit</a></li>
				</ul>
			</li>
			<%  } %>
			
			<% if (menuManager.showLeftMenuTerminals()) { %>
				<li class="section_text"><a href="#">Terminals</a>
				<ul style="top:-151px;">
					<li class="section_header">Terminal Search</li>
					<li class="section_text" >
						<form method="get" action="/terminalsList.i" name="search">
						<table>
							<tr><td><select name="search_type" id="tooltip_sm_terminal_search_type" class="TipsMainMenu menuSelect"><option value="serial_number">Serial Number</option><option value="terminal_number">Terminal Number</option><option value="customer_name">Customer Name</option><option value="location_name">Location Name</option><option value="sales_order_number">Sales Order #</option><option value="purchase_order_number">Purchase Order #</option></select></td><td rowspan="3"><input type="submit" class="cssButton" value="Search" /></td></tr>
							<tr><td><input type="text" name="search_param" maxlength="200" class="TipsMainMenu menuTextbox" id="tooltip_sm_terminal_search" /></td></tr>
							<tr>
							<td>
							<select name="device_type_id" class="TipsMainMenu menuSelect">
							<option value="">All Device Types</option>
							<%
						    	Iterator<NameValuePair> deviceTypeIt = nvp_list_device_types.iterator();
						    	while(deviceTypeIt.hasNext()){
						    		NameValuePair deviceIdNVP = deviceTypeIt.next();
						    %>
								<option value="<%=deviceIdNVP.getValue()%>"><%=deviceIdNVP.getName()%></option>
							<% } %>
							</select>
							</td>
							</tr>
						</table>
						</form>
					</li>
					<li class="section_header">Terminals</li>
					<% if (menuManager.showLeftTNewEdit()) { %>
						<li class="section_text"><a href="newTerminals.i">New</a></li>
					<% } %>
					<% if (menuManager.showLeftTUpdated()){ %>
						<li class="section_text"><a href="updatedTerminals.i">Updated</a></li>
					<%}%>
					<li class="section_text"><a href="/updateTerminals.i" class="TipsMainMenu" id="tooltip_sm_terminals_update_terminals">Update Terminals</a></li>
					<li class="section_text"><a href="/terminalAssignments.i" class="TipsMainMenu" id="tooltip_sm_terminals_terminal_assignments">Terminal Assignments</a></li>
					<li class="section_text"><a href="/createFills.i" class="TipsMainMenu" id="tooltip_sm_terminals_create_fills">Create Fills</a></li>
				</ul>
				</li>
			<% } %>
		</ul>
	</li>
	<% } %>
</ul>
</div>
<div class="spacer1">&nbsp;</div>
</div>

<%if (loggedUser != null) {%>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, request.getRequestURL().toString(), "/js/cssverticalmenu.js")%>"></script>
<%}%>