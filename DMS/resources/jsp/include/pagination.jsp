<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>

<%
/* -------------------------------------------------------------------------- */
/* 1. try to get the id of this pagination, this provides the flexibility to use multiple pagination bar in one page. */
/* -------------------------------------------------------------------------- */
// try to get from the "jsp:param"
String paginationId = request.getParameter(PaginationUtil.PARAM_PAGINATION_ID);

/* -------------------------------------------------------------------------- */
/* 2. try to get the action url for this page, somehow, the url in the address bar just not that one we really want. */
/* -------------------------------------------------------------------------- */
// try to get from the "jsp:param"
String requestUrl = request.getParameter(PaginationUtil.PARAM_REQUEST_URL);
// if not specified, use the url of current request.
requestUrl = StringHelper.isBlank(requestUrl) ? request.getRequestURL().toString() : requestUrl;

//the "total count field"
String totalField = PaginationUtil.getTotalField(paginationId);
// the "page size field"
String sizeField = PaginationUtil.getSizeField(paginationId);
// the "page index field"
String indexField = PaginationUtil.getIndexField(paginationId);
// the "sort index" field
String sortField = PaginationUtil.getSortField(paginationId);

InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
boolean bulk = inputForm.getBoolean("bulk", false, false);
		
//the total count
int totalCount = inputForm.getInt(totalField, false, -1);
// the page size
int pageSize = inputForm.getInt(sizeField, false, PaginationUtil.DEFAULT_PAGE_SIZE);
// the page index
int pageIndex = inputForm.getInt(indexField, false, 1);
// the sort index
String sortIndex = inputForm.getString(sortField, false);

//use the default if not specified
paginationId = StringHelper.isBlank(paginationId) ? PaginationUtil.DEFAULT_PAGINATION_ID : paginationId.trim();

// the list of param names which will be carried over for the pagination/sorting
// EPC NOTE:  	For any paginated pages that include this file, do not set a param explicitly named "action" in the list of "storedNames" passed in here.
// 				This pagination.jsp will dynamically build a form for submittal based on the pagination links.
// 				Whenever the user clicks on a page number or column header for form re-submittal, the form will be submitted
// 				based on the string in the 'action' attribute, i.e <form method="post" action="consumerSearch.i?action=Search" id="_form___paging">
// 				If you send an additional param named "action", in conjunction with one sent as part of the form as part of "storedNames", 
//				it could override the form's 'action' string for the request and send the user to an empty screen
// 				when the form is submitted (see javascript function nav_{using paginationId as part of the method name}(value) below).
String[] storedNames = PaginationUtil.decodeStoredNames(request.getParameter(PaginationUtil.PARAM_STORED_NAMES));

// if the "total count" is not provided or is less than or equal to the lowest number of viewable records per page (15), there's no pagination bar
if (totalCount < 16) {
	if (totalCount == 0) {
	%>
		<table align="center" class="pagination">
			<tr><td>&nbsp;&nbsp;&nbsp;<b><%=totalCount%></b> records found.</td></tr>
		</table>
	<%} else if (totalCount > 0 ) {
		// if the total record count is between 15 and 1, which is equal to the lowest number of viewable records per page (15), show no message, just some empty space
	%>
		<br />
	<%
	}
}else{
	//if (totalCount >= 16) {
	// calculate the page count
	int pageCount = PaginationUtil.getPageCount(pageSize, totalCount);


if (pageCount < 1) {
	
	// if there are no pages, this also indicates no results, just show the no records found message
	%>
	<table align="center" class="pagination">
	<tr>
	<td>&nbsp;&nbsp;&nbsp;<b><%=totalCount%></b> records found. </td>
	</tr>
	</table>
	<%
	
} else {
	
	//show the pagination bar
    int extraCols = 10;
    boolean showPrevGroup = PaginationUtil.getLinkNum(1, pageIndex, pageCount) > 1;
    if (showPrevGroup) {
        extraCols += 1;
    }
    boolean showNextGroup = PaginationUtil.showPageLink(15, pageCount) && PaginationUtil.getLinkNum(15, pageIndex, pageCount) < pageCount;
    if (showNextGroup) {
        extraCols += 1;
    }
    
    int cols;
    if (pageCount > 15) {
        cols = 15 + extraCols;
    } else {
        cols = extraCols + pageCount;
    }
%>
<!-- BEGIN PAGINATION BAR -->
<form method="get">
<table align="center" class="pagination">
<tr>
    <td align="center" colspan="<%=cols%>">Range <%=PaginationUtil.getStartNum(pageSize, pageIndex)%> - <%=(pageIndex == pageCount ? totalCount : PaginationUtil.getEndNum(pageSize, pageIndex))%> of <%=totalCount%> Records</td>
</tr>
<%if (!bulk) {%>
<tr>
<td>&nbsp;</td>
<td colspan="3" align="right">
	<%if (pageIndex > 1) { %><a href="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>(1);" class="noDecoration"><%} %><img src="/images/skip_backward.png" alt="First Page" title="First Page" border="0" align="middle"><%if (pageIndex > 1) { %></a><%} %>
	&nbsp;<%if (pageIndex > 1) { %><a href="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>(<%=(pageIndex - 1)%>);" class="noDecoration"><%} %><img src="/images/rewind.png" alt="Previous Page" title="Previous Page" border="0" align="middle"><%if (pageIndex > 1) { %></a><%} %>
</td>
<td>&nbsp;</td>
<%if (PaginationUtil.getLinkNum(1, pageIndex, pageCount) > 1) {%><td><a href="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>Group(<%=(PaginationUtil.getLinkNum(1, pageIndex, pageCount) - 8)%>);">...</a></td>
<%}
for (int linkIndex = 1; linkIndex <= 15 && linkIndex <= pageCount; linkIndex++) {
    if (PaginationUtil.showPageLink(linkIndex, pageCount)) {
        int linkNo = PaginationUtil.getLinkNum(linkIndex, pageIndex, pageCount);
        if (linkNo == pageIndex) {
%>
<td align="center"><span style="font-weight:bold; color:#990033;">&nbsp;<%=linkNo%>&nbsp;</span></td>
<%     } else {%>
<td align="center"><a href="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>(<%=linkNo%>);"><%=linkNo%></a></td>
<%}}}
if (PaginationUtil.showPageLink(15, pageCount) && PaginationUtil.getLinkNum(15, pageIndex, pageCount) < pageCount) {%><td><a href="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>Group(<%=(PaginationUtil.getLinkNum(15, pageIndex, pageCount) + 8)%>);">...</a></td><%}%>
<td>&nbsp;</td>
<td colspan="3" align="left">	
	<%if (pageIndex < pageCount) { %><a href="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>(<%=(pageIndex + 1)%>);" class="noDecoration"><%} %><img src="/images/fast_forward.png" alt="Next Page" title="Next Page" border="0" align="middle"><%if (pageIndex < pageCount) { %></a><%} %>
	&nbsp;<%if (pageIndex < pageCount) { %><a href="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>(<%=pageCount%>);" class="noDecoration"><%} %><img src="/images/skip_forward.png" alt="Last Page" title="Last Page" border="0" align="middle"><%if (pageIndex < pageCount) { %></a><%} %>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td  align="center" colspan="<%=cols%>">Go to Page <select tabindex="1" onchange="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>(this.options[this.selectedIndex].value)">
<%for (int index = 1; index <= pageCount; index++) {
%>
<option<%if(pageIndex == index) {%> selected="selected"<%}%> value="<%=index%>"><%=index%></option>
<%}%>
</select> of <b><%=pageCount%></b> | Show <select tabindex="2" onchange="javascript:nav_<%=StringUtils.encodeForJavaScript(paginationId)%>PerPage(this.options[this.selectedIndex].value);">
<option<%if(15 == pageSize) {%> selected="selected"<%}%> value="15">15</option>
<option<%if(25 == pageSize) {%> selected="selected"<%}%> value="25">25</option>
<option<%if(50 == pageSize) {%> selected="selected"<%}%> value="50">50</option>
<option<%if(100 == pageSize) {%> selected="selected"<%}%> value="100">100</option>
<option<%if(250 == pageSize) {%> selected="selected"<%}%> value="250">250</option>
<option<%if(500 == pageSize) {%> selected="selected"<%}%> value="500">500</option>
<%if (request.getServletPath().endsWith("EFT.i")) { %>
<option<%if(950 == pageSize) {%> selected="selected"<%}%> value="950">950</option>
<%} %>
<option<%if(1000 == pageSize) {%> selected="selected"<%}%> value="1000">1000</option>
<option<%if(5000 == pageSize) {%> selected="selected"<%}%> value="5000">5000</option>
<option<%if(10000 == pageSize) {%> selected="selected"<%}%> value="10000">10000</option>
<option<%if(25000 == pageSize) {%> selected="selected"<%}%> value="25000">25000</option>
</select> Rows Per Page</td>
</tr>
<%}%>
</table>
</form>

<a name id="__nav_<%=StringUtils.encodeForJavaScript(paginationId)%>">
<script type="text/javascript">
<!--
function nav_<%=StringUtils.encodeForJavaScript(paginationId)%>(value) {
	if (<%=pageIndex%> == value) {
		return;
	}
	
    var myform = create<%=StringUtils.encodeForJavaScript(paginationId)%>Form("_form_<%=StringUtils.encodeForJavaScript(paginationId)%>");
    // the field for "page index"
    var myhidden=document.createElement("input");
    myhidden.setAttribute("type","hidden");
    myhidden.setAttribute("name", "<%=StringUtils.encodeForJavaScript(indexField)%>");
    myhidden.setAttribute("value", value);
    myform.appendChild(myhidden);

    // the field for "page size"
    myhidden=document.createElement("input");
    myhidden.setAttribute("type","hidden");
    myhidden.setAttribute("name", "<%=StringUtils.encodeForJavaScript(sizeField)%>");
    myhidden.setAttribute("value", "<%=pageSize%>");
    myform.appendChild(myhidden);
<% if (!StringHelper.isBlank(sortIndex)) {%>

 	// the field for "sort index"
    myhidden=document.createElement("input");
    myhidden.setAttribute("type","hidden");
    myhidden.setAttribute("name", "<%=StringUtils.encodeForJavaScript(sortField)%>");
    myhidden.setAttribute("value", "<%=StringUtils.encodeForJavaScript(sortIndex)%>");
    myform.appendChild(myhidden);
<%}%>

    var myinsertloc = document.getElementById("__nav_<%=StringUtils.encodeForJavaScript(paginationId)%>");
    myinsertloc.appendChild(myform);
    myform.submit();
}
//-->
</script>
</a>

<a name id="__nav_size_<%=StringUtils.encodeForJavaScript(paginationId)%>">
<script type="text/javascript">
<!--
function nav_<%=StringUtils.encodeForJavaScript(paginationId)%>PerPage(value) {
    var myform = create<%=StringUtils.encodeForJavaScript(paginationId)%>Form("_form_size_<%=StringUtils.encodeForJavaScript(paginationId)%>");
    // the filed for "page size"
    var myhidden=document.createElement("input");
    myhidden.setAttribute("type","hidden");
    myhidden.setAttribute("name", "<%=StringUtils.encodeForJavaScript(sizeField)%>");
    myhidden.setAttribute("value", value);
    myform.appendChild(myhidden);
<% if (!StringHelper.isBlank(sortIndex)) {%>

 	// the field for "sort index"
    myhidden=document.createElement("input");
    myhidden.setAttribute("type","hidden");
    myhidden.setAttribute("name", "<%=StringUtils.encodeForJavaScript(sortField)%>");
    myhidden.setAttribute("value", "<%=sortIndex%>");
    myform.appendChild(myhidden);
<%}%>

    var myinsertloc = document.getElementById("__nav_size_<%=StringUtils.encodeForJavaScript(paginationId)%>");
    myinsertloc.appendChild(myform);
    myform.submit();
}
//-->
</script>
</a>

<script type="text/javascript">
<!--
function nav_<%=StringUtils.encodeForJavaScript(paginationId)%>Group(pageNum) {
	var targetPage;
	if (pageNum < 8) {
		targetPage = 8;
	} else if (pageNum > (<%=pageCount%> - 7)) {
		targetPage = <%=pageCount%> - 7;
	} else {
		targetPage = pageNum;
	}
	return nav_<%=StringUtils.encodeForJavaScript(paginationId)%>(targetPage);
}
//-->
</script>

<%}}%>
<script type="text/javascript">
<!--
function create<%=StringUtils.encodeForJavaScript(paginationId)%>Form(formId) {
	var hidden_names = new Array();
	var hidden_values = new Array();

<%if (storedNames != null && storedNames.length > 0) {
    for(String paramName : storedNames) {
        String paramValue = inputForm.getString(paramName, false);
        if (StringHelper.isBlank(paramValue)) {
            continue;
        }
        // escape the quotes and control-chars in the value
        paramValue = StringUtils.prepareScript(paramValue);
%>
    hidden_names.push("<%=StringUtils.encodeForJavaScript(paramName)%>");
    hidden_values.push("<%=paramValue%>");<%}}%>

<%if (totalCount >= 0) {%>
	//the "total count"
    hidden_names.push("<%=StringUtils.encodeForJavaScript(totalField)%>");
    hidden_values.push("<%=totalCount%>");
<%}%>

    var myform=document.createElement("form");
    myform.setAttribute("method", "get");
    myform.setAttribute("action", "<%=StringUtils.encodeForJavaScript(requestUrl)%>");
    myform.setAttribute("id", formId);
    for (var i = 0; i < hidden_names.length; i++) {
	    var myhidden=document.createElement("input");
	    myhidden.setAttribute("type","hidden");
	    myhidden.setAttribute("name",hidden_names[i]);
	    myhidden.setAttribute("value",hidden_values[i]);
	    myform.appendChild(myhidden);
    }
    return myform;
}
//-->
</script>

<!--END OF PAGINATION BAR -->

<a name id="__id_sort_<%=StringUtils.encodeForJavaScript(paginationId)%>">
<script type="text/javascript">
<!--
function sort_<%=StringUtils.encodeForJavaScript(paginationId)%>(value) {
    var myform = create<%=StringUtils.encodeForJavaScript(paginationId)%>Form("_form_sort_<%=StringUtils.encodeForJavaScript(paginationId)%>");

<%if (totalCount >= 0) {%>
    // the field for "page size"
    var myhidden=document.createElement("input");
    myhidden.setAttribute("type","hidden");
    myhidden.setAttribute("name", "<%=StringUtils.encodeForJavaScript(sizeField)%>");
    myhidden.setAttribute("value", "<%=pageSize%>");
    myform.appendChild(myhidden);
<%}%>

 	// the field for "sort index"
    myhidden=document.createElement("input");
    myhidden.setAttribute("type","hidden");
    myhidden.setAttribute("name", "<%=StringUtils.encodeForJavaScript(sortField)%>");
    myhidden.setAttribute("value", value);
    myform.appendChild(myhidden);

    var myinsertloc = document.getElementById("__id_sort_<%=StringUtils.encodeForJavaScript(paginationId)%>");
    myinsertloc.appendChild(myform);
    myform.submit();
}
//-->
</script>
</a>

