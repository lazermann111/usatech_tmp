<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.BasicServletUser,simple.servlet.SimpleServlet,simple.bean.ConvertUtils,simple.servlet.RequestUtils" %>
<%@page import="simple.servlet.InputForm"%>
<% 
response.addHeader("Expires", "-1");
boolean fragment = ConvertUtils.getBooleanSafely(RequestUtils.getAttribute(request, "fragment", false), false);
boolean headerLoaded = ConvertUtils.getBooleanSafely(RequestUtils.getAttribute(request, "headerLoaded", false), false);
if(!fragment && !headerLoaded) {
%>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<%
String pageType = request.getParameter("pageType");
boolean isBIRT = "BIRT".equalsIgnoreCase(pageType);
String deviceSerialNumber = String.valueOf(request.getAttribute("ssn"));
if (deviceSerialNumber == null || deviceSerialNumber.length() == 0 || deviceSerialNumber.equalsIgnoreCase("null")) {
	String deviceId = String.valueOf(request.getAttribute("device_id"));
	if (deviceId != null && deviceId.length() > 0 && !deviceId.equalsIgnoreCase("null")) {
		InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
		if (form != null)
			deviceSerialNumber = String.valueOf(form.getAttribute("com.usatech.dms.device.serialNumber"));
	}
}
StringBuilder title = new StringBuilder(DMSConstants.APP_CD);
if (deviceSerialNumber != null && deviceSerialNumber.length() > 0 && !deviceSerialNumber.equalsIgnoreCase("null"))
	title.append(" - ").append(deviceSerialNumber);
String servletPath = request.getServletPath();
if (servletPath != null)
	title.append(" - ").append(servletPath.replace("/", "").replace(".i", ""));
String serverName = request.getServerName();
if (serverName != null) {
	if (serverName.startsWith("dms.")) {}
	else if (serverName.startsWith("dms-ecc."))
		title.append(" - ").append("Certification");
	else if (serverName.startsWith("dms-int."))
		title.append(" - ").append("Integration");
	else if (serverName.startsWith("dms-dev."))
		title.append(" - ").append("Development");
	else
		title.append(" - ").append(serverName.replace(".usatech.com", ""));
}
%>
<title><%=title.toString()%></title>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta name="copyright" content="Copyright <%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR) %>, USA Technologies, Inc." />
<link rel="stylesheet" href="<%=RequestUtils.addLastModifiedToUri(request, null, "/css/style.css") %>" type="text/css" media="screen" />
<link rel="stylesheet" href="<%=RequestUtils.addLastModifiedToUri(request, null, "/css/cssverticalmenu.css")%>" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="all" href="<%=RequestUtils.addLastModifiedToUri(request, null, "/css/calendar-blue.css")%>" title="blue" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/common.js") %>"></script>

<%if (isBIRT) {%>
	<link rel="stylesheet" href="<%=RequestUtils.addLastModifiedToUri(request, null, "/styles/iv/style.css") %>" type="text/css" />
	<script src="<%=RequestUtils.addLastModifiedToUri(request, null, "/webcontent/birt/ajax/lib/prototype.js") %>" type="text/javascript"></script>
	
	<style>
	<!-- 
	div.colorbox {border: 1px solid black; width: 30px; height: 16px; margin: 2px; float: left;}
	div.textbox {margin: 2px; font-family: Tahoma, Trebuchet MS, Arial, sans-serif; font-size: 8pt; white-space:nowrap;}
	div.header {border: 1px solid black; color: #000000; width: 560px; height: 20px; margin-bottom: 10px; padding: 2px; font-size: 12pt; font-weight:bold; text-align: center;}
	div.footer {border: 1px solid black; color: #333333; width: 560px; margin-bottom: 10px; padding: 2px; font-size: 10pt;}
	div.boardbox {border: 1px solid black; width: 560px; margin-bottom: 10px; padding: 2px; min-height: 44px}
	-->
	</style>
<%} else {%>
	<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/mootools.v1.11-usat-debug.js") %>"></script>
	<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/ajax.js")%>"></script>
	<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/device.js")%>"></script>
	<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/calendar.js") %>"></script>
	<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/calendar-en.js") %>"></script>
	<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/calendar-setup.js") %>"></script>
	<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/swap_dates.js") %>"></script>
	<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/dateTime.js") %>"></script>	
<%}%>		

<!-- DO NOT REMOVE IE Flicker Bug Fix for CSS Menu re-validating bg imgs http://davidwalsh.name/preventing-css-background-flicker DO NOT REMOVE 
%MINIFYHTML80734ce456a8261c167cd9a535b3a6e97%  
-->
<script type="text/javascript">
function submit(form, action) {
	if(action)
		form.action = action;
	new Ajax(form.action, { method: form.method, async: false, data: form, update: $("contentMiddle"), evalScripts: true }).request();
}
function open(url) {
    new Ajax(url, { method: "get", async: false, update: $("contentMiddle"), evalScripts: true }).request();
}
</script>
</head>
<body>

<div class="wrapper">

<div id="header">
	<div id="logoUSATech" onclick="window.location = '/home.i';"></div>
	<div id="bannerTitle">Device Management System</div>
<%
BasicServletUser loggedUser = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
if (loggedUser != null) {
	String msgClass;
	String msg = ConvertUtils.getStringSafely(request.getAttribute("err"), "");
	if (msg.length() == 0) {
		msg = ConvertUtils.getStringSafely(request.getAttribute("msg"), "");
		msgClass = "msg";
	} else
		msgClass = "err";
%>
	<table id="userInfo">
		<tr>
			<td width="20%"> Welcome, <%=loggedUser.getFullName()%></td>
			<td width="60%" align="center"><div id="msg" class="<%=msgClass%>"><%=msg%></div></td>
	        <td width="20%" align="right"><a href="/logout.i" class="userInfoLink">Logout</a>&nbsp;</td>
	    </tr>
	 </table>
<%}%>
</div>

<div id="loadingDiv">
    <br />
    <span style="font-weight: bold; font-style: italic;">Please wait, page is loading...</span><br /><br />
    <img src="/images/progress.gif" alt="Loading page..." /><br /><br />
</div>
<%out.flush();%>

<div id="mainContent" style="visibility: hidden;">
<!-- load the main menu -->
<jsp:include page="leftMenu.jsp" flush="true" />
<div id="contentMiddle">
<%
	out.flush();
	request.setAttribute("headerLoaded", true);
} 
%>