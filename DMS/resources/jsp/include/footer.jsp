<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.bean.ConvertUtils,simple.servlet.RequestUtils" %>
<%@page import="simple.text.StringUtils"%>
<% 
boolean fragment = ConvertUtils.getBooleanSafely(RequestUtils.getAttribute(request, "fragment", false), false);
if(!fragment) { %>
</div>

<div class="push"></div>

</div>
<%// end of the main content %>

</div>
<%// end of the wrapper %>

<div id="footerContent">
    v.<%=GenerateUtils.getApplicationVersion()%> - USA Technologies, Inc. - &copy; Copyright 2003-<%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR) %> - Confidential
    <script type="text/javascript">
	document.getElementById("loadingDiv").innerHTML = '';
	document.getElementById("mainContent").style.visibility = 'visible';
	</script>
</div>

<script type="text/javascript">
document.getElementById("<%=DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD%>").value = "<%=ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD), "")%>";
document.getElementById("<%=DevicesConstants.CHANGE_HISTORY__OBJECT_CD%>").value = "<%=StringUtils.encodeForJavaScript(ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD), ""))%>";
</script>

</body>
</html>

<%
out.flush();
}
%>