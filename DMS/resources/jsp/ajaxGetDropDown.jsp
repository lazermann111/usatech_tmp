<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.dms.util.Helper"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
	//get the parameters coming in from the ajax request
	String selectListName = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_HTML_FIELD_NAME);
	String selectListID = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_HTML_FIELD_ID);
	if(StringHelper.isBlank(selectListID)){
		selectListID = selectListName;
	}
	String selectListLabel = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_HTML_LABEL);
	if(StringHelper.isBlank(selectListLabel)){
		selectListLabel = "";
	}
	String selectedValue = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_SELECT_LIST_SELECTED_VALUE);
	String onchangeEvent = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_ONCHANGE);
	if(StringHelper.isBlank(onchangeEvent)){
		onchangeEvent="";
	}else{
		onchangeEvent = " onchange=\""+onchangeEvent+"\"";
	}
	String sqlString = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_SQL);
	String style = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_STYLE);
	String paramsDelim = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_SQL_PARAMS_DELIM);
	if(StringHelper.isBlank(paramsDelim)){
		paramsDelim = ",";
	}
	String sqlUndefinedOption = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_SQL_UNDEFINED);
	boolean undefined = false;
	if((sqlUndefinedOption != null && sqlUndefinedOption.trim().length() > 0) && sqlUndefinedOption.equalsIgnoreCase("true")){
		undefined = true;
	}
	String sqlParamsString = (String)request.getAttribute(com.usatech.dms.util.DMSConstants.PARAM_AJAX_SQL_PARAMS);
	Object[] stringParams = StringUtils.split(sqlParamsString, paramsDelim);
	
	List list = (ArrayList)Helper.buildSelectList(sqlString, undefined, stringParams);
%>
<c:set var="selectedValue"><%=selectedValue %></c:set>

<c:out value="<%=selectListLabel %>" escapeXml="false"/><select name="<%=selectListName %>" id="<%=selectListID %>" style="<%=style %>" <%=onchangeEvent %> >
	<c:forEach var="nvp_item" items="<%=list%>">
		<c:choose>
			<c:when test="${nvp_item.value == selectedValue}">
				<option value="${nvp_item.value}" selected="selected">${nvp_item.name}</option>
			</c:when>
			<c:otherwise>
				<option value="${nvp_item.value}">${nvp_item.name}</option>
			</c:otherwise>
		</c:choose>	    	
    </c:forEach>
   </select>