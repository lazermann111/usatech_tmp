<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="com.usatech.dms.util.Helper"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Sprout Card Customer Association</div>
</div>
<form method="post" action="newSproutCardAcctResult.i">
<table class="tabDataDisplayBorder">
		<tr>
			<td width="20%">Reporting Customer</td>			
			<td>				
				<select name="corp_customer_id" id="corp_customer_id" style="font-family: courier; font-size: 12px;">
				<option value="0">Any Customer</option>
				</select>
				<a href="javascript:getData('type=corp_customer&subType=oper&name=corp_customer_id&id=corp_customer_id&defaultValue=0&defaultName=Any Customer');"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Max Number of Cards</td>
			<td><input type="text" name="max_count" size="4" value="20" maxlength="5" /></td>
		</tr>
		<tr>
			<td valign="top">Card Number List<br/>(1 per line)</td>
			<td><textarea name="card_number_list" id="card_number_list" rows="5" style="width: 165px;"></textarea></td>			
		</tr>
		<tr>
			<td colspan="2" align="center" class="gridHeader">
		    	<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
		    	<input type="reset" class="cssButton" value="Reset" />
		    	<input type="submit" class="cssButton" name="action" value="Next &gt;" />
		   </td>
		</tr>
</table>
</form>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />