<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:useBean id="consumer_acct_fmt_id" class="java.lang.String" scope="request" />
<jsp:useBean id="customer_id" class="java.lang.String" scope="request" />
<jsp:useBean id="location_id" class="java.lang.String" scope="request" />
<jsp:useBean id="authority_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_name" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="terminal_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="formAction" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);%>

<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">USAT Card Creation Wizard</div>
</div>
<form method="post" action="newConsumerAcct3.i">
	<div class="gridHeader">Select optional parameters for actions</div>
	<table class="tabDataDisplayBorder" >
	
	<%
	String deviceTypes = ConvertUtils.getStringSafely(request.getAttribute("device_types"), "");
	if (deviceTypes != null && deviceTypes.length() > 0 ) {
	String[] deviceTypesArray = deviceTypes.split(",");
	for (int i=0; i < deviceTypesArray.length; i++) {
		int deviceTypeId = Integer.parseInt(deviceTypesArray[i]);
		int actionId = ConvertUtils.getInt(request.getAttribute(new StringBuilder("action_").append(deviceTypeId).toString()));		
	%>	
	<tr>
		<td colspan="2" class="gridHeader">
			<%=request.getAttribute(new StringBuilder("action_").append(deviceTypeId).append("_name_").append(actionId).toString())%>
			<input type="hidden" name="action_<%=deviceTypeId %>" value="<%=actionId %>" />
		</td>
	</tr>
	<tr><td colspan="2" align="left">Parameters:</td></tr>
	<%
	Results actionParams = DataLayerMgr.executeQuery("GET_ACTION_PARAM_BY_ACTION_ID", new Object[] {actionId});
	if (actionParams != null) {
	while(actionParams.next()) {
	%>
	<tr>
		<td ><input type="checkbox" name="action_<%=deviceTypeId %>_param" value="<%=actionParams.getFormattedValue("action_param_id")%>"></td>
		<td><%=actionParams.getFormattedValue("action_param_name")%></td>
	</tr>
	<% } } } } %>
	<tr>
		<td colspan="2" align="center" >
			<input type="submit" class="cssButton" name="action" value="Next &gt;" />
		</td>
	</tr>
	</table>
	
	<input type="hidden" name="consumer_acct_fmt_id" value="${consumer_acct_fmt_id}"  />
	<input type="hidden" name="customer_id" value="${customer_id}"  />
	<input type="hidden" name="corp_customer_id" value="${corp_customer_id}"  />
	<input type="hidden" name="currency_cd" value="${currency_cd}"  />
	<input type="hidden" name="location_id" value="${location_id}"  />
	<input type="hidden" name="authority_id" value="${authority_id}"  />
	<input type="hidden" name="merchant_id" value="${merchant_id}"  />
	<input type="hidden" name="merchant_name" value="${merchant_name}"  />
	<input type="hidden" name="merchant_desc" value="${merchant_desc}"  />
	<input type="hidden" name="terminal_desc" value="${terminal_desc}"  />
	<input type="hidden" name="device_types" value="<%=deviceTypes %>"  />
	<input type="hidden" name="consumer_acct_sub_type_id" value="<%=inputForm.getStringSafely("consumer_acct_sub_type_id", "")%>" />
		
	</form>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />