<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Results results = RequestUtils.getAttribute(request, "results", Results.class, true);
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">Card Configuration Wizard - Page 2: Card Selection Confirmation</div>
		</div>
		<form method="post" action="cardConfigWizard3.i" id="mainForm">
		<input type="hidden" name="consumer_acct_type_id" value="<%=inputForm.getStringSafely("consumer_acct_type_id", "0")%>" />
		<table class="tabDataDisplayBorder">
			<tr class="gridHeader">
				<td><input type="checkbox" onclick="checkAll(this)" checked="checked" /> Selected</td>
				<td>Account Type</td>
				<td>Account Subtype</td>
				<td>Reporting Customer</td>
				<td>Authorization Location</td>
				<td>Account ID</td>
				<td>Card Number</td>				
				<td>Card ID</td>
				<td>Balance</td>
				<td>Allow Negative Balance</td>				
				<td>Active</td>
				<td>Deactivation Date</td>
			</tr>
			
			<%
			int rows = 0;
			if (!results.next()) {
			%>
			<tr>
				<td colspan="11" align="center">No cards found</td>
			</tr>		
			<%
			} else {
			do {
				rows++;
			%>			
			<tr>
				<td><input type="checkbox" name="include_account_ids" value="<%=results.getFormattedValue("consumerAcctId")%>" checked="checked" /><%=rows%></td>				
				<td><%=results.getFormattedValue("consumerAcctTypeDesc")%></td>
				<td><%=results.getFormattedValue("consumerAcctSubTypeDesc")%></td>
				<td><%=results.getFormattedValue("corpCustomerName")%></td>
				<td><%=results.getFormattedValue("locationName")%></td>
				<td><a href="/editConsumerAcct.i?consumer_acct_id=<%=results.getFormattedValue("consumerAcctId")%>"><%=results.getFormattedValue("consumerAcctId")%></a></td>
				<td><%=results.getFormattedValue("consumerAcctCd")%></td>
				<td><%=results.getFormattedValue("consumerAcctIdentifier")%></td>
				<td><%=String.format("%.2f", results.getValue("consumerAcctBalance"))%> <%=results.getFormattedValue("currencyCd")%></td>
				<td align="center"><%="Y".equalsIgnoreCase(results.getFormattedValue("allowNegativeBalance")) ? "Yes" : "No"%></td>
				<td align="center"><%="Y".equalsIgnoreCase(results.getFormattedValue("consumerAcctActiveFlag")) ? "Yes" : "No"%></td>
				<td><%=results.getFormattedValue("consumerAcctDeactivationDate")%></td> 
			</tr>
			<%} while (results.next()); %>
					  
			<tr class="gridHeader">
				<td colspan="12" align="center">
			       <input type="button" class="cssButton" value="&lt; Back" onclick="javascript:history.go(-1);" />
			       <input type="button" class="cssButton" value="Cancel" onclick="javascript:window.location = '/home.i';" />
			       <input type="submit" class="cssButton" name="action" value="Next &gt;" onclick="return confirmSubmit()" /> 
				</td>
			</tr>			
			<%} %>
		</table>
		
		</form>
		</div>
<script type="text/javascript" defer="defer">
var mainForm = document.getElementById("mainForm");

function confirmSubmit() {	
	<%if (rows > 1) {%>
		var cardCount = 0;
		for (i=0; i<mainForm.include_account_ids.length; i++){
			if (mainForm.include_account_ids[i].checked)
				cardCount++;
		}
		if (cardCount == 0) {
			alert("Please select cards");
			return false;
		}
		return true;
	<%} else if (rows == 1) {%>
		if (mainForm.include_account_ids.checked)
			return true;
		else {
			alert("Please select cards");
			return false;
		}
	<%} else {%>
		return false;
	<%}%>
}

function checkAll(cb) {	
	<%if (rows > 1) {%>
		for (i=0; i<mainForm.include_account_ids.length; i++){
			mainForm.include_account_ids[i].checked = cb.checked;
		}
	<%} else if (rows == 1) {%>
		mainForm.include_account_ids.checked = cb.checked;
	<%}%>
}
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
