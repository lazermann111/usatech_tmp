<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:useBean id="dms_values_list_cardTypesList" class="com.usatech.dms.util.CardTypeList" scope="application" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>

		<div class="largeFormContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">USAT Card Creation Wizard</div>
		</div>
		<form method="post" action="newConsumerAcct1a.i" onsubmit="return validateForm(this);">
		<table class="tabDataDisplayBorder">
		<tbody>
			<tr>
			<td width="20%">Card Type</td>
			<td><select name="consumer_acct_fmt_id" tabindex="2" align="yes" style="font-family: courier; font-size: 12px;" delimiter=".">
				<c:forEach var="nvp_item_cardType" items="${dms_values_list_cardTypesList.list}">
						<option value="${nvp_item_cardType.value}">${nvp_item_cardType.name}</option>		    	
				</c:forEach>
			</select></td>
			</tr>
			<tr>
			
			<td>Customer</td>
			<td>
			<select id="customer_id" name="customer_id" tabindex="4" align="yes" style="font-family: courier; font-size: 12px;" delimiter="." usatRequired="true" label="Customer">
			<option value="">Please select:</option>
			</select>
			<input type="text" name="customer_search" id="customer_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'customer_search_link')" />
			<a id="customer_search_link" href="javascript:getData('type=customer&id=customer_id&name=customer_id&usatRequired=true&label=Customer&search=' + document.getElementById('customer_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
			</tr>
			<tr>
			<td>Authorization Location</td>
			<td>
				<select id="location_id" name="location_id" tabindex="6" align="yes" style="font-family: courier; font-size: 12px;" delimiter="." usatRequired="true" label="Location">
				<option value="">Please select:</option>
			    </select>
				<input type="text" name="location_search" id="location_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'location_search_link')" />
				<a id="location_search_link" href="javascript:getData('type=location&id=location_id&name=location_id&usatRequired=true&label=Authorization+Location&search=' + document.getElementById('location_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
			</tr>
			<tr>
			<td colspan="2" align="center" >
				<input align="middle" type="submit" class="cssButton" tabindex="7" name="action" value="Next &gt;" />
			</td>
			</tr>
		</tbody>
		</table>
		</form>
		</div>
	</c:otherwise>

</c:choose>


<jsp:include page="/jsp/include/footer.jsp" flush="true" />