<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="com.usatech.dms.util.Helper"%>

<%simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Card Configuration Wizard - Page 1: Search Criteria</div>
</div>
<form method="post" action="cardConfigWizard2.i" onsubmit="return validateCardIDs();">
<table class="tabDataDisplayBorder">
		<tr>
			<td width="20%">Reporting Customer</td>			
			<td>				
				<select name="corp_customer_id" id="corp_customer_id" style="font-family: courier; font-size: 12px;">
				<option value="0">Any Customer</option>
				</select>
				<input type="text" name="corp_customer_search" id="corp_customer_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("corp_customer_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'corp_customer_search_link')" />
				<a id="corp_customer_search_link" href="javascript:getData('type=corp_customer&subType=oper&id=corp_customer_id&name=corp_customer_id&defaultValue=0&defaultName=Any+Customer&search=' + document.getElementById('corp_customer_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Authorization Location</td>
			<td>						
				<select name="location_id" id="location_id" style="font-family: courier; font-size: 12px;">
				<option value="0">Any Location</option>
				</select>
				<input type="text" name="location_search" id="location_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'location_search_link')" />
				<a id="location_search_link" href="javascript:getData('type=location&id=location_id&name=location_id&defaultValue=0&defaultName=Any+Location&noDataValue=0&noDataName=Any+Location&search=' + document.getElementById('location_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td class="nowrap">Consumer Account Type&nbsp;</td>
			<td>						
				<select name="consumer_acct_type_id" id="consumer_acct_type_id" style="font-family: courier; font-size: 12px;">
					<option value="0">Any Consumer Account Type</option>
					<%Helper.valueListOptions(request, out, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_TYPES, null);%>
				</select>
			</td>
		</tr>
		<tr>
			<td class="nowrap">Consumer Account Subtype&nbsp;</td>
			<td>						
				<select name="consumer_acct_sub_type_id" id="consumer_acct_sub_type_id" style="font-family: courier; font-size: 12px;">
					<option value="0">Any Consumer Account Subtype</option>
					<%Helper.valueListOptions(request, out, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_SUB_TYPES, null);%>
				</select>
			</td>
		</tr>
		<tr>
			<td class="nowrap">Allow Negative Balance&nbsp;</td>
			<td>
				<select name="allow_negative_balance" id="allow_negative_balance">
					<option value="">Any</option>
					<option value="N">No</option>
					<option value="Y">Yes</option>
				</select>
				(Operator Service Prepaid accounts only)
			</td>
		</tr>
		<tr>
			<td>Card Range Start Card ID</td>
			<td><input type="text" name="start_card_id" id="start_card_id" maxlength="20" value="${start_card_id}"/></td>
		</tr>
		<tr>
			<td>Card Range End Card ID</td>
			<td><input type="text" name="end_card_id" id="end_card_id" maxlength="20" value="${end_card_id}"/></td>
		</tr>
		<tr>
			<td>Consumer Merchant</td>
			<td>
				<select name="consumer_merchant_id" id="consumer_merchant_id" style="font-family: courier; font-size: 12px;">
					<option value="0">Any Consumer Merchant</option>
				</select>
				<input type="text" name="consumer_merchant_search" id="consumer_merchant_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("consumer_merchant_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'consumer_merchant_search_link')" />
				<a id="consumer_merchant_search_link" href="javascript:getData('type=consumer_merchant&subType=oper&id=consumer_merchant_id&name=consumer_merchant_id&defaultValue=0&defaultName=Any+Consumer+Merchant&search=' + document.getElementById('consumer_merchant_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
		</tr>
		<tr>
			<td>Max Number of Cards</td>
			<td><input type="text" name="max_count" size="4" value="20" maxlength="5" /></td>
		</tr>
		<tr>
			<td valign="top">Card ID List<br/>(1 per line)</td>
			<td><textarea name="card_id_list" id="card_id_list" rows="5" style="width: 165px;"></textarea></td>			
		</tr>
		<tr>
			<td valign="top">Card Numbers List<br/>(1 per line)</td>
			<td><textarea name="card_num_list" id="card_num_list" rows="5" style="width: 165px;"></textarea></td>
		</tr>
		<tr>
			<td colspan="2" align="center" class="gridHeader">
		    	<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
		    	<input type="reset" class="cssButton" value="Reset" />
		    	<input type="submit" class="cssButton" name="action" value="Next &gt;" />
		   </td>
		</tr>
</table>
</form>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />