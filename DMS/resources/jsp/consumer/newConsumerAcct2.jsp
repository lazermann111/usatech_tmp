<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:useBean id="dms_values_list_authorityList" class="java.util.ArrayList" scope="request" />
<jsp:useBean id="merchantInfo" type="simple.results.Results" scope="request" />
<jsp:useBean id="consumer_acct_fmt_id" class="java.lang.String" scope="request" />
<jsp:useBean id="customer_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_exists" class="java.lang.String" scope="request" />
<jsp:useBean id="location_id" class="java.lang.String" scope="request" />
<jsp:useBean id="authority_id" class="java.lang.String" scope="request" />
<jsp:useBean id="formAction" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);%>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (not empty(errorMessage))}">
		<div class="tableContainer">
			<span class="error">Error Message: ${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>

		<div class="largeFormContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">USAT Card Creation Wizard</div>
		</div>
		<form method="post" action="${formAction}">
			<table class="tabDataDisplayBorder" >
			<c:choose>
				<c:when test="${merchant_exists eq 'true'}">
				<tbody>
					<tr>
					<td  align=center colspan=2>The following merchants already exist for this customer.  Please select an existing merchant.</td>
					</tr>
						<c:forEach var="nvp_item_merchant" items="${merchantInfo}">
							<tr>
							<td >
								<input type="radio" checked name="merchant_id" value="${nvp_item_merchant.merchant_id}" />
							</td>
							<td >${nvp_item_merchant.merchant_name}</td>	 
							</tr>   	
						</c:forEach>
				</tbody>
				</c:when>
				
				<c:otherwise>
					
					<jsp:useBean id="authorityInfo" type="simple.results.Results" scope="request" />
					<jsp:useBean id="customerInfo" type="simple.results.Results" scope="request" />
					<c:set var="merchant_name" value=""/>
					<c:set var="cust_name" value=""/>
					<c:set var="auth_name" value=""/>
					<c:set var="terminal_desc" value=""/>
					
					<c:set var="cust_name" value="${customerInfo[0].name}" />
					
					<c:set var="auth_name" value="${authorityInfo[0].authority_type_name}"/>
					
					<c:set var="merchant_name" value="${cust_name}${' - '}${auth_name}${' Merchant'}"/>
					<c:set var="terminal_desc" value="${cust_name}${' - '}${auth_name}${' Terminal'}"/>
										
					<tr>
					<td  align=center colspan=2><b>Please create a new merchant and terminal below</b></td>
					</tr>
					
					<tr>
					<td  >Merchant Name</td>
					<td  ><input type="text" name="merchant_name" tabindex="2" value="${merchant_name}" size="60" id="merchant_name" /></td>
					</tr>
					<tr>
					<td  >Merchant Description</td>	
					
					<td  ><input type="text" name="merchant_desc" tabindex="4" value="${merchant_name}" size="60" id="merchant_desc" /></td>
					</tr>
					<tr>
					<td  >Terminal Description</td>
					<td  ><input type="text" name="terminal_desc" tabindex="6" value="${terminal_desc}" size="60" id="terminal_desc" /></td>
					</tr>
				
				
				</c:otherwise>
			
			</c:choose>
				<tr>
					<td colspan="2" align="center" >
						<input align="middle" type="submit" class="cssButton" tabindex="7" name="action" value="Next &gt;" />
					</td>
				</tr>
				</table>
				
				<input type="hidden" name="consumer_acct_fmt_id" value="${consumer_acct_fmt_id}"  />
				<input type="hidden" name="customer_id" value="${customer_id}"  />
				<input type="hidden" name="corp_customer_id" value="${corp_customer_id}"  />
				<input type="hidden" name="currency_cd" value="${currency_cd}"  />
				<input type="hidden" name="location_id" value="${location_id}"  />
				<input type="hidden" name="authority_id" value="${authority_id}"  />
				<input type="hidden" name="consumer_acct_sub_type_id" value="<%=inputForm.getStringSafely("consumer_acct_sub_type_id", "")%>" />
				
			</form>
		</div>
	</c:otherwise>

</c:choose>


<jsp:include page="/jsp/include/footer.jsp" flush="true" />