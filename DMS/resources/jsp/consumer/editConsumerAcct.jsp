<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.util.LocationList"%>
<%@page import="com.usatech.dms.util.CardTypeList"%>
<%@page import="com.usatech.dms.model.CardType"%>
<%@page import="com.usatech.dms.model.ConsumerAccount"%>
<%@page import="com.usatech.dms.location.LocationConstants"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<jsp:useBean id="dms_values_list_cardTypesList" class="com.usatech.dms.util.CardTypeList" scope="application" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<jsp:useBean id="consumer_acct" scope="request" class="com.usatech.dms.model.ConsumerAccount" />  
<jsp:useBean id="cardType" scope="request" class="com.usatech.dms.model.CardType" />

<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMessage");
boolean isClosed=!StringUtils.isBlank(consumer_acct.getCloseDate());
if((missingParam != null && missingParam.booleanValue() == true) || errorMessage != null && errorMessage.trim().length() > 0 || errorMap.size()>0 ) {
	if(missingParam != null && missingParam.booleanValue() == true){
		%>
		<div class="tableContainer">
		<span class="error">Required parameter not found: consumer_acct_id</span>
		</div>
		<%	
	}else if (errorMessage != null && errorMessage.trim().length() > 0){
		%>
		<div class="tableContainer">
		<span class="error">Error Message: <%= errorMessage%></span>
		</div>
		<%	
	}else{
		%>
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
			<div class="error">${errorMessage }</div>
		</c:forEach>
		</div>
		<%
	}
} else {
	Results currencyResults = DataLayerMgr.executeQuery("GET_CURRENCY_CD", null);
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "consumer_acct");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.PARAM_CONSUMER_ACCT_ID), ""));
	simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String action = inputForm.getString(com.usatech.dms.util.DMSConstants.PARAM_ACTION_TYPE, false);
    /* reset the action for display on the page to show "Edit" */
    if(consumer_acct.getConsumerAcctFmtId() != null)    	
    	cardType = (CardType)dms_values_list_cardTypesList.getCardTypeByCardTypeId(consumer_acct.getConsumerAcctFmtId());
%>

<script>
  <!--
    function confirmSubmit()
    {
		  var message = "";
		  
		  <%if (consumer_acct.getConsumerAcctTypeId() != 1) {%>
			  var balance = document.getElementById("consumerAcctBalance").value;
			  if (isNaN(balance)) {
				  alert("Please enter a valid balance");
				  return false;
			  }
		  <%}%>
		  
		  <%if (consumer_acct.getConsumerAcctTypeId() == 3) {%>
			  	var oldBalance = ${consumer_acct.consumerAcctBalance};
			  	var oldReplenishTotal = ${consumer_acct.consumerAcctReplenishTotal};
			  	if (oldBalance >= 0 && balance < 0) {
			  		alert("New balance cannot be negative");
			  		return false;
			  	}
		  
		  		if (oldBalance != balance && balance > 2000) {
			  		alert("Balance cannot exceed 2000 ${consumer_acct.currencyCd}");
			  		return false;
			  	}
			
		  		var balanceChange = balance - oldBalance;
			  				  	  	
			  	if (balanceChange > 0)
			  		message = " The customer EFT will be charged for " + balanceChange.toFixed(2) + " ${consumer_acct.currencyCd}.";
			  	else if (balanceChange < 0)
			  		message = " The customer EFT will be credited for " + (-balanceChange.toFixed(2)) + " ${consumer_acct.currencyCd}.";
			  		
		  <%}%>
		  
	      return confirm("Please confirm your action!" + message);
    }
  // -->
  </script>

<c:set var="cardType" value="<%=cardType %>" />
<form name="editConsumerAcct" id="editConsumerAcct" method="post" action="editConsumerAcct.i">
<div class="tabDataContent">
<div class="innerTable">
<div class="tabHead" align="center" >Consumer Account</div>
<table class="tabDataDisplayBorder">
	<tbody>
		<tr>
			<td>Account ID</td>
			<td>${consumer_acct.consumerAcctId}</td>
			<td>Card Number</td>
			<td>${consumer_acct.consumerAcctCd}</td>
		</tr>
		<tr>
			<td>Card ID</td>
			<td>${consumer_acct.consumerAcctIdentifier}</td>
			<td>Currency</td>
			<td>
				<select name="currencyCd" style="font-family: courier; font-size: 12px;">
				<%while (currencyResults.next()) {%>
					<option <%if(consumer_acct.getCurrencyCd()!=null&&consumer_acct.getCurrencyCd().equals(currencyResults.getFormattedValue("currencyCd"))){ %> selected <%} %> value="<%=currencyResults.getFormattedValue("currencyCd")%>" title="<%=currencyResults.getFormattedValue("currencyName")%>" ><%=currencyResults.getFormattedValue("currencyCd")%></option>
				<%}%>
				</select>
			</td>
		</tr>
		<%if (consumer_acct.getConsumerAcctTypeId() > 0) {%>
		<tr>
			<td>Balance</td>
			<td>
				<%if (consumer_acct.getConsumerAcctTypeId() == 1) {%>
				N/A<input type="hidden" name="consumerAcctBalance" id="consumerAcctBalance" value="${consumer_acct.consumerAcctBalance}"/>
				<%} else {%>
				<input type="text" name="consumerAcctBalance" id="consumerAcctBalance" size="9" maxlength="9" value="${consumer_acct.consumerAcctBalance}"/> ${consumer_acct.currencyCd}
				<%}%>
				<input type="hidden" name="oldConsumerAcctBalance" id="oldConsumerAcctBalance" value="${consumer_acct.consumerAcctBalance}"/>
			</td>
			<td>Active</td>
			<td>
				<input type="hidden" name="consumerAcctActiveFlag" id="consumerAcctActiveFlag" value="${consumer_acct.consumerAcctActiveFlag}" />
				<c:choose>
					<c:when test="${consumer_acct.consumerAcctActiveFlag == 'Y'}">
						<span style="color: green;"><b>Yes</b></span>
					</c:when>
					<c:otherwise>
						<span style="color: red;"><b>No</b></span>
					</c:otherwise>
				</c:choose>
				<%if(!isClosed){ %>
				<input type="submit" name="action" value="Toggle" class="cssButton" onclick="return confirmSubmit()" />
				<%} %>
			</td>
		</tr>
		<tr>
			<td>Created Date</td>
			<td>${consumer_acct.createdDate}</td>
			<td>Last Updated Date</td>
			<td>${consumer_acct.lastUpdatedDate}</td>
		</tr>
		<tr>
			<td>Activation Date</td>
			<td>${consumer_acct.consumerAcctActivationDate}</td>
			<td>Deactivation Date</td>
			<td>
			    <input type="text" name="consumerAcctDeactivationDateMonth" id="consumerAcctDeactivationDateMonth" size="1" maxlength="2" value="${consumer_acct.consumerAcctDeactivationDate.substring(0,2)}"/>  
				/${consumer_acct.consumerAcctDeactivationDate.substring(3,5)}/
				<input type="text" name="consumerAcctDeactivationDateYear" id="consumerAcctDeactivationDateYear" size="1" maxlength="4" value="${consumer_acct.consumerAcctDeactivationDate.substring(6,10)}"/>
				${consumer_acct.consumerAcctDeactivationDate.substring(11,19)}
			</td>
		</tr>
		<tr>
			<td>Consumer ID</td>
			<td><a href="/editConsumer.i?consumer_id=${consumer_acct.consumerId}">${consumer_acct.consumerId}</a></td>
			<td>Consumer Account Type</td>
			<td>${consumer_acct.consumerAcctTypeDesc}</td>
		</tr>
		<tr>
			<td>Card Type</td>
			<td>${cardType.fmtName}</td>
			<td>Card Type Description</td>
			<td>${cardType.fmtDesc}</td>
		</tr>
		<tr>
			<td>Authorization Location</td>
			<td>
				<select name="locationId" id="locationId" style="font-family: courier; font-size: 12px;">
					<%if (consumer_acct.getLocationId() != null) {%>
						<option value="<%=consumer_acct.getLocationId()%>"><%=consumer_acct.getLocationName()%></option>
					<%}%>
					</select>
					<input type="text" name="location_search" id="location_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'location_search_link')" />
					<a id="location_search_link" href="javascript:getData('type=location&id=locationId&name=locationId&search=' + document.getElementById('location_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
			<% if(consumer_acct.getConsumerAcctTypeId() == 7) { %>
			<td>Top Up Amount</td>
			<td><input type="text" name="topUpAmt" id="topUpAmt" size="9" maxlength="9" value="${consumer_acct.topUpAmt}"/> ${consumer_acct.currencyCd}
			<% } %>
		</tr>
		<%if (consumer_acct.getConsumerAcctSubTypeId() != null) {%>
		<tr>
			<td>Consumer Account Subtype</td>
			<td<%if (consumer_acct.getConsumerAcctSubTypeId() != 2) {%> colspan="3"<%}%>>
				<select name="consumerAcctSubTypeId" id="consumerAcctSubTypeId" style="font-family: courier; font-size: 12px;">
					<%Helper.valueListOptions(request, out, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_SUB_TYPES, String.valueOf(consumer_acct.getConsumerAcctSubTypeId()));%>
				</select>
			</td>
			<%if (consumer_acct.getConsumerAcctSubTypeId() == 2) {%>
			<td>Allow Negative Balance</td>
			<td>
				<select name="allowNegativeBalance" id="allowNegativeBalance">
					<option value="N">No</option>
					<option value="Y"<%if ("Y".equalsIgnoreCase(consumer_acct.getAllowNegativeBalance())) {%> selected="selected"<%}%>>Yes</option>
				</select>
			</td>
			<%}%>
		</tr>
		<tr>
			<td>Reporting Customer</td>
			<td>
				<select name="corpCustomerId" id="corpCustomerId" style="font-family: courier; font-size: 12px;">
					<%if (consumer_acct.getCorpCustomerId() != null) {%>
						<option value="<%=consumer_acct.getCorpCustomerId()%>"><%=consumer_acct.getCorpCustomerName()%></option>
					<%}%>
			    </select>
			    <input type="text" name="corp_customer_search" id="corp_customer_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'corp_customer_search_link')" />
				<a id="corp_customer_search_link" href="javascript:getData('type=corp_customer&subType=oper&id=corpCustomerId&name=corpCustomerId&search=' + document.getElementById('corp_customer_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
			</td>
			<td>Loyalty Discount Total</td>
			<td>${consumer_acct.loyaltyDiscountTotal}</td>
		</tr>
		<tr>
			<td>Promo Balance</td>
			<td>${consumer_acct.consumerAcctPromoBalance} ${consumer_acct.currencyCd}</td>
			<td>Replenish Balance</td>
			<td>${consumer_acct.consumerAcctReplenishBalance} ${consumer_acct.currencyCd}</td>
		</tr>
		<tr>
			<td>Promo Total</td>
			<td>${consumer_acct.consumerAcctPromoTotal} ${consumer_acct.currencyCd}</td>
			<td>Replenish Total</td>
			<td>${consumer_acct.consumerAcctReplenishTotal} ${consumer_acct.currencyCd}</td>
		</tr>
		<tr>
			<td>Close Date</td>
			<td>${consumer_acct.closeDate}</td>
			<td>Purchase Discount Total</td>
			<td>${consumer_acct.purchaseDiscountTotal}</td>
		</tr>
		<%if (consumer_acct.getLoyaltyPointsTotal() != null) {%>
		<tr>
			<td>Loyalty Points Balance</td>
			<td>${consumer_acct.loyaltyPointsBalance}</td>
			<td>Loyalty Points Total</td>
			<td>${consumer_acct.loyaltyPointsTotal}</td>
		</tr>
		<%}%>
		<%}%>
		<tr class="gridHeader">
		<td align="center" colspan="4">
			<input type="hidden" id="consumerAcctId" name="consumer_acct_id" value="${consumer_acct.consumerAcctId}" />
			<input type="submit" name="action" class="cssButton" value="Update" onClick="return confirmSubmit()"/>
			<%if (consumer_acct.getConsumerAcctTypeId() == 3 || consumer_acct.getConsumerAcctTypeId() == 7) {%>
				<input type="submit" name="action" class="cssButton" value="<%if(isClosed){%>Reopen Account<%}else{%>Close Account<%}%>" onClick="return confirmSubmit()"/>	
			<%}%>
		</td>		
		</tr>
		<%}%>
	</tbody>
</table>
</div>
</div>
</form>

<div class="spacer5"></div>

<c:choose>
	<c:when test="${consumer_acct.consumerAcctFmtId == 1}">
		<jsp:include page="editConsumerAcctPermissionActions.jsp" flush="true" /> 
	</c:when>
</c:choose>	

<%if ("Y".equalsIgnoreCase(consumer_acct.getAuthHoldInd())) {%>
	<jsp:include page="editConsumerAcctHolds.jsp" flush="true" />
<%}%>

<div class="gridHeader">
	Transaction History From <input type="text" name="transaction_from_date" id="transaction_from_date" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute(DevicesConstants.PARAM_TRAN_FROM_DATE))%>" size="8" maxlength="10" >
    <img src="/images/calendar.gif" id="from_date_trigger" class="calendarIcon" title="Date selector" /> 
    <input type="text" size="6" maxlength="8" id="transaction_from_time" name="transaction_from_time" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute(DevicesConstants.PARAM_TRAN_FROM_TIME))%>" />
    To <input type="text" name="transaction_to_date" id="transaction_to_date" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute(DevicesConstants.PARAM_TRAN_TO_DATE))%>" size="8" maxlength="10">
    <img src="/images/calendar.gif" id="to_date_trigger" class="calendarIcon" title="Date selector" />
    <input type="text" size="6" maxlength="8" id="transaction_to_time" name="transaction_to_time" value="<%=StringUtils.encodeForHTMLAttribute(inputForm.getAttribute(DevicesConstants.PARAM_TRAN_TO_TIME))%>" />
    <input type="button" class="cssButton" value="List" onclick="handleListBtn();"> 
</div>
<div class="spacer2"></div>
	<% 
	Results tranList = (Results) inputForm.getAttribute("tranList");
	if(tranList != null) {
		String sortField = PaginationUtil.getSortField(null);
	    String sortIndex = inputForm.getString(sortField, false);
	    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "-2" : sortIndex;
	%>
<table class="tabDataDisplay">
	<tbody>
		<tr class="sortHeader">			
			<td><a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Tran ID</a> <%=PaginationUtil.getSortingIconHtml("1", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Time</a> <%=PaginationUtil.getSortingIconHtml("2", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">State</a> <%=PaginationUtil.getSortingIconHtml("3", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Type</a> <%=PaginationUtil.getSortingIconHtml("4", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Location</a> <%=PaginationUtil.getSortingIconHtml("5", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Amount</a> <%=PaginationUtil.getSortingIconHtml("6", sortIndex)%></td>
			<td><a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Discount</a> <%=PaginationUtil.getSortingIconHtml("7", sortIndex)%></td>
		</tr>
		<% 
		String tran_id;
		int i = 0; 
		String rowClass;
		while (tranList.next()) {
		    tran_id = tranList.getFormattedValue("TRAN_ID");
			rowClass = (i%2 == 0) ? "row1" : "row0";
			i++;
		%>
		<tr class="<%=rowClass%>">
			<td>
				<%if (StringUtils.isBlank(tran_id)) {%>
				<%=tranList.getFormattedValue("R_TRAN_ID") %>
				<%} else {%>
				<a href="tran.i?tran_id=<%= tran_id%>"><%=tran_id %></a>
				<%}%>
			</td>
			<td><%=tranList.getFormattedValue("CLOSE_DATE_FMT") %></td>
			<td><%=tranList.getFormattedValue("STATE_LABEL") %></td>
			<td><%=tranList.getFormattedValue("TRAN_TYPE") %></td>
			<td><%=tranList.getFormattedValue("LOCATION_NAME") %></td>
			<td><%=tranList.getFormattedValue("TOTAL_AMOUNT_FMT") %>&nbsp;</td>
			<td><%=tranList.getFormattedValue("DISCOUNT_FMT") %>&nbsp;</td>
		</tr>
		<%} %>
	</tbody>
	<%} %>
</table>
<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{
		DevicesConstants.PARAM_CONSUMER_ACCT_ID,DevicesConstants.PARAM_USER_OP,
		DevicesConstants.PARAM_TRAN_FROM_DATE,DevicesConstants.PARAM_TRAN_TO_DATE,
		DevicesConstants.PARAM_TRAN_FROM_TIME,DevicesConstants.PARAM_TRAN_TO_TIME});

%>

<jsp:include page="/jsp/include/pagination.jsp" flush="true">
   	<jsp:param name="_param_request_url" value="editConsumerAcct.i" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

<script type="text/javascript" defer="defer">
    Calendar.setup({
        inputField     :    "transaction_from_date",                  // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "transaction_to_date",                   // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });

    var fromDate = document.getElementById("transaction_from_date");
    var fromTime = document.getElementById("transaction_from_time");
    var toDate = document.getElementById("transaction_to_date");
    var toTime = document.getElementById("transaction_to_time");
    
    function validateDate() {
    	if(!isDate(fromDate.value)) {
        	fromDate.focus();
        	return false;
    	}
    	if(!isTime(fromTime.value)) {
        	fromTime.focus();
        	return false;
    	}
    	if(!isDate(toDate.value)) {
        	toDate.focus();
        	return false;
    	}
    	if(!isTime(toTime.value)) {
        	toTime.focus();
        	return false;
    	}
    	return true;
    }

    function handleListBtn() {
        if(validateDate()) {
        	return redirectWithParams('editConsumerAcct.i','consumer_acct_id=<%=consumer_acct.getConsumerAcctId()%>&userOP=list_tran', new Array(fromDate, fromTime, toDate, toTime));
        }
    }
</script> 

<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />