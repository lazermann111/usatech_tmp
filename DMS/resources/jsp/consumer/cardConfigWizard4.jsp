<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="com.usatech.dms.util.DMSValuesList"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.model.ConsumerAccount"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
int consumerAcctTypeId = inputForm.getInt("consumer_acct_type_id", false, 0);
long corpCustomerId = inputForm.getLong("corp_customer_id", false, 0);
String corpCustomerName = inputForm.getStringSafely("corp_customer_name", "");
if (StringUtils.isBlank(corpCustomerName))
	corpCustomerName = "Do not change";
long locationId = inputForm.getLong("location_id", false, 0);
String locationName = inputForm.getStringSafely("location_name", "");
if (StringUtils.isBlank(locationName))
	locationName = "Do not change";
String activationStatus = inputForm.getStringSafely("activation_status", "");
long consumerAcctSubTypeId = inputForm.getLong("consumer_acct_sub_type_id", false, 0);
String consumerAcctSubTypeName;
if (consumerAcctSubTypeId > 0) {
	DMSValuesList list = (DMSValuesList) request.getSession().getServletContext().getAttribute(DMSConstants.GLOBAL_LIST_CONTEXT_PARAM + DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_SUB_TYPES);
	consumerAcctSubTypeName = list.getName(String.valueOf(consumerAcctSubTypeId));
} else
	consumerAcctSubTypeName = "Do not change";
String balanceChange = inputForm.getStringSafely("balance_change", "0").trim();
if (balanceChange.length() == 0)
	balanceChange = "0";
if (consumerAcctTypeId == 3 && !balanceChange.matches("^-?[0-9]{1,3}(?:\\.[0-9]{1,2})?$") || !balanceChange.matches("^-?[0-9]{1,6}(?:\\.[0-9]{1,2})?$"))
	throw new ServletException("Invalid balance change amount: " + balanceChange);
String includeAccountIds = inputForm.getStringSafely("include_account_ids", "");
String allowNegativeBalance = inputForm.getStringSafely("allow_negative_balance", "");
String deactivationDate = inputForm.getStringSafely("consumer_acct_deactivation_date", "");
if ((!StringUtils.isBlank(deactivationDate))&&(!deactivationDate.matches ("^([0-9]{4})$")))
	throw new ServletException("Invalid deactivation date: " + deactivationDate);
if ((!StringUtils.isBlank(deactivationDate))&&((deactivationDate.charAt(2)>'1')||
		((deactivationDate.charAt(2)=='1')&&(deactivationDate.charAt(3)>'2'))||((deactivationDate.charAt(2)=='0')&&(deactivationDate.charAt(3)=='0'))))
	throw new ServletException("Invalid month in deactivation date: " + deactivationDate);
%>			
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Card Configuration Wizard - Page 4: Confirm Changes</div>
</div>

<% if (corpCustomerId < 1 && locationId < 1 && StringUtils.isBlank(activationStatus) && consumerAcctSubTypeId < 1 && balanceChange.equalsIgnoreCase("0") && StringUtils.isBlank(allowNegativeBalance) && StringUtils.isBlank(deactivationDate)) {%>
<br/><div align="center"><span class="status-error">No changes selected!</span></div><br/><br/>
<div class="gridHeader">
	<input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
	<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
</div>
<%} else { %>

<form method="post" action="cardConfigWizard5.i" onSubmit="return confirmChanges();">
<input type="hidden" name="include_account_ids" value="<%=includeAccountIds%>" />
<input type="hidden" name="corp_customer_id" value="<%=inputForm.getStringSafely("corp_customer_id", "0")%>" />
<input type="hidden" name="corp_customer_name" value="<%=inputForm.getStringSafely("corp_customer_name", "")%>" />
<input type="hidden" name="location_id" value="<%=inputForm.getStringSafely("location_id", "0")%>" />
<input type="hidden" name="location_name" value="<%=inputForm.getStringSafely("location_name", "")%>" />
<input type="hidden" name="activation_status" value="<%=inputForm.getStringSafely("activation_status", "")%>" />
<input type="hidden" name="consumer_acct_sub_type_id" value="<%=consumerAcctSubTypeId%>" />
<input type="hidden" name="consumer_acct_sub_type_name" value="<%=consumerAcctSubTypeName%>" />
<input type="hidden" name="balance_change" value="<%=balanceChange%>" />
<input type="hidden" name="allow_negative_balance" value="<%=StringUtils.prepareCDATA(allowNegativeBalance)%>" />
<input type="hidden" name="consumer_acct_deactivation_date" value="<%=StringUtils.prepareCDATA(deactivationDate)%>" /> 

<textarea id="changes" name="changes" style="height: 0px; visibility: hidden;"></textarea>

<div align="left"><b>&nbsp;Please confirm that you want to make the following changes!</b></div>
<br/>

<div id="changeSummary" style="margin: 5px;">
  	<table class="tabNormal">
   		<tr class="gridHeader">
   			<td>Change</td>
   			<td>New Value</td>
   		</tr>
   		<tr>
   			<td>Change Reporting Customer</td>
   			<td><%=corpCustomerName%></td>
   		</tr>
   		<tr>
   			<td>Change Authorization Location</td>
   			<td><%=locationName%></td>
   		</tr>
   		<tr>
   			<td>Change Activation Status</td>
   			<td><%=StringUtils.isBlank(activationStatus) ? "Do not change" : activationStatus%></td>
   		</tr>
   		<tr>
   			<td>Change Account Subtype</td>
   			<td><%=consumerAcctSubTypeName%></td>
   		</tr>
   		<tr>
   			<td>Increase/Decrease Balance by</td>
   			<td><%="0".equalsIgnoreCase(balanceChange) ? "Do not change" : String.format("%.2f", new BigDecimal(balanceChange))%></td>
   		</tr>
   		<tr>
   			<td>Allow Negative Balance</td>
   			<td><%=StringUtils.isBlank(allowNegativeBalance) ? "Do not change" : StringUtils.prepareCDATA(allowNegativeBalance)%></td>
   		</tr>
   		<tr>
   			<td>Change Deactivation Date (YYMM)</td>
   			<td><%=StringUtils.isBlank(deactivationDate) ? "Do not change" : deactivationDate%></td>
   		<tr>
	</table>
			
	<br/><br/>
	<b>Changes will be made for all of the following cards:</b>
	<br/><br/>
			
	<table class="tabNormal">
		<tr class="gridHeader">
			<td>Account Type</td>
			<td>Account Subtype</td>
			<td>Reporting Customer</td>
			<td>Authorization Location</td>
			<td>Account ID</td>
			<td>Card Number</td>				
			<td>Card ID</td>
			<td>Balance</td>
			<td>Allow Negative Balance</td>
			<td>Active</td>
			<td>Deactivation Date</td>
		</tr>
		<%
		Map<String, Object> params = new HashMap<String, Object>();
    	params.put("consumerAcctIds", includeAccountIds);
		Results results = DataLayerMgr.executeQuery("GET_BULK_CARD_LIST", params, false);
		while (results.next()) {			
			ConsumerAccount account = new ConsumerAccount();
			results.fillBean(account);
		%>
		<tr>				
			<td><%=results.getFormattedValue("consumerAcctTypeDesc")%></td>
			<td><%=results.getFormattedValue("consumerAcctSubTypeDesc")%></td>
			<td><%=results.getFormattedValue("corpCustomerName")%></td>
			<td><%=results.getFormattedValue("locationName")%></td>
			<td><a href="/editConsumerAcct.i?consumer_acct_id=<%=results.getFormattedValue("consumerAcctId")%>"><%=results.getFormattedValue("consumerAcctId")%></a></td>
			<td><%=results.getFormattedValue("consumerAcctCd")%></td>
			<td><%=results.getFormattedValue("consumerAcctIdentifier")%></td>
			<td><%=String.format("%.2f", results.getValue("consumerAcctBalance"))%> <%=results.getFormattedValue("currencyCd")%></td>
			<td align="center"><%="Y".equalsIgnoreCase(results.getFormattedValue("allowNegativeBalance")) ? "Yes" : "No"%></td>
			<td align="center"><%="Y".equalsIgnoreCase(results.getFormattedValue("consumerAcctActiveFlag")) ? "Yes" : "No"%></td>
			<td><%=results.getFormattedValue("consumerAcctDeactivationDate")%></td>
		</tr>
		<%}%>
	 </table> 
</div>
<br/>
<div class="gridHeader">
	<input type="button" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
	<input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
	<input type="submit" class="cssButton" name="action" value="Finish &gt;" />
</div>
</form>
<%} %>
</div>

<script type="text/javascript">
function confirmChanges() {
	if (confirm('Please click OK to continue and wait for your changes to be processed')) {
		document.getElementById('changes').value = document.getElementById('changeSummary').innerHTML;
		return true;
	} else
		return false;
}
</script>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
