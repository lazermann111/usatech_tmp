<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:useBean id="dms_values_list_authorityList" type="simple.results.Results" scope="request" />
<jsp:useBean id="consumer_acct_fmt_id" class="java.lang.String" scope="request" />
<jsp:useBean id="customer_id" class="java.lang.String" scope="request" />
<jsp:useBean id="location_id" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%int consumerAcctFmtId = RequestUtils.getAttribute(request, "consumer_acct_fmt_id", int.class, false); %>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>

		<div class="largeFormContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">USAT Card Creation Wizard</div>
		</div>
		<form method="post" action="newConsumerAcct2.i" onsubmit="return validateForm(this);">
			<table class="tabDataDisplayBorder" >
				<tr>
				<td nowrap>Card Processing Module</td>
				
				<td><select name="authority_id" tabindex="2" align="yes" style="font-family: courier; font-size: 12px;" delimiter=".">
					<c:forEach var="nvp_item_authority" items="${dms_values_list_authorityList}">
							<option value="${nvp_item_authority.aValue}">${nvp_item_authority.aLabel}</option>
					</c:forEach>	
				</select></td>
				
				</tr>
				<%
				if (consumerAcctFmtId == 2 || consumerAcctFmtId == 3) {
				%>
				<tr>
					<td nowrap>Reporting Customer</td>
					<td>
						<select id="corp_customer_id" name="corp_customer_id" style="font-family: courier; font-size: 12px;" usatRequired="true" label="Reporting Customer">
							<option value="">Please select:</option>
						</select>
						<input type="text" name="corp_customer_search" id="corp_customer_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'corp_customer_search_link')" />
						<a id="corp_customer_search_link" href="javascript:getData('type=corp_customer&subType=oper&id=corp_customer_id&name=corp_customer_id&usatRequired=true&label=Reporting+Customer&search=' + document.getElementById('corp_customer_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
					</td>
				</tr>
				<%
				}
				if (consumerAcctFmtId != 1) {
					Results currencies = DataLayerMgr.executeQuery("GET_CURRENCIES", null);
				%>
				<tr>
					<td nowrap>Currency</td>
					<td>
						<select name="currency_cd" style="font-family: courier; font-size: 12px;">
						<%while (currencies.next()) {%>
							<option value="<%=currencies.getFormattedValue("currency_code")%>"><%=currencies.getFormattedValue("currency_code")%> - <%=currencies.getFormattedValue("currency_name")%></option>
						<%}%>
						</select>
					</td>
				</tr>
				<%}%>
				<%
				Results results = DataLayerMgr.executeQuery("GET_CONSUMER_ACCT_SUB_TYPES_BY_CONSUMER_ACCT_FMT_ID", new Object[] {consumerAcctFmtId});
				if (results.next()) {
				%>
				<tr>
					<td nowrap>Card Subtype</td>
					<td>
						<select name="consumer_acct_sub_type_id" style="font-family: courier; font-size: 12px;">
						<%do {%>
							<option value="<%=results.getFormattedValue("consumer_acct_sub_type_id")%>"><%=results.getFormattedValue("consumer_acct_sub_type_desc")%></option>
						<%} while (results.next());%>
						</select>
					</td>
				</tr>
				<%}%>
				<tr>
					<td colspan="2" align="center" >
						<input align="middle" type="submit" class="cssButton" tabindex="3" name="action" value="Next &gt;" />
					</td>
				</tr>
				</table>
				
				<input type="hidden" name="consumer_acct_fmt_id" value="${consumer_acct_fmt_id}"  />
				<input type="hidden" name="customer_id" value="${customer_id}"  />
				<input type="hidden" name="location_id" value="${location_id}"  />
				
			</form>
		</div>
	</c:otherwise>

</c:choose>


<jsp:include page="/jsp/include/footer.jsp" flush="true" />