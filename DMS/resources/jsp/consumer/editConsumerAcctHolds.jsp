<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.model.ConsumerAccount"%>
<%@page import="com.usatech.dms.model.PermissionAction"%>
<%@page import="com.usatech.dms.model.PermissionActionParam"%>
<%@page import="com.usatech.dms.model.Action"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.layers.common.model.Location"%>
<%@page import="com.usatech.dms.location.LocationConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<jsp:useBean id="consumer_acct" scope="request" class="com.usatech.dms.model.ConsumerAccount" /> 
<jsp:useBean id="tempAcctHolds" scope="request" type="simple.results.Results" /> 

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMessage");
if((missingParam != null && missingParam.booleanValue() == true) || errorMessage != null && errorMessage.trim().length() > 0) {
	if(missingParam != null && missingParam.booleanValue() == true){
		%>
		<div class="tableContainer">
		<span class="error">Required parameter not found: location_id, location_name</span>
		</div>
		<%	
	}else{
		%>
		<div class="tableContainer">
		<span class="error">Error Message: <%= errorMessage%></span>
		</div>
		<%	
	}
} else {
	simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String action = inputForm.getString(com.usatech.dms.util.DMSConstants.PARAM_ACTION_TYPE, false);
    
%>
		<c:set var="holdList" value="" />
		<c:set var="showHoldButton" value="true" />
		<c:choose>
			<c:when test="${fn:length(tempAcctHolds) > 0}">
				<c:set var="holdList" value="${tempAcctHolds}" />
			</c:when>
		</c:choose>
		<c:choose>
			<c:when test="${fn:length(tempAcctHolds) <= 0}">
				<c:set var="showHoldButton" value="false" />
			</c:when>
		</c:choose>
		
		<form name="editConsumerAcctHolds" method="post" action="editConsumerAcct.i" >
		<div class="tabDataContent">
		<div class="innerTable">
		<div class="tabHead" align="center">
			Temporary Holds
			<c:choose>
				<c:when test="${showHoldButton}">
				<input type="submit" name="action" class="cssButton" value="Clear All Holds" onClick="return confirmSubmit()" />
				</c:when>
			</c:choose>
		</div>
		<input type="hidden" name="consumer_acct_id" value="${consumer_acct.consumerAcctId}" />
		<table class="tabDataDisplayBorderNoFixedLayout">
			<tr class="gridHeader nowrap">
				<td>Tran Detail ID</td>
				<td>Hold Amount</td>
				<td>Created Date</td>
				<td>Device</td>
				<td>Location</td>
				<c:choose>
					<c:when test="${showHoldButton}">
					<td><input type="submit" name="action" class="cssButton" value="Clear Holds" onClick="return confirmSubmit()" /></td>
					</c:when>
				</c:choose>
			</tr>
		<c:choose>
			<c:when test="${fn:length(holdList) > 0 }">
				<c:forEach var="nvp_item_hold" items="${holdList}">
					<tr>
						<td width="10" align="right">
						    <a href="/auth.i?auth_id=${nvp_item_hold.auth_id}">${nvp_item_hold.auth_id}</a>
						</td>
						<td>${nvp_item_hold.auth_amt}</td>
						<td>${nvp_item_hold.created_ts}</td>
						<td><a href="/profile.i?device_id=${nvp_item_hold.device_id}">${nvp_item_hold.device_serial_cd}</a></td>
						<td><a href="/editLocation.i?location_id=${nvp_item_hold.location_id}">${nvp_item_hold.location_name}</a></td>
						<c:choose>
							<c:when test="${showHoldButton}">
							<td align="center">
						    	<input type="checkbox" name="clear_hold" id="clear_hold" value="${nvp_item_hold.auth_id}">
							</td>
							</c:when>
						</c:choose>
					</tr>
			    </c:forEach>
			   </c:when>
		</c:choose>	
		</table>
		</div>
		</div>
		</form>
<% } %>
