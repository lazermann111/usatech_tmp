<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>

<%@page import="java.util.List"%>
<%@page import="com.usatech.dms.action.ConsumerAction"%>
<%@page import="com.usatech.dms.model.PermissionAction"%>
<%@page import="com.usatech.dms.model.PermissionActionParam"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<% 
long consumerAcctId = ConvertUtils.getLongSafely(request.getAttribute("consumer_acct_id"), -1);
List<PermissionAction> permissionActions = ConsumerAction.getConsumerAccountPermissionActionsByConsumerAccountId(consumerAcctId);
%>

<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">Change Card Actions - <%=consumerAcctId%></div>
</div>
<form method="post" action="editConsumerAcctActions3.i">
	<div class="gridHeader">Select optional parameters for actions</div>
	<table class="tabDataDisplayBorder" >
	
	<%
	String deviceTypes = ConvertUtils.getStringSafely(request.getAttribute("device_types"), "");
	if (deviceTypes != null && deviceTypes.length() > 0 ) {
	String[] deviceTypesArray = deviceTypes.split(",");
	for (int i=0; i < deviceTypesArray.length; i++) {
		int deviceTypeId = Integer.parseInt(deviceTypesArray[i]);
		int actionId = ConvertUtils.getInt(request.getAttribute(new StringBuilder("action_").append(deviceTypeId).toString()));
		List<PermissionActionParam> params = null;
		for (PermissionAction pa: permissionActions) {
			if (pa.getActionId() == actionId) {
				params = pa.getPermissionActionParams();
				break;
			}
		}
	%>	
	<tr>
		<td colspan="2" class="gridHeader">
			<%=request.getAttribute(new StringBuilder("action_").append(deviceTypeId).append("_name_").append(actionId).toString())%>
			<input type="hidden" name="action_<%=deviceTypeId %>" value="<%=actionId %>" />
		</td>
	</tr>
	<tr><td colspan="2" align="left">Parameters:</td></tr>
	<%
	Results actionParams = DataLayerMgr.executeQuery("GET_ACTION_PARAM_BY_ACTION_ID", new Object[] {actionId});
	if (actionParams != null) {
	while(actionParams.next()) {
		long actionParamId = ConvertUtils.getLong(actionParams.get("action_param_id"), -1);
		String checked = "";
		if (params != null) {
			for (PermissionActionParam param: params) {
				if (param.getActionParamId() == actionParamId) {
					checked = " checked";
					break;
				}
			}
		}
	%>
	<tr>
		<td ><input type="checkbox" name="action_<%=deviceTypeId %>_param" value="<%=actionParamId%>"<%=checked%> /></td>
		<td><%=actionParams.getFormattedValue("action_param_name")%></td>
	</tr>
	<% } } } } %>
	<tr>
		<td colspan="2" align="center" >
			<input type="submit" class="cssButton" name="action" value="<%=consumerAcctId > -1 ? "Finish" : "Next &gt;"%>" />
		</td>
	</tr>
	</table>
	
	<input type="hidden" name="device_types" value="<%=deviceTypes %>"  />
	<input type="hidden" name="consumer_acct_id" value="<%=consumerAcctId %>" />
		
	</form>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />