<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
	//get the parameters coming in from the ajax request
	
	Results consumerCardResults = (Results)request.getAttribute(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_CARD_RESULTS);
	String paramTotalCount = PaginationUtil.getTotalField(null);
	String recordCount = (String)request.getAttribute(paramTotalCount);
       
    //Pagination Info
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    int i = 0; 
    
    boolean resultsReturned = false;
    if((recordCount != null && (new Integer(recordCount)) > 0)){
    	resultsReturned = true;
    }
%>
<c:set var="resultsReturned"><%=resultsReturned%></c:set>

		<div class="spacer5"></div>
		<div class="tabHead" align="center">Search Results</div>
		<table class="tabDataDisplayBorder">
			<tr class="sortHeader">
				<td>
					<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Consumer ID</a>
					<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
				</td>
				<td>
					<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">First Name</a>
					<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
				</td>
				<td>
					<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Last Name</a>
					<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
				</td>
				<td>
					<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Email</a>
					<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
				</td>
			</tr>
				
				<c:choose>
					<c:when test="${resultsReturned == true}" >					
						
						<% 
						
						String rowClass = "row0";
						while(consumerCardResults.next()) {
							rowClass = (i%2 == 0) ? "row1" : "row0";
							i++;
						%>
							<tr class="<%=rowClass%>">
							
							    <td align="right">

							    <a href="#" onclick="return redirectWithParams('consumerSearch.i', 'action=Card Search&consumer_id=<%=consumerCardResults.getFormattedValue("id")%>'+buildSearchParamList(), null)"><%=consumerCardResults.getFormattedValue("id")%></a></td>
							    <td><%=consumerCardResults.getFormattedValue("fname")%></td>
							    <td><%=consumerCardResults.getFormattedValue("lname")%></td>
							    <td><%=consumerCardResults.getFormattedValue("emailAddress1")%></>
							</tr>
							<% } %>
						</c:when>
					<c:otherwise>
							<tr>
								<td align="center" colspan="6" width="100%">NO RECORDS</td>
							</tr>
					</c:otherwise>
				</c:choose>	    	
				
				
				
				</table>
						
							<%
		
							String storedNames = PaginationUtil.encodeStoredNames(new String[]{
									DMSConstants.PARAM_ACTION_TYPE,
									ConsumerConstants.PARAM_CONSUMER_ACCOUNT_NUMBER,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_CARD,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_FIRST_NAME_SEARCH_CHAR, 
									ConsumerConstants.PARAM_CONSUMER_CURRENT_FIRST_NAME, 
									ConsumerConstants.PARAM_CONSUMER_CURRENT_LAST_NAME,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_LAST_NAME_SEARCH_CHAR,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_PRIMARY_EMAIL,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_PRIMARY_EMAIL_SEARCH_CHAR,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_NOTIFICATION_EMAIL,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_NOTIFICATION_EMAIL_SEARCH_CHAR,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_ASSIGNED_LOCATION_ID,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_MERCHANT_ID,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_ASSIGNED_LOCATION_LOOKUP_CHARS,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ID,
									ConsumerConstants.PARAM_CONSUMER_START_CARD_ID,
									ConsumerConstants.PARAM_CONSUMER_END_CARD_ID,
									"consumer_acct_type_id", "consumer_acct_sub_type_id"
							});
							
							if(i>0){
							%>
						
							
							    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
							        <jsp:param name="_param_request_url" value="consumerSearch.i?action=Search" />
							    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
							    </jsp:include>	
						
						
							<% } %>
