<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.dms.util.ConsumerTypeList"%>
<%@page import="com.usatech.layers.common.model.Location"%>
<%@page import="com.usatech.dms.location.LocationConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<jsp:useBean id="consumer" scope="request" class="com.usatech.dms.model.Consumer" /> 
<jsp:useBean id="dms_values_list_countryList" class="com.usatech.dms.util.GenericList" scope="application" />
<jsp:useBean id="dms_values_list_stateList" class="com.usatech.dms.util.GenericList" scope="application" />
<jsp:useBean id="dms_values_list_consumerTypesList" class="com.usatech.dms.util.ConsumerTypeList" scope="application" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>



<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMessage");
if((missingParam != null && missingParam.booleanValue() == true) || errorMessage != null && errorMessage.trim().length() > 0) {
	if(missingParam != null && missingParam.booleanValue() == true){
		%>
		<div class="tableContainer">
		<span class="error">Required parameter not found: consumer_id</span>
		</div>
		<%	
	}else{
		%>
		<div class="tableContainer">
		<span class="error">Error Message: <%= errorMessage%></span>
		</div>
		<%	
	}
} else {
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "consumer");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.PARAM_CONSUMER_ID), ""));
	String consumer_state_cd = consumer.getStateCode();
	if(StringHelper.isBlank(consumer_state_cd)){
		consumer_state_cd = " ";
	}
	simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String action = inputForm.getString(com.usatech.dms.util.DMSConstants.PARAM_ACTION_TYPE, false);
    /* reset the action for display on the page to show "Edit" */
    
    List<NameValuePair> nvp_list_location_states = (List<NameValuePair>)dms_values_list_stateList.getList();
    List<NameValuePair> nvp_list_countries = (List<NameValuePair>)dms_values_list_countryList.getList();
    List<NameValuePair> nvp_list_consumer_types =  (List<NameValuePair>)dms_values_list_consumerTypesList.getList();
%>

<form name="editConsumer" id="editConsumer" method="POST"> 
<div class="tabDataContent">
<div class="innerTable">
<div class="tabHead" align="center" >Consumer</div>
<table class="tabDataDisplayBorder">
	<tbody>
		<tr>
			<td>First Name</td>
			<td><input type="text" name="fname" id="fname" size="50" maxlength="60" value="${consumer.fname}"/></td>
			<td>Middle Name</td>
			<td><input type="text" name="mname" id="mname" size="50" maxlength="60" value="${consumer.mname}"/></td>
		</tr>
		<tr>
			<td>Last Name</td>
			<td><input type="text" name="lname" id="lname" size="50" maxlength="60" value="${consumer.lname}"/></td>
			<td>Work Phone</td>
			<td><input type="text" name="phoneNumWork" id="phoneNumWork" size="50" maxlength="60" value="${consumer.phoneNumWork}"/></td>			
		</tr>
		<tr>
			<td>Cell Phone</td>
			<td><input type="text" name="phoneNumCell" id="phoneNumCell" size="50" maxlength="60" value="${consumer.phoneNumCell}"/></td>
			<td>Fax Number</td>
			<td><input type="text" name="phoneNumFax" id="phoneNumFax" size="50" maxlength="60" value="${consumer.phoneNumFax}"/></td>
		</tr>
		<tr>
			<td>Salutation</td>
			<td><input type="text" name="salutation" id="salutation;" size="50" maxlength="60" value="${consumer.salutation}"/></td>
			<td>Title</td>
			<td><input type="text" name="title" id="title" size="50" maxlength="60" value="${consumer.title}"/></td>
		</tr>
		<tr>
			<td>Email 1</td>
			<td><input <%if(consumer.getTypeId()==4){ %>disabled<%} %> type="text" name="emailAddress1" id="emailAddress1" size="50" maxlength="60" value="${consumer.emailAddress1}"/></td>
			<td>Email 2</td>
			<td><input type="text" name="emailAddress2" id="emailAddress2" size="50" maxlength="60" value="${consumer.emailAddress2}"/></td>
		</tr>
		<tr>
			<td>Pager Code</td>
			<td><input type="text" name="pagerCd" id="pagerCd" size="50" maxlength="60" value="${consumer.pagerCd}"/></td>
			<td>Type</td>
			<td>
			<select name="typeId" id="typeId">
				<c:forEach var="nvp_item_consumer_type_cd" items="<%=nvp_list_consumer_types%>">
					<c:choose>
						<c:when test="${nvp_item_consumer_type_cd.value == consumer.typeId}">
							<option value="${nvp_item_consumer_type_cd.value}" selected="selected">${nvp_item_consumer_type_cd.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_consumer_type_cd.value}">${nvp_item_consumer_type_cd.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
		    </td>
		</tr>
		<tr>
			<td>Address Line 1</td>
			<td><input type="text" name="address1" id="address1" size="50" maxlength="60" value="${consumer.address1}"/></td>
			<td>Address Line 2</td>
			<td><input type="text" name="address2" id="address2" size="50" maxlength="60" value="${consumer.address2}"/></td>
		</tr>
		<tr>
			<td>Postal Code</td>
			<td><input type="text" name="postalCode" id="postalCode" size="50" maxlength="10" value="${consumer.postalCode}" onblur="getPostalInfo('countryId=countryCode&stateId=stateCode&postalCd=' + this.value);" /></td>
			<td>City</td>
			<td><input type="text" name="city" id="city" size="50" maxlength="28" value="${consumer.city}"/></td>
		</tr>
		<tr>
			<td>State</td>
			<td>
			<div id=statecd>
			<select name="stateCode" id="stateCode">
			<option value="Undefined">Undefined</option>
					<c:forEach var="nvp_item_location_state" items="<%=nvp_list_location_states%>">
						<c:choose>
							<c:when test="${nvp_item_location_state.value == consumer.stateCode}">
								<option value="${nvp_item_location_state.value}" selected="selected">${nvp_item_location_state.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${nvp_item_location_state.value}">${nvp_item_location_state.name}</option>
							</c:otherwise>
						</c:choose>	    	
				    </c:forEach>
			    </select>
		    </div>
			</td>
			<td>County</td>
			<td><input type="text" name="county" id="county" size="50" maxlength="28" value="${consumer.county}"/></td>
		</tr>
		<tr>
			<td>Country</td>
			<%if (StringUtils.isBlank(consumer.getConsumerIdentifier())) {%>
			<td colspan="3">
			<%} else {%>
			<td>
			<%}%>
			<select name="countryCode" id="countryCode">
				<c:forEach var="nvp_item_location_country" items="<%=nvp_list_countries%>">
					<c:choose>
						<c:when test="${nvp_item_location_country.value == consumer.countryCode}">
							<option value="${nvp_item_location_country.value}" selected="selected">${nvp_item_location_country.name}</option>
						</c:when>
						<c:otherwise>
								<c:choose>
									<c:when test="${(nvp_item_location_country.value == 'Undefined') && (consumer.countryCode == null || consumer.countryCode == '') }">
										<option value="" selected="selected">${nvp_item_location_country.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${nvp_item_location_country.value}">${nvp_item_location_country.name}</option>
									</c:otherwise>
								</c:choose>	   
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
			</td>
			<%if (!StringUtils.isBlank(consumer.getConsumerIdentifier())) {%>
			<td>Consumer Identifier</td>
			<td>${consumer.consumerIdentifier}</td>
			<%}%>
		</tr>
		<tr>
			<td>Customer Assigned Id</td>
			<td><input type="text" name="customerAssignedId" id="customerAssignedId" size="50" maxlength="28" value="${consumer.customerAssignedId}"/></td>
			<td>Department Name</td>
			<td><input type="text" name="departmentName" id="departmentName" size="50" maxlength="28" value="${consumer.departmentName}"/></td>
		</tr>
		<%if (consumer.getIsisTranCount() > 0) {%>
		<tr>
			<td>Qualified Softcard Transactions</td>
			<td>${consumer.isisTranCount}</td>
			<td>Free Softcard Transactions</td>
			<td>${consumer.isisPromoTranCount}</td>
		</tr>
		<%}%>
		<tr class="gridHeader">
		<td align="center" colspan="4">			
			<input type="submit" name="action" class="cssButton" value="Update" />
			<input type="submit" name="action" class="cssButton" value="Delete" onclick="return confirmEditConsumerSubmit('Delete')" />
			<input type="button" name="action" class="cssButton" value="List Cards" onclick="window.location = '/consumerSearch.i?action=Card+Search&consumer_id=${consumer.id}';" />
		</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</form>
<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />