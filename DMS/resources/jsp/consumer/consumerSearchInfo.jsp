<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>
<%@page import="com.usatech.dms.model.Consumer"%>
<%@page import="com.usatech.dms.model.ConsumerAccount"%>
<%@page import="com.usatech.dms.model.Transaction"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="consumer" scope="request" class="com.usatech.dms.model.Consumer"/>

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%
	String paramTransTotalCount = PaginationUtil.getTotalField(null);
String transRecordCount = (String)request.getAttribute(paramTransTotalCount);
   
//Pagination Info
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String transSortField = PaginationUtil.getSortField(null);
String transSortIndex = inputForm.getString(transSortField, false);
transSortIndex = (transSortIndex == null || transSortIndex.trim().equals("")) ? "-2" : transSortIndex;

boolean transResultsReturned = false;
if((transRecordCount != null && (new Integer(transRecordCount)) > 0)){
	transResultsReturned = true;
}

List nvp_list_consumer_merchants = (java.util.ArrayList)Helper.buildSelectList("GET_ALL_CONSUMER_TYPES", false, null);
List<ConsumerAccount> accounts = (List<ConsumerAccount>)consumer.getAccounts();
int numAccounts = 0;
if(accounts!=null && accounts.size()>0)
	numAccounts = accounts.size();

//kludge to work around javascript passing different types in single vs. multiple checkbox cases ###
String jsFuncValue = null;
if(numAccounts > 1){
	jsFuncValue = "this.form.download_card";
}else{
	jsFuncValue = "new Array(this.form.download_card)";
}
%>
<c:set var="transResultsReturned"><%=transResultsReturned%></c:set>
<c:set var="numAccounts"><%=numAccounts%></c:set>
<c:set var="jsFuncValue"><%=jsFuncValue%></c:set>
		
		<div class="spacer5"></div>
		<form name="consumerInfo" id="consumerInfo">
		<div class="tabHead" align="center" >${consumer.fname} ${consumer.lname} (<a href="editConsumer.i?consumer_id=${consumer.id}">Edit</a>)</div>
		<table class="tabDataDisplayBorder">						
								
			<tbody>
			
			<tr>
				<td width="25%">Consumer ID</td>
				<td width="15%">${consumer.id}</td>
				<td width="20%">Created</td>
				<td width="40%" >${consumer.createdDate}</td>
			</tr>
			<tr>
				<td width="25%">Consumer Type</td>
				
				<c:forEach var="nvp_item_consumer_merchants" items="<%=nvp_list_consumer_merchants%>">
					<c:choose>
						<c:when test="${nvp_item_consumer_merchants.value == consumer.typeId}">
							<td width="15%">${nvp_item_consumer_merchants.name}</td>
							
						</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>	    	
			    </c:forEach>
			    
				<td width="20%">Last Updated</td>
				<td width="40%" >${consumer.lastUpdatedDate}</td>
			</tr>
			<tr>
				<td width="25%">First Name</td>
				<td width="15%">${consumer.fname}</td>
				<td width="20%">Last Name</td>
				<td width="40%" >${consumer.lname}</td>
			</tr>
			<tr>
				<td width="25%">Primary Email</td>
				<td colspan="3">${consumer.emailAddress1}</td>
			</tr>
			<tr>
				<td width="25%">Notification Email</td>
				<td colspan="3">${consumer.emailAddress2}</td>
			</tr>
			<tr>
				<td>Customer Assigned Id</td>
				<td>${consumer.customerAssignedId}</td>
				<td>Department Name</td>
				<td>${consumer.departmentName}</td>
			</tr>
			
			</tbody>

		</table>
		<div class="spacer5"></div>
		<div class="tabHead" align="center" >Consumer Accounts (Cards)</div>
		<table class="tabDataDisplayBorder" width="100%">
			<tr class="gridHeader">
				<td>Card Code</td>
				<td>Balance</td>
				<td>Location</td>
				<td>Card Type</td>
				<td>Card Subtype</td>
				<td>Active</td>
			</tr>
				
				<c:choose>
					<c:when test="${numAccounts > 0}" >
						<%
							int i = 0; 
										String rowClass = "row0";
										Iterator accountsIt = accounts.iterator();
										while(accountsIt.hasNext()) {
											ConsumerAccount account = (ConsumerAccount)accountsIt.next();
											rowClass = (i%2 == 0) ? "row1" : "row0";
											i++;
						%>
							<tr class="<%=rowClass%>">
							    <td><a href="editConsumerAcct.i?consumer_acct_id=<%=account.getConsumerAcctId()%>"><%=account.getConsumerAcctCd()%></a></td>
							    <td align="right">
							    <%=account.getConsumerAcctBalance()%></td>
							    <td>
							    <a href="editLocation.i?location_id=<%=account.getLocationId()%>"><%=account.getLocationName()%></a></td>
							    <td><%=account.getConsumerAcctFmtName() == null ? "&nbsp;" : account.getConsumerAcctFmtName()%></td>
							    <td><%=account.getConsumerAcctSubTypeDesc() == null ? "&nbsp;" : account.getConsumerAcctSubTypeDesc()%></td>
							    <%if(account.getConsumerAcctActiveFlag()!=null && account.getConsumerAcctActiveFlag().equalsIgnoreCase("Y")){%>
							    	<td><B><FONT color="green">Yes</FONT></B></td>
							    <%}else{ %>
							    	<td><B><FONT color="red">No</FONT></B></td>
							    <%} %>
							</tr>
						
						<% } %>
											
					</c:when>
					<c:otherwise>
						<tr>
							<td align="center" colspan="6" width="100%">NO ACCOUNTS</td>
						</tr>
					</c:otherwise>
				</c:choose>	    	


		</table>
		<div class="spacer5"></div>
		<div class="tabHead" align="center" >Consumer Transactions</div>
		<table class="tabDataDisplayBorder" width="100%">
					<tr class="sortHeader">
						<td>
							<a href="<%=PaginationUtil.getSortingScript(null, "1", transSortIndex)%>">Tran Id</a>
							<%=PaginationUtil.getSortingIconHtml("1", transSortIndex)%>
						</td>
						<td>
							<a href="<%=PaginationUtil.getSortingScript(null, "2", transSortIndex)%>">Auth Time</a>
							<%=PaginationUtil.getSortingIconHtml("2", transSortIndex)%>
						</td>
						<td>
							<a href="<%=PaginationUtil.getSortingScript(null, "3", transSortIndex)%>">State</a>
							<%=PaginationUtil.getSortingIconHtml("3", transSortIndex)%>
						</td>
						<td>
							<a href="<%=PaginationUtil.getSortingScript(null, "4", transSortIndex)%>">Consumer Account</a>
							<%=PaginationUtil.getSortingIconHtml("4", transSortIndex)%>
						</td>
						<td>
							<a href="<%=PaginationUtil.getSortingScript(null, "5", transSortIndex)%>">Serial Number</a>
							<%=PaginationUtil.getSortingIconHtml("5", transSortIndex)%>
						</td>
					</tr>
				
				<c:choose>
					<c:when test="${transResultsReturned == true}" >
						<% 
						Results transactionList = (Results) request.getAttribute("consumer_transactions");
						int i = 0; 
						String rowClass = "row0";
						while(transactionList.next()) {
							rowClass = (i%2 == 0) ? "row1" : "row0";
							i++;
						%>
							<tr class="<%=rowClass%>">
							    <td align="right">
							    
							    <a href="/tran.i?tran_id=<%=transactionList.getFormattedValue("id")%>"><%=transactionList.getFormattedValue("id")%></a></td>
							    <td><%=transactionList.getFormattedValue("startTime")%></td>
							    <td><%=transactionList.getFormattedValue("stateName")%></td>
							    <td align="right">
							    
							    <a href="/editConsumerAcct.i?consumer_acct_id=<%=transactionList.getFormattedValue("consumerAcctId")%>"><%=transactionList.getFormattedValue("consumerAcctCd")%></a></td>
							    <td align="right">
							    
							    <a href="/profile.i?device_id=<%=transactionList.getFormattedValue("deviceId")%>"><%=transactionList.getFormattedValue("deviceSerialCd")%></a></td>
							</tr>
						<% } %>
						
							<%
		
							String storedNames = PaginationUtil.encodeStoredNames(new String[]{
									"action",
									ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ID,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_CARD,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_FIRST_NAME_SEARCH_CHAR, 
									ConsumerConstants.PARAM_CONSUMER_CURRENT_FIRST_NAME, 
									ConsumerConstants.PARAM_CONSUMER_CURRENT_LAST_NAME,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_LAST_NAME_SEARCH_CHAR,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_PRIMARY_EMAIL,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_PRIMARY_EMAIL_SEARCH_CHAR,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_NOTIFICATION_EMAIL,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_NOTIFICATION_EMAIL_SEARCH_CHAR,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_ASSIGNED_LOCATION_ID,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_MERCHANT_ID,
									ConsumerConstants.PARAM_CONSUMER_CURRENT_ASSIGNED_LOCATION_LOOKUP_CHARS,
									"consumer_acct_type_id", "consumer_acct_sub_type_id"
							});
							
							%>
						
							
							    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
							        <jsp:param name="_param_request_url" value="consumerSearch.i?action=Card Search" />
							    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
							    </jsp:include>	
						
						
						
					</c:when>
					<c:otherwise>
						<tr>
							<td align="center" colspan="5" width="100%">NO RECORDS</td>
						</tr>
					</c:otherwise>
				</c:choose>	    	
												
		</table>
		</form>
		