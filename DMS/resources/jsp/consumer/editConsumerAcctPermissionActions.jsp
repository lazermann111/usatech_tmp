<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.model.ConsumerAccount"%>
<%@page import="com.usatech.dms.model.PermissionAction"%>
<%@page import="com.usatech.dms.model.PermissionActionParam"%>
<%@page import="com.usatech.dms.model.Action"%>
<%@page import="com.usatech.dms.location.LocationConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<jsp:useBean id="consumer_acct" scope="request" class="com.usatech.dms.model.ConsumerAccount" /> 

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMessage");
if(missingParam != null && missingParam.booleanValue() == true){
	%>
	<div class="tableContainer">
	<span class="error">Required parameters not found</span>
	</div>
	<%	
}else if(errorMessage != null && errorMessage.trim().length() > 0) {
	%>
	<div class="tableContainer">
	<span class="error">Error Message: <%= errorMessage%></span>
	</div>
	<%
} else {
	simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String action = inputForm.getString(com.usatech.dms.util.DMSConstants.PARAM_ACTION_TYPE, false);
%>

<form method="post" action="editConsumerAcctActions.i">
<input type="hidden" name="consumer_acct_id" value="${consumer_acct.consumerAcctId}"/>
<div class="tabDataContent">
<div class="innerTable">
<div class="tabHead" align="center">Maintenance Card Actions</div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<c:forEach var="nvp_item_permission_action" items="${consumer_acct.permissionActions}">
	<tr><td colspan="2" class="gridHeader">${nvp_item_permission_action.action.deviceTypeDesc}</td></tr>
	<tr>
		<td>${nvp_item_permission_action.action.actionName}</td>
		<td>${nvp_item_permission_action.action.actionDesc}</td>
	</tr>   
	<c:forEach var="nvp_item_permission_action_param" items="${nvp_item_permission_action.permissionActionParams}">
		<tr>					
			<td colspan="2" align="left">-&nbsp;${nvp_item_permission_action_param.actionParamName}</td>
		</tr>
	</c:forEach> 
   </c:forEach>
   	<tr class="gridHeader">
		<td align="center" colspan="2">
			<input type="submit" name="action" class="cssButton" value="Change Card Actions"/>
		</td>
	</tr>
</table>
</div>
</div>
</form>
<div class="spacer5"></div>
<% } %>
