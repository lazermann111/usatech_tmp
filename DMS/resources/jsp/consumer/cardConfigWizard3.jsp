<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String include_account_ids = inputForm.getStringSafely("include_account_ids", "");
if (StringUtils.isBlank(include_account_ids))
	throw new ServletException("No cards selected!");
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Card Configuration Wizard - Page 3: Card Changes</div>
</div>
<form method="post" action="cardConfigWizard4.i">
<input type="hidden" name="include_account_ids" value="<%=include_account_ids%>" />
<input type="hidden" name="corp_customer_name" id="corp_customer_name" value="" />
<input type="hidden" name="location_name" id="location_name" value="" />
<input type="hidden" name="consumer_acct_type_id" value="<%=inputForm.getStringSafely("consumer_acct_type_id", "0")%>" />
<table class="tabDataDisplayBorder">
	<tr>
		<td width="25%">Change Reporting Customer</td>
		<td>				
			<select name="corp_customer_id" id="corp_customer_id" style="font-family: courier; font-size: 12px;">
			<option value="0">Do not change</option>
			</select>
			<input type="text" name="corp_customer_search" id="corp_customer_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("corp_customer_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'corp_customer_search_link')" />
			<a id="corp_customer_search_link" href="javascript:getData('type=corp_customer&subType=oper&id=corp_customer_id&name=corp_customer_id&onChange=corp_customer_name&defaultValue=0&defaultName=Do+not+change&noDataValue=0&noDataName=Do+not+change&search=' + document.getElementById('corp_customer_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>
	<tr>
		<td>Change Authorization Location</td>
		<td>						
			<select name="location_id" id="location_id" style="font-family: courier; font-size: 12px;">
			<option value="0">Do not change</option>
			</select>
			<input type="text" name="location_search" id="location_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="" onkeypress="return clickLinkOnEnter(event, 'location_search_link')" />
			<a id="location_search_link" href="javascript:getData('type=location&id=location_id&name=location_id&onChange=location_name&defaultValue=0&defaultName=Do+not+change&noDataValue=0&noDataName=Do+not+change&search=' + document.getElementById('location_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
		</td>
	</tr>
	<tr>
		<td>Change Activation Status</td>
		<td>
			<select name="activation_status" id="activation_status" style="font-family: courier; font-size: 12px;">
				<option value="">Do not change</option>
				<option value="Activate">Activate</option>
				<option value="Deactivate">Deactivate</option>
				<option value="Close Account">Close Account</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Change Account Subtype</td>
		<td>				
			<select name="consumer_acct_sub_type_id" id="consumer_acct_sub_type_id" style="font-family: courier; font-size: 12px;">
				<option value="0">Do not change</option>
				<%Helper.valueListOptions(request, out, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_SUB_TYPES, null);%>
			</select>
		</td>
	</tr>
	<tr>
		<td class="nowrap">Increase/Decrease Balance by</td>
		<td><input type="text" name="balance_change" id="balance_change" maxlength="6" size="6" /></td>
	</tr>
	<tr>
		<td>Allow Negative Balance</td>
		<td>
			<select name="allow_negative_balance" id="allow_negative_balance">
				<option value="">Do not change</option>
				<option value="N">No</option>
				<option value="Y">Yes</option>
			</select>
			(Operator Service Prepaid accounts only)
		</td>
	</tr>
	<tr>
		<td class="nowrap">Change Deactivation Date</td>
		<td>
			<input type="text" name="consumer_acct_deactivation_date" id="consumer_acct_deactivation_date" maxlength="4" size="4" />
			YYMM	
		</td>
	</tr>
	<tr class="gridHeader">
		<td colspan="3" align="center">
		    <input type="reset" class="cssButton" value="&lt; Back" onClick="javascript:history.go(-1);" />
		    <input type=button class="cssButton" value="Cancel" onClick="javascript:window.location = '/home.i';" />
		    <input type="submit" class="cssButton" name="action" value="Next &gt;" />
		</td>
	</tr>
</table>
</form>
</div>

<script type="text/javascript">
function selectChange(item) {
	document.getElementById(item.id.replace(/_id$/, "_name")).value = item.options[item.selectedIndex].text;
}
</script>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
