<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String errorMessage = inputForm.getStringSafely("errorMessage", "");
if (!StringUtils.isBlank(errorMessage)) {
%>
				
<div class="tabDataContent" style="width: 980px;">
<table width="100%">
<tr>
<td align="center">
	${errorMessage}
</td>
</tr>
 <tr>
  <td align="center" class="gridHeader">
   <input type="button" value="&lt; Back" onClick="javascript:history.go(-1);" />
   <input type=button value="Cancel" onClick="javascript:window.location = '/home.i';" />
  </td>
 </tr>
</table>
</div>

<%} else {%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Card Configuration Wizard - Page 5: Processing Log</div>
</div>
<br />
<div align="center" >
	<input type=button class="cssButton" value="Start Over" onClick="javascript:window.location = '/cardConfigWizard1.i';"> &nbsp;
</div>

<pre>
<%out.print(inputForm.getStringSafely("bulk_out", "")); %>
</pre>

</div>

<%} %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />
