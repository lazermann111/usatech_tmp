<%@page import="simple.text.StringUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.results.Results"%>

<%@page import="java.util.List"%>
<%@page import="com.usatech.dms.action.ConsumerAction"%>
<%@page import="com.usatech.dms.model.PermissionAction"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<% 
long consumerAcctId = ConvertUtils.getLongSafely(request.getAttribute("consumer_acct_id"), -1);
List<PermissionAction> permissionActions = ConsumerAction.getConsumerAccountPermissionActionsByConsumerAccountId(consumerAcctId);
%>

<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">Change Card Actions - <%=consumerAcctId%></div>
</div>
<form method="post" action="editConsumerAcctActions2.i" onsubmit="return validateSpecific(this);">
	<div class="gridHeader">Maintenance Card Type</div>
	<table class="tabDataDisplayBorder" >			
	<%
	int previousDeviceTypeId = -1;
	StringBuilder deviceTypes = new StringBuilder();
	StringBuilder deviceTypeDescs = new StringBuilder();
    Results actionInfo = (Results) request.getAttribute("actionInfo");
	if (actionInfo != null) {	
	while(actionInfo.next()) {
		int deviceTypeId = ConvertUtils.getInt(actionInfo.get("device_type_id"));
		String deviceTypeDesc = actionInfo.getFormattedValue("device_type_desc");
		long actionId = ConvertUtils.getLong(actionInfo.get("action_id"));
		String actionName = actionInfo.getFormattedValue("action_name");
		String checked = "";
		for (PermissionAction pa: permissionActions) {
			if (pa.getActionId() == actionId) {
				checked = " checked";
				break;
			}
		}
		if (deviceTypeId != previousDeviceTypeId) {
			previousDeviceTypeId = deviceTypeId;
			if (deviceTypes.length() > 0) {
				deviceTypes.append(',');
				deviceTypeDescs.append(',');
			}
			deviceTypes.append(deviceTypeId);
			deviceTypeDescs.append('"').append(StringUtils.prepareScript(deviceTypeDesc)).append('"');
	%>
	<tr><td colspan="2" class="gridHeader"><%=deviceTypeDesc%></td></tr>
	<% } %>			
		<tr>
			<td><input type="radio"<%=checked%> name="action_<%=deviceTypeId %>" value="<%=actionId%>" /></td>
			<td>
			<input type="hidden" name="action_<%=deviceTypeId %>_name_<%=actionId %>" value="<%=deviceTypeDesc%>: <%=actionName %>" />
			<%=actionName%>
			<% if(actionInfo.get("action_desc") != null) { %>
			<br/><font color="gray">&nbsp;<%=actionInfo.getFormattedValue("action_desc")%></font>
			<% } %>
			</td>
		</tr>			    	
	<% } } %>			
	<tr>
		<td colspan="2" align="center" >
			<input align="middle" type="submit" class="cssButton" name="action" value="Next &gt;" />
		</td>
	</tr>
	</table>
		
	<input type="hidden" name="device_types" value="<%=deviceTypes.toString() %>" />
	<input type="hidden" name="consumer_acct_id" value="<%=consumerAcctId %>" />
	<script type="text/javascript">
	function validateSpecific(frm) {
		var labels = [<%=deviceTypeDescs %>];
		return [<%=deviceTypes.toString() %>].every(function(dt, index) {
			var elems = frm["action_" + dt];
			var value = getCheckedValue(elems) || "";
			if(value.length == 0) {
                alert("An action for " + labels[index] + " is required. Please select an option.");
                if(elems.length) {
                	elems[0].focus();
                	elems.each(function(el) {
                		el.addClass("invalidValue");
                	});
                } else {
	                elems.focus();
	                elems.addClass("invalidValue");
                }
                return false;
            }
			return true;
		});
	}</script>
	</form>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />