<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="card_gen_output" class="java.lang.String" scope="request" />

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">USAT Card Creation Wizard</div>
</div>
<c:out value="${card_gen_output}" escapeXml="false" ></c:out>
</div>
	
<jsp:include page="/jsp/include/footer.jsp" flush="true" />