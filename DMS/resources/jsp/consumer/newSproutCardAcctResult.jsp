<%@page import="java.sql.SQLException"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="com.usatech.ec2.EC2CardInfo"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%! private static final Log log = Log.getLog(); %>
<%
StringBuilder errorMessage=new StringBuilder();
StringBuilder successMessage=new StringBuilder();
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
int maxCount=inputForm.getInt("max_count", false, -1);
String cardNumberStr= inputForm.getString("card_number_list", false);
int corpCustomerId=inputForm.getInt("corp_customer_id", false, -1);
if(corpCustomerId<=0){
	errorMessage.append("Reporting Customer is not selected. Please retry.");
}else{
	Results result=DataLayerMgr.executeQuery("GET_SPROUT_VENDOR", new Object[]{corpCustomerId});
	int vendorId=-1;
	if(result.next()){
		vendorId=ConvertUtils.getInt(result.get("vendorId"));
	}
	if(vendorId<0){
		errorMessage.append("Reporting Customer's sprout vendor id is not found. Please set that up first.");
	}else if(StringUtils.isBlank(cardNumberStr)){
		errorMessage.append("Card Number List is not provided. Please retry.");
	}else{
		cardNumberStr=cardNumberStr.replace('\n', ',').replace("\r", "").replace(" ", "");
		long[] cardNumberArray=ConvertUtils.convert(long[].class,cardNumberStr);
	
		if(maxCount>0&&cardNumberArray.length>maxCount){
			errorMessage.append("The number of cards you input is more than the max number of cards.Please retry.").append(" Card count=").append(cardNumberArray.length).append(" Max =").append(maxCount);
		}else{
			String entryType = "N";
			String attributes = "consumerEmail=merchant_sprout@usatech.com\nsproutVendorId="+vendorId+"\n";
			
			for (long cardNumber: cardNumberArray){
				EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoPlain(null,null, "V1-"+corpCustomerId+"-USD", cardNumber+"|1111", entryType, attributes);
				if(infoResp == null) {
				    errorMessage.append("Failed to get sprout card info for cardNumber=").append(cardNumber).append("\n");
				    continue;
				}
				switch(infoResp.getReturnCode()) {
					case 1: //APPROVED
				    case 2: //APPROVED
				    	break;
				    case 3:
				    case 0:
				    	errorMessage.append("Declined for sprout for cardNumber=").append(cardNumber).append("\n");
				    	continue;
				    default:
				    	errorMessage.append("Failed to get sprout card info for cardNumber=").append(cardNumber).append("\n");
				    	continue;
				}
				
				long globalAccountId=infoResp.getCardId();
				String attributesResp=infoResp.getAttributes();
				long sproutAccountId=-1;
				if(attributesResp!=null){
					String[] attArray=attributesResp.split("=");
					if(attArray!=null&&attArray.length==2){
						sproutAccountId=ConvertUtils.getLongSafely(attArray[1], -1);
					}
				}
				log.info("Successfully get sproutAccountId="+sproutAccountId+" globalAccountId="+globalAccountId);
				
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("customerId",corpCustomerId);
				params.put("globalAccountId",globalAccountId);
				params.put("currencyCd","USD");
				params.put("consumerAcctIdentifier",sproutAccountId);
				params.put("consumerAcctEmailAddr1","merchant_sprout@usatech.com");
				try{
					DataLayerMgr.executeCall("INSERT_SPROUT_CONSUMER_ACCOUNT", params, params, true);
					successMessage.append("Successfully created sprout consumer acct for cardNumber=").append(cardNumber).append(" sproutAccountId=").append(sproutAccountId).append(" globalAccountId=").append(globalAccountId).append("\n");
				}catch(SQLException e){
					log.error("Failed to create sprout consumer acct for cardNumber="+cardNumber, e);
					if(e.getErrorCode()==1){
						errorMessage.append("Card already belongs to a consumer account. Failed to create sprout consumer account for cardNumber=").append(cardNumber).append(" sproutAccountId=").append(sproutAccountId).append(" globalAccountId=").append(globalAccountId).append("\n");
					}else{
						errorMessage.append("Failed to create sprout consumer account for cardNumber=").append(cardNumber).append(" sproutAccountId=").append(sproutAccountId).append(" globalAccountId=").append(globalAccountId).append("\n");
					}
					continue;
				}
				
			}
		}
	}
}
%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

		<div class="tableContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">Sprout Card Customer Association Result</div>
		</div>
		<%if(!StringUtils.isBlank(successMessage.toString())){ %>
			<p>
			<span class="status-info"> <%= StringUtils.prepareHTML(successMessage.toString()) %></span>
			</p>
		<%}%>
		<%if(!StringUtils.isBlank(errorMessage.toString())){ %>
			<p>
			<span class="status-error">Error Message: <br/> <%= StringUtils.prepareHTML(errorMessage.toString()) %></span>
			</p>
		<%}%>
		</div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
