<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.dms.util.Helper"%> 
<jsp:useBean id="consumer_card_number" scope="request" class="java.lang.String" /> 
<jsp:useBean id="missingParam" scope="request" class="java.lang.String" />  
<jsp:useBean id="errorMessage" scope="request" class="java.lang.String" />
<jsp:useBean id="action" scope="request" class="java.lang.String" />

<%simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
	
	<c:choose>
		<c:when test="${num_accounts eq null}"><c:set var="num_accounts" value="0" /></c:when>
	</c:choose>
		
		<div class="tableContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">Consumer Card Search</div>
		</div>
		<form name="searchConsumer" id="searchConsumer" method="get" action="consumerCardSearch.i" onsubmit="return validateCardIDs();">
		<table class="tabDataDisplay">
			<tbody>
				<tr>
					<td width="25%">Card Number</td>
					<td id="name"><input type="text" name="consumer_card_number" id="consumer_card_number" size="40" maxlength="65" value="${consumer_card_number}" autocomplete="off" />
					</td>
				</tr>
				<tr>
					<td>Account Number</td>
					<td><input type="text" name="account_number" id="account_number" size="40" maxlength="11" value="${account_number}"/></td>
				</tr>
				<tr>
					<td>Card Range Start Card ID</td>
					<td><input type="text" name="start_card_id" id="start_card_id" size="40" maxlength="20" value="${start_card_id}"/></td>
				</tr>
				<tr>
					<td>Card Range End Card ID</td>
					<td><input type="text" name="end_card_id" id="end_card_id" size="40" maxlength="20" value="${end_card_id}"/></td>
				</tr>
				<tr>
				<td>Consumer Merchant</td>
				<td>					
					<select name="consumer_merchant_id" id="consumer_merchant_id">
					<option value="-1">Undefined</option>
					<%
					String consumerMerchantId = ConvertUtils.getStringSafely(request.getParameter("consumer_merchant_id"), "").trim();
					if (consumerMerchantId.length() > 0 && !"-1".equalsIgnoreCase(consumerMerchantId)) {
					Results results = DataLayerMgr.executeQuery("GET_ALL_CONSUMER_MERCHANT_FORMATTED", null);
					while(results.next()) {
					%>
					<option value="<%=results.getValue("aValue")%>"<%if (consumerMerchantId.equals(results.getFormattedValue("aValue"))) out.write(" selected=\"selected\"");%>><%=results.getFormattedValue("aLabel")%></option>	    	
					<%}}%>
					</select>
					<input type="text" name="consumer_merchant_search" id="consumer_merchant_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("consumer_merchant_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'consumer_merchant_search_link')" />
					<a id="consumer_merchant_search_link" href="javascript:getData('type=consumer_merchant&subType=oper&id=consumer_merchant_id&name=consumer_merchant_id&defaultValue=-1&defaultName=Undefined&selected=<%=StringUtils.encodeForURL(consumerMerchantId)%>&search=' + document.getElementById('consumer_merchant_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
				</td>
				</tr>
				<tr>
					<td class="nowrap">Consumer Account Type&nbsp;</td>
					<td>						
						<select name="consumer_acct_type_id" id="consumer_acct_type_id">
							<option value="0">Any Consumer Account Type</option>
							<%Helper.valueListOptions(request, out, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_TYPES, ConvertUtils.getStringSafely(request.getParameter("consumer_acct_type_id"), ""));%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="nowrap">Consumer Account Subtype&nbsp;</td>
					<td>						
						<select name="consumer_acct_sub_type_id" id="consumer_acct_sub_type_id">
							<option value="0">Any Consumer Account Subtype</option>
							<%Helper.valueListOptions(request, out, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_SUB_TYPES, ConvertUtils.getStringSafely(request.getParameter("consumer_acct_sub_type_id"), ""));%>
						</select>
					</td>
				</tr>
				<tr class="gridHeader">
					<td colspan="3" align="center">
						<input type="submit" name="action" class="cssButton" value="Search" />
						<input type="hidden" name="myaction" value="card_search" />
					</td>	
				</tr>
			</tbody>
		</table>
		
		<%if (request.getParameter("action") == null || !request.getParameter("action").equals("Start Search")) {%>
			<jsp:include page="consumerAccounts.jsp" flush="true" />
		<%}%>
		
		</form>
		
		</div>
	
	</c:otherwise>
</c:choose>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />