<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@page import="simple.bean.ConvertUtils"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="consumer_acct_fmt_id" class="java.lang.String" scope="request" />
<jsp:useBean id="customer_id" class="java.lang.String" scope="request" />
<jsp:useBean id="location_id" class="java.lang.String" scope="request" />
<jsp:useBean id="authority_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_name" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="terminal_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="formAction" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);%>

<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">USAT Card Creation Wizard</div>
</div>
<form method="post" action="newConsumerAcct4.i">
	<%
	String deviceTypes = ConvertUtils.getStringSafely(request.getAttribute("device_types"), "");
	if (deviceTypes != null && deviceTypes.length() > 0 ) {
	String[] deviceTypesArray = deviceTypes.split(",");
	for (int i=0; i < deviceTypesArray.length; i++) {
		int deviceTypeId = Integer.parseInt(deviceTypesArray[i]);
		int actionId = ConvertUtils.getInt(request.getAttribute(new StringBuilder("action_").append(deviceTypeId).toString()));
		String actionParams = ConvertUtils.getStringSafely(request.getAttribute(new StringBuilder("action_").append(deviceTypeId).append("_param").toString()), "");
	%>	
	<input type="hidden" name="action_<%=deviceTypeId %>" value="<%=actionId %>" />
	<input type="hidden" name="action_<%=deviceTypeId %>_params" value="<%=actionParams %>" />
	<% } } %>
	<input type="hidden" name="device_types" value="<%=deviceTypes %>" />
	
	<table class="tabDataDisplayBorder" >
		
		<tr>
		<td  nowrap>Number of Cards to Generate</td>
		<td  ><input type="text" name="num_cards" tabindex="1"  size="4" maxlength="6" id="num_cards" /></td>
		</tr>
		<tr>
		<td  nowrap>Expiration Date (Leave blank for no expiration)</td>
		<td  ><input type="text" name="exp" tabindex="2"  size="4" maxlength="4" id="exp" /> YYMM</td>
		
		</tr>
		<c:choose>
			<c:when test="${consumer_acct_fmt_id != 1 && consumer_acct_fmt_id != 3}">
			<tr>
				<td  nowrap>Card Starting Balance</td>
				<td  ><input type="text" name="balance" tabindex="2" size="4" maxlength="9" id="balance" /> ${currency_cd}</td>
			</tr>
			</c:when>
		</c:choose>
		
		<tr>
		<td colspan="2" align="center" >
			<input align="middle" type="submit" class="cssButton" name="action" value="Next &gt;" />
		</td>
		</tr>
	</table>
	
	<input type="hidden" name="consumer_acct_fmt_id" value="${consumer_acct_fmt_id}"  />
	<input type="hidden" name="customer_id" value="${customer_id}"  />
	<input type="hidden" name="corp_customer_id" value="${corp_customer_id}"  />
	<input type="hidden" name="currency_cd" value="${currency_cd}"  />
	<input type="hidden" name="location_id" value="${location_id}"  />
	<input type="hidden" name="authority_id" value="${authority_id}"  />
	<input type="hidden" name="merchant_id" value="${merchant_id}"  />
	<input type="hidden" name="merchant_name" value="${merchant_name}"  />
	<input type="hidden" name="merchant_desc" value="${merchant_desc}"  />
	<input type="hidden" name="terminal_desc" value="${terminal_desc}"  />
	<input type="hidden" name="consumer_acct_sub_type_id" value="<%=inputForm.getStringSafely("consumer_acct_sub_type_id", "")%>" />
		
	</form>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />