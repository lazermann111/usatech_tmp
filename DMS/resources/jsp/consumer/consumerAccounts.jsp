<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<jsp:useBean id="consumer_accounts" scope="request" type="simple.results.Results" />
<jsp:useBean id="storedNames" scope="request" class="java.lang.String" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
	//Pagination Info
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
%>

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>
		<c:set var="sortIndex" value="${sortIndex}"/>
		<c:set var="rowClass" value="row0"/>
		<c:set var="i" value="0"/>
		<div class="gridHeader" align="center" >Consumer Accounts (Cards)</div>
		<div class="spacer2"></div>
		<table class="tabDataDisplayBorder" width="100%">			
				<tr class="sortHeader">
					<td>
						<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Account ID</a>
						<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
					</td>
					<td>
						<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Card Number</a>
						<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
					</td>
					<td>
						<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Card ID</a>
						<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
					</td>
					<td>
						<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Balance</a>
						<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
					</td>
					<td>
						<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Location</a>
						<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
					</td>
					<td>
						<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Account Type</a>
						<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
					</td>
					<td>
						<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">Account Subtype</a>
						<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
					</td>
					<td>
						<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">Active</a>
						<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
					</td>
					
				</tr>
				
			<c:choose>
				<c:when test="${fn:length(consumer_accounts) > 0}" >
					<c:forEach var="account" items="${consumer_accounts}">
						<c:choose>
							<c:when test="${i%2 == 0}">
								<c:set var="rowClass" value="row1"/>
							</c:when>
							<c:otherwise>
								<c:set var="rowClass" value="row0"/>
							</c:otherwise>
						</c:choose>
						<c:set var="i" value="${i+1}"/>
						<tr class="${rowClass}">
							<td><a href="editConsumerAcct.i?consumer_acct_id=${account.consumerAcctId}">${account.consumerAcctId}</a></td>
						    <td>${account.consumerAcctCd}</td>
						    <td>${account.consumerAcctIdentifier}</td>
						    <td><c:out value="${account.consumerAcctBalance}"/></td>
						    <td>
						    <a href="editLocation.i?location_id=${account.locationId}">${account.locationName}</a></td>
						    <td>${account.consumerAcctTypeDesc}</td>
						    <td>${account.consumerAcctSubTypeDesc}</td>
						    <c:choose>
								<c:when test="${(not empty(account.consumerAcctActiveFlag) and account.consumerAcctActiveFlag == 'Y')}">
									<td align="center"><B><FONT color="green">Yes</FONT></B></td>
							    </c:when>
							    <c:otherwise>
							    	<td align="center"><B><FONT color="red">No</FONT></B></td>
							    </c:otherwise>
						    </c:choose>						    
						</tr>
					</c:forEach>
					
					</c:when>
				<c:otherwise>
					<tr>
						<td align="center" colspan="8" width="100%">NO ACCOUNTS</td>
					</tr>
				</c:otherwise>
			</c:choose>	    	
			</table>
			
			<c:choose>
				<c:when test="${fn:length(consumer_accounts) > 0}" >
				    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
				        <jsp:param name="_param_request_url" value="consumerCardSearch.i?action=Search" />
				    	<jsp:param name="_param_stored_names" value="${storedNames}" />
				    </jsp:include>
				    
		    </c:when>
		    </c:choose>
		