<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@page import="simple.bean.ConvertUtils"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<% String consumerAcctId = ConvertUtils.getStringSafely(request.getAttribute("consumer_acct_id"), ""); %>

<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">Change Card Actions - <%=consumerAcctId%></div>
</div>
<%=request.getAttribute("action_output")%>
<div align="center">
<input type="button" class="cssButton" value="&lt; Back to Consumer Account" onclick="javascript:window.location='editConsumerAcct.i?consumer_acct_id=<%=consumerAcctId %>';"/>
</div>
<div class="spacer10"></div>
</div>
	
<jsp:include page="/jsp/include/footer.jsp" flush="true" />