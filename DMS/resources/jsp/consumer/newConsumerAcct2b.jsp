<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.results.Results"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:useBean id="consumer_acct_fmt_id" class="java.lang.String" scope="request" />
<jsp:useBean id="customer_id" class="java.lang.String" scope="request" />
<jsp:useBean id="location_id" class="java.lang.String" scope="request" />
<jsp:useBean id="authority_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_name" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="terminal_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="device_type_id" class="java.lang.String" scope="request" />
<jsp:useBean id="formAction" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);%>

<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">USAT Card Creation Wizard</div>
</div>
<form method="post" action="newConsumerAcct2c.i">
	<div class="gridHeader">Maintenance Card Type</div>
	<table class="tabDataDisplayBorder" >			
	<%
	int previousDeviceTypeId = -1;
	StringBuilder deviceTypes = new StringBuilder();
	Results actionInfo = (Results) request.getAttribute("actionInfo");
	if (actionInfo != null) {	
	while(actionInfo.next()) {
		int deviceTypeId = ConvertUtils.getInt(actionInfo.get("device_type_id"));
		String deviceTypeDesc = actionInfo.getFormattedValue("device_type_desc");
		String actionId = actionInfo.getFormattedValue("action_id");
		String actionName = actionInfo.getFormattedValue("action_name");
		boolean deviceTypeChange = false;
		if (deviceTypeId != previousDeviceTypeId) {
			previousDeviceTypeId = deviceTypeId;
			deviceTypeChange = true;
			if (deviceTypes.length() > 0) {
				deviceTypes.append(',');
			}
			deviceTypes.append(deviceTypeId);
	%>
	<tr><td colspan="2" class="gridHeader"><%=deviceTypeDesc%></td></tr>
	<% } %>			
		<tr>
			<td><input type="radio"<%=deviceTypeChange ? " checked" : ""%> name="action_<%=deviceTypeId %>" value="<%=actionId%>"></td>
			<td>
			<input type="hidden" name="action_<%=deviceTypeId %>_name_<%=actionId %>" value="<%=deviceTypeDesc%>: <%=actionName %>" />
			<%=actionName%>
			<% if(actionInfo.get("action_desc") != null) { %>
			<br/><font color="gray">&nbsp;<%=actionInfo.getFormattedValue("action_desc")%></font>
			<% } %>
			</td>
		</tr>			    	
	<% } } %>			
	<tr>
		<td colspan="2" align="center" >
			<input align="middle" type="submit" class="cssButton" name="action" value="Next &gt;" />
		</td>
	</tr>
	</table>
		
	<input type="hidden" name="consumer_acct_fmt_id" value="${consumer_acct_fmt_id}"  />
	<input type="hidden" name="customer_id" value="${customer_id}"  />
	<input type="hidden" name="corp_customer_id" value="${corp_customer_id}"  />
	<input type="hidden" name="currency_cd" value="${currency_cd}"  />
	<input type="hidden" name="location_id" value="${location_id}"  />
	<input type="hidden" name="authority_id" value="${authority_id}"  />
	<input type="hidden" name="merchant_id" value="${merchant_id}"  />
	<input type="hidden" name="merchant_name" value="${merchant_name}"  />
	<input type="hidden" name="merchant_desc" value="${merchant_desc}"  />
	<input type="hidden" name="device_type_id" value="${device_type_id}"  />
	<input type="hidden" name="terminal_desc" value="${terminal_desc}"  />
	<input type="hidden" name="device_types" value="<%=deviceTypes.toString() %>" />
	<input type="hidden" name="consumer_acct_sub_type_id" value="<%=inputForm.getStringSafely("consumer_acct_sub_type_id", "")%>" />
		
	</form>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />