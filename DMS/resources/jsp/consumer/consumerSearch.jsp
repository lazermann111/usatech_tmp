<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.layers.common.model.Location"%>
<%@page import="com.usatech.dms.consumer.ConsumerConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:useBean id="consumer_assigned_location_formatted" scope="request" class="java.util.ArrayList"/>
<jsp:useBean id="consumer_card_number" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_first_name_search_char" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_first_name" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_last_name_search_char" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_last_name" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_primary_email_search_char" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_primary_email" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_notification_email_search_char" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_notification_email" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_assigned_location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="location_id" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_assigned_location_lookup_chars" scope="request" class="java.lang.String"/>
<jsp:useBean id="consumer_merchant_id" scope="request" class="java.lang.String"/>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMessage");
if((missingParam != null && missingParam.booleanValue() == true) || errorMessage != null && errorMessage.trim().length() > 0) {
	if(missingParam != null && missingParam.booleanValue() == true){
		%>
		<div class="tableContainer">
		<span class="error">Required parameter not found: location_id, location_name</span>
		</div>
		<%	
	}else{
		%>
		<div class="tableContainer">
		<span class="error">Error Message: <%= errorMessage%></span>
		</div>
		<%
			}
		} else {
			simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
			
			String sortField = PaginationUtil.getSortField(null);
		    String sortIndex = inputForm.getString(sortField, false);
		    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
		    String paramTotalCount = PaginationUtil.getTotalField(null);
		    String recordCount = (String)request.getAttribute(paramTotalCount);
			
			List<NameValuePair> searchOptions = new ArrayList<NameValuePair>();
			searchOptions.add(new NameValuePair(new String("Contains"), new String("C")));
			searchOptions.add(new NameValuePair(new String("Begins"), new String("B")));
			searchOptions.add(new NameValuePair(new String("Ends"), new String("E")));
			
			consumer_last_name = java.net.URLEncoder.encode(consumer_last_name, "UTF-8");
			
		    String action = inputForm.getString(com.usatech.dms.util.DMSConstants.PARAM_ACTION_TYPE, false);
	        consumer_assigned_location_formatted = (java.util.ArrayList)inputForm.get(ConsumerConstants.STORED_NVP_CONSUMER_ASSIGNED_LOCATION_FORMATTED);
	        if(StringHelper.isBlank(consumer_assigned_location_id)){
	        	consumer_assigned_location_id  = location_id;
	        }
	        //if((consumer_assigned_location_lookup_chars != null && !consumer_assigned_location_lookup_chars.equals("")) && (consumer_assigned_location_formatted == null || consumer_assigned_location_formatted.size() <= 0)){
	        if((consumer_assigned_location_formatted == null || consumer_assigned_location_formatted.size() <= 0)){
		        if((!StringHelper.isBlank(consumer_assigned_location_id))){
		        	Object [] sqlParams = {consumer_assigned_location_id};
		        	consumer_assigned_location_formatted = (ArrayList<NameValuePair>)Helper.buildSelectList("GET_ASSIGNED_LOCATION_FORMATTED_BY_LOCATION_ID", true, sqlParams);
		        	inputForm.setAttribute(ConsumerConstants.STORED_NVP_CONSUMER_ASSIGNED_LOCATION_FORMATTED, consumer_assigned_location_formatted);
		        	
		        }else{
		        	if((!StringHelper.isBlank(consumer_assigned_location_lookup_chars))){
		        
			        	String tempChars = (!StringHelper.isBlank(consumer_assigned_location_lookup_chars)&&consumer_assigned_location_lookup_chars.contains("%")?consumer_assigned_location_lookup_chars:consumer_assigned_location_lookup_chars+"%");
			        	Object [] sqlParams = {tempChars,tempChars,tempChars,tempChars};
			        	consumer_assigned_location_formatted = (ArrayList<NameValuePair>)Helper.buildSelectList("GET_ASSIGNED_LOCATION_FORMATTED", true, sqlParams);
			        	inputForm.setAttribute(ConsumerConstants.STORED_NVP_CONSUMER_ASSIGNED_LOCATION_FORMATTED, consumer_assigned_location_formatted);
		        	}
		        }
	        }
		    Results consumerCardResults = (Results)request.getAttribute(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_CARD_RESULTS);
		    boolean executeSearch = false;
		    boolean executeCardSearch = false;
		    if((action != null && action.equalsIgnoreCase("Search"))){
		    	executeSearch = true;
		    }else if((action != null && action.equalsIgnoreCase("Card Search"))){
		    	executeCardSearch = true;
		    }
		    
		%>

<c:set var="executeSearch"><%=executeSearch%></c:set>
<c:set var="executeCardSearch"><%=executeCardSearch%></c:set>


<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Consumer Search</div>
</div>
<div class="innerTable">
<form name="searchConsumer" id="searchConsumer" method="get" action="consumerSearch.i">
<table class="tabDataDisplayBorder">
	<tbody>
		<tr>
			<td style="width:20%">Card Number</td>
			<td>&nbsp;</td>
			<td id="name"><input type="text" name="consumer_card_number" id="consumer_card_number" size="40" maxlength="65" value="${consumer_card_number}" autocomplete="off" />
			</td>
		</tr>
		<tr>
			<td>Account Number</td>
			<td>&nbsp;</td>
			<td><input type="text" name="account_number" id="account_number" size="40" maxlength="11" value="${account_number}"/></td>
		</tr>
		<tr>
			<td>Card Range Start Card ID</td>
			<td>&nbsp;</td>
			<td><input type="text" name="start_card_id" id="start_card_id" size="40" maxlength="20" value="${start_card_id}"/></td>
		</tr>
		<tr>
			<td>Card Range End Card ID</td>
			<td>&nbsp;</td>
			<td><input type="text" name="end_card_id" id="end_card_id" size="40" maxlength="20" value="${end_card_id}"/></td>
		</tr>
		<tr>
			<td>First Name</td>
			<td>
			<select name="consumer_first_name_search_char" id="consumer_first_name_search_char">
				<c:forEach var="nvp_item_search_option" items="<%=searchOptions%>">
					<c:choose>
						<c:when test="${nvp_item_search_option.value == consumer_first_name_search_char || nvp_item_search_option.value == 'C'}">
							<option value="${nvp_item_search_option.value}" selected="selected">${nvp_item_search_option.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_search_option.value}">${nvp_item_search_option.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
		    </td>
			<td><input type="text" name="consumer_first_name" id="consumer_first_name" size="40" maxlength="65" value="${consumer_first_name}"/>
			</td>
		</tr>
		<tr>
		<td>Last Name</td>
			<td>
			<select name="consumer_last_name_search_char" id="consumer_last_name_search_char">
				<c:forEach var="nvp_item_search_option" items="<%=searchOptions%>">
					<c:choose>
						<c:when test="${nvp_item_search_option.value == consumer_last_name_search_char || nvp_item_search_option.value == 'C'}">
							<option value="${nvp_item_search_option.value}" selected="selected">${nvp_item_search_option.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_search_option.value}">${nvp_item_search_option.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
		    </td>
			<td id="name"><input type="text" name="consumer_last_name" id="consumer_last_name" size="40" maxlength="65" value="<%=java.net.URLDecoder.decode(consumer_last_name, "UTF-8") %>"/>
			</td>
		</tr>
		<tr>
		<td>Primary Email</td>
			<td>
			<select name="consumer_primary_email_search_char" id="consumer_primary_email_search_char">
				<c:forEach var="nvp_item_search_option" items="<%=searchOptions%>">
					<c:choose>
						<c:when test="${nvp_item_search_option.value == consumer_primary_email_search_char || nvp_item_search_option.value == 'C'}">
							<option value="${nvp_item_search_option.value}" selected="selected">${nvp_item_search_option.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_search_option.value}">${nvp_item_search_option.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
		    </td>
			<td><input type="text" name="consumer_primary_email" id="consumer_primary_email" size="40" maxlength="65" value="${consumer_primary_email}"/>
			</td>
		</tr>
		<tr>
		<td>Notification Email</td>
			<td>
			<select name="consumer_notification_email_search_char" id="consumer_notification_email_search_char">
				<c:forEach var="nvp_item_search_option" items="<%=searchOptions%>">
					<c:choose>
						<c:when test="${nvp_item_search_option.value == consumer_notification_email_search_char || nvp_item_search_option.value == 'C'}">
							<option value="${nvp_item_search_option.value}" selected="selected">${nvp_item_search_option.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_search_option.value}">${nvp_item_search_option.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
		    </td>
			<td><input type="text" name="consumer_notification_email" id="consumer_notification_email" size="40" maxlength="65" value="${consumer_notification_email}"/>
			</td>
		</tr>
		<tr>
			
			
			<td>Assigned Location</td>
			<td colspan="2">
			<table class="noGrid">
				<tr>
					<td>
					<input type="text" name="consumer_assigned_location_lookup_chars" value="" id="consumer_assigned_location_lookup_chars" size="6" maxlength="5"/>
					<img src="/images/question.gif" title='Enter first couple of Characters of the Location name, then press "Select Location" for Assigned Location button.'>
					<div class="spacer5"></div>
					</td>
					<td colspan="2"><input type="button" id="assignedLocationLookup" name="assignedLocationLookup" class="cssButton" value="Select Location" style="width:130px;"
						onclick="javascript:if(validateAssignedChars()){return refreshAssignedLocationList()}"/>
					</td>

				</tr>
				<tr>
				<td colspan="2">
					<div id="assigned_location">
						<c:choose>
						<c:when
							test="${consumer_assigned_location_formatted ne null && fn:length(consumer_assigned_location_formatted) > 1}">
							<select name="consumer_assigned_location_id" id="consumer_assigned_location_id" style="font-family: courier; font-size: 12px;">
							<c:forEach var="nvp_item_assigned_location" items="${consumer_assigned_location_formatted}">
							<c:choose>
								<c:when
									test="${nvp_item_assigned_location.value == consumer_assigned_location_id || nvp_item_assigned_location.value == 'Undefined'}">
									<option value="${nvp_item_assigned_location.value}" selected="selected" label="${nvp_item_assigned_location.name}">${nvp_item_assigned_location.name}</option>
									
								</c:when>
								<c:otherwise>
									<option value="${nvp_item_assigned_location.value}" label="${nvp_item_assigned_location.name}">${nvp_item_assigned_location.name}</option>
									
								</c:otherwise>
							</c:choose>
							</c:forEach>
							</select>
						</c:when>
						<c:otherwise>
							<B><FONT color="green">Undefined</FONT></B>
						</c:otherwise>
						
					</c:choose>
					</div>
				</td>
				</tr>
				</table>
				</td>
				</tr>
				<tr>
				<td>Consumer Merchant</td>
				<td  colspan="2">
				<table class="noGrid">
					<tr>
						<td>
							<select name="consumer_merchant_id" id="consumer_merchant_id">
							<option value="-1">Undefined</option>
							<%
							String consumerMerchantId = ConvertUtils.getStringSafely(request.getParameter("consumer_merchant_id"), "").trim();
							if (consumerMerchantId.length() > 0 && !"-1".equalsIgnoreCase(consumerMerchantId)) {
							Results results = DataLayerMgr.executeQuery("GET_ALL_CONSUMER_MERCHANT_FORMATTED", null);
							while(results.next()) {
							%>
							<option value="<%=results.getValue("aValue")%>"<%if (consumerMerchantId.equals(results.getFormattedValue("aValue"))) out.write(" selected=\"selected\"");%>><%=results.getFormattedValue("aLabel")%></option>	    	
							<%}}%>
							</select>
							<input type="text" name="consumer_merchant_search" id="consumer_merchant_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("consumer_merchant_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'consumer_merchant_search_link')" />
							<a id="consumer_merchant_search_link" href="javascript:getData('type=consumer_merchant&id=consumer_merchant_id&name=consumer_merchant_id&defaultValue=-1&defaultName=Undefined&selected=<%=StringUtils.encodeForURL(consumerMerchantId)%>&search=' + document.getElementById('consumer_merchant_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
						</td>
					</tr>
				</table>
				
				</td>
				</tr>
				<tr>
					<td class="nowrap">Consumer Account Type&nbsp;</td>
					<td colspan="2">						
						<select name="consumer_acct_type_id" id="consumer_acct_type_id">
							<option value="0">Any Consumer Account Type</option>
							<%Helper.valueListOptions(request, out, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_TYPES, ConvertUtils.getStringSafely(request.getParameter("consumer_acct_type_id"), ""));%>
						</select>
					</td>
				</tr>
				<tr>
					<td class="nowrap">Consumer Account Subtype&nbsp;</td>
					<td colspan="2">						
						<select name="consumer_acct_sub_type_id" id="consumer_acct_sub_type_id">
							<option value="0">Any Consumer Account Subtype</option>
							<%Helper.valueListOptions(request, out, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_SUB_TYPES, ConvertUtils.getStringSafely(request.getParameter("consumer_acct_sub_type_id"), ""));%>
						</select>
					</td>
				</tr>
				<tr class="gridHeader">
				
				<td colspan="3" align="center">
				<c:choose>
					<c:when test="${executeCardSearch == true}" >
					
						<input type="button" onClick="history.go(-1);" id="backToSearchResults" name="backToSearchResults" class="cssButton" value="&lt; Back" style="width:90px;"/>
						
					</c:when>
				</c:choose>
				
					<input type="submit" name="action" class="cssButton" value="Search" style="width:90px;"
								onclick="return validateSearchParamList() && validateCardIDs();" />
				</td>	
				</tr>
	</tbody>
</table>



<c:choose>
	<c:when test="${executeSearch == true}" >
		<jsp:include page="consumerSearchResults.jsp" flush="true" /> 
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${executeCardSearch == true}" >
		<jsp:include page="consumerSearchInfo.jsp" flush="true" /> 
	</c:when>
</c:choose>

</form>	
</div>
</div>	
		
	
<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />