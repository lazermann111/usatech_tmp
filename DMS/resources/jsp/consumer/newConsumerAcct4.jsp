<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="customerInfo" type="simple.results.Results" scope="request" />
<jsp:useBean id="authorityInfo" type="simple.results.Results" scope="request" />
<jsp:useBean id="consumerCardType" type="simple.results.Results" scope="request" />
<jsp:useBean id="locationInfo" type="simple.results.Results" scope="request" />
<jsp:useBean id="consumer_acct_fmt_id" class="java.lang.String" scope="request" />
<jsp:useBean id="customer_id" class="java.lang.String" scope="request" />
<jsp:useBean id="location_id" class="java.lang.String" scope="request" />
<jsp:useBean id="authority_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_id" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_name" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="merchant_cd" class="java.lang.String" scope="request" />
<jsp:useBean id="terminal_id" class="java.lang.String" scope="request" />
<jsp:useBean id="terminal_desc" class="java.lang.String" scope="request" />
<jsp:useBean id="exp" class="java.lang.String" scope="request" />
<jsp:useBean id="balance" class="java.lang.String" scope="request" />
<jsp:useBean id="exp_date" class="java.lang.String" scope="request" />
<jsp:useBean id="exp_display" class="java.lang.String" scope="request" />
<jsp:useBean id="create_date" class="java.lang.String" scope="request" />
<jsp:useBean id="num_cards" class="java.lang.String" scope="request" />
<jsp:useBean id="formAction" class="java.lang.String" scope="request" />
<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
int consumer_acct_sub_type_id = inputForm.getInt("consumer_acct_sub_type_id", false, -1);
%>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	
	<c:otherwise>
		<c:set var="consumer_acct_fmt_name" value="" />
		<c:set var="customer_name" value="" />
		<c:set var="location_name" value="" />
		<c:set var="authority_name" value="" />
		<c:set var="authority_type_name" value="" />
		<c:set var="action_str" value="" />
		
		<div class="largeFormContainer">
		<div class="tableHead">
		<div class="tabHeadTxt">USAT Card Creation Wizard</div>
		</div>
		<form method="post" action="newConsumerAcct5.i">
		
		<table class="tabDataDisplayBorder" >
			<tr>
				<td colspan=2 class="gridHeader">Confirm Choices</td>
			</tr>
			<tr>
				<td>Card Type</td>
					
					<c:choose>
						<c:when test="${consumerCardType ne null}">
							<c:set var="consumer_acct_fmt_name" value="${consumerCardType[0].fmtName}" />
						</c:when>
					</c:choose>
				<td>${consumer_acct_fmt_name}</td>
			</tr>
			<%if (consumer_acct_sub_type_id > 0) {
			Results results = DataLayerMgr.executeQuery("GET_CONSUMER_ACCT_SUB_TYPE_INFO", new Object[] {consumer_acct_sub_type_id});
			if (results.next()) {
			%>
			<tr>
				<td>Card Subtype</td>
				<td><%=results.getFormattedValue("consumer_acct_sub_type_desc")%></td>
			</tr>
			<%}}%>
			<tr>
				<td>Customer</td>
					
					<c:choose>
						<c:when test="${customerInfo ne null}">
							<c:set var="customer_name" value="${customerInfo[0].name}" />
						</c:when>
					</c:choose>
				<td>${customer_name}</td>
			</tr>
			<%
			String corp_customer_name = "";
			Results corpCustomerInfo = RequestUtils.getAttribute(request, "corpCustomerInfo", Results.class, false);
			if (corpCustomerInfo != null && corpCustomerInfo.next()) {
				corp_customer_name = corpCustomerInfo.getFormattedValue("customer_name");
			%>
			<tr>
				<td>Reporting Customer</td>											
				<td><%=corp_customer_name%></td>
			</tr>
			<%} %>
			<tr>
				<td>Location</td>
					
					<c:choose>
						<c:when test="${locationInfo ne null}">
							<c:set var="location_name" value="${locationInfo[0].name}" />
						</c:when>
					</c:choose>
				<td>${location_name}</td>
			</tr>
			<tr>
				<td>Authority</td>
				
					<c:choose>
						<c:when test="${authorityInfo ne null}">
							<c:set var="authority_name" value="${authorityInfo[0].authority_name}" />
							<c:set var="authority_type_name" value="${authorityInfo[0].authority_type_name}" />
						</c:when>
					</c:choose>
				<td>${authority_name}</td>
			</tr>
			<tr>
				<td>Authority Type</td>
				<td>${authority_type_name}</td>
			</tr>
			<c:choose>
				<c:when test="${not empty(merchant_id)}">
					<!--  jsp:useBean id="merchantInfo" type="simple.results.Results" scope="request" /-->
						<tr>
						<td>Merchant ID</td>
						<td>${merchant_id }</td>
						</tr>
						<tr>
						<td>Merchant Name</td>
						<td>${merchant_name }</td>
						</tr>
						<tr>
						<td>Merchant Desc</td>
						<td>${merchant_desc }</td>
						
						</tr>
						<tr>
						<td>Merchant Code (Group Code)</td>
						<td>${merchant_cd } <input type="hidden" name="merchant_cd" value="${merchant_cd}"  /></td>						
						</tr>
						<tr>
						<td>Terminal ID</td>
						<td>${terminal_id }&nbsp;</td>
						</tr>
						<tr>
						<td>Terminal Desc</td>
						<td>${terminal_desc }&nbsp;</td>
						</tr>	
				</c:when>
				<c:otherwise>
						<tr>
						<td>Merchant Name</td>
						<td>${merchant_name }</td>
						</tr>
						<tr>
						<td>Merchant Desc</td>
						<td>${merchant_desc }</td>
						
						</tr>
						<tr>
						<td>Terminal Desc</td>
						<td>${terminal_desc }&nbsp;</td>
						</tr>
				
				</c:otherwise>
			</c:choose>
			<tr>
				<td>Expiration Date</td>
				<td>${exp}</td>
			</tr>
			<tr>
				<td>Expiration Display Date</td>
				<td>${exp_display}</td>
			</tr>
			<tr>
				<td>Deactivation Date</td>
				<td>${exp_date}</td>
			</tr>
			<tr>
				<td>Created Date</td>
				<td>${create_date}</td>
			</tr>
			<tr>
				<td>Number of Cards</td>
				<td>${num_cards}</td>
			</tr>
			
			<c:choose>
				<c:when test="${balance ne null and balance ne ''}">
					<tr>
						<td>Card Starting Balance</td>
						<td>${balance} ${currency_cd}</td>
					</tr>
				</c:when>
			</c:choose>
			
			<%
			String deviceTypes = ConvertUtils.getStringSafely(request.getAttribute("device_types"), "");
			if (deviceTypes != null && deviceTypes.length() > 0 ) {
			String[] deviceTypesArray = deviceTypes.split(",");
			for (int i=0; i < deviceTypesArray.length; i++) {
				int deviceTypeId = Integer.parseInt(deviceTypesArray[i]);
				int actionId = ConvertUtils.getInt(request.getAttribute(new StringBuilder("action_").append(deviceTypeId).toString()));				
				Results actionInfo = DataLayerMgr.executeQuery("GET_DEVICE_TYPE_ACTION_BY_ACTION_ID", new Object[] {actionId});
				if(actionInfo != null && actionInfo.next()) {
			%>					
					<tr class="gridHeader"><td align="left">Device Type</td><td align="left"><%=actionInfo.getFormattedValue("device_type_desc")%></td></tr>
					<tr>
						<td>Maintenance Card Action</td>
						<td>
							<%=actionInfo.getFormattedValue("action_name")%>
							<%
							String actionDesc = actionInfo.getFormattedValue("action_desc");
							if (actionDesc != null && actionDesc.length() > 0) {
							%>
							<br/><font color="gray"><%=actionDesc %></font>
							<% } %>
							</td>
					</tr>
					<tr>
						<td>Parameters</td>
						<td>
						<%
						String actionParams = ConvertUtils.getStringSafely(request.getAttribute(new StringBuilder("action_").append(deviceTypeId).append("_params").toString()), "");
						if (actionParams != null && actionParams.length() > 0) {
							String[] actionParamsArray = actionParams.split(",");
							for (int j=0; j < actionParamsArray.length; j++) {
								Results actionParamInfo = DataLayerMgr.executeQuery("GET_ACTION_PARAM_BY_ACTION_PARAM_ID", new Object[]{actionParamsArray[j]}, true);
								if (actionParamInfo != null && actionParamInfo.next()) {
									if (j > 0)
										out.write("<br />");
									out.write(actionParamInfo.getFormattedValue("action_param_name"));
								}				
							}
						} 
						%>
						&nbsp;
						<input type="hidden" name="action_<%=deviceTypeId %>" value="<%=actionId %>" />
						<input type="hidden" name="action_<%=deviceTypeId %>_params" value="<%=actionParams %>" />
						</td>
					</tr>
			<% } } } %>
			
			<tr>
			<td colspan="2" align="center" >
				<input align="middle" type="submit" class="cssButton" name="action" value="Finish" />
			</td>
			</tr>
		
		</table>
		
		<input type="hidden" name="consumer_acct_fmt_id" value="${consumer_acct_fmt_id}"  />
		<input type="hidden" name="consumer_acct_fmt_name" value="${consumer_acct_fmt_name}"  />
		<input type="hidden" name="customer_id" value="${customer_id}"  />
		<input type="hidden" name="corp_customer_id" value="${corp_customer_id}"  />
		<input type="hidden" name="currency_cd" value="${currency_cd}"  />
		<input type="hidden" name="customer_name" value="${customer_name}"  />
		<input type="hidden" name="corp_customer_name" value="<%=corp_customer_name%>"  />
		<input type="hidden" name="location_id" value="${location_id}"  />
		<input type="hidden" name="location_name" value="${location_name}"  />
		<input type="hidden" name="authority_id" value="${authority_id}"  />
		<input type="hidden" name="merchant_id" value="${merchant_id}"  />
		<input type="hidden" name="merchant_name" value="${merchant_name}"  />
		<input type="hidden" name="merchant_desc" value="${merchant_desc}"  />
		<input type="hidden" name="terminal_id" value="${terminal_id}"  />
		<input type="hidden" name="terminal_desc" value="${terminal_desc}"  />
		<input type="hidden" name="num_cards" value="${num_cards}"  />
		<input type="hidden" name="exp" value="${exp}"  />
		<input type="hidden" name="exp_display" value="${exp_display}"  />
		<input type="hidden" name="deactivation_date" value="${exp_date}"  />
		<input type="hidden" name="create_date" value="${create_date}"  />
		<input type="hidden" name="balance" value="${balance}"  />
		<input type="hidden" name="device_types" value="<%=deviceTypes %>" />
		<input type="hidden" name="consumer_acct_sub_type_id" value="<%=inputForm.getStringSafely("consumer_acct_sub_type_id", "")%>" />
		
		</form>
		</div>
	</c:otherwise>
</c:choose>
	
	
	
	


<jsp:include page="/jsp/include/footer.jsp" flush="true" />