<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="locationTreeOut" class="java.lang.String" scope="request" />

<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<style type="text/css">
	   .warn			{ font-size:10pt; color:#FF0000; font-weight:normal; font-family:'Courier New', monospace; }
	   .location_type	{ font-size:8pt;  color:#999999; font-weight:normal; font-family:'Courier New', monospace; }
	   .location_name	{ font-size:10pt; color:#000000; font-weight:bold; font-family:'Courier New', monospace; }
	   .device			{ font-size:10pt; color:#0000FF; font-weight:normal; font-family:'Courier New', monospace; }
	   .ssn				{ font-size:10pt; color:#0000FF; font-weight:normal; font-family:'Courier New', monospace; }
	   .machine			{ font-size:8pt;  color:#339999; font-weight:normal; font-family:'Courier New', monospace; }
	   .cards			{ font-size:8pt; background-color: #FFFF00; font-weight:normal; font-family:'Courier New', monospace; }
</style>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
	
	<div class="tableContainer">
	<div class="tableHead">
	<div class="tabHeadTxt">DMS Location Hierarchy</div>
	</div>
	<div class="innerTable">
	<table class="whiteBgColor" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
   	<tr>
    	<td align="center" colspan="3">						    
			<c:out value="${locationTreeOut}" escapeXml="false"/>
		</td>
	</tr>
	</table>
	</div>
	</div>
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />