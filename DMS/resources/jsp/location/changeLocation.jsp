<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="simple.servlet.SimpleServlet"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long device_id = inputForm.getLong("device_id", false, -1);
long location_id = inputForm.getLong("location_id", false, -1);
if (device_id <= 0 || location_id <= 0) {
%>
<div class="tableContainer">
<div align="center"><span class="error">Required Parameters Not Found: device_id, location_id</span></div>
</div>
<%} else {%>
<div class="largeFormContainer">
<div class="tableHead">
<div class="tabHeadTxt">Change DMS Location</div>
</div>

<div align="center">
<form method="post" action="changeLocation.i"><BR/><BR/>	
	<input type="hidden" name="device_id" value="<%=device_id %>"/>
	<input type="hidden" name="location_id" value="<%=location_id %>"/>
	<select id="new_location_id" name="new_location_id" style="font-family: courier; font-size: 12px;">
	<%Results locationNameInfo = (Results)inputForm.getAttribute("locationNameInfo");
	String label = "";
	while (locationNameInfo.next()) {
		if(locationNameInfo.getFormattedValue(2) != null && locationNameInfo.getFormattedValue(2).startsWith("Unknown")){
			label = "Undefined";
		}else{
			label = locationNameInfo.getFormattedValue(2);
		}
	
	%>
	   <option value="<%=locationNameInfo.getFormattedValue(1) %>" <%=(locationNameInfo.getValue(1, Integer.class).intValue() == location_id)?" selected":"" %>><%=label %></option>
	<%} %>
	</select>
	<input type="text" name="item_search" id="item_search" class="dataSearch" placeholder="Search" title="Enter search text to filter the dropdown values" value="<%=inputForm.getStringSafely("item_search", "")%>" onkeypress="return clickLinkOnEnter(event, 'item_search_link')" />
	<a id="item_search_link" href="javascript:getData('type=location&id=new_location_id&name=new_location_id&noDataValue=1&noDataName=Undefined&selected=<%=location_id%>&search=' + document.getElementById('item_search').value);"><img src="/images/list.png" title="List" class="list" /></a>
	<BR/><BR/><BR/>
	<input type="submit" class="cssButton" name="userOP" value="Change Location"><BR/><BR/><BR/>
</form>
</div>
</div>
<%} %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />