<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm,simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.dms.action.LocationActions"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.layers.common.model.Location"%>
<%@page import="com.usatech.dms.location.LocationConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="location" scope="request" class="com.usatech.layers.common.model.Location" /> 
<jsp:useBean id="action" scope="request" class="java.lang.String" />
<jsp:useBean id="dms_values_list_stateList" class="com.usatech.dms.util.GenericList" scope="application" /> 
<jsp:useBean id="dms_values_list_countryList" class="com.usatech.dms.util.GenericList" scope="application" /> 
<jsp:useBean id="dms_values_list_timeZoneList" class="com.usatech.dms.util.TimeZoneList" scope="application" />
<jsp:useBean id="dms_values_list_locationTypesList" class="com.usatech.dms.util.LocationTypeList" scope="application" />

<jsp:useBean id="parent_location_formatted" scope="request" class="java.util.ArrayList"/>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/location.js")%>"></script>

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
String errorMessage = (String) request.getAttribute("errorMessage");
if((missingParam != null && missingParam.booleanValue() == true) || errorMessage != null && errorMessage.trim().length() > 0) {
	if(missingParam != null && missingParam.booleanValue() == true){
		%>
		<div class="tableContainer">
		<span class="error">Required parameter not found: location_id, location_name</span>
		</div>
		<%	
	}else{
		%>
		<div class="tableContainer">
		<span class="error">Error Message: <%= errorMessage%></span>
		</div>
		<%	
	}
} else {
	simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "location");
	request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute(DevicesConstants.PARAM_LOCATION_ID), ""));
    
    /* reset the action for display on the page to show "Edit" */
    if(action != null && action.equals(LocationConstants.PARAM_ACTION_PARENT_LOCATION_LOOKUP)){
    	action = null;
    	action = DMSConstants.PARAM_ACTION_EDIT;
    }
    String saveButtonText = null;
    String parentLookupButtonText = null;
    String locationName = null;
    String newCountryCd = null;
    String newStateCd = null;
    String nameReadOnly = "";
    String locationExistsMessage = inputForm.getString("locationExistsMessage", false);
    if(StringHelper.isBlank(locationExistsMessage)){
    	locationExistsMessage = "";
    }
    String deviceCount = inputForm.getString("deviceCount", false);
    if(action != null && action.equals(DMSConstants.PARAM_ACTION_NEW)){
    	action = "New";
    	saveButtonText = "Save New";
    	locationName = inputForm.getString("name", false);
    	deviceCount = "0";
    	newCountryCd = "US";
    	newStateCd = "AL";
    	nameReadOnly = "readonly=\"1\"";
    }else{
    	action = "Edit";
    	saveButtonText = "Save";
    	
    	locationName = (location.getName()==null?"":location.getName());
    	if(StringHelper.isBlank(deviceCount)){
    		Integer dc = ((Integer)LocationActions.getDeviceCountByLocation(location.getId()));
    		if(dc!=null){
    			deviceCount = dc.toString();
    		}else{
    			deviceCount = "0";
    		}
    	}
    	                                    
    }
    if(location.getParentId() == null){
    	parentLookupButtonText = "Select Location";
    }else{
    	parentLookupButtonText = "Parent Location Lookup";
    }
    
    List<NameValuePair> nvp_list_location_states = (List<NameValuePair>)dms_values_list_stateList.getList();
    parent_location_formatted = (java.util.ArrayList)inputForm.get(LocationConstants.STORED_NVP_PARENT_LOCATION_FORMATTED);
    List<NameValuePair> nvp_list_location_countries =  (List<NameValuePair>)dms_values_list_countryList.getList();
    List<NameValuePair> nvp_list_location_timeZones =  (List<NameValuePair>)dms_values_list_timeZoneList.getList();
    List<NameValuePair> nvp_list_location_locationTypes = (List<NameValuePair>)dms_values_list_locationTypesList.getList();
    %>

<form name="editLocation" id="editLocation" method="post">
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt"><%=action %> DMS Location</div>
</div>
<%if (locationExistsMessage != null && locationExistsMessage.length() > 0) { %>
<div class="tabHead" align="center" ><%=locationExistsMessage %></div>
<%} %>
<div class="innerTable">
<div class="tabHead" align="center" ><%=StringUtils.prepareCDATA(locationName) %> (<%=StringUtils.prepareCDATA(deviceCount) %> Devices)</div>
<table class="tabDataDisplayBorder">
	<tbody>
		<tr>
			<td>Name</td>
			<td id="name"><input type="text" name="name" id="name" size="60" maxlength="60" value="<%=locationName %>" <%=nameReadOnly %>/>
			</td>
			<td>Type</td>
			<td>
			<select name="typeId" id="typeId">
				<c:forEach var="nvp_item_location_type" items="<%=nvp_list_location_locationTypes%>">
					<c:choose>
						<c:when test="${nvp_item_location_type.value == location.typeId}">
							<option value="${nvp_item_location_type.value}" selected="selected">${nvp_item_location_type.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_location_type.value}">${nvp_item_location_type.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
		    </td>
		</tr>
		<tr>
			<td>Address Line 1</td>
			<td id="address1"><input type="text" name="address1" id="address1" size="60" maxlength="60" value="${location.address1}"/></td>
			<td>Address Line 2</td>
			<td id="address2"><input type="text" name="address2" id="address2" size="60" maxlength="60" value="${location.address2}"/></td>
		</tr>
		<tr>
		<td>Postal Code</td>
		<td><input type="text" name="postalCode" id="postalCode" size="30" maxlength="10" value="${location.postalCode}" onblur="getPostalInfo('countryId=countryCode&stateId=stateCode&cityId=city&postalCd=' + this.value);" /></td>
		<td>City</td>
		<td><input type="text" name="city" id="city" size="30" maxlength="28" value="${location.city}"/></td>
		</tr>
		<tr>		
		<td>State</td>
		<td>
		<table class="noGrid">
		<tr><td>
		<div id=statecd>
		<c:set var="displayStateCd" >
			<c:choose>
				<c:when test="${action eq 'New'}" >
					<%=newStateCd %>
				</c:when>
				<c:otherwise>
					${location.stateCode}
				</c:otherwise>
			</c:choose>
		</c:set>
		<select name="stateCode" id="stateCode">
				<c:forEach var="nvp_item_location_state" items="<%=nvp_list_location_states%>">
					<c:choose>
						<c:when test="${nvp_item_location_state.value == displayStateCd}">
							<option value="${nvp_item_location_state.value}" selected="selected">${nvp_item_location_state.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_location_state.value}">${nvp_item_location_state.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
		    </div>
		    </td>
		    <td>&nbsp;<input type="button" class="cssButton" value="New State" onclick="return redirectWithParams('newState.i','action=New');"/></td>
		    </tr>
		    </table>
		</td>
			<td>County</td>
			<td id="county"><input type="text" name="county" id="county" size="60" maxlength="28" value="${location.county}"/></td>
		</tr>
		<tr>
			<td>Country</td>
			<td class="data" id="country">
			
			<c:set var="displayCountryCd" >
			<c:choose>
				<c:when test="${action eq 'New'}" >
					<%=newCountryCd %>
				</c:when>
				<c:otherwise>
					${location.countryCode}
				</c:otherwise>
				</c:choose>
			</c:set>
			<select name="countryCode" id="countryCode">
				<c:forEach var="nvp_item_location_country" items="<%=nvp_list_location_countries%>">
					<c:choose>
						<c:when test="${nvp_item_location_country.value == displayCountryCd}">
							<option value="${nvp_item_location_country.value}" selected="selected">${nvp_item_location_country.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_location_country.value}">${nvp_item_location_country.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
			</td>
			<td>Time Zone</td>
			<td class="data" id="timeZone">
			<c:set var="tz_value" >
				<c:choose>
							<c:when test="${fn:length(location.timeZoneCode) > 0}">${location.timeZoneCode}</c:when>
							<c:otherwise>EST</c:otherwise>
				</c:choose>
			</c:set>
			<select name="timeZoneCode" id="timeZoneCode">
				<c:forEach var="nvp_item_location_time_zone" items="<%=nvp_list_location_timeZones%>">
					<c:choose>
						<c:when
							test="${nvp_item_location_time_zone.value eq tz_value}">
							<option value="${nvp_item_location_time_zone.value}" selected="selected">${nvp_item_location_time_zone.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_location_time_zone.value}">${nvp_item_location_time_zone.name}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			</td>
		</tr>		
		<tr>
			
			
			<td>Parent Location</td>
			<td>
			<table class="noBorder">
				<tr>
					<td><input type="text" name="parent_location_lookup_chars" id="parent_location_lookup_chars" size="6" MAXLENGTH="5"/>&nbsp;&nbsp;<img src="/images/question.gif"  title='Enter first characters of the parent location name'></td>
					<td><input type="button" id="parentLocationLookup" name="parentLocationLookup" class="cssButton" value="<%=parentLookupButtonText %>"
						onclick="javascript:if(validateParentChars()){return refreshParentLocationList(<%=location.getId()%>)}"/>
					</td>
				</tr>
				<tr>
				<td>
					<div id="parent_location">
						<c:choose>
						<c:when
							test="${parent_location_formatted ne null && fn:length(parent_location_formatted) > 1}">
							<select name="parentId" id="parentId" style="font-family: courier; font-size: 12px;">
							<c:forEach var="nvp_item_parent_location" items="<%=parent_location_formatted%>">
							<c:choose>
								<c:when
									test="${nvp_item_parent_location.value ne 'Undefined' && nvp_item_parent_location.value == location.parentId}">
									<option value="${nvp_item_parent_location.value}" selected="selected">${nvp_item_parent_location.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${nvp_item_parent_location.value}">${nvp_item_parent_location.name}</option>
								</c:otherwise>
							</c:choose>
							</c:forEach>
							</select>
						</c:when>
							<c:otherwise>
								<B><FONT color="green">Undefined</FONT></B>
							</c:otherwise>
							
						</c:choose>
					</div>
				</td>
				<td>			
				<div id="edit_parent_button_div" 
				<%if(parent_location_formatted == null || parent_location_formatted.size()<=1) out.write("style='visibility:hidden'"); %> >
				<input type="button" id="editParentLocation" name="editParentLocation" class="cssButton" value="Edit Parent" onclick="javascript:if(validateParentLookupId()){return redirectWithParams('editLocation.i', 'location_id=' + document.editLocation.parentId[document.editLocation.parentId.selectedIndex].value, null)}"/>						
				</div>
				</td>
				</tr>
			</table>
			</td>
			<c:choose>
				<c:when test="${action == 'New'}">
				<td colspan="2">&nbsp;</td>
				</c:when>
				<c:otherwise>
					<td>Deleted</td>
					<td>
						<c:choose>
							<c:when
								test="${location.activeFlag == 'Y'}">No
							</c:when>
							<c:otherwise>Yes</c:otherwise>
						</c:choose>
					</td>
				</c:otherwise>
			</c:choose>
		</tr>		
		<tr class="gridHeader">
		<td align="center" colspan="4">
			<input type="hidden" id="action" name="action" value="<%=saveButtonText %>"/>
			
			<c:choose>
			<c:when test="${action == 'New'}">
				<input type="submit" id="saveLocation" name="saveLocation" class="cssButton" value="<%=saveButtonText %>"
				onclick="return validateSaveParams();"/>
			</c:when>
			<c:otherwise>
			<c:choose>
				<c:when test="${location.activeFlag == 'N'}">
					<input type="button" id="undeleteLocation" name="undeleteLocation" class="cssButton" value="Undelete"
					onclick="return redirectWithParams('editLocation.i', '&action=Undelete&location_id=<%=location.getId()%>', null)"/>
				</c:when>
				<c:otherwise>
					<input type="submit" id="saveLocation" name="saveLocation" class="cssButton" value="<%=saveButtonText %>"
						onclick="return validateSaveParams();"/>
						
					<input type="button" id="deleteLocation" name="deleteLocation" class="cssButton" value="Delete"
					onclick="return redirectWithParams('editLocation.i', '&action=Delete&location_id=<%=location.getId()%>', null)"/>
					
					<c:choose>
					<c:when test="${deviceCount > 0}">
						<input type="button" id="listDevices" name="listDevices" class="cssButton" value="List Devices"
							onclick="return redirectWithParams('devicelist.i', '&location_id=<%=location.getId() %>', null)"/>
					</c:when>
					<c:otherwise>
						<input type="button" id="listDevices" name="listDevices" class="cssButton" value="List Devices" disabled="disabled"
						onclick="return redirectWithParams('devicelist.i', '&location_id=<%=location.getId() %>', null)"/>
					</c:otherwise>
					</c:choose>
					
					<input type="button" id="listChildLocations" name="listChildLocations" class="cssButton" value="List Child Locations"
					onclick="return redirectWithParams('locationList.i', '&parentId=<%=location.getId() %>', null)"/>
					
					
					<input type="button" id="viewHierarchy" name="viewHierarchy" class="cssButton" value="View Hierarchy"
					onclick="return redirectWithParams('locationTree.i', '&source_id=<%=location.getId() %>', null)"/>
				</c:otherwise>
		</c:choose>
		</c:otherwise>
		</c:choose>
		</td>
		
		</tr>
		
	</tbody>
</table>
</div>
</div>
</form>
<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />