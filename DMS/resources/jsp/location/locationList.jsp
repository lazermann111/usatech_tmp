<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.location.LocationConstants"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
%>
<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">DMS Location List</div>
</div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Name</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">City</a>
				<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Country</a>
				<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Type</a>
				<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
			</td>
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Time Zone</a>
				<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
			</td>
		</tr>
	</thead>
	<tbody>
	<% 
	Results locationList = (Results) request.getAttribute("locationList");
	int i = 0; 
	String rowClass = "row0";
	while(locationList.next()) {
		rowClass = (i%2 == 0) ? "row1" : "row0";
		i++;
	%>
		<tr class="<%=rowClass%>">
		    <td>
		    
		    <a href="editLocation.i?location_id=<%=locationList.getFormattedValue("location_id")%>"><%=locationList.getFormattedValue("location_name")%></a></td>
		    <td><%=locationList.getFormattedValue("location_city")%></td>
		    <td><%=locationList.getFormattedValue("country_name")%></td>
		    <td><%=locationList.getFormattedValue("location_type_desc")%></td>
		    <td><%=locationList.getFormattedValue("time_zone_name")%></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%

String storedNames = PaginationUtil.encodeStoredNames(new String[]{LocationConstants.PARAM_LOCATION_NAME,
		LocationConstants.PARAM_LOCATION_CITY, LocationConstants.PARAM_LOCATION_COUNTRY_NAME,
		LocationConstants.PARAM_LOCATION_TYPE_DESC, LocationConstants.PARAM_LOCATION_TIME_ZONE_NAME,
		LocationConstants.PARAM_ENABLED, LocationConstants.PARAM_LOCATION_TYPE_ID});

%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="locationlist.i" />
    	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />