<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="tableDataContainer" style="float:none; border: 0;">
<p class="error">
<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String message = inputForm.getString("notFoundMsg", false);
if (message != null && message.trim().length() > 0) {
    out.println(message);
} else {
    out.println("No DMS Location found for this search");
}
%>
</p>
</div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />