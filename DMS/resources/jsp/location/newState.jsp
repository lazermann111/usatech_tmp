<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.DMSConstants"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.util.NameValuePair"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.model.Location"%>
<%@page import="com.usatech.dms.location.LocationConstants"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />
<jsp:useBean id="location" scope="request" class="com.usatech.layers.common.model.Location" />
<jsp:useBean id="dms_values_list_stateList" class="com.usatech.dms.util.GenericList" scope="application" /> 
<jsp:useBean id="dms_values_list_countryList" class="com.usatech.dms.util.GenericList" scope="application" /> 
<jsp:useBean id="dms_values_list_timeZoneList" class="com.usatech.dms.util.TimeZoneList" scope="application" />
<jsp:useBean id="dms_values_list_locationTypesList" class="com.usatech.dms.util.LocationTypeList" scope="application" />

<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/location.js") %>"></script>

<% 
Boolean missingParam = (Boolean) request.getAttribute("missingParam"); 
if(missingParam != null && missingParam.booleanValue() == true) {
%>
<div class="tableContainer">
<span class="error">Required parameter not found: location_id, location_name</span>
</div>
<%	
} else {
	simple.servlet.InputForm inputForm = (simple.servlet.InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String action = inputForm.getString(com.usatech.dms.util.DMSConstants.PARAM_ACTION_TYPE, false);
    String countryCode = inputForm.getString(com.usatech.dms.location.LocationConstants.PARAM_LOCATION_COUNTRY_CD, false);
    if(countryCode != null){
    	countryCode = countryCode.trim();
    }else{
    	countryCode = inputForm.getString("country_cd", false);
    	countryCode = countryCode.trim();
    }
    if(action.equals(DMSConstants.PARAM_ACTION_NEW)){
    	countryCode = DMSConstants.LIST_VALUE_UNDEFINED;
    }
    String deviceCount = inputForm.getString("deviceCount", false);
    Object[] sqlParams = {countryCode};
    List<NameValuePair> nvp_list_location_states = (List<NameValuePair>)dms_values_list_stateList.getList();
    List<NameValuePair> nvp_list_location_countries =  (List<NameValuePair>)dms_values_list_countryList.getList();
    
    //load page with country undefined
    //check the country val, if undefined, only show one row for state code, state name, Check to delete and press button
    //show one row for Code: text box, Name: textbox, <== Enter new state and press button save button
    
%>
<c:set var="countryCode"><%=countryCode%></c:set>
<c:set var="p"><%=""%></c:set>
<form name="newState" id="newState" method="post" action="#">
<div class="tableContainer">
<div class="innerTable">
<div class="tabHead" align="center" >New State</div>
<table class="tabDataDisplayBorder">
	<tbody>
		<tr>
			<td class="data" width="30%">Country:</td>
			<td class="data" width="40%" id="countryCodeDropDown">
			<select name="country_cd" id="country_cd">
				<c:forEach var="nvp_item_location_country" items="<%=nvp_list_location_countries %>">
					<c:choose>
						<c:when test="${nvp_item_location_country.value ne countryCode}">
							<option  value="${nvp_item_location_country.value}">${nvp_item_location_country.name}</option>
						</c:when>
						<c:otherwise>
							<option value="${nvp_item_location_country.value}" selected="selected" >${nvp_item_location_country.name}</option>
						</c:otherwise>
					</c:choose>	    	
			    </c:forEach>
		    </select>
			</td>
			<td width="30%"></td>
			</tr>			  
			
			<tr>
			<td class="data">Code:
					<input type="text" name="state_cd" id="state_cd" size="35" MAXLENGTH="3" />
			</td>
			<td class="data">Name:
					<input type="text" name="state_name" id="state_name" size="35" MAXLENGTH="60" />
			</td>
			<td class="data" id="newState" align="left">Enter new State and press 
				<input type="button" class="cssButton" value="Save" onclick="return redirectWithParams('newState.i', 'action=Save&country_cd=' + document.newState.country_cd[document.newState.country_cd.selectedIndex].value + '&state_cd=' + document.newState.state_cd.value + '&state_name=' + document.newState.state_name.value, null);"/>
			</td>
			</tr>
		
	</tbody>
</table>
</div>
</div>
</form>
<% } %>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />