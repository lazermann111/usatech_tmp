<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>
<%@page import="simple.text.StringUtils"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />


<%
    InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String device_serial_cd = inputForm.getString("device_serial_cd",false);
    if (StringHelper.isBlank(device_serial_cd)){
%>
<div class="tableContainer">
<div align="center"><span class="error">Required Parameter Not Found: device_serial_cd</span></div>
</div>
<%
    }
    else
    {
    	Results dealerid_res = RequestUtils.getAttribute(request, "dealerid_res", Results.class, false); 
    	Results dealers = RequestUtils.getAttribute(request, "dealers", Results.class, true);
    	
        BigDecimal id = new BigDecimal(-1);
        String eid = null;
        if (dealerid_res.next()){
        	id = dealerid_res.getValue(1,BigDecimal.class);
        	eid = dealerid_res.getFormattedValue("EPORT_ID");
        }
%>
<div class="tabDataContent">
<div class="innerTable">
<form name="dealerForm" id="dealerForm" method="post" action="changeDealer.i">
<input type="hidden" name="device_serial_cd" value="<%=StringUtils.encodeForHTMLAttribute(device_serial_cd) %>">
<input type="hidden" name="eport_id" value="<%=eid %>">
<input type="hidden" name="userOP" value="Change Dealer">

<div class="gridHeader">Change Program for <%=StringUtils.encodeForHTML(device_serial_cd) %></div>

<table class="tabDataDisplay">
	<tbody>
		<tr>
			<td class="data" align="center"> 
			<div class="spacer5"></div>
			<select name="dealerId" id="dealerId" <%if(eid == null){ out.write(" disabled='disabled' "); }%> >
			<%if (id == null || id.intValue()==-1){ %>
			   <option value="-1" selected="selected">Undefined</option>
			<%} %>
			   
<%if (dealers!=null){ while(dealers.next()) {
%><option value="<%=dealers.getValue("DEALER_ID")%>"
<% if(id!=null && id.compareTo(dealers.getValue("DEALER_ID", BigDecimal.class)) == 0){ %> selected="selected"<%} %>
><%=dealers.getFormattedValue("DEALER_NAME")%></option>	    	
<%}}%>
			</select>
			<div class="spacer5"></div>
			</td>
		</tr>
		<tr><td align="center">
        <input <%if(eid == null){ out.write(" disabled='disabled' "); }%> onclick="submitForm();" type="button" class="cssButton" style="width: 120px;" name="userOP" value="Change Program">
        </td>
		</tr>
		<%if(eid == null){%>
		<tr>
			<td align="center">No ePort with such serial number.</td>
		</tr>
		<%} %>
	</tbody>
</table>
</form>
</div>
</div>
<%}%>

<div class="spacer5"></div>
<jsp:include page="/jsp/include/footer.jsp" flush="true" />

	<script type="text/javascript">
	var dealerId = document.getElementById("dealerId");
	function submitForm(){
		if(dealerId.value==null||dealerId.value==-1){
			alert('Choose program please');
			return;
		}
		dealerForm.submit();
	}	
	</script>
	
