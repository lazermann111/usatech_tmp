<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="simple.servlet.RequestUtils,java.math.BigDecimal,simple.results.Results"%>

<%
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "dealer");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("dealerId"), ""));

BigDecimal dealerId = RequestUtils.getAttribute(request, "dealerId", BigDecimal.class, false);
Results dealer = RequestUtils.getAttribute(request, "dealer", Results.class, false);
Results list_lic = RequestUtils.getAttribute(request, "list_licenses", Results.class, false);
Results list_dlic = RequestUtils.getAttribute(request, "list_dlicenses", Results.class, false);

%>


<table >
	<tbody>
	<!-- dealer -->
		
		<tr>
			<td colspan="3" class="label">Name
				<input type="text" name="dealerName" onchange="onDetailChange()" usatRequired="true"  label="Name" size="50" maxlength="50"
				<%if (dealer!=null){ while(dealer.next()) {%>
				value="<%=dealer.getFormattedValue("name")%>"
				<%} }%>
				/>
			</td>
		</tr>
	
		<!-- license agreement -->
		<tr>
			<td colspan="3" align="center">			
				<div class="spacer5"></div>
				<table width=100%>
				<tr>
					<td>
						<b>License Agreements</b>
						<a href="javascript:getData('type=license');"><img src="/images/list.png" title="List" class="list" /></a>
					</td>
					<td>&nbsp;</td>
					<td><b>Program License Agreements</b></td>
				</tr>
				<tr>
					<td class="label">
						<select name="license1" id="license1" size="10" style="width: 300px;">
<%
if (list_lic != null) {
while(list_lic.next()) {
BigDecimal lid = list_lic.getValue("LICENSE_ID", BigDecimal.class);
%><option value="<%=lid%>"><%=list_lic.getFormattedValue("LICENSE_NAME")%></option>	    	
<%}}%>
						</select>
					</td>
					<td align="center">
						<input name="addBtn" id="addBtn" type="button"
							value="Add" onclick="clickAdd()" onchange="onDetailChange()"/>	
							<br/>
						<input name="removeBtn" id="removeBtn" type="button" 
							value="Remove" onclick="clickRemove();" onchange="onDetailChange()"/>
					</td>
					<td class="control">
						<div id="listresult">
						<select name="license2" id="license2" size="10" style="width: 300px;" >
<%if (list_dlic!=null){ while(list_dlic.next()) {
BigDecimal ldid = list_dlic.getValue("LICENSE_ID", BigDecimal.class);
%><option value="<%=ldid%>"><%=list_dlic.getFormattedValue("TITLE")%></option>	    	
<%}}%>
						</select>
						</div>	
					</td>
					<td><input type="button" value="Edit License" onclick="clickEditLicense()"/></td>
				</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>

<div class="spacer10"></div>

<script type="text/javascript">
function clickEditLicense(){    
    var list1 = document.getElementById("license2");
    if(isEmptyString(list1) ){
        alert('Choose license please.');
        return;
    }
    window.location.href= "/license.i?licenseId=" + parseInt($(list1).getValue());
}
function clickAdd(){	
	var list1 = document.getElementById("license1");
	if(mainSelect.value==-9 ){
		alert('Choose program please.');
		return;
	}
	if(isEmptyString(list1) ){
		alert('Choose license please.');
		return;
	}
    submit(mainForm, "dealerAddLicense.i");
}
function clickRemove(){
	var list2 = document.getElementById("license2");
	if( mainSelect.value==-9 ){
		alert('Choose program please.');
		return;
	}
	if(isEmptyString(list2) ){
		alert('Choose license please.');
		return;
	}
    submit(mainForm, "dealerRemoveLicense.i");
}

function isEmptyString(item){
	if(item.value == null || item.value.replace(/\s/gi,"").length<=0)
		return true;
	return false;
}


</script>



