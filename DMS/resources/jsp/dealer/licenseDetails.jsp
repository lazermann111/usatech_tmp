	<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<% 
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_TYPE_CD, "license");
request.setAttribute(DevicesConstants.CHANGE_HISTORY__OBJECT_CD, ConvertUtils.getStringSafely(request.getAttribute("licenseId"), ""));

Results licenseDetail = RequestUtils.getAttribute(request, "licenseDetails", Results.class, false);
Results licenseProcessFees = RequestUtils.getAttribute(request, "licenseProcessFees", Results.class, true);
Results licenseServiceFees = RequestUtils.getAttribute(request, "licenseServiceFees", Results.class, true);
Results deviceSubTypes = RequestUtils.getAttribute(request, "deviceSubTypes", Results.class, true);
Results resellerLicenses = RequestUtils.getAttribute(request, "resellerLicenses", Results.class, true);
String title;
String desc;
String type;
Long parentLicenseId;
Set<String> selectedDeviceSubTypes;
if(licenseDetail != null && licenseDetail.next()) {
    title = licenseDetail.getFormattedValue("LICENSE_NAME", "CDATA");
    desc = StringUtils.prepareHTMLBlank(licenseDetail.getFormattedValue("DESCRIPTION"));
    selectedDeviceSubTypes = new HashSet<String>(ConvertUtils.asCollection(licenseDetail.getValue("DEVICE_SUB_TYPES"), String.class));
    type = licenseDetail.getValue("LICENSE_TYPE", String.class);
    parentLicenseId = licenseDetail.getValue("PARENT_LICENSE_ID", Long.class);
} else {
	title = "";
	desc = "";
	type = "S";
	parentLicenseId = null;
	selectedDeviceSubTypes = null;
}%>
<input type="hidden" name="page_code" id="page_code" value="LICENSE_DETAILS"/>
<table class="tabFormControls">
<tr>
	<td class="label">Title</td>
	<td>
		<input type="text" name="licenseTitle" size="50" maxlength="50" tabindex="2" value="<%=title %>" onchange="onDetailChange()" usatRequired="true" label="Title"/>
	</td>
</tr>
<tr>
	<td class="label">Description</td>
	<td>
		<textarea name="licenseDescription" rows="5" cols="40" tabindex="3" onchange="onDetailChange()"><%=desc %></textarea>
	</td>
</tr>
<tr>
	<td class="label">License Type</td>
	<td>
		<input type="radio" name="licenseType" id="licenseType" onchange="licenseTypeChanged(this)" value="S" <%=type.equals("S")?"checked":""%>> Standard&nbsp;
		<input type="radio" name="licenseType" id="licenseType" onchange="licenseTypeChanged(this)" value="R" <%=type.equals("R")?"checked":""%>> Reseller (Buy Rate)&nbsp;
		<input type="radio" name="licenseType" id="licenseType" onchange="licenseTypeChanged(this)" value="C" <%=type.equals("C")?"checked":""%>> Commission Added (Sell Rate)&nbsp;
	</td>
</tr>
<tr id="parentLicenseRow" <% if(!type.equals("C")) { %> style="visibility:hidden;" <% } %>>
	<td class="label">Reseller License</td>
	<td>
		<select name="parentLicenseId" id="parentLicenseId" style="width:340px;" onchange="onDetailChange()"  <% if(type.equals("C")) { %>usatRequired="true"<% } %> label="Reseller License">
		  <option value="">Please select:</option>
		<% while(resellerLicenses.next()) { %>
		  <option value="<%=resellerLicenses.getValue("LICENSE_ID", Long.class)%>"
		  <% if(resellerLicenses.getValue("LICENSE_ID", Long.class).equals(parentLicenseId)){ %> selected="selected"<%} %>>
		  <%=resellerLicenses.getFormattedValue("LICENSE_NAME", "HTML")%></option>
		<%}%>
		</select> <input type="button" value="Show" id="btnShowParentLicense" onclick="window.location = '/license.i?licenseId=' + mainForm.parentLicenseId.value;"/>
	</td>
<tr>
    <td colspan="3"><fieldset id="deviceSubTypeSet" name="deviceSubTypeSet"><legend class="label">Device Types</legend><%
List<Map<String,Object>> deviceSubTypeMaps = new ArrayList<Map<String,Object>>();
while(deviceSubTypes.next()) {
	Long deviceTypeId = deviceSubTypes.getValue("deviceTypeId", Long.class);
    String label = deviceSubTypes.getFormattedValue("deviceTypeDesc", "HTML");
    Long deviceSubTypeId = deviceSubTypes.getValue("deviceSubTypeId", Long.class);
    String key = null;
    if(deviceSubTypeId != null) {
        key = String.format("%s.%s", deviceTypeId, deviceSubTypeId);
        label = deviceSubTypes.getFormattedValue("deviceSubTypeDesc", "HTML");
    } else {
        key = deviceTypeId.toString();
    }
    Map<String,Object> map = new HashMap<String,Object>();
    map.put("key", key);
    map.put("label", label);
    deviceSubTypeMaps.add(map);
}
int cols = 4;
int rows = (int) Math.ceil(deviceSubTypeMaps.size() / (double)cols);
%><table class="fullPage"><%
for(int r = 0; r < rows; r++) {
    %><tr><%
    for(int c = 0; c < cols; c++) {
        int index = c * rows + r;
        if(index < deviceSubTypeMaps.size()) {
            Map<String,Object> map = deviceSubTypeMaps.get(index);
            String key = map.get("key").toString();
            String label = map.get("label").toString();
            %><td><label><input type="checkbox" name="deviceSubTypes" value="<%=key%>" onclick="onDetailChange()"<%if(selectedDeviceSubTypes != null && selectedDeviceSubTypes.contains(key)) { %> checked="checked"<%} %>/> <%=label %></label></td><%
        }
    }
    if(r == 0) {
        %><td rowspan="<%=rows%>" class="center"><button type="button" onclick="toggleSelectAll(this, $('deviceSubTypeSet').getElements('input[name=deviceSubTypes]'))">Select All</button></td><%
     }
    %></tr><%
}%>
</table>
<script type="text/javascript">
function toggleSelectAll(button, checkboxes) {
	button = $(button);
	var checkall = (button.getProperty("toggleSelectAll") == "true");
	button.setProperty("toggleSelectAll", checkall ? "false" : "true");
	button.setText(checkall ? "Select All" : "Deselect All");
	checkboxes.each(function(cbk) { cbk.checked = !checkall;});
	onDetailChange();
}
function licenseTypeChanged(radio) {
	val = radio.getProperty('value')
	if (val == 'C') {
		$('parentLicenseRow').setStyle('visibility', 'visible')
		$$('.comm_cell').each(function(c) { c.setStyle('visibility', 'visible');});
		$('parentLicenseId').setProperty('usatRequired','true');
	} else {
		$('parentLicenseRow').setStyle('visibility', 'hidden');
		$$('.comm_cell').each(function(c) { c.setStyle('visibility', 'hidden');});
		$('parentLicenseId').setProperty('usatRequired','false');
		$('parentLicenseId').setProperty('value', '');
		$$('.commission').each(function(c) { c.setProperty('value', '0');});
	}
	onDetailChange();
}

function feeChanged(field) {
	// dynamically update fees
	onDetailChange();
}
function dynamicFeeChanged(field, feeId) {
	// dynamically update fees
	if ($('serviceFeeDynamicMin_'+feeId).value == '' || $('serviceFeeDynamicMax_'+feeId).value == '' || $('serviceFeeDynamicMin_'+feeId).value <= $('serviceFeeDynamicMax_'+feeId).value){
		feeChanged(field);
	}else{
		alert("For dynamic fees Minimum amount can't be more than Maximum amount. Minimum value will be set to maximum. Please check the values.");
		$('serviceFeeDynamicMin_'+feeId).value = $('serviceFeeDynamicMax_'+feeId).value;
		feeChanged(field);
	} 
}
function onDynamicFeeChange(feeId){
	var frequency = document.getElementById('serviceFeeFrequencyId'+feeId);
	$('serviceFeeAmount_'+feeId).readOnly=false;
	if ($('serviceFeeDynamic_'+feeId).checked){
		$('dynamicAmounts_'+feeId).setStyle('display', '');
		$('serviceFeeAmount_'+feeId).value=0;
		$('serviceFeeAmount_'+feeId).disabled="true";
		frequency.remove(1);
		//frequency[1].style.display="none";
		frequency[0].selected=true;
	}
	else{
		$('dynamicAmounts_'+feeId).setStyle('display', 'none');
		$('serviceFeeAmount_'+feeId).disabled="";
		//frequency[1].style.display="block";
		var option = document.createElement("option");
		option.text = "Daily";
		option.value= "4";
		frequency.add(option);
	}
}
</script>
</fieldset></td>
</tr>
<tr><td colspan="3"><hr/></td></tr>
<tr>
	<td>&nbsp;</td>
	<td><b>Process Fees</b></td>
	<td class="comm_cell" <% if(!type.equals("C")) { %> style="visibility:hidden;" <% } %>><b>Calculated Commissions</b></td>
</tr>
<%StringBuilder sbPF = new StringBuilder();%>
<% while(licenseProcessFees.next()) {
	Long ttId = licenseProcessFees.getValue("TRANS_TYPE_ID", Long.class);
	String feeName = licenseProcessFees.getFormattedValue("TRANS_TYPE_NAME", "HTML");
	double percent = ConvertUtils.getDouble(licenseProcessFees.get("PERCENT"), 0);
	double parentPercent = ConvertUtils.getDouble(licenseProcessFees.get("PARENT_LICENSE_PERCENT"), 0);
	String commissionPercent = ConvertUtils.formatObject((percent-parentPercent), "NUMBER:#######0.######");
	double amount = ConvertUtils.getDouble(licenseProcessFees.get("ADDITIONAL"), 0);
	double parentAmount = ConvertUtils.getDouble(licenseProcessFees.get("PARENT_LICENSE_ADDITIONAL"), 0); 
	String commissionAmount = ConvertUtils.formatObject((amount-parentAmount), "NUMBER:#######0.00######"); %>
<tr>
	<td class="label nowrap"><%=feeName%> Fee</td>
	<td>
	  <input type="text" name="processFeePercent_<%=ttId%>" size="6" value="<%=licenseProcessFees.getFormattedValue("PERCENT") %>" onchange="feeChanged(this)" minValue="0" maxValue="100" label="% <%=feeName%> Fee"/>%
	  + $<input type="text" name="processFeeAdditional_<%=ttId%>" size="6" value="<%=licenseProcessFees.getFormattedValue("ADDITIONAL") %>" onchange="feeChanged(this)" minValue="0.00" label="Per Transaction <%=feeName%> Fee"/>
      ($<input type="text" name="processFeeMinimum_<%=ttId%>" size="6" value="<%=licenseProcessFees.getFormattedValue("MINIMUM")%>" onchange="onDetailChange()" minValue="0.00" label="Minimum <%=feeName%> Fee"/> minimum)
    </td>
    <td class="comm_cell" <% if(!type.equals("C")) { %> style="visibility:hidden;" <% } %>>
	<input type="text" id="processFeeCommissionPercent_<%=ttId%>" size="6" readonly="readonly" value="<%=commissionPercent%>"/>%
	+ $<input type="text" id="processFeeCommissionAmount_<%=ttId%>" size="6" readonly="readonly" value="<%=commissionAmount%>"/>
	</td>
</tr>
<%	sbPF.append("<input type=\"hidden\" id=\"processFee_percent_").append(ttId).append("\" value=\"").append(percent).append("\" />\n");
	sbPF.append("<input type=\"hidden\" id=\"processFee_parent_percent_").append(ttId).append("\" value=\"").append(parentPercent).append("\" />\n");
	sbPF.append("<input type=\"hidden\" id=\"processFee_amount_").append(ttId).append("\" value=\"").append(amount).append("\" />\n");
	sbPF.append("<input type=\"hidden\" id=\"processFee_parent_amount_").append(ttId).append("\" value=\"").append(parentAmount).append("\" />\n");
	sbPF.append("<input type=\"hidden\" id=\"lbn_processFeePercent_").append(ttId).append("\" value=\"").append(licenseProcessFees.getFormattedValue("FEE_PERCENT_LBN")).append("\" />\n");
	sbPF.append("<input type=\"hidden\" id=\"ubn_processFeePercent_").append(ttId).append("\" value=\"").append(licenseProcessFees.getFormattedValue("FEE_PERCENT_UBN")).append("\" />\n");
	sbPF.append("<input type=\"hidden\" id=\"ubn_processFeeAdditional_").append(ttId).append("\" value=\"").append(licenseProcessFees.getFormattedValue("FEE_AMOUNT_UBN")).append("\" />\n");
	sbPF.append("<input type=\"hidden\" id=\"ubn_processFeeMinimum_").append(ttId).append("\" value=\"").append(licenseProcessFees.getFormattedValue("FEE_MIN_UBN")).append("\" />\n");
}%>
<tr><td colspan="3"><hr/></td></tr>
<tr>
	<td>&nbsp;</td>
	<td><b>Service Fees</b></td>
	<td class="comm_cell" <% if(!type.equals("C")) { %> style="visibility:hidden;" <% } %>><b>Calculated Commissions</b></td>
</tr> 
<%
StringBuilder sbSF = new StringBuilder();
while(licenseServiceFees.next()) {
	Long feeId = licenseServiceFees.getValue("FEE_ID", Long.class);
	Integer defaultFrequencyId = licenseServiceFees.getValue("DEFAULT_FREQUENCY_ID", Integer.class);
	Integer frequencyId = licenseServiceFees.getValue("FREQUENCY_ID", Integer.class);
	if(frequencyId==null){
		frequencyId=defaultFrequencyId;
	}
	char dynamicFeeInd = licenseServiceFees.getValue("DYNAMIC_FEE_IND", char.class);
	double dynamicMin = ConvertUtils.getDouble(licenseServiceFees.get("DYNAMIC_MIN_AMOUNT"), 0);
	double dynamicMax = ConvertUtils.getDouble(licenseServiceFees.get("DYNAMIC_MAX_AMOUNT"), 0);
	double dynamicPerTrans = ConvertUtils.getDouble(licenseServiceFees.getValue("DYNAMIC_AMOUNT_PER_TRANS"), 0);
	boolean isDynamic = (dynamicMin > 0 || dynamicMax > 0 || dynamicPerTrans > 0);
	String feeName = licenseServiceFees.getFormattedValue("FEE_NAME", "HTML");
	if(feeId == 7||feeId == 13||feeId == 14)
		continue;
	double amount = ConvertUtils.getDouble(licenseServiceFees.get("AMOUNT"), 0);
	double parentAmount = ConvertUtils.getDouble(licenseServiceFees.get("PARENT_LICENSE_AMOUNT"), 0);
	String commissionAmount = ConvertUtils.formatObject((amount-parentAmount), "NUMBER:#######0.00######");
	char feeTypeCd = licenseServiceFees.getValue("INITIATION_TYPE_CD", Character.class);
	sbSF.append("<input type=\"hidden\" id=\"ubn_serviceFeeAmount_").append(feeId).append("\" value=\"").append(licenseServiceFees.getFormattedValue("FEE_AMOUNT_UBN")).append("\" />\n");
	sbSF.append("<input type=\"hidden\" id=\"serviceFee_amount_").append(feeId).append("\" value=\"").append(amount).append("\" />\n");
	sbSF.append("<input type=\"hidden\" id=\"serviceFee_parent_amount_").append(feeId).append("\" value=\"").append(parentAmount).append("\" />\n"); %>
	<tr>
	    <td class="label nowrap"><%=feeName%><%if(feeTypeCd == 'R') { %> (Active)<%} %></td>
	    <td>
	    	$<input type="text" id="serviceFeeAmount_<%=feeId%>" name="serviceFeeAmount_<%=feeId%>" size="6" maxlength="16" <%if(isDynamic) {%>value="" readonly="true" <%}else{ %>value="<%=licenseServiceFees.getFormattedValue("AMOUNT") %>"<%} %> onchange="feeChanged(this)" minValue="0.00" label="<%=feeName%>" />
	        Start Immediately: <input type="checkbox" name="serviceFeeImmediate_<%=feeId%>" value="Y" onchange="onDetailChange()"<%if("Y".equalsIgnoreCase(licenseServiceFees.getValue("IMMEDIATE", String.class))) { %> checked="checked"<%} %>/>
	        <%if(frequencyId==2 ||frequencyId==4){ %>
	    	<select id="serviceFeeFrequencyId<%=feeId%>" name="serviceFeeFrequencyId<%=feeId%>" onchange="feeChanged(this)">
				<option value="2" <%if(frequencyId==2){ %>selected<%} %>>Monthly</option>
				<%if (!isDynamic) {%><option value="4" <%if(frequencyId==4){ %>selected<%} %>>Daily</option><%} %>
			</select>
			<%} %>
			<%if (dynamicFeeInd =='Y') {%>
			Dynamic Fee: <input type="checkbox" id="serviceFeeDynamic_<%=feeId%>" name="serviceFeeDynamic_<%=feeId%>" value="N" onchange="onDynamicFeeChange(<%=feeId%>)" <%if (isDynamic) {%>checked="checked"<%}%> >
			<%}%>
		</td>
	    <td class="comm_cell" <% if(!type.equals("C")) { %> style="visibility:hidden;" <% } %>>
	    	$<input type="text" name="serviceFeeCommissionAmount_<%=feeId%>" size="6" readonly="readonly" value="<%=commissionAmount%>"/>
		</td>
	</tr>
	<%if (dynamicFeeInd =='Y') {
		String dynamicFeeFields = "none";	
		if (isDynamic) dynamicFeeFields = "table-row"; %>
		<tr id="dynamicAmounts_<%=feeId%>" name="dynamicAmounts_<%=feeId%>" style="display:<%=dynamicFeeFields%>">
		<td/>
		<td>	
			Amounts: 
			Min= <input type="text" name="serviceFeeDynamicMin_<%=feeId%>" id="serviceFeeDynamicMin_<%=feeId%>" size="6" maxlength="16" value="<%=dynamicMin%>" onchange="dynamicFeeChanged(this, <%=feeId%>)" minValue="0.00" label="<%=feeName%>: Min amount">
			Max= <input type="text" name="serviceFeeDynamicMax_<%=feeId%>" id="serviceFeeDynamicMax_<%=feeId%>" size="6" maxlength="16" value="<%=dynamicMax%>" onchange="dynamicFeeChanged(this, <%=feeId%>)" minValue="0.00" label="<%=feeName%>: Max amount">
			per trans= <input type="text" name="serviceFeeDynamicAmount_<%=feeId%>" size="6" maxlength="16" value="<%=dynamicPerTrans%>" onchange="feeChanged(this)" minValue="0.00" label="<%=feeName%>: Amount per transaction">
		</td>
		</tr>	
	<%} %>
    <%if(feeTypeCd == 'R') { %><tr>
        <td class="label nowrap"><%=feeName%> (Inactive)</td>
        <td>$<input type="text" name="serviceFeeInactiveAmount_<%=feeId%>" size="6" maxlength="16" value="<%=licenseServiceFees.getFormattedValue("INACTIVE_FEE_AMOUNT") %>" onchange="feeChanged(this)" minValue="0.00" label="Inactive <%=feeName%>"/> 
          Maximum Reductions: <input type="text" name="serviceFeeInactiveMonths_<%=feeId%>" size="1" maxlength="6" value="<%=licenseServiceFees.getFormattedValue("INACTIVE_MONTHS_REMAINING") %>" onchange="feeChanged(this)" minValue="1" label="Maximum number of times fee will use inactive rate"/>
        </td>
        </tr>
      <%} %>
	
<%} %>
</table>

	
<%=sbPF.toString()%>
<%=sbSF.toString()%>

<span class="note">Note: only customers with an end of month fee schedule can have dynamic service fees</span>
