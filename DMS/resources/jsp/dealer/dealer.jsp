<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<div class="formContainer">

<div class="tableHead">
<div class="tabHeadTxt"><span class="txtWhiteBold">Dealers</span></div>
</div>

<div class="tableContent">

<form name="dealerForm" id="dealerForm" method="post" action="dealer.i">
<input type="hidden" name="dealerId"></input>
<input type="hidden" name="action"></input>

<div id="divDealerSelect" >
<jsp:include page="/jsp/dealer/dealerForm.jsp" flush="true" />	
</div>

</form>
</div>
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />

<script type="text/javascript">
var req;
var newmode = false;
var dealerId = document.getElementById("dealerId");
var dealerName = document.getElementById("dealerName");
var createBtn = document.getElementById("createBtn");
var deleteBtn = document.getElementById("deleteBtn");
var saveBtn = document.getElementById("saveBtn");
var addBtn = document.getElementById("addBtn");
var removeBtn = document.getElementById("removeBtn");
var dealerSelect = document.getElementById("dealerSelect");
var list1 = document.getElementById("list1");
var list2 = document.getElementById("list2");
var status = document.getElementById("status");
var listresult = document.getElementById("listresult");


function clickSelect(){
	new Ajax('dealerGet.i', {method: 'get', async: false, data: "dealerId=" + dealerSelect.value, 
		update: $('divDealerSelect'), evalScripts: true }).request();	
	init();

}

function clickCreate(){
	startNewMode();
}

function clickDelete(){
	if(newmode){
		endNewMode();
	}else{  //delete
		if(dealerForm.dealerId.value === null || dealerForm.dealerId.value.length<=0 ||dealerForm.dealerId.value == -1){
			alert('Choose program, please.');
			return;
		}
		
		dealerForm.dealerId.value = dealerForm.dealerSelect.value;
		dealerForm.action.value = "delete";
		if( !confirmSubmit('Do you really want to delete?'))
			return;

		new Ajax('dealerDelete.i', {method: 'get', async: false, data: "dealerId=" + dealerSelect.value, 
			update: $('divDealerSelect'), evalScripts: true }).request();

		init();
	}

}

function clickSave(){
	if(newmode){  //create
		if( isEmptyString(dealerName) ){
			alert('Enter new name, please.');
			return;
		}
		with(document.dealerForm) {
			dealerId.value = -1;
			new Ajax('dealerCreate.i', {method: 'get', async: false, data: getParams() , 
				update: $('divDealerSelect'), evalScripts: true }).request();
		}
		init();

	}else{   //update
		with(document.dealerForm) {
			dealerId.value = dealerSelect.value;
			action.value = "update";
			if(dealerId.value == null || dealerId.value.length<=0 || dealerForm.dealerId.value == -1){
				alert('Choose program, please.');
				return;
			}
			if(isEmptyString(dealerName) ){
				alert('Enter new name, please.');
				return;
			}
			new Ajax('dealerUpdate.i', {method: 'get', async: false, data: getParams() , 
				update: $('divDealerSelect'), evalScripts: true }).request();

			init();
		}
	}
}


function clickAdd(){
	if(isEmptyString(dealerSelect)||dealerSelect.value==-1){
		alert('Choose program, please.');
		return;
	}
	if(isEmptyString(list1)){
		alert('Choose license, please.');
		return;
	}
	new Ajax('dealerAddLicense.i', {method: 'get', async: false, data: getParams()+"&licenseId="+list1.value , 
		update: $('divDealerSelect'), evalScripts: true }).request();

	init();
	
}

function clickRemove(){
	if(isEmptyString(dealerSelect)||dealerSelect.value==-1){
		alert('Choose program, please.');
		return;
	}
	if(isEmptyString(list2)){
		alert('Choose license, please.');
		return;
	}
	new Ajax('dealerRemoveLicense.i', {method: 'get', async: false, data: getParams()+"&licenseId="+list2.value , 
		update: $('divDealerSelect'), evalScripts: true }).request();

	init();
	
}


function disableButton(item) {
	item.className="cssButtonDisabled";
	item.disabled = "disabled";
}

function enableButton(item) {
	item.className = "cssButton";
	item.removeAttribute("disabled");
}

function startNewMode(){
	newmode=true;
	clearForm();
	setValueInSelectControl(dealerSelect,-1);
	disableButton(createBtn);
	deleteBtn.value="Cancel";
	disableButton(addBtn);
	disableButton(removeBtn);
	dealerSelect.disabled = "disabled";
	list1.disabled = "disabled";
	list2.disabled = "disabled";
	dealerForm.dealerName.focus();
	list2.innerHTML='';
}

function clearForm(){
	dealerName.value='';
	dealerId='';
	action='';
}


function endNewMode(){
	newmode=false;
	enableButton(createBtn);
	deleteBtn.value="Delete Dealer";
	enableButton(addBtn);
	enableButton(removeBtn);
	dealerSelect.removeAttribute("disabled");
	list1.removeAttribute("disabled");
	list2.removeAttribute("disabled");
}

function setValueInSelectControl(select, value) {
    for(var i = 0; i < select.options.length; i++) {
        if(select.options.item(i).value == value) {
            select.selectedIndex = i;
            return;
        }
    }
    select.selectedIndex = -1;
}



function init(){
	newmode = false;
	dealerId = document.getElementById("dealerId");
	dealerName = document.getElementById("dealerName");
	createBtn = document.getElementById("createBtn");
	deleteBtn = document.getElementById("deleteBtn");
	saveBtn = document.getElementById("saveBtn");
	addBtn = document.getElementById("addBtn");
	removeBtn = document.getElementById("removeBtn");
	dealerSelect = document.getElementById("dealerSelect");
	list1 = document.getElementById("list1");
	list2 = document.getElementById("list2");
	status = document.getElementById("status");
	listresult = document.getElementById("listresult");
	dealerId = dealerSelect.value;
}


function isEmptyString(item){
		if(item.value == null || item.value.replace(/\s/gi,"").length<=0)
			return true;
		return false;
}

function getParams(){
	var p = "";

	with(document.dealerForm) {
		p+="&action="+action.value;
		p+="&dealerId="+dealerSelect.value;
		p+="&dealerName="+dealerName.value;
	}
	return p;
	
}



</script>
