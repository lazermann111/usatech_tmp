<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
	int i = 0; 
    InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
    String sortField = PaginationUtil.getSortField(null);
    String sortIndex = inputForm.getString(sortField, false);
    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    boolean norec=false;
    
	String totalCount = (String)request.getAttribute(PaginationUtil.getTotalField(null));
	if (totalCount!=null && totalCount.length()>0 && Integer.parseInt(totalCount) == 0)
		norec = true;
%>

<div class="formContainer">
<div class="tableHead">
<div class="tabHeadTxt">License Agreements</div>
</div>
<table class="tabDataDisplayBorderNoFixedLayout">
	<thead>
		<tr class="sortHeader">
			<td>
				<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">License Name</a>
				<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
			</td>
					
		</tr>
	</thead>
	<tbody>
	<% 
	if (norec){
	    %>
	</tbody>
	</table>
	<div class="tabHead" align="center">NO RECORDS</div>
	<%} else {
	Results list = (Results) request.getAttribute("resultlist");
	while(list.next()) {
	%>
		<tr class="<%=(i++%2==0)?"row1":"row0"%>">
		    <td><a href="/license.i?licenseId=<%=list.getFormattedValue("LICENSE_ID")%>"><%=list.getFormattedValue("LICENSE_TITLE")%></a></td>
		</tr>
	<% } %>
	</tbody>
</table>

<%
    String storedNames = PaginationUtil.encodeStoredNames(new String[] {"license_name"});
%>
    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
        <jsp:param name="_param_request_url" value="licenseList.i" />
        <jsp:param name="_param_stored_names" value="<%=storedNames%>" />
    </jsp:include>

<% } %>


</div>

<div class="spacer10"></div>
				
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
