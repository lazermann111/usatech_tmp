<%@ page language="java" import="java.io.*, java.sql.*, java.text.*, java.util.Date, java.text.SimpleDateFormat" %>

<%@page import="com.usatech.layers.common.ProcessingUtils"%>

<%@page import="simple.db.DataLayerMgr"%>

<jsp:include page="/jsp/include/header.jsp" flush="true">
	<jsp:param name="pageType" value="BIRT" />
</jsp:include>

<%
	Date now = new Date();
	SimpleDateFormat df = new SimpleDateFormat("MMM d - HH:mm:ss");
	final simple.io.Log log = simple.io.Log.getLog();
%>
		
<table cellpadding="3" cellspacing="0" border="0" width="100%">
<tr>
	<td>
		<div class="header">GRPS Devices by State (activity metrics)</div>

<div class="boardbox">
<div class="colorbox" style="background-color: #00CC00;"></div><div class="textbox">99%-100%</div>

<div class="colorbox" style="background-color: #26D400;"></div><div class="textbox">97%-98%</div>
<div class="colorbox" style="background-color: #60E000;"></div><div class="textbox">95%-96%</div>
<div class="colorbox" style="background-color: #9FEC00;"></div><div class="textbox">93%-94%</div>
<div class="colorbox" style="background-color: #D8F800;"></div><div class="textbox">91%-92%</div>

<div class="colorbox" style="background-color: #FFFF00;"></div><div class="textbox">89%-90%</div>

<div class="colorbox" style="background-color: #F8D800;"></div><div class="textbox">87%-88%</div>
<div class="colorbox" style="background-color: #EC9F00;"></div><div class="textbox">85%-86%</div>
<div class="colorbox" style="background-color: #E06000;"></div><div class="textbox">83%-84%</div>
<div class="colorbox" style="background-color: #D42600;"></div><div class="textbox">81%-82%</div>

<div class="colorbox" style="background-color: #CC0000;"></div><div class="textbox">&lt; 81%</div>
</div>
<div class="boardbox" id="flashbox">

</div>
<script>
var myAjax = new Ajax.Updater('flashbox', '/flash/gprs_stat_swf.jswf', {method: 'get'});
function loadFiltered() {
	var uris = '/flash/gprs_stat_swf.jswf?total_days=' +
		document.cf.totald.options[document.cf.totald.selectedIndex].value +
		'&active_days=' +
		document.cf.actived.options[document.cf.actived.selectedIndex].value +
		'&call_ins=' +
		document.cf.calls.options[document.cf.calls.selectedIndex].value +
		'&cust_id=' +
		document.cf.cust.options[document.cf.cust.selectedIndex].value;
		
	if (document.cf.unk.checked) {
		uris = uris + '&include_unk=true'
	};
	
	myAjax = new Ajax.Updater('flashbox', uris, {method: 'get'});
}
</script>

<div class="footer">

<table cellpadding="2" cellspacing="0" border="0" width="450">
<tr><form name="cf" id="cf" onsubmit="loadFiltered(); return false;">
	<td align="right"><b>Reference Period:</b></td>
	<td><select name="totald" id="totald">
		<option value="2">2 Days</option>
		<option value="3">3 Days</option>
		<option value="4">4 Days</option>
		<option value="5">5 Days</option>
		<option value="6">6 Days</option>
		<option value="7" SELECTED>7 Days</option>
		<option value="10">10 Days</option>
		<option value="15">15 Days</option>
		<option value="20">20 Days</option>
		<option value="25">25 Days</option>
		<option value="30">30 Days</option>
		<option value="60">60 Days</option>
		<option value="90">90 Days</option>
		<option value="180">180 Days</option>
		<option value="365">365 Days</option>
</select></td>
</tr><tr>
	<td align="right"><b>Comparative Period:</b></td>
	<td><select name="actived" id="actived">
		<option value="1">1 Day</option>
		<option value="2" SELECTED>2 Days</option>
		<option value="3">3 Days</option>
		<option value="4">4 Days</option>
		<option value="5">5 Days</option>
		<option value="6">6 Days</option>
</select></td>
</tr><tr>
	<td align="right"><b>Threshold:</b></td>
	<td><select name="calls" id="calls">
		<option value="0">0</option>
		<option value="5">5</option>
		<option value="10">10</option>
		<option value="15">15</option>
		<option value="20">20</option>
		<option value="25" SELECTED>25</option>
		<option value="30">30</option>
</select></td>
</tr><tr>
	<td align="right"><b>Filter by Customer:</b></td>
	<td><select name="cust" id="cust">
		<option value="-1" SELECTED>-- All Customers --</option><%
	
	Connection dbcon = null;
	Statement statement = null;
	ResultSet rs = null;
	try {
		dbcon = DataLayerMgr.getConnection("OPER");		
		statement = dbcon.createStatement();
		
		StringBuffer query = new StringBuffer();
		
		// format the query
		
		query.append("SELECT DISTINCT ");
		query.append("cus.customer_id, ");
		query.append("cus.customer_name ");
		query.append("FROM device.gprs_device gprs ");
		query.append("JOIN device.device dev ");
		query.append("ON dev.device_id = gprs.device_id ");
		query.append("AND gprs.gprs_device_state_id = 5 ");
		query.append("AND dev.device_active_yn_flag = 'Y' ");
		query.append("JOIN pss.pos pos ");
		query.append("ON dev.device_id = pos.device_id ");
		query.append("JOIN location.customer cus ");
		query.append("ON pos.customer_id = cus.customer_id ");
		query.append("AND pos.customer_id != 1 ");
		query.append("ORDER BY LOWER(cus.customer_name) ");
		
		// Perform the query
		rs = statement.executeQuery(query.toString());
		
		String col_cust_id = "-1";
		String col_cust_name = "";
		
		// Iterate through each row of rs
		while (rs.next()) {
			col_cust_id = rs.getString("customer_id");
			col_cust_name = rs.getString("customer_name");
			%><option value="<%= col_cust_id %>"><%= col_cust_name %></option>
<%
		}
	} catch (SQLException ex) {
		
		while (ex != null) {
			%><%= "SQL Exception:  " + ex.getMessage() + "\n<br />"%><%
			ex = ex.getNextException ();
		}  // end while
		
	} catch(java.lang.Exception ex) {
		
		%><%= "<HTML>" + "<HEAD><TITLE>" + "Device: Error" + "</TITLE></HEAD>\n<BODY>" + "<P>SQL error in doGet: " + ex.getMessage() + "</P></BODY></HTML>" %><%
		
	}finally{
		ProcessingUtils.closeDbResultSet(log, rs);
		ProcessingUtils.closeDbStatement(log, statement);
		ProcessingUtils.closeDbConnection(log, dbcon);
	}
%>		
</select></td>
</tr><tr>
	<td align="right"><b>Include Unknown City:</b></td>
	<td><input type="checkbox" name="unk" id="unk"></td>
</tr><tr>
	<td colspan="2" align="center"><input type="submit" name="submit" value="Submit"></td>
</form></tr>
</table>
<br><i><b>Note:</b></i> Mouse over each state to see the exact percentage.<br><i><b>Generated:</b></i> <%= df.format(now) %></div>
</td>
</tr>
</table>		
		
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
