<%@ page language="java" import="java.io.*, java.sql.*, java.text.*, java.util.*" %>

<%@page import="com.usatech.layers.common.ProcessingUtils"%>

<%@page import="simple.db.DataLayerMgr"%>

<%@page import="simple.app.DialectResolver"%>

<%
	response.setContentType("text/xml");    // Response mime type
	
	
	//response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
	//response.setHeader("Pragma","no-cache"); //HTTP 1.0
	
	// this is the fix for the IE SSL / Flash XML bug
	// it can only use these headers for cache control
	response.setHeader("Pragma", "");
	response.setHeader("Cache-Control", "must-revalidate");
	response.setDateHeader ("Expires", 0);
	response.setDateHeader("Last-Modified", System.currentTimeMillis());
	
	final simple.io.Log log = simple.io.Log.getLog();
	
	// white house coordinates = 38.8975,-77.0366
	
	%>
<?xml version="1.0" encoding="utf-8"?>
<us_states>
  <state id="range">
    <data>1 - 50</data>
    <color>E0E9F7</color>
  </state>
  <state id="range">
    <data>51 - 100</data>
    <color>CDDBEE</color>
  </state>
  <state id="range">
    <data>101 - 150</data>
    <color>B7CAE3</color>
  </state>
  <state id="range">
    <data>151 - 200</data>
    <color>9EB7D6</color>
  </state>
  <state id="range">
    <data>201 - 250</data>
    <color>84A4C9</color>
  </state>
  <state id="range">
    <data>251 - 300</data>
    <color>6A90BD</color>
  </state>
  <state id="range">
    <data>301 - 350</data>
    <color>517EB0</color>
  </state>
  <state id="range">
    <data>351 - 400</data>
    <color>3B6DA5</color>
  </state>
  <state id="range">
    <data>401 - 450</data>
    <color>285F9C</color>
  </state>
  <state id="range">
    <data>451 - 500</data>
    <color>1B5595</color>
  </state>
  <state id="range">
    <data>501 - 1000000</data>
    <color>959595</color>
  </state>
  <state id="point_range">
    <data>1 - 50</data>
    <color>E0E9F7</color>
  </state>
  <state id="point_range">
    <data>51 - 100</data>
    <color>CDDBEE</color>
  </state>
  <state id="point_range">
    <data>101 - 150</data>
    <color>B7CAE3</color>
  </state>
  <state id="point_range">
    <data>151 - 200</data>
    <color>9EB7D6</color>
  </state>
  <state id="point_range">
    <data>201 - 250</data>
    <color>84A4C9</color>
  </state>
  <state id="point_range">
    <data>251 - 300</data>
    <color>6A90BD</color>
  </state>
  <state id="point_range">
    <data>301 - 350</data>
    <color>517EB0</color>
  </state>
  <state id="point_range">
    <data>351 - 400</data>
    <color>3B6DA5</color>
  </state>
  <state id="point_range">
    <data>401 - 450</data>
    <color>285F9C</color>
  </state>
  <state id="point_range">
    <data>451 - 500</data>
    <color>1B5595</color>
  </state>
  <state id="point_range">
    <data>501 - 1000000</data>
    <color>959595</color>
  </state>
  <state id="outline_color">
    <color>330000</color>
  </state>
  <state id="default_color">
    <color>FFFFFF</color>
  </state>
  <state id="background_color">
    <color>FFFFFF</color>
  </state>
  <state id="hover">
    <font_size>12</font_size>
    <font_color>000000</font_color>
    <background_color>EEEEEE</background_color>
  </state>
  <state id="default_point">
    <color>000000</color>
    <size>3</size>
    <opacity>90</opacity>
  </state>
  <state id="scale_points">
    <data>25</data>
  </state>
<%
	
	Connection dbcon = null;
	Statement statement = null;
	ResultSet rs = null;
	try {		
		dbcon = DataLayerMgr.getConnection("OPER");
		statement = dbcon.createStatement();
		
		StringBuffer query = new StringBuffer();
		
		// format the query
		
		query.append("SELECT 2 ord_pos, st.state_cd, st.state_name, 'Total: ' device_type_desc, COALESCE(devc.dev_count, 0) dev_count ");
		query.append("FROM location.state st ");
		query.append("LEFT OUTER JOIN ( ");
		query.append("    SELECT adev.location_state_cd, SUM(adev.dev_count) dev_count ");
		query.append("    FROM ( ");
		query.append("        SELECT loc.location_state_cd, COUNT(1) dev_count ");
		query.append("        FROM device.device dev ");
		query.append("        JOIN pss.pos pos ");
		query.append("        ON dev.device_id = pos.device_id ");
		query.append("        AND dev.device_active_yn_flag = 'Y' ");
		query.append("        AND dev.device_type_id != 5 ");
		if (!DialectResolver.isOracle()) {
			query.append("        AND dev.last_activity_ts > LOCALTIMESTAMP - interval '1 day' * 30 ");
		} else {
			query.append("        AND dev.last_activity_ts > SYSDATE - 30 ");
		};	
		query.append("        JOIN location.location loc ");
		query.append("        ON pos.location_id = loc.location_id ");
		query.append("        GROUP BY loc.location_state_cd ");
		
		query.append("        UNION ALL ");
		
		query.append("        SELECT ca.location_state_cd, (ca.room_ctrl_cnt + ca.wash_dry_cnt) dev_count  ");
		query.append("        FROM ( ");
		query.append("            SELECT  ");
		query.append("            location_state_cd, ");
		query.append("            COUNT(1) room_ctrl_cnt, ");
		query.append("            SUM(endpoint_count) wash_dry_cnt ");
		query.append("            FROM ( ");
		query.append("                SELECT ");
		query.append("                school.location_name school, ");
		query.append("                campus.location_name campus, ");
		query.append("                dorm.location_name dorm,   ");
		query.append("                room.location_name room, ");
		query.append("                school.location_state_cd, ");
		query.append("                dev.device_serial_cd serial_cd, ");
		query.append("                COUNT(1) endpoint_count ");
		query.append("                FROM ");
		query.append("                device.device dev, ");
		query.append("                device.host hst, ");
		query.append("                pss.pos pos, ");
		query.append("                location.location room, ");
		query.append("                location.location dorm, ");
		query.append("                location.location campus, ");
		query.append("                location.location school ");
		query.append("                WHERE room.parent_location_id = dorm.location_id ");
		query.append("                AND dorm.parent_location_id = campus.location_id ");
		query.append("                AND campus.parent_location_id = school.location_id ");
		query.append("                AND (room.location_type_id = 18 OR room.location_id = 1) ");
		query.append("                AND (dorm.location_type_id = 17 OR dorm.location_id = 1) ");
		query.append("                AND (campus.location_type_id = 16 OR campus.location_id = 1) ");
		query.append("                AND (school.location_type_id = 6 OR school.location_id = 1) ");
		query.append("                AND room.location_id = pos.location_id       ");
		query.append("                AND pos.device_id = dev.device_id ");
		query.append("                AND dev.device_id = hst.device_id ");
		query.append("                AND room.location_active_yn_flag = 'Y' ");
		query.append("                AND dev.device_type_id = 5 ");
		query.append("                AND dev.device_active_yn_flag = 'Y' ");
		if (!DialectResolver.isOracle()) {
			query.append("                AND dev.last_activity_ts > LOCALTIMESTAMP - interval '1 day' * 30 ");
		} else {
			query.append("                AND dev.last_activity_ts > SYSDATE - 30 ");
		};	
		query.append("                GROUP BY ");
		query.append("                school.location_name, ");
		query.append("                campus.location_name, ");
		query.append("                dorm.location_name, ");
		query.append("                room.location_name, ");
		query.append("                school.location_state_cd, ");
		query.append("                dev.device_serial_cd ");
		query.append("            ) a ");
		query.append("            GROUP BY ");
		query.append("            location_state_cd ");
		query.append("        ) ca ");
		query.append("     ");
		query.append("    ) adev ");
		query.append("    GROUP BY adev.location_state_cd ");
		query.append("     ");
		query.append(") devc ");
		query.append("ON st.state_cd = devc.location_state_cd ");
		
		query.append("UNION ALL ");
		
		query.append("SELECT 1 ord_pos, ");
		query.append("st.state_cd, st.state_name, ");
		query.append("adev.device_type_desc, ");
		query.append("adev.dev_count ");
		query.append("FROM ( ");
		query.append("    SELECT cj.location_state_cd, ");
		query.append("    (CASE ");
		query.append("        WHEN cj.e_dev_id = '1' THEN 'eSuds Room Controller: ' ");
		query.append("        ELSE 'eSuds Washer/Dryer: ' ");
		query.append("    END) device_type_desc, ");
		query.append("    (CASE ");
		query.append("        WHEN cj.e_dev_id = '1' THEN cj.room_ctrl_cnt ");
		query.append("        ELSE cj.wash_dry_cnt ");
		query.append("    END) dev_count ");
		query.append("    FROM ( ");
		query.append("        SELECT sa.e_dev_id, ca.location_state_cd, ca.room_ctrl_cnt, ca.wash_dry_cnt ");
		query.append("        FROM ( ");
		query.append("            SELECT  ");
		query.append("            location_state_cd, ");
		query.append("            COUNT(1) room_ctrl_cnt, ");
		query.append("            SUM(endpoint_count) wash_dry_cnt ");
		query.append("            FROM ( ");
		query.append("                SELECT ");
		query.append("                school.location_name school, ");
		query.append("                campus.location_name campus, ");
		query.append("                dorm.location_name dorm,   ");
		query.append("                room.location_name room, ");
		query.append("                school.location_state_cd, ");
		query.append("                dev.device_serial_cd serial_cd, ");
		query.append("                COUNT(1) endpoint_count ");
		query.append("                FROM ");
		query.append("                device.device dev, ");
		query.append("                device.host hst, ");
		query.append("                pss.pos pos, ");
		query.append("                location.location room, ");
		query.append("                location.location dorm, ");
		query.append("                location.location campus, ");
		query.append("                location.location school ");
		query.append("                WHERE room.parent_location_id = dorm.location_id ");
		query.append("                AND dorm.parent_location_id = campus.location_id ");
		query.append("                AND campus.parent_location_id = school.location_id ");
		query.append("                AND (room.location_type_id = 18 OR room.location_id = 1) ");
		query.append("                AND (dorm.location_type_id = 17 OR dorm.location_id = 1) ");
		query.append("                AND (campus.location_type_id = 16 OR campus.location_id = 1) ");
		query.append("                AND (school.location_type_id = 6 OR school.location_id = 1) ");
		query.append("                AND room.location_id = pos.location_id       ");
		query.append("                AND pos.device_id = dev.device_id ");
		query.append("                AND dev.device_id = hst.device_id ");
		query.append("                AND room.location_active_yn_flag = 'Y' ");
		query.append("                AND dev.device_type_id = 5 ");
		query.append("                AND dev.device_active_yn_flag = 'Y' ");
		if (!DialectResolver.isOracle()) {
			query.append("                AND dev.last_activity_ts > LOCALTIMESTAMP - interval '1 day' * 30 ");
		} else {
			query.append("                AND dev.last_activity_ts > SYSDATE - 30 ");
		};	
		query.append("                GROUP BY ");
		query.append("                school.location_name, ");
		query.append("                campus.location_name, ");
		query.append("                dorm.location_name, ");
		query.append("                room.location_name, ");
		query.append("                school.location_state_cd, ");
		query.append("                dev.device_serial_cd ");
		query.append("            ) a ");
		query.append("            GROUP BY ");
		query.append("            location_state_cd ");
		query.append("        ) ca, ");
		query.append("        ( ");
		if (!DialectResolver.isOracle()) {
			query.append("     SELECT generate_series::varchar as e_dev_id ");
			query.append("     FROM generate_series(1,2) ");
		} else {
			query.append("     SELECT TO_CHAR(LEVEL) e_dev_id ");
			query.append("     FROM dual  ");
			query.append("     CONNECT BY LEVEL <= 2 ");
		};
		query.append("        ) sa ");
		query.append("    ) cj ");
		
		query.append("    UNION ALL ");
		
		query.append("    SELECT loc.location_state_cd, (dt.device_type_desc || ': ') device_type_desc, COUNT(1) dev_count ");
		query.append("    FROM device.device dev ");
		query.append("    JOIN pss.pos pos ");
		query.append("    ON dev.device_id = pos.device_id ");
		query.append("    AND dev.device_active_yn_flag = 'Y' ");
		query.append("    AND dev.device_type_id != 5 ");
		if (!DialectResolver.isOracle()) {
			query.append("    AND dev.last_activity_ts > LOCALTIMESTAMP - interval '1 day' * 30 ");
		} else {
			query.append("    AND dev.last_activity_ts > SYSDATE - 30 ");
		};
		query.append("    JOIN device.device_type dt ");
		query.append("    ON dev.device_type_id = dt.device_type_id ");
		query.append("    JOIN location.location loc ");
		query.append("    ON pos.location_id = loc.location_id ");
		query.append("    GROUP BY loc.location_state_cd, dt.device_type_desc ");
		query.append(") adev ");
		query.append("JOIN location.state st ");
		query.append("ON st.state_cd = adev.location_state_cd ");
		
		query.append("ORDER BY 2, 1, 4 ");
		
		// Perform the query
		rs = statement.executeQuery(query.toString());
		
		String last_col_state_cd = "";
		String last_col_dev_count = "0";
		
		String hoverData = "";
		String nameData = "";
		
		boolean sameState;
		
		String col_state_cd = "ZZ";
		String col_state_name = "";
		String col_device_type_desc = "";
		String col_dev_count = "";
		
		
		// Iterate through each row of rs
		while (rs.next()) {
			col_state_cd = rs.getString("state_cd");
			col_state_name = rs.getString("state_name");
			col_device_type_desc = rs.getString("device_type_desc");
			col_dev_count = rs.getString("dev_count");
			
			sameState = col_state_cd.equals(last_col_state_cd);
			
			if (!sameState) {
				
				%><state id="<%= last_col_state_cd %>">
    <name><%= nameData %></name>
    <hover><%= hoverData %></hover>
    <data><%= last_col_dev_count %></data>
  </state>
<%
				if (last_col_state_cd.equals("DC")) {
					%><state id="arc">
	<start>38.9040,-76.9500</start>
	<stop>36.2500,-69.2500</stop>
  </state>
  <state id="point">
    <loc>36.0000,-69.0000</loc>
    <size>11</size>
  </state>
  <state id="point">
    <loc>36.0000,-69.0000</loc>
    <size>10</size>
    <name><%= nameData %></name>
    <hover><%= hoverData %></hover>
    <data><%= last_col_dev_count %></data>
  </state>
<%
				}
				hoverData = "";
				nameData = "&lt;font size='16'&gt;&lt;b&gt;&lt;u&gt;" + col_state_name + "&lt;/u&gt;&lt;/b&gt;&lt;/font&gt;";
			} else {
				
				hoverData += "\n";
			}
			
			if (col_device_type_desc.equals("Total: ")) {
				hoverData += "&lt;b&gt;" + col_device_type_desc + col_dev_count + "&lt;/b&gt;";
			} else {
				hoverData += col_device_type_desc + col_dev_count;
			}
			
			last_col_state_cd = col_state_cd;
			last_col_dev_count = col_dev_count;
		}
		%><state id="<%= col_state_cd %>">
    <name><%= nameData %></name>
    <hover><%= hoverData %></hover>
    <data><%= last_col_dev_count %></data>
  </state>
<%
	} catch (SQLException ex) {
		
		while (ex != null) {
			%><%= "SQL Exception:  " + ex.getMessage() + "\n<br />"%><%
			ex = ex.getNextException ();
		}  // end while
		
	} catch(java.lang.Exception ex) {
		
		%><%= "<HTML>" + "<HEAD><TITLE>" + "Device: Error" + "</TITLE></HEAD>\n<BODY>" + "<P>SQL error in doGet: " + ex.getMessage() + "</P></BODY></HTML>" %><%
		
	}finally{
		ProcessingUtils.closeDbResultSet(log, rs);
		ProcessingUtils.closeDbStatement(log, statement);
		ProcessingUtils.closeDbConnection(log, dbcon);
	}
%></us_states>
