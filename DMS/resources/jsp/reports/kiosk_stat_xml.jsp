<%@ page language="java" import="java.io.*, java.sql.*, java.text.*, java.util.*" %>

<%@page import="com.usatech.layers.common.ProcessingUtils"%>

<%@page import="simple.db.DataLayerMgr"%>

<%@page import="simple.app.DialectResolver"%>
<%@page import="simple.text.StringUtils"%>

<%
	
	response.setContentType("text/xml");    // Response mime type
	
	
	//response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
	//response.setHeader("Pragma","no-cache"); //HTTP 1.0
	
	// this is the fix for the IE SSL / Flash XML bug
	// it can only use these headers for cache control
	response.setHeader("Pragma", "");
	response.setHeader("Cache-Control", "must-revalidate");
	response.setDateHeader ("Expires", 0);
	response.setDateHeader("Last-Modified", System.currentTimeMillis());
	
	final simple.io.Log log = simple.io.Log.getLog();
	
	// white house coordinates = 38.8975,-77.0366
	
	
	String total_days = "7";
	total_days = request.getParameter("total_days") != null ? request.getParameter("total_days") : total_days;
	
	String active_time = "6";
	active_time = request.getParameter("active_time") != null ? request.getParameter("active_time") : active_time;
	
	String cust_id = "-1";
	cust_id = request.getParameter("cust_id") != null ? request.getParameter("cust_id") : cust_id;
	
	String sub_type = "-1";
	sub_type = request.getParameter("sub_type") != null ? request.getParameter("sub_type") : sub_type;
	
	String include_unk = "-1";
	include_unk = request.getParameter("include_unk") != null ? request.getParameter("include_unk") : include_unk;
	
	String subTypeParam = "%25";
	if (!sub_type.equals("-1")) {
		subTypeParam = sub_type;
	}
	
	%><?xml version="1.0" encoding="utf-8"?>
<us_states>
  <state id="range">
    <data>10 - 1000000</data>
    <color>CC0000</color>
  </state>
  <state id="range">
    <data>9</data>
    <color>D42600</color>
  </state>
  <state id="range">
    <data>8</data>
    <color>E06000</color>
  </state>
  <state id="range">
    <data>7</data>
    <color>EC9F00</color>
  </state>
  <state id="range">
    <data>6</data>
    <color>F8D800</color>
  </state>
  <state id="range">
    <data>5</data>
    <color>FFFF00</color>
  </state>
  <state id="range">
    <data>4</data>
    <color>D8F800</color>
  </state>
  <state id="range">
    <data>3</data>
    <color>9FEC00</color>
  </state>
  <state id="range">
    <data>2</data>
    <color>60E000</color>
  </state>
  <state id="range">
    <data>1</data>
    <color>26D400</color>
  </state>
  <state id="range">
    <data>-1000000 - 0</data>
    <color>00CC00</color>
  </state>
  <state id="point_range">
    <data>10 - 1000000</data>
    <color>CC0000</color>
  </state>
  <state id="point_range">
    <data>9</data>
    <color>D42600</color>
  </state>
  <state id="point_range">
    <data>8</data>
    <color>E06000</color>
  </state>
  <state id="point_range">
    <data>7</data>
    <color>EC9F00</color>
  </state>
  <state id="point_range">
    <data>6</data>
    <color>F8D800</color>
  </state>
  <state id="point_range">
    <data>5</data>
    <color>FFFF00</color>
  </state>
  <state id="point_range">
    <data>4</data>
    <color>D8F800</color>
  </state>
  <state id="point_range">
    <data>3</data>
    <color>9FEC00</color>
  </state>
  <state id="point_range">
    <data>2</data>
    <color>60E000</color>
  </state>
  <state id="point_range">
    <data>1</data>
    <color>26D400</color>
  </state>
  <state id="point_range">
    <data>-1000000 - 0</data>
    <color>00CC00</color>
  </state>
  <state id="outline_color">
    <color>330000</color>
  </state>
  <state id="default_color">
    <color>FFFFFF</color>
  </state>
  <state id="background_color">
    <color>FFFFFF</color>
  </state>
  <state id="hover">
    <font_size>12</font_size>
    <font_color>000000</font_color>
    <background_color>EEEEEE</background_color>
  </state>
  <state id="default_point">
    <color>000000</color>
    <size>3</size>
    <opacity>90</opacity>
  </state>
  <state id="scale_points">
    <data>25</data>
  </state>
<%
	
	Connection dbcon = null;
	PreparedStatement statement = null;
	ResultSet rs = null;
	try {		
		dbcon = DataLayerMgr.getConnection("OPER");
		
		StringBuffer query = new StringBuffer();
		
		// format the query
		
		
		query.append("SELECT st.state_cd, ");
		query.append("st.state_name, ");
		query.append("act.total_dev_count, ");
		query.append("act.successful_dev_count, ");
		query.append("(act.total_dev_count - act.successful_dev_count) failing_dev_count, ");
		query.append("ROUND((act.successful_dev_count / act.total_dev_count) * 100, 0) success_per, ");
		query.append("ROUND((act.successful_dev_count / act.total_dev_count) * 100, 2) success_float_per ");
		query.append("FROM ( ");
		query.append("    SELECT loc.location_state_cd, ");
		query.append("    SUM(in_total) total_dev_count, ");
		query.append("    SUM(in_good) successful_dev_count ");
		query.append("    FROM ( ");
		query.append("        SELECT dev.device_id, ");
		query.append("        (CASE ");
		if (!DialectResolver.isOracle()) {
			query.append("            WHEN dev.last_activity_ts > LOCALTIMESTAMP - interval '1 day' * " + '?' /*total_days*/ + " THEN 1 ");
		} else {
			query.append("            WHEN dev.last_activity_ts > SYSDATE - " + '?' /*total_days*/ + " THEN 1 ");
		};	
		query.append("            ELSE 0 ");
		query.append("        END) in_total, ");
		query.append("        (CASE ");
		if (!DialectResolver.isOracle()) {
			query.append("            WHEN dev.last_activity_ts > LOCALTIMESTAMP - interval '1 day' * " + '?' /*active_time*/ + " / 24 THEN 1 ");
		} else {
			query.append("            WHEN dev.last_activity_ts > SYSDATE - " + '?' /*active_time*/ + " / 24 THEN 1 ");
		}
		query.append("            ELSE 0 ");
		query.append("        END) in_good ");
		query.append("        FROM device.device dev ");
		
		if (!sub_type.equals("-1")) {
			query.append("        JOIN ( ");
			query.append("            SELECT ");
			query.append("            kdev.device_id, ");
			query.append("            COALESCE(sdev.device_setting_value, sdev.device_setting_value, 'UNKNOWN') device_setting_value ");
			query.append("            FROM ( ");
			query.append("                SELECT ");
			query.append("                dev.device_id ");
			query.append("                FROM device.device dev ");
			query.append("                WHERE dev.device_active_yn_flag = 'Y' ");
			query.append("                AND dev.device_type_id = 11 ");
			query.append("            ) kdev ");
			query.append("            LEFT OUTER JOIN ( ");
			query.append("                SELECT  ");
			query.append("                ds.device_id, ");
			query.append("                ds.device_setting_value ");
			query.append("                FROM device.device dev ");
			query.append("                JOIN device.device_setting ds ");
			query.append("                ON ds.device_id = dev.device_id ");
			query.append("                AND ds.device_setting_parameter_cd = 'HostType' ");
			query.append("                AND dev.device_active_yn_flag = 'Y' ");
			query.append("                AND dev.device_type_id = 11 ");
			query.append("            ) sdev ");
			query.append("            ON sdev.device_id = kdev.device_id ");
			query.append("            ORDER BY COALESCE(sdev.device_setting_value, sdev.device_setting_value, 'UNKNOWN') ");
			query.append("        ) subt ");
			query.append("        ON subt.device_id = dev.device_id ");
			query.append("        AND subt.device_setting_value = '" + '?' /*sub_type*/ + "' ");
		}
		
		query.append("        WHERE dev.device_type_id = 11 ");
		query.append("        AND dev.device_active_yn_flag = 'Y' ");
		query.append("    ) dcc ");
		query.append("    JOIN pss.pos pos ");
		query.append("    ON dcc.device_id = pos.device_id ");
		
		if (!cust_id.equals("-1")) {
			query.append("    AND pos.customer_id = " + '?' /*cust_id*/ + " ");
		} else {
			query.append("    AND pos.customer_id != 1 ");
		}
		
		query.append("    JOIN location.location loc ");
		query.append("    ON pos.location_id = loc.location_id ");
		
		if (include_unk.equals("-1")) {
			query.append("    AND loc.location_city != 'Unknown' ");
		}
		
		query.append("    GROUP BY loc.location_state_cd ");
		query.append(") act ");
		query.append("JOIN location.state st ");
		query.append("ON st.state_cd = act.location_state_cd ");
		query.append("AND act.total_dev_count > 0 ");
		query.append("ORDER BY st.state_cd ");
		
		
		statement = dbcon.prepareStatement(query.toString());
		statement.setString(1, total_days);
		statement.setString(2, active_time);
		int paramNum = 3;
		if (!sub_type.equals("-1"))
			statement.setString(paramNum++, sub_type);
		if (!cust_id.equals("-1"))
			statement.setString(paramNum++, cust_id);

		// Perform the query
		rs = statement.executeQuery();
		
		
		String hoverData = "";
		String nameData = "";
		
		boolean sameState;
		
		String col_state_cd = "ZZ";
		String col_state_name = "";
		String col_total_dev_count = "";
		String col_successful_dev_count = "";
		String col_failing_dev_count = "";
		String col_success_per = "";
		String col_success_float_per = "";
		
		
		// Iterate through each row of rs
		while (rs.next()) {
			col_state_cd = rs.getString("state_cd");
			col_state_name = rs.getString("state_name");
			col_total_dev_count = rs.getString("total_dev_count");
			col_successful_dev_count = rs.getString("successful_dev_count");
			col_failing_dev_count = rs.getString("failing_dev_count");
			col_success_per = rs.getString("success_per");
			col_success_float_per = rs.getString("success_float_per");
			
			
			hoverData = "";
			
			nameData = "&lt;font size='16'&gt;&lt;b&gt;&lt;u&gt;" + col_state_name + "&lt;/u&gt;&lt;/b&gt;&lt;/font&gt;";
			hoverData += "&lt;u&gt;Activity&lt;/u&gt;\n";
			hoverData += "Last " + StringUtils.encodeForHTML(total_days) + " Days: " + col_total_dev_count + "\n";
			
			
			int activeTime = Integer.parseInt(active_time);
			if (activeTime >= 24) {
				activeTime = activeTime / 24;
				hoverData += "Last " + String.valueOf(activeTime) + " Days: " + col_successful_dev_count + "\n";
			} else {
				hoverData += "Last " + String.valueOf(activeTime) + " Hours: " + col_successful_dev_count + "\n";
			}
			
			hoverData += "Difference: " + col_failing_dev_count + "\n";
			
			hoverData += "&lt;b&gt;Percentage: " + col_success_float_per + "%&lt;/b&gt;";
			
			
			%><state id="<%= col_state_cd %>">
    <name><%= nameData %></name>
    <hover><%= hoverData %></hover>
    <data><%= col_failing_dev_count %></data>
	<url><%
			if (!cust_id.equals("-1")) {
				%>/dashboardReport.i?__report=kiosk_metric_cnt_by_state-cust&Ref%20Period=<%= StringUtils.encodeForURL(total_days) %>&Comp%20Period=<%= StringUtils.encodeForURL(active_time) %>&Sub%20Type=<%= StringUtils.encodeForURL(subTypeParam) %>&State=<%= StringUtils.encodeForURL(col_state_cd) %>&CustId=<%= StringUtils.encodeForURL(cust_id) %>&Include%20Unknown%20City=<%= StringUtils.encodeForURL(include_unk) %><%
			} else {
				%>/dashboardReport.i?__report=kiosk_metric_cnt_by_state&Ref%20Period=<%= StringUtils.encodeForURL(total_days) %>&Comp%20Period=<%= StringUtils.encodeForURL(active_time) %>&Sub%20Type=<%= StringUtils.encodeForURL(subTypeParam) %>&State=<%= StringUtils.encodeForURL(col_state_cd) %>&Include%20Unknown%20City=<%= StringUtils.encodeForURL(include_unk) %><%
			}
%></url>
  </state>
<%
			if (col_state_cd.equals("DC")) {
				%><state id="arc">
	<start>38.9040,-76.9500</start>
	<stop>36.2500,-69.2500</stop>
  </state>
  <state id="point">
    <loc>36.0000,-69.0000</loc>
    <size>11</size>
  </state>
  <state id="point">
    <loc>36.0000,-69.0000</loc>
    <size>10</size>
    <name><%= nameData %></name>
    <hover><%= hoverData %></hover>
    <data><%= col_failing_dev_count %></data>
  </state>
<%
			}
		}
	} catch (SQLException ex) {
		
		while (ex != null) {
			%><%= "SQL Exception:  " + ex.getMessage() + "\n<br />"%><%
			ex = ex.getNextException ();
		}  // end while
		
	} catch(java.lang.Exception ex) {
		
		%><%= "<HTML>" + "<HEAD><TITLE>" + "Device: Error" + "</TITLE></HEAD>\n<BODY>" + "<P>SQL error in doGet: " + ex.getMessage() + "</P></BODY></HTML>" %><%
		
	}finally{
		ProcessingUtils.closeDbResultSet(log, rs);
		ProcessingUtils.closeDbStatement(log, statement);
		ProcessingUtils.closeDbConnection(log, dbcon);
	}
%></us_states>
