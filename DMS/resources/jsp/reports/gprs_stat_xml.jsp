<%@ page language="java" import="java.io.*, java.sql.*, java.text.*, java.util.*" %>

<%@page import="com.usatech.layers.common.ProcessingUtils"%>

<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>

<%	
	response.setContentType("text/xml");    // Response mime type
		
	//response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
	//response.setHeader("Pragma","no-cache"); //HTTP 1.0
	
	// this is the fix for the IE SSL / Flash XML bug
	// it can only use these headers for cache control
	response.setHeader("Pragma", "");
	response.setHeader("Cache-Control", "must-revalidate");
	response.setDateHeader ("Expires", 0);
	response.setDateHeader("Last-Modified", System.currentTimeMillis());
	
	final simple.io.Log log = simple.io.Log.getLog();
	
	// white house coordinates = 38.8975,-77.0366	
	
	String total_days = "7";
	total_days = request.getParameter("total_days") != null ? request.getParameter("total_days") : total_days;
	
	String active_days = "2";
	active_days = request.getParameter("active_days") != null ? request.getParameter("active_days") : active_days;
	
	String call_ins = "25";
	call_ins = request.getParameter("call_ins") != null ? request.getParameter("call_ins") : call_ins;
	
	String cust_id = "-1";
	cust_id = request.getParameter("cust_id") != null ? request.getParameter("cust_id") : cust_id;
	
	String include_unk = "-1";
	include_unk = request.getParameter("include_unk") != null ? request.getParameter("include_unk") : include_unk;
	
%>

<?xml version="1.0" encoding="utf-8"?>
<us_states>
  <state id="range">
    <data>0 - 80</data>
    <color>CC0000</color>
  </state>
  <state id="range">
    <data>81 - 82</data>
    <color>D42600</color>
  </state>
  <state id="range">
    <data>83 - 84</data>
    <color>E06000</color>
  </state>
  <state id="range">
    <data>85 - 86</data>
    <color>EC9F00</color>
  </state>
  <state id="range">
    <data>87 - 88</data>
    <color>F8D800</color>
  </state>
  <state id="range">
    <data>89 - 90</data>
    <color>FFFF00</color>
  </state>
  <state id="range">
    <data>91 - 92</data>
    <color>D8F800</color>
  </state>
  <state id="range">
    <data>93 - 94</data>
    <color>9FEC00</color>
  </state>
  <state id="range">
    <data>95 - 96</data>
    <color>60E000</color>
  </state>
  <state id="range">
    <data>97 - 98</data>
    <color>26D400</color>
  </state>
  <state id="range">
    <data>99 - 1000000</data>
    <color>00CC00</color>
  </state>
  <state id="point_range">
    <data>0 - 80</data>
    <color>CC0000</color>
  </state>
  <state id="point_range">
    <data>81 - 82</data>
    <color>D42600</color>
  </state>
  <state id="point_range">
    <data>83 - 84</data>
    <color>E06000</color>
  </state>
  <state id="point_range">
    <data>85 - 86</data>
    <color>EC9F00</color>
  </state>
  <state id="point_range">
    <data>87 - 88</data>
    <color>F8D800</color>
  </state>
  <state id="point_range">
    <data>89 - 90</data>
    <color>FFFF00</color>
  </state>
  <state id="point_range">
    <data>91 - 92</data>
    <color>D8F800</color>
  </state>
  <state id="point_range">
    <data>93 - 94</data>
    <color>9FEC00</color>
  </state>
  <state id="point_range">
    <data>95 - 96</data>
    <color>60E000</color>
  </state>
  <state id="point_range">
    <data>97 - 98</data>
    <color>26D400</color>
  </state>
  <state id="point_range">
    <data>99 - 1000000</data>
    <color>00CC00</color>
  </state>
  <state id="outline_color">
    <color>330000</color>
  </state>
  <state id="default_color">
    <color>FFFFFF</color>
  </state>
  <state id="background_color">
    <color>FFFFFF</color>
  </state>
  <state id="hover">
    <font_size>12</font_size>
    <font_color>000000</font_color>
    <background_color>EEEEEE</background_color>
  </state>
  <state id="default_point">
    <color>000000</color>
    <size>3</size>
    <opacity>90</opacity>
  </state>
  <state id="scale_points">
    <data>25</data>
  </state>
<%
	
	Connection dbcon = null;
	PreparedStatement statement = null;
	ResultSet rs = null;
	try {
		dbcon = DataLayerMgr.getConnection("OPER");
		
		StringBuffer query = new StringBuffer();
		
		// format the query
		
		query.append("SELECT st.state_cd, ");
		query.append("st.state_name, ");
		query.append("act.total_dev_count, ");
		query.append("act.active_dev_count, ");
		query.append("(act.total_dev_count - act.active_dev_count) active_dif, ");
		query.append("ROUND((act.active_dev_count / act.total_dev_count) * 100, 0) active_per, ");
		query.append("ROUND((act.active_dev_count / act.total_dev_count) * 100, 2) active_float_per ");
		query.append("FROM ( ");
		query.append("    SELECT loc.location_state_cd, ");
		query.append("    SUM(in_total) total_dev_count, ");
		query.append("    SUM(in_good) active_dev_count ");
		query.append("    FROM ( ");
		query.append("        SELECT dcic.device_id, dcic.call_in_count, ");
		query.append("        (CASE ");
		query.append("            WHEN dcic.last_activity_ts > SYSDATE - " + '?' /*total_days*/ + " THEN 1 ");
		query.append("            ELSE 0 ");
		query.append("        END) in_total, ");
		query.append("        (CASE ");
		query.append("            WHEN dcic.last_activity_ts > SYSDATE - " + '?' /*active_days*/ + " THEN 1 ");
		query.append("            ELSE 0 ");
		query.append("        END) in_good ");
		query.append("        FROM ( ");
		query.append("            SELECT dev.device_id, dev.last_activity_ts, COUNT(1) call_in_count ");
		query.append("            FROM device.device dev ");
		query.append("            JOIN device.gprs_device gprs ");
		query.append("            ON dev.device_id = gprs.device_id ");
		query.append("            AND gprs.gprs_device_state_id = 5 ");
		query.append("            AND dev.device_active_yn_flag = 'Y' ");
		query.append("            JOIN device.device_call_in_record cir ");
		query.append("            ON cir.serial_number = dev.device_serial_cd ");
		query.append("            GROUP BY dev.device_id, dev.last_activity_ts ");
		query.append("        ) dcic ");
		query.append("        WHERE dcic.call_in_count > " + '?' /*call_ins*/ + " ");
		query.append("    ) dcc ");
		query.append("    JOIN pss.pos pos ");
		query.append("    ON dcc.device_id = pos.device_id ");
		
		if (!cust_id.equals("-1")) {
			query.append("    AND pos.customer_id = " + '?' /*cust_id*/ + " ");
		} else {
			query.append("    AND pos.customer_id != 1 ");
		}
		
		query.append("    JOIN location.location loc ");
		query.append("    ON pos.location_id = loc.location_id ");
		
		if (include_unk.equals("-1")) {
			query.append("    AND loc.location_city != 'Unknown' ");
		}
		
		query.append("    GROUP BY loc.location_state_cd ");
		query.append(") act ");
		query.append("JOIN location.state st ");
		query.append("ON st.state_cd = act.location_state_cd ");
		query.append("AND act.total_dev_count > 0 ");
		query.append("ORDER BY st.state_cd ");
		
		
		statement = dbcon.prepareStatement(query.toString());
		statement.setString(1, total_days);
		statement.setString(2, active_days);
		statement.setString(3, call_ins);
		if (!cust_id.equals("-1"))
			statement.setString(4, cust_id);

		// Perform the query
		rs = statement.executeQuery();
		
		
		String hoverData = "";
		String nameData = "";
		
		boolean sameState;
		
		String col_state_cd = "ZZ";
		String col_state_name = "";
		String col_total_dev_count = "";
		String col_active_dev_count = "";
		String col_active_dif = "";
		String col_active_per = "";
		String col_active_float_per = "";
		
		
		// Iterate through each row of rs
		while (rs.next()) {
			col_state_cd = rs.getString("state_cd");
			col_state_name = rs.getString("state_name");
			col_total_dev_count = rs.getString("total_dev_count");
			col_active_dev_count = rs.getString("active_dev_count");
			col_active_dif = rs.getString("active_dif");
			col_active_per = rs.getString("active_per");
			col_active_float_per = rs.getString("active_float_per");
			
			
			hoverData = "";
			
			nameData = "&lt;font size='16'&gt;&lt;b&gt;&lt;u&gt;" + col_state_name + "&lt;/u&gt;&lt;/b&gt;&lt;/font&gt;";
			hoverData += "&lt;u&gt;GPRS Device Activity&lt;/u&gt;\n";
			hoverData += "Last " + StringUtils.prepareCDATA(total_days) + " Days: " + col_total_dev_count + "\n";
			hoverData += "Last " + StringUtils.prepareCDATA(active_days) + " Days: " + col_active_dev_count + "\n";
			hoverData += "Difference: " + col_active_dif + "\n";
			
			hoverData += "&lt;b&gt;Change: " + col_active_float_per + "%&lt;/b&gt;";
			
			
			%><state id="<%= col_state_cd %>">
    <name><%= nameData %></name>
    <hover><%= hoverData %></hover>
    <data><%= col_active_per %></data>
	<url><%
			if (!cust_id.equals("-1")) {
				%>/dashboardReport.i?__report=gprs_metric_per_by_state-cust&Ref%20Period=<%= StringUtils.encodeForURL(total_days) %>&Comp%20Period=<%= StringUtils.encodeForURL(active_days) %>&Threshold=<%= StringUtils.encodeForURL(call_ins) %>&State=<%= StringUtils.encodeForURL(col_state_cd) %>&CustId=<%= StringUtils.encodeForURL(cust_id) %><%
			} else {
				%>/dashboardReport.i?__report=gprs_metric_per_by_state&Ref%20Period=<%= StringUtils.encodeForURL(total_days) %>&Comp%20Period=<%= StringUtils.encodeForURL(active_days) %>&Threshold=<%= StringUtils.encodeForURL(call_ins) %>&State=<%= StringUtils.encodeForURL(col_state_cd) %><%
			}
%></url>
  </state>
<%
			if (col_state_cd.equals("DC")) {
				%><state id="arc">
	<start>38.9040,-76.9500</start>
	<stop>36.2500,-69.2500</stop>
  </state>
  <state id="point">
    <loc>36.0000,-69.0000</loc>
    <size>11</size>
  </state>
  <state id="point">
    <loc>36.0000,-69.0000</loc>
    <size>10</size>
    <name><%= nameData %></name>
    <hover><%= hoverData %></hover>
    <data><%= col_active_per %></data>
  </state>
<%
			}
		}
	} catch (SQLException ex) {
		
		while (ex != null) {
			%><%= "SQL Exception:  " + ex.getMessage() + "\n<br />"%><%
			ex = ex.getNextException ();
		}  // end while
		
	} catch(java.lang.Exception ex) {
		
		%><%= "<HTML>" + "<HEAD><TITLE>" + "Device: Error" + "</TITLE></HEAD>\n<BODY>" + "<P>SQL error in doGet: " + ex.getMessage() + "</P></BODY></HTML>" %><%
		
	}finally{
		ProcessingUtils.closeDbResultSet(log, rs);
		ProcessingUtils.closeDbStatement(log, statement);
		ProcessingUtils.closeDbConnection(log, dbcon);
	}
%>
</us_states>
