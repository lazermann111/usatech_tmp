<%@page import="com.usatech.dms.util.Helper"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 
<jsp:useBean id="datePairs" type="simple.results.Results" scope="request" />
<jsp:useBean id="enabledTypes" type="simple.results.Results" scope="request" />
<jsp:useBean id="commMethods" type="simple.results.Results" scope="request" />
<jsp:useBean id="firmwareUpgrades" type="simple.results.Results" scope="request" />
<jsp:useBean id="arrFiles" type="simple.results.Results" scope="request" />
<jsp:useBean id="timeUnits" type="simple.results.Results" scope="request" />
<jsp:useBean id="lastEvents" type="simple.results.Results" scope="request" />
<jsp:useBean id="dms_values_list_deviceTypesList" class="com.usatech.dms.util.DeviceTypeList" scope="application" />

<jsp:useBean id="missingParam" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${(missingParam ne null && missingParam eq 'true') or (errorMessage ne null and errorMessage ne '')}">
		<div class="tableContainer">
			<span class="error">Error Message: <%= errorMessage%></span>
		</div>
	</c:when>
	<c:otherwise>
	<c:set var="pageSize" value="<%=PaginationUtil.DEFAULT_PAGE_SIZE %>" />
	<div class="tableContainer">
	<div class="tableHead">
	<div class="tabHeadTxt">Reports</div>
	</div>
	<table class="tabDataDisplayBorderNoFixedLayout">
		<tr><td colspan="3" class="gridHeader">Devices</td></tr>
		<tr>
 			<form method="get" action="reportsDeviceCountDetails.i" onsubmit="return validateDate(document.getElementById('device_count_from_date'), document.getElementById('device_count_to_date'));">
 			<td class="label">Device Count Details</td>
 			<td>
 				Total number of devices by customer and product from 
 				<input type="text" name="device_count_from_date" id="device_count_from_date" maxlength="10" size="8" value="<%=Helper.getFirstOfMonth()%>" />
 				<img src="/images/calendar.gif" id="device_count_from_date_trigger" class="calendarIcon" title="Date selector" />
 				to <input type="text" name="device_count_to_date" id="device_count_to_date" maxlength="10" size="8" value="<%=Helper.getCurrentDate()%>" />
 				<img src="/images/calendar.gif" id="device_count_to_date_trigger" class="calendarIcon" title="Date selector" />
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<form method="get" action="reportsDeviceActivationChanges.i" onsubmit="return validateDate(document.getElementById('device_activation_from_date'), document.getElementById('device_activation_to_date'));">
 			<td class="label">Device Activation Changes</td>
 			<td>
 				List of devices activated or deactivated from 
 				<input type="text" name="device_activation_from_date" id="device_activation_from_date" maxlength="10" size="8" value="<%=Helper.getFirstOfMonth()%>" />
 				<img src="/images/calendar.gif" id="device_activation_from_date_trigger" class="calendarIcon" title="Date selector" />
 				to <input type="text" name="device_activation_to_date" id="device_activation_to_date" maxlength="10" size="8" value="<%=Helper.getCurrentDate()%>" />
 				<img src="/images/calendar.gif" id="device_activation_to_date_trigger" class="calendarIcon" title="Date selector" />
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
		<tr>
			<form method="post" action="reportsDeviceCountByPostalCode.i" onsubmit="return validateForm(this)">
				<td class="label">Customer Devices by Postal Code</td>
				<td>
					<label>
						Postal codes (1 per line):
						<textarea name="postalCodes" id="postalCodes" rows="3" class="valueList" usatRequired="true" label="Postal codes"></textarea>
					</label>
					<label>
						Search radius (miles):
						<input name="searchRadiusMiles" id="searchRadiusMiles" type="text" value="5" size="2" minValue="0.1" maxValue="100" usatRequired="true" label="Search radius"/>
					</label>
				</td>
				<td align="center"><input type="submit" class="cssButton" value="View" /></td>
			</form>
		</tr>
 		<tr>
 			<form method="get" action="dashboardReport.i">
 			<td class="label">Device Count</td>
 			<td>Total number of devices over time:
 				<select name="__report">
 					<option value="deviceCountOverTime_6mon">Last 6 Months</option>
 					<option value="deviceCountOverTime_1yr">Last Year</option>
 					<option value="deviceCountOverTime_all">From Nov 2003</option>
 				</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<form method="get" action="dashboardReport.i">
 			<td class="label">eSuds Device Count</td>
 			<td>Total number of eSuds devices over time:
 				<select name="__report">
 					<option value="eSudsDevCount_6mon">Last 6 Months</option>
 					<option value="eSudsDevCount_1yr">Last Year</option>
 					<option value="eSudsDevCount_all">From Mar 2004</option>
 				</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<td class="label">eSuds Schools</td>
 			<td>Count of eSuds devices by school, campus, dorm and room</td>
 			<td align="center"><input type="button" class="cssButton" value="View" onclick="window.location = '/dashboardReport.i?__report=esudsSchool';" /></td>
 		</tr>
 		<tr>
 			<td class="label">Active Device Count</td>
 			<td>Count of devices processing transactions</td>
 			<td align="center"><input type="button" class="cssButton" value="View" onclick="window.location = '/dashboardReport.i?__report=deviceCnt';" /></td>
 		</tr>
 		<tr>
 			<td class="label">Device Count by State</td>
 			<td>US Map of devices active in the last 30 days</td>
 			<td align="center"><input type="button" class="cssButton" value="View" onclick="window.location = '/devcount_geo.i';" /></td>
 		</tr>
 		<%if (false) { //removed until device data is migrated into MAIN database%>
 		<tr>
 			<td class="label">GPRS Activity by State</td>
 			<td>US Map of GPRS device activity metrics:
 				<select id="gprsActivityByStateReport">
 					<option value="/gprs_stat_geo.i">Percentage of Active Devices</option>
 					<option value="/gprs_stat_geo_2.i">Count of Failing Devices</option>
 				</select>
 			</td>
 			<td align="center"><input type="button" class="cssButton" value="View" onclick="window.location = document.getElementById('gprsActivityByStateReport').value;" /></td>
 		</tr>
 		<%}%>
 		<tr>
  			<form method="get" action="reportsDeviceRSSILog.i">
  				<input type="hidden" name="pageSize"	value="${pageSize}" />
	  			<td class="label">GPRS Device RSSI Log</td>
	  			<td>	
			    Tolerance RSSI: <input type="text" name="toler_rssi" value="10" size="2" maxlength="2"> &nbsp;
			    Tolerance BER: <input type="text" name="toler_ber" value="0" size="2" maxlength="2"> &nbsp;
	   		   
		  		<select name="enabled" tabindex="5">
					<c:forEach var="nvp_item_enabledType" items="${enabledTypes}">
							<option value="${nvp_item_enabledType.aValue}">${nvp_item_enabledType.aLabel}</option>	    	
					</c:forEach>	
			 	</select>
	
	  			</td>
	 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
  			</form>
 		</tr>
 		<tr>
 			<form method="get" action="reportsWirelessDevices.i">
 			<td class="label">Network Devices</td>
 			<td>
 				Comm Method: 				
 				<select name="comm_method_cd">
					<option value="-">All</option>
					<%
		     		while(commMethods.next()) {
		     		%>
		     		<option value="<%=commMethods.getFormattedValue("aValue")%>"><%=commMethods.getFormattedValue("aLabel")%></option>
		     		<%}%>
			 	</select>
 				&nbsp;Activity Days: <input type="text" name="days" maxlength="4" size="2" value="30" />
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<form method="get" action="reportsFirmwareUpgradeStatus.i">
 			<td class="label">Firmware Upgrade Status</td>
 			<td>Firmware Upgrade: 
 				<select name="firmware_upgrade_id">
					<option value="0">All</option>
					<%
		     		while(firmwareUpgrades.next()) {
		     		%>
		     		<option value="<%=firmwareUpgrades.getFormattedValue("firmwareUpgradeId")%>"><%=firmwareUpgrades.getFormattedValue("firmwareUpgradeName")%></option>
		     		<%}%>
			 	</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<td class="label">Kiosk Activity by State</td>
 			<td>US Map of Kiosk device activity metrics</td>
 			<td align="center"><input type="button" class="cssButton" value="View" onclick="window.location = '/kiosk_stat_geo.i';" /></td>
 		</tr>
 		<tr>
 			<td class="label">Inactive Kiosks</td>
 			<td>Inactive Kiosks by last activity date</td>
 			<td align="center"><input type="button" class="cssButton" value="View" onclick="window.location = '/dashboardReport.i?__report=kiosk_failures';" /></td>
 		</tr>
 		<tr>
 			<form method="get" action="reportsGxDevicesWithPollDEXFlag.i">
 			<td class="label">Gx Devices with Poll DEX Flag</td>
 			<td>Firmware <input type="text" name="minimum_firmware_version" value="6.0.7" maxlength="200" size="7" /> and higher</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<form method="post" action="reportsFirstDeviceActivation.i" onsubmit="return validateForm(this)">
 			<td class="label">First Device Activation</td>
 			<td valign="middle"><label>Serial numbers (1 per line): <textarea name="serialNumbers" id="serialNumbers" rows="3" class="valueList" usatRequired="true" label="Serial numbers"></textarea></label></td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr><td colspan="3" class="gridHeader">Card Authorizations</td></tr>
 		<tr>
 			<form method="get" action="dashboardReport.i">
 			<td class="label">Auth Exec Times</td>
 			<td>Authorization Execution Time statistics:
 				<select name="__report">
 					<option value="authTimes_5min">Last 5 Minutes</option>
 					<option value="authTimes_1hr">Last Hour</option>
 					<option value="authTimes_24hr">Last 24 Hours</option>
 				</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<form method="get" action="dashboardReport.i">
 			<td class="label">Auth Layer Exec Times</td>
 			<td>Authorization Execution Time statistics by Network Layer:
 				<select name="__report">
 					<option value="authLayerTimes_1hr">Last Hour</option>
 					<option value="authLayerTimes_24hr">Last 24 Hours</option>
 				</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
  			<form method="get" action="reportsAuthRtUSAT.i">
				<input type="hidden" name="pageSize"	value="${pageSize}" />
  				<td class="label">Auth Round Trip Report</td>
  				<td>
  				<select name="device_type_id" tabindex="10">
					<c:forEach var="nvp_item_deviceType" items="${dms_values_list_deviceTypesList.list}">
						<option value="${nvp_item_deviceType.value}">${nvp_item_deviceType.name}</option>	    	
					</c:forEach>	
				 </select>
  				Date:  				
  				<select name="auth_date" tabindex="11">
					<c:forEach var="nvp_item_datePair" items="${datePairs}">
						<option value="${nvp_item_datePair.aValue}">${nvp_item_datePair.aLabel}</option>	    	
					</c:forEach>	
		 		</select>
  				</td>
  				<td align="center"><input type="submit" class="cssButton" value="View" /></td>
  			</form>
 		</tr> 		
 		<tr><td colspan="3" class="gridHeader">Payment Metrics</td></tr>
 		<tr>
 			<form method="get" action="dashboardReport.i">
 			<td class="label">Payment Stats</td>
 			<td>Payment statistics:
 				<select name="__report">
 					<option value="paymentStats_5min">Last 5 Minutes</option>
 					<option value="paymentStats_1hr">Last Hour</option>
 					<option value="paymentStats_24hr">Last 24 Hours</option>
 				</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<form method="get" action="dashboardReport.i">
 			<td class="label">Authority Auth Stats</td>
 			<td>Authorization statistics by authority:
 				<select name="__report">
 					<option value="authStats_5min">Last 5 Minutes</option>
 					<option value="authStats_1hr">Last Hour</option>
 					<option value="authStats_24hr">Last 24 Hours</option>
 				</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<form method="get" action="dashboardReport.i">
 			<td class="label">Merchant Auth Stats</td>
 			<td>Authorization statistics by merchant:
 				<select name="__report">
 					<option value="merchAuth_5min">Last 5 Minutes</option>
 					<option value="merchAuth_1hr">Last Hour</option>
 					<option value="merchAuth_24hr">Last 24 Hours</option>
 				</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
 		<tr>
 			<td class="label">Payment Type Counts</td>
 			<td>Count of the configured device payment types</td>
 			<td align="center"><input type="button" class="cssButton" value="View" onclick="window.location = '/dashboardReport.i?__report=paymentSubTypeCnt';" /></td>
 		</tr>
 		<tr><td colspan="3" class="gridHeader">Device Call-In Metrics</td></tr>
 		<%if (false) { //removed until device data is migrated into MAIN database%>
 		<tr>
			<form method="get" action="reportsDeviceCallInAging.i">
				<input type="hidden" name="pageSize"	value="${pageSize}" />	
  				<td class="label">Call-In Aging Report</td>
  				<td>
  				
	  				 <select name="device_type_id" tabindex="1">
						<c:forEach var="nvp_item_deviceType" items="${dms_values_list_deviceTypesList.list}">
								<option value="${nvp_item_deviceType.value}">${nvp_item_deviceType.name}</option>	    	
						</c:forEach>	
					 </select>
  					 &nbsp;
  					<select name="enabled" tabindex="2">
			     		
						<c:forEach var="nvp_item_enabledType" items="${enabledTypes}">
								<option value="${nvp_item_enabledType.aValue}">${nvp_item_enabledType.aLabel}</option>	    	
						</c:forEach>	
					 </select>
  					 &nbsp;
   					<select name="last_event" tabindex="3">
						<c:forEach var="nvp_item_lastEvent" items="${lastEvents}">
							<c:choose>
								<c:when test="${nvp_item_lastEvent.aValue == 2}">
									<option value="${nvp_item_lastEvent.aValue}" selected="selected">${nvp_item_lastEvent.aLabel}</option>	   	
								</c:when>
								<c:otherwise>
									<option value="${nvp_item_lastEvent.aValue}">${nvp_item_lastEvent.aLabel}</option>	    	
								</c:otherwise>
							</c:choose>      	
						</c:forEach>	
					 </select>
					&nbsp; 
   					Activity Days: <input type="text" name="active_days" value="10" size="3" maxlength="3"> &nbsp;
   					Tolerance Days: <input type="text" name="toler_days" value="2" size="3" maxlength="3">
  				</td>
  				<td align="center"><input type="submit" class="cssButton" value="View" /></td>
  			</form>
 		</tr>
 		<%}%>
 		<tr>
  		<form method="get" action="reportsBatchSuccess.i">
  			<input type="hidden" name="pageSize"	value="${pageSize}" />
  			<td class="label">Batch Success Report</td>
  			<td>  			
  			<select name="device_type_id" tabindex="8">
					<c:forEach var="nvp_item_deviceType" items="${dms_values_list_deviceTypesList.list}">
							<option value="${nvp_item_deviceType.value}">${nvp_item_deviceType.name}</option>	    	
					</c:forEach>	
				 </select>
  			Date: 
			
			 <select name="batch_date" tabindex="9">
				<c:forEach var="nvp_item_datePair" items="${datePairs}">
						<option value="${nvp_item_datePair.aValue}">${nvp_item_datePair.aLabel}</option>	    	
				</c:forEach>	
		 	</select>
  			</td>
  			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
  			</form>
 		</tr>
 		<tr>
 			<form method="get" action="dashboardReport.i">
 			<td class="label">Call-In Stats</td>
 			<td>Device Call-In Statistics:
 				<select name="__report">
 					<option value="callIn_1day">Last 1 Day</option>
 					<option value="callIn_3day">Last 3 Days</option>
 					<option value="callIn_7day">Last 7 Days</option>
 				</select>
 			</td>
 			<td align="center"><input type="submit" class="cssButton" value="View" /></td>
 			</form>
 		</tr>
	</table>
	</div>
		
	</c:otherwise>
</c:choose>

<script type="text/javascript" defer="defer">
    Calendar.setup({
        inputField     :    "device_count_from_date",      // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "device_count_from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "device_count_to_date",         // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "device_count_to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "device_activation_from_date",      // id of the input field
        ifFormat       :    "%m/%d/%Y",            // format of the input field
        button         :    "device_activation_from_date_trigger",   // trigger for the calendar (button ID)
        align          :    "B2",                  // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });
    Calendar.setup({
        inputField     :    "device_activation_to_date",         // id of the input field
        ifFormat       :    "%m/%d/%Y",             // format of the input field
        button         :    "device_activation_to_date_trigger",      // trigger for the calendar (button ID)
        align          :    "B2",                   // alignment (defaults to "Bl")
        singleClick    :    true,
        onUpdate       :    "swap_dates"
    });    
    
    function validateDate(fromDate, toDate) {
    	if(!isDate(fromDate.value)) {
        	fromDate.focus();
        	return false;
    	}
    	if(!isDate(toDate.value)) {
        	toDate.focus();
        	return false;
    	}
    	return true;
    }
</script>


<jsp:include page="/jsp/include/footer.jsp" flush="true" />