<%@page import="simple.results.Results"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String action = inputForm.getStringSafely("action", "");
String messageQuery = inputForm.getStringSafely("messageQuery", "");
String appCd = inputForm.getStringSafely("appCd", "");
String appHostname = inputForm.getStringSafely("hostname", "");
String globalSession = inputForm.getStringSafely("globalSession", "");
String deviceIdentifier = inputForm.getStringSafely("deviceIdentifier", "");
String maxCount = inputForm.getStringSafely("maxCount", ""); 

String startDate = "";
String startTime = "";
String endDate = "";
String endTime = "";
Results results = null;	

startTime = inputForm.getString("StartTime", false);
if (StringHelper.isBlank(startTime))
	startTime = Helper.getCurrentTime();

startDate = inputForm.getString("StartDate", false);
if (StringHelper.isBlank(startDate))
	startDate = Helper.getDate(-24);

endDate = inputForm.getString("EndDate", false);
if (StringHelper.isBlank(endDate))
	endDate = Helper.getCurrentDate();

endTime = inputForm.getString("EndTime", false);
if (StringHelper.isBlank(endTime))
	endTime = Helper.getCurrentTime();

Map<String, Object> params = new HashMap<String, Object>();
params.put("startDate", startDate);
params.put("startTime", startTime);
params.put("endDate", endDate);
params.put("endTime", endTime);
params.put("maxCount", maxCount);
if (!StringHelper.isBlank(messageQuery)) {
	params.put("messageTxt", "%" + messageQuery + "%");
}

if (!StringHelper.isBlank(appCd)) {
	params.put("appCd", "%" + appCd + "%");
}

if (!StringHelper.isBlank(appHostname)) {
	params.put("hostname", "%" + appHostname + "%");
}

if (!StringHelper.isBlank(globalSession)) {
	params.put("globalSession", globalSession);
}

if (!StringHelper.isBlank(deviceIdentifier)) {
	int identIdx = deviceIdentifier.indexOf(",");
	if (identIdx != -1) {
		params.put("deviceIdentifier", deviceIdentifier.substring(0, identIdx));
		params.put("deviceSerial", deviceIdentifier.substring(identIdx + 1));
	} else {
		params.put("deviceIdentifier", deviceIdentifier);
	}
}



%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Application Log History</div>
</div>
<form method="get" action="logsHistory.i" onsubmit="return validateForm(this) && validateFilters();">
<table align="center" width="100%" class="tabDataDisplay">
	<tr class="gridHeader">
		<td align="center">
		Date/Time Range
		<table class="noBorder">
			<tr align="left">
				<td>
					<b>Start</b>
				</td> 
				<td style="padding: 0px">
					<input type="text" id="StartDate" size="8" maxlength="10" name="StartDate" value="<%=startDate %>" usatRequired="true" label="Start Date" />
				</td>
				<td style="padding: 0px"> 
					<img src="/images/calendar.gif" id="StartDate_trigger" class="calendarIcon" style="float: left" title="Date selector" />
				</td>
				<td>
					<input type="text" size="6" maxlength="8" id="StartTime" name="StartTime" value="<%=startTime %>" usatRequired="true" label="Start Time" /> 
				</td>
			</tr>
			<tr align="left">
				<td>
					<b>End</b>
				</td>
				<td style="padding: 0px"> 
					<input type="text" id="EndDate" size="8" maxlength="10" name="EndDate" value="<%=endDate %>" usatRequired="true" label="End Date" />
				</td>
				<td style="padding: 0px"> 
					<img src="/images/calendar.gif" id="EndDate_trigger" class="calendarIcon" style="float: left" title="Date selector" />
				</td>
				<td>
					<input type="text" size="6" maxlength="8" id="EndTime" name="EndTime" value="<%=endTime %>" usatRequired="true" label="End Time" />
				</td>
			</tr>
		</table>
		</td>
		<td>
			<table class="noBorder">
				<tr align="left">
					<td>
						<b>Max count of log records</b>
					</td>
					<td>
						<input id="maxCount" type="text" name="maxCount" size="4" value="<%=StringHelper.isBlank(maxCount) ? "100" : maxCount %>" maxlength="5" />
					</td>
					<td>
						<b>Global session code</b>
					</td>
					<td>
						<input type="text" name="globalSession" value="<%=globalSession %>"/>
					</td>
					<td>
						<b>Device name or serial code</b>
					</td>
					<td>
						<input type="text" name="deviceIdentifier" value="<%=deviceIdentifier %>"/>
					</td>
				</tr>
				<tr align="left">
					<td>
						<b>Message text</b>
					</td>
					<td>
						<input type="text" name="messageQuery" value="<%=messageQuery %>" />
					</td>
					<td>
						<b>App code</b>
					</td>
					<td> 
						<input type="text" name="appCd" value="<%=appCd %>" />
					</td>
					<td>
						<b>Hostname</b>
					</td>
					<td> 
						<input type="text" name="hostname" value="<%=appHostname %>" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div class="spacer5"></div>
		<input class="cssButton" type="submit" name="action" value="Submit" />
		<div class="spacer5"></div>
		</td>
	</tr>
</table>
</form>

<script type="text/javascript" defer="defer">
Calendar.setup({
    inputField     :    "StartDate",                  // id of the input field
    ifFormat       :    "%m/%d/%Y",            // format of the input field
    button         :    "StartDate_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                  // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
Calendar.setup({
    inputField     :    "EndDate",                   // id of the input field
    ifFormat       :    "%m/%d/%Y",             // format of the input field
    button         :    "EndDate_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                   // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

var fromDate = document.getElementById("StartDate");
var fromTime = document.getElementById("StartTime");
var toDate = document.getElementById("EndDate");
var toTime = document.getElementById("EndTime");
var maxCountEl = document.getElementById("maxCount");

function validateFilters() {
	if(!isDate(fromDate.value)) {
    	fromDate.focus();
    	return false;
	}
	if(!isTime(fromTime.value)) {
    	fromTime.focus();
    	return false;
	}
	if(!isDate(toDate.value)) {
    	toDate.focus();
    	return false;
	}
	if(!isTime(toTime.value)) {
    	toTime.focus();
    	return false;
	}
	
	var maxCount = parseInt(maxCountEl.value, 10);
	if (isNaN(maxCount) || maxCount > 1000) {
		alert("Incorrect max count format or value. Expected: integer and less 1000");
		maxCountEl.focus();
		return false;
	}
	
	return true;
}

function toggleData(itemId) {
	if (document.getElementById('toggle_' + itemId).innerHTML == '+') {		
		if (document.getElementById('data_' + itemId).innerHTML == '') { 
			getData('type=app_req_params&item_id=' + itemId + '&id=data_' + itemId);
		}
		document.getElementById('toggle_' + itemId).innerHTML = '-';
		document.getElementById('data_' + itemId).style.display = 'block';
	} else {
		document.getElementById('toggle_' + itemId).innerHTML = '+';
		document.getElementById('data_' + itemId).style.display = 'none';
	}
}

</script>

<%if ("Submit".equalsIgnoreCase(action)) {%>
<div class="spacer5"></div>

<table class="tabDataDisplay">
	<tr class="gridHeader">
		<td>Log time</td>
		<td>Hostname</td>
		<td>App CD</td>
		<td>Message</td>
	</tr>

<%
results = DataLayerMgr.executeQuery("GET_LOGS_HISTORY", params, false);
if (results.next()){ 
	int i = 0;
	do{
		i++;
	%>
	<tr class="row<%=i % 2%>">
		<td valign="top"><%=results.getFormattedValue("LOG_TS")%></td>
		<td valign="top"><%=results.getFormattedValue("APP_HOST")%></td>
		<td valign="top"><%=results.getFormattedValue("APP_CD")%></td>
		<td valign="top"><%=results.getFormattedValue("MESSAGE_TXT")%></td>
	</tr>
	<%} while(results.next());%>
<%} else {%>
	<tr><td colspan="12" align="center">NO RECORDS</td></tr>
<%}%>
</table>
<%}%>
		
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />