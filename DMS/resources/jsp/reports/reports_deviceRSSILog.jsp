<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="toler_rssi" class="java.lang.Object" scope="request" />
<jsp:useBean id="toler_ber" class="java.lang.Object" scope="request" />
<jsp:useBean id="enabled" class="java.lang.Object" scope="request" />
<jsp:useBean id="sortIndex" class="java.lang.String" scope="request" />
<jsp:useBean id="call_count" type="java.lang.Object" scope="request" />

<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	
	<c:when test="${not empty(errorMessage)}">
		<div class="tableContainer">				    
			<span class="error">${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>
	
			<c:set var="sortIndex" value="${sortIndex}"/>
			<c:set var="rowClass" value="row0"/>
			<c:set var="i" value="0"/>
			<div align="center">
			<div class="tabDataContent">
			<div class="innerTable" >
					<div class="tableDataHead" align="center">
						<span class="txtWhiteBold">
						GPRS Device RSSI Log</span>
					    </div>								
					<table class="tabDataDisplayBorderNoFixedLayout" width="100%">
						
					<thead>
							<tr class="sortHeader">
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "5", (String)pageContext.getAttribute("sortIndex"))%>">Device Type</a>
									<%=PaginationUtil.getSortingIconHtml("5", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "2", (String)pageContext.getAttribute("sortIndex"))%>">EV Number</a>
									<%=PaginationUtil.getSortingIconHtml("2", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "3", (String)pageContext.getAttribute("sortIndex"))%>">Serial Number</a>
									<%=PaginationUtil.getSortingIconHtml("3", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "4", (String)pageContext.getAttribute("sortIndex"))%>">ICCID</a>
									<%=PaginationUtil.getSortingIconHtml("4", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "1", (String)pageContext.getAttribute("sortIndex"))%>">RSSI</a>
									<%=PaginationUtil.getSortingIconHtml("1", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "6", (String)pageContext.getAttribute("sortIndex"))%>">BER</a>
									<%=PaginationUtil.getSortingIconHtml("6", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "7", (String)pageContext.getAttribute("sortIndex"))%>">GPRS&nbsp;Last&nbsp;Updated</a>
									<%=PaginationUtil.getSortingIconHtml("7", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "8", (String)pageContext.getAttribute("sortIndex"))%>">Last&nbsp;Activity</a>
									<%=PaginationUtil.getSortingIconHtml("8", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								<td>
									<a href="<%=PaginationUtil.getSortingScript(null, "9", (String)pageContext.getAttribute("sortIndex"))%>">Status</a>
									<%=PaginationUtil.getSortingIconHtml("9", (String)pageContext.getAttribute("sortIndex"))%>
								</td>
								
							</tr>
						</thead>
						
						<c:choose>
							<c:when test="${call_count > 0}" >
								<jsp:useBean id="reportResults" type="simple.results.Results" scope="request" />
								<tbody>
								<c:forEach var="report_item" items="${reportResults}">
									<c:choose>
										<c:when test="${i%2 == 0}">
											<c:set var="rowClass" value="row1"/>
										</c:when>
										<c:otherwise>
											<c:set var="rowClass" value="row0"/>
										</c:otherwise>
									</c:choose>
									<c:set var="i" value="${i+1}"/>
									<tr class="${rowClass}">
									    <td nowrap align="left">${report_item.device_type_desc }&nbsp;</td>
									    <td nowrap align="left"><a href="profile.i?device_id=${report_item.device_id }" >${report_item.device_name }</a>&nbsp;</td>
									    <td nowrap align="left">${report_item.device_serial_cd }&nbsp;</td>
									    <td nowrap align="left"><a href="iccidDetail.i?iccid=${report_item.iccid }" >${report_item.iccid }</a>&nbsp;</td>
									    <c:set var="font_color" value="" />
										<c:choose>
											<c:when test="${report_item.rssi < 7}">
												<c:set var="font_color" value="red" />
												<td nowrap align="left"><b><font color="${font_color}">${report_item.rssi }&nbsp;</font></b></td>
											</c:when>
											<c:when test="${report_item.rssi < 10 || report_item.rssi == 99}">
												<c:set var="font_color" value="orange" />
												<td nowrap align="left"><b><font color="${font_color}">${report_item.rssi }&nbsp;</font></b></td>
											</c:when>
											<c:otherwise>
												<c:set var="font_color" value="green" />
												<td nowrap align="left" ><b><font color="${font_color}">${report_item.rssi }&nbsp;</font></b></td>
											</c:otherwise>
										</c:choose>
										<c:set var="font_color" value="" />
										<c:choose>
											<c:when test="${report_item.ber > 3}">
												<c:set var="font_color" value="red" />
												<td nowrap align="left"><b><font color="${font_color}">${report_item.ber }&nbsp;</font></b></td>
											</c:when>
											<c:when test="${report_item.ber > 1}">
												<c:set var="font_color" value="orange" />
												<td nowrap align="left"><b><font color="${font_color}">${report_item.ber }&nbsp;</font></b></td>
											</c:when>
											<c:otherwise>
												<c:set var="font_color" value="green" />
												<td nowrap align="left" ><b><font color="${font_color}">${report_item.ber }&nbsp;</font></b></td>
											</c:otherwise>
										</c:choose>
										
										<c:set var="font_color" value="" />
										<c:choose>
											<c:when test="${report_item.gprs_last_updated_days > 90}">
												<c:set var="font_color" value="red" />
												<td nowrap align="left"><b><font color="${font_color}">${report_item.gprs_last_updated }<br/>(${report_item.gprs_last_updated_days}&nbsp;days)</font></b></td>
											</c:when>
											<c:when test="${report_item.gprs_last_updated_days > 45}">
												<c:set var="font_color" value="orange" />
												<td nowrap align="left"><b><font color="${font_color}">${report_item.gprs_last_updated }<br/>(${report_item.gprs_last_updated_days}&nbsp;days)</font></b></td>
											</c:when>
											<c:otherwise>
												<c:set var="font_color" value="green" />
												<td nowrap align="left" ><b><font color="${font_color}">${report_item.gprs_last_updated }&nbsp;</font></b></td>
											</c:otherwise>
										
										</c:choose>
										
										<c:set var="font_color" value="" />
										<c:choose>
											<c:when test="${report_item.last_activity_days > 5}">
												<c:set var="font_color" value="red" />
												<td nowrap align="left"><b><font color="${font_color}">${report_item.last_activity }<br/>(${report_item.last_activity_days}&nbsp;days)</font></b></td>
											</c:when>
											<c:when test="${report_item.last_activity_days > 2}">
												<c:set var="font_color" value="orange" />
												<td nowrap align="left"><b><font color="${font_color}">${report_item.last_activity }<br/>(${report_item.last_activity_days}&nbsp;days)</font></b></td>
											</c:when>
											<c:otherwise>
												<c:set var="font_color" value="green" />
												<td nowrap align="left" ><b><font color="${font_color}">${report_item.last_activity }&nbsp;</font></b></td>
											</c:otherwise>
										
										</c:choose>
									    <c:choose>
									    	<c:when test="${report_item.device_active_yn_flag eq 'Y'}">
									    		<td nowrap align="left"><B><FONT color="green">Enabled&nbsp;</FONT></B></td>
									    	</c:when>
									    	<c:when test="${report_item.device_active_yn_flag eq 'N'}">
									    		<td nowrap align="left"><B><FONT color="red">Disabled&nbsp;</FONT></B></td>
									    	</c:when>
									    	<c:otherwise>
									    		<td nowrap align="left"><B>Unknown&nbsp;</B></td>
									    	</c:otherwise>
									    </c:choose>														    
									</tr>
								</c:forEach>
			
								</tbody>
							<c:set var="storedNames" value="enabled,toler_rssi,toler_ber,call_count" />	
							
							    <jsp:include page="/jsp/include/pagination.jsp" flush="true">
							        <jsp:param name="_param_request_url" value="reportsDeviceRSSILog.i" />
							    	<jsp:param name="_param_stored_names" value="${storedNames}" />
							    </jsp:include>
							</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="9" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>
		</table>
		</div>
		</div>
		</div>
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />