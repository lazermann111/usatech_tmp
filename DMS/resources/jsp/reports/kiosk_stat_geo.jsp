<%@ page language="java" import="java.io.*, java.sql.*, java.text.*, java.util.Date, java.text.SimpleDateFormat" %>

<%@page import="com.usatech.layers.common.ProcessingUtils"%>

<%@page import="simple.db.DataLayerMgr"%>

<%@page import="simple.app.DialectResolver"%>
<%@page import="simple.text.StringUtils"%>

<jsp:include page="/jsp/include/header.jsp" flush="true">
	<jsp:param name="pageType" value="BIRT" />
</jsp:include>

<%
	response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
	response.setHeader("Pragma","no-cache"); //HTTP 1.0
	response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	
	Date now = new Date();
	SimpleDateFormat df = new SimpleDateFormat("MMM d - HH:mm:ss");
	final simple.io.Log log = simple.io.Log.getLog();
%>
		
<table cellpadding="3" cellspacing="0" border="0" width="100%">
<tr>
	<td>
		<div class="header">Kiosk Activity by State</div>

<div class="boardbox">
<div class="colorbox" style="background-color: #00CC00;"></div><div class="textbox">0</div>

<div class="colorbox" style="background-color: #26D400;"></div><div class="textbox">1</div>
<div class="colorbox" style="background-color: #60E000;"></div><div class="textbox">2</div>
<div class="colorbox" style="background-color: #9FEC00;"></div><div class="textbox">3</div>
<div class="colorbox" style="background-color: #D8F800;"></div><div class="textbox">4</div>

<div class="colorbox" style="background-color: #FFFF00;"></div><div class="textbox">5</div>

<div class="colorbox" style="background-color: #F8D800;"></div><div class="textbox">6</div>
<div class="colorbox" style="background-color: #EC9F00;"></div><div class="textbox">7</div>
<div class="colorbox" style="background-color: #E06000;"></div><div class="textbox">8</div>
<div class="colorbox" style="background-color: #D42600;"></div><div class="textbox">9</div>

<div class="colorbox" style="background-color: #CC0000;"></div><div class="textbox">10+</div>
</div>
<div class="boardbox" id="flashbox">

</div>
<script>
var myAjax = new Ajax.Updater('flashbox', '/flash/kiosk_stat_swf.jswf', {method: 'get'});
function loadFiltered() {
	var uris = '/flash/kiosk_stat_swf.jswf?total_days=' +
		document.cf.totald.options[document.cf.totald.selectedIndex].value +
		'&active_time=' +
		document.cf.activet.options[document.cf.activet.selectedIndex].value +
		'&cust_id=' +
		document.cf.cust.options[document.cf.cust.selectedIndex].value +
		'&sub_type=' +
		document.cf.devst.options[document.cf.devst.selectedIndex].value;
		
	if (document.cf.unk.checked) {
		uris = uris + '&include_unk=true'
	};
	
	myAjax = new Ajax.Updater('flashbox', uris, {method: 'get'});
}
</script>

<div class="footer">

<table cellpadding="2" cellspacing="0" border="0" width="450">
<tr><form name="cf" id="cf" onsubmit="loadFiltered(); return false;">
	<td align="right"><b>Reference Period:</b></td>
	<td><select name="totald" id="totald">
		<option value="2">2 Days</option>
		<option value="3">3 Days</option>
		<option value="4">4 Days</option>
		<option value="5">5 Days</option>
		<option value="6">6 Days</option>
		<option value="7" SELECTED>7 Days</option>
		<option value="10">10 Days</option>
		<option value="15">15 Days</option>
		<option value="20">20 Days</option>
		<option value="25">25 Days</option>
		<option value="30">30 Days</option>
		<option value="60">60 Days</option>
		<option value="90">90 Days</option>
		<option value="180">180 Days</option>
		<option value="365">365 Days</option>
</select></td>
</tr><tr>
	<td align="right"><b>Comparative Period:</b></td>
	<td><select name="activet" id="activet">
		<option value="3">3 Hours</option>
		<option value="6" SELECTED>6 Hours</option>
		<option value="12">12 Hours</option>
		<option value="24">1 Day</option>
		<option value="48">2 Days</option>
		<option value="72">3 Days</option>
</select></td>
</tr><tr>
	<td align="right"><b>Filter by Customer:</b></td>
	<td><select name="cust" id="cust">
		<option value="-1" SELECTED>-- All Customers --</option><%
			Connection dbcon = null;
			Statement statement = null;
			ResultSet rs = null;
			try {
				dbcon = DataLayerMgr.getConnection("OPER");		
				statement = dbcon.createStatement();
				
				StringBuffer query = new StringBuffer();
				
				// format the query
				
				query.append("SELECT DISTINCT ");
				query.append("cus.customer_id, ");
				query.append("cus.customer_name FROM ( ");
				query.append("SELECT cus.customer_id, cus.customer_name, LOWER(cus.customer_name) ");
				query.append("FROM device.device dev ");
				query.append("JOIN pss.pos pos ");
				query.append("ON dev.device_id = pos.device_id ");
				query.append("AND dev.device_active_yn_flag = 'Y' ");
				if (!DialectResolver.isOracle()) {
					query.append("AND dev.last_activity_ts > LOCALTIMESTAMP - interval '1 day' * 30 ");
				} else {
					query.append("AND dev.last_activity_ts > SYSDATE - 30 ");
				};
				query.append("AND dev.device_type_id = 11 ");
				query.append("JOIN location.customer cus ");
				query.append("ON pos.customer_id = cus.customer_id ");
				query.append("AND pos.customer_id != 1 ");
				query.append("ORDER BY LOWER(cus.customer_name) ");
				query.append(") cus ");

				// Perform the query
				rs = statement.executeQuery(query.toString());

				String col_cust_id = "-1";
				String col_cust_name = "";

				// Iterate through each row of rs
				while (rs.next()) {
					col_cust_id = rs.getString("customer_id");
					col_cust_name = rs.getString("customer_name");
		%><option value="<%= col_cust_id %>"><%= StringUtils.encodeForHTML(col_cust_name) %></option>
<%
		}		
%>		
</select></td>
</tr><tr>
	<td align="right"><b>Filter by Kiosk Type:</b></td>
	<td><select name="devst" id="devst">
		<option value="-1" SELECTED>-- All Kiosks --</option><%
	
		query = new StringBuffer();
		
		// format the query
		
		query.append("SELECT DISTINCT ");
		query.append("COALESCE(sdev.device_setting_value, sdev.device_setting_value, 'UNKNOWN') device_setting_value ");
		query.append("FROM ( ");
		query.append("    SELECT ");
		query.append("    dev.device_id ");
		query.append("    FROM device.device dev ");
		query.append("    WHERE dev.device_active_yn_flag = 'Y' ");
		query.append("    AND dev.device_type_id = 11 ");
		query.append(") kdev ");
		query.append("LEFT OUTER JOIN ( ");
		query.append("    SELECT  ");
		query.append("    ds.device_id, ");
		query.append("    ds.device_setting_value ");
		query.append("    FROM device.device dev ");
		query.append("    JOIN device.device_setting ds ");
		query.append("    ON ds.device_id = dev.device_id ");
		query.append("    AND ds.device_setting_parameter_cd = 'HostType' ");
		query.append("    AND dev.device_active_yn_flag = 'Y' ");
		query.append("    AND dev.device_type_id = 11 ");
		query.append(") sdev ");
		query.append("ON sdev.device_id = kdev.device_id ");
		query.append("ORDER BY COALESCE(sdev.device_setting_value, sdev.device_setting_value, 'UNKNOWN') ");
		
		// Perform the query
		rs = statement.executeQuery(query.toString());
		
		String col_device_setting_value = "";
		
		// Iterate through each row of rs
		while (rs.next()) {
			col_device_setting_value = rs.getString("device_setting_value");
			%><option value="<%= col_device_setting_value %>"><%= col_device_setting_value %></option>
<%
		}
	} catch (SQLException ex) {
		
		while (ex != null) {
			%><%= "SQL Exception:  " + ex.getMessage() + "\n<br />"%><%
			ex = ex.getNextException ();
		}  // end while
		
	} catch(java.lang.Exception ex) {
		
		%><%= "<HTML>" + "<HEAD><TITLE>" + "Device: Error" + "</TITLE></HEAD>\n<BODY>" + "<P>SQL error in doGet: " + ex.getMessage() + "</P></BODY></HTML>" %><%
		
	}finally{
		ProcessingUtils.closeDbResultSet(log, rs);
		ProcessingUtils.closeDbStatement(log, statement);
		ProcessingUtils.closeDbConnection(log, dbcon);
	}
%>		
</select></td>
</tr><tr>
	<td align="right"><b>Include Unknown City:</b></td>
	<td><input type="checkbox" name="unk" id="unk"></td>
</tr><tr>
	<td colspan="2" align="center"><input type="submit" name="submit" value="Submit"></td>
</form></tr>
</table>
<br><i><b>Note:</b></i> Mouse over each state to see the exact percentage.<br><i><b>Generated:</b></i> <%= df.format(now) %></div>
</td>
</tr>
</table>		
		
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
