<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="time_value" class="java.lang.Object" scope="request" />
<jsp:useBean id="time_units" class="java.lang.Object" scope="request" />
<jsp:useBean id="device_limit" class="java.lang.Object" scope="request" />
<jsp:useBean id="activeDevicesExist" class="java.lang.Object" scope="request" />

<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	
	<c:when test="${not empty(errorMessage)}">
		<div class="tableContainer">				    
			<span class="error">${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>
	
			
			<div align="center">
			<div class="tableContainer">
			<div class="innerTable" >
					<div class="tableDataHead" align="center">
						<span class="txtWhiteBold">
						Active Devices</span>
					    </div>								
					<table class="tabDataDisplayBorder" width="100%">
						
					<thead>
							<tr class="sortHeader">
								<td>Message Count</td>
								<td>Device Name</td>
								<td>Serial Number</td>
								<td>Device Type</td>
								<td>Location</td>
								<td>Customer</td>
							</tr>
						</thead>
						
						<c:choose>
							<c:when test="${activeDevicesExist eq 'true'}" >
								<jsp:useBean id="activeDevices" type="simple.results.Results" scope="request" />
								<tbody>
								<c:forEach var="report_item" items="${activeDevices}">
									<tr>
									    <td nowrap align="left">${report_item.total }&nbsp;</td>
									    <td nowrap align="left">${report_item.device_name }&nbsp;</td>
									    <td nowrap align="left"><a href="profile.i?device_id=${report_item.device_id }" >${report_item.device_serial_cd }</a>&nbsp;</td>
									    <td nowrap align="left">${report_item.device_type_desc }&nbsp;</td>
									    <td nowrap align="left">${report_item.location_name }&nbsp;</td>
									    <td nowrap align="left">${report_item.customer_name }&nbsp;</td>
									</tr>
									<c:set var="commands" value="${report_item.cmmd}" />
									<c:choose>
										<c:when test="${not empty commands}">
											<tr>
												<td colspan="6">
													<table class="tabDataDisplayNoBorder">
														<c:forEach var="cmmd_item" items="${fn:split(commands, '|')}">
															<tr>
															<c:set var="cnt" value="0" />
															<c:forEach var="cmmd_item_count_cmmd" items="${fn:split(cmmd_item, '~')}">
																	<c:set var="cnt" value="${cnt+1}" />
																	<c:set var="width" value="0" />
																	<c:choose>
																		<c:when test="${cnt == 1}">
																			<c:set var="width" value="5%" />
																		</c:when>
																		<c:otherwise>
																			<c:set var="width" value="95%" />
																		</c:otherwise>
																	</c:choose>
																	<td align="left" width="${width }">
																		<c:out value="${fn:replace(cmmd_item_count_cmmd, '|', '')}" /> 
										                       		</td>
										                    </c:forEach>
										                   </tr>
									                    </c:forEach>
								                    </table>
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<tr>
												<td>No Commands found</td>
											</tr>
										</c:otherwise>
									</c:choose>
								</c:forEach>
			
								</tbody>
						</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="8" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>
		</table>
		</div>
		</div>
		</div>
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />