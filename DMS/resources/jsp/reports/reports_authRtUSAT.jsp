<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<jsp:include page="/jsp/include/header.jsp" flush="true" /> 

<jsp:useBean id="auth_date" class="java.lang.Object" scope="request" />
<jsp:useBean id="device_type_id" class="java.lang.Object" scope="request" />
<jsp:useBean id="authRtUSAT" type="simple.results.Results" scope="request" />

<jsp:useBean id="errorMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="errorMap" class="java.util.HashMap" scope="request" />
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/js/consumer.js") %>"></script>

<c:choose>
	<c:when test="${not empty(errorMap)}">
		<div class="tableContainer">
		<c:forEach var="errorMessage" items="${errorMap}">
				    
			<span class="error">${errorMessage }</span>
		
		</c:forEach>
		</div>
	</c:when>
	
	<c:when test="${not empty(errorMessage)}">
		<div class="tableContainer">				    
			<span class="error">${errorMessage }</span>
		</div>
	</c:when>
	<c:otherwise>
	
			
			<div align="center">
			<div class="tableContainer">
			<div class="innerTable" >
					<div class="tableDataHead" align="center">
						<span class="txtWhiteBold">
						Auth Round Trip Report for ${auth_date }</span>
					    </div>								
					<table class="tabDataDisplayBorder" width="100%">
						
					<thead>
							<tr class="sortHeader">
								<td>Total Auths</td>
								<td>Min Session Time</td>
								<td>Avg Session Time</td>
								<td>Max Session Time</td>
								
							</tr>
						</thead>
						
						<c:choose>
							<c:when test="${fn:length(authRtUSAT) > 0}" >
								
								<tbody>
								<c:forEach var="report_item" items="${authRtUSAT}">
									<tr>
									    <td nowrap align="center">${report_item.auth_count }&nbsp;</td>
									    <td nowrap align="center">${report_item.min_session_time } sec.&nbsp;</td>
									    <td nowrap align="center">${report_item.avg_session_time } sec.&nbsp;</td>
									    <td nowrap align="center">${report_item.max_session_time } sec.&nbsp;</td>
									</tr>
								</c:forEach>
			
								</tbody>
						</c:when>
						<c:otherwise>
							<tbody>
								<tr>
									<td align="center" colspan="8" width="100%">NO RECORDS FOUND</td>
								</tr>
							</tbody>
						</c:otherwise>
					</c:choose>
		</table>
		</div>
		</div>
		</div>
	</c:otherwise>

</c:choose>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />