<%@page import="simple.results.Results"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.dms.util.Helper"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
InputForm inputForm = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String action = inputForm.getStringSafely("action", "");
String appCd = inputForm.getStringSafely("app_cd", "");
String objectTypeCd = inputForm.getStringSafely("object_type_cd", "");
String objectCd = inputForm.getStringSafely("object_cd", "");
String startDate = "";
String startTime = "";
String endDate = "";
String endTime = "";
	
startTime = inputForm.getString("StartTime", false);
if (StringHelper.isBlank(startTime))
	startTime = Helper.getCurrentTime();

startDate = inputForm.getString("StartDate", false);
if (StringHelper.isBlank(startDate))
	startDate = Helper.getDate(-24);

endDate = inputForm.getString("EndDate", false);
if (StringHelper.isBlank(endDate))
	endDate = Helper.getCurrentDate();

endTime = inputForm.getString("EndTime", false);
if (StringHelper.isBlank(endTime))
	endTime = Helper.getCurrentTime();

String accessLogs = inputForm.getString("access_logs", false) != null ? "Y" : "N";

Map<String, Object> params = new HashMap<String, Object>();
params.put("startDate", startDate);
params.put("startTime", startTime);
params.put("endDate", endDate);
params.put("endTime", endTime);
if (!StringHelper.isBlank(appCd))
	params.put("appCd", appCd);
if (!StringHelper.isBlank(objectCd))
	params.put("objectCd", objectCd);
if (!StringHelper.isBlank(objectTypeCd))
	params.put("objectTypeCd", objectTypeCd);
params.put("accessLogs", accessLogs);
%>

<div class="tableContainer">
<div class="tableHead">
<div class="tabHeadTxt">Change History</div>
</div>
<form method="get" action="changeHistory.i" onsubmit="return validateForm(this) && validateDate();">
<table align="center" width="100%" class="tabDataDisplay">
	<tr class="gridHeader">
		<td align="center">
		Date/Time Range
		<table class="noBorder">
			<tr>
				<td align="center">
					<b>Start</b> 
					<input type="text" id="StartDate" size="8" maxlength="10" name="StartDate" value="<%=startDate %>" usatRequired="true" label="Start Date" /> 
					<img src="/images/calendar.gif" id="StartDate_trigger" class="calendarIcon" title="Date selector" />
					<input type="text" size="6" maxlength="8" id="StartTime" name="StartTime" value="<%=startTime %>" usatRequired="true" label="Start Time" /> 
					&nbsp;&nbsp;<b>End</b> 
					<input type="text" id="EndDate" size="8" maxlength="10" name="EndDate" value="<%=endDate %>" usatRequired="true" label="End Date" /> 
					<img src="/images/calendar.gif" id="EndDate_trigger" class="calendarIcon" title="Date selector" />
					<input type="text" size="6" maxlength="8" id="EndTime" name="EndTime" value="<%=endTime %>" usatRequired="true" label="End Time" />
				</td>
			</tr>
		</table>
		</td>
		<td>
			<b>App</b>
			<select name="app_cd">
				<option value="">All</option>
				<option value="DMS"<%if ("DMS".equalsIgnoreCase(appCd)) out.write(" selected=\"selected\"");%>>DMS</option>
				<option value="USALive"<%if ("USALive".equalsIgnoreCase(appCd)) out.write(" selected=\"selected\"");%>>USALive</option>
			</select>
			<b>Object Type</b>
			<select name="object_type_cd">
				<option value="">All</option>
			<%
				Results results = DataLayerMgr.executeQuery("GET_OBJECT_TYPES", null, false);
				while (results.next()) {
			%>
				<option value="<%=results.getFormattedValue("OBJECT_TYPE_CD")%>"<%if (results.getFormattedValue("OBJECT_TYPE_CD").equalsIgnoreCase(objectTypeCd)) out.write(" selected=\"selected\"");%>><%=results.getFormattedValue("OBJECT_TYPE_NAME")%></option>
			<%}%>
			</select>
			<b>Object Code</b> 
			<input type="text" name="object_cd" value="<%=objectCd %>" />
			<input type="checkbox" name="access_logs"<%if ("Y".equalsIgnoreCase(accessLogs)) out.write(" checked=\"checked\"");%> />Access Logs
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div class="spacer5"></div>
		<input class="cssButton" type="submit" name="action" value="Submit" />
		<div class="spacer5"></div>
		</td>
	</tr>
</table>
</form>

<script type="text/javascript" defer="defer">
Calendar.setup({
    inputField     :    "StartDate",                  // id of the input field
    ifFormat       :    "%m/%d/%Y",            // format of the input field
    button         :    "StartDate_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                  // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});
Calendar.setup({
    inputField     :    "EndDate",                   // id of the input field
    ifFormat       :    "%m/%d/%Y",             // format of the input field
    button         :    "EndDate_trigger",  // trigger for the calendar (button ID)
    align          :    "B2",                   // alignment (defaults to "Bl")
    singleClick    :    true,
    onUpdate       :    "swap_dates"
});

var fromDate = document.getElementById("StartDate");
var fromTime = document.getElementById("StartTime");
var toDate = document.getElementById("EndDate");
var toTime = document.getElementById("EndTime");

function validateDate() {
	if(!isDate(fromDate.value)) {
    	fromDate.focus();
    	return false;
	}
	if(!isTime(fromTime.value)) {
    	fromTime.focus();
    	return false;
	}
	if(!isDate(toDate.value)) {
    	toDate.focus();
    	return false;
	}
	if(!isTime(toTime.value)) {
    	toTime.focus();
    	return false;
	}
	return true;
}

function toggleData(itemId) {
	if (document.getElementById('toggle_' + itemId).innerHTML == '+') {		
		if (document.getElementById('data_' + itemId).innerHTML == '') { 
			getData('type=app_req_params&item_id=' + itemId + '&id=data_' + itemId);
		}
		document.getElementById('toggle_' + itemId).innerHTML = '-';
		document.getElementById('data_' + itemId).style.display = 'block';
	} else {
		document.getElementById('toggle_' + itemId).innerHTML = '+';
		document.getElementById('data_' + itemId).style.display = 'none';
	}
}

</script>

<%if ("Submit".equalsIgnoreCase(action)) {%>
<div class="spacer5"></div>

<table class="tabDataDisplay">
	<tr class="gridHeader">
		<td>Time</td>
		<td>App</td>
		<td>URL</td>
		<td>Action</td>
		<td>Username</td>
		<td>IP Address</td>
		<td>Port</td>
		<td>Duration (ms)</td>
		<td>Object Type</td>
		<td>Object Code</td>
		<td>Exception</td>
		<td>Parameters</td>
	</tr>

<%
results = DataLayerMgr.executeQuery("GET_CHANGE_HISTORY", params, false);
if (results.next()){ 
	int i = 0;
	do{
		i++;
	%>
	<tr class="row<%=i % 2%>">
		<td valign="top"><%=results.getFormattedValue("REQUEST_TS")%></td>
		<td valign="top"><%=results.getFormattedValue("APP_CD")%></td>
		<td valign="top"><%=results.getFormattedValue("REQUEST_URL")%></td>
		<td valign="top"><%=results.getFormattedValue("ACTION_NAME")%></td>
		<td valign="top"><%=results.getFormattedValue("USERNAME")%></td>
		<td valign="top"><%=results.getFormattedValue("REMOTE_ADDR")%></td>
		<td valign="top"><%=results.getFormattedValue("REMOTE_PORT")%></td>
		<td valign="top"><%=results.getFormattedValue("PROCESS_TIME_MS")%></td>
		<td valign="top"><%=results.getFormattedValue("OBJECT_TYPE_NAME")%></td>
		<td valign="top"><%=results.getFormattedValue("OBJECT_CD")%></td>
		<td valign="top"><%=results.getFormattedValue("EXCEPTION_MESSAGE")%></td>
		<td valign="top">
			<%
			if (results.getValue("REQUEST_PARAMETERS_LENGTH", Long.class) > 2) {
				String itemId = results.getFormattedValue("APP_REQUEST_ID");
			%>
			<a href="javascript:toggleData('<%=itemId%>');" class="toggle"><span id="toggle_<%=itemId%>">+</span></a>			
			<div id="data_<%=itemId%>"></div>
			<%} else {%>
			&nbsp;			
			<%}%>			
		</td>
	</tr>
	<%} while(results.next());%>
<%} else {%>
	<tr><td colspan="12" align="center">NO RECORDS</td></tr>
<%}%>
</table>
<%}%>
		
</div>

<div class="spacer10"></div>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />