<%@ page language="java" import="java.io.File, java.util.Date, java.text.SimpleDateFormat" %>

<jsp:include page="/jsp/include/header.jsp" flush="true">
	<jsp:param name="pageType" value="BIRT" />
</jsp:include>

<%
	Date now = new Date();
	SimpleDateFormat df = new SimpleDateFormat("MMM d - HH:mm:ss");
%>
		
<table cellpadding="3" cellspacing="0" border="0" width="100%">
<tr>
	<td>
		<div class="header">Device Count by State</div>

<div class="boardbox">
<div class="colorbox" style="background-color: #FFFFFF;"></div><div class="textbox">0</div>
<div class="colorbox" style="background-color: #E0E9F7;"></div><div class="textbox">1-50</div>
<div class="colorbox" style="background-color: #CDDBEE;"></div><div class="textbox">51-100</div>
<div class="colorbox" style="background-color: #B7CAE3;"></div><div class="textbox">101-150</div>
<div class="colorbox" style="background-color: #9EB7D6;"></div><div class="textbox">151-200</div>
<div class="colorbox" style="background-color: #84A4C9;"></div><div class="textbox">201-250</div>
<div class="colorbox" style="background-color: #6A90BD;"></div><div class="textbox">251-300</div>
<div class="colorbox" style="background-color: #517EB0;"></div><div class="textbox">301-350</div>
<div class="colorbox" style="background-color: #3B6DA5;"></div><div class="textbox">351-400</div>
<div class="colorbox" style="background-color: #285F9C;"></div><div class="textbox">401-450</div>
<div class="colorbox" style="background-color: #1B5595;"></div><div class="textbox">451-500</div>
<div class="colorbox" style="background-color: #959595;"></div><div class="textbox">&gt;500</div>
</div>
<div class="boardbox" id="flashbox">

</div>
<script>
var myAjax = new Ajax.Updater('flashbox', 'flash/us.jswf', {method: 'get'});
</script>

<div class="footer"><i><b>Note:</b></i> Mouse over each state to see the exact count.<br><i><b>Generated:</b></i> <%= df.format(now) %></div>

</td>
</tr>
</table>		
		
<jsp:include page="/jsp/include/footer.jsp" flush="true" />
