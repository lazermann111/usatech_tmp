<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.Enumeration" %>

<%@page import="simple.app.DialectResolver" %>

<%@ taglib uri="/birt.tld" prefix="birt" %>

<jsp:include page="/jsp/include/header.jsp" flush="true" />

<%
String reportName = (String) request.getAttribute("__report");
if (reportName == null)
	throw new ServletException("Missing __report parameter");
StringBuilder report = new StringBuilder(reportName);
if (!DialectResolver.isOracle()) 
	report.append("_pgs"); 
if (report.indexOf(".rptdesign") < 0)
	report.append(".rptdesign");
String contentHeight = (String) request.getSession().getAttribute("contentHeight");
Enumeration<String> attributes = request.getAttributeNames();
%>

<birt:viewer id="birtViewer" reportDesign="<%=report.toString()%>" pattern="frameset" format="html" showTitle="false" 
	forceOverwriteDocument="true" height="<%=contentHeight%>" style="width:100%;">
	
<%
if (attributes != null) {
	while (attributes.hasMoreElements()) {
		String attribute = (String) attributes.nextElement();
		if ("__report".equalsIgnoreCase(attribute))
			continue;
%>	
		<birt:param name="<%=attribute%>" value="<%=request.getAttribute(attribute)%>" />
	
<%}}%>
</birt:viewer>

<jsp:include page="/jsp/include/footer.jsp" flush="true" />