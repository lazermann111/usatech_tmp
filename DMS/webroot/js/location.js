/**
 * DMS/LOCATION SPECIFIC DHTML FUNCTIONS
 * These functions used within the location component of DMS.
 *
 * By Ed C (consultant at USATECH)
 * http://www.usatech.com/
 *
 */

	
	/**
* editLocation.jsp - Checks to make sure text is entered on parent location search.
*/
function validateParentChars() {
   var parent_id = document.editLocation.parent_location_lookup_chars.value;
   if(parent_id == '' ) {
	   alert('Please Enter a Location Prefix');
       return false;
   }

   return true;
}

/**
* editLocation.jsp - Makes sure user did not select Undefined parent.
*/
function validateParentLookupId() {
	var selectParentValue = null;
	
	selectParentValue = document.editLocation.parentId[document.editLocation.parentId.selectedIndex].value;
	//alert(selectParentValue);
	if(selectParentValue == 'Undefined' ) {
	   alert('Please Select a Valid Location');
       return false;
    }
       
	return true;
	
}

/**
* editLocation.jsp - Validates location_type and location_name on Save call.
*/
function validateSaveParams(){
   var location_type_id = '';
   var parentLocation = '';
   location_type_id = document.editLocation.typeId[document.editLocation.typeId.selectedIndex].value;
   
   var location_name = '';
   location_name = document.editLocation.name.value;
   if(location_name == null || location_name == ''){
	   alert('Please Enter a Location Name');
       return false;
   }
   
   if(location_type_id == '' || location_type_id == 'Undefined'){
	   alert('Please Enter a Location Type');
       return false;
   }
   return true;
}

/* 
* editLocation.jsp - Submitting param values can somtimes send across the string 'null' instead of ''.(Huh???Why)  
* Make sure if its null we send '' as the param value in the uri. 
* Validate that the submitted param for null, and make blank if so.
*/
function validateSubmitParam(paramVal){
   if(paramVal == null || paramVal == 'null'){
	   paramVal = '';
   }
   return paramVal;
}

/**
* editLocation.jsp - Prepares and executes the Ajax call to get parent location data.
*/ 
	function refreshParentLocationList(locationId){
		if(locationId){
			//no-op
		}else{
			locationId = null;
		}
		
		var sqlString = 'GET_PARENT_LOCATION_FORMATTED';
		var undefinedString = 'true';
		var selectListName = 'parentId';	
	
		// in this case selectListSelectedValue is the parentId
		var selectListSelectedValue = null;
		if(document.editLocation.parentId){
			selectListSelectedValue = document.editLocation.parentId[document.editLocation.parentId.selectedIndex].value;
		}
		
		var lookupChars = document.editLocation.parent_location_lookup_chars.value;
		lookupChars = lookupChars.replace(',', '');
		lookupChars+="\%";
		
		var style = 'font-family: courier; font-size: 12px;';
		
		if(locationId == null || locationId == '' || locationId == 'null'){
			locationId = 0;
		}
		if(selectListSelectedValue == null || selectListSelectedValue == '' || selectListSelectedValue == 'Undefined' || selectListSelectedValue == 'null'){
			selectListSelectedValue = 0;
		}
		/* encodeURIComponent Notation:
		 * have to encode the individual parameters (user character entry) as they may contains special characters such as % or &.
		 * This will be translated back after the HTTP process takes over.
		 * Have to do this at the user entry level, NOT at the entire URI level.
		*/
		lookupChars = encodeURIComponent(lookupChars);
		selectListSelectedValue = encodeURIComponent(selectListSelectedValue);
		selectListName = encodeURIComponent(selectListName);
		//var sqlParams = [lookupChars, lookupChars, selectListSelectedValue, lookupChars, lookupChars, lookupChars, selectListSelectedValue, locationId];
		var sqlParams = [selectListSelectedValue, lookupChars, lookupChars, lookupChars, selectListSelectedValue, locationId];
		var urlAjaxPage = 'getAjaxDropDown.i?';
		var urlAjaxParamString = 'ajax_sql=' + sqlString + '&ajax_params=' + sqlParams + '&ajax_html_field_name=' + selectListName + '&ajax_selected_value=' + selectListSelectedValue + '&ajax_undefined=' + undefinedString + '&ajax_style=' + style;
		var divName = 'parent_location';
		
		/*
		* NOTE: Need to encode the uri as it contains special characters such as %, 
		* this will be translated back when we call get from the request later on
		*/
		
		getAXAHUpdateDiv(urlAjaxPage + urlAjaxParamString, divName);
		document.getElementById('edit_parent_button_div').style.visibility = "visible";
		
	
	}
	
	/**
	* newState.jsp - Makes sure selection was made on Delete button press.
	*/ 
	function validateDeleteButton(){
        var sel="";
        
        for(var i=0;i<document.newState.states.length;i++){
              if(document.newState.states[i].checked){
                    sel=document.newState.states[i].value;
                    break;
              }
        }
        if(sel==""){
        	return false;
        }
        return true;
  	}
	
	/**
	* newState.jsp - Gets the value of the state to Delete.
	*/ 
    function getStateToDelete(){
		var sel="";
  
		for(var i=0;i<document.newState.states.length;i++){
        	if(document.newState.states[i].checked){
            	sel=document.newState.states[i].value;
            	break;
        	}
  		}
  		return sel;
    }
	
	/**
	* newState.jsp - Gets the value of the country when changed.
	*/ 
	function getCountryOnChange(){
    	var sel="";
      
    	for(var i=0;i<document.newState.country_cd.length;i++){
            if(document.newState.country_cd[i].checked){
                  sel=document.newState.country_cd[i].value;
                  break;
            }
      	}
      	return sel;
	}