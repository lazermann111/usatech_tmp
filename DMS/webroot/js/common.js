/**
 * COMMON DHTML FUNCTIONS
 * These are handy functions I use all the time.
 *
 * By Seth Banks (webmaster at subimage dot com)
 * http://www.subimage.com/
 *
 * Up to date code can be found at http://www.subimage.com/dhtml/
 *
 * This code is free for you to use anywhere, just keep this comment block.
 */

/**
 * X-browser event handler attachment and detachment
 * TH: Switched first true to false per http://www.onlinetools.org/articles/unobtrusivejavascript/chapter4.html
 *
 * @argument obj - the object to attach event to
 * @argument evType - name of the event - DONT ADD "on", pass only "mouseover", etc
 * @argument fn - function to call
 */
function addEvent(obj, evType, fn){
	if (obj.addEventListener){
		obj.addEventListener(evType, fn, false);
		return true;
	} else if (obj.attachEvent){
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	} else {
		return false;
	}
}
function removeEvent(obj, evType, fn, useCapture){
	if (obj.removeEventListener){
		obj.removeEventListener(evType, fn, useCapture);
		return true;
	} else if (obj.detachEvent){
		var r = obj.detachEvent("on"+evType, fn);
		return r;
	} else {
		return false;
	}
}

/**
 * Browser independent version of onload, just pass in the function name. 
 * EC: Best practice per http://tiger-inc.com/javascript/execution-after-page-load/
 *
 * @argument func - the name of the function
 */
function myWindowOnload(func) {
    if(window.addEventListener) {
        // See if the browser has the function addEventListener
       window.addEventListener('load', func, false);
    } else if(window.attachEvent) {
        // See if the browser has the function attachEvent
       window.attachEvent('onload', func);
    } else {
        window.alert('Sorry, but your browser is not supported');
    }
}

/**
 * Code below taken from - http://www.evolt.org/article/document_body_doctype_switching_and_more/17/30655/
 *
 * Modified 4/22/04 to work with Opera/Moz (by webmaster at subimage dot com)
 *
 * Gets the full width/height because it's different for most browsers.
 */
function getViewportHeight() {
	if (window.innerHeight!=window.undefined) return window.innerHeight;
	if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;
	if (document.body) return document.body.clientHeight; 

	return window.undefined; 
}
function getViewportWidth() {
	var offset = 17;
	var width = null;
	if (window.innerWidth!=window.undefined) return window.innerWidth; 
	if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth; 
	if (document.body) return document.body.clientWidth; 
}

/**
 * Gets the real scroll top
 */
function getScrollTop() {
	if (self.pageYOffset) // all except Explorer
	{
		return self.pageYOffset;
	}
	else if (document.documentElement && document.documentElement.scrollTop)
		// Explorer 6 Strict
	{
		return document.documentElement.scrollTop;
	}
	else if (document.body) // all other Explorers
	{
		return document.body.scrollTop;
	}
}
function getScrollLeft() {
	if (self.pageXOffset) // all except Explorer
	{
		return self.pageXOffset;
	}
	else if (document.documentElement && document.documentElement.scrollLeft)
		// Explorer 6 Strict
	{
		return document.documentElement.scrollLeft;
	}
	else if (document.body) // all other Explorers
	{
		return document.body.scrollLeft;
	}
}

function createArray() {
	return arguments;
}

function getContainingTable(tobj) {
	return getContainingTag(tobj, "TABLE");
}

function getContainingTag(tobj, tagName) {
	while(tobj != null && tobj.tagName.toUpperCase() != tagName.toUpperCase()) {
		tobj = tobj.parentNode;
	}
	return tobj;
}

function findIndex(arr, obj) {
	for(var i = 0; i < arr.length; i++) {
		if(arr[i] == obj) return i;
	}
	return -1;
}
function hide(elem) {
	if(document.layers) elem.visibility = "hide";
	else elem.style.display = "none";
}
function show(elem) {
	if(document.layers) elem.visibility = "show";
	else elem.style.display = "block";
}
function hideById(id) {
	hide(document.getElementById(id));
}
function showById(id) {
	show(document.getElementById(id));
}

function disableSubmit(form) {
	for(var i = 0; i < form.elements.length; i++) {
		var elem = form.elements[i];
		if((elem.type == "submit" || elem.type == "button") && elem.name == "")
			elem.disabled = true;
	}
	return true;
}

function openCalendar(fieldId){
	showCalendar(document.getElementById(fieldId));
}

function getValue(select) {
	if(document.layers) {
		if(select.selectedIndex == -1) return "";
		return select.options.item(select.selectedIndex).value;
	} else {
		return select.value;
	}
}

function setValue(select, value) {
	if(document.layers) {
		for(var i = 0; i < select.options.length; i++) {
			if(select.options.item(i).value == value) {
				select.selectedIndex = i;
				return;
			}
		}
		select.selectedIndex = -1;
	} else {
		select.value = value;
	}
}

//to submit the form when user press Enter/Return key
function submitWithEnter(e, myform) { // e is event object passed from
	// function invocation
	var characterCode;
	// literal character code will be stored in this variable

	if (e && e.which) {
		// if which property of event object is supported (NN4)
		e = e;
		// character code is contained in NN4's which property
		characterCode = e.which;
	} else {
		e = event;
		// character code is contained in IE's keyCode property
		characterCode = e.keyCode;
	}

	// if generated character code is equal to ascii 13
	if (characterCode == 13) {
		// (if enter key)
		myform.submit(); // submit the form
		return false;
	} else {
		return true;
	}

}

function confirmSubmit(str)
{
	var agree=confirm(str);
	if (agree)
		return true ;
	else
		return false ;
}

function ignoreEnterKey(e) {
	var key;
	
	if (window.event) {
		key = window.event.keyCode;
	} else if (e) {
		key = e.which;
	} else {
		return true;
	}
	
	return key != 13;
}

function clickLinkOnEnter(e, linkId) {
	if (ignoreEnterKey(e))
		return true;
	else {
		document.getElementById(linkId).click();
		return false;
	}
}

/*
 * Allows only "0-9" <input name="number" onKeyPress="return numbersonly(event,
 * false, false)">
 */
function numbersonly(e, decimal, allowEnter) {
	var key;
	var keychar;

	if (window.event) {
		key = window.event.keyCode;
	} else if (e) {
		key = e.which;
	} else {
		return true;
	}
	keychar = String.fromCharCode(key);

	// the empty key, "tab", "backspace" and "Esc" key
	if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 27)) {
		return true;
	} else if ((("0123456789").indexOf(keychar) > -1)) {
		return true;
	} else if (decimal && (keychar == ".")) {
		return true;
	} else if (allowEnter && (key == 13)) {
		return true;
	} else {
		return false;
	}
}

function invalidInput(elem, msg) {
	alert(msg);
	elem.focus();
	elem.addClass("invalidValue");
	elem.select();
}

function trim(s) {
	return s.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '');
}

function validateForm(form) {
	for(var i = 0; i < form.elements.length; i++) {
		if(form.elements[i].type=='hidden')
			continue;
		var elem = $(form.elements[i]);
		var label = elem.getAttribute("label");
		if(typeof(label) == "string" && label.length > 0 && !elem.disabled) {
			if(form.getAttribute("bulk") == "true") {
				var cb = document.getElementById("cb_" + elem.id);
				if(cb && !cb.checked)
					continue;
			}
			var value = $(elem).getValue() || "";
			var required = elem.getAttribute("usatRequired"); 
			if(typeof(required) == "string" && "true" == required && value.length == 0) {
				alert(label + " is a required element. Please enter a value.");
				elem.focus();
				elem.addClass("invalidValue");
				if(elem.select) elem.select();
				return false;
			}
			if(typeof(value) == "string") {
				/* regex match */
				var valid = elem.getAttribute("valid");
				if(typeof(valid) == "string" && (valid=trim(valid)).length > 0 && value.length > 0) {
					if(!value.match(valid)) {
						alert(label + " is not valid. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						if(elem.select) elem.select();
						return false;
					}
				}
				/*less than or equal to max*/
				var maxValue = elem.getAttribute("maxValue");
				if(value.length > 0 && typeof(maxValue) == "string" && (maxValue=trim(maxValue)).length > 0) {
					if(!value.match(/^\-?\d+(\.\d+)?$/)) {
						alert(label + " is not a number. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						if(elem.select) elem.select();
						return false;
					} else if(parseFloat(value) > parseFloat(maxValue)) {
						alert(label + " must be less than or equal to " + maxValue + ". Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						if(elem.select) elem.select();
						return false;
					}
				}
				/* greater than or equal to min*/
				var minValue = elem.getAttribute("minValue");
				if(value.length > 0 && typeof(minValue) == "string" && (minValue=trim(minValue)).length > 0) {
					if(!value.match(/^\-?\d+(\.\d+)?$/)) {
						alert(label + " is not a number. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						if(elem.select) elem.select();
						return false;
					} else if(parseFloat(value) < parseFloat(minValue)) {
						alert(label + " must be greater than or equal to " + minValue + ". Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						if(elem.select) elem.select();
						return false;
					}
				}
			}
			elem.removeClass("invalidValue");
		}
	}
	return true;
}

function toggle(name) {
	alert("in toggle name= " + name);
	var checkBoxFieldName = name + '_chkbx';
	var defaultValueFieldName = name + '_default';
	var originalValueFieldName = name + '_original';
	var currentValueFieldName = name + '_field';
	alert("in toggle currentValueFieldName = " + currentValueFieldName);
	var currentValue = getValue(currentValueFieldName);
	alert("in toggle currentValue= " + currentValue);
	var check_box = document.getElementByName(checkBoxFieldName);
	if(check_box.value = true){
		document.getElementByName(checkBoxName).value = false;
		setValue(document.getElementByName(currentValueFieldName), getValue(document.getElementByName(originalValueFieldName)));
	}else{
		document.getElementsByName(checkBoxName).value = true;
		setValue(document.getElementsByName(originalValueFieldName), currentValue);
		setValue(document.getElementsByName(currentValueFieldName), getValue(document.getElementsByName(defaultValueFieldName)));

	}
}

function getValue(name) {

	alert("in getValue field name = " + name);
	var elem = document.getElementByName(name);
	//var alertType = ""
	//alertText += "Element Type: " + theForm.elements[i].type + "\n"
	alert("in getValue type = " + elem.type);
	if(elem){
		if(elem.type == "text"){
			return elem.value;
		}
		else if(elem.type == "checkbox" || elem.type == "radio"){
			return getCheckedValue(elem);
		}
		else if(elem.type == "select-one"){
			return getSelectValue(elem);
		}else{
			return "";
		}
	}

}

function setValue(elemObj, value) {

	if(elemObj){

		var alertType = ""
			//alertText += "Element Type: " + theForm.elements[i].type + "\n"

			if(elem.type == "text"){
				elem.value = value;
			}
			else if(elem.type == "checkbox" || elem.type == "radio"){
				setCheckedValue(elem, value);
			}
			else if(elem.type == "select-one"){
				setSelectValue(elem, value);
			}else{
				//no-op
			}
	}

}

function setCheckedValue(radioObj, newValue) {
	if(!radioObj)
		return;
	var radioLength = radioObj.length;
	if(radioLength == undefined) {
		radioObj.checked = (radioObj.value == newValue.toString());
		return;
	}
	for(var i = 0; i < radioLength; i++) {
		radioObj[i].checked = false;
		if(radioObj[i].value == newValue.toString()) {
			radioObj[i].checked = true;
		}
	}
}

function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

function setSelectValue(selectName, value) {
	eval('SelectObject = document.' + 
			SelectName + ';');
	for(index = 0; 
	index < SelectObject.length; 
	index++) {
		if(SelectObject[index].value == Value)
			SelectObject.selectedIndex = index;
	}
}

function getSelectValue(selectObj){
	var dropdownValue = "";
	if(selectObj){
		var dropdownIndex = selectObj.selectedIndex;
		var dropdownValue = selectObj[dropdownIndex].value;

	}
	return dropdownValue;

}

function disableForm(form, excludeCtls) {
	form.getFormElements().each(function(el){
		if (!el.name || el.type == 'hidden' || (excludeCtls instanceof Array && excludeCtls.contains(el))) return;
		disable(el);
	});
} 

function enableForm(form, excludeCtls) {
	form.getFormElements().each(function(el){
		if (!el.name || el.type == 'hidden' || (excludeCtls instanceof Array && excludeCtls.contains(el))) return;
		enable(el);
	});
}

function disable(item) {
	/*if(item.className) {
		if(!item.className.match(/\-disabled$/))
			item.className = item.className + "-disabled";
	} else {
		item.className = "disabled";
	}*/
	if (item != null)
		item.disabled = "disabled";
}

function enable(item) {
	/*if(item.className)
	    item.className = item.className.replace(/\-disabled$/, "");
	 */
	if(item != null)
		item.removeAttribute("disabled");
}

function testRegex(ID) {
    var regexid = 'regex_';
    var backrid = 'backr_';
    var testvid = 'testv_';
	if(ID) {
       regexid = 'regex_'+ID;
       backrid = 'backr_'+ID;
       testvid = 'testv_'+ID;
    }
    var regex = document.getElementById(regexid).value;
    var backr = document.getElementById(backrid).value;
    var testv = document.getElementById(testvid).value;

	var regexBrefCodes = new Array('1','1.1','1.2','1.3','1.4','1.5','1.6','1.7','1.8','1.9','10','11','12','13','14','15','16','17','18','19','20','2','3','4','5','6','7','8','9');
	var regexBrefNames = new Array('Primary\ Account\ Number','Primary\ Account\ Number\,\ Part\ 1','Primary\ Account\ Number\,\ Part\ 2','Primary\ Account\ Number\,\ Part\ 3','Primary\ Account\ Number\,\ Part\ 4','Primary\ Account\ Number\,\ Part\ 5','Primary\ Account\ Number\,\ Part\ 6','Primary\ Account\ Number\,\ Part\ 7','Primary\ Account\ Number\,\ Part\ 8','Primary\ Account\ Number\,\ Part\ 9','Issue\ Code\ \(Issue\ Num\,\ Lost\ Card\ Code\,\ etc\.\)','Country\ Code\ \(numeric\)','Check\ Data\:\ Longitude\ Redundancy','Check\ Data\:\ mod\ 10\ \(Luhn\)','Check\ Data\:\ mod\ 97','Group\ Code','Verification\ Value','Service\ Card\ Process\ Code','Zip\ Code','Address','Partial Primary Account Number','Name\ \(Card\ Holder\)','Expiration\ Date\ \(YYMM\ or\ YYYYMM\)','Additional\ Data\ \(service\ code\,\ etc\.\)','Discretionary\ Data\ \(PVKI\,\ PVV\,\ Offset\,\ CVV\,\ CVC\,\ etc\.\)','Use\ and\ Security\ Data\ \(Track\ 3\)','Custom\ Data\ 1','Custom\ Data\ 2','Custom\ Data\ 3');
	var regexBrefDescs = new Array('','','','','','','','','','','','','','','','','','','','','','','','','','');
	var regexBrefTypes = new Array();
	for (var i = 0; i < regexBrefCodes.length; i++) {
		regexBrefTypes[regexBrefCodes[i]] = regexBrefNames[i];
	}

	var myRegex = new RegExp(regex, 'i');
	var myResArr = myRegex.exec(testv);
	var myRegexBref = backr;	
	if (myResArr == null) {
		alert("Invalid Match.\n\nPlease check that you have selected the correct method\nand have entered a correct sample string.");
	}
	else {
		var brefResStr = "";
		if (myRegexBref != '') {
			brefResStr = "\n\nMatched substring results are as follows:\n";
			var myBrefCodeArr = myRegexBref.split('|');
			for (var i = 0; i < myBrefCodeArr.length; i++) {
				var myBrefPartsArr = myBrefCodeArr[i].split(':');	//0=num, 1=db id
				//alert("myBrefPartsArr="+myBrefPartsArr+"\nmyResArr="+myResArr+"\nregexBrefTypes="+regexBrefTypes);
				brefResStr = brefResStr + " - " + regexBrefTypes[myBrefPartsArr[1]] + ": " + myResArr[myBrefPartsArr[0]] + "\n";
			}
		}
		alert('Successful Match!' + brefResStr);
	}
}

function getData(params) {
	var msg = document.getElementById("msg");
	msg.className = "msg";
	msg.innerHTML = "<img src='/images/spinner.gif' style='vertical-align:middle;' /> Please wait, loading data...";
	new Ajax("/getData.i", {method: 'post', async: true, data: params, evalScripts: true }).request();
}

function getPostalInfo(params) {
	new Ajax("postalInfo.i", {method: 'post', async: true, data: params.replace(/\s/gi, ''), evalScripts: true }).request();
}

function deviceNumberTypeChange() {
	if (document.getElementById("device_number_type").value == "serial")
		document.getElementById("tr_terminal_date").style.visibility = "visible";
	else
		document.getElementById("tr_terminal_date").style.visibility = "hidden";
}

function toggleCommissions() {
	var rows = $$('tr.tr_commissions');
	var button = $('button_toggle_commissions')
	if (button.value == 'Show Commissions') {
		rows.setStyle('display', 'table-row');
		button.value = 'Hide Commissions';
	} else {
		rows.setStyle('display', 'none');
		button.value = 'Show Commissions';
	}
}

function validateCardIDs() {
	var startCardId = document.getElementById("start_card_id").value.trim();
	var endCardId = document.getElementById("end_card_id").value.trim();
	if (startCardId != '' && endCardId == '' || startCardId == '' && endCardId != '') {
		alert("Please enter both 'Card Range Start Card ID' and 'Card Range End Card ID'");
		return false;
	} else
		return true;
}
