/* 
 * Creates main leftMenu structure ( parent arrowdiv, arrowdiv_child, onmouseover, onmouseout (with timeouts) 
 * and loads tooltip where needed ).
 */

var menuids=new Array("verticalmenu"); //Enter id(s) of UL menus, separated by commas
var submenuoffset=-2; //Offset of submenus from main menu. Default is -2 pixels.
var topLevelMenuTimeout=20000; //Timeout for onmouseout event in a topLevelMenu (i.e. mouse out of parent "Devices" menu link will hide "ul_devices" block in n seconds).
var subLevelMenuTimeout=2000; //Timeout for onmouseout event in a subLevelMenu (i.e. mouse out of child "Devices", "by Type" menu link will hide the associated child "ul" block in n seconds).

/* Method : createcssmenu
 * Desc :	This method will set up the arrow icons at the topLevelMenu (parent arrow is arrowdiv=white colored arrow)
 * 			and the subLevelMenus (parent arrow is arrowdiv_child=red colored arrow).
 * 
 * 			Additionally this method will set the onmouseover and onmouseout events for topLevelMenu, and subLevelMenu items
 * 			with timeouts.
 * 
 */
function createcssmenu(){
	for (var i=0; i<menuids.length; i++){
	  var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
	    for (var t=0; t<ultags.length; t++){
	    	ultags[t].style.display="none";
	    	/*
	    	 * Create the arrow links per topLevelMenu and subLevelMenu item.
	    	 */
	    	var spanref=document.createElement("span");
			spanref.className="arrowdiv";
			spanref.innerHTML="&nbsp;&nbsp;";
			var parent =	ultags[t].parentNode;
	    	//alert(parent)
			var elemClassName =	ultags[t].parentNode.className;
			var ulElem = ultags[t];
			//alert(elemClassName)
			if(elemClassName=='parent_nav' || elemClassName=='last' || elemClassName=='parent_nav_sub'){
				ultags[t].parentNode.getElementsByTagName("a")[0].appendChild(spanref);				
			}else{
				var spanrefChild=document.createElement("span");
				spanrefChild.className="arrowdiv_child";
				spanrefChild.innerHTML="&nbsp;&nbsp;";
				ultags[t].parentNode.getElementsByTagName("a")[0].appendChild(spanrefChild);
			}
			
			/*
	    	 * Build the onmouseover and onmouseout events for each topLevelMenu and subLevelMenu item,
	    	 * with timeouts.
	    	 */
		    
		    ultags[t].parentNode.onmouseover=function(e){
		    	this.getElementsByTagName("ul")[0].isMouseOver=true;
		    	this.getElementsByTagName("ul")[0].style.display="block";
		    	hideAllOther(this);
		
		    	if(this.toref)
		    		window.clearTimeout(this.toref);
		    }
		    ultags[t].parentNode.onmouseout=function(){
		    	var saveref=this.getElementsByTagName("ul")[0];
		    	saveref.isMouseOver=false;
		    	if(this.toref)
		    		window.clearTimeout(this.toref);
		    
		    	this.toref=window.setTimeout(function(e){
		    		if(!saveref.isMouseOver)
		    			saveref.style.display="none";
		    	},(saveref.parentNode.className=="parent_nav")?topLevelMenuTimeout:subLevelMenuTimeout);
		    }
			
	    }
	}
}

/* Method : hideAllOther
 * Desc:	Called from buildsubmenus, hides all of the subLevelMenu underneath each topLevelMenu if needed.
 * 
 */
function hideAllOther(liobj){
	var p=liobj.parentNode.childNodes;
  
	//alert(liobj.parentNode.childNodes[3]);
	//alert(p);
	var i;
	try{
	  for (i=0; i<p.length; i++){
	    if (liobj != p[i] && "LI"==p[i].tagName && p[i].getElementsByTagName("ul").length)
	      p[i].getElementsByTagName("ul")[0].style.display="none";
	  }
	}catch(e){
	    alert(p[i].innerHTML);
	}
}

/* Method : setupTips
 * Desc :	This method will set up tooltips for each
 * 			topLevelMenu, and any other element that has class="TipsMainMenu".
 * 
 */
function setupTips(){
	try {
		var tipProbe = $$('.TipsMainMenu');
	} catch (e) {
		return;
	}
	
	var wildcard_search_help = 'You can search with no text to list all records and use these wildcard characters:<br /> ^ (starts with), $ (ends with), % (any character).';
	
	//main menu tooltips
	var tooltip_mm_home = $("tooltip_mm_home");
	if (tooltip_mm_home)
		tooltip_mm_home.title = '<span>Home</span> :: <span>Back to home page</span>';
	
	var tooltip_mm_devices = $("tooltip_mm_devices");
	if (tooltip_mm_devices)
		tooltip_mm_devices.title = '<span>Devices</span> :: <span>Tools to search, view, and modify device information</span>';
	
	var tooltip_mm_locations = $("tooltip_mm_locations");
	if (tooltip_mm_locations)
		tooltip_mm_locations.title = '<span>Locations</span> :: <span>Search, create, or edit location information</span>';
	
	var tooltip_mm_customers = $("tooltip_mm_customers");
	if (tooltip_mm_customers)
		tooltip_mm_customers.title = '<span>Customers</span> :: <span>Search, create, or edit customer information</span>';
	
	var tooltip_mm_consumers = $("tooltip_mm_consumers");
	if (tooltip_mm_consumers)
		tooltip_mm_consumers.title = '<span>Consumers</span> :: <span>Search, view, or edit consumer and consumer account information</span>';
	
	var tooltip_mm_consumers2 = $("tooltip_mm_consumers2");
	if (tooltip_mm_consumers2)
		tooltip_mm_consumers2.title = '<span>Consumers</span> :: <span>Search, view, or edit consumer and consumer account information</span>';
	
	var tooltip_mm_cards = $("tooltip_mm_cards");
	if (tooltip_mm_cards)
		tooltip_mm_cards.title = '<span>Cards</span> :: <span>Search, view, or edit card information for given Consumer merchant</span>';
	
	var tooltip_mm_menu_transactions = $("tooltip_mm_menu_transactions");
	if (tooltip_mm_menu_transactions)
		tooltip_mm_menu_transactions.title = '<span>Transactions</span> :: <span>Tools to view and manage transactions and related entities</span>';
	
	var tooltip_mm_transactions = $("tooltip_mm_transactions");
	if (tooltip_mm_transactions)
		tooltip_mm_transactions.title = '<span>Transactions</span> :: <span>Search for authority terminals, batches and transactions</span>';
	
	var tooltip_mm_dfr_rejections = $("tooltip_mm_dfr_rejections");
	if (tooltip_mm_dfr_rejections)
		tooltip_mm_dfr_rejections.title = '<span>DFR Rejections</span> :: <span>Search for DFR rejections</span>';
	
	var tooltip_mm_pending_reversals = $("tooltip_mm_pending_reversals");
	if (tooltip_mm_pending_reversals)
		tooltip_mm_pending_reversals.title = '<span>Pending Reversals</span> :: <span>View, approve or reject Pending Reversals</span>';
	
	var tooltip_mm_file_type = $("tooltip_mm_file_type");
	if (tooltip_mm_file_type)
		tooltip_mm_file_type.title = '<span>File Types</span> :: <span>Search for files transferred by (or to be transferred to) devices</span>';
	
	var tooltip_mm_dfr_files = $("tooltip_mm_dfr_files");
	if (tooltip_mm_dfr_files)
		tooltip_mm_dfr_files.title = '<span>DFR Files</span> :: <span>View Delimited File Reporting files downloaded from Chase Paymentech</span>';
	
	var tooltip_mm_config_templates = $("tooltip_mm_config_templates");
	if (tooltip_mm_config_templates)
		tooltip_mm_config_templates.title = '<span>Configuration Templates</span> :: <span>Edit device configuration or payment templates</span>';

    var tooltip_sm_mass_sim_activation = $("tooltip_sm_mass_sim_activation");
    if (tooltip_sm_mass_sim_activation)
        tooltip_sm_mass_sim_activation.title = '<span>Mass SIM activation</span> :: <span>First time activation of multiple SIM cards</span>';
	
	var tooltip_mm_posm_admin = $("tooltip_mm_posm_admin");
	if (tooltip_mm_posm_admin)
		tooltip_mm_posm_admin.title = '<span>POSM Administration</span> :: <span>Issue commands to control POSM settlements</span>';
	
	var tooltip_mm_reports = $("tooltip_mm_reports");
	if (tooltip_mm_reports)
		tooltip_mm_reports.title = '<span>Reports</span> :: <span>View performance metrics or exception reports</span>';
	
	var tooltip_mm_advanced_tools = $("tooltip_mm_advanced_tools");
	if (tooltip_mm_advanced_tools)
		tooltip_mm_advanced_tools.title = '<span>Advanced Tools</span> :: <span>Access GPRSWeb.<br />Configure various application templates.</span>';
	
 	var tooltip_mm_eport_manager = $("tooltip_mm_risk_alerts");
 	if (tooltip_mm_eport_manager)
 		tooltip_mm_eport_manager.title = '<span>Risk Alerts</span> :: <span>Tranaction monitoring and fraud detection.</span>';
	
 	var tooltip_mm_eport_manager = $("tooltip_mm_sales");
 	if (tooltip_mm_eport_manager)
 		tooltip_mm_eport_manager.title = '<span>Sales</span> :: <span>Sales reps, affiliations, pricing, and categorization.</span>';

 	var tooltip_mm_eport_manager = $("tooltip_mm_eport_manager");
 	if (tooltip_mm_eport_manager)
 		tooltip_mm_eport_manager.title = '<span>ePort Manager</span> :: <span>Manage Bank Accounts, Customers, Dealers, EFTs, License Agreements and Terminals in the Reporting System</span>';

	//devices menu tooltips
	var tooltip_sm_device_search_type = $("tooltip_sm_device_search_type");
	if (tooltip_sm_device_search_type)
		tooltip_sm_device_search_type.title = '<span>Device Search Type</span> :: <span>Select search filter</span>';
	
	var tooltip_sm_device_search_checkbox = $("tooltip_sm_device_search_checkbox");
	if (tooltip_sm_device_search_checkbox)
		tooltip_sm_device_search_checkbox.title = '<span>Device Search Checkbox</span> :: <span>Check to include disabled devices</span>';
	
	var tooltip_sm_device_search = $("tooltip_sm_device_search");
	if (tooltip_sm_device_search)
		tooltip_sm_device_search.title = '<span>Device Search</span> :: <span>Enter a few letters of or a complete value to search. ' + wildcard_search_help + ' Examples:<br /><br /><b>^EE1</b> to find devices with Serial Number starting with EE1<br /><b>24$</b> to find devices with Device Name ending with 24<br /><b>^USA%C$</b> to find devices with Firmware Version starting with USA and ending with C</span>';
	
	var tooltip_sm_devices_load_customer = $("tooltip_sm_devices_load_customer");
	if (tooltip_sm_devices_load_customer)
		tooltip_sm_devices_load_customer.title = '<span>Load Customers</span> :: <span>Click to return to home page with populated \'by Customers\' menu item</span>';
	
	var tooltip_sm_devices_load_location = $("tooltip_sm_devices_load_location");
	if (tooltip_sm_devices_load_location)
		tooltip_sm_devices_load_location.title = '<span>Load Locations</span> :: <span>Click to return to home page with populated \'by Locations\' menu item</span>';
	
	var tooltip_sm_locations_load_esuds_school = $("tooltip_sm_locations_load_esuds_school");
	if (tooltip_sm_locations_load_esuds_school)
		tooltip_sm_locations_load_esuds_school.title = '<span>Load eSuds Schools</span> :: <span>Click to return to home page with populated \'by eSuds School\' menu item</span>';
	
	var tooltip_sm_devices_load_firmware_version = $("tooltip_sm_devices_load_firmware_version");
	if (tooltip_sm_devices_load_firmware_version)
		tooltip_sm_devices_load_firmware_version.title = '<span>Load Firmware Versions</span> :: <span>Click to return to home page with populated \'by Firmware Version\' menu item</span>';
	
	var tooltip_sm_devices_device_configuration_wizard = $("tooltip_sm_devices_device_configuration_wizard");
	if (tooltip_sm_devices_device_configuration_wizard)
		tooltip_sm_devices_device_configuration_wizard.title = '<span>Device Config Wizard</span> :: <span>Configure a variety of settings for multiple devices at the same time</span>';
	
	var tooltip_sm_devices_mass_file_transfers = $("tooltip_sm_devices_mass_file_transfers");
	if (tooltip_sm_devices_mass_file_transfers)
		tooltip_sm_devices_mass_file_transfers.title = '<span>Mass File Transfers</span> :: <span>Upload multiple files to multiple devices</span>';
	
	var tooltip_sm_devices_serial_number_allocations = $("tooltip_sm_devices_serial_number_allocations");
	if (tooltip_sm_devices_serial_number_allocations)
		tooltip_sm_devices_serial_number_allocations.title = '<span>Serial Number Allocations</span> :: <span>Reserve or report reserved serial numbers for any supported device</span>';
	
	var tooltip_sm_devices_device_cloning = $("tooltip_sm_devices_device_cloning");
	if (tooltip_sm_devices_device_cloning)
		tooltip_sm_devices_device_cloning.title = '<span>Device Cloning</span> :: <span>Clone customer, location, and server payment settings from one device to another</span>';
	
	var tooltip_sm_devices_device_init = $("tooltip_sm_devices_device_init");
	if (tooltip_sm_devices_device_init)
		tooltip_sm_devices_device_init.title = '<span>Device Activations</span> :: <span>Initialize devices and deactivate card authorizations in the transaction processing system. Register devices, deactivate devices, clear service fees and activate devices in the reporting system.</span>';
	
	var tooltip_sm_devices_device_messages = $("tooltip_sm_devices_device_messages");
	if (tooltip_sm_devices_device_messages)
		tooltip_sm_devices_device_messages.title = '<span>Device Messages</span> :: <span>View messages sent by a device</span>';
	
	var tooltip_sm_devices_credentials = $("tooltip_sm_devices_credentials");
	if (tooltip_sm_devices_credentials)
		tooltip_sm_devices_credentials.title = '<span>Credentials</span> :: <span>Manage device username/password credentials</span>';
	
	var tooltip_sm_devices_public_keys = $("tooltip_sm_devices_public_keys");
	if (tooltip_sm_devices_public_keys)
		tooltip_sm_devices_public_keys.title = '<span>Public Keys</span> :: <span>Manage Apple VAS and other public keys</span>';
	
	var tooltip_sm_devices_firmware_upgrades = $("tooltip_sm_devices_firmware_upgrades");
	if (tooltip_sm_devices_firmware_upgrades)
		tooltip_sm_devices_firmware_upgrades.title = '<span>Firmware Upgrades</span> :: <span>Manage multiple step device firmware and bezel upgrades</span>';
	
	var tooltip_sm_devices_support = $("tooltip_sm_devices_support");
	if (tooltip_sm_devices_support)
		tooltip_sm_devices_support.title = '<span>Device Support</span> :: <span>Apply MEID and ICCID numbers to devices</span>';
	
	//locations menu tooltips
	var tooltip_sm_locations_search_location_name = $("tooltip_sm_locations_search_location_name");
	if (tooltip_sm_locations_search_location_name)
		tooltip_sm_locations_search_location_name.title = '<span>Search by Location Name</span> :: <span>Enter a few letters of or a complete location name to search. ' + wildcard_search_help + '</span>';
	
	var tooltip_sm_locations_search_location_city = $("tooltip_sm_locations_search_location_city");
	if (tooltip_sm_locations_search_location_city)
		tooltip_sm_locations_search_location_city.title = '<span>Search by Location City</span> :: <span>Enter a few letters of or a complete city name to search. ' + wildcard_search_help +'</span>';
	
	var tooltip_sm_locations_new_location = $("tooltip_sm_locations_new_location");
	if (tooltip_sm_locations_new_location)
		tooltip_sm_locations_new_location.title = '<span>Enter New Location</span> :: <span>Enter a complete location name and click \'Next\' to begin creating a new location<span>';
	
	//customers menu tooltips
	var tooltip_sm_customers_search_customer_name = $("tooltip_sm_customers_search_customer_name");
	if (tooltip_sm_customers_search_customer_name)
		tooltip_sm_customers_search_customer_name.title = '<span>Search by Customer Name</span> :: <span>Enter a few letters of or a complete customer name to search. ' + wildcard_search_help +'</span>';
	
	var tooltip_sm_customers_new_customer = $("tooltip_sm_customers_new_customer");
	if (tooltip_sm_customers_new_customer)
		tooltip_sm_customers_new_customer.title = '<span>Enter New Customer</span> :: <span>Enter a complete customer name and click \'Next\' to begin creating a new customer</span>';
	
	var tooltip_sm_customers_esuds_privileges = $("tooltip_sm_customers_esuds_privileges");
	if (tooltip_sm_customers_esuds_privileges)
		tooltip_sm_customers_esuds_privileges.title = '<span>Esuds Privileges</span> :: <span>Search, create, or edit eSuds web user accounts and privileges</span>';
	
	//files menu tooltips
	var tooltip_sm_files_search_file_name = $("tooltip_sm_files_search_file_name");
	if (tooltip_sm_files_search_file_name)
		tooltip_sm_files_search_file_name.title = '<span>Search by File Name</span> :: <span>Enter a few letters of or a complete file name to search. ' + wildcard_search_help +'</span>';
	
	var tooltip_sm_files_search_file_id = $("tooltip_sm_files_search_file_id");
	if (tooltip_sm_files_search_file_id)
		tooltip_sm_files_search_file_id.title = '<span>Search by File ID</span> :: <span>Enter a complete file id to search</span>';
	
	var tooltip_sm_files_create_ext_file_transfer = $("tooltip_sm_files_create_ext_file_transfer");
	if (tooltip_sm_files_create_ext_file_transfer)
		tooltip_sm_files_create_ext_file_transfer.title = '<span>Create External File Transfer</span> :: <span>Enter a complete Device Name and click \'Next\' to<br />begin creating an external file transfer</span>';
	
	//bank menu tooltips
	var tooltip_sm_bank_search_customer_name = $("tooltip_sm_bank_search_customer_name");
	if (tooltip_sm_bank_search_customer_name)
		tooltip_sm_bank_search_customer_name.title = '<span>Search by Customer Name</span> :: <span>Enter a few letters of or a complete customer name to search. ' + wildcard_search_help +'</span>';
	
	//customer menu tooltips
	var tooltip_sm_customers2_search_customer_name = $("tooltip_sm_customers2_search_customer_name");
	if (tooltip_sm_customers2_search_customer_name)
		tooltip_sm_customers2_search_customer_name.title = '<span>Search by Customer Name</span> :: <span>Enter a few letters of or a complete customer name to search. ' + wildcard_search_help +'</span>';
	
	//dealer menu tooltips
	var tooltip_sm_dealer_search_customer_name = $("tooltip_sm_dealer_search_customer_name");
	if (tooltip_sm_dealer_search_customer_name)
		tooltip_sm_dealer_search_customer_name.title = '<span>Search by Dealer Name</span> :: <span>Enter a few letters of or a complete dealer name to search. ' + wildcard_search_help + '</span>';
	
	//EFT menu tooltips
	var tooltip_sm_pending_eft_search_type = $("tooltip_sm_pending_eft_search_type");
	if (tooltip_sm_pending_eft_search_type)
		tooltip_sm_pending_eft_search_type.title = '<span>Pending EFT Search Type</span> :: <span>Select search filter</span>';
	
	var tooltip_sm_pending_eft_search = $("tooltip_sm_pending_eft_search");
	if (tooltip_sm_pending_eft_search)
		tooltip_sm_pending_eft_search.title = '<span>Pending EFT Search</span> :: <span>Enter a few letters of or a complete value to search. ' + wildcard_search_help +'</span>';
	
	var tooltip_sm_efts_add_adjustments = $("tooltip_sm_efts_add_adjustments");
	if (tooltip_sm_efts_add_adjustments)
		tooltip_sm_efts_add_adjustments.title = '<span>Add Adjustments</span> :: <span>Create EFT adjustments for multiple devices</span>';
	
	var tooltip_sm_fees_update_process_fees = $("tooltip_sm_fees_update_process_fees");
	if (tooltip_sm_fees_update_process_fees)
		tooltip_sm_fees_update_process_fees.title = '<span>Update Process Fees</span> :: <span>Update process fees for multiple devices</span>';
	
	var tooltip_sm_fees_update_service_fees = $("tooltip_sm_fees_update_service_fees");
	if (tooltip_sm_fees_update_service_fees)
		tooltip_sm_fees_update_service_fees.title = '<span>Update Service Fees</span> :: <span>Update service fees for multiple devices</span>';
	
	//license agreement menu tooltips
	var tooltip_sm_license_search_license_name = $("tooltip_sm_license_search_license_name");
	if (tooltip_sm_license_search_license_name)
		tooltip_sm_license_search_license_name.title = '<span>Search by License Name</span> :: <span>Enter a few letters of or a complete license name to search. ' + wildcard_search_help + '</span>';
	
	//terminal menu tooltips
	var tooltip_sm_terminal_search_type = $("tooltip_sm_terminal_search_type");
	if (tooltip_sm_terminal_search_type)
		tooltip_sm_terminal_search_type.title = '<span>Terminal Search Type</span> :: <span>Select search filter</span>';
	
	var tooltip_sm_terminal_search = $("tooltip_sm_terminal_search");
	if (tooltip_sm_terminal_search)
		tooltip_sm_terminal_search.title = '<span>Terminal Search</span> :: <span>Enter a few letters of or a complete value to search. ' + wildcard_search_help + '</span>';
	
	var tooltip_sm_terminals_update_terminals = $("tooltip_sm_terminals_update_terminals");
	if (tooltip_sm_terminals_update_terminals)
		tooltip_sm_terminals_update_terminals.title = '<span>Update Terminals</span> :: <span>Update terminal settings for multiple devices</span>';
	
	var tooltip_sm_terminals_terminal_assignments = $("tooltip_sm_terminals_terminal_assignments");
	if (tooltip_sm_terminals_terminal_assignments)
		tooltip_sm_terminals_terminal_assignments.title = '<span>Terminal Assignments</span> :: <span>Update master user account assignments for multiple devices</span>';
	
	var tooltip_sm_terminals_update_terminals = $("tooltip_sm_terminals_create_fills");
	if (tooltip_sm_terminals_create_fills)
		tooltip_sm_terminals_create_fills.title = '<span>Create Fills</span> :: <span>Create manual fill events</span>';
		
	/* Tips 1 */
	var mainMenuTips = new Tips($$('.TipsMainMenu'),
	{
		showDelay: 400,
		offsets: {'x': 200, 'y': 100},
		fixed:false
	});
}

/* 
 * When user clicks on non-menu portion of the screen, close all open menus.
 */
document.onclick=closeAllMenus;

/*
 * Attribute that represents the last target of the users mouse click.
 */
var clickTarget = '';

/*
 * Capture the last click target.
 */
document.body.onmousedown = function (e) {
	e = e || window.event;
	clickTarget = (e.target || e.srcElement);
}

/* Method : closeAllMenus
 * Desc :	This method will enable the user to click on a non-menu portion of the screen, and clear any open menus.
 * 
 */
function closeAllMenus(){
	if (!isMenuElement(clickTarget, document.getElementById("verticalmenu")))
		createcssmenu();
}

/* Method : isMenuElement
 * Desc : 	Checks to see if the passed in element is a component of the main menu.
 */
function isMenuElement(elem, menuRootUL){
	var parent = elem.parentNode;
	if (!parent)
		return false;
	if (parent == menuRootUL)
		return true;
	return isMenuElement(parent, menuRootUL);
}

/*
 * Load the methods.
 */
myWindowOnload(createcssmenu);
myWindowOnload(setupTips);
