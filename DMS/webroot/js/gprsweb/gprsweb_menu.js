function setupGPRSWebTips(){
	
	//gprs specific menu links
	var elementH = $("gprsweb_homeElem");
	elementH.title = "Home :: Back to home page";
	var elementS = $("gprsweb_searchElem");
	elementS.title = "Search :: Search for SIM card information using a variety of methods";
	var elementReg = $("gprsweb_registerElem");
	elementReg.title = "Register :: Register a new Cingular SIM card order";
	var elementA = $("gprsweb_allocateElem");
	elementA.title = "Allocate :: Allocate a block of new SIM cards";
	var elementUpdate = $("gprsweb_updateElem");
	elementUpdate.title = "Update :: Enable or disable SIM cards";
	var elementAc = $("gprsweb_activateElem");
	elementAc.title = "Activate :: Search for SIM cards to activate";
	var elementU = $("gprsweb_usageElem");
	elementU.title = "Usage :: View reports on SIM card usage, plan information, or exceeded monthly plan limits";
	var elementSt = $("gprsweb_statusElem");
	elementSt.url = "gprswebStatus.i";
	var elementAll = $("gprsweb_allElem");
	elementAll.url = "gprswebSearchResults.i?status=1&action=Search+SIMs";
	var elementRegd = $("gprsweb_registeredElem");
	elementRegd.url = "gprswebSearchResults.i?status=2&action=Search+SIMs";
	var elementAd = $("gprsweb_allocatedElem");
	elementAd.url = "gprswebSearchResults.i?status=4&action=Search+SIMs";
	var elementAcd = $("gprsweb_activatedElem");
	elementAcd.url = "gprswebSearchResults.i?status=3&action=Search+SIMs";
	var elementP = $("gprsweb_pendingElem");
	elementP.url = "gprswebSearchResults.i?status=3&action=Search+SIMs";
	var elementAs = $("gprsweb_assignedElem");
	elementAs.url = "gprswebSearchResults.i?status=5&action=Search+SIMs";
	
	var gprsMenuTips = new Tips($$('.TipsGPRS'), {
		initialize:function(){
			this.fx = new Fx.Style(this.toolTip, 'opacity', {duration: 500, wait: false}).set(0);
		},
		onShow: function(toolTip) {
			this.fx.start(1);
		},
		onHide: function(toolTip) {
			this.fx.start(0);
		}
	});	
}
	
myWindowOnload(setupGPRSWebTips);