//object detection to return the correct object depending upon broswer type. Used by the getAXAH(); function.
function getNewHttpObject() {
    var objType = false;
    try {
        objType = new ActiveXObject('Msxml2.XMLHTTP');
    } catch(e) {
        try {
            objType = new ActiveXObject('Microsoft.XMLHTTP');
        } catch(e) {
            objType = new XMLHttpRequest();
        }
    }
    return objType;
}

//Function used to update page content with new xhtml fragments by using a javascript object, the dom, and http.
function getAXAH(url, arrayOfHTTPRequestStateFunctions, makeRequestSyncronous, theHttpRequest){
// url - exact query string to call (with GET request)
/* arrayOfHTTPRequestStateFunctions - array of null or array elements, with each array element comprised of an array of anonymous sub and (optional) arguments.  The index of the elemenent indicates the execution state that it will be executed at:
	0 = uninitialized
	1 = open
	2 = sent
	3 = receiving
	4 = loaded
*/
// (optional) makeRequestSyncronous - default is to perform action asynchronously; true = perform action synchronously
// (optional) theHttpRequest - new getNewHttpObject() object, if the caller wishes to create it itself

	if (typeof makeRequestSyncronous == "undefined" || makeRequestSyncronous === null)
		makeRequestSyncronous = false;

	var myRandom = parseInt(Math.random()*99999999); // cache buster
	if (typeof theHttpRequest == "undefined" || theHttpRequest === null)
		theHttpRequest = getNewHttpObject();
	theHttpRequest.onreadystatechange = function() {processAXAH(arrayOfHTTPRequestStateFunctions);};
	theHttpRequest.open("GET", url + "&rand__param=" + myRandom, !makeRequestSyncronous);
	theHttpRequest.send(false);
	function processAXAH(funcArr) {
		var state = theHttpRequest.readyState;
		if (funcArr != null && funcArr[theHttpRequest.readyState] != null)
			getAXAH_func_dofunction(funcArr[theHttpRequest.readyState]);
	}
}

function getAXAHUpdateDiv(url, elementContainer, makeRequestSyncronous) {
//url - same as getAXAH() url
//elementContainer - id of DIV element to update 
//makeRequestSyncronous - same as getAXAH() makeRequestSyncronous

	var theHttpRequest = getNewHttpObject();
	var myArr = new Array(
		null,
		new Array(
			function (elementContainer) {
				document.getElementById(elementContainer).innerHTML = "<img src=\"/images/progress_bar.gif\">";
			},
			elementContainer
		),
		null, 
		null,
		new Array(
			function (theHttpRequest, elementContainer) {
			   if (theHttpRequest.status == 200) {
				   document.getElementById(elementContainer).innerHTML = theHttpRequest.responseText;
			   } else {
				   document.getElementById(elementContainer).innerHTML="<span class=\"imageviewtitle\">The requested content is currently unavailable.<\/span>";
			   }
			},
			theHttpRequest,
			elementContainer
		)
	);
	getAXAH(url, myArr, makeRequestSyncronous, theHttpRequest);
}

//internal helper function
function getAXAH_func_dofunction(params) {
	var call = "params[0](";
	for(i = 1; i < params.length-1; i++)
		call += "params["+i+"],";
	call += "params["+(params.length-1)+"]);";
	return eval(call);
}
