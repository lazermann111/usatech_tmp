/**
 * DMS/CONSUMER SPECIFIC DHTML FUNCTIONS
 * These functions used within the consumer component of DMS.
 *
 * By Ed C (consultant at USATECH)
 * http://www.usatech.com/
 *
 */
	
	/**
	 * consumerSearch.jsp - Validates the existence of a value for the assigned location.
	 */
	function validateAssignedChars() {
       var assigned_id = document.searchConsumer.consumer_assigned_location_lookup_chars.value;
       if(assigned_id == '' ) {
    	   alert('Please Enter a Location Prefix');
           return false;
       }

       return true;
	}
	
	/**
	 * consumerSearchResults.jsp - Builds list of request params for search.
	 * @return
	 */
   	function buildSearchParamList() {
       var paramString = '';
       
       paramString = paramString+'&consumer_card_number='+encodeURIComponent(document.searchConsumer.consumer_card_number.value);
       paramString = paramString+'&account_number='+encodeURIComponent(document.searchConsumer.account_number.value);
       paramString = paramString+'&start_card_id='+encodeURIComponent(document.searchConsumer.start_card_id.value);
       paramString = paramString+'&end_card_id='+encodeURIComponent(document.searchConsumer.end_card_id.value);

       paramString = paramString+'&consumer_first_name_search_char='+document.searchConsumer.consumer_first_name_search_char[document.searchConsumer.consumer_first_name_search_char.selectedIndex].value;
       paramString = paramString+'&consumer_first_name='+encodeURIComponent(document.searchConsumer.consumer_first_name.value);

       paramString = paramString+'&consumer_last_name_search_char='+document.searchConsumer.consumer_last_name_search_char[document.searchConsumer.consumer_last_name_search_char.selectedIndex].value;
       paramString = paramString+'&consumer_last_name='+encodeURIComponent(document.searchConsumer.consumer_last_name.value);
       //alert(paramString);

       paramString = paramString+'&consumer_primary_email_search_char='+document.searchConsumer.consumer_primary_email_search_char[document.searchConsumer.consumer_primary_email_search_char.selectedIndex].value;
       paramString = paramString+'&consumer_primary_email='+encodeURIComponent(document.searchConsumer.consumer_primary_email.value);

       paramString = paramString+'&consumer_notification_email_search_char='+document.searchConsumer.consumer_notification_email_search_char[document.searchConsumer.consumer_notification_email_search_char.selectedIndex].value;
       paramString = paramString+'&consumer_notification_email='+encodeURIComponent(document.searchConsumer.consumer_notification_email.value);
      
       var paramStringAssignedLocationId = '';
       if(document.searchConsumer.consumer_assigned_location_id){
    	   paramStringAssignedLocationId = document.searchConsumer.consumer_assigned_location_id[document.searchConsumer.consumer_assigned_location_id.selectedIndex].value;
       		if(paramStringAssignedLocationId == 'Undefined' || paramStringAssignedLocationId == '' || paramStringAssignedLocationId == 'null' || paramStringAssignedLocationId == null){
       			//no-op
       		}else{
       		 	
	       		paramString = paramString+'&consumer_assigned_location_id='+encodeURIComponent(paramStringAssignedLocationId);
	       		
       		}
       }
       
       paramString = paramString+'&consumer_assigned_location_lookup_chars='+encodeURIComponent(document.searchConsumer.consumer_assigned_location_lookup_chars.value);
       
       var paramConsumerMerchantId = '';
       if(document.searchConsumer.consumer_merchant_id){
    	   paramConsumerMerchantId = document.searchConsumer.consumer_merchant_id[document.searchConsumer.consumer_merchant_id.selectedIndex].value;
      		if(paramConsumerMerchantId == 'Undefined' || paramConsumerMerchantId == '' || paramConsumerMerchantId == 'null' || paramConsumerMerchantId == null){
   	   			//no-op
      		}else{
	      		paramString = paramString+'&consumer_merchant_id='+encodeURIComponent(paramConsumerMerchantId);
      		}
      }
      paramString = paramString+'&consumer_acct_type_id='+document.searchConsumer.consumer_acct_type_id.value;
      paramString = paramString+'&consumer_acct_sub_type_id='+document.searchConsumer.consumer_acct_sub_type_id.value;
	  paramString = (paramString);
      return paramString;
       
  	}
   	
   	/**
   	 * consumerSearch.jsp - Validates and corrects undefined or null values for params.
   	 * @return
   	 */
   	function validateSearchParamList() {       
        var paramStringAssignedLocationId = '';
        if(document.searchConsumer.consumer_assigned_location_id){
     	   paramStringAssignedLocationId = document.searchConsumer.consumer_assigned_location_id[document.searchConsumer.consumer_assigned_location_id.selectedIndex].value;
        		if(paramStringAssignedLocationId == 'Undefined' || paramStringAssignedLocationId == 'null'){
        			//no-op
        			document.searchConsumer.consumer_assigned_location_id[document.searchConsumer.consumer_assigned_location_id.selectedIndex].value = "";
        		}
        		
        }
        
        var paramConsumerMerchantId = '';
        
        if(document.searchConsumer.consumer_merchant_id){
     	   paramConsumerMerchantId = document.searchConsumer.consumer_merchant_id[document.searchConsumer.consumer_merchant_id.selectedIndex].value;
       		if(paramConsumerMerchantId == 'Undefined' || paramConsumerMerchantId == 'null'){
    	   			//no-op
    	   			
       			document.searchConsumer.consumer_merchant_id[document.searchConsumer.consumer_merchant_id.selectedIndex].value = "";
       			
       		}
       }
 	  
       return true;
        
   	}
   	
   	/**
   	 * consumerSearch.jsp - Prepares and executes the Ajax call to get assigned location data.
   	 */
  	function refreshAssignedLocationList(){
		
		var sqlString = 'GET_ASSIGNED_LOCATION_FORMATTED';
		var undefinedString = 'true';
		var selectListName = 'consumer_assigned_location_id';	

		// in this case selectListSelectedValue is the parent_location_id
		var selectListSelectedValue = null;
		if(document.searchConsumer.consumer_assigned_location_id){
			selectListSelectedValue = document.searchConsumer.consumer_assigned_location_id[document.searchConsumer.consumer_assigned_location_id.selectedIndex].value;
		}
		
		var lookupChars = document.searchConsumer.consumer_assigned_location_lookup_chars.value;
		lookupChars = lookupChars.replace(',', '');
		lookupChars+="\%";
		/* encodeURIComponent Notation:
		 * have to encode the individual parameters (user character entry) as they may contains special characters such as % or &.
		 * This will be translated back after the HTTP process takes over.
		 * Have to do this at the user entry level, NOT at the entire URI level.
		*/
		lookupChars = encodeURIComponent(lookupChars);
		var style = 'font-family: courier; font-size: 12px;';
		
		
		if(selectListSelectedValue == null || selectListSelectedValue == '' || selectListSelectedValue == 'Undefined' || selectListSelectedValue == 'null'){
			selectListSelectedValue = 0;
		}
		
		var sqlParams = [lookupChars, lookupChars, lookupChars, lookupChars];
		var urlAjaxPage = 'getAjaxDropDown.i?';
		var urlAjaxParamString = 'ajax_sql=' + sqlString + '&ajax_params=' + sqlParams + '&ajax_html_field_name=' + selectListName + '&ajax_selected_value=' + selectListSelectedValue + '&ajax_undefined=' + undefinedString + '&ajax_style=' + style;
		var divName = 'assigned_location';		
		
		getAXAHUpdateDiv(urlAjaxPage + urlAjaxParamString, divName);
	}
  	
  	/**
  	 * consumerSearchInfo.jsp - Validates the DownloadCSVButton click.
  	 * @param field
  	 * @return
  	 */
  	function validateDownloadCSVButton(field) {
		
	    var sel="";
	    
	    for(var i=0;i<field.length;i++){
          if(field[i].checked == true){
                sel=field[i].value;
                break;
          }
	    }
	    if(sel==""){
	    	return false;
	    }
	    return true;
	}
  	
	var checkflag = "false";
	
	/**
	 * consumerSearchInfo.jsp - Counts the checked boxes.
	 * @param field
	 * @return
	 */
    function count_check(field) {
       var count = 0;
       var obj = document.getElementById("check_count");
       //var obj = document.consumerInfo.check_count;
       for (i = 0; i < field.length; i++) {
          if(field[i].checked == true) {
             count++;
          }
       }
       obj.value = count;
      
       var obj2 = document.getElementById("document.consumerInfo.card_check_btn");
       if(obj2){
	       if(count==0){    
	       	document.consumerInfo.card_check_btn.value = 'Check All';
	       	checkflag = "false";
	       }else{
	    	document.consumerInfo.card_check_btn.value = 'Uncheck All';
	    	checkflag = "true";
	       }
       }
       
    }

    /**
	 * consumerSearchInfo.jsp - Checks all boxes.
	 * @param field
	 * @return
	 */
    function check(field) {
       var obj = document.getElementById("check_count");
       //var obj = document.consumerInfo.check_count;
       var count = 0;
       if (checkflag == "false") {
          for (i = 0; i < field.length; i++) {
             field[i].checked = true;
             count++;
          }
          checkflag = "true";
          obj.value = count;
          return "Uncheck All";
       } else {
          for (i = 0; i < field.length; i++) {
             field[i].checked = false;
          }
          checkflag = "false";
          obj.value = 0;
          return "Check All";
       }
    }
    
    /**
	 * consumerSearchInfo.jsp - Builds list of cards to download.
	 * @param field
	 * @return
	 */
    function buildCardDownloadList(field) {
        //var obj = document.getElementById("download_card");
        
        var paramString = '';
        if (checkflag == "true") {
           for (i = 0; i < field.length; i++) {
              if(field[i].checked == true){
        	  	paramString+=field[i].value;
        	  	paramString+=',';
              }
        	
           }
           
        } else {
           return false;
        }
        
        var lastIndex = paramString.lastIndexOf(",");
        paramString = paramString.substring(0, lastIndex);
        
        return paramString;
     }
    
    /**
     * editConsumer.jsp - Confirm user agrees to 'Delete' consumer.
     * @param action
     * @return
     */
  	function confirmEditConsumerSubmit(action)
	{
		var agree;
		var fname = document.getElementById('fname');
		switch(action)
		{
			case 'Delete':
			{
				var fname = document.getElementById('fname').value;
				var lname = document.getElementById('lname').value;
				var confirmMessage = ("To delete a consumer record you must first reassign all its devices to a different consumer.");
				confirmMessage+=("\nIf you have not done this already, please click Cancel and do so now.\n\nContinue to delete " + fname + " " + lname + "?");
				agree=confirm(confirmMessage);
				break;
			}
		}
		if (agree)
			return true ;
		else
			return false ;
  	}
  	
  	/**
  	 * On hyperlinks on the search pages, capture and send the correct consumer id.
  	 * @param key
  	 * @return
  	 */
  	function runThisConsumerSearch(key)
  	{
  	    document.searchConsumer.consumer_acct_id.value = key;
  	    document.searchConsumer.action='editConsumerAcct.i';
  	    document.searchConsumer.submit();
  	    return false;
  	}
  	

