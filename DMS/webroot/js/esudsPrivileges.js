function toggleColor(objElement)
{
   if (objElement.className=='normal') {
       objElement.className='focus';
//alert('AAA ' + objElement.className);
    } else {
       objElement.className='normal';
   }
}
// if user select a customer and a location and checkes a checkbox
// for insertion he can press submit buttons, however if he changes
// his mind and select undefined customer, system will reset it self
// so that user can't insert a value
function confirmCustomer(str)
{
   var cus_obj = document.getElementById(str+'customer_id_div');
   if(cus_obj.value == 1) {
      document.myForm.reset();
   }
}

// if user select a customer and a location and checkes a checkbox
// for insertion he can press submit buttons, however if he changes
// his mind and select undefined location, system will reset it self
// so that user can't insert a value
function confirmLocation(loc_prefix)
{
   var loc_obj = document.getElementById(loc_prefix+'__location_id_div');
   if(loc_obj.value == 1) {
      document.myForm.reset();
   }
}

// in order user to search something he/she should at list enter
// a first name, last name or email, if these fields are
// empty, then there will be no search
function confirmSubmit()
{
   obj1 = document.getElementById('checkbox1_id');
   obj2 = document.getElementById('checkbox2_id');
   obj3 = document.getElementById('checkbox3_id');
   obj5 = document.getElementById('checkbox5_id');
   obj6 = document.getElementById('checkbox6_id');

   if(!obj1.checked && !obj2.checked && !obj3.checked && !obj5.checked && !obj6.checked ) { 
     alert('You must check at least one checkbox!');
     return false ;
   }
   
   flag = 0;
   obj = document.getElementById('id_app_user_fname');
   if(obj.value != '') { flag = 1; }
   obj = document.getElementById('id_app_user_lname');
   if(obj.value != '') { flag = 1; }
   obj = document.getElementById('id_app_user_email_addr');
   if(obj.value != '') { flag = 1; }
   if (flag) {
       return true ;
   } else {
     alert('You must enter a value in least one of the form fields.');
     return false ;
   }
}

// when a user wants to check a insertsion box for insert purposes
// this function will make sure that customer and location are
// checked, othervise no box will be checked.
function confirmSubmit1(operation_type)
{

   var cus_obj = document.getElementById(operation_type + '__customer_id_div');

   if(cus_obj == null || cus_obj.value != 1) {
      if(operation_type != 'operator_reports') {
         var loc_obj = document.getElementById(operation_type + '__location_id_div');
         var location_value = loc_obj.value;
     
         if(loc_obj.value != 1 && loc_obj.value != 'Undefined') { 
             return true ;
         } else {
           alert('Select a location to check this box!.');
           document.myForm.reset();
           loc_obj.value = location_value;
           return false ;
         }
      }
   } else {
     alert('First select a customer to check this box!.');
     document.myForm.reset();
     return false ;
   }
}

function confirmSubmit2()
{
   var str = 'Are you sure you wish to continue?';
   var agree=confirm(str);
   if (agree)
     return true ;
   else
     document.myForm.reset();
     return false ;
}

function runThis(key)
{
    document.myForm.app_user_id.value = key;
    document.myForm.submit();
    return false;
}

function display_message1() {
	var location_div1 = document.getElementById('location_div1');
	location_div1.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src="/images/progress_bar.gif" ></b>';
}
function display_message2() {
	var location_div2 = document.getElementById('location_div2');
   location_div2.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src="/images/progress_bar.gif" ></b>';
}
function display_message3() {
	var location_div3 = document.getElementById('location_div3');
   location_div3.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src="/images/progress_bar.gif" ></b>';
}
function display_message3s() {
	var location_div3s = document.getElementById('location_div3s');
   location_div3s.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src="/images/progress_bar.gif" ></b>';
}
function display_message4() {
	var location_div4 = document.getElementById('location_div4');
   location_div4.innerHTML = '<b><font color=red size=3>Please Wait Loading Location Info </font> <img src="/images/progress_bar.gif" ></b>';
}
function get_location1(notinthis) {
   var idValue = document.getElementById("user_admin_reports__customer_id_div").value;
   var url1 = "rpcLocation.i?notinthis="+notinthis+"&location_div_prefix=user_admin_reports&location=S&ajax_params="+escape(idValue)+"|"+notinthis+"|"+notinthis+"|"+notinthis+"&param=";
   getAXAHUpdateDiv(url1 + escape(idValue),'location_div1');
}

function get_location2(notinthis) {
   var idValue = document.getElementById("user_maintenance__customer_id_div").value;
   var url2 = "rpcLocation.i?notinthis="+notinthis+"&location_div_prefix=user_maintenance&location=SC&ajax_params="+escape(idValue)+"|"+notinthis+"|"+notinthis+"|"+notinthis+"|"+escape(idValue)+"&param=";
   getAXAHUpdateDiv(url2 + escape(idValue),'location_div2');
}

function get_location3(notinthis) {
   var idValue = document.getElementById("operator_access_campus__customer_id_div").value;
   var url3 = "rpcLocation.i?notinthis="+notinthis+"&location_div_prefix=operator_access_campus&location=C&ajax_params="+escape(idValue)+"|"+notinthis+"|"+notinthis+"|"+notinthis+"&param=";
   getAXAHUpdateDiv(url3 + escape(idValue),'location_div3');
}
function get_location3s(notinthis) {
   var idValue = document.getElementById("operator_access_school__customer_id_div").value;
   var url3s = "rpcLocation.i?notinthis="+notinthis+"&location_div_prefix=operator_access_school&location=S&ajax_params="+escape(idValue)+"|"+notinthis+"|"+notinthis+"|"+notinthis+"&param=";
   getAXAHUpdateDiv(url3s + escape(idValue),'location_div3s');
}

function get_location4(notinthis) {
   var idValue = document.getElementById("operator_reports__customer_id_div").value;
   var url4 = "rpcLocation.i?notinthis="+notinthis+"&location_div_prefix=operator_reports&location=S&ajax_params="+escape(idValue)+"|"+notinthis+"|"+notinthis+"|"+notinthis+"&param=";
   getAXAHUpdateDiv(url4 + escape(idValue),'location_div4');
}