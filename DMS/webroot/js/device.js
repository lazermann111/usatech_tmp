function getCheckedValue(radioObj) {
	if (!radioObj)
		return "";
	var radioLength = radioObj.length;
	if (radioLength == undefined)
		if (radioObj.checked)
			return radioObj.value;
		else
			return "";
	for (var i = 0; i < radioLength; i++) {
		if (radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}
function getSelectedValues(selObj) {
	if (!selObj)
		return new Array();
	var selLength;
	if ("options" in selObj)
		selLength = selObj.options.length;
	if (selLength == undefined)
		return new Array();
	var selectedArray = new Array();
	var count = 0;
	for (var i = 0; i < selLength; i++) {
		if (("selected" in selObj.options[i] && selObj.options[i].selected)
			|| ("checked" in selObj.options[i] && selObj.options[i].checked))
			selectedArray[count++] = selObj.options[i].value;
	}
	return selectedArray;
}

// Author: 	YNing.
// Year:	2009
// 			Reusable javascript method to redirect with post or get (and params).
//
//
function redirectWithParams(targ, str, params, func_method) {	//TODO: test m-select and m-checkbox fields
// targ   - i.e. document.location.pathname
// str    - (optional) hand-coded query string (i.e. 'myaction=test1234&somevar=test')
// params - (optional) Array of objects (text, select, checkbox, etc.) to append to query string
	if (typeof func_method == "undefined")
		func_method = 'get';	//'get' (for debugging) or 'post'

	// use current page pathname if no target specified
	if (targ === null || targ == '')
		targ = document.location.pathname;
	var newloc = '';
	if (func_method == "get")
		if (str != "" || (params != undefined && params.length > 0))
			newloc = targ+"?";
		else
			newloc = targ;
	if (str != "")
		newloc += str;
	if (params != undefined) {
		for (var i = 0; i < params.length; i++) {
			var name = '';
			var val = '';
			if ("length" in params[i]) {
				if ("options" in params[i]) {
					name = params[i].name;
					var selVals = getSelectedValues(params[i]);
					for (var i = 0; i < selVals.length; i++) {
						if (val == "")
							val = escape(selVals[i]);
						else
							val = val + "," + escape(selVals[i]);
					}
				}
				else {
					name = params[i][0].name;
					val = escape(getCheckedValue(params[i]));
				}
			} else if ("checked" in params[i] && params[i].checked == true) {
				name = params[i].name;
				val = escape(getCheckedValue(params[i]));
			} else {
				name = params[i].name;
				val = escape(params[i].value);
			}
			newloc += "&"+name+"="+val;
		}
	}

	if (func_method == 'get')
		document.location = newloc;
	else {
		var myform=document.createElement("form");
		myform.setAttribute("method", "post");
		myform.setAttribute("action", targ);

		var newvars = newloc.split("&");
		for (var i = 0; i < newvars.length; i++) {
			var pair = newvars[i].split("=");
			var myhidden=document.createElement("input");
			myhidden.setAttribute("type","hidden");
			myhidden.setAttribute("name",unescape(pair[0]));
			myhidden.setAttribute("value",unescape(pair[1]));
			myform.appendChild(myhidden);
		}
		// Author: 	Peter Finch
		// Year:	2009	
		//			Original code using myinsertloc logic did not work on 'POST' calls
		//			Code below by author above to append form directly to document.body is the fix.
		document.body.appendChild(myform);
		myform.submit();
		document.body.removeChild(myform);
	}

	return false;
}
Element.extend({
	getSelected: function(){
		this.selectedIndex; // Safari 3.2.1
		return new Elements($A(this.options).filter(function(option){
			return option.selected;
		}));
	},
	fireCustomEvent: function(type, props) {
		var evtProps = {type: type, target: this, returnValue: true};
		if(props)
			evtProps = $merge(props, evtProps);			
		var args = [ new Event(evtProps) ];
		if(this.$events && this.$events[type])
			return this.fireEvent(type, args);
		
		var handler = this["on" + type]; 
		if(!handler) 
			return;
		if($type(handler) != 'function') {
			handler = new Function("event", "" + handler);
		}
		return handler.apply(this, args);
	}
});
(function() {
var doSelectFn = function(option) {
	return $(option).fireCustomEvent("select");
};
var onSelectFn = function() {
	return this.getSelected().every(doSelectFn);
};
window.attachOnSelect = function(select) {
	if(select.options && select.options.length && $$(select.options).some(function(option){return option.onselect || option.getAttribute("onselect");})) {					
		select.addEvent('change', onSelectFn);
		select.getSelected().every(doSelectFn);
	}
};
})();

(function() {
/*<ltIE9>*/
//technique by jdbarlett - http://jdbartlett.com/innershiv/
var div = document.createElement('div');
div.innerHTML = '<nav></nav>';
var supportsHTML5Elements = (div.childNodes.length == 1);
if (!supportsHTML5Elements){
	var tags = 'abbr article aside audio canvas datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video'.split(' '),
		fragment = document.createDocumentFragment(), l = tags.length;
	while (l--) fragment.createElement(tags[l]);
}
div = null;
/*</ltIE9>*/

/*<IE>*/
var supportsTableInnerHTML = Function.attempt(function(){
	var table = document.createElement('table');
	table.innerHTML = '<tr><td></td></tr>';
	return true;
});

/*<ltFF4>*/
var tr = document.createElement('tr'), html = '<td></td>';
tr.innerHTML = html;
var supportsTRInnerHTML = (tr.innerHTML == html);
tr = null;
/*</ltFF4>*/

if (!supportsTableInnerHTML || !supportsTRInnerHTML || !supportsHTML5Elements){
	var translations = {
		table: [1, '<table>', '</table>'],
		select: [1, '<select>', '</select>'],
		tbody: [2, '<table><tbody>', '</tbody></table>'],
		tr: [3, '<table><tbody><tr>', '</tr></tbody></table>']
	};

	translations.thead = translations.tfoot = translations.tbody;

	var flatten = function(orig){
		var array = [];
		for (var i = 0, l = orig.length; i < l; i++){
			var type = $type(orig[i]);
			if (type == 'null') continue;
			array = array.concat((type == 'array' || type == 'collection' || type == 'arguments' || type == 'object') ? flatten(orig[i]) : orig[i]);
		}
		return array;
	};
	Element.extend({
	    setHTML: function(html){
			var wrap = translations[this.getTag()];
			if (!wrap && !supportsHTML5Elements) wrap = [0, '', ''];
			if (!wrap) {
				this.innerHTML = html;
				return this;
			}

			var level = wrap[0], wrapper = document.createElement('div'), target = wrapper;
			if (!supportsHTML5Elements) fragment.appendChild(wrapper);
			wrapper.innerHTML = flatten([wrap[1], html, wrap[2]]).join('');
			while (level--) target = target.firstChild;
			while(this.firstChild)
				this.removeChild(this.firstChild);
			this.adopt($A(target.childNodes));
			if (!supportsHTML5Elements) fragment.removeChild(wrapper);
			wrapper = null;
			return this;
		}
	});
}
/*</IE>*/
})();