package test;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;

import com.usatech.dms.sales.PricingTier;
import com.usatech.dms.sales.PricingTier.StatusCode;

public class PricingTierTest {

	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("dms-dev.properties", PricingTierTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
	}
	
	@Test
	public void testFindAll() throws Exception {
		List<PricingTier> pricingTiers = PricingTier.findAll();
		assertNotNull(pricingTiers);
		assertFalse(pricingTiers.isEmpty());
	}
	
	@Test
	public void testFindAllActive() throws Exception {
		List<PricingTier> pricingTiers = PricingTier.findAllActive();
		assertFalse(pricingTiers.stream().anyMatch(pricingTier -> pricingTier.getName().equals("Expensive")));
		assertTrue(pricingTiers.stream().anyMatch(pricingTier -> pricingTier.getName().equals("Cheap")));
	}

	@Test
	public void testFindByPk() throws Exception {
		PricingTier pricingTier = PricingTier.findByPk(1000);
		assertNotNull(pricingTier);
		assertEquals("Cheap", pricingTier.getName());
		assertEquals(StatusCode.A, pricingTier.getStatusCode());
	}
	
	@Test
	public void testForCustomer() throws Exception {
		PricingTier pricingTier = PricingTier.findPricingTierForCustomer(3484);
		assertNotNull(pricingTier);
		assertEquals("Cheap", pricingTier.getName());
	}
	
	@Test
	public void testSave() throws Exception {
		String randomName = Double.toHexString(Math.random()).substring(4);
		
		PricingTier pricingTier = new PricingTier();
		pricingTier.setName(randomName);
		
		assertTrue(pricingTier.isNew());
		pricingTier.save();
		assertFalse(pricingTier.isNew());
		
		PricingTier pricingTier2 = PricingTier.findByPk(pricingTier.getPricingTierId());
		assertEquals(randomName, pricingTier2.getName());
		assertEquals(pricingTier, pricingTier2);
		
		randomName = Double.toHexString(Math.random()).substring(4);

		pricingTier.setName(randomName);
		pricingTier.save();
		
		pricingTier2 = PricingTier.findByPk(pricingTier.getPricingTierId());
		assertEquals(randomName, pricingTier2.getName());
		assertEquals(pricingTier, pricingTier2);
	}

}
