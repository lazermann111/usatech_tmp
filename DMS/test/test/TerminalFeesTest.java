package test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;

import org.apache.commons.configuration.Configuration;

import com.usatech.layers.common.fees.*;
import com.usatech.layers.common.fees.Fees.Fee;

public class TerminalFeesTest {

	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("dms-dev.properties", TerminalFeesTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
	}
	
	@Test
	public void test() throws Exception {
		Random random = new Random();
		int n = random.nextInt(6) + 4;
		TerminalFees terminalFees = TerminalFees.loadByTerminalId(20440);
		assertNotNull(terminalFees);
		Fee fee = terminalFees.getFee(EntryType.SERVICE_FEE, ExchangeType.BUY_RATE, FeeType.getById(EntryType.SERVICE_FEE, 1), RateType.FLAT);
		fee.setValue(n);
		terminalFees.setServiceFeeFrequencyId(2);
		terminalFees.setCommissionBankId(4128);
		terminalFees.saveToDatabase();
	}
	
}
