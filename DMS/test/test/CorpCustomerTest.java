package test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;

import org.apache.commons.configuration.Configuration;

import com.usatech.dms.action.CustomerAction;
import com.usatech.dms.model.CorpCustomer;

public class CorpCustomerTest {

	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("dms-dev.properties", CorpCustomerTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
	}
	
	@Test
	public void testFindCorpCustomersByName() throws Exception {
		List<CorpCustomer> customers = CustomerAction.findCorpCustomersByName("test%");
		assertFalse(customers.isEmpty());
	}
	
	@Test
	public void testGetCorpCustomer() throws Exception {
		CorpCustomer customer = CustomerAction.getCorpCustomer(3484);
		assertNotNull(customer);
		assertNotNull(customer.getAffiliateName());
		assertNotNull(customer.getCategoryName());
		assertNotNull(customer.getPricingTierName());
	}
}
