package test;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import simple.bean.MapDynaBean;
import simple.servlet.MapInputForm;
import simple.test.UnitTest;

import com.usatech.layers.common.model.PayrollSchedule;
import com.usatech.layers.common.model.PayrollSchedule.PayPeriod;
import com.usatech.layers.common.model.PayrollSchedule.PayrollScheduleType;

// this should really be in LayersCommon but it's difficult to initialize a database connection from there
public class PayrollScheduleTests extends UnitTest {

	@Before
	public void setUp() throws Exception {
		super.setupDSF("DMS.properties");
	}

	@Test
	public void testSetCurrentDateWeekly() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.FEBRUARY, 5));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 13));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 14));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 18));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 19));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 26), paySchedule.getNextPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 20));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 26), paySchedule.getNextPayrollDate());
	}

	@Test
	public void testWeekly() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.FEBRUARY, 5));
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 1));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 8));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 15));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 22));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 29));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 5));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 12));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 19));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 26));
		payDates.add(LocalDate.of(2016, Month.MARCH, 4));
		payDates.add(LocalDate.of(2016, Month.MARCH, 11));
		payDates.add(LocalDate.of(2016, Month.MARCH, 18));
		payDates.add(LocalDate.of(2016, Month.MARCH, 25));
		payDates.add(LocalDate.of(2016, Month.APRIL, 1));
		payDates.add(LocalDate.of(2016, Month.APRIL, 8));
		payDates.add(LocalDate.of(2016, Month.APRIL, 15));
		payDates.add(LocalDate.of(2016, Month.APRIL, 22));
		payDates.add(LocalDate.of(2016, Month.APRIL, 29));
		payDates.add(LocalDate.of(2016, Month.MAY, 6));
		payDates.add(LocalDate.of(2016, Month.MAY, 13));
		payDates.add(LocalDate.of(2016, Month.MAY, 20));
		payDates.add(LocalDate.of(2016, Month.MAY, 27));
		payDates.add(LocalDate.of(2016, Month.JUNE, 3));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}

	@Test
	public void testWeeklyYearEnd() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.FEBRUARY, 5));
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.DECEMBER, 2));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 9));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 16));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 23));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 30));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 6));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 13));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testWeeklyNonFriday() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.FEBRUARY, 1));
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 29));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 8));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 22));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 29));
		payDates.add(LocalDate.of(2016, Month.MARCH, 7));
		
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}

	@Test
	public void testWeeklyFutureOnly() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.now());
		paySchedule.setCurrentDate(LocalDate.now());
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.now().plus(Period.ofWeeks(1)));
		payDates.add(LocalDate.now().plus(Period.ofWeeks(2)));
		payDates.add(LocalDate.now().plus(Period.ofWeeks(3)));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}

	@Test
	public void testMonthly() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_DAY_OF_MONTH, LocalDate.of(2016, Month.FEBRUARY, 1), LocalDate.of(2016, Month.FEBRUARY, 15));
		paySchedule.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 1));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2016, Month.MARCH, 1));
		payDates.add(LocalDate.of(2016, Month.MARCH, 15));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testSetCurrentDateMonthly() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_DAY_OF_MONTH, LocalDate.of(2016, Month.FEBRUARY, 19));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 13));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 14));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 18));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 19));
		assertEquals(LocalDate.of(2016, Month.MARCH, 19), paySchedule.getNextPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 20));
		assertEquals(LocalDate.of(2016, Month.MARCH, 19), paySchedule.getNextPayrollDate());
	}
	
	@Test
	public void testMonthlyMonthEnd() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_DAY_OF_MONTH, LocalDate.of(2016, Month.JANUARY, 15), LocalDate.of(2016, Month.JANUARY, 31));
		paySchedule.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 15));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 31));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 29));
		payDates.add(LocalDate.of(2016, Month.MARCH, 15));
		payDates.add(LocalDate.of(2016, Month.MARCH, 31));
		payDates.add(LocalDate.of(2016, Month.APRIL, 15));
		payDates.add(LocalDate.of(2016, Month.APRIL, 30));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testMonthlySingle() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_DAY_OF_MONTH, LocalDate.of(2016, Month.FEBRUARY, 1));
		paySchedule.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 1));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2016, Month.MARCH, 1));
		payDates.add(LocalDate.of(2016, Month.APRIL, 1));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testMonthlySingleMonthEnd() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_DAY_OF_MONTH, LocalDate.of(2016, Month.JANUARY, 31));
		paySchedule.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 31));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 29));
		payDates.add(LocalDate.of(2016, Month.MARCH, 31));
		payDates.add(LocalDate.of(2016, Month.APRIL, 30));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testMonthlyPrior15E() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_PRIOR_WEEKDAY, LocalDate.of(2016, Month.JANUARY, 15), LocalDate.of(2016, Month.JANUARY, 31));
		paySchedule.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 15));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 29));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 29));
		payDates.add(LocalDate.of(2016, Month.MARCH, 15));
		payDates.add(LocalDate.of(2016, Month.MARCH, 31));
		payDates.add(LocalDate.of(2016, Month.APRIL, 15));
		payDates.add(LocalDate.of(2016, Month.APRIL, 29));
		payDates.add(LocalDate.of(2016, Month.MAY, 13));
		payDates.add(LocalDate.of(2016, Month.MAY, 31));
		payDates.add(LocalDate.of(2016, Month.JUNE, 15));
		payDates.add(LocalDate.of(2016, Month.JUNE, 30));
		payDates.add(LocalDate.of(2016, Month.JULY, 15));
		payDates.add(LocalDate.of(2016, Month.JULY, 29));
		payDates.add(LocalDate.of(2016, Month.AUGUST, 15));
		payDates.add(LocalDate.of(2016, Month.AUGUST, 31));
		payDates.add(LocalDate.of(2016, Month.SEPTEMBER, 15));
		payDates.add(LocalDate.of(2016, Month.SEPTEMBER, 30));
		payDates.add(LocalDate.of(2016, Month.OCTOBER, 14));
		payDates.add(LocalDate.of(2016, Month.OCTOBER, 31));
		payDates.add(LocalDate.of(2016, Month.NOVEMBER, 15));
		payDates.add(LocalDate.of(2016, Month.NOVEMBER, 30));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 15));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 30));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 13));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 31));
		payDates.add(LocalDate.of(2017, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2017, Month.FEBRUARY, 28));
		payDates.add(LocalDate.of(2017, Month.MARCH, 15));
		payDates.add(LocalDate.of(2017, Month.MARCH, 31));
		payDates.add(LocalDate.of(2017, Month.APRIL, 14));
		payDates.add(LocalDate.of(2017, Month.APRIL, 28));
		payDates.add(LocalDate.of(2017, Month.MAY, 15));
		payDates.add(LocalDate.of(2017, Month.MAY, 31));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testMonthlyPrior115() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_PRIOR_WEEKDAY, LocalDate.of(2016, Month.JANUARY, 15), LocalDate.of(2016, Month.JANUARY, 1));
		paySchedule.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 1));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2016, Month.MARCH, 1));
		payDates.add(LocalDate.of(2016, Month.MARCH, 15));
		payDates.add(LocalDate.of(2016, Month.APRIL, 1));
		payDates.add(LocalDate.of(2016, Month.APRIL, 15));
		payDates.add(LocalDate.of(2016, Month.APRIL, 29));
		payDates.add(LocalDate.of(2016, Month.MAY, 13));
		payDates.add(LocalDate.of(2016, Month.JUNE, 1));
		payDates.add(LocalDate.of(2016, Month.JUNE, 15));
		payDates.add(LocalDate.of(2016, Month.JULY, 1));
		payDates.add(LocalDate.of(2016, Month.JULY, 15));
		payDates.add(LocalDate.of(2016, Month.AUGUST, 1));
		payDates.add(LocalDate.of(2016, Month.AUGUST, 15));
		payDates.add(LocalDate.of(2016, Month.SEPTEMBER, 1));
		payDates.add(LocalDate.of(2016, Month.SEPTEMBER, 15));
		payDates.add(LocalDate.of(2016, Month.SEPTEMBER, 30));
		payDates.add(LocalDate.of(2016, Month.OCTOBER, 14));
		payDates.add(LocalDate.of(2016, Month.NOVEMBER, 1));
		payDates.add(LocalDate.of(2016, Month.NOVEMBER, 15));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 1));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 15));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 30));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 13));
		payDates.add(LocalDate.of(2017, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2017, Month.FEBRUARY, 15));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testMonthlyNext15E() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_NEXT_WEEKDAY, LocalDate.of(2016, Month.JANUARY, 15), LocalDate.of(2016, Month.JANUARY, 31));
		paySchedule.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 29));
		payDates.add(LocalDate.of(2016, Month.MARCH, 15));
		payDates.add(LocalDate.of(2016, Month.MARCH, 31));
		payDates.add(LocalDate.of(2016, Month.APRIL, 15));
		payDates.add(LocalDate.of(2016, Month.MAY, 2));
		payDates.add(LocalDate.of(2016, Month.MAY, 16));
		payDates.add(LocalDate.of(2016, Month.MAY, 31));
		payDates.add(LocalDate.of(2016, Month.JUNE, 15));
		payDates.add(LocalDate.of(2016, Month.JUNE, 30));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testMonthlyNext115() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_NEXT_WEEKDAY, LocalDate.of(2016, Month.JANUARY, 1), LocalDate.of(2016, Month.JANUARY, 15));
		paySchedule.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 1));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2016, Month.MARCH, 1));
		payDates.add(LocalDate.of(2016, Month.MARCH, 15));
		payDates.add(LocalDate.of(2016, Month.APRIL, 1));
		payDates.add(LocalDate.of(2016, Month.APRIL, 15));
		payDates.add(LocalDate.of(2016, Month.MAY, 2));
		payDates.add(LocalDate.of(2016, Month.MAY, 16));
		payDates.add(LocalDate.of(2016, Month.JUNE, 1));
		payDates.add(LocalDate.of(2016, Month.JUNE, 15));
		payDates.add(LocalDate.of(2016, Month.JULY, 1));
		payDates.add(LocalDate.of(2016, Month.JULY, 15));
		payDates.add(LocalDate.of(2016, Month.AUGUST, 1));
		payDates.add(LocalDate.of(2016, Month.AUGUST, 15));
		payDates.add(LocalDate.of(2016, Month.SEPTEMBER, 1));
		payDates.add(LocalDate.of(2016, Month.SEPTEMBER, 15));
		payDates.add(LocalDate.of(2016, Month.OCTOBER, 3));
		payDates.add(LocalDate.of(2016, Month.OCTOBER, 17));
		payDates.add(LocalDate.of(2016, Month.NOVEMBER, 1));
		payDates.add(LocalDate.of(2016, Month.NOVEMBER, 15));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 1));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 15));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 2));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 16));
		payDates.add(LocalDate.of(2017, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2017, Month.FEBRUARY, 15));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testMonthlyNextSetCurrent() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_NEXT_WEEKDAY, LocalDate.of(2016, Month.JANUARY, 1), LocalDate.of(2016, Month.JANUARY, 15));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 29));
		assertEquals(LocalDate.of(2016, Month.MAY, 2), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 30));
		assertEquals(LocalDate.of(2016, Month.MAY, 2), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MAY, 1));
		assertEquals(LocalDate.of(2016, Month.MAY, 2), paySchedule.getNextPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MAY, 2));
		assertEquals(LocalDate.of(2016, Month.MAY, 16), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MAY, 3));
		assertEquals(LocalDate.of(2016, Month.MAY, 16), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MAY, 15));
		assertEquals(LocalDate.of(2016, Month.MAY, 16), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MAY, 16));
		assertEquals(LocalDate.of(2016, Month.JUNE, 1), paySchedule.getNextPayrollDate());
	}
	
	@Test
	public void testSetCurrentDateBiWeekly() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.BIWEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.FEBRUARY, 5));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 13));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 14));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 18));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 19), paySchedule.getNextPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 19));
		assertEquals(LocalDate.of(2016, Month.MARCH, 4), paySchedule.getNextPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 20));
		assertEquals(LocalDate.of(2016, Month.MARCH, 4), paySchedule.getNextPayrollDate());
	}
	
	@Test
	public void testBiWeekly() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.BIWEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.FEBRUARY, 5));
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 1));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.JANUARY, 8));
		payDates.add(LocalDate.of(2016, Month.JANUARY, 22));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 5));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 19));
		payDates.add(LocalDate.of(2016, Month.MARCH, 4));
		payDates.add(LocalDate.of(2016, Month.MARCH, 18));
		payDates.add(LocalDate.of(2016, Month.APRIL, 1));
		payDates.add(LocalDate.of(2016, Month.APRIL, 15));
		payDates.add(LocalDate.of(2016, Month.APRIL, 29));
		payDates.add(LocalDate.of(2016, Month.MAY, 13));
		payDates.add(LocalDate.of(2016, Month.MAY, 27));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testBiWeeklyYearEnd() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.BIWEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.FEBRUARY, 5));
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.DECEMBER, 2));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 9));
		payDates.add(LocalDate.of(2016, Month.DECEMBER, 23));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 6));
		payDates.add(LocalDate.of(2017, Month.JANUARY, 20));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testBiWeeklyNonFriday() {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.BIWEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.FEBRUARY, 1));
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 29));
		
		List<LocalDate> payDates = new ArrayList<LocalDate>();
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 1));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 15));
		payDates.add(LocalDate.of(2016, Month.FEBRUARY, 29));
		payDates.add(LocalDate.of(2016, Month.MARCH, 14));
		
		for(int i=0; i<payDates.size(); i++) {
			LocalDate pd = paySchedule.getNextPayrollDate();
			assertEquals(payDates.get(i), pd);
		}
	}
	
	@Test
	public void testLoadFromString() throws Exception {
		PayrollSchedule paySchedule1 = PayrollSchedule.loadFrom("BIWEEKLY_ON_DAY_OF_WEEK", "12/31/2015");
		paySchedule1.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 1));
		assertEquals(LocalDate.of(2016, Month.JANUARY, 14), paySchedule1.getNextPayrollDate());

		PayrollSchedule paySchedule2 = PayrollSchedule.loadFrom("WEEKLY_ON_DAY_OF_WEEK", "12/28/2015");
		paySchedule2.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 1));
		assertEquals(LocalDate.of(2016, Month.JANUARY, 4), paySchedule2.getNextPayrollDate());

		PayrollSchedule paySchedule3 = PayrollSchedule.loadFrom("MONTHLY_ON_DAY_OF_MONTH", "01/15/2015");
		paySchedule3.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 1));
		assertEquals(LocalDate.of(2016, Month.JANUARY, 15), paySchedule3.getNextPayrollDate());

		PayrollSchedule paySchedule4 = PayrollSchedule.loadFrom("SEMIMONTHLY_ON_DAYS_OF_MONTH", "01/01/2016,01/15/2016");
		paySchedule4.setCurrentDate(LocalDate.of(2015, Month.DECEMBER, 31));
		assertEquals(LocalDate.of(2016, Month.JANUARY, 1), paySchedule4.getNextPayrollDate());
		assertEquals(LocalDate.of(2016, Month.JANUARY, 15), paySchedule4.getNextPayrollDate());
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 1), paySchedule4.getNextPayrollDate());
	}

	@Test
	public void testSaveToForm() {
		MapInputForm form = new MapInputForm(new HashMap<String, Object>());
		
		PayrollSchedule paySchedule1 = PayrollSchedule.create(PayrollScheduleType.BIWEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2015, Month.DECEMBER, 31));
		paySchedule1.saveTo(form);
		assertEquals("BIWEEKLY_ON_DAY_OF_WEEK", form.getAttribute("payrollScheduleType"));
		assertEquals("12/31/2015", form.getAttribute("payrollScheduleData"));
		assertEquals("12/31/2015", form.getAttribute("payDay1"));
		
		PayrollSchedule paySchedule2 = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.JANUARY, 5));
		paySchedule2.saveTo(form);
		assertEquals("WEEKLY_ON_DAY_OF_WEEK", form.getAttribute("payrollScheduleType"));
		assertEquals("01/05/2016", form.getAttribute("payrollScheduleData"));
		assertEquals("01/05/2016", form.getAttribute("payDay1"));
		
		PayrollSchedule paySchedule3 = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_DAY_OF_MONTH, LocalDate.of(2016, Month.JANUARY, 15));
		paySchedule3.saveTo(form);
		assertEquals("MONTHLY_ON_DAY_OF_MONTH", form.getAttribute("payrollScheduleType"));
		assertEquals("01/15/2016", form.getAttribute("payrollScheduleData"));
		assertEquals("01/15/2016", form.getAttribute("payDay1"));

		PayrollSchedule paySchedule4 = PayrollSchedule.create(PayrollScheduleType.SEMIMONTHLY_ON_DAYS_OF_MONTH, LocalDate.of(2016, Month.JANUARY, 1), LocalDate.of(2016, Month.JANUARY, 15));
		paySchedule4.saveTo(form);
		assertEquals("SEMIMONTHLY_ON_DAYS_OF_MONTH", form.getAttribute("payrollScheduleType"));
		assertEquals("01/01/2016,01/15/2016", form.getAttribute("payrollScheduleData"));
		assertEquals("01/01/2016", form.getAttribute("payDay1"));
		assertEquals("01/15/2016", form.getAttribute("payDay2"));
	}
	
	@Test
	public void testLoadFromForm() throws Exception {
		MapInputForm form = new MapInputForm(new HashMap<String, Object>());
		
		form.set("payrollScheduleType", "BIWEEKLY_ON_DAY_OF_WEEK");
		form.set("payDay1", "02/29/2016");
		
		PayrollSchedule paySchedule1 = PayrollSchedule.loadFrom(form);
		paySchedule1.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 29));
		assertEquals(LocalDate.of(2016, Month.MARCH, 14), paySchedule1.getNextPayrollDate());
		
		form.set("payrollScheduleType", "SEMIMONTHLY_ON_DAYS_OF_MONTH");
		form.set("payDay1", "01/31/2016");
		form.set("payDay2", "01/15/2016");
		
		PayrollSchedule paySchedule2 = PayrollSchedule.loadFrom(form);
		paySchedule2.setCurrentDate(LocalDate.of(2016, Month.FEBRUARY, 1));
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 15), paySchedule2.getNextPayrollDate());
		assertEquals(LocalDate.of(2016, Month.FEBRUARY, 29), paySchedule2.getNextPayrollDate());
		
	}
	
	@Test
	public void testPrevPayrollDateWeekly() throws Exception {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.APRIL, 1));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 29));
		assertEquals(LocalDate.of(2016, Month.APRIL, 22), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 28));
		assertEquals(LocalDate.of(2016, Month.APRIL, 22), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 23));
		assertEquals(LocalDate.of(2016, Month.APRIL, 22), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 22));
		assertEquals(LocalDate.of(2016, Month.APRIL, 15), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 30));
		assertEquals(LocalDate.of(2016, Month.APRIL, 29), paySchedule.getPrevPayrollDate());
	}

	@Test
	public void testPrevPayrollDateMonthly() throws Exception {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_DAY_OF_MONTH, LocalDate.of(2016, Month.APRIL, 1));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 29));
		assertEquals(LocalDate.of(2016, Month.APRIL, 1), paySchedule.getPrevPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 30));
		assertEquals(LocalDate.of(2016, Month.APRIL, 1), paySchedule.getPrevPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MAY, 1));
		assertEquals(LocalDate.of(2016, Month.APRIL, 1), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MAY, 2));
		assertEquals(LocalDate.of(2016, Month.MAY, 1), paySchedule.getPrevPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MAY, 31));
		assertEquals(LocalDate.of(2016, Month.MAY, 1), paySchedule.getPrevPayrollDate());
	}

	@Test
	public void testPrevPayrollDateBiWeekly() throws Exception {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.BIWEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.APRIL, 1));

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 29));
		assertEquals(LocalDate.of(2016, Month.APRIL, 15), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 28));
		assertEquals(LocalDate.of(2016, Month.APRIL, 15), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 23));
		assertEquals(LocalDate.of(2016, Month.APRIL, 15), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 22));
		assertEquals(LocalDate.of(2016, Month.APRIL, 15), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 30));
		assertEquals(LocalDate.of(2016, Month.APRIL, 29), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 1));
		assertEquals(LocalDate.of(2016, Month.MARCH, 18), paySchedule.getPrevPayrollDate());
	}
	
	@Test
	public void testPrevPayrollDateYearEnd() throws Exception {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.BIWEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.JANUARY, 8));

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 1));
		assertEquals(LocalDate.of(2015, Month.DECEMBER, 25), paySchedule.getPrevPayrollDate());

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.JANUARY, 8));
		assertEquals(LocalDate.of(2015, Month.DECEMBER, 25), paySchedule.getPrevPayrollDate());
	}
	
	@Test
	public void testPrevNext() throws Exception {
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.APRIL, 1));

		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 1));
		assertEquals(LocalDate.of(2016, Month.MARCH, 25), paySchedule.getPrevPayrollDate());
		assertEquals(LocalDate.of(2016, Month.APRIL, 1), paySchedule.getNextPayrollDate());
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 1));
		assertEquals(LocalDate.of(2016, Month.APRIL, 8), paySchedule.getNextPayrollDate());
		assertEquals(LocalDate.of(2016, Month.APRIL, 1), paySchedule.getPrevPayrollDate());
		
	}
	
	@Test
	public void testLoadByCustomerId() throws Exception {
		PayrollSchedule paySchedule = PayrollSchedule.loadByCustomerId(3);
		assertNotNull(paySchedule);
		assertEquals(PayrollScheduleType.SEMIMONTHLY_ON_DAYS_OF_MONTH, paySchedule.getPayrollScheduleType());
		assertEquals(paySchedule.getPayrollScheduleData(), "03/18/2016,03/04/2016");
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MARCH, 5));
		assertEquals(LocalDate.of(2016, Month.MARCH, 18), paySchedule.getNextPayrollDate());
	}
	
	@Test
	public void testLoadByUserId() throws Exception {
		PayrollSchedule paySchedule = PayrollSchedule.loadByUserId(165);
		assertNotNull(paySchedule);
		assertEquals(PayrollScheduleType.SEMIMONTHLY_ON_DAYS_OF_MONTH, paySchedule.getPayrollScheduleType());
		assertEquals(paySchedule.getPayrollScheduleData(), "03/18/2016,03/04/2016");
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.MARCH, 5));
		assertEquals(LocalDate.of(2016, Month.MARCH, 18), paySchedule.getNextPayrollDate());
	}
	
	@Test
	public void testGetCurrentPayPeriod() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.MONTHLY_ON_DAY_OF_MONTH, LocalDate.of(2016, Month.MARCH, 31));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 29));
		PayPeriod payPeriod = paySchedule.getCurrentPayPeriod();
		assertEquals(dateFormat.parse("04/01/2016 00:00:00"), payPeriod.getStartDate());
		assertEquals(dateFormat.parse("04/30/2016 23:59:59"), payPeriod.getEndDate());
	}
	
	@Test
	public void testGetPayPeriodSemiMonthly() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.SEMIMONTHLY_ON_DAYS_OF_MONTH, LocalDate.of(2016, Month.MARCH, 31), LocalDate.of(2016, Month.MARCH, 15));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 6));
		
		PayPeriod currentPayPeriod = paySchedule.getPrevPayPeriod();
		assertEquals(dateFormat.parse("03/16/2016 00:00:00"), currentPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("03/31/2016 23:59:59"), currentPayPeriod.getEndDate());

		PayPeriod prevPayPeriod = paySchedule.getCurrentPayPeriod();
		assertEquals(dateFormat.parse("04/01/2016 00:00:00"), prevPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("04/15/2016 23:59:59"), prevPayPeriod.getEndDate());

		PayPeriod nextPayPeriod = paySchedule.getNextPayPeriod();
		assertEquals(dateFormat.parse("04/16/2016 00:00:00"), nextPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("04/30/2016 23:59:59"), nextPayPeriod.getEndDate());
	}

	@Test
	public void testGetPayPeriodSemiMonthlyPriorWeekday() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.SEMIMONTHLY_ON_PRIOR_WEEKDAY, LocalDate.of(2016, Month.MARCH, 31), LocalDate.of(2016, Month.MARCH, 15));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 6));
		
		PayPeriod currentPayPeriod = paySchedule.getPrevPayPeriod();
		assertEquals(dateFormat.parse("03/16/2016 00:00:00"), currentPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("03/31/2016 23:59:59"), currentPayPeriod.getEndDate());

		PayPeriod prevPayPeriod = paySchedule.getCurrentPayPeriod();
		assertEquals(dateFormat.parse("04/01/2016 00:00:00"), prevPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("04/15/2016 23:59:59"), prevPayPeriod.getEndDate());

		PayPeriod nextPayPeriod = paySchedule.getNextPayPeriod();
		assertEquals(dateFormat.parse("04/16/2016 00:00:00"), nextPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("04/29/2016 23:59:59"), nextPayPeriod.getEndDate());
	}
	
	@Test
	public void testGetPayPeriodSemiMonthlyNextWeekday() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.SEMIMONTHLY_ON_NEXT_WEEKDAY, LocalDate.of(2016, Month.MARCH, 31), LocalDate.of(2016, Month.MARCH, 15));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 6));
		
		PayPeriod currentPayPeriod = paySchedule.getPrevPayPeriod();
		assertEquals(dateFormat.parse("03/16/2016 00:00:00"), currentPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("03/31/2016 23:59:59"), currentPayPeriod.getEndDate());

		PayPeriod prevPayPeriod = paySchedule.getCurrentPayPeriod();
		assertEquals(dateFormat.parse("04/01/2016 00:00:00"), prevPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("04/15/2016 23:59:59"), prevPayPeriod.getEndDate());

		PayPeriod nextPayPeriod = paySchedule.getNextPayPeriod();
		assertEquals(dateFormat.parse("04/16/2016 00:00:00"), nextPayPeriod.getStartDate());
		assertEquals(dateFormat.parse("05/02/2016 23:59:59"), nextPayPeriod.getEndDate());
	}
	
	@Test
	public void testGetNextPayPeriod() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.WEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.APRIL, 5));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 6));
		PayPeriod payPeriod = paySchedule.getNextPayPeriod();
		assertEquals(dateFormat.parse("04/13/2016 00:00:00"), payPeriod.getStartDate());
		assertEquals(dateFormat.parse("04/19/2016 23:59:59"), payPeriod.getEndDate());
	}
	
	@Test
	public void testGetPrevPayPeriod() throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		PayrollSchedule paySchedule = PayrollSchedule.create(PayrollScheduleType.BIWEEKLY_ON_DAY_OF_WEEK, LocalDate.of(2016, Month.APRIL, 5));
		
		paySchedule.setCurrentDate(LocalDate.of(2016, Month.APRIL, 6));
		PayPeriod payPeriod = paySchedule.getPrevPayPeriod();
		assertEquals(dateFormat.parse("03/23/2016 00:00:00"), payPeriod.getStartDate());
		assertEquals(dateFormat.parse("04/05/2016 23:59:59"), payPeriod.getEndDate());
	}
	
	@Test
	public void testLoadFromNone() throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("payrollScheduleType", "NONE");
		
		PayrollSchedule payrollSchedule = PayrollSchedule.loadFrom(new MapDynaBean(map));
		assertTrue(payrollSchedule.isNone());
	}



	
}
