package test;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;

import com.usatech.dms.sales.Category;
import com.usatech.dms.sales.Category.StatusCode;

public class CategoryTest {

	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("dms-dev.properties", CategoryTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
	}
	
	@Test
	public void testFindAll() throws Exception {
		List<Category> categories = Category.findAll();
		assertNotNull(categories);
		assertFalse(categories.isEmpty());
	}
	
	@Test
	public void testFindAllActive() throws Exception {
		List<Category> categories = Category.findAllActive();
		assertFalse(categories.stream().anyMatch(category -> category.getName().equals("Square")));
		assertTrue(categories.stream().anyMatch(category -> category.getName().equals("Cool")));
	}

	@Test
	public void testFindByPk() throws Exception {
		Category category = Category.findByPk(1000);
		assertNotNull(category);
		assertEquals("Cool", category.getName());
		assertEquals(StatusCode.A, category.getStatusCode());
	}
	
	@Test
	public void testForCustomer() throws Exception {
		Category category = Category.findCategoryForCustomer(3484);
		assertNotNull(category);
		assertEquals("Cool", category.getName());
	}
	
	@Test
	public void testSave() throws Exception {
		String randomName = Double.toHexString(Math.random()).substring(4);
		
		Category category = new Category();
		category.setName(randomName);
		
		assertTrue(category.isNew());
		category.save();
		assertFalse(category.isNew());
		
		Category category2 = Category.findByPk(category.getCategoryId());
		assertEquals(randomName, category2.getName());
		assertEquals(category, category2);
		
		randomName = Double.toHexString(Math.random()).substring(4);

		category.setName(randomName);
		category.save();
		
		category2 = Category.findByPk(category.getCategoryId());
		assertEquals(randomName, category2.getName());
		assertEquals(category, category2);
	}

}
