package test;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;

import com.usatech.dms.sales.Affiliate;
import com.usatech.dms.sales.Affiliate.StatusCode;

public class AffiliateTest {

	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("dms-dev.properties", AffiliateTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
	}
	
	@Test
	public void testFindAll() throws Exception {
		List<Affiliate> affiliates = Affiliate.findAll();
		assertNotNull(affiliates);
		assertFalse(affiliates.isEmpty());
	}
	
	@Test
	public void testFindAllActive() throws Exception {
		List<Affiliate> affiliates = Affiliate.findAllActive();
		assertFalse(affiliates.stream().anyMatch(affiliate -> affiliate.getName().equals("Saddam Hussein")));
		assertTrue(affiliates.stream().anyMatch(affiliate -> affiliate.getName().equals("Canada")));
	}

	@Test
	public void testFindByPk() throws Exception {
		Affiliate affiliate = Affiliate.findByPk(1000);
		assertNotNull(affiliate);
		assertEquals("Canada", affiliate.getName());
		assertEquals(StatusCode.A, affiliate.getStatusCode());
	}
	
	@Test
	public void testForCustomer() throws Exception {
		Affiliate affiliate = Affiliate.findAffiliateForCustomer(3484);
		assertNotNull(affiliate);
		assertEquals("Canada", affiliate.getName());
	}
	
	@Test
	public void testSave() throws Exception {
		String randomName = Double.toHexString(Math.random()).substring(4);
		
		Affiliate affiliate = new Affiliate();
		affiliate.setName(randomName);
		
		assertTrue(affiliate.isNew());
		affiliate.save();
		assertFalse(affiliate.isNew());
		
		Affiliate affiliate2 = Affiliate.findByPk(affiliate.getAffiliateId());
		assertEquals(randomName, affiliate2.getName());
		assertEquals(affiliate, affiliate2);
		
		randomName = Double.toHexString(Math.random()).substring(4);

		affiliate.setName(randomName);
		affiliate.save();
		
		affiliate2 = Affiliate.findByPk(affiliate.getAffiliateId());
		assertEquals(randomName, affiliate2.getName());
		assertEquals(affiliate, affiliate2);
	}

}
