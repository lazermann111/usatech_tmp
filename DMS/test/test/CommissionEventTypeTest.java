package test;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;

import org.apache.commons.configuration.Configuration;

import com.usatech.dms.sales.*;
import com.usatech.dms.sales.CommissionEventType.CommissionEventTypeCode;
import com.usatech.dms.sales.CommissionEventType.YesNo;

public class CommissionEventTypeTest {
	
	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("dms-dev.properties", CommissionEventTypeTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
	}
	
	@Test
	public void testByPk() throws Exception {
		CommissionEventType eventType = CommissionEventType.findByPk(CommissionEventTypeCode.ACT);
		assertNotNull(eventType);
		assertEquals("Activation", eventType.getName());
		assertNotNull(eventType.getDesc());
		assertNotNull(eventType.getCommissionDesc());
		assertEquals(YesNo.Y, eventType.getResponsibilityFlag());
		assertEquals(CommissionEventTypeCode.ACT, eventType.getCode());
	}
	
	@Test
	public void testCodes() throws Exception {
		assertNotNull(CommissionEventType.findByPk("ACT"));
		assertNotNull(CommissionEventType.findByPk("DEACT"));
		assertNotNull(CommissionEventType.findByPk("ASS"));
		assertNotNull(CommissionEventType.findByPk("UNASS"));
		assertNotNull(CommissionEventType.findByPk("POSADJ"));
		assertNotNull(CommissionEventType.findByPk("NEGADJ"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testWrongCode() throws Exception {
		CommissionEventType.findByPk("FOO");
	}
	
	@Test
	public void testFixedWidth() throws Exception {
		System.out.println(CommissionEventType.findByPk("UNASS").getFixedWidthDesc());
	}
	
	@Test
	public void testEquals() throws Exception {
		CommissionEventType et1 = CommissionEventType.findByPk("ASS");
		CommissionEventType et2 = CommissionEventType.findByPk(CommissionEventType.CommissionEventTypeCode.ASS);
		assertEquals(et1, et2);
	}
}
