package test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;

import org.apache.commons.configuration.Configuration;

import com.ibm.icu.text.DateFormat;
import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.sales.*;
import com.usatech.dms.sales.CommissionEventType.CommissionEventTypeCode;
import com.usatech.dms.sales.SalesRep.StatusCode;

public class CommissionEventTest {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/DD/YYYY HH:mm:ss");
	
	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("dms-dev.properties", CommissionEventTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
	}
	
	@Before
	public void clearEvents() throws Exception {
		DataLayerMgr.executeCall("DELETE_ALL_DEVICE_COMMISSION_EVENTS", new String[] {"K3TS0001"}, true);
	}
	
	@Test
	public void testFindBySalesRep() throws Exception {
		List<CommissionEvent> events = CommissionEvent.findBySalesRep(1000);
		assertNotNull(events);
		assertFalse(events.isEmpty());
	}

	@Test
	public void testByPk() throws Exception {
		CommissionEvent event = CommissionEvent.findByPk(1000);
		assertNotNull(event);
		assertEquals(1000, event.getSalesRepId());
		assertEquals("K3TS123123123", event.getDeviceSerialCd());
		assertEquals(CommissionEventTypeCode.ACT, event.getEventTypeCode());
		assertEquals(1440708738000L, event.getTimestamp().getTime());
		assertEquals("test", event.getNotes());
	}
	
	@Test
	public void testFindCurrentByDevice() throws Exception {
		CommissionEvent event = CommissionEvent.findCurrentByDevice("K3TS123123123");
		assertNotNull(event);
		assertEquals(1060, event.getCommissionEventId());
	}
	
	@Test
	public void testSave() throws Exception {
		String randomNotes = Double.toHexString(Math.random());
		
		CommissionEvent event = new CommissionEvent();
		event.setDeviceSerialCd("K3TS0001");
		event.setSalesRepId(1001);
		event.setEventTypeCode(CommissionEventTypeCode.ASS);
		event.setTimestamp(new Date());
		event.setNotes(randomNotes);
		
		assertTrue(event.isNew());
		event.save();
		assertFalse(event.isNew());
		
		CommissionEvent event2 = CommissionEvent.findByPk(event.getCommissionEventId());
		assertEquals(randomNotes, event2.getNotes());
		assertEquals(event, event2);
		
		event.delete();
	}
	
	@Test
	public void testCreateCommissionEvent() throws Exception {
		CommissionEvent event = DeviceActions.createCommissionEvent("K3TS0001", 1001L, CommissionEventTypeCode.ASS.toString(), new Date());
		assertFalse(event.isNew());
		
		CommissionEvent event2 = CommissionEvent.findByPk(event.getCommissionEventId());
		assertEquals(event, event2);
		
		event.delete();
	}
	
	@Test
	public void testCreateCommissionEventFallback() throws Exception {
		CommissionEvent event = DeviceActions.createCommissionEvent("K3TS0001", CommissionEvent.USE_CURRENT_SALES_REP_ID, CommissionEventTypeCode.ASS.toString(), new Date(), 1001L);
		assertFalse(event.isNew());
		
		CommissionEvent event2 = CommissionEvent.findByPk(event.getCommissionEventId());
		assertEquals(event, event2);
		
		event.delete();
	}
	
	@Test
	public void testDelete() throws Exception {
		CommissionEvent event = new CommissionEvent();
		event.setDeviceSerialCd("K3TS0001");
		event.setSalesRepId(1001);
		event.setEventTypeCode(CommissionEventTypeCode.ASS);
		event.setTimestamp(new Date());
		
		assertTrue(event.isNew());
		event.save();
		assertFalse(event.isNew());
		
		List<CommissionEvent> events1 = CommissionEvent.findByDevice("K3TS0001");
		assertTrue(events1.stream().anyMatch(e -> e.equals(event)));
		
		event.delete();
		
//		List<CommissionEvent> events2 = CommissionEvent.findByDevice("K3TS0001");
//		assertFalse(events2.stream().anyMatch(e -> e.equals(event)));
	}

	@Test
	public void testDuplicateDetection() throws Exception {
		CommissionEvent adj1 = new CommissionEvent();
		adj1.setDeviceSerialCd("K3TS0001");
		adj1.setSalesRepId(1001);
		adj1.setEventTypeCode(CommissionEventTypeCode.POSADJ);
		adj1.setTimestamp(new Date());
		adj1.save(); 
		
		CommissionEvent adj2 = new CommissionEvent();
		adj2.setDeviceSerialCd("K3TS0001");
		adj2.setSalesRepId(1001);
		adj2.setEventTypeCode(CommissionEventTypeCode.POSADJ);
		adj2.setTimestamp(new Date());
		adj2.save(); 

		assertEquals(2, CommissionEvent.findByDevice("K3TS0001").size()); // adjustments are always inserted
		
		CommissionEvent act1 = new CommissionEvent();
		act1.setDeviceSerialCd("K3TS0001");
		act1.setSalesRepId(1001);
		act1.setEventTypeCode(CommissionEventTypeCode.ACT);
		act1.setTimestamp(new Date());
		act1.save(); 
		
		CommissionEvent act2 = new CommissionEvent();
		act2.setDeviceSerialCd("K3TS0001");
		act2.setSalesRepId(1001);
		act2.setEventTypeCode(CommissionEventTypeCode.ACT);
		act2.setTimestamp(new Date());
		act2.save(); 
		
		assertEquals(3, CommissionEvent.findByDevice("K3TS0001").size()); // act2 should be a dupe
		assertEquals(act1.getCommissionEventId(), act2.getCommissionEventId());
		
		CommissionEvent ass1 = new CommissionEvent();
		ass1.setDeviceSerialCd("K3TS0001");
		ass1.setSalesRepId(1001);
		ass1.setEventTypeCode(CommissionEventTypeCode.ASS);
		ass1.setTimestamp(new Date());
		ass1.save(); 
		
		assertEquals(4, CommissionEvent.findByDevice("K3TS0001").size()); // 
	}
	
	@Test
	public void testUseCurrentSalesRep() throws Exception {
		CommissionEvent event1 = new CommissionEvent();
		event1.setDeviceSerialCd("K3TS0001");
		event1.setSalesRepId(1001);
		event1.setEventTypeCode(CommissionEventTypeCode.ASS);
		event1.setTimestamp(new Date());
		
		assertTrue(event1.isNew());
		event1.save();
		assertFalse(event1.isNew());
		
		CommissionEvent event2 = CommissionEvent.findByPk(event1.getCommissionEventId());
		assertEquals(event1, event2);
		
		CommissionEvent event3 = new CommissionEvent();
		event3.setDeviceSerialCd("K3TS0001");
		event3.setSalesRepId(CommissionEvent.USE_CURRENT_SALES_REP_ID);
		event3.setEventTypeCode(CommissionEventTypeCode.DEACT);
		event3.setTimestamp(new Date());
		
		event3.save();
		
		CommissionEvent event4 = CommissionEvent.findCurrentByDevice("K3TS0001");
		//assertTrue(event4.getEventTypeCode() == CommissionEventTypeCode.DEACT);
		//assertTrue(event4.getSalesRepId() == 1001);
		assertNull(event4);
	}
	
	@Test
	public void testFallbackSalesRep() throws Exception {
		CommissionEvent event3 = new CommissionEvent();
		event3.setDeviceSerialCd("K3TS0001");
		event3.setSalesRepId(CommissionEvent.USE_CURRENT_SALES_REP_ID);
		event3.setEventTypeCode(CommissionEventTypeCode.ACT);
		event3.setTimestamp(new Date());
		event3.setFallbackSalesRepId(1001);
		event3.save();
		
		CommissionEvent event4 = CommissionEvent.findCurrentByDevice("K3TS0001");
		assertNotNull(event4);
		assertTrue(event4.getEventTypeCode() == CommissionEventTypeCode.ACT);
		assertTrue(event4.getSalesRepId() == 1001);
	}

	@Test(expected=SQLException.class)
	public void testFallbackSalesRepNoCurrentNoFallback() throws Exception {
		CommissionEvent event3 = new CommissionEvent();
		event3.setDeviceSerialCd("K3TS0001");
		event3.setSalesRepId(CommissionEvent.USE_CURRENT_SALES_REP_ID);
		event3.setEventTypeCode(CommissionEventTypeCode.DEACT);
		event3.setTimestamp(new Date());
		event3.save();
	}

	@Test(expected=SQLException.class)
	public void testNoSalesRep() throws Exception {
		try {
			CommissionEvent event1 = new CommissionEvent();
			event1.setDeviceSerialCd("K3TS0001");
			event1.setSalesRepId(1001);
			event1.setEventTypeCode(CommissionEventTypeCode.ACT);
			event1.setTimestamp(new Date());
			event1.save();
		} catch (SQLException e) {
			// don't throw a SQLException here because this isn't the part under 
			// test and a SQLException would cause the test to pass
			throw new AssertionError();
		}

		CommissionEvent event3 = new CommissionEvent();
		event3.setDeviceSerialCd("K3TS0001");
		event3.setEventTypeCode(CommissionEventTypeCode.DEACT);
		event3.setTimestamp(new Date());
		event3.save();
	}
	
	@Test
	public void testInstant() throws Exception {
		Date terminalDate = ConvertUtils.convert(Date.class, "09/07/2015 14:42:08");
		Instant endDateInstant = terminalDate.toInstant();
		Instant midnightInstant = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).toInstant(ZoneOffset.UTC);
		
//		System.out.println(midnightInstant);
//		System.out.println(endDateInstant);
//		System.out.println(endDateInstant.isBefore(midnightInstant));
		
		assertTrue(endDateInstant.isBefore(midnightInstant));
	}
	
	@Test
	public void testChangeSalesRep() throws Exception {
		CommissionEvent event1 = new CommissionEvent();
		event1.setDeviceSerialCd("K3TS0001");
		event1.setSalesRepId(1000);
		event1.setEventTypeCode(CommissionEventTypeCode.ACT);
		event1.setTimestamp(new Date());
		event1.save();
		
		CommissionEvent event2 = new CommissionEvent();
		event2.setDeviceSerialCd("K3TS0001");
		event2.setSalesRepId(1001);
		event2.setEventTypeCode(CommissionEventTypeCode.ACT);
		event2.setTimestamp(new Date());
		event2.save();
		
		List<CommissionEvent> events = CommissionEvent.findByDevice("K3TS0001");
		assertEquals(1, events.size());
		assertEquals(1001, events.get(0).getSalesRepId());
	}
}
