package test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;

import org.apache.commons.configuration.Configuration;

import com.usatech.dms.sales.SalesRep;
import com.usatech.dms.sales.SalesRep.StatusCode;

public class SalesRepTest {

	private static Configuration config;
	
	@BeforeClass
	public static void init() throws Exception {
		config = MainWithConfig.loadConfig("dms-dev.properties", SalesRepTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
	}
	
	@Test
	public void testFindAll() throws Exception {
		List<SalesRep> salesReps = SalesRep.findAll();
		assertNotNull(salesReps);
		assertFalse(salesReps.isEmpty());
	}
	
	@Test
	public void testFindAllActive() throws Exception {
		List<SalesRep> salesReps = SalesRep.findAllActive();
		assertFalse(salesReps.stream().anyMatch(salesRep -> salesRep.getLastName().equals("McCormick")));
		assertTrue(salesReps.stream().anyMatch(salesRep -> salesRep.getLastName().equals("Marsh")));
	}

	@Test
	public void testFindByPk() throws Exception {
		SalesRep salesRep = SalesRep.findByPk(1000);
		assertNotNull(salesRep);
		assertEquals("Stan", salesRep.getFirstName());
		assertEquals("Marsh", salesRep.getLastName());
		assertEquals(StatusCode.A, salesRep.getStatusCode());
	}
	
	@Test
	public void testResponsibleForDeviceSimple() throws Exception {
		SalesRep salesRep = SalesRep.findSalesRepResponsibleForDevice("K3TS123123123");
		assertNotNull(salesRep);
		assertEquals("Stan Marsh", salesRep.getName());
	}
	
	@Test
	public void testResponsibleForDeviceComplex() throws Exception {
		// dependent on specific database data
		SalesRep salesRep = SalesRep.findSalesRepResponsibleForDevice("K3TS123123126");
		assertNotNull(salesRep);
		assertEquals(1008, salesRep.getSalesRepId());
	}
	
	@Test
	public void testResponsibleForTerminal() throws Exception {
		SalesRep salesRep = SalesRep.findSalesRepResponsibleForTerminal(20387, new Date());
		assertNotNull(salesRep);
		assertEquals(1002, salesRep.getSalesRepId());
	}
	@Test
	public void testSave() throws Exception {
		String randomFirstName = Double.toHexString(Math.random());
		
		SalesRep salesRep = new SalesRep();
		salesRep.setFirstName(randomFirstName);
		salesRep.setLastName("Last Name");
		
		assertTrue(salesRep.isNew());
		salesRep.save();
		assertFalse(salesRep.isNew());
		
		SalesRep salesRep2 = SalesRep.findByPk(salesRep.getSalesRepId());
		assertEquals(randomFirstName, salesRep2.getFirstName());
		assertEquals(salesRep, salesRep2);
	}

}
