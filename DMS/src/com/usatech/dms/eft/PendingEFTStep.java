package com.usatech.dms.eft;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
import simple.util.LinkedHashMap;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class PendingEFTStep extends AbstractStep{

    private static final String DEFAULT_SORT_INDEX = "15, 1, -14";
    
	private static final String SQL_START  = "SELECT P.CUSTOMER_NAME, P.CUSTOMER_BANK_ID, P.BANK_ACCT_NBR, P.PAYMENT_METHOD, P.BUSINESS_UNIT_NAME, P.ACCOUNT_TITLE,"+
    "P.BANK_ROUTING_NBR, P.PAY_MIN_AMOUNT, P.EFT_ID, P.STATUS,"+
    "P.BATCH_REF_NBR, P.DESCRIPTION, P.PAY_CYCLE_ID,"+
    "COALESCE(P.CURRENCY_ID, 0) CURRENCY_ID, C.CURRENCY_CODE, C.CURRENCY_SYMBOL,"+
    "TO_CHAR(P.GROSS_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') GROSS_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.REFUND_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') REFUND_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.CHARGEBACK_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') CHARGEBACK_AMOUNT_FORMATTED, "+
    "TO_CHAR(CASE WHEN P.REFUND_AMOUNT IS NULL THEN P.CHARGEBACK_AMOUNT WHEN P.CHARGEBACK_AMOUNT IS NULL THEN P.REFUND_AMOUNT ELSE P.REFUND_AMOUNT + P.CHARGEBACK_AMOUNT END, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') RF_CB_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.PROCESS_FEE_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') PROCESS_FEE_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.PROCESS_FEE_COMMISSION_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') PFC_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.SERVICE_FEE_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') SERVICE_FEE_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.SERVICE_FEE_COMMISSION_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') SFC_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.ADJUST_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') ADJUST_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.FAILED_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') FAILED_AMOUNT_FORMATTED, "+
    "TO_CHAR(P.NET_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') NET_AMOUNT_FORMATTED, "+
    "TO_CHAR(NULLIF(P.BATCH_DATE, MIN_DATE()),'mm/dd/yyyy') BATCH_DATE, "+
    "CASE WHEN COALESCE(P.NET_AMOUNT, 0) > 0 THEN 'green' ELSE 'red' END NET_AMOUNT_COLOR, " +
    "CS.STATUS_NAME CUSTOMER_STATUS, CA.NAME ADDRESS_NAME, CA.ADDRESS1, CA.ADDRESS2, CA.CITY, CA.STATE, CA.ZIP, UL.FIRST_NAME, UL.LAST_NAME, UL.EMAIL, P.CUSTOMER_ID, "+
    "P.GROSS_AMOUNT, P.REFUND_AMOUNT, P.CHARGEBACK_AMOUNT, P.PROCESS_FEE_AMOUNT, P.PROCESS_FEE_COMMISSION_AMOUNT, P.SERVICE_FEE_AMOUNT, P.SERVICE_FEE_COMMISSION_AMOUNT, P.ADJUST_AMOUNT, P.FAILED_AMOUNT, P.NET_AMOUNT, "+
	"CASE WHEN P.REFUND_AMOUNT IS NULL THEN P.CHARGEBACK_AMOUNT WHEN P.CHARGEBACK_AMOUNT IS NULL THEN P.REFUND_AMOUNT ELSE P.REFUND_AMOUNT + P.CHARGEBACK_AMOUNT END RF_CB_AMOUNT ";
	
    private static final String SQL_BASE = 	" FROM CORP.VW_PENDING_OR_LOCKED_PAYMENTS P" +
    " JOIN CORP.CUSTOMER CUS ON P.CUSTOMER_ID = CUS.CUSTOMER_ID" +
    " JOIN CORP.CUSTOMER_STATUS CS ON CUS.STATUS = CS.STATUS" +
	" LEFT OUTER JOIN CORP.CURRENCY C ON P.CURRENCY_ID = C.CURRENCY_ID" +
    " LEFT OUTER JOIN CORP.CUSTOMER_ADDR CA ON CUS.CUSTOMER_ID = CA.CUSTOMER_ID AND CA.STATUS = 'A' AND CA.ADDR_TYPE = 2" +
	" LEFT OUTER JOIN REPORT.USER_LOGIN UL ON CUS.USER_ID = UL.USER_ID ";
    	
    private static final String[] SORT_FIELDS = {
    	"UPPER(P.CUSTOMER_NAME)","UPPER(P.ACCOUNT_TITLE)","P.BANK_ACCT_NBR","P.CURRENCY_ID","UPPER(P.BUSINESS_UNIT_NAME)",
    	"P.PAYMENT_METHOD","P.GROSS_AMOUNT","COALESCE(P.REFUND_AMOUNT, 0) + COALESCE(P.CHARGEBACK_AMOUNT, 0)","P.PROCESS_FEE_AMOUNT","P.PROCESS_FEE_COMMISSION_AMOUNT","P.SERVICE_FEE_AMOUNT","P.SERVICE_FEE_COMMISSION_AMOUNT",
    	"P.ADJUST_AMOUNT","P.NET_AMOUNT","P.FAILED_AMOUNT","P.BATCH_DATE","P.STATUS","P.EFT_ID",
    	"CUS.STATUS", "UPPER(CA.NAME)", "UPPER(CA.ADDRESS1)", "UPPER(CA.ADDRESS2)", "UPPER(CA.CITY)", "UPPER(CA.STATE)", "UPPER(CA.ZIP)", "UPPER(UL.FIRST_NAME)", 
    	"UPPER(UL.LAST_NAME)", "UPPER(UL.EMAIL)", "P.CUSTOMER_ID"
        };
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);		
			if (!StringUtils.isBlank(action)) {
				try {
					String[] ids = form.getStringArray("pid", false);
					if(ids.length > 0){
						for(int i=0; i<ids.length; i++) {
					        if ("Lock".equalsIgnoreCase(action)) {
					        	if (!"Locked".equalsIgnoreCase(form.getString("pid_status_" + ids[i] , false)))
					        		DataLayerMgr.executeCall("LOCK_EFT", new Object[] {ids[i]}, true);
					        } else if ("Unlock".equalsIgnoreCase(action)) {
					        	if ("Locked".equalsIgnoreCase(form.getString("pid_status_" + ids[i] , false)))
					        		DataLayerMgr.executeCall("UNLOCK_EFT", new Object[] {ids[i]}, true);
					        } else if ("Approve".equalsIgnoreCase(action)) {
					        	if ("Locked".equalsIgnoreCase(form.getString("pid_status_" + ids[i] , false)))
					        		DataLayerMgr.executeCall("APPROVE_EFT", new Object[] {ids[i]}, true);
					        }
						}
					}
				} catch (Exception e) {
					throw new ServletException("Error performing action " + action, e);
				}
			}
			
			String uiAction = form.getStringSafely("ui_action", "");
			if ("List EFTs".equalsIgnoreCase(uiAction)) {
				Map<Integer, String> params = new LinkedHashMap<Integer, String>();
				int paramCnt = 0;
				StringBuilder queryBase = new StringBuilder(SQL_BASE);
		
				String search_param = form.getString(DevicesConstants.PARAM_SEARCH_PARAM, false);
				if (!StringHelper.isBlank(search_param)) { 
					String search_type = form.getString(DevicesConstants.PARAM_SEARCH_TYPE, false);
		        	if ("customer_name".equalsIgnoreCase(search_type)) {
		        		queryBase.append(" WHERE LOWER(P.CUSTOMER_NAME) LIKE LOWER(?) ");
		        		params.put(paramCnt++, Helper.convertParam(search_param, false));
		        	} else if ("account_title".equalsIgnoreCase(search_type)) {
		        		queryBase.append(" WHERE LOWER(P.ACCOUNT_TITLE) LIKE LOWER(?) ");
		        		params.put(paramCnt++, Helper.convertParam(search_param, false));
		        	} else if ("eft_id".equalsIgnoreCase(search_type)) {
		        		queryBase.append(" WHERE P.EFT_ID = CAST (? AS NUMERIC) ");
		        		params.put(paramCnt++, search_param);
		        	} else if ("net_amount".equalsIgnoreCase(search_type)) {
		        		if (Helper.isNumeric(search_param)) {
		        			queryBase.append(" WHERE P.NET_AMOUNT >= CAST (? AS NUMERIC) ");
		        			params.put(paramCnt++, search_param);
		        		}
		        	} else if ("net_amt_less_eq".equalsIgnoreCase(search_type)) {
		        		if (Helper.isNumeric(search_param)) {
		        			queryBase.append(" WHERE P.NET_AMOUNT <= CAST (? AS NUMERIC) ");
		        			params.put(paramCnt++, search_param);
		        		}
		        	}
				}
				
				String pay_cycle = form.getString("pay_cycle", false);
				if (Helper.isNumeric(pay_cycle)) {
					if (queryBase.indexOf(" WHERE ") > -1)
						queryBase.append(" AND P.PAY_CYCLE_ID = CAST (? AS NUMERIC) ");
					else
						queryBase.append(" WHERE P.PAY_CYCLE_ID = CAST (? AS NUMERIC) ");
					params.put(paramCnt++, pay_cycle);
				}
				
				String business_unit = form.getString("business_unit", false);
				if (Helper.isNumeric(business_unit)) {
					if (queryBase.indexOf(" WHERE ") > -1)
						queryBase.append(" AND ");
					else
						queryBase.append(" WHERE ");
					queryBase.append("P.BUSINESS_UNIT_NAME = (SELECT BUSINESS_UNIT_NAME FROM CORP.BUSINESS_UNIT WHERE BUSINESS_UNIT_ID = CAST (? AS NUMERIC)) ");			
					params.put(paramCnt++, business_unit);
				}
				
				String currency = form.getString("currency", false);
				if (Helper.isNumeric(currency)) {
					if (queryBase.indexOf(" WHERE ") > -1)
						queryBase.append(" AND ");
					else
						queryBase.append(" WHERE ");
					queryBase.append("P.CURRENCY_ID = CAST (? AS NUMERIC) ");			
					params.put(paramCnt++, currency);
				}
				
				String status = form.getString("status", false);
				if (!StringUtils.isBlank(status)) {
					if (queryBase.indexOf(" WHERE ") > -1)
						queryBase.append(" AND ");
					else
						queryBase.append(" WHERE ");
					queryBase.append("P.STATUS = ? ");
					params.put(paramCnt++, status);
				}
											
				String dataFormat = form.getStringSafely("data_format", "");
				if ("CSV".equalsIgnoreCase(dataFormat)) {
					response.setContentType("application/force-download");
		    		response.setHeader("Content-Transfer-Encoding", "binary"); 
		    		response.setHeader("Content-Disposition", "attachment;filename=\"Pending EFTs.csv.txt\"");
		    		PrintWriter out = response.getWriter();
		    		StringBuilder sb = new StringBuilder();
		    		String[] headers = {"EFT ID", "Customer", "Account Title", "Account #", "Currency", "Business Unit", "Method", "Settled", "Refunds & Chargebacks", "Process Fees", "Process Fee Commissions", "Service Fees", "Service Fee Commissions", "Adjustments", "Net Amount", "Declined", "Batch Date", "Status", "Customer Status", "Address Name", "Address 1", "Address 2", "City", "State", "Postal", "First Name", "Last Name", "Email", "Customer ID"};         	    	
		    		out.print(StringUtils.join(headers, ",", "\""));
		    		out.flush();
		    		String query = new StringBuilder(SQL_START).append(queryBase).append(" ORDER BY P.STATUS, UPPER(P.CUSTOMER_NAME), P.BATCH_DATE DESC").toString();
		    		Results rs = DataLayerMgr.executeSQL("REPORT", query,  params.values().toArray(), null);
		    		while (rs.next()) {
		    			sb.setLength(0);    	    		
			    		sb.append("\""); sb.append(rs.getFormattedValue("EFT_ID")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("CUSTOMER_NAME")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("ACCOUNT_TITLE")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("BANK_ACCT_NBR")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("CURRENCY_CODE")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("BUSINESS_UNIT_NAME")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("PAYMENT_METHOD")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("GROSS_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("RF_CB_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("PROCESS_FEE_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("PROCESS_FEE_COMMISSION_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("SERVICE_FEE_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("SERVICE_FEE_COMMISSION_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("ADJUST_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("NET_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("FAILED_AMOUNT")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("BATCH_DATE")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("STATUS")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("CUSTOMER_STATUS")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("ADDRESS_NAME")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("ADDRESS1")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("ADDRESS2")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("CITY")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("STATE")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("ZIP")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("FIRST_NAME")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("LAST_NAME")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("EMAIL")); sb.append("\",");
			    		sb.append("\""); sb.append(rs.getFormattedValue("CUSTOMER_ID")); sb.append("\"");
			    		out.print("\r\n");
			    		out.print(sb.toString().replaceAll("\r|\n", ""));
			    		out.flush();
			    	};
			    	out.close();
			    	return;
				} else if ("HTML".equalsIgnoreCase(dataFormat)) {
			        String paramTotalCount = PaginationUtil.getTotalField(null);
			        String paramPageSize = PaginationUtil.getSizeField(null);
			        String paramSortIndex = PaginationUtil.getSortField(null);
			        
			        request.setAttribute(paramTotalCount, "-1");
			        request.setAttribute(paramPageSize, "-1");
			
			        String sortIndex = form.getString(paramSortIndex, false);
			        sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
			        String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
			
			        String query = new StringBuilder(SQL_START).append(queryBase).append(orderBy).toString();
			        Results results = DataLayerMgr.executeSQL("REPORT", query, params.values().toArray(), null);
			        request.setAttribute("resultlist", results);
				}
			}
		} catch(Exception e) {
	        throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
}
