package com.usatech.dms.eft;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.*;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class SearchACHStep extends AbstractStep {

	private static final String DEFAULT_SORT_INDEX = "-2";
	private static final String SQL_START  = !DialectResolver.isOracle()
			? "select a.ach_id ACH_ID, a.created_ts CREATE_TS, TO_CHAR(a.created_ts, 'MM/DD/YYYY HH24:MI:SS') CREATE_TSF, a.credit_count CREDIT_COUNT, TO_CHAR(a.credit_total, 'FM' || coalesce(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') CREDIT_TOTAL, a.debit_count DEBIT_COUNT, TO_CHAR(a.debit_total, 'FM' || coalesce(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') DEBIT_TOTAL, a.usat_bank_acct_nbr USAT_BANK_ACCT_NUM, a.usat_bank_routing_nbr USAT_ROUTING_NUM, c.currency_id CURRENCY_ID, c.currency_code CURRENCY_CD, a.create_user_id CREATE_USER_ID, cu.user_name CREATE_USER_NAME, af.ach_file_id ACH_FILE_ID, af.status_cd STATUS_CD, (CASE af.status_cd WHEN 'N' THEN 'Not Uploaded' WHEN 'M' THEN 'Uploaded Manually' WHEN 'A' THEN 'Uploaded Automatically' WHEN 'F' THEN 'Upload Failed' WHEN 'E' THEN 'Expired' WHEN 'C' THEN 'Cancelled' END) STATUS_NAME, af.create_user_id FILE_CREATE_USER_ID, fcu.user_name FILE_CREATE_USER_NAME, af.created_ts FILE_CREATE_TS, TO_CHAR(af.created_ts, 'MM/DD/YYYY HH24:MI:SS') FILE_CREATE_TSF, af.update_user_id FILE_UPDATE_USER_ID, uu.user_name FILE_UPDATE_USER_NAME, af.upload_ts FILE_UPLOAD_TS, TO_CHAR(af.upload_ts, 'MM/DD/YYYY HH24:MI:SS') FILE_UPLOAD_TSF, af.cancel_ts FILE_CANCEL_TS, TO_CHAR(af.cancel_ts, 'MM/DD/YYYY HH24:MI:SS') FILE_CANCEL_TSF, case when af.created_ts < LOCALTIMESTAMP::date then 'Y' else 'N' end IS_EXPIRED, case when af.created_ts < LOCALTIMESTAMP::date then 'Expired' else TO_CHAR(((LOCALTIMESTAMP +interval '1 day')::date - af.created_ts)*24, 'FM99.9') || ' hours before expired' end TIME_REMAINING, af.file_transfer_id FILE_TRANSFER_ID, ft.file_transfer_name FILE_TRANSFER_NAME "
			: "select a.ach_id ACH_ID, a.created_ts CREATE_TS, TO_CHAR(a.created_ts, 'MM/DD/YYYY HH24:MI:SS') CREATE_TSF, a.credit_count CREDIT_COUNT, TO_CHAR(a.credit_total, 'FM' || NVL(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') CREDIT_TOTAL, a.debit_count DEBIT_COUNT, TO_CHAR(a.debit_total, 'FM' || NVL(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') DEBIT_TOTAL, a.usat_bank_acct_nbr USAT_BANK_ACCT_NUM, a.usat_bank_routing_nbr USAT_ROUTING_NUM, c.currency_id CURRENCY_ID, c.currency_code CURRENCY_CD, a.create_user_id CREATE_USER_ID, cu.user_name CREATE_USER_NAME, af.ach_file_id ACH_FILE_ID, af.status_cd STATUS_CD, decode(af.status_cd, 'N', 'Not Uploaded', 'M', 'Uploaded Manually', 'A', 'Uploaded Automatically', 'F', 'Upload Failed', 'E', 'Expired', 'C', 'Cancelled') STATUS_NAME, af.create_user_id FILE_CREATE_USER_ID, fcu.user_name FILE_CREATE_USER_NAME, af.created_ts FILE_CREATE_TS, TO_CHAR(af.created_ts, 'MM/DD/YYYY HH24:MI:SS') FILE_CREATE_TSF, af.update_user_id FILE_UPDATE_USER_ID, uu.user_name FILE_UPDATE_USER_NAME, af.upload_ts FILE_UPLOAD_TS, TO_CHAR(af.upload_ts, 'MM/DD/YYYY HH24:MI:SS') FILE_UPLOAD_TSF, af.cancel_ts FILE_CANCEL_TS, TO_CHAR(af.cancel_ts, 'MM/DD/YYYY HH24:MI:SS') FILE_CANCEL_TSF, case when af.created_ts < trunc(sysdate) then 'Y' else 'N' end IS_EXPIRED, case when af.created_ts < trunc(sysdate) then 'Expired' else TO_CHAR((trunc(sysdate+1) - af.created_ts)*24, 'FM99.9') || ' hours before expired' end TIME_REMAINING, af.file_transfer_id FILE_TRANSFER_ID, ft.file_transfer_name FILE_TRANSFER_NAME ";
	private static final String SQL_BASE = !DialectResolver.isOracle() 
			? "from corp.ach a join corp.ach_file af on af.ach_file_id = (  select max(fid) from (select first_value(ach_file_id) OVER (ORDER BY af2.created_ts DESC) as fid from corp.ach_file af2 where af2.ach_id = a.ach_id) sq_end) left outer join corp.currency c on a.currency_id = c.currency_id left outer join report.user_login cu on cu.user_id = a.create_user_id left outer join report.user_login fcu on fcu.user_id = af.create_user_id left outer join report.user_login uu on uu.user_id = af.update_user_id left outer join device.file_transfer ft on ft.file_transfer_id = af.file_transfer_id "
			: "from corp.ach a join corp.ach_file af on af.ach_file_id = ( select max(af2.ach_file_id) keep (dense_rank first order by af2.created_ts desc) from corp.ach_file af2 where af2.ach_id = a.ach_id) left outer join corp.currency c on a.currency_id = c.currency_id left outer join report.user_login cu on cu.user_id = a.create_user_id left outer join report.user_login fcu on fcu.user_id = af.create_user_id left outer join report.user_login uu on uu.user_id = af.update_user_id left outer join device.file_transfer ft on ft.file_transfer_id = af.file_transfer_id ";
	private static final String[] SORT_FIELDS = {"a.ach_id", "a.created_ts", "a.credit_count", "a.credit_total", "a.debit_count", "a.debit_total", "af.status_cd", "trunc(sysdate+1) - af.created_ts"};
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);
			
			StringBuilder queryBase = new StringBuilder(SQL_BASE);
			String paramTotalCount = PaginationUtil.getTotalField(null);
			String paramPageIndex = PaginationUtil.getIndexField(null);
			String paramPageSize = PaginationUtil.getSizeField(null);
			String paramSortIndex = PaginationUtil.getSortField(null);
			List<Object> params = new ArrayList<>();
			
			String search_param = form.getString(DevicesConstants.PARAM_SEARCH_PARAM, false);
			if (!StringHelper.isBlank(search_param)) {
				String search_type = form.getString(DevicesConstants.PARAM_SEARCH_TYPE, false);
				if ("customer_name".equalsIgnoreCase(search_type)) {
					queryBase.append(" where a.ach_id in (select distinct da.ach_id from corp.doc_ach da join corp.doc d on d.doc_id = da.doc_id join corp.customer_bank cb on cb.customer_bank_id = d.customer_bank_id join corp.customer c on c.customer_id = cb.customer_id where lower(c.customer_name) like lower(?))");
					params.add(Helper.convertParam(search_param, false));
				} else if ("account_title".equalsIgnoreCase(search_type)) {
					queryBase.append(" where a.ach_id in (select distinct da.ach_id from corp.doc_ach da join corp.doc d on d.doc_id = da.doc_id join corp.customer_bank cb on cb.customer_bank_id = d.customer_bank_id join corp.customer c on c.customer_id = cb.customer_id where lower(cb.account_title) like lower(?))");
					params.add(Helper.convertParam(search_param, false));
				} else if ("account_num".equalsIgnoreCase(search_type)) {
					queryBase.append(" where a.ach_id in (select distinct da.ach_id from corp.doc_ach da join corp.doc d on d.doc_id = da.doc_id join corp.customer_bank cb on cb.customer_bank_id = d.customer_bank_id join corp.customer c on c.customer_id = cb.customer_id where cb.bank_acct_nbr = ?)");
					params.add(search_param);
				} else if ("routing_num".equalsIgnoreCase(search_type)) {
					queryBase.append(" where a.ach_id in (select distinct da.ach_id from corp.doc_ach da join corp.doc d on d.doc_id = da.doc_id join corp.customer_bank cb on cb.customer_bank_id = d.customer_bank_id join corp.customer c on c.customer_id = cb.customer_id where cb.bank_routing_nbr = ?)");
					params.add(search_param);
				} else if ("eft_id".equalsIgnoreCase(search_type)) {
					queryBase.append(" where a.ach_id in (select distinct da.ach_id from corp.doc_ach da where da.doc_id = ?)");
					params.add(search_param);
				} else if ("ach_id".equalsIgnoreCase(search_type)) {
					queryBase.append(" where a.ach_id = ?");
					params.add(search_param);
				}
			}
			
			String uiAction = form.getStringSafely("ui_action", "");
			if ("List ACHs".equalsIgnoreCase(uiAction)) {
				String status = form.getString("status", false);
				if (!StringUtils.isBlank(status)) {
					if (params.isEmpty())
						queryBase.append("where ");
					else
						queryBase.append("and ");
					if (status.equalsIgnoreCase("U")) {
						queryBase.append("af.status_cd in (?,?) ");
						params.add("M");
						params.add("A");
					} else {
						queryBase.append("af.status_cd = ? ");
						params.add(status);
					}
				}
			}
			


			int totalCount = form.getInt(paramTotalCount, false, -1);
			if (totalCount == -1 || !StringHelper.isBlank(action)) {
				Results total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1) " + queryBase, params.toArray(), null);
				if (total.next()) {
					totalCount = total.getValue(1, int.class);
				} else {
					totalCount = 0;
				}
				request.setAttribute(paramTotalCount, String.valueOf(totalCount));
			}

			int pageIndex = form.getInt(paramPageIndex, false, 1);
			int pageSize = form.getInt(paramPageSize, false, -1);
			if (pageSize < 0) {
				pageSize = 5000;
				request.setAttribute(paramPageSize, pageSize);
			}
			int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
			int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

			String sortIndex = form.getString(paramSortIndex, false);
			sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
			String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

			String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ queryBase + orderBy + " limit ?::bigint) pagination_temp ) sq_end where rnum  >= ?::bigint";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + queryBase
						+ orderBy + ") pagination_temp where ROWNUM <= ?) where rnum  >= ?";
			}
			
			params.add(maxRowToFetch);
			params.add(minRowToFetch);

			Results results = DataLayerMgr.executeSQL("REPORT", query, params.toArray(), null);
			request.setAttribute("resultlist", results);

		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}

}
