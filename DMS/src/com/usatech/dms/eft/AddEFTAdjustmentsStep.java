package com.usatech.dms.eft;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.util.DMSUtils;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.process.ProcessType;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class AddEFTAdjustmentsStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		try {
			if (!"POST".equalsIgnoreCase(request.getMethod()))
				return;

			StringBuilder err = new StringBuilder();
			int updatedCount = 0;
			Object[] result;
			Map<String, Object> params = new HashMap<String, Object>();
			Date terminalDate = new Date();
			params.put("terminalDate", terminalDate);

			String contentType = request.getContentType();
			String uploadFileName = form.getString("file_data", false);
			if (contentType != null && contentType.indexOf("multipart/form-data") >= 0
					&& !StringHelper.isBlank(uploadFileName)) {
				String[] contentTypes = RequestUtils.getPreferredContentType(request, "text/html",
						"application/xhtml+xml", "text/plain");
				boolean textResult = (contentTypes != null && contentTypes.length > 0
						&& contentTypes[0].equalsIgnoreCase("text/plain"));
				Map<String, Object> parameters = new LinkedHashMap<>();
				Object updateFile = form.getAttribute("file_data");
				if (updateFile == null) {
					RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-missing-parameter",
							"The {0} was not specified in the request", "file to upload");
					if (textResult)
						RequestUtils.writeTextResultFromMessages(request, response);
					else
						RequestUtils.redirectWithCarryOver(request, response, "addEFTAdjustments.i", false,
								true);
					return;
				}
				String notifyEmail = form.getString("notifyEmail", false);
				if (!StringUtils.isBlank(notifyEmail))
					try {
						WebHelper.checkEmail(notifyEmail);
					} catch (MessagingException e) {
						RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-invalid-email",
								"You entered an invalid email: {0}. Please enter a valid one", e.getMessage());
						if (textResult)
							RequestUtils.writeTextResultFromMessages(request, response);
						else
							RequestUtils.redirectWithCarryOver(request, response, "update_devices_user_excel.i", false,
									true);
						return;
					}
				InputStream content;
				String path;
				long processRequestId;
				if (updateFile instanceof InputFile) {
					InputFile inputFile = (InputFile) updateFile;
					path = inputFile.getPath();
					content = inputFile.getInputStream();
				} else {
					path = "updateFile";
					content = ConvertUtils.convert(InputStream.class, updateFile);
				}
				parameters.put("emailFromName", Helper.getDMS_EMAIL_FROM_NAME());
				parameters.put("emailFromAddress", Helper.getDMS_EMAIL());
				try {
					parameters.put("updateFile", path);
					String desc = RequestUtils.getTranslator(request).translate("device.update.process.description",
							"Mass EFT Adjustments ''{0}''", path);
					try {
						processRequestId = DMSUtils.registerProcessRequest(request,
								ProcessType.MASS_EFT_ADJUSTMENTS_CSV, desc, notifyEmail, parameters, content);
					} catch(Exception e) {
						RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "device.update.error.enqueue", "Sorry for the inconvenience. We could not process your request at this time. Please try again.", e.getMessage());
						if(textResult)
							RequestUtils.writeTextResultFromMessages(request, response);
						else
							RequestUtils.redirectWithCarryOver(request, response, "addEFTAdjustments.i", false, true);
						return;
					}
				} finally {
					content.close();
				}
				String message = "Request is registered. You will receive notification email when process is finished.";
				form.setRedirectUri("/addEFTAdjustments.i?message=" + message);
			} else {
				String list = form.getString("dev_list", false);
				if (StringHelper.isBlank(list))
					return;

				BigDecimal amount = form.getBigDecimal("amount", true);
				String reason = form.getString("reason", true);

				params.put("eportSerialNums", list.replace('\n', ',').replace("\r", "").replace(" ", ""));
				params.put("terminalDate", terminalDate);
				Results rs = DataLayerMgr.executeQuery("GET_BULK_TERMINAL_INFO", params);
				while (rs.next()) {
					long terminalId = rs.getValue("terminalId", long.class);
					long customerBankId = rs.getValue("customerBankId", long.class);
					long currencyId = rs.getValue("feeCurrencyId", long.class);
					long businessUnitId = rs.getValue("businessUnitId", long.class);
					try {
						result = DataLayerMgr.executeCall("GET_OR_CREATE_DOC",
								new Object[] { customerBankId, currencyId, businessUnitId }, true);
						long eftId = ConvertUtils.getLongSafely(result[1], 0);
						if (eftId > 0) {
							result = DataLayerMgr.executeCall("ADJUSTMENT_INS",
									new Object[] { eftId, terminalId, reason, amount }, true);
							if (ConvertUtils.getLongSafely(result[1], 0) > 0)
								updatedCount++;
						}
					} catch (Exception e) {
						err.append("Error adding EFT adjustment for ").append(rs.getFormattedValue("eportSerialNum"))
								.append(": ").append(e.getMessage()).append("<br /><br />");
						log.error(new StringBuilder("Error adding EFT adjustment for ")
								.append(rs.getFormattedValue("eportSerialNum")).toString(), e);
					}
				}
				if (err.length() > 0) {
					request.setAttribute("message", "EFT adjustments added: " + updatedCount);
					request.setAttribute("error", err.toString());
				} else
					form.setRedirectUri(
							"/addEFTAdjustments.i?message=EFT+adjustments+added:+" + updatedCount);
			}
		} catch (Exception e) {
			throw new ServletException("Error adding EFT adjustments", e);
		}
	}
}