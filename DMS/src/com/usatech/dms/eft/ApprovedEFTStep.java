package com.usatech.dms.eft;

import java.sql.SQLException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class ApprovedEFTStep extends AbstractStep {

	private static final String DEFAULT_SORT_INDEX = "3, -7";

	private static final String SQL_START  = "SELECT A.EFT_ID, A.CURRENCY_ID, A.BATCH_REF_NBR, A.DESCRIPTION," + 
			"A.CUSTOMER_BANK_ID, A.CUSTOMER_NAME, A.BANK_ACCT_NBR, A.BANK_ROUTING_NBR," +
			"TO_CHAR(A.EFT_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00')" + 
			"|| (CASE WHEN A.EFT_AMOUNT IS NULL THEN '' ELSE COALESCE(' ' || C.CURRENCY_CODE, '') END) EFT_FMT_AMOUNT";

	private static final String SQL_BASE = " FROM CORP.VW_APPROVED_PAYMENTS A" +
			" LEFT JOIN corp.CURRENCY C ON A.CURRENCY_ID = C.CURRENCY_ID ";

	private static final String[] SORT_FIELDS = {
		"A.BATCH_REF_NBR", "UPPER(A.DESCRIPTION)", "UPPER(A.CUSTOMER_NAME)", "A.BANK_ACCT_NBR", "A.BANK_ROUTING_NBR", "A.EFT_AMOUNT", "A.EFT_ID"
	};
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);
			if (!StringHelper.isBlank(action))
				if (processAction(action, form, request, response))
					return;

			String queryBase = SQL_BASE;
			String paramTotalCount = PaginationUtil.getTotalField(null);
			String paramPageIndex = PaginationUtil.getIndexField(null);
			String paramPageSize = PaginationUtil.getSizeField(null);
			String paramSortIndex = PaginationUtil.getSortField(null);

			int totalCount = form.getInt(paramTotalCount, false, -1);
			if (totalCount == -1 || !StringHelper.isBlank(action)) {
				Results total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1) " + queryBase, null, null);
				if (total.next()) {
					totalCount = total.getValue(1, int.class);
				} else {
					totalCount = 0;
				}
				request.setAttribute(paramTotalCount, String.valueOf(totalCount));
			}

			int pageIndex = form.getInt(paramPageIndex, false, 1);
			int pageSize = form.getInt(paramPageSize, false, -1);
			if (pageSize < 0) {
				pageSize = 5000;
				request.setAttribute(paramPageSize, pageSize);
			}
			int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
			int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

			String sortIndex = form.getString(paramSortIndex, false);
			sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
			String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

			String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ queryBase + orderBy + " limit ?::numeric) pagination_temp ) sq_end where rnum  >= ?::numeric";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + queryBase
						+ orderBy + ") pagination_temp where ROWNUM <= ?) where rnum  >= ?";
			}

			Results results = DataLayerMgr.executeSQL("REPORT", query, new Object[] {maxRowToFetch,minRowToFetch}, null);
			request.setAttribute("resultlist", results);

		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
	
	private boolean processAction(String action, InputForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String[] ids = form.getStringArray("pid", false);
		if (ids == null || ids.length == 0) {
			request.setAttribute("error", "No EFTs selected!");
			return false;
		}
		
		BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
		if (user == null || user.getUserName() == null) {
			request.setAttribute("error", "Unknown user!");
			return false;
		}
		
		if ("Mark as Paid".equalsIgnoreCase(action)) {
			for (int i = 0; i < ids.length; i++) {
				Object[] params = new Object[] {ids[i], user.getEmailAddress()};
				DataLayerMgr.executeCall("MARK_EFT_AS_PAID", params, true);
			}
			request.setAttribute("message", String.format("Marked EFTs %s as paid.", StringUtils.join(ids, ",")));
			return false;
		} else if ("Generate ACH Debit".equalsIgnoreCase(action)) {
			Map<String, Object> params = new HashMap<>();
			params.put("user_name", user.getUserName());
			params.put("doc_ids", ids);
			try {
				DataLayerMgr.executeCall("GENERATE_ACH_DEBIT", params, true);
			} catch (SQLException e) { 
				if (e.getErrorCode() == 20731) { 
					request.setAttribute("error", "You can not include credit EFTs in an ACH Debit.  Choose only EFTs with a negative amount.");
					return false;
				} else if (e.getErrorCode() == 20732) {
					request.setAttribute("error", "You can not include non-US EFTs in an ACH Debit.  Choose only EFTs with US currency.");
					return false;
				} else if (e.getErrorCode() == 20734) {
					request.setAttribute("error", "You can not include paper EFTs in an ACH Debit.  Choose only EFTs that are electronic.");
					return false;
				} else {
					throw e;
				}
			}
			String achId = ConvertUtils.getStringSafely(params.get("ach_id"));
			if (!StringUtils.isBlank(achId)) {
				form.setRedirectUri("manageACH.i?achId="+achId);
			}
			return true;
		} else {
			request.setAttribute("error", String.format("Unknown action (%s)!", action));
			return false;
		}
	}
}
