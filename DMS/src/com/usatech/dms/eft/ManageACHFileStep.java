package com.usatech.dms.eft;

import static simple.text.MessageFormat.format;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.model.DmsPrivilege;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.db.DataLayerMgr;
import simple.servlet.*;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class ManageACHFileStep extends AbstractStep {
	
	private static final simple.io.Log log = simple.io.Log.getLog();

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
		if (!user.hasPrivilege(DmsPrivilege.DMS_ACH_ADMINS.getValue())) {
			form.setAttribute("error", "Permission Denied");
			return;
		}
		
		String action = form.getString("action", false);
		long achFileId = form.getLong("achFileId", true, -1);
		
		try {
			WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, new DMSRecordRequestFilter(), new CallInputs(), System.currentTimeMillis(), action == null ? "View" : action, "ACH File", Long.toString(achFileId), log);
		} catch (Exception e) {
			log.error("Failed to publishAppRequestRecord for action=%s achFileId=%s", action, achFileId, e);
		}

		if (StringHelper.isBlank(action))
			return;
		
		long achId = form.getLong("achId", true, -1);
		long fileId = form.getLong("fileId", true, -1);
		String fileName = form.getString("fileName", true);
		
		if ("Download File".equalsIgnoreCase(action)) {
			try {
				Helper.streamFileToClient(response, fileName, fileId);
			} catch (Exception e) {
				throw new ServletException("Download failed!", e);
			}
		} else if ("Mark As Uploaded".equalsIgnoreCase(action)) {
			try {
				Object[] params = new Object[] {user.getUserName(), achFileId};
				DataLayerMgr.executeCall("MARK_ACH_UPLOADED", params, true);
				response.sendRedirect(format("/manageACH.i?achId={0}&message={1}", achId, StringUtils.prepareURLPart("Marked ACH Uploaded")));
			} catch (Exception e) {
				throw new ServletException("Error marking as paid!", e);
			}
		}
	}
}
