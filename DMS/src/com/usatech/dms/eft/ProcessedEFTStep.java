package com.usatech.dms.eft;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.*;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
import simple.util.LinkedHashMap;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class ProcessedEFTStep extends AbstractStep {

	private static final String DEFAULT_SORT_INDEX = "3, -7";
	
	private static final String SQL_START  = "SELECT D.DOC_ID, D.REF_NBR, D.DESCRIPTION,"+ 
		" CUS.CUSTOMER_NAME, CB.BANK_ACCT_NBR, CB.BANK_ROUTING_NBR,"+
		" TO_CHAR(D.TOTAL_AMOUNT, 'FM' || COALESCE(C.CURRENCY_SYMBOL, '$') || '9,999,999,999,999,990.00') TOTAL_AMOUNT_FMT,"+ 
		" C.CURRENCY_CODE, D.TOTAL_AMOUNT, CASE WHEN D.AUTO_PROCESS_START_TS IS NULL THEN 'N' ELSE 'Y' END AUTO_PROCESSED, TO_CHAR(D.SENT_DATE, 'MM/DD/YYYY HH24:MI:SS') SENT_DATE_FORMATTED," +
		" CS.STATUS_NAME CUSTOMER_STATUS, CA.NAME ADDRESS_NAME, CA.ADDRESS1, CA.ADDRESS2, CA.CITY, CA.STATE, CA.ZIP, UL.FIRST_NAME, UL.LAST_NAME, UL.EMAIL, CB.CUSTOMER_ID ";
		
	private static final String SQL_BASE = " FROM CORP.DOC D"+
		" JOIN CORP.CUSTOMER_BANK CB ON D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID"+
		" JOIN CORP.CUSTOMER CUS ON CB.CUSTOMER_ID = CUS.CUSTOMER_ID"+
		" JOIN CORP.CUSTOMER_STATUS CS ON CUS.STATUS = CS.STATUS" +
		" LEFT JOIN CORP.CURRENCY C ON D.CURRENCY_ID = C.CURRENCY_ID " +
		" LEFT OUTER JOIN CORP.CUSTOMER_ADDR CA ON CUS.CUSTOMER_ID = CA.CUSTOMER_ID AND CA.STATUS = 'A' AND CA.ADDR_TYPE = 2" +
		" LEFT OUTER JOIN REPORT.USER_LOGIN UL ON CUS.USER_ID = UL.USER_ID ";
		
	private static final String[] SORT_FIELDS = {
		"D.REF_NBR", "UPPER(D.DESCRIPTION)", "UPPER(CUS.CUSTOMER_NAME)", "CB.BANK_ACCT_NBR", "CB.BANK_ROUTING_NBR", "D.TOTAL_AMOUNT", "D.DOC_ID", "C.CURRENCY_CODE", "AUTO_PROCESSED", "D.SENT_DATE",
		"CUS.STATUS", "UPPER(CA.NAME)", "UPPER(CA.ADDRESS1)", "UPPER(CA.ADDRESS2)", "UPPER(CA.CITY)", "UPPER(CA.STATE)", "UPPER(CA.ZIP)", "UPPER(UL.FIRST_NAME)", 
		"UPPER(UL.LAST_NAME)", "UPPER(UL.EMAIL)", "CB.CUSTOMER_ID"
	};
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);
			if ("Generate ACH Debit".equalsIgnoreCase(action)) {
				BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
				if (user == null || user.getUserName() == null) {
					request.setAttribute("error", "Unknown user!");
				} else {
					String[] ids = form.getStringArray("pid", false);
					if (ids == null || ids.length == 0) {
						request.setAttribute("error", "No EFTs selected!");
					} else {
						Map<String, Object> params = new HashMap<>();
						params.put("user_name", user.getUserName());
						params.put("doc_ids", ids);
						DataLayerMgr.executeCall("GENERATE_PROCESSED_ACH_DEBIT", params, true);
						String achId = ConvertUtils.getStringSafely(params.get("ach_id"));
						if (!StringUtils.isBlank(achId)) {
							form.setRedirectUri("manageACH.i?achId=" + achId);
							return;
						}
					}
				}
			}

			if ("List EFTs".equalsIgnoreCase(action) || "Generate ACH Debit".equalsIgnoreCase(action)) {
				Map<Integer, Object> params = new LinkedHashMap<Integer, Object>();
				int paramCnt = 0;

				String eftFromDate = form.getString("eft_from_date", true);
				String eftFromTime = form.getString("eft_from_time", true);
				String eftToDate = form.getString("eft_to_date", true);
				String eftToTime = form.getString("eft_to_time", true);

				StringBuilder queryBase = new StringBuilder(SQL_BASE);

				queryBase.append(!DialectResolver.isOracle() 
						? " WHERE D.SENT_DATE BETWEEN TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')"
				        : " WHERE D.SENT_DATE BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')");
				params.put(paramCnt++, eftFromDate);
				params.put(paramCnt++, eftFromTime);
				params.put(paramCnt++, eftToDate);
				params.put(paramCnt++, eftToTime);

				String currencyCode = form.getStringSafely("currency_code", "");
				if (!StringHelper.isBlank(currencyCode)) {
					queryBase.append(" AND C.CURRENCY_CODE = ?");
					params.put(paramCnt++, currencyCode);
				}

				String autoProcessed = form.getStringSafely("auto_processed", "");
				if ("Y".equalsIgnoreCase(autoProcessed))
					queryBase.append(" AND D.AUTO_PROCESS_START_TS IS NOT NULL");
				else if ("N".equalsIgnoreCase(autoProcessed))
					queryBase.append(" AND D.AUTO_PROCESS_START_TS IS NULL");

				String dataFormat = form.getStringSafely("data_format", "HTML");
				if ("CSV".equalsIgnoreCase(dataFormat)) {
					response.setContentType("application/force-download");
					response.setHeader("Content-Transfer-Encoding", "binary");
					response.setHeader("Content-Disposition", new StringBuilder("attachment;filename=\"").append(StringHelper.isBlank(currencyCode) ? "All" : currencyCode).append(" Processed EFTs ").append(eftFromDate.replaceAll("/", "-")).append(" ").append(eftFromTime.replaceAll(":", "-")).append(" to ").append(eftToDate.replaceAll("/", "-")).append(" ").append(eftToTime.replaceAll(":", "-")).append(".csv.txt\"").toString());
					PrintWriter out = response.getWriter();
					StringBuilder sb = new StringBuilder();
					String[] headers = {"EFT ID","Customer","Batch #","Description","Bank Account #","Bank Routing #","EFT Amount","Currency","Auto Processed","Processed Date","Customer Status","Address Name","Address1","Address2","City","State","Postal","First Name","Last Name","Email","Customer ID"};
					out.print(StringUtils.join(headers, ",", "\""));
					out.flush();
					String query = new StringBuilder(SQL_START).append(queryBase).append(" ORDER BY UPPER(CUS.CUSTOMER_NAME), D.DOC_ID DESC").toString();
					Results rs = DataLayerMgr.executeSQL("REPORT", query, params.values().toArray(), null);
					while (rs.next()) {
						sb.setLength(0);
						sb.append("\"");
						sb.append(rs.getFormattedValue("DOC_ID"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("CUSTOMER_NAME"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("REF_NBR"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("DESCRIPTION"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("BANK_ACCT_NBR"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("BANK_ROUTING_NBR"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("TOTAL_AMOUNT"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("CURRENCY_CODE"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("AUTO_PROCESSED"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("SENT_DATE_FORMATTED"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("CUSTOMER_STATUS"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("ADDRESS_NAME"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("ADDRESS1"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("ADDRESS2"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("CITY"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("STATE"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("ZIP"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("FIRST_NAME"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("LAST_NAME"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("EMAIL"));
						sb.append("\",");
						sb.append("\"");
						sb.append(rs.getFormattedValue("CUSTOMER_ID"));
						sb.append("\"");
						out.print("\r\n");
						out.print(sb.toString().replaceAll("\r|\n", ""));
						out.flush();
					}
					out.close();
					return;
				} else if ("HTML".equalsIgnoreCase(dataFormat)) {
					String paramTotalCount = PaginationUtil.getTotalField(null);
					String paramPageIndex = PaginationUtil.getIndexField(null);
					String paramPageSize = PaginationUtil.getSizeField(null);
					String paramSortIndex = PaginationUtil.getSortField(null);

					int totalCount = form.getInt(paramTotalCount, false, -1);
					if (totalCount == -1 || !StringHelper.isBlank(action)) {
						Results total = DataLayerMgr.executeSQL("REPORT", new StringBuilder("SELECT COUNT(1) ").append(queryBase).toString(), params.values().toArray(), null);
						if (total.next()) {
							totalCount = total.getValue(1, int.class);
						} else {
							totalCount = 0;
						}
						request.setAttribute(paramTotalCount, String.valueOf(totalCount));
					}

					int pageIndex = form.getInt(paramPageIndex, false, 1);
					int pageSize = form.getInt(paramPageSize, false, -1);
					if (pageSize < 0) {
						pageSize = 5000;
						request.setAttribute(paramPageSize, pageSize);
					}
					int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
					int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

					String sortIndex = form.getString(paramSortIndex, false);
					sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
					String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

					String query;
					if (!DialectResolver.isOracle()) {
						query = new StringBuilder("select * from (").append(" select pagination_temp.*, row_number() over() as rnum from (").append(SQL_START).append(queryBase).append(orderBy).append(" limit ?::bigint) pagination_temp) sq_end where rnum  >= ?::bigint").toString();
					} else {
						query = new StringBuilder("select * from (").append(" select pagination_temp.*, ROWNUM rnum from (").append(SQL_START).append(queryBase).append(orderBy).append(") pagination_temp where ROWNUM <= ?) where rnum  >= ?").toString();
					}

					params.put(paramCnt++, String.valueOf(maxRowToFetch));
					params.put(paramCnt++, String.valueOf(minRowToFetch));
					Results results = DataLayerMgr.executeSQL("REPORT", query, params.values().toArray(), null);
					request.setAttribute("resultlist", results);
				}
			}
		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
}
