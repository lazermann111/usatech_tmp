package com.usatech.dms.eft;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.db.DataLayerMgr;
import simple.servlet.*;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.steps.AbstractStep;

public class ManageACHStep extends AbstractStep {
	
	private static final simple.io.Log log = simple.io.Log.getLog();

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String action = form.getString("action", false);
		if (StringHelper.isBlank(action))
			return;
		
		BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
		if (user == null || user.getUserName() == null) {
			request.setAttribute("error", "Unknown user!");
			return;
		}
		
		long achId = -1;

		try {
			achId = RequestUtils.getAttribute(request, "achId", Long.class, true);
		} catch (Exception e) {
			request.setAttribute("error", "No ACH ID!");
			return;
		}

		try {
			WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, new DMSRecordRequestFilter(), new CallInputs(), System.currentTimeMillis(), action, "ACH", Long.toString(achId), log);
		} catch (Exception e) {
			log.error("Failed to publishAppRequestRecord for action=%s achId=%s", action, achId, e);
		}
		
		if (action.equals("Regenerate ACH File")) {
			try {
				Object[] params = new Object[] {user.getUserName(), achId};
				DataLayerMgr.executeCall("REGENERATE_ACH_DEBIT", params, true);
				request.setAttribute("message", "ACH file regenerated successfully!");
			} catch (Exception e) {
				throw new ServletException("Error regenerating ACH file!", e);
			}
		} else if (action.equals("Cancel")) {
			long achFileId = -1;

			try {
				achFileId = RequestUtils.getAttribute(request, "achFileId", Long.class, true);
			} catch (Exception e) {
				request.setAttribute("error", "No ACH FILE ID!");
				return;
			}
			
			try {
				Object[] params = new Object[] {user.getUserName(), achFileId};
				DataLayerMgr.executeCall("CANCEL_ACH", params, true);
				request.setAttribute("message", "ACH cancelled!");
			} catch (Exception e) {
				throw new ServletException("Error cancelling ACH file!", e);
			}
		}
	}
}
