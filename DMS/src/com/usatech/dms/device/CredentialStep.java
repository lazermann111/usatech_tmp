/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.security.SecureHash;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class CredentialStep extends AbstractStep
{	
	public static final String CSRNG_ALGORITHM = "SHA1PRNG";
	public static final String CSRNG_PROVIDER = "SUN";
	
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	String username = form.getString("username", false);
    	try {
	    	String action = form.getString("action", false);
	    	if (!StringUtils.isBlank(action)) {
	    		String message = "";
	    		
	    		long credentialId = form.getLong("credential_id", false, 0);
	    		String search = form.getStringSafely("credential_search", "");
	    		Map<String, Object> params = new HashMap<String, Object>();
	    		params.put("credentialId", credentialId);
	    		params.put("credentialName", form.getString("credential_name", false));
	    		params.put("credentialDesc", form.getString("credential_desc", false));
	    		params.put("username", username);
	    		
	    		if ("Create".equalsIgnoreCase(action) || "Change Passcode".equalsIgnoreCase(action)) {
		    		String password = getRandomPasscode();
		    		byte[] salt = SecureHash.getSalt();
		    		byte[] hash = SecureHash.getHash(password, salt);
		    		params.put("passwordHash", hash);
		    		params.put("passwordSalt", salt);
		    		request.getSession().setAttribute("credentialPwd" + username, password);
	    		}
	    		
	    		BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
	    		params.put("usernamePasswordCreatedBy", user.getUserName());
	    		
		    	if ("Save".equalsIgnoreCase(action)) {		    		
		    		DataLayerMgr.executeUpdate("UPDATE_CREDENTIAL", params, true);
		    		message = "Credential+updated";
		    	} else if ("Create".equalsIgnoreCase(action)) {
		    		DataLayerMgr.executeUpdate("INSERT_CREDENTIAL", params, true);
		            credentialId = ConvertUtils.getLong(params.get("credentialId"));		    		
		    		message = "Credential+created";
		    	} else if ("Change Passcode".equalsIgnoreCase(action)) {
		    		DataLayerMgr.executeUpdate("UPDATE_CREDENTIAL_PASSWORD", params, true);
		    		message = "Passcode+changed";
		    	} else if ("Delete Previous Values".equalsIgnoreCase(action)) {
		    		DataLayerMgr.executeUpdate("REMOVE_PREVIOUS_CREDENTIAL", params, true);
		    		message = "Previous+values+deleted";
		    	}
		    	form.setRedirectUri(new StringBuilder("/credential.i?credential_id=").append(credentialId).append("&msg=").append(message).append("&search=").append(search).toString());
	    	}
    	} catch (SQLIntegrityConstraintViolationException e) {
    		form.set("statusMessage", "Credential with this Credential Name or Username already exists");
		    form.set("statusType", "error");
		    request.getSession().removeAttribute("credentialPwd" + username);
    	} catch (Exception e) {
    		throw new ServletException(e);
    	}
    }
    
	public static String getRandomString(String chars, int stringLength) throws NoSuchAlgorithmException, NoSuchProviderException {
		SecureRandom random = SecureRandom.getInstance(CSRNG_ALGORITHM, CSRNG_PROVIDER);
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<stringLength; i++) {
			int index = (int)(random.nextDouble() * chars.length());
			sb.append(chars.substring(index, index + 1));
		}
		return sb.toString();
	}

	public static String getRandomPasscode() throws NoSuchAlgorithmException, NoSuchProviderException {
		return getRandomString("0123456789", 8);
	}	
}