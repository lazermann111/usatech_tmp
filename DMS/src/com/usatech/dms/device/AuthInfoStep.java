/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class AuthInfoStep extends AbstractStep
{
    // by default, sort by "merchant_cd" descending
    private static final String DEFAULT_SORT_INDEX = "3";
    private static final String SQL_START = "SELECT merchant.merchant_id," +
            "merchant.merchant_cd," +
            "merchant.merchant_name, merchant.mcc, merchant.merchant_group_cd ";
    
    private static final String SQL_BASE = "FROM pss.merchant WHERE authority_id = CAST (? AS numeric) AND status_cd = 'A' ";
    
    private static final String[] SORT_FIELDS = {"merchant.merchant_id", // merchant id
        "merchant.merchant_cd", // merchant cd
        "merchant.merchant_name", // merchant name
        "merchant.mcc",
        "merchant.merchant_group_cd"
        };

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String authority_id = form.getString(DevicesConstants.PARAM_AUTHORITY_ID, false);
        if (authority_id == null || authority_id.length() == 0)
        {
            request.setAttribute("missingParam", true);
            return;
        }
        //String sql = SQL_BASE + authority_id;
        String sql = SQL_BASE;
        String paramTotalCount = PaginationUtil.getTotalField(null);
        String paramPageIndex = PaginationUtil.getIndexField(null);
        String paramPageSize = PaginationUtil.getSizeField(null);
        String paramSortIndex = PaginationUtil.getSortField(null);

        try
        {
            // pagination parameters
            int totalCount = form.getInt(paramTotalCount, false, -1);
            if (totalCount == -1)
            {
                // if the total count is not retrieved yet, get it now
                Results total = DataLayerMgr.executeSQL("OPER", "SELECT COUNT(1) " + sql, new Object[]{authority_id}, null);
                if (total.next())
                {
                    totalCount = total.getValue(1, int.class);
                }
                else
                {
                    totalCount = 0;
                }
                request.setAttribute(paramTotalCount, String.valueOf(totalCount));
            }

            int pageIndex = form.getInt(paramPageIndex, false, 1);
            int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
            int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
            int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

            String sortIndex = form.getString(paramSortIndex, false);
            sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
            String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
            StringBuilder query = new StringBuilder();

			if (DialectResolver.isOracle()) {
				query.append("select * from (select pagination_temp.*, ROWNUM rnum from (").append(SQL_START)
						.append(sql).append(orderBy).append(") pagination_temp where ROWNUM <= ?) where rnum  >= ? ");

			} else {
				query.append("select * from (select pagination_temp.*, row_number()over() rnum from (")
						.append(SQL_START).append(sql).append(orderBy)
						.append(" ) pagination_temp limit CAST (? AS numeric)) as gen_alias where rnum  >= CAST (? AS numeric) ");
			}
			       
            Results results = DataLayerMgr.executeSQL("OPER", query.toString(), new Object[]{authority_id, maxRowToFetch, minRowToFetch}, null);
            request.setAttribute("merchantinfo", results);
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }

}
