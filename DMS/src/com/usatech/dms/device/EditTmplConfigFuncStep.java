package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.device.DevicesConstants;

public class EditTmplConfigFuncStep extends AbstractStep
{
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String action = form.getString(DevicesConstants.PARAM_ACTION, false);
    	long templateId = form.getLong("template_id", false, -1);
    	if ("Delete Template".equalsIgnoreCase(action)) {
        	try {
        		DataLayerMgr.executeUpdate("DELETE_CONFIG_TEMPLATE", new Object[] {templateId}, true);
        		form.setRedirectUri("/templateMenu.i?msg=Configuration+template+deleted");
        	} catch (Exception e) {
        		throw new ServletException(e);
        	}
        }
    }
}
