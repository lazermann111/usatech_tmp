/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.ServiceException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.util.StringHelper;

/**
 * Step to process the "edit configuration" (G4, G5, MEI)
 */
public class EditTmplConfigStep extends AbstractStep {

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData;
		String config_info = form.getString("template_info", false);
		Map<String, String> errorMap = new HashMap<String, String>();

		if ((StringHelper.isBlank(config_info))) {
			errorMap.put("template_info", "Template Name is Required!");
			request.setAttribute("errorMap", errorMap);
			dispatcher.dispatch("/errormap", true);
			return;
		}
		String[] config_info_data = config_info.split("_", 5);
		int deviceTypeId = Integer.parseInt(config_info_data[0]);
		int templateTypeId = Integer.parseInt(config_info_data[1]);
		long templateId = Long.parseLong(config_info_data[2]);
		String templateType = config_info_data[3];
		String templateName = config_info_data[4];

		request.setAttribute("templateCategory", deviceTypeId == DeviceType.EDGE.getValue() ? "Edge" : "Map");

		if (deviceTypeId < 0) {
			request.setAttribute("errorMessage", "<br/><br/><b><font color='red'>Undefined Device Type!</font></b><br/><br/>");
			return;
		}

		form.set("device_type_id", deviceTypeId);
		form.set("template_type", templateType);
		form.set("template_id", templateId);
		form.set("template_name", templateName);
		form.set("template_type_id", templateTypeId);

		int propertyListVersion = ConfigFileActions.getPropertyListVersionForConfigTemplateCustom(deviceTypeId, templateName);
		try {
			defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceTypeId, propertyListVersion);
		} catch (ServiceException e) {
			throw new ServletException(e);
		}
		request.setAttribute("defaultSettingData", defaultSettingData);

		LinkedHashMap<String, String> targetSettingData = ConfigFileActions.getConfigTemplateSettingData(defaultSettingData, templateId, deviceTypeId, true, null);
		form.set("targetSettingData", targetSettingData);
	}
}
