package com.usatech.dms.device.modem;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.app.task.BridgeTask;
import com.usatech.dms.util.DMSUtils;
import com.usatech.layers.common.constants.CommonAttrEnum;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.io.Log;
import simple.modem.USATModemService;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.dto.ModemApiCommand;
import simple.service.modem.service.DeviceModemStatusDao;
import simple.service.modem.service.DeviceModemStatusDaoImpl;
import simple.service.modem.service.dto.DeviceModemState;
import simple.service.modem.service.dto.USATDevice;
import simple.service.modem.service.dto.ModemState;
import simple.util.ClassSerializer;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * DMS modem service to manage modem status and read it state.
 * This service asynchronously post modem task to external modem API provider (EsEye, Verizon, etc)
 * through app layers:
 * DMS -> MST_2_MSR_BRIDGE -> TRANSPORTER -> Carrier ext. API -> USALIVE -> REPORT GENERATOR -> Ora USAPDB
 *
 * @author dlozenko
 */
public class AsyncDmsModemService implements USATModemService {
    static final Log log = Log.getLog();
    DeviceModemStatusDao deviceModemStatusDao = new DeviceModemStatusDaoImpl();
    private final ClassSerializer<USATDevice[]> paramsSerializer = new ClassSerializer<>();

    @Override
    public void activate(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException {
        USATDevice[] devicesToUpdate = saveRequestToDb(devices, ModemState.ACTIVE);
        sendModemCommandToModemProcessor(ModemApiCommand.ACTIVATE, devicesToUpdate);
    }

    @Override
    public void deactivate(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException {
        USATDevice[] devicesToUpdate = saveRequestToDb(devices, ModemState.DEACTIVATED);
        sendModemCommandToModemProcessor(ModemApiCommand.DEACTIVATE, devicesToUpdate);
    }

    @Override
    public void suspend(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException {
        USATDevice[] devicesToUpdate = saveRequestToDb(devices, ModemState.SUSPENDED);
        sendModemCommandToModemProcessor(ModemApiCommand.SUSPEND, devicesToUpdate);
    }

    @Override
    public void resume(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException {
        USATDevice[] devicesToUpdate = saveRequestToDb(devices, ModemState.ACTIVE);
        sendModemCommandToModemProcessor(ModemApiCommand.RESUME, devicesToUpdate);
    }

    @Override
    public void refreshActivationStatus(@Nonnull USATDevice[] devices) throws DataLayerException, SQLException, ConvertException, ServletException {
        USATDevice[] devicesToUpdate = saveRequestToDb(devices, ModemState.UNKNOWN);
        sendModemCommandToModemProcessor(ModemApiCommand.GET_STATE, devicesToUpdate);
    }

    void sendModemCommandToModemProcessor(ModemApiCommand command, USATDevice[] devices) throws ServletException {
        if (devices == null || devices.length == 0) {
            return;
        }
        MessageChain mc = new MessageChainV11();
        MessageChainStep step = mc.addStep("usat.bridge.to.msr");
        step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, "usat.device.modem.status");
        step.setAttribute(CommonAttrEnum.ATTR_ITEM_ID, command);
        step.setAttribute(CommonAttrEnum.ATTR_OBJECT, paramsSerializer.toBytes(devices));
        publishMessage(mc);
    }

    USATDevice[] saveRequestToDb(USATDevice[] devices, ModemState requestedState) throws DataLayerException, SQLException, ConvertException {
        List<USATDevice> rv = new ArrayList<>();
        for (USATDevice dev : devices) {
            DeviceModemState existedSimActivation = getSimActivationStatus(dev);
            if (hasUnfinishedSimActivation(existedSimActivation)) {
                // Don't allow to do anything with devices which still in activation process
            		log.warn("The device with modem " + dev.getSerialCode() + " is still in activation process.");
                continue;
            }
            boolean newSimActivation = isFirstTimeActivation(dev);
            if (newSimActivation && existedSimActivation != null && existedSimActivation.getErrorCode() == null) {
                // Don't allow to activate SIM twice
          			log.warn("The device with modem " + dev.getSerialCode() + " is activated already.");
                continue;
            }
            DeviceModemState status = newSimActivation ? existedSimActivation : deviceModemStatusDao.getDeviceModemStatus(dev.getSerialCode());
            boolean hasStatusRequest = status != null;
            if (hasStatusRequest) {
                if (status.getRequestId() != null &&
                    status.getStatusRequestedTs() != null &&
                    status.getStatusRequestedTs().after(obsoleteRequestTime(status))) {
                    // This device still have unfinished request, so skip it
                    continue;
                }
            } else {
                status = new DeviceModemState();
                status.setStatusUpdatedTs(new Date());
								status.setLastModemStatusCd((dev.getCurrentStatus() == null || dev.getCurrentStatus().trim().isEmpty())
										? requestedState.getStatus() : dev.getCurrentStatus());
            }
            status.setModemSerialCode(dev.getSerialCode());
            status.setRequestId(dev.getSerialCode());
            status.setRequestedStatusCd(requestedState.getStatus());
            status.setStatusRequestedTs(new Date());
            status.setErrorCode(null);
            status.setErrorMessage(null);
            if (status.getProvider() == null) {
                status.setProvider(ModemApiProvider.fromString(dev.getProviderName()));
            }
            if (hasStatusRequest) {
                deviceModemStatusDao.updateDeviceModemStatus(status);
            } else {
                deviceModemStatusDao.insertDeviceModemStatus(status);
            }
            rv.add(dev);
        }
        return rv.toArray(new USATDevice[rv.size()]);
    }

    private boolean isFirstTimeActivation(@Nonnull USATDevice simInfo) {
        return ModemState.PENDING_ACTIVATION.getStatus().equals(simInfo.getCurrentStatus()) &&
               ModemState.ACTIVE.getStatus().equals(simInfo.getRequestedStatus()) &&
               simInfo.getDeviceId() != null;
    }

    private void publishMessage(MessageChain mc) throws ServletException {
        try {
            MessageChainService.publish(mc, DMSUtils.getPublisher());
        } catch (ServiceException serviceException) {
            throw new ServletException("Failed to send message", serviceException);
        }
    }

    Date obsoleteRequestTime(DeviceModemState status) {
        String toStatusCd = status.getRequestedStatusCd();
        int obsoleteHours;
        if ("D".equals(toStatusCd)) {
            obsoleteHours = 100;
        } else {
            obsoleteHours = 3;
        }
        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.HOUR_OF_DAY, -obsoleteHours);
        return ca.getTime();
    }

    @Nullable
    DeviceModemState getSimActivationStatus(@Nonnull USATDevice dev) throws DataLayerException, SQLException, ConvertException {
    		log.info("Trying to get status for the device: " + dev);
        return deviceModemStatusDao.getDeviceModemStatus(dev.getSerialCode());
    }

    boolean hasUnfinishedSimActivation(@Nullable DeviceModemState modemState) throws DataLayerException, SQLException, ConvertException {
        return  modemState != null &&
                modemState.getRequestId() != null &&
                modemState.getStatusRequestedTs() != null &&
                modemState.getStatusRequestedTs().after(obsoleteRequestTime(modemState));
    }
}
