package com.usatech.dms.device.modem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.istack.internal.NotNull;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.modem.USATModemService;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.service.DeviceModemStatusDao;
import simple.service.modem.service.DeviceModemStatusDaoImpl;
import simple.service.modem.service.dto.ModemState;
import simple.service.modem.service.dto.USATDevice;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.exception.ValidationException;
import com.usatech.layers.common.util.StringHelper;

public class MassSimActivationStep extends AbstractStep {
    private static Pattern ICCID_SEPARATOR = Pattern.compile("[\\s,]+");
    private static Pattern NUMBER = Pattern.compile("^[\\d]+$");
    private static Pattern HEX_NUMBER = Pattern.compile("^[\\dabcdefABCDEF]+$");
    private static final simple.io.Log LOG = simple.io.Log.getLog();
    private DeviceModemStatusDao deviceModemStatusDao = new DeviceModemStatusDaoImpl();

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    		if (form.get("action") == null) {
    			return;
    		}
        try {
		    		USATDevice[] devices = null;
		        String iccIdsStr = extractIccIdParam(form);
		        if (iccIdsStr != null) {
		        	devices = getEsEyeDevices(request, iccIdsStr);
		        } else if (form.get("file_data_verizon") != null) {
		        	devices = getVerizonDevicesToModemActivate(form, request);
		        }
		        if (devices != null && devices.length > 0) {
	            enqueueSimActivation(devices);
	            request.setAttribute("error", "SIMs activation scheduled. Activation process usually take 3 hours. You can check activation state <a href='simActivationStatus.i'>here</a>.");
		        }
        } catch (Exception e) {
            throw new ServletException("Error in MassSimActivationStep", e);
        }
    }

    private USATDevice[] getEsEyeDevices(HttpServletRequest request, String iccIdsStr) throws SQLException, DataLayerException, ConvertException {
      String[] iccIds = splitAndValidateEsEyeIccIds(request, iccIdsStr);
      if (iccIds == null) {
      	return null;
      }
    	USATDevice[] devices = populateEsEyeDevicesToModemActivate(iccIds);
			return devices;
		}

		private USATDevice[] getVerizonDevicesToModemActivate(InputForm form, HttpServletRequest request) throws ServletException, SQLException, DataLayerException, ConvertException {
      int simType = form.getInt("sim_type", false, 0);
    	USATDevice[] devices = null;
  		List<String[]> ids = getVerizonIDsFromCSV(form.get("file_data_verizon"));
      switch (simType) {
      	case 1:
        	// Verizon CDMA
      		if (ids != null && ids.size() > 0) {
      			devices = populateVerizonCDMADevicesToModemActivate(request, ids);
      		}
      		break;
      	case 2:
        	// Verizon LTE
      		if (ids != null && ids.size() > 0) {
      			devices = populateVerizonLTEDevicesToModemActivate(request, ids);
      		}
      		break;
      	default:
      		request.setAttribute("error", "Unknown SIM type.");
      		return null;
      }
      
			return devices;
		}

		private List<String[]> getVerizonIDsFromCSV(Object object) throws ConvertException {
			List<String[]> ret = new ArrayList<>();
			InputStream stream = ConvertUtils.convert(InputStream.class, object);
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new InputStreamReader(stream));
				String csvLine;
				while ((csvLine = reader.readLine()) != null) {
					ret.add(csvLine.split(","));
				}
			} catch (Exception e) {
				LOG.error("Could not convert <" + object + "> to InputStream", e);
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						LOG.error("Exception while closing reader", e);
					}
				}
			}
			return ret;
		}

		String[] splitAndValidateEsEyeIccIds(HttpServletRequest request, String iccIdsStr) {
        if (iccIdsStr == null) {
            return null;
        }
        if (StringHelper.isBlank(iccIdsStr)) {
            request.setAttribute("error", "No ICCIDs entered");
            return null;
        }
        LOG.info("Mass SIM activation is starting, SIM ICCIDs: " + iccIdsStr);
        String[] iccIds = ICCID_SEPARATOR.split(iccIdsStr);
        for (String iccid : iccIds) {
            if (iccid == null || !iccid.startsWith("89") || iccid.length() < 17 || iccid.length() > 19 || !NUMBER.matcher(iccid).matches()) {
                request.setAttribute("error", "Invalid ICCID: " + iccid);
                return null;
            }
        }
        return iccIds;
    }

    String extractIccIdParam(InputForm form) {
        return form.getStringSafely("icc_ids", null);
    }

		private USATDevice[] populateVerizonCDMADevicesToModemActivate(HttpServletRequest request, List<String[]> ids) throws SQLException, DataLayerException, ConvertException {
      USATDevice[] rv = new USATDevice[ids.size()];
			try {
	      int i = 0;
	      for (String[] device : ids) {
	      		validateDeviceIDsRow(device, 2);
	      		String meid = device[0];
	      		String zip = device[1];
            validateMeid(meid);
            validateZip(zip);
            
            rv[i] = new USATDevice();
         		rv[i].setMeid(meid);
	          rv[i].setSerialCode(meid);
	          rv[i].setZipCode(zip);
	          rv[i].setCurrentStatus(ModemState.PENDING_ACTIVATION.getStatus());
	          rv[i].setRequestedStatus(ModemState.ACTIVE.getStatus());
	          rv[i].setProviderName(ModemApiProvider.VERIZON.name());
	          i++;
	      }
			} catch (ValidationException e) {
				request.setAttribute("error", e.getDefaultMessage());
				return null;
			}
      return rv;
		}

		private USATDevice[] populateVerizonLTEDevicesToModemActivate(HttpServletRequest request, List<String[]> ids) {
      USATDevice[] rv = new USATDevice[ids.size()];
			try {
	      int i = 0;
	      for (String[] device : ids) {
	      		validateDeviceIDsRow(device, 3);
	      		String iccid = device[0];
	      		String imei = device[1];
	      		String zip = device[2];
            validateICCID(iccid);
            validateIMEI(iccid);
            validateZip(zip);
            
            rv[i] = new USATDevice();
         		rv[i].setIccid(iccid);
         		rv[i].setWithSim(true);
         		rv[i].setImei(imei);
	          rv[i].setSerialCode(iccid);
	          rv[i].setZipCode(zip);
	          rv[i].setCurrentStatus(ModemState.PENDING_ACTIVATION.getStatus());
	          rv[i].setRequestedStatus(ModemState.ACTIVE.getStatus());
	          rv[i].setProviderName(ModemApiProvider.VERIZON.name());
	          i++;
	      }
			} catch (ValidationException e) {
				request.setAttribute("error", e.getDefaultMessage());
				return null;
			}
      return rv;
		}

		private void validateIMEI(String imei) throws ValidationException {
			if (imei == null || !NUMBER.matcher(imei).matches() || imei.length() < 16) {
				throw new ValidationException("dms-invalid-imei", "Invalid IMEI: " + imei);
			}
		}

		private void validateICCID(String iccid) throws ValidationException {
			if (iccid == null || !NUMBER.matcher(iccid).matches() || iccid.length() < 20) {
				throw new ValidationException("dms-invalid-iccid", "Invalid ICCID: " + iccid);
			}
		}

		private void validateDeviceIDsRow(String[] device, int length) throws ValidationException {
			if (device == null || device.length < length) {
				throw new ValidationException("dms-invalid-csv-for-device", "Invalid CSV line: " + device);
			}
		}

		private void validateZip(String zip) throws ValidationException {
			if (zip == null || !NUMBER.matcher(zip).matches()) {
				throw new ValidationException("dms-invalid-zip", "Invalid ZIP: " + zip);
			}
		}

		private void validateMeid(String meid) throws ValidationException {
			if (meid == null || !HEX_NUMBER.matcher(meid).matches()) {
			  throw new ValidationException("dms-invalid-meid", "Invalid MEID: " + meid);
			}
		}

    @NotNull
    USATDevice[] populateEsEyeDevicesToModemActivate(String[] deviceIccIds) throws SQLException, DataLayerException, ConvertException {
        USATDevice[] rv = new USATDevice[deviceIccIds.length];
        for (int i = 0; i < deviceIccIds.length; i++) {
            rv[i] = new USATDevice();
            rv[i].setIccid(deviceIccIds[i]);
            rv[i].setSerialCode(deviceIccIds[i]);
            rv[i].setDeviceId(deviceModemStatusDao.findDeviceIdBySerialCode(deviceIccIds[i]));
            rv[i].setCurrentStatus(ModemState.PENDING_ACTIVATION.getStatus());
            rv[i].setRequestedStatus(ModemState.ACTIVE.getStatus());
            rv[i].setProviderName(ModemApiProvider.ESEYE.name());
        }
        return rv;
    }

    private void enqueueSimActivation(USATDevice[] devices) throws SQLException, ServletException, DataLayerException, ConvertException {
        USATModemService dmsModemService = new AsyncDmsModemService();
        dmsModemService.activate(devices);
    }
}
