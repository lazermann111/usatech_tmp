package com.usatech.dms.device.modem;

import simple.service.modem.service.DeviceModemStatusDao;
import simple.service.modem.service.DeviceModemStatusDaoImpl;
import simple.service.modem.service.dto.DeviceModemState;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.TimeUtils;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MassSimActivationStatusStep extends AbstractStep {
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            DeviceModemStatusDao modemStatusDao = new DeviceModemStatusDaoImpl();
            boolean showPending = extractShowPendingParam(form);
            boolean showActivated = extractShowActivatedParam(form);
            boolean showFailed = extractShowFailedParam(form);
            if (!showPending && !showActivated && !showFailed) {
                // When no params then show all
                showPending = showActivated = showFailed = true;
            }
            List<DeviceModemState> statuses = modemStatusDao.getLatestActivatedDevicesStatuses(showPending, showActivated, showFailed);
            List<List<DeviceModemState>> groupedStatuses = groupStatuses(statuses);
            request.setAttribute("groupedStatuses", groupedStatuses);
        } catch (Exception e) {
            throw new ServletException("Error in MassSimActivationStatusStep", e);
        }
    }

    List<List<DeviceModemState>> groupStatuses(List<DeviceModemState> statuses) {
        List<List<DeviceModemState>> groupedStatuses = new ArrayList<>();
        groupedStatuses.add(new ArrayList<>());
        DeviceModemState prevDms = null;
        for (DeviceModemState dms : statuses) {
            if (!isSameGroup(prevDms, dms)) {
                groupedStatuses.add(new ArrayList<>());
            }
            groupedStatuses.get(groupedStatuses.size() - 1).add(dms);
            prevDms = dms;
        }
        return groupedStatuses;
    }

    boolean isSameGroup(@Nullable DeviceModemState prevDms, @Nonnull DeviceModemState dms) {
        if (prevDms == null) {
            return true;
        }
        Date prevDate = prevDms.getCreatedTs();
        Date curDate = dms.getCreatedTs();
        return TimeUtils.areBothDatesInOneMinuteTimeFrame(prevDate, curDate);
    }

    boolean extractShowPendingParam(InputForm form) throws ServletException {
        return form.getInt("status_pending", false, 0) != 0;
    }

    boolean extractShowActivatedParam(InputForm form) throws ServletException {
        return form.getInt("status_activated", false, 0) != 0;
    }

    boolean extractShowFailedParam(InputForm form) throws ServletException {
        return form.getInt("status_failed", false, 0) != 0;
    }
}
