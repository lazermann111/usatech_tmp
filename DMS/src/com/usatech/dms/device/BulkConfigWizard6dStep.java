/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.DMSValuesList;
import com.usatech.dms.util.ValuesListLoader;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to page 2 of the bulk configuration wizard.
 */
public class BulkConfigWizard6dStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {	
    	if ("Next >".equalsIgnoreCase(form.getString("step6dAction", false))) {
			form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, "/bulkConfigWizard7.i");
        	return;
		}
    	
    	form.setAttribute("forward", "bulkConfigWizard6d.i");
    	try{
	    	String myaction = form.getString("myaction", false);    	
	    	if (!StringHelper.isBlank(myaction) && !"Upload to Device".equalsIgnoreCase(myaction)) {
	    		if ("External File Transfer".equalsIgnoreCase(myaction)) {
	            	form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, "/createExternalFileTransfer.i");
	            	return;
	            }
	    		if ("Change Credential".equalsIgnoreCase(myaction)) {
	    			long credentialId = form.getLong("credential_id", true, -1);
	    			form.setAttribute("ec_credential_id", credentialId);
	    			if (credentialId > 0) {
	    				Results credential = DataLayerMgr.executeQuery("GET_CREDENTIAL_INFO", new Object[] {credentialId});
	    				if (credential.next())
	    					form.setAttribute("ec_credential_name", credential.getFormattedValue("credentialName"));
	    			} else if (credentialId == 0)
	    				form.setAttribute("ec_credential_name", "No Credential");
	    			else
	    				form.setAttribute("ec_credential_name", "");
	    			return;
	    		}
	    		String s2cRequestName, s2cRequestField;
	    		if ("Queue Command".equalsIgnoreCase(myaction)) {
	            	DMSValuesList s2cActionDescList = ValuesListLoader.getValuesList(request, DMSConstants.GLOBAL_LIST_EDGE_GENERIC_RESPONSE_S2C_ACTIONS);
	            	String genericResS2C = form.getString("generic_response_s2c", false);
	            	String payload = form.getStringSafely("payload", "");
	            	boolean hasPayload = "Y".equalsIgnoreCase(genericResS2C.substring(0, 1));
	                if (hasPayload && StringHelper.isBlank(payload))
	                	throw new ServletException("Required Parameter Not Found: Payload");
	            	String actionCode = genericResS2C.substring(1);	            	
	            	s2cRequestName = s2cActionDescList.getName(actionCode);
	            	if (StringHelper.isBlank(s2cRequestName))
	            		s2cRequestName = actionCode;
	                s2cRequestField = new StringBuilder(genericResS2C).append("~").append(payload).toString();
	            } else { 
	            	s2cRequestName = myaction;
	            	s2cRequestField = s2cRequestName;
	            }
	    		if (DeviceActions.populateBulkConfigList(form, "s2c_requests", s2cRequestName, true))
	    			DeviceActions.populateBulkConfigList(form, "s2c_request_fields", s2cRequestField, false);
	    	}
    	} catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
