/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import com.usatech.dms.action.DeviceActions;
import com.usatech.layers.common.device.DevicesConstants;

public class DeviceTabStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        long deviceId = form.getLong(DevicesConstants.PARAM_DEVICE_ID, false, -1);
        if (deviceId == -1) {
            form.setRedirectUri("/deviceList.i");
            return;
        }

        // load the device information
        try
        {
            DeviceActions.loadDevice(deviceId, request);
        }
        catch (Exception e)
        {
            throw new ServletException("Failed to load device...", e);
        }
    }

}
