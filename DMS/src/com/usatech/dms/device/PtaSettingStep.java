/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.device.DevicesConstants;

public class PtaSettingStep extends AbstractStep
{

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        boolean paramFlag = true;
        long device_id = form.getLong(DevicesConstants.PARAM_DEVICE_ID, false, -1);
        long pos_id = form.getLong(DevicesConstants.PARAM_POS_ID, false, -1);
        Integer payment_subtype_id = form.getInt(DevicesConstants.PARAM_PAYMENT_SUBTYPE_ID, false, -1);
        String pos_pta_duplicate_settings = form.getString(DevicesConstants.PARAM_PTA_DUPLICATE_SETTINGS, false);
        if (device_id <= 0)
        {
            paramFlag = false;
        }
        if (payment_subtype_id == null || payment_subtype_id <= 0)
        {
            paramFlag = false;
        }
        if (pos_pta_duplicate_settings == null || pos_pta_duplicate_settings.length() == 0)
        {
            paramFlag = false;
        }
        if (!paramFlag)
        {
            request.setAttribute("errorParam", paramFlag);
            return;
        }
        Results ptaInfo = (Results)form.getAttribute("ptaInfo");
        boolean ptaInfoFlag = false;
        if (ptaInfo.next())
        {
            ptaInfoFlag = true;
        }
        String pos_pta_encrypt_key = "";
        String pos_pta_encrypt_key2 = "";
        Integer payment_subtype_key_id = null;
        String pos_pta_device_serial_cd = "";
        String pos_pta_pin_req_yn_flag = "";
        String pos_pta_regex = "";
        String pos_pta_regex_bref = "";
        Integer authority_payment_mask_id = null;
        String pos_pta_passthru_allow_yn_flag = "";
        String pos_pta_disable_debit_denial = "";
        String no_convenience_fee = "";
        Float pos_pta_pref_auth_amt = null;
        Float pos_pta_pref_auth_amt_max = null;
        String currency_cd = "";

        if (pos_pta_duplicate_settings.equalsIgnoreCase("Y") && ptaInfoFlag)
        {
            ptaInfo.setRow(1);
            pos_pta_encrypt_key = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_POS_PTA_ENCRYPT_KEY);
            pos_pta_encrypt_key2 = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_POS_PTA_ENCRYPT_KEY2);
            String temp = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_PAYMENT_SUBTYPE_KEY_ID);
            payment_subtype_key_id = (temp == null || temp.length() == 0) ? null : Integer.parseInt(temp);
            pos_pta_device_serial_cd = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_PTA_DEVICE_SERIAL_CD);
            pos_pta_pin_req_yn_flag = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_PTA_PIN_REQ_YN_FLAG);
            pos_pta_regex = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_POS_PTA_REGEX);
            pos_pta_regex_bref = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_PTA_REGEX_BREF);
            temp = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_AUTHORITY_PAYMENT_MASK_ID);
            authority_payment_mask_id = (temp == null || temp.length() == 0) ? null : Integer.parseInt(temp);
            pos_pta_passthru_allow_yn_flag = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_POS_PTA_PASSTHRU_ALLOW_YN_FLAG);
            pos_pta_disable_debit_denial = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_POS_PTA_DISABLE_DEBIT_DENIAL);
            no_convenience_fee = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_NO_CONVENIENCE_FEE);
            temp = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_POS_PTA_PREF_AUTH_AMT);
            pos_pta_pref_auth_amt = (temp == null || temp.length() == 0) ? null : Float.parseFloat(temp);
            temp = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_POS_PTA_PREF_AUTH_AMT_MAX);
            pos_pta_pref_auth_amt_max = (temp == null || temp.length() == 0) ? null : Float.parseFloat(temp);
            currency_cd = ptaInfo.getFormattedValue(DevicesConstants.COLUMN_CURRENCY_CD);
        }
        try
        {
			Map<String, Object> params = new HashMap<>();
			params.put("pos_id", pos_id);
			params.put("payment_subtype_id", payment_subtype_id);
			params.put("pos_pta_encrypt_key", pos_pta_encrypt_key);
			params.put("pos_pta_encrypt_key2", pos_pta_encrypt_key2);
			params.put("payment_subtype_key_id", payment_subtype_key_id);
			params.put("pos_pta_device_serial_cd", pos_pta_device_serial_cd);
			params.put("pos_pta_pin_req_yn_flag", (!pos_pta_pin_req_yn_flag.equals("")) ? pos_pta_pin_req_yn_flag : "N");
			params.put("pos_pta_regex", pos_pta_regex);
			params.put("pos_pta_regex_bref", pos_pta_regex_bref);
			params.put("authority_payment_mask_id", authority_payment_mask_id);
			params.put("pos_pta_passthru_allow_yn_flag", (!pos_pta_passthru_allow_yn_flag.equals("")) ? pos_pta_passthru_allow_yn_flag : "N");
			params.put("pos_pta_disable_debit_denial", pos_pta_disable_debit_denial);
			params.put("pos_pta_pref_auth_amt", pos_pta_pref_auth_amt);
			params.put("pos_pta_pref_auth_amt_max", pos_pta_pref_auth_amt_max);
			params.put("currency_cd", currency_cd);
			params.put("no_convenience_fee", no_convenience_fee);
			DataLayerMgr.executeUpdate("INSERT_POS_PTA", params, true);
			long ptaId = ConvertUtils.getLong(params.get("pos_pta_id"));
			request.setAttribute(DevicesConstants.PARAM_POS_OLD_DATA, ptaInfoFlag);
            request.setAttribute(DevicesConstants.PARAM_POS_PTA_ID, ptaId);
		} catch(SQLException | ConvertException | DataLayerException e) {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
