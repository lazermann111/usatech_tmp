/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.DeviceActions;
import com.usatech.layers.common.model.Device;
import com.usatech.layers.common.util.StringHelper;

public class DeviceCloningStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm inputForm, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        processUserOP(inputForm, request);
    }

    private void processUserOP(InputForm inputForm, HttpServletRequest request) throws ServletException
    {
        long targetDeviceId = -1;
        long sourceDeviceId = -1;
        Results sourceDeviceInfo;
        Results targetDeviceInfo;
        Device sourceDevice;
        Device targetDevice;
        String sourceNumber = inputForm.getString("sourceNumber", false);
        String targetNumber = inputForm.getString("targetNumber", false);
        String userOP = inputForm.getString("userOP", false);
        String cloningType = inputForm.getString("cloningType", false);        
        try
        {
            if (StringHelper.isBlank(userOP))
            {
                return;
            }
            if ("S".equals(cloningType))
            {
                sourceDeviceInfo = DataLayerMgr.executeQuery("GET_DEVICE_INFO_BY_SERIAL_NUMBER", new Object[] {sourceNumber}, false);
                targetDeviceInfo = DataLayerMgr.executeQuery("GET_DEVICE_INFO_BY_SERIAL_NUMBER", new Object[] {targetNumber}, false);
            }
            else
            {
                sourceDeviceInfo = DataLayerMgr.executeQuery("GET_DEVICE_INFO_BY_EV_NUMBER", new Object[] {sourceNumber}, false);
                targetDeviceInfo = DataLayerMgr.executeQuery("GET_DEVICE_INFO_BY_EV_NUMBER", new Object[] {targetNumber}, false);
            }
            if (sourceDeviceInfo.next())
                sourceDeviceId = sourceDeviceInfo.getValue("device_id", long.class);
            if (targetDeviceInfo.next())
                targetDeviceId = targetDeviceInfo.getValue("device_id", long.class);

            if (sourceDeviceId < 1 || targetDeviceId < 1)
            {
                return;
            }
            sourceDevice = DeviceActions.getDevice(request, userOP, sourceDeviceId);
            targetDevice = DeviceActions.getDevice(request, userOP, targetDeviceId);

            if ("Clone".equals(userOP))
            	DeviceActions.cloneDevice(inputForm, request, userOP, sourceDevice, targetDevice, true);

            inputForm.setAttribute("sourceDevice", sourceDevice);
            inputForm.setAttribute("targetDevice", targetDevice);
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }
}