/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.dms.action.DeviceActions;
import com.usatech.layers.common.device.DevicesConstants;

public class PosptaTmplImportStep extends AbstractStep
{

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
		String errorMessage = null;

        long device_id = form.getLong(DevicesConstants.PARAM_DEVICE_ID, false, -1);
        int pos_pta_tmpl_id = form.getInt(DevicesConstants.PARAM_POS_PTA_TMPL_ID, false, -1);
        if (pos_pta_tmpl_id < 0)
        {
            errorMessage = "Required Parameter Not Found: pos_pta_tmpl_id";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        String mode_cd = form.getString(DevicesConstants.PARAM_MODE_CD, false);
        if (!"|S|MS|MO|O|CO|".contains("|" + mode_cd + "|"))
        {
            errorMessage = "Unrecognized import mode_cd: " + mode_cd;
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        String order_cd = form.getString(DevicesConstants.PARAM_ORDER_CD, false);
		String set_terminal_cd_to_serial = form.getStringSafely("set_terminal_cd_to_serial", "N");
		String only_no_two_tier_pricing = form.getStringSafely("only_no_two_tier_pricing", "N");
        
		try {
			DeviceActions.importPosPtaTemplate(device_id, pos_pta_tmpl_id, mode_cd, order_cd, set_terminal_cd_to_serial, only_no_two_tier_pricing, null);
		} catch(ServletException e) {
			if(e.getCause() instanceof SQLException) {
				SQLException sqle = (SQLException) e.getCause();
				if(sqle.getErrorCode() == 20151) {
					form.setRedirectUri(new StringBuilder("/handleMerchantFull.i?submit_to=importPosptaTmplFunc.i&device_ids_param=device_id&device_id=")
							.append(device_id).append("&pos_pta_tmpl_id=").append(pos_pta_tmpl_id).append("&mode_cd=").append(mode_cd)
							.append("&order_cd=").append(order_cd).append("&set_terminal_cd_to_serial=").append(set_terminal_cd_to_serial)
							.append("&only_no_two_tier_pricing=").append(only_no_two_tier_pricing).toString());
					return;
				}
			}
			throw e;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("/paymentConfig.i?device_id=").append(device_id).append("&userOP=payment_types");
		if(!StringUtils.isBlank(errorMessage))
			sb.append("&errorMessage=").append(StringUtils.prepareURLPart(errorMessage));
		form.setRedirectUri(sb.toString());
    }
}
