package com.usatech.dms.device;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class FirmwareUpgradeStep extends AbstractStep
{
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	try {
	    	String action = form.getString("action", false);
	    	if (!StringUtils.isBlank(action)) {
	    		String message = "";
	    		
	    		long firmwareUpgradeId = form.getLong("firmwareUpgradeId", false, 0);
	    		Map<String, Object> params = new HashMap<String, Object>();
	    		params.put("firmwareUpgradeId", firmwareUpgradeId);
	    		params.put("firmwareUpgradeName", form.getString("firmware_upgrade_name", false));
	    		params.put("firmwareUpgradeDesc", form.getString("firmware_upgrade_desc", false));
	    		params.put("deviceTypeId", form.getString("device_type_id", false));
	    		
		    	if ("Save".equalsIgnoreCase(action)) {
		    		try {
		    			DataLayerMgr.executeUpdate("UPDATE_FIRMWARE_UPGRADE", params, true);
		    		} catch (SQLIntegrityConstraintViolationException e) {
	    	    		form.set("statusMessage", "Firmware Upgrade with this name already exists");
	    			    form.set("statusType", "error");
	    			    return;
		    		}
		    		message = "Firmware+Upgrade+updated";
		    	} else if ("Create".equalsIgnoreCase(action)) {
		    		try {
		    			DataLayerMgr.executeUpdate("INSERT_FIRMWARE_UPGRADE", params, true);
		    		} catch (SQLIntegrityConstraintViolationException e) {
	    	    		form.set("statusMessage", "Firmware Upgrade with this name already exists");
	    			    form.set("statusType", "error");
	    			    return;
		    		}
		            firmwareUpgradeId = ConvertUtils.getLong(params.get("firmwareUpgradeId"));		    		
		    		message = "Firmware+Upgrade+created";
		    	} else if ("Delete".equalsIgnoreCase(action)) {
		    		DataLayerMgr.executeUpdate("DELETE_FIRMWARE_UPGRADE", params, true);
		    		message = "Firmware+Upgrade+deleted";
		    	} else if ("Add Step".equalsIgnoreCase(action)) {
		    		try {
		    			long stepId = 0;
		    			params.put("firmwareUpgradeStepName", form.getString("step_name_" + stepId, true));
		    			int stepNumber = form.getInt("step_number_" + stepId, true, 0);
		    			if (stepNumber <= 0) {
		    				form.set("statusMessage", "Firmware Upgrade step # must be > 0");
		    			    form.set("statusType", "error");
		    			    return;
		    			}
		    			params.put("stepNumber", stepNumber);
		    			params.put("branchParamCd", form.getString("branch_param_cd_" + stepId, true));
		    			params.put("branchValueRegex", form.getString("branch_value_regex_" + stepId, false));
		    			params.put("targetParamCd", form.getString("target_param_cd_" + stepId, false));
		    			params.put("targetVersion", form.getString("target_version_" + stepId, false));
		    			params.put("targetValueRegex", form.getString("target_value_regex_" + stepId, false));
		    			params.put("fileTransferId", form.getString("file_transfer_id_" + stepId, true));
		    			DataLayerMgr.executeUpdate("INSERT_FIRMWARE_UPGRADE_STEP", params, true);
		    		} catch (SQLIntegrityConstraintViolationException e) {
	    	    		form.set("statusMessage", "Firmware Upgrade step with this step # or name already exists");
	    			    form.set("statusType", "error");
	    			    return;
		    		}
		    		message = "Firmware+Upgrade+step+added";
		    	} else if ("Update Step".equalsIgnoreCase(action)) {
		    		try {
		    			long stepId = form.getLong("current_step_id", true, 0);
		    			params.put("firmwareUpgradeStepId", stepId);
		    			params.put("firmwareUpgradeStepName", form.getString("step_name_" + stepId, true));
		    			int stepNumber = form.getInt("step_number_" + stepId, true, 0);
		    			if (stepNumber <= 0) {
		    				form.set("statusMessage", "Firmware Upgrade step # must be > 0");
		    			    form.set("statusType", "error");
		    			    return;
		    			}
		    			params.put("stepNumber", stepNumber);
		    			params.put("branchParamCd", form.getString("branch_param_cd_" + stepId, true));
		    			params.put("branchValueRegex", form.getString("branch_value_regex_" + stepId, false));
		    			params.put("targetParamCd", form.getString("target_param_cd_" + stepId, false));
		    			params.put("targetVersion", form.getString("target_version_" + stepId, false));
		    			params.put("targetValueRegex", form.getString("target_value_regex_" + stepId, false));
		    			params.put("fileTransferId", form.getString("file_transfer_id_" + stepId, true));
		    			DataLayerMgr.executeUpdate("UPDATE_FIRMWARE_UPGRADE_STEP", params, true);
		    		} catch (SQLIntegrityConstraintViolationException e) {
	    	    		form.set("statusMessage", "Firmware Upgrade step with this step # or name already exists");
	    			    form.set("statusType", "error");
	    			    return;
		    		}
		    		message = "Firmware+Upgrade+step+updated";
		    	} else if ("Delete Step".equalsIgnoreCase(action)) {
		    		long stepId = form.getLong("current_step_id", true, 0);
		    		params.put("firmwareUpgradeStepId", stepId);
		    		DataLayerMgr.executeUpdate("DELETE_FIRMWARE_UPGRADE_STEP", params, true);
		    		message = "Firmware+Upgrade+step+deleted";
		    	}
		    	form.setRedirectUri(new StringBuilder("/firmwareUpgrade.i?firmwareUpgradeId=").append(firmwareUpgradeId).append("&msg=").append(message).toString());
	    	}
    	} catch (Exception e) {
    		throw new ServletException(e);
    	}
    }
}