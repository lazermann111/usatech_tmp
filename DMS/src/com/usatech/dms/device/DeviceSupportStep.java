package com.usatech.dms.device;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.util.DMSUtils;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.exception.ValidationException;
import com.usatech.layers.common.process.ProcessType;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class DeviceSupportStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response)
			throws ServletException {

		if (!"POST".equalsIgnoreCase(request.getMethod())) {
			return;
		}
		updateCellularDeviceNumbers(form, request, response);
	}

	private boolean isValidContentType(HttpServletRequest request) {
		String contentType = request.getContentType();
		if (contentType == null || contentType.indexOf("multipart/form-data") < 0) {
			return false;
		}
		return true;
	}

	private void validateUpdateFile(Object updateFile) throws ValidationException {
		if (updateFile == null) {
			throw new ValidationException("dms-missing-parameter",
					"The file to upload was not specified in the request");
		}
	}

	private void validateEmail(String email) throws ValidationException {
		if (StringUtils.isBlank(email)) {
			throw new ValidationException("dms-invalid-email", "Notification email was not specified");
		} else {
			try {
				WebHelper.checkEmail(email);
			} catch (MessagingException e) {
				throw new ValidationException("dms-invalid-email",
						"You entered an invalid email: " + e.getMessage() + ". Please enter a valid one");
			}
		}
	}

	private void updateCellularDeviceNumbers(InputForm form, HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		try {
			if (!isValidContentType(request)) {
				return;
			}

			Object updateFile = form.getAttribute("file_data");
			validateUpdateFile(updateFile);
			String notifyEmail = form.getString("notifyEmail", false);
			validateEmail(notifyEmail);
			InputStream content;
			String path;

			if (updateFile instanceof InputFile) {
				InputFile inputFile = (InputFile) updateFile;
				path = inputFile.getPath();
				content = inputFile.getInputStream();
			} else {
				path = "updateFile";
				content = ConvertUtils.convert(InputStream.class, updateFile);
			}

			Map<String, Object> parameters = new LinkedHashMap<>();
			parameters.put("emailFromName", Helper.getDMS_EMAIL_FROM_NAME());
			parameters.put("emailFromAddress", Helper.getDMS_EMAIL());
			parameters.put("hostTypeId", form.getInt("host_type_id", true, -1));
			parameters.put("updateFile", path);
			String desc = RequestUtils.getTranslator(request).translate("device.update.cellular.numbers",
					"Cellular Device Support ''{0}''", path);

			try {
				DMSUtils.registerProcessRequest(request, ProcessType.CELLULAR_DEVICE_SUPPORT_CSV, desc, notifyEmail,
						parameters, content);
			} catch (Exception e) {
				RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "device.update.error.enqueue",
						"Sorry for the inconvenience. We could not process your request at this time. Please try again.",
						e.getMessage());
				errorResult(request, response);
				return;
			} finally {
				content.close();
			}
			addSuccessMessage(form);
		} catch (ValidationException e) {
			RequestUtils.getOrCreateMessagesSource(request).addMessage("error", e.getMessageId(),
					e.getDefaultMessage());
			try {
				errorResult(request, response);
			} catch (IOException ioException) {
				throw new ServletException("Error applying SIM/MEID numbers", ioException);
			}
		} catch (Exception e) {
			throw new ServletException("Error applying SIM/MEID numbers", e);
		}
	}

	private void addSuccessMessage(InputForm form) {
		String message = "Request is registered. You will receive notification email when process is finished.";
		form.setRedirectUri("/deviceSupport.i?message=" + message);
	}

	private void errorResult(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] contentTypes = RequestUtils.getPreferredContentType(request, "text/html", "application/xhtml+xml",
				"text/plain");
		boolean textResult = (contentTypes != null && contentTypes.length > 0
				&& contentTypes[0].equalsIgnoreCase("text/plain"));
		if (textResult)
			RequestUtils.writeTextResultFromMessages(request, response);
		else
			RequestUtils.redirectWithCarryOver(request, response, "deviceSupport.i", false, true);
	}
}