/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.action.PendingCmdActions;
import com.usatech.layers.common.ProcessingUtils;

/**
 * Step to process the "Import" action from the "Device Configuration" page.
 */
public class GxCloneFuncStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {    	
        long source_device_id = form.getLong("source_device_id", false, -1);
        long target_device_id = form.getLong("target_device_id", false, -1);
        String source_ev_number = form.getString("source_ev_number", false);
        String target_ev_number = form.getString("target_ev_number", false);
        long source_pos_id = form.getLong("source_pos_id", false, -1);
        long target_pos_id = form.getLong("target_pos_id", false, -1);
        long source_location_id = form.getLong("source_location_id", false, -1);
        long target_location_id = form.getLong("target_location_id", false, -1);
        long source_customer_id = form.getLong("source_customer_id", false, -1);
        long target_customer_id = form.getLong("target_customer_id", false, -1);
                
        Connection conn = null;
    	boolean success = false;
        try{
        	conn = DataLayerMgr.getConnection("OPER");
        	StringBuilder out = new StringBuilder("<table class=\"tabDataDisplayBorderNoFixedLayout\" width=\"100%\" ><tr><td><pre>\n");
        	
        	/**
        	 * 	# we are sending very specific sections of the config, not the whole thing for Gx
			 *	#	0 	- 135	/ 2 = 0		length = 136
			 *	#	142 - 283	/ 2 = 71	length = 142
			 *	#	356 - 511	/ 2 = 178	length = 156
        	 */
        	
        	Object[] result = DataLayerMgr.executeCall(conn, "CLONE_GX_CONFIG", new Object[] {source_device_id, target_device_id, 0, 0, 0, 0});
        	if (ConvertUtils.getInt(result[1]) > 0) {
        		PendingCmdActions.createPoke(conn, target_ev_number, 0, 136, 0);
        		out.append("Poke 1 created successfully!\n");
        	} else
				out.append("Poke 1 skipped, no change.\n");
        	if (ConvertUtils.getInt(result[2]) > 0) {
        		PendingCmdActions.createPoke(conn, target_ev_number, 71, 142, 0);
        		out.append("Poke 2 created successfully!\n");
        	} else
				out.append("Poke 2 skipped, no change.\n");
        	if (ConvertUtils.getInt(result[3]) > 0) {
        		PendingCmdActions.createPoke(conn, target_ev_number, 178, 156, 0);
        		out.append("Poke 3 created successfully!\n");
        	} else
				out.append("Poke 3 skipped, no change.\n");
        	if (ConvertUtils.getInt(result[4]) > 0) {
        		PendingCmdActions.createPoke(conn, target_ev_number, 160, 36, 0);
        		out.append("Counters Poke created successfully!\n");
        	} else
				out.append("Counters Poke skipped, no change.\n");
        	
        	/**
        	 * Execute customer and location changes.
        	 */
        	if(source_location_id != target_location_id || source_customer_id != target_customer_id){
        		Object[] updateLocCustResult = DataLayerMgr.executeCall(conn, "UPDATE_LOC_ID_CUST_ID", new Object[] {target_device_id, source_location_id, source_customer_id, 20, 20, 20, "2000"});

	        	if(updateLocCustResult!=null &&updateLocCustResult.length>0){
	        		int custLoc_result = ((BigDecimal)updateLocCustResult[3]).intValue();
	        		String custLoc_msg = (String)updateLocCustResult[4];
	        		if(custLoc_result < 0){
	        			out.append("Error creating Counters Poke! " + custLoc_msg + "\n");
					}else if(custLoc_result == 0){
						out.append("Customer and Location Updated successfully!\n");
						target_device_id = ConvertUtils.getLong(updateLocCustResult[1]);
						target_pos_id = ConvertUtils.getLong(updateLocCustResult[2]);
					}else{
						out.append("Customer and Location update skipped: no change.\n");
					}
	        	}
        	}else{
        		out.append("Customer and Location update skipped: no change.\n");
        	}
        	
        	/**
        	 * Execute clone PosPta changes.
        	 */         	
        	DeviceActions.cloneDevicePosPtasNoUpdateOldPriorities(conn, source_device_id, target_device_id, source_pos_id, target_pos_id);
            out.append("Payment Types Cloned Successfully!\n");
        	
        	out.append("<a href=\"profile.i?device_id=" +target_device_id + "\"><b>Continue</b></a>\n");
        	out.append("</pre></td></tr></table>");
        	form.set("cloneOut", out.toString());
        	
    		out.append("\n\nDevice Cloned Successfully!\n\n");
    		conn.commit();
        	success = true;
        }catch (Exception e){
            throw new ServletException("Failed to clone from '" + source_ev_number + "' to '" + target_ev_number, e);
        }finally{
    		if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
    	}
    }
}
