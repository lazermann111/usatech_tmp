package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.layers.common.posm.AdminCommandUtil;

public class POSMAdminStep extends AbstractStep
{
	protected int priority = 100;
	
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	try {
	    	String action = form.getStringSafely("action", "");
	    	if ("Submit".equalsIgnoreCase(action)) {
	    		String adminCmd = form.getString("admin_cmd", false); 
	    		if (StringUtils.isBlank(adminCmd)) {
	    			request.setAttribute("err", "Please select a command");
	    			return;
	    		}
	    		String param1 = form.getStringSafely("param1", "");
	    		String param2 = form.getStringSafely("param2", "");
	    		BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
	    		String requestedBy = user.getEmailAddress();
	    		int adminCmdType = Integer.valueOf(adminCmd.split("_")[0]);
	    		long itemId = -1;
	    		switch (adminCmdType) {
	    			case 3: case 4: case 5: case 6: case 7: case 8:
	    				itemId = ConvertUtils.getLongSafely(param1, -1);
		    			if (itemId < 1) {
		    				request.setAttribute("err", "Invalid Id");
		    				return;
		    			}
		    			break;
	    		}
	    		switch (adminCmdType) {
	    			case 5: case 6: case 7:
		    			if (StringUtils.isBlank(param2)) {
		    				request.setAttribute("err", "Please enter a reason");
		    				return;
		    			}
		    			break;
	    		}
	    		switch (adminCmdType) {
	    			case 1:
	    				AdminCommandUtil.settleAll(requestedBy, priority);
	    				break;
	    			case 2:
	    				AdminCommandUtil.retryAll(requestedBy, priority);
	    				break;
	    			case 3:
	    				AdminCommandUtil.retrySettlement(itemId, requestedBy, priority);
	    				break;
	    			case 4:
	    				AdminCommandUtil.retryTransaction(itemId, requestedBy, priority);
	    				break;
	    			case 5:
	    				AdminCommandUtil.forceTransaction(itemId, param2, requestedBy, priority);
	    				break;
	    			case 6:
	    				AdminCommandUtil.forceSettlement(itemId, param2, requestedBy, priority);
	    				break;
	    			case 7:
	    				AdminCommandUtil.errorTransaction(itemId, param2, requestedBy, priority);
	    				break;
	    			case 8:
	    				AdminCommandUtil.settleBatch(itemId, requestedBy, priority);
	    				break;
	    		}
		    	request.setAttribute("msg", "Command created");
	    	}
    	} catch (Exception e) {
    		throw new ServletException("Error occurred while creating command, please check your command parameters", e);
    	}
    }

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}