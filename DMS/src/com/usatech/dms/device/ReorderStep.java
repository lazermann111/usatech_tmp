/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

public class ReorderStep extends AbstractStep
{

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        Map<String, Object> params = form.getParameters();
        String reorderError = null;
        String[] errorValue = new String[3];
        HashMap<String, Object[]> hash = null;
        String pos_pta_id = null;
        String new_priority = null;
        String old_priority = null;
        String[] hashValue = null;
        Object[] sqlParams = null;
        if (params != null && !params.isEmpty())
        {
            hash = new HashMap<String, Object[]>();
            Iterator<String> keys = params.keySet().iterator();
            String key = null;
            String pos_pta_payment_action_type_cd = null;
            String pos_pta_payment_entry_method_cd = null;
            while (keys.hasNext())
            {
                key = (String)keys.next();
                if (key.startsWith("new_priority_"))
                {
                    pos_pta_id = key.substring(13);
                    pos_pta_payment_action_type_cd = (String)params.get("pos_pta_payment_action_type_cd_" + pos_pta_id);
                    pos_pta_payment_entry_method_cd = (String)params.get("pos_pta_payment_entry_method_cd_" + pos_pta_id);
                    if (params.get(key) == null)
                    {
                        reorderError = "MissingPriority";
                        errorValue[0] = pos_pta_payment_action_type_cd;
                        errorValue[1] = pos_pta_payment_entry_method_cd;
                        errorValue[2] = pos_pta_id;
                        break;
                    }
                    new_priority = (String)params.get(key);
                    old_priority = (String)params.get("old_priority_" + pos_pta_id);
                    if (hash.containsKey(pos_pta_payment_action_type_cd + "." + pos_pta_payment_entry_method_cd + "." + new_priority))
                    {
                        reorderError = "DuplicateError";
                        errorValue[0] = pos_pta_payment_action_type_cd;
                        errorValue[1] = pos_pta_payment_entry_method_cd;
                        errorValue[2] = new_priority;
                        break;
                    }
                    else
                    {
                        hashValue = new String[3];
                        hashValue[0] = pos_pta_id;
                        hashValue[1] = new_priority;
                        hashValue[2] = old_priority;
                        hash.put(pos_pta_payment_action_type_cd + "." + pos_pta_payment_entry_method_cd + "." + new_priority, hashValue);
                    }

                }
                else
                {
                    continue;
                }
            }

        }
        else
        {
            reorderError = "MissingParam";
        }
        if (reorderError != null)
        {
			RequestUtils.storeForNextRequest(request.getSession(), "reorderError", reorderError);
			RequestUtils.storeForNextRequest(request.getSession(), "reorderValue", errorValue);
            return;
        }
        if (hash != null)
        {
            Iterator<Object[]> values = hash.values().iterator();
            try
            {
                while (values.hasNext())
                {
                    hashValue = (String[])values.next();
                    pos_pta_id = hashValue[0];
                    new_priority = hashValue[1];
                    old_priority = hashValue[2];
                    if (!new_priority.equals(old_priority))
                    {
                        sqlParams = new Object[] {new Integer(new_priority), new Integer(pos_pta_id)};
                        DataLayerMgr.executeUpdate("UPDATE_POS_PTA_PRIORITY", sqlParams, true);

                    }
                }
            }
            catch (SQLException e)
            {
                throw new ServletException();
            }
            catch (DataLayerException e)
            {
                throw new ServletException();
            }
        }

    }

}
