/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.model.ConfigTemplateSetting;

/**
 * Step to process the "edit configuration" (G4, G5, MEI)
 */
public class BulkConfigWizard5Step extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	try {
	        long templateId =  form.getLong("templateId", false, -1);
	        if(templateId < 1){
	            request.setAttribute("errorMessage", "<br/><br/><b><font color='red'>Undefined Template!</font></b><br/><br/>");
	            return;
	        }
	        
	        int deviceTypeId =  form.getInt("device_type_id", false, -1);
	        if(deviceTypeId < 0){
	            request.setAttribute("errorMessage", "<br/><br/><b><font color='red'>Undefined Device Type!</font></b><br/><br/>");
	            return;
	        }
	        
	        int propertyListVersion = -1;
	        Results results = DataLayerMgr.executeQuery("GET_CONFIG_TEMPLATE_INFO", new Object[]{templateId}, false);
	        if (results.next()) {
	        	String templateName = results.getFormattedValue("config_template_name");
	        	request.setAttribute("file_name", templateName);
	        	propertyListVersion = ConfigFileActions.getPropertyListVersionForConfigTemplateCustom(deviceTypeId, templateName);
	        }
	        request.setAttribute("plv", propertyListVersion);
	        
	        LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceTypeId, propertyListVersion);        
	        request.setAttribute("defaultSettingData", defaultSettingData);
	        
	        LinkedHashMap<String, String> targetSettingData = ConfigFileActions.getConfigTemplateSettingData(defaultSettingData, templateId, deviceTypeId, true, null);
	        request.setAttribute("targetSettingData", targetSettingData);
    	} catch (Exception e) {
        	throw new ServletException(e);
        }
    }
}
