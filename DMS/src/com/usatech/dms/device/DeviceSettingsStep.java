/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies.  All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL.  Use is subject to license terms.
 */

package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.model.Device;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class DeviceSettingsStep extends AbstractStep
{
    // by default, sort by "Time" descending
    private static final String DEFAULT_SORT_INDEX = "-2";

    private static final String SQL_START = "SELECT /*+INDEX(e IDX_EVENT_GLOBAL_TRANS_CD)*/ e.event_id, " +
    "TO_CHAR(e.event_start_ts, 'MM/DD/YYYY HH24:MI:SS') as event_start_ts, " +
    "es.event_state_name, " +
    "et.event_type_name, " +
    "ht.host_type_desc, " +
    "e.global_session_cd, " +
    "substr(e.global_session_cd, instr(e.global_session_cd, ':', 1, 3) + 1) session_id ";

    private static final String SQL_BASE = "FROM device.event e, device.event_state es, device.event_type et, device.host h, device.host_type ht " +
            "WHERE e.host_id = h.host_id " +
            "AND e.event_type_id = et.event_type_id " +
            "AND e.event_state_id = es.event_state_id " +
            "AND h.host_type_id = ht.host_type_id ";

    private static final String[] SORT_FIELDS = {"e.event_id",
        "e.event_start_ts",
        "es.event_state_name",
        "et.event_type_name",
        "ht.host_type_desc",
        "e.global_session_cd"
        };

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);        
        String userOP = form.getString(DevicesConstants.PARAM_USER_OP, false);        

        try
        {
        	if (!device.hasCounters() && !device.hasEvents() || DevicesConstants.USER_OP_LIST_ALL.equals(userOP) || DevicesConstants.USER_OP_LIST_SETTINGS.equals(userOP)) {
        		Results deviceSettings = (Results)DataLayerMgr.executeQuery("GET_DEVICE_SETTINGS", new Object[] {device.getId()}, false);
        		request.setAttribute("deviceSettings", deviceSettings);
        	}

            if (device.hasCounters())
            {
            	int counter = form.getInt(DevicesConstants.PARAM_COUNTER_COUNT, false, 7);
            	
            	if ((DevicesConstants.USER_OP_LIST_ALL.equals(userOP) || DevicesConstants.USER_OP_LIST_COUNTERS.equals(userOP))) {
	                String[] counter_names;
	                if (device.isEDGE())
	                {
	                    counter_names = DevicesConstants.counter_names_for_edge;
	                }
	                else
	                {
	                    counter_names = DevicesConstants.counter_names_for_other;
	                }
	                Object[] counters_param = new Object[3];
	                counters_param[0] = device.getDeviceName();
	                counters_param[2] = counter;
	                for (int i = 0; i < counter_names.length; i++)
	                {
	                    counters_param[1] = counter_names[i];
	                    Results counters = (Results)DataLayerMgr.executeQuery("GET_COUNTERS", counters_param, false);
	                    request.setAttribute("counters_" + counter_names[i], counters);
	                }
            	}
            	
                request.setAttribute(DevicesConstants.PARAM_COUNTER_COUNT, counter);
            }

            if (device.hasEvents())
            {
            	String event_from_time = form.getString(DevicesConstants.PARAM_EVENT_FROM_TIME, false);
                if (StringHelper.isBlank(event_from_time)){
                    event_from_time = Helper.getDefaultStartTime();
                    request.setAttribute(DevicesConstants.PARAM_EVENT_FROM_TIME, event_from_time);
                }        
                String event_from_date = form.getString(DevicesConstants.PARAM_EVENT_FROM_DATE, false);
                if (StringHelper.isBlank(event_from_date)){
                    event_from_date = Helper.getDefaultStartDate();
                    request.setAttribute(DevicesConstants.PARAM_EVENT_FROM_DATE, event_from_date);
                }
                String event_to_time = form.getString(DevicesConstants.PARAM_EVENT_TO_TIME, false);
                if (StringHelper.isBlank(event_to_time)){
                    event_to_time = Helper.getDefaultEndTime();
                    request.setAttribute(DevicesConstants.PARAM_EVENT_TO_TIME, event_to_time);
                }        
                String event_to_date = form.getString(DevicesConstants.PARAM_EVENT_TO_DATE, false);
                if (StringHelper.isBlank(event_to_date)){
                    event_to_date = Helper.getDefaultEndDate();
                    request.setAttribute(DevicesConstants.PARAM_EVENT_TO_DATE, event_to_date);
                }
            	
            	if ((DevicesConstants.USER_OP_LIST_ALL.equals(userOP) || DevicesConstants.USER_OP_LIST_EVENTS.equals(userOP))) {
	                StringBuilder sql_criteria = new StringBuilder(SQL_BASE);
					if (!DialectResolver.isOracle()) {
						sql_criteria.append(
								" AND e.event_global_trans_cd LIKE 'A:' || ? || ':%' AND e.event_start_ts BETWEEN TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')");
						sql_criteria.append(" AND TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') ");
					} else {
						sql_criteria.append(
								" AND e.event_global_trans_cd LIKE 'A:' || ? || ':%' AND e.event_start_ts BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')");
						sql_criteria.append(" AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') ");
					}
	                String queryBase = sql_criteria.toString();
	
	                String paramTotalCount = PaginationUtil.getTotalField(null);
	                String paramPageIndex = PaginationUtil.getIndexField(null);
	                String paramPageSize = PaginationUtil.getSizeField(null);
	                String paramSortIndex = PaginationUtil.getSortField(null);
	
	                // pagination parameters
	                int totalCount = form.getInt(paramTotalCount, false, -1);
	                if (totalCount == -1)
	                {
	                    // if the total count is not retrieved yet, get it now
	                    Results total = DataLayerMgr.executeSQL("OPER", new StringBuilder("SELECT COUNT(1) ").append(queryBase).toString(), new Object[]{device.getDeviceName(), event_from_date, event_from_time, event_to_date, event_to_time}, null);
	                    if (total.next())
	                    {
	                        totalCount = total.getValue(1, int.class);
	                    }
	                    else
	                    {
	                        totalCount = 0;
	                    }
	                    request.setAttribute(paramTotalCount, String.valueOf(totalCount));
	                }
	
	                int pageIndex = form.getInt(paramPageIndex, false, 1);
	                int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
	                int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
	                int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);
	
	                String sortIndex = form.getString(paramSortIndex, false);
	                sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
	                String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
	
	                String query;
					if (!DialectResolver.isOracle()) {
						query = new StringBuilder("select * from (")
								.append(" select pagination_temp.*, row_number() over() rnum from (").append(SQL_START)
								.append(queryBase).append(orderBy).append(" LIMIT ?::bigint) pagination_temp")
								.append(")sq_end where rnum  >= ?::bigint ").toString();
					} else {
						query = new StringBuilder("select * from (")
								.append(" select pagination_temp.*, ROWNUM rnum from (").append(SQL_START)
								.append(queryBase).append(orderBy).append(") pagination_temp where ROWNUM <= ? ")
								.append(") where rnum  >= ? ").toString();
					}
	
	                Results events = DataLayerMgr.executeSQL("OPER", query, new Object[]{device.getDeviceName(), event_from_date, event_from_time, event_to_date, event_to_time, maxRowToFetch, minRowToFetch}, null);
	                request.setAttribute("events", events);
            	}
            }
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }
}
