/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class PosptaTmplEditStep extends AbstractStep
{

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String action = form.getString(DevicesConstants.PARAM_ACTION, false);
        int pos_pta_tmpl_id = form.getInt(DevicesConstants.PARAM_POS_PTA_TMPL_ID, false, -1);
        String errorMessage = null;
        if (action == null || action.length() == 0)
        {
            errorMessage = "Required Parameter Not Found: action";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        if (pos_pta_tmpl_id < 0 && !("Save".equals(action)))
        {
            errorMessage = "Required Parameter Not Found: pos_pta_tmpl_id";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        Results templateData = (Results)form.getAttribute("templateData");
        if ((templateData == null) && !("Save".equals(action)))
        {
            return;
        }
        
        if (action.equalsIgnoreCase("Reorder"))
        {
            Map<String, Object> params = form.getParameters();
            HashMap<String, Object[]> hash = null;
            String pos_pta_tmpl_entry_id = null;
            String new_priority = null;
            String old_priority = null;
            String[] hashValue = null;
            Object[] sqlParams = null;
            if (params != null && !params.isEmpty())
            {
                hash = new HashMap<String, Object[]>();
                Iterator<String> keys = params.keySet().iterator();
                String key = null;
                String payment_action_type_cd = null;
                String payment_entry_method_cd = null;
                while (keys.hasNext())
                {
                    key = (String)keys.next();
                    if (key.startsWith("new_priority_"))
                    {
                        pos_pta_tmpl_entry_id = key.substring(13);
                        payment_action_type_cd = (String)params.get("payment_action_type_cd_" + pos_pta_tmpl_entry_id);
                        payment_entry_method_cd = (String)params.get("payment_entry_method_cd_" + pos_pta_tmpl_entry_id);
                        if (params.get(key) == null)
                        {
                            errorMessage = "Missing Category Priority Value! (" + pos_pta_tmpl_entry_id + ", " + new_priority + ", " + payment_action_type_cd + ", " + payment_entry_method_cd + ")";
                            request.setAttribute("errorMessage", errorMessage);
                            return;
                        }
                        new_priority = (String)params.get(key);
                        old_priority = (String)params.get("old_priority_" + pos_pta_tmpl_entry_id);
                        if (hash.containsKey(payment_action_type_cd + "." + payment_entry_method_cd + "." + new_priority))
                        {
                            errorMessage = "Duplicate Category Priority Values! (" + pos_pta_tmpl_entry_id + ", " + new_priority + ", " + payment_action_type_cd + ", " + payment_entry_method_cd + ")";
                            request.setAttribute("errorMessage", errorMessage);
                            return;
                        }
                        else
                        {
                            hashValue = new String[3];
                            hashValue[0] = pos_pta_tmpl_entry_id;
                            hashValue[1] = new_priority;
                            hashValue[2] = old_priority;
                            hash.put(payment_action_type_cd + "." + payment_entry_method_cd + "." + new_priority, hashValue);
                        }

                    }
                    else
                    {
                        continue;
                    }
                }

            }
            if (hash != null)
            {
                Iterator<Object[]> values = hash.values().iterator();
                try
                {
                    while (values.hasNext())
                    {
                        hashValue = (String[])values.next();
                        pos_pta_tmpl_entry_id = hashValue[0];
                        new_priority = hashValue[1];
                        old_priority = hashValue[2];
                        if (!new_priority.equals(old_priority))
                        {
                            sqlParams = new Object[] {new Integer(new_priority), new Integer(pos_pta_tmpl_entry_id)};
                            DataLayerMgr.executeUpdate("UPDATE_TEMPLATE_ENTRY_PRIORITY", sqlParams, true);

                        }
                    }
                }
                catch (SQLException e)
                {
                    throw new ServletException(e);
                }
                catch (DataLayerException e)
                {
                    throw new ServletException(e);
                }
            }
        }
        else if (action.equalsIgnoreCase("Add New"))
        {
        	String payment_action_type_cd = form.getString(DevicesConstants.PARAM_PAYMENT_ACTION_TYPE_CD, false);
            if (StringHelper.isBlank(payment_action_type_cd))
            {
                errorMessage = "Required Parameter Not Found: payment_action_type_cd";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            String payment_entry_method_cd = form.getString(DevicesConstants.PARAM_PAYMENT_ENTRY_METHOD_CD, false);
            if (payment_entry_method_cd == null || payment_entry_method_cd.length() == 0)
            {
                errorMessage = "Required Parameter Not Found: payment_entry_method_cd";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            try
            {
            	String payment_action_type_desc = null;
                Results paymentActionType = DataLayerMgr.executeQuery("GET_PAYMENT_ACTION_TYPE", new Object[] {payment_action_type_cd}, true);
                if (paymentActionType.next())
                	payment_action_type_desc = paymentActionType.getFormattedValue("payment_action_type_desc");
                request.setAttribute("payment_action_type_desc", payment_action_type_desc);
                
                String payment_entry_method_desc = null;
                Results entryMethod = DataLayerMgr.executeQuery("GET_PAYMENT_ENTRY_METHOD", new Object[] {payment_entry_method_cd}, true);
                if (entryMethod.next())
                    payment_entry_method_desc = entryMethod.getFormattedValue(1);
                request.setAttribute("payment_entry_method_desc", payment_entry_method_desc);
                
                Results payment_subtypes = DataLayerMgr.executeQuery("GET_AVAILABLE_SUBTYPE_BY_METHOD_CD", new Object[] {payment_action_type_cd, payment_entry_method_cd}, true);
                if (!payment_subtypes.next())
                {
                    errorMessage = "No payment types available to be added at this time.";
                    request.setAttribute("errorMessage", errorMessage);
                    return;
                }
                payment_subtypes.setRow(0);
                request.setAttribute("payment_subtypes", payment_subtypes);                
            }
            catch (SQLException e)
            {
                throw new ServletException(e);
            }
            catch (DataLayerException e)
            {
                throw new ServletException(e);
            }
        }
        else if (action.equalsIgnoreCase("Edit"))
        {
            int pos_pta_tmpl_entry_id = form.getInt(DevicesConstants.PARAM_POS_PTA_TMPL_ENTRY_ID, false, -1);
            if (pos_pta_tmpl_entry_id < 0)
            {
                errorMessage = "Required Parameter Not Found: pos_pta_tmpl_entry_id";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
        }
        else if (action.equalsIgnoreCase("Edit Template"))
        {
           return;
        }
        else if (action.equalsIgnoreCase("Clone"))
        {
           return;
        }
        else if (action.equalsIgnoreCase("Save Clone"))
        {	
        	String name = form.getString("name", false);
        	String desc = form.getString("desc", false);
        	Map<String, Object> pos_pta_tmpl_params = new HashMap<String, Object>();
        	pos_pta_tmpl_params.put("name", name);
        	pos_pta_tmpl_params.put("desc", desc);
        	try{
        		
        		int new_pos_pta_tmpl_id = -1;
				
				Object[] result = null;
	    		result = DataLayerMgr.executeCall("INSERT_POS_PTA_TMPL", pos_pta_tmpl_params, true);
				if(result!=null && ((BigDecimal)result[1]).intValue() > 0){
					new_pos_pta_tmpl_id = ((BigDecimal)result[1]).intValue();
				}else{
					errorMessage = "Unable to create new pos pta template for clone template name=" + name;
	                request.setAttribute("errorMessage", errorMessage);
	                return;
				}
        		
        		Results tmplEntries = DataLayerMgr.executeQuery("GET_POS_PTA_TMPL_ENTRIES", new Object[] {pos_pta_tmpl_id}, true);
        		while(tmplEntries.next()){   
        			Map<String, Object> params = new HashMap<String, Object>();
        			params.put("new_pos_pta_tmpl_id", new_pos_pta_tmpl_id);
        			params.put("pos_pta_tmpl_id", pos_pta_tmpl_id);
        			params.put("pos_pta_tmpl_entry_id", tmplEntries.get("pos_pta_tmpl_entry_id"));
        			result = null;
        			result = DataLayerMgr.executeCall("CLONE_POS_PTA_TMPL_ENTRY", params, true);
        			if(result!=null && ((Integer)result[0]).intValue() > 0){
    					//no-op, successful insert
    				}else{
						errorMessage = "Unable to added template entry for: pos_pta_tmpl_id=" + new_pos_pta_tmpl_id + ", pos_pta_tmpl_entry_id=" + tmplEntries.get("pos_pta_tmpl_entry_id");
		                request.setAttribute("errorMessage", errorMessage);
		                return;
					}
                            
        		}
        	}catch (SQLException e)
            {
                throw new ServletException(e);
            }
            catch (DataLayerException e)
            {
                throw new ServletException(e);
            }
        }
        else if (action.equalsIgnoreCase("Save"))
        {	
        	String name = form.getString("name", false);
        	String desc = form.getString("desc", false);
        	Map<String, Object> pos_pta_tmpl_params = new HashMap<String, Object>();
        	pos_pta_tmpl_params.put("name", name);
        	pos_pta_tmpl_params.put("desc", desc);
        	try{
        		Results nameResults = DataLayerMgr.executeQuery("GET_POS_PTA_TMPL_BY_POS_PTA_TMPL_NAME", new Object[] {name}, true);
        		String existName = null;
        		if (nameResults.next())
                {
        			existName = nameResults.getFormattedValue(2);
                }
        		if(!StringHelper.isBlank(existName)){
        			errorMessage = ("Template Name \"" + name + "\" Already Exists!  Please choose another name.");
	                request.setAttribute("errorMessage", errorMessage);
	                return;
        		}
				
				Object[] result = null;
	    		result = DataLayerMgr.executeCall("INSERT_POS_PTA_TMPL", pos_pta_tmpl_params, true);
				if(result!=null && ((BigDecimal)result[1]).intValue() > 0){
					//int new_pos_pta_tmpl_id = ((BigDecimal)result[1]).intValue();
				}else{
					errorMessage = "Unable to create new pos pta template for clone template name=" + name;
	                request.setAttribute("errorMessage", errorMessage);
	                return;
				}
        	}catch (SQLException e)
            {
                throw new ServletException(e);
            }
            catch (DataLayerException e)
            {
                throw new ServletException(e);
            }
        }
        else if (action.equalsIgnoreCase("Update"))
        {
            String name = request.getParameter("name");
            String desc = request.getParameter("desc");
            if (StringHelper.isBlank(name))
            {
                errorMessage = "Required Parameter Not Found: name";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            if (StringHelper.isBlank(desc))
            {
                errorMessage = "Required Parameter Not Found: desc";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            try
            {
            	templateData.setRow(0);
                DataLayerMgr.executeUpdate("UPDATE_PTA_TEMPLATE", new Object[] {name.trim(), desc.trim(), pos_pta_tmpl_id, name.trim(), pos_pta_tmpl_id}, true);
                templateData = DataLayerMgr.executeQuery("GET_PTA_TEMPLATE_DATA", new Object[] {pos_pta_tmpl_id});
                request.setAttribute("templateData", templateData);
            }
            catch (SQLException e)
            {
                throw new ServletException(e);
            }
            catch (DataLayerException e)
            {
                throw new ServletException(e);
            }
        }
        else if (action.equalsIgnoreCase("Delete Template"))
        {
        	try {
        		DataLayerMgr.executeUpdate("DELETE_PTA_TEMPLATE", new Object[] {pos_pta_tmpl_id}, true);
        		form.setRedirectUri("/templateMenu.i?msg=Payment+template+deleted");
        	} catch (Exception e) {
        		throw new ServletException(e);
        	}
        }
        else
        {
            errorMessage = "Unknown Action Parameter: " + action;
            request.setAttribute("errorMessage", errorMessage);
            return;
        }

    }

}
