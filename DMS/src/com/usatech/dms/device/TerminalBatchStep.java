/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class TerminalBatchStep extends AbstractStep
{

    // by default, sort by "tran_id" descending
    private static final String DEFAULT_SORT_INDEX = "-2";
    private static final String SQL_START = 
    		(!DialectResolver.isOracle() 
    		? " with auth as (SELECT * from pss.auth where auth.terminal_batch_id = ?::numeric) SELECT auth.auth_id," 
    		: " SELECT auth.auth_id,") +
            "auth.tran_id," +
            "auth_type.auth_type_desc," +
            "auth_state.auth_state_name," +
            "auth.auth_parsed_acct_data, " +
            "acct_entry_method.acct_entry_method_desc," +
            "auth.auth_amt," +
            "to_char(auth.auth_ts, 'MM/DD/YYYY HH24:MI:SS')," +
            "auth.auth_result_cd, " +
            "auth.auth_resp_cd," +
            "auth.auth_resp_desc," +
            "auth.auth_authority_tran_cd," +
            "auth.auth_authority_ref_cd, " +
            "to_char(auth.auth_authority_ts, 'MM/DD/YYYY HH24:MI:SS')," +
            "to_char(auth.created_ts, 'MM/DD/YYYY HH24:MI:SS'), " +
            "to_char(auth.last_updated_ts, 'MM/DD/YYYY HH24:MI:SS')," +
            "auth.terminal_batch_id," +
            "auth.auth_amt_approved, " +
            "auth.auth_authority_misc_data," +
            "tran_state.tran_state_desc ";
    
    private static final String SQL_BASE = 
    		!DialectResolver.isOracle() 
    		? 
    		"FROM auth, pss.auth_type, pss.auth_state, pss.acct_entry_method, pss.tran, pss.tran_state " +
            "WHERE auth.auth_type_cd = auth_type.auth_type_cd " +
            "AND auth.auth_state_id = auth_state.auth_state_id " +
            "AND auth.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd " +
            "AND auth.tran_id = tran.tran_id " +
            "AND tran.tran_state_cd = tran_state.tran_state_cd "
            :
            "FROM pss.auth, pss.auth_type, pss.auth_state, pss.acct_entry_method, pss.tran, pss.tran_state " +
            "WHERE auth.auth_type_cd = auth_type.auth_type_cd " +
            "AND auth.auth_state_id = auth_state.auth_state_id " +
            "AND auth.acct_entry_method_cd = acct_entry_method.acct_entry_method_cd " +
            "AND auth.tran_id = tran.tran_id " +
            "AND tran.tran_state_cd = tran_state.tran_state_cd " +
            "AND auth.terminal_batch_id = ? ";
    
    private static final String[] SORT_FIELDS = {"auth.auth_id", // Detail ID
        "auth.tran_id", // Tran ID
        "auth_type.auth_type_desc", // Type
        "auth_state.auth_state_name", // Detail State
        "tran_state.tran_state_desc", // Transaction State
        "auth.auth_amt", // Amount
        "COALESCE(auth.auth_ts, TO_DATE('1970', 'YYYY'))", // Timestamp
        };

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String terminal_batch_id = form.getString(DevicesConstants.PARAM_TERMINAL_BATCH_ID, false);
        if (terminal_batch_id == null || terminal_batch_id.length() == 0)
        {
            request.setAttribute("missingParam", true);
            return;
        }
        String sql = SQL_BASE;// + terminal_batch_id;
        String paramTotalCount = PaginationUtil.getTotalField(null);
        String paramPageIndex = PaginationUtil.getIndexField(null);
        String paramPageSize = PaginationUtil.getSizeField(null);
        String paramSortIndex = PaginationUtil.getSortField(null);

        try
        {
            // pagination parameters
            int totalCount = form.getInt(paramTotalCount, false, -1);
            if (totalCount == -1)
            {
                // if the total count is not retrieved yet, get it now
				Results total = DataLayerMgr.executeSQL("OPER",
						(!DialectResolver.isOracle()
								? "with auth as (SELECT * from pss.auth where auth.terminal_batch_id = ?::numeric) SELECT COUNT(1) "
								: "SELECT COUNT(1) ") + sql,
						new Object[] { terminal_batch_id }, null);
                if (total.next())
                {
                    totalCount = total.getValue(1, int.class);
                }
                else
                {
                    totalCount = 0;
                }
                request.setAttribute(paramTotalCount, String.valueOf(totalCount));
            }

            int pageIndex = form.getInt(paramPageIndex, false, 1);
            int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
            int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
            int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

            String sortIndex = form.getString(paramSortIndex, false);
            sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
            String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

			String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ sql + orderBy + " LIMIT ?::bigint) pagination_temp)sq_end where rnum  >= ?::bigint ";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + sql + orderBy
						+ ") pagination_temp where ROWNUM <= ? ) where rnum  >= ? ";
			}

            Results results = DataLayerMgr.executeSQL("OPER", query, new Object[]{terminal_batch_id, maxRowToFetch, minRowToFetch}, null);
            request.setAttribute("salesinfo", results);
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }

}
