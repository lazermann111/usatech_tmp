/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

public enum EsudsCycleCode
{

    CodeZero("No Status Available", "0"), 
    CodeOne("Idle, Available", "1"), 
    CodeTwo("In 1st Cycle", "2"), 
    CodeThree("Out Of Service", "3"), 
    CodeFour("Nothing on Port", "4"), 
    CodeFive("Idle, Not Available", "5"), 
    CodeSix("Manual Service Mode", "6"), 
    CodeSeven("In 2nd Cycle", "7"), 
    CodeEight("Transaction In Progress", "8");

    private String label;
    private String value;

    private EsudsCycleCode(String label, String value)
    {
        this.label = label;
        this.value = value;
    }

    /**
     * Gets the EsudsCycleCode whose label matches the given string. The label is not case
     * sensitive.
     * @param label the given label
     * @return <code>null</code> if not found
     */
    public static EsudsCycleCode forLabel(String label)
    {
        EsudsCycleCode result = null;
        for (EsudsCycleCode item : EsudsCycleCode.values())
        {
            if (item.getLabel().equalsIgnoreCase(label))
            {
                result = item;
                break;
            }
        }
        return result;
    }

    /**
     * Gets the EsudsCycleCode whose value matches the given value.
     * @param value the given value
     * @return <code>null</code> if not found
     */
    public static EsudsCycleCode forValue(String value)
    {
        EsudsCycleCode result = null;
        for (EsudsCycleCode item : EsudsCycleCode.values())
        {
            if (item.getValue().equals(value))
            {
                result = item;
                break;
            }
        }
        return result;
    }

    /**
     * @return the label of this code
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * @return the value of this code
     */
    public String getValue()
    {
        return value;
    }

}
