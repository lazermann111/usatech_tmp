/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.istack.internal.NotNull;
import com.usatech.dms.action.ConfigFileActions;
import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.action.FileActions;
import com.usatech.dms.action.PendingCmdActions;
import com.usatech.dms.device.modem.AsyncDmsModemService;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.DMSSecurityUtil;
import com.usatech.dms.util.DMSValuesList;
import com.usatech.dms.util.Helper;
import com.usatech.dms.util.ValuesListLoader;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.modem.USATDeviceDao;
import simple.modem.USATDeviceDaoImpl;
import simple.modem.USATModemService;
import simple.results.Results;
import simple.service.modem.service.dto.USATDevice;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

/**
 * This step will process all of the data modifications selected by the user via the bulk config wizard for edge devices.
 */
public class BulkConfigWizard8Step extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	
    	StringBuilder out = new StringBuilder();
    	String action = form.getString("action", false);
    	if(!"Finish >".equalsIgnoreCase(action)){
             request.setAttribute("errorMessage", "<br/><br/><b><font color='red'>Undefined Action! *" + action + "* </font></b><br/><br/>");
             return;
    	}
    	
    	String includeDeviceIds = form.getString("include_device_ids", false);
    	if(StringHelper.isBlank(includeDeviceIds)){
            request.setAttribute("errorMessage", "<br/><br/><b><font color='red'>No devices selected!</font></b><br/><br/>");
            return;
    	}
    	
    	log.info("Bulk device configuration is starting...");
    	
		int selectionDeviceType = ConvertUtils.getIntSafely(form.getString("device_type_id", false), 13);
    	request.setAttribute("edge_device_type", new Integer(DeviceType.EDGE.getValue()));
    	String customerId = form.getString("customer_id", false);
    	String locationId = form.getString("location_id", false);
    	String parentLocationId = form.getString("parent_location_id", false);
    	String zeroCounters = form.getString("zero_counters", false);
    	boolean resetCounters = "1".equalsIgnoreCase(zeroCounters);
    	long posPtaTmplId = form.getLong("pos_pta_tmpl_id", false, -1);
    	String modeCd = form.getString("mode_cd", false);
    	String orderCd = form.getString("order_cd", false);
    	String setTerminalCdToSerial = form.getStringSafely("set_terminal_cd_to_serial", "N");
    	String onlyNoTwoTierPricing = form.getStringSafely("only_no_two_tier_pricing", "N");
    	boolean debug = "1".equalsIgnoreCase(form.getStringSafely("debug", "0"));
		int error_count = 0;
    	int no_change_count = 0;
    	int change_count = 0;
    	int import_error_count = 0;
    	int import_count = 0;
    	int custloc_error_count = 0;
    	int custloc_update_count = 0;
    	int parentloc_update_count = 0;
    	int parentloc_error_count = 0;
    	int file_upload_count = 0;
    	int file_upload_error_count = 0;
    	int file_download_count = 0;
    	int file_download_error_count = 0;
    	int eft_count = 0;
    	int eft_error_count = 0;
    	int s2c_request_count = 0;
    	int s2c_request_error_count = 0;
    	int firmware_upgrade_count = 0;
    	int firmware_upgrade_error_count = 0;
    	int ec_credential_count = 0;
    	int ec_credential_error_count = 0;
		
		Results rs = null;    	
		
		if(debug)
			out.append("<center><font color=\"red\"><b>DEBUG IS ON!  CHANGES WILL NOT BE SAVED!</b></font></center><br/>");
		
		out.append("\nSelected Device IDs: ").append(includeDeviceIds.replace(",", ", ")).append("\n");
		if (!StringHelper.isBlank(customerId))
			out.append("Selected Customer ID: ").append(customerId).append("\n");
		if (!StringHelper.isBlank(locationId))
			out.append("Selected Location ID: ").append(locationId).append("\n");
		if (!StringHelper.isBlank(parentLocationId))
			out.append("Selected Parent Location ID: ").append(parentLocationId).append("\n");
		out.append("\n\n");
		
		String[] parametersStr = form.getStringArray("params_to_change", false);
    	String parametersMapStr = form.getString("params_to_change_request", false);
		HashMap<String, Object> paramMap = StringHelper.decodeRequestMap(parametersMapStr);
		List<String> paramList = Arrays.asList(parametersStr);
		boolean updateConfig = paramList.size() > 0;
		
		String file_uploads = form.getStringSafely("file_uploads", "");
		String file_upload_fields = form.getStringSafely("file_upload_fields", "");
    	boolean processFileUploads = !StringHelper.isBlank(file_uploads) && !StringHelper.isBlank(file_upload_fields);
    	String[] fileUploadArray = processFileUploads ? file_uploads.split(", ", -1) : null;
		String[] fileUploadFieldArray = processFileUploads ? file_upload_fields.split(", ", -1) : null;
    	
    	String eft = form.getStringSafely("eft", "");
    	boolean processEFT = !StringHelper.isBlank(eft);
    	
    	String file_downloads = form.getStringSafely("file_downloads", "");
		String file_download_fields = form.getStringSafely("file_download_fields", "");
    	boolean processFileDownloads = !StringHelper.isBlank(file_downloads) && !StringHelper.isBlank(file_download_fields);
    	String[] fileDownloadArray = processFileDownloads ? file_downloads.split(", ", -1) : null;
		String[] fileDownloadFieldArray = processFileDownloads ? file_download_fields.split(", ", -1) : null;
		
		String s2c_request_fields = form.getStringSafely("s2c_request_fields", "");
    	boolean processS2CRequests = !StringHelper.isBlank(s2c_request_fields);
    	String[] s2cRequestFieldArray = processS2CRequests ? s2c_request_fields.split(", ", -1) : null;
		DMSValuesList s2cActionDescList = processS2CRequests ? ValuesListLoader.getValuesList(request, DMSConstants.GLOBAL_LIST_EDGE_GENERIC_RESPONSE_S2C_ACTIONS) : null;
		
		String firmware_upgrades = form.getStringSafely("firmware_upgrades", "");
		boolean processFirmwareUpgrades = !StringHelper.isBlank(firmware_upgrades);
		String[] firmwareUpgradeArray = processFirmwareUpgrades ? firmware_upgrades.split(", ", -1) : null;
		
		long ec_credential_id = form.getLong("ec_credential_id", false, -1);
		String ec_credential_name = form.getStringSafely("ec_credential_name", "");
		
		BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
		String userName = user.getUserName();
		
		// set the initial value for the property list version
		int propertyListVersion = -1;
		String deviceName;
		Object[] result = null;
        Connection conn = null;
        int parentLocationCount = 0;
        boolean success = false;
    	try{ 
    		conn = DataLayerMgr.getConnection("OPER");
    		
    		// create a map for the default config for the property list version
    		LinkedHashMap<String, ConfigTemplateSetting> defaultConfigMap = null;
			if(updateConfig && selectionDeviceType < DeviceType.EDGE.getValue() && selectionDeviceType != DeviceType.KIOSK.getValue()) {
    			try {
					defaultConfigMap = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(selectionDeviceType, -1, null, conn);
    			} catch (Exception e) {
    				throw new ServletException("Error getting default config data", e);
    			}
    			if(defaultConfigMap.size() == 0)
    				throw new ServletException("Error getting default config data");
    		}
    		
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();
    		rs = DataLayerMgr.executeQuery(conn, "GET_BULK_PLV_DEVICE_INFO", new Object[]{includeDeviceIds});
			List<Long> deviceIds = new ArrayList<>();
			List<String> oldValues = new ArrayList<>();
			List<String> newValues = new ArrayList<>();
	        while(rs.next()) {
	        	boolean deviceSuccess = false;
	        	long startTsMs = System.currentTimeMillis();	        	
	        	DEVICE: while(true) {
		        	long deviceId = ConvertUtils.getLong(rs.get("device_id"));
		        	deviceName = ConvertUtils.getString(rs.get("device_name"), true);		        	
					int deviceType = ConvertUtils.getInt(rs.get("device_type_id"));
					boolean isGx = deviceType == DeviceType.GX.getValue() || deviceType == DeviceType.G4.getValue();
					boolean isMEI = deviceType == DeviceType.MEI.getValue();
					String restrictSettingsUpdate = ConvertUtils.getString(rs.get("restrict_settings_update"), false);
		        	
		        	/* *****************************
		        	 * Process Cust Loc Changes, create return msg 
		        	 * 
		        	 * *****************************/
			    	if(!StringHelper.isBlank(customerId) || !StringHelper.isBlank(locationId) || !StringHelper.isBlank(parentLocationId)){
			    		out.append("Updating Customer and/or Location for ").append(deviceName).append("\n");
		    			if(!StringHelper.isBlank(customerId) && !StringHelper.isBlank(locationId))
		    	        {
		    				out.append("CUSTOMER and LOCATION changed, ").append("customer_id = ").append(customerId).append(", location_id = ").append(locationId);
		    	        }
		    			else if(!StringHelper.isBlank(customerId) && !StringHelper.isBlank(parentLocationId))
		    	        {
		    				out.append("CUSTOMER and PARENT LOCATION changed, ").append("customer_id = ").append(customerId).append(", parent_location_id = ").append(parentLocationId);
		    	        }
		    			else if(!StringHelper.isBlank(customerId))
		    	        {
		    				out.append("ONLY CUSTOMER changed, customer_id = ").append(customerId);
		    	        }
		    			else if(!StringHelper.isBlank(locationId))
		    	        {
		    				out.append("ONLY LOCATION changed, location_id = ").append(locationId);
		    	        }
		    			else if(!StringHelper.isBlank(parentLocationId))
		    	        {
		    				out.append("ONLY PARENT LOCATION changed, parent_location_id = ").append(parentLocationId);
		    	        }
		    			out.append("\n");
		    			
		    			if(!debug){
			    			
			    	        try
			    	        {	
			    	        	/*
			    	        	 * Attempt the update on the customer and location stored procedures. 
			    	        	 */
			    	        	String dbCallName = null;
			    	        	Object[] params = null;
			    	        	if(!StringHelper.isBlank(customerId) && !StringHelper.isBlank(locationId)){
			    	        		dbCallName = "UPDATE_LOC_ID_CUST_ID";
			    	        		params =  new Object[] {deviceId, locationId, customerId, 20, 20, 20, "2000"};
			    	        		
			    	        	}else if(!StringHelper.isBlank(customerId)){
			    	        		dbCallName = "UPDATE_CUST_ID";
			    	        		params = new Object[] {deviceId, customerId, 20, 20, 20, "2000"};
			    	        	}else if(!StringHelper.isBlank(locationId)){
			    	        		dbCallName = "UPDATE_LOC_ID";
			    	        		params = new Object[] {deviceId, locationId, 20, 20, 20, "2000"};
			    	        	}
			    	        	
			    	        	if(params !=null){
				    	        	result = DataLayerMgr.executeCall(conn, dbCallName, params);
				    	        	
				    	        	long newDeviceId = ConvertUtils.getLongSafely(result[1], -1);
				    	        	if(newDeviceId > 0){
				    	        		deviceId = newDeviceId;
				    	        		custloc_update_count++;
				    	        	}else{
				    	        		custloc_error_count++;
				    	        		out.append("<font color=\"red\"><b>Error changing customer ").append(customerId).append( " or location ").append(locationId).append("</b></font>\n");
				    	        		log.error(new StringBuilder("Error changing customer ").append(customerId).append( " or location ").append(locationId).append(" for deviceId ").append(deviceId));
				    	        		break DEVICE;
				    	        	}
			    	        	}
			    	        	/*
			    	        	 * Attempt the update on the parent location (is a sql update, not stored procedure). 
			    	        	 * If parent location exists on the request, but cannot be updated, lets warn.
			    	        	 */
			    	        	if(!StringHelper.isBlank(parentLocationId)){
			    	        		parentLocationCount = DataLayerMgr.executeUpdate(conn, "UPDATE_PARENT_LOC_ID", new Object[] {parentLocationId, deviceId}, false);
			    	        		if(parentLocationCount > 0){			    	        			
			    	        			parentloc_update_count++;
			    	        		}else{
			    	        			parentloc_error_count++;
			    	        			Results r = DataLayerMgr.executeQuery("GET_LOCATION", new Object[] {deviceId}, false);
			    	        			String locationName = "";
			    	        			if(r!= null && r.next()){
			    	        				locationName = (String)r.get("location_name");
			    	        			}
			    	        			out.append("<font color=\"red\"><b>Error updating parent location of location ").append(locationName).append(" for deviceId: ").append(deviceId).append(", parentLocationId: ").append(parentLocationId).append("</b></font>\n");
			    	        			log.error(new StringBuilder("Cannot update parent location of location ").append(locationName).append(" for deviceId: ").append(deviceId).append(", parentLocationId: ").append(parentLocationId));
			    	        			break DEVICE;
			    	        		}
			    	        	}
		    	            }
		    	            catch (Exception e){		    	            	
		    	            	out.append("<font color=\"red\"><b>Error updating customer or location for device ").append(deviceId).append(": ").append(e.getMessage()).append("</b></font>\n");
		    	            	log.error(new StringBuilder("Error updating customer or location for device ").append(deviceId), e);
		    	            	custloc_error_count++;
		    	            	break DEVICE;
		    	            }
		    			}else{
		    				out.append("Success, debug mode is enabled\n");
		    			}
		    			
			    	}else{
			    		out.append("No Customer or Location changes for ").append(deviceName).append("\n");
			    	}
			    	
			    	/* *****************************
		        	 * Process Payment Template Changes, create return msg 
		        	 * *****************************/
					int returnCd = 0;
			    	if(posPtaTmplId > 0 && !StringHelper.isBlank(modeCd)){
		    			if(!debug){
		    				try{
		    					// method should return 1 on success, raise exception otherwise
								returnCd = DeviceActions.importPosPtaTemplate(deviceId, posPtaTmplId, modeCd, orderCd, setTerminalCdToSerial, onlyNoTwoTierPricing, conn);
		    				}catch(ServletException e){
		    					out.append("<font color=\"red\"><b>Error importing payment template ").append(posPtaTmplId).append(" into device ").append(deviceName).append(" in mode ").append(modeCd).append(": ").append(e.getMessage()).append("</b></font>\n");
		    					log.error(new StringBuilder("Error importing payment template, deviceId: ").append(deviceId).append(", posPtaTmplId: ").append(posPtaTmplId).append(", modeCd: ").append(modeCd), e);
		    					import_error_count++;
		    					break DEVICE;
		    				}
		    				if(returnCd==1){
		    					import_count++;
		    					out.append("Imported payment template ").append(posPtaTmplId).append(" into device ").append(deviceName).append("\n");
		    				}else{
	    	        			out.append("<font color=\"red\"><b>Error importing payment template ").append(posPtaTmplId).append(" into device ").append(deviceName).append(", returnCd: ").append(returnCd).append("</b></font>\n");
	    	        			log.error(new StringBuilder("Error importing payment template ").append(posPtaTmplId).append(" into device ").append(deviceName).append(", returnCd: ").append(returnCd));
	    	        			import_error_count++;
	    	        			break DEVICE;
		    				}
		    			}
		    			else
		    				import_count++;
			    	}
			    	
			    	if (debug)
			    		change_count++;
			    	else if(resetCounters && (DeviceType.GX.getValue() == deviceType || DeviceType.G4.getValue() == deviceType)){
			    		for (int offset = 320; offset <= 352; offset += 4) {
							try{
				                result = DataLayerMgr.executeCall(conn, "SP_UPDATE_CONFIG_AND_RETURN", new Object[] {deviceId, offset, "00000000", "H", deviceType, 0, 0, "", "", "", ""});
							}catch(Exception e){					
								out.append("<font color=\"red\"><b>Error Zeroing Out Counters for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");
								log.error(new StringBuilder("Error Zeroing Out Counters for device ").append(deviceName), e);
					            error_count++;
								break DEVICE;
							}
			    			
							int returnValue = ConvertUtils.getIntSafely(result[1], -1);
							if(returnValue < 0) {
								out.append("<font color=\"red\"><b>Error Zeroing Out Counters for device ").append(deviceName).append("</b></font>\n");
								log.error(new StringBuilder("Error Zeroing Out Counters for device ").append(deviceName));
					            error_count++;
								break DEVICE;
							}else if(returnValue == 0) {
								no_change_count++;
								out.append("Counter at offset ").append(offset).append(" was already zeroed out for ").append(deviceName).append("\n");
							} else {							
								change_count++;
								out.append("Zeroed out counter at offset ").append(offset).append(" for ").append(deviceName).append("\n");
							}
			    		}
			    	}
			    	
			    	/* *****************************
		        	 * Process Config Parameter Changes 
		        	 * *****************************/			    	
			    	if(updateConfig){
			    		if (debug)
			    			change_count++;
			    		else if ("Y".equals(restrictSettingsUpdate)){
			    			out.append("Configuration Changes are restricted for device ").append(deviceName).append(": \n");
			    		} else {
			    			LinkedHashMap<String, String> deviceConfigMap = null;
				    		String deviceConfigFileName = DeviceActions.getDeviceConfigName(deviceName);
				    		StringBuilder changesForDevice = new StringBuilder();
				    		boolean displayChangesInline = false;
				    		int callInTimeChangeCount = 0;
				        	int callInTimeWindowChangeCount = 0;
				        	
				        	if (deviceType == DeviceType.EDGE.getValue()) {
				        		//following block is used to detect the changes of PLV ( Property List Value )
					    		int currentDevicePropListVer = ConvertUtils.getIntSafely(rs.get("device_setting_value"), 0);
					    		if(propertyListVersion != currentDevicePropListVer){
					    			propertyListVersion = currentDevicePropListVer;
					    			try{			    				
						    			defaultConfigMap = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceType, propertyListVersion);				    							    			
						    			if(defaultConfigMap.size() == 0){
						    				out.append("<font color=\"red\"><b>Error getting default config data for device ").append(deviceName).append("</b></font>\n");
						    				log.error(new StringBuilder("Error getting default config data for device ").append(deviceName));
						    				error_count++;
						    				break DEVICE;
						    			}
					    			}catch(Exception e){
					    				out.append("<font color=\"red\"><b>Error parsing default config data for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");
					    				log.error(new StringBuilder("Error parsing default config data for device ").append(deviceName), e);
					    				error_count++;
					    				break DEVICE;
					    			}
					    		}
				        	}
				        	
				        	if (deviceType != DeviceType.KIOSK.getValue()) {
				    			try{
					    			deviceConfigMap = ConfigFileActions.getDeviceSettingData(defaultConfigMap, deviceId, deviceType, true, conn);
					    			if(deviceConfigMap.size() == 0){
					    				out.append("<font color=\"red\"><b>Error getting device config data for device ").append(deviceName).append("</b></font>\n");			    				
					    				log.error(new StringBuilder("Error getting device config data for device ").append(deviceName));
					    				error_count++;
					    				break DEVICE;
					    			}
				    			}catch(Exception e){
				    				out.append("<font color=\"red\"><b>Error parsing device config data for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");			    				
				    				log.error(new StringBuilder("Error parsing device config data for device ").append(deviceName), e);
				    				error_count++;
				    				break DEVICE;
				    			}
				        	}
			    			
				    		if (deviceType == DeviceType.EDGE.getValue()) {
					    		StringBuilder newIniFileShort = new StringBuilder();
					    		for (String userIndex: paramList) {
					    			if (StringHelper.isBlank(userIndex) || userIndex.startsWith("sch_"))
					    				continue;
					    			ConfigTemplateSetting defaultSetting = defaultConfigMap.get(userIndex);
					    			String defaultConfigVal = null;
					    			String fieldLabel;
					    			boolean isServerOnly = false;
					    			boolean propertySupported = false;
					    			boolean storedInPennies = false;
					    			if (defaultSetting != null) {
					    				defaultConfigVal = defaultSetting.getConfigTemplateSettingValue();
					    				fieldLabel = defaultSetting.getLabel();
					    				isServerOnly = defaultSetting.isServerOnly();
					    				propertySupported = defaultSetting.isSupported();
					    				storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
					    			} else
					    				fieldLabel = userIndex;
					    			if (defaultConfigVal == null)
					    				defaultConfigVal = "";
					    			String userValue = ConvertUtils.getStringSafely(paramMap.get(new StringBuilder("cfg_field_").append(userIndex).toString()), "");
					    			if (storedInPennies && StringHelper.isNumeric(userValue))
					    				userValue = String.valueOf((new BigDecimal(userValue).multiply(BigDecimal.valueOf(100)).intValue()));
					    			if ("BITMAP".equalsIgnoreCase(defaultSetting.getEditorType())) {
					    				int intParamValue = 0;
					    				if (!StringHelper.isBlank(userValue)) {
					    					String[] options = userValue.split(",", -1);
					    					for (String option: options)
					    						intParamValue += Integer.valueOf(option);
					    				}
					    				userValue = String.valueOf(intParamValue);
					    			} else if ("SCHEDULE".equalsIgnoreCase(defaultSetting.getEditorType()))
					    				userValue = WebHelper.buildSchedule(request, paramMap, defaultSetting, userIndex, userValue, rs.getValue("device_utc_offset_min", int.class));
					    			String deviceConfigVal = deviceConfigMap.get(userIndex);
			    					if (deviceConfigVal == null)
			    						deviceConfigVal = "";
					    			if (log.isDebugEnabled())
					    				log.debug(new StringBuilder("Updating device configuration for ").append(deviceName).append(": ").append(userIndex).append("|").append(userValue).append("|").append(defaultConfigVal).append("|").append(deviceConfigVal));
				    				//see if param name exists and not blank, and the current device config value equals the incoming user value
					    			if(StringHelper.equalConfigValues(deviceConfigVal, userValue, storedInPennies))
					    				no_change_count++;
					    			else if (isServerOnly || !Helper.isInteger(userIndex)) {		    							
				    					if (userIndex.startsWith("CALL_IN_TIME_WINDOW_"))
				    						callInTimeWindowChangeCount++;
				    					changesForDevice.append(fieldLabel).append(" [").append(userIndex).append("]: ").append(userValue);
				    					if(StringHelper.equalConfigValues(userValue, defaultConfigVal, storedInPennies))
				    						changesForDevice.append(" (default)");
				    					changesForDevice.append(", old: ").append(deviceConfigVal).append("\n");
										if (isDeviceModemStatusField(userIndex)) {
											deviceIds.add(deviceId);
											oldValues.add(deviceConfigVal);
											newValues.add(userValue);
										}
				    					DeviceConfigurationUtils.upsertDeviceSetting(deviceId, userIndex, userValue, conn, userName);
				    					change_count++;
					    			} else {
				    					// change this check to see if the default config actually contains the key
				    					if(propertySupported){
				    						//if a DNS host name or IP address doesn't have two dots, use the default value
				    						if (DeviceUtils.EDGE_COMM_SETTING_EXPRESSION.matcher(userIndex).matches() && !DeviceUtils.TWO_DOT_EXPRESSION.matcher(userValue).find())
				    							userValue = defaultConfigVal;
				    						changesForDevice.append(fieldLabel).append(" [").append(userIndex).append("]: ").append(userValue);
				    						//if default val = incoming user value
				    						if(StringHelper.equalConfigValues(defaultConfigVal, userValue, storedInPennies)){
				    							newIniFileShort.append(userIndex).append("!\n");
				    							changesForDevice.append(" (default)");
				    						}else {
												newIniFileShort.append(userIndex).append("=").append(userValue).append("\n");
												changesForDevice.append(", old: ").append(deviceConfigVal).append("\n");
												if (isDeviceModemStatusField(userIndex)) {
													deviceIds.add(deviceId);
													oldValues.add(deviceConfigVal);
													newValues.add(userValue);
												}
											}
				    						
				    						if ("SCHEDULE".equalsIgnoreCase(defaultSetting.getEditorType()))
				    							callInTimeChangeCount++;
				    						
				    						//change device value, this will be used to create new device configuration file;
					    					deviceConfigMap.put(userIndex, userValue);
					    					
					    					DeviceConfigurationUtils.upsertDeviceSetting(deviceId, userIndex, userValue, conn, userName);
					    					change_count++;
				    					}else {
				    						changesForDevice.append(fieldLabel).append(" [").append(userIndex).append("] IS NOT SUPPORTED\n");
				    						no_change_count++;
				    					}
				    				}
				    			}
				    			
					    		if(newIniFileShort.length() > 0){
						    		StringBuilder newDeviceConfigFile = new StringBuilder();
						    		List<String> deviceConfigList = new ArrayList<String>(deviceConfigMap.keySet());
						    		for (String key: deviceConfigList) {			    		
						    			boolean isServerOnly = false;
						    			ConfigTemplateSetting defaultSetting = defaultConfigMap.get(key);
						    			if (defaultSetting != null)
						    				isServerOnly = defaultSetting.isServerOnly();				    		
						    			if (isServerOnly || !Helper.isInteger(key))
						    				continue;
						    							    			
						    			String deviceConfigVal = deviceConfigMap.get(key);
					    				if (deviceConfigVal == null)
					    					deviceConfigVal = "";
					    				//if a DNS host name or IP address doesn't have two dots, use the default value
			    						if (DeviceUtils.EDGE_COMM_SETTING_EXPRESSION.matcher(key).matches() && !DeviceUtils.TWO_DOT_EXPRESSION.matcher(deviceConfigVal).find()) {
			    							deviceConfigVal = defaultConfigMap.get(key).getConfigTemplateSettingValue();
			    							if (deviceConfigVal == null)
						    					deviceConfigVal = "";
			    						}		    						
			    						newDeviceConfigFile.append(key).append("=").append(deviceConfigVal).append("\n");
						    		}
					    			
					    			LinkedHashMap<String, String> deviceSettingData = ConfigFileActions.parseIniFileData(newDeviceConfigFile.toString());
					    	    	DeviceConfigurationUtils.saveDeviceSettingData(deviceId, deviceSettingData, true, false, conn, userName);
					    			
					    			//create a new file transfer record
					    			long newEdgeFileTransferId = ConfigFileActions.getNextFileTransferSequenceNum(conn);
						    		ConfigFileActions.saveFile(new StringBuilder(deviceConfigFileName).append("-bulk-").append(newEdgeFileTransferId).toString(), newIniFileShort.toString(), newEdgeFileTransferId, FileType.PROPERTY_LIST.getValue(), conn);
						    		
						    		try{
						    			DeviceUtils.sendCommand(deviceType, deviceId, deviceName, newEdgeFileTransferId, newIniFileShort.length(), DeviceUtils.DEFAULT_PACKET_SIZE, DeviceUtils.DEFAULT_EXECUTE_ORDER, conn, String.valueOf(newEdgeFileTransferId), false);
						    		}catch(Exception e){
						    			out.append("<font color=\"red\"><b>Error inserting pending file transfer for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");
						    			log.error(new StringBuilder("Error inserting pending file transfer for device ").append(deviceName), e);
						    			error_count++;
						    			break DEVICE;
						    		}
					    		}
				    		} else if (isGx || isMEI) {
				    			try{
				    				displayChangesInline = true;
				    				for (String offset: paramList) {
				    					String data = ConvertUtils.getStringSafely(paramMap.get(new StringBuilder("cfg_field_").append(offset).toString()), "");
				    					ConfigTemplateSetting configTemplateSetting = defaultConfigMap.get(offset);
			    		    			int size = configTemplateSetting.getFieldSize();		    			
			    		    			String align = configTemplateSetting.getAlign();
			    		    			String pad_with = configTemplateSetting.getPadChar();
			    		    			String data_mode = configTemplateSetting.getDataMode();
			    		    			String key = configTemplateSetting.getKey();
			    		    			String label = configTemplateSetting.getLabel();
			    		    			boolean storedInPennies = "Y".equalsIgnoreCase(configTemplateSetting.getStoredInPennies());
			    		    			
			    		    			if (storedInPennies && StringHelper.isNumeric(data))
						    				data = String.valueOf((new BigDecimal(data).multiply(BigDecimal.valueOf(100)).intValue()));
			    		    			
			    		    			String deviceConfigVal = deviceConfigMap.get(key);
				    					if(StringHelper.equalConfigValues(data, deviceConfigVal, storedInPennies)) {
						    				no_change_count++;
						    				continue;
				    					}
			    		    			
			    		    			if (configTemplateSetting.isServerOnly())
			    		    				out.append(label).append(": ").append(data).append("\n");
			    		    			else {
			    			    			out.append(configTemplateSetting.getCounter()).append(") ").append(configTemplateSetting.getDisplayOrder()).append(" ").append(offset)
			    			    				.append(" ").append(size).append(" ").append(align).append(" ").append(pad_with).append(" ").append(data_mode)
			    			    				.append(" [ ").append(label).append(" ]\n");
			    		    			
			    			    			out.append("IN: ").append(data).append(" len=").append(data.length()).append("\n");
			    			    			if("H".equalsIgnoreCase(data_mode) && data.length() < size * 2 
			    			    					|| !"H".equalsIgnoreCase(data_mode) && data.length() < size){
			    			    				String[] results = PendingCmdActions.pad(data, pad_with, size, align, data_mode);
			    			    				if(results!=null){
			    			    					data = results[1];
			    			    					out.append(results[0]).append("\n");
			    			    				}
			    			    			}else
			    			    				out.append("No Padding Needed\n");
			    			    			out.append("OUT: \"" ).append(data).append("\" len=").append(data.length()).append("\n\n");
			    		    			}
			    		    			
		    		    				if (configTemplateSetting.isServerOnly()) {
		    		    					DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, data, conn, userName);		    		    					
		    		    					if (configTemplateSetting.getKey().startsWith("CALL_IN_TIME_WINDOW_"))
		    		    						callInTimeWindowChangeCount++;
		    		    					change_count++;
		    		    					continue;
		    			    			}
		    		    				
		    		    				//save the changed param value to the map
		    		    				try{
		    				                result = DataLayerMgr.executeCall(conn, "SP_UPDATE_CONFIG_AND_RETURN", new Object[] {deviceId, offset, data, data_mode, deviceType, 0, 0, "", "", "", ""});
		    		    				}catch(Exception e){
		    		    					out.append("<font color=\"red\"><b>Error updating device configuration for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");
		    		    					log.error(new StringBuilder("Error updating device configuration for device ").append(deviceName).toString(), e);
		    		    					error_count++;
		    		    					break DEVICE;
		    		    				}
		    		    				
		    		    				returnCd = -1;
		    		    				if (result != null) {
		    		    					returnCd = ConvertUtils.getIntSafely(result[1], -1);
		    		    					String returnMsg = ConvertUtils.getStringSafely(result[2]);
		    		    					
		    		    					if (!StringHelper.isBlank(returnMsg))
		    			    					out.append(returnMsg).append("\n");
		    		    				
		    			    				if(returnCd < 0) {
		    			    					out.append("<font color=\"red\"><b>Error updating device configuration for device ").append(deviceName).append("</b></font>\n");
			    		    					log.error(new StringBuilder("Error updating device configuration for device ").append(deviceName).toString());
			    		    					error_count++;
			    		    					break DEVICE;
		    			    				} else if(returnCd == 0)
		    			    					no_change_count++;
		    			    				else {
		    			    					if (configTemplateSetting.getFieldOffset() == DevicesConstants.GX_MAP_CALL_IN_TIME && isGx
		    			    							|| configTemplateSetting.getFieldOffset() == DevicesConstants.MEI_MAP_CALL_IN_TIME && isMEI)
			    		    						callInTimeChangeCount++;
		    			    					change_count++;
		    			    				}
		    		    				}
			    		    			out.append("\n");
				    	    		}
					        	}catch(Exception e){
					        		out.append(new StringBuilder("<font color=\"red\"><b>Error updating device configuration for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n").toString());
					                log.error(new StringBuilder("Error updating device configuration for device ").append(deviceName).toString(), e);
					                error_count++;
					                break DEVICE;
					        	}
				    		} else if (deviceType == DeviceType.KIOSK.getValue()) {
				    			LinkedHashMap<String, String> oldDeviceSettingData = ConfigFileActions.getDeviceSettingData(null, deviceId, deviceType, true, conn);
				    			LinkedHashMap<String, String> deviceSettingData = new LinkedHashMap<String, String> ();
				    			
				    			if (oldDeviceSettingData.size() > 0) {
				    				LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceType, -1, null, conn);
				    				boolean publicPc = oldDeviceSettingData.containsKey("Public_PC_Version");
				    				for (Map.Entry<String, String> entry : oldDeviceSettingData.entrySet()) {
					    				String configParam = entry.getKey();
				    					String oldValue = entry.getValue();
					    				if (oldValue == null)
					    					oldValue = "";
					    				ConfigTemplateSetting defaultSetting = defaultSettingData.get(configParam);
					    				boolean isDailyExtendedSchedule=WebHelper.isDailyExtendedSchedule(defaultSetting);
					    				boolean storedInPennies = defaultSetting != null && "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
					    				String paramValue = ConvertUtils.getStringSafely(paramMap.get(new StringBuilder("cfg_field_").append(configParam).toString()), oldValue);
				    					if (isDailyExtendedSchedule){
											paramValue=WebHelper.buildScheduleDailyExtended(paramValue);
				    					}
				    					else if (storedInPennies && StringHelper.isNumeric(paramValue))
						    				paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
			
				    					if (StringHelper.equalConfigValues(paramValue, oldValue, storedInPennies))
				    						no_change_count++;
				    					else {
				    						changesForDevice.append(configParam).append(": ").append(paramValue);
					    					changesForDevice.append(", old: ").append(oldValue).append("\n");
											if (isDeviceModemStatusField(configParam)) {
												deviceIds.add(deviceId);
												oldValues.add(oldValue);
												newValues.add(paramValue);
											}
				    						change_count++;
				    					}
					    				
					    				if(publicPc && (configParam.indexOf("OpenAPIPassword") >-1 || configParam.indexOf("MFPAdminPassword") >-1 || configParam.indexOf("SNMPCommunity") >-1)) {				
					    					String encryptedOutput = DMSSecurityUtil.rijndaelEncrypt(paramValue);
					    					paramValue = StringHelper.encodeHexString(encryptedOutput);
					    				}
					    				
					    				if(!deviceSettingData.containsKey(configParam))
					    					deviceSettingData.put(configParam, paramValue);
					    			}
					    			if (changesForDevice.length() > 0) {
					    				DeviceConfigurationUtils.saveDeviceSettingData(deviceId, deviceSettingData, true, false, conn, userName);
					    				if(!WebHelper.isCraneDevice(ConvertUtils.getString(rs.get("device_serial_cd"), true))){
						    				String newIniFile = ConfigFileActions.getIniFileFromDeviceSettingData(deviceSettingData);
									    	long fileId = ConfigFileActions.saveFile(deviceConfigFileName, newIniFile, -1, FileType.CONFIGURATION_FILE.getValue(), conn);
									    	DeviceUtils.sendCommand(deviceType, deviceId, deviceName, fileId, newIniFile.length(), DeviceUtils.DEFAULT_PACKET_SIZE, DeviceUtils.DEFAULT_EXECUTE_ORDER, conn, DeviceActions.KIOSK_CONFIG_FILE_NAME_HEX, true);
					    				}
					    			}
				    			}
				    		}
				    		if (!displayChangesInline) {
					    		if (changesForDevice.length() > 0)
					    			out.append("Configuration Changes for ").append(deviceName).append(": \n").append(StringUtils.prepareCDATA(changesForDevice.toString()));
					    		else				    			
					    			out.append("No Configuration Changes for ").append(deviceName).append("\n");
				    		}
				    		
				    		if (callInTimeWindowChangeCount > 0)
					    		DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] {1, deviceId});
					    	else if (callInTimeChangeCount > 0)
					    		DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] {0, deviceId});
			    		}
			        }
			    	
			    	if (processFileUploads) {			    		
				    	for (int i = 0; i < fileUploadArray.length; i++) {
				    		if (debug) {
				    			file_upload_count++;
				    			continue;
				    		}
				    		long fileId = Long.valueOf(fileUploadArray[i]);
				    		try {
					    		String[] fileUploadFields = fileUploadFieldArray[i].split("~", -1);
					    		int executeOrder = Integer.valueOf(fileUploadFields[0]);
					    		int packetSize = Integer.valueOf(fileUploadFields[1]);				    		
					    		DeviceUtils.sendCommand(deviceType, deviceId, deviceName, fileId, -1, packetSize, executeOrder, conn, null, true);
					    		out.append("Enqueued file upload request for device ").append(deviceName).append(", file id: ").append(fileId).append("\n");
			    				file_upload_count++;
				    		} catch (Exception e) {
				    			out.append("<font color=\"red\"><b>Error processing file upload request for device ").append(deviceName).append(", file id: ").append(fileId).append(": ").append(e.getMessage()).append("</b></font>\n");
					    		log.error(new StringBuilder("Error processing file upload request for device ").append(deviceName).append(", file id: ").append(fileId).toString(), e);
					    		file_upload_error_count++;
					    		//break DEVICE;
				    		}
			    		}
			    	}
			    	
			    	if (processEFT) {
			    		if (debug)
			    			eft_count++;
			    		else {
			    			try {
			    				PendingCmdActions.createPendingCommand(conn, deviceName, FileActions.externalFileTransferDataType, eft, "P", 0, true);
							    out.append("Enqueued External File Transfer request for device ").append(deviceName).append("\n");
							    eft_count++;
			    			} catch (Exception e) {
			    				out.append("<font color=\"red\"><b>Error processing external file transfer for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");
					    		log.error(new StringBuilder("Error processing external file transfer for device ").append(deviceName), e);
					    		eft_error_count++;
					    		break DEVICE;
			    			}
			    		}
			    	}
			    	
			    	if (processFileDownloads) {			    		
				    	for (int i = 0; i < fileDownloadArray.length; i++) {
				    		if (debug) {
				    			file_download_count++;
				    			continue;
				    		}
				    		String downloadFileName = fileDownloadArray[i];
				    		String downloadFileType = fileDownloadFieldArray[i];
				    		String[] actionParams = {downloadFileName, downloadFileType};
				    		try {
					    		DeviceActions.processDeviceAction(conn, "Download File", form, deviceId, deviceName, deviceType, -1, true, actionParams);
					    		out.append("Enqueued file download request for device ").append(deviceName).append(", file name: ").append(downloadFileName);
					    		if (!"-1".equalsIgnoreCase(downloadFileType))
					    			out.append(", file type: ").append(downloadFileType);
					    		out.append("\n");
			    				file_download_count++;
				    		} catch (Exception e) {
				    			out.append("<font color=\"red\"><b>Error processing file download request for device ").append(deviceName).append(", file name: ").append(downloadFileName).append(", file type: ").append(downloadFileType).append(": ").append(e.getMessage()).append("</b></font>\n");
					    		log.error(new StringBuilder("Error processing file download request for device ").append(deviceName).append(", file name: ").append(downloadFileName).append(", file type: ").append(downloadFileType).toString(), e);
					    		file_download_error_count++;
					    		//break DEVICE;
				    		}
			    		}
			    	}
			    	
			    	if (processS2CRequests) {
			    		for (int i = 0; i < s2cRequestFieldArray.length; i++) {
			    			if (debug) {
				    			s2c_request_count++;
				    			continue;
				    		}
				    		String s2cRequest = s2cRequestFieldArray[i];
			    			if (deviceType == DeviceType.EDGE.getValue()) {
					    		String[] s2cRequestFields = null;
					    		String s2cAction;
					    		if ("Download Config".equalsIgnoreCase(s2cRequest))
					    			s2cAction = s2cRequest;
					    		else {					    			
					    			s2cRequestFields = s2cRequest.split("~", -1);
					    			s2cRequest = "Queue Command";
					    			String s2cActionCode = s2cRequestFields[0].substring(1);
						    		s2cAction = s2cActionDescList.getName(s2cActionCode);
						    		if (StringHelper.isBlank(s2cAction))
						    			s2cAction = s2cActionCode;
					    		}					    		
				    			try {
				    				DeviceActions.processDeviceAction(conn, s2cRequest, form, deviceId, deviceName, deviceType, propertyListVersion, true, s2cRequestFields);
				    				out.append("Enqueued ").append(s2cAction).append(" request for device ").append(deviceName).append("\n");
				    				s2c_request_count++;
				    			} catch (Exception e) {
				    				out.append("<font color=\"red\"><b>Error processing server to client request ").append(s2cAction).append(" for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");
						    		log.error(new StringBuilder("Error processing server to client request ").append(s2cAction).append(" for device ").append(deviceName).toString(), e);
				    				s2c_request_error_count++;
				    				break DEVICE;
				    			}
				    		} else {
				    			try {
				    				DeviceActions.processDeviceAction(conn, s2cRequest, form, deviceId, deviceName, deviceType, -1, true, null);
				    				out.append("Enqueued request ").append(s2cRequest).append(" for device ").append(deviceName).append("\n");
				    				s2c_request_count++;
				    			} catch (Exception e) {
				    				out.append("<font color=\"red\"><b>Error processing server to client request ").append(s2cRequest).append(" for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");
						    		log.error(new StringBuilder("Error processing server to client request ").append(s2cRequest).append(" for device ").append(deviceName).toString(), e);
				    				s2c_request_error_count++;
				    				break DEVICE;
				    			}
				    		}
			    		}
			    	}
			    	
			    	if (processFirmwareUpgrades) {			    		
				    	for (int i = 0; i < firmwareUpgradeArray.length; i++) {
				    		if (debug) {
				    			firmware_upgrade_count++;
				    			continue;
				    		}
				    		long firmwareUpgradeId = Long.valueOf(firmwareUpgradeArray[i]);
				    		try {
					    		ConfigFileActions.scheduleUpgrade(deviceId, firmwareUpgradeId);
					    		out.append("Enqueued firmware upgrade request for device ").append(deviceName).append(", firmware upgrade id: ").append(firmwareUpgradeId).append("\n");
					    		firmware_upgrade_count++;
				    		} catch (Exception e) {
				    			out.append("<font color=\"red\"><b>Error processing firmware upgrade request for device ").append(deviceName).append(", firmware upgrade id: ").append(firmwareUpgradeId).append(": ").append(e.getMessage()).append("</b></font>\n");
					    		log.error(new StringBuilder("Error processing firmware upgrade request for device ").append(deviceName).append(", firmware upgrade id: ").append(firmwareUpgradeId).toString(), e);
					    		firmware_upgrade_error_count++;
					    		break DEVICE;
				    		}
			    		}
			    	}
			    	
			    	if (ec_credential_id > -1 && deviceType == DeviceType.KIOSK.getValue() && ConvertUtils.getString(rs.get("device_serial_cd"), true).startsWith("K3")) {
			    		try {
			    			DataLayerMgr.executeUpdate(conn, "UPDATE_DEVICE_CREDENTIAL", new Object[] {ec_credential_id, deviceId});
			    			out.append("Changed Quick Connect credential to ").append(ec_credential_name).append(" for device ").append(deviceName).append("\n");
			    			ec_credential_count++;
			    		} catch (Exception e) {
			    			out.append("<font color=\"red\"><b>Error changing Quick Connect credential to ").append(ec_credential_name).append(" for device ").append(deviceName).append(": ").append(e.getMessage()).append("</b></font>\n");
				    		log.error(new StringBuilder("Error changing Quick Connect credential to ").append(ec_credential_name).append(" for device ").append(deviceName).toString(), e);
			    			ec_credential_error_count++;
			    			break DEVICE;
			    		}
			    	}
			    	
			    	deviceSuccess = true;
			    	break DEVICE;
	        	}
	    		if(deviceSuccess) {
    				conn.commit();
    				out.append("\n");
					WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, startTsMs, "Finish >", "device", deviceName, log);
    			}else{
    				ProcessingUtils.rollbackDbConnection(log, conn);
    				out.append("<font color=\"red\"><b>Operation rolled back for ").append(deviceName).append("</b></font>\n\n");
			    	log.error(new StringBuilder("Operation rolled back for ").append(deviceName));
    			}
			}
			Map<DeviceGroupKey,List<USATDevice>> modemGroupsToUpdateStatus = new HashMap<>();
	        USATDevice[] allDevices = populateDevicesToModemStatusUpdate(deviceIds, oldValues, newValues);
	        for (USATDevice md : allDevices) {
	        	groupDevice(md, modemGroupsToUpdateStatus);
			}
			enqueueAllDevicesToModemStatusUpdate(modemGroupsToUpdateStatus);
	        success = true;
	    }catch(Exception e){
    		throw new ServletException("Error in bulk configuration wizard", e);
		}finally{
			if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
		}
		
		out.append("\nOperation Summary\n\n");
		out.append("Parameters Changed: ").append(change_count).append("\n");
		out.append("Parameters Already Up-To-Date: ").append(no_change_count).append("\n");
		if (error_count > 0)
			out.append("<font color=\"red\"><b>Parameter Errors: ").append(error_count).append("</b></font>\n");
		if(StringHelper.isBlank(parentLocationId)) {
		   out.append("Customer/Locations Updated: ").append(custloc_update_count).append("\n");
		   if (custloc_error_count > 0)
			   out.append("<font color=\"red\"><b>Customer/Location Errors: ").append(custloc_error_count).append("</b></font>\n");
		} else {
		   out.append("Customer/Parent Locations Updated: ").append(parentloc_update_count).append("\n");
		   if (parentloc_error_count > 0)
			   out.append("<font color=\"red\"><b>Customer/Parent Location Errors: ").append(parentloc_error_count).append("</b></font>\n");
		}
		out.append("Payment Templates Imported: ").append(import_count).append("\n");
		if (import_error_count > 0)
			out.append("<font color=\"red\"><b>Payment Template Errors: ").append(import_error_count).append("</b></font>\n");
		if (processFileUploads) {
			out.append("File Upload Requests Enqueued: ").append(file_upload_count).append("\n");
			if (file_upload_error_count > 0)
				out.append("<font color=\"red\"><b>File Upload Request Errors: ").append(file_upload_error_count).append("</b></font>\n");
		}
		if (processEFT) {
			out.append("External File Transfers Enqueued: ").append(eft_count).append("\n");
			if (eft_error_count > 0)
				out.append("<font color=\"red\"><b>External File Transfer Errors: ").append(eft_error_count).append("</b></font>\n");
		}
		if (processFileDownloads) {
			out.append("File Download Requests Enqueued: ").append(file_download_count).append("\n");
			if (file_download_error_count > 0)
				out.append("<font color=\"red\"><b>File Download Request Errors: ").append(file_download_error_count).append("</b></font>\n");
		}
		if (processS2CRequests) {
			out.append("Server to Client Requests Enqueued: ").append(s2c_request_count).append("\n");
			if (s2c_request_error_count > 0)
				out.append("<font color=\"red\"><b>Server to Client Request Errors: ").append(s2c_request_error_count).append("</b></font>\n");
		}
		if (processFirmwareUpgrades) {
			out.append("Firmware Upgrade Requests Enqueued: ").append(firmware_upgrade_count).append("\n");
			if (firmware_upgrade_error_count > 0)
				out.append("<font color=\"red\"><b>Firmware Upgrade Request Errors: ").append(firmware_upgrade_error_count).append("</b></font>\n");
		}
		if (ec_credential_id > -1) {
			out.append("Quick Connect Credentials Changed: ").append(ec_credential_count).append("\n");
			if (ec_credential_error_count > 0)
				out.append("<font color=\"red\"><b>Quick Connect Credential Errors: ").append(ec_credential_error_count).append("</b></font>\n");
		}
		
		if(debug)
			out.append("<center><font color=\"red\"><b>DEBUG IS ON!  CHANGES WILL NOT BE SAVED!</b></font></center><br/>");
		String changes = form.getString("changes", true);
        StringBuilder email = new StringBuilder("<html><head>").append(Helper.getBasicStyle()).append("</head><body>").append(changes)
        		.append("<br/><b>Results:</b><br/><br/><pre>").append(out.toString()).append("</pre></body></html>");
        try {
        	Helper.sendEmail(Helper.DMS_EMAIL, user.getEmailAddress(), "Device Configuration Wizard Confirmation", email.toString(), Helper.DMS_EMAIL_CONTENT_TYPE_HTML);
        	out.append("\nEmailed confirmation to ").append(user.getEmailAddress()).append("\n");
        } catch (MessagingException e) {
        	out.append("<font color=\"red\">Error emailing confirmation: ").append(e.getMessage()).append("</font>\n");
        	log.error("Error emailing confirmation", e);
        }
		request.setAttribute("bulk_out", out.toString());
		log.info("Bulk device configuration finished");
    }

	boolean isDeviceModemStatusField(String changedKey) {
		return "DEVICE_MODEM_STATUS".equals(changedKey);
	}

	void enqueueAllDevicesToModemStatusUpdate(Map<DeviceGroupKey,List<USATDevice>> allGroups) throws SQLException, ServletException, DataLayerException, ConvertException {
    	if (allGroups.isEmpty()) {
    		return;
		}
		Set<DeviceGroupKey> keys = allGroups.keySet();
		for (DeviceGroupKey key : keys) {
			List<USATDevice> devs = allGroups.get(key);
			enqueueModemsStatusChange(devs, key.getOldStatus(), key.getNewStatus());
		}
	}

	void groupDevice(USATDevice dev, Map<DeviceGroupKey,List<USATDevice>> allGroups) {
    	if (dev == null) {
    		return;
		}
		DeviceGroupKey myKey = new DeviceGroupKey(dev.getCurrentStatus(), dev.getRequestedStatus(), dev.getOwnerEmail());
		List<USATDevice> keyDev = allGroups.get(myKey);
		if (keyDev == null) {
			keyDev = new ArrayList<>();
			allGroups.put(myKey, keyDev);
		}
		keyDev.add(dev);
	}

	@NotNull
	USATDevice[] populateDevicesToModemStatusUpdate(List<Long> deviceIds, List<String> oldStatusKeys,
			List<String> newStatusKeys) throws DataLayerException, SQLException, ConvertException {
		if (deviceIds.isEmpty() || oldStatusKeys.isEmpty() || newStatusKeys.isEmpty()) {
			return new USATDevice[0];
		}
		USATDeviceDao modemDeviceDao = new USATDeviceDaoImpl();
		List<Long> deviceIdsFiltered = new ArrayList<>();
		List<String> oldStatusKeysFiltered = new ArrayList<>();
		List<String> newStatusKeysFiltered = new ArrayList<>();
		// Filter devices with wrong status
		for (int i = 0; i < deviceIds.size(); i++) {
			if (newStatusKeys.get(i) == null || newStatusKeys.get(i).length() != 1) {
				// Unexpected new status
				log.warn("BulkConfigWizard8Step::populateDevicesToModemStatusUpdate() - unexpected status requested: "
						+ newStatusKeys.get(i));
				continue;
			}
			if (oldStatusKeys.get(i) != null && oldStatusKeys.get(i).equals(newStatusKeys.get(i))) {
				// Same status requested, just skip in that case
				continue;
			}
			deviceIdsFiltered.add(deviceIds.get(i));
			oldStatusKeysFiltered.add(oldStatusKeys.get(i));
			newStatusKeysFiltered.add(newStatusKeys.get(i));
		}
		if (deviceIdsFiltered.isEmpty()) {
			// Nothing to update
			return new USATDevice[0];
		}
		long[] devIdsArray = new long[deviceIdsFiltered.size()];
		for (int i = 0; i < deviceIdsFiltered.size(); i++) {
			devIdsArray[i] = deviceIdsFiltered.get(i);
		}
		
		USATDevice[] devs = modemDeviceDao.populateByDeviceIds(devIdsArray);
		if (devs != null && devs.length > 0) {
			List<USATDevice> filteredDevices = new ArrayList<>();
			for (USATDevice dev: devs) {
				if (dev.hasSerialNumberAndProvider()) {
					filteredDevices.add(dev);
				} else {
					log.warn(
							"BulkConfigWizard8Step::populateDevicesToModemStatusUpdate() - unexpected Device without ICCID or Provider: "
									+ dev);
				}
			}
			if (!filteredDevices.isEmpty()) {
				// Add status information to filtered devices
				for (int idIdx = 0; idIdx < deviceIdsFiltered.size(); idIdx++) {
					//For every requested device ID
					for (int deviceIdx = 0; deviceIdx < filteredDevices.size(); deviceIdx++) {
						// Look for filtered device with ICCID and Provider
						if (deviceIdsFiltered.get(idIdx).equals(filteredDevices.get(deviceIdx).getDeviceId())) {
							// Found device with ID = our filtered ID
							// So, add status information to that device
							filteredDevices.get(deviceIdx).setCurrentStatus(oldStatusKeysFiltered.get(idIdx));
							filteredDevices.get(deviceIdx).setRequestedStatus(newStatusKeysFiltered.get(idIdx));
							break;
						}
					}
				}
				return filteredDevices.toArray(new USATDevice[filteredDevices.size()]);
			}
		}
		return new USATDevice[0];
	}

	void enqueueModemsStatusChange(List<USATDevice> devices, String oldStatusKey, String newStatusKey) throws DataLayerException, SQLException, ConvertException, ServletException {
    	if (devices == null || devices.isEmpty()) {
    		return;
		}
		USATModemService dmsModemService = new AsyncDmsModemService();
		USATDevice[] devArr = devices.toArray(new USATDevice[devices.size()]);
		if ("N".equals(newStatusKey)) {
			dmsModemService.refreshActivationStatus(devArr);
		} else if ("A".equals(newStatusKey)) {
			if (oldStatusKey == null || !"S".equals(oldStatusKey)) {
				dmsModemService.activate(devArr);
			} else {
				dmsModemService.resume(devArr);
			}
		} else if ("D".equals(newStatusKey)) {
			dmsModemService.deactivate(devArr);
		} else if ("S".equals(newStatusKey)) {
			dmsModemService.suspend(devArr);
		} else {
			dmsModemService.refreshActivationStatus(devArr);
		}
	}

	static class DeviceGroupKey {
    	private final String oldStatus;
    	private final String newStatus;
    	private final String email;

		public DeviceGroupKey(String oldStatus, String newStatus, String email) {
			this.oldStatus = oldStatus;
			this.newStatus = newStatus;
			this.email = email;
		}

		public String getOldStatus() {
			return oldStatus;
		}

		public String getNewStatus() {
			return newStatus;
		}

		public String getEmail() {
			return email;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null || getClass() != o.getClass())
				return false;

			DeviceGroupKey that = (DeviceGroupKey) o;

			if (oldStatus != null ? !oldStatus.equals(that.oldStatus) : that.oldStatus != null)
				return false;
			if (newStatus != null ? !newStatus.equals(that.newStatus) : that.newStatus != null)
				return false;
			if (email != null ? !email.equals(that.email) : that.email != null)
				return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = oldStatus != null ? oldStatus.hashCode() : 0;
			result = 31 * result + (newStatus != null ? newStatus.hashCode() : 0);
			result = 31 * result + (email != null ? email.hashCode() : 0);
			return result;
		}

		@Override
		public String toString() {
			return "DeviceGroupKey{" + "oldStatus='" + oldStatus + '\'' + ", newStatus='" + newStatus + '\'' + ", email='" + email + '\'' + '}';
		}
	}
}
