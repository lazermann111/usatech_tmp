/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.NameValuePair;

import com.usatech.dms.action.ConfigFileActions;

/**
 * Step for "Template Configuration Menu" page.
 */
public class TemplateMenuStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        try
        {
            List<NameValuePair> posPtaTmplSelections = ConfigFileActions.getPosPtaTemplateSelections();
            form.set("com.usatech.dms.tmpl_menu.pos_pta_tmpl_selections", posPtaTmplSelections);
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }
    }

}
