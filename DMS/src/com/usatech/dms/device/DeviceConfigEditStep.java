/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.util.StringHelper;

/**
 * Step to process the "edit configuration" (EDGE, KIOSK, G4, G5, MEI, etc)
 */
public class DeviceConfigEditStep extends AbstractStep
{   
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        long deviceId = form.getLong("device_id", false, -1);
        int deviceTypeId = form.getInt("device_type_id", false, -1);
        String serialNumber = form.getString("ssn", false);
        String evNumber = form.getString("ev_number", false);
        if (deviceId == -1 || deviceTypeId == -1 || StringHelper.isBlank(serialNumber) || StringHelper.isBlank(evNumber))
            throw new ServletException("Device not specified!");
        int plv = form.getInt("plv", false, -1);
        
        String errorMessage;
        String fileName = form.getString("file_name", false);
        if (StringHelper.isBlank(fileName))
        {
        	errorMessage = "Required parameter not found: file_name!";
            setErrorMessage(errorMessage, form, new ServletException(errorMessage));
            return;
        }
        
        String editMode = form.getString("edit_mode", false);
        if (StringHelper.isBlank(editMode))
        {
        	errorMessage = "Required parameter not found: edit_mode!";
            setErrorMessage(errorMessage, form, new ServletException(errorMessage));
            return;
        }    	    	

        String action = form.getString("myaction", false);
        long templateId = form.getLong("template_id", false, -1);
        String templateName = form.getString("template_name", false);
		String targetFile;
		long targetDeviceId = deviceId;
		long templateDeviceId = -1;
    	if ("Import".equalsIgnoreCase(action)) {
    		templateDeviceId = form.getLong("template_device_id", false, -1);
            if (templateDeviceId > 0)
            	targetDeviceId = templateDeviceId;            
            else if (templateId < 1 || StringHelper.isBlank(templateName))
            {
            	errorMessage = "Required parameter not found: template_id or template_name!";
                setErrorMessage(errorMessage, form, new ServletException(errorMessage));
                return;
            }
            targetFile = templateName;
    	} else
    		targetFile = fileName;
    	
		LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData;
		try {
			defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceTypeId, plv);
		} catch (Exception e) {
			throw new ServletException(e);
		}
		request.setAttribute("defaultSettingData", defaultSettingData);
    	
		LinkedHashMap<String, String> targetSettingData;
    	if ("Import".equalsIgnoreCase(action)) {
    		if (templateDeviceId > 0)
    			targetSettingData = ConfigFileActions.getDeviceSettingData(defaultSettingData, templateDeviceId, deviceTypeId, true, null);
    		else if (evNumber.concat("-BAK").equalsIgnoreCase(templateName))
    			targetSettingData = ConfigFileActions.getBackupDeviceSettingData(defaultSettingData, targetDeviceId, deviceTypeId, true, null);
    		else
    			targetSettingData = ConfigFileActions.getConfigTemplateSettingData(defaultSettingData, templateId, deviceTypeId, true, null);
    		
    		if(!"R".equalsIgnoreCase(editMode)){
	    		LinkedHashMap<String, String> currentSettingData = ConfigFileActions.getDeviceSettingData(defaultSettingData, deviceId, deviceTypeId, true, null);
	    		if (currentSettingData == null) {
	        		errorMessage = "Device config file data not found for deviceName " + evNumber;
	                setErrorMessage(errorMessage, form, new ServletException(errorMessage));
	                return;
	        	}
	    		request.setAttribute("currentSettingData", currentSettingData);
        	}
    	} else        	        	
    		targetSettingData = ConfigFileActions.getDeviceSettingData(defaultSettingData, targetDeviceId, deviceTypeId, !"R".equalsIgnoreCase(editMode), null);
    	
    	if (targetSettingData == null) {
    		errorMessage = "Config file data not found for file " + targetFile;
            setErrorMessage(errorMessage, form, new ServletException(errorMessage));
            return;
    	}
    	
    	if("R".equalsIgnoreCase(editMode)){
	    	String fileData = ConfigFileActions.getIniFileFromDeviceSettingData(targetSettingData);
		    request.setAttribute("file_data", fileData);
    	} else
    		request.setAttribute("targetSettingData", targetSettingData);
    	
    	form.set("ev_number", evNumber);
        form.set("device_id", deviceId);
        form.set("device_type_id", deviceTypeId);
    }

    private void setErrorMessage(String message, InputForm form, Exception e)
    {
        form.set("errorMessage", message);
        form.set("simple.servlet.SimpleAction.exception", e);
    }
}
