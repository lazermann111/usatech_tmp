/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.dms.action.DeviceActions;
import com.usatech.layers.common.model.Device;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class PosptaSettingsEditFuncStep extends AbstractStep {
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	public static final SimpleDateFormat dateTimeMMDDYYYHHMIFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm");
	public static final SimpleDateFormat dateTimeDDMMYYYYFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	public static final SimpleDateFormat dateTimeYYYYMMDDFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat declineUntilFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	public static final SimpleDateFormat hourFormatter = new SimpleDateFormat("h");
	public static final SimpleDateFormat ampmFormatter = new SimpleDateFormat("a");
	public static final SimpleDateFormat tzFormatter = new SimpleDateFormat("z");

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		//### determine action ###
		String errorMessage = null;
		Map<String, String> errorMap = new HashMap<String, String>();
		if (StringHelper.isBlank(form.getString("myaction", true)) && StringHelper.isBlank(form.getString("action", true))) {
			errorMessage = "Required Parameter Not Found: myaction or action";
			request.setAttribute("errorMessage", errorMessage);
			return;
		}
		//### check input variables ###
		long device_id = form.getLong("device_id", false, -1);
		long pos_pta_id = form.getLong("pos_pta_id", false, -1);
		long pos_id = form.getLong("pos_id", false, -1);

		String pos_pta_device_serial_cd = form.getString("pos_pta_device_serial_cd", false);
		String pos_pta_encrypt_key = form.getString("pos_pta_encrypt_key", false);
		String pos_pta_encrypt_key2 = form.getString("pos_pta_encrypt_key2", false);
		String pos_pta_encrypt_key_hex = form.getString("pos_pta_encrypt_key_hex", false);
		String pos_pta_encrypt_key2_hex = form.getString("pos_pta_encrypt_key2_hex", false);
		String pos_pta_pin_req_yn_flag = form.getStringSafely("pos_pta_pin_req_yn_flag", "N");
		String pos_pta_passthru_allow_yn_flag = form.getString("pos_pta_passthru_allow_yn_flag", false);
		String pos_pta_disable_debit_denial = form.getString("pos_pta_disable_debit_denial", false);
		String no_convenience_fee = form.getString("no_convenience_fee", false);

		String pos_pta_pref_auth_amt = form.getString("pos_pta_pref_auth_amt", false);
		String pos_pta_pref_auth_amt_max = form.getString("pos_pta_pref_auth_amt_max", false);

		int payment_subtype_id = form.getInt("payment_subtype_id", false, -1);
		String payment_subtype_key_id = form.getString("payment_subtype_key_id", false);

		int authority_payment_mask_id = form.getInt("authority_payment_mask_id", false, -1);
		String pos_pta_regex = form.getString("pos_pta_regex", false);
		String pos_pta_regex_bref = form.getString("pos_pta_regex_bref", false);
		String currency_cd = form.getString("currency_cd", false);

		int toggle_cd = form.getInt("toggle_cd", false, -1);

		int original_payment_subtype_id = form.getInt("original_payment_subtype_id", false, -1);
		String original_payment_subtype_key_id = form.getString("original_payment_subtype_key_id", false);
		int original_authority_payment_mask_id = form.getInt("original_authority_payment_mask_id", false, -1);
		String original_pos_pta_regex = form.getString("original_pos_pta_regex", false);
		String original_pos_pta_regex_bref = form.getString("original_pos_pta_regex_bref", false);
		String original_pos_pta_device_serial_cd = form.getString("original_pos_pta_device_serial_cd", false);
		String original_pos_pta_encrypt_key = form.getString("original_pos_pta_encrypt_key", false);
		String original_pos_pta_encrypt_key2 = form.getString("original_pos_pta_encrypt_key2", false);
		String original_pos_pta_pin_req_yn_flag = form.getString("original_pos_pta_pin_req_yn_flag", false);
		String original_pos_pta_passthru_allow_yn_flag = form.getString("original_pos_pta_passthru_allow_yn_flag", false);
		String original_pos_pta_disable_debit_denial = form.getString("original_pos_pta_disable_debit_denial", false);
		String original_no_convenience_fee = form.getString("original_no_convenience_fee", false);
		String original_pos_pta_pref_auth_amt = form.getString("original_pos_pta_pref_auth_amt", false);
		String original_pos_pta_pref_auth_amt_max = form.getString("original_pos_pta_pref_auth_amt_max", false);

		Helper.validateExists(errorMap, form.getString("device_id", false), "device_id");
		Helper.validateExists(errorMap, form.getString("pos_pta_id", false), "pos_pta_id");
		Helper.validateExists(errorMap, form.getString("pos_id", false), "pos_id");
		Helper.validateExists(errorMap, form.getString("pos_pta_pin_req_yn_flag", false), "pos_pta_pin_req_yn_flag");
		Helper.validateExists(errorMap, form.getString("payment_subtype_id", false), "payment_subtype_id");
		Helper.validateExists(errorMap, form.getString("payment_subtype_key_id", false), "payment_subtype_key_id");
		Helper.validateExists(errorMap, form.getString("pos_pta_regex", false), "pos_pta_regex");
		Helper.validateExists(errorMap, form.getString("toggle_cd", false), "toggle_cd");
		Helper.validateExists(errorMap, form.getString("pos_pta_passthru_allow_yn_flag", false), "pos_pta_passthru_allow_yn_flag");
		Helper.validateExists(errorMap, form.getString("pos_pta_disable_debit_denial", false), "pos_pta_disable_debit_denial");
		Helper.validateExists(errorMap, form.getString("no_convenience_fee", false), "no_convenience_fee");
		Helper.validateExists(errorMap, form.getString("currency_cd", false), "currency_cd");

		if (!(StringHelper.isBlank(pos_pta_encrypt_key)))
			Helper.validateRegex(errorMap, form.getString("pos_pta_encrypt_key", false), "pos_pta_encrypt_key:Encryption key " + pos_pta_encrypt_key + " not a valid Hex string", "^[a-zA-Z0-9]*$");
		if (!(StringHelper.isBlank(pos_pta_encrypt_key2)))
			Helper.validateRegex(errorMap, form.getString("pos_pta_encrypt_key2", false), "pos_pta_encrypt_key2:Encryption key " + pos_pta_encrypt_key2 + " not a valid Hex string", "^[a-zA-Z0-9]*$");

		Helper.validateRegex(errorMap, form.getString("pos_pta_pin_req_yn_flag", false), "pos_pta_pin_req_yn_flag:Invalid PIN required flag " + pos_pta_pin_req_yn_flag + " must be Y or N", "^[NY]$");
		Helper.validateRegex(errorMap, form.getString("pos_pta_passthru_allow_yn_flag", false), "pos_pta_passthru_allow_yn_flag:Invalid PIN required flag " + pos_pta_passthru_allow_yn_flag + " must be Y or N", "^[NY]$");
		Helper.validateRegex(errorMap, form.getString("pos_pta_disable_debit_denial", false), "pos_pta_disable_debit_denial:Invalid Disable Debit Denial flag " + pos_pta_disable_debit_denial + " must be Y or N", "^[NY]$");
		Helper.validateRegex(errorMap, form.getString("no_convenience_fee", false), "no_convenience_fee:Invalid No Two-Tier Pricing flag " + no_convenience_fee + " must be Y or N", "^[NY]$");
		if (!(StringHelper.isBlank(pos_pta_pref_auth_amt)))
			Helper.validateRegex(errorMap, form.getString("pos_pta_pref_auth_amt", false), "pos_pta_pref_auth_amt:Override Amount " + pos_pta_pref_auth_amt + " not a valid number", "^[0-9]+(\\.[0-9]{0,2})?$");
		if (!(StringHelper.isBlank(pos_pta_pref_auth_amt_max)))
			Helper.validateRegex(errorMap, form.getString("pos_pta_pref_auth_amt_max", false), "pos_pta_pref_auth_amt_max:Override Limit " + pos_pta_pref_auth_amt_max + " not a valid number", "^[0-9]+(\\.[0-9]{0,2})?$");

		String pos_pta_regex_final = "";
		String pos_pta_regex_bref_final = "";
		try {
			try {

				if (!(StringHelper.isBlank(pos_pta_pref_auth_amt)) && (StringHelper.isBlank(pos_pta_pref_auth_amt_max))) {
					errorMap.put("pos_pta_pref_auth_amt_max", "Override limit is required with override amount!");
				}

				if ((StringHelper.isBlank(pos_pta_pref_auth_amt)) && !(StringHelper.isBlank(pos_pta_pref_auth_amt_max))) {
					errorMap.put("pos_pta_pref_auth_amt", "Override amount is required with override limit!");
				}
				if (!(StringHelper.isBlank(pos_pta_pref_auth_amt)) && !(StringHelper.isBlank(pos_pta_pref_auth_amt_max))) {
					Double pos_pta_pref_auth_amt_db = new Double(pos_pta_pref_auth_amt);
					Double pos_pta_pref_auth_amt_max_db = new Double(pos_pta_pref_auth_amt_max);

					if (pos_pta_pref_auth_amt_db > pos_pta_pref_auth_amt_max_db) {
						errorMap.put("pos_pta_pref_auth_amt", "Override amount can not be greater than limit! (" + pos_pta_pref_auth_amt_db + " > " + pos_pta_pref_auth_amt_max_db + ")");
					}
				}

			} catch (Exception e) {
				errorMap.put("pos_pta_pref_auth_amt", "Exception processing Override Amt and Limit!");
			}

			Date declineUntil = null;
			String whitelist = form.getString("whitelist", false);

			if (!"Y".equalsIgnoreCase(whitelist)) {
				String declineUntilDate = form.getString("declineUntilDate", false);
				String declineUntilTime = form.getString("declineUntilTime", false);
				if (!StringUtils.isBlank(declineUntilDate) && !StringUtils.isBlank(declineUntilTime)) {
					String declineUntilDateTimeStr = String.format("%s %s", declineUntilDate, declineUntilTime);
					try {
						declineUntil = declineUntilFormat.parse(declineUntilDateTimeStr);
					} catch (Exception e) {
						errorMap.put("declineUntilDate", "Invalid decline until date/time format!");
					}
				}
			}

			String toggle_date = form.getString("toggle_date", false);
			long now_ts = System.currentTimeMillis();
			Long toggle_ts = null;
			//get the deactivation_date

			int toggle_time = form.getInt("toggle_time", false, -1);
			String toggle_full_date = form.getString("toggle_full_date", false);
			int toggle_month = -1;
			int toggle_day = -1;
			int toggle_year = -1;
			String toggle_ampm = form.getString("toggle_ampm", false);
			String toggle_tzone = form.getString("toggle_tzone", false);

			Long pos_pta_ts = null;

			if ("now".equalsIgnoreCase(toggle_date)) {
				toggle_ts = now_ts;
				pos_pta_ts = toggle_ts;
			} else if ("custom".equalsIgnoreCase(toggle_date) && toggle_cd != -1) { //#either timestamp editable

				if (!(StringHelper.isBlank(toggle_full_date))) {

					try {
						String[] toggle_full_date_data = toggle_full_date.split("/");
						String m = toggle_full_date_data[0];
						String d = toggle_full_date_data[1];
						String y = toggle_full_date_data[2];
						toggle_month = new Integer(m).intValue();
						toggle_day = new Integer(d).intValue();
						toggle_year = new Integer(y).intValue();

						//check out the year, there is a two year window currently
						Calendar now = Calendar.getInstance();
						int curyyyy = now.get(Calendar.YEAR);
						Calendar limitYear = Calendar.getInstance();
						limitYear.add(Calendar.YEAR, DevicesConstants.PAYMENT_CONFIG_POS_PTA_CUSTOM_YEAR_LIMIT);
						int lastyyyy = limitYear.get(Calendar.YEAR);
						if (toggle_year != curyyyy && toggle_year != lastyyyy) {
							errorMap.put("custom_date", "Invalid custom year entry " + toggle_year + "! Year must be either " + curyyyy + " or " + lastyyyy);
						} else {

							int hour = (("PM".equalsIgnoreCase(toggle_ampm)) ? ((toggle_time == 12) ? toggle_time : toggle_time + 12) : ((toggle_time == 12) ? 0 : toggle_time));
							Calendar toggle_in_date = Calendar.getInstance();
							toggle_in_date.set(toggle_year, toggle_month - 1, toggle_day, hour, 0);
							int move_tz = new Integer(toggle_tzone).intValue();
							toggle_in_date.add(Calendar.HOUR, (move_tz < 0 ? move_tz : (-1 * move_tz)));
							toggle_ts = toggle_in_date.getTimeInMillis();
							pos_pta_ts = toggle_ts;
						}
					} catch (Exception e) {
						errorMap.put("custom_date", "Unable to parse month, date and year for custom date! : " + e.getMessage());
					}

				}

			}

			if (errorMap.size() > 0) {
				Map<String, String> errorMapSorted = new TreeMap<String, String>(new ConfigFileActions.KeySorter());
				errorMapSorted.putAll(errorMap);
				Map<String, String> temp = new HashMap<String, String>();
				temp.putAll(errorMapSorted);
				form.setAttribute("errorMap", temp);
				return;
			}

			Object[] result = null;
			Results results = null;

			//### determine authority_payment_mask settings, if applicable ###
			Device device;
			boolean okay = false;
			Connection conn = DataLayerMgr.getConnection("OPER");
			try {
				if (authority_payment_mask_id != 1) {
					results = DataLayerMgr.executeQuery(conn, "GET_PAYMENT_SUBTYPE", new Object[] {payment_subtype_id});
					if (results != null && results.next()) {
						int returnAuthPaymentMask = ((BigDecimal)results.get("authority_payment_mask_id")).intValue();
						if (returnAuthPaymentMask == authority_payment_mask_id)
							authority_payment_mask_id = -1;//#undef pp.authority_payment_mask_id
					}
				} else {
					pos_pta_regex_final = pos_pta_regex;
					pos_pta_regex_bref_final = pos_pta_regex_bref;
				}

				//### determine if we need to create new pos_pta (e.g if pos_pta is active, immutable property has been changed, and transactions exist) ###
				if (toggle_cd > 0 || toggle_cd == -1) {
					int immutable_setting_change_occured = 0;
					if (original_payment_subtype_id != payment_subtype_id || !(original_payment_subtype_key_id.equalsIgnoreCase(payment_subtype_key_id)) || original_authority_payment_mask_id != authority_payment_mask_id || !(original_pos_pta_regex.equalsIgnoreCase(pos_pta_regex_final)) || !(original_pos_pta_regex_bref.equalsIgnoreCase(pos_pta_regex_bref_final)) || !(original_pos_pta_device_serial_cd.equalsIgnoreCase(pos_pta_device_serial_cd)) || !(original_pos_pta_encrypt_key.equalsIgnoreCase(pos_pta_encrypt_key)) || !(original_pos_pta_encrypt_key2.equalsIgnoreCase(pos_pta_encrypt_key2)) || !(original_pos_pta_pin_req_yn_flag.equalsIgnoreCase(pos_pta_pin_req_yn_flag)) || !(original_pos_pta_pref_auth_amt.equalsIgnoreCase(pos_pta_pref_auth_amt)) || !(original_pos_pta_pref_auth_amt_max.equalsIgnoreCase(pos_pta_pref_auth_amt_max)) || !(original_pos_pta_passthru_allow_yn_flag.equalsIgnoreCase(pos_pta_passthru_allow_yn_flag)) || !(original_pos_pta_disable_debit_denial.equalsIgnoreCase(pos_pta_disable_debit_denial)) || !(original_no_convenience_fee.equalsIgnoreCase(no_convenience_fee))) {
						results = DataLayerMgr.executeQuery(conn, "GET_TRAN_COUNT", new Object[] {pos_pta_id});
						if (results != null && results.next()) {
							String tran_count = results.getFormattedValue("tran_count");
							if (new Integer(tran_count).intValue() > 0) {
								immutable_setting_change_occured = 1;
							}

						}

					}
					if (immutable_setting_change_occured == 1) {
						//### deactivate old pos_pta ###
						result = DataLayerMgr.executeCall(conn, "DEACTIVATE_OLD_POS_PTA", new Object[] {now_ts,pos_pta_id});
						if (result != null && ((Integer)result[0]).intValue() > 0) {
							//no-op
						} else {
							errorMap.put("pos_pta_id", "Database error updating pos_pta $pos_pta_id: unable to update.");
							form.setAttribute("errorMap", errorMap);
							return;
						}

						//### get settings from old pos_pta ###
						int old_pos_id, old_payment_subtype_id;
						Integer old_payment_subtype_key_id, old_authority_payment_mask_id;
						String old_pos_pta_encrypt_key, old_pos_pta_encrypt_key2, old_pos_pta_device_serial_cd, old_pos_pta_pin_req_yn_flag, old_pos_pta_regex, old_pos_pta_regex_bref, old_pos_pta_passthru_allow_yn_flag, old_pos_pta_disable_debit_denial, old_no_convenience_fee, old_pos_pta_pref_auth_amt, old_pos_pta_pref_auth_amt_max, old_currency_cd = "";
						Date old_decline_until = null;
						String old_whitelist = null;

						results = DataLayerMgr.executeQuery(conn, "GET_PTA_SETTINGS", new Object[] {pos_pta_id});
						if (results != null && results.next()) {
							old_pos_id = (StringHelper.isBlank(results.getFormattedValue("pos_id")) ? -1 : new Integer(results.getFormattedValue("pos_id")).intValue());
							old_payment_subtype_id = (StringHelper.isBlank(results.getFormattedValue("payment_subtype_id")) ? -1 : new Integer(results.getFormattedValue("payment_subtype_id")).intValue());
							old_payment_subtype_key_id = StringHelper.isBlank(results.getFormattedValue("payment_subtype_key_id")) ? null : new Integer(results.getFormattedValue("payment_subtype_key_id"));
							old_authority_payment_mask_id = StringHelper.isBlank(results.getFormattedValue("authority_payment_mask_id")) ? null : new Integer(results.getFormattedValue("authority_payment_mask_id"));
							old_pos_pta_encrypt_key = results.getFormattedValue("pos_pta_encrypt_key");
							old_pos_pta_encrypt_key2 = results.getFormattedValue("pos_pta_encrypt_key2");
							old_pos_pta_device_serial_cd = results.getFormattedValue("pos_pta_device_serial_cd");
							old_pos_pta_pin_req_yn_flag = results.getFormattedValue("pos_pta_pin_req_yn_flag");
							old_pos_pta_regex = results.getFormattedValue("pos_pta_regex");
							old_pos_pta_regex_bref = results.getFormattedValue("pos_pta_regex_bref");
							old_pos_pta_passthru_allow_yn_flag = results.getFormattedValue("pos_pta_passthru_allow_yn_flag");
							old_pos_pta_disable_debit_denial = results.getFormattedValue("pos_pta_disable_debit_denial");
							old_no_convenience_fee = results.getFormattedValue("no_convenience_fee");
							old_pos_pta_pref_auth_amt = results.getFormattedValue("pos_pta_pref_auth_amt");
							old_pos_pta_pref_auth_amt_max = results.getFormattedValue("pos_pta_pref_auth_amt_max");
							old_currency_cd = results.getFormattedValue("currency_cd");
							old_decline_until = results.getValue("decline_until", Date.class);
							old_whitelist = results.getFormattedValue("whitelist");
						} else {
							errorMap.put("pos_pta_id", "Database error loading data for pos_pta $pos_pta_id: unable to load old pos pta settings.");
							form.setAttribute("errorMap", errorMap);
							return;
						}

						//### create and immediately activate new pos_pta ###
						Map<String, Object> params = new HashMap<>();
						params.put("pos_id", old_pos_id);
						params.put("payment_subtype_id", old_payment_subtype_id);
						params.put("pos_pta_encrypt_key", old_pos_pta_encrypt_key);
						params.put("pos_pta_encrypt_key2", old_pos_pta_encrypt_key2);
						params.put("payment_subtype_key_id", old_payment_subtype_key_id);
						params.put("pos_pta_device_serial_cd", old_pos_pta_device_serial_cd);
						params.put("pos_pta_pin_req_yn_flag", old_pos_pta_pin_req_yn_flag);
						params.put("pos_pta_regex", old_pos_pta_regex);
						params.put("pos_pta_regex_bref", old_pos_pta_regex_bref);
						params.put("authority_payment_mask_id", old_authority_payment_mask_id);
						params.put("pos_pta_passthru_allow_yn_flag", old_pos_pta_passthru_allow_yn_flag);
						params.put("pos_pta_disable_debit_denial", old_pos_pta_disable_debit_denial);
						params.put("pos_pta_pref_auth_amt", old_pos_pta_pref_auth_amt);
						params.put("pos_pta_pref_auth_amt_max", old_pos_pta_pref_auth_amt_max);
						params.put("currency_cd", old_currency_cd);
						params.put("pos_pta_activation_ts", now_ts);
						params.put("no_convenience_fee", old_no_convenience_fee);
						params.put("decline_until", old_decline_until);
						params.put("whitelist", old_whitelist);
						DataLayerMgr.executeCall(conn, "INSERT_POS_PTA", params);
						long new_pos_pta_id = ConvertUtils.getLong(params.get("pos_pta_id"));

						//### update priorities to correct values ###
						result = DataLayerMgr.executeCall(conn, "UPDATE_POS_PTA_PRIORITIES", new Object[] {pos_pta_id,new_pos_pta_id,new_pos_pta_id,pos_pta_id,pos_pta_id,new_pos_pta_id});
						if (result != null && ((Integer)result[0]).intValue() > 0) {
							//no-op
						} else {
							errorMap.put("pos_pta_id", "Database error updating pos_pta " + pos_pta_id + ", " + new_pos_pta_id + ": unable to update pos pta priorities.");
							form.setAttribute("errorMap", errorMap);
							return;
						}

						pos_pta_id = new_pos_pta_id; //#pos_pta we want to update has changed
					}

				}

				//### update data ###
				Long activation_ts = (toggle_cd < -1 || toggle_cd == 0) ? !StringHelper.isBlank(toggle_date) ? pos_pta_ts : null : null;
				Long deactivation_ts = (toggle_cd > 0 || toggle_cd == -1) ? !StringHelper.isBlank(toggle_date) ? pos_pta_ts : 253402232400000L/*MAX_DATE*/: null;
				Map<String, Object> params = new HashMap<>();
				params.put("pos_pta_id", pos_pta_id);
				params.put("pos_pta_encrypt_key", (!(StringHelper.isBlank(pos_pta_encrypt_key)) ? ((!(StringHelper.isBlank(pos_pta_encrypt_key_hex)) && "1".equals(pos_pta_encrypt_key_hex)) ? pos_pta_encrypt_key : StringHelper.encodeHexString(pos_pta_encrypt_key)) : null));
				params.put("pos_pta_encrypt_key2", (!(StringHelper.isBlank(pos_pta_encrypt_key2)) ? ((!(StringHelper.isBlank(pos_pta_encrypt_key2_hex)) && "1".equals(pos_pta_encrypt_key2_hex)) ? pos_pta_encrypt_key2 : StringHelper.encodeHexString(pos_pta_encrypt_key2)) : null));
				params.put("payment_subtype_key_id", ("N/A".equalsIgnoreCase(payment_subtype_key_id) ? null : payment_subtype_key_id));
				params.put("pos_pta_device_serial_cd", (StringHelper.isBlank(pos_pta_device_serial_cd) ? null : (pos_pta_device_serial_cd)));
				params.put("pos_pta_pin_req_yn_flag", pos_pta_pin_req_yn_flag);
				params.put("pos_pta_regex", (StringHelper.isBlank(pos_pta_regex_final) ? null : (pos_pta_regex_final)));
				params.put("pos_pta_regex_bref", (StringHelper.isBlank(pos_pta_regex_bref_final) ? null : (pos_pta_regex_bref_final)));
				params.put("authority_payment_mask_id", (authority_payment_mask_id == -1 ? null : authority_payment_mask_id));
				params.put("pos_pta_passthru_allow_yn_flag", pos_pta_passthru_allow_yn_flag);
				params.put("pos_pta_disable_debit_denial", pos_pta_disable_debit_denial);
				params.put("pos_pta_pref_auth_amt", (StringHelper.isBlank(pos_pta_pref_auth_amt) ? null : pos_pta_pref_auth_amt));
				params.put("pos_pta_pref_auth_amt_max", (StringHelper.isBlank(pos_pta_pref_auth_amt_max) ? null : pos_pta_pref_auth_amt_max));
				params.put("currency_cd", currency_cd);
				params.put("pos_pta_activation_ts", activation_ts);
				params.put("pos_pta_deactivation_ts", deactivation_ts);
				params.put("no_convenience_fee", no_convenience_fee);
				params.put("decline_until", declineUntil);
				params.put("whitelist", whitelist);
				DataLayerMgr.executeCall(conn, "UPDATE_POS_PTA_SETTINGS", params);
				device = DeviceActions.loadDevice(device_id, request);
				okay = true;
			} finally {
				DbUtils.commitOrRollback(conn, okay, true);
			}
			form.set(DevicesConstants.PARAM_DEVICE_TYPE_ID, device.getTypeId());
			form.set(DevicesConstants.PARAM_SERIAL_NUMBER, device.getSerialNumber());
			form.setRedirectUri(new StringBuilder("/paymentConfig.i?device_id=").append(device_id).append("&userOP=payment_types").toString());
			return;
		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}

	}

}
