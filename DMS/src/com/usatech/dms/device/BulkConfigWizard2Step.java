/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.sql.ArraySQLType;
import simple.sql.SQLType;
import simple.text.StringUtils;

import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to page 2 of the bulk configuration wizard.
 */
public class BulkConfigWizard2Step extends AbstractStep
{	
	protected static final String SQL_GET_BULK_CONFIG_SEARCH_LIST_START = "SELECT /*+ALL_ROWS*/ * FROM ( "
			+ "SELECT DISTINCT device.device_id, device.device_name, device.device_serial_cd, device_type.device_type_desc, c.customer_name corp_customer_name, customer.customer_name, location.location_name, device.firmware_version, device_data.diag_app_version, device_data.ptest_version, host_equipment.host_equipment_mfgr bezel_mfgr, host_setting.host_setting_value, comm_method.comm_method_name, he_modem.host_equipment_mfgr modem_mfgr, to_char(device.last_activity_ts, 'MM/DD/YYYY HH24:MI:SS') last_activity_ts";

	protected static final String SQL_GET_BULK_CONFIG_SEARCH_LIST_FROM = " FROM device.device "
		   + "JOIN device.device_type ON device.device_type_id = device_type.device_type_id "
		   + "LEFT OUTER JOIN pss.pos ON device.device_id = pos.device_id "
		   + "LEFT OUTER JOIN location.location ON pos.location_id = location.location_id "
		   + "LEFT OUTER JOIN location.customer ON pos.customer_id = customer.customer_id "
		   + "LEFT OUTER JOIN device.comm_method ON device.comm_method_cd = comm_method.comm_method_cd "
		   + "LEFT OUTER JOIN device.device_data ON device.device_name = device_data.device_name "
		   + "LEFT OUTER JOIN device.host ON device.device_id = host.device_id AND host.host_type_id = 400 AND host.host_active_yn_flag = 'Y' "
			+ "LEFT OUTER JOIN report.eport ON device.device_serial_cd = eport.eport_serial_num "
			+ "LEFT OUTER JOIN report.vw_terminal_eport ON eport.eport_id = vw_terminal_eport.eport_id "
			+ "LEFT OUTER JOIN report.terminal ON vw_terminal_eport.terminal_id = terminal.terminal_id "
			+ "LEFT OUTER JOIN corp.customer c ON terminal.customer_id = c.customer_id "
			+ "LEFT OUTER JOIN REPORT.PRODUCT_TYPE ON terminal.PRODUCT_TYPE_ID = PRODUCT_TYPE.PRODUCT_TYPE_ID "
		   + "LEFT OUTER JOIN device.host_equipment ON host.host_equipment_id = host_equipment.host_equipment_id "
		   + "LEFT OUTER JOIN device.host_setting ON host.host_id = host_setting.host_id AND host_setting.host_setting_parameter = 'Application Version' "
			+ "LEFT OUTER JOIN (device.host h_modem JOIN device.host_type ht_modem ON h_modem.host_type_id = ht_modem.host_type_id) ON device.device_id = h_modem.device_id AND ht_modem.comm_method_cd IS NOT NULL AND h_modem.host_active_yn_flag = 'Y' "
			+ "LEFT OUTER JOIN device.host_equipment he_modem ON h_modem.host_equipment_id = he_modem.host_equipment_id ";

	protected static final String SQL_GET_BULK_CONFIG_SEARCH_LIST_WHERE = "WHERE device.device_active_yn_flag = 'Y' ";
	protected static final String SQL_END = " ORDER BY device.device_serial_cd ) sq_end ";
	protected static final String SQL_LIMIT = DialectResolver.isOracle() ? " WHERE rownum <= ?" : " LIMIT ?::bigint";
	protected static final String[] SQL_GET_BULK_CONFIG_SEARCH_INITIAL_COLUMNS = new String[] { "device_id", "Device Name", "Serial Number", "Device Type", "Reporting Customer", "Customer", "Location", "Firmware", "Diagnostic", "PTest", "Bezel Mfgr", "Bezel App Version", "Comm Method", "Modem Mfgr", "Last Activity" };
	
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	
    	StringBuilder errorMessage = new StringBuilder("");
    	String deviceType = form.getString(DevicesConstants.PARAM_DEVICE_TYPE_ID, false);
    	int deviceTypeId = -1;
    	if(!(StringHelper.isBlank(deviceType))){
    		//if value of deviceType non-numeric, just trap exception
    		try{deviceTypeId = new BigDecimal(deviceType).intValue();}catch(Exception e){}
    		
    	}
    	String action = form.getString("action", false);
    	String commMethod = form.getString(DevicesConstants.PARAM_COMM_METHOD, false);
    	String corpCustomerId = form.getString("corp_customer_id", false);
        String locationId = form.getString(DevicesConstants.PARAM_LOCATION_ID, false);
        String customerId = form.getString("customer_id", false);
        String firmwareVersion = form.getString(DevicesConstants.PARAM_FIRMWARE_VERSION, false);
        String firmwareVersionPrefix = form.getString(DevicesConstants.PARAM_FIRMWARE_VERSION_PREFIX, false);
        String diagAppVersion = form.getString(DevicesConstants.PARAM_DIAG_APP_VERSION, false);
        String diagAppVersionPrefix = form.getString(DevicesConstants.PARAM_DIAG_APP_VERSION_PREFIX, false);
        String ptestVersion = form.getString(DevicesConstants.PARAM_PTEST_VERSION, false);
        String ptestVersionPrefix = form.getString(DevicesConstants.PARAM_PTEST_VERSION_PREFIX, false);
        String bezelMfgrPrefix = form.getString(DevicesConstants.PARAM_BEZEL_MFGR_PREFIX, false);
        String bezelAppVerPrefix = form.getString(DevicesConstants.PARAM_BEZEL_APP_VER_PREFIX, false);
        String propertyListVersion = form.getString(DevicesConstants.PARAM_PROPERTY_LIST_VERSION, false);
        String serialNumberPrefix = form.getString("serial_number_prefix", false);
        String startSerialNumber = form.getString(DevicesConstants.PARAM_START_SERIAL_NUMBER, false);
        String numDevicesSeq = form.getString("num_devices_seq", false);
        String numDevices = form.getString("num_devices", false);
        String serialNumberListString = form.getString("serial_number_list", false);
		String parameterCode1 = form.getString("ps1", false);
		String parameterValue1 = form.getString("pv1", false);
		String parameterCode2 = form.getString("ps2", false);
		String parameterValue2 = form.getString("pv2", false);
		String productTypeId = form.getString("productType", false);
		String vipTypeId = form.getString("vipType", false);
		String vmcTypeId = form.getString("vmcType", false);
		Results rs;
        try
        {
			StringBuilder sqlString = new StringBuilder(SQL_GET_BULK_CONFIG_SEARCH_LIST_START);
			StringBuilder sqlFrom = new StringBuilder(SQL_GET_BULK_CONFIG_SEARCH_LIST_FROM);
			StringBuilder condition = new StringBuilder(SQL_GET_BULK_CONFIG_SEARCH_LIST_WHERE);
			List<String> columnNames = new ArrayList<>(SQL_GET_BULK_CONFIG_SEARCH_INITIAL_COLUMNS.length);
			for(String cn : SQL_GET_BULK_CONFIG_SEARCH_INITIAL_COLUMNS)
				columnNames.add(cn);
			int sqlParamsCntr = -1;
			int sqlFromCntr = -1;
			List<Object> sqlParams = new ArrayList<Object>();
			List<SQLType> sqlTypes = new ArrayList<SQLType>();
			if(!StringHelper.isBlank(vipTypeId)) {
				sqlString.append(", dsVip.device_setting_value dsVip");
				//columnNames.add("Gx Vendor Interface Type");// .replaceAll("[^#-~ !]", "_")
				sqlFrom.append(" LEFT OUTER JOIN device.device_setting dsVip ON device.device_id = dsVip.device_id AND dsVip.device_setting_parameter_cd = ?");
				sqlFromCntr++;
				sqlParamsCntr++;
				sqlParams.add(sqlFromCntr, "241");
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				condition.append(" AND dsVip.device_setting_value = ?");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, vipTypeId);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
			}
			if(!StringHelper.isBlank(vmcTypeId)) {
				sqlString.append(", dsVmc.device_setting_value dsVmc");
				//columnNames.add("G9/Edge VMC Interface Type");// .replaceAll("[^#-~ !]", "_")
				sqlFrom.append(" LEFT OUTER JOIN device.device_setting dsVmc ON device.device_id = dsVmc.device_id AND dsVmc.device_setting_parameter_cd = ?");
				sqlFromCntr++;
				sqlParamsCntr++;
				sqlParams.add(sqlFromCntr, "1500");
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				condition.append(" AND dsVmc.device_setting_value = ?");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, vmcTypeId);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
			}
			if(!StringHelper.isBlank(parameterCode1)) {
				sqlString.append(", ds1.device_setting_value ds1");
				columnNames.add("Parameter " + parameterCode1);// .replaceAll("[^#-~ !]", "_")
				sqlFrom.append(" LEFT OUTER JOIN device.device_setting ds1 ON device.device_id = ds1.device_id AND ds1.device_setting_parameter_cd = ?");
				sqlFromCntr++;
				sqlParamsCntr++;
				sqlParams.add(sqlFromCntr, parameterCode1);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				if(!StringHelper.isBlank(parameterValue1)) {
					condition.append(" AND ds1.device_setting_value like ?");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, parameterValue1);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}
			if(!StringHelper.isBlank(parameterCode2)) {
				sqlString.append(", ds2.device_setting_value ds2");
				columnNames.add("Parameter " + parameterCode2);// .replaceAll("[^#-~ !]", "_")
				sqlFrom.append(" LEFT OUTER JOIN device.device_setting ds2 ON device.device_id = ds2.device_id AND ds2.device_setting_parameter_cd = ?");
				sqlFromCntr++;
				sqlParamsCntr++;
				sqlParams.add(sqlFromCntr, parameterCode2);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				if(!StringHelper.isBlank(parameterValue2)) {
					condition.append(" AND ds2.device_setting_value like ?");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, parameterValue2);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}
			if(deviceTypeId > -1) {
				condition.append(" AND device.device_type_id = ?");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, String.valueOf(deviceTypeId));
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.NUMERIC));
			}
			if(!StringHelper.isBlank(locationId)) {
				condition.append(" AND location.location_id = CAST (? AS numeric)");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, locationId);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.NUMERIC));
			}
			if(!StringHelper.isBlank(customerId)) {
				condition.append(" AND customer.customer_id = CAST (? AS numeric)");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, customerId);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.NUMERIC));
			}
			if(!StringHelper.isBlank(corpCustomerId)) {
				condition.append(" AND terminal.customer_id = CAST (? AS numeric)");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, corpCustomerId);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.NUMERIC));
			}

			if(!StringHelper.isBlank(firmwareVersion)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(firmwareVersion))
					condition.append(" AND device.firmware_version IS NULL");
				else {
					condition.append(" AND device.firmware_version = ?");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, firmwareVersion);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(productTypeId)) {
				condition.append(" AND PRODUCT_TYPE.PRODUCT_TYPE_ID = ?");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, productTypeId);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
			}

			if(!StringHelper.isBlank(firmwareVersionPrefix)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(firmwareVersionPrefix))
					condition.append(" AND device.firmware_version IS NULL");
				else {
					condition.append(" AND device.firmware_version LIKE ? || '%'");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, firmwareVersionPrefix);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(commMethod)) {
				condition.append(" AND device.comm_method_cd = ?");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, commMethod);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
			}

			if(!StringHelper.isBlank(diagAppVersion)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(diagAppVersion))
					condition.append(" AND device_data.diag_app_version IS NULL");
				else {
					condition.append(" AND device_data.diag_app_version = ?");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, diagAppVersion);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(diagAppVersionPrefix)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(diagAppVersionPrefix))
					condition.append(" AND device_data.diag_app_version IS NULL");
				else {
					condition.append(" AND device_data.diag_app_version LIKE ? || '%'");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, diagAppVersionPrefix);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(ptestVersion)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(ptestVersion))
					condition.append(" AND device_data.ptest_version IS NULL");
				else {
					condition.append(" AND device_data.ptest_version = ?");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, ptestVersion);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(ptestVersionPrefix)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(ptestVersionPrefix))
					condition.append(" AND device_data.ptest_version IS NULL");
				else {
					condition.append(" AND device_data.ptest_version LIKE ? || '%'");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, ptestVersionPrefix);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(bezelMfgrPrefix)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(bezelMfgrPrefix))
					condition.append(" AND host_equipment.host_equipment_mfgr IS NULL");
				else {
					condition.append(" AND host_equipment.host_equipment_mfgr LIKE ? || '%'");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, bezelMfgrPrefix);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(bezelAppVerPrefix)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(bezelAppVerPrefix))
					condition.append(" AND host_setting.host_setting_value IS NULL");
				else {
					condition.append(" AND host_setting.host_setting_value LIKE ? || '%'");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, bezelAppVerPrefix);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(propertyListVersion)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(propertyListVersion))
					condition.append(" AND device_data.property_list_version IS NULL");
				else {
					condition.append(" AND device_data.property_list_version = ?");
	        		sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, propertyListVersion);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}
			}

			if(!StringHelper.isBlank(serialNumberPrefix)) {
				condition.append(" AND device.device_serial_cd LIKE ?");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, serialNumberPrefix + "%");
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
	        }

			if(!StringHelper.isBlank(serialNumberListString)) {
				condition.append(DialectResolver.isOracle() ? " AND device.device_serial_cd MEMBER OF ?" : " AND device.device_serial_cd = ANY(?)");
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, serialNumberListString.replace('\n', ',').replace("\r", "").replace(" ", ""));
				sqlTypes.add(sqlParamsCntr, new ArraySQLType(new SQLType(Types.VARCHAR)));
				sqlString.append(sqlFrom).append(condition.toString()).append(SQL_END);
			} else {
				int intNumDevices = -1;
				String startSerialHead = form.getString("start_serial_head", false);
				String endSerialNum = null;
				if((startSerialHead != null) && (startSerialHead.equalsIgnoreCase(""))) {
					startSerialHead = "%";
				}

				if(!(StringHelper.isBlank(startSerialNumber))) {
					startSerialNumber = padSer(startSerialHead, startSerialNumber, deviceTypeId);
				}

				if(!(StringHelper.isBlank(startSerialNumber)) && !(StringHelper.isBlank(numDevicesSeq))) {
					if(deviceTypeId == DeviceType.EDGE.getValue()) {
						BigDecimal tempEndSerialNum = (new BigDecimal(startSerialNumber.substring(3))).add((new BigDecimal(numDevicesSeq)).add(new BigDecimal(-1)));
						endSerialNum = (padSer(startSerialHead, tempEndSerialNum.toString(), deviceTypeId));
					} else {
						BigDecimal tempEndSerialNum = (new BigDecimal(startSerialNumber.substring(2))).add((new BigDecimal(numDevicesSeq)).add(new BigDecimal(-1)));
						endSerialNum = (padSer(startSerialHead, tempEndSerialNum.toString(), deviceTypeId));
					}
				}

				if(StringHelper.isBlank(numDevices)) {
					intNumDevices = 99;
				} else {
					intNumDevices = new BigDecimal(numDevices).intValue();
				}

				if(intNumDevices > 999999 && !"Download CSV".equalsIgnoreCase(action)) {
					errorMessage.append("<br/><br/><font color=\"red\">Sorry, I cannot update more than 999999 devices at a time.<br/><br/>Please refine your search criteria to be more specific.</font><br/><br/>");
					errorMessage.append("<input type=\"button\" value=\"< Back\" onClick=\"javascript:history.go(-1);\"><br/><br/>");
					request.setAttribute("errorMessage", errorMessage.toString());
					return;
				}

				if(!StringHelper.isBlank(startSerialNumber)) {
					condition.append(" AND device.device_serial_cd >= ?");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, startSerialNumber);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				} else {
					condition.append(" AND device.device_serial_cd like ?");
					sqlParamsCntr++;
					String tempStartSerialHead = (startSerialHead + "%");
					sqlParams.add(sqlParamsCntr, tempStartSerialHead);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}

				if(endSerialNum != null && !StringHelper.isBlank(endSerialNum.toString())) {
					condition.append(" AND device.device_serial_cd <= ?");
					sqlParamsCntr++;
					sqlParams.add(sqlParamsCntr, endSerialNum);
					sqlTypes.add(sqlParamsCntr, new SQLType(Types.VARCHAR));
				}

				request.setAttribute("start_serial_head", startSerialHead);
				sqlString.append(sqlFrom).append(condition.toString()).append(SQL_END).append(SQL_LIMIT);
				sqlParamsCntr++;
				sqlParams.add(sqlParamsCntr, intNumDevices);
				sqlTypes.add(sqlParamsCntr, new SQLType(Types.NUMERIC));
			}

			Object[] sqlParamsArray = sqlParams.toArray();
			SQLType[] sqlTypesArray = sqlTypes.toArray(new SQLType[sqlTypes.size()]);
			rs = DataLayerMgr.executeSQLx("OPER", sqlString.toString(), sqlParamsArray, sqlTypesArray, columnNames.toArray(new String[columnNames.size()]));
	        if ("Download CSV".equalsIgnoreCase(action)) {
        		response.setContentType("application/force-download");
        		response.setHeader("Content-Transfer-Encoding", "binary"); 
        		response.setHeader("Content-Disposition","attachment;filename=\"devices.csv.txt\"");
        		PrintWriter out = response.getWriter();
				StringUtils.writeCSV(rs, out, true);
        		out.flush();
    	    	out.close();
        	} else {
				request.setAttribute("bulk_config_wizard_2_search_results", rs);
        	}
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }
    
    /**
     * Pad the incoming serial number depending on device type.
     * @param head
     * @param num
     * @param deviceType
     * @return
     */
    static String padSer(String head, String num, int deviceType){
    	StringBuilder tempNum = new StringBuilder("");
    	String newNum = null;
    	String zeros = "000000000";
    	int numLengthCheck = 0;
    	if(DeviceType.EDGE.getValue() == deviceType){
    		numLengthCheck = 8;
    	}else{
    		numLengthCheck = 6;
    	}
    	if(!(StringHelper.isBlank(num)) && num.length() <= numLengthCheck){
			tempNum.append(zeros.substring(0, numLengthCheck-num.length()));
			tempNum.append(num);
			newNum =  new String(head+tempNum.toString());
		}
    	return newNum;
    }
    
    
}
