/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import com.usatech.dms.user.DmsMenuManager;
import com.usatech.layers.common.device.DevicesConstants;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class ViewDevicesMenuStep extends AbstractStep {
    
    private static final String PARAM_MENU = "menu";
    
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        DmsMenuManager menuManager = (DmsMenuManager)request.getSession().getAttribute(DmsMenuManager.ATTRIBUTE_MENU_MANAGER);
        if(!menuManager.showTopDevicesView()) {
            return;
        }
        String menu = form.getString(PARAM_MENU, false);
        if(menu == null)
            menu = "";
        if("location".equals(menu))
            viewDevicesByLocation(request);
        else if("customer".equals(menu))
            viewDevicesByCustomer(request);
        request.setAttribute(PARAM_MENU, menu);
    }
    
    public void viewDevicesByCustomer(HttpServletRequest request) throws ServletException  {
        try {
            //Get customers whose names begin with digit number.
            List<String> customersByDigitName = new ArrayList<String>();
            Integer[] customer_id_param = new Integer[1];
            Results customer_by_numeric_name = (Results) DataLayerMgr.executeQuery("GET_CUSTOMER_BY_NUMERIC_NAME", null, false);
            setCustomerSubMenu(customer_by_numeric_name, customer_id_param, customersByDigitName);
            request.setAttribute("customersByDigitName", customersByDigitName);
            
            //Get customers whose names begin with letters.
            String[] customer_name_param = new String[1];
            for(String l : DevicesConstants.letter) {
                List<String> customersByLetterName = new ArrayList<String>();
                customer_name_param[0] = l.toUpperCase() + "%";
                Results customer_by_char_name = (Results) DataLayerMgr.executeQuery("GET_CUSTOMER_BY_CHAR_NAME", customer_name_param, false);
                setCustomerSubMenu(customer_by_char_name, customer_id_param, customersByLetterName);
                request.setAttribute("customersByLetterName" + l, customersByLetterName);
            }
                
        } catch(Exception e) {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }
    
    public void setCustomerSubMenu(Results result, Integer[] customer_id_param, List<String> customerMenuItem) throws ServletException  {
        try {
            while(result.next()) {
                int customer_id = result.getValue("customer_id", Integer.class);
                customer_id_param[0] = customer_id;
                Results customer_menu_item = (Results) DataLayerMgr.executeQuery("GET_CUSTOMER_MENU_SUB_ITEM", customer_id_param, false);
                while(customer_menu_item.next()) {
                    int count = customer_menu_item.getValue("count", Integer.class);
                    if(count > 0) {
	                    StringBuilder menuItem = new StringBuilder();
	                    menuItem.append("text=");
	                    menuItem.append(result.getFormattedValue("name_city"));
	                    menuItem.append(" ");
	                    menuItem.append(result.getFormattedValue("customer_state_cd"));
                        menuItem.append(" (");
                        menuItem.append(count);
                        menuItem.append(");url=deviceList.i?customer_id=");
                        menuItem.append(result.getFormattedValue("customer_id"));
                        menuItem.append(";");
                        customerMenuItem.add(menuItem.toString());
                    }
                }
            }
        } catch(Exception e) {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }
    
    
    
    public void viewDevicesByLocation(HttpServletRequest request) throws ServletException  {
        try {
            //Get locations whose names begin with digit number.
            List<String> locationsByDigitName = new ArrayList<String>();
            Integer[] location_id_param = new Integer[1];
            Results location_by_numeric_name = (Results) DataLayerMgr.executeQuery("GET_LOCATION_BY_NUMERIC_NAME", null, false);
            setLocationSubMenu(location_by_numeric_name, location_id_param, locationsByDigitName);
            request.setAttribute("locationsByDigitName", locationsByDigitName);
            
          //Get locations whose names begin with letters.
            String[] location_name_param = new String[1];
            for(String l : DevicesConstants.letter) {
                List<String> locationsByLetterName = new ArrayList<String>();
                location_name_param[0] = l.toUpperCase() + "%";
                Results location_by_char_name = (Results) DataLayerMgr.executeQuery("GET_LOCATION_BY_CHAR_NAME", location_name_param, false);
                setLocationSubMenu(location_by_char_name, location_id_param, locationsByLetterName);
                request.setAttribute("locationsByLetterName" + l, locationsByLetterName);
            }
        } catch(Exception e) {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }
    
    public void setLocationSubMenu(Results result, Integer[] location_id_param, List<String> locationMenuItem) throws ServletException  {
        try {
            while(result.next()) {
                int location_id = result.getValue("location_id", Integer.class);
                location_id_param[0] = location_id;
                Results location_menu_item = (Results) DataLayerMgr.executeQuery("GET_LOCATION_MENU_SUB_ITEM", location_id_param, false);
                while(location_menu_item.next()) {
                    int count = location_menu_item.getValue("count", Integer.class);
                    if(count > 0) {
	                    StringBuilder menuItem = new StringBuilder();
	                    menuItem.append("text=");
	                    menuItem.append(result.getFormattedValue("name_city"));
	                    menuItem.append(" ");
	                    menuItem.append(result.getFormattedValue("location_state_cd"));
	                    menuItem.append(" ");
	                    menuItem.append(result.getFormattedValue("location_country_cd"));
	                    menuItem.append(" (");
	                    menuItem.append(count);
	                    menuItem.append(");url=deviceList.i?location_id=");
	                    menuItem.append(result.getFormattedValue("location_id"));
	                    menuItem.append(";");
	                    locationMenuItem.add(menuItem.toString());
                    }
                }
            }
        } catch(Exception e) {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }
}
