/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class TerminalInfoStep extends AbstractStep
{
    // by default, sort by "batch id" ascending
    private static final String DEFAULT_SORT_INDEX = "1";
    private static final String SQL_START = "SELECT terminal_batch.terminal_batch_id," +
            "terminal_batch.terminal_id," +
            "terminal_batch.terminal_batch_num," +
            "to_char(terminal_batch.terminal_batch_open_ts, 'MM/DD/YYYY HH24:MI:SS')," +
            "to_char(terminal_batch.terminal_batch_close_ts, 'MM/DD/YYYY HH24:MI:SS')," +
            "to_char(terminal_batch.created_ts, 'MM/DD/YYYY HH24:MI:SS'), " +
            "to_char(terminal_batch.last_updated_ts, 'MM/DD/YYYY HH24:MI:SS') ";
    
    private static final String SQL_BASE = "FROM pss.terminal_batch WHERE terminal_batch.terminal_id = CAST (? AS numeric) ";
    
    private static final String[] SORT_FIELDS = {"terminal_batch.terminal_batch_id", // batch id
        "terminal_batch.terminal_batch_num", // batch num
        "terminal_batch.terminal_batch_open_ts", // time opened
        "terminal_batch.terminal_batch_close_ts", // time closed
        };

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String terminal_id = form.getString(DevicesConstants.PARAM_TERMINAL_ID, false);
        if (terminal_id == null || terminal_id.length() == 0)
        {
            request.setAttribute("missingParam", true);
            return;
        }
        String sql = SQL_BASE;// + terminal_id;
        String paramTotalCount = PaginationUtil.getTotalField(null);
        String paramPageIndex = PaginationUtil.getIndexField(null);
        String paramPageSize = PaginationUtil.getSizeField(null);
        String paramSortIndex = PaginationUtil.getSortField(null);

        try
        {
            // pagination parameters
            int totalCount = form.getInt(paramTotalCount, false, -1);
            if (totalCount == -1)
            {
                // if the total count is not retrieved yet, get it now
                Results total = DataLayerMgr.executeSQL("OPER", "SELECT COUNT(1) " + sql, new Object[]{terminal_id}, null);
                if (total.next())
                {
                    totalCount = total.getValue(1, int.class);
                }
                else
                {
                    totalCount = 0;
                }
                request.setAttribute(paramTotalCount, String.valueOf(totalCount));
            }

            int pageIndex = form.getInt(paramPageIndex, false, 1);
            int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
            int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
            int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

            String sortIndex = form.getString(paramSortIndex, false);
            sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
            String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

			String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() rnum from (" + SQL_START
						+ sql + orderBy + ") pagination_temp LIMIT ?::bigint) sq_end where rnum  >= ?::bigint ";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + sql + orderBy
						+ ") pagination_temp where ROWNUM <= ?) where rnum  >= ? ";
			}

            Results results = DataLayerMgr.executeSQL("OPER", query, new Object[]{terminal_id, maxRowToFetch, minRowToFetch}, null);
            request.setAttribute("terminalbatchInfo", results);
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
