/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class PosptaTmplEntryEditFuncStep extends AbstractStep
{

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        int pos_pta_tmpl_id = form.getInt(DevicesConstants.PARAM_POS_PTA_TMPL_ID, false, -1);
        String errorMessage = null;
        if (pos_pta_tmpl_id <= 0)
        {
            errorMessage = "Required Parameter Not Found:pos_pta_tmpl_id";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        int pos_pta_tmpl_entry_id = 0;
        String pos_pta_tmpl_entry_id_str = request.getParameter("pos_pta_tmpl_entry_id");
        if (pos_pta_tmpl_entry_id_str == null || pos_pta_tmpl_entry_id_str.length() == 0)
        {
            errorMessage = "Required Parameter Not Found: pos_pta_tmpl_entry_id";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        pos_pta_tmpl_entry_id = Integer.parseInt(pos_pta_tmpl_entry_id_str);
        String userAction = form.getString("userAction", false);
        if (userAction == null || userAction.length() == 0)
        {
            errorMessage = "Required Parameter Not Found: userAction";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }

        if (userAction.equalsIgnoreCase("Delete"))
        {
            try
            {
                DataLayerMgr.executeUpdate("DELETE_TEMPLATE_ENTRY", new Object[] {pos_pta_tmpl_entry_id}, true);
            }
            catch (Exception e)
            {
                throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
            }
        }
        else if (userAction.equalsIgnoreCase("Save"))
        {
            // ### Update Template Entry ###
            StringBuilder nonReqMessage = new StringBuilder();
            String payment_action_type_cd = request.getParameter("payment_action_type_cd");
            if (StringHelper.isBlank(payment_action_type_cd))
            {
                errorMessage = "Required Parameter Not Found: payment_action_type_cd";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: payment_action_type_cd = " + payment_action_type_cd + "\n");
            }
            String payment_entry_method_cd = request.getParameter("payment_entry_method_cd");
            if (payment_entry_method_cd == null || payment_entry_method_cd.length() == 0)
            {
                errorMessage = "Required Parameter Not Found: payment_entry_method_cd";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: payment_entry_method_cd = " + payment_entry_method_cd + "\n");
            }
            String pos_pta_pin_req_yn_flag = request.getParameter("pos_pta_pin_req_yn_flag");
            if (pos_pta_pin_req_yn_flag == null || pos_pta_pin_req_yn_flag.length() == 0)
            {
                errorMessage = "Required Parameter Not Found: pos_pta_pin_req_yn_flag";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: pos_pta_pin_req_yn_flag = " + pos_pta_pin_req_yn_flag + "\n");
            }
            String payment_subtype_id_str = request.getParameter("payment_subtype_id");
            if (payment_subtype_id_str == null || payment_subtype_id_str.length() == 0)
            {
                errorMessage = "Required Parameter Not Found:payment_subtype_id";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: payment_subtype_id = " + payment_subtype_id_str + "\n");
            }
            String payment_subtype_key_id_str = request.getParameter("payment_subtype_key_id");
            if (payment_subtype_key_id_str == null || payment_subtype_key_id_str.length() == 0)
            {
                errorMessage = "Required Parameter Not Found:payment_subtype_key_id";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: payment_subtype_key_id = " + payment_subtype_key_id_str + "\n");
            }
            String pos_pta_priority_str = request.getParameter("pos_pta_priority");
            if (pos_pta_priority_str == null || pos_pta_priority_str.length() == 0)
            {
                errorMessage = "Required Parameter Not Found:pos_pta_priority_str";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: pos_pta_priority = " + pos_pta_priority_str + "\n");
            }
            String currency_cd = request.getParameter("currency_cd");
            if (currency_cd == null || currency_cd.length() == 0)
            {
                errorMessage = "Required Parameter Not Found: currency_cd";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: currency_cd = " + currency_cd + "\n");
            }
            String pos_pta_passthru_allow_yn_flag = request.getParameter("pos_pta_passthru_allow_yn_flag");
            if (pos_pta_passthru_allow_yn_flag == null || pos_pta_passthru_allow_yn_flag.length() == 0)
            {
                errorMessage = "Required Parameter Not Found: pos_pta_passthru_allow_yn_flag";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: pos_pta_passthru_allow_yn_flag = " + pos_pta_passthru_allow_yn_flag + "\n");
            }
            String pos_pta_disable_debit_denial = request.getParameter("pos_pta_disable_debit_denial");
            if (pos_pta_disable_debit_denial == null || pos_pta_disable_debit_denial.length() == 0)
            {
                errorMessage = "Required Parameter Not Found: pos_pta_disable_debit_denial";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: pos_pta_disable_debit_denial = " + pos_pta_disable_debit_denial + "\n");
            }
            
            String no_convenience_fee = request.getParameter("no_convenience_fee");
            if (no_convenience_fee == null || no_convenience_fee.length() == 0)
            {
                errorMessage = "Required Parameter Not Found: no_convenience_fee";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            else
            {
                nonReqMessage.append("Required Param: no_convenience_fee = " + no_convenience_fee + "\n");
            }

            String pos_pta_device_serial_cd = request.getParameter("pos_pta_device_serial_cd");
            if (pos_pta_device_serial_cd != null && pos_pta_device_serial_cd.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_device_serial_cd = " + pos_pta_device_serial_cd + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_device_serial_cd = \n");
            }

            String pos_pta_encrypt_key = request.getParameter("pos_pta_encrypt_key");
            if (pos_pta_encrypt_key != null && pos_pta_encrypt_key.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_encrypt_key = " + pos_pta_encrypt_key + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_encrypt_key = \n");
            }
            String pos_pta_encrypt_key2 = request.getParameter("pos_pta_encrypt_key2");
            if (pos_pta_encrypt_key2 != null && pos_pta_encrypt_key2.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_encrypt_key2 = " + pos_pta_encrypt_key2 + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_encrypt_key2 = \n");
            }

            String pos_pta_encrypt_key_hex_str = request.getParameter("pos_pta_encrypt_key_hex");
            if (pos_pta_encrypt_key_hex_str != null && pos_pta_encrypt_key_hex_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_encrypt_key_hex = " + pos_pta_encrypt_key_hex_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_encrypt_key_hex = \n");
            }

            String pos_pta_encrypt_key2_hex_str = request.getParameter("pos_pta_encrypt_key2_hex");
            if (pos_pta_encrypt_key2_hex_str != null && pos_pta_encrypt_key2_hex_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_encrypt_key2_hex = " + pos_pta_encrypt_key2_hex_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_encrypt_key2_hex = \n");
            }

            String pos_pta_activation_oset_hr_str = request.getParameter("pos_pta_activation_oset_hr");
            if (pos_pta_activation_oset_hr_str != null && pos_pta_activation_oset_hr_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_activation_oset_hr = " + pos_pta_activation_oset_hr_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_activation_oset_hr = \n");
                pos_pta_activation_oset_hr_str = null;
            }

            String pos_pta_deactivation_oset_hr_str = request.getParameter("pos_pta_deactivation_oset_hr");
            if (pos_pta_deactivation_oset_hr_str != null && pos_pta_deactivation_oset_hr_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_deactivation_oset_hr = " + pos_pta_deactivation_oset_hr_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_deactivation_oset_hr = \n");
                pos_pta_deactivation_oset_hr_str = null;
            }

            String authority_payment_mask_id_str = request.getParameter("authority_payment_mask_id");
            if (authority_payment_mask_id_str != null && authority_payment_mask_id_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: authority_payment_mask_id = " + authority_payment_mask_id_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: authority_payment_mask_id = \n");
            }

            String pos_pta_pref_auth_amt_str = request.getParameter("pos_pta_pref_auth_amt");
            if (pos_pta_pref_auth_amt_str != null && pos_pta_pref_auth_amt_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_pref_auth_amt = " + pos_pta_pref_auth_amt_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_pref_auth_amt = \n");
                pos_pta_pref_auth_amt_str = null;
            }

            String pos_pta_pref_auth_amt_max_str = request.getParameter("pos_pta_pref_auth_amt_max");
            if (pos_pta_pref_auth_amt_max_str != null && pos_pta_pref_auth_amt_max_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: pos_pta_pref_auth_amt_max = " + pos_pta_pref_auth_amt_max_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: pos_pta_pref_auth_amt_max = \n");
                pos_pta_pref_auth_amt_max_str = null;
            }

            String original_payment_subtype_id_str = request.getParameter("original_payment_subtype_id");
            if (original_payment_subtype_id_str != null && original_payment_subtype_id_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_payment_subtype_id = " + original_payment_subtype_id_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_payment_subtype_id = \n");
            }

            String original_payment_subtype_key_id_str = request.getParameter("original_payment_subtype_key_id");
            if (original_payment_subtype_key_id_str != null && original_payment_subtype_key_id_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_payment_subtype_key_id = " + original_payment_subtype_key_id_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_payment_subtype_key_id = \n");
            }

            String original_authority_payment_mask_id_str = request.getParameter("original_authority_payment_mask_id");
            if (original_authority_payment_mask_id_str != null && original_authority_payment_mask_id_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_authority_payment_mask_id = " + original_authority_payment_mask_id_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_authority_payment_mask_id = \n");
            }
            String original_pos_pta_device_serial_cd = request.getParameter("original_pos_pta_device_serial_cd");
            if (original_pos_pta_device_serial_cd != null && original_pos_pta_device_serial_cd.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_device_serial_cd = " + original_pos_pta_device_serial_cd + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_device_serial_cd = \n");
            }

            String original_pos_pta_encrypt_key = request.getParameter("original_pos_pta_encrypt_key");
            if (original_pos_pta_encrypt_key != null && original_pos_pta_encrypt_key.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_encrypt_key = " + original_pos_pta_encrypt_key + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_encrypt_key = \n");
            }

            String original_pos_pta_encrypt_key2 = request.getParameter("original_pos_pta_encrypt_key2");
            if (original_pos_pta_encrypt_key2 != null && original_pos_pta_encrypt_key2.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_encrypt_key2 = " + original_pos_pta_encrypt_key2 + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_encrypt_key2 = \n");
            }

            String original_pos_pta_pin_req_yn_flag = request.getParameter("original_pos_pta_pin_req_yn_flag");
            if (original_pos_pta_pin_req_yn_flag != null && original_pos_pta_pin_req_yn_flag.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_pin_req_yn_flag = " + original_pos_pta_pin_req_yn_flag + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_pin_req_yn_flag = \n");
            }

            String original_pos_pta_passthru_allow_yn_flag = request.getParameter("original_pos_pta_passthru_allow_yn_flag");
            if (original_pos_pta_passthru_allow_yn_flag != null && original_pos_pta_passthru_allow_yn_flag.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_passthru_allow_yn_flag = " + original_pos_pta_passthru_allow_yn_flag + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_passthru_allow_yn_flag = \n");
            }
            
            String original_pos_pta_disable_debit_denial = request.getParameter("original_pos_pta_disable_debit_denial");
            if (original_pos_pta_disable_debit_denial != null && original_pos_pta_disable_debit_denial.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_disable_debit_denial = " + original_pos_pta_disable_debit_denial + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_disable_debit_denial = \n");
            }
            
            String original_no_convenience_fee = request.getParameter("original_no_convenience_fee");
            if (original_no_convenience_fee != null && original_no_convenience_fee.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_no_convenience_fee = " + original_no_convenience_fee + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_no_convenience_fee = \n");
            }

            String original_pos_pta_pref_auth_amt_str = request.getParameter("original_pos_pta_pref_auth_amt");
            if (original_pos_pta_pref_auth_amt_str != null && original_pos_pta_pref_auth_amt_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_pref_auth_amt = " + original_pos_pta_pref_auth_amt_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_pref_auth_amt = \n");
            }

            String original_pos_pta_pref_auth_amt_max_str = request.getParameter("original_pos_pta_pref_auth_amt_max");
            if (original_pos_pta_pref_auth_amt_max_str != null && original_pos_pta_pref_auth_amt_max_str.length() > 0)
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_pref_auth_amt_max = " + original_pos_pta_pref_auth_amt_max_str + "\n");
            }
            else
            {
                nonReqMessage.append("Non-Required Param: original_pos_pta_pref_auth_amt_max = \n");
            }
            request.setAttribute("nonReqMessage", nonReqMessage.toString());
            // ### validate/parse input ###
            StringBuilder errorMsg = new StringBuilder();
            if (pos_pta_encrypt_key_hex_str != null && pos_pta_encrypt_key_hex_str.equals("1") && !Pattern.matches("^[a-fA-F0-9]*$", pos_pta_encrypt_key))
            {
                errorMsg.append("<li>Encryption key '" + pos_pta_encrypt_key + "' not a valid Hex string");
            }

            if (pos_pta_encrypt_key2_hex_str != null && pos_pta_encrypt_key2_hex_str.equals("1") && !Pattern.matches("^[a-fA-F0-9]*$", pos_pta_encrypt_key2))
            {
                errorMsg.append("<li>Encryption key 2 '" + pos_pta_encrypt_key2 + "' not a valid Hex string");
            }
            if (!Pattern.matches("^[NY]$", pos_pta_pin_req_yn_flag))
            {
                errorMsg.append("<li>Invalid PIN required flag 'pos_pta_pin_req_yn_flag': must be Y or N");
            }
            if (!Pattern.matches("^[NY]$", pos_pta_passthru_allow_yn_flag))
            {
                errorMsg.append("<li>Invalid authority pass-through required flag 'pos_pta_passthru_allow_yn_flag': must be Y or N");
            }
            if (!Pattern.matches("^[NY]$", pos_pta_disable_debit_denial))
            {
                errorMsg.append("<li>Invalid disable debit denial 'pos_pta_disable_debit_denial': must be Y or N");
            }
            if (!Pattern.matches("^[NY]$", no_convenience_fee))
            {
                errorMsg.append("<li>Invalid No Two-Tier Pricing 'no_convenience_fee': must be Y or N");
            }
            if (pos_pta_activation_oset_hr_str != null && !Pattern.matches("^[-]?\\d+$", pos_pta_activation_oset_hr_str))
            {
                errorMsg.append("<li>Invalid Activation Offset: must be numeric (" + pos_pta_activation_oset_hr_str + ")");
            }
            if (pos_pta_deactivation_oset_hr_str != null && !Pattern.matches("^[-]?\\d+$", pos_pta_deactivation_oset_hr_str))
            {
                errorMsg.append("<li>Invalid Deactivation Offset: must be numeric (" + pos_pta_deactivation_oset_hr_str + ")");
            }
            if (pos_pta_priority_str != null && !Pattern.matches("[1-9][0-9]*", pos_pta_priority_str))
            {
                errorMsg.append("<li>Invalid Priority: must be numeric");
            }
            boolean isNumber = true;
            if (pos_pta_pref_auth_amt_str == null)
            {
                isNumber = false;
            }
            if (pos_pta_pref_auth_amt_max_str == null)
            {
                isNumber = false;
            }
            if (pos_pta_pref_auth_amt_str != null && !Pattern.matches("^[0-9]+(\\.[0-9]{0,2})?$", pos_pta_pref_auth_amt_str))
            {
                errorMsg.append("<li>Override Amount '" + pos_pta_pref_auth_amt_str + "' not a valid number");
                isNumber = false;
            }
            if (pos_pta_pref_auth_amt_max_str != null && !Pattern.matches("^[0-9]+(\\.[0-9]{0,2})?$", pos_pta_pref_auth_amt_max_str))
            {
                errorMsg.append("<li>Override Limit '" + pos_pta_pref_auth_amt_max_str + "' not a valid number");
                isNumber = false;
            }
            if (isNumber && Float.parseFloat(pos_pta_pref_auth_amt_str) > Float.parseFloat(pos_pta_pref_auth_amt_max_str))
            {
                errorMsg.append("<li>Override amount can not be greater than limit! (" + pos_pta_pref_auth_amt_str + " > " + pos_pta_pref_auth_amt_max_str + ")");
            }
            if (pos_pta_pref_auth_amt_str != null && pos_pta_pref_auth_amt_max_str == null)
            {
                errorMsg.append("<li>Override limit is required with override amount!");
            }
            if (pos_pta_pref_auth_amt_str == null && pos_pta_pref_auth_amt_max_str != null)
            {
                errorMsg.append("<li>Override amount is required with override limit!");
            }

            try
            {
                Results checkPriority = DataLayerMgr.executeQuery("CHECK_PRIORITY", new Object[] {pos_pta_tmpl_id, pos_pta_tmpl_entry_id, Integer.parseInt(pos_pta_priority_str),
                        payment_action_type_cd, payment_entry_method_cd}, true);
                if (checkPriority != null && checkPriority.next())
                {
                    errorMsg.append("<li>Duplicate Category Priority Value: " + pos_pta_priority_str + "!");
                }

                if (errorMsg.length() > 0)
                {
                    request.setAttribute("errorMessage", errorMsg.toString());
                    return;
                }

                if (pos_pta_tmpl_entry_id > 0)
                {
                    // ### update data ###
                    if (pos_pta_encrypt_key != null && pos_pta_encrypt_key.length() > 0)
                    {
                        if (!(pos_pta_encrypt_key_hex_str != null && pos_pta_encrypt_key_hex_str.equalsIgnoreCase("1")))
                        {
                            pos_pta_encrypt_key = StringHelper.encodeHexString(pos_pta_encrypt_key);
                        }
                    }
                    if (pos_pta_encrypt_key2 != null && pos_pta_encrypt_key2.length() > 0)
                    {
                        if (!(pos_pta_encrypt_key2_hex_str != null && pos_pta_encrypt_key2_hex_str.equalsIgnoreCase("1")))
                        {
                            pos_pta_encrypt_key2 = StringHelper.encodeHexString(pos_pta_encrypt_key2);
                        }
                    }
                    // pos_pta_encrypt_key=
                    // (pos_pta_encrypt_key!=null)?((pos_pta_encrypt_key_hex_str!=null &&
                    // pos_pta_encrypt_key_hex_str.equals("1"))?pos_pta_encrypt_key:unpack("H*",
                    // pack("A*", $pos_pta_encrypt_key))):pos_pta_encrypt_key;
                    // pos_pta_encrypt_key2 =
                    // (pos_pta_encrypt_key2!=null)?((pos_pta_encrypt_key2_hex_str!=null &&
                    // pos_pta_encrypt_key2_hex_str.equals("1"))?pos_pta_encrypt_key2:unpack("H*",
                    // pack("A*", $pos_pta_encrypt_key2))):pos_pta_encrypt_key2;
                    Object[] params = new Object[] {pos_pta_device_serial_cd, pos_pta_encrypt_key, pos_pta_encrypt_key2, pos_pta_activation_oset_hr_str, pos_pta_deactivation_oset_hr_str,
                            payment_subtype_id_str, payment_subtype_key_id_str.equals("N/A") ? null : Integer.parseInt(payment_subtype_key_id_str), pos_pta_pin_req_yn_flag,
                            (authority_payment_mask_id_str==null || authority_payment_mask_id_str.length() == 0 || authority_payment_mask_id_str.equals("undefined"))?null:Integer.parseInt(authority_payment_mask_id_str), 
                            Integer.parseInt(pos_pta_priority_str), currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, pos_pta_pref_auth_amt_str,
                            pos_pta_pref_auth_amt_max_str, no_convenience_fee, pos_pta_tmpl_entry_id};
                    DataLayerMgr.executeUpdate("UPDATE_POST_PTA_TEMPLATE_ENTRY", params, true);

                }
                else
                {
                    // ### insert data ###
                    // pos_pta_encrypt_key=
                    // (pos_pta_encrypt_key!=null)?((pos_pta_encrypt_key_hex_str!=null &&
                    // pos_pta_encrypt_key_hex_str.equals("1"))?pos_pta_encrypt_key:unpack("H*",
                    // pack("A*", $pos_pta_encrypt_key))):pos_pta_encrypt_key;
                    // pos_pta_encrypt_key2 =
                    // (pos_pta_encrypt_key2!=null)?((pos_pta_encrypt_key2_hex_str!=null &&
                    // pos_pta_encrypt_key2_hex_str.equals("1"))?pos_pta_encrypt_key2:unpack("H*",
                    // pack("A*", $pos_pta_encrypt_key2))):pos_pta_encrypt_key2;
                    if (pos_pta_encrypt_key != null && pos_pta_encrypt_key.length() > 0)
                    {
                        if (!(pos_pta_encrypt_key_hex_str != null && pos_pta_encrypt_key_hex_str.equalsIgnoreCase("1")))
                        {
                            pos_pta_encrypt_key = StringHelper.encodeHexString(pos_pta_encrypt_key);
                        }
                    }
                    if (pos_pta_encrypt_key2 != null && pos_pta_encrypt_key2.length() > 0)
                    {
                        if (!(pos_pta_encrypt_key2_hex_str != null && pos_pta_encrypt_key2_hex_str.equalsIgnoreCase("1")))
                        {
                            pos_pta_encrypt_key2 = StringHelper.encodeHexString(pos_pta_encrypt_key);
                        }
                    }

                    Object[] params = new Object[] {pos_pta_tmpl_id, Integer.parseInt(payment_subtype_id_str), pos_pta_encrypt_key, pos_pta_activation_oset_hr_str, pos_pta_deactivation_oset_hr_str,
                            payment_subtype_key_id_str.equals("N/A") ? null : Integer.parseInt(payment_subtype_key_id_str), pos_pta_pin_req_yn_flag, pos_pta_device_serial_cd, pos_pta_encrypt_key2,
                                    (authority_payment_mask_id_str==null || authority_payment_mask_id_str.length() == 0 || authority_payment_mask_id_str.equals("undefined"))?null:Integer.parseInt(authority_payment_mask_id_str), 
                                            Integer.parseInt(pos_pta_priority_str), currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, pos_pta_pref_auth_amt_str,
                            pos_pta_pref_auth_amt_max_str, no_convenience_fee};
                    DataLayerMgr.executeUpdate("INSERT_POST_PTA_TEMPLATE_ENTRY", params, true);
                }
            }
            catch (Exception e)
            {
                throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
            }

        }
        else
        {
            errorMessage = "Unknown userAction Parameter: " + userAction;
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        request.setAttribute(DevicesConstants.PARAM_POS_PTA_TMPL_ID, pos_pta_tmpl_id);
    }

}
