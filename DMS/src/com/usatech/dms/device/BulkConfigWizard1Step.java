/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.NameValuePair;

import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.DeviceTypeList;
import com.usatech.dms.util.Helper;

/**
 * This step will build the non-global lists need for the first page of the bulk configuration wizard.
 */
public class BulkConfigWizard1Step extends AbstractStep
{
	public static final String ORA_QUERY_GET_DEVICE_PARAMS = "GET_DEVICE_PARAMS";
	public static final String ORA_GET_DEVICE_PARAM_CODE = "DEVICE_SETTING_PARAMETER_CD";
	public static final String ORA_GET_DEVICE_PARAM_NAME = "NAME";

	public static final String ORA_QUERY_GET_PRODUCT_TYPES = "GET_PRODUCT_TYPES";
	public static final String REQ_PARAM_PRODUCT_TYPES = "PRODUCT_TYPES";

	public static final String REQ_PARAM_VENDOR_INTERFACE_TYPE_PARAMS = "VENDOR_INTERFACE_TYPE_PARAMS";
	public static final String REQ_PARAM_VMC_INTERFACE_TYPE_PARAMS = "VMC_INTERFACE_TYPE_PARAMS";

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	try{
	    	//get any lists that are not globally accessible for the request
	    	Helper.getSelectList(form, "bulk_config_wizard_1_plv_list", "GET_ALL_PROPERTY_LIST_VERSIONS", false, null, null, null, null);
	    	
	    	DeviceTypeList deviceTypeList = (DeviceTypeList)request.getSession().getServletContext().getAttribute(DMSConstants.GLOBAL_LIST_CONTEXT_PARAM + DMSConstants.GLOBAL_LIST_DEVICE_TYPES);
	    	List<NameValuePair> bulk_config_wizard_1_device_type_list = deviceTypeList.getList();
	    	request.setAttribute("bulk_config_wizard_1_device_type_list", bulk_config_wizard_1_device_type_list);
	    	Results productTypes = queryProductTypes();
			addProductTypesToRequest(request, productTypes);
			Results vipTypes = queryVendorInterfaceTypeParams();
			addVendorInterfaceTypeParamsToRequest(request, vipTypes);
			Results vmcTypes = queryVmcInterfaceTypeParams();
			addVmcInterfaceTypeParamsToRequest(request, vmcTypes);
    	}catch(Exception e){
    		throw new ServletException(e);
    	}
    }

	private void addProductTypesToRequest(HttpServletRequest request, Results productTypes) {
		List<NameValuePair> prodTypesList = new ArrayList<>();
		while (productTypes.next()) {
            NameValuePair nvp = new NameValuePair();
            nvp.setValue(productTypes.getFormattedValue("PRODUCT_TYPE_ID"));
            nvp.setName(productTypes.getFormattedValue("PRODUCT_TYPE_NAME"));
            prodTypesList.add(nvp);
        }
		request.setAttribute(REQ_PARAM_PRODUCT_TYPES, prodTypesList);
	}

	private Results queryProductTypes() throws SQLException, DataLayerException {
		Map<String, Object> sqlParams = new HashMap<>();
		return DataLayerMgr.executeQuery(ORA_QUERY_GET_PRODUCT_TYPES, sqlParams);
	}

	private Results queryDeviceParams(String paramCode, String paramName) throws SQLException, DataLayerException {
		Map<String, Object> sqlParams = new HashMap<>();
		sqlParams.put(ORA_GET_DEVICE_PARAM_CODE, paramCode);
		sqlParams.put(ORA_GET_DEVICE_PARAM_NAME, paramName);
		return DataLayerMgr.executeQuery(ORA_QUERY_GET_DEVICE_PARAMS, sqlParams);
	}

	private void addVendorInterfaceTypeParamsToRequest(HttpServletRequest request, Results params) {
		addParamsToRequest(request, params, REQ_PARAM_VENDOR_INTERFACE_TYPE_PARAMS);
	}

	private void addVmcInterfaceTypeParamsToRequest(HttpServletRequest request, Results params) {
		addParamsToRequest(request, params, REQ_PARAM_VMC_INTERFACE_TYPE_PARAMS);
	}

	private void addParamsToRequest(HttpServletRequest request, Results params, String attrName) {
		if (params.next()) {
			// Gets string template, for ex: SELECT:01=MDB;07=Coin Pulse\enableOptions(...)
			String paramsStr = params.getFormattedValue("params");
			String paramsMap = paramsStr.split(":")[1];
			String[] paramItems = paramsMap.split(";");
			List<NameValuePair> paramList = new ArrayList<>();
			for (String paramItem : paramItems) {
				NameValuePair nvp = new NameValuePair();
				String[] itemKeyValue = paramItem.split("=");
				nvp.setValue(itemKeyValue[0]);
				nvp.setName(itemKeyValue[1].split("\\\\")[0]);
				paramList.add(nvp);
			}
			request.setAttribute(attrName, paramList);
		}
	}

	private Results queryVendorInterfaceTypeParams() throws SQLException, DataLayerException {
		return queryDeviceParams("241", "Vendor Interface Type");
	}

	private Results queryVmcInterfaceTypeParams() throws SQLException, DataLayerException {
		return queryDeviceParams("1500", "VMC Interface Type");
	}
}
