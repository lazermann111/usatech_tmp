package com.usatech.dms.device;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

public class MassFileTransfersStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {    	    	
    	String action = form.getString("action", false);
    	if(!"Upload to Devices".equalsIgnoreCase(action))
             return;    	
    	
    	String serialNumberList = form.getStringSafely("serial_number_list", "").replace('\n', ',').replace("\r", "").replace(" ", "");
    	if(StringHelper.isBlank(serialNumberList)){
            request.setAttribute("error", "No device serial numbers entered");
            return;
    	}
    	
    	String fileIdList = form.getStringSafely("file_id_list", "").replace('\n', ',').replace("\r", "").replace(" ", "");
    	if(StringHelper.isBlank(fileIdList)){
            request.setAttribute("error", "No file IDs entered");
            return;
    	}
    	
    	log.info(new StringBuilder("Mass file transfer is starting, Serial Numbers: ").append(serialNumberList.replace(",", ", "))
    			.append(", File IDs: ").append(fileIdList.replace(",", ", ")).toString());
    			
    	List<Long> fileIds = new ArrayList<Long>();
    	int fileUploadCount = 0;
		Results rs = null;				
		String deviceName;
		String serialNumber;
		int packetSize;
        Connection conn = null;
        boolean success = false;
    	try{ 
    		conn = DataLayerMgr.getConnection("OPER");
    		
    		rs = DataLayerMgr.executeQuery(conn, "GET_MASS_FILE_INFO", new Object[]{fileIdList});
    		while (rs.next())
    			fileIds.add(rs.getValue("FILE_TRANSFER_ID", Long.class));
    		rs.close();
    		
        	if(fileIds.size() < 1){
                request.setAttribute("error", "No files found");
                conn.close();
                return;
        	}    		
    		
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();
    		rs = DataLayerMgr.executeQuery(conn, "GET_MASS_DEVICE_INFO", new Object[]{serialNumberList});    		
	        while(rs.next()){
	        	long startTsMs = System.currentTimeMillis();
	        	long deviceId = rs.getValue("device_id", long.class);
	        	deviceName = rs.getFormattedValue("device_name");
	        	serialNumber = rs.getFormattedValue("device_serial_cd");
				int deviceType = rs.getValue("device_type_id", int.class);				
				if (deviceType == DeviceType.GX.getValue())
			    	packetSize = DeviceUtils.GX_PACKET_SIZE;
			    else if (deviceType == DeviceType.KIOSK.getValue() && serialNumber.startsWith("K3"))
			    	packetSize = DeviceUtils.EPORT_CONNECT_PACKET_SIZE;
			    else
			    	packetSize = DeviceUtils.DEFAULT_PACKET_SIZE;
		    		    		
		    	for (long fileId : fileIds) {
		    		try {
		    			fileUploadCount += DeviceUtils.sendCommand(deviceType, deviceId, deviceName, fileId, -1, packetSize, 0, conn, null, true);
			    		conn.commit();
		    		} catch (Exception e) {
		    			ProcessingUtils.rollbackDbConnection(log, conn);
		    			if (e.getMessage() != null && e.getMessage().indexOf("ORA-20208") > -1)
		    				log.warn(new StringBuilder("Upload of file ID ").append(fileId).append(" to ").append(serialNumber).append(": ").append(e.getMessage()).toString()); 
		    			else
		    				log.error(new StringBuilder("Upload of file ID ").append(fileId).append(" to ").append(serialNumber).toString(), e);
		    			continue;
		    		}
	    		}
				WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, startTsMs, "Upload to Devices", "device", deviceName, log);
			}

	        success = true;
	    }catch(Exception e){
    		throw new ServletException("Error in mass file transfer", e);
		}finally{
			if (rs != null)
				rs.close();
			if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
		}
    	
    	request.setAttribute("message", new StringBuilder("Scheduled upload of ").append(fileUploadCount).append(" files").toString());
		
        StringBuilder email = new StringBuilder("<html><head>").append(Helper.getBasicStyle()).append("</head><body>")
        	.append("Mass File Transfer: scheduled upload of ").append(fileUploadCount).append(" files")
			.append("<br/><br/>Serial Numbers: ").append(serialNumberList.replace(",", ", "))
			.append("<br/><br/>File IDs: ").append(fileIdList.replace(",", ", "))
        	.append("</body></html>");
        BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
        try {
        	Helper.sendEmail(Helper.DMS_EMAIL, user.getEmailAddress(), "Mass File Transfer Confirmation", email.toString(), Helper.DMS_EMAIL_CONTENT_TYPE_HTML);
        } catch (MessagingException e) {
        	log.error("Error emailing confirmation", e);
        }
		log.info("Mass file transfer finished");
    }
}
