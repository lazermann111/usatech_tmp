/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.action.PendingCmdActions;
import com.usatech.layers.common.model.Device;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

/**
 * Step to process "Device Configuration" request.
 */
public class DeviceConfigStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
        processUserOp(form, request, device);
        
        String action = form.getString("myaction", false);
        if (!StringHelper.isBlank(action))
        	DeviceActions.processDeviceAction(null, action, form, device.getId(), device.getDeviceName(), device.getTypeId(), device.getPropertyListVersion(), false, null);
        
        String pendingCommandId = form.getString("cancel_pending_command_id", false);
        if (!StringHelper.isBlank(pendingCommandId))
        {
            // process the "Cancel" action for the pending command
            try {
            	PendingCmdActions.cancel(pendingCommandId);
            } catch (Exception e) {
            	throw new ServletException("Error canceling pending command " + pendingCommandId, e);
            }
            form.setRedirectUri(new StringBuilder("/deviceConfig.i?device_id=").append(device.getId()).append("&msg=Command+cancelled").toString());
        }        
    }

    private void processUserOp(InputForm form, HttpServletRequest request, Device device) throws ServletException
    {
        String userOp = form.getString("userOP", false);
        if (StringHelper.isBlank(userOp))
            return;

        if ("Backup".equalsIgnoreCase(userOp)) {
        	ConfigFileActions.backupConfiguration(device.getId());
        	form.setRedirectUri(new StringBuilder("/deviceConfig.i?device_id=").append(device.getId()).append("&msg=Backup+created").toString());
        } else if ("List".equalsIgnoreCase(userOp))
            processUserOpList(form, request, device);
    }

    private void processUserOpList(InputForm form, HttpServletRequest request, Device device) throws ServletException
    {
        int cmdInt = ConvertUtils.getIntSafely(form.get("cmd_cnt"), 5);
        try
        {
            request.setAttribute("com.usatech.dms.device.CommandHistory", PendingCmdActions.loadCommandHistory(request.getSession().getServletContext(), device, cmdInt));
        }
        catch (SQLException e)
        {
            throw new ServletException("Failed to load command history for device " + device.getDeviceName(), e);
        }
        catch (DataLayerException e)
        {
            throw new ServletException("Failed to load command history for device " + device.getDeviceName(), e);
        }
    }
}
