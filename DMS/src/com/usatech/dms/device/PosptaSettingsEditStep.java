/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
import simple.util.NameValuePair;

public class PosptaSettingsEditStep extends AbstractStep {
	
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	public static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
	public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	public static final SimpleDateFormat hourFormatter = new SimpleDateFormat("h");
	public static final SimpleDateFormat ampmFormatter = new SimpleDateFormat("a");
	public static final SimpleDateFormat tzFormatter = new SimpleDateFormat("z");

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		//### check input variables ###
		long pos_pta_id = form.getLong("pos_pta_id", true, -1);
		long device_id = form.getLong("device_id", true, -1);
		String errorMessage = null;
		if (pos_pta_id < 0) {
			errorMessage = "Required Parameter Not Found:pos_pta_id";
			request.setAttribute("errorMessage", errorMessage);
			return;
		}

		if (device_id < 0) {
			errorMessage = "Required Parameter Not Found:device_id";
			request.setAttribute("errorMessage", errorMessage);
			return;
		}

		HashMap<String, String> params = new HashMap<String, String>();

		//### load settings for pos_pta and device ###
		try {
			Results pos_pta_data = DataLayerMgr.executeQuery("GET_POS_PTA_SETTINGS", new Object[] {pos_pta_id}, true);
			if (pos_pta_data == null || !pos_pta_data.next()) {
				errorMessage = "Template not found: " + pos_pta_id;
				request.setAttribute("errorMessage", errorMessage);
				return;
			}
			request.setAttribute("pos_pta_data", pos_pta_data);
			String pp_pos_pta_device_serial_cd = null;
			String device_serial_cd = null;
			int device_type_id = -1;
			String pp_pos_pta_encrypt_key = "";
			String pp_pos_pta_encrypt_key2 = "";
			int pp_payment_subtype_key_id = -1;
			String pp_pos_pta_pin_req_yn_flag = "";
			int ps_authority_payment_mask_id = -1;
			int pp_authority_payment_mask_id = -1;
			String pos_pta_regex = "";
			String pos_pta_regex_bref = "";
			int pp_payment_subtype_id = -1;
			String ps_payment_subtype_table_name = "";
			String ps_payment_subtype_key_name = null;
			String ps_payment_subtype_key_desc_name = "";
			String ps_payment_subtype_name = "";
			String sysDate = null;
			String pp_pos_pta_activation_ts = "";
			String pp_pos_pta_deactivation_ts = "";
			String status_disabled = "";
			String status_enabled = "";
			int pp_pos_id = -1;
			String pp_pos_pta_passthru_allow_yn_flag = "";
			String pp_pos_pta_disable_debit_denial = "";
			String pp_no_convenience_fee = "";
			BigDecimal pp_pos_pta_pref_auth_amt = new BigDecimal(0);
			BigDecimal pp_pos_pta_pref_auth_amt_max = new BigDecimal(0);
			String pp_currency_cd = "";

			pp_pos_pta_device_serial_cd = pos_pta_data.getFormattedValue(1);
			device_serial_cd = pos_pta_data.getFormattedValue(2);
			String temp = pos_pta_data.getFormattedValue(3);
			device_type_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
			pp_pos_pta_encrypt_key = pos_pta_data.getFormattedValue(4);
			pp_pos_pta_encrypt_key2 = pos_pta_data.getFormattedValue(5);
			temp = pos_pta_data.getFormattedValue(6);
			pp_payment_subtype_key_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
			pp_pos_pta_pin_req_yn_flag = pos_pta_data.getFormattedValue(7);
			temp = pos_pta_data.getFormattedValue(8);
			ps_authority_payment_mask_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
			temp = pos_pta_data.getFormattedValue(9);
			pp_authority_payment_mask_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
			pos_pta_regex = pos_pta_data.getFormattedValue(10);
			pos_pta_regex_bref = pos_pta_data.getFormattedValue(11);
			temp = pos_pta_data.getFormattedValue(12);
			pp_payment_subtype_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
			ps_payment_subtype_table_name = pos_pta_data.getFormattedValue(13);
			ps_payment_subtype_key_name = pos_pta_data.getFormattedValue(14);
			ps_payment_subtype_key_desc_name = pos_pta_data.getFormattedValue(15);
			ps_payment_subtype_name = pos_pta_data.getFormattedValue(16);
			sysDate = pos_pta_data.getFormattedValue(17);
			pp_pos_pta_activation_ts = pos_pta_data.getFormattedValue(18);
			pp_pos_pta_deactivation_ts = pos_pta_data.getFormattedValue(19);
			status_disabled = pos_pta_data.getFormattedValue(20);
			status_enabled = pos_pta_data.getFormattedValue(21);
			temp = pos_pta_data.getFormattedValue(22);
			pp_pos_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
			pp_pos_pta_passthru_allow_yn_flag = pos_pta_data.getFormattedValue(23);
			temp = pos_pta_data.getFormattedValue(24);
			pp_pos_pta_pref_auth_amt = (temp != null && temp.length() > 0) ? new BigDecimal(temp) : new BigDecimal(0.0);
			temp = pos_pta_data.getFormattedValue(25);
			pp_pos_pta_pref_auth_amt_max = (temp != null && temp.length() > 0) ? new BigDecimal(temp) : new BigDecimal(0.0);
			pp_currency_cd = pos_pta_data.getFormattedValue(26);
			String pp_payment_action_type_cd = pos_pta_data.getFormattedValue("payment_action_type_cd");
			String pp_payment_action_type_desc = pos_pta_data.getFormattedValue("payment_action_type_desc");
			String pp_payment_entry_method_cd = pos_pta_data.getFormattedValue(27);
			String pp_payment_entry_method_desc = pos_pta_data.getFormattedValue("payment_entry_method_desc");
			pp_pos_pta_disable_debit_denial = pos_pta_data.getFormattedValue(28);
			pp_no_convenience_fee = pos_pta_data.getFormattedValue(29);

			Date declineUntil = pos_pta_data.getValue("decline_until", Date.class);
			if (declineUntil != null) {
				params.put("pp_decline_until_date", dateFormat.format(declineUntil));
				params.put("pp_decline_until_time", timeFormat.format(declineUntil));
			}

			params.put("pp_whitelist", pos_pta_data.getFormattedValue("whitelist"));

			// ### determine which regex is used (based on presidence) ###
			int authority_payment_mask_id = -1;
			String regex_final = "";
			String regex_final_bref = "";
			String regex_final_name = "";
			String regex_final_desc = "";

			authority_payment_mask_id = pp_authority_payment_mask_id != -1 ? pp_authority_payment_mask_id : ps_authority_payment_mask_id;

			request.setAttribute("authority_payment_mask_id", new BigDecimal(authority_payment_mask_id).toString());

			Results authority_payment_mask = DataLayerMgr.executeQuery("GET_AUTHORITY_PAYMENT_MASK", new Object[] {authority_payment_mask_id}, true);

			if (authority_payment_mask == null || !authority_payment_mask.next()) {
				errorMessage = "authority_payment_mask_id " + authority_payment_mask_id + " not found!: ";
				request.setAttribute("errorMessage", errorMessage);
				return;
			}
			String authority_payment_mask_regex = authority_payment_mask.getFormattedValue(1) == null || authority_payment_mask.getFormattedValue(1).equals("") ? "" : authority_payment_mask.getFormattedValue(1);
			String authority_payment_mask_bref = authority_payment_mask.getFormattedValue(2) == null || authority_payment_mask.getFormattedValue(2).equals("") ? "" : authority_payment_mask.getFormattedValue(2);
			String authority_payment_mask_name = authority_payment_mask.getFormattedValue(3) == null || authority_payment_mask.getFormattedValue(3).equals("") ? "" : authority_payment_mask.getFormattedValue(3);
			String authority_payment_mask_desc = authority_payment_mask.getFormattedValue(4) == null || authority_payment_mask.getFormattedValue(4).equals("") ? "" : authority_payment_mask.getFormattedValue(4);

			if ((!(StringHelper.isBlank(pos_pta_regex)) && pos_pta_regex.equalsIgnoreCase(authority_payment_mask_regex)) && (!(authority_payment_mask_bref.equalsIgnoreCase(pos_pta_regex_bref)))) {
				regex_final = pos_pta_regex;
				regex_final_bref = pos_pta_regex_bref;
				regex_final_name = "";
				regex_final_desc = "";
				authority_payment_mask_id = -1;

			} else {
				regex_final = authority_payment_mask_regex;
				regex_final_bref = authority_payment_mask_bref;
				regex_final_name = authority_payment_mask_name;
				regex_final_desc = authority_payment_mask_desc;
			}

			// ### some text formatting ###
			String payment_subtype_key_name_formatted = ps_payment_subtype_key_name.toLowerCase();
			Pattern p = Pattern.compile("[^a-z]|id$");
			Matcher m = p.matcher(payment_subtype_key_name_formatted);
			payment_subtype_key_name_formatted = m.replaceAll(" ");
			String[] words = payment_subtype_key_name_formatted.split("\\s");
			payment_subtype_key_name_formatted = "";
			for (int i = 0; i < words.length; i++) {
				words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1);
				payment_subtype_key_name_formatted += words[i] + " ";
			}

			request.setAttribute("payment_subtype_key_name_formatted", payment_subtype_key_name_formatted);
			// payment_subtype_key_name_formatted =~ s/[^a-z]|id$/ /gio;
			// payment_subtype_key_name_formatted =~ s/\b(\w)/\U$1/go;
			String pos_pta_encrypt_key_hex = (pp_pos_pta_encrypt_key != null && pp_pos_pta_encrypt_key.length() > 0) ? "CHECKED" : "";
			String pos_pta_encrypt_key2_hex = (pp_pos_pta_encrypt_key2 != null && pp_pos_pta_encrypt_key2.length() > 0) ? "CHECKED" : "";

			Results regex_types = (Results)DataLayerMgr.executeQuery("GET_REGEX_TYPES", new Object[] {pp_payment_action_type_cd,pp_payment_entry_method_cd}, true);

			//### load backref settings ###
			Results regex_bref_types = (Results)DataLayerMgr.executeQuery("GET_REGEX_BREF_TYPES", null, true);

			//### load other settings ###
			Results payment_subtype_table_key_ids_res = DataLayerMgr.executeQuery("GET_PAYMENT_SUBTYPE_KEY_IDS", new Object[] {device_id,pp_payment_subtype_id}, true);
			request.setAttribute("payment_subtype_table_key_ids", payment_subtype_table_key_ids_res);

			//### do not query the database to get current date, get jvm sysdate ###
			String currentDate = dateFormat.format(Calendar.getInstance().getTime());
			String currentTS = dateTimeFormat.format(Calendar.getInstance().getTime());
			//get current time
			String currentHour = hourFormatter.format(Calendar.getInstance().getTime());
			String currentAmPm = ampmFormatter.format(Calendar.getInstance().getTime());
			String currentTz = tzFormatter.format(Calendar.getInstance().getTime());
			//request.setAttribute("currentDate", currentDate);
			//request.setAttribute("currentTz", currentTz);
			//request.setAttribute("currentHour", currentHour);
			//request.setAttribute("currentAmPm", currentAmPm);
			//build hour list
			List<NameValuePair> toggle_hour_loop = new ArrayList<NameValuePair>();
			int i = 1;
			while (i < 13) {
				toggle_hour_loop.add(new NameValuePair(new Integer(i).toString(), new Integer(i).toString()));
				i++;
			}

			request.setAttribute("toggle_hour_loop", toggle_hour_loop);

			//build ampm list
			List<NameValuePair> toggle_ampm_loop = new ArrayList<NameValuePair>();
			toggle_ampm_loop.add(new NameValuePair("AM", "AM"));
			toggle_ampm_loop.add(new NameValuePair("PM", "PM"));

			request.setAttribute("toggle_ampm_loop", toggle_ampm_loop);

			//build tz list
			List<NameValuePair> toggle_tzone_loop = new ArrayList<NameValuePair>();
			toggle_tzone_loop.add(new NameValuePair("EST", "0"));
			toggle_tzone_loop.add(new NameValuePair("CST", "-1"));
			toggle_tzone_loop.add(new NameValuePair("MST", "-2"));
			toggle_tzone_loop.add(new NameValuePair("PST", "-3"));

			request.setAttribute("toggle_tzone_loop", toggle_tzone_loop);

			//build type list
			List<NameValuePair> toggle_type_loop = new ArrayList<NameValuePair>();
			toggle_type_loop.add(new NameValuePair("None", ""));
			toggle_type_loop.add(new NameValuePair("Immediately", "now"));
			toggle_type_loop.add(new NameValuePair("Custom:", "custom"));

			request.setAttribute("toggle_type_loop", toggle_type_loop);

			List<NameValuePair> toggle_currency = Helper.buildSelectList("GET_CURRENCY_FORMATTED", false, null);

			request.setAttribute("toggle_currency", toggle_currency);

			String toggle_cd = !StringHelper.isBlank(pp_pos_pta_deactivation_ts) ? status_disabled : !(StringHelper.isBlank(pp_pos_pta_activation_ts)) ? status_enabled : "0"; //0 = new but not yet enabled
			String toggle_ts = (!(StringHelper.isBlank(toggle_cd)) && new Integer(toggle_cd).intValue() > 0) ? pp_pos_pta_deactivation_ts : (!(StringHelper.isBlank(toggle_cd)) && new Integer(toggle_cd).intValue() < -1) ? pp_pos_pta_activation_ts : null;
			String toggle_display_date = "";
			String toggle_ampm = "";
			String toggle_hour = "";
			String toggle_time = "";
			boolean toggle_custom = false;
			if (!(StringHelper.isBlank(toggle_ts))) {
				Calendar toggle_cal = Calendar.getInstance();
				//just need to worry about the time
				String[] data = toggle_ts.split(" ");
				toggle_display_date = data[0];
				String[] data2 = toggle_display_date.split("/");
				String toggle_m = data2[0];
				String toggle_d = data2[1];
				String toggle_y = data2[2];
				toggle_time = data[1];
				toggle_ampm = data[2];
				String[] timData = toggle_time.split(":");
				toggle_hour = timData[0];
				toggle_cal.set(new Integer(toggle_y).intValue(), new Integer(toggle_m).intValue(), new Integer(toggle_d).intValue(), new Integer(toggle_hour).intValue(), 0);
				if (toggle_cal.after(Calendar.getInstance())) {
					toggle_custom = true;
				}
			} else {
				String[] data = currentTS.split(" ");
				toggle_display_date = data[0];
				toggle_time = data[1];
				toggle_ampm = data[2];
				String[] timData = toggle_time.split(":");
				toggle_hour = timData[0];
			}
			if (toggle_custom) {
				request.setAttribute("currentDate", toggle_display_date);
				request.setAttribute("currentTz", currentTz);
				request.setAttribute("currentHour", toggle_hour);
				request.setAttribute("currentAmPm", toggle_ampm);
			} else {
				request.setAttribute("currentDate", currentDate);
				request.setAttribute("currentTz", currentTz);
				request.setAttribute("currentHour", currentHour);
				request.setAttribute("currentAmPm", currentAmPm);
			}

			request.setAttribute("toggle_cd", toggle_cd);
			request.setAttribute("toggle_ampm", toggle_ampm);
			request.setAttribute("toggle_hour", toggle_hour);
			request.setAttribute("toggle_custom", String.valueOf(toggle_custom));

			String default_found = null;
			LinkedList<HashMap<String, String>> toggle_regex_methods = new LinkedList<HashMap<String, String>>();
			HashMap<String, String> map = null;
			while (regex_types.next()) {
				map = new HashMap<String, String>();
				map.put("value", regex_types.getFormattedValue(2));
				if (default_found == null && regex_types.getFormattedValue(4).equalsIgnoreCase(String.valueOf(authority_payment_mask_id))) {
					default_found = "selected";
					map.put("default", default_found);
				} else {
					map.put("default", "");
				}
				map.put("text", regex_types.getFormattedValue(1));
				map.put("bref", regex_types.getFormattedValue(3));
				map.put("id", regex_types.getFormattedValue(4));
				toggle_regex_methods.add(map);
			}
			if (default_found == null) {
				map = new HashMap<String, String>();
				map.put("value", regex_final);
				map.put("default", "selected");
				map.put("text", "Custom method");
				map.put("bref", regex_final_bref);
				map.put("id", "");
				toggle_regex_methods.addFirst(map);
			}

			request.setAttribute("toggle_regex_methods", toggle_regex_methods);

			// ### compile settings into javascript-readable arrays ###
			StringBuilder regex_bref_codes_str = new StringBuilder();
			StringBuilder regex_bref_names_str = new StringBuilder();
			StringBuilder regex_bref_descs_str = new StringBuilder();
			while (regex_bref_types.next()) {
				regex_bref_codes_str.append(",'" + regex_bref_types.getFormattedValue(1) + "'");
				regex_bref_names_str.append(",'" + regex_bref_types.getFormattedValue(2) + "'");
				regex_bref_descs_str.append(",'" + regex_bref_types.getFormattedValue(3) + "'");
			}

			StringBuilder regex_ids_str = new StringBuilder();
			StringBuilder regex_methods_str = new StringBuilder();
			StringBuilder regex_bref_methods_str = new StringBuilder();
			for (HashMap<String, String> method: toggle_regex_methods) {
				regex_ids_str.append(",\"" + method.get("id") + "\"");
				regex_methods_str.append(",\"" + (StringUtils.escape(((String)method.get("value")), new char[] {'\\'}, '\\') + "\""));
				regex_bref_methods_str.append(",\"" + method.get("bref") + "\"");
			}

			params.put("pp_pos_pta_device_serial_cd", String.valueOf(pp_pos_pta_device_serial_cd));
			params.put("device_serial_cd", String.valueOf(device_serial_cd));
			params.put("device_type_id", device_type_id >= 0 ? String.valueOf(device_type_id) : "");
			params.put("pp_pos_pta_encrypt_key", String.valueOf(pp_pos_pta_encrypt_key));
			params.put("pp_pos_pta_encrypt_key2", String.valueOf(pp_pos_pta_encrypt_key2));
			params.put("pp_payment_subtype_key_id", pp_payment_subtype_key_id >= 0 ? String.valueOf(pp_payment_subtype_key_id) : "");
			params.put("pp_pos_pta_pin_req_yn_flag", pp_pos_pta_pin_req_yn_flag);
			params.put("ps_authority_payment_mask_id", ps_authority_payment_mask_id >= 0 ? String.valueOf(ps_authority_payment_mask_id) : "");
			params.put("pp_authority_payment_mask_id", pp_authority_payment_mask_id >= 0 ? String.valueOf(pp_authority_payment_mask_id) : "");
			params.put("pos_pta_regex", pos_pta_regex);
			params.put("pos_pta_regex_bref", pos_pta_regex_bref);
			params.put("pp_payment_subtype_id", pp_payment_subtype_id >= 0 ? String.valueOf(pp_payment_subtype_id) : "");
			params.put("ps_payment_subtype_table_name", ps_payment_subtype_table_name);
			params.put("ps_payment_subtype_key_name", ps_payment_subtype_key_name);
			params.put("ps_payment_subtype_key_desc_name", ps_payment_subtype_key_desc_name);
			params.put("ps_payment_subtype_name", ps_payment_subtype_name);
			params.put("sysDate", sysDate);
			params.put("pp_pos_pta_activation_ts", pp_pos_pta_activation_ts);
			params.put("pp_pos_pta_deactivation_ts", pp_pos_pta_deactivation_ts);
			params.put("status_disabled", status_disabled);
			params.put("status_enabled", status_enabled);
			params.put("pp_pos_id", pp_pos_id >= 0 ? String.valueOf(pp_pos_id) : "");
			params.put("pp_pos_pta_passthru_allow_yn_flag", pp_pos_pta_passthru_allow_yn_flag);
			params.put("pp_pos_pta_disable_debit_denial", pp_pos_pta_disable_debit_denial);
			params.put("pp_no_convenience_fee", pp_no_convenience_fee);
			params.put("pp_pos_pta_pref_auth_amt", pp_pos_pta_pref_auth_amt.compareTo(BigDecimal.ZERO) == 1 ? String.valueOf(pp_pos_pta_pref_auth_amt) : "");
			params.put("pp_pos_pta_pref_auth_amt_max", pp_pos_pta_pref_auth_amt_max.compareTo(BigDecimal.ZERO) == 1 ? String.valueOf(pp_pos_pta_pref_auth_amt_max) : "");
			params.put("pp_currency_cd", pp_currency_cd);
			params.put("pp_payment_action_type_desc", pp_payment_action_type_desc);
			params.put("pp_payment_entry_method_desc", pp_payment_entry_method_desc);

			params.put("pos_pta_encrypt_key_hex", pos_pta_encrypt_key_hex);
			params.put("pos_pta_encrypt_key2_hex", pos_pta_encrypt_key2_hex);

			params.put("regex_final", regex_final);
			params.put("regex_final_bref", regex_final_bref);
			params.put("regex_final_name", regex_final_name);
			params.put("regex_final_desc", regex_final_desc);

			params.put("regex_ids_str", regex_ids_str.toString().substring(1));
			params.put("regex_methods_str", regex_methods_str.toString().substring(1));
			params.put("regex_bref_methods_str", regex_bref_methods_str.toString().substring(1));
			temp = null;
			temp = regex_bref_codes_str.toString();
			if (temp.length() > 0) {
				temp = temp.substring(1);
			}
			params.put("regex_bref_codes_str", temp);
			temp = regex_bref_names_str.toString();
			if (temp.length() > 0) {
				temp = temp.substring(1);
			}
			params.put("regex_bref_names_str", temp);
			temp = regex_bref_descs_str.toString();
			if (temp.length() > 0) {
				temp = temp.substring(1);
			}
			params.put("regex_bref_descs_str", temp);
			request.setAttribute("params", params);

		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}

	}

}
