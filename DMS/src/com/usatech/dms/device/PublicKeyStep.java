package com.usatech.dms.device;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Base64;
import simple.io.ByteArrayUtils;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.util.StringHelper;

public class PublicKeyStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	try {
	    	String action = form.getString("action", false);
    		if ("Create".equalsIgnoreCase(action)) {
    			String publicKeyHex = form.getString("publicKeyHex", true);
    			if (!StringHelper.isHex(publicKeyHex))
    				throw new ServletException("Invalid Public Key Hex");
    			String publicKeyBase64 = Base64.encodeBytes(ByteArrayUtils.fromHex(publicKeyHex), true);
    			byte[] publicKeyBytes = ByteArrayUtils.fromHex(publicKeyHex);
    			int publicKeyCRC = InteractionUtils.generateCRC(publicKeyBytes);

	    		Map<String, Object> params = new HashMap<String, Object>();
	    		params.put("publicKeyTypeCd", form.getString("publicKeyTypeCd", true));
	    		params.put("publicKeyHex", publicKeyHex);
	    		params.put("publicKeyBase64", publicKeyBase64);
	    		params.put("publicKeyIdentifier", form.getString("publicKeyIdentifier", true));

	    		BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
	    		params.put("usernamePublicKeyCreatedBy", user.getUserName());

	    		DataLayerMgr.executeUpdate("INSERT_PUBLIC_KEY", params, true);
	        long publicKeyId = ConvertUtils.getLong(params.get("publicKeyId"));
	    		form.setRedirectUri(new StringBuilder("/publicKey.i?publicKeyId=").append(publicKeyId).append("&msg=Public+Key+created,+CRC:+").append(publicKeyCRC).toString());
	    	}
    	} catch (Exception e) {
    		throw new ServletException(e);
    	}
    }
}