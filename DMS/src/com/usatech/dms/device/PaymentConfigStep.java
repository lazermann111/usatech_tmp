/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.model.Device;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class PaymentConfigStep extends AbstractStep
{
 // by default, sort by "t.tran_start_ts" descending
    private static final String DEFAULT_SORT_INDEX = "-2";
    private static final String SQL_START = "SELECT /*+INDEX(t IDX_TRAN_DEVICE_TRAN_START_TS)*/ DISTINCT t.tran_id, " +
            "TO_CHAR(t.tran_start_ts, 'MM/DD/YYYY HH24:MI:SS'), " +
            "ts.tran_state_desc, " +
            "ps.payment_subtype_name, " +
            "COALESCE(aem.acct_entry_method_name, pem.payment_entry_method_desc), " +
            "t.tran_received_raw_acct_data, " +
            "TO_CHAR(COALESCE(s.sale_amount, a.auth_amt), 'FM9,999,999,990.00') tran_amt, " +
            "c.consumer_email_addr1, t.tran_start_ts, s.sale_amount, a.auth_amt ";
    
    private static final String SQL_BASE = "FROM pss.tran t " + 
    		"JOIN pss.pos_pta pp ON t.pos_pta_id = pp.pos_pta_id " +
    		"JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id " +
    		"JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd " +
    		"JOIN pss.payment_entry_method pem ON cpt.payment_entry_method_cd = pem.payment_entry_method_cd " +
    		"JOIN pss.tran_state ts ON t.tran_state_cd = ts.tran_state_cd " +
    		"LEFT OUTER JOIN pss.consumer_acct ca ON t.consumer_acct_id = ca.consumer_acct_id " +
    		"LEFT OUTER JOIN pss.consumer c ON ca.consumer_id = c.consumer_id " + 
			(DialectResolver.isOracle()
					? "LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N' "
							+ "LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id "
					: "LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N' AND a.tran_id BETWEEN ?::numeric AND ?::numeric "
							+ "LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id AND s.tran_id BETWEEN ?::numeric AND ?::numeric ") +
			  "LEFT OUTER JOIN pss.acct_entry_method aem ON a.acct_entry_method_cd = aem.acct_entry_method_cd AND aem.acct_entry_method_cd != '1' ";
    
    private static final String SQL_BASE_COUNT = "FROM pss.tran t " + 
    		"JOIN pss.pos_pta pp ON t.pos_pta_id = pp.pos_pta_id " +
    		"JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id " +
    		"JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd " +
    		"JOIN pss.payment_entry_method pem ON cpt.payment_entry_method_cd = pem.payment_entry_method_cd " +
    		"JOIN pss.tran_state ts ON t.tran_state_cd = ts.tran_state_cd ";
    
    private static final String[] SORT_FIELDS = {"t.tran_id", // tran id
        "t.tran_start_ts",//Time
        "ts.tran_state_desc",  // state
        "ps.payment_subtype_name", // payment type
        "COALESCE(aem.acct_entry_method_name, pem.payment_entry_method_desc)", // entry method
        "t.tran_received_raw_acct_data", // card data
        "COALESCE(s.sale_amount, a.auth_amt)",
        "LOWER(c.consumer_email_addr1)"
     };

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
        String transaction_from_time = form.getString(DevicesConstants.PARAM_TRAN_FROM_TIME, false);
        if (StringHelper.isBlank(transaction_from_time)){
            transaction_from_time = Helper.getDefaultStartTime();
            request.setAttribute(DevicesConstants.PARAM_TRAN_FROM_TIME, transaction_from_time);
        }        
        String transaction_from_date = form.getString(DevicesConstants.PARAM_TRAN_FROM_DATE, false);
        if (StringHelper.isBlank(transaction_from_date)){
            transaction_from_date = Helper.getDefaultStartDate();
            request.setAttribute(DevicesConstants.PARAM_TRAN_FROM_DATE, transaction_from_date);
        }
        String transaction_to_time = form.getString(DevicesConstants.PARAM_TRAN_TO_TIME, false);
        if (StringHelper.isBlank(transaction_to_time)){
            transaction_to_time = Helper.getDefaultEndTime();
            request.setAttribute(DevicesConstants.PARAM_TRAN_TO_TIME, transaction_to_time);
        }        
        String transaction_to_date = form.getString(DevicesConstants.PARAM_TRAN_TO_DATE, false);
        if (StringHelper.isBlank(transaction_to_date)){
            transaction_to_date = Helper.getDefaultEndDate();
            request.setAttribute(DevicesConstants.PARAM_TRAN_TO_DATE, transaction_to_date);
        }
        String userOP = form.getString(DevicesConstants.PARAM_USER_OP, false);
        
        if (DevicesConstants.USER_OP_LIST_TRAN.equalsIgnoreCase(userOP) || DevicesConstants.USER_OP_LIST_ALL.equalsIgnoreCase(userOP))
        {
        	StringBuilder sql_criteria = new StringBuilder();
        	sql_criteria.append("WHERE t.device_name = ? ");
			sql_criteria.append(DialectResolver.isOracle()
					? "AND t.tran_start_ts BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')"
							+ " AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') "
					: " AND t.tran_id BETWEEN ?::numeric AND ?::numeric ");
        	String paramTotalCount = PaginationUtil.getTotalField(null);
        	String paramPageIndex = PaginationUtil.getIndexField(null);
        	String paramPageSize = PaginationUtil.getSizeField(null);
        	String paramSortIndex = PaginationUtil.getSortField(null);

        	try {
        		
        		long minId = 0;
				long maxId = 0;
				if (!DialectResolver.isOracle()) {
					// get between range
					StringBuilder sql_where = new StringBuilder(
							" WHERE t.tran_start_ts BETWEEN TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')");
					sql_where.append(" AND TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')");
					Results ranges = DataLayerMgr.executeSQL("OPER",
							new StringBuilder(
									"SELECT COALESCE (min(tran_id_min), 0) as min_id, COALESCE(max(tran_id_max), 0) as max_id FROM pss.tran_range t ")
											.append(sql_where).toString(),
							new Object[] { transaction_from_date, transaction_from_time, transaction_to_date,
									transaction_to_time },
							null);
					if (ranges.next()) {
						minId = ranges.getValue("min_id", long.class);
						maxId = ranges.getValue("max_id", long.class);
					}
				}

        		// pagination parameters
        		int totalCount = form.getInt(paramTotalCount, false, -1);
        		if (totalCount == -1) {
        			// if the total count is not retrieved yet, get it now
        			Results total;
					if (!DialectResolver.isOracle()) {
						total = DataLayerMgr.executeSQL(
								"OPER", new StringBuilder("SELECT COUNT(1) ").append(SQL_BASE_COUNT)
										.append(sql_criteria).toString(),
								new Object[] { device.getDeviceName(), minId, maxId }, null);
					} else {
						total = DataLayerMgr.executeSQL("OPER",
								new StringBuilder("SELECT COUNT(1) ").append(SQL_BASE_COUNT).append(sql_criteria)
										.toString(),
								new Object[] { device.getDeviceName(), transaction_from_date, transaction_from_time,
										transaction_to_date, transaction_to_time },
								null);
					}
        			if (total.next()) {
        				totalCount = total.getValue(1, int.class);
        			} else {
        				totalCount = 0;
        			}
        			request.setAttribute(paramTotalCount, String.valueOf(totalCount));
        		}

        		int pageIndex = form.getInt(paramPageIndex, false, 1);
        		int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
        		int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
        		int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

        		String sortIndex = form.getString(paramSortIndex, false);
        		sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
        		String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

				String query;
				Object[] params;
				if (!DialectResolver.isOracle()) {
					query = new StringBuilder("select * from (").append(SQL_START).append(SQL_BASE).append(sql_criteria)
							.append(orderBy).append(" limit ?::numeric ) gen_alias offset ?::numeric - 1").toString();
					params = new Object[] { minId, maxId, minId, maxId, device.getDeviceName(), minId, maxId,
							maxRowToFetch, minRowToFetch };
				} else {
					query = new StringBuilder("select * from (").append(" select pagination_temp.*, ROWNUM rnum from (")
							.append(SQL_START).append(SQL_BASE).append(sql_criteria).append(orderBy)
							.append(") pagination_temp where ROWNUM <= ? ").append(") where rnum  >= ? ").toString();
					params = new Object[] { device.getDeviceName(), transaction_from_date, transaction_from_time,
							transaction_to_date, transaction_to_time, maxRowToFetch, minRowToFetch };
				}

				Results results = DataLayerMgr.executeSQL("OPER", query, params, null);
        		request.setAttribute("tranList", results);
        	} catch (Exception e) {
        		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        	}
        }
    }
}
