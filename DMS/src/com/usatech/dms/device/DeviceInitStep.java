package com.usatech.dms.device;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.action.SerialAllocatAction;
import com.usatech.dms.action.TerminalActions;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.DMSUtils;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.InitializationReason;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.device.DeviceTypeRegex;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.messagedata.MessageData_AD;
import com.usatech.layers.common.messagedata.MessageData_C0;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.model.Device;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.Censor;
import simple.text.StringUtils;

public class DeviceInitStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();

	protected long encryptionKeyMinAgeMin = 1440;
	protected boolean deviceReactivationOnReinit = true;
	protected int defaultFilePacketSize = 1024;
	protected String deviceFirmwareVersion = DMSConstants.APP_CD;
	protected int protocolComplianceRevision = 4001009;

	private void reuseTerminalSwapping(String serialNumOld, String serialNumNew, Date endDate, Date startDate)
			throws ServiceException, DataLayerException {
		Object[] params = new Object[] { serialNumOld, serialNumNew, endDate, startDate };
		try {
			DataLayerMgr.executeCall("REPLACE_TERMINAL_EPORT", params, true);
		} catch (SQLException e) {
			if (e.getErrorCode() == 20201) {
				throw new ServiceException("Invalid eport serial number");
			}
		}
	}

	private long createTerminalSwapping(String sourceSerialNumber, String targetSerialNumber, Date startDate,
			long userId) throws SQLException, DataLayerException, ConvertException, ServletException, ServiceException {
		Map<String, Object> terminalParams = new HashMap<String, Object>();
		terminalParams.put("terminalDate", new Date());
		terminalParams.put("eportSerialNums", sourceSerialNumber);
		Results results = DataLayerMgr.executeQuery("GET_BULK_TERMINAL_INFO", terminalParams);
		long terminalId = 0;
		if (results.next()) {
			terminalId = results.getValue("terminalId", long.class);
		} else {
			throw new ServiceException("Terminal ID not found for " + sourceSerialNumber);
		}

		Map<String, Object> cloneTerminalParams = new HashMap<String, Object>();
		cloneTerminalParams.put("terminalId", terminalId);
		cloneTerminalParams.put("deviceSerialNumber", targetSerialNumber);
		cloneTerminalParams.put("startDate", startDate);
		cloneTerminalParams.put("userId", userId);
		cloneTerminalParams.put("newTerminalId", 0);
		Connection conn = DataLayerMgr.getConnectionForCall("CLONE_TERMINAL_BY_ID");
		try {
			DataLayerMgr.executeUpdate(conn, "CLONE_TERMINAL_BY_ID", cloneTerminalParams);
			conn.commit();
		} catch (SQLException e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			if (e.getErrorCode() == 20205) {
				throw new ServiceException("ePort has already been activated");
			} else if (e.getErrorCode() == 20201) {
				throw new ServiceException("Invalid eport serial number");
			} else {
				throw e;
			}
		} finally {
			conn.close();
		}
		long newTerminalId = ConvertUtils.getLongSafely(cloneTerminalParams.get("newTerminalId"), 0);
		return newTerminalId;
	}

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		String devList = form.getString("dev_list", false);
		String devListToSwap = form.getString("dev_list_to_swap", false);
		if (devList == null || devList.length() <= 0) {
			return;
		}
		String serialNums[] = devList.split("\n", -1);

		Connection conn = null;
		try {
			if (serialNums.length <= 0) {
				return;
			}
			BasicServletUser user = (BasicServletUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
			String userName = user.getUserName();
			String userFullName = user.getFullName();

			long dealerId = form.getLong("dealerId", true, -1);
			byte deviceTypeId = (byte) form.getInt("typeId", true, -1);
			byte deviceTypeIdOld = deviceTypeId;
			boolean deviceTypeDetection = false;
			LinkedList<DeviceTypeRegex> deviceTypeRegexList = null;
			DeviceType deviceType = null;
			DeviceType deviceTypeOld = null;
			String serialNumberRegex = null;
			Pattern serialNumberPattern = null;
			if (deviceTypeId < 0) {
				deviceTypeDetection = true;
				deviceTypeRegexList = Helper.getDeviceTypeRegexList();
			} else {
				deviceType = DeviceType.getByValue(deviceTypeId);
				serialNumberRegex = SerialAllocatAction.getSerialRegex((int) deviceTypeId);
				serialNumberPattern = Pattern.compile(serialNumberRegex);
			}
			boolean swap = "Y".equalsIgnoreCase(form.getString("swap_devices", false));
			boolean reuseTerminals = "Y".equalsIgnoreCase(form.getString("reuse_terminal", false));
			boolean initialize = "Y".equalsIgnoreCase(form.getString("initialize", false));
			boolean register = "Y".equalsIgnoreCase(form.getString("register", false));
			boolean activate = "Y".equalsIgnoreCase(form.getString("activate", false));
			boolean deactivate = "Y".equalsIgnoreCase(form.getString("deactivate", false));
			boolean deactivateRentals = "Y".equalsIgnoreCase(form.getString("deactivate_rentals", false));
			boolean deactivateQuickStarts = "Y".equalsIgnoreCase(form.getString("deactivate_quick_starts", false));
			boolean clearServiceFees = "Y".equalsIgnoreCase(form.getString("clear_service_fees", false));
			boolean deactivateCashless = "Y".equalsIgnoreCase(form.getString("deactivate_cashless", false));
			long credentialId = form.getLong("credential_id", false, -1);
			boolean changeCredential = "Y".equalsIgnoreCase(form.getString("change_credential", false))
					&& credentialId > -1;
			boolean clone = "Y".equalsIgnoreCase(form.getString("clone", false));
			boolean importConfigTemplate = "Y".equalsIgnoreCase(form.getString("import_config_template", false));
			boolean changeTwoTierPricing = "Y".equalsIgnoreCase(form.getString("change_two_tier_pricing", false));
			String twoTierPricingValue = form.getStringSafely("two_tier_pricing_value", "").trim();
			boolean importPaymentTemplate = "Y".equalsIgnoreCase(form.getString("import_payment_template", false));
			String cloneSerialNumber = form.getStringSafely("clone_serial_number", "").trim();
			boolean isCloneCrane = clone && WebHelper.isCraneDevice(cloneSerialNumber);
			boolean errorOnMultipleDevicePerTerminal = "Y"
					.equalsIgnoreCase(form.getString("error_on_multiple_device_per_terminal", false));
			boolean errorOnCampusCardFee = "Y".equalsIgnoreCase(form.getString("error_on_campus_card_fee", false));
			Set<String> deviceDeactivationDisabled = new HashSet<>();
			boolean gotErrorOnMultipleDevices = false;
			boolean gotErrorOnActiveCampusCardFee=false;
			String serialNumsToSwap[] = null;
			if (swap) {
				if (devListToSwap == null || devListToSwap.length() <= 0) {
					throw new ServiceException("Serial numbers for swapping are missing");
				}
				serialNumsToSwap = devListToSwap.split("\n", -1);
				if (serialNums.length != serialNumsToSwap.length) {
					throw new ServiceException("Different amount of serial numbers to swapping");
				}
			}

			Results results;
			StringBuilder err = new StringBuilder();

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("typeId", deviceTypeId);
			params.put("dealerId", dealerId);

			String terminalOverride = form.getStringSafely("terminal_override", "N");
			Date terminalEndDate = ConvertUtils.convert(Date.class,
					new StringBuilder(form.getStringSafely("terminal_end_date", "").trim()).append(" ")
							.append(form.getStringSafely("terminal_end_time", "").trim()).toString());

			Date serviceFeeEndDate = null;
			Date terminalDate = new Date();
			String serviceFeeOverride = form.getStringSafely("service_fee_override", "N");
			Map<Integer, Integer> fees = null;
			Map<String, Long> eportTerminals = new HashMap<String, Long>();
			Map<String, Long> eportTerminalsOld = new HashMap<String, Long>();
			
			if (deactivate || clearServiceFees) {
				serviceFeeEndDate = ConvertUtils.convert(Date.class,
						new StringBuilder(form.getStringSafely("service_fee_end_date", "").trim()).append(" ")
								.append(form.getStringSafely("service_fee_end_time", "").trim()).toString());
			}

			if (clearServiceFees) {
				fees = new HashMap<Integer, Integer>();
				results = DataLayerMgr.executeQuery("GET_FEES", null);
				while (results.next())
					fees.put(results.getValue("FEE_ID", int.class),
							results.getValue("DEFAULT_FREQUENCY_ID", int.class));
			}
			String feeNoTriggerEventFlag = form.getStringSafely("fee_no_trigger_event_flag", null);

			Date cashlessEndDate = null;
			if (deactivateCashless)
				cashlessEndDate = ConvertUtils.convert(Date.class,
						new StringBuilder(form.getStringSafely("cashless_end_date", "").trim()).append(" ")
								.append(form.getStringSafely("cashless_end_time", "").trim()).toString());

			int rentalsSkippedCount = 0;
			int quickStartsSkippedCount = 0;
			if (deactivate || clearServiceFees) {
				if (swap) {
					Map<String, Object> terminalParams = new HashMap<String, Object>();
					terminalParams.put("terminalDate", terminalDate);
					terminalParams.put("eportSerialNums",
							devListToSwap.replace('\n', ',').replace("\r", "").replace(" ", "").toUpperCase());
					results = DataLayerMgr.executeQuery("GET_BULK_TERMINAL_INFO", terminalParams);
					while (results.next()) {
						int rentalFeeCount = results.getValue("rentalFeeCount", Integer.class);
						int quickStartCount = results.getValue("quickStartFeeCount", Integer.class);
						if (!deactivateRentals && rentalFeeCount > 0)
							rentalsSkippedCount++;
						else if (!deactivateQuickStarts && quickStartCount > 0)
							quickStartsSkippedCount++;
						else {
							eportTerminalsOld.put(results.getValue("eportSerialNum", String.class),
									results.getValue("terminalId", long.class));
						}
					}
				}
				Map<String, Object> terminalParams = new HashMap<String, Object>();
				terminalParams.put("terminalDate", terminalDate);
				terminalParams.put("eportSerialNums",
						devList.replace('\n', ',').replace("\r", "").replace(" ", "").toUpperCase());
				results = DataLayerMgr.executeQuery("GET_BULK_TERMINAL_INFO", terminalParams);
				while (results.next()) {
					int rentalFeeCount = results.getValue("rentalFeeCount", Integer.class);
					int quickStartCount = results.getValue("quickStartFeeCount", Integer.class);
					Long terminalId = results.getValue("terminalId", Long.class);
					if (errorOnMultipleDevicePerTerminal) {
						String eportSerialNum = results.getValue("eportSerialNum", String.class);
						Map<String, Object> terminalEportParam = new HashMap<>();
						terminalEportParam.put("terminalId", terminalId);
						Results terminalResults = DataLayerMgr.executeQuery("GET_ACTIVE_DEVICES_COUNT_BY_TERMINAL_ID",
								terminalEportParam);
						terminalResults.next();
						long deviceCount = terminalResults.getValue("deviceCount", long.class);
						if (deviceCount > 1) {
							deviceDeactivationDisabled.add(eportSerialNum);
						}
					}
					//USAT-1156
					if(errorOnCampusCardFee) {
						Map<String, Object> feesTermnalParam = new HashMap<>();
						feesTermnalParam.put("terminalId", terminalId);
						feesTermnalParam.put("startDate", serviceFeeEndDate);
						feesTermnalParam.put("endDate", serviceFeeEndDate);
						Results feeResults = DataLayerMgr.executeQuery("GET_ACTIVE_CAMPUS_CARD_FEE_COUNT_BY_TERMINAL_ID",feesTermnalParam);
						feeResults.next();
						long feeCount = feeResults.getValue("feeCount", long.class);
						if (feeCount > 0) {
							gotErrorOnActiveCampusCardFee=true;
						}
					}
					if (!deactivateRentals && rentalFeeCount > 0 && !swap)
						rentalsSkippedCount++;
					else if (!deactivateQuickStarts && quickStartCount > 0 && !swap)
						quickStartsSkippedCount++;
					else {
						eportTerminals.put(results.getValue("eportSerialNum", String.class), terminalId);

					}
				}
			}

			String swap_effective_date_str = form.getStringSafely("swap_effective_date", "").trim();
			if (StringHelper.isBlank(swap_effective_date_str) || swap_effective_date_str.length() < 10) {
				swap_effective_date_str = Helper.getCurrentDate();
			}
			Date swapStartDate = ConvertUtils.convert(Date.class, new StringBuilder(swap_effective_date_str).append(" ")
					.append(form.getStringSafely("swap_effective_time", "").trim()).toString());
			if (swapStartDate == null) {
				swapStartDate = new Date();
			}

			Date activateDate = null;

			Map<String, Object> activationParams = null;
			if (activate) {
				activationParams = new HashMap<String, Object>();
				activationParams.put("userId", 0);
				activationParams.put("TERMINAL_ID", 0);
				activationParams.put("dealerId", dealerId);
				activationParams.put("ASSET_NUMBER", form.getString("asset_number", false));
				activationParams.put("MACHINE_MAKE", form.getString("machine_make", true));
				activationParams.put("MACHINE_MODEL", form.getString("machine_model", true));
				activationParams.put("TELEPHONE", form.getString("telephone", false));
				activationParams.put("OUTSIDE_LINE", null);
				activationParams.put("REGION", form.getString("region", false));
				activationParams.put("LOCATION_NAME", form.getString("location_name", true));
				activationParams.put("LOCATION_DETAILS", form.getString("location_details", false));
				activationParams.put("ADDRESS", form.getString("address", false));
				activationParams.put("CITY", form.getString("city", true));
				activationParams.put("STATE", form.getString("state", true));
				activationParams.put("ZIP", form.getString("postal", true).replaceAll("\\s", ""));
				activationParams.put("COUNTRY", form.getString("country", false));
				activationParams.put("custBankId", form.getLong("customer_bank_id", true, -1));
				activationParams.put("PAY_SCHEDULE", form.getInt("payment_schedule_id", false, -1));
				activationParams.put("LOCATION_TYPE", form.getString("location_type", false));
				activationParams.put("LOCATION_TYPE_SPECIFY", form.getString("location_type_specific", false));
				activationParams.put("PRODUCT_TYPE", form.getString("product_type", true));
				activationParams.put("PRODUCT_TYPE_SPECIFY", form.getString("product_type_specific", false));
				activationParams.put("TIME_ZONE", form.getInt("time_zone_id", true, -1));
				activationParams.put("CUSTOM_1", form.getString("custom_1", false));
				activationParams.put("CUSTOM_2", form.getString("custom_2", false));
				activationParams.put("CUSTOM_3", form.getString("custom_3", false));
				activationParams.put("CUSTOM_4", form.getString("custom_4", false));
				activationParams.put("CUSTOM_5", form.getString("custom_5", false));
				activationParams.put("CUSTOM_6", form.getString("custom_6", false));
				activationParams.put("CUSTOM_7", form.getString("custom_7", false));
				activationParams.put("CUSTOM_8", form.getString("custom_8", false));
				activationParams.put("KEEP_EXISTING_DATA",
						"Y".equalsIgnoreCase(form.getString("keep_existing_data", false)) ? "Y" : "N");
				activationParams.put("OVERRIDE_PAYMENT_SCHEDULE",
						"Y".equalsIgnoreCase(form.getString("override_payment_schedule", false)) ? "Y" : "N");
				activationParams.put("DOING_BUSINESS_AS",
						Censor.sanitizeText(form.getStringSafely("doing_business_as", "").trim()));
				activationParams.put("FEE_NO_TRIGGER_EVENT_FLAG",
						"Y".equalsIgnoreCase(form.getString("fee_no_trigger_event_flag", false)) ? "Y" : "N");
				activationParams.put("SALES_ORDER_NUMBER", form.getString("sales_order_number", false));
				activationParams.put("PURCHASE_ORDER_NUMBER", form.getString("purchase_order_number", false));
				activationParams.put("BUSINESS_TYPE", form.getString("business_type", false));
				activationParams.put("CUSTOMER_SERVICE_PHONE",
						StringUtils.trim(form.getString("customer_service_phone", false)));
				activationParams.put("CUSTOMER_SERVICE_EMAIL",
						StringUtils.trim(form.getString("customer_service_email", false)));
				activationParams.put("COMMISSION_BANK_ID", form.getLong("commission_bank_id", false, -1));

				String effective_date_str = form.getStringSafely("effective_date", "").trim();
				if (StringHelper.isBlank(effective_date_str) || effective_date_str.length() < 10)
					effective_date_str = Helper.getCurrentDate();
				activateDate = ConvertUtils.convert(Date.class, new StringBuilder(effective_date_str).append(" ")
						.append(form.getStringSafely("effective_time", "").trim()).toString());
				if (activateDate == null)
					activateDate = new Date();
				activationParams.put("ACTIVATE_DATE", activateDate);

				activationParams.put("FEE_GRACE_DAYS", form.getInt("fee_grace_days", true, 60));

				char mobileIndicator = ConvertUtils.convertSafely(Character.class,
						RequestUtils.getAttribute(request, "mobile_indicator", false), '?');
				Set<Character> entryList = ConvertUtils.convert(Set.class, Character.class,
						RequestUtils.getAttribute(request, "entry_list", false));
				if (entryList == null)
					entryList = new HashSet<Character>();
				Character posEnvironment = null;
				switch (mobileIndicator) {
				case 'Y':
					entryList.add('M');
					posEnvironment = 'C'; // Mobile
					break;
				case 'N':
					char terminalActivator = ConvertUtils.convertSafely(Character.class,
							RequestUtils.getAttribute(request, "terminal_activator", false), '?');
					switch (terminalActivator) {
					case 'C':
						if (!entryList.contains('M') || entryList.contains('S') || entryList.contains('P')
								|| entryList.contains('E'))
							posEnvironment = 'U'; // Unattended
						else
							posEnvironment = 'E'; // Ecommerce
						break;
					case 'A':
						if (entryList.contains('S'))
							posEnvironment = 'A'; // Attended
						else
							posEnvironment = 'M'; // MOTO
						break;
					default:
						// don't set
					}
					break;
				default:
					// don't set
				}
				activationParams.put("POS_ENVIRONMENT_CD", posEnvironment);
				if (!entryList.isEmpty())
					activationParams.put("PIN_CAPABILITY_FLAG", entryList.contains('M') ? 'Y' : 'N');
				char[] entryCapability = new char[entryList.size()];
				int i = 0;
				for (Character ec : entryList)
					if (ec != null)
						entryCapability[i++] = ec;
				activationParams.put("ENTRY_CAPABILITY_CDS", new String(entryCapability, 0, i));
			}

			Device sourceDevice = null;
			long sourceDeviceId = -1;
			int sourceDeviceTypeId = -1;
			LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = null;
			LinkedHashMap<String, ConfigTemplateSetting> targetDefaultSettingData = null;
			LinkedHashMap<String, String> sourceDeviceSettingData = null;
			LinkedHashMap<String, String> targetDeviceSettingData;

			if (clone) {
				Results sourceDeviceInfo = DataLayerMgr.executeQuery("GET_DEVICE_INFO_BY_SERIAL_NUMBER",
						new Object[] { cloneSerialNumber }, false);
				if (sourceDeviceInfo.next())
					sourceDeviceId = sourceDeviceInfo.getValue("device_id", long.class);
				if (sourceDeviceId < 1) {
					err.append("Source device serial number ").append(cloneSerialNumber)
							.append(" not found for cloning<br /><br />");
					log.error(new StringBuilder("Source device serial number ").append(cloneSerialNumber)
							.append(" not found for cloning").toString());
				} else {
					sourceDevice = DeviceActions.getDevice(request, "", sourceDeviceId);
					sourceDeviceTypeId = sourceDevice.getTypeId();
					defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(
							sourceDeviceTypeId, sourceDevice.getPropertyListVersion(), null, null);
					sourceDeviceSettingData = DeviceUtils.getDeviceSettingData(defaultSettingData, sourceDeviceId,
							sourceDeviceTypeId, true, false, null);
				}
			}

			String configTemplate = form.getStringSafely("config_template_id", "");
			long configTemplateId = -1;
			int configTemplateDeviceTypeId = -1;
			LinkedHashMap<String, String> configTemplateSettingData = null;
			if (importConfigTemplate && configTemplate.length() > 0) {
				String[] configTemplateFields = configTemplate.split("_");
				configTemplateDeviceTypeId = ConvertUtils.getIntSafely(configTemplateFields[0], -1);
				configTemplateId = ConvertUtils.getLongSafely(configTemplateFields[2], -1);
				if (defaultSettingData == null)
					defaultSettingData = DeviceConfigurationUtils
							.getDefaultConfFileDataByDeviceTypeId(configTemplateDeviceTypeId, -1, null, null);
				configTemplateSettingData = DeviceUtils.getConfigTemplateSettingData(defaultSettingData,
						configTemplateId, configTemplateDeviceTypeId, true, false, null);
				if (configTemplateSettingData == null) {
					err.append("Configuration Template ID ").append(configTemplateId).append(" not found<br /><br />");
					log.error(new StringBuilder("Configuration Template ID ").append(configTemplateId)
							.append(" not found").toString());
				}
			}

			long paymentTemplateId = form.getLong("pos_pta_tmpl_id", false, 0);
			String paymentTemplateModeCd = form.getString("mode_cd", false);
			String paymentTemplateOrderCd = form.getString("order_cd", false);
			String paymentTemplateSetTerminalCdToSerial = form.getStringSafely("set_terminal_cd_to_serial", "N");
			String paymentTemplateOnlyNoTwoTierPricing = form.getStringSafely("only_no_two_tier_pricing", "N");
			if (importPaymentTemplate && paymentTemplateId < 1) {
				err.append("Payment Template ID ").append(paymentTemplateId).append(" not found<br /><br />");
				log.error(new StringBuilder("Payment Template ID ").append(paymentTemplateId).append(" not found")
						.toString());
			}

			Object[] result = null;
			int propertyListVersion = 0;
			int initializedCount = 0;
			int registeredCount = 0;
			int deactivatedCount = 0;
			int clearedServiceFeeCount = 0;
			int deactivatedCashlessCount = 0;
			int activatedCount = 0;
			int credentialChangeCount = 0;
			int clonedCount = 0;
			int importedConfigTemplateCount = 0;
			int changedTwoTierPricingCount = 0;
			int importedPaymentTemplateCount = 0;
			int swappedCount = 0;
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();

			if (activate) {
				String activateDetail = form.getString("activateDetail", false);
				if (!StringUtils.isBlank(activateDetail)) {
					activateDetail = activateDetail.trim();
					activationParams.put("ACTIVATE_DETAIL", activateDetail);
				}
				activationParams.put("ACTIVATE_DETAIL_ID", form.getInt("activateDetailId", false, 1));
				
				int distributorId = form.getInt("distributorName", false, 0);
				if (distributorId != 0) {
					activationParams.put("DISTRIBUTOR_ID", distributorId);
				}else {
					activationParams.put("DISTRIBUTOR_ID", null);
				}
			}
			String deactivateDetail = null;
			Integer deactivateDetailId = null;
			if (deactivate) {
				deactivateDetail = form.getString("deactivateDetail", false);
				deactivateDetailId = form.getInt("deactivateDetailId", false, 1);
				if (!StringUtils.isBlank(deactivateDetail)) {
					deactivateDetail = deactivateDetail.trim();
				}
			}

			for (int i = 0; i < serialNums.length; i++) {
				String serialNumOld = null;
				String serialNum = serialNums[i].trim().toUpperCase();
				if (serialNum.length() == 0) {
					continue;
				}
				if (swap) {
					serialNumOld = serialNumsToSwap[i].trim().toUpperCase();
				}

				if (isCloneCrane) {
					if (!WebHelper.isCraneDevice(serialNum)) {
						err.append("Invalid serial number ").append(serialNum)
								.append(" to clone from a crane device <br />");
						continue;
					}
				}

				long startTsMs = System.currentTimeMillis();
				try {
					if (deviceTypeDetection) {
						deviceTypeId = Helper.getDeviceTypeBySerialNumber(deviceTypeRegexList, serialNum);
						if (deviceTypeId < 0) {
							err.append("Invalid serial number ").append(serialNum).append("<br />");
							continue;
						}
						deviceType = DeviceType.getByValue(deviceTypeId);
						if (swap) {
							deviceTypeIdOld = Helper.getDeviceTypeBySerialNumber(deviceTypeRegexList, serialNumOld);
							if (deviceTypeIdOld < 0) {
								err.append("Invalid serial number ").append(serialNumOld).append("<br />");
								continue;
							}
							deviceTypeOld = DeviceType.getByValue(deviceTypeIdOld);
						}

					} else {
						if ((initialize || register || deactivate)
								&& !serialNumberPattern.matcher(serialNum).matches()) {
							err.append("Invalid serial number ").append(serialNum).append(" for device type ")
									.append(deviceType).append("<br />");
							continue;
						}
						if (deactivate && swap && !serialNumberPattern.matcher(serialNumOld).matches()) {
							err.append("Invalid serial number ").append(serialNumOld).append(" for device type ")
									.append(deviceTypeOld).append("<br />");
							continue;
						}
					}

					if (initialize) {
						boolean deviceExists = false;
						results = DataLayerMgr.executeQuery("GET_DEVICE_COUNT_BY_SERIAL_NUMBER",
								new Object[] { serialNum });
						if (results.next())
							deviceExists = results.getValue("device_count", int.class) > 0;

						if (deviceExists)
							initializedCount++;
						else {
							if (deviceTypeId < DeviceType.EDGE.getValue()) {
								MessageData_AD messageData = new MessageData_AD();
								messageData.setDeviceType(DeviceType.getByValue(deviceTypeId));
								messageData.setDeviceSerialNum(serialNum);
								AppLayerUtils.processOfflineInit(messageData, log, encryptionKeyMinAgeMin,
										deviceReactivationOnReinit);
							} else {
								if (propertyListVersion < 1)
									propertyListVersion = DeviceUtils.getMaxPropertyListVersion(null);

								MessageData_C0 messageData = new MessageData_C0();
								messageData.setDeviceType(DeviceType.getByValue(deviceTypeId));
								messageData.setDeviceSerialNum(serialNum);
								messageData.setDeviceFirmwareVersion(deviceFirmwareVersion);
								messageData.setPropertyListVersion(propertyListVersion);
								messageData.setProtocolComplianceRevision(4001009);
								messageData.setReasonCode(InitializationReason.NEW_DEVICE);
								AppLayerUtils.processOfflineInit(messageData, log, encryptionKeyMinAgeMin,
										deviceReactivationOnReinit);
							}
							results = DataLayerMgr.executeQuery("GET_DEVICE_COUNT_BY_SERIAL_NUMBER",
									new Object[] { serialNum });
							if (results.next())
								deviceExists = results.getValue("device_count", int.class) > 0;
							if (deviceExists) {
								initializedCount++;
							}
						}
					}

					BigDecimal ePortIdForRegister = null;
					BigDecimal ePortIdForDeactivate = null;
					if (register) {
						params.put("serialNum", serialNum);
						params.put("typeId", deviceTypeId);
						DataLayerMgr.executeUpdate("EPORT_INS", params, true);
						ePortIdForRegister = (BigDecimal) params.get("neweportid");
					}
					if (deactivate && !deviceDeactivationDisabled.contains(serialNum)) {
						params.put("serialNum", (swap) ? serialNumOld : serialNum);
						params.put("typeId", (swap) ? deviceTypeIdOld : deviceTypeId);
						DataLayerMgr.executeUpdate("EPORT_INS", params, true);
						ePortIdForDeactivate = (BigDecimal) params.get("neweportid");
					}

					boolean deviceRegistered = false;
					if (register) {
						params.put("eportId", ePortIdForRegister);
						if (swap) {
							Results dealerResult = DataLayerMgr.executeQuery("GET_EPORT_DEALER",
									new Object[] { serialNumOld });
							if (dealerResult.next()) {
								params.put("dealerId", dealerResult.getValue("DEALER_ID"));
							}
						}
						DataLayerMgr.executeUpdate("DEALER_EPORT_INS", params, true);
						deviceRegistered = true;
						registeredCount++;
					}

					if (swap && reuseTerminals) {
						try {
							reuseTerminalSwapping(serialNumOld, serialNum, terminalEndDate, swapStartDate);
							swappedCount++;
						} catch (ServiceException e) {
							err.append(e.getMessage()).append(" when swapping ").append(serialNumOld).append(" and ")
									.append(serialNum);
						}
					}

					if (swap && !reuseTerminals) {
						createTerminalSwapping(serialNumOld, serialNum, swapStartDate,
								DMSUtils.getUserIdByEmail(user.getEmailAddress()));
						swappedCount++;
					}

					Long terminalId = (swap) ? eportTerminalsOld.get(serialNumOld) : eportTerminals.get(serialNum);
					if (terminalId != null && terminalId.longValue() > 0) {
						if (deactivate) {
							//                                                  && USAT-1156
							if (!deviceDeactivationDisabled.contains(serialNum) && !gotErrorOnActiveCampusCardFee) {
								TerminalActions.updateTerminalEport(null, ePortIdForDeactivate.longValue(),
										terminalEndDate, null, null, terminalOverride, null, deactivateDetail, null,
										deactivateDetailId);
								deactivatedCount++;

								long salesRepId = form.getLong("deact_salesRepId", false, 0);
								if (salesRepId != 0) {
									Date commissionDate = terminalDate;
									Instant commissionDateInstant = commissionDate.toInstant();
									Instant midnightInstant = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT)
											.toInstant(ZoneOffset.UTC);
									if (commissionDateInstant.isBefore(midnightInstant)) {
										log.debug(
												"During Deactivate Devices, terminal '{0}' end date '{1}' is in the past, so using current time",
												(swap) ? serialNumOld : serialNum, commissionDate);
										commissionDate = new Date();
									}
									long fallbackSalesRepId = form.getLong("deact_fallback_salesRepId", false, 0);
									String eventTypeCode = form.getString("deact_eventTypeCode", true);
									DeviceActions.createCommissionEvent((swap) ? serialNumOld : serialNum, salesRepId,
											eventTypeCode, terminalEndDate, fallbackSalesRepId);
								}
								       //USAT-1156
							} else if (gotErrorOnActiveCampusCardFee) {
								err.append("Can't deactivate device ").append(serialNum)
								.append(". Has active campus card fee.").append("<br />");
							} else {
								gotErrorOnMultipleDevices = true;
								err.append("Can't deactivate device ").append(serialNum)
								.append(". Has multiple devices per terminal").append("<br />");
							}
						}
						//                   && USAT-1155                  && USAT-1156
						if (clearServiceFees && !gotErrorOnMultipleDevices && !gotErrorOnActiveCampusCardFee) {
							for (int feeId : fees.keySet()) {
								int freqId = fees.get(feeId);
								DataLayerMgr.executeUpdate("SERVICE_FEES_UPD",
										new Object[] { terminalId, feeId, freqId, 0, 0, serviceFeeEndDate, null,
												serviceFeeOverride, null, feeNoTriggerEventFlag },
										true);
							}
							clearedServiceFeeCount++;
						}
					}

					if (deactivateCashless) {
						DataLayerMgr.executeUpdate("DEACTIVATE_CASHLESS_PAYMENT_TYPES",
								new Object[] { cashlessEndDate, (swap) ? serialNumOld : serialNum }, true);
						deactivatedCashlessCount++;
					}

					if (activate) {
						if (!deviceRegistered) {
							results = DataLayerMgr.executeQuery("GET_EPORT_DEALER", new Object[] { serialNum });
							deviceRegistered = results.next();
						}
						if (deviceRegistered) {
							activationParams.put("SERIAL_NUMBER", serialNum);
							activationParams.put("activatedBy", userFullName);
							DataLayerMgr.executeUpdate("ACTIVATE_DEVICE", activationParams, true);
							activatedCount++;

							long salesRepId = form.getLong("act_salesRepId", false, 0);
							if (salesRepId > 0) {
								String eventTypeCode = form.getString("act_eventTypeCode", true);
								if (activateDate == null)
									activateDate = new Date();
								DeviceActions.createCommissionEvent(serialNum, salesRepId, eventTypeCode, activateDate);
							}
						} else
							err.append("Device ").append(serialNum)
									.append(" cannot be activated because it is not registered<br />");
					}

					if (changeCredential && serialNum.startsWith("K3")) {
						DataLayerMgr.executeUpdate("UPDATE_DEVICE_CREDENTIAL_BY_SERIAL_NUMBER",
								new Object[] { credentialId, serialNum }, true);
						credentialChangeCount++;
					}

					long targetDeviceId = -1;
					int targetDeviceTypeId = -1;
					Device targetDevice = null;
					if (clone || importConfigTemplate || changeTwoTierPricing || importPaymentTemplate) {
						Results targetDeviceInfo = DataLayerMgr.executeQuery("GET_DEVICE_INFO_BY_SERIAL_NUMBER",
								new Object[] { serialNum }, false);
						if (targetDeviceInfo.next()) {
							targetDeviceId = targetDeviceInfo.getValue("device_id", long.class);
							targetDeviceTypeId = targetDeviceInfo.getValue("device_type_id", int.class);
						}
						if (targetDeviceId > 0)
							targetDevice = DeviceActions.getDevice(request, "", targetDeviceId);
						else {
							err.append("Target device serial number ").append(serialNum)
									.append(" not found<br /><br />");
							log.error(new StringBuilder("Target device serial number ").append(serialNum)
									.append(" not found").toString());
						}
					}

					if (clone && sourceDeviceSettingData != null && targetDeviceId > 0) {
						DeviceActions.cloneDevice(form, request, "", sourceDevice, targetDevice, false);
						if (sourceDeviceTypeId == targetDeviceTypeId && (isCloneCrane || targetDevice.isEDGE()
								|| targetDevice.isGX() || targetDevice.isG4())) {
							if (conn == null)
								conn = DataLayerMgr.getConnection("OPER");

							int callInTimeChangeCount = 0;
							int callInTimeWindowChangeCount = 0;
							StringBuilder newIniFileShort = new StringBuilder();
							targetDeviceSettingData = DeviceUtils.getDeviceSettingData(defaultSettingData,
									targetDeviceId, targetDeviceTypeId, true, false, conn);
							for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
								ConfigTemplateSetting defaultSetting = entry.getValue();
								String key = defaultSetting.getKey();
								String paramValue = sourceDeviceSettingData.get(key);
								boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());

								if (sourceDeviceSettingData.containsKey(key) && targetDeviceSettingData.containsKey(key)
										&& !StringHelper.equalConfigValues(paramValue, targetDeviceSettingData.get(key),
												storedInPennies)) {
									if (defaultSetting.isServerOnly()) {
										if (key.startsWith("CALL_IN_TIME_WINDOW_"))
											callInTimeWindowChangeCount++;
										DeviceConfigurationUtils.upsertDeviceSetting(targetDeviceId, key, paramValue,
												conn, userName);
										continue;
									}

									if (isCloneCrane) {
										DeviceConfigurationUtils.upsertDeviceSetting(targetDeviceId, key, paramValue,
												conn, userName);
									} else if (targetDevice.isEDGE()) {
										DeviceConfigurationUtils.upsertDeviceSetting(targetDeviceId, key, paramValue,
												conn, userName);

										if (StringHelper.equalConfigValues(
												defaultSetting.getConfigTemplateSettingValue(), paramValue,
												storedInPennies))
											newIniFileShort.append(key).append("!\n");
										else
											newIniFileShort.append(key).append("=").append(paramValue).append("\n");

										if ("SCHEDULE".equalsIgnoreCase(defaultSetting.getEditorType()))
											callInTimeChangeCount++;
									} else {
										int offset = defaultSetting.getFieldOffset();
										if (DevicesConstants.GX_MAP_IMPORT_PROTECTED_FIELDS.contains(offset))
											continue;

										result = DataLayerMgr.executeCall(conn, "SP_UPDATE_CONFIG_AND_RETURN",
												new Object[] { targetDeviceId, offset, paramValue, 'H', deviceType, 0,
														0, "", "", "", "" });
										int returnCd = -1;
										if (result != null) {
											returnCd = ConvertUtils.getIntSafely(result[1], -1);
											String returnMsg = ConvertUtils.getStringSafely(result[2]);

											if (returnCd < 0) {
												err.append("Error updating device configuration for device ")
														.append(serialNum).append(" ").append(returnMsg);
												log.error(new StringBuilder(
														"Error updating device configuration for device ")
																.append(serialNum).append(" ").append(returnMsg)
																.toString());
											} else if (returnCd > 0) {
												if (offset == DevicesConstants.GX_MAP_CALL_IN_TIME)
													callInTimeChangeCount++;
											}
										}
									}
								}
							}
							if (!isCloneCrane) {
								if (callInTimeWindowChangeCount > 0)
									DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION",
											new Object[] { 1, targetDeviceId });
								else if (callInTimeChangeCount > 0)
									DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION",
											new Object[] { 0, targetDeviceId });

								if (newIniFileShort.length() > 0) {
									long newFileId = ConfigFileActions.getNextFileTransferSequenceNum(conn);
									ConfigFileActions.saveFile(
											new StringBuilder(targetDevice.getDeviceName()).append("-CFG-")
													.append(newFileId).toString(),
											newIniFileShort.toString(), newFileId, FileType.PROPERTY_LIST.getValue(),
											conn);
									DeviceUtils.sendCommand(targetDeviceTypeId, targetDeviceId,
											targetDevice.getDeviceName(), newFileId, newIniFileShort.length(),
											DeviceUtils.DEFAULT_PACKET_SIZE, DeviceUtils.DEFAULT_EXECUTE_ORDER, conn,
											null, false);
								}
							}

							conn.commit();
							clonedCount++;
						}
					}

					if (importConfigTemplate && configTemplateSettingData != null
							&& (configTemplateDeviceTypeId == targetDeviceTypeId
									|| configTemplateDeviceTypeId == DeviceType.G4.getValue()
											&& targetDeviceTypeId == DeviceType.GX.getValue())
							&& (targetDevice.isEDGE() || targetDevice.isGX() || targetDevice.isG4())) {
						if (conn == null)
							conn = DataLayerMgr.getConnection("OPER");

						int callInTimeChangeCount = 0;
						int callInTimeWindowChangeCount = 0;
						StringBuilder newIniFileShort = new StringBuilder();
						targetDeviceSettingData = DeviceUtils.getDeviceSettingData(defaultSettingData, targetDeviceId,
								targetDeviceTypeId, true, false, conn);
						for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
							ConfigTemplateSetting defaultSetting = entry.getValue();
							String key = defaultSetting.getKey();
							String paramValue = configTemplateSettingData.get(key);
							boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());

							if (configTemplateSettingData.containsKey(key) && targetDeviceSettingData.containsKey(key)
									&& !StringHelper.equalConfigValues(paramValue, targetDeviceSettingData.get(key),
											storedInPennies)) {
								if (defaultSetting.isServerOnly()) {
									if (key.startsWith("CALL_IN_TIME_WINDOW_"))
										callInTimeWindowChangeCount++;
									DeviceConfigurationUtils.upsertDeviceSetting(targetDeviceId, key, paramValue, conn,
											userName);
									continue;
								}

								if (targetDevice.isEDGE()) {
									DeviceConfigurationUtils.upsertDeviceSetting(targetDeviceId, key, paramValue, conn,
											userName);

									if (StringHelper.equalConfigValues(defaultSetting.getConfigTemplateSettingValue(),
											paramValue, storedInPennies))
										newIniFileShort.append(key).append("!\n");
									else
										newIniFileShort.append(key).append("=").append(paramValue).append("\n");

									if ("SCHEDULE".equalsIgnoreCase(defaultSetting.getEditorType()))
										callInTimeChangeCount++;
								} else {
									int offset = defaultSetting.getFieldOffset();
									if (DevicesConstants.GX_MAP_IMPORT_PROTECTED_FIELDS.contains(offset))
										continue;

									result = DataLayerMgr.executeCall(conn, "SP_UPDATE_CONFIG_AND_RETURN",
											new Object[] { targetDeviceId, offset, paramValue, 'H', deviceType, 0, 0,
													"", "", "", "" });
									int returnCd = -1;
									if (result != null) {
										returnCd = ConvertUtils.getIntSafely(result[1], -1);
										String returnMsg = ConvertUtils.getStringSafely(result[2]);

										if (returnCd < 0) {
											err.append("Error updating device configuration for device ")
													.append(serialNum).append(" ").append(returnMsg);
											log.error(
													new StringBuilder("Error updating device configuration for device ")
															.append(serialNum).append(" ").append(returnMsg)
															.toString());
										} else if (returnCd > 0) {
											if (offset == DevicesConstants.GX_MAP_CALL_IN_TIME)
												callInTimeChangeCount++;
										}
									}
								}
							}
						}
						if (callInTimeWindowChangeCount > 0)
							DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION",
									new Object[] { 1, targetDeviceId });
						else if (callInTimeChangeCount > 0)
							DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION",
									new Object[] { 0, targetDeviceId });

						if (newIniFileShort.length() > 0) {
							long newFileId = ConfigFileActions.getNextFileTransferSequenceNum(conn);
							ConfigFileActions.saveFile(
									new StringBuilder(targetDevice.getDeviceName()).append("-CFG-").append(newFileId)
											.toString(),
									newIniFileShort.toString(), newFileId, FileType.PROPERTY_LIST.getValue(), conn);
							DeviceUtils.sendCommand(targetDeviceTypeId, targetDeviceId, targetDevice.getDeviceName(),
									newFileId, newIniFileShort.length(), DeviceUtils.DEFAULT_PACKET_SIZE,
									DeviceUtils.DEFAULT_EXECUTE_ORDER, conn, null, false);
						}

						conn.commit();
						importedConfigTemplateCount++;
					}

					if (changeTwoTierPricing && (targetDevice.isEDGE() || targetDevice.isGX())) {
						if (conn == null)
							conn = DataLayerMgr.getConnection("OPER");

						StringBuilder newIniFileShort = new StringBuilder();
						targetDefaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(
								targetDevice.getTypeId(), targetDevice.getPropertyListVersion(), null, null);
						targetDeviceSettingData = DeviceUtils.getDeviceSettingData(targetDefaultSettingData,
								targetDeviceId, targetDeviceTypeId, true, false, conn);

						String key;
						if (targetDevice.isEDGE())
							key = String.valueOf(DeviceProperty.CONVENIENCE_FEE.getValue());
						else
							key = String.valueOf(DevicesConstants.GX_MAP_CONVENIENCE_FEE);

						if (targetDefaultSettingData.containsKey(key)) {
							ConfigTemplateSetting defaultSetting = targetDefaultSettingData.get(key);
							boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());

							String paramValue;
							if (storedInPennies && StringHelper.isNumeric(twoTierPricingValue))
								paramValue = String.valueOf((new BigDecimal(twoTierPricingValue)
										.multiply(BigDecimal.valueOf(100)).intValue()));
							else
								paramValue = twoTierPricingValue;

							if (!StringHelper.equalConfigValues(paramValue, targetDeviceSettingData.get(key),
									storedInPennies)) {

								if (targetDevice.isEDGE()) {
									DeviceConfigurationUtils.upsertDeviceSetting(targetDeviceId, String.valueOf(key),
											paramValue, conn, userName);
									if (StringHelper.equalConfigValues(defaultSetting.getConfigTemplateSettingValue(),
											paramValue, storedInPennies))
										newIniFileShort.append(key).append("!\n");
									else
										newIniFileShort.append(key).append("=").append(paramValue).append("\n");
								} else {
									int offset = defaultSetting.getFieldOffset();
									result = DataLayerMgr.executeCall(conn, "SP_UPDATE_CONFIG_AND_RETURN",
											new Object[] { targetDeviceId, offset, paramValue, 'H', deviceType, 0, 0,
													"", "", "", "" });
									int returnCd = -1;
									if (result != null) {
										returnCd = ConvertUtils.getIntSafely(result[1], -1);
										String returnMsg = ConvertUtils.getStringSafely(result[2]);

										if (returnCd < 0) {
											err.append("Error updating two-tier pricing for device ").append(serialNum)
													.append(" ").append(returnMsg);
											log.error(new StringBuilder("Error updating two-tier pricing for device ")
													.append(serialNum).append(" ").append(returnMsg).toString());
										}
									}
								}
							}

							if (newIniFileShort.length() > 0) {
								long newFileId = ConfigFileActions.getNextFileTransferSequenceNum(conn);
								ConfigFileActions.saveFile(
										new StringBuilder(targetDevice.getDeviceName()).append("-CFG-")
												.append(newFileId).toString(),
										newIniFileShort.toString(), newFileId, FileType.PROPERTY_LIST.getValue(), conn);
								DeviceUtils.sendCommand(targetDeviceTypeId, targetDeviceId,
										targetDevice.getDeviceName(), newFileId, newIniFileShort.length(),
										DeviceUtils.DEFAULT_PACKET_SIZE, DeviceUtils.DEFAULT_EXECUTE_ORDER, conn, null,
										false);
							}

							conn.commit();
							changedTwoTierPricingCount++;
						}
					}

					if (importPaymentTemplate && paymentTemplateId > 0 && targetDeviceId > 0) {
						int returnCd = DeviceActions.importPosPtaTemplate(targetDeviceId, paymentTemplateId,
								paymentTemplateModeCd, paymentTemplateOrderCd, paymentTemplateSetTerminalCdToSerial,
								paymentTemplateOnlyNoTwoTierPricing, null);
						if (returnCd == 1)
							importedPaymentTemplateCount++;
						else {
							err.append("Error importing payment template ").append(paymentTemplateId)
									.append(" into device ").append(serialNum).append(", returnCd: ").append(returnCd)
									.append("<br />");
							log.error(new StringBuilder("Error importing payment template ").append(paymentTemplateId)
									.append(" into device ").append(serialNum).append(", returnCd: ").append(returnCd));
						}
					}
				} catch (Exception e) {
					ProcessingUtils.rollbackDbConnection(log, conn);
					err.append("Error processing device ").append(serialNum).append(": ").append(e.getMessage())
							.append("<br /><br />");
					log.error(new StringBuilder("Error processing device ").append(serialNum).toString(), e);
				}

				String deviceName = DeviceActions.getDeviceName(serialNum);
				WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, startTsMs, "Submit", "device",
						deviceName, log);
			}
			StringBuilder msg = new StringBuilder();
			if (initialize)
				msg.append(", initialized: ").append(initializedCount);
			if (register)
				msg.append(", registered: ").append(registeredCount);
			if (swap)
				msg.append(", swapped: ").append(swappedCount);
			if (deactivate)
				msg.append(", deactivated: ").append(deactivatedCount);
			if (rentalsSkippedCount > 0)
				msg.append(", skipped rentals: ").append(rentalsSkippedCount);
			if (quickStartsSkippedCount > 0)
				msg.append(", skipped quick starts: ").append(quickStartsSkippedCount);
			if (clearServiceFees)
				msg.append(", cleared service fees for: ").append(clearedServiceFeeCount);
			if (deactivateCashless)
				msg.append(", deactivated card authorizations for: ").append(deactivatedCashlessCount);
			if (activate)
				msg.append(", activated: ").append(activatedCount);
			if (changeCredential)
				msg.append(", changed credentials for: ").append(credentialChangeCount);
			if (clone)
				msg.append(", cloned: ").append(clonedCount);
			if (importConfigTemplate)
				msg.append(", imported configuration templates: ").append(importedConfigTemplateCount);
			if (changeTwoTierPricing)
				msg.append(", changed two-tier pricing values: ").append(changedTwoTierPricingCount);
			if (importPaymentTemplate)
				msg.append(", imported payment templates: ").append(importedPaymentTemplateCount);
			if (msg.length() > 0)
				msg.replace(0, 1, "Results: ");
			else {
				if (err.length() > 0)
					err.insert(0, "No actions selected<br />");
				else
					err.append("No actions selected");
			}
			request.setAttribute("message", msg.toString());
			if (err.length() > 0) {
				request.setAttribute("error", err.toString());
			}
		} catch (ServiceException e) {
			request.setAttribute("error", e.getMessage());
		} catch (Exception e) {
			throw new ServletException("Error processing devices", e);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}

	public long getEncryptionKeyMinAgeMin() {
		return encryptionKeyMinAgeMin;
	}

	public void setEncryptionKeyMinAgeMin(long encryptionKeyMinAgeMin) {
		this.encryptionKeyMinAgeMin = encryptionKeyMinAgeMin;
	}

	public boolean isDeviceReactivationOnReinit() {
		return deviceReactivationOnReinit;
	}

	public void setDeviceReactivationOnReinit(boolean deviceReactivationOnReinit) {
		this.deviceReactivationOnReinit = deviceReactivationOnReinit;
	}

	public int getDefaultFilePacketSize() {
		return defaultFilePacketSize;
	}

	public void setDefaultFilePacketSize(int defaultFilePacketSize) {
		this.defaultFilePacketSize = defaultFilePacketSize;
	}

	public String getDeviceFirmwareVersion() {
		return deviceFirmwareVersion;
	}

	public void setDeviceFirmwareVersion(String deviceFirmwareVersion) {
		this.deviceFirmwareVersion = deviceFirmwareVersion;
	}

	public int getProtocolComplianceRevision() {
		return protocolComplianceRevision;
	}

	public void setProtocolComplianceRevision(int protocolComplianceRevision) {
		this.protocolComplianceRevision = protocolComplianceRevision;
	}
}
