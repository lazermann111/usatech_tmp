/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.HashMap;
import java.util.Map;

public class MessageMap
{
    private static Map<String, String> deviceCallType = new HashMap<String, String>();
    private static Map<String, String> deviceCallTypeColor = new HashMap<String, String>();
    private static Map<String, String> statusCode = new HashMap<String, String>();
    private static Map<String, String> gprsDeviceState = new HashMap<String, String>();
    private static Map<String, String> gprsDeviceStateColor = new HashMap<String, String>();
    
    static {
        initializeCallType();
        initializeCallTypeColor();
        initializeStatusCode();
        initializeGprsDeviceState();
        initializeGprsDeviceStateColor();
    }
    
    public static void initializeCallType() {
        deviceCallType.put("U", "Session");
        deviceCallType.put("B", "Batch");
        deviceCallType.put("A", "Auth");
        deviceCallType.put("C", "Combo");
    }
    
    public static void initializeCallTypeColor() {
        deviceCallTypeColor.put("U", "#F3F3F3");
        deviceCallTypeColor.put("B", "#EEEECC");
        deviceCallTypeColor.put("A", "#9FBFDF");
        deviceCallTypeColor.put("C", "#D6E7D3");
    }
    
    public static void initializeStatusCode() {
        statusCode.put("0", "No Status Available");
        statusCode.put("1", "Idle, Available");
        statusCode.put("2", "In 1st Cycle");
        statusCode.put("3", "Out Of Service");
        statusCode.put("4", "Nothing on Port");
        statusCode.put("5", "Idle, Not Available");
        statusCode.put("6", "Manual Service Mode");
        statusCode.put("7", "In 2nd Cycle");
        statusCode.put("8", "Transaction In Progress");
    }
    
    public static void initializeGprsDeviceState() {
        gprsDeviceState.put("1", "Registered");
        gprsDeviceState.put("2", "Allocated");
        gprsDeviceState.put("3", "Activation Pending");
        gprsDeviceState.put("4", "Activated");
        gprsDeviceState.put("5", "Assigned");
    }
    
    public static void initializeGprsDeviceStateColor() {
        gprsDeviceStateColor.put("1", "#9999FF");
        gprsDeviceStateColor.put("2", "#99CCCC");
        gprsDeviceStateColor.put("3", "#FFFF99");
        gprsDeviceStateColor.put("4", "#99FF99");
        gprsDeviceStateColor.put("5", "orange");
    }
    
    public static Map<String, String> getDeviceCallType() {
        return deviceCallType;
    }
    
    public static Map<String, String> getDeviceCallTypeColor() {
        return deviceCallTypeColor;
    }
    
    public static Map<String, String> getStatusCode() {
        return statusCode;
    }
    
    public static Map<String, String> getGprsDeviceState() {
        return gprsDeviceState;
    }
    
    public static Map<String, String> getGprsDeviceStateColor() {
        return gprsDeviceStateColor;
    }
}
