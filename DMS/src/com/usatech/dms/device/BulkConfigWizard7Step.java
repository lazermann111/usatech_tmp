/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.model.Customer;
import com.usatech.layers.common.model.Device;
import com.usatech.layers.common.model.Location;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.NameValuePair;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to page 2 of the bulk configuration wizard.
 */
public class BulkConfigWizard7Step extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {       	
    	StringBuilder errorMessage = new StringBuilder("");
    	
    	String includeDeviceIds = form.getString("include_device_ids", false);
    	if(StringHelper.isBlank(includeDeviceIds)){
    		errorMessage.append("<br/><br/><b><font color='red'>No devices selected!</font></b><br/><br/>");
            request.setAttribute("errorMessage", errorMessage.toString());
            return;
    	}
    	
    	int deviceType = form.getInt("device_type_id", false, -1);
    	String customerId = form.getString("customer_id", false);
    	String locationId = form.getString("location_id", false);
    	String parentLocationId = form.getString("parent_location_id", false); 
    	String zeroCounters = form.getString("zero_counters", false);
    	String posPtaTmplId = form.getString("pos_pta_tmpl_id", false);
    	String[] parametersStr = form.getStringArray("params_to_change", false);
    	String parametersMapStr = form.getString("params_to_change_request", false);
    	String s2c_requests = form.getString("s2c_requests", false);
    	String file_uploads = form.getString("file_uploads", false);
    	String firmware_upgrades = form.getString("firmware_upgrades", false);
    	String eft = form.getString("eft", false);
    	long ec_credential_id = form.getLong("ec_credential_id", false, -1);
    	Device device = null;
    	
    	if(StringHelper.isBlank(posPtaTmplId) && StringHelper.isBlank(parametersMapStr) && StringHelper.isBlank(customerId) && StringHelper.isBlank(locationId) && StringHelper.isBlank(parentLocationId)  
    			&&  StringHelper.isBlank(zeroCounters) && StringHelper.isBlank(s2c_requests) && StringHelper.isBlank(eft)
    			&& StringHelper.isBlank(file_uploads) && StringHelper.isBlank(firmware_upgrades) && ec_credential_id < 0){
    		errorMessage.append("<br/><b><font color='red'>Nothing to do!</font></b><br/><br/>");
            request.setAttribute("errorMessage", errorMessage.toString());
            return;
    	}
    	
    	try{
    		Results rs = null;
	    	Customer c = null;
			if(!(StringHelper.isBlank(customerId))){
				rs = DataLayerMgr.executeQuery("GET_CUSTOMER_INFO", new Object[] {customerId}, false);
			    if(rs.next()){
			    	//populate an array of classes that that have at least one property named the same as in the resultset, and get the count
			    	c = new Customer();
			    	rs.fillBean(c);
			    }
			    request.setAttribute("customer_name", c.getName());
			}
			
			
			//get customer_location_name, parent or otherwise
			Location l = null;
			if(!(StringHelper.isBlank(locationId))){
				rs = DataLayerMgr.executeQuery("GET_LOCATION_BY_LOCATION_ID", new Object[] {locationId}, false);
			    if(rs.next()){
			    	//populate an array of classes that that have at least one property named the same as in the resultset, and get the count
			    	l = new Location();
			    	rs.fillBean(l);
			    }
			    request.setAttribute("location", l);
			}else if(!(StringHelper.isBlank(parentLocationId))){
				rs = DataLayerMgr.executeQuery("GET_LOCATION_BY_LOCATION_ID", new Object[] {parentLocationId}, false);
			    if(rs.next()){
			    	//populate an array of classes that that have at least one property named the same as in the resultset, and get the count
			    	l = new Location();
			    	rs.fillBean(l);
			    }
			    request.setAttribute("location", l);
			}
			
			if(!(StringHelper.isBlank(posPtaTmplId))) {
				Map<String, Object> params = new HashMap<>();
				params.put("deviceIds", includeDeviceIds);
				params.put("posPtaTmplId", posPtaTmplId);

				rs = DataLayerMgr.executeQuery("CHECK_POS_PTA_TEMPLATE_SELECTION", params, false);
				if(rs.next()) {
					request.setAttribute("pos_pta_tmpl_name", rs.getValue("posPtaTmplName"));
				}
			}

		    if(!StringHelper.isBlank(parametersMapStr)){	    		
		    	HashMap<String, Object> paramMap = StringHelper.decodeRequestMap(parametersMapStr);
		    	List<String> parameters = Arrays.asList(parametersStr);
		    	List<NameValuePair> parametersList = new ArrayList<NameValuePair>();
		    	
		    	LinkedHashMap<String, ConfigTemplateSetting> defaultConfData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceType, -1);
		    	Map<String, String> errorMap = new HashMap<String, String>();
		    	for (Map.Entry<String, ConfigTemplateSetting> entry : defaultConfData.entrySet()) {
		    		if (!parameters.contains(entry.getKey()))
		    			continue;
		    		ConfigTemplateSetting configTemplateSetting = entry.getValue();
		    		String key = configTemplateSetting.getKey();
	    			String label = configTemplateSetting.getLabel();
		    		String regex = configTemplateSetting.getRegex();
		    		String category = configTemplateSetting.getCategoryName();
		    		String editorType = configTemplateSetting.getEditorType();
		    		String userValue = ConvertUtils.getStringSafely(paramMap.get(new StringBuilder("cfg_field_").append(key).toString()), "");
	    			
	    			String displayedValue;
	    			if (editorType.equalsIgnoreCase("BITMAP")) {
	    				int intParamValue = 0;
	    				if (!StringHelper.isBlank(userValue)) {
	    					String[] options = userValue.split(",", -1);
	    					for (String option: options)
	    						intParamValue += Integer.valueOf(option);
	    				}
	    				displayedValue = String.valueOf(intParamValue);
	    			} else if ("SCHEDULE".equalsIgnoreCase(editorType)) {
    					if (device == null) {	    						
    						int pos = includeDeviceIds.indexOf(",");
    						long deviceId = Long.valueOf(pos >= 0 ? includeDeviceIds.substring(0, pos) : includeDeviceIds);
    						device = DeviceUtils.generateDevice(deviceId);
    					}
    					displayedValue = WebHelper.buildSchedule(request, paramMap, configTemplateSetting, key, userValue, device.getDeviceUtcOffsetMin());
	    			} else
	    				displayedValue = Helper.getDisplayedConfigValue(configTemplateSetting.getDataMode(), editorType, userValue);
	    			
	    			if(!StringHelper.isBlank(regex))
	    				Helper.validateRegexWithMessage(errorMap, userValue, key, regex, new StringBuilder("Value validation failed for field: ").append(StringHelper.isBlank(category) || category.equalsIgnoreCase("null") ? "" : category).append(": ").append(configTemplateSetting.getLabel()).toString());
	    			
	    			if (deviceType == DeviceType.EDGE.getValue())
	    				parametersList.add(new NameValuePair(new StringBuilder(label).append(" [").append(key).append("]").toString(), displayedValue));
	    			else
	    				parametersList.add(new NameValuePair(label, displayedValue));
			    	
		    		if(errorMap.size() > 0){
		    			request.setAttribute("errorMap", errorMap);
		    			return;
		    		}
		    	}
		    	
		    	request.setAttribute("params_to_change_list", parametersList);
		    }
	    	
		    Map<String, Object> params = new HashMap<String, Object>();
        	params.put("deviceIds", includeDeviceIds);
			rs = DataLayerMgr.executeQuery("GET_BULK_DEVICE_SEARCH_LIST", params);
	    	request.setAttribute("include_device_ids_results", rs);
    	}catch(Exception e){
    		errorMessage.append("<br/><br/><font color='red'>Exception occurred! " + e + "</font><br/><br/>");
            request.setAttribute("errorMessage", errorMessage.toString());
            log.error("Exception occurred!", e);
    	}
    }
    
}
