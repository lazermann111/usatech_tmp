/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies.  All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL.  Use is subject to license terms.
 */

package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.DeviceActions;
import com.usatech.layers.common.model.Device;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class FileTransferStep extends AbstractStep
{
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {    	
        String action = form.getString("myaction", false);
        if (!StringHelper.isBlank(action)) {
        	Device device = (Device)request.getAttribute(DevicesConstants.STORED_DEVICE);
        	DeviceActions.processDeviceAction(null, action, form, device.getId(), device.getDeviceName(), device.getTypeId(), device.getPropertyListVersion(), false, null);
        }
    }
}
