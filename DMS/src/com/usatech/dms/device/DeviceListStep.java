/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.util.LinkedHashMap;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class DeviceListStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();

	// by default, sort by "Last Activity" descending and "Status" descending
	private static final String DEFAULT_SORT_INDEX = "-12,-13";
	private static final Set<Integer> REQUIRED_SORT_INDEXES = Collections.singleton(new Integer(3)); // Also always sort by device serial cd
	private static final String SQL_START = new StringBuilder("SELECT device_id, device_name, device_serial_cd, device_type_desc,")
			.append(" coalesce(to_char(last_activity_ts, 'MM/DD/YYYY HH24:MI:SS'), '(n/a)') as char_last_activity_ts, device_type_id, device_active_yn_flag, customer_name,")
			.append(" location_name, firmware_version, diag_app_version, ptest_version, bezel_mfgr, bezel_app_version,")
			.append(DialectResolver.isOracle() 
					? " ROUND((SYSDATE - last_activity_ts), 1) as round_last_activity_ts, comm_method_name "
					: " dbadmin.datediff(LOCALTIMESTAMP, last_activity_ts) as round_last_activity_ts, comm_method_name "
				   )
			.append(", corp_customer_name ").toString();

	private static final String SQL_BASE = new StringBuilder("FROM (SELECT d.device_id, d.device_name, d.device_serial_cd, dt.device_type_desc,")
			.append(" d.last_activity_ts, d.device_type_id, d.device_active_yn_flag, c.customer_name, l.location_name,")
			.append(" d.firmware_version, dd.diag_app_version, dd.ptest_version, b_he.host_equipment_mfgr bezel_mfgr, b_app_ver.host_setting_value bezel_app_version,")
			.append(" d.comm_method_cd, cm.comm_method_name, cc.customer_name corp_customer_name")
			.append(" FROM device.device d")
			.append(" JOIN device.device_type dt ON d.device_type_id = dt.device_type_id")
			.append(" LEFT OUTER JOIN pss.pos p ON d.device_id = p.device_id")
			.append(" LEFT OUTER JOIN location.customer c ON p.customer_id = c.customer_id")
			.append(" LEFT OUTER JOIN location.location l ON p.location_id = l.location_id")
			.append(" LEFT OUTER JOIN device.comm_method cm ON d.comm_method_cd = cm.comm_method_cd")
			.append(" LEFT OUTER JOIN device.device_data dd ON d.device_name = dd.device_name")
			.append(" LEFT OUTER JOIN device.host b_h ON d.device_id = b_h.device_id AND b_h.host_type_id = 400 AND b_h.host_active_yn_flag = 'Y'")
			.append(" LEFT OUTER JOIN device.host_equipment b_he ON b_h.host_equipment_id = b_he.host_equipment_id")
			.append(" LEFT OUTER JOIN device.host_setting b_app_ver ON b_h.host_id = b_app_ver.host_id AND b_app_ver.host_setting_parameter = 'Application Version'")
			.append(" LEFT OUTER JOIN report.eport e ON d.device_serial_cd = e.eport_serial_num")
			.append(" LEFT OUTER JOIN report.vw_terminal_eport te ON e.eport_id = te.eport_id")
			.append(" LEFT OUTER JOIN report.terminal t ON te.terminal_id = t.terminal_id")
			.append(" LEFT OUTER JOIN corp.customer cc ON t.customer_id = cc.customer_id")
			.append(" WHERE 1 = 1 ").toString();

	private static final String SQL_END = ")";

	private static final String[] SORT_FIELDS = { "LOWER(dt.device_type_desc)", "d.device_name", "d.device_serial_cd", "LOWER(c.customer_name)", "LOWER(l.location_name)",
			"d.firmware_version", "dd.diag_app_version", "dd.ptest_version", "b_he.host_equipment_mfgr", "b_app_ver.host_setting_value", "cm.comm_method_name", "coalesce(d.last_activity_ts, TO_DATE('1970', 'YYYY'))", "d.device_active_yn_flag", "LOWER(cc.customer_name)" };
	
	private static final String SQL_SYSDATE = DialectResolver.isOracle() ? "SYSDATE" : "LOCALTIMESTAMP";

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String serial_number = form.getString(DevicesConstants.PARAM_SERIAL_NUMBER, false);
		String firmware_version = form.getString(DevicesConstants.PARAM_FIRMWARE_VERSION, false);
		String customer_id = form.getString(DevicesConstants.PARAM_CUSTOMER_ID, false);
		String location_id = form.getString(DevicesConstants.PARAM_LOCATION_ID, false);
		String device_type_id = form.getString(DevicesConstants.PARAM_DEVICE_TYPE_ID, false);
		String comm_method = form.getString(DevicesConstants.PARAM_COMM_METHOD, false);
		String ev_number = form.getString(DevicesConstants.PARAM_EV_NUMBER, false);
		String enabled = form.getString(DevicesConstants.PARAM_ENABLED, false);
		if(enabled == null)
			enabled = "Y";
		String terminal_id = form.getString(DevicesConstants.PARAM_TERMINAL_ID, false);
		String merchant_id = form.getString(DevicesConstants.PARAM_MERCHANT_ID, false);
		String authority_id = form.getString(DevicesConstants.PARAM_AUTHORITY_ID, false);
		String credential_id = form.getString(DevicesConstants.PARAM_CREDENTIAL_ID, false);

		StringBuilder sql = new StringBuilder(SQL_BASE);
		Map<Integer, String> params = new LinkedHashMap<Integer, String>();
		int paramCnt = 0;
		String search_param = form.getString(DevicesConstants.PARAM_SEARCH_PARAM, false);
		String search_type = form.getString(DevicesConstants.PARAM_SEARCH_TYPE, false);
		if(search_param != null && search_param.length() > 0) {
			if(DevicesConstants.PARAM_SERIAL_NUMBER.equalsIgnoreCase(search_type)) {
				sql.append("AND d.device_serial_cd like ? ");
				params.put(paramCnt++, Helper.convertParam(search_param));
			} else if(DevicesConstants.PARAM_EV_NUMBER.equalsIgnoreCase(search_type)) {
				sql.append("AND d.device_name like ? ");
				params.put(paramCnt++, Helper.convertParam(search_param));
			} else if(DevicesConstants.PARAM_CORP_CUSTOMER_NAME.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND TRIM(cc.customer_name) IS NULL ");
				else {
					sql.append(" AND LOWER(cc.customer_name) LIKE LOWER(?) ");
					params.put(paramCnt++, Helper.convertParam(search_param, false));
				}
			} else if(DevicesConstants.PARAM_CUSTOMER_NAME.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND TRIM(c.customer_name) IS NULL ");
				else {
					sql.append(" AND LOWER(c.customer_name) LIKE LOWER(?) ");
					params.put(paramCnt++, Helper.convertParam(search_param, false));
				}
			} else if(DevicesConstants.PARAM_LOCATION_NAME.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND TRIM(l.location_name) IS NULL ");
				else {
					sql.append(" AND LOWER(l.location_name) LIKE LOWER(?) ");
					params.put(paramCnt++, Helper.convertParam(search_param, false));
				}
			} else if(DevicesConstants.PARAM_FIRMWARE_VERSION.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND d.firmware_version IS NULL ");
				else {
					sql.append(" AND d.firmware_version LIKE ? ");
					params.put(paramCnt++, search_param + "%");
				}
			} else if(DevicesConstants.PARAM_DIAG_APP_VERSION.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND dd.diag_app_version IS NULL ");
				else {
					sql.append(" AND dd.diag_app_version LIKE ? ");
					params.put(paramCnt++, search_param + "%");
				}
			} else if(DevicesConstants.PARAM_PTEST_VERSION.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND dd.ptest_version IS NULL ");
				else {
					sql.append(" AND dd.ptest_version LIKE ? ");
					params.put(paramCnt++, search_param + "%");
				}
			} else if(DevicesConstants.PARAM_PROPERTY_LIST_VERSION.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND dd.property_list_version IS NULL ");
				else {
					sql.append(" AND dd.property_list_version = ? ");
					params.put(paramCnt++, search_param);
				}
			} else if(DevicesConstants.PARAM_HOST_SERIAL_NUMBER.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND d.device_id IN (SELECT device_id FROM device.host WHERE host_serial_cd IS NULL) ");
				else {
					sql.append(" AND d.device_id IN (SELECT device_id FROM device.host WHERE host_serial_cd LIKE ?) ");
					params.put(paramCnt++, search_param + "%");
				}
			} else if(DevicesConstants.PARAM_BEZEL_MFGR.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND b_he.host_equipment_mfgr IS NULL ");
				else {
					sql.append(" AND b_he.host_equipment_mfgr LIKE ? ");
					params.put(paramCnt++, search_param + "%");
				}
			} else if(DevicesConstants.PARAM_BEZEL_APP_VER.equalsIgnoreCase(search_type)) {
				if(DevicesConstants.NULL.equalsIgnoreCase(search_param))
					sql.append(" AND b_app_ver.host_setting_value IS NULL ");
				else {
					sql.append(" AND b_app_ver.host_setting_value LIKE ? ");
					params.put(paramCnt++, search_param + "%");
				}
			}
		}
		if(ev_number != null && ev_number.length() > 0) {
			sql.append("AND d.device_name like ? ");
			params.put(paramCnt++, Helper.convertParam(ev_number));
		}
		if(device_type_id != null && device_type_id.length() > 0) {
			sql.append("AND d.device_type_id = CAST (? AS numeric) ");
			params.put(paramCnt++, device_type_id);
		}
		if(comm_method != null && comm_method.length() > 0) {
			sql.append("AND d.comm_method_cd = ? ");
			params.put(paramCnt++, comm_method);
		}
		if(location_id != null && location_id.length() > 0) {
			sql.append("AND l.location_id = CAST (? AS numeric) ");
			params.put(paramCnt++, location_id);
		}
		if(customer_id != null && customer_id.length() > 0) {
			sql.append("AND c.customer_id = CAST (? AS numeric)");
			params.put(paramCnt++, customer_id);
		}
		if(serial_number != null && serial_number.length() > 0) {
			sql.append("AND d.device_serial_cd like ? ");
			params.put(paramCnt++, Helper.convertParam(serial_number));
		}
		if(firmware_version != null && firmware_version.length() > 0) {
			if(DevicesConstants.NULL.equalsIgnoreCase(firmware_version))
				sql.append("AND d.firmware_version IS NULL ");
			else {
				sql.append("AND d.firmware_version = ? ");
				params.put(paramCnt++, firmware_version);
			}
		}
		if(!enabled.equalsIgnoreCase("A")) {
			sql.append("AND d.device_active_yn_flag = ? ");
			params.put(paramCnt++, enabled);
		}
		if(terminal_id != null && terminal_id.length() > 0) {
			sql.append("AND EXISTS (SELECT 1 FROM pss.pos_pta pp, pss.payment_subtype ps WHERE p.pos_id = pp.pos_id").append(
					" AND pp.pos_pta_activation_ts < " + SQL_SYSDATE + " AND (pp.pos_pta_deactivation_ts IS NULL OR " + SQL_SYSDATE + " < pp.pos_pta_deactivation_ts)").append(
					" AND pp.payment_subtype_id = ps.payment_subtype_id AND ps.payment_subtype_table_name = 'TERMINAL' AND pp.payment_subtype_key_id = CAST (? AS numeric)) ");
			params.put(paramCnt++, terminal_id);
		}
		if(merchant_id != null && merchant_id.length() > 0) {
			sql.append("AND EXISTS (SELECT 1 FROM pss.pos_pta pp, pss.payment_subtype ps, pss.terminal t WHERE p.pos_id = pp.pos_id").append(
					" AND pp.pos_pta_activation_ts < " + SQL_SYSDATE + " AND (pp.pos_pta_deactivation_ts IS NULL OR " + SQL_SYSDATE + " < pp.pos_pta_deactivation_ts)").append(
					" AND pp.payment_subtype_id = ps.payment_subtype_id AND ps.payment_subtype_table_name = 'TERMINAL' AND pp.payment_subtype_key_id = t.terminal_id").append(
					" AND t.merchant_id = CAST (? AS numeric) ) ");
			params.put(paramCnt++, merchant_id);
		}
		if(authority_id != null && authority_id.length() > 0) {
			sql.append("AND EXISTS (SELECT 1 FROM pss.pos_pta pp, pss.payment_subtype ps, pss.terminal t, pss.merchant m WHERE p.pos_id = pp.pos_id").append(
					" AND pp.pos_pta_activation_ts < " + SQL_SYSDATE + " AND (pp.pos_pta_deactivation_ts IS NULL OR " + SQL_SYSDATE + " < pp.pos_pta_deactivation_ts)").append(
					" AND pp.payment_subtype_id = ps.payment_subtype_id AND ps.payment_subtype_table_name = 'TERMINAL'").append(
					" AND pp.payment_subtype_key_id = t.terminal_id AND t.merchant_id = m.merchant_id AND m.authority_id = CAST (? AS numeric)) ");
			params.put(paramCnt++, authority_id);
		}
		if (!StringHelper.isBlank(credential_id)){
			sql.append("AND d.credential_id = CAST (? AS numeric) ");
			params.put(paramCnt++, credential_id);
		}

		String paramTotalCount = PaginationUtil.getTotalField(null);
		String paramPageIndex = PaginationUtil.getIndexField(null);
		String paramPageSize = PaginationUtil.getSizeField(null);
		String paramSortIndex = PaginationUtil.getSortField(null);

		try {
			// pagination parameters
			int totalCount = form.getInt(paramTotalCount, false, -1);
			if(totalCount == -1) {
				// if the total count is not retrieved yet, get it now
				String SqlText = new StringBuilder("SELECT COUNT(1), MAX(device_id) ").append(sql).append(SQL_END).append(" gen_alias").toString(); 
				Results total = DataLayerMgr.executeSQL("OPER", SqlText, params.values().toArray(), null);
				if(total.next()) {
					totalCount = total.getValue(1, int.class);
					if(totalCount == 1) {
						form.setRedirectUri(new StringBuilder("/profile.i?device_id=").append(total.getValue(2, String.class)).toString());
						return;
					}
				} else {
					totalCount = 0;
				}
				if(log.isDebugEnabled())
					log.debug(">>>> " + totalCount + " Records found....");
				request.setAttribute(paramTotalCount, String.valueOf(totalCount));
			}

			int pageIndex = form.getInt(paramPageIndex, false, 1);
			int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
			int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
			int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

			String sortIndex = form.getString(paramSortIndex, false);
			sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
			String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex, REQUIRED_SORT_INDEXES);
			
			String query;
			if (DialectResolver.isOracle()) {
				query = new StringBuilder("select * from (select pagination_temp.*, ROWNUM rnum from (")
						.append(SQL_START).append(sql).append(orderBy).append(SQL_END)
						.append(") pagination_temp where ROWNUM <= ?) where rnum  >= ? ").toString();

			} else {
				query = new StringBuilder("select * from (select pagination_temp.*, row_number()over() rnum from (")
						.append(SQL_START).append(sql).append(orderBy).append(SQL_END)
						.append(" gen_alias) pagination_temp LIMIT CAST (? AS numeric)) as gen_alias where rnum  >= CAST (? AS numeric) ")
						.toString();
			}

			params.put(paramCnt++, String.valueOf(maxRowToFetch));
			params.put(paramCnt++, String.valueOf(minRowToFetch));
			Results results = DataLayerMgr.executeSQL("OPER", query, params.values().toArray(), null);
			request.setAttribute("deviceList", results);
		} catch(Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}

	}

}
