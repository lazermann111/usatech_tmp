/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.device.DevicesConstants;

public class PosptaTmplEntryEditStep extends AbstractStep
{

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        int pos_pta_tmpl_id = form.getInt(DevicesConstants.PARAM_POS_PTA_TMPL_ID, false, -1);
        String errorMessage = null;
        if (pos_pta_tmpl_id < 0)
        {
            errorMessage = "Required Parameter Not Found:pos_pta_tmpl_id";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        int pos_pta_tmpl_entry_id = form.getInt("pos_pta_tmpl_entry_id", false, 0);
        int pp_payment_subtype_id = form.getInt("payment_subtype_id", false, 0);
        if (pos_pta_tmpl_entry_id == 0 && pp_payment_subtype_id == 0)
        {
            errorMessage = "Required Parameter Not Found: payment_subtype_id";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }

        try
        {
            Results template_data = DataLayerMgr.executeQuery("GET_PTA_TEMPLATE_DATA", new Object[] {pos_pta_tmpl_id}, true);
            if (template_data == null || !template_data.next())
            {
                errorMessage = "Template not found: " + pos_pta_tmpl_id;
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            request.setAttribute("template_data", template_data);
            String payment_action_type_cd = null;
            String payment_entry_method_cd = null;
            int ps_authority_payment_mask_id = -1;
            int pp_authority_payment_mask_id = -1;
            String ps_payment_subtype_key_name = null;
            String pp_pos_pta_encrypt_key = "";
            String pp_pos_pta_encrypt_key2 = "";
            String ps_payment_subtype_table_name = "";
            String ps_payment_subtype_key_desc_name = "";
            String sysDate = null;
            String pp_pos_pta_device_serial_cd = "";
            int pp_payment_subtype_key_id = -1;
            String pp_pos_pta_pin_req_yn_flag = "";
            String ps_payment_subtype_name = "";
            int pp_pos_pta_activation_oset_hr = -1;
            int pp_pos_pta_deactivation_oset_hr = -1;
            int pp_pos_pta_priority = -1;
            int pp_terminal_id = -1;
            int pp_merchant_bank_acct_id = -1;
            String pp_currency_cd = "";
            String payment_action_type_desc = "";
            String payment_entry_method_desc = "";
            String pp_pos_pta_passthru_allow_yn_flag = "";
            String pp_pos_pta_disable_debit_denial = "";
            String pp_no_convenience_fee = "";
            float pos_pta_pref_auth_amt = -1;
            float pos_pta_pref_auth_amt_max = -1;
            if (pos_pta_tmpl_entry_id > 0)
            {
                // ### load settings for pos_pta_tmpl_entry ###
                Results pos_pta_data = DataLayerMgr.executeQuery("GET_TEMPLATE_ENTRY_SETTING", new Object[] {pos_pta_tmpl_entry_id}, true);
                if (pos_pta_data == null || !pos_pta_data.next())
                {

                    errorMessage = "pos_pta_tmpl_entry_id " + pos_pta_tmpl_entry_id + " not found!";
                    request.setAttribute("errorMessage", errorMessage);
                    return;
                }
                pp_pos_pta_device_serial_cd = pos_pta_data.getFormattedValue(1);
                String temp = pos_pta_data.getFormattedValue(4);
                pp_payment_subtype_key_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
                pp_pos_pta_pin_req_yn_flag = pos_pta_data.getFormattedValue(5);
                ps_payment_subtype_name = pos_pta_data.getFormattedValue(12);
                temp = pos_pta_data.getFormattedValue(14);
                pp_pos_pta_activation_oset_hr = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
                temp = pos_pta_data.getFormattedValue(15);
                pp_pos_pta_deactivation_oset_hr = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
                temp = pos_pta_data.getFormattedValue(16);
                pp_pos_pta_priority = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
                temp = pos_pta_data.getFormattedValue(17);
                pp_terminal_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
                temp = pos_pta_data.getFormattedValue(18);
                pp_merchant_bank_acct_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
                pp_currency_cd = pos_pta_data.getFormattedValue(19);
                payment_action_type_desc = pos_pta_data.getFormattedValue("payment_action_type_desc");
                payment_entry_method_desc = pos_pta_data.getFormattedValue(21);
                pp_pos_pta_passthru_allow_yn_flag = pos_pta_data.getFormattedValue(22);
                temp = pos_pta_data.getFormattedValue(23);
                pos_pta_pref_auth_amt = (temp != null && temp.length() > 0) ? Float.parseFloat(temp) : -1;
                temp = pos_pta_data.getFormattedValue(24);
                pos_pta_pref_auth_amt_max = (temp != null && temp.length() > 0) ? Float.parseFloat(temp) : -1;
                pp_pos_pta_disable_debit_denial = pos_pta_data.getFormattedValue(25);
                pp_no_convenience_fee = pos_pta_data.getFormattedValue(26);
                payment_entry_method_cd = pos_pta_data.getFormattedValue(20);
                payment_action_type_cd = pos_pta_data.getFormattedValue("payment_action_type_cd");
                temp = pos_pta_data.getFormattedValue(6);
                ps_authority_payment_mask_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
                temp = pos_pta_data.getFormattedValue(7);
                pp_authority_payment_mask_id = (temp != null && temp.length() > 0) ? Integer.parseInt(temp) : -1;
                ps_payment_subtype_key_name = pos_pta_data.getFormattedValue(10);
                pp_pos_pta_encrypt_key = pos_pta_data.getFormattedValue(2);
                pp_pos_pta_encrypt_key2 = pos_pta_data.getFormattedValue(3);
                ps_payment_subtype_table_name = pos_pta_data.getFormattedValue(9);
                ps_payment_subtype_key_desc_name = pos_pta_data.getFormattedValue(11);
                sysDate = pos_pta_data.getFormattedValue(13);
                if (pp_payment_subtype_id == 0)
                {
                    pp_payment_subtype_id = Integer.parseInt(pos_pta_data.getFormattedValue(8));
                }
                else
                {
                    Results subtypeData = DataLayerMgr.executeQuery("GET_PAYMENT_SUBTYPE", new Object[] {pp_payment_subtype_id}, true);
                    if (subtypeData.next())
                    {
                        ps_authority_payment_mask_id = subtypeData.getFormattedValue(1) == null ? -1 : Integer.parseInt(subtypeData.getFormattedValue(1));
                        ps_payment_subtype_key_name = subtypeData.getFormattedValue(3);
                        ps_payment_subtype_table_name = subtypeData.getFormattedValue(2);
                        ps_payment_subtype_key_desc_name = subtypeData.getFormattedValue(4);
                        ps_payment_subtype_name = subtypeData.getFormattedValue(5);
                    }

                }
            }
            else
            {
                // ### load settings for payment_subtype ###
                Results subtypeData = DataLayerMgr.executeQuery("GET_PAYMENT_SUBTYPE_SETTINGS", new Object[] {pp_payment_subtype_id}, true);
                if (subtypeData == null || !subtypeData.next())
                {
                    errorMessage = "payment_subtype_id " + pp_payment_subtype_id + " not found!";
                    request.setAttribute("errorMessage", errorMessage);
                    return;
                }
                ps_authority_payment_mask_id = subtypeData.getFormattedValue(1) == null ? -1 : Integer.parseInt(subtypeData.getFormattedValue(1));
                payment_action_type_cd = subtypeData.getFormattedValue("payment_action_type_cd");
                payment_entry_method_cd = subtypeData.getFormattedValue(7);
                ps_payment_subtype_key_name = subtypeData.getFormattedValue(3);
                ps_payment_subtype_table_name = subtypeData.getFormattedValue(2);
                ps_payment_subtype_key_desc_name = subtypeData.getFormattedValue(4);
                sysDate = subtypeData.getFormattedValue(6);
                payment_action_type_desc = subtypeData.getFormattedValue("payment_action_type_desc");
                payment_entry_method_desc = subtypeData.getFormattedValue(8);
                ps_payment_subtype_name = subtypeData.getFormattedValue(5);
            }
            Results currency_list = (Results)DataLayerMgr.executeQuery("GET_CURRENCY", null, true);
            request.setAttribute("currency_list", currency_list);
            Results payment_subtypes = (Results)DataLayerMgr.executeQuery("GET_AVAILABLE_SUBTYPE_BY_METHOD_CD", new Object[] {payment_action_type_cd, payment_entry_method_cd}, true);
            if (payment_subtypes == null || !payment_subtypes.next())
            {
                errorMessage = "No payment types found";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            payment_subtypes.setRow(0);
            request.setAttribute("payment_subtypes", payment_subtypes);
            int authority_payment_mask_id = pp_authority_payment_mask_id > 0 ? pp_authority_payment_mask_id : ps_authority_payment_mask_id;
            Results authority_payment_mask_id_data = DataLayerMgr.executeQuery("GET_AUTHORITY_PAYMENT_MASK", new Object[] {authority_payment_mask_id}, true);
            if (authority_payment_mask_id_data == null || !authority_payment_mask_id_data.next())
            {
                errorMessage = "authority_payment_mask_id " + authority_payment_mask_id + " not found!";
                request.setAttribute("errorMessage", errorMessage);
                return;
            }
            String regex_final = authority_payment_mask_id_data.getFormattedValue(1);
            String regex_final_bref = authority_payment_mask_id_data.getFormattedValue(2);
            String regex_final_name = authority_payment_mask_id_data.getFormattedValue(3);
            String regex_final_desc = authority_payment_mask_id_data.getFormattedValue(4);
            // ### some text formatting ###
            String payment_subtype_key_name_formatted = ps_payment_subtype_key_name.toLowerCase();
            Pattern p = Pattern.compile("[^a-z]|id$");
            Matcher m = p.matcher(payment_subtype_key_name_formatted);
            payment_subtype_key_name_formatted = m.replaceAll(" ");
            String[] words = payment_subtype_key_name_formatted.split("\\s");
            payment_subtype_key_name_formatted = "";
            for (int i = 0; i < words.length; i++)
            {
                words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1);
                payment_subtype_key_name_formatted += words[i] + " ";
            }
            // payment_subtype_key_name_formatted =~ s/[^a-z]|id$/ /gio;
            // payment_subtype_key_name_formatted =~ s/\b(\w)/\U$1/go;
            String pos_pta_encrypt_key_hex = (pp_pos_pta_encrypt_key != null && pp_pos_pta_encrypt_key.length() > 0) ? "CHECKED" : "";
            String pos_pta_encrypt_key2_hex = (pp_pos_pta_encrypt_key2 != null && pp_pos_pta_encrypt_key2.length() > 0) ? "CHECKED" : "";
            Results regex_types = (Results)DataLayerMgr.executeQuery("GET_REGEX_TYPES", new Object[] {payment_action_type_cd, payment_entry_method_cd}, true);

            // ### load backref settings ###
            Results regex_bref_types = (Results)DataLayerMgr.executeQuery("GET_REGEX_BREF_TYPES", null, true);
            // ### load other settings ###
			Results payment_subtype_table_key_ids = DataLayerMgr.executeQuery("GET_PAYMENT_SUBTYPE_KEY_IDS", new Object[] { null, pp_payment_subtype_id }, true);
			request.setAttribute("payment_subtype_table_key_ids", payment_subtype_table_key_ids);

			// ### query the database to get current date ###
            String[] curData = sysDate.split(" ");
            String[] curDate = curData[0].split("/");
            // ### generate form select elements ###
            String default_found = null;
            LinkedList<HashMap> toggle_regex_methods = new LinkedList<HashMap>();
            HashMap<String, String> map = new HashMap<String, String>();
            while (regex_types.next())
            {
                map = new HashMap<String, String>();
                map.put("value", regex_types.getFormattedValue(2));
                if (default_found == null && regex_types.getFormattedValue(4).equalsIgnoreCase(String.valueOf(authority_payment_mask_id))){
                    default_found = "selected";
                    map.put("default", default_found);
                }else
                	map.put("default", "");
                map.put("text", regex_types.getFormattedValue(1));
                map.put("bref", regex_types.getFormattedValue(3));
                map.put("id", regex_types.getFormattedValue(4));
                toggle_regex_methods.add(map);
            }
            if (default_found == null)
            {
                map = new HashMap<String, String>();
                map.put("value", regex_final);
                map.put("default", "selected");
                map.put("text", "Custom method");
                map.put("bref", regex_final_bref);
                map.put("id", "");
                toggle_regex_methods.addFirst(map);
            }

            request.setAttribute("toggle_regex_methods", toggle_regex_methods);

            // ### compile settings into javascript-readable arrays ###
            StringBuilder regex_bref_codes_str = new StringBuilder();
            StringBuilder regex_bref_names_str = new StringBuilder();
            StringBuilder regex_bref_descs_str = new StringBuilder();
            while (regex_bref_types.next())
            {
                regex_bref_codes_str.append(",'" + regex_bref_types.getFormattedValue(1) + "'");
                regex_bref_names_str.append(",'" + regex_bref_types.getFormattedValue(2) + "'");
                regex_bref_descs_str.append(",'" + regex_bref_types.getFormattedValue(3) + "'");
            }

            StringBuilder regex_ids_str = new StringBuilder();
            StringBuilder regex_methods_str = new StringBuilder();
            StringBuilder regex_bref_methods_str = new StringBuilder();
            for (HashMap method : toggle_regex_methods)
            {
                regex_ids_str.append(",\"" + method.get("id") + "\"");
                regex_methods_str.append(",\"" + method.get("value") + "\"");
                regex_bref_methods_str.append(",\"" + method.get("bref") + "\"");
            }

            HashMap<String, String> params = new HashMap<String, String>();
            params.put("pos_pta_tmpl_id", String.valueOf(pos_pta_tmpl_id));
            params.put("pos_pta_tmpl_entry_id", String.valueOf(pos_pta_tmpl_entry_id));
            params.put("payment_action_type_cd", payment_action_type_cd);
            params.put("payment_entry_method_cd", payment_entry_method_cd);
            params.put("pp_payment_subtype_id", pp_payment_subtype_id >= 0 ? String.valueOf(pp_payment_subtype_id) : "");
            params.put("pp_payment_subtype_key_id", pp_payment_subtype_key_id >= 0 ? String.valueOf(pp_payment_subtype_key_id) : "");
            params.put("pp_authority_payment_mask_id", pp_authority_payment_mask_id >= 0 ? String.valueOf(pp_authority_payment_mask_id) : "");
            params.put("authority_payment_mask_id", authority_payment_mask_id >= 0 ? String.valueOf(authority_payment_mask_id) : "");
            params.put("regex_final", regex_final);
            params.put("regex_final_bref", regex_final_bref);
            params.put("pp_pos_pta_device_serial_cd", pp_pos_pta_device_serial_cd);
            params.put("pp_pos_pta_encrypt_key", pp_pos_pta_encrypt_key);
            params.put("pp_pos_pta_encrypt_key2", pp_pos_pta_encrypt_key2);
            params.put("pp_pos_pta_pin_req_yn_flag", pp_pos_pta_pin_req_yn_flag);
            params.put("pp_pos_pta_passthru_allow_yn_flag", pp_pos_pta_passthru_allow_yn_flag);
            params.put("pp_pos_pta_disable_debit_denial", pp_pos_pta_disable_debit_denial);
            params.put("pp_no_convenience_fee", pp_no_convenience_fee);
            params.put("pos_pta_pref_auth_amt", pos_pta_pref_auth_amt >= 0 ? String.valueOf(pos_pta_pref_auth_amt) : "");
            params.put("pos_pta_pref_auth_amt_max", pos_pta_pref_auth_amt_max >= 0 ? String.valueOf(pos_pta_pref_auth_amt_max) : "");
            params.put("payment_subtype_key_name_formatted", payment_subtype_key_name_formatted);
            params.put("payment_action_type_desc", payment_action_type_desc);
            params.put("payment_entry_method_desc", payment_entry_method_desc);
            params.put("pp_pos_pta_activation_oset_hr", pp_pos_pta_activation_oset_hr >= 0 ? String.valueOf(pp_pos_pta_activation_oset_hr) : "");
            params.put("pp_pos_pta_deactivation_oset_hr", pp_pos_pta_deactivation_oset_hr >= 0 ? String.valueOf(pp_pos_pta_deactivation_oset_hr) : "");
            params.put("pos_pta_encrypt_key_hex", pos_pta_encrypt_key_hex);
            params.put("pos_pta_encrypt_key2_hex", pos_pta_encrypt_key2_hex);
            params.put("pp_currency_cd", pp_currency_cd);
            params.put("pp_pos_pta_priority", pp_pos_pta_priority >= 0 ? String.valueOf(pp_pos_pta_priority) : "");

            params.put("regex_ids_str", regex_ids_str.toString().substring(1));
            params.put("regex_methods_str", regex_methods_str.toString().substring(1));
            params.put("regex_bref_methods_str", regex_bref_methods_str.toString().substring(1));           
            String temp = regex_bref_codes_str.toString();
            if (temp.length() > 0)
            {
                temp = temp.substring(1);
            }
            params.put("regex_bref_codes_str", temp);
            temp = regex_bref_names_str.toString();
            if (temp.length() > 0)
            {
                temp = temp.substring(1);
            }
            params.put("regex_bref_names_str", temp);
            temp = regex_bref_descs_str.toString();
            if (temp.length() > 0)
            {
                temp = temp.substring(1);
            }
            params.put("regex_bref_descs_str", temp);
            request.setAttribute("params", params);

        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
