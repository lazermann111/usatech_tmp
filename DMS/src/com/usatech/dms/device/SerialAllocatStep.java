/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.device;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.NameValuePair;

import com.usatech.dms.action.SerialAllocatAction;

public class SerialAllocatStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {

        String txtSerialNumber = form.getString("txtSerialNumber", false);
        String userOP = form.getString("userOP", false);
        String insertType = form.getString("insertType", false);
        String email = form.getString("email", false);
        String desc = form.getString("desc", false);
        int deviceTypeId = form.getInt("deviceTypeId", false, -1);
        int serialFormatId = form.getInt("serialFormatId", false, -1);
        long numToAllocate;
        try {
        	numToAllocate = form.getLong("numToAllocate", false, 0);
        } catch (Exception e) {
        	throw new ServletException("Please enter a valid numeric \"Number of Serial Num to Allocate\"");
        }
        long numToStart;
        try {
        	numToStart = form.getLong("numToStart", false, 0);
        } catch (Exception e) {
        	throw new ServletException("Please enter a valid numeric \"Serial Number to Start At\"");
        }

        try
        {
            if (("Assign Numbers".equals(userOP)) && (deviceTypeId >= 0) && (serialFormatId > 0))
            {
                form.setAttribute("regex", SerialAllocatAction.getSerialRegex(deviceTypeId));
                NameValuePair prefixSuffix = SerialAllocatAction.getPrefixSuffix(serialFormatId);
                form.setAttribute("prefix", (prefixSuffix == null) ? null : prefixSuffix.getName());
                form.setAttribute("suffix", (prefixSuffix == null) ? null : prefixSuffix.getValue());
               
                long sequenceKey = SerialAllocatAction.getSequence(serialFormatId);
                form.set("sequenceKey", sequenceKey);
                List<Long> sequences = (List<Long>)SerialAllocatAction.sequenceMap.get(sequenceKey);
                form.setAttribute("sequences", sequences);
            }
            else if ("List Assigned Logs".equals(userOP))
            {
                Long infoId = null;
                Results serialInfoBySerialNumber = SerialAllocatAction.getSerialInfoBySerialNumber(txtSerialNumber.toUpperCase());
                while (serialInfoBySerialNumber.next())
                {
                    infoId = serialInfoBySerialNumber.getValue("device_serial_info_id", Long.class);
                }
                serialInfoBySerialNumber.setRow(0);
                form.setAttribute("serialInfoBySerialNumber", serialInfoBySerialNumber);
                if (infoId != null)
                {
                    form.setAttribute("serialInfoByInfoId", SerialAllocatAction.getSerialInfoByInfoId(infoId));
                }
            }
            else if ("Submit".equals(userOP) && serialFormatId > 0 && numToAllocate >= 0)
            {
            	List<String> assigned = null;
            	//# now make sure that if sequential record is asked 
            	//# there is no existing record in the sequence
            	boolean range_flag = true;
            	long sequenceKey = form.getLong("sequenceKey", false, -1);
            	//List<Long> sequence = (List<Long>)form.getAttribute("sequences");
            	try{
	            	List<Long> sequence = (List<Long>)SerialAllocatAction.sequenceMap.get(sequenceKey);
	            	if("S".equalsIgnoreCase(insertType)){
	            		
	            	    long count = numToAllocate;
	            	    long begin = numToStart;
	            	    long end   = begin + count;
	            	    if(sequence!=null){
		            	    for(int i = 1; i < sequence.size(); i++ ) {
		            	    	if(begin > sequence.get(i-1) &&  begin < sequence.get(i)) {
		            	    		if(end   > sequence.get(i-1) && end   <= sequence.get(i)) {
		            	    			//; # everything is fine
		            	    			range_flag = false;
		            	    		} else {
		            	    			sequence = null;
		            	    			break;
		        	             	}
		            	    	}
		            	    }
	            	    }
	            		
	            	    
	            	}
	            	if((!"S".equalsIgnoreCase(insertType) || !range_flag ) && sequence!=null){
	            		
		                //List<String> assigned = SerialAllocatAction.getAssigned(serialFormatId, numToStart, numToAllocate, insertType);
		            	assigned = SerialAllocatAction.getAssigned(serialFormatId, numToStart, numToAllocate, insertType, sequence);
		                if (!assigned.isEmpty())
		                {
		                    SerialAllocatAction.assignSerials(email, desc, serialFormatId, assigned, insertType);
		                }
	            	}
	                form.setAttribute("assigned", assigned);
            	}catch(SQLIntegrityConstraintViolationException e){
            		log.error("Exception occurred in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage() + " : Serial numbers may already exist in the db!"), e);
            		assigned = null;
            		return;
            	
            	}catch(Exception e){
            		throw e;
            		
            	}finally{            		
            		try{SerialAllocatAction.clearSequenceMap(-1);}catch(ServletException e){throw e;}
            	}
            }
            else
            {
                Results deviceTypes = SerialAllocatAction.getDeviceTypes();
                Map<Integer, List<NameValuePair>> serialFormats = SerialAllocatAction.getSerialFormats(deviceTypes);
                deviceTypes.setRow(0);
                form.setAttribute("deviceTypes", deviceTypes);
                form.setAttribute("serialFormats", serialFormats);
            }
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

	

}
