package com.usatech.dms.device;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.ibm.icu.text.SimpleDateFormat;

public class BlacklistStep extends AbstractStep {

	public static final SimpleDateFormat declineUntilFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	private static Map<Integer, String> periods = new LinkedHashMap<>();

	public BlacklistStep() {
		periods.put(5, "5 Minutes");
		periods.put(10, "10 Minutes");
		periods.put(15, "15 Minutes");
		periods.put(30, "30 Minutes");
		periods.put(60, "1 Hour");
		periods.put(120, "2 Hours");
		periods.put(360, "6 Hours");
		periods.put(720, "12 Hours");
		periods.put(1440, "1 Day");
		periods.put(2880, "2 Days");
		periods.put(4320, "3 Days");
		periods.put(10080, "1 Week");
		periods.put(20160, "2 Weeks");
	}

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		form.set("periods", periods);
		String action = form.getStringSafely("action", null);
		if ("Submit".equalsIgnoreCase(action)) {
			long deviceId = form.getLong("device_id", true, -1);
			long posId = form.getLong("pos_id", true, -1);
			String handlerClass = form.getString("handler_class", true);
			String operation = form.getString("operation", true);
			String type = form.getString("type", true);
			
			if (type.equalsIgnoreCase("W")) {
				try {
					DataLayerMgr.executeCall("UPDATE_WHITELIST", new Object[] {posId,handlerClass,operation}, true);
					form.setRedirectUri(String.format("paymentConfig.i?device_id=%s&userOP=payment_types", deviceId));
				} catch (Exception e) {
					throw new ServletException("UPDATE_WHITELIST failed", e);
				}
			} else if (type.equalsIgnoreCase("B")) {
				Date declineUntil = null;
				if (operation.equalsIgnoreCase("Y")) {
					try {
						String declineUntilDate = form.getString("decline_until_date", true);
						String declineUntilTime = form.getString("decline_until_time", true);
						String declineUntilDateTimeStr = String.format("%s %s", declineUntilDate, declineUntilTime);
						declineUntil = declineUntilFormat.parse(declineUntilDateTimeStr);
					} catch (Exception e) {
						throw new ServletException("Invalid decline until date/time format!");
					}
				}
	
				try {
					DataLayerMgr.executeCall("DECLINE_UNTIL", new Object[] {posId,handlerClass,declineUntil}, true);
					form.setRedirectUri(String.format("paymentConfig.i?device_id=%s&userOP=payment_types", deviceId));
				} catch (Exception e) {
					throw new ServletException("DECLINE_UNTIL failed", e);
				}
			} else {
			}
		}
	}
}
