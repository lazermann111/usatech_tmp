/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to page 2 of the bulk configuration wizard.
 */
public class BulkConfigWizard6cStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {	
    	int device_type_id = form.getInt("device_type_id", false, -1);
    	if ("Next >".equalsIgnoreCase(form.getString("step6cAction", false))) {
			if (device_type_id > -1)
				form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, "bulkConfigWizard6d.i"); // server to client requests
	    	else
	    		form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, "bulkConfigWizard7.i"); // summary	    			
        	return;
		}
    	
    	form.setAttribute("forward", "bulkConfigWizard6c.i");
    	String myaction = form.getString("myaction", false);
    	if (!StringHelper.isBlank(myaction)) {
    		if ("Upload to Device".equalsIgnoreCase(myaction)) {
    			if (Helper.isLong(form.getStringSafely("file_transfer_id", "").trim()))
    				form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, "/fileDetailsFunc.i");
    			else
    				form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, "/fileList.i");
            } else if ("Schedule Device Upload".equals(myaction)) {
    			long fileId = form.getLong("file_transfer_id", false, 0);
    			int executeOrder = form.getInt("execute_order", false, 0);
    			int packetSize = form.getInt("packet_size", false, 0);		        	
	        	if(fileId < 1 || executeOrder < 0 || packetSize < 1)
	        		throw new ServletException("Invalid file ID, execute order or packet size");
	        			        			        	
	        	if (DeviceActions.populateBulkConfigList(form, "file_uploads", String.valueOf(fileId), true)) {
	        		String fileUpload = new StringBuilder().append(executeOrder).append("~").append(packetSize).toString();
	        		DeviceActions.populateBulkConfigList(form, "file_upload_fields", fileUpload, false);
	        	}
    		} else if ("Download File".equalsIgnoreCase(myaction)) {
    			String downloadFileName = form.getString("download_file_name", false);
    			if(StringHelper.isBlank(downloadFileName) 
    					|| "full path on client".equalsIgnoreCase(downloadFileName) 
    					|| "name or path on client".equalsIgnoreCase(downloadFileName))            		 
	    			throw new ServletException("Required Parameter Not Found: download_file_name");
    			
    			int downloadFileType = form.getInt("download_file_type", false, -1);
    				    			
    			if (DeviceActions.populateBulkConfigList(form, "file_downloads", String.valueOf(downloadFileName), true))
    				DeviceActions.populateBulkConfigList(form, "file_download_fields", String.valueOf(downloadFileType), false);
            } else if("Download Config".equalsIgnoreCase(myaction)) {
            	if (DeviceActions.populateBulkConfigList(form, "s2c_requests", myaction, true))
            		DeviceActions.populateBulkConfigList(form, "s2c_request_fields", myaction, false);
            } else if ("Upgrade Device".equalsIgnoreCase(myaction)) {
            	long firmwareUpgradeId = form.getLong("firmware_upgrade_id", false, 0);
            	DeviceActions.populateBulkConfigList(form, "firmware_upgrades", String.valueOf(firmwareUpgradeId), true);
            }
    	}
    }
}
