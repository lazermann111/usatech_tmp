/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.NameValuePair;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to page 2 of the bulk configuration wizard.
 */
public class BulkConfigWizard4Step extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String changeConfiguration = form.getString("change_configuration", false);
    	String deviceType = form.getString("device_type_id", false);
    	int deviceTypeId = -1;
    	String formAction = null;
    	String editConfigFormAction = null;
    
    	if(!(StringHelper.isBlank(deviceType))){
    		//if value of deviceType non-numeric, just trap exception
    		try{deviceTypeId = new BigDecimal(deviceType).intValue();}catch(Exception e){}
    		
    	}
    	
    	if(!(StringHelper.isBlank(changeConfiguration)) &&(changeConfiguration.equals("0"))){
    		formAction = "bulkConfigWizard6b.i";
    		//just return
    		request.setAttribute("form_action", formAction);
    		return;
    		
    	}else{
    		if(DeviceType.G4.getValue() == deviceTypeId || DeviceType.GX.getValue() == deviceTypeId){
    			formAction = "bulkConfigWizard5.i";
    			editConfigFormAction = "bulkConfigWizard6.i";
    			request.setAttribute("edit_config_form_action", editConfigFormAction);
    		}else if(deviceTypeId == DeviceType.EDGE.getValue() || deviceTypeId == DeviceType.KIOSK.getValue() || deviceTypeId == DeviceType.MEI.getValue()){
    			formAction = "bulkConfigWizard5.i";
    			editConfigFormAction = "bulkConfigWizard6b.i";
    			request.setAttribute("edit_config_form_action", editConfigFormAction);
    		}else{
    			formAction = "bulkConfigWizard6b.i";
    			request.setAttribute("form_action", formAction);
    			//reset change_configuration as we won't be updating params
    			request.setAttribute("change_configuration", "0");
    			return;
    		}
    	}
    	request.setAttribute("form_action", formAction);
    	
    	List<NameValuePair> configTemplateSelections = ConfigFileActions.getConfigTemplates(deviceTypeId, -1);
    	request.setAttribute("config_template_selections", configTemplateSelections);    	
    }
}
