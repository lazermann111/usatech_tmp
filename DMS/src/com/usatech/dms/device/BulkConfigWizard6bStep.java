/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.device;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.constants.DeviceType;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to page 2 of the bulk configuration wizard.
 */
public class BulkConfigWizard6bStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	try{
    		Helper.getSelectList(form, "pos_pta_templates", "GET_POS_PTA_TEMPLATE_SELECTIONS", false, null, null, null, null);
    	} catch (Exception e) {
            throw new ServletException(e);
        }
    	ConfigFileActions.buildParamMap(request, form);    	
    	
    	int device_type_id = form.getInt("device_type_id", false, -1);
    	String form_action;
    	if (device_type_id > -1) {
    		if (DeviceType.G4.getValue() == device_type_id || DeviceType.MEI.getValue() == device_type_id)
    			form_action = "bulkConfigWizard6d.i"; // server to client requests
    		else
    			form_action = "bulkConfigWizard6c.i"; // file transfers
    	} else
    		form_action = "bulkConfigWizard7.i"; // summary	    	
    	
    	form.set("form_action", form_action);
    }
}
