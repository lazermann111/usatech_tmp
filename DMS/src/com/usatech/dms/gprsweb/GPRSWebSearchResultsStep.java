/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.gprsweb;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class GPRSWebSearchResultsStep extends DMSPaginationStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	Map errorMap = new HashMap();
    	String errorMsg = null;
    	String action = form.getString("action", false);
    	int input_validated = 1;
    	if("List SIMs".equalsIgnoreCase(action) || "List SIMs".equalsIgnoreCase(action)){
    		
    		String iccid_first="";
    		String iccid_last="";
    		int count=0;
    		
            String frICCID = request.getParameter("frICCID");
            String toICCID = request.getParameter("toICCID");
            
    		if(!(StringHelper.isBlank(frICCID)) || !(StringHelper.isBlank(toICCID))){
                	if(!frICCID.matches("\\d{20}")){
                    	errorMap.put("frICCID", "From field should contain 20 Digits!");
                    	input_validated = 0;            		
                	}            

                	if(!toICCID.matches("\\d{20}")){
                    	errorMap.put("toICCID", "To field should contain 20 Digits!");
                    	input_validated = 0;            		
                	}
    		}else{
        		iccid_first = form.getString("iccid_first", false);
                iccid_last = form.getString("iccid_last", false);
                String count1 = form.getString("count", false);
                count = form.getInt("count", false, -1);
                
                if((!(StringHelper.isBlank(iccid_first))) && iccid_first.length() != 16){
                	errorMap.put("iccid_first", "Invalid or Missing First 16 Digits!");
                	input_validated = 0;
                }
                
                if(StringHelper.isBlank(iccid_last) || iccid_last.length() < 4){
                	errorMap.put("iccid_last", "Invalid or Missing Last 4 Digits!");
                	input_validated = 0;
                }
                if(StringHelper.isBlank(count1) || count1.length() <=0){
                	errorMap.put("sim_count", "SIM Count is Required!");
                	input_validated = 0;
                }
    		}

            if(input_validated==0){
            	request.setAttribute("errorMap", errorMap);
            	dispatcher.dispatch("/errormap", true);
            	return;
            }
                        
            if(!(StringHelper.isBlank(frICCID)) && !(StringHelper.isBlank(toICCID))){
            	String tempfrICCID = frICCID; 
            	String temptoICCID = toICCID;
            	iccid_first = tempfrICCID.substring(0, 16);
            	String fr_num = tempfrICCID.substring(0, 19);
            	String to_num = temptoICCID.substring(0, 19);

            	count = new Long(to_num).intValue() - new Long(fr_num).intValue() + 1;
            	iccid_last = tempfrICCID.substring(16);
            	
            }
    		
            input_validated = 1;

            if((!(StringHelper.isBlank(iccid_last))) && iccid_last.length() == 4){
            	iccid_last = iccid_last.substring(0, 3);
            }
    		
            String iccid = iccid_first+iccid_last+"0";
            
            if(input_validated==0){
            	request.setAttribute("errorMap", errorMap);
            	return;
            }else{
            	request.setAttribute("iccid", iccid);
            	request.setAttribute("count", count);
            	return;
            }
            
    	}else if("Search SIMs".equalsIgnoreCase(action)){
    		int status = form.getInt("status", false, -1);
            String billable_to_name = form.getString("billable_to_name", false);
            String allocated_to_name = form.getString("allocated_to", false);
            String rate_plan = form.getString("rate_plan_name", false);
            
            if(StringHelper.isBlank(billable_to_name)){billable_to_name = null;}
            if(StringHelper.isBlank(allocated_to_name)){allocated_to_name = null;}
            if(StringHelper.isBlank(rate_plan)){rate_plan = null;}
        
	    	try{
	    		//prepare the sql params needed for query
	    		Object[] params = {	status, status,  // these params retrieve if acct_cd contains{'C'} characters in 'cardNumber'
	    							billable_to_name, billable_to_name,  // these params retrieve if acct_cd begins{'B'} with characters in 'cardNumber'
	    							allocated_to_name, allocated_to_name,  // these params retrieve if acct_cd ends{'E'} with characters in 'cardNumber'
	    							rate_plan, rate_plan, 
	    							"Y".equalsIgnoreCase(form.getString("disabled", false)) ? "D" : "A"};
	    			    		
	    		String sortField = PaginationUtil.getSortField(null);
	    	    String sortIndex = form.getString(sortField, false);
	    	    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
	    	    request.setAttribute("sortIndex", sortIndex);
	    		
	    		setPaginatedResultsOnRequest(form, request, GPRSWEBConstants.SQL_GPRSWEB_SEARCH_RESULTS_START, GPRSWEBConstants.SQL_GPRSWEB_SEARCH_RESULTS_BASE, "", 
	    				GPRSWEBConstants.DEFAULT_SORT_INDEX_GPRSWEB_SEARCH_RESULTS,
	    				GPRSWEBConstants.SORT_FIELDS_GET_FILE_LIST, "gprsweb_search_results", params);
	    		   		
	    		
	    	}catch(Exception e){
	    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
	    	}
        
    	}

    }
    
}
