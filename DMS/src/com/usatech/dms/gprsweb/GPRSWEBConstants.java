/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.gprsweb;

import java.util.HashMap;
import java.util.Map;


public class GPRSWEBConstants
{   
	public static final String PARAM_ACTION = "action";
    public static final String PARAM_ICCID = "iccid";
    public static final String PARAM_ICCID_FORMATTED = "iccid_formatted";
    public static final String PARAM_STATE_ID = "gprs_device_state_id";
    public static final String PARAM_ALLOCATED_TO = "allocated_to";
    public static final String PARAM_BILLABLE_TO = "billable_to_name";
    public static final String PARAM_DEVICE_ID = "device_id";
    public static final String PARAM_RATE_PLAN_NAME = "rate_plan_name";
    public static final String PARAM_ORDERED_TS = "ordered_ts";    
    public static final String PARAM_ALLOCATED_TS = "allocated_ts";
    public static final String PARAM_ACTIVATED_TS = "activated_ts";
    public static final String PARAM_PROVIDER_ACTIVATION_TS = "provider_activation_ts";
    public static final String PARAM_ASSIGNED_TS = "provider_activation_ts";
    public static final String PARAM_STATUS = "status";
    public static Map gprswebUsageCodeHash = null;
    
    static{
    	gprswebUsageCodeHash = new HashMap();
    	gprswebUsageCodeHash.put(1, new Object[]{"Normal", "#99FF99"});
    	gprswebUsageCodeHash.put(2, new Object[]{"Overused", "#FF9999"});
    	
		
    }
    
    
    public static final String SQL_GPRSWEB_SEARCH_RESULTS_START = "SELECT gprs_device.gprs_device_id, "
              + "gprs_device.iccid, "
              + "case when length(to_char(gprs_device.iccid))>=17  "
              + "then ( "
              + "substr(to_char(gprs_device.iccid), 0, 4) || ' ' || " 
              + "substr(to_char(gprs_device.iccid), 5, 4) || ' ' || " 
              + "substr(to_char(gprs_device.iccid), 9, 4) || ' ' ||  "
              + "substr(to_char(gprs_device.iccid), 13, 4) || ' ' || "
              + "substr(to_char(gprs_device.iccid), 17)) "
              + "else '' end as iccid_formatted, "
              + "gprs_device.imsi, "
              + "gprs_device.gprs_device_state_id, "
              + "gprs_device.ordered_by, "
              + "gprs_device.allocated_to, "
              + "gprs_device.billable_to_name, "
              + "gprs_device.device_id, "
              + "gprs_device.rate_plan_name, "
              + "to_char(gprs_device.ordered_ts, 'MM/DD/YY') as ordered_ts, "
              + "to_char(gprs_device.allocated_ts, 'MM/DD/YY') as allocated_ts, "
              + "to_char(gprs_device.activated_ts, 'MM/DD/YY') as activated_ts, "
              + "to_char(gprs_device.provider_activation_ts, 'MM/DD/YY') as provider_activation_ts, "
              + "to_char(gprs_device.assigned_ts, 'MM/DD/YY') as assigned_ts, "
              + "device.device_serial_cd ";
    public static final String SQL_GET_FILE_LIST_BASE = " FROM device.file_transfer ft, ("
		   + " SELECT /*+ index(ftx idx_file_transfer_name) */ file_transfer_id "
        + " FROM device.file_transfer ftx ";
    
    public static final String SQL_GPRSWEB_SEARCH_RESULTS_BASE = " FROM device.gprs_device LEFT JOIN device.device ON gprs_device.device_id = device.device_id WHERE "
       + "case  when -1 = CAST (? as numeric) then 1 "
       + "when gprs_device_state_id = CAST (? as numeric) then 1 "
       + "else 0 "
       + "end = 1 "
       + "AND "
       + "case  when ' ' = COALESCE(?, ' ') then 1 "
       + "when billable_to_name = COALESCE(?,' ') then 1 "
       + "else 0 "
       + "end = 1 "
       + "AND  "
       + "case  when ' ' = COALESCE(?, ' ') then 1 "
       + "when allocated_to = COALESCE(?,' ') then 1 "
       + "else 0 "
       + "end = 1 "
       + "AND  "
       + "case  when ' ' = COALESCE(?, ' ') then 1 "
       + "when rate_plan_name = COALESCE(?,' ') then 1 "
       + "else 0 "
       + "end = 1 "
       + "AND status_cd = ? ";
           
    public static final String[] SORT_FIELDS_GET_FILE_LIST = {"iccid", // Location Name
        " (CASE gprs_device_state_id WHEN 1 THEN 'Registered' WHEN 2 THEN 'Allocated' WHEN 3 THEN 'Activation Pending' WHEN 4 THEN 'Activated' ELSE  'Assigned' END), "
    	+ "case gprs_device_state_id when 1 then gprs_device.ordered_ts "
    	+ "when 2 then gprs_device.assigned_ts "
    	+ "when 3 then gprs_device.allocated_ts  " 
    	+ "when 4 then gprs_device.provider_activation_ts "
        + "when 5 then gprs_device.assigned_ts "
        + "end ",
        "allocated_to", 
        "billable_to_name", 
        "rate_plan_name", 
        "device_id"
        };
    
    public static final String DEFAULT_SORT_INDEX_GPRSWEB_SEARCH_RESULTS = "1";
    
    public static final String DEFAULT_SORT_INDEX_GPRSWEB_SHOW_ALL_RESULTS = "1";
    
    /**
     * Format the iccid with spaces every 4 characters.
     * @param iccid
     * @return
     */
    public static String formatIccid(String iccid){
    	int l = 0;
    	int cntr = 0;
    	StringBuilder iccid_formatted = new StringBuilder("");
    	while(l<20){
    		String part = iccid.substring(l, l+4);
    		if(cntr!=0){
    			iccid_formatted.append(" ");
    		}
    		iccid_formatted.append(part);
    		l+=4;
    		cntr++;
    	}
    	return iccid_formatted.toString();
    }
}