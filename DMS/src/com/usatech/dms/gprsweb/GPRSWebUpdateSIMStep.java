package com.usatech.dms.gprsweb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.util.StringHelper;

public class GPRSWebUpdateSIMStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String action = form.getString("action", false);
    	String list = form.getString("item_list", false);
    	if (StringHelper.isBlank(action) || StringHelper.isBlank(list))
    		return;
		String items[] = list.split("\n", -1);
		boolean enable = "Enable".equalsIgnoreCase(action);
		boolean disable = "Disable".equalsIgnoreCase(action);
		int enabledCount = 0;
		int disabledCount = 0;
		StringBuilder err = new StringBuilder();
		String item;
		for(int i=0; i<items.length; i++){
			item = items[i].trim();
			try {				
		        if (StringHelper.isBlank(item))
		        	continue;
		        if (enable) {
		        	DataLayerMgr.executeUpdate("ENABLE_SIM", new Object[]{item}, true);
					enabledCount++;
		        } else if (disable) {
		        	DataLayerMgr.executeUpdate("DISABLE_SIM", new Object[]{item}, true);
					disabledCount++;
		        }
			} catch (Exception e) {
				err.append("Error processing SIM ").append(item).append(": ").append(e.getMessage()).append("<br /><br />");
				log.error(new StringBuilder("Error processing SIM ").append(item).toString(), e);
			}	
		}
		StringBuilder msg = new StringBuilder();
		if (enable)
			msg.append("SIMs enabled: ").append(enabledCount);
		else if (disable)
			msg.append("SIMs disabled: ").append(disabledCount);
		request.setAttribute("msg", msg.toString());
		if (err.length() > 0)
			request.setAttribute("error", err.toString());
    }    
}
