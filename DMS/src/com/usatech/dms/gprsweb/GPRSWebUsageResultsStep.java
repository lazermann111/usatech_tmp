/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.gprsweb;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class GPRSWebUsageResultsStep extends DMSPaginationStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	Map errorMap = new HashMap();
    	String errorMsg = null;
    	String action = form.getString("action", false);
    	int input_validated = 1;
    	if("Show All".equalsIgnoreCase(action)){
            String start_date = form.getString("start_date", false);
            String end_date = form.getString("end_date", false);
        
	    	try{
	    		//prepare the sql params needed for query
	    		Object[] params = {	start_date,  
	    							end_date};  
	    		
	    		//prepare and separate out the order by sql params needed for query
	    		Object[] defaultOrderByParams = {1,1,1,1,1,1,1,1,1,1,1,1,1,1};  
	    		
	    		/* Stored names for request params we need to carry over for pagination related requests */
	    		String storedNames = PaginationUtil.encodeStoredNames(new String[]{
						
						"start_date",
						"end_date"
				});
	    		request.setAttribute("storedNames", storedNames);
	    		
	    		String sortField = PaginationUtil.getSortField(null);
	    	    String sortIndex = form.getString(sortField, false);
	    	    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
	    	    request.setAttribute("sortIndex", sortIndex);
	    	    
	    		setPaginatedResultsOnRequestNamedSQL(form, request, "GET_GPRSWEB_USAGE_SHOW_ALL", "GET_GPRSWEB_USAGE_SHOW_ALL_COUNT",
	    				sortIndex, "gprsweb_usage_show_all_results", params, defaultOrderByParams);
	    		   		
	    		
	    	}catch(Exception e){
	    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
	    	}
        
    	}else if("Show Overage".equalsIgnoreCase(action)){
    		String month = form.getString("month", false);
    		int billing_date = form.getInt("billing_date", false, -1);
    		String start_date = "";
    		String end_date = "";
    		try{
    			if(StringHelper.isBlank(month)){
    				throw new Exception("Invalid date! Please enter a date in MM/YYYY format.");
    			}
	    		String[] monthData = month.split("/");
	    		String m = monthData[0];
	    		String y = monthData[1];
	    		
	    		if(StringHelper.isBlank(m) || m.length()!=2){
    				throw new Exception("Invalid date format! Please enter a date in MM/YYYY format.");
    			}
	    		
	    		if(StringHelper.isBlank(y) || y.length()!=4){
    				throw new Exception("Invalid date format! Please enter a date in MM/YYYY format.");
    			}
	    		
	    		Calendar start_cal = Calendar.getInstance();
	    		start_cal.set(new Integer(y).intValue(),new Integer(m).intValue(), billing_date);
	    		
	    		Calendar end_cal = Calendar.getInstance();
	    		end_cal.set(new Integer(y).intValue(),new Integer(m).intValue()+1, (billing_date-1));
	    		start_date = start_cal.get(Calendar.MONTH) + "/" + start_cal.get(Calendar.DAY_OF_MONTH) + "/" + start_cal.get(Calendar.YEAR);
	    		end_date = end_cal.get(Calendar.MONTH) + "/" + end_cal.get(Calendar.DAY_OF_MONTH) + "/" + end_cal.get(Calendar.YEAR);
	    		
    		}catch(Exception e){
    			form.setAttribute("errorMsg", e.getMessage());
    			return;
    		}
    		form.setAttribute("start_date", start_date);
    		form.setAttribute("end_date", end_date);        
	    	try{
	    		//prepare the sql params needed for query
	    		//Object[] params = {	billing_date, month, billing_date, month, billing_date, billing_date};  
	    		Object[] params = {	billing_date, month, billing_date, month, billing_date};
	    		
	    		//prepare and separate out the order by sql params needed for query
	    		Object[] defaultOrderByParams = {1,1,1,1,1,1,1,1,1,1,1,1};  
	    		
	    		/* Stored names for request params we need to carry over for pagination related requests */
	    		String storedNames = PaginationUtil.encodeStoredNames(new String[]{
						
	    				"month",
	    				"start_date",
						"end_date",
						"billing_date"
				});
	    		request.setAttribute("storedNames", storedNames);
	    		
	    		String sortField = PaginationUtil.getSortField(null);
	    	    String sortIndex = form.getString(sortField, false);
	    	    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
	    	    request.setAttribute("sortIndex", sortIndex);
	    	    
	    		setPaginatedResultsOnRequestNamedSQL(form, request, "GET_GPRSWEB_SHOW_USAGE_OVERAGE", "GET_GPRSWEB_USAGE_SHOW_OVERAGE_COUNT",
	    				sortIndex, "gprsweb_usage_show_overage_results", params, defaultOrderByParams);
	    		   		
	    		
	    	}catch(Exception e){
	    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
	    	}
    	}

    }
    
}
