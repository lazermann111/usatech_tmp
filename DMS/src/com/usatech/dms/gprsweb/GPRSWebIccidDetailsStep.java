/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.gprsweb;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConsumerAction;
import com.usatech.dms.model.Consumer;
import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class GPRSWebIccidDetailsStep extends DMSPaginationStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	Map errorMap = new HashMap();
    	String errorMsg = null;
    	String action = form.getString("action", false);
    	int input_validated = 1;
		String billing_date = "12";
		form.setAttribute("billing_date", billing_date);
		String iccid = (String)form.get("iccid");
		if(!(StringHelper.isBlank(iccid)) && iccid.length() != 20){
			errorMap.put("iccid", "Invalid or Missing ICCID!");
        	input_validated = 0;
		}
		
		if(input_validated == 0){
			form.setAttribute("errorMap", errorMap);
        	return;
		}else{
			
	    	try{
	    		Results gprsweb_device = (Results)DataLayerMgr.executeQuery("GET_GPRSWEB_DEVICE_BY_ICCID", new Object[] {iccid}, true);
				if(gprsweb_device == null){
					errorMap.put("iccid", "ICCID " + iccid + " not found!");
					form.setAttribute("errorMap", errorMap);
		        	return;
				}
				gprsweb_device.next();
				form.setAttribute("gprsweb_device", gprsweb_device);
				return;
	    	}catch(Exception e){
	    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
	    	}
        
    	}

    }
    
}
