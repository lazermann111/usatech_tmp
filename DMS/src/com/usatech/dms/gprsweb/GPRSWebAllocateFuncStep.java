/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.gprsweb;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConsumerAction;
import com.usatech.dms.model.Consumer;
import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class GPRSWebAllocateFuncStep extends DMSPaginationStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	Map errorMap = new HashMap();
    	String errorMsg = null;
    	String action = form.getString("action", false);
    	int input_validated = 1;
    	StringBuilder out = new StringBuilder("");
    	//if("Allocate".equalsIgnoreCase(action)){
    		//return;
    		
    		//String iccid_str = form.getString("iccid", false);
    		String[] iccid_str = request.getParameterValues("iccid");
    		String allocated_by = form.getString("allocated_by", false);
            String allocated_to = form.getString("allocated_to", false);
            String allocated_notes = form.getString("allocated_notes", false);
            String billable_to = form.getString("billable_to", false);
            String billable_notes = form.getString("billable_notes", false);
            
            if(iccid_str != null && iccid_str.length < 1){
            	errorMap.put("iccid_str", "No ICCIDs Checked!");
            	input_validated = 0;
            }
            
            if((!(StringHelper.isBlank(allocated_by))) && allocated_by.length() <= 0){
            	errorMap.put("allocated_by", "Allocated By is Required!!");
            	input_validated = 0;
            }
            
            if((!(StringHelper.isBlank(allocated_to))) && allocated_to.length() <= 0){
            	errorMap.put("allocated_to", "Allocated To is Required!");
            	input_validated = 0;
            }
            
            if((!(StringHelper.isBlank(billable_to))) && billable_to.length() <= 0){
            	errorMap.put("billable_to", "Billable To is Required!");
            	input_validated = 0;
            }
            
            if(input_validated==0){
            	request.setAttribute("errorMap", errorMap);
            	return;
            }else{
            	
            	int update_count = 0;
            	int failed_count = 0;
            	String msg = null;
            	out.append("<table  width=\"100%\" class=\"tabDataDisplayBorder\">\n");
            	out.append("<tr><td><pre>\n");
            	
            	for(int i=0; i<iccid_str.length;i++){
            		msg = "";
            		String iccid = iccid_str[i];
            		if(!(StringHelper.isBlank(iccid))){
            			iccid = iccid.trim();
            		}
            		
            		if((StringHelper.isBlank(iccid)) || iccid.length() != 20){
            			out.append("Error: ICCID too short!\n");
            			failed_count++;
            			continue;
            		}
            		
            		out.append("Updating <a href=\"iccidDetail.i?iccid="+iccid+"\">" + GPRSWEBConstants.formatIccid(iccid)+ "</a>... \t");
            		
            		try{Object[] result = null;
			    		result = DataLayerMgr.executeCall("UPDATE_GPRSWEB_ICCID_ALLOCATION", 
			    				new Object[] {2, 
			    				allocated_by,
			    				allocated_to,
			    				allocated_notes,
			    				billable_to,
			    				billable_notes,
			    				iccid}, true);
						if(result!=null && ((Integer)result[0]).intValue() > 0){
							out.append("Success: Update Succeeded!\n");
							update_count++;
						}else{
							out.append("Failed: Could't update iccid=" + iccid + "! returned rowCount = 0.\n");
							failed_count++;
						}
            		}catch(Exception e){
            			msg = "Could't execute statement! : "+ e.getMessage();
            			out.append("Failed: " + msg);
            			failed_count++;
            		}
            		out.append("\n");
            		
            	}
            	out.append("</pre></td></tr></table>\n");
            	
            	
            	if(failed_count > 0)
            	{
            		msg = "Processing failed!  For help please copy this screen and send it to an administrator.";
            	}
            	else
            	{
            		msg = "Allocation processed successfully!";
            	}
            	msg = msg+"\\n\\n SIMs Allocated: " + update_count +  "\\n Failures: " + failed_count;
            	out.append("<script language=\"javascript\">window.alert(\"" + msg + "\");</script>\n");
            	
            	form.setAttribute("outStr", out.toString());
            	return;
            }
            
        
    	}

    //}
    
}
