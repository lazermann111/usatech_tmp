/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.exception;

/**
 * Internal Exception for DSM.
 */
public class InternalException extends RuntimeException
{
    private static final long serialVersionUID = -2333160942005664911L;

    public InternalException()
    {
        super();
    }

    /**
     * @param message
     * @param cause
     */
    public InternalException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param message
     */
    public InternalException(String message)
    {
        super(message);
    }

    /**
     * @param cause
     */
    public InternalException(Throwable cause)
    {
        super(cause);
    }

}
