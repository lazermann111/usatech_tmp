/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.location;


public class LocationConstants
{        
    public static final String STORED_LOCATION = "location";
    public static final String STORED_NVP_PARENT_LOCATION_FORMATTED = "parent_location_formatted";
    public static final String STORED_NVP_COUNTRIES = "nvp_list_countries";
    public static final String STORED_NVP_TIME_ZONES = "nvp_list_time_zones";
    public static final String STORED_NVP_LOCATION_TYPES = "nvp_list_location_types";
	public static final String PARAM_LOCATION_NAME = "name";
	/* current_parent_only is passed in to the method in the edit location screen to get just the current parent location */
	public static final String PARAM_LOCATION_CURRENT_PARENT_ONLY = "current_parent_only";
	public static final String PARAM_ACTION_PARENT_LOCATION_LOOKUP = "parent_location_lookup";
	public static final String PARAM_ACTION_LOCATION_NAME_EXISTS = "location_name_exists";
	public static final String PARAM_PARENT_LOCATION_LOOKUP_CHARS = "parent_location_lookup_chars";
    public static final String PARAM_LOCATION_CITY = "city";
    public static final String PARAM_LOCATION_COUNTRY_NAME = "country";
    public static final String PARAM_LOCATION_COUNTRY_CD = "countryCode";
    public static final String PARAM_LOCATION_STATE_CD = "stateCode";
    public static final String PARAM_LOCATION_STATE_NAME = "state_name";
    public static final String PARAM_LOCATION_TYPE_ID = "location_type_id";
    public static final String PARAM_LOCATION_TYPE_DESC = "typeDesc";
    public static final String PARAM_LOCATION_TIME_ZONE_NAME = "timeZoneCode";
    public static final String PARAM_PARENT_LOCATION_ID = "parentId";
    public static final String PARAM_ESUDS_ALPHA_CHAR = "esuds_alpha_char";
    public static final String PARAM_ENABLED = "enabled";
    
    public static final String SQL_GET_LOCATION_LIST_START = "SELECT location_id,"
        + " location_name,"
        + " location_city,"
        + " country_name,"
        + " location_type_desc,"
        + " time_zone_name ";
    public static final String SQL_GET_LOCATION_LIST_BASE = "FROM ("
		   + "SELECT location_id,"
        + " location_name,"
        + " location_city,"
        + " country_name,"
        + " location_type.location_type_desc,"
        + " time_zone_name"
    + " from location.location"
        + " LEFT JOIN location.location_type ON location.location_type_id = location_type.location_type_id"
        + " LEFT JOIN location.country ON location.location_country_cd = country.country_cd"
        + " LEFT JOIN location.time_zone ON location.location_time_zone_cd = time_zone.time_zone_cd"
  + " WHERE 1 = 1";
    public static final String SQL_END = ") sq_end ";
           
    public static final String[] SORT_FIELDS_GET_LOCATION_LIST = {"LOWER(location_name)", // Location Name
        "LOWER(location_city)", // Location city
        "LOWER(country_name)", // Country Name
        "LOWER(location_type_desc)", // Location Type Desc
        "LOWER(time_zone_name)" // Location Time Zone Name
        };
    
    public static final String DEFAULT_SORT_INDEX_GET_LOCATION_LIST = "1";
    
    public static String DEFAULT_eSuds_DOMAIN_NAME = "esuds.net";
    
}