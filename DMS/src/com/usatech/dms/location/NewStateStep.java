/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.location;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.LocationActions;
import com.usatech.dms.util.DMSConstants;
import com.usatech.layers.common.util.StringHelper;

public class NewStateStep extends AbstractStep
{
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String state_cd = (form.getString("state_cd", false));
        String state_name = form.getString("state_name", false);
        String country_cd = form.getString("country_cd", false);
        
    	if(StringHelper.isBlank(country_cd)||country_cd.equalsIgnoreCase(com.usatech.dms.util.DMSConstants.LIST_VALUE_UNDEFINED)){
    		country_cd="US";
    		form.setAttribute("country_cd", "US");
    	}
        
        String actionType = form.getString(DMSConstants.PARAM_ACTION_TYPE, false);
     
        if((actionType != null && actionType.equals(DMSConstants.PARAM_ACTION_NEW)) || (actionType != null && actionType.equals("country_change")))
        	return;

        try
        {
        	if(actionType != null && actionType.equals(DMSConstants.PARAM_ACTION_SAVE)){
            	
            	
            	if((state_cd == null || state_cd.length() <= 0) 
            			|| (state_name == null || state_name.length() <= 0) 
            			|| (country_cd == null || country_cd.length() <= 0)){
            	}else{
            		//make call to save code and name
            		Object[] countParams = {state_cd, country_cd};
            		Integer stateCount = (Integer)LocationActions.getCountState(countParams);
            		if(stateCount != null && stateCount.intValue() == 0){
            			Object[] insertParams = {state_cd, country_cd, state_name, state_cd, country_cd, state_name};
            			DataLayerMgr.executeUpdate("INSERT_NEW_STATE", insertParams, true);
            			request.setAttribute("msg", "State added");
            		}else{
            			request.setAttribute("msg", "State already exists");
            		}
            	}
            }
            if(actionType != null && actionType.equals(DMSConstants.PARAM_ACTION_DELETE)){
            	
            	if((state_cd == null || state_cd.length() <= 0) 
            			|| (country_cd == null || country_cd.length() <= 0)){
            	}else{
            		//make call to save code and name
            		Object[] params = {state_cd, country_cd};
            		DataLayerMgr.executeUpdate("DELETE_STATE", params, true);
            	}
            }
        	

        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
