/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.location;

import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleAction;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.LocationActions;
import com.usatech.layers.common.model.Location;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.StringHelper;

public class EditLocationStep extends AbstractStep
{
    //private static final simple.io.Log log = simple.io.Log.getLog();

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String location_id = form.getString(DevicesConstants.PARAM_LOCATION_ID, false);
        String location_name = form.getString(LocationConstants.PARAM_LOCATION_NAME, false);
        String parent_location_lookup_chars = form.getString(LocationConstants.PARAM_PARENT_LOCATION_LOOKUP_CHARS, false);
        String parent_location_id = form.getString(LocationConstants.PARAM_PARENT_LOCATION_ID, false);
        if(StringHelper.isBlank(parent_location_id) || "Undefined".equalsIgnoreCase(parent_location_id)){
        	parent_location_id = null;
        	form.set(LocationConstants.PARAM_PARENT_LOCATION_ID, parent_location_id);
        }
        String state_cd = form.getString("stateCode", false);
        if(StringHelper.isBlank(state_cd) || "Undefined".equalsIgnoreCase(state_cd)){
        	state_cd = null;
        	form.set("stateCode", state_cd);
        }
        String actionType = form.getString(DMSConstants.PARAM_ACTION_TYPE, false);
        Location location = null;
        String message = null;
        boolean isNew = false;
        
        Object locationIdentifier = validateParams(request, location_id, location_name, parent_location_lookup_chars);

        try
        {
        	
        	 if(actionType == null || actionType.equals(DMSConstants.PARAM_ACTION_NEW)){
        		 isNew = true;
             	location = LocationActions.loadLocation(locationIdentifier);
             	if(location.getId() == null || (location.getId() < 0)){
             		//no-op, keep action as New
             	}else{
             		//here we need to switch out the Action to equal Edit, as the name came back with an existing Location
             		form.set(DMSConstants.PARAM_ACTION_TYPE, "Edit");
             		actionType = "Edit";
             		isNew = false;
             		//continue to process as if it were a normal Edit
             	}
             	
             }
        	
        	String redirectQuery = null;
        	if(actionType != null && actionType.equals(DMSConstants.PARAM_ACTION_SAVE_NEW)){
        		
        		Long alocationId = LocationActions.saveNewLocation(form);
        		locationIdentifier = validateParams(request, alocationId.toString(),
        				null, null);
        		if (locationIdentifier != null)
        			redirectQuery = new StringBuilder("location_id=").append(locationIdentifier).toString();
        	
        		
        	}else if(actionType != null && actionType.equals(DMSConstants.PARAM_ACTION_SAVE)){
        		
        		location = LocationActions.loadLocation(locationIdentifier);
        		if((location_name!=null && location!=null) && location_name.equals(location.getName())){
        			LocationActions.updateLocation(form);
        		}else{
        			Object[] sqlParams = {location_name, parent_location_id, location_id};
            		message = locationNameExists(sqlParams, "GET_COUNT_LOCATION_BY_NAME");
            		if(message != null){
            			String locationExistsMessage = "<B><FONT color=\"RED\" size=4>LOCATION => " + location_name + " <= ALREADY EXISTS</FONT></B>";
            			request.setAttribute("locationExistsMessage", locationExistsMessage);
            			form.set(LocationConstants.STORED_LOCATION, location);
            			
            			getParentLocationInfo(form, "GET_PARENT_LOCATION_FORMATTED_BY_LOCATION_ID", location, new Object[]{((location.getParentId()==null)?"-1":location.getParentId().toString())});
            			request.setAttribute("location", location);
            			return;
            		}else{
            			LocationActions.updateLocation(form);
            		}
        		}
        		
        	}else if(actionType != null && actionType.equals(DMSConstants.PARAM_ACTION_UNDELETE)){
        		LocationActions.undeleteLocation(form);
        		
        	}else if(actionType != null && actionType.equals(DMSConstants.PARAM_ACTION_DELETE)){
        		LocationActions.deleteLocation(form);
        		
        	}
        	
        	if (RequestUtils.handleActionRedirect(form, request, response, redirectQuery) == SimpleAction.INVOKE_RESULT_REDIRECT)
        		return;

        	location = LocationActions.loadLocation(locationIdentifier);
        	form.set(LocationConstants.STORED_LOCATION, location);
            if(isNew){
            	form.set(DMSConstants.PARAM_ACTION_TYPE, DMSConstants.PARAM_ACTION_NEW);
            	return;
            }
        	if(actionType != null && actionType.equals(DMSConstants.PARAM_ACTION_EDIT) && (parent_location_lookup_chars != null && parent_location_lookup_chars.trim().length() > 0)){
        		if(parent_location_lookup_chars != null && parent_location_lookup_chars.length() > 0){
	        		getParentLocationList(form, location_id,
							parent_location_lookup_chars, parent_location_id,
							location);
        		}
            }
            // if the total device count is not retrieved yet, get it now
	        Integer deviceCount = (Integer)LocationActions.getDeviceCountByLocation(location.getId());
	        request.setAttribute("deviceCount", deviceCount);
            
	        // 2010-03-01 EPC - Remove first two params, not needed in sql
        	Object[] sqlParamsCurrentParentOnly = {//LocationConstants.PARAM_LOCATION_CURRENT_PARENT_ONLY,
    				//LocationConstants.PARAM_LOCATION_CURRENT_PARENT_ONLY,
    				location.getParentId(),
    				LocationConstants.PARAM_LOCATION_CURRENT_PARENT_ONLY,
    				LocationConstants.PARAM_LOCATION_CURRENT_PARENT_ONLY,
    				LocationConstants.PARAM_LOCATION_CURRENT_PARENT_ONLY,
    				location.getParentId(),
    				location.getId()
    				};
        	getParentLocationInfo(form, "GET_PARENT_LOCATION_FORMATTED", location, sqlParamsCurrentParentOnly);
        	
        	location = LocationActions.loadLocation(locationIdentifier);
        	request.setAttribute("location", location);

        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

	private void getParentLocationList(InputForm form, String location_id,
			String parent_location_lookup_chars, String parent_location_id,
			Location location) throws SQLException, DataLayerException {
		
			Long parentLocationIdTemp = new Long(0);
			Long locationIdTemp = new Long(0);
			
			if ((location_id != null && location_id.length() > 0) 
					&&(parent_location_id != null && parent_location_id.length() > 0)){
				parentLocationIdTemp = new Long(location.getParentId());
				locationIdTemp = new Long(location.getId());
			}
			parent_location_lookup_chars = parent_location_lookup_chars+"%";
			
			Object[] sqlParamsParentLookupChars = {parent_location_lookup_chars,
					parent_location_lookup_chars,
					parentLocationIdTemp,
					parent_location_lookup_chars,
					parent_location_lookup_chars,
					parent_location_lookup_chars,
					parentLocationIdTemp,
					locationIdTemp
					};
			getParentLocationInfo(form, "GET_PARENT_LOCATION_FORMATTED", location, sqlParamsParentLookupChars);
			return;
			
		
	}

	private String locationNameExists(Object[] sqlParams, String sqlQuery) throws SQLException, DataLayerException,
			ConvertException {
		
		return LocationActions.getLocationNameCount(sqlParams, sqlQuery);
	}

	private Object validateParams(HttpServletRequest request,
			String location_id, String location_name,
			String parent_location_lookup_chars) {
		Object locationIdentifier = null;
        
        if ((location_id == null || location_id.length() == 0) && (location_name == null || location_name.length() == 0) && (parent_location_lookup_chars == null || parent_location_lookup_chars.length() == 0))
        {
            request.setAttribute("missingParam", true);
            return null;
        }else{
        	if(location_id == null || location_id.length() == 0 || location_id.equals("null")){
        		locationIdentifier = (String)location_name;
        	}else{
        		locationIdentifier = (Long)new Long(location_id);
        	}
        	
        }
		return locationIdentifier;
	}
    
    /**
     * Get the parent location info
     * @param form
     * @param location
     * @throws SQLException
     * @throws DataLayerException
     */
	private void getParentLocationInfo(InputForm form, String sqlQuery, Location location, Object[] sqlParams)
			throws SQLException, DataLayerException {
		
		Helper.getSelectList(form, LocationConstants.STORED_NVP_PARENT_LOCATION_FORMATTED, sqlQuery, true, sqlParams, null, null, null);
	}

}
