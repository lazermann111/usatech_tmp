/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.location;

import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.util.StringHelper;

public class LocationTreeStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();	
	
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String errorMsg = "";
    	int source_id = form.getInt("source_id", false, -1);
    	if(source_id == -1){
			form.setAttribute("missingParam", true);
			errorMsg = "Required parameter not found: source_id";
			form.setAttribute("errorMsg", errorMsg);
            return;
		}
    	boolean hasParentFlag = true;
    	int root_id = source_id;
    	StringBuilder sb = new StringBuilder("");
    	while(hasParentFlag){
	    	try{
		    	Results locationInfo = DataLayerMgr.executeQuery("GET_PARENT_LOCATION_BY_LOCATION_ID", new Object[] {root_id}, false);
		    	if(locationInfo.next()){
		    		
		    		try{
		    			
		    			BigDecimal val = (BigDecimal)locationInfo.get("parent_location_id");
		    			if(val==null){
		    				hasParentFlag=false;
		    				break;
		    			}
		    			root_id = val.intValue();
		    			
		    		}
		    		catch(Exception e){
		    			//if we fail, this is the parent
		    			hasParentFlag=false;
		    			log.error(simple.text.StringUtils.exceptionToString(e));
		    		}
		    	}else{
		    		hasParentFlag=false;
		    	}
		    	
	    	}catch(Exception e){
	    		throw new ServletException(e.getMessage());
	    	}
    	}
    	printLocation(source_id, root_id, root_id, sb);
    	form.setAttribute("locationTreeOut", sb.toString());
        return;        
        
    }
    
    /**
     * Build the string for the location hierarchy tree for esuds locations.
     * @param location_id
     * @param sb
     * @throws ServletException
     */
    public static void printLocation(int source_id, int root_id, int location_id, StringBuilder sb)throws ServletException{
    	
    	
    	try{ 
    		//get location info
    		Results locationInfo = DataLayerMgr.executeQuery("GET_LOCATION_INFO", new Object[] {location_id}, false);
    		
    		String location_name = "";
    		String location_type = null;
    		int loc_id = -1;
    		int num_cons_cards = -1;
    		if(locationInfo.next()){
    			location_type=(String)locationInfo.get("location_type_desc");
    			location_name=(String)locationInfo.get("location_name");
    			loc_id=((BigDecimal)locationInfo.get("location_id")).intValue();
    			num_cons_cards = ((BigDecimal)locationInfo.get("count_consumer_acct_id")).intValue();
    			if(!StringHelper.isBlank(location_type)){
    				location_type=location_type.replace("Education - Secondary", "");
    			}
    		}
    		//get devices
    		Results devices = DataLayerMgr.executeQuery("GET_DEVICES_BY_LOCATION_ID", new Object[] {location_id}, false);
    		//get device count
    		int device_count = -1;
    		Results deviceCount = DataLayerMgr.executeQuery("GET_COUNT_DEVICES_BY_LOCATION_ID", new Object[] {location_id}, false);
    		if(deviceCount.next()){device_count=((BigDecimal)deviceCount.get("count")).intValue();}
    		//get esuds_device_count
    		int esuds_device_count = -1;
    		Results esudsDeviceCount = DataLayerMgr.executeQuery("GET_COUNT_ESUDS_DEVICES_BY_LOCATION_ID", new Object[] {location_id}, false);
    		if(esudsDeviceCount.next()){esuds_device_count=((BigDecimal)esudsDeviceCount.get("count")).intValue();}
    		//get children
    		Results children = DataLayerMgr.executeQuery("GET_LOCATION_CHILDREN", new Object[] {location_id}, false);
    		//get child count
    		Results childrenCount = DataLayerMgr.executeQuery("GET_COUNT_LOCATION_CHILDREN", new Object[] {location_id}, false);
    		int child_count = -1;
    		if(childrenCount.next()){child_count=((BigDecimal)childrenCount.get("count")).intValue();}
    		//start the table
    		
    		String bgColor = "#FFFFFF";
    		
    		if((source_id != root_id) && (location_id == source_id))
    			bgColor = "#FFFFCC";
    		
    		sb.append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" bgcolor=\"white\" nowrap>"
    					+"<tr>"
    					 +"<td align=\"left\" valign=\"center\" bgcolor=\"" + bgColor + "\" style=\"border-top: 1px solid black\">"
    					  +"<img src=\"/images/pixel.gif\" width=\"219\" height=\"1\" border=\"0\"><br/>"
    					  +"<span class=\"location_type\">" + location_type + " ");
    		
    		if(child_count > 1){sb.append("(" + child_count + " children)");}
    		
    		sb.append("</span><br/>"
    					+ "<a href=\"editLocation.i?location_id=" + loc_id + "\" class=\"location_name\">" + location_name + "</a>&nbsp;&nbsp;&nbsp;&nbsp;\n");
    		
    		if(device_count > 0){
    			if(esuds_device_count > 1){
    				
    				sb.append("<span class=\"warn\">(" + esuds_device_count + " Room Controllers!)</span>\n");
        		
    			}else if(device_count > 1){
    				
    				sb.append("<span class=\"location_type\">" + location_type + " (" + device_count + " devices)</span>\n");
    				
    			}
    			
    			if(num_cons_cards > 0){
    				sb.append("<br/><a href=\"consumerSearch.i?location_id=" + location_id + "&action=Search\" class=\"cards\">" + num_cons_cards + " cards at this location!</a>\n");
    			}
    			
    			while(devices.next()){
    				String device_type_desc = (String)devices.get("device_type_desc");
    				String device_serial_cd = (String)devices.get("device_serial_cd");
    				int device_type_id = ((BigDecimal)devices.get("device_type_id")).intValue();
    				long device_id = ((BigDecimal)devices.get("device_id")).longValue();
    				int customer_id = ((BigDecimal)devices.get("customer_id")).intValue();
    				int date_diff = ((BigDecimal)devices.get("date_diff")==null? (new BigDecimal(0)) : (BigDecimal)devices.get("date_diff")).intValue();
    				String last_activity_ts = (String)devices.get("last_activity_ts");
    				String customer_name = (String)devices.get("customer_name");
    				sb.append("<table width=\"100%\" cellspacing=\"0\" align=\"left\" cellpadding=\"0\" border=\"0\" nowrap >\n");
    				sb.append("<tr>\n");
    				sb.append("<td  class=\"device\">&#149;&nbsp;</td>\n");
    				sb.append("<td  class=\"device\" align=\"left\" valign=\"top\" nowrap>" + device_type_desc + ":");
    				sb.append("<a href=\"profile.i?device_id=" + device_id + "\" class=\"ssn\">" + device_serial_cd + "</a>");
    				sb.append("</td>");
    				sb.append("</tr>");
    				
    				
    				
    				/*
    				 * HOST, CUSTOMER, LAST ACTIVITY, PAYMENT TYPES section
    				 * 
    				 * Wrap any information in this next secion inside a new <tr><td  class=\"machine\" align=\"left\" valign=\"top\" nowrap>
    				 * 
    				 * For the next section of the location tree, we are going to build the html that renders some more detail as follows;
    				 * FOR ESUDS - 1) display any host machines, 2) display Last Acivity Info, 3) Display Payment Type info
    				 * FOR NON-ESUDS - 1) display Customer info, 2) display Last Acivity Info
    				 * 
    				 * Finish any info in this section with an end </td></tr>
    				 * 
    				 */
    				sb.append("<tr>");
					sb.append("<td  >&nbsp;</td>");
					sb.append("<td  class=\"machine\" align=\"left\" valign=\"top\" >");
    				StringBuilder room_string = new StringBuilder("");
    				if(device_type_id == 5){
    					
    					int host_count = -1;
    					Results hostCount = DataLayerMgr.executeQuery("GET_COUNT_HOST_INFO_BY_DEVICE_ID", new Object[] {device_id}, false);
    					if(hostCount.next()){host_count=((BigDecimal)hostCount.get("count")).intValue();}
    					if(host_count > 0){
    						
	    					Results hostTotals = DataLayerMgr.executeQuery("GET_HOST_INFO_BY_DEVICE_ID", new Object[] {device_id}, false);
	    					while(hostTotals.next()){
	    						String host_type_desc = (String)hostTotals.get("host_type_desc");
	    						int host_totals = ((BigDecimal)hostTotals.get("host_totals")).intValue();
	    						room_string.append(host_totals + " " + host_type_desc + "s, \n" );
	    					}
	    					room_string.append("<br/>\n" );
    					}
	    					
    					
    				}else{
    					room_string.append("Customer: <a href=\"editCustomer.i?customer_id=" + customer_id + "\" class=\"machine\">" + customer_name + "</a><br/>\n");
    				}
    				room_string.append("Last Activity: " + (date_diff > 75 ? ("<font color=\"red\">" + last_activity_ts + "</font>") : last_activity_ts) + "<br/>");
    				
    				if(device_type_id == 5){
    					Results paymentTypesCount = DataLayerMgr.executeQuery("GET_COUNT_PAYMENT_TYPES_BY_DEVICE_ID", new Object[] {device_id}, false);
    					int payment_types_count = -1;
    					if(paymentTypesCount.next()){payment_types_count=((BigDecimal)paymentTypesCount.get("count")).intValue();}
    					if(payment_types_count > 0){
	    					room_string.append("Payment Types: ");
	    					Results paymentTypes = DataLayerMgr.executeQuery("GET_PAYMENT_TYPES_BY_DEVICE_ID", new Object[] {device_id}, false);
	    					while(paymentTypes.next()){
	    						String payment_subtype_name = (String)paymentTypes.get("payment_subtype_name");
	    						int pos_pta_id = ((BigDecimal)paymentTypes.get("pos_pta_id")).intValue();
	    						room_string.append("<a href=\"editPosPtaSettings.i?pos_pta_id=" + pos_pta_id + "&device_id=" + device_id + "&action=Edit%20Settings\" class=\"machine\">" + payment_subtype_name + "</a> ");
	    					}
	    					room_string.append("<div class=\"spacer5\"></div>\n" );
    					}
    				}
    				
    				sb.append(room_string.toString());
    				sb.append("</td>");
    				sb.append("<td>");
    				sb.append("</td>");
					sb.append("</tr>");
    				sb.append(" </table>\n");
    				
    			}
  
    		}else if(child_count <= 0){
    			
    			if(num_cons_cards > 0){
    				
    				sb.append("<br/><a href=\"consumerSearch.i?location_id=" + location_id + "&action=Search\" class=\"cards\">" + num_cons_cards + " cards at this location!</a>");
    				
    			}
    			
    			sb.append("<br/>&nbsp;<span class=\"warn\">Empty Location!</span>");
    			
    		}else{
    			
    			if(num_cons_cards > 0){
    				sb.append("<br/><a href=\"consumerSearch.i?location_id=" + location_id + "&action=Search\" class=\"cards\">" + num_cons_cards + " cards at this location!</a>");
    			}
    			
    		}
    		sb.append(" </td>\n");
    		
    		
    		if(child_count > 0){
    			sb.append(" <td>\n");
	    		while(children.next()){
	    			int child_location_id = ((BigDecimal)children.get("location_id")).intValue();
	    			printLocation(source_id, root_id, child_location_id, sb);
	    			
	    		}
	    		sb.append(" </td>\n");
    		}
    		
    		
    		sb.append(" </tr>\n");
    		sb.append("</table>\n");
    		
    	}catch(Exception e){
    		throw new ServletException(e.getMessage());
    	}	
    }
    
}
