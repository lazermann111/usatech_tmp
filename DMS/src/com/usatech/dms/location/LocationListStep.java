/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.location;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.util.LinkedHashMap;

import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.dms.util.Helper;

public class LocationListStep extends DMSPaginationStep
{
    

    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String location_name = form.getString(LocationConstants.PARAM_LOCATION_NAME, false);
        String location_city = form.getString(LocationConstants.PARAM_LOCATION_CITY, false);
        String location_type_id = form.getString(LocationConstants.PARAM_LOCATION_TYPE_ID, false);
        String parent_location_id = form.getString(LocationConstants.PARAM_PARENT_LOCATION_ID, false);

        StringBuilder sql = new StringBuilder();
        Map params = new LinkedHashMap();
        int paramCntr = 0;
        if (location_name != null && location_name.length() > 0)
        {
        	sql.append("AND lower(location.location_name) like lower(?) ");
            params.put(paramCntr++, Helper.convertParam(location_name, false));
        }
        if (location_city != null && location_city.length() > 0)
        {
        	sql.append("AND lower(location.location_city) like lower(?) ");
            params.put(paramCntr++, Helper.convertParam(location_city, false));
        }
        if (location_type_id != null && location_type_id.length() > 0)
        {
        	sql.append("AND location.location_type_id = CAST (? AS numeric) ");
            params.put(paramCntr++, location_type_id);
        }
        if (parent_location_id != null && parent_location_id.length() > 0)
        {
        	sql.append("AND location.parent_location_id = CAST (? AS numeric)");
            params.put(paramCntr++, parent_location_id);
        }
        
        String queryBase = LocationConstants.SQL_GET_LOCATION_LIST_BASE + sql.toString();
        
        setPaginatedResultsOnRequest(form, request, LocationConstants.SQL_GET_LOCATION_LIST_START, queryBase, LocationConstants.SQL_END, LocationConstants.DEFAULT_SORT_INDEX_GET_LOCATION_LIST,
        		LocationConstants.SORT_FIELDS_GET_LOCATION_LIST, "locationList", params != null & params.size() > 0 ? params.values().toArray() : null,
        				"location_id", "/editLocation.i?location_id=");
    }
}
