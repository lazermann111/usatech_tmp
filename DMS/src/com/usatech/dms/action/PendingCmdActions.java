/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import com.usatech.layers.common.model.Device;
import com.usatech.dms.model.PendingCommand;
import com.usatech.dms.util.ValuesListLoader;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.MessageType;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteArrayUtils;
import simple.results.Results;

/**
 * Provides the methods to manage the "Pending Command".
 */
public final class PendingCmdActions
{
    private static final simple.io.Log log = simple.io.Log.getLog();

    public static final String DA_CMD_P_PENDING = "P";
    public static final String DA_CMD_C_CANCEL = "C";

    // some other commonly used Peek/Pokes
    private static Map<String/* start_addr size */, String/* name */> peekCmdMap;
    // esuds cycle codes
    //private static Map<String/* esuds cycle */, String/* desc */> esudsCycleCodeMap;
    // enps remote cmd request numbers
    private static Map<String/* enps remote cmd request numbers */, String/* desc */> requestNumberMap;
    
    public static Map<String/* fileTransferCmdList commands */, String/* name */> fileTransferCmdList;

    /* first thing, to load up the command maps */
    static
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put("320 36", "All Counters");
        map.put("0 136", "Semi-Complete, Part 1");
        map.put("142 142", "Semi-Complete, Part 2");
        map.put("356 156", "Semi-Complete, Part 3");
        map.put("0 200", "Complete, Part 1");
        map.put("200 200", "Complete, Part 2");
        map.put("400 112", "Complete, Part 3");
        map.put("65088 15", "Firmware Version");
        map.put("16799744 400", "Modem Info");
        map.put("16842016 33", "Boot Loader/App Rev");
        map.put("16842084 76", "Attached Bezel Info");
        map.put("0 57", "Full Config");
        map.put("0 255", "Full Config");
        peekCmdMap = Collections.unmodifiableMap(map);

        map = new HashMap<String, String>();
        map.put("0", "No Status Available");
        map.put("1", "Idle, Available");
        map.put("2", "In 1st Cycle");
        map.put("3", "Out Of Service");
        map.put("4", "Nothing on Port");
        map.put("5", "Idle, Not Available");
        map.put("6", "Manual Service Mode");
        map.put("7", "In 2nd Cycle");
        map.put("8", "Transaction In Progress");
        //esudsCycleCodeMap = Collections.unmodifiableMap(map);

        map = new HashMap<String, String>();
        map.put("00", "Software Reboot");
        map.put("01", "Hardware Reboot");
        map.put("02", "Dump Transactions");
        map.put("03", "Signal Strength");
        map.put("04", "GX Counters");
        map.put("05", "Force Initialization");
        map.put("06", "Compressed DEX Response");
        map.put("07", "Compressed DEX Response Complete");
        map.put("08", "DEX Column Mapping");
        map.put("09", "DEX Price Mapping");
        map.put("0A", "DEX Machine Information");
        map.put("0B", "Client Version");
        map.put("0C", "eSuds Room Layout Request");
        map.put("0D", "Get Logs");
        map.put("0E", "Enable Logins");
        map.put("0F", "Synchronize Time");
        map.put("10", "Clear Card Cache");
        requestNumberMap = Collections.unmodifiableMap(map);
        
        map = new HashMap<String, String>();//"'7C', 'A4', 'C7', 'C8'"
        map.put("7C", "(7Ch) File Transfer Start");
        map.put("A4", "(A4h) File Transfer Start V1.1");
        map.put("C7", "(C7h) Short File Transfer V4.1");
        map.put("C8", "(C8h) File Transfer Start V4.1");
        fileTransferCmdList = Collections.unmodifiableMap(map);
    }

    /** Don't let anyone instantiate this class. */
    private PendingCmdActions()
    {}

    /**
     * Constructs the map for the given csv data.
     * 
     * @param csvdata
     * @return
     * @throws IOException
     */
    public static Map<String, String> constructConfigParamMap(int deviceTypeId) throws ServletException
    {
    	
        Map<String/* command */, String/* desc */> mapFieldNames = new ConcurrentHashMap<String, String>();
    	
    	mapFieldNames = new ConcurrentHashMap<String, String>();
        {	try{
	            Results csvline = DataLayerMgr.executeQuery("GET_MAP_FIELD_NAMES", new Object[]{deviceTypeId}, false);
	            while (csvline.next())
	            {
	            	mapFieldNames.put(new StringBuilder(csvline.getFormattedValue("field_offset")).append(" ").append(csvline.getFormattedValue("field_size")).toString(), csvline.getFormattedValue("field_name"));
	            }
        	}catch(Exception e){
    			throw new ServletException("Unable to get map field names for pending commands", e);
        	}
        }
        // now load up some other commonly used Peek/Pokes that may will not be in the list
        mapFieldNames.putAll(peekCmdMap);
    	
        return mapFieldNames;
    }

    /**
     * Retrieve the pending command data for the given device, the data includes: <ul>
     * <li>machine_command_pending_id</li> <li>data_type</li> <li>command</li> <li>execute_cd</li>
     * <li>execute_date</li> <li>execute_order</li> <li>created_ts</li> <li>file_transfer_id</li>
     * <li>file_transfer_name</li> <li>file_transfer_type_cd</li> </ul>
     * 
     * @param device
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static List<PendingCommand> loadPendingData(ServletContext servletContext, Device device) throws ServletException, SQLException, DataLayerException
    {
        String[] params = {device.getDeviceName(), device.getDeviceName()};
        Results result = DataLayerMgr.executeQuery("GET_PENDING_DATA", params, false);
        List<PendingCommand> list = new ArrayList<PendingCommand>();
        Map<String, String> configParamMap = ValuesListLoader.getConfigParamMap(servletContext, device.getTypeId());
        while (result.next())
        {
            list.add(generatePendingCommand(servletContext, result, device, configParamMap));
        }
        return list;
    }

    /**
     * Retrieve the pending command history data for the given device and specified count, the data
     * includes: <ul> <li>machine_command_pending_id</li> <li>data_type</li> <li>command</li>
     * <li>execute_cd</li> <li>execute_date</li> <li>execute_order</li> <li>created_ts</li>
     * <li>file_transfer_id</li> <li>file_transfer_name</li> <li>file_transfer_type_cd</li> </ul>
     * @param device
     * @param cmdInt
     * @return
     * @throws SQLException
     * @throws DataLayerException
     */
    public static List<PendingCommand> loadCommandHistory(ServletContext servletContext, Device device, int cmdInt) throws ServletException, SQLException, DataLayerException
    {
        Object[] params = {device.getDeviceName(), cmdInt};
        Results result = DataLayerMgr.executeQuery("GET_COMMAND_HISTORY", params, false);
        List<PendingCommand> list = new ArrayList<PendingCommand>();
        Map<String, String> configParamMap = ValuesListLoader.getConfigParamMap(servletContext, device.getTypeId());
        while (result.next())
        {
            list.add(generatePendingCommand(servletContext, result, device, configParamMap));
        }
        return list;
    }

    private static PendingCommand generatePendingCommand(ServletContext servletContext, Results result, Device device, Map<String, String> configParamMap) throws ServletException
    {
        PendingCommand pendingCmd = new PendingCommand();
        pendingCmd.setId(result.getFormattedValue("machine_command_pending_id"));
        pendingCmd.setType(result.getFormattedValue("data_type"));
        pendingCmd.setTypeDesc(result.getFormattedValue("message_type_name"));
        pendingCmd.setCommand(result.getFormattedValue("command"));
        pendingCmd.setExecuteCd(result.getFormattedValue("execute_cd"));
        pendingCmd.setExecuteDate(result.getFormattedValue("execute_date"));
        pendingCmd.setExecuteOrder(result.getFormattedValue("execute_order"));
        pendingCmd.setCreatedTs(result.getFormattedValue("created_ts"));
        pendingCmd.setFileTransId(result.getFormattedValue("file_transfer_id"));
        pendingCmd.setFileTransName(result.getFormattedValue("file_transfer_name"));
        pendingCmd.setFileTransTypeCd(result.getFormattedValue("file_transfer_type_cd"));
        pendingCmd.setDeviceFileTransferId(result.getFormattedValue("device_file_transfer_id"));
        pendingCmd.setActionDesc(result.getFormattedValue("action_desc"));
		pendingCmd.setFirmwareUpgradeId(result.getFormattedValue("firmware_upgrade_id"));
		pendingCmd.setFirmwareUpgradeName(result.getFormattedValue("firmware_upgrade_name"));
		pendingCmd.setFirmwareUpgradeDesc(result.getFormattedValue("firmware_upgrade_desc"));
		pendingCmd.setStatusDesc(result.getFormattedValue("execute_name"));
		pendingCmd.setGlobalSessionCd(result.getFormattedValue("global_session_cd"));
		pendingCmd.setSessionId(result.getFormattedValue("session_id"));		
		pendingCmd.setDeviceFirmwareUpgradeStatusName(result.getFormattedValue("device_fw_upg_status_name"));
		pendingCmd.setFirmwareUpgradeFileId(result.getFormattedValue("fw_upg_file_transfer_id"));
		pendingCmd.setFirmwareUpgradeFileName(result.getFormattedValue("fw_upg_file_transfer_name"));
		pendingCmd.setErrorMessage(result.getFormattedValue("error_message"));
		pendingCmd.setAttemptCount(result.getFormattedValue("attempt_count"));
        pendingCmd.setFirmwareUpdate(result.getValue("fwu_yn_flag") != null);
        
        int deviceTypeId = device.getTypeId();
        String command = pendingCmd.getCommand();
        String dataType = pendingCmd.getType();
        
        try {
	        if (deviceTypeId == DeviceType.EDGE.getValue() && command != null) {
	        	byte[] commandBytes = ByteArrayUtils.fromHex(command);
	        	String commandString = new String(commandBytes);
	        	StringBuilder actionDesc = new StringBuilder(pendingCmd.getActionDesc());
	        	if (actionDesc.length() > 0)
    				actionDesc.append(", ");
	        	if (MessageType.GENERIC_REQUEST_4_1.getHex().equalsIgnoreCase(dataType)) {
	        		if (commandBytes.length > 4 && commandBytes[0] == 0x01) {
	        			pendingCmd.setActionDesc(actionDesc.append("File Transfer Request, File Name: ").append(commandString.substring(4))
	        				.append(", File Type: ").append(FileType.getByValue(ByteArrayUtils.readUnsignedShort(commandBytes, 1))).toString());
	        		}
	        	} else if (MessageType.GENERIC_RESPONSE_4_1.getHex().equalsIgnoreCase(dataType)) {
	        		if (commandBytes.length > 2 && commandBytes[0] == 0x00 && commandBytes[1] == 0x00) {
	        			switch (commandBytes[2]) {
	        				case 5:
	        				case 10:
	        					if (commandBytes.length > 4)
	        						pendingCmd.setActionDesc(actionDesc.append("Reconnect Time: ").append(ByteArrayUtils.readUnsignedShort(commandBytes, 3)).append(" sec").toString());
	        					break;
	        				case 8:
	        				case 14:
	        					if (commandBytes.length > 5) {
	        						String indexList = commandString.substring(5);
	        						if (indexList.length() > 50 && !indexList.contains(" "))
	        							pendingCmd.setActionDesc(actionDesc.append("<input type=\"text\" class=\"cssText\" value=\"")
	        								.append(indexList).append("\" style=\"width:98%\" readonly=\"readonly\" /><div class=\"spacer3\"></div>").toString());
	        						else
	        							pendingCmd.setActionDesc(actionDesc.append(indexList).toString());
	        					}
	        					break;
	        			}
	        		}	        		
	        	}
	        }
        } catch (Exception e) {
        	log.error("Error parsing Edge pending command", e);
        }
        	
        pendingCmd.setCommandDesc(getPendingCommandName(deviceTypeId, pendingCmd.getType(), command, configParamMap));
        return pendingCmd;
    }

    /**
     * get the command name for the given pendingCommand.
     * 
     * @param deviceTypeId
     * @param data
     * @param configParamMap
     * @return
     */
    private static String getPendingCommandName(int deviceTypeId, String commandType, String command, Map<String, String> configParamMap)
    {
        String result;
        if (DeviceType.ESUDS.getValue() == deviceTypeId || DeviceType.KIOSK.getValue() == deviceTypeId || DeviceType.EDGE.getValue() == deviceTypeId || DeviceType.T2.getValue() == deviceTypeId)
        {
            if ("83".equals(commandType))
            {
                result = requestNumberMap.get(command);
                if (result == null)
                	result = command;
            }
            else if ("9B".equals(commandType))
            {
                result = StringHelper.decodeHexString(command);
            }
            else
            {
                result = command;
            }
        }
        else
        {
            result = getEPortPendingCmdName(commandType, command, configParamMap);
        }
        return result;
    }

    private static String getEPortPendingCmdName(String type, String command, Map<String, String> configParamMap)
    {
        if ("87".equals(type) || "88".equals(type))
        {
        	int pokeLoc = 0;
            int pokeLen = 0;
            
            try {
	            String addr = command.substring(2, 10);
	            pokeLoc = Integer.parseInt(addr, 16) * 2;
	            
	            String len = command.substring(10, 18);		           
	            pokeLen = Integer.parseInt(len, 16);
            } catch (Exception e) { }

            if (command.startsWith("42"))
            {
            	StringBuilder commandName = new StringBuilder();
                String paramName = configParamMap.get(new StringBuilder().append(pokeLoc).append(" ").append(pokeLen).toString());
                if (paramName != null)
                	commandName.append(paramName);
                	                	
            	if (pokeLen == 2) {
                    paramName = configParamMap.get(new StringBuilder().append(pokeLoc + 1).append(" ").append(pokeLen).toString());
                    if (paramName != null) {
                        if (commandName.length() > 0)
                        	commandName.append(" / ");
                        commandName.append(paramName);
                    }
            	}
                
            	if (commandName.length() > 0)
            		return commandName.toString();
            	else
            		return new StringBuilder().append(pokeLoc).append(" - ").append(pokeLoc + pokeLen).toString();
            }
            else if (command.startsWith("44"))
            {
            	StringBuilder commandName = new StringBuilder("RAM: ");
                String paramName = configParamMap.get(new StringBuilder().append(pokeLoc).append(" ").append(pokeLen).toString());
                if (paramName == null)
                    return commandName.append(pokeLoc).append(" - ").append(pokeLoc + pokeLen).toString();
                else
                    return commandName.append(paramName).toString();
            }
            else
            {
                return new StringBuilder("Unknown: ").append(pokeLoc).append(" - ").append(pokeLoc + pokeLen).toString();
            }
        } else if ("9B".equals(type))
        {
            return StringHelper.decodeHexString(command);
        } else
        	return command;
    }

    /**
     * Create a command request for the device by the given evNumber.
     * 
     * @param evNumber
     * @param dataType
     * @param command
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static void createRequest(Connection conn, String evNumber, String dataType, String command) throws SQLException, DataLayerException
    {
        createPendingCommand(conn, evNumber, dataType, command, DA_CMD_P_PENDING, 0, false);
    }

    /**
     * Create a pending command for the ePort device by the given evNumber.
     * 
     * @param evNumber
     * @param command
     * @param order
     * @throws DataLayerException
     * @throws SQLException
     */
    public static void createEPortPendingCommand(Connection conn, String evNumber, String command, int order) throws SQLException, DataLayerException
    {
        createPendingCommand(conn, evNumber, DeviceUtils.EPORT_CMD_PEEK_V1, command, DA_CMD_P_PENDING, order, false);
    }
    
    /**
     * Create a pending command for the device by the given evNumber, passing in Connection for transactional integrity.
     * 
     * @param evNumber
     * @param dataType
     * @param command
     * @param executeCd
     * @param order
     * @param Connection
     * @throws DataLayerException
     * @throws SQLException
     */
    public static void createPendingCommand(Connection conn, String evNumber, String dataType, String command, String executeCd, int order, boolean cancelExisting) throws SQLException, DataLayerException
    {
    	DeviceUtils.createPendingCommand(conn, evNumber, dataType, command, executeCd, order, cancelExisting);
    }

    /**
     * Cancel the pending command record by the given id.
     * 
     * @param pendingCommandId
     * @throws SQLException
     * @throws DataLayerException
     */
    public static void cancel(String pendingCommandId) throws SQLException, DataLayerException
    {
        String[] params = {pendingCommandId};
        DataLayerMgr.executeCall("CANCEL_PENDING_CMD", params, true);
    }

    /**
     * Cancel all pending command records for the device by the given evNumber.
     * 
     * @param evNumber
     * @throws SQLException
     * @throws DataLayerException
     */
    public static void cancelAll(String evNumber) throws SQLException, DataLayerException
    {
        Object[] params = {evNumber};
        DataLayerMgr.executeCall("CANCEL_ALL_PENDING_CMD", params, true);
    }

    /**
     * Create the poke command by the given parameters, this is used to process the "Save" for
     * "edit configuration".
     * 
     * @param evNumber
     * @param addr
     * @param length
     * @param execOrderCounter
     * @return
     * @throws ServletException
     */
    public static void createPoke(Connection conn, String evNumber, int addr, int length, int execOrder) throws ServletException
    {
    	DeviceUtils.createPoke(conn, evNumber, addr, length, execOrder);
    }

    /**
     * Pad the value with the given parameters, this is used to process the "Save" for
     * "edit configuration".
     * 
     * @param data
     * @param padWith
     * @param size
     * @param align
     * @param dataMode
     * @return a string array, where array[0] is the message, and array[1] is the padded data
     */
    public static String[] pad(String data, String padWith, int size, String align, String dataMode)
    {
    	return DeviceUtils.pad(data, padWith, size, align, dataMode);
    }
}
