/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.usatech.dms.model.ActionPayloadTriple;
import com.usatech.dms.sales.CommissionEvent;
import com.usatech.dms.sales.CommissionEventType.CommissionEventTypeCode;
import com.usatech.layers.common.LegacyUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.GenericResponseServerActionCode;
import com.usatech.layers.common.constants.PosEnvironment;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.model.Customer;
import com.usatech.layers.common.model.Device;
import com.usatech.layers.common.model.Location;
import com.usatech.layers.common.model.PaymentConfig;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.lang.InvalidByteValueException;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.text.StringUtils;

/**
 * Class provides the methods for device operations.
 */
public final class DeviceActions
{
    private static final simple.io.Log log = simple.io.Log.getLog();
    
    public static final String KIOSK_CONFIG_FILE_NAME = "EportNW.ini";
    public static final String KIOSK_CONFIG_FILE_NAME_HEX = StringHelper.encodeHexString(KIOSK_CONFIG_FILE_NAME);
    public static final String KIOSK_PUBLIC_PC = "Public_PC_Version";
    public static final String ESUDS_CONFIG_FILE_NAME = "/usr/local/usatech/etc/esuds.properties";
    public static final String ESUDS_CONFIG_FILE_NAME_HEX = StringHelper.encodeHexString(ESUDS_CONFIG_FILE_NAME);

    public static final String DEFAULT_CFG_KIOSK_PUBLICPC = "PUBLICPC-DEFAULT-CFG";
    public static final String DEFAULT_CFG_T2 = "T2-DEFAULT-CFG";
    public static final String DEFAULT_CFG_PREFIX_EDGE = "DEFAULT-CFG-13-";
    public static final String DEFAULT_CFG_KIOSK = "KIOSK-DEFAULT-CFG";

    /** Don't let anyone instantiate this class. */
    private DeviceActions()
    {}

    public static Device loadDevice(long deviceId, HttpServletRequest request) throws ServletException
    {
        Device device = DeviceUtils.generateDevice(deviceId);
        request.setAttribute(DevicesConstants.STORED_DEVICE, device);
        return device;
    }
    
    public static long getDeviceIdByName(String deviceName, Timestamp effectiveDate) throws ServletException {
        try {
            Results result = DataLayerMgr.executeQuery("GET_DEVICE_ID_BY_NAME", new Object[] {deviceName, effectiveDate}, false);
            long deviceId = -1;        
            if(result.next())            
                deviceId = ConvertUtils.getLongSafely(result.get("device_id"), -1);
            if(deviceId < 1)
                throw new ServletException("No device found for device name " + deviceName);
            return deviceId;
        } catch (Exception e) {
            throw new ServletException("Error getting device ID by device name", e);
        }
    }
    
    public static long getDeviceIdBySerial(String deviceSerialCd, Timestamp effectiveDate) throws ServletException {
        try {
            Results result = DataLayerMgr.executeQuery("GET_DEVICE_ID_BY_SERIAL", new Object[] {deviceSerialCd, effectiveDate}, false);
            long deviceId = -1;
            if(result.next())            
                deviceId = ConvertUtils.getLongSafely(result.get("device_id"), -1);
            if(deviceId < 1) {
                result = DataLayerMgr.executeQuery("GET_DEVICE_ID_BY_NAME", new Object[] {deviceSerialCd, effectiveDate}, false);
                if(result.next())            
                    deviceId = ConvertUtils.getLongSafely(result.get("device_id"), -1);
                if(deviceId < 1)
                    throw new DataLayerException("No device found for device serial number " + deviceSerialCd);
            }
            return deviceId;
        } catch (Exception e) {
            throw new ServletException("Error getting device ID by serial number", e);
        }
    }
    
    public static String getDeviceName(String deviceNameOrSerial) throws ServletException {
        try {
            Results result = DataLayerMgr.executeQuery("GET_DEVICE_NAME", new Object[] {deviceNameOrSerial, deviceNameOrSerial});
            String deviceName = null;        
            if(result.next())            
                deviceName = result.getValue("device_name", String.class);
            return deviceName;
        } catch (Exception e) {
            throw new ServletException("Error getting device name by device name or serial", e);
        }
    }
    
    public static String getDeviceName(long deviceId) throws ServletException {
        try {
            Results result = DataLayerMgr.executeQuery("GET_DEVICE_NAME_BY_DEVICE_ID", new Object[] {deviceId});
            String deviceName = null;
            if(result.next())            
                deviceName = result.getValue("device_name", String.class);
            return deviceName;
        } catch (Exception e) {
            throw new ServletException("Error getting device name by device ID", e);
        }
    }
    
    public static Device loadDevice(String deviceName, HttpServletRequest request) throws ServletException
    {
        long deviceId = getDeviceIdByName(deviceName, null);
        return loadDevice(deviceId, request);
    }

    /**
     * patch for Oracle 10.1.0.x session default date format bug, causing occasional ORA-01801
     * errors on some views and PL/SQL
     * 
     * @throws DataLayerException
     * @throws SQLException
     */
    public static void setNLSDateFormat() throws SQLException, DataLayerException
    {
        DataLayerMgr.executeCall("SET_NLS_DATE_LANGUAGE", null, true);
        DataLayerMgr.executeCall("SET_NLS_DATE_FORMAT", null, true);
    }

    /**
     * Retrieve the "action value", "action description" and "payload value" from the
     * "Grsa"(device.generic_response_s2c_action) by the given condition.
     * 
     * @param deviceTypeId
     * @param propertyListVersion
     * @param protocolResivision
     * @return the list of ActionPayloadTriple
     * @throws SQLException
     * @throws DataLayerException
     */
    public static List<ActionPayloadTriple> getGrsaActionPayloads(int deviceTypeId, int propertyListVersion, int protocolResivision) throws SQLException, DataLayerException
    {
        List<ActionPayloadTriple> actionPayloads = new ArrayList<ActionPayloadTriple>();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("deviceTypeId", deviceTypeId);
        params.put("plv", propertyListVersion);
        params.put("protocolRevision", protocolResivision);
        Results result = DataLayerMgr.executeQuery("GET_EDGE_GENERIC_RESPONSE_S2C_ACTION_SELECTIONS", params, false);
        while (result.next())
        {
            ActionPayloadTriple grsaItem = new ActionPayloadTriple();
            grsaItem.setActionValue(result.getFormattedValue("actionValue"));
            grsaItem.setActionDesc(result.getFormattedValue("actionDesc"));
            grsaItem.setPayloadValue(result.getFormattedValue("payloadValue"));
            actionPayloads.add(grsaItem);
        }
        return actionPayloads;
    }

    /**
     * Retrieve the location for the given device id.
     * 
     * @param deviceId
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static Location getLocation(long deviceId) throws SQLException, DataLayerException, ConvertException
    {
        Location location = new Location();
        Results result = DataLayerMgr.executeQuery("GET_LOCATION", new Object[] {deviceId}, false);
        if (result.next())
        {
            location.setId(result.getValue("location_id", Long.class));
            location.setName(result.getValue("location_name", String.class));
            location.setAddress1(result.getValue("location_addr1", String.class));
            location.setAddress2(result.getValue("location_addr2", String.class));
            location.setCity(result.getValue("location_city", String.class));
            location.setCountry(result.getValue("location_country", String.class));
            location.setPostalCode(result.getValue("location_postal_cd", String.class));
            location.setParentId(result.getValue("parent_location_id", Long.class));
            location.setCountryCode(result.getValue("location_country_cd", String.class));
            location.setTypeId(result.getValue("location_type_id", Integer.class));
            location.setStateCode(result.getValue("location_state_cd", String.class));
            location.setTimeZoneCode(result.getValue("location_time_zone_cd", String.class));
            location.setActiveFlag(result.getValue("location_active_yn_flag", String.class));
        }
        return location;
    }

    /**
     * Retrieve the customer for the given device id.
     * 
     * @param deviceId
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static Customer getCustomer(long deviceId) throws SQLException, DataLayerException, ConvertException
    {
        Customer customer = new Customer();
        Results result = DataLayerMgr.executeQuery("GET_CUSTOMER", new Object[] {deviceId}, false);
        if (result.next())
        {
            customer.setId(result.getValue("customer_id", Long.class));
            customer.setName(result.getValue("customer_name", String.class));
            customer.setCity(result.getValue("customer_city", String.class));
            customer.setAddress1(result.getValue("customer_addr1", String.class));
            customer.setAddress1(result.getValue("customer_addr2", String.class));
            customer.setCountry(result.getValue("customer_country", String.class));
            customer.setPostalCode(result.getValue("customer_postal_cd", String.class));
            customer.setCountryCode(result.getValue("customer_country_cd", String.class));
            customer.setTypeId(result.getValue("customer_type_id", Integer.class));
            customer.setStateCode(result.getValue("customer_state_cd", String.class));
            customer.setActiveFlag(result.getValue("customer_active_yn_flag", String.class));
        }
        return customer;
    }

    /**
     * Retrieve the payment configuration list for the given device id.
     * 
     * @param deviceId
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static List<PaymentConfig> getConfigList(long deviceId) throws SQLException, DataLayerException, ConvertException
    {
        List<PaymentConfig> configList = new ArrayList<PaymentConfig>();
        Results result = DataLayerMgr.executeQuery("GET_POS_PTA", new Object[] {deviceId, deviceId}, false);
        while (result.next())
        {
            PaymentConfig config = new PaymentConfig();
            String paymentSubTypeTableName = result.getValue("payment_subtype_table_name", String.class);
            String paymentSubtypeKeyName = result.getValue("payment_subtype_key_name", String.class);
            Integer paymentSubtypeKeyId = result.getValue("payment_subtype_key_id", Integer.class);
            String merchantDesc = HostAction.getMerchantDesc(paymentSubTypeTableName, paymentSubtypeKeyName, paymentSubtypeKeyId);

            config.setMerchantDesc(merchantDesc);
            config.setPaymentSubtypeName(result.getValue("payment_subtype_name", String.class));
            config.setDeactivationTs(result.getValue("pos_pta_deactivation_ts", Date.class));
            config.setStatusTxtDisabled(result.getValue("status_txt_disabled", String.class));
            config.setActivationTs(result.getValue("pos_pta_activation_ts", Date.class));
            config.setStatusTxtEnabled(result.getValue("status_txt_enabled", String.class));
            config.setPaymentEntryMethodCd(result.getValue("payment_entry_method_cd", String.class));
            config.setPaymentSubtypeTableName(paymentSubTypeTableName);
            config.setPaymentSubtypeKeyName(paymentSubtypeKeyName);
            config.setPaymentSubtypeKeyId(paymentSubtypeKeyId);
            config.setPaymentEntryMethodDesc(result.getValue("payment_entry_method_desc", String.class));
            config.setPrefAuthAmount(result.getValue("pos_pta_pref_auth_amt", String.class));

            configList.add(config);

        }
        return configList;
    }

    private static long validatePosId(long posId, long deviceId) throws SQLException, DataLayerException, ConvertException
    {
        if (posId < 1)
        {
            Results result = DataLayerMgr.executeQuery("GET_ACTIVE_POS_ID", new Object[] {deviceId}, false);
            if (result.next())
            {
                posId = result.getValue("pos_id", Long.class);
            }
        }
        return posId;
    }

    /**
     * Clone Device Pos Ptas.
     * 
     * @param conn
     * @param sourceDeviceId
     * @param targetDeviceId
     * @param errorMsg
     * @param sourcePosId
     * @param targetPosId
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static void cloneDevicePosPtas(Connection conn, long sourceDeviceId, long targetDeviceId, long sourcePosId, long targetPosId, String targetDeviceName) throws SQLException,
            DataLayerException, ConvertException, ServletException
    {
        sourcePosId = validatePosId(sourcePosId, sourceDeviceId);
        targetPosId = validatePosId(targetPosId, targetDeviceId);
        if (sourcePosId <= 0 || targetPosId <= 0)
            throw new ServletException(new StringBuilder("Error cloning Payment Types, invalid source_pos_id ").append(sourcePosId).append(" or target_pos_id ").append(targetPosId).toString());
        Object[] result = DataLayerMgr.executeCall(conn, "DISABLE_OLD_POS_PTAS", new Object[] {targetPosId});
        if (result[0] == null)
            throw new ServletException("Error cloning Payment Types, couldn't update old pos_ptas to disabled");
        result = DataLayerMgr.executeCall(conn, "UPDATE_OLD_POS_PTA_PRIORITIES", new Object[] {sourcePosId, targetPosId, targetPosId});
        if (result[0] == null)
            throw new ServletException("Error cloning Payment Types, couldn't update old pos_pta priorities");
        result = DataLayerMgr.executeCall(conn, "INSERT_NEW_POS_PTAS", new Object[] {targetPosId, sourcePosId});
        if (result[0] == null)
            throw new ServletException("Error cloning Payment Types, couldn't insert one or more pos_pta(s)");
    }
    
    /**
     * Clone Device Pos Ptas.
     * 
     * @param conn
     * @param sourceDeviceId
     * @param targetDeviceId
     * @param errorMsg
     * @param sourcePosId
     * @param targetPosId
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static void cloneDevicePosPtasNoUpdateOldPriorities(Connection conn, long sourceDeviceId, long targetDeviceId, long sourcePosId, long targetPosId) throws SQLException,
            DataLayerException, ConvertException, ServletException
    {
        sourcePosId = validatePosId(sourcePosId, sourceDeviceId);
        targetPosId = validatePosId(targetPosId, targetDeviceId);
        if (sourcePosId <= 0 || targetPosId <= 0)
            throw new ServletException(new StringBuilder("Error cloning Payment Types, invalid source_pos_id ").append(sourcePosId).append(" or target_pos_id ").append(targetPosId).toString());
        Object[] result = DataLayerMgr.executeCall(conn, "DISABLE_OLD_POS_PTAS", new Object[] {targetPosId});
        if (result[0] == null)
            throw new ServletException("Error cloning Payment Types, couldn't update old pos_ptas to disabled");
        result = DataLayerMgr.executeCall(conn, "INSERT_NEW_POS_PTAS", new Object[] {targetPosId, sourcePosId});
        if (result[0] == null)
            throw new ServletException("Error cloning Payment Types, couldn't insert one or more pos_pta(s)");
    }
    
    /**
     * Modify POS_PTA records based on the device, template id and mode code passed in.
     *         # Import mode codes: 
     *        #    S = Safe Mode
     *        #        - Create new POS_PTA records based on the given template where no active
     *        #          pos_pta already exists per client_payment_type_cd
     *        #    MS = Merge Safe Mode
     *        #        - Create new POS_PTA records based on the given template where no active
     *        #          pos_pta already exists per payment_subtype_id
     *        #    MO = Merge Overwrite Mode
     *        #        - Deactive any existing POS_PTA records where a new POS_PTA records is defined 
     *        #          in the template per payment_subtype_id
     *        #        - Create all new POS_PTA records based on the given template
     *        #    O = Overwrite Mode
     *        #        - Deactivate all existing POS_PTA records where a new POS_PTA records is 
     *        #          being imported for the client_payment_type_cd
     *        #        - Create all new POS_PTA records based on the given template
     * @param form
     * @param request
     * @throws ServletException
     * @throws NumberFormatException
     */
    public static int importPosPtaTemplate(long device_id, long pos_pta_tmpl_id, String mode_cd, String order_cd, String set_terminal_cd_to_serial, String only_no_two_tier_pricing, Connection conn) throws ServletException, NumberFormatException {
        /*
         * S = Safe Mode - Create new POS_PTA records based on the given template where no active
         * pos_pta already exists per client_payment_type_cd MS = Merge Safe Mode - Create new
         * POS_PTA records based on the given template where no active pos_pta already exists per
         * payment_subtype_id MO = Merge Overwrite Mode - Deactive any existing POS_PTA records
         * where a new POS_PTA records is defined in the template per payment_subtype_id - Create
         * all new POS_PTA records based on the given template O = Overwrite Mode - Deactivate all
         * existing POS_PTA records where a new POS_PTA records is being imported for the
         * client_payment_type_cd - Create all new POS_PTA records based on the given template
         */

        try
        {
            Object[] result = null;
            if(conn != null)
                result = DataLayerMgr.executeCall(conn, "SP_IMPORT_POS_PTA_TEMPLATE", new Object[] { device_id, pos_pta_tmpl_id, mode_cd, order_cd, set_terminal_cd_to_serial, null, null, only_no_two_tier_pricing });
            else
                result = DataLayerMgr.executeCall("SP_IMPORT_POS_PTA_TEMPLATE", new Object[] { device_id, pos_pta_tmpl_id, mode_cd, order_cd, set_terminal_cd_to_serial, null, null, only_no_two_tier_pricing }, true);
            return ConvertUtils.getIntSafely(result[1], -1);
        }
        catch (Exception e)
        {
            throw new ServletException("Error importing payment template, device_id: " + device_id + ", pos_pta_tmpl_id: " + pos_pta_tmpl_id, e);
        }
    }

    public static String getDeviceConfigName(String deviceName) {
        return new StringBuilder(deviceName).append("-CFG").toString();
    }

    public static void processDeviceAction(Connection conn, String action, InputForm form, long deviceId, String deviceName, int deviceTypeId, int plv, boolean bulk, String[] actionParams) throws ServletException
    {
        if (StringHelper.isBlank(action))
            return;

        String message = null;
        try
        {
            /* actions for "Device Configuration" table */
            if ("Device Reactive".equalsIgnoreCase(action))
            {
                // "Device Reactive" for ESUDS device
                PendingCmdActions.createRequest(conn, deviceName, "76", "");
            }
            else if ("Shutdown".equalsIgnoreCase(action))
            {
                // "Shutdown" for ESUDS device
                String command = form.getString("shutdown_option", false) + form.getString("shutdown_state", false);
                PendingCmdActions.createRequest(conn, deviceName, "73", command);
            }

            /* actions for "Server to Client request" table */
            // "Server to Client request" actions for ESUDS device
            else if ("Software Reboot".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "00");
            }
            else if ("Hardware Reboot".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "01");
            }
            else if ("Dump Transactions".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "02");
            }
            else if ("Client Version".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "0B");
            }
            else if ("Room Layout".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "0C");
            }
            else if ("Send Logs".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "0D");
            }
            else if ("Enable Logins".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "0E");
            }
            else if ("Synch Time".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "0F");
            }
            else if ("Clear Card Cache".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "83", "10");
            }
            else if ("Machine Diagnostics".equalsIgnoreCase(action))
            {
                PendingCmdActions.createRequest(conn, deviceName, "9A61", null);
            }
            // "Server to Client request" actions for other types device
            else if ("Firmware Version".equalsIgnoreCase(action))
            {
                PendingCmdActions.createEPortPendingCommand(conn, deviceName, LegacyUtils.CMD_FIRMWARE_VERSION, 0);
            }
            else if ("Upload Config".equalsIgnoreCase(action))
            {
                PendingCmdActions.createEPortPendingCommand(conn, deviceName, LegacyUtils.CMD_UPLOAD_CONFIG, 0);
            }
            else if ("Wavecom Modem Info".equalsIgnoreCase(action))
            {
                PendingCmdActions.createEPortPendingCommand(conn, deviceName, LegacyUtils.CMD_WAVECOM_MODEM_INFO, 0);
            }
            else if ("Boot Load/App Rev".equalsIgnoreCase(action))
            {
                PendingCmdActions.createEPortPendingCommand(conn, deviceName, LegacyUtils.CMD_BOOTLOAD_APPVER, 0);
            }
            else if ("Bezel Info".equalsIgnoreCase(action))
            {
                PendingCmdActions.createEPortPendingCommand(conn, deviceName, LegacyUtils.CMD_BEZEL_INFO, 0);
            }
            else if("Download GPRS Info".equalsIgnoreCase(action))
            {
                String fileTransferName = "EportGPRSInfo";
                String dfn = StringHelper.encodeHexString(fileTransferName);
                PendingCmdActions.createRequest(conn, deviceName, DeviceUtils.EPORT_CMD_FILE_TRANSFER_REQUEST_9BH, dfn);
            }
            else if ("External File Transfer".equalsIgnoreCase(action))
            {
                form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, "/createExternalFileTransfer.i");
                return;
            }
            else if ("Queue Command".equalsIgnoreCase(action))
            {
                // "Queue Command" for EDGE device
                String genericResS2C = bulk ? actionParams[0] : form.getString("generic_response_s2c", false);
                String payload = bulk ? actionParams[1] : form.getString("payload", false);
                queueCommand(conn, deviceName, genericResS2C, payload);
            }
            /* actions for "Pending Command" table */
            else if ("Cancel All".equalsIgnoreCase(action))
            {
                PendingCmdActions.cancelAll(deviceName);
                message = "Commands+cancelled";
            } 
            else if("Download File".equalsIgnoreCase(action))
            {
                String downloadFileName = bulk ? actionParams[0] : form.getString("download_file_name", false); 
                if(StringHelper.isBlank(downloadFileName) 
                        || "full path on client".equalsIgnoreCase(downloadFileName) 
                        || "name or path on client".equalsIgnoreCase(downloadFileName))                     
                    throw new ServletException("Required Parameter Not Found: download_file_name");
                 
                // remove any pending File Transfer Start messages for this file name
                StringBuilder fileTransferCmdStr = getFileTransferCmdStr();
                Object[] params = new Object[] {"A", downloadFileName, downloadFileName+".gz", deviceId, 0, "O", 
                        deviceName, fileTransferCmdStr, fileTransferCmdStr, fileTransferCmdStr, "P"};
                
                removePendingFTStartMsgs(conn, params, 2);
                
                if(DeviceType.EDGE.getValue() == deviceTypeId){
                    int downloadFileType = bulk ? Integer.valueOf(actionParams[1]) : form.getInt("download_file_type", true, -1);
                    StringBuilder command = new StringBuilder("");
                    int rt = 1;
                    String requestType = Integer.toHexString( 0x10000 | rt).substring(3).toUpperCase();
                    String fileType = Integer.toHexString( 0x10000 | downloadFileType).substring(1).toUpperCase();
                    int dfnLen = downloadFileName.length();
                    String fileNameLength = Integer.toHexString( 0x10000 | dfnLen).substring(3).toUpperCase();
                    String fileName = StringHelper.encodeHexString(downloadFileName);
                    command.append(requestType).append(fileType).append(fileNameLength).append(fileName);
                    PendingCmdActions.createRequest(conn, deviceName, DeviceUtils.EPORT_CMD_GENERIC_REQUEST_V4_1, command.toString().toUpperCase());
                }else{ 
                    String fileName = StringHelper.encodeHexString(downloadFileName);
                    PendingCmdActions.createRequest(conn, deviceName, DeviceUtils.EPORT_CMD_FILE_TRANSFER_REQUEST_9BH, fileName.toUpperCase());
                }                
            } else if("Download Config".equalsIgnoreCase(action)){
                if(DeviceType.KIOSK.getValue() == deviceTypeId || DeviceType.ESUDS.getValue() == deviceTypeId) {
                    String fileTransferName, fileTransferNameHex;
                    if(DeviceType.KIOSK.getValue() == deviceTypeId) {
                        fileTransferName = KIOSK_CONFIG_FILE_NAME;
                        fileTransferNameHex = KIOSK_CONFIG_FILE_NAME_HEX;
                    } else {
                        fileTransferName = ESUDS_CONFIG_FILE_NAME;
                        fileTransferNameHex = ESUDS_CONFIG_FILE_NAME_HEX;
                    }

                    // remove any pending File Transfer Start messages for this file name
                    StringBuilder fileTransferCmdStr = DeviceActions.getFileTransferCmdStr();
                    Object[] params = new Object[] { "A", fileTransferName, deviceId, 0, "O", deviceName, fileTransferCmdStr, fileTransferCmdStr, fileTransferCmdStr, "P" };
                    DeviceActions.removePendingFTStartMsgs(conn, params, 1);

                    PendingCmdActions.createRequest(conn, deviceName, DeviceUtils.EPORT_CMD_FILE_TRANSFER_REQUEST_9BH, fileTransferNameHex);
                } else if(DeviceType.EDGE.getValue() == deviceTypeId) {
                    PendingCmdActions.createRequest(conn, deviceName, DeviceUtils.EPORT_CMD_GENERIC_RESPONSE_V4_1, "00000E0008302D32303030210A");
                } else
                    return;
            } else if ("Upload to Device".equalsIgnoreCase(action)) {
                form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, "/fileDetailsFunc.i");
                return;
            } else if ("Change Credential".equalsIgnoreCase(action)) {
                if (conn == null)
                    DataLayerMgr.executeUpdate("UPDATE_DEVICE_CREDENTIAL", new Object[] {form.getLong("credential_id", true, 0), deviceId}, true);
                else
                    DataLayerMgr.executeUpdate(conn, "UPDATE_DEVICE_CREDENTIAL", new Object[] {form.getLong("credential_id", true, 0), deviceId});
                message = "Credential+changed";
            } else if ("Upgrade Device".equalsIgnoreCase(action)) {
                ConfigFileActions.scheduleUpgrade(deviceId, form.getLong("firmware_upgrade_id", true, 0));
                message = "Upgrade+scheduled";
            }
        }
        catch (Exception e)
        {
            throw new ServletException("Error while processing action '" + action + "'.", e);
        }

        if (!bulk) {
            StringBuilder redirect = new StringBuilder("/deviceConfig.i?device_id=").append(deviceId);
            if (message != null)
                redirect.append("&msg=").append(message);
            form.setRedirectUri(redirect.toString());
        }
    }
    
    public static String getDeviceDefaultConfigFile(Device device, LinkedHashMap<String, String> deviceSettingData) {
        if (device.isEDGE())
            return new StringBuilder(DEFAULT_CFG_PREFIX_EDGE).append(device.getPropertyListVersion()).toString();
        else if (device.isGX())
            return "G5-DEFAULT-CFG";
        else if (device.isG4())
            return "G4-DEFAULT-CFG";
        else if (device.isMEI())
            return "MEI-DEFAULT-CFG";
        else if (device.isKIOSK() && deviceSettingData != null && deviceSettingData.containsKey(KIOSK_PUBLIC_PC))
            return DEFAULT_CFG_KIOSK_PUBLICPC;
        else if (device.isT2())
            return DEFAULT_CFG_T2;
        else
            return DEFAULT_CFG_KIOSK;
    }
    
    public static void queueCommand(Connection conn, String deviceName, String genericResS2C, String payload) throws ServletException
    {
        // CB command consist of
        // 1 byte result code for this case it is 0
        // 1-256 bytes response message, we will use 1 byte and set it to zero
        // 1 byte action code which comes from $PARAM{generic_response_s2c}
        // payload dependent on action code
        
        if (StringHelper.isBlank(genericResS2C) || "-1".equalsIgnoreCase(genericResS2C))
            return;

        GenericResponseServerActionCode actionCode;
        try {
            actionCode = GenericResponseServerActionCode.getByValue(ConvertUtils.getByte(genericResS2C.substring(1)));
        } catch(InvalidByteValueException e) {
            throw new ServletException("Invalid Generic Response Action Code '" + genericResS2C.substring(1) + "'");
        } catch(ConvertException e) {
            throw new ServletException("Invalid Generic Response Action Code '" + genericResS2C.substring(1) + "'");
        }
        StringBuilder command = new StringBuilder();
        command.append("00"); // result code
        command.append("00"); // response message
        command.append(StringUtils.toHex(actionCode.getValue()));
        switch(actionCode) {
            case REBOOT:
            case DISCONNECT:
                int reconnectTime;
                try {
                    reconnectTime = ConvertUtils.getInt(payload, 300);
                } catch(ConvertException e) {
                    throw new ServletException("Payload is not formatted properly; Use an number of seconds before reconnect");
                }
                command.append(StringUtils.toHex((short) reconnectTime));
                break;
            case UPLOAD_PROPERTY_VALUE_LIST:
                // Verify payload format
                try {
                    ProcessingUtils.parsePropertyIndexList(payload);
                } catch(ParseException e) {
                    throw new ServletException("Payload is not formatted properly; Use an index list format");
                }
                command.append(StringUtils.toHex((short) payload.length()));
                command.append(StringHelper.encodeHexString(payload));
                break;
            case PROCESS_PROP_LIST:
                // Verify payload format
                try {
                    ProcessingUtils.parsePropertyList(payload, ProcessingUtils.DO_NOTHING_PROPERTY_VALUE_HANDLER);
                } catch(NumberFormatException e) {
                    throw new ServletException("Payload is not formatted properly; Use an value list format");
                } catch(ParseException e) {
                    throw new ServletException("Payload is not formatted properly; Use an value list format");
                } catch(IOException e) {
                    throw new ServletException("Payload is not formatted properly; Use an value list format");
                }
                command.append(StringUtils.toHex((short) payload.length()));
                command.append(StringHelper.encodeHexString(payload));
                break;
            case TRACE_EVENTS:
                String[] parts = StringUtils.split(payload, ',');
                long maxBytes;
                try {
                    maxBytes = ConvertUtils.getLong(parts.length > 0 ? parts[0] : null, 200 * 1024L);
                } catch(ConvertException e) {
                    throw new ServletException("Payload is not formatted properly; Use an max number of bytes, then a comma, then max number of seconds, then a comma, then a number bitmap of options");
                }
                long maxSeconds;
                try {
                    maxSeconds = ConvertUtils.getLong(parts.length > 1 ? parts[1] : null, 4 * 60 * 60L);
                } catch(ConvertException e) {
                    throw new ServletException("Payload is not formatted properly; Use an max number of bytes, then a comma, then max number of seconds, then a comma, then a number bitmap of options");
                }
                int optionsBitmap;
                try {
                    optionsBitmap = ConvertUtils.getInt(parts.length > 2 ? parts[2] : null, 0x01);
                } catch(ConvertException e) {
                    throw new ServletException("Payload is not formatted properly; Use an max number of bytes, then a comma, then max number of seconds, then a comma, then a number bitmap of options");
                }
                command.append(StringUtils.toHex((int) maxBytes));
                command.append(StringUtils.toHex((int) maxSeconds));
                command.append(StringUtils.toHex((short) optionsBitmap));
                break;
        }

        try {
            PendingCmdActions.createRequest(conn, deviceName, DeviceUtils.EPORT_CMD_GENERIC_RESPONSE_V4_1, command.toString());
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
    
    /**
     * Helper to build file transfer cmd string.
     * @return
     */
    public static StringBuilder getFileTransferCmdStr() {
        StringBuilder fileTransferCmdStr = new StringBuilder("");
        
        Set<String> keys = PendingCmdActions.fileTransferCmdList.keySet();
            
        Iterator<String> it = keys.iterator();
        int ctr = 0;
        while(it.hasNext()){
            ctr++;
            if(ctr!=1){
                fileTransferCmdStr.append(", ");
            }
            String key = it.next();
            fileTransferCmdStr.append("'");
            fileTransferCmdStr.append(key);
            fileTransferCmdStr.append("'");
        }
        return fileTransferCmdStr;
    }
    
    /**
     * Helper to remove any pending cmd start messages.
     * @param params
     * @param fileTransferCmdStr
     * @throws SQLException
     * @throws DataLayerException
     */
    public static int removePendingFTStartMsgs(Connection conn, Object[] params, int fileNameCnt) throws ServletException  {
        
        int success = -1;
        try{
            if(fileNameCnt>1){
                if (conn == null)
                    success = DataLayerMgr.executeUpdate("UPDATE_PENDING_CMD_MULTI_FILE_NAME", params, true);
                else
                    success = DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_CMD_MULTI_FILE_NAME", params);
            }else{
                if (conn == null)
                    success = DataLayerMgr.executeUpdate("UPDATE_PENDING_CMD", params, true);
                else
                    success = DataLayerMgr.executeUpdate(conn, "UPDATE_PENDING_CMD", params);
            }
        }catch(Exception sqle){
            throw new ServletException("Encountered Exception removing Pending File Transfer Start Msg: " + (sqle.getClass().getName() + " : " + sqle.getMessage()));
        }
        return success;
    }
    
    public static boolean populateBulkConfigList(InputForm form, String listName, String listItem, boolean checkExisting) {
        String listValues = form.getStringSafely(listName, null);
        if (StringHelper.isBlank(listValues)) {
            form.setAttribute(listName, listItem);
            return true;
        }
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(listValues.split(", ", -1)));
        if(checkExisting && list.contains(listItem))
            return false;
        form.setAttribute(listName, new StringBuilder(listValues).append(", ").append(listItem).toString());
        return true;
    }
    
    public static Device getDevice(HttpServletRequest request, String userOP, long deviceId) throws ServletException
    {
        try
        {
            Device device = DeviceActions.loadDevice(deviceId, request);
            Location location = DeviceActions.getLocation(deviceId);
            Customer customer = DeviceActions.getCustomer(deviceId);
            device.setLocation(location);
            device.setCustomer(customer);
            if ("Compare".equals(userOP))
            {
                List<PaymentConfig> paymentConfigList = DeviceActions.getConfigList(deviceId);
                device.setPaymentConfigList(paymentConfigList);
            }
            return device;

        }
        catch (Exception e)
        {
            throw new ServletException("Exception getting device information", e);
        }
    }

    public static void cloneDevice(InputForm inputForm, HttpServletRequest request, String userOP, Device sourceDevice, Device targetDevice, boolean interactive) throws ServletException
    {
        StringBuilder msg = new StringBuilder();
        String locationCheckbox = inputForm.getString("locationCheckbox", false);
        boolean success = false;
        Connection conn = null;
        try
        {
            conn = DataLayerMgr.getConnection("OPER");

            Object[] result = DataLayerMgr.executeCall(conn, "UPDATE_LOC_ID_CUST_ID", new Object[] {targetDevice.getId(), sourceDevice.getLocation().getId(), sourceDevice.getCustomer().getId(), 20, 20, 20, "2000"});
            if (result[3] == null || ConvertUtils.getInt(result[3]) != 0)
                throw new ServletException("Error updating customer or location: " + result[4]);
            
            long newTargetDeviceId  = ConvertUtils.getLong(result[1]);
            long newTargetPosId  = ConvertUtils.getLong(result[2]);
            
            if (interactive) {
                msg.append("NEW DEVICE_ID = ");
                msg.append(newTargetDeviceId);
                msg.append("  NEW POS_ID = ");
                msg.append(newTargetPosId);
                msg.append("<br/>");
            }
            
            DeviceActions.cloneDevicePosPtas(conn, sourceDevice.getId(), newTargetDeviceId, sourceDevice.getPosId(), newTargetPosId, targetDevice.getDeviceName());

            long newSourceDeviceId = sourceDevice.getId();
            if ("1".equalsIgnoreCase(locationCheckbox))
            {
                result = DataLayerMgr.executeCall(conn, "UPDATE_LOC_ID", new Object[] {sourceDevice.getId(), 1, 20, 20, 20, "2000"});
                if (result[3] == null || ConvertUtils.getInt(result[3]) != 0)
                    throw new ServletException("Error setting Location to Unknown: " + result[4]);

                if (interactive) {
                    msg.append("NEW SOURCE DEVICE_ID = ");
                    msg.append(result[1]);
                    msg.append("  NEW SOURCE POS_ID = ");
                    msg.append(result[2]);
                    msg.append("<br/>");
                }
            }                        
            
            conn.commit();
            success = true;            
            
            if (interactive) {
                if (newTargetDeviceId != targetDevice.getId())
                    targetDevice = getDevice(request, userOP, newTargetDeviceId);
                
                if (newSourceDeviceId != sourceDevice.getId())
                    sourceDevice = getDevice(request, userOP, newSourceDeviceId);
                
                DeviceActions.loadDevice(newTargetDeviceId, request);
                        
                inputForm.setAttribute("errorMsg", "<br /><span class='txtLightGreen'>Device cloning started...</span><br/>" + msg.toString()
                    + "<span class='txtLightGreen'>Device cloning succeeded</span><br/>");
            }
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }
        finally
        {
            if (!success)
                ProcessingUtils.rollbackDbConnection(log, conn);
            ProcessingUtils.closeDbConnection(log, conn);
        }
    }
    
    public static CommissionEvent createCommissionEvent(String deviceSerialCd, long salesRepId, String eventTypeCode, java.util.Date eventTimestamp) throws SQLException, DataLayerException, ConvertException
    {
    	return createCommissionEvent(deviceSerialCd, salesRepId, eventTypeCode, eventTimestamp, 0);
    }

    public static CommissionEvent createCommissionEvent(String deviceSerialCd, long salesRepId, String eventTypeCode, java.util.Date eventTimestamp, long fallbackSalesRepId) throws SQLException, DataLayerException, ConvertException
    {
      CommissionEvent event = new CommissionEvent();
      event.setDeviceSerialCd(deviceSerialCd);
      event.setSalesRepId(salesRepId);
      event.setEventTypeCode(CommissionEventTypeCode.valueOf(eventTypeCode));
      event.setTimestamp(eventTimestamp);
      event.setFallbackSalesRepId(fallbackSalesRepId);
      event.save();
      return event;
    }


}
