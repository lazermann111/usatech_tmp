/*
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.usatech.dms.model.CorpCustomer;
import com.usatech.layers.common.model.CustomerBankTerminal;
import com.usatech.dms.model.RiskAlert;
import com.usatech.dms.model.UserView;
import com.usatech.layers.common.model.Customer;

import simple.bean.ConvertException;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;
import simple.results.Results;

/**
 * Class provides the methods for customer operations.
 */
public class CustomerAction
{
    public static Customer getCustomer(int customer_id) throws SQLException, DataLayerException, BeanException
    {
        Customer customer = new Customer();
        Results result = DataLayerMgr.executeQuery("GET_CUSTOMER_INFO", new Object[] {customer_id}, false);

        if (result.next())
        {
            result.fillBean(customer);
        }
        return customer;
    }

    public static CorpCustomer getCorpCustomer(int customer_id) throws SQLException, DataLayerException, BeanException
    {
        CorpCustomer customer = new CorpCustomer();
        Results result = DataLayerMgr.executeQuery("GET_CORP_CUSTOMER", new Object[] {customer_id});
        if (result.next())
        {
            result.fillBean(customer);
        }
        return customer;
    }

    public static List<UserView> getCustomerUsers(int customer_id) throws SQLException, DataLayerException, BeanException
    {
        List<UserView> users = new ArrayList<UserView>();
        UserView user = null;
        Results result = DataLayerMgr.executeQuery("GET_USERS_BY_CUST_ID", new Object[] {customer_id}, false);
        while (result.next())
        {
            user = new UserView();
            result.fillBean(user);
            users.add(user);
        }
        return users;
    }

    public static List<CustomerBankTerminal> getBankAcctByCustId(int customer_id) throws SQLException, DataLayerException, BeanException
    {
        List<CustomerBankTerminal> bankAccts = new ArrayList<CustomerBankTerminal>();
        CustomerBankTerminal bankAcct = null;
        Results result = DataLayerMgr.executeQuery("GET_BANK_ACCT_BY_CUST_ID", new Object[] {customer_id}, false);
        while (result.next())
        {
            bankAcct = new CustomerBankTerminal();
            result.fillBean(bankAcct);
            bankAccts.add(bankAcct);
        }
        return bankAccts;
    }

    public static List<CustomerBankTerminal> getBankAcctByTermId(int terminal_id) throws SQLException, DataLayerException, BeanException
    {
        List<CustomerBankTerminal> bankAccts = new ArrayList<CustomerBankTerminal>();
        CustomerBankTerminal bankAcct = null;
        Results result = DataLayerMgr.executeQuery("GET_BANK_ACCT_BY_TERM_ID", new Object[] {terminal_id}, false);
        while (result.next())
        {
            bankAcct = new CustomerBankTerminal();
            result.fillBean(bankAcct);
            bankAccts.add(bankAcct);
        }
        return bankAccts;
    }

    public static void updateCustomer(CorpCustomer customer) throws SQLException, DataLayerException
    {
        DataLayerMgr.executeCall("CUSTOMER_UPD", customer, true);
    }

    public static void deleteCustomer(int customer_id) throws SQLException, DataLayerException
    {
        DataLayerMgr.executeCall("CUSTOMER_DEL", new Object[] {customer_id}, true);
    }

    public static CorpCustomer getCustomerInfo(int customer_id) throws SQLException, DataLayerException, BeanException, ConvertException
    {
        CorpCustomer customer = getCorpCustomer(customer_id);
        customer.setUsers(getCustomerUsers(customer_id));
        customer.setTerminals(TerminalActions.getTerminalsByCustomerId((long)customer_id));
        customer.setBankAccts(getBankAcctByCustId(customer_id));
        return customer;
    }

    public static void acceptCustomer(Integer customer_id, Integer user_id) throws SQLException, DataLayerException
    {
     DataLayerMgr.executeCall("CUSTOMER_ACCEPT", new Object[]{customer_id,user_id}, true);
    }

    public static boolean checkActiveTermOfCust(Integer customer_id) throws SQLException, DataLayerException, ConvertException
    {
        Results result = DataLayerMgr.executeQuery("GET_ACTIVE_TERMINAL_COUNT", new Object[] {customer_id});
        if (result.next())
        {
            if (result.getValue(1, int.class) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
    
    public static String parentOrChildLocation(int app_user_id, int app_id, int app_object_type_id, String parent_object_cd, String child_object_cd) throws SQLException, DataLayerException, ConvertException
    {
        Results parent_result = DataLayerMgr.executeQuery("GET_APP_USER_PERMISSIONS_COUNT_BY_UID_APPID_APPOBJTYPEID_OBJCD", new Object[] {app_user_id, app_id, app_object_type_id, parent_object_cd});
        if (parent_result.next())
        {
            if (parent_result.getValue(1, BigDecimal.class).intValue() > 0)
            {
                return "P"+parent_object_cd;
            }
            else
            {	Results child_results = DataLayerMgr.executeQuery("GET_APP_USER_PERMISSIONS_COUNT_BY_UID_APPID_APPOBJTYPEID_OBJCD", new Object[] {app_user_id, app_id, app_object_type_id, child_object_cd});
	            if (child_results.next())
	            {
	                if (child_results.getValue(1, BigDecimal.class).intValue() > 0)
	                {
	                    return "C";
	                }
	            }
            }
        }
        return "";
    }
    
    public static List<RiskAlert> getRiskAlerts(Long customerId)  throws SQLException, DataLayerException, ConvertException
    {
      List<RiskAlert> list = new ArrayList<>();
      Results results = DataLayerMgr.executeQuery("GET_CUSTOMER_RISK_ALERTS", new Object[] {customerId});
      while(results.next()) {
       Long id = results.getValue("id", Long.class);
       RiskAlert alert = new RiskAlert(id);
        try {
         ReflectionUtils.populateProperties(alert, results.getValuesMap());
        } catch (Exception e) {
         throw new ConvertException("Failed to populate RiskAlert " + id + " from results", RiskAlert.class, results, e);
        }
        list.add(alert);
       }
      return list;
    }

  	public static List<CorpCustomer> findCorpCustomersByName(String name) throws SQLException, DataLayerException, BeanException {
  		Results results = DataLayerMgr.executeQuery("FIND_CORP_CUSTOMERS_BY_NAME", new Object[] {name}, false);
  		List<CorpCustomer> customers = new ArrayList<>();
  		while(results.next()) {
  			CorpCustomer customer = new CorpCustomer();
  			results.fillBean(customer);
  			customers.add(customer);
  		}
  		return customers;
  	}
}
