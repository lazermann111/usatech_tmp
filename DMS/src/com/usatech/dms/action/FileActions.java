/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.util.NameValuePair;

import com.usatech.dms.model.FileTransfer;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;


/**
 * Class provides the methods for device operations.
 */
public final class FileActions
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	public static final DateFormat dateTimeformatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

    /** Don't let anyone instantiate this class. */
    private FileActions()
    {}
    
    public static final List<NameValuePair> fileEncapsulations;
    public static final List<NameValuePair> crcTypes; 
    public static final List<NameValuePair> protocols;
    public static final List<NameValuePair> fileFormats;    
	
    public static final String regexPatternAlpha = "^[A-Za-z]$";
    public static final String regexPatternIpAddress = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$";
    public static final String regexPatternPort = "^\\d{1,5}$";
    public static final String regexPatternAllCharTypesLength256 = "^.{1,256}$";
    public static final String regexPatternAlphaNumeric = "^[0-9a-zA-Z]{2}$";
    public static final String regexPatternHex = "^[0-9a-fA-F]{4,8}$";
    public static final String regexPatternHexEncryption = "^[0-9a-fA-F]{2,2048}$";
    public static final String regexPatternNumericLength3 = "^\\d{1,3}$";
    
    public static final String externalFileTransferDataType = "A9";
    
    static{
    	fileEncapsulations = new ArrayList<NameValuePair>();
    	fileEncapsulations.add(new NameValuePair("None (Raw File)", "00"));
    	fileEncapsulations.add(new NameValuePair("Compressed using GZIP", "01"));
    	fileEncapsulations.add(new NameValuePair("Compressed using GZIP, then encrypted using AES 128 with CBC", "02"));
    	fileEncapsulations.add(new NameValuePair("Encrypted using AES 128 with CBC", "03"));
    	fileEncapsulations.add(new NameValuePair("Archived using TAR, then compressed using GZIP", "04"));
    	fileEncapsulations.add(new NameValuePair("Encrypted using AES 128 without CBC", "05"));
    	
    	crcTypes = new ArrayList<NameValuePair>();
    	crcTypes.add(new NameValuePair("CRC16", "00"));
    	crcTypes.add(new NameValuePair("CRC32", "01"));
    	
    	protocols = new ArrayList<NameValuePair>();
    	protocols.add(new NameValuePair("FTP (Active Mode)", "F"));
    	protocols.add(new NameValuePair("FTP (Passive Mode)", "P"));
    	protocols.add(new NameValuePair("HTTP", "H"));
    	protocols.add(new NameValuePair("HTTPS", "S"));
    	
    	fileFormats = new ArrayList<NameValuePair>();
    	fileFormats.add(new NameValuePair("G5 - FMM Binary Image", "00"));
    	fileFormats.add(new NameValuePair("Kiosk - Configuration File", "01"));
    	fileFormats.add(new NameValuePair("Kiosk - Application Upgrade", "05"));
    	fileFormats.add(new NameValuePair("Kiosk - Executable File", "07"));
    	fileFormats.add(new NameValuePair("Kiosk - Executable File with Restart", "08"));
    	fileFormats.add(new NameValuePair("Kiosk - Log File", "09"));
    	fileFormats.add(new NameValuePair("Kiosk - Generic File", "10"));
    }
    
    /**
     * Loops through and grabs the file_transfer_name for the passed in cd.
     * @param nvpList
     * @param fileTransferTypeCd
     * @return
     */
    public static String getFileTransferTypeName(List<NameValuePair> nvpList, String fileTransferTypeCd){
    	
    	if(!StringHelper.isBlank(fileTransferTypeCd)){
    		for(NameValuePair nvp : nvpList){
    			String value = nvp.getValue();
    			if((!(StringHelper.isBlank(fileTransferTypeCd))) && value.trim().equalsIgnoreCase(fileTransferTypeCd.trim())){
    				return nvp.getName();
    			}
    		}
    	}
    	return "";
    }
    
    /**
     * Retrieve the profile title for the given deviceId, the profile title includes: <ul>
     * <li>customer_name</li> <li>location_name</li> </ul>
     * 
     * @param deviceId
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static String getConfigurablePropertyListIds(int deviceTypeId, int propertyListVersion) throws SQLException, DataLayerException
    {
        Results result = DataLayerMgr.executeQuery("GET_CONFIGURABLE_PROPERTY_LIST_IDS", new Integer[] {deviceTypeId, propertyListVersion}, false);
        String retStr = "";
        if (result.next())
        {
        	return result.getFormattedValue("config_prop_list_ids");
        }
        return retStr;
    }
    
    /**
     * Retrieve the profile title for the given deviceId, the profile title includes: <ul>
     * <li>customer_name</li> <li>location_name</li> </ul>
     * 
     * @param deviceId
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static Results getFileTransferHistory(String deviceName, int fileTransferTypeCd, int topRows) throws SQLException, DataLayerException
    {
    	if(topRows<0)
    		topRows = 5;
		return DataLayerMgr.executeQuery("GET_FILE_TRANSFER_HISTORY", new Object[] { deviceName, fileTransferTypeCd, fileTransferTypeCd, fileTransferTypeCd, topRows }, false);
    }
    
    public static void streamFileTransfer(ServletOutputStream out, long fileId, Connection conn) throws ServletException {
    	if (fileId < 1)
    		throw new ServletException("Invalid fileId: " + fileId);
    	
    	String sql = "SELECT file_transfer_content FROM device.file_transfer WHERE file_transfer_id = ?";
    	boolean newConnection = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        InputStream in = null;
        
        boolean success = false;
        try
        {
        	if (conn == null) {
        		conn = DataLayerMgr.getConnection("OPER");
        		newConnection = true;
        	}
            pstmt = conn.prepareCall(sql);
            pstmt.setLong(1, fileId);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
            	in = rs.getBinaryStream(1); 
            	int bytesRead; 
            	byte[] bytes = new byte[1024];
            	while ((bytesRead = in.read(bytes)) > 0) {
            		out.write(bytes, 0, bytesRead);
            		out.flush();
            	}
            }
            success = true;
        } catch(Exception e) {
        	throw new ServletException("Error retrieving file content for fileId: " + fileId, e);
        }
        finally
        {
        	if (in != null) {
        		try {
                    in.close();
                } catch (IOException e) {
                    log.warn(">>> Failed to close database input stream...", e);
                }
        	}
        	
            // close the result set
            if (rs != null)
            {
                try
                {
                    rs.close();
                }
                catch (SQLException se)
                {
                    log.warn(">>> Failed to close result set...", se);
                }
            }
            // close the prepared statement
            if (pstmt != null)
            {
                try
                {
                    pstmt.close();
                }
                catch (SQLException se)
                {
                    log.warn(">>> Failed to close prepared statement...", se);
                }
            }
            // close the database connection
            if (newConnection)
            {
            	if (!success)
        			ProcessingUtils.rollbackDbConnection(log, conn);    		
        		ProcessingUtils.closeDbConnection(log, conn);
            }
        }
    }
 	
	/**
     * Retrieve the raw file content for fileId passed in.
     * 
     * @param fileId
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static FileTransfer getFileTransfer(long fileId, String fileName, int numBytes, Connection conn) throws ServletException
    {
    	if (fileId < 1 && StringHelper.isBlank(fileName))
    		throw new ServletException("Invalid fileId: " + fileId + " and fileName: " + fileName);
    	
        FileTransfer fileTransfer = new FileTransfer();
    	String sql;
    	if (fileId > 0)
    		sql = "SELECT file_transfer_content, file_transfer_type_cd, file_transfer_id FROM device.file_transfer WHERE file_transfer_id = ?";
    	else
    		sql = "SELECT file_transfer_content, file_transfer_type_cd, file_transfer_id FROM (SELECT /*+INDEX(ft IDX_FILE_TRANSFER_NAME)*/ file_transfer_content, file_transfer_type_cd, file_transfer_id FROM device.file_transfer ft WHERE file_transfer_name = ? ORDER BY file_transfer_type_cd, created_ts) WHERE ROWNUM = 1";
    	boolean newConnection = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean success = false;
        try
        {
        	if (conn == null) {
        		conn = DataLayerMgr.getConnection("OPER");
        		newConnection = true;
        	}
            pstmt = conn.prepareCall(sql);
            pstmt.setLong(1, fileId);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
            	byte[] fileBlob = rs.getBytes(1);
            	if (fileBlob != null) {
            		long fileSize = fileBlob.length;
                	fileTransfer.setFileSize(fileSize);
                	if (fileSize > 0) {
	            		            		
		                	byte[] bytes;
		                	if (fileSize > numBytes)
		                		bytes = new byte[numBytes];
		                	else
		                		bytes = new byte[(int) fileSize];
		                	System.arraycopy(fileBlob, 0, bytes, 0, bytes.length);
		                	fileTransfer.setFileContent(new String(bytes));
                	}
            	}
            	fileTransfer.setFileTypeId(rs.getInt(2));
            	fileTransfer.setFileId(rs.getLong(3));
            }
            success = true;
        } catch(Exception e) {
        	throw new ServletException("Error retrieving file content for fileId: " + fileId, e);
        }
        finally
        {
            // close the result set
            if (rs != null)
            {
                try
                {
                    rs.close();
                }
                catch (SQLException se)
                {
                    log.warn(">>> Failed to close result set...", se);
                }
            }
            // close the prepared statement
            if (pstmt != null)
            {
                try
                {
                    pstmt.close();
                }
                catch (SQLException se)
                {
                    log.warn(">>> Failed to close prepared statement...", se);
                }
            }
            // close the database connection
            if (newConnection)
            {
            	if (!success)
        			ProcessingUtils.rollbackDbConnection(log, conn);    		
        		ProcessingUtils.closeDbConnection(log, conn);
            }
        }
        return fileTransfer;
    }
    
    /**
     * Retrieve the file Transfer id for fileName passed in.
     * 
     * @param fileName
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static long getFileTranferIdByFileTransferName(String fileName) throws SQLException, DataLayerException
    {
    	long result = -1;
        if (!StringHelper.isBlank(fileName))
        {
        	String sql = "SELECT file_transfer_id FROM (SELECT /*+INDEX(ft IDX_FILE_TRANSFER_NAME)*/ file_transfer_id FROM device.file_transfer ft WHERE file_transfer_name = ? ORDER BY file_transfer_type_cd, created_ts) WHERE ROWNUM = 1";
        	
            Connection conn = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            boolean success = false;
            try
            {
                conn = DataLayerMgr.getConnection("OPER");
                pstmt = conn.prepareCall(sql);
                pstmt.setString(1, fileName);
                rs = pstmt.executeQuery();
                if (rs.next())
                {
                    result = rs.getLong("file_transfer_id");
                }
                success = true;
            }
            finally
            {
                // close the result set
                if (rs != null)
                {
                    try
                    {

                        rs.close();
                    }
                    catch (SQLException se)
                    {
                        log.warn(">>> Failed to close result set...", se);
                    }
                }
                // close the prepared statement
                if (pstmt != null)
                {
                    try
                    {

                        pstmt.close();
                    }
                    catch (SQLException se)
                    {
                        log.warn(">>> Failed to close prepared statement...", se);
                    }
                }
                // close the database connection
                if (!success)
        			ProcessingUtils.rollbackDbConnection(log, conn);    		
        		ProcessingUtils.closeDbConnection(log, conn);
            }

        }
        return result;
    }
    
	public static long getFileSize(Connection conn, long fileTransferId) throws ServletException{
		return DeviceUtils.getFileSize(conn, fileTransferId);
	}
	
	public static String getFileSizeFormatted(Connection conn, long fileTransferId) throws ServletException{
		return DeviceUtils.getFileSizeFormatted(conn, fileTransferId);
	}
}
