/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.InputForm;

import com.usatech.dms.location.LocationConstants;
import com.usatech.layers.common.model.Location;

/**
 * This is a utility class for Location.
 */
public class LocationActions
{
	
	/**
	 * Can load location by passing in id or name.
     * Try to load the stored location from http session by the given Id or name, if the stored location
     * not found or the passed in param is not matching the given Id or name, will instantiate a new Location and
     * load the information for it by the given Id.
     * 
     * @param Id
     * @param session
     * @param showDisabled the parameter transfered from "location list". <not in use at this time>
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static Location loadLocation(Object locationIdentifier) throws ServletException, SQLException, DataLayerException, ConvertException
    {
        Location location = loadLocationData(locationIdentifier);    	
    	if(location == null){
        	location = new Location();
        	location.setName(null);
        	location.setId(null);
        	location.setCountryCode("US");
        	location.setStateCode("AL");
        	location.setTypeId(10);
        	location.setTypeDesc("USA Technologies School");
    	}
        return location;
    }
    
   /**
    * Saves new location.
    * @param form
    * @return
    * @throws SQLException
    * @throws DataLayerException
    * @throws ConvertException
    */
    public static Long saveNewLocation(InputForm form) throws ServletException, SQLException, DataLayerException, ConvertException
    {
    	long locationId = -1;
    	Location l = null;        
        
        try{
        	l = new Location();
        	BeanUtils.populate(l, form.getParameters());
        	if("Undefined".equalsIgnoreCase(l.getCountryCode())){
        		l.setCountryCode(null);
        	}
        	if("Undefined".equalsIgnoreCase(l.getStateCode())){
        		l.setStateCode(null);
        	}
        	if((form.get("parentId")==null) || ("Undefined".equalsIgnoreCase((String)form.get("parentId")))){
        		l.setParentId(null);
        	}
        	DataLayerMgr.executeUpdate("INSERT_NEW_LOCATION", l, true);
            locationId = ConvertUtils.getLong(l.getId());
        }catch(Exception e){
        	throw new ServletException("LocationActions::updateLocation: exception occurred: "+e.getMessage());
        }
     
        return new Long(locationId);
    }
    
    /**
     * 
     * @param form
     * @param location
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static void storeClientValues(InputForm form, Location location) throws ServletException, SQLException, DataLayerException, ConvertException
    {
    	try{
        	BeanUtils.populate(location, form.getParameters());
        }catch(Exception e){
        	throw new ServletException("LocationActions::updateLocation: exception occurred: "+e.getMessage());
        }
        
    }
    
    /**
     * Updates location data.
     * @param form
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     * @throws ServletException
     */
    public static void updateLocation(InputForm form) throws SQLException, DataLayerException, ConvertException, ServletException
    {
    	Location l = new Location();
        try{
        	BeanUtils.populate(l, form.getParameters());
        	if (l.getId() == null)
        		l.setId(form.getLong("location_id", false, -1));
        	if("Undefined".equalsIgnoreCase(l.getCountryCode())){
        		l.setCountryCode(null);
        	}
        	if("Undefined".equalsIgnoreCase(l.getStateCode())){
        		l.setStateCode(null);
        	}
        	if((form.get("parentId")==null) || ("Undefined".equalsIgnoreCase((String)form.get("parentId")))){
        		l.setParentId(null);
        	}
        	DataLayerMgr.executeUpdate("UPDATE_LOCATION", l, true);
        }catch(Exception e){
        	throw new ServletException("LocationActions::updateLocation: exception occurred: "+e.getMessage());
        }
    }
    
    /**
     * Effectively Deletes the location by setting location_active_yn_flag = 'N'.
     * @param form
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    public static void deleteLocation(InputForm form) throws SQLException, DataLayerException, ConvertException
    {
    	Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("location_id", ConvertUtils.getLong(form.get("location_id")));
        DataLayerMgr.executeUpdate("UPDATE_DELETE_LOCATION", paramMap, true);
        
    }
    
    public static void undeleteLocation(InputForm form) throws SQLException, DataLayerException, ConvertException
    {
    	Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("location_id", ConvertUtils.getLong(form.get("location_id")));
        DataLayerMgr.executeUpdate("UPDATE_UNDELETE_LOCATION", paramMap, true);
        
    }

    
    /**
     * Loads location data either by name or id.
     * @param locationIdentifier
     * @return
     * @throws ServletException
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
    private static Location loadLocationData(Object locationIdentifier) throws ServletException, SQLException, DataLayerException, ConvertException
    {
    	Location location = new Location();
    	Results result = null;
		if(locationIdentifier != null){
	       if(locationIdentifier instanceof String){
	    	   result = DataLayerMgr.executeQuery("GET_LOCATION_BY_LOCATION_NAME", new String[] {(String)locationIdentifier}, false);
	       }else{
	       	   result = DataLayerMgr.executeQuery("GET_LOCATION_BY_LOCATION_ID", new Long[] {(Long)locationIdentifier}, false);
	       }
	       	   
       	   try{
       		   if(result!= null && result.next()){
       			   result.fillBean(location);
       		   }
       	   
       	   }catch(BeanException e){
       		   throw new ServletException("LocationActions::generateLocation: exception occurred: "+e.getMessage());
       	   }
	       
	    }
        
        return location;
        
    }
	
    /**
     * Gets the number of times this location name exists in the DB.
     * @param sqlParams
     * @param sqlQuery
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
	public static String getLocationNameCount(Object[] sqlParams, String sqlQuery) throws SQLException, DataLayerException, ConvertException
    {
    	Results result = null;
    	Integer retCount = new Integer(0);
    	String exception = null;
    	String message = null;
    	try{
    		result = DataLayerMgr.executeQuery(sqlQuery, sqlParams, false);
    		while(result.next()){
    			retCount = ConvertUtils.getInt(result.getFormattedValue("location_name_count"));
    		}
    	}catch(SQLException sqle){
    		exception = ": Error Message=" + sqle.getMessage() + " :SQLState=" + sqle.getSQLState();
    		
    	}finally{
    		if(retCount.intValue() > 0 ){
    			if(exception == null)
    				exception = "";
    			message = ("Location Name " + sqlParams[0] + " already exists for parent location, please choose a different location name!" + exception);
    		}
    		
    	}
        
		return message;
    }
	
	/**
	 * Counts all locations.
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 */
	public static Integer getAllLocationCount() throws SQLException, DataLayerException, ConvertException
    {
    	Results result = null;
    	Integer retCount = new Integer(0);
    	result = DataLayerMgr.executeQuery("GET_COUNT_ALL_LOCATIONS", null, false);
		while(result.next()){
			retCount = ConvertUtils.getInt(result.getFormattedValue("location_count"));
		}
    	
		return retCount;
    }
	
	/**
	 * Get Device count by location.
	 * @param locationIdentifier
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 */
	public static Integer getDeviceCountByLocation(Object locationIdentifier) throws SQLException, DataLayerException, ConvertException
    {
    	Results result = null;
    	Integer retCount = new Integer(0);
		result = DataLayerMgr.executeQuery("GET_DEVICE_COUNT_BY_LOCATION", new Object[] {locationIdentifier}, false);
		while(result.next()){
			retCount = ConvertUtils.getInt(result.getFormattedValue("device_count"));
		}
        
        return retCount;
    }
	
	/**
	 * Gets the count of how many times a state name is in the db.
	 * @param params
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 */
	public static Integer getCountState(Object[] params) throws SQLException, DataLayerException, ConvertException
    {
    	Results result = null;
    	Integer retCount = new Integer(0);
		result = DataLayerMgr.executeQuery("GET_COUNT_STATE_BY_COUNTRY", params, false);
		while(result.next()){
			retCount = ConvertUtils.getInt(result.getFormattedValue("state_count"));
		}
        
        return retCount;
    }
	
	/**
	 * Gets the count of how many times a state name is in the db.
	 * @param params
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 */
	public static boolean validStatePerCountry(Object[] params) throws SQLException, DataLayerException, ConvertException
    {
    	Results result = null;
    	String stateCd;
    	boolean returnBool = false;
		result = DataLayerMgr.executeQuery("GET_STATE_BY_COUNTRY_CD_STATE_CD", params, false);
		while(result.next()){
			stateCd = (String)ConvertUtils.getString(((String)result.getFormattedValue("aValue")), false);
			if(!com.usatech.layers.common.util.StringHelper.isBlank(stateCd)){
				returnBool = true;
			}
		}
        
        return returnBool;
    }
	

}
