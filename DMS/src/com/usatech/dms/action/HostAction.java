/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.text.StringUtils;

public class HostAction
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    public static Results getHosts(long device_id) throws SQLException, DataLayerException
    {
        String sql = "select host.host_id,"
            + "host.host_status_cd," 
            + "host.host_active_yn_flag,"
            + "host.host_port_num," 
            + "host.host_position_num," 
            + "host.host_serial_cd," 
            + "host.host_label_cd," 
            + "host.host_type_id," 
            + "host.host_equipment_id," 
            + "host_type.host_type_desc," 
            + "host_equipment.host_equipment_mfgr," 
            + "host_equipment.host_equipment_model," 
            + "device_type_host_type.device_type_host_type_cd " 
            + "from device.device_type, device.device, device.host, device.host_type, device.host_equipment, device.device_type_host_type " 
            + "where device.device_id = host.device_id " 
            + "and host_type.host_type_id = device_type_host_type.host_type_id " 
            + "and device_type.device_type_id = device_type_host_type.device_type_id " 
            + "and device.device_type_id = device_type.device_type_id " 
            + "and host.host_type_id = host_type.host_type_id " 
            + "and host.host_equipment_id = host_equipment.host_equipment_id " 
            + "and device.device_id = ? ";
        Results hostInfo = DataLayerMgr.executeSQL("OPER", sql, new Object[]{device_id}, null);
        return hostInfo;
    }

    public static int createDefaultHostsIfNeeded(long device_id) throws SQLException, DataLayerException
    {
        // get a list of this device's hosts
        Results hosts = null;
        try
        {
            hosts = getHosts(device_id);
        }
        catch (Exception e)
        {
            return 0;
        }

        // now search the hosts to see what type are assigned
        boolean has_base_host = false;
        boolean has_other_host = false;

        while (hosts.next() && !has_base_host && !has_other_host)
        {
            if (!has_base_host && hosts.getFormattedValue(13).equalsIgnoreCase("B"))
                has_base_host = true;
            if (!has_other_host && !hosts.getFormattedValue(13).equalsIgnoreCase("B"))
                has_other_host = true;
        }

        // if exists both a base and another host, then there's nothing to do here
        if (has_base_host && has_other_host)
        {
            return 0;
        }

        String sql = "select device.device_name," 
                + "device.device_serial_cd," 
                + "device_type.device_type_id," 
                + "device_type.def_base_host_type_id," 
                + "device_type.def_base_host_equipment_id,"
                + "device_type.def_prim_host_type_id," 
                + "device_type.def_prim_host_equipment_id " 
                + "from device.device_type, device.device " 
                + "where device.device_type_id = device_type.device_type_id "
                + "and device.device_id = ? ";

        Results deviceInfo = DataLayerMgr.executeSQL("OPER", sql, new Object[]{device_id}, null);
        if (deviceInfo == null || !deviceInfo.next())
        {
            return 0;
        }
        
        int insert_count = 0;
        Object[] params = null;
        // Create base host if it doesn't exist
        String typeId = null;
        typeId = deviceInfo.getFormattedValue(4);
        if (typeId != null && typeId.length() > 0 && Integer.parseInt(typeId) > 0 && !has_base_host)
        {
            params = new Object[] {deviceInfo.getFormattedValue(2), 0, device_id, deviceInfo.getFormattedValue(4), deviceInfo.getFormattedValue(5)};

            try {
	            int count = DataLayerMgr.executeUpdate("INSERT_HOST", params, true);
	            if (count == 0)
	            {
	                return 0;
	            }
	            insert_count++;
            } catch (SQLIntegrityConstraintViolationException e) {
            	log.warn("Error while inserting a host with host_port_num 0, device_id " + device_id + ": " + e.getMessage());
            }
        }
        // Create primary host if no other hosts exist

        typeId = deviceInfo.getFormattedValue(6);
        if (typeId != null && typeId.length() > 0 && Integer.parseInt(typeId) > 0 && !has_other_host)
        {
            params = new Object[] {"", 1, device_id, deviceInfo.getFormattedValue(6), deviceInfo.getFormattedValue(7)};
            
            try {
	            int count = DataLayerMgr.executeUpdate("INSERT_HOST", params, true);
	            if (count == 0)
	            {
	                return 0;
	            }
	            insert_count++;
            } catch (SQLIntegrityConstraintViolationException e) {
            	log.warn("Error while inserting a host with host_port_num 1, device_id " + device_id + ": " + e.getMessage());
            }
        }

        return insert_count;
    }

    public static String getMerchantDesc(String table_name, String key_name, Integer key_id)
    {
        Object[] params = null;
        String merchant_desc = null;
        try
        {
            if (table_name != null && table_name.equalsIgnoreCase("TERMINAL"))
            {
                params = new Object[] {key_id};
                Results merchantInfo = DataLayerMgr.executeQuery("GET_MERCHANT_BY_TERMINAL", params, false);
                if (merchantInfo.next())
                {
                    merchant_desc = "<a href=\"merchantinfo.i?merchant_id=" + merchantInfo.getFormattedValue(1) + "\">" + merchantInfo.getFormattedValue(2) + "</a>";
                }
            }
            else
            {
                params = new Object[] {key_name, key_id};
                Results authName = DataLayerMgr.executeQuery("GET_AUTH_NAME", params, false);
                if (authName.next())
                {
                    merchant_desc = authName.getFormattedValue(1);
                }
            }
        }
        catch (Exception e)
        {
            log.error(StringUtils.exceptionToString(e));
            return null;
        }
        return merchant_desc;
    }
}
