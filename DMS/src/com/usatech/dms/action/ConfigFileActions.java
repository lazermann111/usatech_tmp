/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.usatech.dms.util.DMSSecurityUtil;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.util.NameValuePair;

/**
 * Provides the methods to operate the configuration files.
 */
public class ConfigFileActions
{
    private static final simple.io.Log log = simple.io.Log.getLog();        

    /** Don't let anyone instantiate this class. */
    private ConfigFileActions()
    {}

    /**
     * Gets the selection items of configuration file for EDGE devices.
     * 
     * @param evNumber
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static List<NameValuePair> getConfigTemplates(int deviceTypeId, long deviceId) throws ServletException
    {
    	try {
	        List<NameValuePair> items = new ArrayList<NameValuePair>();
	        Map<String, Object> params = new HashMap<String, Object>();
			params.put("device_type_id", deviceTypeId);
			params.put("device_id", deviceId);
	        Results result = DataLayerMgr.executeQuery("GET_CONFIG_TEMPLATES", params, false);
	        while (result.next())
	            items.add(new NameValuePair(result.getFormattedValue("aLabel"), result.getFormattedValue("aValue")));
	        return items;
    	} catch (Exception e) {
    		throw new ServletException("Error loading configuration templates", e);
    	}
    }    
    
    /**
     * Gets the file transfer type cd for the file name.
     * 
     * @param evNumber
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static int getFileTransferTypeByFileName(String fileName) throws SQLException, DataLayerException
    {
        int returnType = -1;
        if (!StringHelper.isBlank(fileName))
        {
            Results result = DataLayerMgr.executeQuery("GET_FILE_TRANSFER_TYPE_BY_FILE_NAME", new Object[] {fileName}, false);
            if (result.next())
            {
            	returnType = ConvertUtils.getIntSafely(result.getValue("file_transfer_type_cd"), -1);
            }
        }
        return returnType;
    }    
    
    public static LinkedHashMap<String, String> getDeviceSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long deviceId, int deviceTypeId, boolean includeServerOnlySettings, Connection conn) throws ServletException
    {
    	return DeviceUtils.getDeviceSettingData(defaultConfData, deviceId, deviceTypeId, includeServerOnlySettings, conn);
    }
    
    public static LinkedHashMap<String, String> getBackupDeviceSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long deviceId, int deviceTypeId, boolean includeServerOnlySettings, Connection conn) throws ServletException
    {
    	return DeviceUtils.getBackupDeviceSettingData(defaultConfData, deviceId, deviceTypeId, includeServerOnlySettings, conn);
    }
    
    public static LinkedHashMap<String, String> getConfigTemplateSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long templateId, int deviceTypeId, boolean includeServerOnlySettings, Connection conn) throws ServletException
    {
    	return DeviceUtils.getConfigTemplateSettingData(defaultConfData, templateId, deviceTypeId, includeServerOnlySettings, conn);
    }
    
    public static void decryptDeviceSettingValue(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, LinkedHashMap<String, String> deviceSettingData, String key) throws Exception {
    	if (!deviceSettingData.containsKey(key))
    		return;
    	try {
	    	String param_value = deviceSettingData.get(key);
	    	if(StringHelper.isBlank(param_value)){
				ConfigTemplateSetting configTemplateSetting = defaultConfData.get(key);
				if (configTemplateSetting != null)
					param_value = configTemplateSetting.getConfigTemplateSettingValue();
			} else
				param_value = DMSSecurityUtil.rijndaelDecryptHex(param_value.trim()).trim();
	    	deviceSettingData.put(key, param_value);
    	} catch (Exception e) {
    		deviceSettingData.put(key, "");
    		log.warn("Error decrypting device setting " + key, e);
    	}
    }
	
	/**
	 * Gets the property list version of the custom config template.
	 * @param deviceTypeId
	 * @param templateFileName
	 * @param propertyListVerson
	 * @return
	 * @throws ServletException
	 * @throws NumberFormatException
	 */
	public static int getPropertyListVersionForConfigTemplateCustom(int deviceTypeId, String templateName) {
		int propertyListVersion = -1;
		if(deviceTypeId == DeviceType.EDGE.getValue()){
			int lastDash = templateName.lastIndexOf("-");
			try{
				String propListTemp = templateName.substring(lastDash + 1, templateName.length());
				propertyListVersion = Integer.valueOf(propListTemp);
			}catch(Exception e){
				propertyListVersion = -1;
			}
			if (propertyListVersion < 0) {
				try {
					propertyListVersion = DeviceUtils.getMaxPropertyListVersion(null);
				} catch (Exception e) {
					
				}
			}
		}
		return propertyListVersion;
	}
	
	//use this class to sort the string keys, when numeric, as Integers
	public static class KeySorter implements Comparator<Object> {
        public int compare(Object o1, Object o2) {
        	if (o1 == null && o2 == null)
        		return 0;
        	else if (o1 == null && o2 != null)
        		return -1;
        	else if (o1 != null && o2 == null)
        		return 1;
        	try{
              return new Integer((String) o1).compareTo(new Integer((String) o2));
        	}catch(Exception e){
        		return ((String) o1).compareTo(((String) o2));
        	}
        }
	}

    /**
     * Gets the selection items of pos_pta_template.
     * 
     * @return
     * @throws SQLException
     * @throws DataLayerException
     */
    public static List<NameValuePair> getPosPtaTemplateSelections() throws SQLException, DataLayerException
    {
        List<NameValuePair> items = new ArrayList<NameValuePair>();
        Results result = DataLayerMgr.executeQuery("GET_POS_PTA_TEMPLATE_SELECTIONS", null, false);
        while (result.next())
        {
            items.add(new NameValuePair(result.getFormattedValue("aLabel"), result.getFormattedValue("aValue")));
        }
        return items;
    }
    
    /**
     * Save the device property list file for the specified file name and hex data.
     * 
     * @param fileName
     * @param hexData
     * @return <code>null</code> if saved successfully.
     */
    public static long saveFile(String fileName, String fileContent, long fileTransferId, int fileTransferTypeCd, Connection conn) throws ServletException
    {
    	return DeviceUtils.saveFile(fileName, fileContent, fileTransferId, fileTransferTypeCd, conn);
    }

    public static void updateFileData(long fileId, String fileName, String fileContent, Connection conn) throws DataLayerException, SQLException
    {
        DeviceUtils.updateFileData(fileId, fileName, fileContent, conn);
    }    

    /**
     * Backup the configuration for the device by the given evNumber.
     * 
     * @param evNumber
     * @param cfgFileData
     * @return
     * @throws SQLException
     * @throws DataLayerException
     */
    public static void backupConfiguration(long deviceId) throws ServletException
    {
    	try {
	        DataLayerMgr.executeCall("BACK_UP_DEVICE_SETTINGS", new Object[] {deviceId}, true);
        } catch (Exception e) {
        	throw new ServletException(e);
        }
    }
    
    public static void scheduleUpgrade(long deviceId, long firmwareUpgradeId) throws ServletException
    {
    	try {
	        DataLayerMgr.executeCall("SCHEDULE_DEVICE_FIRMWARE_UPGRADE", new Object[] {deviceId, firmwareUpgradeId}, true);
        } catch (Exception e) {
        	throw new ServletException(e);
        }
    }

    /**
     * Parse the config data and return a map, for fixed width files only.
     * 
     * @param hexData
     * @return
     */
    public static Map<String, String> parseCfgFileData(String hexData)
    {
        Map<String, String> map = new HashMap<String, String>();
        if (!StringHelper.isBlank(hexData))
        {
            String asciiData = StringHelper.decodeHexString(hexData);
            String[] lines = asciiData.split("\\n", -1);
            int pos;
            String name;
            String value;
            for (String line : lines)
            {
                pos = line.indexOf('=');
                name = line.substring(0, pos).trim();
                value = line.substring(pos + 1).trim();
                map.put(name, value);
            }
        }
        return map;
    }

    /**
     * Parse the config data and return a map, for KIOSK, T2 and EDGE only
     * @param iniFileData
     * @return
     */
    public static LinkedHashMap<String, String> parseIniFileData(String iniFileData)
    {
    	LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        if (!StringHelper.isBlank(iniFileData))
        {
            String[] lines = iniFileData.split("\\n", -1);
            int pos;
            String name;
            String value;
            for (String line : lines)
            {
                pos = line.indexOf('=');
                if(pos<=0)
                	continue;
                name = line.substring(0, pos).trim();
                value = line.substring(pos + 1).trim();
                
                map.put(name, value);
            }
        }
        return map;
    }
    
    public static String getIniFileFromDeviceSettingData(LinkedHashMap<String, String> deviceSettingData) {
    	StringBuilder sb = new StringBuilder();
    	for (Map.Entry<String, String> entry : deviceSettingData.entrySet())
    		sb.append(entry.getKey()).append('=').append(entry.getValue()).append("\n");
    	return sb.toString();
    }
    
    public static String getMapFileFromDeviceSettingData(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, LinkedHashMap<String, String> deviceSettingData, boolean returnHex) {
    	StringBuilder sb = new StringBuilder();
    	for (Map.Entry<String, String> entry : deviceSettingData.entrySet()) {
    		ConfigTemplateSetting defaultSetting = defaultConfData.get(entry.getKey());
    		if (defaultSetting != null && !defaultSetting.isServerOnly())
    			sb.append(entry.getValue());
    	}
    	String fileHex = sb.toString().toUpperCase();
    	if (returnHex)
    		return fileHex;
    	else
    		return StringHelper.decodeHexString(fileHex);
    }
    
    public static String getDeviceIniFile(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long deviceId, int deviceType, Connection conn) throws ServletException {
    	LinkedHashMap<String, String> deviceSettingData = getDeviceSettingData(null, deviceId, deviceType, false, conn);
    	return getIniFileFromDeviceSettingData(deviceSettingData);
    }
    
    public static String getDeviceMapFile(LinkedHashMap<String, ConfigTemplateSetting> defaultConfData, long deviceId, int deviceType, boolean returnHex, Connection conn) throws ServletException {
    	LinkedHashMap<String, String> deviceSettingData = getDeviceSettingData(null, deviceId, deviceType, false, conn);
    	return getMapFileFromDeviceSettingData(defaultConfData, deviceSettingData, returnHex);
    }
	
	/**
	 * Update a Custom Template Setting.
	 * @param params
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	public static int updateDefaultTemplateSetting(Object[] params)
			throws SQLException, DataLayerException {
		return DataLayerMgr.executeUpdate("UPDATE_CONFIG_TEMPLATE_SETTING", params, true);
	}
	
	/**
	 * Update a list of new Device Parameter Settings, using the stored procedure. 
	 * @param newLocationParamList
	 * @return
	 */
	public static Object[] updateDeviceSettings(Object[] params, Connection conn) throws SQLException, DataLayerException {
		return DataLayerMgr.executeCall(conn, "UPDATE_DEVICE_SETTINGS", params);
	}
	
	/**
	 * Get from device settings if the passed in device id subtype is public pc.
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 */
	public static boolean isSubtypePublicPc(long deviceId) throws SQLException, DataLayerException, ConvertException{
    	int retCount = 0;
    	Results result = null;
    	try {
	    	result = DataLayerMgr.executeQuery("IS_SUBTYPE_PUBLIC_PC", new Object[] {deviceId}, false);
			if(result.next())
				retCount = ConvertUtils.getIntSafely(result.getFormattedValue("count"), 0);
	        return retCount > 0; 
    	} finally {
    		if (result != null)
    			result.close();
    	}
	}
	
	/**
	 * Delete device settings per the passed in device_id.
	 * @param deviceId
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 */
	public static void deleteDeviceSettings(long deviceId, Connection conn) throws SQLException, DataLayerException {
		if(conn==null)
			DataLayerMgr.executeQuery("DELETE_DEVICE_SETTINGS", new Object[] {deviceId}, true);
		else
			DataLayerMgr.executeQuery(conn, "DELETE_DEVICE_SETTINGS", new Object[] {deviceId});
	}
	
	/**
	 * Get the file transfer id for a device.
	 * @param deviceId
	 * @param deviceName
	 * @param cmd_file_transfer_start
	 * @param fileTransferId
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 */
	public static long getFileTransferId(String fileName, int fileTransferTypeCd, Connection conn) throws SQLException, DataLayerException, ConvertException {
		return DeviceUtils.getFileTransferId(fileName, fileTransferTypeCd, conn);
	}
	
	public static long getFileTransferId(String fileName, Connection conn) throws SQLException, DataLayerException, ConvertException {
		long fileTransferId = -1;
		
		Results result;
		if(conn == null)
			result = DataLayerMgr.executeQuery("GET_FILE_TRANSFER_ID_BY_FILE_NAME", new Object[] {fileName}, false);
		else
			result = DataLayerMgr.executeQuery(conn, "GET_FILE_TRANSFER_ID_BY_FILE_NAME", new Object[] {fileName});
		if(result.next())
			fileTransferId = result.getValue("fileTransferId", long.class);
		
		return fileTransferId;
	}
	
	public static Long getFileTransferTypeSizeLimit(int fileTypeId, Connection conn) throws SQLException, DataLayerException {
   	Results result = DataLayerMgr.executeQuery(conn, "GET_SIZE_LIMIT_FOR_FILE_TYPE", new Object[] {fileTypeId});
   	Long ret = null;
		if(result.next()) {
			try {
				ret = ConvertUtils.getLong(result.getFormattedValue("size_limit"));
			} catch (ConvertException e) {
				log.warn(String.format("Could not convert %s to long", result.getFormattedValue("size_limit")), e);
			}
		}
		return ret;
	}
	
	public static boolean configTemplateNameExists(String templateName) throws ServletException{
		int retCount = 0;
		Object [] param = new Object[] {templateName};
		try{
			DataLayerMgr.executeCall("RENAME_CONFIG_TEMPLATE_IF_DEACTIVATED", param, true);
			Results result = DataLayerMgr.executeQuery("CHECK_CONFIG_TEMPLATE_NAME_EXISTS", param, false);
			if(result.next())
				retCount = result.getValue("count", int.class);
		}catch(Exception e){
			throw new ServletException("Error checking if template already exists", e);
		}
		return retCount > 0;
	}
	
	/**
	 * Checks if any custom template exists.
	 * @param deviceId
	 * @param fileTransferId
	 * @param deviceName
	 * @param cmdFileTransferStart
	 * @return
	 * @throws ServletException
	 */
	public static boolean fileTransferNameExists(String fileName) throws ServletException{
    	int retCount = 0;
    	try{
    		Results result = DataLayerMgr.executeQuery("CHECK_FILE_TRANSFER_NAME_EXISTS", new Object[] {fileName}, false);
    		if(result.next())
    			retCount = result.getValue("count", int.class);
        
		}catch(Exception e){
			throw new ServletException("Error checking if file already exists", e);
		}
		return retCount > 0;
	}
	
	public static long getNextFileTransferSequenceNum(Connection conn) throws ServletException{
		return DeviceUtils.getNextFileTransferSequenceNum(conn);
	}
	
	public static long getNextConfigTemplateSequenceNum(Connection conn) throws ServletException{
		Results result;
    	try{
    		if (conn == null)
    			result = DataLayerMgr.executeQuery("GET_NEXT_CONFIG_TEMPLATE_SEQ_ID", null, false);
    		else
    			result = DataLayerMgr.executeQuery(conn, "GET_NEXT_CONFIG_TEMPLATE_SEQ_ID", null);
    		if(result.next())
    			return result.getValue("seq_config_template_id", long.class);
    		else
    			throw new ServletException("Error getting next config template id");
		}catch(Exception e){
			throw new ServletException("Error getting next config template id", e);
		}
	}
	
	/**
	 * Gets the device_file_transfer seq num.
	 * @param deviceName
	 * @return
	 * @throws ServletException
	 */
	public static long getNextFileTransferSequenceNum() throws ServletException{
		return getNextFileTransferSequenceNum(null);
	}
	
	/**
	 * Inserts a new record into File_Transfer with only file_name and Type_code.  Returns the file_transfer_id.
	 * @param fileName
	 * @param fileTransferTypeCd
	 * @return
	 * @throws ServletException
	 */
	public static Integer insertFileTransfer(String fileName, int fileTransferTypeCd, Connection conn) throws ServletException{
		Integer returnId = null;
		Object[] result = null;
		Map<String, Object> params = null;
		params = new HashMap<String, Object>();
		params.put("file_transfer_name", fileName);
		params.put("file_transfer_type_cd", fileTransferTypeCd);
	
		try{
			if(conn == null)
				result = DataLayerMgr.executeCall("INSERT_FILE_TRANSFER", params, true);
			else
				result = DataLayerMgr.executeCall(conn, "INSERT_FILE_TRANSFER", params);
			returnId = (Integer)result[0];
		}catch(Exception e){
			throw new ServletException("Unable to insert File Transfer for " + fileName, e);
		}
		return returnId;
	}
	
	/**
	 * Inserts a new record into File_Transfer with file_transfer_id, file_name and Type_code.  Returns the file_transfer_id.
	 * @param fileTransferId
	 * @param fileName
	 * @param fileTransferTypeCd
	 * @return
	 * @throws ServletException
	 */
	public static void insertFileTransferWithId(long fileTransferId, String fileName, int fileTransferTypeCd) throws ServletException{
		DeviceUtils.insertFileTransferWithId(fileTransferId, fileName, fileTransferTypeCd, null);
	}
	
	/**
	 * Inserts a new record into File_Transfer with file_transfer_id, file_name and Type_code.  Returns the file_transfer_id.
	 * @param fileTransferId
	 * @param fileName
	 * @param fileTransferTypeCd
	 * @return
	 * @throws ServletException
	 */
	public static void insertFileTransferWithId(long fileTransferId, String fileName, int fileTransferTypeCd, Connection conn) throws ServletException{
		DeviceUtils.insertFileTransferWithId(fileTransferId, fileName, fileTransferTypeCd, conn);
	}
	
	/**
	 * Updates the file transfer name.
	 * @param fileTransferId
	 * @param newFileName
	 * @param fileTransferTypeCd
	 * @throws ServletException
	 */
	public static void updateFileTransferName(long fileTransferId, String newFileName, Connection conn) throws ServletException{
				
		Map<String, Object> params = null;
		params = new HashMap<String, Object>();
		params.put("file_transfer_name", newFileName);
		params.put("file_transfer_id", fileTransferId);
	
		try{
			if(conn==null)
				DataLayerMgr.executeCall("UPDATE_FILE_TRANSFER_NAME_BY_FILE_TRANSFER_ID", params, true);
			else
				DataLayerMgr.executeCall(conn, "UPDATE_FILE_TRANSFER_NAME_BY_FILE_TRANSFER_ID", params);
			
		}catch(Exception e){
			throw new ServletException("Unable to insert File Transfer for " + newFileName, e);
		}
	}
	
	public static void insertConfigTemplateWithId(long templateId, String templateName, int templateTypeId, int deviceTypeId, Connection conn) throws ServletException{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("config_template_id", templateId);
		params.put("config_template_name", templateName);
		params.put("config_template_type_id", templateTypeId);
		params.put("device_type_id", deviceTypeId);
	
		try{
			if(conn == null)
				DataLayerMgr.executeCall("INSERT_CONFIG_TEMPLATE_WITH_ID", params, true);
			else
				DataLayerMgr.executeCall(conn,"INSERT_CONFIG_TEMPLATE_WITH_ID", params);
			
		}catch(Exception e){
			throw new ServletException("Unable to insert config template for " + templateName, e);
		}
	}	

	public static void buildParamMap(HttpServletRequest request, InputForm form) throws ServletException{
		if (!StringHelper.isBlank((String) request.getAttribute("params_to_change")))
			return;
		String paramsToChange = form.getString("param_to_change", false);
		request.setAttribute("params_to_change", paramsToChange);
		if (StringHelper.isBlank(paramsToChange))
			request.setAttribute("params_to_change_request", "");
		else
			request.setAttribute("params_to_change_request", StringHelper.encodeRequestMap(request.getParameterMap()));
	}
}
