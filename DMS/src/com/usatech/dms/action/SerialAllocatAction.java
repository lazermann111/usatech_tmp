/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.mail.MessagingException;
import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.util.NameValuePair;

import com.usatech.dms.util.Helper;

/**
 * @author Administrator
 * 
 */
public class SerialAllocatAction
{
    private static String prefix = null;
    private static String suffix = null;
    private static int maxLen = 0;
    //private static List<Long> sequences = new ArrayList<Long>();
    public static Map sequenceMap = new ConcurrentHashMap();

    /** Don't let anyone instantiate this class. */
    private SerialAllocatAction()
    {

    }

    /**
     * Get serial format list for the given device type.
     * 
     * @param deviceTypeId
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static List<NameValuePair> getSerialFormat(int deviceTypeId) throws SQLException, DataLayerException
    {
        List<NameValuePair> serialFormat = new ArrayList<NameValuePair>();
        Results result = DataLayerMgr.executeQuery("GET_SERIAL_FORMAT", new Integer[] {deviceTypeId});
        while (result.next())
        {
            serialFormat.add(new NameValuePair(result.getFormattedValue("device_serial_format_id"), result.getFormattedValue("device_serial_format_name")));
        }
        return serialFormat;
    }

    /**
     * Get all the device types.
     * 
     * @param
     * @return
     * @throws DataLayerException
     * @throws SQLException
     */
    public static Results getDeviceTypes() throws SQLException, DataLayerException
    {
        return DataLayerMgr.executeQuery("GET_DEVICE_TYPE", null);
    }
    
    /**
     * Get the to be assigned serial numbers for the given serial format id, starting serial number,
     * number to allocate, insert type.
     * 
     * @param serialFormatId
     * @param numToStart
     * @param numToAllocate
     * @param insertType
     * @return
     */
    public static List<String> getAssigned(Integer serialFormatId, Long numToStart, Long numToAllocate, String insertType, List<Long> inSequences)
    {
        List<String> assigned = new ArrayList<String>();
        if (numToAllocate > 0)
        {
            long count = numToAllocate;
            if (numToStart > 0)
            {
                if ("S".equals(insertType))
                {
                    for (int i = 1; i < inSequences.size(); i++)
                    {
                        if ((numToStart <= inSequences.get(i - 1)) || (numToStart >= inSequences.get(i)))
                        {
                            continue;
                        }
                        long numToEnd = numToStart + count;
                        if ((numToEnd <= inSequences.get(i - 1)) || (numToEnd > inSequences.get(i)))
                        {
                            break;
                        }
                        for (long j = numToStart; j < numToEnd; j++)
                        {
                            count--;
                            assigned.add(getFormattedSerialNumber(j));
                        }
                        if (count == 0)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    long begin = numToStart;
                    for (int i = 1; i < inSequences.size(); i++)
                    {
                        long diff = inSequences.get(i) - inSequences.get(i - 1);
                        if ((diff > 1) && (begin > numToStart))
                        {
                            begin = inSequences.get(i - 1) + 1;
                        }

                        while ((inSequences.get(i - 1) < begin) && (inSequences.get(i) > begin))
                        {
                            if (count == 0)
                            {
                                break;
                            }
                            assigned.add(getFormattedSerialNumber(begin));
                            begin++;
                            count--;
                        }
                    }
                }
            }
            else
            {
                if ("S".equals(insertType))
                {
                    for (int i = 1; i < inSequences.size(); i++)
                    {
                        long numToEnd = inSequences.get(i - 1) + 1 + count;
                        if ((numToEnd <= inSequences.get(i - 1)) || (numToEnd > inSequences.get(i)))
                        {
                            continue;
                        }
                        for (long j = inSequences.get(i - 1) + 1; j < numToEnd; j++)
                        {
                            count--;
                            assigned.add(getFormattedSerialNumber(j));
                        }
                        if (count == 0)
                        {
                            break;
                        }

                    }
                }
                else
                {
                    for (int i = 1; i < inSequences.size(); i++)
                    {
                        if (count == 0)
                        {
                            break;
                        }
                        for (long j = inSequences.get(i - 1) + 1; j < inSequences.get(i); j++)
                        {
                            if (count == 0)
                            {
                                break;
                            }
                            count--;
                            assigned.add(getFormattedSerialNumber(j));
                        }
                    }
                }
            }
        }
        return assigned;
    }
    
    
    /**
     * Get all the assigned serial numbers for the given serial format.
     * 
     * @param serialFormatId
     * @return
     * @throws DataLayerException
     * @throws SQLException
     * @throws ConvertException
     */
    public static long getSequence(Integer serialFormatId) throws SQLException, DataLayerException, ConvertException
    {
        
        List<Long> returnSequences = new CopyOnWriteArrayList<Long>();
        long maxValue = 1;
        Results serialFormat = DataLayerMgr.executeQuery("GET_SERIAL_FORMAT_BY_ID", new Integer[] {serialFormatId});
        if (serialFormat.next())
        {
            prefix = serialFormat.getFormattedValue("device_serial_format_prefix");
            suffix = serialFormat.getFormattedValue("device_serial_format_suffix");
            maxLen = serialFormat.getValue("device_serial_format_length", int.class);
            maxLen -= prefix.length();
            maxLen -= suffix.length();

            for (int i = 1; i <= maxLen; i++)
            {
                maxValue *= 10;
            	//maxLenStr.append("9");
            }
            //maxValue = (new Long(maxLenStr.toString())).longValue();
        }

        Results serialCd = DataLayerMgr.executeQuery("GET_DEVICE_SERIAL_CD", new Integer[] {serialFormatId});
        if (!serialCd.next())
        {
            //sequences.add(new Long(0));
        	returnSequences.add(new Long(0));
        }
        else
        {
            serialCd.setRow(0);
            while (serialCd.next())
            {
                String serial = serialCd.getFormattedValue("device_serial_cd");
                if (prefix != null)
                {
                    serial = serial.replace(prefix, "");
                }

                returnSequences.add(Long.parseLong(serial.replaceAll("[^\\d]", "")));
                //sequences.add(Long.parseLong(serial.replaceAll("[^\\d]", "")));
               
            }
        }

        //sequences.add(maxValue);
        returnSequences.add(maxValue);
        Calendar now = Calendar.getInstance();
        long nowMillis = now.getTimeInMillis();
        if(sequenceMap==null)
        	sequenceMap = new ConcurrentHashMap();
        sequenceMap.put(nowMillis, returnSequences);
        //sequences.addAll(returnSequences);
        return nowMillis;

    }

    /**
     * Get the formatted serial number.
     * 
     * @param serialNumber
     * @return
     */
    private static String getFormattedSerialNumber(long serialNumber)
    {
        StringBuilder result = new StringBuilder();
        result.append(prefix);
        int size = String.valueOf(serialNumber).length();
        while (size < maxLen)
        {
            result.append("0");
            size++;
        }
        result.append(serialNumber);
        return result.toString();
    }

    /**
     * Get serial info for the given serial number.
     * 
     * @param serialNumber
     * @return
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Results getSerialInfoBySerialNumber(String serialNumber) throws SQLException, DataLayerException
    {
        return DataLayerMgr.executeQuery("GET_SERIAL_INFO_BY_SERIAL_NUMBER", new String[] {serialNumber});
    }

    /**
     * Get serial info for the given info id.
     * 
     * @param infoId
     * @return
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Results getSerialInfoByInfoId(long infoId) throws SQLException, DataLayerException
    {
        return DataLayerMgr.executeQuery("GET_SERIAL_INFO_BY_INFO_ID", new Long[] {infoId});
    }

    /**
     * Assign serial numbers and send email notification.
     * 
     * @param email
     * @param desc
     * @param serialFormatId
     * @param serials
     * @param insertType
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws MessagingException
     * @throws ConvertException
     */
    public static void assignSerials(String email, String desc, int serialFormatId, List<String> serials, String insertType) throws SQLException, DataLayerException, MessagingException,
            ConvertException
    {
        StringBuilder emailContent = new StringBuilder();
        StringBuilder serialString = new StringBuilder();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("device_serial_info_email", email);
        paramMap.put("device_serial_info_desc", desc);
        DataLayerMgr.executeUpdate("NEW_DEVICE_SERIAL_INFO", paramMap, true);
        long deviceSerialInfoId = ConvertUtils.getLong(paramMap.get("device_serial_info_id"));

        Object[] params = new Object[3];
        params[0] = serialFormatId;
        params[1] = deviceSerialInfoId;
        for (String serial : serials)
        {
            serialString.append(serial);
            serialString.append("\n");
            params[2] = serial;
            DataLayerMgr.executeUpdate("NEW_DEVICE_SERIAL", params, true);
        }
        String requestedInsertType = "S".equals(insertType) ? "SEQUENTIAL" : "NON sequential ( NEXT AVAILABLE )";
        emailContent.append("This is an automatic mail from Serial Number Assignment Tool.\n\n");
        emailContent.append("Your requested ");
        emailContent.append(serials.size());
        emailContent.append(" ");
        emailContent.append(requestedInsertType);
        emailContent.append(" serial numbers listed below.\n\n");
        emailContent.append(serialString.toString());
        emailContent.append("\n\n");

        Helper.sendEmail(Helper.DMS_EMAIL, email, "Serial Numbers", emailContent.toString(), null);
    }

    

    /**
     * Get serial regular expression string for the given device type id.
     * 
     * @param deviceTypeId
     * @return
     * @throws SQLException
     * @throws DataLayerException
     */
    public static String getSerialRegex(Integer deviceTypeId) throws SQLException, DataLayerException
    {
        Results result = DataLayerMgr.executeQuery("GET_SERIAL_REGEX_BY_TYPE", new Integer[] {deviceTypeId});
        if (result.next())
        {
            return result.getFormattedValue("device_type_serial_cd_regex");
        }
        return null;
    }

    /**
     * Get prefix and suffix pair for the given serial format id.
     * 
     * @param serialFormatId
     * @return
     * @throws SQLException
     * @throws DataLayerException
     */
    public static NameValuePair getPrefixSuffix(Integer serialFormatId) throws SQLException, DataLayerException
    {
        Results result = DataLayerMgr.executeQuery("GET_SERIAL_FORMAT_BY_ID", new Integer[] {serialFormatId});
        if (result.next())
        {
            return new NameValuePair(result.getFormattedValue("device_serial_format_prefix"), result.getFormattedValue("device_serial_format_suffix"));
        }
        return null;
    }
    
    /**
     * Clean out the sequenceMap if any lists have been in there for more than 10 minutes.
     * if key equals -1, then remove all older than 10 minutes.
     * If key not equals -1, then attempt to remove the key.
     */
    public static void clearSequenceMap(long key)throws ServletException{
    	try{
    	if(key==-1){
    		Set<Long> keys = sequenceMap.keySet();
        	Iterator<Long> i = keys.iterator();
        	Calendar tenMinsAgo = Calendar.getInstance();
	    	while(i.hasNext()){
	    		Long timeInMillis = (Long)i.next();
	    		tenMinsAgo.add(Calendar.MINUTE, -10);
	    		Calendar c = Calendar.getInstance();
	    		c.setTimeInMillis(timeInMillis);
	    		if(c.before(tenMinsAgo)){
	    			i.remove();
	    		}
	    	}
    	}else{
    		if(sequenceMap!=null && sequenceMap.containsKey(key)){
    			sequenceMap.remove(key);
    		}
    	}
    	}catch(Exception e){
    		throw new ServletException("Exception occured in SerialAllocationAction :: clearSequenceMap :: " + (e.getClass().getName() + " : " + e.getMessage()));
    	}
    	
    }
    
    /**
     * Based on the deviceTypes passed in, get a list of serialFormats for each.
     * @param deviceTypes
     * @return
     * @throws ConvertException
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Map<Integer, List<NameValuePair>> getSerialFormats(
			Results deviceTypes) throws ConvertException, SQLException,
			DataLayerException {
		Map<Integer, List<NameValuePair>> serialFormats = new HashMap<Integer, List<NameValuePair>>();
		while (deviceTypes.next())
		{
		    int device_type_id = deviceTypes.getValue("device_type_id", int.class);
		    serialFormats.put(device_type_id, SerialAllocatAction.getSerialFormat(device_type_id));
		}
		return serialFormats;
	}
}
