/**
 * $Id$
 *
 * Copyright 2009 USATechnologies. All rights reserved.
 *
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.util.Hashtable;
import java.util.ResourceBundle;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.servlet.ServletException;

import simple.servlet.BasicServletUser;
import simple.servlet.steps.LoginFailureException;

import com.usatech.layers.common.util.StringHelper;

/**
 * Provides the methods to authenticate via LDAP server.
 */
public class LdapLoginActions
{
    private static final simple.io.Log log = simple.io.Log.getLog();

    private static final int LDAP_TYPE_ACTIVE_DIRECTORY = 1;
    private static final int LDAP_TYPE_389_DIRECTORY_SERVER = 2;

    private static String ldapTypeString = "ACTIVE_DIRECTORY";
    private static int ldapType = LDAP_TYPE_ACTIVE_DIRECTORY;
    private static String ldapUrl;
    private static String ldapDomain = null;
    private static String ldapDNBase;

    /* First thing, load the LDAP configurations. */
    static
    {
        ResourceBundle bundle = ResourceBundle.getBundle("ldap_conf");
        try {
        	ldapTypeString = bundle.getString("ldap.type");
        } catch (Exception e) { }
        if (ldapTypeString.equalsIgnoreCase("ACTIVE_DIRECTORY")) {
        	ldapType = LDAP_TYPE_ACTIVE_DIRECTORY;
        	ldapDomain = bundle.getString("ldap.domain");
        } else if (ldapTypeString.equalsIgnoreCase("389_DIRECTORY_SERVER"))
        	ldapType = LDAP_TYPE_389_DIRECTORY_SERVER;
        ldapUrl = bundle.getString("ldap.url");        
        ldapDNBase = bundle.getString("ldap.dn.base");        
    }

    public static BasicServletUser authenticate(String username, String password) throws ServletException
    {
    	if (ldapType < 1)
    		throw new ServletException("Unsupported ldap.type configuration");
    	
        // if username/password is not found, set error message and return.
        if (StringHelper.isBlank(username) || StringHelper.isBlank(password))
        {
            throw new LoginFailureException("Please enter your username and password and try again", LoginFailureException.INVALID_USERNAME);
        }

        BasicServletUser user = new BasicServletUser();
    	user.setUserName(username);
        LdapContext ldapContext = null;

        try {
        	ldapContext = bind(username, password);
        	assignPrivileges(ldapContext, user);
        } finally {
	        try
	        {
	        	if (ldapContext != null)
	        		ldapContext.close();
	        }
	        catch (NamingException e)
	        {
	            log.warn("Failed to close authentication server connection", e);
	        }
        }

        return user;
    }

    private static LdapContext bind(String username, String password) throws ServletException
    {
        Hashtable<String, Object> env = new Hashtable<String, Object>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapUrl);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PROTOCOL, "ssl");
        java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

        switch (ldapType) {
        	case LDAP_TYPE_ACTIVE_DIRECTORY:        
        		env.put(Context.SECURITY_PRINCIPAL, ldapDomain + "\\" + username);
        		break;
        	case LDAP_TYPE_389_DIRECTORY_SERVER:        
        		env.put(Context.SECURITY_PRINCIPAL, "uid=" + username + "," + ldapDNBase);
        		break;
        }
        env.put(Context.SECURITY_CREDENTIALS, password);
        //env.put("com.sun.jndi.ldap.trace.ber", System.out);

        try
        {
            return new InitialLdapContext(env, null);
        }
        catch (AuthenticationException e)
        {
            throw new LoginFailureException("Invalid username or password", LoginFailureException.INVALID_CREDENTIALS);
        }
        catch (NamingException e)
        {
            throw new ServletException("Error establishing authentication context", e);
        }

    }

    private static void assignPrivileges(LdapContext context, BasicServletUser user) throws ServletException
    {
        try
        {
            String userName = user.getUserName();
        	// remove the domain name, and get the LDAP server UID
        	String uid;
        	if(userName.contains("@"))
        		uid = userName.substring(0, userName.indexOf("@"));
        	else
        		uid = userName;
            
            /* 1. Get the Common Name of the user. */
            String returnedAtts[] = {"CN"};
            String searchFilter = null;
            switch (ldapType) {
            	case LDAP_TYPE_ACTIVE_DIRECTORY:
            		searchFilter = "(&(objectClass=user)(sAMAccountName=" + uid + "))";
            		break;
            	case LDAP_TYPE_389_DIRECTORY_SERVER:
            		searchFilter = "(&(objectClass=inetorgperson)(uid=" + uid + "))";
            		break;
            }
            
            // Create the search controls
            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);
            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration<SearchResult> answer = context.search(ldapDNBase, searchFilter, searchCtls);
            
            // we only get the first one found.
            if (answer.hasMoreElements())
            {
                SearchResult searchResult = answer.next();
                Attributes attrs = searchResult.getAttributes();
                String fullLdapName = searchResult.getNameInNamespace();

                Attribute attr = attrs.get("CN");                
                String userFullName = (String)attr.get(0);
            	if (StringHelper.isBlank(userFullName))
            		user.setFullName(userName);
            	else
            		user.setFullName(userFullName);

	            /* 2. Get the groups of which the user is a direct member. */
                switch (ldapType) {
                	case LDAP_TYPE_ACTIVE_DIRECTORY:
                		searchFilter = "(&(objectClass=Group)(objectCategory=Group)(member=" + fullLdapName + "))";
                		break;
                	case LDAP_TYPE_389_DIRECTORY_SERVER:
                		searchFilter = "(&(objectClass=groupOfUniqueNames)(uniqueMember=" + fullLdapName + "))";
                		break;
                }
	            answer = context.search(ldapDNBase, searchFilter, searchCtls);
	
	            while (answer.hasMoreElements())
	            {
	                searchResult = answer.next();
	                attrs = searchResult.getAttributes();
	                attr = attrs.get("CN");
	                user.addPrivilege((String)attr.get(0));
	            }
            }
        }
        catch (NamingException e)
        {
            throw new ServletException("Error communicating with authentication server", e);
        }

    }
}
