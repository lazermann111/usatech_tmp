/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.action;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.usatech.dms.model.BusinessUnit;
import com.usatech.dms.model.Currency;
import com.usatech.layers.common.model.CustomerBankTerminal;
import com.usatech.dms.model.PaymentSchedule;
import com.usatech.dms.model.RiskAlert;
import com.usatech.dms.model.RiskAlertDetails;
import com.usatech.dms.model.Terminal;
import com.usatech.dms.model.TerminalChange;
import com.usatech.dms.model.TerminalEport;
import com.usatech.layers.common.model.Customer;

import simple.bean.ConvertException;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;
import simple.results.Results;

/**
 * Class provides the methods for terminal operations.
 */
public class TerminalActions
{
    public static final int ACT_CHANGE_START_DATE = 16;
    public static final int ACT_CHANGE_END_DATE = 17;
    public static final int ACT_DEACTIVATE_EPORT = 18;
    public static final int ACT_ASSIGN_EPORT = 19;
    public static final int ACT_CHANGE_FEE_EFFECTIVE_DATE = 20;
    public static final int ACT_CHANGE_FEE_END_DATE = 21;
    public static final int ACT_CHANGE_BANK_ACCT_DATE = 22;

    /** Don't let anyone instantiate this class. */
    private TerminalActions()
    {

    }

    public static List<Customer> getCustomers() throws SQLException, DataLayerException, ConvertException
    {
        List<Customer> customerList = new ArrayList<Customer>();
        Results result = DataLayerMgr.executeQuery("GET_CUSTOMERS", null);
        while (result.next())
        {
            Customer customer = new Customer();
            customer.setId(result.getValue("customer_id", Long.class));
            customer.setName(result.getValue("customer_name", String.class));
            customer.setAltName(result.getValue("customer_alt_name", String.class));
            customer.setStatus(result.getFormattedValue("status"));
            customer.setSalesTerm(result.getValue("sales_term", String.class));
            customer.setCreditTerm(result.getValue("credit_term", String.class));
            customerList.add(customer);
        }
        return customerList;
    }

    public static List<PaymentSchedule> getPaymentSchedules() throws SQLException, DataLayerException, ConvertException
    {
        List<PaymentSchedule> scheduleList = new ArrayList<PaymentSchedule>();
        Results result = DataLayerMgr.executeQuery("GET_PAYMENT_SCHEDULES", null);
        while (result.next())
        {
            PaymentSchedule schedule = new PaymentSchedule();
            schedule.setId(result.getValue("payment_schedule_id", Long.class));
            schedule.setDescription(result.getValue("description", String.class));
            schedule.setInterval(result.getFormattedValue("interval"));
            schedule.setMonths(result.getValue("months", Long.class));
            schedule.setDays(result.getValue("days", Long.class));
            schedule.setOffsetHours(result.getValue("offset_hours", Long.class));
            schedule.setSelectable("Y".equals(result.getFormattedValue("selectable")) ? true : false);
            scheduleList.add(schedule);
        }
        return scheduleList;
    }

    public static List<Currency> getCurrencies() throws SQLException, DataLayerException, ConvertException
    {
        List<Currency> currencyList = new ArrayList<Currency>();
        Results result = DataLayerMgr.executeQuery("GET_CURRENCIES", null);
        while (result.next())
        {
            Currency currency = new Currency();
            currency.setId(result.getValue("currency_id", Long.class));
            currency.setName(result.getValue("currency_name", String.class));
            currency.setCode(result.getFormattedValue("currency_code"));
            currency.setSymbol(result.getFormattedValue("currency_symbol"));
            currencyList.add(currency);
        }
        return currencyList;
    }

    public static List<BusinessUnit> getBusinessUnits() throws SQLException, DataLayerException, ConvertException
    {
        List<BusinessUnit> unitList = new ArrayList<BusinessUnit>();
        Results result = DataLayerMgr.executeQuery("GET_BUSINESS_UNITS", null);
        while (result.next())
        {
            BusinessUnit unit = new BusinessUnit();
            unit.setId(result.getValue("business_unit_id", Long.class));
            unit.setName(result.getValue("business_unit_name", String.class));
            unit.setDescription(result.getValue("business_unit_desc", String.class));
            unit.setDefaultPaymentScheduleId(result.getValue("default_payment_schedule_id", Long.class));
            unitList.add(unit);
        }
        return unitList;
    }

    public static List<Terminal> getTerminalsByCustomerId(Long customerId) throws SQLException, DataLayerException, ConvertException
    {
        List<Terminal> terminalList = new ArrayList<Terminal>();
        Results result = DataLayerMgr.executeQuery("GET_TERMINALS_BY_CUSTOMER_ID", new Long[] {customerId});
        while (result.next())
        {
            Terminal terminal = new Terminal();
            terminal.setId(result.getValue("terminal_id", Long.class));
            terminal.setTerminalNumber(result.getValue("terminal_number", String.class));
            terminalList.add(terminal);
        }
        return terminalList;
    }

    public static Terminal getTerminalDetails(Long terminalId) throws SQLException, DataLayerException, ConvertException
    {
        Terminal terminal = new Terminal();
        Results terminalResult = DataLayerMgr.executeQuery("GET_TERMINAL_DETAILS", new Long[] {terminalId});
        if (terminalResult.next())
        {
        	terminal.setId(terminalResult.getValue("terminal_id", Long.class));
        	terminal.setCustomerId(terminalResult.getValue("customer_id", Long.class));
        	terminal.setCustomerName(terminalResult.getValue("customer_name", String.class));
            terminal.setLocationName(terminalResult.getValue("location_name", String.class));
            terminal.setAssetNumber(terminalResult.getValue("asset_number", String.class));
            terminal.setCustomerTerminalNumber(terminalResult.getValue("customer_terminal_number", String.class));
            terminal.setPaymentScheduleId(terminalResult.getValue("payment_schedule_id", Long.class));
            terminal.setFeeCurrencyId(terminalResult.getValue("fee_currency_id", Long.class));
            terminal.setBusinessUnitId(terminalResult.getValue("business_unit_id", Long.class));
            terminal.setTerminalNumber(terminalResult.getValue("terminal_number", String.class));
            terminal.setSalesOrderNumber(terminalResult.getValue("sales_order_number", String.class));
            terminal.setPurchaseOrderNumber(terminalResult.getValue("purchase_order_number", String.class));
            terminal.setAvgTransAmt(terminalResult.getValue("avg_trans_amt", Double.class));
            terminal.setMaxTransAmt(terminalResult.getValue("max_trans_amt", Double.class));
            terminal.setAvgTransCnt(terminalResult.getValue("avg_trans_cnt", Double.class));
            terminal.setLastRiskCheck(terminalResult.getValue("last_risk_check", Date.class));
        }

        terminal.setTerminalEports(getTerminalEports(terminalId));
        terminal.setCustomerBankTerminals(getCustomerBankTerminals(terminalId));

        return terminal;
    }

    private static List<TerminalEport> getTerminalEports(Long terminalId) throws SQLException, DataLayerException, ConvertException
    {
        List<TerminalEport> eportList = new ArrayList<TerminalEport>();
        Results result = DataLayerMgr.executeQuery("GET_TERMINAL_EPORTS_BY_TERMINAL_ID", new Long[] {terminalId});
        while (result.next())
        {
            TerminalEport terminalEport = new TerminalEport();
            terminalEport.setId(result.getValue("terminal_eport_id", Long.class));
            terminalEport.setEportId(result.getValue("eport_id", Long.class));
            terminalEport.setEportSerialNumber(result.getValue("eport_serial_number", String.class));
            terminalEport.setStartDate(result.getValue("start_date", Date.class));
            terminalEport.setEndDate(result.getValue("end_date", Date.class));
            eportList.add(terminalEport);

        }
        return eportList;
    }

    private static List<CustomerBankTerminal> getCustomerBankTerminals(Long terminalId) throws SQLException, DataLayerException, ConvertException
    {
        List<CustomerBankTerminal> terminalList = new ArrayList<CustomerBankTerminal>();
        Results result = DataLayerMgr.executeQuery("GET_CUSTOMER_BANK_TERMINALS_BY_TERMINAL_ID", new Long[] {terminalId});
        while (result.next())
        {
            CustomerBankTerminal terminal = new CustomerBankTerminal();
            terminal.setCustomerBankId(result.getValue("customer_bank_id", Long.class));
            terminal.setBankAccountNumber(result.getValue("bank_account_number", String.class));
            terminal.setStartDate(result.getValue("start_date", Date.class));
            terminal.setEndDate(result.getValue("end_date", Date.class));
            terminalList.add(terminal);

        }
        return terminalList;
    }

    public static void updateTerminal(String assetNumber, String customerTerminalNumber, Long paymentScheduleId, Long feeCurrencyId, Long businessUnitId, BigDecimal avgTransAmt, BigDecimal maxTransAmt, BigDecimal avgTransCnt, Long terminalId) throws SQLException, DataLayerException
    {
        Object[] params = new Object[] {assetNumber, customerTerminalNumber, paymentScheduleId, feeCurrencyId, businessUnitId, avgTransAmt, maxTransAmt, avgTransCnt, terminalId};
        DataLayerMgr.executeUpdate("UPDATE_TERMINAL", params, true);
    }

    public static List<Terminal> getChangedTerminals() throws SQLException, DataLayerException, ConvertException
    {
        List<Terminal> terminals = new ArrayList<Terminal>();
        Results result = DataLayerMgr.executeQuery("GET_CHANGED_TERMINALS", null);
        while (result.next())
        {
            Terminal terminal = new Terminal();
            terminal.setId(result.getValue("terminal_id", Long.class));
            terminal.setTerminalNumber(result.getValue("terminal_number", String.class));
            terminals.add(terminal);
        }
        return terminals;
    }

    public static List<TerminalChange> getTerminalChanges(Long terminalId) throws SQLException, DataLayerException, ConvertException
    {
        List<TerminalChange> terminalChanges = new ArrayList<TerminalChange>();
        Results result = DataLayerMgr.executeQuery("GET_TERMINAL_CHANGES", new Long[] {terminalId});
        while (result.next())
        {
            TerminalChange change = new TerminalChange();
            change.setTerminalId(terminalId);
            change.setAttribute(result.getValue("attribute", String.class));
            change.setEportSerialNumber(result.getValue("eport_serial_number", String.class));
            change.setOldValue(result.getValue("old_value", String.class));
            change.setNewValue(result.getValue("new_value", String.class));
            change.setUpdateDate(result.getValue("update_date", Date.class));
            terminalChanges.add(change);
        }
        return terminalChanges;
    }

    public static Date getPreEndDate(Long eportId, Date date) throws SQLException, DataLayerException, ConvertException
    {
        Date preEndDate = null;
        Results result = DataLayerMgr.executeQuery("GET_PRE_ACTIVATION_END_DATE", new Object[] {eportId, date});
        if (result.next())
        {
            preEndDate = result.getValue("end_date", Date.class);

        }
        return preEndDate;
    }

    public static Date getPostStartDate(Long eportId, Date date) throws SQLException, DataLayerException, ConvertException
    {
        Date postStartDate = null;
        Results result = DataLayerMgr.executeQuery("GET_POST_ACTIVATION_START_DATE", new Object[] {eportId, date});
        if (result.next())
        {
            postStartDate = result.getValue("start_date", Date.class);

        }
        return postStartDate;
    }

    public static void updateTerminalEport(Long terminalId, Long eportId, Date startDate, Date endDate, Long terminalEportId, String override) throws SQLException, DataLayerException
    {
    	updateTerminalEport(terminalId, eportId, startDate, endDate, terminalEportId, override, null, null, null, null);
    }
    
    public static void updateTerminalEport(Long terminalId, Long eportId, Date startDate, Date endDate, Long terminalEportId, String override, String activateDetail, String deactivateDetail,Integer activateDetailId,Integer deactivateDetailId) throws SQLException, DataLayerException
    {
        Object[] params = new Object[] {terminalId, eportId, startDate != null ? startDate : new Date(), endDate, terminalEportId, override,activateDetail,deactivateDetail,activateDetailId,deactivateDetailId};
        DataLayerMgr.executeCall("UPDATE_TERMINAL_EPORT", params, true);
    }

    public static void reassignBankAccount(Long terminalId, Long customerBankId, Date date) throws SQLException, DataLayerException
    {
        Object[] params = new Object[] {terminalId, customerBankId, date != null ? date : new Date()};
        DataLayerMgr.executeCall("REASSIGN_BANK_ACCOUNT", params, true);
    }
    
    public static List<RiskAlert> getRiskAlerts(Long terminalId)  throws SQLException, DataLayerException, ConvertException
    {
      List<RiskAlert> list = new ArrayList<>();
      Results results = DataLayerMgr.executeQuery("GET_TERMINAL_RISK_ALERTS", new Object[] {terminalId});
      while(results.next()) {
       Long id = results.getValue("id", Long.class);
       RiskAlert alert = new RiskAlert(id);
        try {
         ReflectionUtils.populateProperties(alert, results.getValuesMap());
        } catch (Exception e) {
         throw new ConvertException("Failed to populate RiskAlert " + id + " for terminal " + terminalId, RiskAlert.class, results, e);
        }
        list.add(alert);
       }
      return list;
    }

    public static RiskAlertDetails getRiskAlert(Long riskAlertId)  throws SQLException, DataLayerException, ConvertException
    {
      RiskAlertDetails alertDetails = new RiskAlertDetails(riskAlertId);
      try {
        DataLayerMgr.selectInto("GET_RISK_ALERT", new Object[] {riskAlertId}, alertDetails);
      } catch (BeanException e) {
        throw new DataLayerException(e.getMessage(), e);
      }
      return alertDetails;
    }
    
    public static void updateRiskAlert(Long riskAlertId, String statusCd, String notes)  throws SQLException, DataLayerException, ConvertException
    {
      Object[] params = new Object[] {statusCd, notes, riskAlertId};
      DataLayerMgr.executeCall("UPDATE_RISK_ALERT", params, true);
    }
}
