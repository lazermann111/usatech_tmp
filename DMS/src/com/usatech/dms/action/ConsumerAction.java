/*
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.dms.model.Action;
import com.usatech.dms.model.Consumer;
import com.usatech.dms.model.ConsumerAccount;
import com.usatech.layers.common.model.Device;
import com.usatech.dms.model.Hold;
import com.usatech.layers.common.model.Location;
import com.usatech.dms.model.PermissionAction;
import com.usatech.dms.model.PermissionActionParam;
import com.usatech.dms.model.Transaction;

/**
 * Class provides the methods for customer operations.
 */
public class ConsumerAction
{
    public static Consumer getConsumer(String consumer_id) throws SQLException, DataLayerException, BeanException
    {
        Consumer consumer = new Consumer();
        Results result = DataLayerMgr.executeQuery("GET_CONSUMER_INFO", new Object[] {consumer_id}, false);

        if (result.next())
        {
            result.fillBean(consumer);
        }
        return consumer;
    }
    
    public static List<ConsumerAccount> getConsumerAccounts(String consumer_id) throws SQLException, DataLayerException, BeanException
    {
    	List<ConsumerAccount> accounts = new ArrayList<ConsumerAccount>();
    	ConsumerAccount account = null;
        Results result = DataLayerMgr.executeQuery("GET_ALL_ACCOUNTS_BY_CONSUMER_ID", new Object[] {consumer_id}, false);

        while (result.next())
        {
        	account = new ConsumerAccount();
            result.fillBean(account);
            accounts.add(account);
        }
        return accounts;
    }
    
    public static Results getConsumerAccountsForDownload(String consumer_acct_id) throws SQLException, DataLayerException, BeanException
    {
    
        return DataLayerMgr.executeQuery("GET_ALL_ACCOUNTS_BY_CONSUMER_ACCT_ID_FOR_DOWNLOAD", new Object[] {consumer_acct_id}, false);

    }
    
    public static Results getDeviceTypeActionCdByAcctId(String consumer_acct_id) throws SQLException, DataLayerException, BeanException
    {
    	
    	return DataLayerMgr.executeQuery("GET_DEVICE_ACTION_CD_BY_CONSUMER_ACCT_ID", new Object[] {consumer_acct_id}, false);

    }
    
    public static List<Transaction> getConsumerTransactions(String consumer_id) throws SQLException, DataLayerException, BeanException
    {
    	List<Transaction> transactions = new ArrayList<Transaction>();
    	Transaction transaction = null;
        Results result = DataLayerMgr.executeQuery("GET_ALL_TRANSACTIONS_BY_CONSUMER_ID", new Object[] {consumer_id}, false);

        if (result.next())
        {
        	transaction = new Transaction();
            result.fillBean(transaction);
            transactions.add(transaction);
        }
        return transactions;
    }
    
    public static void updateConsumer(Consumer consumer) throws SQLException, DataLayerException
    {
        DataLayerMgr.executeCall("UPDATE_CONSUMER", consumer, true);
    }

    public static ConsumerAccount getConsumerAccountByConsumerAccountId(String consumer_acct_id) throws SQLException, DataLayerException, BeanException
    {
		ConsumerAccount consumerAccount = null;
		Results result = DataLayerMgr.executeQuery("GET_CONSUMER_ACCOUNT_BY_CONSUMER_ACCT_ID", new Object[] {consumer_acct_id}, false);
		
		if (result.next())
		{
			consumerAccount = new ConsumerAccount();
		    result.fillBean(consumerAccount);
		}
		return consumerAccount;
	}
    
    public static List<Hold> getConsumerAccountHoldsByConsumerAccountId(Long consumer_acct_id) throws SQLException, DataLayerException, BeanException
    {
		List<Hold> holds = new ArrayList<Hold>();
    	Hold hold = null;
		Location location = null;
		Device device = null;
		Results result = DataLayerMgr.executeQuery("GET_ALL_HOLDS_FOR_CONSUMER_ACCT", new Object[] {consumer_acct_id}, false);
		
		while (result.next())
		{
			hold = new Hold();
		    result.fillBean(hold);
		    Results locationResult = null;
		    locationResult = DataLayerMgr.executeQuery("GET_LOCATION_BY_LOCATION_ID", new Object[] {hold.getLocationId()}, false);
		    if (locationResult.next()){
		    	location = new Location();
		    	locationResult.fillBean(location);
		    	hold.setLocation(location);
		    }
		    
		    Results deviceResult = null;
		    deviceResult = DataLayerMgr.executeQuery("GET_DEVICE_PROFILE_INFO2", new Object[] {hold.getDeviceId()}, false);
		    if (deviceResult.next()){
		    	device = new Device();
		    	deviceResult.fillBean(device);
		    	hold.setDevice(device);
		    }
		    
		    holds.add(hold);
		}
		return holds;
	}
    
    public static List<PermissionAction> getConsumerAccountPermissionActionsByConsumerAccountId(Long consumer_acct_id) throws SQLException, DataLayerException, BeanException
    {
		List<PermissionAction> permissionActions = new ArrayList<PermissionAction>();
		Results result = DataLayerMgr.executeQuery("GET_CONSUMER_ACCT_PERMISSION_ACTIONS", new Object[] {consumer_acct_id}, false);
		while (result.next())
		{			
			PermissionAction permissionAction = new PermissionAction();
		    result.fillBean(permissionAction);
		    //assign params to dependent Action
		    Action action = new Action();
		    result.fillBean(action);
		    permissionAction.setAction(action);
		    List<PermissionActionParam> permissionActionParams = new ArrayList<PermissionActionParam>();
		    Results permissionActionParamsResult = DataLayerMgr.executeQuery("GET_CONSUMER_ACCT_PERMISSION_ACTION_PARAMS", new Object[] {permissionAction.getPermissionActionId()}, false);
		    while (permissionActionParamsResult.next()){
		    	PermissionActionParam permissionActionParam = new PermissionActionParam();
		    	permissionActionParamsResult.fillBean(permissionActionParam);
		    	permissionActionParams.add(permissionActionParam);
		    }
		    permissionAction.setPermissionActionParams(permissionActionParams);
		    permissionActions.add(permissionAction);
		}
		
		return permissionActions;
	}
}
