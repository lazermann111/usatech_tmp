/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.action;

import com.usatech.layers.common.MessageResponseUtils;

/**
 * This is a utility class for some Credit Card /Debit Card validation/processing.
 */
public class CardAction
{
    /**
     * Mask credit card data
     * @param rawData
     * @return masked data
     */

    public static String maskCreditCard(String rawData)
    {
    	return MessageResponseUtils.maskCardNumber(rawData);
    }
}
