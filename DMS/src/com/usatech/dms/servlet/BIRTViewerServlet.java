package com.usatech.dms.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.birt.report.servlet.ViewerServlet;

import simple.servlet.BasicServletUser;
import simple.servlet.SimpleServlet;

public class BIRTViewerServlet extends ViewerServlet {
	private static final long serialVersionUID = 3055504548145998868L;

	protected boolean __authenticate(HttpServletRequest request, HttpServletResponse response) {
		BasicServletUser loggedOnUser = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
		return loggedOnUser != null;
	}
}
