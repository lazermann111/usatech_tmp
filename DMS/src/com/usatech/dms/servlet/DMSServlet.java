/**
 * $Id$
 *
 * Copyright 2009 USATechnologies. All rights reserved.
 *
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL.Use is subject to license terms.
 */
package com.usatech.dms.servlet;

import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.util.CaseInsensitiveComparator;

import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.ValuesListLoader;
import com.usatech.layers.common.util.WebHelper;

public class DMSServlet extends SimpleServlet
{
    private static final long serialVersionUID = 6583654836754549855L;    
    private static final simple.io.Log log = simple.io.Log.getLog();
    
    protected final Set<String> postMethodPaths = new TreeSet<String>(new CaseInsensitiveComparator<String>());
    protected static String[] additionalMaskedParameters;
    protected static String[] additionalTruncatedParameters;

    public static final String ATTRIBUTE_DEFAULT_CSS_PATH = "DEFAULT_CSS_PATH";
    public static final String ATTRIBUTE_DEFAULT_SCRIPT_PATH = "DEFAULT_SCRIPT_PATH";

    /*
     * (non-Javadoc)
     *
     * @see simple.servlet.SimpleServlet#init()
     */
    @Override
    public void init() throws ServletException
    {
        super.init();
        // load up the "device configuration template"
        //ConfigTemplateMgr.registerLoader(new ConfigTemplateLoader());
        loadValuesLists();
    }

	private void loadValuesLists() throws ServletException {
		try {
        	log.info("Loading ValueLists...");
            ValuesListLoader.createValidValuesList(getServletContext());
            log.info("ValueList loading complete.");
          } catch(Exception e){
        	log.error("Error loading ValueLists.");
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
          }
	}

    /**
     * set configure files path, search path, form attributes and user attributes
     */
    public DMSServlet()
    {
        super();
        
        if (additionalMaskedParameters != null && additionalMaskedParameters.length > 0) {
        	for (String param: additionalMaskedParameters)
        		maskedParameterNames.add(param);
        }
        
        if (additionalTruncatedParameters != null && additionalTruncatedParameters.length > 0) {
        	for (String param: additionalTruncatedParameters)
        		truncatedParameterNames.add(param);
        }
        
        WebHelper.setTruncatedParameterNames(truncatedParameterNames);
        
        restrictedParameterNames.add(ATTRIBUTE_DEFAULT_CSS_PATH);
        restrictedParameterNames.add(ATTRIBUTE_DEFAULT_SCRIPT_PATH);
        
        postMethodPaths.add("/bulkConfigWizard2.i");
        postMethodPaths.add("/bulkConfigWizard3.i");
        postMethodPaths.add("/bulkConfigWizard4.i");
        postMethodPaths.add("/bulkConfigWizard5.i");
        postMethodPaths.add("/bulkConfigWizard6.i");
        postMethodPaths.add("/bulkConfigWizard6b.i");
        postMethodPaths.add("/bulkConfigWizard6c.i");
        postMethodPaths.add("/bulkConfigWizard6d.i");
        postMethodPaths.add("/bulkConfigWizard7.i");
        postMethodPaths.add("/bulkConfigWizard8.i");
        postMethodPaths.add("/cardConfigWizard2.i");
        postMethodPaths.add("/cardConfigWizard3.i");
        postMethodPaths.add("/cardConfigWizard4.i");
        postMethodPaths.add("/cardConfigWizard5.i");
        postMethodPaths.add("/editConsumerAcctActions.i");
        postMethodPaths.add("/editConsumerAcctActions2.i");
        postMethodPaths.add("/editConsumerAcctActions3.i");
        postMethodPaths.add("/editPosptaTmpl.i");
        postMethodPaths.add("/logon.i");
        postMethodPaths.add("/newConsumerAcct1a.i");
        postMethodPaths.add("/newConsumerAcct2.i");
        postMethodPaths.add("/newConsumerAcct2b.i");
        postMethodPaths.add("/newConsumerAcct2c.i");
        postMethodPaths.add("/newConsumerAcct3.i");
        postMethodPaths.add("/newConsumerAcct4.i");
        postMethodPaths.add("/newConsumerAcct5.i");
    }
    
    public InputForm processInput(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	InputForm form = super.processInput(request, response);
    	if (postMethodPaths.contains(request.getServletPath()) && request.getMethod().equalsIgnoreCase("GET") && request.getQueryString() == null) {
    		log.info(new StringBuilder("Received a GET request for '").append(request.getServletPath()).append("' which requires POST, redirecting to the home page..."));
    		form.setRedirectUri(DMSConstants.HOME_PAGE);
    	}
    	return form;
    }

    public void notFound(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        log.warn(new StringBuilder("Resource '").append(request.getServletPath()).append("' not found, forwarding to the home page..."));
        dispatcher.forward(DMSConstants.HOME_PAGE);
    }
    
    /**
     * @see javax.servlet.GenericServlet#void ()
     */
     @Override
	public void destroy() {

       super.destroy();

   }

	public static String[] getAdditionalMaskedParameters() {
		return additionalMaskedParameters;
	}

	public static void setAdditionalMaskedParameters(String[] additionalMaskedParameters) {
		DMSServlet.additionalMaskedParameters = additionalMaskedParameters;
	}

	public static String[] getAdditionalTruncatedParameters() {
		return additionalTruncatedParameters;
	}

	public static void setAdditionalTruncatedParameters(String[] additionalTruncatedParameters) {
		DMSServlet.additionalTruncatedParameters = additionalTruncatedParameters;
	}
}
