package com.usatech.dms.servlet;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.servlet.RecordRequestFilter;
import simple.text.StringUtils;

import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.util.DMSConstants;
import com.usatech.layers.common.util.WebHelper;

public class DMSRecordRequestFilter extends RecordRequestFilter {
	private static final Log log = Log.getLog();
		
    protected void recordRequest(HttpServletRequest request, long duration, String exceptionText) {
    	try {
			CallInputs ci = new CallInputs();
			ci.setDuration(duration);
			ci.setExceptionText(exceptionText);
			updateWithRequest(ci, request);
			ci.setAppCd(DMSConstants.APP_CD);
    		String servletPath = request.getServletPath();
    		Map<String, String> parameters = ci.getParameters();
    		if (!StringUtils.isBlank(parameters.get("ev_number"))) {
    			ci.setObjectTypeCd("device");
    			ci.setObjectCd(parameters.get("ev_number"));
    		} else if (!StringUtils.isBlank(parameters.get("device_id"))) {	    			
    			String deviceName = DeviceActions.getDeviceName(ConvertUtils.getLongSafely(parameters.get("device_id"), -1));
    			if (!StringUtils.isBlank(deviceName)) {
    				ci.setObjectTypeCd("device");
    				ci.setObjectCd(deviceName);
    			}
    		} else if (!StringUtils.isBlank(parameters.get("accountId"))) {
    			ci.setObjectTypeCd("bank_account");
    			ci.setObjectCd(parameters.get("accountId"));
    		} else if (!StringUtils.isBlank(parameters.get("terminalId"))) {
    			ci.setObjectTypeCd("terminal");
    			ci.setObjectCd(parameters.get("terminalId"));
    		} else if (!StringUtils.isBlank(parameters.get("consumer_id"))) {
    			ci.setObjectTypeCd("consumer");
    			ci.setObjectCd(parameters.get("consumer_id"));
    		} else if (!StringUtils.isBlank(parameters.get("consumer_acct_id"))) {
    			ci.setObjectTypeCd("consumer_acct");
    			ci.setObjectCd(parameters.get("consumer_acct_id"));
    		} else if (!StringUtils.isBlank(parameters.get("customer_id"))) {
    			ci.setObjectTypeCd("customer");
    			ci.setObjectCd(parameters.get("customer_id"));
    		} else if (!StringUtils.isBlank(parameters.get("customerId"))) {
    			ci.setObjectTypeCd("reporting_customer");
    			ci.setObjectCd(parameters.get("customerId"));
    		} else if (!StringUtils.isBlank(parameters.get("dealerId"))) {
    			ci.setObjectTypeCd("dealer");
    			ci.setObjectCd(parameters.get("dealerId"));
    		} else if (!StringUtils.isBlank(parameters.get("eftId"))) {
    			ci.setObjectTypeCd("eft");
    			ci.setObjectCd(parameters.get("eftId"));
    		} else if (!StringUtils.isBlank(parameters.get("file_transfer_id"))) {
    			ci.setObjectTypeCd("file");
    			ci.setObjectCd(parameters.get("file_transfer_id"));
    		} else if (!StringUtils.isBlank(parameters.get("licenseId"))) {
    			ci.setObjectTypeCd("license");
    			ci.setObjectCd(parameters.get("licenseId"));
    		} else if (!StringUtils.isBlank(parameters.get("location_id"))) {
    			ci.setObjectTypeCd("location");
    			ci.setObjectCd(parameters.get("location_id"));
    		} else if (!StringUtils.isBlank(parameters.get("merchant_id"))) {
    			ci.setObjectTypeCd("authority_merchant");
    			ci.setObjectCd(parameters.get("merchant_id"));
    		} else if (!StringUtils.isBlank(parameters.get("terminal_id"))) {
    			ci.setObjectTypeCd("authority_terminal");
    			ci.setObjectCd(parameters.get("terminal_id"));
    		} else if (!StringUtils.isBlank(parameters.get("tran_id"))) {
    			ci.setObjectTypeCd("transaction");
    			ci.setObjectCd(parameters.get("tran_id"));
    		} else if (!StringUtils.isBlank(parameters.get("achId"))) {
    			ci.setObjectTypeCd("ach");
    			ci.setObjectCd(parameters.get("achId"));
    		} else if (!StringUtils.isBlank(parameters.get("riskAlertId"))) {
    			ci.setObjectTypeCd("riskAlertId");
    			ci.setObjectCd(parameters.get("riskAlertId"));
    		} else if (!StringUtils.isBlank(parameters.get("categoryId"))) {
    			ci.setObjectTypeCd("category");
    			ci.setObjectCd(parameters.get("categoryId"));
    		} else if (!StringUtils.isBlank(parameters.get("pricingTierId"))) {
    			ci.setObjectTypeCd("pricingTier");
    			ci.setObjectCd(parameters.get("pricingTierId"));
    		} else if (!StringUtils.isBlank(parameters.get("salesRepId"))) {
    			ci.setObjectTypeCd("salesRep");
    			ci.setObjectCd(parameters.get("salesRepId"));
    		} else if (!StringUtils.isBlank(parameters.get("dfrFileId"))) {
    			ci.setObjectTypeCd("dfr_file");
    			ci.setObjectCd(parameters.get("dfrFileId"));
    		}
    		if ("POST".equalsIgnoreCase(request.getMethod())) {
	    		if (!StringUtils.isBlank(parameters.get("action")))
	    			ci.setActionName(parameters.get("action"));
	    		else if (!StringUtils.isBlank(parameters.get("myaction")))
	    			ci.setActionName(parameters.get("myaction"));
	    		else if (!StringUtils.isBlank(parameters.get("eftAction")))
	    			ci.setActionName(parameters.get("eftAction"));
    			else if (servletPath.endsWith("AddLicense.i"))
    				ci.setActionName("Add License");
    			else if (servletPath.equalsIgnoreCase("/columnMappingTerminal.i"))
    				ci.setActionName("Column Mapping");
    			else if (servletPath.endsWith("editTerminalsChange.i"))
    				ci.setActionName("Edit Terminal");
    			else if (servletPath.endsWith("Insert.i"))
    				ci.setActionName("Insert");
    			else if (servletPath.endsWith("RemoveLicense.i"))
    				ci.setActionName("Remove License");
    			else if (servletPath.endsWith("Update.i"))
    				ci.setActionName("Update");
    			else if (servletPath.endsWith("updateFees.i"))
    				ci.setActionName("Update Fees");
    		}
    		WebHelper.publishAppRequestRecord(ci, null, log);
		} catch (Exception e) {
			log.error("Could not record request", e);
		}
    }	
}
