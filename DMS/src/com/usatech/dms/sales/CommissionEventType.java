package com.usatech.dms.sales;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import simple.app.ServiceException;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class CommissionEventType {

	public static enum CommissionEventTypeCode {
		ACT,
		DEACT,
		ASS,
		UNASS,
		POSADJ,
		NEGADJ;
	}
	
	public static enum YesNo {
		Y,
		N
	}
	
	private CommissionEventTypeCode code;
	private String name;
	private String desc;
	private YesNo responsibilityFlag;
	private String commissionDesc;
	
	public CommissionEventType() {
		
	}
	
	public CommissionEventType(String code) {
		this.code = CommissionEventTypeCode.valueOf(code.toUpperCase());
	}
	
	public CommissionEventType(CommissionEventTypeCode code) {
		this.code = code;
	}
	
	public CommissionEventTypeCode getCode() {
		return code;
	}
	
	public void setCode(CommissionEventTypeCode code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public YesNo getResponsibilityFlag() {
		return responsibilityFlag;
	}
	
	public void setResponsibilityFlag(YesNo responsibilityFlag) {
		this.responsibilityFlag = responsibilityFlag;
	}
	
	public String getCommissionDesc() {
		return commissionDesc;
	}
	
	public void setCommissionDesc(String commissionDesc) {
		this.commissionDesc = commissionDesc;
	}
	
	public String getFixedWidthDesc() {
		return String.format("%1$-22s%2$-56s%3$-20s", name, desc, commissionDesc).replaceAll(" ", "&nbsp;");
	}
	
	public boolean isDeact() {
		return code == CommissionEventTypeCode.DEACT || code == CommissionEventTypeCode.UNASS;
	}
	
	public boolean isAct() {
		return code == CommissionEventTypeCode.ACT || code == CommissionEventTypeCode.ASS;
	}
	
	public static CommissionEventType findByPk(String code) throws ServiceException, DataLayerException, SQLException {
		return findByPk(CommissionEventTypeCode.valueOf(code.toUpperCase()));
	}

	public static CommissionEventType findByPk(CommissionEventTypeCode code) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_COMMISSION_EVENT_TYPE", new Object[] { code });
		if (!results.next())
			return null;
		
		CommissionEventType commissionEventType = new CommissionEventType(code);
		try {
			ReflectionUtils.populateProperties(commissionEventType, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return commissionEventType;
	}

	public static List<CommissionEventType> findAll() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_COMMISSION_EVENT_TYPES", new Object[] {}).toBeanArray(CommissionEventType.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((commissionDesc == null) ? 0 : commissionDesc.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((responsibilityFlag == null) ? 0 : responsibilityFlag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		CommissionEventType other = (CommissionEventType)obj;
		if(code != other.code)
			return false;
		if(commissionDesc == null) {
			if(other.commissionDesc != null)
				return false;
		} else if(!commissionDesc.equals(other.commissionDesc))
			return false;
		if(desc == null) {
			if(other.desc != null)
				return false;
		} else if(!desc.equals(other.desc))
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(responsibilityFlag != other.responsibilityFlag)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("CommissionEventType [code=%s, name=%s, desc=%s, responsibilityFlag=%s, commissionDesc=%s]", code, name, desc, responsibilityFlag, commissionDesc);
	}	
}
