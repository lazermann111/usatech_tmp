package com.usatech.dms.sales;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class Category {

	public static enum StatusCode {
		A("Active"), 
		I("Inactive"); 
		
		private String desc;
		
		private StatusCode(String desc) {
			this.desc = desc;
		}
		
		public String getDesc() {
			return desc;
		}
	}
	
	private long categoryId;
	private String name;
	private StatusCode statusCode = StatusCode.A;
	
	public Category() {
		
	}
	
	public Category(long categoryId) {
		this.categoryId = categoryId;
	}
	
	public long getCategoryId() {
		return categoryId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	
	public StatusCode getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}
	
	public boolean isActive() {
		return statusCode != null && statusCode == StatusCode.A;
	}
	
	public boolean isNew() {
		return categoryId <= 0;
	}
	
	public static List<Category> findAll() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ALL_CATEGORIES", new Object[] {}).toBeanArray(Category.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public static List<Category> findAllActive() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ACTIVE_CATEGORIES", new Object[] {}).toBeanArray(Category.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	public static Category findByPk(long categoryId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_CATEGORY", new Object[] { categoryId });
		if (!results.next())
			return null;
		
		Category category = new Category(categoryId);
		try {
			ReflectionUtils.populateProperties(category, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return category;
	}
	
	public static Category findCategoryForCustomer(long customerId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_CATEGORY_FOR_CUSTOMER", new Object[] { customerId });
		if (!results.next())
			return null;
		
		Category category = new Category();
		try {
			ReflectionUtils.populateProperties(category, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return category;
	}
	
	public void save() throws DataLayerException, SQLException, ConvertException {
		if (isNew()) {
			Object[] results = DataLayerMgr.executeCall("CATEGORY_INS", this, true);
			if (results == null || results.length != 2)
				throw new DataLayerException("CATEGORY_INS returned unexpected results: " + results);
			setCategoryId(ConvertUtils.getLong(results[1]));
		} else {
			DataLayerMgr.executeCall("CATEGORY_UPD", this, true);
		}
	}

	@Override
	public String toString() {
		return String.format("Category [categoryId=%s, name=%s, statusCode=%s]", categoryId, name, statusCode);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int)(categoryId ^ (categoryId >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((statusCode == null) ? 0 : statusCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Category other = (Category)obj;
		if(categoryId != other.categoryId)
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(statusCode != other.statusCode)
			return false;
		return true;
	}
	
}
