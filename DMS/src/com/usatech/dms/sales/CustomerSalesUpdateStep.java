package com.usatech.dms.sales;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.birt.report.model.api.util.StringUtil;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.CustomerAction;
import com.usatech.dms.model.CorpCustomer;
import com.usatech.layers.common.model.Customer;
import com.usatech.layers.common.util.StringHelper;

public class CustomerSalesUpdateStep extends AbstractStep {

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);
			if (StringHelper.isBlank(action))
				return;
			
			if (action.equals("Find Customers >")) {
				createResults(form, "lines");
			} else if (action.equals("< Back")) {
				request.setAttribute("lines", request.getAttribute("old_lines"));
				request.removeAttribute("results");
			} else if (action.equals("Update Customers >")) {
				long[] customerIds = form.getLongArray("customerIds", false);
				if (customerIds == null || customerIds.length == 0) {
					form.setAttribute("error", "Select customers to update!");
					createResults(form, "old_lines");
					return;
				}
				
				long affiliateId = form.getLong("affiliateId", false, 0);
				long pricingTierId = form.getLong("pricingTierId", false, 0);
				long categoryId = form.getLong("categoryId", false, 0);
				if (affiliateId == 0 && pricingTierId == 0 && categoryId == 0) {
					form.setAttribute("error", "Affiliate, Pricing Tier, or Category are required!");
					createResults(form, "old_lines");
					return;
				}
				
				Map<String, Object> params = new HashMap<>();
				if (affiliateId > 0)
					params.put("affiliateId", affiliateId);
				if (pricingTierId > 0) 
					params.put("pricingTierId", pricingTierId);
				if (categoryId > 0) 
					params.put("categoryId", categoryId);
				
				params.put("customerIds", customerIds);
				
				DataLayerMgr.executeCall("CUSTOMER_SALES_UPD", params, true);
				
				form.setAttribute("message", customerIds.length + " customer(s) updated.");
				
				createResults(form, "old_lines");
			}
		} catch (Exception e) { 
  		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
	
	private void createResults(InputForm form, String attributeName) throws Exception {
		Map<String, List<CorpCustomer>> results = new LinkedHashMap<>();
		String lines = form.getString(attributeName, false);
		if (StringUtil.isEmpty(lines)) {
			form.setAttribute("error", "Input 1 customer name or id number per line!");
			return;
		}
		String[] linesArrays = lines.split("\\r?\\n");
		for(String line : linesArrays) {
			if (StringUtil.isBlank(line))
					continue;
			line = line.trim();
			if (line.matches("^[0-9]+$")) {
				int customerId = ConvertUtils.getInt(line);
				List<CorpCustomer> list = new ArrayList<>();
				CorpCustomer customer = CustomerAction.getCorpCustomer(customerId);
				if (customer != null && customer.getId() != null) {
					list.add(customer);
				}
				results.put(line, list);
			} else {
				String nameParam = line.toLowerCase() + "%";
				List<CorpCustomer> customers = CustomerAction.findCorpCustomersByName(nameParam);
				results.put(line, customers);
			}
		}
		form.setAttribute("results", results);
	}
}
