package com.usatech.dms.sales;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class Affiliate {

	public static enum StatusCode {
		A("Active"), 
		I("Inactive"); 
		
		private String desc;
		
		private StatusCode(String desc) {
			this.desc = desc;
		}
		
		public String getDesc() {
			return desc;
		}
	}
	
	private long affiliateId;
	private String name;
	private StatusCode statusCode = StatusCode.A;
	
	public Affiliate() {
		
	}
	
	public Affiliate(long affiliateId) {
		this.affiliateId = affiliateId;
	}
	
	public long getAffiliateId() {
		return affiliateId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}
	
	public StatusCode getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}
	
	public boolean isActive() {
		return statusCode != null && statusCode == StatusCode.A;
	}
	
	public boolean isNew() {
		return affiliateId <= 0;
	}
	
	public static List<Affiliate> findAll() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ALL_AFFILIATES", new Object[] {}).toBeanArray(Affiliate.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public static List<Affiliate> findAllActive() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ACTIVE_AFFILIATES", new Object[] {}).toBeanArray(Affiliate.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	public static Affiliate findByPk(long affiliateId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_AFFILIATE", new Object[] { affiliateId });
		if (!results.next())
			return null;
		
		Affiliate affiliate = new Affiliate(affiliateId);
		try {
			ReflectionUtils.populateProperties(affiliate, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return affiliate;
	}
	
	public static Affiliate findAffiliateForCustomer(long customerId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_AFFILIATE_FOR_CUSTOMER", new Object[] { customerId });
		if (!results.next())
			return null;
		
		Affiliate affiliate = new Affiliate();
		try {
			ReflectionUtils.populateProperties(affiliate, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return affiliate;
	}
	
	public void save() throws DataLayerException, SQLException, ConvertException {
		if (isNew()) {
			Object[] results = DataLayerMgr.executeCall("AFFILIATE_INS", this, true);
			if (results == null || results.length != 2)
				throw new DataLayerException("AFFILIATE_INS returned unexpected results: " + results);
			setAffiliateId(ConvertUtils.getLong(results[1]));
		} else {
			DataLayerMgr.executeCall("AFFILIATE_UPD", this, true);
		}
	}

	@Override
	public String toString() {
		return String.format("Affiliate [affiliateId=%s, name=%s, statusCode=%s]", affiliateId, name, statusCode);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int)(affiliateId ^ (affiliateId >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((statusCode == null) ? 0 : statusCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Affiliate other = (Affiliate)obj;
		if(affiliateId != other.affiliateId)
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(statusCode != other.statusCode)
			return false;
		return true;
	}
	
}
