package com.usatech.dms.sales;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

import com.usatech.dms.sales.CommissionEventType.CommissionEventTypeCode;

public class CommissionEvent {
	
	public static long USE_CURRENT_SALES_REP_ID = -999;

	private long commissionEventId;
	private long salesRepId;
	private String deviceSerialCd;
	private CommissionEventTypeCode eventTypeCode;
	private Date timestamp;
	private String notes;
	private long fallbackSalesRepId; // only used during save
	
	public CommissionEvent() {
		
	}
	
	public CommissionEvent(long commissionEventId) {
		this.commissionEventId = commissionEventId;
	}
	
	public long getCommissionEventId() {
		return commissionEventId;
	}
	
	public void setCommissionEventId(long commissionEventId) {
		this.commissionEventId = commissionEventId;
	}
	
	public long getSalesRepId() {
		return salesRepId;
	}
	
	public void setSalesRepId(long salesRepId) {
		this.salesRepId = salesRepId;
	}
	
	public String getDeviceSerialCd() {
		return deviceSerialCd;
	}
	
	public void setDeviceSerialCd(String deviceSerialCd) {
		this.deviceSerialCd = deviceSerialCd;
	}
	
	public CommissionEventTypeCode getEventTypeCode() {
		return eventTypeCode;
	}
	
	public void setEventTypeCode(CommissionEventTypeCode eventTypeCode) {
		this.eventTypeCode = eventTypeCode;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getNotes() {
		return notes;
	}
	
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public boolean isNew() {
		return commissionEventId <= 0;
	}
	
	public long getFallbackSalesRepId() {
		return fallbackSalesRepId;
	}
	
	public void setFallbackSalesRepId(long fallbackSalesRepId) {
		this.fallbackSalesRepId = fallbackSalesRepId;
	}
	
	public static CommissionEvent findByPk(long commissionEventId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_COMMISSION_EVENT", new Object[] { commissionEventId });
		if (!results.next())
			return null;
		
		CommissionEvent commissionEvent = new CommissionEvent(commissionEventId);
		try {
			ReflectionUtils.populateProperties(commissionEvent, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return commissionEvent;
	}
	
	public static List<CommissionEvent> findBySalesRep(long salesRepId) throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_COMMISSION_EVENTS_BY_SALES_REP", new Object[] {salesRepId}).toBeanArray(CommissionEvent.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public static List<CommissionEvent> findByDevice(String deviceSerialCd) throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_COMMISSION_EVENTS_BY_DEVICE", new Object[] {deviceSerialCd}).toBeanArray(CommissionEvent.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public static CommissionEvent findCurrentByDevice(String deviceSerialCd) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_DEVICE_CURRENT_ASSIGNMENT_EVENT", new Object[] { deviceSerialCd });
		if (!results.next())
			return null;
		
		CommissionEvent commissionEvent = new CommissionEvent();
		try {
			ReflectionUtils.populateProperties(commissionEvent, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return commissionEvent;
	}

	public void save() throws DataLayerException, SQLException, ConvertException {
		if (isNew()) {
			Object[] results = DataLayerMgr.executeCall("INSERT_COMMISSION_EVENT", this, true);
			if (results == null || results.length != 3)
				throw new DataLayerException("INSERT_COMMISSION_EVENT returned unexpected results: " + Arrays.toString(results));
			setCommissionEventId(ConvertUtils.getLong(results[1]));
			setSalesRepId(ConvertUtils.getLong(results[2]));
		} else {
			DataLayerMgr.executeCall("UPDATE_COMMISSION_EVENT", this, true);
		}
	}
	
	public void delete() throws DataLayerException, SQLException, ConvertException {
		DataLayerMgr.executeCall("DELETE_COMMISSION_EVENT", this, true);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int)(commissionEventId ^ (commissionEventId >>> 32));
		result = prime * result + ((deviceSerialCd == null) ? 0 : deviceSerialCd.hashCode());
		result = prime * result + ((eventTypeCode == null) ? 0 : eventTypeCode.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result + (int)(salesRepId ^ (salesRepId >>> 32));
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		CommissionEvent other = (CommissionEvent)obj;
		if(commissionEventId != other.commissionEventId)
			return false;
		if(deviceSerialCd == null) {
			if(other.deviceSerialCd != null)
				return false;
		} else if(!deviceSerialCd.equals(other.deviceSerialCd))
			return false;
		if(eventTypeCode != other.eventTypeCode)
			return false;
		if(notes == null) {
			if(other.notes != null)
				return false;
		} else if(!notes.equals(other.notes))
			return false;
		if(salesRepId != other.salesRepId)
			return false;
		if(timestamp == null) {
			if(other.timestamp != null)
				return false;
		//} else if(!timestamp.equals(other.timestamp))
		} else if(timestamp.getTime()/1000 != other.timestamp.getTime()/1000) // oracle stores to second precision
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("CommissionEvent [commissionEventId=%s, salesRepId=%s, deviceSerialCd=%s, eventTypeCode=%s, timestamp=%s, notes=%s]", commissionEventId, salesRepId, deviceSerialCd, eventTypeCode, timestamp, notes);
	}
}
