package com.usatech.dms.sales;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.steps.AbstractStep;

public class BulkSalesRepStep extends AbstractStep{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);
			if (StringHelper.isBlank(action) || !action.equals("Change Sales Reps"))
				return;
			
			String list = form.getString("dev_list", true);
	
			long salesRepId = form.getLong("salesRepId", true, 0);
			long fallbackSalesRepId = form.getLong("fallback_salesRepId", false, 0);
			String eventTypeCode = form.getString("eventTypeCode", true);
			
			String effective_date_str = form.getString("date", false);
			if (StringHelper.isBlank(effective_date_str) || effective_date_str.length() < 10)
				effective_date_str = Helper.getCurrentDate();
			Date effectiveDate = ConvertUtils.convert(Date.class, new StringBuilder(effective_date_str).append(" ").append(form.getString("time", false)).toString());
			if (effectiveDate == null)
				effectiveDate = new Date();
			
			String flatList = list.replace('\n', ',').replace("\r", "").replace(" ", "");
		
			StringBuilder err = new StringBuilder();
			int updatedCount = 0;
			Results rs = DataLayerMgr.executeQuery("GET_DEVICES", new Object[] {flatList});
			
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();
			
			while (rs.next()) {
				String eportSerialNum = rs.getFormattedValue("DEVICE_SERIAL_CD");
				String deviceName = rs.getFormattedValue("DEVICE_NAME");
				try {
					DeviceActions.createCommissionEvent(eportSerialNum, salesRepId, eventTypeCode, effectiveDate, fallbackSalesRepId);
					updatedCount++;
					WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, System.currentTimeMillis(), "Submit", "device", String.valueOf(deviceName), log);
				} catch (Exception e) {
					err.append("Error updating sales reps for device ").append(eportSerialNum).append(": ").append(e.getMessage()).append("<br /><br />");
					log.error(new StringBuilder("Error updating sales rep for device ").append(eportSerialNum).toString(), e);
				}
			}
			request.setAttribute("message", "Sales reps updated: " + updatedCount);
			if (err.length() > 0)
				request.setAttribute("error", err.toString());
		} catch(Exception e){
			throw new ServletException("Error updating fees", e);
		}
	}
}
