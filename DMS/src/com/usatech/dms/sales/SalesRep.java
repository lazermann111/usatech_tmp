package com.usatech.dms.sales;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class SalesRep {

	public static enum StatusCode {
		A("Active"), 
		I("Inactive"); 
		
		private String desc;
		
		private StatusCode(String desc) {
			this.desc = desc;
		}
		
		public String getDesc() {
			return desc;
		}
	}
	
	private long salesRepId;
	private String firstName;
	private String lastName;
	private StatusCode statusCode = StatusCode.A;
	
	public SalesRep() {
		
	}
	
	public SalesRep(long salesRepId) {
		this.salesRepId = salesRepId;
	}
	
	public long getSalesRepId() {
		return salesRepId;
	}
	
	public void setSalesRepId(long salesRepId) {
		this.salesRepId = salesRepId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getName() {
		return String.format("%s %s%s", firstName, lastName, isActive() ? "" : " (Inactive)");
	}
	
	public StatusCode getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}
	
	public boolean isActive() {
		return statusCode != null && statusCode == StatusCode.A;
	}
	
	public boolean isNew() {
		return salesRepId <= 0;
	}
	
	public static List<SalesRep> findAll() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ALL_SALES_REPS", new Object[] {}).toBeanArray(SalesRep.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public static List<SalesRep> findAllActive() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ACTIVE_SALES_REPS", new Object[] {}).toBeanArray(SalesRep.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	public static SalesRep findByPk(long salesRepId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_SALES_REP", new Object[] { salesRepId });
		if (!results.next())
			return null;
		
		SalesRep salesRep = new SalesRep(salesRepId);
		try {
			ReflectionUtils.populateProperties(salesRep, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return salesRep;
	}
	
	public static SalesRep findSalesRepResponsibleForDevice(String deviceSerialCd) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_SALES_REP_RESPONSIBLE_FOR_DEVICE", new Object[] { deviceSerialCd });
		if (!results.next())
			return null;
		
		SalesRep salesRep = new SalesRep();
		try {
			ReflectionUtils.populateProperties(salesRep, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return salesRep;
	}
	
	public static SalesRep findSalesRepResponsibleForTerminal(long terminalId, Date date) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_SALES_REP_RESPONSIBLE_FOR_TERMINAL", new Object[] { terminalId, date });
		if (!results.next())
			return null;
		
		SalesRep salesRep = new SalesRep();
		try {
			ReflectionUtils.populateProperties(salesRep, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return salesRep;
	}


	public void save() throws DataLayerException, SQLException, ConvertException {
		if (isNew()) {
			Object[] results = DataLayerMgr.executeCall("SALES_REP_INS", this, true);
			if (results == null || results.length != 2)
				throw new DataLayerException("INSERT_SALES_REP returned unexpected results: " + results);
			setSalesRepId(ConvertUtils.getLong(results[1]));
		} else {
			DataLayerMgr.executeCall("SALES_REP_UPD", this, true);
		}
	}
	
	@Override
	public String toString() {
		return String.format("SalesRep [salesRepId=%s, firstName=%s, lastName=%s, statusCode=%s]", salesRepId, firstName, lastName, statusCode);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + (int)(salesRepId ^ (salesRepId >>> 32));
		result = prime * result + ((statusCode == null) ? 0 : statusCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		SalesRep other = (SalesRep)obj;
		if(firstName == null) {
			if(other.firstName != null)
				return false;
		} else if(!firstName.equals(other.firstName))
			return false;
		if(lastName == null) {
			if(other.lastName != null)
				return false;
		} else if(!lastName.equals(other.lastName))
			return false;
		if(salesRepId != other.salesRepId)
			return false;
		if(statusCode != other.statusCode)
			return false;
		return true;
	}
}
