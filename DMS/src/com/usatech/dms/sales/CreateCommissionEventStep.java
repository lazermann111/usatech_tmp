package com.usatech.dms.sales;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.ThreadSafeDateFormat;

import com.usatech.dms.action.DeviceActions;

public class CreateCommissionEventStep extends AbstractStep {
	
	private static ThreadSafeDateFormat EVENT_TS_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"));
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String deviceSerialCd = form.getString("deviceSerialCd", true);
			long salesRepId = form.getLong("salesRepId", true, -1);
			String eventTypeCode = form.getString("eventTypeCode", true);
			String dateStr = form.getString("date", true);
			String timeStr = form.getString("time", true);
			Date eventTimestamp = EVENT_TS_FORMAT.parse(String.format("%s %s", dateStr, timeStr));
			
			DeviceActions.createCommissionEvent(deviceSerialCd, salesRepId, eventTypeCode, eventTimestamp);
		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
}
