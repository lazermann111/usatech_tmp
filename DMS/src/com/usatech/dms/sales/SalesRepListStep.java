package com.usatech.dms.sales;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class SalesRepListStep extends AbstractStep {

	private static final String DEFAULT_SORT_INDEX = "3";
	private static final String SQL_START = " SELECT SALES_REP_ID, FIRST_NAME, LAST_NAME, (CASE STATUS_CD WHEN 'A' THEN 'Active' WHEN 'I' THEN 'Inactive' END) STATUS ";
	private static final String SQL_BASE = " FROM (SELECT SALES_REP_ID, FIRST_NAME, LAST_NAME, STATUS_CD FROM REPORT.SALES_REP) SALES_REP ";
	private static final String[] SORT_FIELDS = {"SALES_REP_ID", "FIRST_NAME", "LAST_NAME", "STATUS_CD"};

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String lastName = form.getString("lastName", false);

			String sql = "";
			if(!StringHelper.isBlank(lastName)) {
				//lastName += "%";
				sql = " where lower(last_name) like lower(?) ";
			}

			String queryBase = SQL_BASE + sql;

			String paramTotalCount = PaginationUtil.getTotalField(null);
			String paramPageIndex = PaginationUtil.getIndexField(null);
			String paramPageSize = PaginationUtil.getSizeField(null);
			String paramSortIndex = PaginationUtil.getSortField(null);

			int totalCount = form.getInt(paramTotalCount, false, -1);
			if(totalCount == -1) {
				Results total = null;
				if(!StringHelper.isBlank(lastName)) {
					total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1), MAX(SALES_REP_ID) " + queryBase, new Object[] {(Helper.convertParam(lastName, false))}, null);
				} else {
					total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1), MAX(SALES_REP_ID) " + queryBase, null, null);
				}

				if(total.next()) {
					totalCount = total.getValue(1, int.class);
					if(totalCount == 1) {
						form.setRedirectUri(new StringBuilder("/salesRep.i?salesRepId=").append(total.getValue(2, String.class)).toString());
						return;
					}
				} else {
					totalCount = 0;
				}
				request.setAttribute(paramTotalCount, String.valueOf(totalCount));
			}

			int pageIndex = form.getInt(paramPageIndex, false, 1);
			int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
			int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
			int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

			String sortIndex = form.getString(paramSortIndex, false);
			sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
			String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

			String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ queryBase + orderBy + " limit ?::numeric) pagination_temp ) sq_end where rnum  >= ?::numeric";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + queryBase
						+ orderBy + ") pagination_temp where ROWNUM <= ?) where rnum  >= ?";
			}
			
			Object[] params = null;
			if(StringHelper.isBlank(lastName)) {
				params = new Object[] {maxRowToFetch,minRowToFetch};
			} else {
				params = new Object[] {Helper.convertParam(lastName, false),maxRowToFetch,minRowToFetch};
			}

			Results results = DataLayerMgr.executeSQL("REPORT", query, params, null);
			request.setAttribute("resultlist", results);

		} catch(Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}

	}

}
