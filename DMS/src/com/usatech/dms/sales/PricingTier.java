package com.usatech.dms.sales;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class PricingTier {

	public static enum StatusCode {
		A("Active"), 
		I("Inactive"); 
		
		private String desc;
		
		private StatusCode(String desc) {
			this.desc = desc;
		}
		
		public String getDesc() {
			return desc;
		}
	}
	
	private long pricingTierId;
	private String name;
	private StatusCode statusCode = StatusCode.A;
	
	public PricingTier() {
		
	}
	
	public PricingTier(long pricingTierId) {
		this.pricingTierId = pricingTierId;
	}
	
	public long getPricingTierId() {
		return pricingTierId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPricingTierId(long pricingTierId) {
		this.pricingTierId = pricingTierId;
	}
	
	public StatusCode getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}
	
	public boolean isActive() {
		return statusCode != null && statusCode == StatusCode.A;
	}
	
	public boolean isNew() {
		return pricingTierId <= 0;
	}
	
	public static List<PricingTier> findAll() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ALL_PRICING_TIERS", new Object[] {}).toBeanArray(PricingTier.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public static List<PricingTier> findAllActive() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ACTIVE_PRICING_TIERS", new Object[] {}).toBeanArray(PricingTier.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	public static PricingTier findByPk(long pricingTierId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_PRICING_TIER", new Object[] { pricingTierId });
		if (!results.next())
			return null;
		
		PricingTier pricingTier = new PricingTier(pricingTierId);
		try {
			ReflectionUtils.populateProperties(pricingTier, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return pricingTier;
	}
	
	public static PricingTier findPricingTierForCustomer(long customerId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_PRICING_TIER_FOR_CUSTOMER", new Object[] { customerId });
		if (!results.next())
			return null;
		
		PricingTier pricingTier = new PricingTier();
		try {
			ReflectionUtils.populateProperties(pricingTier, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return pricingTier;
	}
	
	public void save() throws DataLayerException, SQLException, ConvertException {
		if (isNew()) {
			Object[] results = DataLayerMgr.executeCall("PRICING_TIER_INS", this, true);
			if (results == null || results.length != 2)
				throw new DataLayerException("PRICING_TIER_INS returned unexpected results: " + results);
			setPricingTierId(ConvertUtils.getLong(results[1]));
		} else {
			DataLayerMgr.executeCall("PRICING_TIER_UPD", this, true);
		}
	}

	@Override
	public String toString() {
		return String.format("PricingTier [pricingTierId=%s, name=%s, statusCode=%s]", pricingTierId, name, statusCode);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int)(pricingTierId ^ (pricingTierId >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((statusCode == null) ? 0 : statusCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		PricingTier other = (PricingTier)obj;
		if(pricingTierId != other.pricingTierId)
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(statusCode != other.statusCode)
			return false;
		return true;
	}
	
}
