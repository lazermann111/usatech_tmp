package com.usatech.dms.sales;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class Distributor {
	
	private int distributorId;
	private String distributorName;
	private String description;
	
	public Distributor() {
		
	}
	
	public Distributor(int distributorId) {
		this.distributorId = distributorId;
	}
	
	public void setDistributorId(int distributorId) {
		this.distributorId = distributorId;
	}
	
	public int getDistributorId() {
		return this.distributorId;
	}
	
	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}
	
	public String getDistributorName() {
		return this.distributorName;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public static List<Distributor> findAll() throws ServiceException, DataLayerException, SQLException {
		try {
			return Arrays.asList(DataLayerMgr.executeQuery("GET_ALL_DISTRIBUTORS", new Object[] {}).toBeanArray(Distributor.class));
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public static Distributor findDistributorForDevice(String deviceSerialCd) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_DISTRIBUTOR_FOR_DEVICE", new Object[] { deviceSerialCd });
		if (!results.next())
			return null;
		
		Distributor distributor = new Distributor();
		try {
			ReflectionUtils.populateProperties(distributor, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		return distributor;
	}
	
	public static Distributor findByPk(int distributorId) throws ServiceException, DataLayerException, SQLException {
		Results results = DataLayerMgr.executeQuery("GET_DISTRIBUTOR", new Object[] { distributorId });
		if (!results.next())
			return null;
		
		Distributor distributor = new Distributor(distributorId);
		try {
			ReflectionUtils.populateProperties(distributor, results.getValuesMap());
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return distributor;
	}
	
	public boolean isNew() {
		return distributorId <= 0;
	}
	
	public void save() throws DataLayerException, SQLException, ConvertException {
		if (isNew()) {
			Object[] results = DataLayerMgr.executeCall("DISTRIBUTOR_INS", this, true);
			if (results == null || results.length != 2)
				throw new DataLayerException("INSERT_DISTRIBUTOR returned unexpected results: " + results);
			setDistributorId(ConvertUtils.getInt(results[1]));
		} else {
			DataLayerMgr.executeCall("DISTRIBUTOR_UPD", this, true);
		}
	}
	
}
