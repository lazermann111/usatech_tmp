package com.usatech.dms.sales;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.steps.AbstractStep;

public class BulkDistributorStep extends AbstractStep{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);
			if (StringHelper.isBlank(action) || !action.equals("Change Distributors"))
				return;
			
			String list = form.getString("dev_list", true);
	
			int distributorId = form.getInt("distributorId", true, 0);
			long fallbackSalesRepId = form.getLong("fallback_salesRepId", false, 0);
			
			String flatList = list.replace('\n', ',').replace("\r", "").replace(" ", "");
		
			StringBuilder err = new StringBuilder();
			int updatedCount = 0;
			Results rs = DataLayerMgr.executeQuery("GET_DEVICES", new Object[] {flatList});
			
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();
			
			while (rs.next()) {
				String eportSerialNum = rs.getFormattedValue("DEVICE_SERIAL_CD");
				String deviceName = rs.getFormattedValue("DEVICE_NAME");
				Map<String, Object> params = new HashMap<>();
				params.put("deviceSerialCd", eportSerialNum);
				params.put("distributorId", distributorId);
				try {
					DataLayerMgr.executeCall("DEVICE_DISTRIBUTOR_UPD", params, true);
					updatedCount++;
					WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, System.currentTimeMillis(), "Submit", "device", String.valueOf(deviceName), log);
				} catch (Exception e) {
					err.append("Error updating distributors for device ").append(eportSerialNum).append(": ").append(e.getMessage()).append("<br /><br />");
					log.error(new StringBuilder("Error updating distributor for device ").append(eportSerialNum).toString(), e);
				}
			}
			request.setAttribute("message", "Distributors updated: " + updatedCount);
			if (err.length() > 0)
				request.setAttribute("error", err.toString());
		} catch(Exception e){
			throw new ServletException("Error updating fees", e);
		}
	}
}
