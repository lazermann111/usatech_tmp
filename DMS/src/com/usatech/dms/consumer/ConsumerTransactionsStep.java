/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

import com.usatech.dms.location.LocationConstants;
import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class ConsumerTransactionsStep extends DMSPaginationStep
{
    //private static final String ACTION_CONSUMER_CARD_SEARCH_RESULTS = "/consumerSearch";
    //private static final String ACTION_NOT_FOUND = "/consumerSearchResultsEmpty";

    private StringBuilder notFoundMsg = new StringBuilder("No Results for Consumer/Card Search on the following search params ");

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String consumerId = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ID, false);
        
        
        StringBuilder condition = new StringBuilder("");

        if (consumerId != null && !consumerId.trim().equals(""))
        {
        	
            log.debug(">> search for Transactions by Consumer Id...");
            notFoundMsg.append(": CONSUMER ID \"" + consumerId + "\" ");
            // search by Consumer Id
            condition.append(" AND ca.consumer_id = CAST (? AS numeric) ");
        }

        try
        {
            String queryBase = ConsumerConstants.SQL_CONSUMER_INFO_TRANSACTIONS_BASE + condition.toString();
            
            setPaginatedResultsOnRequest(form, request, ConsumerConstants.SQL_CONSUMER_INFO_TRANSACTIONS_START, queryBase, ConsumerConstants.SQL_END, ConsumerConstants.DEFAULT_SORT_INDEX_CONSUMER_INFO_TRANSACTIONS,
            		ConsumerConstants.SORT_FIELDS__CONSUMER_INFO_TRANSACTIONS, ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_TRANSACTIONS, ((!StringHelper.isBlank(consumerId))?
            				new Object[]{consumerId}:null));

        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
