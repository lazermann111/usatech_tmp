/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.dms.model.PermissionAction;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class EditConsumerAcctStep extends AbstractStep
{	
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String consumerAcctId = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ACCT_ID, false);
        com.usatech.dms.model.ConsumerAccount consumerAcct = null;
        
        if (StringUtils.isBlank(consumerAcctId)) {
        	request.setAttribute("missingParam", true);
        	return;
        }		
		
		try {
			consumerAcct = com.usatech.dms.action.ConsumerAction.getConsumerAccountByConsumerAccountId(consumerAcctId);
			if (consumerAcct == null) {
				request.setAttribute("errorMessage", "Consumer account not found");
				return;
			}
			
			List<PermissionAction> permissionActions = com.usatech.dms.action.ConsumerAction.getConsumerAccountPermissionActionsByConsumerAccountId(new Long(consumerAcctId));
			consumerAcct.setPermissionActions(permissionActions);
	    	request.setAttribute(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ACCT, consumerAcct);
		
	    	String transaction_from_time = form.getString(DevicesConstants.PARAM_TRAN_FROM_TIME, false);
	        if (StringHelper.isBlank(transaction_from_time)){
	            transaction_from_time = Helper.getDefaultStartTime();
	            request.setAttribute(DevicesConstants.PARAM_TRAN_FROM_TIME, transaction_from_time);
	        }        
	        String transaction_from_date = form.getString(DevicesConstants.PARAM_TRAN_FROM_DATE, false);
	        if (StringHelper.isBlank(transaction_from_date)){
	            transaction_from_date = Helper.getDefaultStartDate();
	            request.setAttribute(DevicesConstants.PARAM_TRAN_FROM_DATE, transaction_from_date);
	        }
	        String transaction_to_time = form.getString(DevicesConstants.PARAM_TRAN_TO_TIME, false);
	        if (StringHelper.isBlank(transaction_to_time)){
	            transaction_to_time = Helper.getDefaultEndTime();
	            request.setAttribute(DevicesConstants.PARAM_TRAN_TO_TIME, transaction_to_time);
	        }        
	        String transaction_to_date = form.getString(DevicesConstants.PARAM_TRAN_TO_DATE, false);
	        if (StringHelper.isBlank(transaction_to_date)){
	            transaction_to_date = Helper.getDefaultEndDate();
	            request.setAttribute(DevicesConstants.PARAM_TRAN_TO_DATE, transaction_to_date);
	        }
	    	String userOP = form.getString(DevicesConstants.PARAM_USER_OP, false);
	    	
	    	if (DevicesConstants.USER_OP_LIST_TRAN.equalsIgnoreCase(userOP)) {
	    		String DEFAULT_SORT_INDEX = "-2";
	            String[] SORT_FIELDS = {"COALESCE(TRAN_ID, R_TRAN_ID)",
	                    "CLOSE_DATE",
	                    "STATE_LABEL",
	                    "TRAN_TYPE",
	                    "LOCATION_NAME",
	                    "TOTAL_AMOUNT",
	                    "DISCOUNT"
	                 };
	    		
	    		StringBuilder sql = new StringBuilder("SELECT DISTINCT TRAN_ID, R_TRAN_ID, CLOSE_DATE, STATE_LABEL, TOTAL_AMOUNT, DISCOUNT, TRAN_TYPE, LOCATION_NAME")
	    			.append(", TO_CHAR(CLOSE_DATE, 'MM/DD/YYYY HH24:MI:SS') CLOSE_DATE_FMT, TO_CHAR(TOTAL_AMOUNT, 'FM999,999,999,999,990.00') TOTAL_AMOUNT_FMT, TO_CHAR(DISCOUNT, 'FM999,999,999,999,990.00') DISCOUNT_FMT FROM (")
			    	.append("SELECT /*+ NO_INDEX(X USAT_IX_TRANS_CLOSE_DT) */ PX.TRAN_ID, X.TRAN_ID R_TRAN_ID, X.CLOSE_DATE, COALESCE(PTS.TRAN_STATE_DESC, TS.STATE_LABEL) STATE_LABEL, X.TOTAL_AMOUNT, SUM(P.AMOUNT * P.PRICE) DISCOUNT, COALESCE(L.DESCRIPTION, L.LOCATION_NAME) LOCATION_NAME,") 
					.append(" (CASE X.TRANS_TYPE_ID WHEN 20 THEN 'Refund' WHEN 21 THEN 'Chargeback' ELSE 'Purchase'END) TRAN_TYPE")
					.append(" FROM PSS.CONSUMER_ACCT_BASE CA")
					.append(" JOIN REPORT.TRANS X ON CA.CONSUMER_ACCT_ID = X.CONSUMER_ACCT_ID")
					.append(" JOIN REPORT.TRANS_STATE TS ON X.SETTLE_STATE_ID = TS.STATE_ID")
					.append(" LEFT OUTER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID")
					.append(" LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID")
					.append(" LEFT OUTER JOIN REPORT.PURCHASE P ON X.TRAN_ID = P.TRAN_ID AND P.TRAN_LINE_ITEM_TYPE_ID IN(204)")
					.append(" LEFT OUTER JOIN PSS.TRAN PX ON X.MACHINE_TRANS_NO = PX.TRAN_GLOBAL_TRANS_CD")
					.append(" LEFT OUTER JOIN PSS.TRAN_STATE PTS ON PX.TRAN_STATE_CD = PTS.TRAN_STATE_CD")
					.append(" WHERE CA.CONSUMER_ACCT_ID = CAST (? AS numeric)")
					.append(!DialectResolver.isOracle() 
							? " AND X.CLOSE_DATE BETWEEN ?::timestamp AND ?::timestamp"
							: " AND X.CLOSE_DATE BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')" )
					.append(" GROUP BY PX.TRAN_ID, X.TRAN_ID, X.CLOSE_DATE, PTS.TRAN_STATE_DESC, TS.STATE_LABEL, X.TOTAL_AMOUNT, L.DESCRIPTION, L.LOCATION_NAME, X.TRANS_TYPE_ID")
					.append(" UNION ALL")
					.append(" SELECT PX.TRAN_ID, NULL R_TRAN_ID, PX.TRAN_START_TS CLOSE_DATE, TS.TRAN_STATE_DESC STATE_LABEL, COALESCE(S.SALE_AMOUNT, A.AUTH_AMT) TOTAL_AMOUNT, NULL DISCOUNT, L.LOCATION_NAME")
					.append(", CASE WHEN S.TRAN_ID IS NULL THEN 'Authorization' ELSE 'Purchase' END TRAN_TYPE")
					.append(" FROM PSS.TRAN PX")
					.append(" JOIN PSS.TRAN_STATE TS ON PX.TRAN_STATE_CD = TS.TRAN_STATE_CD")
					.append(" JOIN PSS.POS_PTA PP ON PX.POS_PTA_ID = PP.POS_PTA_ID")
					.append(" JOIN PSS.POS P ON PP.POS_ID = P.POS_ID")
					.append(" JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID")
					.append(" LEFT OUTER JOIN PSS.AUTH A ON PX.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N'")
					.append(" LEFT OUTER JOIN PSS.SALE S ON PX.TRAN_ID = S.TRAN_ID")
					.append(" LEFT OUTER JOIN REPORT.TRANS X ON PX.TRAN_GLOBAL_TRANS_CD = X.MACHINE_TRANS_NO")
					.append(" WHERE PX.CONSUMER_ACCT_ID = CAST (? AS numeric)")
					.append(!DialectResolver.isOracle()
							? " AND PX.TRAN_START_TS BETWEEN ?::timestamp AND ?::timestamp"
							: " AND PX.TRAN_START_TS BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')")
					.append(" AND X.TRAN_ID IS NULL")
					.append(" UNION ALL")
					.append(" SELECT PX.TRAN_ID, X.TRAN_ID R_TRAN_ID, X.CLOSE_DATE, COALESCE(PTS.TRAN_STATE_DESC, TS.STATE_LABEL) STATE_LABEL, SUM(P.AMOUNT * P.PRICE), NULL, NULL, 'Replenish' TRAN_TYPE")
					.append(" FROM PSS.CONSUMER_ACCT CA")
					.append(" JOIN REPORT.PURCHASE P ON CA.CONSUMER_ACCT_ID = P.APPLY_TO_CONSUMER_ACCT_ID AND P.TRAN_LINE_ITEM_TYPE_ID IN(550, 552)")
					.append(" JOIN REPORT.TRANS X ON P.TRAN_ID = X.TRAN_ID")
					.append(" JOIN REPORT.TRANS_STATE TS ON X.SETTLE_STATE_ID = TS.STATE_ID")
					.append(" JOIN CORP.CURRENCY CU ON X.CURRENCY_ID = CU.CURRENCY_ID")
					.append(" LEFT OUTER JOIN PSS.TRAN PX ON X.MACHINE_TRANS_NO = PX.TRAN_GLOBAL_TRANS_CD")
					.append(" LEFT OUTER JOIN PSS.TRAN_STATE PTS ON PX.TRAN_STATE_CD = PTS.TRAN_STATE_CD")
					.append(" WHERE CA.CONSUMER_ACCT_ID = CAST (? AS numeric)")
					.append(!DialectResolver.isOracle()
							? " AND X.CLOSE_DATE BETWEEN ?::timestamp AND ?::timestamp"
							: " AND X.CLOSE_DATE BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')")
					.append(" GROUP BY PX.TRAN_ID, X.TRAN_ID, X.CLOSE_DATE, PTS.TRAN_STATE_DESC, TS.STATE_LABEL")  
					.append(" UNION ALL")
					.append(" SELECT PX.TRAN_ID, X.TRAN_ID R_TRAN_ID, X.CLOSE_DATE, COALESCE(PTS.TRAN_STATE_DESC, TS.STATE_LABEL) STATE_LABEL, SUM(P.PRICE) TOTAL_AMOUNT, -SUM(P.AMOUNT * P.PRICE) DISCOUNT, NULL, 'Cashback' TRAN_TYPE")
					.append(" FROM PSS.CONSUMER_ACCT CA")
					.append(" JOIN REPORT.PURCHASE P ON CA.CONSUMER_ACCT_ID = P.APPLY_TO_CONSUMER_ACCT_ID AND P.TRAN_LINE_ITEM_TYPE_ID IN(554)")
					.append(" JOIN REPORT.TRANS X ON P.TRAN_ID = X.TRAN_ID")
					.append(" JOIN REPORT.TRANS_STATE TS ON X.SETTLE_STATE_ID = TS.STATE_ID")
					.append(" LEFT OUTER JOIN PSS.TRAN PX ON X.MACHINE_TRANS_NO = PX.TRAN_GLOBAL_TRANS_CD")
					.append(" LEFT OUTER JOIN PSS.TRAN_STATE PTS ON PX.TRAN_STATE_CD = PTS.TRAN_STATE_CD")
					.append(" WHERE CA.CONSUMER_ACCT_ID = CAST (? AS numeric)")
					.append(!DialectResolver.isOracle()
							? " AND X.CLOSE_DATE BETWEEN ?::timestamp AND ?::timestamp"
							: " AND X.CLOSE_DATE BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS')")
					.append(" GROUP BY PX.TRAN_ID, X.TRAN_ID, X.CLOSE_DATE, PTS.TRAN_STATE_DESC, TS.STATE_LABEL) tmp");
		
			    String queryBase = sql.toString();
			    String paramTotalCount = PaginationUtil.getTotalField(null);
			    String paramPageIndex = PaginationUtil.getIndexField(null);
			    String paramPageSize = PaginationUtil.getSizeField(null);
			    String paramSortIndex = PaginationUtil.getSortField(null);
			    String fromDate = new StringBuilder(transaction_from_date).append(' ').append(transaction_from_time).toString();
			    String toDate = new StringBuilder(transaction_to_date).append(' ').append(transaction_to_time).toString();
			    Object[] params;
		
		        // pagination parameters
		        int totalCount = form.getInt(paramTotalCount, false, -1);
		        if (totalCount == -1)
		        {
		            // if the total count is not retrieved yet, get it now
					if (!DialectResolver.isOracle()) {
						params = new Object[] { consumerAcctId, fromDate, toDate, consumerAcctId, fromDate, toDate,
								consumerAcctId, fromDate, toDate, consumerAcctId, fromDate, toDate };
					} else {
						params = new Object[] { consumerAcctId, transaction_from_date, transaction_from_time,
								transaction_to_date, transaction_to_time, consumerAcctId, transaction_from_date,
								transaction_from_time, transaction_to_date, transaction_to_time, consumerAcctId,
								transaction_from_date, transaction_from_time, transaction_to_date, transaction_to_time,
								consumerAcctId, transaction_from_date, transaction_from_time, transaction_to_date,
								transaction_to_time };
					}
 	        	    	
		        	Results total = DataLayerMgr.executeSQL("OPER", new StringBuilder("SELECT COUNT(1) FROM (").append(queryBase).append(") sq_end").toString(), params, null);
		            if (total.next())
		                totalCount = total.getValue(1, int.class);
		            else
		                totalCount = 0;
		            request.setAttribute(paramTotalCount, String.valueOf(totalCount));
		        }
		
		        int pageIndex = form.getInt(paramPageIndex, false, 1);
		        int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
		        int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
		        int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);                
		
		        String sortIndex = form.getString(paramSortIndex, false);                
		        sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
		        String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
		
				String query;
				if (!DialectResolver.isOracle()) {
					query = new StringBuilder("select * from (")
							.append(" select pagination_temp.*, row_number() over() as rnum from (").append(queryBase)
							.append(orderBy).append(") pagination_temp LIMIT ?::bigint ")
							.append(") sq_end where rnum  >= ?::bigint ").toString();
					params = new Object[] { consumerAcctId, fromDate, toDate, consumerAcctId, fromDate, toDate,
							consumerAcctId, fromDate, toDate, consumerAcctId, fromDate, toDate, maxRowToFetch,
							minRowToFetch };
				} else {
					query = new StringBuilder("select /*+ALL_ROWS*/ * from (")
							.append(" select pagination_temp.*, ROWNUM rnum from (").append(queryBase).append(orderBy)
							.append(") pagination_temp where ROWNUM <= ? ").append(") where rnum  >= ? ").toString();
					params = new Object[] { consumerAcctId, transaction_from_date, transaction_from_time,
							transaction_to_date, transaction_to_time, consumerAcctId, transaction_from_date,
							transaction_from_time, transaction_to_date, transaction_to_time, consumerAcctId,
							transaction_from_date, transaction_from_time, transaction_to_date, transaction_to_time,
							consumerAcctId, transaction_from_date, transaction_from_time, transaction_to_date,
							transaction_to_time, maxRowToFetch, minRowToFetch };
				}
		
		        Results results = DataLayerMgr.executeSQL("OPER", query, params, null);
		        request.setAttribute("tranList", results);
	    	}
		} catch (Exception e) {
			throw new ServletException(e);
		}
    }
}
