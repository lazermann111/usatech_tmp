/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

public class NewConsumerAcct4Step extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	public static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	Map errorMap = new HashMap();
    	   	
    	int num_cards = -1;
    	String balance = "";
    	
    	if(!(StringHelper.isBlank(form.getString("exp", false))))
    			Helper.validateRegex(errorMap, form.getString("exp", false), "exp", "^([0-9]{4})$");
    	Helper.validateExists(errorMap, form.getString("num_cards", false), "num_cards");
    	Helper.validateRegex(errorMap, form.getString("num_cards", false), "num_cards", "^([0-9]{1,6})$");
    	
    	int consumer_acct_fmt_id = form.getInt("consumer_acct_fmt_id", false, -1);
    	if(consumer_acct_fmt_id == 0){
    		Helper.validateExists(errorMap, form.getString("balance", false), "balance");
        	Helper.validateRegex(errorMap, form.getString("balance", false), "balance", "^[0-9]{1,6}(?:\\.[0-9]{1,2})?$");
    	} else if(consumer_acct_fmt_id == 2 && !StringHelper.isBlank(form.getStringSafely("balance", ""))) {
    		Helper.validateRegex(errorMap, form.getString("balance", false), "balance", "^[0-9]{1,3}(?:\\.[0-9]{1,2})?$");
    	}
    	if(errorMap.size()>0){
			form.setAttribute("errorMap", errorMap);
            return;
		}
    	
    	num_cards = form.getInt("num_cards", false, -1);
    	balance = ((form.getString("balance", false) == null ? "" : (form.getString("balance", false))));
    	
    	String exp = form.getString("exp", false);
    	String merchant_id = form.getString("merchant_id", false);
    	if(StringHelper.isBlank(exp))
    		exp = "9912";
    	String exp_year = "20"+exp.substring(0, 2);
    	String exp_month = exp.substring(2, 4);
    	String exp_dislpay = exp_month+"/"+exp_year;
    	
    	String currentTS = dateTimeFormat.format(Calendar.getInstance().getTime());
    	String[] data =  currentTS.split(" ");
    	String dateData = data[0];
    	
    	String[] today = dateData.split("/");
    	String year = today[2];
    	String month = today[0];
    	
    	String create_date = (month.length() == 1 ? "0"+month : month)+"/"+year;
    	
    	if(new Integer(exp_month).intValue() < 1 || new Integer(exp_month).intValue() > 12){
    		if(!(errorMap.containsKey("expiration month")))
				errorMap.put("expiration month", "Invalid parameter: expiration month must be between 01 and 12!");
    	}
    	
    	if(new Integer(exp_year).intValue() < Calendar.getInstance().get(Calendar.YEAR) || new Integer(exp_year).intValue() > 2099){
    		if(!(errorMap.containsKey("expiration month")))
				errorMap.put("expiration month", "Invalid parameter: expiration year must be between " + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2) + " and 99!");
    	}
    	Calendar exp_cal = null;
    	int exp_days = -1;
    	exp_cal = new GregorianCalendar(new Integer(exp_year).intValue(), new Integer(exp_month).intValue()-1, 1); 
    	exp_days = exp_cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    	exp_cal = new GregorianCalendar(new Integer(exp_year).intValue(), new Integer(exp_month).intValue()-1, exp_cal.getActualMaximum(Calendar.DAY_OF_MONTH)); 
    	if(errorMap.size()==0){
	    	
	    	if(exp_cal.before(Calendar.getInstance())){
	    		if(!(errorMap.containsKey("expiration month")))
					errorMap.put("expiration month", "Invalid parameter: expiration year can not be larger than 37!");
	    	}
    	}
    	
    	if(errorMap.size()>0){
			form.setAttribute("errorMap", errorMap);
            return;
		}
        
    	String exp_data = dateTimeFormat.format(exp_cal.getTime());
    	String[] exp_cal_data =  exp_data.split(" ");
    	String exp_cal_date_data = exp_cal_data[0];
    	
    	String[] exp_cal_split = exp_cal_date_data.split("/");
    	String y = exp_cal_split[2];
    	String m = exp_cal_split[0];
    	String d = exp_cal_split[1];
    	
    	if(!StringHelper.isBlank(merchant_id)){
    		try{
    			Results merchant_terminal_info = (Results)DataLayerMgr.executeQuery("GET_MERCHANT_AND_TERMINAL_INFO", new Object[]{merchant_id}, true);
    			if(merchant_terminal_info.next()){
    				form.set("merchant_id",((BigDecimal)merchant_terminal_info.get("merchant_id")).toString());
    				form.set("merchant_desc",(String)merchant_terminal_info.get("merchant_desc"));
    				form.set("merchant_name",(String)merchant_terminal_info.get("merchant_name"));
    				form.set("merchant_cd",(String)merchant_terminal_info.get("merchant_cd"));
    				form.set("terminal_id",((BigDecimal)merchant_terminal_info.get("terminal_id")).toString());
    				form.set("terminal_desc",(String)merchant_terminal_info.get("terminal_desc"));
    			}
    			
    		}catch(Exception e){
	    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
	    	}
    	}
    	request.setAttribute("exp", exp);
    	request.setAttribute("exp_date", (y+"-"+m+"-"+d+" 23:59:59"));
    	request.setAttribute("exp_display", exp_dislpay);
    	request.setAttribute("create_date", create_date);
        
    }
    
    
}
