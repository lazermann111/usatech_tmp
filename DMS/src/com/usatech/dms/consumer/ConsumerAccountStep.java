/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.util.LinkedHashMap;
import simple.util.NameValuePair;

import com.usatech.cardnumbergenerator.cd.Luhn;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.dms.util.DMSValuesList;
import com.usatech.dms.util.ValuesListLoader;
import com.usatech.ec.ECResponse;
import com.usatech.ec2.EC2CardInfo;
import com.usatech.ec2.EC2ClientUtils;
import com.usatech.ec2.EC2ExecutionException;
import com.usatech.layers.common.constants.CommonConstants;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class ConsumerAccountStep extends DMSPaginationStep
{

	protected static final char[] POSSIBLE_PROCESS_CODES = "52013489".toCharArray(); // most frequent first
	protected boolean usingCardInfo = false;
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	try {
	    	String action = form.getString("action", false);
	    	if("Start Search".equalsIgnoreCase(action)){
	    		return;
	    	}
	    	String cardNumber = form.getStringSafely(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_CARD, "").trim();
	    	String accountNumber = form.getStringSafely(ConsumerConstants.PARAM_CONSUMER_ACCOUNT_NUMBER, "").trim();
	        long startCardId = form.getLong(ConsumerConstants.PARAM_CONSUMER_START_CARD_ID, false, 0);
	        long endCardId = form.getLong(ConsumerConstants.PARAM_CONSUMER_END_CARD_ID, false, 0);
	        long consumerMerchantId = form.getLong(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_MERCHANT_ID, false, -1);
	        int consumerAcctTypeId = form.getInt("consumer_acct_type_id", false, 0);
	        int consumerAcctSubTypeId = form.getInt("consumer_acct_sub_type_id", false, 0);
	        
	        StringBuilder condition = new StringBuilder(ConsumerConstants.SQL_CARD_SEARCH_BASE);
	        StringBuilder condition2 = new StringBuilder(ConsumerConstants.SQL_CARD_SEARCH_BASE2);
	        Map<Integer, Object> params = new LinkedHashMap<Integer, Object>();
	        Map<Integer, Object> params2 = new LinkedHashMap<Integer, Object>();
	        int paramsCnt = 0;
	        int paramsCnt2 = 0;
	        
	        if (!StringHelper.isBlank(cardNumber))
	        {
				Long globalAccountId = getInternalGlobalAccountId(cardNumber);
				if(globalAccountId != null) {
					condition.append(" AND cab.global_account_id = CAST (? AS NUMERIC) ");
					params.put(paramsCnt++, globalAccountId);
					condition2.append(" AND cab.global_account_id = CAST (? AS NUMERIC) ");
					params2.put(paramsCnt2++, globalAccountId);
				} else {
					condition.append(" AND 1 = 0 ");
					condition2.append(" AND 1 = 0 ");
				}
			} else if(!StringHelper.isBlank(accountNumber)) {
				DMSValuesList cardTypeList = ValuesListLoader.getValuesList(request, DMSConstants.GLOBAL_LIST_CARD_TYPES);
				int i = 0;
				for(NameValuePair cardType : cardTypeList.getList()) {
					StringBuilder cardBuilder = new StringBuilder();
					cardBuilder.append(CommonConstants.USAT_IIN).append(cardType.getValue());
					String[] possibleCardNumbers;
					if(cardType.getValue().equals("1") && accountNumber.length() == 9) {
						cardBuilder.append('_').append(accountNumber).append('_');
						possibleCardNumbers = new String[POSSIBLE_PROCESS_CODES.length];
						for(int k = 0; k < POSSIBLE_PROCESS_CODES.length; k++) {
							cardBuilder.setCharAt(CommonConstants.USAT_IIN.length() + 1, POSSIBLE_PROCESS_CODES[k]);
							int luhn = Luhn.generate(cardBuilder.substring(0, cardBuilder.length() - 1));
							cardBuilder.setCharAt(cardBuilder.length() - 1, (char) ('0' + luhn));
							possibleCardNumbers[k] = cardBuilder.toString();
						}
					} else if(accountNumber.length() == 11) {
						cardBuilder.append(accountNumber);
						int luhn = Luhn.generate(cardBuilder.toString());
						cardBuilder.append(luhn);
						possibleCardNumbers = new String[] { cardBuilder.toString() };
					} else
						possibleCardNumbers = new String[0];
					for(String possibleCardNumber : possibleCardNumbers) {
						Long globalAccountId = getInternalGlobalAccountId(possibleCardNumber);
						if(globalAccountId != null) {
							if(i++ > 0) {
								condition.append(", ");
								condition2.append(", ");
							} else {
								condition.append(" AND cab.global_account_id IN (");
								condition2.append(" AND cab.global_account_id IN (");
							}
							condition.append("CAST (? AS NUMERIC)");
							params.put(paramsCnt++, globalAccountId);
							condition2.append("CAST (? AS NUMERIC)");
							params2.put(paramsCnt2++, globalAccountId);
						}
					}
	        	}
				if(i == 0) {
					condition.append(" AND 1 = 0 ");
					condition2.append(" AND 1 = 0 ");
				} else {
					condition.append(") ");
					condition2.append(") ");
				}
	        }
	        if (startCardId > 0 && endCardId > 0)
	        {
	        	condition.append(" AND ca.consumer_acct_identifier BETWEEN CAST (? AS NUMERIC) AND CAST (? AS NUMERIC) ");
	        	params.put(paramsCnt++, startCardId);
	        	params.put(paramsCnt++, endCardId);
	        	condition2.append(" AND cab.global_account_id BETWEEN CAST (? AS NUMERIC) AND CAST (? AS NUMERIC) ");
	        	params2.put(paramsCnt2++, startCardId);
	        	params2.put(paramsCnt2++, endCardId);
	        }
	        else if (startCardId > 0)
	        {
	        	condition.append(" AND ca.consumer_acct_identifier >= CAST (? AS NUMERIC) ");
	        	params.put(paramsCnt++, startCardId);
	        	condition2.append(" AND cab.global_account_id >= CAST (? AS NUMERIC) ");
	        	params2.put(paramsCnt2++, startCardId);
	        }
	        else if (endCardId > 0)
	        {
	        	condition.append(" AND ca.consumer_acct_identifier <= CAST (? AS NUMERIC) ");
	        	params.put(paramsCnt++, endCardId);
	        	condition2.append(" AND cab.global_account_id <= CAST (? AS NUMERIC) ");
	        	params2.put(paramsCnt2++, endCardId);
	        }
	        if(consumerMerchantId > 0)
	        {
	            condition.append(" AND ca.consumer_acct_id IN (SELECT consumer_acct_id FROM pss.merchant_consumer_acct WHERE merchant_id = CAST (? AS NUMERIC)) ");
	            params.put(paramsCnt++, consumerMerchantId);
	            condition2.append(" AND cab.consumer_acct_id IN (SELECT consumer_acct_id FROM pss.merchant_consumer_acct WHERE merchant_id = CAST (? AS NUMERIC)) ");
	            params2.put(paramsCnt2++, consumerMerchantId);
	        }
	        if (consumerAcctTypeId > 0)
	        {
	        	condition.append(" AND ca.consumer_acct_type_id = CAST (? AS NUMERIC) ");
	        	params.put(paramsCnt++, consumerAcctTypeId);
	        	condition2.append(" AND ca.consumer_acct_type_id = CAST (? AS NUMERIC) ");
	        	params2.put(paramsCnt2++, consumerAcctTypeId);
	        }
	        if (consumerAcctSubTypeId > 0)
	        {
	        	condition.append(" AND ca.consumer_acct_sub_type_id = CAST (? AS NUMERIC) ");
	        	params.put(paramsCnt++, consumerAcctSubTypeId);
	        	condition2.append(" AND ca.consumer_acct_sub_type_id = CAST (? AS NUMERIC) ");
	        	params2.put(paramsCnt2++, consumerAcctSubTypeId);
	        }

    		String storedNames = PaginationUtil.encodeStoredNames(new String[]{
					ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ID,
					ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_CARD,
					ConsumerConstants.PARAM_CONSUMER_ACCOUNT_NUMBER,
					ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_MERCHANT_ID,
					ConsumerConstants.PARAM_CONSUMER_START_CARD_ID,
					ConsumerConstants.PARAM_CONSUMER_END_CARD_ID,
					"num_accounts", "consumer_acct_type_id", "consumer_acct_sub_type_id"
			});
    		request.setAttribute("storedNames", storedNames);
    		
    		String sortField = PaginationUtil.getSortField(null);
    	    String sortIndex = form.getString(sortField, false);
    	    sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "1" : sortIndex;
    	    
    	    String queryBase = new StringBuilder(" FROM (").append(ConsumerConstants.SQL_CARD_SEARCH_START).append(condition.toString()).append(" UNION ").append(ConsumerConstants.SQL_CARD_SEARCH_START).append(condition2.toString()).append(") subq ").toString();
    	    
	    	Object[] inParams = null;
	    	if (params.size() > 0 || params2.size() > 0) {
	    		inParams = new Object[params.size() + params2.size()];
	    	if (params.size() > 0)
	    		System.arraycopy(params.values().toArray(), 0, inParams, 0, params.size());
	    	if (params2.size() > 0)
	    		System.arraycopy(params2.values().toArray(), 0, inParams, params.size(), params2.size());
	    	}
	    	  
			if (inParams == null || inParams.length == 0)
				if (StringHelper.isBlank(accountNumber)) {
					request.setAttribute("errorMessage", "At least one search parameter is required.");
					return;
				}
    	    
    		setPaginatedResultsOnRequest(form, request, ConsumerConstants.SQL_CARD_SEARCH_START_OUTER, queryBase, "", ConsumerConstants.DEFAULT_SORT_INDEX_CARD_SEARCH,
            		ConsumerConstants.SORT_FIELDS__CARD_SEARCH, "consumer_accounts", inParams,
            		"consumerAcctId", "editConsumerAcct.i?consumer_acct_id=");
    	}catch(Exception e){
    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}
    }

	protected Long getInternalGlobalAccountId(String cardNumber) throws EC2ExecutionException {
		if(isUsingCardInfo()) {
			EC2CardInfo cardInfo = EC2ClientUtils.getEportConnect().getCardInfoPlain(null, null, EC2ClientUtils.getVirtualDeviceSerialCd(), cardNumber, "N", null);
			switch(cardInfo.getReturnCode()) {
				case ECResponse.RES_APPROVED:
				case ECResponse.RES_PARTIALLY_APPROVED:
					if(cardInfo.getCardId() > 0)
						return cardInfo.getCardId();
			}
			return null;
		}
		return EC2ClientUtils.getGlobalAccountId(cardNumber);
	}

	public boolean isUsingCardInfo() {
		return usingCardInfo;
	}

	public void setUsingCardInfo(boolean usingCardInfo) {
		this.usingCardInfo = usingCardInfo;
	}
}
