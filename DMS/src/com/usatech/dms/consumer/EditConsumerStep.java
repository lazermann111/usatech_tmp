/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class EditConsumerStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String consumerId = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ID, false);
        com.usatech.dms.model.Consumer consumer = null;

        if (consumerId != null && !consumerId.trim().equals(""))
        {
        	try{
	        	consumer = com.usatech.dms.action.ConsumerAction.getConsumer(consumerId);
	        	request.setAttribute(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER, consumer);
        	}catch(Exception e){
        		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        	}
        }

    }

}
