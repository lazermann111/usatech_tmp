/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;


public class ConsumerConstants
{        
	//consumer specific
	public static final String STORED_NVP_CONSUMER_MERCHANT_FORMATTED = "consumer_merchant_formatted";
	public static final String STORED_NVP_CONSUMER_ASSIGNED_LOCATION_FORMATTED = "consumer_assigned_location_formatted";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_CARD = "consumer_card_number";
	public static final String PARAM_CONSUMER_ACCOUNT_NUMBER = "account_number";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER = "consumer";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_TRANSACTIONS = "consumer_transactions";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_ACCOUNTS_FOR_DOWNLOAD = "consumer_accounts_download";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_ID = "consumer_id";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_ACCT_ID = "consumer_acct_id";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_ACCT = "consumer_acct";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_CARD_RESULTS = "consumerCardResults";
	public static final String PARAM_CONSUMER_CURRENT_FIRST_NAME = "consumer_first_name";
	public static final String PARAM_CONSUMER_CURRENT_MIDDLE_NAME = "consumer_middle_name";
	public static final String PARAM_CONSUMER_CURRENT_FIRST_NAME_SEARCH_CHAR = "consumer_first_name_search_char";
	public static final String PARAM_CONSUMER_CURRENT_LAST_NAME = "consumer_last_name";
	public static final String PARAM_CONSUMER_CURRENT_LAST_NAME_SEARCH_CHAR = "consumer_last_name_search_char";
	public static final String PARAM_CONSUMER_CURRENT_PRIMARY_EMAIL = "consumer_primary_email";
	public static final String PARAM_CONSUMER_CURRENT_PRIMARY_EMAIL_SEARCH_CHAR = "consumer_primary_email_search_char";
	public static final String PARAM_CONSUMER_CURRENT_NOTIFICATION_EMAIL = "consumer_notification_email";
	public static final String PARAM_CONSUMER_CURRENT_ADDRESS1 = "consumer_addr1";
	public static final String PARAM_CONSUMER_CURRENT_ADDRESS2 = "consumer_addr2";
	public static final String PARAM_CONSUMER_CURRENT_STATE = "consumer_state_cd";
	public static final String PARAM_CONSUMER_CURRENT_COUNTRY = "consumer_country_cd";
	public static final String PARAM_CONSUMER_CURRENT_CITY = "consumer_city";
	public static final String PARAM_CONSUMER_CURRENT_POSTAL_CD = "consumer_postal_cd";
	public static final String PARAM_CONSUMER_CURRENT_COUNTY = "consumer_county";
	public static final String PARAM_CONSUMER_CURRENT_ACTIVE_FLAG = "consumer_active_flag";
	public static final String PARAM_CONSUMER_CURRENT_TITLE = "consumer_title";
	public static final String PARAM_CONSUMER_CURRENT_SALUTATION = "consumer_salutation";
	public static final String PARAM_CONSUMER_CURRENT_HOME_PHONE = "consumer_home_phone";
	public static final String PARAM_CONSUMER_CURRENT_CELL_PHONE = "consumer_cell_phone";
	public static final String PARAM_CONSUMER_CURRENT_FAX = "consumer_fax_phone";
	public static final String PARAM_CONSUMER_CURRENT_PAGER = "consumer_pager_cd";
	public static final String PARAM_CONSUMER_CURRENT_TYPE_ID = "consumer_type_id";
	public static final String PARAM_CONSUMER_CURRENT_NOTIFICATION_EMAIL_SEARCH_CHAR = "consumer_notification_email_search_char";
	public static final String PARAM_CONSUMER_CURRENT_ASSIGNED_LOCATION_ID = "consumer_assigned_location_id";
	public static final String PARAM_CONSUMER_CURRENT_ASSIGNED_LOCATION_NAME = "consumer_assigned_location_name";
	public static final String PARAM_CONSUMER_CURRENT_ASSIGNED_LOCATION_LOOKUP_CHARS = "consumer_assigned_location_lookup_chars";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_MERCHANT_ID = "consumer_merchant_id";
	public static final String PARAM_CONSUMER_CURRENT_CONSUMER_MERCHANT_NAME = "consumer_merchant_name";
	public static final String PARAM_CONSUMER_END_CARD_ID = "end_card_id";
	public static final String PARAM_CONSUMER_START_CARD_ID = "start_card_id";
	    
    public static final String SQL_CONSUMER_CARD_SEARCH_BASE = " FROM ("
    	+ " SELECT c.consumer_id id,"
        + " c.consumer_fname fname,"
        + " c.consumer_lname lname,"
        + " c.consumer_email_addr1 emailAddress1,"
        + " c.consumer_email_addr2 emailAddress2"
        + " FROM pss.consumer c "
        + " WHERE 1 = 1 ";
    public static final String SQL_CONSUMER_CARD_SEARCH_START = "SELECT id,"
        + " fname,"
        + " lname,"
        + " emailAddress1,"
        + " emailAddress2";
    
    public static final String SQL_END = ") sq_end ";
    
    public static final String SQL_CONSUMER_INFO_TRANSACTIONS_START = " SELECT id,"
	    + " startTime,"
	    + " stateName,"
	    + " consumerAcctCd,"
	    + " consumerAcctId,"
	    + " deviceSerialCd,"
	    + " deviceId,"
	    + " uploadTime,"
	    + " endTime,"
	    + " stateCd,"
	    + " tranDesc," 
	    + " deviceTranCd,"
	    + " createdBy,"
	    + " createdDate,"
	    + " lastUpdatedBy,"
	    + " lastUpdatedDate,"
	    + " posPtaId,"
	    + " deviceResultTypeCd,"
	    + " glogalTransCd,"
	    + " legacyTransNo,"
	    + " parentTranId,"
	    + " previousTranStateCd";
    
	public static final String SQL_CONSUMER_INFO_TRANSACTIONS_BASE = " FROM ("
		+ " SELECT /*+ INDEX(T INX_TRANCONSACCTID) */ t.tran_id as id,"
	    + " to_char(t.tran_start_ts, 'MM/DD/YYYY HH24:MI:SS') as startTime,"
	    + " ts.tran_state_desc as stateName,"
	    + " ca.consumer_acct_cd as consumerAcctCd,"
	    + " ca.consumer_acct_id as consumerAcctId,"
	    + " d.device_serial_cd as deviceSerialCd,"
	    + " d.device_id as deviceId,"
	    + " case when t.tran_upload_ts is null then '' else to_char(t.tran_upload_ts, 'MM/DD/YYYY HH24:MI:SS') end as uploadTime,"
	    + " case when t.tran_end_ts is null then '' else to_char(t.tran_end_ts, 'MM/DD/YYYY HH24:MI:SS') end as endTime,"
	    + " t.tran_state_cd as stateCd,"
	    + " null tranDesc," 
	    + " t.tran_device_tran_cd as deviceTranCd,"
	    + " t.created_by as createdBy,"
	    + " to_char(t.created_ts,  'MM/DD/YYYY HH24:MI:SS') as createdDate,"
	    + " t.last_updated_by as lastUpdatedBy,"
	    + " case when t.last_updated_ts is null then '' else to_char(t.last_updated_ts, 'MM/DD/YYYY HH24:MI:SS') end  as lastUpdatedDate,"
	    + " t.pos_pta_id as posPtaId,"
	    + " t.tran_device_result_type_cd as deviceResultTypeCd,"
	    + " t.tran_global_trans_cd as glogalTransCd,"
	    + " null legacyTransNo,"
	    + " t.parent_tran_id as parentTranId,"
	    + " t.previous_tran_state_cd as previousTranStateCd"
	    + " FROM pss.consumer_acct ca,"
	    + " pss.tran t,"
	    + " pss.pos p,"
	    + " device.device_type dt,"
	    + " device.device d,"
	    + " pss.pos_pta pp,"
	    + " pss.tran_state ts"
	    + " WHERE ca.consumer_acct_id = t.consumer_acct_id"
	    + " AND t.pos_pta_id = pp.pos_pta_id"
	    + " AND pp.pos_id = p.pos_id"
	    + " AND p.device_id = d.device_id"
	    + " AND d.device_type_id = dt.device_type_id"
	    + " AND t.tran_state_cd = ts.tran_state_cd";
	
	
	public static final String SQL_CARD_SEARCH_START = " SELECT cab.consumer_acct_id as consumerAcctId, "
		+ " COALESCE(ca.consumer_acct_cd, cab.truncated_card_number) as consumerAcctCd, "
		+ " ca.consumer_acct_balance as consumerAcctBalance, "
		+ " l.location_id as locationId, "
		+ " l.location_name as locationName, "
		+ " ca.consumer_acct_active_yn_flag as consumerAcctActiveFlag, "
		+ " ca.consumer_acct_type_id as consumerAcctTypeId, "
		+ " COALESCE(cat.consumer_acct_type_desc, ps.payment_subtype_name) as consumerAcctTypeDesc, "
		+ " ca.consumer_acct_sub_type_id as consumerAcctSubTypeId, "
		+ " cst.consumer_acct_sub_type_desc as consumerAcctSubTypeDesc, "
		+ " ca.CONSUMER_ID as consumerId, "
		+ " ca.CREATED_BY as createdBy, "
		+ " ca.CREATED_TS as createdDate, "
		+ " ca.LAST_UPDATED_BY as lastUpdatedBy, "
		+ " ca.LAST_UPDATED_TS as lastUpdatedDate, "
		+ " ca.CONSUMER_ACCT_CONFIRMATION_CD as consumerAcctConfCd, "
		+ " ca.PAYMENT_SUBTYPE_ID as paymentSubtypeId, "
		+ " ca.CONSUMER_ACCT_ISSUE_NUM as consumerAcctIssueNum, "
		+ " ca.CONSUMER_ACCT_ACTIVATION_TS as consumerAcctActivationDate, "
		+ " ca.CONSUMER_ACCT_DEACTIVATION_TS as consumerAcctDeactivationDate, "
		+ " ca.CURRENCY_CD as currencyCd, "
		+ " ca.CONSUMER_ACCT_BALREF_AMT as consumerAcctBalrefAmt, "
		+ " ca.CONSUMER_ACCT_BALREF_PERIOD_HR as consumerAcctBalrefPeriodHr, "
		+ " ca.CONSUMER_ACCT_LAST_BALREF_TS as consumerAcctLastBalrefDate, "
		+ " ca.CONSUMER_ACCT_VALIDATION_CD as consumerAcctValidationCd, "
		+ " ca.CONSUMER_ACCT_ISSUE_CD as consumerAcctIssueCd, "
		+ " COALESCE(ca.consumer_acct_identifier, cab.global_account_id) as consumerAcctIdentifier ";
	
	public static final String SQL_CARD_SEARCH_START_OUTER = " SELECT consumerAcctId, "
		+ " consumerAcctCd, "
		+ " consumerAcctBalance, "
		+ " locationId, "
		+ " locationName, "
		+ " consumerAcctActiveFlag, "
		+ " consumerAcctTypeId, "
		+ " consumerAcctTypeDesc, "
		+ " consumerAcctSubTypeId, "
		+ " consumerAcctSubTypeDesc, "
		+ " consumerId, "
		+ " createdBy, "
		+ " createdDate, "
		+ " lastUpdatedBy, "
		+ " lastUpdatedDate, "
		+ " consumerAcctConfCd, "
		+ " paymentSubtypeId, "
		+ " consumerAcctIssueNum, "
		+ " consumerAcctActivationDate, "
		+ " consumerAcctDeactivationDate, "
		+ " currencyCd, "
		+ " consumerAcctBalrefAmt, "
		+ " consumerAcctBalrefPeriodHr, "
		+ " consumerAcctLastBalrefDate, "
		+ " consumerAcctValidationCd, "
		+ " consumerAcctIssueCd, "
		+ " consumerAcctIdentifier ";	
	
	public static final String SQL_CARD_SEARCH_BASE = " FROM pss.consumer_acct ca "
		+ " JOIN location.location l ON ca.location_id = l.location_id "
		+ " JOIN pss.consumer_acct_type cat ON ca.consumer_acct_type_id = cat.consumer_acct_type_id "
		+ " JOIN pss.consumer_acct_base cab ON ca.consumer_acct_id = cab.consumer_acct_id "
		+ " LEFT OUTER JOIN pss.consumer_acct_sub_type cst ON ca.consumer_acct_sub_type_id = cst.consumer_acct_sub_type_id "
		+ " LEFT OUTER JOIN pss.payment_subtype ps ON cab.payment_subtype_id = ps.payment_subtype_id "
		+ " WHERE 1 = 1 ";

	public static final String SQL_CARD_SEARCH_BASE2 = " FROM pss.consumer_acct_base cab "
		+ " LEFT OUTER JOIN pss.consumer_acct ca ON cab.consumer_acct_id = ca.consumer_acct_id "
		+ " LEFT OUTER JOIN location.location l ON ca.location_id = l.location_id "
		+ " LEFT OUTER JOIN pss.consumer_acct_type cat ON ca.consumer_acct_type_id = cat.consumer_acct_type_id "	
		+ " LEFT OUTER JOIN pss.consumer_acct_sub_type cst ON ca.consumer_acct_sub_type_id = cst.consumer_acct_sub_type_id "
		+ " LEFT OUTER JOIN pss.payment_subtype ps ON cab.payment_subtype_id = ps.payment_subtype_id "
		+ " WHERE 1 = 1 ";
	
    /**
     * Make sure these are in the same order as the columns on the screen.
     */
    public static final String[] SORT_FIELDS__CONSUMER_CARD_SEARCH = {"id",
        "LOWER(fname)",
        "LOWER(lname)",
        "LOWER(emailAddress1)"
        };
    
    public static final String[] SORT_FIELDS__CARD_SEARCH = {"consumerAcctId",
    	"consumerAcctCd",
    	"consumerAcctIdentifier",
        "consumerAcctBalance",
        "locationName", 
        "consumerAcctTypeDesc",
        "consumerAcctSubTypeDesc",
        "consumerAcctActiveFlag"
        };
    
    /**
     * Make sure these are in the same order as the columns on the screen.
     */
    public static final String[] SORT_FIELDS__CONSUMER_INFO_TRANSACTIONS = {"id", // Consumer Id
        "t.tran_start_ts", // First Name
        "LOWER(stateName)", // Last Name
        "consumerAcctCd", // Number of cards per consumer
        "deviceSerialCd"// Number of trans per consumer
        };
    
    public static final String DEFAULT_SORT_INDEX_CONSUMER_CARD_SEARCH = "-1";
    
    public static final String DEFAULT_SORT_INDEX_CONSUMER_INFO_TRANSACTIONS = "-2";
    
    public static final String DEFAULT_SORT_INDEX_CARD_SEARCH = "-1";
    
    public static String DEFAULT_eSuds_DOMAIN_NAME = "esuds.net";
    
}