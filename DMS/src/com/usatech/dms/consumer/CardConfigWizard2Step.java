package com.usatech.dms.consumer;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import simple.app.DialectResolver;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.security.SecureHash;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;
import simple.text.StringUtils;

public class CardConfigWizard2Step extends AbstractStep {
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try
        {
            String cardIdList = form.getString("card_id_list", false);
			String cardNumList = form.getString("card_num_list", false);
            Results rs = null;
			if(!StringHelper.isBlank(cardNumList)) {
				rs = searchByCardNumbersList(cardNumList);
			} else if(!StringHelper.isBlank(cardIdList)) {
	        	Map<String, Object> params = new HashMap<String, Object>();
	        	params.put("consumerAcctIdentifiers", cardIdList.replace('\n', ',').replace("\r", "").replace(" ", ""));
	        	rs = DataLayerMgr.executeQuery("GET_BULK_CARD_LIST", params, false);
	        } else {
	        	long corpCustomerId = form.getLong("corp_customer_id", false, 0);
	            long locationId = form.getLong("location_id", false, 0);
	            int consumerAcctTypeId = form.getInt("consumer_acct_type_id", false, 0);
	            int consumerAcctSubTypeId = form.getInt("consumer_acct_sub_type_id", false, 0);
	            long startCardId = Helper.isLong(form.getString("start_card_id", false)) ? form.getLong("start_card_id", false, 0) : 0;
	            long endCardId = Helper.isLong(form.getString("end_card_id", false)) ? form.getLong("end_card_id", false, 0) : 0;
	            long consumerMerchantId = form.getLong("consumer_merchant_id", false, 0);
	            String allowNegativeBalance = form.getStringSafely("allow_negative_balance", "");
	            int maxCount = form.getInt("max_count", false, 20);
	        	
	        	StringBuilder sql = new StringBuilder();
		        sql.append("SELECT * FROM (SELECT ca.consumer_acct_id consumerAcctId, ca.consumer_acct_identifier consumerAcctIdentifier, ca.consumer_acct_cd consumerAcctCd, ca.consumer_acct_balance consumerAcctBalance");
				sql.append(", ca.consumer_acct_active_yn_flag consumerAcctActiveFlag, ca.currency_cd currencyCd, ca.consumer_acct_promo_total consumerAcctPromoTotal, ca.consumer_acct_replenish_total");
				sql.append(", ca.consumer_acct_type_id consumerAcctTypeId, cat.consumer_acct_type_desc consumerAcctTypeDesc, c.customer_id corpCustomerId, c.customer_name corpCustomerName, l.location_id locationId, l.location_name locationName");
				sql.append(", ca.consumer_acct_sub_type_id consumerAcctSubTypeId, cst.consumer_acct_sub_type_desc consumerAcctSubTypeDesc, ca.consumer_acct_promo_balance, ca.consumer_acct_replen_balance, ca.allow_negative_balance allowNegativeBalance, TO_CHAR(ca.CONSUMER_ACCT_DEACTIVATION_TS, 'MM/DD/YYYY') as consumerAcctDeactivationDate");
				sql.append(" FROM pss.consumer_acct ca");
				sql.append(" JOIN pss.consumer_acct_type cat ON ca.consumer_acct_type_id = cat.consumer_acct_type_id");
				sql.append(" LEFT OUTER JOIN pss.consumer_acct_sub_type cst ON ca.consumer_acct_sub_type_id = cst.consumer_acct_sub_type_id");
				sql.append(" LEFT OUTER JOIN corp.customer c ON ca.corp_customer_id = c.customer_id");
				sql.append(" LEFT OUTER JOIN location.location l ON ca.location_id = l.location_id"); 
				sql.append(" WHERE 1 = 1");
		        
		        int paramsCnt = 0;
		        List<Object> params = new ArrayList<Object>();
		        
		        if (corpCustomerId > 0)
		        {
		            sql.append(" AND ca.corp_customer_id = CAST (? AS NUMERIC)");
		            params.add(paramsCnt++, corpCustomerId);
		        }
		        if (locationId > 0)
		        {
		            sql.append(" AND ca.location_id = CAST (? AS NUMERIC)");
		            params.add(paramsCnt++, locationId);
		        }
		        if (consumerAcctTypeId > 0)
		        {
		            sql.append(" AND ca.consumer_acct_type_id = CAST (? AS NUMERIC)");
		            params.add(paramsCnt++, consumerAcctTypeId);
		        }
		        if (consumerAcctSubTypeId > 0)
		        {
		            sql.append(" AND ca.consumer_acct_sub_type_id = CAST (? AS NUMERIC)");
		            params.add(paramsCnt++, consumerAcctSubTypeId);
		        }
		        if (startCardId > 0 && endCardId > 0)
		        {
		        	sql.append(" AND ca.consumer_acct_identifier BETWEEN CAST (? AS NUMERIC) AND CAST (? AS NUMERIC) ");
		        	params.add(paramsCnt++, startCardId);
		        	params.add(paramsCnt++, endCardId);
		        }
		        else if (startCardId > 0)
		        {
		        	sql.append(" AND ca.consumer_acct_identifier >= CAST (? AS NUMERIC) ");
		        	params.add(paramsCnt++, startCardId);
		        }
		        else if (endCardId > 0)
		        {
		        	sql.append(" AND ca.consumer_acct_identifier <= CAST (? AS NUMERIC) ");
		        	params.add(paramsCnt++, endCardId);
		        }
		        if (consumerMerchantId > 0)
		        {
		        	sql.append(" AND ca.consumer_acct_id IN (SELECT consumer_acct_id FROM pss.merchant_consumer_acct WHERE merchant_id = CAST (? AS NUMERIC)) ");
		        	params.add(paramsCnt++, consumerMerchantId);
		        }
		        if ("Y".equalsIgnoreCase(allowNegativeBalance))
		        	sql.append(" AND ca.allow_negative_balance = 'Y' ");
		        else if ("N".equalsIgnoreCase(allowNegativeBalance))
		        	sql.append(" AND (ca.allow_negative_balance IS NULL OR ca.allow_negative_balance = 'N') ");
		        
		        params.add(paramsCnt++, maxCount);
	        	sql.append(" ORDER BY cat.consumer_acct_type_desc, cst.consumer_acct_sub_type_desc, c.customer_name, l.location_name, COALESCE(ca.consumer_acct_identifier, ca.consumer_acct_id)) sq_end"
	        		+ (!DialectResolver.isOracle() ? " LIMIT ?::numeric " : " WHERE ROWNUM <= ?"));
	        		
	        	rs = DataLayerMgr.executeSQL("OPER", sql.toString(), params.toArray(), null);
	        }
	        request.setAttribute("results", rs);
        } catch (Exception e) {
            throw new ServletException("Error retrieving cards", e);
        }
    }

	private Results searchByCardNumbersList(String cardNumList) throws SQLException, DataLayerException, NoSuchAlgorithmException {
		Map<String, Object> params = new HashMap<String, Object>();
		String[] cardNumbers = StringUtils.splitNumbers(cardNumList);
		String[] hashedCardNumbers = hashCardNumbers(cardNumbers);
		params.put("hashedCardNumbers", hashedCardNumbers);
		return DataLayerMgr.executeQuery("GET_BULK_CARD_LIST_BY_NUM", params, false);
	}

	private String[] hashCardNumbers(String[] unhashedCardNumbers) throws NoSuchAlgorithmException {
		if (unhashedCardNumbers == null) {
			return null;
		}
		String[] rv = new String[unhashedCardNumbers.length];
		int i = 0;
		for (String unHashedCardNum : unhashedCardNumbers) {
			rv[i] = DatatypeConverter.printHexBinary(SecureHash.getUnsaltedHash(unHashedCardNum.getBytes()));
			i++;
		}
		return rv;
	}
}
