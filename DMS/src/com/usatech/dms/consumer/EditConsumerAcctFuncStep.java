/*
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerMgr;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConsumerAction;
import com.usatech.dms.model.ConsumerAccount;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.util.StringHelper;

public class EditConsumerAcctFuncStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
		
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
	{
		Map errorMap = new HashMap();
		
		String myAction = form.getString("action", false);
		if(StringHelper.isBlank(myAction))
			myAction = form.getString("myaction", false);
		long consumerAcctId = form.getLong(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ACCT_ID, false, -1);
		ConsumerAccount consumerAcct = null;
		boolean success = false;
		Connection conn = null;
		try
		{
			if (!StringHelper.isBlank(myAction)) {
				String msg = "";
				if (myAction.equalsIgnoreCase("Clear Holds"))
				{
					
					String[] holds = request.getParameterValues("clear_hold");
					if(holds!=null && holds.length>0){
						for (int i = 0; i < holds.length; ++i){						
							DataLayerMgr.executeUpdate("DELETE_CONSUMER_ACCT_HOLD", new Object[]{consumerAcctId, holds[i]}, true);					  
						}
					}
					msg = "Consumer+account+holds+cleared";
				}
				else if (myAction.equalsIgnoreCase("Clear All Holds"))
				{						
					DataLayerMgr.executeUpdate("DELETE_CONSUMER_ACCT_HOLDS", new Object[]{consumerAcctId}, true);
					msg = "All+consumer+account+holds+cleared";
				}
				else if (myAction.equalsIgnoreCase("Update"))
				{
					//get the current values from the db
					consumerAcct = ConsumerAction.getConsumerAccountByConsumerAccountId(new Long(consumerAcctId).toString());
					Long oldCorpCustomerId = consumerAcct.getCorpCustomerId();
					BigDecimal oldPromoBalance = new BigDecimal(consumerAcct.getConsumerAcctPromoBalance());
					Long oldConsumerAcctSubTypeId = consumerAcct.getConsumerAcctSubTypeId();
					BeanUtils.populate(consumerAcct, request.getParameterMap());
					Map<String, Object> params = (Map<String, Object>) ReflectionUtils.toPropertyMap(consumerAcct);
					Long corpCustomerId = consumerAcct.getCorpCustomerId();
					BigDecimal oldBalance = form.getBigDecimal("oldConsumerAcctBalance", false);
					BigDecimal balance = form.getBigDecimal("consumerAcctBalance", false);
					BigDecimal balanceChange = (oldBalance == null || balance == null ? BigDecimal.ZERO : balance.subtract(oldBalance));
					params.put("consumerAcctBalanceChange", balanceChange);
					conn = DataLayerMgr.getConnection("OPER");
					params.put("currencyCd", form.get("currencyCd"));
					
					if(!(StringHelper.isBlank(form.getString("consumerAcctDeactivationDateYear", false))))
		    			Helper.validateRegex(errorMap, form.getString("consumerAcctDeactivationDateYear", false), "consumerAcctDeactivationDateYear", "^([0-9]{4})$");
					if(!(StringHelper.isBlank(form.getString("consumerAcctDeactivationDateMonth", false))))
		    			Helper.validateRegex(errorMap, form.getString("consumerAcctDeactivationDateMonth", false), "consumerAcctDeactivationDateMonth", "^([0-9]{2})$");
					if(!(StringHelper.isBlank(form.getString("topUpAmt", false))))
						Helper.validateRegex(errorMap, form.getString("topUpAmt", false), "topUpAmt", "^[0-9]{1,6}(?:\\.[0-9]{1,2})?$");
					
					if(errorMap.size()>0){
						form.setAttribute("errorMap", errorMap);
			            return;
					}
										
					String exp_year = form.getString("consumerAcctDeactivationDateYear", false);
					String exp_month = form.getString("consumerAcctDeactivationDateMonth", false);
					
					if (!(StringHelper.isBlank(form.getString("consumerAcctDeactivationDateMonth", false)))) {
						if((exp_month.charAt(0)>'1')||((exp_month.charAt(0)=='1')&&(exp_month.charAt(1)>'2')||((exp_month.charAt(0)=='0')&&(exp_month.charAt(1)=='0'))))
							errorMap.put("expiration month", "Invalid parameter: expiration month must be between 01 and 12!"); 
						if (StringHelper.isBlank(form.getString("consumerAcctDeactivationDateYear", false)))
							errorMap.put("expiration year", "Invalid parameter: expiration year missed!");
					}else
						if (!(StringHelper.isBlank(form.getString("consumerAcctDeactivationDateYear", false))))
							errorMap.put("expiration year", "Invalid parameter: expiration month missed!");
					
					if(errorMap.size()>0){
						form.setAttribute("errorMap", errorMap);
			            return;
					}
					
					if ((!(StringHelper.isBlank(form.getString("consumerAcctDeactivationDateYear", false))))&&(!(StringHelper.isBlank(form.getString("consumerAcctDeactivationDateMonth", false))))) {
						Calendar exp_cal = null;
						exp_cal = new GregorianCalendar(new Integer(exp_year).intValue(), new Integer(exp_month).intValue()-1, 1);
						exp_cal = new GregorianCalendar(new Integer(exp_year).intValue(), new Integer(exp_month).intValue()-1, exp_cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59);
						params.put("consumer_acct_deactivation_ts", exp_cal.getTime());
					}else params.put("consumer_acct_deactivation_ts", null);
					
					DataLayerMgr.executeUpdate(conn, "UPDATE_CONSUMER_ACCOUNT", params);
					BigDecimal effectiveBalanceChange = ConvertUtils.convert(BigDecimal.class, params.get("effectiveConsumerAcctBalanceChange"));
					BigDecimal promoBalance = oldPromoBalance.add(effectiveBalanceChange);
					msg = "Consumer+account+updated";
					if (consumerAcct.getConsumerAcctTypeId() == 3) {
						// Prepaid Account
						params.put("currencyCd", consumerAcct.getCurrencyCd());
						if (consumerAcct.getConsumerAcctSubTypeId() == 1) {
							// USAT Serviced
							BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
							params.put("username", user.getEmailAddress());
							if (corpCustomerId.compareTo(oldCorpCustomerId) != 0) {
								if (oldPromoBalance.compareTo(BigDecimal.ZERO) > 0) {
									params.put("corpCustomerId", oldCorpCustomerId);
									params.put("amount", oldPromoBalance);
									params.put("reason", "Credit for " + String.format("%.2f", oldPromoBalance) + " " + consumerAcct.getCurrencyCd() + " for removing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier());						
									Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
									msg += ",+EFT+" + ConvertUtils.getStringSafely(result[1], "") + "+credited";
								}
								if (promoBalance.compareTo(BigDecimal.ZERO) > 0) {
									params.put("corpCustomerId", corpCustomerId);
									params.put("amount", promoBalance.negate());
									params.put("reason", "Charge for " + String.format("%.2f", promoBalance) + " " + consumerAcct.getCurrencyCd() + " for adding the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier());						
									Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
									msg += ",+EFT+" + ConvertUtils.getStringSafely(result[1], "") + "+charged";
								}
							} else if (effectiveBalanceChange.compareTo(BigDecimal.ZERO) != 0) {
								msg += ",+balance+change:+" + String.format("%.2f", effectiveBalanceChange);
								params.put("corpCustomerId", corpCustomerId);
								params.put("amount", effectiveBalanceChange.negate());
								if (effectiveBalanceChange.compareTo(BigDecimal.ZERO) > 0) {								
									params.put("reason", "Charge for " + String.format("%.2f", effectiveBalanceChange.abs()) + " " + consumerAcct.getCurrencyCd() + " balance increase on the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier());						
									Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
									msg += ",+EFT+" + ConvertUtils.getStringSafely(result[1], "") + "+charged";
								} else if (effectiveBalanceChange.compareTo(BigDecimal.ZERO) < 0) {
									params.put("reason", "Credit for " + String.format("%.2f", effectiveBalanceChange.abs()) + " " + consumerAcct.getCurrencyCd() + " balance decrease on the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier());						
									Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
									msg += ",+EFT+" + ConvertUtils.getStringSafely(result[1], "") + "+credited";
								}
							}
						}
						if (balance.compareTo(BigDecimal.ZERO) != 0) {
							if (oldConsumerAcctSubTypeId == 1 && consumerAcct.getConsumerAcctSubTypeId() == 2) {
								// from USAT Serviced to Operator Serviced
								msg += ",+subtype+change";
								params.put("corpCustomerId", corpCustomerId);
								params.put("amount", balance);
								if (balance.compareTo(BigDecimal.ZERO) < 0) {								
									params.put("reason", "Charge for " + String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd() + " for changing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier() + " with " + String.format("%.2f", balance) + " " + consumerAcct.getCurrencyCd() + " balance from USAT Serviced to Operator Serviced");						
									Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
									msg += ",+EFT+" + ConvertUtils.getStringSafely(result[1], "") + "+charged";
								} else if (balance.compareTo(BigDecimal.ZERO) > 0) {
									params.put("reason", "Credit for " + String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd() + " for changing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier() + " with " + String.format("%.2f", balance) + " " + consumerAcct.getCurrencyCd() + " balance from USAT Serviced to Operator Serviced");						
									Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
									msg += ",+EFT+" + ConvertUtils.getStringSafely(result[1], "") + "+credited";
								}
							} else if (oldConsumerAcctSubTypeId == 2 && consumerAcct.getConsumerAcctSubTypeId() == 1) {
								// from Operator Serviced to USAT Serviced
								msg += ",+subtype+change";
								params.put("corpCustomerId", corpCustomerId);
								params.put("amount", balance.negate());
								if (balance.compareTo(BigDecimal.ZERO) > 0) {								
									params.put("reason", "Charge for " + String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd() + " for changing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier() + " with " + String.format("%.2f", balance) + " " + consumerAcct.getCurrencyCd() + " balance from Operator Serviced to USAT Serviced");						
									Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
									msg += ",+EFT+" + ConvertUtils.getStringSafely(result[1], "") + "+charged";
								} else if (balance.compareTo(BigDecimal.ZERO) < 0) {
									params.put("reason", "Credit for " + String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd() + " for changing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier() + " with " + String.format("%.2f", balance) + " " + consumerAcct.getCurrencyCd() + " balance from Operator Serviced to USAT Serviced");						
									Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
									msg += ",+EFT+" + ConvertUtils.getStringSafely(result[1], "") + "+credited";
								}
							}
						}
					}
					conn.commit();
				}
				else if (myAction.equalsIgnoreCase("Toggle"))
				{
					//get the current values from the db
					consumerAcct = ConsumerAction.getConsumerAccountByConsumerAccountId(new Long(consumerAcctId).toString());
					String active = form.getString("consumerAcctActiveFlag", false);
					consumerAcct.setConsumerAcctActiveFlag(StringHelper.isBlank(active) || active.equalsIgnoreCase("Y") ? "N" : "Y");
					DataLayerMgr.executeUpdate("UPDATE_CONSUMER_ACCOUNT", consumerAcct, true);
					msg = "Consumer+account+active+status+toggled";
				}
				else if (myAction.equalsIgnoreCase("Close Account"))
				{
					BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
					Object[] result = DataLayerMgr.executeCall("CLOSE_CONSUMER_ACCT", new Object[] {consumerAcctId, user.getEmailAddress()}, true);
					msg = "Account+closed";
					if (result != null) {
						BigDecimal refundAmt = ConvertUtils.convert(BigDecimal.class, result[1]);
						BigDecimal eftCreditAmt = ConvertUtils.convert(BigDecimal.class, result[2]);
						if (refundAmt.compareTo(BigDecimal.ZERO) == 1)
							msg += ",+created+refund+for+" + String.format("%.2f", refundAmt);
						if (eftCreditAmt.compareTo(BigDecimal.ZERO) == 1)
							msg += ",+credited+EFT+" + result[3] + " for " + String.format("%.2f", eftCreditAmt);
					}
				} 
				else if (myAction.equalsIgnoreCase("Reopen Account"))
				{
					int updateCount= DataLayerMgr.executeUpdate("REOPEN_CONSUMER_ACCT", new Object[] {consumerAcctId}, true);
					if(updateCount==1){
						msg = "Account+reopened";
					}else{
						msg = "Account+failed.+Please+retry";
					}
				}
				form.setRedirectUri(new StringBuilder("/editConsumerAcct.i?consumer_acct_id=").append(consumerAcctId).append("&msg=").append(msg).toString());
				return;
			}
			request.setAttribute(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ACCT_ID, consumerAcctId);
		}
		catch (SQLException e)
		{
			if(e.getErrorCode()==20208) {
				String msg="Negative balance can be enabled only for Prepaid Operator Serviced accounts. Please go back and try again.";
				form.setRedirectUri(new StringBuilder("/editConsumerAcct.i?consumer_acct_id=").append(consumerAcctId).append("&errorMessage=").append(msg).toString());
 				return;
			}else{
				throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
			}
		}
		catch (Exception e)
		{
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		} finally {
			if (!success)
				ProcessingUtils.rollbackDbConnection(log, conn);			
			ProcessingUtils.closeDbConnection(log, conn);
		}
	}
}
