/*
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.LocationActions;
import com.usatech.dms.model.Consumer;
import com.usatech.layers.common.util.StringHelper;

public class EditConsumerFuncStep extends AbstractStep
{

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String myAction = form.getString("action", false);
        String temp = form.getString("consumer_id", false);
        String countryCd = form.getString("countryCode", false);
        String stateCd = form.getString("stateCode", false);
        
        try
        {
        	
        	long consumer_id = -1;
            if (temp != null && temp.length() > 0)
            {
            	consumer_id = Long.parseLong(temp);
            }
            String errorMessage = null;
            Consumer consumer = null;
            if (myAction.equalsIgnoreCase("Delete"))
            {
                
                errorMessage = "Can not delete this consumer, consumer delete function not operational at this time.";
                request.setAttribute("errorMessage", errorMessage);
                return;
                        
            }
            else
            {

                if (myAction.equalsIgnoreCase("Update"))
                {
                	
                	consumer = new Consumer();
                    BeanUtils.populate(consumer, request.getParameterMap());
                    consumer.setId(new Long(consumer_id));
                    
                    /**
                     * DA spits out error when country is not blank and state is undefined, put that into nicer error message.
                     */
                    
                	errorMessage = validateCountryState(request, countryCd, stateCd,
							consumer);
                	if(!StringHelper.isBlank(errorMessage)){
                		request.setAttribute("errorMessage", errorMessage);
                        return;
                	}
                	DataLayerMgr.executeUpdate("UPDATE_CONSUMER", consumer, true);
                	form.setRedirectUri(new StringBuilder("/editConsumer.i?consumer_id=").append(consumer_id).append("&msg=").append("Consumer+updated").toString());
                	return;
                }

            }
            request.setAttribute("consumer_id", consumer_id);
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }
    
    /**
     * Validate the country and state.
     * @param request
     * @param countryCd
     * @param stateCd
     * @param consumer
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws ConvertException
     */
	private String validateCountryState(HttpServletRequest request,
			String countryCd, String stateCd, Consumer consumer)
			throws SQLException, DataLayerException, ConvertException {
		
		if((!StringHelper.isBlank(countryCd)) && (!"Undefined".equalsIgnoreCase(countryCd))){
			Results result = DataLayerMgr.executeQuery("GET_STATES_BY_COUNTRY", new Object[] {countryCd}, false);
			if(result.next()){
				
				if("Undefined".equalsIgnoreCase(stateCd) || (!LocationActions.validStatePerCountry(new Object[]{stateCd, countryCd}))){
					return "State code \"" + stateCd + "\" does not match with country code \"" + countryCd + "\"! Please choose a valid state for this country.";
			        
				}
					
			}else{
				if((!StringHelper.isBlank(stateCd)) && ("Undefined".equalsIgnoreCase(stateCd))){
					consumer.setStateCode(null);
				}else{
					return "State code \"" + stateCd + "\" does not match with country code \"" + countryCd + "\"! Please choose a valid state for this country.";
			        
				}
			}
		}else{
			if((StringHelper.isBlank(countryCd)) || ("Undefined".equalsIgnoreCase(countryCd)))
				consumer.setCountryCode(null);
			if((StringHelper.isBlank(stateCd)) || ("Undefined".equalsIgnoreCase(stateCd)))
				consumer.setStateCode(null);
			
		}
		return null;
	}

}
