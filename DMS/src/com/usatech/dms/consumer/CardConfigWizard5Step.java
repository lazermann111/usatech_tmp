package com.usatech.dms.consumer;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.model.ConsumerAccount;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class CardConfigWizard5Step extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {    	
    	StringBuilder out = new StringBuilder();
    	String action = form.getString("action", false);
    	if(!"Finish >".equalsIgnoreCase(action)){
             request.setAttribute("errorMessage", "<br/><br/><b><font color='red'>Undefined Action: *" + action + "* </font></b><br/><br/>");
             return;
    	}
    	
    	String includeAccountIds = form.getString("include_account_ids", false);
    	if(StringHelper.isBlank(includeAccountIds)){
            request.setAttribute("errorMessage", "<br/><br/><b><font color='red'>No cards selected!</font></b><br/><br/>");
            return;
    	}
    	
    	log.info("Bulk card configuration is starting...");
    	
    	BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
    	int card_update_count = 0;
    	int account_closed_count = 0;
    	int error_count = 0;
    	
    	Connection conn = null;
        boolean success = false;
    	try {
	    	long corpCustomerId = form.getLong("corp_customer_id", false, 0);
	    	String corpCustomerName = form.getStringSafely("corp_customer_name", "");
	    	long locationId = form.getLong("location_id", false, 0);
	    	String locationName = form.getStringSafely("location_name", "");
	    	String activationStatus = form.getStringSafely("activation_status", "");
	    	int consumerAcctSubTypeId = form.getInt("consumer_acct_sub_type_id", false, 0);
	    	String consumerAcctSubTypeName = form.getStringSafely("consumer_acct_sub_type_name", "");
	    	BigDecimal balanceChange = ConvertUtils.convert(BigDecimal.class, form.getStringSafely("balance_change", "0"));
	    	String allowNegativeBalance = form.getStringSafely("allow_negative_balance", "");
	    	String deactivationDate = form.getStringSafely("consumer_acct_deactivation_date", "");
			
	    	boolean updateAccount = false;
			boolean closeAccount = false;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("refundIssueBy", user.getEmailAddress());
			params.put("username", user.getEmailAddress());
			
			out.append("\nSelected Account IDs: ").append(includeAccountIds.replace(",", ", ")).append("\n");
			if (corpCustomerId > 0) {
				updateAccount = true;
				params.put("corpCustomerId", corpCustomerId);
				out.append("Selected Reporting Customer ID: ").append(corpCustomerId).append(", Name: ").append(corpCustomerName).append("\n");
			}
			if (locationId > 0) {
				updateAccount = true;
				params.put("locationId", locationId);
				out.append("Selected Authorization Location ID: ").append(locationId).append(", Name: ").append(locationName).append("\n");
			}
			if (!StringHelper.isBlank(activationStatus)) {
				closeAccount = "Close Account".equalsIgnoreCase(activationStatus);
				if ("Activate".equalsIgnoreCase(activationStatus)) {
					updateAccount = true;
					params.put("consumerAcctActiveFlag", "Y");
				} else if ("Deactivate".equalsIgnoreCase(activationStatus)) {
					updateAccount = true;
					params.put("consumerAcctActiveFlag", "N");
				}
				out.append("Selected Activation Status: ").append(activationStatus).append("\n");
			}
			if (consumerAcctSubTypeId > 0) {
				updateAccount = true;
				params.put("consumerAcctSubTypeId", consumerAcctSubTypeId);
				out.append("Selected Account Subtype ID: ").append(consumerAcctSubTypeId).append(", Name: ").append(consumerAcctSubTypeName).append("\n");
			}
			if (balanceChange.compareTo(BigDecimal.ZERO) != 0) {
				updateAccount = true;
				params.put("consumerAcctBalanceChange", balanceChange);
				out.append("Selected Balance Change: ").append(String.format("%.2f", balanceChange)).append("\n");
			}
			if (!StringUtils.isBlank(allowNegativeBalance)) {
				updateAccount = true;
				params.put("allowNegativeBalance", allowNegativeBalance);
				out.append("Selected Allow Negative Balance: ").append(allowNegativeBalance).append("\n");
			}
			if (!StringUtils.isBlank(deactivationDate)) {
				updateAccount = true;
				
				Calendar exp_cal = null;
				String exp_year = "20" + deactivationDate.substring(0, 2);
				String exp_month = deactivationDate.substring(2, 4);
		    	exp_cal = new GregorianCalendar(new Integer(exp_year).intValue(), new Integer(exp_month).intValue()-1, 1); 
		    	exp_cal = new GregorianCalendar(new Integer(exp_year).intValue(), new Integer(exp_month).intValue()-1, exp_cal.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59); 
				params.put("consumer_acct_deactivation_ts", exp_cal);
				out.append("Selected Deactivation Date: ").append(exp_cal.getTime().toString()).append("\n");
			}
			out.append("\n\n");
    	 
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();
    		ConsumerAccount consumerAcct = new ConsumerAccount();
    		Map<String, Object> cardListParams = new HashMap<String, Object>();
    		cardListParams.put("consumerAcctIds", includeAccountIds);
    		conn = DataLayerMgr.getConnection("OPER");
    		Results results = DataLayerMgr.executeQuery(conn, "GET_BULK_CARD_LIST", cardListParams);
    		while (results.next()) {    			
	        	boolean cardSuccess = false;
	        	long startTsMs = System.currentTimeMillis();
	        	results.fillBean(consumerAcct);
	        	String accountId = String.valueOf(consumerAcct.getConsumerAcctId());
	        	String cardId = String.valueOf(consumerAcct.getConsumerAcctIdentifier());
	        	if (cardId == null || "0".equalsIgnoreCase(cardId) || "null".equalsIgnoreCase(cardId))
	        		cardId = "";
	        	CARD: while(true) {
	        		params.put("consumerAcctId", accountId);
	        		if (corpCustomerId > 0)
	        			params.put("corpCustomerId", corpCustomerId);
	        		else if (consumerAcct.getCorpCustomerId() != null)
	        			params.put("corpCustomerId", consumerAcct.getCorpCustomerId());
	        		
	        		if (updateAccount) {
		        		try {
			                BigDecimal oldPromoBalance = new BigDecimal(consumerAcct.getConsumerAcctPromoBalance());
		        			
		        			DataLayerMgr.executeUpdate(conn, "UPDATE_CONSUMER_ACCOUNT", params);
		        			card_update_count++;
							BigDecimal effectiveBalanceChange = ConvertUtils.convert(BigDecimal.class, params.get("effectiveConsumerAcctBalanceChange"));
		        			out.append("Updated card Account ID: ").append(accountId).append(", Card ID: ").append(cardId);
		        			BigDecimal promoBalance = oldPromoBalance.add(effectiveBalanceChange);
		        					        				
		        			if (consumerAcct.getConsumerAcctTypeId() == 3) {
		        				// Prepaid Account
		        				long oldCorpCustomerId = consumerAcct.getCorpCustomerId() == null ? 0 : consumerAcct.getCorpCustomerId().longValue();
		        				params.put("currencyCd", consumerAcct.getCurrencyCd());
		        				if (consumerAcct.getConsumerAcctSubTypeId() == 1) {
		        					// USAT Serviced
			        				if (corpCustomerId > 0 && corpCustomerId != oldCorpCustomerId) {
			        					if (oldPromoBalance.compareTo(BigDecimal.ZERO) > 0) {
				                			params.put("corpCustomerId", oldCorpCustomerId);
				                			params.put("amount", consumerAcct.getConsumerAcctPromoBalance());
					                		params.put("reason", "Credit for " + String.format("%.2f", oldPromoBalance) + " " + consumerAcct.getCurrencyCd() + " for removing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier());	                	
						                	Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
						                	out.append(", EFT " + ConvertUtils.getStringSafely(result[1], "") + " credited");
				                		}
				                		if (promoBalance.compareTo(BigDecimal.ZERO) > 0) {
				                			params.put("corpCustomerId", corpCustomerId);
				                			params.put("amount", promoBalance.negate());
					                		params.put("reason", "Charge for " + String.format("%.2f", promoBalance) + " " + consumerAcct.getCurrencyCd() + " for adding the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier());	                	
						                	Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
						                	out.append(", EFT " + ConvertUtils.getStringSafely(result[1], "") + " charged");
				                		}
			        				} else if (effectiveBalanceChange.compareTo(BigDecimal.ZERO) != 0) {
			        					out.append(", balance change: ").append(String.format("%.2f", effectiveBalanceChange));
			        					params.put("corpCustomerId", corpCustomerId > 0 ? corpCustomerId : consumerAcct.getCorpCustomerId());
			        					params.put("amount", effectiveBalanceChange.negate());
			        					if (effectiveBalanceChange.compareTo(BigDecimal.ZERO) > 0) {					                		
					                		params.put("reason", new StringBuilder("Charge for ").append(String.format("%.2f", effectiveBalanceChange.abs())).append(" ").append(consumerAcct.getCurrencyCd()).append(" balance increase on the prepaid card # ").append(consumerAcct.getConsumerAcctCd()).append(", card ID ").append(cardId).toString());	                	
						                	Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
						                	out.append(", EFT ").append(ConvertUtils.getStringSafely(result[1], "")).append(" charged");
					                	} else if (effectiveBalanceChange.compareTo(BigDecimal.ZERO) < 0) {
					                		params.put("reason", new StringBuilder("Charge for ").append(String.format("%.2f", effectiveBalanceChange.abs())).append(" ").append(consumerAcct.getCurrencyCd()).append(" balance decrease on the prepaid card # ").append(consumerAcct.getConsumerAcctCd()).append(", card ID ").append(cardId).toString());	                	
						                	Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
						                	out.append(", EFT ").append(ConvertUtils.getStringSafely(result[1], "")).append(" credited");
					                	}
			        				}
				                }
		        				BigDecimal balance = new BigDecimal(consumerAcct.getConsumerAcctBalance()).add(effectiveBalanceChange);
		        				if (balance.compareTo(BigDecimal.ZERO) != 0) {
									if (consumerAcct.getConsumerAcctSubTypeId() == 1 && consumerAcctSubTypeId == 2) {
										// from USAT Serviced to Operator Serviced
										out.append(", subtype change");
										params.put("corpCustomerId", corpCustomerId > 0 ? corpCustomerId : consumerAcct.getCorpCustomerId());
					                	params.put("amount", balance);
					                	if (balance.compareTo(BigDecimal.ZERO) < 0) {		                		
						                	params.put("reason", "Charge for " + String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd() + " for changing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier() + " with " + String.format("%.2f", balance) + " " + consumerAcct.getCurrencyCd() + " balance from USAT Serviced to Operator Serviced");	                	
						                	Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
						                	out.append(", EFT ").append(ConvertUtils.getStringSafely(result[1], "")).append(" charged for ").append(String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd());
					                	} else if (balance.compareTo(BigDecimal.ZERO) > 0) {
					                		params.put("reason", "Credit for " + String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd() + " for changing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier() + " with " + String.format("%.2f", balance) + " " + consumerAcct.getCurrencyCd() + " balance from USAT Serviced to Operator Serviced");	                	
						                	Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
						                	out.append(", EFT ").append(ConvertUtils.getStringSafely(result[1], "")).append(" credited for ").append(String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd());
					                	}
									} else if (consumerAcct.getConsumerAcctSubTypeId() == 2 && consumerAcctSubTypeId == 1) {
										// from Operator Serviced to USAT Serviced
										out.append(", subtype change");
										params.put("corpCustomerId", corpCustomerId > 0 ? corpCustomerId : consumerAcct.getCorpCustomerId());
					                	params.put("amount", balance.negate());
					                	if (balance.compareTo(BigDecimal.ZERO) > 0) {		                		
						                	params.put("reason", "Charge for " + String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd() + " for changing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier() + " with " + String.format("%.2f", balance) + " " + consumerAcct.getCurrencyCd() + " balance from Operator Serviced to USAT Serviced");	                	
						                	Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
						                	out.append(", EFT ").append(ConvertUtils.getStringSafely(result[1], "")).append(" charged for ").append(String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd());
					                	} else if (balance.compareTo(BigDecimal.ZERO) < 0) {
					                		params.put("reason", "Credit for " + String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd() + " for changing the prepaid card # " + consumerAcct.getConsumerAcctCd() + ", card ID " + consumerAcct.getConsumerAcctIdentifier() + " with " + String.format("%.2f", balance) + " " + consumerAcct.getCurrencyCd() + " balance from Operator Serviced to USAT Serviced");	                	
						                	Object[] result = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", params);
						                	out.append(", EFT ").append(ConvertUtils.getStringSafely(result[1], "")).append(" credited for ").append(String.format("%.2f", balance.abs()) + " " + consumerAcct.getCurrencyCd());
					                	}
									}
								}
		        			}
		        			
		        			out.append("\n");
		        		} catch (Exception e) {
				        	out.append("<br/><font color=\"red\"><b>Error updating card, Account ID: ").append(accountId).append(", Card ID: ").append(consumerAcct.getConsumerAcctIdentifier()).append(", error: ").append(e.getMessage()).append("</b></font>\n");
		        			log.error(new StringBuilder("Error updating card, Account ID: ").append(accountId).append(", Card ID: ").append(consumerAcct.getConsumerAcctIdentifier()).toString(), e);
		        			break CARD;
		        		}
	        		}
	        		
	        		if (closeAccount) {
	        			try {
		        			Object[] result = DataLayerMgr.executeCall(conn, "CLOSE_CONSUMER_ACCT", params);
		        			account_closed_count++;
		        			out.append("Closed account, Account ID: ").append(accountId).append(", Card ID: ").append(cardId);
			            	if (result != null) {
			            		BigDecimal refundAmt = ConvertUtils.convert(BigDecimal.class, result[1]);
			            		BigDecimal eftCreditAmt = ConvertUtils.convert(BigDecimal.class, result[2]);
			            		if (refundAmt.compareTo(BigDecimal.ZERO) == 1)
			            			out.append(", created refund for ").append(String.format("%.2f", refundAmt));
			            		if (eftCreditAmt.compareTo(BigDecimal.ZERO) == 1)
			            			out.append(", credited EFT ").append(result[3]).append(" for ").append(String.format("%.2f", eftCreditAmt));
			            	}
			            	out.append("\n");
	        			} catch (Exception e) {
				        	out.append("<font color=\"red\"><b>Error closing account, Account ID: ").append(accountId).append(", Card ID: ").append(consumerAcct.getConsumerAcctIdentifier()).append(", error: ").append(e.getMessage()).append("</b></font>\n");
		        			log.error(new StringBuilder("Error closing account, Account ID: ").append(accountId).append(", Card ID: ").append(consumerAcct.getConsumerAcctIdentifier()).toString(), e);
		        			break CARD;
		        		}
	        		}
	        			        			        						    	
	        		cardSuccess = true;
			    	break CARD;
	        	}
	    		if(cardSuccess) {
    				conn.commit();
    				out.append("\n");
					WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, startTsMs, "Finish >", "consumer_acct", accountId, log);
    			}else{    				
    				ProcessingUtils.rollbackDbConnection(log, conn);
    				error_count++;
    				out.append("<font color=\"red\"><b>Operation rolled back for Account ID: ").append(accountId).append(", Card ID: ").append(consumerAcct.getConsumerAcctIdentifier()).append("</b></font>\n\n");
			    	log.error(new StringBuilder("Operation rolled back for Account ID: ").append(accountId).append(", Card ID: ").append(consumerAcct.getConsumerAcctIdentifier()).toString());
    			}
	        }	        
	        success = true;
	    }catch(Exception e){
    		throw new ServletException("Error in bulk configuration wizard", e);
		}finally{
			if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
		}
		
		out.append("\nOperation Summary\n\n");
		if (card_update_count > 0)
			out.append("Cards Updated: ").append(card_update_count).append("\n");
		if (account_closed_count > 0)
			out.append("Accounts Closed: ").append(account_closed_count).append("\n");
		if (error_count > 0)
			out.append("<font color=\"red\"><b>Errors Occurred: ").append(error_count).append("</b></font>\n");

		String changes = form.getString("changes", true);
        StringBuilder email = new StringBuilder("<html><head>").append(Helper.getBasicStyle()).append("</head><body>").append(changes)
        		.append("<br/><b>Results:</b><br/><br/><pre>").append(out.toString()).append("</pre></body></html>");
        try {
        	Helper.sendEmail(Helper.DMS_EMAIL, user.getEmailAddress(), "Card Configuration Wizard Confirmation", email.toString(), Helper.DMS_EMAIL_CONTENT_TYPE_HTML);
        	out.append("\nEmailed confirmation to ").append(user.getEmailAddress()).append("\n");
        } catch (MessagingException e) {
        	out.append("<font color=\"red\">Error emailing confirmation: ").append(e.getMessage()).append("</font>\n");
        	log.error("Error emailing confirmation", e);
        }
		request.setAttribute("bulk_out", out.toString());
		log.info("Bulk card configuration finished");
    }
}
